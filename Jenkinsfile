pipeline {

    agent any

    options {
        timeout(time: 30, unit: 'MINUTES')
        buildDiscarder(logRotator(numToKeepStr: '5'))
        disableConcurrentBuilds()
    }

    tools {
        nodejs 'Node10'
    }

    environment {
        NEXUS_GROUPID='vznl'
        NEXUS_ENDPOINT='https://nexus.dynacommercelab.com'
        NEXUS_CRED = credentials('Nexus')
    }

    stages{

        stage('Install dependencies') {
            steps {
                parallel (
                    "Composer": {
                        sh '''
                            composer install  --prefer-dist --optimize-autoloader --no-interaction
                            composer run-script post-install-cmd -vvv -- --redeploy # initiate a redeploy of magento modules
                        '''
                    },
                    "NPM": {
                        sh '''
                            npm install
                            npm run build
                            npm run build:translations
                        '''
                    }
                )
            }
        }

        stage('Code checks') {
            steps {
                parallel (
                    "PHP Coding Standards / Local": {
                        sh '''
                            echo Running php code sniffer...
                            ./dev/phpcs.sh app/code/local/Vznl 200 900
                        '''
                    },

                    "PHP Coding Standards / Dyna": {
                        sh '''
                            echo Running php code sniffer...
                            ./dev/phpcs.sh app/code/community/Dyna/Address 1 5
                            ./dev/phpcs.sh app/code/community/Dyna/Adminhtml 0 2
                            ./dev/phpcs.sh app/code/community/Dyna/Agent 15 82
                            ./dev/phpcs.sh app/code/community/Dyna/AgentDE 20 186
                            ./dev/phpcs.sh app/code/community/Dyna/Audit 5 0
                            ./dev/phpcs.sh app/code/community/Dyna/Bundles 18 101
                            ./dev/phpcs.sh app/code/community/Dyna/Cable 4 13
                            ./dev/phpcs.sh app/code/community/Dyna/Cache 4 2
                            ./dev/phpcs.sh app/code/community/Dyna/Catalog 17 57
                            ./dev/phpcs.sh app/code/community/Dyna/CatalogLog 1 6
                            ./dev/phpcs.sh app/code/community/Dyna/Checkout 13 260
                            ./dev/phpcs.sh app/code/community/Dyna/Configurator 14 105 # treshold baseline 11 / 104
                            ./dev/phpcs.sh app/code/community/Dyna/Core 9 8
                            ./dev/phpcs.sh app/code/community/Dyna/Customer 12 113
                            ./dev/phpcs.sh app/code/community/Dyna/Fixed 4 6
                            ./dev/phpcs.sh app/code/community/Dyna/Import 33 28
                            ./dev/phpcs.sh app/code/community/Dyna/IpSecurity 15 15
                            ./dev/phpcs.sh app/code/community/Dyna/Job 10 0
                            ./dev/phpcs.sh app/code/community/Dyna/LoginSecurity 1 37
                            ./dev/phpcs.sh app/code/community/Dyna/Magemonolog 1 1
                            ./dev/phpcs.sh app/code/community/Dyna/MixMatch 10 22
                            ./dev/phpcs.sh app/code/community/Dyna/Mobile 3 16
                            ./dev/phpcs.sh app/code/community/Dyna/MultiMapper 6 34
                            ./dev/phpcs.sh app/code/community/Dyna/Operator 1 0
                            ./dev/phpcs.sh app/code/community/Dyna/Page 1 0
                            ./dev/phpcs.sh app/code/community/Dyna/PriceRules 7 32
                            ./dev/phpcs.sh app/code/community/Dyna/ProductMatchRule 17 108
                            ./dev/phpcs.sh app/code/community/Dyna/RuleMatch 0 4
                            ./dev/phpcs.sh app/code/community/Dyna/Sandbox 18 34
                            ./dev/phpcs.sh app/code/community/Dyna/Service 7 5
                            ./dev/phpcs.sh app/code/community/Dyna/Superorder 28 53
                            ./dev/phpcs.sh app/code/community/Dyna/Validators 0 4
                        '''
                    },

                    "PHP Coding Standards / Omnius": {
                        sh '''
                            echo Running php code sniffer...
                            ./dev/phpcs.sh app/code/community/Omnius/Audit 19 9
                            ./dev/phpcs.sh app/code/community/Omnius/Bundles 24 34
                            ./dev/phpcs.sh app/code/community/Omnius/Campaign 0 3
                            ./dev/phpcs.sh app/code/community/Omnius/Catalog 2 44
                            ./dev/phpcs.sh app/code/community/Omnius/Checkout 17 245
                            ./dev/phpcs.sh app/code/community/Omnius/Configurator 2 37
                            ./dev/phpcs.sh app/code/community/Omnius/Core 10 5
                            ./dev/phpcs.sh app/code/community/Omnius/Ctn 1 4
                            ./dev/phpcs.sh app/code/community/Omnius/Customer 4 72
                            ./dev/phpcs.sh app/code/community/Omnius/Field 0 15
                            ./dev/phpcs.sh app/code/community/Omnius/Import 2 9
                            ./dev/phpcs.sh app/code/community/Omnius/MixMatch 2 7
                            ./dev/phpcs.sh app/code/community/Omnius/MultiMapper 6 24
                            ./dev/phpcs.sh app/code/community/Omnius/Operator 0 10
                            ./dev/phpcs.sh app/code/community/Omnius/Package 2 46
                            ./dev/phpcs.sh app/code/community/Omnius/PriceRules 14 43
                            ./dev/phpcs.sh app/code/community/Omnius/ProductMatchRule 9 44
                            ./dev/phpcs.sh app/code/community/Omnius/Proxy 14 5
                            ./dev/phpcs.sh app/code/community/Omnius/RuleMatch 1 8
                            ./dev/phpcs.sh app/code/community/Omnius/Sandbox 47 80
                            ./dev/phpcs.sh app/code/community/Omnius/Service 19 4
                            ./dev/phpcs.sh app/code/community/Omnius/Superorder 0 60
                            ./dev/phpcs.sh app/code/community/Omnius/Validators 20 23
                        '''
                    },

                    "PHP Mess Detector": {
                        sh '''
                            echo Running php mess detector...
                        '''
                    }
                )
            }
        }

        stage('Unit Tests') {
            steps {
                parallel (
                    "PHP": {
                        echo "Running PHPUnit tests"
                        sh '''
                            composer test-coverage
                        '''
                    },

                    "ReactJS": {
                        echo "Running NPM tests"
                        sh '''
                            npm run test
                        '''
                    }
                )
            }
        }

        stage('Sonarqube') {
            when {
                expression {
                    BRANCH_NAME ==~ /develop/
                }
            }
            steps {
                echo 'Analyzing code'
                withSonarQubeEnv('My SonarQube Server') {
                    script {
                        def scannerHome = tool 'SonarQube Scanner';
                        sh "${scannerHome}/bin/sonar-scanner"
                    }
                }
                //timeout(time: 10, unit: 'MINUTES') { // Just in case something goes wrong, pipeline will be killed after a timeout
                //    script {
                //        def qg = waitForQualityGate() // Reuse taskId previously collected by withSonarQubeEnv
                //        if (qg.status != 'OK') {
                //            error "Pipeline aborted due to quality gate failure: ${qg.status}"
                //      }
                //   }
                //}
            }
        }

        stage('Build Dev') {
            when { expression { BRANCH_NAME ==~ /develop(-.*)?/ } }
            steps {
                sh '''
                    ./package/build.sh
                '''
            }
        }

        stage('Package Dev') {
            when { expression { BRANCH_NAME ==~ /develop(-.*)?/ } }
            steps {
                sh '''
                    ./package/package.sh SNAPSHOT rpm
                    ./package/package.sh SNAPSHOT deb
                    ./package/package.sh SNAPSHOT zpk
                '''
            }
        }

        stage('Publish dev artifacts') {
            when { expression { BRANCH_NAME ==~ /develop(-.*)?/ } }
            steps {
                parallel (
                    "Publish DEB": {
                        echo "Publish ${BUILD_NUMBER}"
                        sh '''
                            for package in ./build/deb/*.deb; do
                                PACKAGE_NAME=$(basename $package)
                                curl -L -v --user ${NEXUS_CRED_USR}:${NEXUS_CRED_PSW} --upload-file ./build/deb/${PACKAGE_NAME} ${NEXUS_ENDPOINT}/repository/deb-packages/${NEXUS_GROUPID}/${PACKAGE_NAME}
                            done
                        '''
                    },

                    "Publish RPM": {
                        echo "Publish ${BUILD_NUMBER}"
                        sh '''
                            for package in ./build/rpm/*.rpm; do
                                PACKAGE_NAME=$(basename $package)
                                curl -L -v --user ${NEXUS_CRED_USR}:${NEXUS_CRED_PSW} --upload-file ./build/rpm/${PACKAGE_NAME} ${NEXUS_ENDPOINT}/repository/rpm-packages/${NEXUS_GROUPID}/${PACKAGE_NAME}
                            done
                        '''
                    },

                    "Publish ZPK": {
                        echo "Publish ${BUILD_NUMBER}"
                        sh '''
                            for package in ./build/zpk/*.zpk; do
                                PACKAGE_NAME=$(basename $package)
                                curl -L -v --user ${NEXUS_CRED_USR}:${NEXUS_CRED_PSW} --upload-file ./build/zpk/${PACKAGE_NAME} ${NEXUS_ENDPOINT}/repository/zpk-packages/${NEXUS_GROUPID}/${PACKAGE_NAME}
                            done
                        '''
                    },

                    "Publish DB-Seed": {
                        echo "Publish ${BUILD_NUMBER}"
                        sh '''
                            git-lfs pull
                            PACKAGE_VERSION=$(composer config version)
                            PACKAGE_NAME="$(composer config name)-${PACKAGE_VERSION}-SNAPSHOT.sql.gz"
                            curl -L -v --user ${NEXUS_CRED_USR}:${NEXUS_CRED_PSW} --upload-file ./var/data/seed.sql.gz ${NEXUS_ENDPOINT}/repository/db-seeds/${NEXUS_GROUPID}/${PACKAGE_NAME}
                        '''
                    }
                )
            }
        }

        stage('Build Release') {
            when { expression { BRANCH_NAME ==~ /release[\/-][0-9]+\.[0-9]+\.[0-9]+(-.*)?/ } }
            steps {
                sh '''
                    ./package/build.sh --obfuscate
                '''
            }
        }

        stage('Package Release') {
            when { expression { BRANCH_NAME ==~ /release[\/-][0-9]+\.[0-9]+\.[0-9]+(-.*)?/ } }
            steps {
                sh '''
                    ./package/package.sh ${BUILD_NUMBER} rpm
                    ./package/package.sh ${BUILD_NUMBER} deb
                    ./package/package.sh ${BUILD_NUMBER} zpk
                '''
            }
        }

        stage('Publish artifacts') {
            when { expression { BRANCH_NAME ==~ /release[\/|-]\d.\d.\d(-.*)?/  } }
            steps {
                parallel (
                    "Publish DEB": {
                        echo "Publish ${BUILD_NUMBER}"
                        sh '''
                            for package in ./build/deb/*.deb; do
                                PACKAGE_NAME=$(basename $package)
                                curl -L -v --user ${NEXUS_CRED_USR}:${NEXUS_CRED_PSW} --upload-file ./build/deb/${PACKAGE_NAME} ${NEXUS_ENDPOINT}/repository/deb-packages/${NEXUS_GROUPID}/${PACKAGE_NAME}
                            done
                        '''
                    },

                    "Publish RPM": {
                        echo "Publish ${BUILD_NUMBER}"
                        sh '''
                            for package in ./build/rpm/*.rpm; do
                                PACKAGE_NAME=$(basename $package)
                                curl -L -v --user ${NEXUS_CRED_USR}:${NEXUS_CRED_PSW} --upload-file ./build/rpm/${PACKAGE_NAME} ${NEXUS_ENDPOINT}/repository/rpm-packages/${NEXUS_GROUPID}/${PACKAGE_NAME}
                            done
                        '''
                    },

                    "Publish ZPK": {
                        echo "Publish ${BUILD_NUMBER}"
                        sh '''
                            for package in ./build/zpk/*.zpk; do
                                PACKAGE_NAME=$(basename $package)
                                curl -L -v --user ${NEXUS_CRED_USR}:${NEXUS_CRED_PSW} --upload-file ./build/zpk/${PACKAGE_NAME} ${NEXUS_ENDPOINT}/repository/zpk-packages/${NEXUS_GROUPID}/${PACKAGE_NAME}
                            done
                        '''
                    },

                    "Publish DB-Seed": {
                        echo "Publish ${BUILD_NUMBER}"
                        sh '''
                            git-lfs pull
                            PACKAGE_VERSION=$(composer config version)
                            PACKAGE_NAME="$(composer config name)-${PACKAGE_VERSION}-1.sql.gz"
                            curl -L -v --user ${NEXUS_CRED_USR}:${NEXUS_CRED_PSW} --upload-file ./var/data/seed.sql.gz ${NEXUS_ENDPOINT}/repository/db-seeds/${NEXUS_GROUPID}/${PACKAGE_NAME}
                        '''
                    }
                )
            }
        }

    }

    post {
        always {
            script {
                if (BRANCH_NAME.startsWith('PR')) {
                    env.channel = '#vznl-dev-pr'
                } else {
                    env.channel = '#vznl-dev-ci'
                }
            }

            echo 'Publishing results...'
            junit 'reports/phpunit/junit.xml'
            publishHTML target: [
                allowMissing         : false,
                alwaysLinkToLastBuild: false,
                keepAll              : true,
                reportDir            : 'reports/phpunit/html',
                reportFiles          : 'index.html',
                reportName           : "PHPUnit code coverage results"
            ]

            publishHTML target: [
                allowMissing         : false,
                alwaysLinkToLastBuild: false,
                keepAll              : true,
                reportDir            : 'reports/react/coverage/lcov-report',
                reportFiles          : 'index.html',
                reportName           : "React code coverage results"
            ]
        }

        success {
            echo 'Success!'
            slackSend channel: env.channel, color: "good", message: "Build Passed: <${env.JOB_DISPLAY_URL}|${env.JOB_NAME}> <${env.RUN_DISPLAY_URL}|#${env.BUILD_NUMBER}>"
        }

        failure {
            echo 'Failure!'
            slackSend channel: env.channel, color: "danger", message: "Build Failed: <${env.JOB_DISPLAY_URL}|${env.JOB_NAME}> <${env.RUN_DISPLAY_URL}|#${env.BUILD_NUMBER}>"
        }
    }
}