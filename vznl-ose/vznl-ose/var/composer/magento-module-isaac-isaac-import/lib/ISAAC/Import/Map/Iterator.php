<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
/**
 * Iterator that applies a callback function to every element of the supplied iterator
 */
class ISAAC_Import_Map_Iterator implements OuterIterator
{
    /** @var Iterator $innerIterator */
    protected $innerIterator;

    /** @var callable $callback */
    protected $callback;

    public function __construct(Iterator $innerIterator, callable $callback)
    {
        $this->innerIterator = $innerIterator;
        $this->callback = $callback;
    }

    public function getInnerIterator()
    {
        return $this->innerIterator;
    }

    public function next()
    {
        $this->getInnerIterator()->next();
    }

    public function rewind()
    {
        $this->getInnerIterator()->rewind();
    }

    public function valid()
    {
        return $this->getInnerIterator()->valid();
    }

    public function current()
    {
        return call_user_func($this->callback, $this->getInnerIterator()->current(), $this->getInnerIterator()->key());
    }

    public function key()
    {
        return $this->getInnerIterator()->key();
    }
}
