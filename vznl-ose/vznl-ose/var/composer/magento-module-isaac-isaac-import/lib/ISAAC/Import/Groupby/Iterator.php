<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
/**
 * Iterator to group iterator values by a grouping function
 */
class ISAAC_Import_Groupby_Iterator implements OuterIterator
{
    /** @var Iterator $innerIterator */
    protected $innerIterator;

    /** @var callable $groupingFunction */
    protected $groupingFunction;

    /** @var int $groupKey */
    protected $groupKey = -1;

    /** @var array $groupValues */
    protected $groupValues = [];

    /**
     * @param Iterator $innerIterator
     * @param callable $groupingFunction
     */
    public function __construct(Iterator $innerIterator, callable $groupingFunction)
    {
        $this->innerIterator = $innerIterator;
        $this->groupingFunction = $groupingFunction;
    }

    /**
     * @return Iterator
     */
    public function getInnerIterator()
    {
        return $this->innerIterator;
    }

    /**
     *
     */
    public function rewind()
    {
        $this->innerIterator->rewind();
        $this->groupKey = -1;
        $this->groupValues = [];
        $this->calculateNextGroupValues();
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return (!empty($this->groupValues)) || $this->innerIterator->valid();
    }

    /**
     * @return array
     */
    public function current()
    {
        return $this->groupValues;
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->groupKey;
    }

    /**
     *
     */
    public function next()
    {
        $this->calculateNextGroupValues();
    }

    /**
     *
     */
    protected function calculateNextGroupValues()
    {
        if (!$this->innerIterator->valid()) {
            $this->groupValues = [];
            return;
        }
        $groupValues = [];
        $currentKey = $this->innerIterator->key();
        $currentValue = $this->innerIterator->current();
        $groupId = call_user_func($this->groupingFunction, $currentValue);
        while (call_user_func($this->groupingFunction, $currentValue) == $groupId) {
            $groupValues[$currentKey] = $currentValue;
            $this->innerIterator->next();
            if (!$this->innerIterator->valid()) {
                break;
            }
            $currentKey = $this->innerIterator->key();
            $currentValue = $this->innerIterator->current();
        }
        $this->groupValues = $groupValues;
        $this->groupKey = $groupId;
    }
}
