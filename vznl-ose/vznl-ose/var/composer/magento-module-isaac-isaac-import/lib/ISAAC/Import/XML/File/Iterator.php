<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_XML_File_Iterator implements IteratorAggregate
{
    /**
     * @var string
     */
    protected $xmlFilePath = '';

    /**
     * @var string
     */
    protected $validationSchema = '';

    public function __construct($xmlFilePath)
    {
        $this->xmlFilePath = $xmlFilePath;
    }

    /**
     * @return string
     */
    public function getValidationSchema()
    {
        return $this->validationSchema;
    }

    /**
     * @param string $validationSchema
     */
    public function setValidationSchema($validationSchema)
    {
        $this->validationSchema = $validationSchema;
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        if (!file_exists($this->xmlFilePath)) {
            Mage::throwException(sprintf('file %s does not exist', $this->xmlFilePath));
        }

        if (!is_readable($this->xmlFilePath)) {
            Mage::throwException(sprintf('file %s is not readable', $this->xmlFilePath));
        }

        if ($this->getValidationSchema()) {
            $this->validateSchema($this->xmlFilePath, $this->getValidationSchema());
        }

        return new SimpleXMLIterator(file_get_contents($this->xmlFilePath), LIBXML_NOBLANKS | LIBXML_NOCDATA);
    }

    /**
     * @param $xmlFilePath
     * @param $validationSchema
     * @throws Mage_Core_Exception
     */
    protected function validateSchema($xmlFilePath, $validationSchema)
    {
        $previousUseInternalErrors = libxml_use_internal_errors(true);
        $xml = new DOMDocument();
        $xml->load($xmlFilePath);
        if ($xml->schemaValidateSource($validationSchema)) {
            libxml_clear_errors();
            libxml_use_internal_errors($previousUseInternalErrors);
            return;
        }
        $errors = libxml_get_errors();
        libxml_clear_errors();
        libxml_use_internal_errors($previousUseInternalErrors);

        $formattedErrors = [];
        foreach ($errors as $error) {
            $formattedErrors[] = sprintf('Line %d: %s', $error->line, trim($error->message));
        }

        $domValidationSchema = new DOMDocument("1.0");
        $domValidationSchema->preserveWhiteSpace = false;
        $domValidationSchema->formatOutput = true;
        $domValidationSchema->loadXML($validationSchema);

        throw new Mage_Core_Exception(sprintf(
            "File \"%s\" did not comply with the following schema:" . PHP_EOL . PHP_EOL . "%s" . PHP_EOL .
                "Errors:" . PHP_EOL . "%s",
            $xmlFilePath,
            $domValidationSchema->saveXML(),
            implode(PHP_EOL, $formattedErrors)
        ));
    }
}
