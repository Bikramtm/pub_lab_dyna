<?php
/**
* ISAAC ISAAC_Import
*
* @category ISAAC
* @package ISAAC_Import
* @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
* @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
*/
/**
 * Iterator for reading the contents of a CSV file such that the values of the first line are used as keys for the
 * values of the remaining lines.
 *
 * This is similar to the Python csv.DictReader class.
 */
class ISAAC_Import_CSV_Dict_Reader implements Iterator {

    /** @var SplFileObject $fileObject */
    protected $fileObject;

    /** @var string[] */
    protected $keys = array();

    /** @var bool */
    protected $trimKeys = false;

    /** @var bool */
    protected $trimValues = false;

    /**
     * @param string $filePath
     */
    public function __construct($filePath) {
        $this->fileObject = new SplFileObject($filePath, 'r');
        $this->fileObject->setFlags(SplFileObject::READ_CSV);
    }

    /**
     * @param string $delimiter
     * @return $this
     */
    public function setDelimiter($delimiter) {
        $this->fileObject->setCsvControl($delimiter);
        return $this;
    }

    /**
     * @param bool $skipEmptyRows
     * @return $this
     */
    public function setSkipEmptyLines($skipEmptyRows)
    {
        $flags = SplFileObject::READ_CSV;
        if ($skipEmptyRows) {
            $flags |= SplFileObject::READ_AHEAD;
            $flags |= SplFileObject::SKIP_EMPTY;
            $flags |= SplFileObject::DROP_NEW_LINE;
        }
        $this->fileObject->setFlags($flags);
        return $this;
    }

    /**
     * @param bool $trimKeys
     * @return $this
     */
    public function setTrimKeys($trimKeys)
    {
        $this->trimKeys = $trimKeys;
        return $this;
    }

    /**
     * @param bool $trimValues
     * @return $this
     */
    public function setTrimValues($trimValues)
    {
        $this->trimValues = $trimValues;
        return $this;
    }

    /**
     *
     */
    public function rewind() {
        $this->fileObject->rewind();
        $keys = $this->fileObject->current();
        if ($this->trimKeys) {
            $keys = array_map('trim', $keys);
        }
        $this->keys = $keys;
        $this->fileObject->next();
    }

    /**
     * @return bool
     */
    public function valid() {
        return $this->fileObject->valid();
    }

    /**
     * @return array
     */
    public function current() {
        $values = $this->fileObject->current();
        if ($this->trimValues) {
            $values = array_map('trim', $values);
        }
        return array_combine($this->keys, $values);
    }

    /**
     * @return int
     */
    public function key() {
        return $this->fileObject->key() + 1;
    }

    /**
     *
     */
    public function next() {
        $this->fileObject->next();
    }

}
