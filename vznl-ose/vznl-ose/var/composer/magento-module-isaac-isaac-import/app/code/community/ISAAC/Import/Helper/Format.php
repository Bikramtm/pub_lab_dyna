<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class ISAAC_Import_Helper_Format extends Mage_Core_Helper_Data
{
    /** @var ISAAC_Import_Helper_Data */
    protected $importHelper;

    /** */
    public function __construct()
    {
        $this->importHelper = Mage::helper('isaac_import');
    }

    /**
     * @param $entityName
     * @param $valueIdentifier
     * @return string
     */
    public function formatNoDataChanges($entityName, $valueIdentifier)
    {
        return sprintf('fields for %s %s are up to date', $entityName, $valueIdentifier);
    }

    /**
     * @param $entityName
     * @param $valueIdentifier
     * @return string
     */
    public function formatDataChangesDeletion($entityName, $valueIdentifier)
    {
        return sprintf('deleted all fields for %s %s', $entityName, $valueIdentifier);
    }

    /**
     * @param $entityName
     * @param $valueIdentifier
     * @param $isValueNew
     * @return string
     */
    public function formatDataChangesHeader($entityName, $valueIdentifier, $isValueNew)
    {
        return sprintf(
            'changed fields for %s %s %s:',
            $isValueNew ? 'new' : 'existing',
            $entityName,
            $valueIdentifier
        );
    }

    public function formatDataChange($key, $oldValue, $newValue)
    {
        if ($this->isAssociativeArray($oldValue) && $this->isAssociativeArray($newValue)) {
            foreach ($newValue as $newElementKey => $newElementValue) {
                $keyExistsInOldValue = array_key_exists($newElementKey, $oldValue);
                if (!$keyExistsInOldValue || $newElementValue != $oldValue[$newElementKey]) {
                    return $this->formatDataChange(
                        $key . '[' . $newElementKey . ']',
                        $keyExistsInOldValue ? $oldValue[$newElementKey] : '',
                        $newElementValue
                    );
                }
            }
            foreach ($oldValue as $oldElementKey => $oldElementValue) {
                if (!array_key_exists($oldElementKey, $newValue)) {
                    return $this->formatDataChange($key . '[' . $oldElementKey . ']', $oldElementValue, null);
                }
            }
        } else {
            return sprintf(
                '-- %s: %s > %s',
                $key,
                $this->formatValueAsString($oldValue),
                $this->formatValueAsString($newValue)
            );
        }
    }

    /**
     * @param mixed $value
     * @return bool
     */
    protected function isAssociativeArray($value)
    {
        if (!is_array($value)) {
            return false;
        }
        if (empty($value)) {
            return true;
        }
        $currentIndex = 0;
        foreach ($value as $elementKey => $elementValue) {
            if ($elementKey !== $currentIndex) {
                return true;
            }
            $currentIndex++;
        }
        return false;
    }

    /**
     * @param mixed $value
     * @param array $objectContextHashes
     * @return string
     */
    public function formatValueAsString($value, array $objectContextHashes = array())
    {
        if (is_null($value)) {
            return 'null';
        }
        if (is_bool($value)) {
            return $value ? 'true' : 'false';
        }
        if (is_int($value)) {
            return sprintf('%d', $value);
        }
        if (is_float($value)) {
            return sprintf('%f', $value);
        }
        if (is_string($value)) {
            return sprintf('\'%s\'', $value);
        }
        if (is_array($value)) {
            return sprintf('[%s]', $this->formatValuesAsString($value, $objectContextHashes));
        }
        if (is_object($value)) {
            return sprintf('%s {%s}', get_class($value), $this->formatObjectAsString($value, $objectContextHashes));
        }
        if (is_resource($value)) {
            return 'resource';
        }
        return 'unknown';
    }

    /**
     * @param array $values
     * @param array $objectContextHashes
     * @return string
     */
    protected function formatValuesAsString(array $values, array $objectContextHashes)
    {
        $isFirstElement = true;
        $printKeys = $this->isAssociativeArray($values);
        $result = '';
        foreach ($values as $key => $value) {
            if (!$isFirstElement) {
                $result .= ', ';
            }
            if ($printKeys) {
                if (is_string($key)) {
                    $result .= '\'' . $key . '\'';
                } else {
                    $result .= $key;
                }
                $result .= ' => ';
            }
            $result .= $this->formatValueAsString($value, $objectContextHashes);
            $isFirstElement = false;
        }
        return $result;
    }

    /**
     * @param object $value
     * @param array $objectContextHashes
     * @return string
     */
    protected function formatObjectAsString($value, array $objectContextHashes)
    {
        $objectHash = spl_object_hash($value);
        if (in_array($objectHash, $objectContextHashes)) {
            return 'recursion';
        }
        $objectContextHashes[] = $objectHash;
        return $this->formatValuesAsString(get_object_vars($value), $objectContextHashes);
    }
}
