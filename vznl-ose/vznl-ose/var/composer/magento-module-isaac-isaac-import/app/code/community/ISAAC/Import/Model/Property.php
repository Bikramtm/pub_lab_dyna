<?php
/**
* ISAAC ISAAC_Import
*
* @category ISAAC
* @package ISAAC_Import
* @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)

* @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
*/
class ISAAC_Import_Model_Property
{
    const PROPERTY_TYPE_STRING                  = 'string';
    const PROPERTY_TYPE_BIT                     = 'bit';
    const PROPERTY_TYPE_UNSIGNED_INTEGER        = 'unsigned_integer';
    const PROPERTY_TYPE_NUMBER                  = 'number';
    const PROPERTY_TYPE_STRING_ARRAY            = 'string_array';
    const PROPERTY_TYPE_NUMERIC_ARRAY           = 'numeric_array';
    const PROPERTY_TYPE_UNSIGNED_INTEGER_ARRAY  = 'unsigned_integer_array';
    const PROPERTY_TYPE_STORE_ID                = 'store_id';
    const PROPERTY_TYPE_STORE_CODE              = 'store_code';
    const PROPERTY_TYPE_PRODUCT_TYPE            = 'product_type';
    const PROPERTY_TYPE_PRODUCT_STATUS          = 'product_status';
    const PROPERTY_TYPE_PRODUCT_VISIBILITY      = 'product_visibility';

    /** @var string */
    protected $key = '';

    /** @var bool */
    protected $emptyAllowed = true;

    /** @var string */
    protected $type = self::PROPERTY_TYPE_STRING;

    /**
     * @var string[]
     */
    protected $storeCodes = array();

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return $this
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isEmptyAllowed()
    {
        return $this->emptyAllowed;
    }

    /**
     * @param boolean $emptyAllowed
     * @return $this
     */
    public function setEmptyAllowed($emptyAllowed)
    {
        $this->emptyAllowed = $emptyAllowed;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $key
     * @param bool $isEmptyAllowed
     * @param string $type
     * @return $this
     */
    public function initialize($key, $isEmptyAllowed = true, $type = self::PROPERTY_TYPE_STRING) {
        $this->setKey($key);
        $this->setEmptyAllowed($isEmptyAllowed);
        $this->setType($type);
        return $this;
    }

    /**
     * @param $value
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function validateValue($value) {
        if (!$this->IsEmptyAllowed() && $this->isValueEmpty($value)) {
            Mage::throwException('value is empty');
        }
        if (!$this->isValueValid($value)) {
            Mage::throwException('value "' . $value . '" does not have type "' . $this->getType() . '"');
        }
        return $this;
    }

    /**
     * @param $value
     * @return bool
     */
    public function isValueEmpty($value) {
        return empty($value);
    }

    /**
     * @param $value
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function isValueValid($value) {
        if ($this->isValueEmpty($value)) {
            return $this->IsEmptyAllowed();
        }
        switch ($this->getType()) {
            case self::PROPERTY_TYPE_STRING:
                return is_string($value);
            case self::PROPERTY_TYPE_BIT:
                return $value == 0 || $value == 1;
            case self::PROPERTY_TYPE_UNSIGNED_INTEGER:
                return $this->isValueUnsignedInteger($value);
            case self::PROPERTY_TYPE_NUMBER:
                return $this->isValueNumber($value);
            case self::PROPERTY_TYPE_STORE_ID:
                return $this->isValueStoreId($value);
            case self::PROPERTY_TYPE_STORE_CODE:
                return $this->isValueStoreCode($value);
            case self::PROPERTY_TYPE_PRODUCT_TYPE:
                return $value == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE ||
                       $value == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE ||
                       $value == Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL;
            case self::PROPERTY_TYPE_PRODUCT_STATUS:
                return $value == Mage_Catalog_Model_Product_Status::STATUS_ENABLED ||
                       $value == Mage_Catalog_Model_Product_Status::STATUS_DISABLED;
            case self::PROPERTY_TYPE_PRODUCT_VISIBILITY:
                return $value == Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE ||
                       $value == Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG ||
                       $value == Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH ||
                       $value == Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
            case self::PROPERTY_TYPE_STRING_ARRAY:
                return $this->isValueStringArray($value);
            case self::PROPERTY_TYPE_NUMERIC_ARRAY:
                return $this->isValueNumericArray($value);
            case self::PROPERTY_TYPE_UNSIGNED_INTEGER_ARRAY:
                return $this->isValueUnsignedIntegerArray($value);
            default:
                throw new Mage_Core_Exception(sprintf(
                    'validation of value %s failed: unsupported property type %s',
                    print_r($value, true),
                    $this->getType()
                ));
        }
    }

    /**
     * @param $value
     * @return bool
     */
    protected function isValueUnsignedInteger($value) {
        return (bool) preg_match('/^0|[1-9]\d*$/', $value);
    }

    /**
     * @param $value
     * @return bool
     */
    protected function isValueNumber($value) {
        return is_numeric(str_replace(',', '', $value));
    }

    /**
     * @param $value
     * @return bool
     */
    protected function isValueStringArray($value) {
        if (!is_array($value)) {
            return false;
        }
        foreach ($value as $valueElement) {
            if (!is_string($valueElement)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function isValueNumericArray($value) {
        if (!is_array($value)) {
            return false;
        }
        foreach ($value as $valueElement) {
            if (!$this->isValueNumber($valueElement)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function isValueUnsignedIntegerArray($value) {
        if (!is_array($value)) {
            return false;
        }
        foreach ($value as $valueElement) {
            if (!$this->isValueUnsignedInteger($valueElement)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function isValueStoreId($value)
    {
        foreach (Mage::app()->getStores(true) as $store) {
            /** @var Mage_Core_Model_Store $store */
            if ($value == $store->getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function isValueStoreCode($value) {
        if ($value === 'admin') {
            return true;
        }

        if (!array_key_exists($value, $this->storeCodes)) {
            /** @var Mage_Core_Model_Store $store */
            $store = Mage::getModel('core/store')->load($value);
            $this->storeCodes[$value] = $store && $store->getId();
        }

        return $this->storeCodes[$value];
    }

    /**
     * @param $value
     * @return bool|int|string
     * @throws Mage_Core_Exception
     */
    public function getValue($value) {
        switch ($this->getType()) {
            case self::PROPERTY_TYPE_STRING:
                return (string) $value;
            case self::PROPERTY_TYPE_BIT:
                return (bool) $value;
            case self::PROPERTY_TYPE_UNSIGNED_INTEGER:
                return (string) $value;
            case self::PROPERTY_TYPE_NUMBER:
                return floatval(str_replace(',', '', $value));
            case self::PROPERTY_TYPE_STRING_ARRAY:
                $valueElements = array();
                foreach ($value as $valueKey => $valueElement) {
                    $valueElements[$valueKey] = (string) $valueElement;
                }
                return $valueElements;
            case self::PROPERTY_TYPE_NUMERIC_ARRAY:
                $valueElements = array();
                foreach ($value as $valueKey => $valueElement) {
                    $valueElements[$valueKey] = floatval(str_replace(',', '', $valueElement));
                }
                return $valueElements;
            case self::PROPERTY_TYPE_UNSIGNED_INTEGER_ARRAY:
                $valueElements = array();
                foreach ($value as $valueKey => $valueElement) {
                    $valueElements[$valueKey] = (string) $valueElement;
                }
                return $valueElements;
            case self::PROPERTY_TYPE_STORE_ID:
                return (int) $value;
            case self::PROPERTY_TYPE_STORE_CODE:
                return (string) $value;
            case self::PROPERTY_TYPE_PRODUCT_TYPE:
                return (string) $value;
            case self::PROPERTY_TYPE_PRODUCT_STATUS:
                return (int) $value;
            case self::PROPERTY_TYPE_PRODUCT_VISIBILITY:
                return (int) $value;
            default:
                throw new Mage_Core_Exception(sprintf(
                    'retrieval of value %s failed: unsupported property type %s',
                    print_r($value, 1),
                    $this->getType()
                ));
        }
    }

}
