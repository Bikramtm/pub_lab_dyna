<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2015 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

require_once 'Mage/Widget/controllers/Adminhtml/Widget/InstanceController.php';
class ISAAC_Import_Adminhtml_Widget_InstanceController
    extends Mage_Widget_Adminhtml_Widget_InstanceController
{
    /**
     * Save action
     *
     */
    public function saveAction()
    {
        $widgetInstance = $this->_initWidgetInstance();
        if (!$widgetInstance) {
            $this->_redirect('*/*/');
            return;
        }
        $widgetInstance->setTitle($this->getRequest()->getPost('title'))
            ->setStoreIds($this->getRequest()->getPost('store_ids', array(0)))
            ->setSortOrder($this->getRequest()->getPost('sort_order', 0))
            ->setPageGroups($this->getRequest()->getPost('widget_instance'))
            ->setWidgetParameters($this->getRequest()->getPost('parameters'));

        // ISAAC: Start: Add widget identifier to widget instance
        $widgetInstance->setData('identifier', $this->getRequest()->getPost('identifier'));
        // ISAAC: End: Add widget identifier to widget instance

        try {
            $widgetInstance->save();
            $this->_getSession()->addSuccess(
                Mage::helper('widget')->__('The widget instance has been saved.')
            );
            if ($this->getRequest()->getParam('back', false)) {
                $this->_redirect('*/*/edit', array(
                    'instance_id' => $widgetInstance->getId(),
                    '_current' => true
                ));
            } else {
                $this->_redirect('*/*/');
            }
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($this->__('An error occurred during saving a widget: %s', $e->getMessage()));
        }
        $this->_redirect('*/*/edit', array('_current' => true));
    }
}
