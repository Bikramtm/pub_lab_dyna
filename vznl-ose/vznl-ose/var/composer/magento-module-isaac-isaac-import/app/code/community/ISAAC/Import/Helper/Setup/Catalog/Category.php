<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Helper_Setup_Catalog_Category extends ISAAC_Import_Helper_Setup
{
    /**
     * @inheritdoc
     */
    public function getIdentifier()
    {
        return 'isaac_import_catalog_category';
    }

    /**
     * @inheritdoc
     */
    public function getGenerator($format)
    {
        $generator = '';
        switch ($format) {
            case 'xml':
                $generator = 'isaac_import/generator_xml_file_catalog_category';
                break;
            case 'csv':
                $generator = 'isaac_import/generator_csv_file_catalog_category';
                break;
            default:
                Mage::throwException('Unsupported generator format');
        }

        return $generator;
    }

    /**
     * @inheritdoc
     */
    public function getLoader()
    {
        return 'isaac_import/loader_model_catalog_category_singly';
    }
}
