<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Loader_Model_Catalog_Category_Singly extends ISAAC_Import_Model_Loader_Model_Singly
{
    /** @var ISAAC_Import_Helper_Catalog */
    protected $catalogHelper;

    /** @var ISAAC_Import_Helper_Category */
    protected $categoryHelper;

    /** @var Mage_Catalog_Model_Category */
    protected $rootCategory;

    /** @var Mage_Core_Model_Store */
    protected $adminStore;

    /** @var Mage_Core_Model_Store */
    protected $defaultStore;

    /** @var Mage_Eav_Model_Config */
    protected $eavConfig;

    /** @var string[] $mandatoryAttributeCodesForSavingAsKeys */
    protected $mandatoryAttributeCodesForSavingAsKeys = array();

    /** @var null|array */
    protected $mandatoryAttributeCodes;

    public function __construct()
    {
        parent::__construct();

        $this->catalogHelper = Mage::helper('isaac_import/catalog');
        $this->categoryHelper = Mage::helper('isaac_import/category');

        /** @var Mage_Core_Model_Store $store */
        $store = Mage::getModel('core/store');
        $store->load(Mage_Core_Model_App::DISTRO_STORE_ID);

        /** @var Mage_Catalog_Model_Category $category */
        $category = Mage::getModel('catalog/category');
        $category->load($store->getRootCategoryId());

        $this->rootCategory = $category;
        $this->adminStore = Mage::app()->getStore(Mage_Core_Model_Store::ADMIN_CODE);
        $this->defaultStore = Mage::app()->getDefaultStoreView();
        $this->eavConfig = Mage::getSingleton('eav/config');

        // @codingStandardsIgnoreStart
        global $_FILES;
        $_FILES = array();
        $_FILES['image']['tmp_name'] = '';
        $_FILES['thumbnail']['tmp_name'] = '';
        // @codingStandardsIgnoreEnd
    }

    /**
     * @inheritDoc
     */
    protected function supportsImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        return $importValue instanceof ISAAC_Import_Model_Import_Value_Catalog_Category;
    }

    /**
     * @inheritDoc
     */
    protected function getModelByImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        /** @var ISAAC_Import_Model_Import_Value_Catalog_Category $importValue */
        $categoryModel = $this->getCategoryByCategoryCode(
            $importValue->getIdentifier(),
            $this->getStoreIdByStoreCode($importValue->getStoreCode())
        );

        if ($categoryModel->isObjectNew()) {
            $categoryModel->setData('category_code', $importValue->getIdentifier());
        }

        return $categoryModel;
    }

    /**
     * @inheritdoc
     */
    protected function getModelValuesToUpdate(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
        /** @var Mage_Catalog_Model_Category $model */
        /** @var ISAAC_Import_Model_Import_Value_Catalog_Category $importValue */

        return $this->catalogHelper->getAttributeValuesToUpdate(
            $model,
            $importValue->getDataValues(),
            $this->getStoreIdByStoreCode($importValue->getStoreCode())
        );
    }

    /**
     * @inheritDoc
     */
    protected function beforeSave(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue,
        $isObjectNew
    ) {
        /** @var Mage_Catalog_Model_Category $model */
        if ($isObjectNew) {
            return;
        }

        /** @var Mage_Catalog_Model_Category $model */
        if ($model->getStoreId() == Mage_Core_Model_App::ADMIN_STORE_ID) {
            return;
        }

        foreach ($model->getData() as $key => $value) {
            if ($this->useDefaultValueForSaving($model, $key)) {
                $model->setData($key, false);
            }
        }
    }

    /**
     * @inheritDoc
     */
    protected function afterSave(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue,
        $isObjectNew
    ) {
        /** @var Mage_Catalog_Model_Category $model */
        /** @var ISAAC_Import_Model_Import_Value_Catalog_Category $importValue */
        $parentCategory = null;
        if ($importValue->getParent() !== null) {
            $parentCategory = $this->getCategoryByCategoryCode(
                $importValue->getParent(),
                $this->getStoreIdByStoreCode($importValue->getStoreCode())
            );
        } elseif ($isObjectNew) {
            $parentCategory = $this->rootCategory;
        }

        if ($parentCategory !== null && $parentCategory->getId()) {
            $this->moveCategoryModelUnderParentCategory($model, $parentCategory);
        } elseif ($importValue->getParent() !== null) {
            Mage::throwException(sprintf(
                'could not find parent category with category_code %s',
                $importValue->getParent()
            ));
        }
    }

    /**
     * @inheritDoc
     */
    protected function modelUpToDate(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
        /** @var Mage_Catalog_Model_Category $model */
        /** @var ISAAC_Import_Model_Import_Value_Catalog_Category $importValue */
        if ($importValue->getParent() === null) {
            parent::modelUpToDate($model, $importValue);
        } else {
            $parentCategory = $this->getCategoryByCategoryCode(
                $importValue->getParent(),
                $this->getStoreIdByStoreCode($importValue->getStoreCode())
            );

            if ($parentCategory === null || !$parentCategory->getId()) {
                Mage::throwException(sprintf(
                    'could not find parent category with category_code %s',
                    $importValue->getParent()
                ));
            }

            if ($model->getParentId() == $parentCategory->getId()) {
                parent::modelUpToDate($model, $importValue);
            } else {
                $this->moveCategoryModelUnderParentCategory($model, $parentCategory);
            }
        }
    }

    /**
     * @param Mage_Catalog_Model_Category $model
     * @param $key
     * @return bool
     */
    protected function useDefaultValueForSaving(Mage_Catalog_Model_Category $model, $key)
    {
        /** @var false|Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
        $attribute = $this->eavConfig->getAttribute(Mage_Catalog_Model_Category::ENTITY, $key);
        if (!$attribute || !$attribute->getId() || $attribute->isScopeGlobal()) {
            return false;
        }

        if ($model->getOrigData($key) === $model->getData($key)) {
            return !$model->getExistsStoreValueFlag($key);
        }

        $defaultValue = $model->getAttributeDefaultValue($key);
        return $defaultValue !== false && $model->getData($key) === $defaultValue;
    }

    /**
     * @param Mage_Catalog_Model_Category $categoryModel
     * @param Mage_Catalog_Model_Category $parentCategory
     */
    protected function moveCategoryModelUnderParentCategory(
        Mage_Catalog_Model_Category $categoryModel,
        Mage_Catalog_Model_Category $parentCategory
    ) {
        if ($categoryModel->getParentId() == $parentCategory->getId()) {
            return;
        }

        /** @var Mage_Catalog_Model_Resource_Category $categoryResource */
        $categoryResource = $categoryModel->getResource();
        $categoryResource->changeParent($categoryModel, $parentCategory, $this->getAfterCategoryId($parentCategory));

        $this->importHelper->logMessage(sprintf(
            'moved category with category_code %s under parent with name %s',
            $categoryModel->getData('category_code'),
            $parentCategory->getName()
        ), null, 'isaac_import_debug.log');
    }

    /**
     * @param Mage_Catalog_Model_Category $parentCategory
     * @return int
     */
    protected function getAfterCategoryId(Mage_Catalog_Model_Category $parentCategory)
    {
        /** @var Mage_Catalog_Model_Resource_Category $parentCategoryResource */
        $parentCategoryResource = $parentCategory->getResource();
        $connection = $parentCategoryResource->getReadConnection();
        $select = $connection->select()
            ->from($parentCategoryResource->getTable('catalog/category'), ['entity_id'])
            ->where('parent_id = ?', $parentCategory->getId())
            ->order('position DESC')
            ->limit(1);

        return $connection->fetchOne($select);
    }

    /**
     * @param Mage_Catalog_Model_Category $categoryModel
     * @param int $position
     */
    protected function saveCategoryPosition(
        Mage_Catalog_Model_Category $categoryModel,
        $position
    ) {
        /** @var Mage_Catalog_Model_Resource_Category $categoryResource */
        $categoryResource = $categoryModel->getResource();

        $table = $categoryResource->getEntityTable();
        $adapter = $categoryResource->getWriteConnection();

        $where = array('entity_id = ?' => $categoryModel->getId());
        $bind = array('position' => $position);
        $adapter->update($table, $bind, $where);
    }

    /**
     * @param $categoryCode
     * @param $storeId
     * @return Mage_Catalog_Model_Category
     * @throws Mage_Core_Exception
     */
    protected function getCategoryByCategoryCode($categoryCode, $storeId = null)
    {
        /** @var Mage_Catalog_Model_Category $catalogCategoryModel */
        $catalogCategoryModel = Mage::getModel('catalog/category');
        $catalogCategoryModel->setStoreId($storeId);

        $entityId = $this->categoryHelper->getIdByCode($categoryCode);

        return $entityId === null ? $catalogCategoryModel : $catalogCategoryModel->load($entityId);
    }

    /**
     * @param string $storeCode
     * @return int
     */
    protected function getStoreIdByStoreCode($storeCode)
    {
        $storeId = Mage::app()->getStore($storeCode)->getId();
        if ($this->defaultStore->getId() == $storeId) {
            return $this->adminStore->getId();
        }

        return $storeId;
    }
}
