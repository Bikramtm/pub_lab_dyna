<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

Mage::log('Running install file: ' . __FILE__, null, 'install.log', true);

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();



// add identifier field to product option, pre-fill it with the title and add a unique index
$productOptionTable = $installer->getTable('catalog/product_option');

$installer
    ->getConnection()
    ->addColumn($productOptionTable, 'identifier', 'VARCHAR(255) NULL');

/** @var Mage_Catalog_Model_Resource_Product_Option_Collection $productOptionCollection */
$productOptionCollection = Mage::getResourceModel('catalog/product_option_collection');
$productOptionCollection->addTitleToResult(Mage_Core_Model_App::ADMIN_STORE_ID);
/** @var Mage_Catalog_Model_Product_Option $item */
foreach ($productOptionCollection->getItems() as $item) {
    $title = $item->getData('identifier') ? $item->getData('identifier') : $item->getData('title');
    $identifier = $title;

    // test uniqueness
    $productOptionTest = Mage::getResourceModel('catalog/product_option_collection')
        ->addFieldToFilter('option_id', ['neq' => $item->getData('option_id')])
        ->addFieldToFilter('product_id', ['eq' => $item->getData('product_id')])
        ->addFieldToFilter('identifier', ['eq' => $identifier]);

    $i = 0;
    while ($productOptionTest->getSize() > 0) {
        $identifier = $title . '_' . $i;

        $productOptionTest = Mage::getResourceModel('catalog/product_option_collection')
            ->addFieldToFilter('option_id', ['neq' => $item->getData('option_id')])
            ->addFieldToFilter('product_id', ['eq' => $item->getData('product_id')])
            ->addFieldToFilter('identifier', ['eq' => $identifier]);

        $i++;
    }

    $installer->getConnection()->update(
        $productOptionTable,
        ['identifier' => $identifier],
        ['option_id = ?' => $item->getData('option_id')]
    );
}

$installer->getConnection()->addIndex(
    $productOptionTable,
    $installer->getIdxName($productOptionTable, ['identifier', 'product_id'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
    ['identifier', 'product_id'],
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);



// add identifier field to product option value
$productOptionValueTable = $installer->getTable('catalog/product_option_type_value');

$installer
    ->getConnection()
    ->addColumn($productOptionValueTable, 'identifier', 'VARCHAR(255) NULL');

/** @var Mage_Catalog_Model_Resource_Product_Option_Value_Collection $productOptionValueCollection */
$productOptionValueCollection = Mage::getResourceModel('catalog/product_option_value_collection');
$productOptionValueCollection->addTitlesToResult(Mage_Core_Model_App::ADMIN_STORE_ID);
/** @var Mage_Catalog_Model_Product_Option_Value $item */
foreach ($productOptionValueCollection->getItems() as $item) {
    /** @var Mage_Catalog_Model_Product_Option $item */
    $title = $item->getData('identifier') ? $item->getData('identifier') : $item->getData('title');
    $identifier = $title;

    // test uniqueness
    $productOptionValueTest = Mage::getResourceModel('catalog/product_option_value_collection')
        ->addFieldToFilter('option_type_id', ['neq' => $item->getData('option_type_id')])
        ->addFieldToFilter('option_id', ['eq' => $item->getData('option_id')])
        ->addFieldToFilter('identifier', ['eq' => $identifier]);

    $i = 0;
    while ($productOptionValueTest->getSize() > 0) {
        $identifier = $title . '_' . $i;

        $productOptionValueTest = Mage::getResourceModel('catalog/product_option_value_collection')
            ->addFieldToFilter('option_type_id', ['neq' => $item->getData('option_type_id')])
            ->addFieldToFilter('option_id', ['eq' => $item->getData('option_id')])
            ->addFieldToFilter('identifier', ['eq' => $identifier]);

        $i++;
    }

    $installer->getConnection()->update(
        $productOptionValueTable,
        ['identifier' => $identifier],
        ['option_type_id = ?' => $item->getData('option_type_id')]
    );
}

$installer->getConnection()->addIndex(
    $productOptionValueTable,
    $installer->getIdxName($productOptionValueTable, ['identifier', 'option_id'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
    ['identifier', 'option_id'],
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);



$installer->getConnection()->resetDdlCache();

$installer->endSetup();

Mage::log('Finished install file: ' . __FILE__, null, 'install.log', true);
