<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Loader_Model_Catalog_Product_Singly extends ISAAC_Import_Model_Loader_Model_Singly
{
    /** @var Mage_Eav_Model_Config */
    protected $eavConfig;

    /** @var ISAAC_Import_Helper_Import_Catalog_Product */
    protected $isaacImportCatalogProductHelper;

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct();

        $this->eavConfig = Mage::getModel('eav/config');
        $this->isaacImportCatalogProductHelper = Mage::helper('isaac_import/import_catalog_product');
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return bool
     */
    protected function supportsImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        return $this->isaacImportCatalogProductHelper->supportsImportValue($importValue);
    }

    /**
     * @inheritDoc
     */
    protected function getModelValuesToUpdate(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
        return $this->isaacImportCatalogProductHelper->getModelValuesToUpdate($model, $importValue);
    }

    /**
     * @inheritDoc
     */
    protected function beforeSave(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue,
        $isObjectNew
    ) {
        /** @var Mage_Catalog_Model_Product $model */
        $model->validate();

        if ($isObjectNew) {
            return;
        }

        if ($model->getStoreId() == Mage_Core_Model_App::ADMIN_STORE_ID) {
            return;
        }

        foreach ($model->getData() as $key => $value) {
            if ($this->useDefaultValueForSaving($model, $key)) {
                $model->setData($key, false);
            }
        }
    }

    /**
     * @inheritDoc
     */
    protected function afterSave(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue,
        $isObjectNew
    ) {
        return $this->isaacImportCatalogProductHelper->afterSave($model, $importValue, $isObjectNew);
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return Mage_Core_Model_Abstract
     */
    protected function getModelByImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        /** @var ISAAC_Import_Model_Import_Value_Catalog_Product $importValue */
        $storeId = Mage::app()->getStore($importValue->getStoreCode())->getId();

        /** @var ISAAC_Import_Model_Import_Value_Catalog_Product $importValue */
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product');
        $product->setData('store_id', $storeId);
        $product->load($product->getIdBySku($importValue->getIdentifier()));
        if ($product->getId()) {
            return $product;
        }

        if ($storeId !== Mage_Core_Model_App::ADMIN_STORE_ID) {
            Mage::throwException(sprintf(
                'product %s cannot be created as this is not an admin import',
                $importValue->__toString()
            ));
        }

        if ($importValue->hasDataValue('_attribute_set')) {
            $attributeSetId = $this->isaacImportCatalogProductHelper->getProductAttributeSetIdByName(
                $importValue->getDataValue('_attribute_set')
            );
            $product->setData('attribute_set_id', $attributeSetId);
        } else {
            $product->setData('attribute_set_id', $product->getDefaultAttributeSetId());
        }
        $product->setData('sku', $importValue->getIdentifier());
        if ($importValue->hasDataValue('_entity_type')) {
            $product->setData('type_id', $importValue->hasDataValue('_entity_type'));
        } else {
            $product->setData('type_id', Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        }

        return $product;
    }

    /**
     * @param Mage_Catalog_Model_Product $model
     * @param $key
     * @return bool
     */
    protected function useDefaultValueForSaving(Mage_Catalog_Model_Product $model, $key)
    {
        /** @var false|Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
        $attribute = $this->eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY, $key);
        if (!$attribute || !$attribute->getId() || $attribute->isScopeGlobal()) {
            return false;
        }

        if ($model->getOrigData($key) === $model->getData($key)) {
            return !$model->getExistsStoreValueFlag($key);
        }

        $defaultValue = $model->getAttributeDefaultValue($key);
        return $defaultValue !== false && $model->getData($key) === $defaultValue;
    }
}
