<?php
/**
* ISAAC ISAAC_Import
*
* @category ISAAC
* @package ISAAC_Import
* @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)

* @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
*/
class ISAAC_Import_Helper_Category extends Mage_Core_Helper_Abstract
{
    /**
     * @param $categoryCode
     * @return int|null The id for the category with category_code $categoryCode
     */
    public function getIdByCode($categoryCode)
    {
        /** @var Mage_Catalog_Model_Resource_Category_Collection $categoryCollection */
        $categoryCollection = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToFilter('category_code', array('eq' => $categoryCode))
        ;
        $select = $categoryCollection->getSelect();
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns('e.entity_id');
        $id = $categoryCollection->getConnection()->fetchOne($select);

        return (false === $id) ? null : $id;
    }
}
