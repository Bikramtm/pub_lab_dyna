<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class ISAAC_Import_Model_Import_Value
{
    /** @var string Flag for create */
    const CREATE = 'CREATE';

    /** @var string Flag for update */
    const UPDATE = 'UPDATE';

    /** @var string Flag for delete */
    const DELETE = 'DELETE';

    /** @var string */
    protected $identifier;

    /** @var array */
    protected $flags = [];

    /**
     * ISAAC_Import_Model_Import_Value constructor.
     * @param string $identifier
     */
    public function __construct($identifier)
    {
        $this->identifier = $identifier;
        $this->flags = [ self::CREATE => 1, self::UPDATE => 1 ];
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param array $flags
     */
    public function setLoadingFlags(array $flags)
    {
        $this->flags = array();
        foreach ($flags as $flag) {
            $this->addLoadingFlag($flag);
        }
    }

    /**
     * @param string $flag
     */
    public function addLoadingFlag($flag)
    {
        if (!in_array($flag, [ self::CREATE, self::UPDATE, self::DELETE ])) {
            Mage::throwException(sprintf('invalid flag: "%s"', $flag));
        } elseif ($flag === self::CREATE && isset($this->flags[self::DELETE])) {
            Mage::throwException('cannot add CREATE flag when DELETE flag exists');
        } elseif ($flag === self::UPDATE && isset($this->flags[self::DELETE])) {
            Mage::throwException('cannot add UPDATE flag when DELETE flag exists');
        } elseif ($flag === self::DELETE && isset($this->flags[self::CREATE])) {
            Mage::throwException('cannot add DELETE flag when CREATE flag exists');
        } elseif ($flag === self::DELETE && isset($this->flags[self::UPDATE])) {
            Mage::throwException('cannot add DELETE flag when UPDATE flag exists');
        }

        $this->flags[$flag] = 1;
    }

    /**
     * @return int
     */
    public function getLoadingFlags()
    {
        return array_keys($this->flags);
    }

    /**
     * @return bool
     */
    public function isCreate()
    {
        return isset($this->flags[self::CREATE]);
    }

    /**
     * @return bool
     */
    public function isUpdate()
    {
        return isset($this->flags[self::UPDATE]);
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return isset($this->flags[self::DELETE]);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    abstract public function getEntityName();

}
