<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class ISAAC_Import_Model_Configuration_Group
{

    /** @var string $id */
    protected $id;

    /** @var ISAAC_Import_Model_Configuration[] $configurations */
    protected $configurations = [];

    /** @var bool $active */
    protected $active = true;

    /** @var string $cliCode */
    protected $cliCode = '';

    /** @var string $cliDescription */
    protected $cliDescription = '';

    /**
     * @param array $configurationGroupValues
     * @throws Mage_Core_Exception
     */
    public function __construct(array $configurationGroupValues) {
        $requiredFields = ['id', 'configuration'];
        foreach ($requiredFields as $requiredField) {
            if (!array_key_exists($requiredField, $configurationGroupValues)) {
                Mage::throwException('could not create import configuration group: required field ' . $requiredField . ' is missing');
            }
        }
        $this->id = $configurationGroupValues['id'];
        if (is_array($configurationGroupValues['configuration'])) {
            foreach ($configurationGroupValues['configuration'] as $configurationId => $configurationData) {
                $configurationData['id'] = $configurationId;
                $this->configurations[$configurationId] =
                    Mage::getModel('isaac_import/configuration', $configurationData);
            }
        }
        if (array_key_exists('active', $configurationGroupValues)) {
            $this->active = (bool) $configurationGroupValues['active'];
        }
        if (array_key_exists('cli_code', $configurationGroupValues)) {
            $this->cliCode = $configurationGroupValues['cli_code'];
        }
        if (array_key_exists('cli_description', $configurationGroupValues)) {
            $this->cliDescription = $configurationGroupValues['cli_description'];
        }
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ISAAC_Import_Model_Configuration[]
     */
    public function getConfigurations() {
        return $this->configurations;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string
     */
    public function getCliCode()
    {
        return $this->cliCode;
    }

    /**
     * @return string
     */
    public function getCliDescription()
    {
        return $this->cliDescription;
    }

}