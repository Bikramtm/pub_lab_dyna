<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class ISAAC_Import_Model_Property_Factory
{
    /**
     * @param string $key
     * @param bool $isEmptyAllowed
     * @param string $type
     * @return ISAAC_Import_Model_Property
     */
    public function createProperty($key, $isEmptyAllowed = true, $type = ISAAC_Import_Model_Property::PROPERTY_TYPE_STRING) {
        /** @var ISAAC_Import_Model_Property $property */
        $property = Mage::getModel('isaac_import/property');
        return $property->initialize($key, $isEmptyAllowed, $type);
    }

}