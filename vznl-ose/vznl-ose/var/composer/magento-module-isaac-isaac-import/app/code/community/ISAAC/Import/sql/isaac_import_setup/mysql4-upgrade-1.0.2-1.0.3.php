<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

Mage::log('Running install file: ' . __FILE__, null, 'install.log', true);

$this->startSetup();

$widgetInstanceTable = $this->getTable('widget/widget_instance');

$this->getConnection()->dropIndex(
    $widgetInstanceTable,
    $this->getIdxName(
        $widgetInstanceTable,
        'identifier',
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    )
);

$this->getConnection()->addIndex(
    $widgetInstanceTable,
    $this->getIdxName(
        $widgetInstanceTable,
        ['identifier', 'store_ids'],
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    ),
    ['identifier', 'store_ids'],
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$this->endSetup();

Mage::log('Finished install file: ' . __FILE__, null, 'install.log', true);
