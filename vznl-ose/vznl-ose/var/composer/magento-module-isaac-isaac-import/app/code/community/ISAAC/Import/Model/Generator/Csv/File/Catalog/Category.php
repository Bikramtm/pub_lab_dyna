<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Generator_Csv_File_Catalog_Category extends ISAAC_Import_Model_Generator_Csv_File
{
    /**
     * @var string[]
     */
    protected $requiredFields = [
        'category_code',
        'flags'
    ];

    /**
     * @inheritDoc
     */
    public function transformValue($value, $key)
    {
        foreach ($this->requiredFields as $requiredField) {
            if (!array_key_exists($requiredField, $value)) {
                throw new Mage_Core_Exception(sprintf('required field "%s" is missing', $requiredField));
            }
        }

        /** @var ISAAC_Import_Model_Import_Value_Catalog_Category $importValueCatalogCategory */
        $importValueCatalogCategory = Mage::getModel(
            'isaac_import/import_value_catalog_category',
            $value['category_code']
        );
        unset($value['category_code']);

        $importValueCatalogCategory->setLoadingFlags(explode(',', $value['flags']));
        unset($value['flags']);

        if (array_key_exists('store_code', $value)) {
            if (!empty($value['store_code'])) {
                $importValueCatalogCategory->setStoreCode($value['store_code']);
            }
            unset($value['store_code']);
        }

        if (array_key_exists('parent', $value)) {
            if (!empty($value['parent'])) {
                $importValueCatalogCategory->setParent($value['parent']);
            }
            unset($value['parent']);
        }

        $importValueCatalogCategory->addDataValues($value);

        return $importValueCatalogCategory;
    }

    /**
     * @inheritDoc
     */
    public function cleanup()
    {
        // TODO: Implement cleanup() method.
    }
}
