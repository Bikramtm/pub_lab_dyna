<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (http://www.isaac.nl)
 
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
 
abstract class ISAAC_Import_Model_Generator_Xml_File extends ISAAC_Import_Model_Extract_Transform_Generator
{
    /**
     * @var string
     */
    protected $importFilePath = '';

    /**
     * @param array $initParams
     */
    public function __construct(array $initParams)
    {
        $requiredFields = ['importFileName'];
        foreach ($requiredFields as $requiredField) {
            if (!array_key_exists($requiredField, $initParams)) {
                Mage::throwException(sprintf(
                    'could not create import configuration: required field %s is missing',
                    $requiredField
                ));
            }
        }

        $this->importFilePath = $initParams['importFileName'];
    }

    /**
     * @inheritDoc
     */
    public function getExtractIterator()
    {
        $xmlIterator = new ISAAC_Import_XML_File_Iterator($this->getImportFilePath());
        $xmlIterator->setValidationSchema($this->getValidationSchema());

        return $xmlIterator->getIterator();
    }

    /**
     * @return string
     */
    public function getImportFilePath()
    {
        return $this->importFilePath;
    }

    /**
     * @param $xmlObject
     * @param array $out
     * @return array
     */
    public function convertXmlToArray(SimpleXMLElement $xmlObject, array $out = array())
    {
        foreach ((array) $xmlObject as $index => $node) {
            if (is_object($node)) {
                $out[$index] = $node->count() ? $this->convertXmlToArray($node) : null;
            } elseif (is_scalar($node)) {
                $out[$index] = $node;
            }
        }

        return $out;
    }

    /**
     * @return string
     */
    abstract protected function getValidationSchema();
}
