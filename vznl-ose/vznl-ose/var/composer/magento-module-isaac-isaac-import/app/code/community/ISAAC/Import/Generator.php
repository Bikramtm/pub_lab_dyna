<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
interface ISAAC_Import_Generator extends IteratorAggregate {

    /**
     * @return Iterator
     */
    public function getIterator();

    /**
     * @return void
     */
    public function cleanup();

}