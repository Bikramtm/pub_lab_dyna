<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Import_Value_Cms_Widget extends ISAAC_Import_Model_Import_Value
{
    /** @var string */
    protected $type;

    /** @var string */
    protected $title;

    /** @var string */
    protected $packageTheme;

    /** @var int */
    protected $sortOrder;

    /** @var string */
    protected $storeCode = Mage_Core_Model_Store::ADMIN_CODE;

    /** @var array */
    protected $widgetParameters = [];

    /** @var array */
    protected $pageGroups = [];

    /**
     * @inheritdoc
     */
    public function getEntityName()
    {
        return 'widget';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $result = $this->getIdentifier();
        $title = $this->getTitle();
        if ($title) {
            $result .= ' (' . $title . ')';
        }
        $result .= ' for store ' . $this->getStoreCode();
        return $result;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPackageTheme()
    {
        return $this->packageTheme;
    }

    /**
     * @param string $packageTheme
     */
    public function setPackageTheme($packageTheme)
    {
        $this->packageTheme = $packageTheme;
    }

    /**
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param int $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->storeCode;
    }

    /**
     * @param string $storeCode
     */
    public function setStoreCode($storeCode)
    {
        $this->storeCode = $storeCode;
    }

    /**
     * @return array
     */
    public function getWidgetParameters()
    {
        return $this->widgetParameters;
    }

    /**
     * @param array $widgetParameters
     */
    public function setWidgetParameters(array $widgetParameters)
    {
        $this->widgetParameters = $widgetParameters;
    }

    /**
     * @return array
     */
    public function getPageGroups()
    {
        return $this->pageGroups;
    }

    /**
     * @param array $pageGroups
     */
    public function setPageGroups(array $pageGroups)
    {
        $this->pageGroups = [];
        foreach ($pageGroups as $pageGroup) {
            $this->addPageGroup($pageGroup);
        }
    }

    /**
     * @param array $pageGroup
     */
    public function addPageGroup(array $pageGroup)
    {
        $this->pageGroups[] = $pageGroup;
    }
}
