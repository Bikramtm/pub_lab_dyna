<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class ISAAC_Import_Model_Configuration
{

    /** @var string $id */
    protected $id;

    /** @var string $generatorClass */
    protected $generatorClass;

    /** @var string $code */
    protected $loaderClass;

    /** @var bool $active */
    protected $active = true;

    /** @var int $batchSize */
    protected $batchSize = 0;

    /** @var string[] $generatorInitParams */
    protected $generatorInitParams = [];

    /** @var string[] $indexers */
    protected $indexers = [];

    /** @var bool $loaderViaExec */
    protected $loaderViaExec = false;

    /** @var string $cliCode */
    protected $cliCode = '';

    /** @var string $cliDescription */
    protected $cliDescription = '';

    /**
     * @param array $configurationValues
     * @throws Mage_Core_Exception
     */
    public function __construct(array $configurationValues) {
        $requiredFields = ['id', 'generator_class', 'loader_class'];
        foreach ($requiredFields as $requiredField) {
            if (!array_key_exists($requiredField, $configurationValues)) {
                Mage::throwException('could not create import configuration: required field ' . $requiredField . ' is missing');
            }
        }
        $this->id = $configurationValues['id'];
        $this->generatorClass = $configurationValues['generator_class'];
        $this->loaderClass = $configurationValues['loader_class'];
        if (array_key_exists('active', $configurationValues)) {
            $this->active = (bool) $configurationValues['active'];
        }
        if (array_key_exists('batch_size', $configurationValues)) {
            if (!is_numeric($configurationValues['batch_size']) || $configurationValues['batch_size'] < 0) {
                Mage::throwException('could not create import configuration: batch_size should be a non-negative number');
            }
            $this->batchSize = $configurationValues['batch_size'];
        }
        if (array_key_exists('generator_init_params', $configurationValues)) {
            if (!is_array($configurationValues['generator_init_params'])) {
                Mage::throwException('could not create import configuration: generator_init_params should be an array');
            }
            $this->generatorInitParams = $configurationValues['generator_init_params'];
        }
        if (array_key_exists('indexers', $configurationValues)) {
            if (!is_array($configurationValues['indexers'])) {
                Mage::throwException('could not create import configuration: indexers should be an array, found ' . print_r($configurationValues['indexers'], true));
            }
            $this->indexers = $configurationValues['indexers'];
        }
        if (array_key_exists('loader_via_exec', $configurationValues)) {
            $this->loaderViaExec = (bool) $configurationValues['loader_via_exec'];
        }
        if (array_key_exists('cli_code', $configurationValues)) {
            $this->cliCode = $configurationValues['cli_code'];
        }
        if (array_key_exists('cli_description', $configurationValues)) {
            $this->cliDescription = $configurationValues['cli_description'];
        }
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGeneratorClass()
    {
        return $this->generatorClass;
    }

    /**
     * @return string
     */
    public function getLoaderClass()
    {
        return $this->loaderClass;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return int
     */
    public function getBatchSize()
    {
        return $this->batchSize;
    }

    /**
     * @return string[]
     */
    public function getGeneratorInitParams() {
        return $this->generatorInitParams;
    }

    /**
     * @return string[]
     */
    public function getIndexers() {
        return $this->indexers;
    }

    /**
     * @return int
     */
    public function getLoaderViaExec()
    {
        return $this->loaderViaExec;
    }

    /**
     * @return string
     */
    public function getCliCode()
    {
        return $this->cliCode;
    }

    /**
     * @return string
     */
    public function getCliDescription()
    {
        return $this->cliDescription;
    }

}