<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class ISAAC_Import_Model_Group_Importer
{

    /** @var ISAAC_Import_Model_Configuration_Group $configurationGroup */
    protected $configurationGroup;

    /** @var ISAAC_Import_Helper_Data $helper */
    protected $helper;

    /** @var bool */
    protected $reindex = true;

    /** @var bool */
    protected $cleanup = true;

    /** @var bool */
    protected $profile = false;

    /**
     * @param ISAAC_Import_Model_Configuration_Group $configurationGroup
     */
    public function __construct(ISAAC_Import_Model_Configuration_Group $configurationGroup)
    {
        $this->configurationGroup = $configurationGroup;
        $this->helper = Mage::helper('isaac_import');
    }

    /**
     * @param bool $reindex
     * @return $this
     */
    public function setReindex($reindex)
    {
        $this->reindex = $reindex;
        return $this;
    }

    /**
     * @param bool $cleanup
     * @return $this
     */
    public function setCleanup($cleanup)
    {
        $this->cleanup = $cleanup;
        return $this;
    }

    /**
     * @param bool $profile
     * @return $this
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        return $this;
    }

    public function import()
    {
        try {
            Mage::dispatchEvent(
                $this->getEventPrefix() . '_before',
                ['configuration_group' => $this->configurationGroup]
            );
            if ($this->configurationGroup->getActive()) {
                $configurationGroupId = $this->configurationGroup->getId();
                $this->helper->logMessage('started importing values for import group ' . $configurationGroupId);
                foreach ($this->configurationGroup->getConfigurations() as $configuration) {
                    /** @var ISAAC_Import_Model_Importer $importer */
                    $importer = Mage::getModel('isaac_import/importer', $configuration);
                    $importer->setReindex($this->reindex);
                    $importer->setCleanup($this->cleanup);
                    $importer->setProfile($this->profile);
                    $importer->import();
                }
                $this->helper->logMessage('finished importing values for import group ' . $configurationGroupId);
            }
            Mage::dispatchEvent(
                $this->getEventPrefix() . '_after',
                ['configuration_group' => $this->configurationGroup]
            );
        } catch (Exception $exception) {
            Mage::dispatchEvent($this->getEventPrefix() . '_error', [
                'configuration_group' => $this->configurationGroup,
                'exception' => $exception
            ]);
            throw $exception;
        }
    }

    /**
     * @return string
     */
    protected function getEventPrefix()
    {
        return 'isaac_import_' . $this->configurationGroup->getId() . '_group_import';
    }

}