<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (http://www.isaac.nl)
 
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Loader_Model_Cms_Page_Singly extends ISAAC_Import_Model_Loader_Model_Singly
{
    /**
     * @inheritdoc
     */
    public function supportsImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        return $importValue instanceof ISAAC_Import_Model_Import_Value_Cms_Page;
    }

    /**
     * @inheritDoc
     */
    protected function getModelByImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        /** @var ISAAC_Import_Model_Import_Value_Cms_Page $importValue */
        /** @var Mage_Cms_Model_Page $cmsPageModel */
        $cmsPageModel = Mage::getModel('cms/page');
        $cmsPageModel->setData('store_id', $this->getStoreIdByStoreCode($importValue->getStoreCode()));
        $cmsPageModel->load($importValue->getIdentifier(), 'identifier');

        if (!$cmsPageModel->getId()) {
            $cmsPageModel->setData('identifier', $importValue->getIdentifier());
        }

        return $cmsPageModel;
    }

    /**
     * @inheritDoc
     */
    protected function getModelValuesToUpdate(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
        /** @var ISAAC_Import_Model_Import_Value_Cms_Page $importValue */
        $importValuesToModelValuesMapping = [
            'title'             => $importValue->getTitle(),
            'root_template'     => $importValue->getRootTemplate(),
            'content_heading'   => $importValue->getContentHeading(),
            'content'           => $importValue->getContent(),
            'layout_update_xml' => $importValue->getLayoutUpdateXml(),
            'is_active'         => $importValue->getIsActive(),
        ];

        $modelValuesToUpdate = [];
        foreach ($importValuesToModelValuesMapping as $modelKey => $newValue) {
            if ($newValue !== null && $model->getData($modelKey) !== $newValue) {
                $modelValuesToUpdate[$modelKey] = $newValue;
            }
        }

        $storeIds = array($this->getStoreIdByStoreCode($importValue->getStoreCode()));
        if ($model->getData('store_id') !== $storeIds) {
            $modelValuesToUpdate['stores'] = $storeIds;
        }

        return $modelValuesToUpdate;
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param $key
     * @return bool
     */
    protected function unsetAllowed(Mage_Core_Model_Abstract $model, $key)
    {
        return false;
    }

    /**
     * Gets the storeId for the specified $storeCode.
     * @param $storeCode string The storeCode to get the storeId for.
     * @return int The storeId
     */
    protected function getStoreIdByStoreCode($storeCode)
    {
        return Mage::app()->getStore($storeCode)->getId();
    }
}
