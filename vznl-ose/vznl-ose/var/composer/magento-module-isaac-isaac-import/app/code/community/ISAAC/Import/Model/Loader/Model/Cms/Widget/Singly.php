<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (http://www.isaac.nl)
 
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
 
class ISAAC_Import_Model_Loader_Model_Cms_Widget_Singly extends ISAAC_Import_Model_Loader_Model_Singly
{
    /**
     * @inheritDoc
     */
    protected function supportsImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        return $importValue instanceof ISAAC_Import_Model_Import_Value_Cms_Widget;
    }

    /**
     * @inheritDoc
     */
    protected function getModelByImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        /** @var ISAAC_Import_Model_Import_Value_Cms_Widget $importValue */
        $widgetInstanceId = $this->getWidgetInstanceId(
            $this->getStoreIdByStoreCode($importValue->getStoreCode()),
            $importValue->getIdentifier()
        );

        /** @var Mage_Widget_Model_Widget_Instance $widgetInstanceModel */
        $widgetInstanceModel = Mage::getModel('widget/widget_instance');
        if ($widgetInstanceId !== null) {
            $widgetInstanceModel->load($widgetInstanceId);
        } else {
            $widgetInstanceModel->setData('identifier', $importValue->getIdentifier());
        }

        return $widgetInstanceModel;
    }

    /**
     * @inheritDoc
     */
    protected function getModelValuesToUpdate(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
        /** @var ISAAC_Import_Model_Import_Value_Cms_Widget $importValue */
        $widgetType = $importValue->getType();

        $widgetParameters = null;
        if ($importValue->getWidgetParameters() !== null) {
            $widgetParameters = $this->applyWidgetParametersDecorator($widgetType, $importValue->getWidgetParameters());
        }

        $importValuesToModelValuesMapping = [
            'type'              => $importValue->getType(),
            'title'             => $importValue->getTitle(),
            'package_theme'     => $importValue->getPackageTheme(),
            'store_ids'         => array($this->getStoreIdByStoreCode($importValue->getStoreCode())),
            'sort_order'        => $importValue->getSortOrder(),
            'widget_parameters' => $widgetParameters
        ];

        $modelValuesToUpdate = [];
        foreach ($importValuesToModelValuesMapping as $modelKey => $newValue) {
            if ($newValue !== null && $model->getData($modelKey) !== $newValue) {
                $modelValuesToUpdate[$modelKey] = $newValue;
            }
        }

        if ($importValue->getPageGroups() !== null) {
            $modelValuesToUpdate['page_groups'] = $this->formatNewPageGroups($importValue->getPageGroups());
        } elseif (!$model->isObjectNew()) {
            $modelValuesToUpdate['page_groups'] = $this->formatExistingPageGroups($model->getData('page_groups'));
        }

        return $modelValuesToUpdate;
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param $key
     * @return bool
     */
    protected function unsetAllowed(Mage_Core_Model_Abstract $model, $key)
    {
        return false;
    }

    /**
     * @param $storeId
     * @param $identifier
     * @return int|null
     */
    protected function getWidgetInstanceId($storeId, $identifier)
    {
        /** @var ISAAC_Import_Model_Import_Value_Cms_Widget $importValue */
        /** @var Mage_Widget_Model_Resource_Widget_Instance_Collection $widgetInstanceCollection */
        $widgetInstanceCollection = Mage::getResourceModel('widget/widget_instance_collection');
        $widgetInstanceCollection->addStoreFilter($storeId);
        $widgetInstanceCollection->addFieldToFilter('identifier', $identifier);
        $widgetInstanceCollection->addFieldToSelect('instance_id');

        foreach ($widgetInstanceCollection->getAllIds() as $widgetInstanceId) {
            return $widgetInstanceId;
        }

        return null;
    }

    /**
     * @param $widgetType
     * @param array $parameters
     * @return array
     */
    protected function applyWidgetParametersDecorator($widgetType, array $parameters)
    {
        $decorator = $this->getDecorator('parameters', $widgetType);
        return $decorator !== null ? $decorator->apply($parameters) : $parameters;
    }

    /**
     * @param $pageGroup
     * @param array $parameters
     * @return array
     */
    protected function applyPageGroupDecorator($pageGroup, array $parameters)
    {
        $decorator = $this->getDecorator('page_groups', $pageGroup);
        return $decorator !== null ? $decorator->apply($parameters) : $parameters;
    }

    /**
     * @param $decoratorType
     * @param $nodeType
     * @return ISAAC_Import_TransformerInterface|null
     */
    protected function getDecorator($decoratorType, $nodeType)
    {
        $decoratorNode = Mage::getConfig()->getNode(sprintf(
            'default/isaac_import/decorators/%s/%s',
            $decoratorType,
            $nodeType
        ));
        if (!$decoratorNode) {
            return null;
        }

        /** @var SimpleXMLElement $decoratorClassNode */
        $decoratorClassNode = $decoratorNode->class;
        if (!$decoratorClassNode->count()) {
            return null;
        }

        $decoratorClass = $decoratorClassNode->__toString();
        if (class_exists($decoratorClass)) {
            $decorator = new $decoratorClass();
            if (!$decorator instanceof ISAAC_Import_TransformerInterface) {
                return null;
            }

            return $decorator;
        }

        return null;
    }

    /**
     * @param array $pageGroups
     * @return array
     */
    protected function formatNewPageGroups(array $pageGroups)
    {
        $formattedPageGroups = [];
        foreach ($pageGroups as $index => $pageGroup) {
            if (!isset($pageGroup['page_id'])) {
                $formattedPageGroups['page_id'] = false;
            }
            if (!isset($pageGroup['for'])) {
                $formattedPageGroups['for'] = false;
            }

            $groupType = $pageGroup['type'];
            $formattedPageGroups[$index]['page_group'] = $groupType;
            $formattedPageGroups[$index][$groupType] = $this->applyPageGroupDecorator($groupType, $pageGroup);
        }

        return $formattedPageGroups;
    }

    /**
     * @param array $pageGroups
     * @return array
     */
    protected function formatExistingPageGroups(array $pageGroups)
    {
        $formattedPageGroups = [];
        foreach ($pageGroups as $pageGroup) {
            $pageGroupName = $pageGroup['page_group'];
            $formattedPageGroups[] = [
                'page_group' => $pageGroupName,
                $pageGroupName => [
                    'page_id' => $pageGroup['page_id'],
                    'layout_handle' => $pageGroup['layout_handle'],
                    'for' => $pageGroup['page_for'],
                    'block' => $pageGroup['block_reference'],
                    'entities' => $pageGroup['entities'],
                    'template' => $pageGroup['page_template']
                ]
            ];
        }

        return $formattedPageGroups;
    }

    /**
     * Gets the storeId for the specified $storeCode.
     * @param $storeCode string The storeCode to get the storeId for.
     * @return int The storeId
     */
    protected function getStoreIdByStoreCode($storeCode)
    {
        return Mage::app()->getStore($storeCode)->getId();
    }
}
