<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class ISAAC_Import_Model_Loader_Model_Collection extends ISAAC_Import_Model_Loader_Model
{
    /**
     * @inheritdoc
     */
    public function import()
    {
        $groupByIterator = new ISAAC_Import_Groupby_Iterator(
            new ArrayIterator($this->getImportValues()),
            function (ISAAC_Import_Model_Import_Value $importValue) {
                /** @var ISAAC_Import_Model_Loader_Model_Collection $this */
                return $this->getGroupValueByImportValue($importValue);
            }
        );

        foreach ($groupByIterator as $groupValue => $importValues) {
            $collection = $this->getCollectionByGroupValueAndImportValues($groupValue, $importValues);
            foreach ($importValues as $importValue) {
                /** @var ISAAC_Import_Model_Import_Value $importValue */
                if (array_key_exists($importValue->getIdentifier(), $collection)) {
                    $model = $collection[$importValue->getIdentifier()];
                } else {
                    $model = $this->getNewModelByImportValue($importValue);
                }

                $this->importModel($importValue, $model);
            }
        }
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return callable
     */
    abstract protected function getGroupValueByImportValue(ISAAC_Import_Model_Import_Value $importValue);

    /**
     * @param mixed $groupValue
     * @param ISAAC_Import_Model_Import_Value[] $importValues
     * @return Varien_Data_Collection
     */
    abstract protected function getCollectionByGroupValueAndImportValues($groupValue, array $importValues);

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return Mage_Core_Model_Abstract
     */
    abstract protected function getNewModelByImportValue(ISAAC_Import_Model_Import_Value $importValue);
}
