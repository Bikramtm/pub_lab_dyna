<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class ISAAC_Import_Model_Loader_Model_Singly extends ISAAC_Import_Model_Loader_Model
{
    /**
     * @inheritdoc
     */
    public function import()
    {
        foreach ($this->getImportValues() as $importValue) {
            $this->importModel($importValue, $this->getModelByImportValue($importValue));
        }
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return Mage_Core_Model_Abstract
     */
    abstract protected function getModelByImportValue(ISAAC_Import_Model_Import_Value $importValue);
}
