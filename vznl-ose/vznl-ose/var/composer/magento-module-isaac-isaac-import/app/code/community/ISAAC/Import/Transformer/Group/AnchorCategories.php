<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Transformer_Group_AnchorCategories implements ISAAC_Import_TransformerInterface
{
    /** @var ISAAC_Import_Helper_Data */
    protected $isaacCmsHelper;

    public function __construct()
    {
        $this->isaacCmsHelper = Mage::helper('isaac_import/data');
    }

    /**
     * @param array $parameters
     * @return array
     */
    public function apply($parameters)
    {
        $parameters['layout_handle'] = 'default,catalog_category_layered';
        $parameters['is_anchor_only'] = 1;
        $parameters['product_type_id'] = '';

        if (array_key_exists('entities', $parameters)) {
            $categoryCodes = explode(',', $parameters['entities']);
            $parameters['entities'] = $this->isaacCmsHelper->getEntityIdsFromCategoryCodes($categoryCodes);
        }

        return $parameters;
    }
}
