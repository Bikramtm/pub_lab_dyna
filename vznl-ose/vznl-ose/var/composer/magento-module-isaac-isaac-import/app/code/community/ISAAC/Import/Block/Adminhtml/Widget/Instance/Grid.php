<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl) 
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Block_Adminhtml_Widget_Instance_Grid
    extends Mage_Widget_Block_Adminhtml_Widget_Instance_Grid
{
    /**
     * Prepare grid columns
     *
     * @return Mage_Widget_Block_Adminhtml_Widget_Instance_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumnAfter('identifier', array(
            'header'    => Mage::helper('widget')->__('Identifier'),
            'align'     => 'left',
            'index'     => 'identifier',
        ), 'instance_id');

        return parent::_prepareColumns();
    }
}
