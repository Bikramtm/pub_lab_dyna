<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

Mage::log('Running install file: ' . __FILE__, null, 'install.log', true);

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$entityTypeId     = $installer->getEntityTypeId('catalog_category');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);

$installer->addAttribute('catalog_category', 'category_code', array(
    'type'         => 'varchar',
    'label'        => 'Category Code',
    'input'        => 'text',
    'required'     => false,
    'user_defined' => true
));

$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    'General Information',
    'category_code',
    100
);

//NB This attribute should be required to allow the product import to use this code:
//only required attributes are created for the admin store
$installer->addAttribute('catalog_category', 'assign_products_automatically', array(
        'type'              => 'int',
        'label'             => 'Assign Products Automatically',
        'required'          => true,
        'input'             => 'select',
        'source'            => 'eav/entity_attribute_source_boolean',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'default'           => false
));


$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    'General Information',
    'assign_products_automatically',
    110
);

$widgetInstanceTable = $installer->getTable('widget/widget_instance');

$installer->getConnection()->addColumn(
    $widgetInstanceTable,
    'identifier',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Unique widget identifier',
        'length' => 255
    )
);

$installer->getConnection()->addIndex(
    $widgetInstanceTable,
    $installer->getIdxName($widgetInstanceTable, 'identifier', Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
    'identifier',
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$installer->getConnection()->query(sprintf('UPDATE %s SET `identifier` = `title`', $widgetInstanceTable));

$installer->getConnection()->resetDdlCache();

$installer->endSetup();

Mage::log('Finished install file: ' . __FILE__, null, 'install.log', true);
