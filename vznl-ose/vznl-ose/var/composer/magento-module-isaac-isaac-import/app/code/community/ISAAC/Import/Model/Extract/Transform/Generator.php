<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
abstract class ISAAC_Import_Model_Extract_Transform_Generator implements ISAAC_Import_Generator
{
    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        $mapIterator = new ISAAC_Import_Map_Iterator($this->getExtractIterator(), [$this, 'transformValue']);
        return $mapIterator;
    }

    /**
     * @return Iterator
     */
    abstract public function getExtractIterator();

    /**
     * @param mixed $value
     * @param mixed $key
     * @return ISAAC_Import_Model_Import_Value
     */
    abstract public function transformValue($value, $key);

    /**
     * @inheritdoc
     */
    abstract public function cleanup();

}

