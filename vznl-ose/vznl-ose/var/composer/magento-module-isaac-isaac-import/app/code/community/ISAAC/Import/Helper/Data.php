<?php
/**
* ISAAC ISAAC_Import
*
* @category ISAAC
* @package ISAAC_Import
* @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)

* @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
*/
class ISAAC_Import_Helper_Data extends Mage_Core_Helper_Abstract
{

    //---------------------------
    // Importer / loader routines
    //---------------------------

    const LOADER_EXIT_CODE_SUCCESS = 123;

    /** @var Mage_Eav_Model_Config */
    protected $eavConfig;

    public function __construct()
    {
        $this->eavConfig = Mage::getSingleton('eav/config');
    }

    /**
     * @return int
     */
    public function getLoaderExitCodeSuccess() {
        return self::LOADER_EXIT_CODE_SUCCESS;
    }

    //-----------------
    // Logging routines
    //-----------------
    
    /**
     *  Write message to a log file
     *
     *  @param string $message the message to be logged
     *  @param integer $level the debug level (see Zend_Log for applicable values)
     *  @param string $file the file to which is being logged (relative to var/import)
     *  @param bool $forceLog ignore disabling of logging via the system configuration
     */
    public function logMessage($message, $level = null, $file = null, $forceLog = true)
    {
        Varien_Profiler::start(__METHOD__);
        if (!$file) {
            $file = $this->getLogFileName();
        }
        Mage::log($message, $level, $file, $forceLog);
        Varien_Profiler::stop(__METHOD__);
    }

    /**
     * @return string
     */
    public function getLogFileName()
    {
        return 'isaac_import.log';
    }

    /**
     *  Write debug message to the debug log file
     *
     *  @param string $message the message to be logged
     */
    public function logDebugMessage($message)
    {
        $this->logMessage($message, Zend_Log::DEBUG, $this->getDebugLogFileName());
    }

    /**
     * @return string
     */
    public function getDebugLogFileName()
    {
        return 'isaac_import_debug.log';
    }

    /**
     *  Strip file extension from a filename
     *
     *  @param string $filename a file name, possibly containing an extension
     *  @return string $filename without extension
     */
    public function stripFileExtension($filename) {
        return preg_replace('/\.[^.]*$/', '', $filename);
    }
    
    /**
     *  Log memory currently used by the script
     */
    public function logMemoryUsage() {
        $this->logMessage('memory usage (emalloc[peak], real[peak]): ' . memory_get_usage() . '[' . memory_get_peak_usage() . '], ' . memory_get_usage(true) . '[' . memory_get_peak_usage(true) . ']');
    }
    
    /**
     *  Get singular or plural of a word based on a count
     *
     *  @param int $count the number of occurrences of the word
     *  @param string $singular the singular version of the word
     *  @param string|bool $plural the plural version of the word ($singular . 's' is used, if left out)
     *
     *  @return string the singular version of the word, in case $count == 1
     *          the plural version of the word,   in case $count != 1
     */
    public function pluralize($count, $singular, $plural = false) {
       if ($count == 1) {
           return $singular;
       } else {
           if ($plural === false) {
               $plural = $singular . 's';
           }
           return $plural;
       }
    }
    
    /**
     *  Implode array of values to a human readable string
     *
     *  @param array $values an array of values
     *  @param string $separator a separator to be printed between two successive values, except for the last pair
     *  @param string $endSeparator a glue word to be printed between the last pair of successive values
     *
     *  @return string a human readable version of $values, using the specified glue words and separator symbol
     */
    public function implodeToReadableString($values, $separator, $endSeparator) {
        //return an empty string if the input is not an array number of values is 0
        if (count($values) == 0) {
            return '';
        }
        //pop last value from the array
        $lastValue = array_pop($values);
        if (count($values) == 0) {
            return $lastValue;
        } else {
            return implode($separator , $values) . $endSeparator . $lastValue;
        }
    }

    /**
     *  Sends an email if a new error message occurs. An error message is
     *  considered new if and only if it is not equal to the last error message
     *  that occured before this one.
     *
     *  The equality of a message is determined by comparing the base64
     *  encoded serialization of both messages. The previous message
     *  is stored in a file in the same directory as the file of this class.
     *
     *  If the previous message is older than $maximumAge seconds, an email
     *  is always sent, regardless of equality.
     *
     * @param string $id
     * @param string $errorMessage
     * @param string $importName
     * @param int $maximumAge
     */
    public function emailErrorMessage($id, $errorMessage, $importName, $maximumAge = 3600)
    {
        $previousErrorMessage = '';
        $errorFileStat = FALSE;
        $errorFileName = 'import_supplier_products_' . $id . '.last_error';
        $errorFilePath = Mage::getBaseDir('var') . '/log/' . $errorFileName;

        if (file_exists($errorFilePath)) {
            $errorFile = fopen($errorFilePath, 'r');
            $errorFileStat = fstat($errorFile);
            while (!feof($errorFile)) {
                $previousErrorMessage .= fread($errorFile, 8192);
            }
            fclose($errorFile);
        }

        // If the previous file did not exist, or
        // If the previous error is longer than $maximumAge seconds ago, or
        // If the previous error is different from the new one
        if (!$previousErrorMessage || !$errorFileStat
                || $errorFileStat['mtime'] < time() - $maximumAge
                || $previousErrorMessage != $errorMessage) {
            $this->emailMessage($importName, 'error', 'The following error occurred:' . "\n\n" . $errorMessage . "\n\n" . 'The import failed.');

            // Only write the new error if it is different, so that we can
            // check for the maximum age of the old one

            $newFile = fopen($errorFilePath, 'w');
            fwrite($newFile, $errorMessage);
            fclose($newFile);
        }
    }

    /**
     * @param string $importName
     * @param string $messageType
     * @param string $message
     * @return $this
     */
    public function emailMessage($importName, $messageType, $message)
    {
        $emailAddress = Mage::getStoreConfig('isaac_import/log_settings/recipient_email');
        if (!$emailAddress) {
            Mage::log('cannot send email because email address is empty');
            return $this;
        }
        $emailSubject = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . ': ' . $importName . ' import ' . $messageType;
        mail($emailAddress, $emailSubject, $message);

        return $this;
    }


    //----------------------
    // File related routines
    //----------------------
    
    /**
     * returns all readable file paths of certain allowed file extensions and prefixes from a directory
     * when a file is encountered that does not satisfy the requirements, a message is logged (except for the default directories '.', '..', and '.svn')
     *
     * @param string $dirName the directory from which the files are read
     * @param string[]|null $allowedFileExtensions a list of the allowed file extensions (without the '.')
     * @param string[]|null $allowedFilePrefixes a list of the allowed file prefixes
     * @return string[] an array of file paths of allowed file extensions from $allowedFileExtensions and prefixes from $allowedFilePrefixes
     * @throws Exception
     */
    public function getReadableFilePaths($dirName, $allowedFileExtensions = null, $allowedFilePrefixes = null) {
        //open input directory
        $resource = opendir($dirName);
        if (!$resource) {
            throw new Exception('could not open directory ' . $dirName);
        }
        //store all readable files of the allowed file extensions in an array
        $filePaths = array();
        while (($fileName = readdir($resource)) !== false) {
            $filePath = $dirName . '/' . $fileName;
            if ($fileName === '.' || $fileName === '..' || $fileName === '.svn') {
                //silently ignore '.', '..' and '.svn' directories
            } elseif (!file_exists($filePath)) {
                //ignore non-existing file
                $this->logMessage('ignoring file ' . $fileName . ': file does not exist');
            } elseif (!is_file($filePath)) {
                //ignore non-regular file
                $this->logMessage('ignoring file ' . $fileName . ': not a regular file');
            } elseif (!is_null($allowedFileExtensions) && !in_array(strtolower(pathinfo($filePath, PATHINFO_EXTENSION)), $allowedFileExtensions)) {
                //ignore file with unsupported file extension
                $this->logMessage('ignoring file ' . $fileName . ': file extension should be ' . $this->implodeToReadableString($allowedFileExtensions, ', ', ' or '));
            } elseif (!is_null($allowedFilePrefixes) && $this->startsWithOneOf($fileName, $allowedFilePrefixes, false) === false) {
                //ignore files with unsupported file prefixes
                $this->logMessage('ignoring file ' . $fileName . ': file prefix should be ' . $this->implodeToReadableString($allowedFilePrefixes, ', ', ' or '));
            } elseif (!is_readable($filePath)) {
                //ignore files that cannot be read
                $this->logMessage('ignoring file ' . $fileName . ': file is not readable');
            } else {
                //add filepath to the array
                $filePaths[] = $filePath;
            }
        }
        closedir($resource);
        return $filePaths;
    }

    /**
     * Return the first key of the $needles array where the corresponding value is a prefix of $haystack
     * Or false, in case no matches are found
     * @param string $haystack
     * @param string[] $needles
     * @param bool $matchCase
     * @return bool|string
     */
    public function startsWithOneOf($haystack, $needles, $matchCase = true) {
        foreach ($needles as $key => $needle) {
            if ($this->startsWith($haystack, $needle, $matchCase)) {
                return $key;
            }
        }
        return false;
    }

    /**
     * Return the contents of the log file starting at a certain line
     * @param string $startLogMessage the starting point for the requested content
     * @return string[] $output the contents of the log file starting from the line defined at $startLogMessage
               in a sorted array
     */
    public function getMessagesFromLogFileStartingFrom($startLogMessage)
    {
        $logFileName = Mage::getBaseDir('var') . DS . 'log' . DS . $this->getLogFileName();
        $fileHandle = fopen($logFileName, "r");
        if ($fileHandle === false) {
            return array();
        }
        $output = array();
        $lineNumber = 0;
        for ($characterPosition = 0; fseek($fileHandle, $characterPosition, SEEK_END) === 0; $characterPosition--) {
            if (!array_key_exists($lineNumber, $output)) {
                $output[$lineNumber] = '';
            }
            $character = fgetc($fileHandle);
            if ($character === false) {
                break;
            }
            if ($character === "\n") {
                if (strpos($output[$lineNumber], $startLogMessage)) {
                    break;
                }
                $lineNumber++;
            } else {
                $output[$lineNumber] = $character . $output[$lineNumber];
            }
        }
        fclose($fileHandle);
        $output = array_reverse($output);
        return $output;
    }

    /**
     * Test if $needle is a prefix of $haystack respecting $matchCase
     * @param string $haystack a string
     * @param string $needle a string
     * @param bool $matchCase match case
     * @return bool
     */
    public function startsWith($haystack, $needle, $matchCase = true)
    {
        if ($matchCase) {
            return strpos($haystack, $needle) === 0;
        }
        $length = strlen($needle);
        return strcasecmp(substr($haystack, 0, $length), $needle) === 0;
    }

    /**
     *  returns the lexicographically last readable file path of certain allowed file extensions and prefixes from a directory
     *  when a file is encountered that does not satisfy the requirements, a message is logged (except for the default directories '.', '..', and '.svn')
     *
     * @param string $dirName the directory from which the files are read
     * @param string[]|null $allowedFileExtensions a list of the allowed file extensions (without the '.')
     * @param string[]|null $allowedFilePrefixes
     * @return string the file path of the lexicographically last readable file path of which the extension is from $allowedFileExtensions and the prefix is from $allowedFilePrefixes
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function getLatestFilePath($dirName, $allowedFileExtensions = null, $allowedFilePrefixes = null) {
        if (!is_dir($dirName)) {
            Mage::throwException('path "' . $dirName . '" is not a directory');
        }
        $filePaths = $this->getReadableFilePaths($dirName, $allowedFileExtensions, $allowedFilePrefixes);
        if (count($filePaths) == 0) {
            Mage::throwException('could not find any readable files in directory "' . $dirName. '"');
        }
        //sort file paths in reverse
        sort($filePaths);
        $latestFilePath = array_pop($filePaths);
        foreach ($filePaths as $filePath) {
            $this->logMessage('ignoring file ' . basename($filePath) . ': there is a lexicographically newer file');
        }
        return $latestFilePath;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return string[]
     */
    public function getFullMediaGalleryFilePaths(Mage_Catalog_Model_Product $product) {
        if(!$product->getId()) {
            return array();
        }
        $mediaGallery = $product->getData('media_gallery');
        if (!$mediaGallery || !is_array($mediaGallery) || !array_key_exists('images', $mediaGallery)) {
            $this->logMessage('warning ('.__METHOD__.'): could not load media_gallery for product with sku '. $product->getSku());
            return array();
        }
        $images = $mediaGallery['images'];
        if (!is_array($images)) {
            return array();
        }
        $imageFilePaths = array();
        foreach ($images as $image) {
            if (array_key_exists('file', $image)) {
                $imageFilePaths[] = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product' . $image['file'];
            }
        }
        return $imageFilePaths;
    }

    /**
     * @param string $filePath
     * @param string[] $filePaths
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     * @throws Exception
     */
    public function mediaGalleryContainsFileWithEqualFileContentsAndFileName($filePath, array $filePaths, Mage_Catalog_Model_Product $product) {
        if($mediaGalleryData = $this->getMediaGalleryImageDataByImportFilePath($filePath, $product)) {
            foreach ($filePaths as $filePath2) {
                if($this->hasEqualFileContents($filePath, $filePath2) && $this->stripMagentoImageSuffix(basename($mediaGalleryData['file'])) == $this->stripMagentoImageSuffix(basename($filePath2))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param string $importFilePath
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function getMediaGalleryImageDataByImportFilePath($importFilePath, Mage_Catalog_Model_Product $product)
    {
        if(!$product->getId()) {
            return false;
        }
        $fileName = basename($importFilePath);
        $magentoStyleImportFileName = str_replace(' ','_',$fileName);

        $mediaGallery = $product->getData('media_gallery');
        if (!$mediaGallery || !is_array($mediaGallery) || !array_key_exists('images', $mediaGallery)) {
            $this->logMessage('warning ('.__METHOD__.'): could not load media_gallery for product with sku '. $product->getSku());
            return false;
        }
        foreach($mediaGallery['images'] as $mediaGalleryImageData) {
            $mediaGalleryImageFileName = basename($this->stripMagentoImageSuffix($mediaGalleryImageData['file']));
            if($magentoStyleImportFileName == $mediaGalleryImageFileName) {
                return $mediaGalleryImageData;
            }
        }

        return false;
    }

    /**
     * @param string $mediaGalleryImageFilePath
     * @return string
     */
    public function stripMagentoImageSuffix($mediaGalleryImageFilePath)
    {
        $mediaGalleryImageFilePathWithoutExtension = pathinfo($mediaGalleryImageFilePath, PATHINFO_FILENAME);
        if (preg_match('/(_[0-9]+)+$/', $mediaGalleryImageFilePathWithoutExtension)) {
            //Strip the _NUM parts
            $mediaGalleryImageExtension = pathinfo($mediaGalleryImageFilePath, PATHINFO_EXTENSION);
            $strippedMagentoImageSuffixFileName = preg_replace(
                '/(_[0-9]+)+$/',
                '',
                $mediaGalleryImageFilePathWithoutExtension
            );

            return $strippedMagentoImageSuffixFileName . '.' . $mediaGalleryImageExtension;
        }

        return $mediaGalleryImageFilePath;
    }

    /**
     * Test whether two files have equal contents
     * For performance reasons, the method avoids reading the two files completely
     *
     * @param string $filePath1 a filename that can be opened for reading in binary mode
     * @param string $filePath2 a filename that can be opened for reading in binary mode
     * @return bool whether the two files have equal contents
     * @throws Exception
     */
    public function hasEqualFileContents($filePath1, $filePath2)
    {
        if(filetype($filePath1) !== filetype($filePath2)) {
            return false;
        }
    
        if(filesize($filePath1) !== filesize($filePath2)) {
            return false;
        }
    
        if(!$filePointer1 = fopen($filePath1, 'rb')) {
            throw new Exception('cannot open file ' . $filePath1 . ' for reading in binary mode');
        }
    
        if(!$filePointer2 = fopen($filePath2, 'rb')) {
            fclose($filePointer1);
            throw new Exception('cannot open file ' . $filePath2 . ' for reading in binary mode');
        }
    
        $same = true;
        while (!feof($filePointer1) and !feof($filePointer2)) {
            if(fread($filePointer1, 4096) !== fread($filePointer2, 4096)) {
                $same = false;
                break;
            }
        }
    
        if(feof($filePointer1) !== feof($filePointer2)) {
            $same = false;
        }
    
        fclose($filePointer1);
        fclose($filePointer2);
    
        return $same;
    }
    
    //------------------------------
    // Import value related routines
    //------------------------------

    /**
     * @param array $rowCells a numerically indexed array of primitive values
     * @param ISAAC_Import_Model_Property[] $importValueProperties a numerically indexed array of property objects
     * @return array an array indexed by the property keys of $importValueProperties and the corresponding value from $rowCells
     * @throws Mage_Core_Exception when a property in $importValueProperties does not have a corresponding value in $rowCells or when this value is not valid for the property
     */
    public function getImportValueFromRowCells(array $rowCells, array $importValueProperties) {
        $importValue = array();
        foreach ($importValueProperties as $valueIndex => $valueProperty) {
            $valueKey = $valueProperty->getKey();
            if (!array_key_exists($valueIndex, $rowCells)) {
                Mage::throwException('column ' . $valueIndex . ' (' . $valueKey . ') does not exist');
            }
            $rowCell = $rowCells[$valueIndex];
            try {
                $valueProperty->validateValue($rowCell);
            } catch (Exception $e) {
                Mage::throwException('column ' . $valueIndex . ' (' . $valueKey . ') is invalid (' . $e->getMessage() . ')');
            }
            $importValue[$valueKey] = $valueProperty->getValue($rowCell);
        }
        return $importValue;
    }


    //-----------------------------------
    // Product attribute related routines
    //-----------------------------------
    
    /**
     * Find product attribute id by code
     *
     * @param string $attributeCode an attribute code
     * @return int|false
     */
    public function findProductAttributeId($attributeCode) {
    
        //check input parameter
        if (!preg_match('/^[a-z][a-z0-9_]*$/', $attributeCode)) {
            Mage::throwException('"' . $attributeCode . '" is not a valid attribute code');
        }
    
        //look up product attribute id in the database
        /** @var Mage_Eav_Model_Entity_Attribute $attribute */
        $attribute = Mage::getModel('eav/entity_attribute');
        /** @var Mage_Eav_Model_Entity_Attribute $attributeResource */
        $attributeResource = $attribute->getResource();
        $attributeId = $attributeResource->getIdByCode('catalog_product', $attributeCode); //Note: getResource is used to avoid caching of the id
        //logMessage('findProductAttributeId(' . $attributeCode . '): ' . $attributeId . '(' . ($attributeId ? 'not false' : 'false') . ')');
        if (!$attributeId) {
                return false;
        } else {
                return $attributeId;
        }
    }
    
    /**
     * Get product attribute option values by code
     *
     * @param string $attributeCode an attribute code
     * @return int|false
     */
    public function getProductAttributeOptionValues($attributeCode) {
    
        //look up product attribute id in the database
        $attributeId = $this->findProductAttributeId($attributeCode);
        if (!$attributeId) {
            return false;
        }
        /** @var Mage_Eav_Model_Entity_Attribute $attribute */
        $attribute = Mage::getModel('eav/entity_attribute')->load($attributeId);
        /** @var Mage_Eav_Model_Entity_Attribute_Source_Table $attributeSource */
        $attributeSource = $attribute->getSource();
        $attributeOptionValues = array();
        foreach ($attributeSource->getAllOptions(false) as $attributeOption) {
            $attributeOptionValues[] = $attributeOption['label'];
        }
        return $attributeOptionValues;
    }

    //------------------
    // Indexing routines
    //------------------

    /**
     * @param array $indexerCodes
     * @param string $indexMode
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function setIndexModeForIndexers(array $indexerCodes, $indexMode) {
        if (!in_array($indexMode, [Mage_Index_Model_Process::MODE_MANUAL, Mage_Index_Model_Process::MODE_REAL_TIME])) {
            Mage::throwException('invalid index mode' . $indexMode);
        }
        foreach ($indexerCodes as $indexerCode) {
            $indexProcess = $this->getIndexProcessByCode($indexerCode);
            if ($indexProcess->getMode() != $indexMode) {
                $indexProcess->setMode($indexMode);
                $indexProcess->save();
                $indexerName = $indexProcess->getIndexer()->getName();
                $indexModeLabel = $indexProcess->getModesOptions()[$indexMode];
                $this->logMessage('set index mode for ' . $indexerName . ' index to ' . $indexModeLabel);
            }
        }
    }

    /**
     * @param array $indexerCodes
     * @param bool $allowPartialReindex
     */
    public function reindexViaExecForIndexers(array $indexerCodes, $allowPartialReindex) {
        foreach ($indexerCodes as $indexerCode) {
            Varien_Profiler::start(__METHOD__ . '-' . $indexerCode);
            $indexProcess = $this->getIndexProcessByCode($indexerCode);
            $indexerName = $indexProcess->getIndexer()->getName();
            if (!$indexProcess->getIndexer()->isVisible()) {
                $this->logMessage('skipped reindexing the invisible ' . $indexerName . ' index');
                Varien_Profiler::stop(__METHOD__ . '-' . $indexerCode);
                return;
            }
            try {
                if (!$allowPartialReindex) {
                    /** @var $eventResource Mage_Index_Model_Resource_Event */
                    $eventResource = Mage::getResourceSingleton('index/event');
                    $eventResource->updateProcessEvents($indexProcess);
                    $this->logMessage('mark all events as done for the ' . $indexerName . ' index');
                }
                $startTime = microtime(true);
                $this->logMessage('started reindexing the ' . $indexerName . ' index');
                $shellIndexerPath = Mage::getBaseDir() . DS . 'shell' . DS . 'indexer.php';
                $reindexCommand = 'php ' . $shellIndexerPath. ' --reindex ' . escapeshellarg($indexerCode);
                $outputLines = [];
                $exitCode = 0;
                exec($reindexCommand, $outputLines, $exitCode);
                foreach ($outputLines as $outputLine) {
                    $this->logMessage($outputLine);
                }
                if ($exitCode !== 0) {
                    Mage::throwException('got exit code ' . $exitCode . ' from shell indexer');
                }
                $this->logMessage('finished reindexing the ' . $indexerName . ' index; time taken: ' . (microtime(true) - $startTime) . ' seconds');
            } catch (Exception $e) {
                $this->logMessage('error reindexing the ' . $indexerName . ' index: ' . $e->getMessage());
            }
            Varien_Profiler::stop(__METHOD__ . '-' . $indexerCode);
        }
    }

    /**
     * @param string $indexerCode
     * @return Mage_Index_Model_Process
     * @throws Mage_Core_Exception
     */
    public function getIndexProcessByCode($indexerCode)
    {
        /** @var Mage_Index_Model_Indexer $indexer */
        $indexer = Mage::getSingleton('index/indexer');
        $process = $indexer->getProcessByCode($indexerCode);
        if (!$process) {
            Mage::throwException('could not find index process with code ' . $indexerCode);
        }
        return $process;
    }

    /**
     * @return string[]
     */
    public function getAllIndexerCodes()
    {
        $indexerCodes = [];
        /** @var Mage_Index_Model_Indexer $indexer */
        $indexer = Mage::getSingleton('index/indexer');
        /** @var Mage_Index_Model_Process $indexProcess */
        foreach ($indexer->getProcessesCollection() as $indexProcess) {
            $indexerCodes[] = $indexProcess->getIndexerCode();
        }
        return $indexerCodes;
    }

    //-----------------------
    // Miscellaneous routines
    //-----------------------

    /**
     * @param array $values
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getValueWithMostDuplicates(array $values) {
        if (empty($values)) {
            Mage::throwException('argument may not be empty');
        }
        $valueCounts = array_count_values($values);
        asort($valueCounts);
        end($valueCounts);
        return key($valueCounts);
    }

    /**
     * @param array $values
     * @return bool
     */
    public function allValuesEmpty(array $values) {
        foreach ($values as $value) {
            if (!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param string $value
     * @return string
     */
    public function removeDuplicateSpaces($value) {
        return preg_replace('/\s{2,}/', ' ', $value);
    }

    /**
     * @return string
     */
    public function getCurrentTimeStamp()
    {
        date_default_timezone_set('Europe/Amsterdam');
        return utf8_encode(date('Y-m-d H:i:s'));
    }

    /**
     * @param mixed $value
     * @param array $results
     * @param array $keys
     */
    public function appendValueToResultsIndexedByKeys($value, array &$results, array $keys) {
        if (empty($keys)) {
            $results[] = $value;
            return;
        }
        $key = array_shift($keys);
        if (empty($keys)) {
            $results[$key] = $value;
            return;
        }
        if (!isset($results[$key])) {
            $results[$key] = array(); //NOTE: for performance reasons, isset is used instead of array_key_exists
        }
        $this->appendValueToResultsIndexedByKeys($value, $results[$key], $keys);
    }

    /**
     * @param string $text
     * @return string[]
     */
    public function explodeOnNewLines($text) {
        return preg_split('/\n|\r/', $text, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * @param array $categoryCodes
     * @return string
     */
    public function getEntityIdsFromCategoryCodes($categoryCodes)
    {
        $entities = array();
        foreach ($categoryCodes as $categoryCode) {
            /** @var Mage_Catalog_Model_Category $categoryModel */
            $categoryModel = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToFilter('category_code', $categoryCode)
                ->setPageSize(1)
                ->getFirstItem();

            if ($categoryEntityId = $categoryModel->getEntityId()) {
                $entities[] = $categoryEntityId;
            }
        }

        return implode(',', $entities);
    }
}
