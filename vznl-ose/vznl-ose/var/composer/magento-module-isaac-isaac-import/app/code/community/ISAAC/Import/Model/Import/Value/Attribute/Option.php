<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Import_Value_Attribute_Option extends ISAAC_Import_Model_Import_Value
{
    /**
     * @var array
     */
    protected $requiredArguments = [
        'entity_type',
        'attribute_code',
        'identifier'
    ];

    /**
     * @var string
     */
    protected $entityType;

    /**
     * @var string
     */
    protected $attributeCode;

    /**
     * @var array
     */
    protected $storeLabels = [];

    /**
     * @inheritDoc
     */
    public function __construct(array $arguments)
    {
        foreach ($this->requiredArguments as $requiredArgument) {
            if (!array_key_exists($requiredArgument, $arguments)) {
                Mage::throwException(sprintf('Missing required argument "%s"', $requiredArgument));
            }
        }

        $this->entityType = $arguments['entity_type'];
        $this->attributeCode = $arguments['attribute_code'];

        parent::__construct($arguments['identifier']);
    }

    /**
     * @inheritDoc
     */
    public function getEntityName()
    {
        return 'attribute option';
    }

    /**
     * @return string
     */
    public function getEntityType()
    {
        return $this->entityType;
    }

    /**
     * @return string
     */
    public function getAttributeCode()
    {
        return $this->attributeCode;
    }

    /**
     * @param $storeCode
     * @param $label
     */
    public function addStoreLabel($storeCode, $label)
    {
        $this->storeLabels[$storeCode] = $label;
    }

    /**
     * @return array
     */
    public function getStoreLabels()
    {
        return $this->storeLabels;
    }
}
