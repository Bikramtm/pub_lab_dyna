<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
 
class ISAAC_Import_Block_Adminhtml_Widget_Instance_Edit_Tab_Main
    extends Mage_Widget_Block_Adminhtml_Widget_Instance_Edit_Tab_Main
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Widget_Block_Adminhtml_Widget_Instance_Edit_Tab_Main
     */
    protected function _prepareForm()
    {
        $form = parent::_prepareForm();
        /** @var Varien_Data_Form_Element_Fieldset $baseFieldSet */
        $baseFieldSet = $form->getForm()->getElement('base_fieldset');
        $baseFieldSet->addField('identifier', 'text', array(
            'name'  => 'identifier',
            'label' => Mage::helper('isaac_import')->__('Identifier'),
            'title' => Mage::helper('isaac_import')->__('Identifier'),
            'class' => '',
            'required' => true,
            'note' => Mage::helper('isaac_import')->__('Widget identifier (unique per store)')
        ), 'package_theme');

        return $form;
    }
}
