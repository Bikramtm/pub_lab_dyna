##############################
## UPDATE SCRIPT FOR ENDPOINTS
### SIT SANDBOX ###
##############################

# Set webroot
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = 'http://deossyvr.dc-ratingen.de/' WHERE `path` = 'web/unsecure/base_url';
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = 'http://deossyvr.dc-ratingen.de/' WHERE `path` = 'web/secure/base_url';

# set the product visibility enabled
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = 1 WHERE `path` = 'catalog/frontend/product_visibility_enabled';

#DISABLE STUB USAGE
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = 1 WHERE `path` = 'omnius_service/general/use_stubs';

#SET OGW AS PARTYNAME
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = 'OGW' WHERE `path` = 'omnius_service/service_settings_header/party_name';

#ENDPOINT FOR POTENTIAL LINKS => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/potential_links/wsdl_potential_links';

#ENDPOINT FOR SEND DOCUMENT => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/send_document/wsdl_send_document';

#ENDPOINT FOR CREATE MANUAL WORKITEM => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/create_manual_work_item/wsdl_create_manual_work_item';

#ENDPOINT FOR SEARCHCUSTOMER => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/customer_search/wsdl';

#ENDPOINT FOR SEARCHCUSTOMER USAGE URL => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/customer_search/usage_url';

#ENDPOINT FOR SEARCHCUSTOMERFROMLEGACY => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/customer_search_legacy/wsdl';

#ENDPOINT FOR SEARCHCUSTOMERFROMLEGACY USAGE URL => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/customer_search_legacy/usage_url';

#ENDPOINT FOR RETRIEVECUSTOMERINFO => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/customer_info/wsdl';

#ENDPOINT FOR RETRIEVECREDITPROFILE => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/customer_credit_profile/wsdl_customer_credit_profile';

#ENDPOINT FOR CREATEORADDLINK => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/create_add_link/wsdl_create_add_link';

#ENDPOINT FOR INVALIDPRODUCTS => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/invalid_products/endpoint_invalid_products';

#WSDL FOR INVALIDPRODUCTS => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/invalid_products/wsdl_invalid_products';

#ENDPOINT FOR GETIPEQUIPMENTFEATURES => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/ip_equipment_features/endpoint_ip_equipment_features';

#WSDL FOR GETIPEQUIPMENTFEATURES => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/ip_equipment_features/wsdl_ip_equipment_features';

#ENDPOINT FOR NONSTANDARDRATE => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/non_standard_rate/endpoint_non_standard_rate';

#WSDL FOR NONSTANDARDRATE => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/non_standard_rate/wsdl_non_standard_rate';

#ENDPOINT FOR GETSMARTCARDCOMPATIBILITY => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/smart_card_compatibility/endpoint_smart_card_compatibility';

#WSDL FOR GETSMARTCARDCOMPATIBILITY => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/smart_card_compatibility/wsdl_smart_card_compatibility';

#ENDPOINT FOR GETSMARTCARDTYPE => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/smart_card_type/endpoint_smart_card_type';

#WSDL FOR GETSMARTCARDTYPE => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/smart_card_type/wsdl_smart_card_type';

#ENDPOINT FOR GETTVEQUIPMENTFEATURES => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/tv_equipment_features/wsdl_tv_equipment_features';

#ENDPOINT FOR GETTVEQUIPMENTFEATURES ENDPOINT => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/tv_equipment_features/endpoint_tv_equipment_features';

#ENDPOINT FOR GETCONFIGURATIONDATA => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/get_configuration_data/wsdl_get_configuration_data';

#ENDPOINT FOR GETCONFIGURATIONDATA USAGE URL => OGW
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/get_configuration_data/endpoint_get_configuration_data';

#ENDPOINT FOR SEARCHPOTENTIALLINKS => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/service_ability/wsdl_potential_links';

#ENDPOINT FOR SERVICEABILITY => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/service_ability/wsdl';

#ENDPOINT FOR VALIDATEADDRESS => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/validate_address/wsdl';

#ENDPOINT FOR CONVERTORVALIDATEIBAN => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/convert_iban/wsdl_convert_iban';

#ENDPOINT FOR GETPAYERANDPAYMENTINFOR => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/payer_and_payment_info/wsdl';

#ENDPOINT FOR SUBMITORDER => OIL
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/process_order/wsdl_process_order';

#ENDPOINT FOR SHIPPINGCONDITION => OIL => SAP
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/sap_shipping_condition/wsdl';

#ENDPOINT FOR SHIPPINGFEECALCULATE => OIL => SAP
UPDATE `omnius_sbox1`.`core_config_data` SET `value` = '' WHERE `path` = 'omnius_service/shipping_fee/wsdl';
