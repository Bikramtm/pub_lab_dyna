#!/bin/bash -v

ipaddressLNPArray=(
                    10.97.109.107
                    10.97.109.108
                    10.97.109.109
                    10.97.109.110
                    10.97.109.111
                    10.97.109.112
                    10.97.109.113
                    10.97.109.114
                  )
username=dynalean

echo "Which file do you want to copy to lnp?" "insert the full path of the file"
read FILENAME

echo "Copying files to LNP"

for ipaddress in "${ipaddressLNPArray[@]}"
do :
echo "Copying file to $ipaddress"
scp $FILENAME $username@$ipaddress:$FILENAME
done

echo "Done"