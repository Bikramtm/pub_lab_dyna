#!/bin/bash
#Simple performance test which clicks on the alcatel mobile product so the configurator loads the rules

domain=https://osf-telesales-lnp.vodafone.de
printf "\n\nExecuting test for: $domain\n"
start=`date +%s`

echo "Logging in"
curl -w "\nLookup:\t%{time_namelookup}\nConnect :\t%{time_connect}\nPre transfer:\t%{time_pretransfer}\nStart transfer:\t%{time_starttransfer}\n\nTotal execution time:\t%{time_total}\n" -o /dev/null -s "$domain/agent/account/loginPost/" -H "Cookie: oshop=queryparams||vfchannel||1001; PV=97498552; AMCV_AE8901AC513145B60A490D4C%40AdobeOrg=1304406280%7CMCIDTS%7C17303%7CMCMID%7C86416519298738753963098071537329850415%7CMCAAMLH-1495526543%7C6%7CMCAAMB-1495526543%7CNRX38WO0n5BH8Th-nqAG_A%7CMCAID%7CNONE; utag_main=v_id:015c10478fbc001343d850c0739905072001806a00bd0$_sn:1$_ss:1$_pn:1%3Bexp-session$_st:1494923544316$ses_id:1494921744316%3Bexp-session; mbox=check#true#1494921803|session#1494921742838-745344#1494923603|PC#1494921742838-745344.26_26#1496131345; adminhtml=cg47g3tspffm9no7pplfcalk0mm145p9; PHPSESSID=1b3jegngksjhi1f2l4mdecl8g6v9jbbl; _cid=L39NnMplZRB4c0xH; frontend_cid=6ZCyjfjFa54Iti8F; frontend=59v5lvtb9usar7vupp8uots4j4o87uou" -H "Origin: $domain" -H "Accept-Encoding: gzip, deflate, br" -H "Accept-Language: en-US,en;q=0.8" -H "Upgrade-Insecure-Requests: 1" -H "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36" -H "Content-Type: application/x-www-form-urlencoded" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" -H "Cache-Control: max-age=0" -H "Referer: $domain/agent/account/login/" -H "Connection: keep-alive" --data "post_key=56b78e78304ffd5623a171386c6b9bee227664e7bc9e85d92c56719bd841750c&login%5Busername%5D=demo-agent&login%5Bpassword%5D=Dyna123" --compressed
totaltimefortest=%{time_total}
echo "Done"

for i in `seq 1 100`;
do
printf "\n\n"
echo "---------Loop $i-------------------"
startloop=`date +%s`
echo "Clicking on Angebote mit vertrag"
curl -w "\nLookup:\t%{time_namelookup}\nConnect :\t%{time_connect}\nPre transfer:\t%{time_pretransfer}\nStart transfer:\t%{time_starttransfer}\n\nTotal execution time:\t%{time_total}\n" -o /dev/null -s "$domain/configurator/cart/getRules?websiteId=2&type=mobile&kip=&dsl=&address_id=&identifier=3284261&packageId=1&customer_type=0&customer_number=&hash=7304d5e8401d922852e8e13e5afc33b2" -H "Cookie: oshop=queryparams||vfchannel||1001; PV=97498552; AMCV_AE8901AC513145B60A490D4C%40AdobeOrg=1304406280%7CMCIDTS%7C17303%7CMCMID%7C86416519298738753963098071537329850415%7CMCAAMLH-1495526543%7C6%7CMCAAMB-1495526543%7CNRX38WO0n5BH8Th-nqAG_A%7CMCAID%7CNONE; utag_main=v_id:015c10478fbc001343d850c0739905072001806a00bd0$_sn:1$_ss:1$_pn:1%3Bexp-session$_st:1494923544316$ses_id:1494921744316%3Bexp-session; mbox=check#true#1494921803|session#1494921742838-745344#1494923603|PC#1494921742838-745344.26_26#1496131345; adminhtml=cg47g3tspffm9no7pplfcalk0mm145p9; PHPSESSID=1b3jegngksjhi1f2l4mdecl8g6v9jbbl; _cid=L39NnMplZRB4c0xH; frontend_cid=6ZCyjfjFa54Iti8F; frontend=sj1s4n71lie4mvdr0eu8cclgvu8peakv" -H "Accept-Encoding: gzip, deflate, sdch, br" -H "Accept-Language: en-US,en;q=0.8" -H "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36" -H "Accept: */*" -H "Referer: $domain/" -H "X-Requested-With: XMLHttpRequest" -H "Connection: keep-alive" --compressed
echo "Done"

echo "Clicking on package (AddMulti call)"
curl -w "\nLookup:\t%{time_namelookup}\nConnect :\t%{time_connect}\nPre transfer:\t%{time_pretransfer}\nStart transfer:\t%{time_starttransfer}\n\nTotal execution time:\t%{time_total}\n" -o /dev/null -s "$domain/configurator/cart/addMulti" -H "Cookie: oshop=queryparams||vfchannel||1001; PV=97498552; AMCV_AE8901AC513145B60A490D4C%40AdobeOrg=1304406280%7CMCIDTS%7C17303%7CMCMID%7C86416519298738753963098071537329850415%7CMCAAMLH-1495526543%7C6%7CMCAAMB-1495526543%7CNRX38WO0n5BH8Th-nqAG_A%7CMCAID%7CNONE; utag_main=v_id:015c10478fbc001343d850c0739905072001806a00bd0$_sn:1$_ss:1$_pn:1%3Bexp-session$_st:1494923544316$ses_id:1494921744316%3Bexp-session; mbox=check#true#1494921803|session#1494921742838-745344#1494923603|PC#1494921742838-745344.26_26#1496131345; adminhtml=cg47g3tspffm9no7pplfcalk0mm145p9; PHPSESSID=1b3jegngksjhi1f2l4mdecl8g6v9jbbl; _cid=L39NnMplZRB4c0xH; frontend_cid=6ZCyjfjFa54Iti8F; frontend=sj1s4n71lie4mvdr0eu8cclgvu8peakv" -H "Origin: $domain" -H "Accept-Encoding: gzip, deflate, br" -H "PostKey: f3630c0bd94e4a18cd3a09d4bdd5e78eb28ba2f34436a995e3edf4c7c71347fc" -H "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36" -H "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" -H "Accept-Language: en-US,en;q=0.8" -H "Accept: */*" -H "Referer: $domain/" -H "X-Requested-With: XMLHttpRequest" -H "Connection: keep-alive" --data "products%5B%5D=5236&type=mobile&section=device&simType=&oldSim=false&packageId=1&clickedItemId=5236&clickedItemSelected=true" --compressed
echo "Done"

echo "getRules"
curl -w "\nLookup:\t%{time_namelookup}\nConnect :\t%{time_connect}\nPre transfer:\t%{time_pretransfer}\nStart transfer:\t%{time_starttransfer}\n\nTotal execution time:\t%{time_total}\n" -o /dev/null -s "$domain/configurator/cart/getRules?websiteId=2&type=mobile&kip=&dsl=&address_id=&identifier=3284261&packageId=1&customer_type=0&customer_number=&hash=d9a515039c3a7274be53ea3925b4d824&products=5236" -H "Cookie: oshop=queryparams||vfchannel||1001; PV=97498552; AMCV_AE8901AC513145B60A490D4C%40AdobeOrg=1304406280%7CMCIDTS%7C17303%7CMCMID%7C86416519298738753963098071537329850415%7CMCAAMLH-1495526543%7C6%7CMCAAMB-1495526543%7CNRX38WO0n5BH8Th-nqAG_A%7CMCAID%7CNONE; utag_main=v_id:015c10478fbc001343d850c0739905072001806a00bd0$_sn:1$_ss:1$_pn:1%3Bexp-session$_st:1494923544316$ses_id:1494921744316%3Bexp-session; mbox=check#true#1494921803|session#1494921742838-745344#1494923603|PC#1494921742838-745344.26_26#1496131345; adminhtml=cg47g3tspffm9no7pplfcalk0mm145p9; PHPSESSID=1b3jegngksjhi1f2l4mdecl8g6v9jbbl; _cid=L39NnMplZRB4c0xH; frontend_cid=6ZCyjfjFa54Iti8F; frontend=sj1s4n71lie4mvdr0eu8cclgvu8peakv" -H "Accept-Encoding: gzip, deflate, sdch, br" -H "Accept-Language: en-US,en;q=0.8" -H "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36" -H "Accept: */*" -H "Referer: $domain/" -H "X-Requested-With: XMLHttpRequest" -H "Connection: keep-alive" --compressed
echo "Done"

echo "Get prices"
curl -w "\nLookup:\t%{time_namelookup}\nConnect :\t%{time_connect}\nPre transfer:\t%{time_pretransfer}\nStart transfer:\t%{time_starttransfer}\n\nTotal execution time:\t%{time_total}\n" -o /dev/null -s "$domain/configurator/init/getPrices?website_id=2&type=0&packageType=mobile&products=5236" -H "Cookie: oshop=queryparams||vfchannel||1001; PV=97498552; AMCV_AE8901AC513145B60A490D4C%40AdobeOrg=1304406280%7CMCIDTS%7C17303%7CMCMID%7C86416519298738753963098071537329850415%7CMCAAMLH-1495526543%7C6%7CMCAAMB-1495526543%7CNRX38WO0n5BH8Th-nqAG_A%7CMCAID%7CNONE; utag_main=v_id:015c10478fbc001343d850c0739905072001806a00bd0$_sn:1$_ss:1$_pn:1%3Bexp-session$_st:1494923544316$ses_id:1494921744316%3Bexp-session; mbox=check#true#1494921803|session#1494921742838-745344#1494923603|PC#1494921742838-745344.26_26#1496131345; adminhtml=cg47g3tspffm9no7pplfcalk0mm145p9; PHPSESSID=1b3jegngksjhi1f2l4mdecl8g6v9jbbl; _cid=L39NnMplZRB4c0xH; frontend_cid=6ZCyjfjFa54Iti8F; frontend=sj1s4n71lie4mvdr0eu8cclgvu8peakv" -H "Accept-Encoding: gzip, deflate, sdch, br" -H "Accept-Language: en-US,en;q=0.8" -H "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36" -H "Accept: */*" -H "Referer: $domain/" -H "X-Requested-With: XMLHttpRequest" -H "Connection: keep-alive" --compressed
echo "Done"
endloop=`date +%s`
runtimeloop=$((endloop-startloop))
printf "Total loop time: $runtimeloop \n"
echo "----------Loop $i------------------"
printf "\n\n"
done

end=`date +%s`
runtime=$((end-start))

echo "TOTAL EXECUTION TIME $runtime"