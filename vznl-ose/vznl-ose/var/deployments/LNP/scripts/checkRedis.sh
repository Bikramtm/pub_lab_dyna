#!/usr/bin/env bash

checkInMiliSeconds=30000 #sleep 30 seconds

human_size() {
        awk -v sum="$1" ' BEGIN {hum[1024^3]="Gb"; hum[1024^2]="Mb"; hum[1024]="Kb"; for (x=1024^3; x>=1024; x/=1024) { if (sum>=x) { printf "%.2f %s\n",sum/x,hum[x]; break; } } if (sum<1024) print "1kb"; } '
}

redis_cmd="redis-cli -h 10.97.109.193 -p 6379 -n 1 DBSIZE"
loopcount=0;
#for i in `seq 1 10`;
while true;
do
((loopcount++))
count=eval $redis_cmd
# get keys and sizes

# sort the list
#sorted_key_list=`echo -e "$size_key_list" | sort -n`

# print out the list with human readable sizes
#echo -e "$sorted_key_list" | while read l; do
#    if [[ -n "$l" ]]; then
#        size=`echo $l | perl -wpe 's/^(\d+).+/$1/g'`; hsize=`human_size "$size"`; key=`echo $l | perl -wpe 's/^\d+(.+)/$1/g'`; printf "%-10s%s\n" "$hsize" "$key";
#    fi

#done
echo "$(date) : Loop $loopcount : Total session keys: $count"
sleep $checkInMiliSeconds
done
