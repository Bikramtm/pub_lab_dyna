<?php
/* The script post_stage.php will be executed after the staging process ends. This will allow
 * users to perform some actions on the source tree or server before an attempt to
 * activate the app is made. For example, this will allow creating a new DB schema
 * and modifying some file or directory permissions on staged source files
 * The following environment variables are accessable to the script:
 * 
 * - ZS_RUN_ONCE_NODE - a Boolean flag stating whether the current node is
 *   flagged to handle "Run Once" actions. In a cluster, this flag will only be set when
 *   the script is executed on once cluster member, which will allow users to write
 *   code that is only executed once per cluster for all different hook scripts. One example
 *   for such code is setting up the database schema or modifying it. In a
 *   single-server setup, this flag will always be set.
 * - ZS_WEBSERVER_TYPE - will contain a code representing the web server type
 *   ("IIS" or "APACHE")
 * - ZS_WEBSERVER_VERSION - will contain the web server version
 * - ZS_WEBSERVER_UID - will contain the web server user id
 * - ZS_WEBSERVER_GID - will contain the web server user group id
 * - ZS_PHP_VERSION - will contain the PHP version Zend Server uses
 * - ZS_APPLICATION_BASE_DIR - will contain the directory to which the deployed
 *   application is staged.
 * - ZS_CURRENT_APP_VERSION - will contain the version number of the application
 *   being installed, as it is specified in the package descriptor file
 * - ZS_PREVIOUS_APP_VERSION - will contain the previous version of the application
 *   being updated, if any. If this is a new installation, this variable will be
 *   empty. This is useful to detect update scenarios and handle upgrades / downgrades
 *   in hook scripts
 */


$appLocation = getenv('ZS_APPLICATION_BASE_DIR');

$localFile = $appLocation.'/app/etc/local.xml';
$originLocalFile = __DIR__.'/local.xml';



$arr = array(
	'CRYPT' => getenv('ZS_CRYPT'),
	'DISABLE_LOCAL_MODULES' => getenv('ZS_DISABLE_LOCAL_MODULES'),
	'DB_TABLE_PREFIX' => getenv('ZS_DB_TABLE_PREFIX'),

	'DB_HOST' => getenv('ZS_DB_HOST'),
	'DB_USERNAME' => getenv('ZS_DB_USERNAME'),
	'DB_PASSWORD' => getenv('ZS_DB_PASSWORD'),
	'DB_NAME' => getenv('ZS_DB_NAME'),
	'DB_INITSTATEMENTS' => getenv('ZS_DB_INITSTATEMENTS'),
	'COMPOSER_PATH' => getenv('ZS_APPLICATION_BASE_DIR'),
    'DB_MODEL' => getenv('ZS_DB_MODEL'),
	'DB_TYPE' => getenv('ZS_DB_TYPE'),
	'DB_PDOTYPE' => getenv('ZS_DB_PDOTYPE'),
	'DB_ACTIVE' => getenv('ZS_DB_ACTIVE'),

	'SESSION_SAVE' => getenv('ZS_SESSION_SAVE'),

	'REDIS_HOST' => getenv('ZS_REDIS_HOST'),
	'REDIS_PORT' => getenv('ZS_REDIS_PORT'),
	'REDIS_TIMEOUT' => getenv('ZS_REDIS_TIMEOUT'),
	'REDIS_DB' => getenv('ZS_REDIS_DB'),
	'REDIS_COMPRESSION_THRESHOLD' => getenv('ZS_REDIS_COMPRESSION_THRESHOLD'),
	'REDIS_COMPRESSION_LIB' => getenv('ZS_REDIS_COMPRESSION_LIB'),
	'REDIS_LOG_LEVEL' => getenv('ZS_REDIS_LOG_LEVEL'),
	'REDIS_MAX_CONCURRENCY' => getenv('ZS_REDIS_MAX_CONCURRENCY'),
	'REDIS_BREAK_AFTER_FRONTEND' => getenv('ZS_REDIS_BREAK_AFTER_FRONTEND'),
	'REDIS_BREAK_AFTER_ADMINHTML' => getenv('ZS_REDIS_BREAK_AFTER_ADMINHTML'),
	'REDIS_BOT_LIFETIME' => getenv('ZS_REDIS_BOT_LIFETIME'),
	'REDIS_DISABLE_LOCKING' => getenv('ZS_REDIS_DISABLE_LOCKING'),

	'BACKEND_SERVER' => getenv('ZS_BACKEND_SERVER'),
	'BACKEND_PORT' => getenv('ZS_BACKEND_PORT'),
	'BACKEND_DB' => getenv('ZS_BACKEND_DB'),
	'BACKEND_FORCE_STANDALONE' => getenv('ZS_BACKEND_FORCE_STANDALONE'),
	'BACKEND_CONNECT_RETRIES' => getenv('ZS_BACKEND_CONNECT_RETRIES'),
	'BACKEND_READ_TIMEOUT' => getenv('ZS_BACKEND_READ_TIMEOUT'),
	'BACKEND_AUTOMATIC_CLEANING_FACTOR' => getenv('ZS_BACKEND_AUTOMATIC_CLEANING_FACTOR'),
	'BACKEND_COMPRESS_DATA' => getenv('ZS_BACKEND_COMPRESS_DATA'),
	'BACKEND_COMPRESS_TAGS' => getenv('ZS_BACKEND_COMPRESS_TAGS'),
	'BACKEND_COMPRESS_THRESHOLD' => getenv('ZS_BACKEND_COMPRESS_THRESHOLD'),
	'BACKEND_COMPRESSION_LIB' => getenv('ZS_BACKEND_COMPRESSION_LIB'),
	'BACKEND_USE_LUA' => getenv('ZS_BACKEND_USE_LUA'),
	'BACKEND_NOMATCHINGTAGS' => getenv('ZS_BACKEND_NOMATCHINGTAGS'),
);


if (file_exists($originLocalFile)) {
	$bodyXml = file_get_contents($originLocalFile);
}

if(isset($bodyXml)) {
	$bodyXml = str_replace(array_keys($arr), $arr, $bodyXml);
	file_put_contents($localFile, $bodyXml);
}

//set media permission
$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($appLocation.'/media'));
foreach($iterator as $item) {
    chmod($item, 0777);
} 


//import backup
if(file_exists($appLocation.'/var/backup.sql.gz') && getenv('ZS_RESTORE_DATABASE') == true ) {
    exec('zcat '.$appLocation.'/var/backup.sql.gz | mysql -h '.getenv('ZS_DB_HOST').' -u '.getenv('ZS_DB_USERNAME').' -p'.getenv('ZS_DB_PASSWORD').' '.getenv('ZS_DB_NAME') );
} 
