
Test webserver:

telnet 10.97.109.35 80 
GET /index.php HTTP/1.1

OR

/usr/bin/curl --verbose -H 'Host: osf-telesales-sit1.vodafone.de' 'http://10.97.109.35/index.php'




tar -xvf release.tar.gz


Database Import:



mysql -u omnius_sit1 -h 10.97.104.124 -p omnius_osf1 < SIT.sql








Config file location:



app/etc/local.xml



Zend server LogFiles:

/usr/local/zend/var/log

Logfiles:

/var/SP/data/logs




Remote tailing for logFiles:

ssh dynalean@10.97.109.35 'tail -f /var/SP/data/logs/osf-telesales-sit1.vodafone.de.error.log'
ssh dynalean@10.97.109.35 'tail -f /usr/local/zend/var/log/php-fpm.log'

Remote tailing for directories:

ssh dynalean@10.97.109.35 'tailf /var/SP/data/logs/*.log'
ssh dynalean@10.97.109.35 'tailf /usr/local/zend/var/log/*.log'

Merge tail logFiles:

ssh dynalean@10.97.109.35 'tail -f /var/SP/data/logs/osf-telesales-sit1.vodafone.de.error.log & tail -f /usr/local/zend/var/log/php-fpm.log'

Merge tail logFiles for directories:

ssh dynalean@10.97.109.35 'tailf /var/SP/data/logs/*.log & tailf /usr/local/zend/var/log/*.log'

Merge tail logFiles for directories (excluding specific files):

ssh dynalean@10.97.109.35 ls /usr/local/zend/var/log/*.log | grep -v access.log | xargs tail -f & ls /var/SP/data/logs/*.log | xargs tail -f



Redis FLUSH:

redis-cli -h 10.97.104.124 -p 6379 flushall  #SIT1
redis-cli -h 10.97.104.124 -p 6380 flushall  #SIT2
redis-cli -h 10.97.104.124 -p 6381 flushall  #SIT3
redis-cli -h 10.97.104.124 -p 6382 flushall  #SIT4


Generate logStash filters:

php generate_filters.php --environment st
Finished writing logstash filters
Download files at: /tmp/logstash-filters/

Kibana:

SIT1: http://10.97.109.74:8001
SIT2: http://10.97.109.74:8002
SIT3: http://10.97.109.74:8003
SIT4: http://10.97.109.74:8004


Permissions:

Go to the rootDirectory of the application and execute the following commands (do this for each application folder and server)

find -type d -exec chmod 755 {} \;
find -type f -exec chmod 644 {} \;
chmod 777 -R var media


Host file entry adds:


#BEGIN OGW SERVERS
10.78.58.6 deossevr.dc-ratingen.de
10.78.58.7 deossfvr.dc-ratingen.de
10.78.58.8 deossgvr.dc-ratingen.de
10.78.58.9 deosshvr.dc-ratingen.de
#END OGW SERVERS

