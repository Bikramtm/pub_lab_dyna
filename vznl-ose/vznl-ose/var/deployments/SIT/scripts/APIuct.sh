#!/bin/bash -v

clear

environmentsArray=(1,2,3,4,'a')

echo "SIT Deployment script for UCT API started"

echo "Which build version is this going to be?" "e.g. 376"
read BUILD_VERSION

echo "For which environment is this script running?" [${environmentsArray[@]}] "a = all environments"
read INPUT_ENVIRONMENT

if ! [[ " ${environmentsArray[@]} " =~ "${INPUT_ENVIRONMENT}" ]]; then
	echo "Invalid environment specified"
	exit;
fi

function postStatus
{
    curl -X POST --data "payload={ \"username\": \"$1\", \"icon_emoji\": \":octopus:\",  \"text\": \"$2\"}" https://hooks.slack.com/services/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
}

function installUCTAPI
{
    #this should be removed later on when the docroots are set correctly
    seperatefolder=1
	INPUT_ENVIRONMENT=$1
	DOCROOT_DIR='/var/SP/data/docroots/'
	ENVIRONMENT='osf-api-sit'$INPUT_ENVIRONMENT'.vodafone.de'
	DEPLOYMENT_DIR='/var/SP/data/dynalean'
	APP_DIR="${DOCROOT_DIR}${ENVIRONMENT}"
	DATE=`date +"%Y-%m-%d %H:%M"`

    #this should be removed later on when the docroots are set correctly
    if [ "$seperatefolder" == 1 ]; then
        APP_DIR="$APP_DIR/uct"
    fi

	if ! [ -d "$APP_DIR" ]; then
		echo "Application directory '$APP_DIR' doesnt exist"
		exit;
	fi

	if ! [ -d "$DEPLOYMENT_DIR" ]; then
		echo "Deployment directory '$DEPLOYMENT_DIR' doesnt exist"
		exit;
	fi

    if ! [ -f "$DEPLOYMENT_DIR/uct-api.tar.gz" ]; then
		echo "Deployment package '$DEPLOYMENT_DIR/uct-api.tar.gz' doesnt exist"
		exit;
	fi

    echo "Deploying to '$APP_DIR' NOW!"

    postStatus "Deployment-Bot-VFDE-UCT-API", "Manual deployment SIT UCT-API ${BUILD_VERSION} to UCT-API${INPUT_ENVIRONMENT}"

	# Put in new code
	cd $APP_DIR

    # match all files except
    find . -xdev -type f \
        ! -name '.env' -a \
        ! -path 'storage/*' -a \
        -delete

    # match all directories that are now empty ...
    find  . -xdev -type d \
        ! -wholename './storage' -a \
        -empty \
        -delete

	tar xf $DEPLOYMENT_DIR/uct-api.tar.gz -C $APP_DIR

	# Set permissions
	#chmod -R g+rw $APP_DIR
	#chmod -R 777 $APP_DIR/app/code/community/Dyna/Proxy

	find -type d -exec chmod 755 {} \;
	find -type f -exec chmod 644 {} \;
	chmod 777 -R storage

	# Configs
	# Local
	#echo '${bamboo.application_config_local}' > $APP_DIR + '/app/etc/local.xml'
	# Version label
	echo "<?php return 'B${BUILD_VERSION} ${DATE}';" >$APP_DIR'/SOFTWARE_VERSION.php'
	# Jobs
	#echo '${bamboo.application_config_jobs}' > $APP_DIR + '/app/etc/jobs.xml'
	# Monolog
	#echo '${bamboo.application_config_monolog}' > $APP_DIR + '/app/etc/monolog.xml'
	# AMQP
	#echo '${bamboo.application_config_amqp}' > $APP_DIR + '/shell/amqp/ESBQueueReaderMonoService.exe.config'
	# Sandbox
	#echo '${bamboo.application_config_replicas}' > $APP_DIR + '/app/etc/replicas.xml'
	#echo '${bamboo.application_config_sandbox}' > $APP_DIR + '/app/etc/sandbox.xml'

	# Restart services
	#$APP_DIR + '/shell/mono_restart.sh > /dev/null 2>&1'

	# Clear cache
	# Varnish
	#curl -X BAN 127.0.0.1
	# Redis

	#redisIP=10.97.104.124

	#echo "Flushing redis cache for SIT${1}"
	#if [ "$1" == "1" ]; then
	#{
	#   redisPort=6379
	#}
	#elif [ "$1" == "2" ]; then
	#{
	#   redisPort=6380
	#}
	#elif [ "$1" == "3" ]; then
	#{
	#   redisPort=6381
	#}
	#elif [ "$1" == "4" ]; then
	#{
	#   redisPort=6382
	#}
	#fi

	#redis-cli -h $redisIP -p $redisPort "FLUSHALL"

	# Remove maintanance
	#rm -f $APP_DIR/maintenance.flag

	postStatus "Deployment-Bot-VFDE-UCT-API", "Manual deployment UCT-API ${BUILD_VERSION} of SIT UCT-API${INPUT_ENVIRONMENT} done"
}

if [ "$INPUT_ENVIRONMENT" == "a" ]; then
	installUCTAPI 1
	installUCTAPI 2
	installUCTAPI 3
	installUCTAPI 4
else
	installUCTAPI $INPUT_ENVIRONMENT
fi

echo "Deployment succeeded"


