#!/bin/bash -v

clear

environmentsArray=(1,2,3,4,'a')

echo "Change build version script for SIT started"

echo "Which build version is this going to be?" "e.g. 376"
read BUILD_VERSION

echo "For which environment is this script running?" [${environmentsArray[@]}] "a = all environments"
read INPUT_ENVIRONMENT

if ! [[ " ${environmentsArray[@]} " =~ "${INPUT_ENVIRONMENT}" ]]; then
	echo "Invalid environment specified"
	exit;
fi

function installSit
{
	INPUT_ENVIRONMENT=$1
	DOCROOT_DIR='/var/SP/data/docroots/'
	ENVIRONMENT='osf-telesales-sit'$INPUT_ENVIRONMENT'.vodafone.de'
	DEPLOYMENT_DIR='/var/SP/data/dynalean'
	APP_DIR="${DOCROOT_DIR}${ENVIRONMENT}"
	DATE=`date +"%Y-%m-%d %H:%M"`

	if ! [ -d "$APP_DIR" ]; then
		echo "Application directory '$APP_DIR' doesnt exist"
		exit;
	fi

	if ! [ -d "$DEPLOYMENT_DIR" ]; then
		echo "Deployment directory '$DEPLOYMENT_DIR' doesnt exist"
		exit;
	fi

	# Put application in maintenance
	touch $APP_DIR/maintenance.flag

	# Version label
	echo "<?php return 'B${BUILD_VERSION} ${DATE}';" >$APP_DIR'/app/etc/SOFTWARE_VERSION.php'
	
	# Remove maintanance
	rm -f $APP_DIR/maintenance.flag
}

if [ "$INPUT_ENVIRONMENT" == "a" ]; then
	installSit 1
	installSit 2
	installSit 3
	installSit 4
else
	installSit $INPUT_ENVIRONMENT
fi


