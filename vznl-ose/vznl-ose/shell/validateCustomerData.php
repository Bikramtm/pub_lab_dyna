<?php
$empty_value_found = false;
$file = 'Full_CustomerImport_Fixed_20191108.csv';
//$file = 'fourmillion.csv';
//$file = 'Full_CustomerImport_Fixed_20191024.csv';
$handle = fopen($file, "r");
$start = date("d/m/Y h:i:sa");
$record = 0;
$telephoneCount =0;
$segmentCount =0;
$streetCount =0;
$houseNumberCount =0;
$emailCount =0;
$postalCount =0;
$dobCount =0;
$errorRecords = array();
while (($fileop = fgetcsv($handle, 1000, ",")) !== false) {
    $record++;
    if ($record == 1) {
        continue;
    }
//print_r($fileop);
    $TelephoneContactNumber = isset($fileop[0])?trim($fileop[0]):'';
    $CustomerID = isset($fileop[1])?trim($fileop[1]):'';
    $SegmentName = isset($fileop[2])?trim($fileop[2]):'';
    $Empty1 = isset($fileop[3])?trim($fileop[3]):'';
    $Empty2 = isset($fileop[4])?trim($fileop[4]):'';
    $OrgName = isset($fileop[5])?trim($fileop[5]):'';
    $CocNumber = isset($fileop[6])?trim($fileop[6]):'';
    $Initials = isset($fileop[7])?trim($fileop[7]):'';
    $NamePrefixTxt = isset($fileop[8])?trim($fileop[8]):'';
    $FamilyName = isset($fileop[9])?trim($fileop[9]):'';
    $Street = isset($fileop[10])?trim($fileop[10]):'';
    $HouseNum = isset($fileop[11])?trim($fileop[11]):'';
    $HouseNumDetail = isset($fileop[12])?trim($fileop[12]):'';
    $PostalCd = isset($fileop[13])?trim($fileop[13]):'';
    $CityName = isset($fileop[14])?trim($fileop[14]):'';
    $Mail = isset($fileop[15])?trim($fileop[15]):'';
    $BirthDt = isset($fileop[16])?trim($fileop[16]):'';
    $Gender = isset($fileop[17])?trim($fileop[17]):'';
    $Status = isset($fileop[18])?trim($fileop[18]):'';

    //telephone
//    if($TelephoneContactNumber == '') {
//        $errorRecords[$record][$CustomerID][] = "Invalid Telephone number";
//        $telephoneCount++;
//    }
    //segment
    if($SegmentName == ''){
        $errorRecords[$record][$CustomerID][] = "Invalid Customer Type/Segemnt";
        $segmentCount++;
    }
    //street
    if($Street == ''){
        $errorRecords[$record][$CustomerID][] = "Invalid Street";
        $streetCount++;
    }
    //House number
    if($HouseNum == ''){
        $errorRecords[$record][$CustomerID][] = "Invalid House Number";
        $houseNumberCount++;
    }
    //Email
    $checkEmail = checkEmail($Mail);
  //  if ($Mail == '' || !$checkEmail) {
    if ($Mail != '' && !$checkEmail) {
        $errorRecords[$record][$CustomerID][] = "Invalid Email Id";
        $emailCount++;
    }
    //Postal Code
    $checkPostal = checkPostalCode($PostalCd);
    if ($PostalCd == '' || !$checkPostal) {
        $errorRecords[$record][$CustomerID][] = "Invalid Postal Code";
        $postalCount++;
    }
    //DOB
    $checkDate = checkDOB(date("m-d-Y", strtotime($BirthDt)));
    if ( $SegmentName == 'Residential' && ($BirthDt == '' || !$checkDate)) {
        $errorRecords[$record][$CustomerID][] = "Invalid DOB";
        $dobCount++;
    }


}
//print_r($errorRecords);
logError($errorRecords);
$end = date("d/m/Y h:i:sa");
echo '#####################################################'. PHP_EOL;
echo '                 VALIDATION REPORT'. PHP_EOL;
echo '#####################################################'. PHP_EOL;
//echo 'Total Telephone Failure:' . $telephoneCount . PHP_EOL;
echo 'Total Segment Failure:' . $segmentCount . PHP_EOL;
echo 'Total Street Failure:' . $streetCount . PHP_EOL;
echo 'Total House Number Failure:' . $houseNumberCount . PHP_EOL;
echo 'Total Email Failure:' . $emailCount . PHP_EOL;
echo 'Total Postal Code Failure:' . $postalCount . PHP_EOL;
echo 'Total DOB Failure:' . $dobCount . PHP_EOL;
echo '#####################################################'. PHP_EOL;
echo '                  SUMMARY'. PHP_EOL;
echo '#####################################################'. PHP_EOL;
echo 'Total Records:' . $record . PHP_EOL;
echo 'Total Failed Records:' . sizeof($errorRecords) . PHP_EOL;
echo 'start time:' . $start . PHP_EOL . 'End time:' . $end . PHP_EOL;
echo 'Total seconds: ' . round((strtotime($end) - strtotime($start)), 1) .'S'. PHP_EOL;
echo '#####################################################'. PHP_EOL;
echo '                  END'. PHP_EOL;
echo '#####################################################'. PHP_EOL;

function checkDOB($dob)
{
    $tempDate = explode('-', $dob);
    // checkdate(month, day, year)
    return checkdate($tempDate[0], $tempDate[1], $tempDate[2]);
}

function checkEmail($email)
{
    // return filter_var($email, FILTER_VALIDATE_EMAIL);
    if (preg_match("/^[a-zA-Z0-9.!#$%&'*+=?^_`{|}~-]+@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $email)) {
        return true;
    }
    return false;
}

function checkTelphone($phone)
{
    if(!preg_match("/^(097[0-9]{9}|3197[0-9]{9}|\+3197[0-9]{9}|003197[0-9]{9}|06[0-9]{8}|316[0-9]{8}|\+316[0-9]{8}|00316[0-9]{8})$/", $phone)) {
        return true;
    }
    return false;

}

function checkPostalCode($postalCode)
{
    if(preg_match("/^([0-9]{4} [A-Z]{2})$/", $postalCode)) {
        return true;
    }
    return false;
}
function logError($log_msg)
{
    $log_filename = "log";
    if (!file_exists($log_filename))
    {
        // create directory/folder uploads.
        mkdir($log_filename, 0777, true);
    }
    $log_file = $log_filename.'/customerError.log';
    // if you don't add `FILE_APPEND`, the file will be erased each time you add a log
    file_put_contents($log_file, print_r($log_msg, true) . "\n");
}

?>