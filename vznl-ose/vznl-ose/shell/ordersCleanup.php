<?php

/**
 * Shell script to delete the old orders permanently based on the number of days that have passed since the order creation
 * (configurable admin setting)
 */

require_once 'abstract.php';

/**
 * Magento Log Shell Script
 *
 * @category    Dyna
 * @author      Dyna
 */
class Dyna_Orders_Cleaner extends Mage_Shell_Abstract
{
    private $cliDebug = false;

    private function log($message)
    {
        if(!Mage::getStoreConfig('omnius_general/cleanup_settings/log_file_debug')) {
            return;
        }
        Mage::log($message, null, "orders-cleanup.log");
    }

    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }
    /**
     * Run script
     *
     */
    public function run()
    {
        if ($noOfDaysForCleanup = Mage::getStoreConfig('omnius_general/cleanup_settings/no_of_days_order_alive')) {
            $date = date('Y-m-d H:i:s', strtotime('-'.$noOfDaysForCleanup.' day', strtotime(now())));

            $superOrders = Mage::getModel('superorder/superorder')->getCollection()
                ->addFieldToFilter('created_at', array('lteq' => $date));

            if(!$superOrders) {
                $msg = "No super orders were found older than ". $date;
                //write cli log
                if ($this->cliDebug) {
                    $this->_writeLine($msg);
                }
                $this->log($msg);
                return;
            }

            $msg = "Starting deleting orders older than ".$noOfDaysForCleanup." days, meaning older than: " . $date;
            if ($this->cliDebug) {
                $this->_writeLine($msg);
            }
            $this->log($msg);

            $deletedSuperOrdersIds = [];
            foreach($superOrders as $order)
            {
                $id = $order->getId();

                try{
                    $order->delete();
                    $msg = "super order #".$id." is removed";
                    if ($this->cliDebug) {
                        $this->_writeLine($msg);
                    }
                    $this->log($msg);
                } catch(Exception $e){
                    $msg = "super order #".$id." could not be removed: ".$e->getMessage();
                    if ($this->cliDebug) {
                        $this->_writeLine($msg);
                    }
                    $this->log($msg);
                    $id = null;
                }
                if($id) {
                    $deletedSuperOrdersIds[] = $id;
                }
            }

            $orders = Mage::getModel('sales/order')->getCollection()
                ->addFieldToFilter('superorder_id', array('in' => $deletedSuperOrdersIds));

            foreach($orders as $order)
            {
                $id = $order->getId();

                try{
                    $order->delete();
                    $msg = "sales order #".$id." is removed";
                    if ($this->cliDebug) {
                        $this->_writeLine($msg);
                    }
                    $this->log($msg);
                }catch(Exception $e){
                    $msg = "sales order #".$id." could not be removed: ".$e->getMessage();
                    if ($this->cliDebug) {
                        $this->_writeLine($msg);
                    }
                    $this->log($msg);
                }
            }

            $this->log($msg);
            $msg = "Finished orders deletion... ";
            if ($this->cliDebug) {
                $this->_writeLine($msg);
            }
            $this->log($msg);
        }
        else {
            $msg = "The number of days should be set in Admin. Only the orders older than that number of days will be deleted";
            if ($this->cliDebug) {
                $this->_writeLine($msg);
            }
            $this->log($msg);
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php ordersCleanup.php 
  help              This help

USAGE;
    }
}

$shell = new Dyna_Orders_Cleaner();
$shell->run();