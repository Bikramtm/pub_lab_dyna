<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'dyna_abstract.php';

class Dyna_Import_Xml_Cli extends Dyna_Shell_Abstract
{
    protected $types;
    protected $_importHelper;
    /**
     * Run appropriate import script based on the --type param
     *
     */
    public function run()
    {
        parent::run();
        if (!$this->setStack()) {
            $this->handleOutputWithInterruptOrNot(
                $this->getMessage('noStack'),
                true,
                'STDERR'
            );
        }
        $this->_importHelper = Mage::helper("dyna_import/data");
        $this->types = Mage::helper('dyna_import')->getXMLImportCategories();
        $args = [
            'type'        => $this->getArg('type'),
            'file'        => $this->getArg('file'),
            'schema'      => $this->getArg('schema'),
            'status'      => $this->getArg('status'),
            'executionId' => $this->getArg('executionId'),
            'logCategory' => $this->getArg('logCategory'),
        ];

        if (!$args['type'] || !in_array($args['type'], $this->types)) {
            fwrite(STDERR, 'Invalid type, please specify [' . implode('/', $this->types) . ']');
            exit(1);
        }

        try {
            //skipping reindex on save models to reduce import time. we reindex everything at the end of import_full.sh
            //or manually reindex after manual individual imports
            Mage::register('import-skip-reindex-on-save', 1);

            switch ($args['type']) {
                case 'futureTransactions':
                    /** @var Dyna_Customer_Model_Import_FutureTransactions $importModel */
                    $importModel = Mage::getModel('dyna_customer/import_futureTransactions', $args);
                    $importModel->run();
                    break;
                case 'packageTypes':
                    /** @var Dyna_Package_Model_Import_PackageTypes $importModel */
                    $importModel = Mage::getModel('dyna_package/import_packageTypes', $args);
                    $importModel->setStack($this->stack);
                    $importModel->run();
                    break;
                case 'categories':
                    /** @var Dyna_Catalog_Model_Import_Categories $importModel */
                    $importModel = Mage::getModel('dyna_catalog/import_categories', $args);
                    $importModel->setStack($this->stack);
                    $importModel->run();
                    break;
                case 'products':
                    /** @var Dyna_Catalog_Model_Import_Products $importModel */
                    $importModel = Mage::getModel('dyna_catalog/import_products', $args);
                    $importModel->setStack($this->stack);
                    $importModel->run();
                    break;
                case 'productMatchRules':
                    /** @var Dyna_ProductMatchRule_Model_ImporterXML $importModel */
                    $importModel = Mage::getModel('dyna_productmatchrule/importerXML', $args);
                    $importModel->setStack($this->stack);
                    $importModel->run();
                    break;
                case 'multimapper':
                    /** @var Dyna_MultiMapper_Model_ImporterXML $importModel */
                    $importModel = Mage::getModel('dyna_multimapper/importerXML', $args);
                    $importModel->setStack($this->stack);
                    $importModel->run();
                    break;
                case 'typeSubtypeCombinations':
                    /** @var Dyna_Customer_Model_TypeSubtypeImporterXML $importModel */
                    $importModel = Mage::getModel('dyna_customer/typeSubtypeImporterXML', $args);
                    $importModel->run();
                    break;
                case 'activityCodes':
                    /** @var Dyna_Customer_Model_ActivityCodesImporterXML $importModel */
                    $importModel = Mage::getModel('dyna_customer/activityCodesImporterXML', $args);
                    $importModel->run();
                    break;
                case 'salesRules':
                    /** @var Dyna_PriceRules_Model_ImporterXML $importModel */
                    $importModel = Mage::getModel('dyna_pricerules/importerXML', $args);
                    $importModel->setStack($this->stack);
                    $importModel->run();
                    break;
                default:
                    // -- nothing
            }
            // --
            Mage::unregister('import-skip-reindex-on-save');
        } catch (Exception $exception) {
            $this->_hasError = true;

            if ($importModel) {
                Mage::log($exception->getMessage(), Zend_Log::ERR, $importModel->_helper->getImportLogFile());
            } else {
                Mage::getSingleton('core/logger')->logException($exception);
            }
            $msg = 'While processing file ' . Mage::getBaseDir('var') . DS . 'import' . DS . $args['file'] . ' we encountered the following:';
            $this->_importHelper->postToSlack($msg, $exception->getMessage());
            echo $exception->getMessage() . PHP_EOL;
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $this->types = Mage::helper('dyna_import')->getXMLImportCategories();
        $availableTypes = PHP_EOL. "\t\t\t\t\t". implode(PHP_EOL."\t\t\t\t\t", $this->types) .PHP_EOL."\t\t\t\t";
        return <<<USAGE
Usage:  php file -- [options]

  help							This help
  file							Filename to import
  type							Import type [$availableTypes]
  logCategory					Log category type used to group the imports when an summary export is made
  status                        Check whether an import is running

USAGE;
    }
}

// Example
// php import_xml.php --type futureTransactions --file futureTransactions.xml --schema futureTransactions.xsd

$import = new Dyna_Import_Xml_Cli();
$import->run();
