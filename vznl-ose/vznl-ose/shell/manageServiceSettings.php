<?php

require_once 'abstract.php';

class Omnius_ServiceSettings_Cli extends Mage_Shell_Abstract
{
    private $availableSections = array();

    public function run()
    {
        $this->availableSections = json_decode(file_get_contents('manageServiceSettings.json'), true);

        $action = $this->getArg('a');
        $configSection = $this->getArg('s');
        $selectedKey = $this->getArg('k');
        $configValue = $this->getArg('v');

        switch ($action){
            case 'list-all' :
                echo $this->getAvailableSections();
                break;
            case 'list-option' :
                echo $this->getAvailableKeys($configSection);
                break;
            case 'get-value' :
                echo Mage::getStoreConfig($this->getLabelBase($configSection).$selectedKey);
                break;
            case 'set-value' :
                $labelBase = $this->getLabelBase($configSection);

                Mage::log(get_current_user(). '|' . $labelBase.$selectedKey . '|' . $configValue, null, 'manage_service_settings.log');
                Mage::getConfig()->saveConfig($labelBase.$selectedKey , $configValue, 'default', 0);
                Mage::getModel('core/config')->cleanCache();
                echo 'Saved config and flushed config cache.';
                break;
            default:
                echo 'Action not found, try --help to list commands';
        }

        echo PHP_EOL;
    }

    /**
     * Iterates over the json string and prints the nodes within it
     * @return string
     */
    public function getAvailableSections(){
        $returnString = '';

        foreach ($this->availableSections as $configNode){
            $returnString .= $configNode['label'] . PHP_EOL;
        }

        return $returnString;
    }

    /**
     * Iterates over the json string and finds all keys related to the config section
     * @param $name string Label of the config section
     * @return string
     */
    public function getAvailableKeys($name){
        $returnString = '';

        foreach ($this->availableSections as $configNode){
            if($configNode['label'] == $name){
                foreach($configNode['keys'] as $key => $value){
                    $returnString .= $key. ' : '. $value . PHP_EOL;
                }
                return $returnString;
            }
        }
    }

    /**
     * Iterates over the json string and finds the related base string
     * @param $name string The label of the config section
     * @return string
     */
    public function getLabelBase($name){
        foreach ($this->availableSections as $configNode){
            if($configNode['label'] == $name){
                return $configNode['base'];
            }
        }
    }

    public function usageHelp()
    {
        return '
        Available commands :
        [-a]        Execute action
        [-s]        Related config section, quoted
        [-k]        Related config key
        [-v]        New config key value, quoted
        
        Available actions:
        list-all
        list-option -s \'SECTION\'
        get-value -s \'SECTION\' -k KEY
        set-value -s \'SECTION\' -k KEY -v \'VALUE\'
        
        Examples: 
        -a list-option -s \'Job Queue AMQP Settings\'
        -a get-value -s \'Job Queue AMQP Settings\' -k amqp_user
        -a set-value -s \'Job Queue AMQP Settings\' -k amqp_user -v \'new_user\'' . PHP_EOL;
    }
}

$serviceSettings = new Omnius_ServiceSettings_Cli();
$serviceSettings->run();