<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'dyna_abstract.php';

/**
 * Class Dyna_Import_Cli
 */
class Dyna_Import_Cli extends Dyna_Shell_Abstract
{
    const XML_CAMPAIGN = 'campaign';
    const XML_CAMPAIGN_OFFER = 'campaignOffer';
    const XML_SALESID_DEALERCODE = 'salesIdDealercode';
    const XML_BUNDLES = 'bundles';
    const XML_CATALOG_IMPORT = 'catalog_import_log';
    const XML_MNP_FREE_DAYS = 'mnpFreeDays';
    const XML_MOBILEPROVIDERS = 'mobileProvidersList';
    const LOG_EXTENSION = '.log';
    // enable/disable cli debug
    protected $cliDebug = true;

    protected $types = null;

    protected $validationSchemas = [
        self::XML_CAMPAIGN => 'Campaign.xsd',
        self::XML_CAMPAIGN_OFFER => 'Campaign_offer.xsd',
        self::XML_SALESID_DEALERCODE => 'Campaign_dealercode.xsd',
        self::XML_BUNDLES => 'Bundles.xsd',
        self::XML_CATALOG_IMPORT => 'Catalog_Import_Log.xsd',
        self::XML_MNP_FREE_DAYS => 'MNPfreedays.xsd',
        self::XML_MOBILEPROVIDERS => 'Mobileproviderlist.xsd'
    ];

    protected $_convertToUTF8;

    protected $_pathToFile = '';
    /** @var Dyna_Import_Helper_Data _importHelper */
    protected $_importHelper;

    protected $_importLogCategory;

    protected $_simpleImportObject = null;

    protected $_hasError = false;

    protected $_executionId = null;
    protected $importId = null;
    protected $_file = null;

    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()) . " " . $msg . PHP_EOL;
    }

    /**
     * Run appropriate import script based on the --type param
     *
     */
    public function run()
    {
        parent::run();
        $this->types = Mage::helper('dyna_import')->getImportCategories();

        if ($this->getArg('status')) {
            $messages = Mage::helper("dyna_sandbox")->checkRunningImports();
            count($messages);
            foreach ($messages as $message) {
                $this->_writeLine($message);
            }
            //if count > 1, we have errors
            if (count($messages) > 1) {
                fwrite(STDERR, var_dump($messages));
                exit(1);
            }
        }

        $this->getExecutionId();
        if (!$this->setStack()) {
            $this->handleOutputWithInterruptOrNot(
                $this->getMessage('noStack'),
                true,
                'STDERR'
            );
        }

        $type = $this->getArg('type');
        if (!$type || !in_array($type, $this->types)) {
            $this->_writeLine('Invalid type, please specify [' . implode('/', $this->types) . ']');
            fwrite(STDERR, 'Invalid type, please specify [' . implode('/', $this->types) . ']');
            exit(1);
        }

        $category = $this->getArg('logCategory');
        $this->_importLogCategory = $category ? $category : $type;
        unset($category);

        $file = $this->getArg('file');
        $this->_file = $file;
        if($type === 'provisSalesId' && file_exists($this->getImportDir() . DS . 'provis' .  DS .  $file))
        {
            $path = $this->getImportDir() . DS . 'provis' .  DS .  $file;
        }
        else
        {
            $path = $this->getImportDir() . DS . $file;
        }

        if (!$file || !file_exists($path)) {
            $this->_writeLine("Import file '$file' not found, should be in '$path'\n");
            fwrite(STDERR, "Import file '$file' not found, should be in '$path'\n");
            exit(1);
        }

        $contents = file_get_contents($path);
        $originalPath = $path;
        $isUTF8Converted = false;
        $this->_hasError = false;
        $bom = pack("CCC", 0xef, 0xbb, 0xbf);
        (0 === strncmp($contents, $bom, 3)) ? $withBoom = true : $withBoom = false;

        if (in_array(pathinfo($path, PATHINFO_EXTENSION), ['json', 'csv', 'xml']) && (!mb_check_encoding($contents, 'UTF-8') || $withBoom) && $type != 'provis_sales_id') {
            $this->_writeLine('********************************************');
            $this->_writeLine('[WARNING] Import file is not encoded in UTF8.');
            $this->_writeLine('********************************************');
            $this->_writeLine('File "' . $originalPath . '" will be converted to UTF8.');
            if ($withBoom) {
                $contents = substr($contents, 3);
            } else {
                $encoding = mb_detect_encoding($contents, mb_detect_order(), true);
                if ($encoding) {
                    iconv($encoding, "UTF-8", $contents);
                } else {
                    $this->_writeLine('Failed to detect encoding for file contents: . ');
                }
            }
            file_put_contents($path . '_UTF8', $contents);
            $path = $path . '_UTF8';
            $isUTF8Converted = true;
            $this->_writeLine('File converted to UTF8.');
        }

        $bundlesSchemaPath = Mage::getModuleDir('etc', 'Dyna_Bundles') . DIRECTORY_SEPARATOR . 'schema' . DIRECTORY_SEPARATOR . 'import';
        $importSchemaPath = Mage::getBaseDir() . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'xsd';
        StopWatch::start();
        //path to imported file
        $this->_pathToFile = $path;
        $this->_importHelper = Mage::helper("vznl_import");

        try {
            //skipping reindex on save models to reduce import time. we reindex everything at the end of import_full.sh
            //or manually reindex after manual individual imports
            Mage::register('import-skip-reindex-on-save', 1);

            $this->_importHelper->setPathToImportFile($this->_pathToFile);
            switch ($type) {
                case 'cableArtifact':
                    $this->_importArtifact($path);
                    break;
                case 'cableProduct':
                    $this->_importProductFile($path, 'cable');
                    break;
                case 'mobileProduct':
                    $this->_importProductFile($path, 'mobile');
                    break;
                case 'fixedProduct':
                    $this->_importProductFile($path, 'fixed');
                    break;
                case 'mixmatch':
                    $this->_importMixMatchFile($path);
                    break;
                case 'multimapper':
                    $this->_importMultiMapperFile($path);
                    break;
                case 'categories':
                    $this->_importCategoriesFile($path);
                    break;
                case 'categoriesTree':
                    $this->_importCategoriesTree('tree');
                    break;
                case 'categoriesTreeProducts':
                    $this->_importCategoriesTree('products');
                    break;
                case 'dealerCampaigns':
                    $this->_importDealerCampaignFile($path, 'dealerImportCampaign');
                    break;
                case 'mobileSalesRules':
                    $this->_importSalesRules($path, 'mobileSalesRules');
                    break;
                case 'fixedSalesRules':
                    $this->_importSalesRules($path, 'fixedSalesRules');
                    break;
                case 'salesRules':
                    $this->_importSalesRules($path, 'salesRules');
                    break;
                case 'productMatchRules': //compatibility rules
                    $this->_importProductMatchRules($path);
                    break;
                case 'packageTypes':
                    $this->_importPackageTypes($path, 'import_package');
                    break;
                case 'packageCreationTypes':
                    $this->_importPackageCreationTypes($path);
                    break;
                case 'campaign':
                    $this->_importCampaignData($path, self::XML_CAMPAIGN, $importSchemaPath);
                    break;
                case 'campaignOffer':
                    $this->_importCampaignData($path, self::XML_CAMPAIGN_OFFER, $importSchemaPath);
                    break;
                case 'salesIdDealercode':
                    $this->_importSalesIdDealerCode($path, $importSchemaPath);
                    break;
                case 'bundles':
                    $this->_importBundlesData($path, $importSchemaPath);
                    break;
                case 'vodafoneShip2Stores':
                    $this->_importVodafoneShip2Stores();
                    break;
                case 'serviceProviders':
                    $this->_importServiceProviders();
                    break;
                case 'agentCommisions':
                    $this->_importAgentCommisions();
                    break;
                case 'checkoutProducts':
                    $this->_importCheckoutProducts();
                    break;
                case 'provisSalesId':
                    $this->_importProvisSalesId();
                    break;
                case 'logfile':
                    $this->_importLogFile($path, $importSchemaPath);
                    break;
                case 'productFamily':
                    $this->_importProductFamily();
                    break;
                case 'productVersion':
                    $this->_importProductVersion();
                    break;
                case 'optionTypes':
                    $this->_importOptionTypes($path);
                    break;
                case 'communicationVariables':
                    $this->_importCommunicationVariables($path);
                    break;
                case 'phoneBookKeywords':
                    $this->_importPhoneBookKeywords();
                    break;
                case 'phoneBookIndustries':
                    $this->_importPhoneBookIndustries();
                    break;
                case 'mnpFreeDays':
                    $this->_importMnpFreeDaysData($path, self::XML_MNP_FREE_DAYS, $importSchemaPath);
                    break;
                case 'mobileProvidersList':
                    $this->_importMsProvidersData($path, self::XML_MOBILEPROVIDERS, $importSchemaPath);
                    break;
                default:
                    break;
            }

            Mage::unregister('import-skip-reindex-on-save');

        } catch (Exception $e) {
            $this->_hasError = true;
            Mage::getSingleton('core/logger')->logException($e);
            Zend_Debug::dump($e->getMessage());

            if ($this->_simpleImportObject) {
                $this->_simpleImportObject->_logError('Skipped file because of ' . $e->getMessage());
                $this->logImportedFile($this->_simpleImportObject);
            }

            if ($this->_executionId) {
                /** @var Dyna_Sandbox_Model_ImportInformation $executionModel */
                $executionModel = Mage::getModel('dyna_sandbox/importInformation');
                $executionModel->load($this->_executionId)
                    ->setStatus($executionModel::STATUS_ERROR)
                    ->setErrors(1)
                    ->save();
            }
            fwrite(STDERR, 'Skipped file because of ' . $e->getMessage());
            exit(1);
        }

        if ($isUTF8Converted && !$this->_hasError) {
            $this->_writeLine('++++++++++++++++');
            $this->_writeLine('File "' . $originalPath . '" was imported with auto convert to UTF8.');
            $this->_writeLine('++++++++++++++++');
        }
        $this->_writeLine(sprintf('Time taken for import %s', StopWatch::elapsed()));
    }

    /**
     * Import mobile sales rules or fixed sales rules
     * @param $path
     * @param $type
     */
    protected function _importSalesRules($path, $type = null)
    {
        /** @var Vznl_PriceRules_Model_ImportPriceRules $simpleImport */
        $simpleImport = Mage::getModel("vznl_pricerules/importPriceRules");
        $simpleImport->setStack($this->stack);
        $simpleImport->setExistingRules();
        $this->_simpleImportObject = $simpleImport;
        $simpleImport->setDebug(false);
        $simpleImport->setImportType($type);
        $this->handleImportLogFile($path, $simpleImport->getFileLogName());

        $this->_writeLine('Importing ' . $type . ' sales rules ' . $path);
        $file = fopen($path, 'r');
        $lineCnt = 0;

        $this->logHeader($simpleImport);        
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {            
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->process($data, $lineCnt);
            } else {
                $check = $this->_importHelper->checkHeader($line, 'salesRules');
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if ($check) {
                    $simpleImport->setHeader($header);
                    continue;
                }
                break;
            }
        }        
        $simpleImport->removeUnusedRules();
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing ' . $type . ' sale rules file ' . $path);
    }

    /**
     * Import options type reference
     * @param $path
     */
    protected function _importOptionTypes($path)
    {
        /** @var Dyna_Catalog_Model_ImportOptionTypes $simpleImport */
        $simpleImport = Mage::getModel("dyna_catalog/import_OptionType");
        $this->_simpleImportObject = $simpleImport;
        $simpleImport->setDebug(false);
        $simpleImport->setImportType('optionTypes');
        $this->handleImportLogFile($simpleImport);

        $this->_writeLine('Importing option type ' . $path);
        $file = fopen($path, 'r');
        $lineCnt = 0;

        $this->logHeader($simpleImport);

        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->process($data);
            } else {
                $check = $this->_importHelper->checkHeader($line, 'optionTypes');
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if($check){
                    $simpleImport->setHeader($header);
                    $simpleImport->removePreviousData(Mage::getModel('dyna_catalog/checkoutOptionTypeReferencetable'));
                    continue;
                }
                break;
            }
        }
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing option type file ' . $path);
    }

    /**
     * Import communication variable reference
     * @param $path
     */
    protected function _importCommunicationVariables($path)
    {
        /** @var Dyna_Catalog_Model_ImportOptionTypes $simpleImport */
        $simpleImport = Mage::getModel("dyna_catalog/import_CommunicationVariable");
        $this->_simpleImportObject = $simpleImport;
        $simpleImport->setDebug(false);
        $simpleImport->setImportType('communicationVariables');
        $this->handleImportLogFile($simpleImport);

        $this->_writeLine('Importing communication variable ' . $path);
        $file = fopen($path, 'r');
        $lineCnt = 0;

        $this->logHeader($simpleImport);

        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->process($data);
            } else {
                $check = $this->_importHelper->checkHeader($line, 'communicationVariables');
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if($check){
                    $simpleImport->setHeader($header);
                    $simpleImport->removePreviousData(Mage::getModel('dyna_catalog/communicationVariableReference'));
                    continue;
                }
                break;
            }
        }
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing option type file ' . $path);
    }

    protected function _importLogFile($path, $importSchemaPath)
    {
        /** @var Vznl_Import_Model_Summary $model */
        $model = Mage::getModel("vznl_import/summary");
        $xml = $this->getXmlData($path, self::XML_CATALOG_IMPORT, true, $importSchemaPath, $model);
        $pathFile = explode('/',$this->_pathToFile);
        $fileName = end($pathFile);
        $model->setFilename($fileName)
            ->setCategory($this->_importLogCategory)
            ->setCreatedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()))
            ->setHasErrors(1)
            ->setHeadersValidated(0)
            ->setTotalRows(0)
            ->setImportedRows(0)
            ->setSkippedRows(0);
        if (isset($this->_executionId)) {
            $model->setExecutionId($this->_executionId);
        }
        $model->save();
        $this->_writeLine('Importing ' . $xml["Stack"] . ' version file from ' . $this->_pathToFile);

        //ignoring Stack node from xml file - VFDED1W3S-2782
//        $importStack = $xml["Stack"];
        $interfaceVersion = $xml["InterfaceVersion"] ?? null;
        $referenceDataBuild = $xml["ReferenceDataBuild"] ?? null;
        $importDate = $xml["Date"] ?? null;
        $importLog = $xml["Log"] ?? null;
        $importAuthor = $xml["Author"] ?? null;

        if (!is_array($xml["Files"]["File"])) {
            $xml["Files"]["File"] = [$xml["Files"]["File"]];
        }

        $this->handleImportLogFile($path, strtolower($this->stack) . '_' . $model->getFileLogName());

        $this->_importHelper->logMsg(sprintf('Starting import of file: %s', $this->_pathToFile), false);

        $skippedFileRows = $totalFileRows = 0;
        foreach ($xml["Files"]["File"] as $file) {
            $entry = new Dyna_CatalogLog_Model_Cataloglog();
            $entry->setStack($this->stack);
            $entry->setDate($importDate);
            $entry->setLog($importLog);
            $entry->setAuthor($importAuthor);
            $entry->setInterfaceVersion($interfaceVersion);
            $entry->setReferenceDataBuild($referenceDataBuild);

            $entry->setFileName($file);
            try {
                $entry->save();
                $totalFileRows++;
            } catch (Exception $e) {
                $skippedFileRows++;
            }
        }

        $this->_importHelper->logMsg(sprintf('Import contains in total %d rows', $totalFileRows), false);
        $this->_importHelper->logMsg(sprintf('Successfully imported %d rows', $totalFileRows - $skippedFileRows), false);
        $this->_importHelper->logMsg(sprintf('Skipped rows with error %d', $skippedFileRows), false);
        $this->_importHelper->logMsg(sprintf('Ending import of file: %s', $this->_pathToFile), false);

        $pathFile = explode('/', $this->_pathToFile);
        $fileName = end($pathFile);
        $model->setFilename($fileName)
            ->setCategory('logfile')
            ->setCreatedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()));

        if ($this->_hasError) {
            $model->setHasErrors(1);
        } else {
            if ($totalFileRows && $skippedFileRows == $totalFileRows) {
                $model->setHasErrors(1);
            } else {
                $model->setHasErrors(0);
            }
            $model->setHeadersValidated(($totalFileRows == 0 && $skippedFileRows == 0 ? 0 : 1))
                ->setTotalRows($totalFileRows)
                ->setImportedRows($totalFileRows - $skippedFileRows)
                ->setSkippedRows($skippedFileRows);

            if (isset($this->_executionId)) {
                $model->setExecutionId($this->_executionId);
            }
        }

        $model->save();

        $this->_writeLine('Finished importing ' . $xml["Stack"] . ' version file from ' . $this->_pathToFile);
    }

    /**
     * Method that executes import for package types
     * @param $path
     * @param $type
     */
    protected function _importPackageTypes($path, $type)
    {
        /** @var Vznl_Package_Model_Import_Package $simpleImport */
        $simpleImport = Mage::getModel("vznl_package/" . $type);

        $simpleImport->setStack($this->stack);
        $this->_simpleImportObject = $simpleImport;
        $simpleImport->setDebug(false);
        $this->handleImportLogFile($path, $simpleImport->getFileLogName());

        $this->_writeLine('Importing package types ' . $path);
        $file = fopen($path, 'r');
        $lineCnt = 0;
        $header = [];

        $this->logHeader($simpleImport);
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->importPackage($data);
            } else {
                $line = !empty($header) ? $header : $line;
                //check if match our template
                $check = $this->_importHelper->checkHeader($line, 'importPackage');
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if (!$check) {
                    break;
                }
                //this will map the file header
                $simpleImport->setHeader($header);
            }
        }
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing package types/subtypes file ' . $path);
    }

    /**
     * Import package creation types
     *
     * @param $path
     */
    public function _importPackageCreationTypes($path)
    {
        /** @var Vznl_Package_Model_Import_PackageCreationTypes $simpleImport */
        $simpleImport = Mage::getModel("vznl_package/import_packageCreationTypes");

        $simpleImport->setStack($this->stack);
        $this->_simpleImportObject = $simpleImport;
        $simpleImport->setDebug(false);
        $this->handleImportLogFile($path, $simpleImport->getFileLogName());

        $this->_writeLine('Importing package creation types ' . $path);

        $file = fopen($path, 'r');
        $lineCnt = 0;

        $this->logHeader($simpleImport);

        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);

            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->importCreationType($data);
            } else {
                //check if match our template
                $check = $this->_importHelper->checkHeader($line, 'packageCreationTypes');
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if (!$check) {
                    break;
                }
                //this will map the file header
                $simpleImport->setHeader($header);
            }
        }
        $this->logFooter($simpleImport);
        $this->_writeLine("Finished importing package creation types file " . $path);
    }

    /**
     * @param $path
     * @param $type
     */
    public function _importProductFile($path, $type)
    {        
        /** @var Vznl_Mobile_Model_Import_MobileProduct $simpleImport */
        $simpleImport = Mage::getModel("vznl_{$type}/import_{$type}Product");
        $simpleImport->setStack($this->stack);
        $this->_simpleImportObject = $simpleImport;
        $simpleImport->setDebug(false);
        // get the import file name in order to settle the log file name
        $productFileNameArray = explode('/', $path);
        $productFileName = array_pop($productFileNameArray);
        $productFileName = substr($productFileName, 0, strpos($productFileName, '.'));
        $this->handleImportLogFile($path, $simpleImport->getFileLogName($productFileName));
        unset($productFileNameArray, $productFileName);

        $this->_writeLine('Importing product file ' . $path);
        $file = fopen($path, 'r');
        $lineCnt = 0;
        $maxColumns = 0;
        $header = [];
        //log header
        $this->logHeader($simpleImport);

        // disable replication on import
        Mage::register('bypass_replication_on_import', true, true);
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            foreach ($line as $col) {
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    if ($colCnt >= $maxColumns) {
                        $skippedRowMsg = sprintf('[ERROR] Skipping line %s because of invalid format (check if text is enclosed in quotation marks)', $lineCnt);
                        $this->_writeLine($skippedRowMsg);
                        $simpleImport->_logError($skippedRowMsg);
                        $simpleImport->_totalFileRows++;
                        $simpleImport->_skippedFileRows++;
                        continue(2);
                    }
                    $data[$header[$colCnt]] = $col;
                }
                $colCnt++;
            }
            
            if ($lineCnt == 1) {
                //check if match our template; we send 'product_template' as type because the mandatory columns are on the all product types('mobile','fixed','cable')
                $check = $this->_importHelper->checkHeader($line, 'product_template');
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if (!$check) {
                    break;
                }
                $maxColumns = count($header);

            }

            if ($lineCnt != 1) {
                $simpleImport->importProduct($data);
            }
        }
        //log header
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing product file ' . $path);

        /** @var Dyna_Cache_Model_Cache $cacheModel */
        $cacheModel = Mage::getModel('dyna_cache/cache');
        $cacheModel->clean(array($cacheModel::PRODUCT_TAG));
    }


    /**
     * @param $path
     */
    public function _importArtifact($path)
    {
        /** @var Dyna_Cable_Model_Import_Artifact $simpleImport */
        $simpleImport = Mage::getModel("dyna_cable/import_artifact");
        $this->_simpleImportObject = $simpleImport;
        $this->handleImportLogFile($simpleImport);
        $simpleImport->setPath($path);
        $this->_writeLine('Importing Cable Artifact PHAR file...');
        //log header
        $this->logHeader($simpleImport);
        $simpleImport->import();

        //save import to db
        $this->logImportedFile($simpleImport);
        $this->_writeLine('Finished importing Cable Artifact PHAR file...');
    }

    /**
     * @param $path
     * @param $type
     */
    public function _importDealerCampaignFile($path, $type)
    {
        /** @var Dyna_AgentDE_Model_DealerImportCampaign $simpleImport */
        $simpleImport = Mage::getModel("agentde/" . $type);
        $this->_simpleImportObject = $simpleImport;
        $simpleImport->setDebug(false);
        $this->handleImportLogFile($simpleImport);

        $this->_writeLine('Importing dealer assigned to campaigns file...');
        $file = fopen($path, 'r');
        $lineCnt = 0;
        //log header
        $this->logHeader($simpleImport);

        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            $simpleImport->setCurrentLine($lineCnt);

            if ($lineCnt != 1) {
                $simpleImport->importDealer($data);
            } else {
                $check = $this->_importHelper->checkHeader($line, 'dealerCampaign');
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if ($check) {
                    $simpleImport->mapHeaderToFields($header);
                    continue;
                }
                break;

            }
        }

        //log header
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing dealer assigned to campaigns file...');
    }

    /**
     * @param $path
     */
    public function _importMixMatchFile($path)
    {
        /** Session not used, just initialised */
        //$session = Mage::getSingleton('admin/session');
        //@todo was this used in backend import of mixmatches?
        $this->_writeLine('Importing mixmatch file ' . $path);

        /**
         * @var Vznl_MixMatch_Model_Importer $importer
         */
        $importer = Mage::getModel('vznl_mixmatch/importer');
        $importer->setStack($this->stack);
        $this->_simpleImportObject = $importer;
        $this->handleImportLogFile($path, $importer->getFileLogName());
        $importer->executionId = $this->_executionId;
        $this->logHeader($importer);
        $importer->importFile($path, true);
        $this->logFooter($importer);
        $this->_writeLine('Finished importing mixmatch file ' . $path);
    }

    /**
     * @param $path
     */
    public function _importMultiMapperFile($path)
    {
        /**
         * @var Vznl_MultiMapper_Model_Importer $importer
         */
        $importer = Mage::getModel('vznl_multimapper/importer');
        $importer->setStack($this->stack);
        $this->_simpleImportObject = $importer;
        $this->handleImportLogFile($path, $importer->getFileLogName());
        $this->_writeLine('Importing MultiMapper ' . $path);
        //log header
        $this->logHeader($importer);
        //import data
        $importer->import($path);

        //log footer
        $this->logFooter($importer);
        $this->_writeLine('Finished importing MultiMapper ' . $path);
    }

    /**
     * @param $path
     */
    public function _importCategoriesFile($path)
    {
        /** @var Dyna_Mobile_Model_Import_MobileCategory $simpleImport */
        $simpleImport = Mage::getModel("dyna_mobile/import_mobileCategory");
        $simpleImport->setStack($this->stack);
        $this->_simpleImportObject = $simpleImport;
        $this->handleImportLogFile($simpleImport);
        $simpleImport->setDebug(false);

        $this->_writeLine('Importing categories file ' . $path);
        $file = fopen($path, 'r');
        $lineCnt = 0;

        //log header
        $this->logHeader($simpleImport);
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatkey($col));
                } else {
                    $data[$header[$colCnt]] = $col;
                }
            }
            if ($lineCnt != 1) {
                $simpleImport->importCategory($data);
            } else {
                $check = $this->_importHelper->checkHeader($line, 'categories');
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if (!$check) {
                    break;
                }
            }
        }

        //log footer
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing categories file ' . $path);
    }

    /**
     * @param $path
     */
    public function _importProductMatchRules($path)
    {
        /** @var Vznl_ProductMatchRule_Model_Importer $simpleImport */
        $simpleImport = Mage::getSingleton('vznl_productmatchrule/importer');
        $simpleImport->setStack($this->stack);
        $this->_simpleImportObject = $simpleImport;
        $this->handleImportLogFile($path, $simpleImport->getFileLogName());
        $this->_writeLine('Importing product match rules file ' . $path);
        //log header
        $this->logHeader($simpleImport);
        $simpleImport->import($path);
        //log footer
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing product match rules file ' . $path);

        //also invalidate indexers
        foreach (array('product_match_whitelist', 'product_match_defaulted') as $indexName) {
            $indexer = Mage::getSingleton('index/indexer')->getProcessByCode($indexName);
            if ($indexer) {
                $indexer->setStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX)->save();
            }
        }
    }

    /**
     * @param stdClass $data
     * @param string $type
     */
    private function _importCampaignData($path, $type, $importSchemaPath)
    {
        /** @var Dyna_Bundles_Model_Import_Campaign $campaignImportModel */
        $campaignImportModel = Mage::getModel(sprintf('vznl_bundles/import_%s', $type));
        $campaignImportModel->setStack($this->stack);
        $this->_simpleImportObject = $campaignImportModel;
        $this->handleImportLogFile($path, $campaignImportModel->getFileLogName());

        switch ($type) {
            case self::XML_CAMPAIGN:
                $data = $this->getXmlData($path, self::XML_CAMPAIGN, true, $importSchemaPath, $campaignImportModel);
                break;
            case self::XML_CAMPAIGN_OFFER:
                $data = $this->getXmlDataWithAttributes($path, self::XML_CAMPAIGN_OFFER, true, $importSchemaPath, $campaignImportModel);
                break;
        }
        if ($data === false) {
            return;
        }

        //log header
        $this->logHeader($campaignImportModel);
        switch ($type) {
            case self::XML_CAMPAIGN:
            case self::XML_CAMPAIGN_OFFER:
                $rows = $data['row'];
                break;
            default:
                $rows = [];
                break;
        }

        $rows = (is_array($rows[0])) ? $rows : [$rows];

        foreach ($rows as $row) {
            $campaignImportModel->import($row);
        }
        //log footer
        $this->logFooter($campaignImportModel);
    }

    /**
     * @param stdClass $data
     * @param string $type
     */
    private function _importSalesIdDealerCode($path, $importSchemaPath)
    {
        /** @var Dyna_Bundles_Model_Import_CampaignDealercode $importModel */
        $importModel = Mage::getModel(sprintf('dyna_bundles/import_campaignDealercode'));
        $this->_simpleImportObject = $importModel;
        $data = $this->getXmlData($path, self::XML_SALESID_DEALERCODE, true, $importSchemaPath, $importModel);
        if ($data === false) {
            return;
        }
        $this->handleImportLogFile($importModel);
        //log header
        $this->logHeader($importModel);
        $rows = (isset($data['Mapping'][0])) ? $data['Mapping'] : [$data['Mapping']];
        foreach ($rows as $row) {
            $importModel->import($row);
        }
        //log footer
        $this->logFooter($importModel);
    }

    /**
     * @param stdClass $data
     */
    private function _importBundlesData($path, $importSchemaPath)
    {
        /** @var Vznl_Bundles_Model_Import_Bundle $bundlesImportModel */
        $bundlesImportModel = Mage::getModel('vznl_bundles/import_bundle');
        $bundlesImportModel->setStack($this->stack);
        $this->_simpleImportObject = $bundlesImportModel;
        $this->handleImportLogFile($path, $bundlesImportModel->getFileLogName());

        $data = $this->getXmlData($path, self::XML_BUNDLES, true, $importSchemaPath, $bundlesImportModel);
        $data = $this->stripEmptyArrayValues($this->stripAttributes($data));
        if ($data === false) {
            return;
        }
        $this->logHeader($bundlesImportModel);

        // clear actions and conditions from bundles - new one will be imported each time
        $bundlesImportModel
            ->clearBundleActionsTable()
            ->clearBundleConditionsTable()
            ->clearBundleHintsTable()
            ->clearBundleProcessContextTable();
        if (array_key_exists('bundle_rule_name', $data['bundle_rule'])) {
            $bundlesImportModel->import($data['bundle_rule']);
        } else {
            foreach ($data['bundle_rule'] as $rule) {
                $bundlesImportModel->import($rule);
            }
        }
        //log footer
        $this->logFooter($bundlesImportModel);
    }

    /**
     * php import.php --type vodafoneShip2Stores --file Ship2Store_List_VFDE.csv
     */
    private function _importVodafoneShip2Stores()
    {
        /** @var Dyna_AgentDE_Model_Import_VodafoneShip2Stores $simpleImport */
        $simpleImport = Mage::getModel("agentde/import_vodafoneShip2Stores");
        $simpleImport->setDebug(false);
        $this->_simpleImportObject = $simpleImport;
        $this->handleImportLogFile($simpleImport);

        $this->_writeLine('Importing vodafone ship to stores...');
        $this->logHeader($simpleImport);
        $file = fopen($this->_pathToFile, 'r');

        $lineCnt = 0;
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->process($data);
            } else {
                if (!$simpleImport->setHeader($header)) {
                    break;
                }
                $simpleImport->removePreviousData(Mage::getModel('agentde/vodafoneShip2Stores'));
            }
        }
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing vodafone ship to stores...');
    }

    /**
     * php import.php --type vodafoneShip2Stores --file Ship2Store_List_VFDE.csv
     */
    private function _importServiceProviders()
    {
        /** @var Dyna_Operator_Model_Import_ServiceProvider $simpleImport */
        $simpleImport = Mage::getModel("dyna_operator/import_serviceProvider");
        $this->_simpleImportObject = $simpleImport;
        $this->handleImportLogFile($simpleImport);
        /** @var Dyna_Import_Helper_Data $importHelper */
        $importHelper = Mage::helper("dyna_import/data");
        $simpleImport->setDebug(false);

        $this->_writeLine('Importing service providers...');
        $this->logHeader($simpleImport);
        $file = fopen($this->_pathToFile, 'r');
        $header = [];
        $lineCnt = 0;
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($col);
                } else {
                    $data[$header[$colCnt]] = $col;
                }
            }


            if ($lineCnt != 1) {
                $simpleImport->process($data);
            } else {
                //check if match our template
                $checkHeader = $importHelper->checkHeader(array_map('strtolower', $line), 'serviceProviders');

                //log messages
                $messages = $importHelper->getMessages();
                $attachments = [];
                foreach ($messages as $key => $msg) {
                    //write file log
                    $simpleImport->_log($msg);
                    if ($key !== 'success' && substr($this->_pathToFile, 0, 9) != '/vagrant/') {
                        $attachments[] = ['key' => $key, 'text' => $msg];
                    }
                }
                if (count($attachments)) {
                    $msg = 'While processing file ' . $this->_pathToFile . ' we encountered the following:';
                    $importHelper->postToSlack($msg, $attachments);
                }
                //check if the headers are ok
                if (!$checkHeader) {
                    break;
                }
                $simpleImport->removePreviousData(Mage::getModel('dyna_operator/serviceProvider'));
            }
        }

        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing service providers...');
    }

    /**
     * php import.php --type agentCommisions --file TOBEDETERMINED.csv
     */
    private function _importAgentCommisions()
    {
        // Delete all agent commisions
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_write');
        $query = "DELETE from `agent_commision`";
        $connection->exec($query);

        /** @var Dyna_AgentDE_Helper_Import $importHelper */
        $importHelper = Mage::helper('agentde/import');

        $this->_writeLine('Importing agent commisions...');
        $importHelper->importAgentCommisions($this->_pathToFile);
        $this->_writeLine('Finished agent commisions import');
    }

    /**
     * php import.php --type checkoutProducts --file Cable_Checkout_Products.csv
     */
    private function _importCheckoutProducts()
    {
        /** @var Dyna_Catalog_Model_Product_Import_CheckoutProduct $simpleImport */
        $simpleImport = Mage::getModel("dyna_catalog/product_import_checkoutProduct");

        $this->_simpleImportObject = $simpleImport;
        $this->handleImportLogFile($simpleImport);
        /** @var Dyna_Import_Helper_Data $importHelper */
        $importHelper = Mage::helper("dyna_import/data");
        $simpleImport->setDebug(false);

        $this->_writeLine('Importing checkout products...');
        $file = fopen($this->_pathToFile, 'r');
        $header = [];
        $lineCnt = 0;

        $this->logHeader($simpleImport);
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($col);
                } else {
                    $data[$header[$colCnt]] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->importProduct($data);
            } else {
                //check if match our template
                $checkHeader = $importHelper->checkHeader($line, 'checkoutProducts');
                //log messages
                $messages = $importHelper->getMessages();
                $attachments = [];
                foreach ($messages as $key => $msg) {
                    //write file log
                    $simpleImport->_log($msg);
                    if ($key !== 'success' && substr($this->_pathToFile, 0, 9) != '/vagrant/') {
                        $attachments[] = ['key' => $key, 'text' => $msg];
                    }
                }
                if (count($attachments)) {
                    $msg = 'While processing file ' . $this->_pathToFile . ' we encountered the following:';
                    $importHelper->postToSlack($msg, $attachments);
                }
                //check if the headers are ok
                if (!$checkHeader) {
                    break;
                }
            }
        }

        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing checkout products...');

    }

    /**
     * php import.php --type provisSalesId --file toBeDetermined.csv
     */
    private function _importProvisSalesId()
    {
        /** @var Dyna_AgentDE_Model_Import_ProvisSalesIds $simpleImport */
        $simpleImport = Mage::getModel("agentde/import_provisSalesIds");
        $this->_simpleImportObject = $simpleImport;
        $this->handleImportLogFile($simpleImport);
        $simpleImport->setDebug(false);

        if (!is_readable($this->_pathToFile)) {
            $simpleImport->_errorCode = $simpleImport::TEHNICAL_ERROR;
            $simpleImport->_log(sprintf('The file is not accessible'));
            $simpleImport->_log(sprintf('ERROR CODE `%d`', $simpleImport->_errorCode));
            exit($simpleImport->_errorCode);
        }

        $contents = file_get_contents($this->_pathToFile);
        if (!mb_detect_encoding($contents, 'ASCII', true)) {
            $simpleImport->_errorCode = $simpleImport::TEHNICAL_ERROR;
            $simpleImport->_log(sprintf('Invalid file encoding'));
            $simpleImport->_log(sprintf('ERROR CODE `%d`', $simpleImport->_errorCode));
            exit($simpleImport->_errorCode);
        }

        if (strtolower(pathinfo($this->_pathToFile, PATHINFO_EXTENSION)) != 'csv') {
            $simpleImport->_errorCode = $simpleImport::TEHNICAL_ERROR;
            $simpleImport->_log(sprintf('The file type is incorrect'));
            $simpleImport->_log(sprintf('ERROR CODE `%d`', $simpleImport->_errorCode));
            exit($simpleImport->_errorCode);
        }

        if (!is_writable(Mage::getBaseDir('var') . DS . 'log')) {
            $simpleImport->_errorCode = $simpleImport::TEHNICAL_ERROR;
            $simpleImport->_log(sprintf('Canâ€™t create a logfile'));
            $simpleImport->_log(sprintf('ERROR CODE `%d`', $simpleImport->_errorCode));
            fwrite(STDERR, sprintf('ERROR CODE `%d`', $simpleImport->_errorCode));
            exit($simpleImport->_errorCode);
        }


        $this->_writeLine('Importing provis sales ids...');
        $this->logHeader($simpleImport);
        $file = fopen($this->_pathToFile, 'r');

        $simpleImport->beginTransaction();
        $simpleImport->removePreviousData(Mage::getModel('agentde/provisSales'));

        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $data[++$colCnt] = $col;
            }

            $simpleImport->process($data);
        }

        switch($simpleImport->_errorCode) {
            case $simpleImport::TEHNICAL_ERROR:
                $simpleImport->rollBack();
                $simpleImport->_skippedFileRows = $simpleImport->_totalFileRows;
                break;
            default:
                $simpleImport->commit();
                break;
        }

        $this->logFooter($simpleImport, $simpleImport->_errorCode);
        $this->_writeLine('Finished importing provis sales ids...');
    }

    private function _importProductFamily()
    {
        /** @var Vznl_Catalog_Model_Product_Import_ProductFamily $simpleImport */
        $simpleImport = Mage::getModel("vznl_catalog/product_import_productFamily");
        $simpleImport->setDebug(false);
        $this->_simpleImportObject = $simpleImport;
        $simpleImport->setStack($this->stack);
        $this->handleImportLogFile($this->_pathToFile, $simpleImport->getLogFileName());

        $this->_writeLine('Importing product family file ' . $this->_pathToFile);
        $file = fopen($this->_pathToFile, 'r');
        $lineCnt = 0;

        $this->logHeader($simpleImport);
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->process($data);
            } else {
                //check if match our template
                $check = $this->_importHelper->checkHeader($line, 'productFamily');
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if (!$check) {
                    break;
                }
                //this will map the file header
                if (!$simpleImport->setHeader($header)) {
                    break;
                }
            }
        }
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing product family file ' . $this->_pathToFile);
    }

    private function _importProductVersion()
    {
        /** @var Vznl_Catalog_Model_Product_Import_ProductVersion $simpleImport */
        $simpleImport = Mage::getModel("vznl_catalog/product_import_productVersion");

        $simpleImport->setDebug(false);
        $this->_simpleImportObject = $simpleImport;
        $simpleImport->setStack($this->stack);
        $this->handleImportLogFile($this->_pathToFile, $simpleImport->getFileLogName());

        $this->_writeLine('Importing product version file ' . $this->_pathToFile);
        $file = fopen($this->_pathToFile, 'r');
        $lineCnt = 0;

        $this->logHeader($simpleImport);
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->process($data);
            } else {
                //check if match our template
                $check = $this->_importHelper->checkHeader($line, 'productVersion');
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if (!$check) {
                    break;
                }
                //this will map the file header
                if (!$simpleImport->setHeader($header)) {
                    break;
                }
            }
        }
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing product version file ' . $this->_pathToFile);
    }

    /**
     * Used to import categories and associative products
     * @param $type
     */
    private function _importCategoriesTree($type)
    {
        /** @var Vznl_Catalog_Model_Import_CategoriesTree $simpleImport */
        $simpleImport = Mage::getModel("vznl_catalog/import_categoriesTree");
        $simpleImport->setStack($this->stack);
        $simpleImport->setDebug(false);
        $this->_simpleImportObject = $simpleImport;
        $this->handleImportLogFile($this->_pathToFile, $simpleImport->getFileLogName($type));

        $this->_writeLine('Importing ' . ($type == "tree" ? 'categories' : 'products for categories') . $this->_pathToFile);
        $file = fopen($this->_pathToFile, 'r');
        $lineCnt = 0;

        $this->logHeader($simpleImport);
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                if ($type == 'tree') {
                    $simpleImport->importCategoryTree($data);
                } else {
                    $simpleImport->importCategoryProducts($data);
                }
            } else {
                //check if match our template
                $check = $this->_importHelper->checkHeader($line, "categories_" . $type);
                //log messages
                $this->_writeHeadersLogs($simpleImport);
                //check if the headers are ok
                if (!$check) {
                    break;
                }
                //this will map the file header
                if (!$simpleImport->setHeader($header)) {
                    break;
                }
            }
        }
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing ' . ($type == "tree" ? 'categories' : 'products for categories') . ' file ' . $this->_pathToFile);
    }

    /**
     * Rremove BOM from string
     * @param $s
     * @return string
     */
    protected function removeBomUtf8($s)
    {
        if (substr($s, 0, 3) == chr(hexdec('EF')) . chr(hexdec('BB')) . chr(hexdec('BF'))) {
            return substr($s, 3);
        } else {
            return $s;
        }
    }

    /**
     * Get data as object from XML
     *
     * @param string $path
     * @param string $type
     * @param bool $validateXml
     *
     * @param string $schemaPath
     *
     * @return bool|mixed
     */
    protected function getXmlData($path, $type, $validateXml = false, $schemaPath = '', $import)
    {
        if ($validateXml) {
            $schemaPath .= DIRECTORY_SEPARATOR . $this->validationSchemas[$type];
            if (!file_exists($schemaPath)) {
                $this->_writeLine('Validation schema file not found, should be in ' . $schemaPath);
                return false;
            }

            libxml_use_internal_errors(true);
            $document = new DOMDocument();
            $document->load($path);
            $xmlString = $document->saveXML();
            if (!$document->schemaValidate($schemaPath)) {
                $this->_hasError = true;
                $import->_skippedFileRows = 1;
                $xmlErrors = libxml_get_errors();

                $attachments = [];
                $errors = 0;
                foreach ($xmlErrors as $xmlError) {
                    if ($xmlError->level >= LIBXML_ERR_ERROR) {
                        $errors++;
                        $msg = $this->libxmlDisplayError($xmlError);
                        $import->_logError($msg);
                        $attachments[] = ['key' => $xmlError->level, 'text' => $msg];
                    }
                }
                if ($errors) {
                    $msg = 'While processing file ' . $this->_pathToFile . ' we encountered the following:';
                    $this->_importHelper->postToSlack($msg, $attachments);
                    $this->_writeLine('The provided XML ' . $path . ', is not valid according to schema');
                    libxml_clear_errors();
                    return false;
                }
            }

            // remove nanemspaces
            $result = $this->getXmlError($xmlString);
        }

        return $result ?: json_decode(json_encode(simplexml_load_file($path)), true);
    }


    /**
     * @param $str
     * @return SimpleXMLElement
     * @throws Exception
     */
    public function getXmlError($str)
    {
        try {
            $xml = new SimpleXMLElement($str);
            foreach ($xml->getNamespaces(true) as $ns => $uri) {
                $str = str_replace(sprintf('%s:', $ns), '', $str);
            }
            $xml = new SimpleXMLElement($str);
            return json_decode(json_encode($xml), true);
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
    }


    protected function stripEmptyArrayValues($array)
    {
        $data = [];
        foreach ($array as $key => $value) {
            if (is_array($value) && !empty($value)) {
                $data[$key] = $this->stripEmptyArrayValues($value);
            } else {
                $data[$key] = is_array($value) ? null : trim($value);
            }
        }
        return $data;
    }


    /**
     * @param $array
     * @return array|void
     */
    protected function stripAttributes($array)
    {
        $data = [];
        if (empty($array)) {
            return array();
        }

        foreach ($array as $key => $value) {
            if (is_array($value) && $key !== '@attributes') {
                $data[$key] = $this->stripAttributes($value);
            } else {
                if ($key !== '@attributes') {
                    $data[$key] = $value;
                }
            }
        }
        return $data;
    }


    /**
     * get XML data with the attributes it requires path, type validationflag, xsd path and import modal
     * @param $import
     */
    protected function getXmlDataWithAttributes($path, $type, $validateXml = false, $schemaPath = '', $import)
    {
        if ($validateXml) {
            $schemaPath .= DIRECTORY_SEPARATOR . $this->validationSchemas[$type];
            if (!file_exists($schemaPath)) {
                $this->_writeLine('Validation schema file not found, should be in ' . $schemaPath);
                return false;
            }

            libxml_use_internal_errors(true);
            $document = new DOMDocument();
            $document->load($path);
            if (!$document->schemaValidate($schemaPath)) {
                $this->_hasError = true;
                $import->_skippedFileRows = 1;
                $this->logImportedFile($import);
                $xmlErrors = libxml_get_errors();

                $attachments = [];
                foreach ($xmlErrors as $xmlError) {
                    $msg = $this->libxmlDisplayError($xmlError);
                    $import->_log($msg);
                    $attachments[] = ['key' => $xmlError->level, 'text' => $msg];
                }
                $msg = 'While processing file ' . $this->_pathToFile . ' we encountered the following:';
                $this->_importHelper->postToSlack($msg, $attachments);
                $this->_writeLine('The provided XML is not valid according to schema');
                libxml_clear_errors();
                return false;
            }
        }

        $result = array();
        $xmlObject = simplexml_load_file($path);
        $i = 0;
        foreach ($xmlObject->children() as $key => $value) {
            foreach ($value->children() as $key1 => $value1) {
                foreach ($value->$key1 as $key2 => $value2) {
                    $temparray = (array)$value2->attributes();
                    $xmlvalue = (string)$value2['0'];

                    $result['row'][$i][$key1]['value'] = $xmlvalue;
                    if (!empty($temparray)) {
                        foreach ($temparray as $key3 => $value3) {
                            foreach ($value3 as $key4 => $value4) {
                                $result['row'][$i][$key1]['attributes'][$key4] = $value4;
                            }
                        }
                    }
                }
            }
            $i++;
        }
        return $result;
    }
    /**
     * get Xml data with attributes ends here
     */

    /**
     * Logs the end of the imported file
     * @param $import
     */
    protected function logFooter($import, $errorCode = null)
    {
        $import->_log(sprintf('Import contains in total %d rows', $import->_totalFileRows));
        $import->_log(sprintf('Successfully imported %d rows', $import->_totalFileRows - $import->_skippedFileRows));
        $import->_log(sprintf('Skipped rows with error %d', $import->_skippedFileRows));
        $import->_log(sprintf('Ending import of file: %s', $this->_pathToFile));
        if (!is_null($errorCode)) {
            $import->_log(sprintf('ERROR CODE `%d`', $errorCode));
        }
        //save import to db
        $this->logImportedFile($import);
    }

    /**
     * Logs the header of the imported file
     * @param $import
     */
    protected function logHeader($import)
    {
        $import->_log(sprintf('Starting import of file: %s', $this->_pathToFile));
        //create the import summary entry so we have it even for missing files
        $this->logImportedFile($import);
        $this->exitIfFileNotFound();
    }

    /**
     * @param $import
     */
    protected function _writeHeadersLogs($import)
    {
        $messages = $this->_importHelper->getMessages();
        $attachments = [];
        foreach ($messages as $key => $msg) {
            //write file log
            $import->_log($msg);
            if ($key !== 'success' && substr($this->_pathToFile, 0, 9) != '/vagrant/') {
                $attachments[] = ['key' => $key, 'text' => $msg];
            }
        }
        if (count($attachments)) {
            $msg = 'While processing file ' . $this->_pathToFile . ' we encountered the following:';
            $this->_importHelper->postToSlack($msg, $attachments);
        }
    }

    protected function logImportedFile($import)
    {
        /** @var Dyna_Import_Model_Resource_Summary $importSummaryModel */
        $importSummaryModel = Mage::getModel("vznl_import/summary");

        $pathFile = explode('/', $this->_pathToFile);
        $fileName = end($pathFile);
        /** @var Dyna_Import_Model_Resource_Summary_Collection $collection */
        $collection = $importSummaryModel->getCollection();

        if (isset($this->_executionId)) {
            $collection = $collection->addFieldToFilter('execution_id', $this->_executionId);
        }
        if (!is_null($this->importId)) {
            $collection = $collection->addFieldToFilter('import_id', $this->importId)
                ->addFieldToFilter('filename', $fileName);
            $collection->getSelect()->order('import_id DESC');
            /** @var Dyna_Import_Model_Summary $model */
            $model = $collection->getFirstItem();
        } else {
            /** @var Dyna_Import_Model_Summary $model */
            $model = $importSummaryModel;
        }
        if ($model->getId()) {
            if ($this->_hasError) {
                $model->setHasErrors(1);
            } else {
                if ($import->_totalFileRows && $import->_skippedFileRows == $import->_totalFileRows) {
                    $model->setHasErrors(1);
                } else {
                    $model->setHasErrors(0);
                }
                $model->setHeadersValidated(($import->_totalFileRows == 0 && $import->_skippedFileRows == 0 ? 0 : 1))
                    ->setTotalRows($import->_totalFileRows)
                    ->setImportedRows($import->_totalFileRows - $import->_skippedFileRows)
                    ->setSkippedRows($import->_skippedFileRows);

                if (isset($this->_executionId)) {
                    $model->setExecutionId($this->_executionId);
                }
            }
        } else {
            $model->setFilename($fileName)
                ->setCategory($this->_importLogCategory)
                ->setCreatedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()))
                ->setHasErrors($this->_hasError)
                ->setHeadersValidated(0)
                ->setTotalRows(0)
                ->setImportedRows(0)
                ->setSkippedRows(0);

            if (isset($this->_executionId)) {
                $model->setExecutionId($this->_executionId);
            }
        }

        $model->save();
        if (is_null($this->importId)) {
            $this->importId = $model->getId();
        } else {
            $this->importId = null;
        }
    }

    /**
     * Return path for import dir (if not exist it will create it)
     * @return string
     */
    private function getImportDir()
    {
        if (!is_dir(Mage::getBaseDir('var') . DS . 'import')) {
            mkdir(Mage::getBaseDir('var') . DS . 'import', 0777, true);
        }

        return Mage::getBaseDir('var') . DS . 'import';
    }

    /**
     * Checks executionId argument and adds it to the import log file
     *
     * @param $importedFilePath
     * @param $singleImportLogName
     */
    protected function handleImportLogFile($importedFilePath = false, $singleImportLogName = false)
    {
        /** @var Vznl_Import_Helper_Data $importHelper */
        $importHelper = Mage::helper('vznl_import');
        if (isset($this->_executionId)) {
            $importFileParts = explode('.', $this->_file);
            $importFileParts = $importFileParts[0] . '_' . Dyna_Import_Helper_Data::LOG_FILE_EXECUTION_SUFFIX . '_' . $this->_executionId . self::LOG_EXTENSION;
            $importHelper->setImportLogFile($importFileParts);
        } else {
            $importFileParts = explode('.', $this->_file);
            $importFileParts = $importFileParts[0] . self::LOG_EXTENSION;
            $importHelper->setImportLogFile($importFileParts);
        }
    }

    /**
     * Checks executionId argument, sets property and returns property if valid
     */
    protected function getExecutionId()
    {
        $executionId = $this->getArg('executionId');

        if (!isset($this->_executionId) && $executionId && is_numeric($executionId)) {
            $this->_executionId = $executionId;
        }

        return $this->_executionId;
    }

    protected function libxmlDisplayError($xmlError)
    {
        switch ($xmlError->level) {
            case LIBXML_ERR_WARNING:
                $return = "[Warning] $xmlError->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return = "[Error] $xmlError->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return = "[Fatal] $xmlError->code: ";
                break;
            default:
                $return = '';
        }

        $return .= trim($xmlError->message);

        if ($xmlError->file) {
            $return .= " in $xmlError->file";
        }

        return $return . " on line $xmlError->line";
    }

    /**
     * php import.php --type phoneBookKeywords --file Stichwort_list.csv
     */
    private function _importPhoneBookKeywords()
    {
        /** @var Dyna_AgentDE_Model_Import_PhoneBookKeywords $simpleImport */
        $simpleImport = Mage::getModel("agentde/import_phoneBookKeywords");
        $simpleImport->setDebug(false);
        $this->_simpleImportObject = $simpleImport;
        $this->handleImportLogFile($simpleImport);

        $this->_writeLine('Importing phone book keywords...');
        $file = fopen($this->_pathToFile, 'r');
        $lineCnt = 0;

        $this->logHeader($simpleImport);
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->process($data);
            } else {
                if (!$simpleImport->setHeader($header)) {
                    break;
                }
                $simpleImport->removePreviousData(Mage::getModel('agentde/phoneBookKeywords'));
            }
        }
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing phone book keywords...');
    }

    /**
     * php import.php --type phoneBookIndustries --file telefonbuch-branchenliste.csv
     */
    private function _importPhoneBookIndustries()
    {
        /** @var Dyna_AgentDE_Model_Import_PhoneBookIndustries $simpleImport */
        $simpleImport = Mage::getModel("agentde/import_phoneBookIndustries");
        $simpleImport->setDebug(false);
        $this->_simpleImportObject = $simpleImport;
        $this->handleImportLogFile($simpleImport);

        $this->_writeLine('Importing phone book industries...');
        $file = fopen($this->_pathToFile, 'r');
        $lineCnt = 0;

        $this->logHeader($simpleImport);
        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = strtolower($simpleImport->formatKey($col));
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->process($data);
            } else {
                if (!$simpleImport->setHeader($header)) {
                    break;
                }
                $simpleImport->removePreviousData(Mage::getModel('agentde/phoneBookIndustries'));
            }
        }
        $this->logFooter($simpleImport);
        $this->_writeLine('Finished importing phone book industries...');
    }

    /**
     * @param $path
     * @param $type
     * @param $importSchemaPath
     */
    private function _importMnpFreeDaysData($path, $type, $importSchemaPath)
    {
        /** @var Dyna_Catalog_Model_Import_MnpFreeDays $mnpFreeDaysImportModel */
        $mnpFreeDaysImportModel = Mage::getModel('dyna_catalog/import_mnpFreeDays');
        $this->_simpleImportObject = $mnpFreeDaysImportModel;

        $data = $this->getXmlData($path, self::XML_MNP_FREE_DAYS, true, $importSchemaPath, $mnpFreeDaysImportModel);

        if ($data === false) {
            return;
        }

        $this->handleImportLogFile($mnpFreeDaysImportModel);
        //log header
        $this->logHeader($mnpFreeDaysImportModel);

        $rows = $data['MobilePorting'];
        foreach ($rows as $row) {
            $mnpFreeDaysImportModel->import($row);
        }
        //log footer
        $this->logFooter($mnpFreeDaysImportModel);
    }

    /**
     * @param $path
     * @param $type
     * @param $importSchemaPath
     */
    private function _importMsProvidersData($path, $type, $importSchemaPath)
    {
        /** @var Dyna_Catalog_Model_Import_MsproviderList $msProviderslistImportModel */
        $msProviderslistImportModel = Mage::getModel('dyna_catalog/import_mobileProvidersList');
        $this->_simpleImportObject = $msProviderslistImportModel;

        $data = $this->getXmlData($path, self::XML_MOBILEPROVIDERS, true, $importSchemaPath, $msProviderslistImportModel);

        if ($data === false) {
            return;
        }

        $this->handleImportLogFile($msProviderslistImportModel);
        //log header
        $this->logHeader($msProviderslistImportModel);

        $rows = $data['ProviderList'];
        foreach ($rows as $row) {
            $msProviderslistImportModel->import($row);
        }
        //log footer
        $this->logFooter($msProviderslistImportModel);
    }

    protected function exitIfFileNotFound() {
        $pathFile = explode('/',$this->_pathToFile);
        $file = end($pathFile);
        $path = $this->_pathToFile;
        if (!$file || !file_exists($path)) {
            $this->_writeLine("Import file '$file' not found, should be in '$path'\n");
            fwrite(STDERR, "Import file '$file' not found, should be in '$path'\n");
            exit(1);
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $this->types = Mage::helper('dyna_import')->getImportCategories();
        sort($this->types);
        $availableTypes = PHP_EOL. "\t\t\t\t\t". implode(PHP_EOL."\t\t\t\t\t", $this->types) .PHP_EOL."\t\t\t\t";
        return <<<USAGE
Usage:  php file -- [options]

  help                          This help
  file                          Filename to import
  type                          Import type [$availableTypes]
  logCategory                   Log category type used to group the imports when an summary export is made
  website                       Websites to import
  status                        Check whether an import is running

USAGE;
    }
}

// Example
//php import.php --type mobileProduct --file mobileHw.csv

$import = new Dyna_Import_Cli();
$import->run();