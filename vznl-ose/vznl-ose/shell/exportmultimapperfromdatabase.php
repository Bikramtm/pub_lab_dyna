<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'catalogtables.php';

class Dyna_MultiMapperDatabaseExport_Cli extends Dyna_CatalogTables_Cli
{
    private $hostname = null;
    private $username = null;
    private $password = null;
    private $databaseName = null;
    private $temporaryDirectory = "/tmp"; #Temporary linux folder
    private $destinationDirectory = "~/"; #Home directory of current user

    public function run()
    {
        $tempFolder = $this->getArg('temp');
        $destinationFolder = $this->getArg('destination');

        if ($tempFolder) {
            $this->temporaryDirectory = $tempFolder;
        }

        if ($destinationFolder) {
            $this->destinationDirectory = $destinationFolder;
        }

        if (empty(parent::MULTIMAPPERTABLES))
        {
            $this->_writeLine('The multimapper constant is empty, cannot continue the script');
            exit;
        }

        $config  = Mage::getConfig()->getResourceConnectionConfig("default_setup");

        parent::_writeLine('Connecting to database');

        $databaseInfo = array(
            "host"   => $config->host,
            "user"   => $config->username,
            "pass"   => $config->password,
            "dbname" => $config->dbname
        );

        $hostname = $databaseInfo["host"];
        $username = $databaseInfo["user"];
        $password = $databaseInfo["pass"];
        $databaseName = $databaseInfo["dbname"];

        if ($databaseName == "magento")
        {
            parent::_writeLine('Database could not be found, cannot continue script');
            exit;
        }

        parent::_writeLine('Starting export of multimapper from ' . $databaseName);
        $this->export_tables($hostname, $username, $password, $databaseName, parent::MULTIMAPPERTABLES);
    }

    function export_tables($host, $user, $pass, $name, $tables=false, $backup_name=false)
    {
        $backup_file = ($backup_name ? $backup_name : "multimapper") . '_' . date("Y-m-d-H-i-s") . '_' . $host . '_' . $name . '.gz';
        $tableNames = "";

        if ($tables)
        {
            foreach ($tables as $exportTable)
            {
                if (parent::tableExist($exportTable))
                {
                    $tableNames .= $exportTable . " ";
                }
            }
        }

        if ($onlyExportTables = strlen($tableNames) > 0)
        {
            $command = "mysqldump -h$host -u$user --default-character-set=utf8 -p$pass $name $tableNames| gzip > $this->temporaryDirectory" . DIRECTORY_SEPARATOR . "$backup_file";
        }
        else
        {
            $command = "mysqldump -h$host -u$user --default-character-set=utf8  -p$pass $name | gzip > $this->temporaryDirectory" . DIRECTORY_SEPARATOR . "$backup_file";
        }

        system($command, $returnvalue);

        if ($returnvalue == 0) {
            parent::_writeLine(($onlyExportTables ? "Only selected" : "All") . " tables exported ");
        }
        else
        {
            parent::_writeLine("Could not create backup -> command failed");
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php file      
  help				This help
  destination       Export multimapper to specified destination         (Default: /~  [USER HOME DIRECTORY])
  temp              Use specified temporary directory for temp files    (Default: /tmp)
  
  Example: php exportmultimapperfromdatabase.php destination /~/SIT temp /~/tmp     [Creates a multimapper backup to specified destinaion]
                                                                                     and uses temp folder]
USAGE;
    }
}

// Example
//php exportmultimapperfromdatabase.php

$import = new Dyna_MultiMapperDatabaseExport_Cli();
$import->run();
