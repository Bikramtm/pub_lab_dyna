<?php
require_once 'vznl_abstract.php';

class Vznl_Clean_Expired_Quotes_Cli extends Vznl_Shell_Abstract
{
    const VF_DUMP_QUOTES_LOG = 'quotes_dump.log';

    public function __construct()
    {
        parent::__construct();
    }

    private function _writeLine($msg) {
        echo $msg . PHP_EOL;
    }

    /**
     * Run script
     *
     */
    public function run() {
        $checkoutHelper = Mage::helper('vznl_checkout');
        $carts = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter(
                ['cart_status', 'is_offer'],
                [['eq' => Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED], ['eq' => 1]]
            )->addFieldToFilter(
                'created_at',
                ['lt' => Mage::getModel('core/date')->date('Y-m-d H:i:s', strtotime('-3 month'))]
            );
        foreach ($carts as $quote) {
            if ($quote->isOffer()) {
                // Quote is offer
                $isExpired = $checkoutHelper->isOfferReadonly($quote->getUpdatedAt()??$quote->getCreatedAt());
            } else {
                // Quote is saved shopping cart
                $isExpired = $checkoutHelper->isCartReadonly($quote->getUpdatedAt()??$quote->getCreatedAt());
            }
            if ($isExpired) {
                // Remove quote
                $this->_writeLine("Deleting quote ID: " . $quote->getId() . " (was created at: " . Mage::getModel('core/date')->date('Y-m-d H:i:s', $quote->getCreatedAt()) . ")" );
                $quote->delete();
            }
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp() {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  help                            This help
  file                            File to read

USAGE;
    }
}

$import = new Vznl_Clean_Expired_Quotes_Cli();
$import->run();
