<?php

/**
 * Script for extracting process results from the update_customers.php customer_dump*.log logs and customer_dump*.log.gz logs
 */

class Vznl_Analyze_Customer_Dump_Logs
{
    protected $home;
    protected $scriptLogFolder;
    protected $GZWorkFolder;
    protected $sessionLog;
    protected $logHandle;
    protected $cleanUpFile = false;
    protected $date;
    protected $previousLineData = '';
    protected $uniqueErrors = [
        "Partial" => [
            "Errors" => []
        ],
        "Failed" => [
            "Errors" => []
        ]
    ];

    protected function initiate()
    {
        print_r(PHP_EOL . "######################################" . PHP_EOL .
            "##### Analyze Customer Dump Logs #####" . PHP_EOL .
            "######################################" . PHP_EOL . PHP_EOL);
    }

    protected function evalSlash($path)
    {
        $length = strlen("/");

        if(substr($path, -$length) === "/"){
            return $path;
        }

        return $path . "/";
    }

    protected function logToFile($includeCLI, $message)
    {
        $message .= PHP_EOL;

        fwrite($this->logHandle, $message);
        if($includeCLI){
            print_r($message);
        }
    }

    public function run()
    {
        $this->date = date('Ymd_His');

        $this->initiate();
        $this->checkLogRequirements();
        $this->processCustomerDumpLog($this->checkForCustomerDumpLogs());

        fclose($this->logHandle);
        print_r("[INFO]: Please find the log file here: " . $this->sessionLog . PHP_EOL);
    }

    protected function checkLogRequirements()
    {
        $this->home = $this->evalSlash(getenv("HOME"));
        $this->scriptLogFolder = $this->home . "customer_dump_log_analyses/";

        if(!is_dir($this->scriptLogFolder)){
            shell_exec("mkdir " . $this->scriptLogFolder);
        }

        $this->sessionLog = $this->scriptLogFolder . $this->date . ".log";
        shell_exec("touch " . $this->sessionLog);
        $this->logHandle = fopen($this->sessionLog, 'a') or die("[ERROR]: Cannot open application log file");
    }

    protected function checkForCustomerDumpLogs()
    {
        $index = 0;
        $files = glob("customer_dump*.log");
        foreach(glob("customer_dump*.log.gz") as $GZ){
            array_push($files, $GZ);
        }

        if(count($files) > 1){
            $highestIndex = -1;

            foreach($files as $file){
                $highestIndex++;
                $this->logToFile(1,"Index: " . $highestIndex . ", File: " . $file);
            }

            $this->logToFile(1, PHP_EOL . "[INPUT]: Found above files. Please provide the index of the log file to analyze...:");
            $inputIndex = fopen("php://stdin", "r");
            $index = trim(fgets($inputIndex));
            $this->logToFile(0, "Input received: " . $index);

            if($index == ''){
                $this->logToFile(1,"[ERROR]: You did not provide an index. Aborting");
                die(PHP_EOL);
            }else if(!is_numeric($index)) {
                $this->logToFile(1,"[ERROR]: The index should be an integer. Aborting");
                die(PHP_EOL);
            }else if($index > $highestIndex || $index < 0){
                $this->logToFile(1,"[ERROR]: There is no such index. Aborting");
                die(PHP_EOL);
            }
        }else if(count($files) == 0){
            $this->logToFile(1,"[ERROR]: No customer dump logs found in the current working directory. Aborting");
            die();
        }

        $this->logToFile(0, "[INFO]: Using '" . $files[$index] . "' for analysis");

        $file = $files[$index];

        if($this->endsWith($file, ".gz")){
            $file = $this->prepareGZ($file);
        }

        return $file;
    }

    protected function processCustomerDumpLog($customerDumpLog)
    {
        $this->logToFile(1, "[INFO]: Analyzing...");

        $totalRequests = 0;
        $totalResponses = 0;
        $totalUnclassified = 0;
        $totalSuccess = 0;
        $totalPartial = 0;
        $totalFailed = 0;
        $badRequestCounter = 0;
        $timeOutCounter = 0;

        $handle = fopen($customerDumpLog, "r") or die("[ERROR]: Cannot open customer dump log file");
        $lineCount = 0;

        while(!feof($handle)){
            $lineData = trim(fgets($handle));
            $lineCount++;

            if($this->startsWith($lineData, '[{"orderNumbers"')) {
                $totalRequests++;
            }else if($this->startsWith($lineData, '{"data":{"message":"Import customer data are processed successfully."')){
                $totalResponses++;
                $totalSuccess++;

            }else if($this->startsWith($lineData, '{"data":{"message":"Import customer data are processed partially.')){
                $totalResponses++;
                $totalPartial++;

                $invalidData = json_decode($lineData)->{"data"}->{"invalidData"};
                foreach($invalidData as $invalidDataArray){
                    $errors = $invalidDataArray->{"errors"};
                    foreach($errors as $errorData){
                        $error = $errorData->{"title"};
                        $ban = json_decode($errorData->{"source"}->{"data"})->{"accountNumber"};

                        if($this->testErrorUniqueness($error, "Partial")){
                            $this->uniqueErrors["Partial"]["Errors"][$error][] = $ban;
                        }else{
                            array_push($this->uniqueErrors["Partial"]["Errors"][$error][], $ban);
                        }
                    }
                }
            }else if($this->startsWith($lineData,'{"data":{"message":"Failed')){
                $totalResponses++;
                $totalFailed++;
                $badRequestCounter++;

                if($this->endsWith($this->previousLineData, '`400 Bad Request` response:')){
                    $this->uniqueErrors["Failed"]["Errors"]["400 Bad Request"][0] = $badRequestCounter;
                }else{
                    $this->logToFile(1, "[WARNING]: No error other than '400 Bad Request' was found for 'Client error' in the past. Report this so this script can be adjusted");
                }

            }else if($this->startsWith($lineData, 'Server error:')){
                $totalResponses++;
                $totalFailed++;
                $timeOutCounter++;

                if($this->endsWith($lineData, '`504 Gateway Time-out` response:')){
                    $this->uniqueErrors["Failed"]["Errors"]["504 Gateway Time-out"][0] = $timeOutCounter;
                }else{
                    $this->logToFile(1, "[WARNING]: No error other than '504 Gateway Time-out' was found for 'Server error' in the past. Report this so this script can be adjusted");
                }
            }else{
                $totalUnclassified++;
            }

            $this->previousLineData = $lineData;
        }

        fclose($handle);

        if($this->cleanUpFile){
            shell_exec("rm " . $customerDumpLog);
        }

        $this->logToFile(1, "[INFO]: Finished");
        $this->logToFile(0, "[INFO]: Lines read: " . $lineCount);
        $this->logToFile(0, "[INFO]: Lines identified as requests: " . $totalRequests);
        $this->logToFile(0, "[INFO]: Lines identified as responses: " . $totalResponses);
        $this->logToFile(0, "[INFO]: Lines unclassified: " . $totalUnclassified);
        $this->logToFile(0, "[INFO]: Sum identified + unclassified lines: " . ((int)$totalRequests+(int)$totalResponses+(int)$totalUnclassified));
        $this->logToFile(0, "[INFO]: Handled successfully: " . $totalSuccess);
        $this->logToFile(0, "[INFO]: Handled partially: " . $totalPartial);
        $this->logToFile(0, "[INFO]: Failed: " . $totalFailed);

        foreach($this->uniqueErrors as $bucket => $errors){
            $this->logToFile(0, PHP_EOL . "[" . strtoupper($bucket) . "]");
            foreach($errors["Errors"] as $error => $subjects){
                $this->logToFile(0, PHP_EOL . "[" . $error . "]");
                foreach($subjects as $subject){
                    $this->logToFile(0, $subject);
                }
            }
        }
    }

    protected function prepareGZ($GZFile)
    {
        $this->GZWorkFolder = $this->scriptLogFolder . "GZWorkFolder/";
        if(!is_dir($this->GZWorkFolder)){
            shell_exec("mkdir " . $this->GZWorkFolder);
        }

        shell_exec("cp " . $GZFile . " " . $this->GZWorkFolder);
        $GZFilePath = $this->GZWorkFolder . $GZFile;
        shell_exec("gunzip " . $GZFilePath);
        $filePath = glob($this->GZWorkFolder . "customer_dump*.log")[0];
        $this->cleanUpFile = true;

        return $filePath;
    }

    protected function testErrorUniqueness($errorToTest, $bucket)
    {
        if(!empty($this->uniqueErrors)){
            foreach($this->uniqueErrors[$bucket]["Errors"] as $uniqueError){
                if($errorToTest === $uniqueError){
                    return false;
                }
            }
        }

        return true;
    }

    protected function startsWith ($string, $startString)
    {
        $length = strlen($startString);
        return (substr($string, 0, $length) === $startString);
    }

    protected function endsWith($string, $endString)
    {
        $length = strlen($endString);
        return (substr($string, -$length) === $endString);
    }
}

$analyzeLogs = new Vznl_Analyze_Customer_Dump_Logs();
$analyzeLogs->run();
