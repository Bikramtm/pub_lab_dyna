<?php
require_once(realpath(dirname(__FILE__)).'/../app/Mage.php');

$storeIds = array();
$defaultStoreIds = array_keys(Mage::app()->getStores());
$belcompanyId = array_filter(array_map(function($store) {
    if (false !== strpos($store->getCode(), 'belcompany')) {
        return $store->getId();
    }
}, Mage::app()->getStores()));
$vRetailId = array_filter(array_map(function($store) {
    if (false !== strpos($store->getCode(), 'default')) {
        return $store->getId();
    }
}, Mage::app()->getStores()));
$isCli = false;

if (php_sapi_name() == 'cli' ||(is_numeric($_SERVER['argc']) && $_SERVER['argc'] > 0)) { //console
    $isCli = true;
    if (isset($argv[1]) && 0 === strpos($argv[1], 'stores=')) { //second argument must be stores=x,y,z
        $storeIds = array_unique(array_filter(array_map('intval', array_map('trim', explode(',', str_replace('stores=', '', $argv[1]))))));
    }
} elseif (isset($_GET['stores']) || isset($_POST['stores'])) { //web
    $stores = isset($_GET['stores'])
        ? $_GET['stores']
        : $_POST['stores'];
    $storeIds = array_unique(array_filter(array_map('intval', array_map('trim', explode(',', str_replace('stores=', '', $stores))))));
}

if ( ! count($storeIds)) {
    $storeIds = array(reset($vRetailId), reset($belcompanyId));
}

if (count(array_diff($storeIds, $defaultStoreIds))) {
    $message = sprintf(
        'Invalid store ID(s) provided. Provided store IDs must be part of the current existing store list (%s). Invalid IDs: %s',
        join(', ', $defaultStoreIds),
        join(', ', array_diff($storeIds, $defaultStoreIds))
    );
    if ($isCli) {
        echo sprintf("\n%s\n", $message);
    } else {
        header('HTTP/1.1 400 Bad Request', true, 400);
        echo $message;
    }
    exit(1);
}

$exporter = Mage::getSingleton('vznl_sandbox/catalogExporter'); /** @var Dyna_Sandbox_Model_CatalogExporter $exporter */
$strategy = Mage::getSingleton('vznl_sandbox/exportStrategy_AxiXmlCatalog'); /** @var Dyna_Sandbox_Model_ExportStrategy_AxiXmlCatalog $strategy */
$appEmulation = Mage::getSingleton('core/app_emulation');
$exportXml = null;

foreach ($storeIds as $storeId) {
    $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

    $collection = Mage::getModel('catalog/product')
        ->getCollection()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('is_deleted', array('neq' => 1))
        ->addAttributeToFilter('type_id', array('eq' => 'simple'))
        ->addAttributeToFilter('status', array('gt' => 0))
        ->addStoreFilter();

    /** @var Dyna_Service_Model_SimpleDOM $exportXml */
    $exportXml = $exporter->export($collection, $strategy, $exportXml);

    $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
}

if ( ! $isCli) {
    header("Content-type: text/xml; charset=utf-8");
}

echo str_replace('<?xml version="1.0"?>', '<?xml version="1.0" encoding="UTF-8"?>', $exportXml->asPrettyXML());