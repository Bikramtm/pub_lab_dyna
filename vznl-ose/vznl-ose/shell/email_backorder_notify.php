<?php

require_once 'abstract.php';

/**
 * Class Vznl_Email_Backorder_Notify
 */
class Vznl_Email_Backorder_Notify extends Mage_Shell_Abstract
{
    protected $triggerTime;

    /**
     * @var Vznl_Communication_Helper_Email
     */
    protected $emailHelper;

    public function __construct()
    {
        parent::__construct();
        Mage::getSingleton('core/translate')->init('frontend');
    }

    /**
     * Run script
     */
    public function run()
    {
        // Set current trigger time
        $this->triggerTime = new DateTime();

        // Get the email helper
        $this->emailHelper = Mage::helper('communication/email');

        // Trigger all emails
        $this->triggerPostpaidNonPortingPackages();
        $this->triggerPostpaidPortingPackages();
        $this->triggerPrepaidPackages();
    }

    protected function triggerPostpaidNonPortingPackages()
    {
        $packageIds = $this->getPostPaidNonPortingPackageIds(false);

        foreach ($packageIds as $item) {
            $packageId = $item->getEntityId();
            /** @var Vznl_Package_Model_Package $package */
            $package = Mage::getModel('vznl_package/package')->load($packageId);

            if (!$package || !$package->getId()) {
                continue;
            }

            $superOrderWebsiteCode = Mage::app()->getWebsite($package->getSuperOrder()->getWebsiteId())->getCode();
            /** @var Vznl_Customer_Model_Customer_Customer $customer */
            $customer = $package->getDeliveryOrder()->getCustomer();
            $emailCode = !$customer->getIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_BACKORDER_POSTPAID : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_BACKORDER_POSTPAID_BUSINESS;

            try {
                $payload = $this->emailHelper->getBackOrderPayload($package, $emailCode);
                Mage::getSingleton('communication/pool')->add($payload, $superOrderWebsiteCode);
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, 'EmailErrorLog.log', true);
            }
        }
    }

    protected function triggerPostpaidPortingPackages()
    {
        $packageIds = $this->getPostPaidPortingPackageIds(false);

        foreach ($packageIds as $item) {
            $packageId = $item->getEntityId();
            /** @var Vznl_Package_Model_Package $package */
            $package = Mage::getModel('vznl_package/package')->load($packageId);

            if (!$package || !$package->getId()) {
                continue;
            }

            $superOrderWebsiteCode = Mage::app()->getWebsite($package->getSuperOrder()->getWebsiteId())->getCode();
            /** @var Vznl_Customer_Model_Customer_Customer $customer */
            $customer = $package->getDeliveryOrder()->getCustomer();
            $emailCode = !$customer->getIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_BACKORDER_POSTPAID : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_BACKORDER_POSTPAID_BUSINESS;

            try {
                $payload = $this->emailHelper->getBackOrderPayload($package, $emailCode);
                Mage::getSingleton('communication/pool')->add($payload, $superOrderWebsiteCode);
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, 'EmailErrorLog.log', true);
            }
        }
    }

    protected function triggerPrepaidPackages()
    {
        $packageIds = $this->getPrePaidPackageIds(false);

        foreach ($packageIds as $item) {
            $packageId = $item->getEntityId();
            /** @var Vznl_Package_Model_Package $package */
            $package = Mage::getModel('vznl_package/package')->load($packageId);

            if (!$package || !$package->getId()) {
                continue;
            }

            $superOrderWebsiteCode = Mage::app()->getWebsite($package->getSuperOrder()->getWebsiteId())->getCode();
            /** @var Vznl_Customer_Model_Customer_Customer $customer */
            $customer = $package->getDeliveryOrder()->getCustomer();
            $emailCode = !$customer->getIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_BACKORDER_PREPAID : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_BACKORDER_PREPAID_BUSINESS;

            try {
                $payload = $this->emailHelper->getBackOrderPayload($package, $emailCode);
                Mage::getSingleton('communication/pool')->add($payload, $superOrderWebsiteCode);
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, 'EmailErrorLog.log', true);
            }
        }
    }

    /**
     * Getting postpaid home delivery packages without porting and with status backorder.
     * @param bool $reminder <true> will find all packages that require a reminder email, <false> will find all packages
     * that need to have their first backorder email.
     */
    protected function getPostPaidNonPortingPackageIds($reminder = false)
    {
        // Check if it is a reminder or not
        $hours = !$reminder ? 24 : 120;

        /** @var DateTime $dateTime */
        $dateTime = $this->triggerTime;
        $dateTime->sub(new DateInterval(sprintf('PT%sH', $hours)));
        $toDate = $dateTime->format('Y-m-d H:i:s');

        $dateTime->sub(new DateInterval(sprintf('PT%sH', 1)));
        // add 1 to make sure that it is not possible to have an email send twice for an order.
        $dateTime->add(new DateInterval('PT1S'));
        $fromDate = $dateTime->format('Y-m-d H:i:s');

        $collection = Mage::getModel('vznl_package/package')->getCollection()
            ->distinct(true)
            ->addFieldToFilter('main_table.status', Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_BACK_ORDER)
            ->addFieldToFilter('main_table.type', Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE)
            ->addFieldToFilter('sales_flat_order_address.delivery_store_id', array('null' => true))
            ->addFieldToFilter('main_table.current_number', array('null' => true))
            ->addFieldToFilter('status_history.created_at', array(
                'from' => $fromDate,
                'to' => $toDate,
                'date' => true,
            ));
        $collection->getSelect()->join(
            array('sales_flat_order'),
            'main_table.order_id = sales_flat_order.superorder_id'
        );
        $collection->getSelect()->join(
            array('sales_flat_order_item'),
            'sales_flat_order.entity_id = sales_flat_order_item.order_id AND main_table.package_id = sales_flat_order_item.package_id'
        );
        $collection->getSelect()->join(
            array('sales_flat_order_address'),
            'sales_flat_order.entity_id = sales_flat_order_address.parent_id AND sales_flat_order_address.address_type = \'shipping\''
        );
        $collection->getSelect()->join(
            array('status_history'),
            'main_table.entity_id = status_history.package_id AND main_table.status = status_history.status',
            array('status_history.created_at')
        );

        // Remove all selected columns and select only the package entity_id
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS)
            ->columns('main_table.entity_id');

        return $collection;
    }

    /**
     * Getting postpaid home delivery packages with porting and with status backorder.
     * @param bool $reminder <true> will find all packages that require a reminder email, <false> will find all packages
     * that need to have their first backorder email.
     */
    protected function getPostPaidPortingPackageIds($reminder = false)
    {
        // Check if it is a reminder or not
        $hours = !$reminder ? 48 : 168;

        /** @var DateTime $dateTime */
        $dateTime = $this->triggerTime;
        $dateTime->sub(new DateInterval(sprintf('PT%sH', $hours)));
        $toDate = $dateTime->format('Y-m-d H:i:s');

        $dateTime->sub(new DateInterval(sprintf('PT%sH', 1)));
        // add 1 to make sure that it is not possible to have an email send twice for an order.
        $dateTime->add(new DateInterval('PT1S'));
        $fromDate = $dateTime->format('Y-m-d H:i:s');

        $collection = Mage::getModel('vznl_package/package')->getCollection()
            ->distinct(true)
            ->addFieldToFilter('main_table.status', Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_BACK_ORDER)
            ->addFieldToFilter('main_table.type', Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE)
            ->addFieldToFilter('sales_flat_order_address.delivery_store_id', array('null' => true))
            ->addFieldToFilter('main_table.current_number', array('neq' => 'NULL' ))
            ->addFieldToFilter('status_history.created_at', array(
                'from' => $fromDate,
                'to' => $toDate,
                'date' => true,
            ));
        $collection->getSelect()->join(
            array('sales_flat_order'),
            'main_table.order_id = sales_flat_order.superorder_id'
        );
        $collection->getSelect()->join(
            array('sales_flat_order_item'),
            'sales_flat_order.entity_id = sales_flat_order_item.order_id AND main_table.package_id = sales_flat_order_item.package_id'
        );
        $collection->getSelect()->join(
            array('sales_flat_order_address'),
            'sales_flat_order.entity_id = sales_flat_order_address.parent_id AND sales_flat_order_address.address_type = \'shipping\''
        );
        $collection->getSelect()->join(
            array('status_history'),
            'main_table.entity_id = status_history.package_id AND main_table.status = status_history.status',
            array('status_history.created_at')
        );

        // Remove all selected columns and select only the package entity_id
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS)
            ->columns('main_table.entity_id');

        return $collection;
    }

    /**
     * Getting prepaid home delivery packages with status backorder.
     * @param bool $reminder <true> will find all packages that require a reminder email, <false> will find all packages
     * that need to have their first backorder email.
     */
    protected function getPrePaidPackageIds($reminder = false)
    {
        // Check if it is a reminder or not
        $hours = !$reminder ? 24 : 120;

        /** @var DateTime $dateTime */
        $dateTime = $this->triggerTime;
        $dateTime->sub(new DateInterval(sprintf('PT%sH', $hours)));
        $toDate = $dateTime->format('Y-m-d H:i:s');

        $dateTime->sub(new DateInterval(sprintf('PT%sH', 1)));
        // add 1 to make sure that it is not possible to have an email send twice for an order.
        $dateTime->add(new DateInterval('PT1S'));
        $fromDate = $dateTime->format('Y-m-d H:i:s');

        $collection = Mage::getModel('vznl_package/package')->getCollection()
            ->distinct(true)
            ->addFieldToFilter('main_table.status', Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_BACK_ORDER)
            ->addFieldToFilter('main_table.type', Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID)
            ->addFieldToFilter('sales_flat_order_address.delivery_store_id', array('null' => true))
            ->addFieldToFilter('status_history.created_at', array(
                'from' => $fromDate,
                'to' => $toDate,
                'date' => true,
            ));
        $collection->getSelect()->join(
            array('sales_flat_order'),
            'main_table.order_id = sales_flat_order.superorder_id'
        );
        $collection->getSelect()->join(
            array('sales_flat_order_item'),
            'sales_flat_order.entity_id = sales_flat_order_item.order_id AND main_table.package_id = sales_flat_order_item.package_id'
        );
        $collection->getSelect()->join(
            array('sales_flat_order_address'),
            'sales_flat_order.entity_id = sales_flat_order_address.parent_id AND sales_flat_order_address.address_type = \'shipping\''
        );
        $collection->getSelect()->join(
            array('status_history'),
            'main_table.entity_id = status_history.package_id AND main_table.status = status_history.status',
            array('status_history.created_at')
        );

        // Remove all selected columns and select only the package entity_id
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS)
            ->columns('main_table.entity_id');

        return $collection;
    }

    /**
     * Display help on CLI
     * @return string
     */
    public function usageHelp()
    {
        $f = basename(__FILE__);

        return <<< USAGE
Usage: php ${f} [options]

  -q            Quiet, do not output to cli
  -h            Short alias for help
  help          This help
USAGE;
    }
}

$shell = new Vznl_Email_Backorder_Notify();
$shell->run();
