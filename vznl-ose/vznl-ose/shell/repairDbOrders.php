<?php
require_once 'repairDbClass.php';

/*
 * Fix relationship between shopping cart, order and catalogue
 * */
class Tools_Db_Repair_Orders extends Tools_Db_Repair
{
    const LIMIT    = 100;

    /**
     * Tools_Db_Repair_Orders constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->logFile = "repairdb_fix_orders_".date('Y_m_d').".log";
    }

    /**
     * Run script
     */
    public function run()
    {
        $this->log("Start script.");
        $results = $this->getBrokenData();

        // if in readonly mode, just return the required actions
        if ($this->getReadOnly() == true) {
            $this->log("Stop script.");
            return (!empty($results)) ? ["Found broken relationship between shopping cart, order and catalogue."] : [];
        }

        // update db records
        while (!empty($results)) {
            foreach ($results as $result) {
                $sql = "UPDATE sales_flat_order_item AS item 
                        SET item.product_id = (SELECT entity_id FROM catalog_product_entity WHERE sku='{$result['sku']}' LIMIT 1) 
                        WHERE item_id={$result['item_id']} AND product_id = {$result['product_id']};";

                $this->log("Running: ".$sql);
                $this->writeConnection->exec($sql);
            }

            $this->step += self::LIMIT;
            usleep(500000);
            $results = $this->getBrokenData();
        }
        $this->log("Stop script.");
    }

    /**
     * Get broken records
     * @return mixed
     */
    private function getBrokenData()
    {
        $sql  = "SELECT item.item_id, item.sku, item.product_id 
                 FROM sales_flat_order_item AS item
                 WHERE item.product_id != (SELECT ent.entity_id FROM catalog_product_entity AS ent WHERE ent.sku = item.sku) 
                 ORDER BY item.item_id DESC 
                 LIMIT ".self::LIMIT;
        return $this->readConnection->fetchAll($sql);
    }
}

