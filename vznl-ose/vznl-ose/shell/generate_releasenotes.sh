#!/bin/bash

LASTCOMMIT=$(git rev-list --tags --max-count=1)
LASTDATE=$(git show -s --format=%ad `git rev-list --tags --max-count=1`)

git log --pretty=format:"- [%h](https://bitbucket.org/dynalean/baseline-ose/commits/%H)%n  %s (%an)" --date=short --since="$LASTDATE"
