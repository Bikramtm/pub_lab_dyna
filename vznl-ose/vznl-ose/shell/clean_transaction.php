<?php

require_once 'vznl_abstract.php';

/**
 * Class Vznl_Clean_Transaction
 */
class Vznl_Clean_Transaction extends Vznl_Shell_Abstract
{
    const BATCH_SIZE = 500;
    /** @var float */
    protected $_startTime = 0.0;
    protected $enabled = false;
    protected $deleteOlderThanDays = 14;

    /**
     * Initialize application and parse input parameters
     */
    public function __construct()
    {
        $this->_lockExpireInterval = 14400;
        $this->_startTime = microtime(true);
        parent::__construct();
    }

    /**
     * @param $data
     * @return array
     */
    protected function toArray($data)
    {
        $items = [];
        if (is_null($data)) {
            return $data;
        } elseif (is_scalar($data)) {
            return $data;
        } elseif (is_array($data)) {
            foreach ($data as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif ($data instanceof Varien_Data_Collection) {
            foreach ($data as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif ($data instanceof Varien_Object) {
            foreach ($data->getData() as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif (is_object($data)) {
            foreach (get_object_vars($data) as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        }

        return $items;
    }

    /**
     * Run script
     */
    public function run()
    {
        try {
            if(!$this->hasLock()) {
                $this->createLock();
                $interval = 60;
                $batch = self::BATCH_SIZE;
                $date = new DateTime(sprintf('-%s days', $interval));
                do {
                    $now = new DateTime();
                    /** @var Omnius_Audit_Model_Mysql4_Snapshot_Collection $collection */
                    $collection = Mage::getModel('transaction/transaction')->getCollection()
                        ->addFieldToFilter('created_at', ['lt' => $date->format('Y-m-d H:i:s')]);
                    $collection->getSelect()->limit($batch);
                    $nr = 0;
                    $count = $collection->count();
                    if ($count == 0) {
                        break;
                    }

                    if ($this->enabled) {
                        $filename = sprintf('transaction_backup_%s.tar.gz',
                            strtr($now->format('c'), [':' => '', '-' => '', '+' => '']));
                        $tmpDir = sprintf('%s/transaction_backup_%s', sys_get_temp_dir(),
                            md5(time() . rand(0, 100)));
                        if (!@mkdir($tmpDir)) {
                            throw new Exception(sprintf('Could not create temp dir (%s)', $tmpDir));
                        }
                    }

                    /** @var Omnius_Audit_Model_Snapshot $transaction */
                    foreach ($collection->getItems() as $transaction) {
                        if ($this->enabled) {
                            $tempFile = tempnam($tmpDir, sprintf('transaction_%s_', $transaction->getId()));
                            $data = $transaction->toArray();
                            file_put_contents($tempFile, json_encode($data));
                        }
                        $nr++;
                    }

                    $coreResource = Mage::getSingleton('core/resource');
                    $conn = $coreResource->getConnection('core_read');

                    if ($this->enabled) {
                        $command = sprintf(
                            'cd %s && tar -zcvf %s %s/ && mv %s %s',
                            sys_get_temp_dir(),
                            $filename,
                            str_replace(sprintf('%s/', sys_get_temp_dir()), '', $tmpDir),
                            $filename,
                            sprintf('%s/export/%s', Mage::getBaseDir('var'), $filename)
                        );

                        if (false === system($command)) {
                            throw new Exception(sprintf('Could not create archive (%s).', $command));
                        } else {
                            //delete folder after archiving
                            array_map('unlink', glob("$tmpDir/*"));
                            rmdir($tmpDir);
                            if (!$this->getArg('q')) {
                                $this->printLn(sprintf('Backup of %s audit snapshots made in %s', $nr,
                                    sprintf('%s/export/%s', Mage::getBaseDir('var'), $filename)));
                            }
                        }
                    }

                    $deleteStmt = sprintf("DELETE FROM `transaction` WHERE (`created_at` < '%s') limit %s",
                        $date->format('Y-m-d H:i:s'), $batch);
                    $conn->query($deleteStmt);
                    if (!$this->getArg('q')) {
                        $this->printLn(sprintf('Deleted %s transaction in %ss', $nr,
                            number_format(microtime(true) - $this->_startTime, 3)));
                    }
                } while ($count == $batch);
                if ($this->enabled) {
                    //delete older archives
                    foreach (glob("/tmp/transaction_backup_*") as $oldFile) {
                        if ((time() - filectime($oldFile)) / 86400 >= $this->deleteOlderThanDays) {
                            unlink($oldFile);
                        }
                    }
                }
                $this->removeLock();
                exit(0);
            } else{
                throw new Exception('Cannot start script due to existing lock: '.$this->getLockPath());
            }
        } catch (Exception $e) {
            $this->printLn($e->getMessage());
            $this->printLn($e->getTraceAsString());
            exit(1);
        }
    }

    /**
     * @param $message
     */
    protected function printLn($message)
    {
        if (is_array($message)) {
            foreach ($message as $line) {
                echo sprintf("%s\n\r", $line);
            }
        } else {
            echo sprintf("%s\n\r", $message);
        }
    }

    /**
     * Display help on CLI
     * @return string
     */
    public function usageHelp()
    {
        $f = basename(__FILE__);

        return <<< USAGE
Usage: php ${f} [options]

  -q            Quiet, do not output to cli
  -h            Short alias for help
  help          This help
USAGE;
    }
}

$shell = new Vznl_Clean_Transaction();
$shell->run();
