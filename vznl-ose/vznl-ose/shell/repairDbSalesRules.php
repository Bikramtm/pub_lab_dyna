<?php
require_once 'repairDbClass.php';
/*
 * Update Lifecycle in sales rules's conditions (NL to DE values)
 */

/**
 * Class Tools_Db_Repair_SalesRules
 */
class Tools_Db_Repair_SalesRules extends Tools_Db_Repair
{
    /**
     * Sales rule table
     * @var string
     */
    private $table = 'salesrule';
    private $lifecycleType = 'pricerules/condition_lifecycle';
    private $processContextType = 'dyna_pricerules/condition_processContext';
    private $processContextAttribute = 'process_context';

    /**
     *  Tools_Db_Repair_SalesRules constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->logFile = "repairdb_sales_rules_" . date('Y_m_d') . ".log";
    }

    /**
     * Run script
     */
    public function run()
    {
        $this->log("Start script.");
        $salesRules = $this->getAllSalesRules();

        // if in readonly mode, just return the required actions
        if ($this->getReadOnly() == true) {
            $this->log("Stop script.");
            return;
        }

        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $processContextIds = $processContextHelper->getProcessContextsByNamesForForm();
        $mappedProcessContexts = $mappedOperators = array();
        // map process contexts with labels as key because the same labels are the values for lifecycles
        foreach ($processContextIds as $processContextId) {
            $mappedProcessContexts[$processContextId['label']] = $processContextId['value'];
        }
        // also, map operators (switch the lifecycle ones with those from process contexts)
        $mappedOperators['=='] = '()';
        $mappedOperators['!='] = '!()';

        foreach ($salesRules as $salesRule) {
            // unserialize conditions
            $conditions = unserialize($salesRule['conditions_serialized']);
            if (!empty($conditions['conditions'])) {
                // search for acquisitie (NL) and replace it with ACQ (DE)
                if (($searchedValue = array_search(Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE, array_column($conditions['conditions'], 'value'))) !== false) {
                    $conditions['conditions'][$searchedValue]['value'] = Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION;
                }
                // search for retentie (NL) and replace it with RETBLU (DE)
                if (($searchedValue = array_search(Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE, array_column($conditions['conditions'], 'value'))) !== false) {
                    $conditions['conditions'][$searchedValue]['value'] = Dyna_Checkout_Model_Sales_Quote_Item::RETENTION;
                }
                // replace lifecycle with process context
                if (($searchedValue = array_search($this->lifecycleType, array_column($conditions['conditions'], 'type'))) !== false) {
                    $conditions['conditions'][$searchedValue]['type'] = $this->processContextType;
                    $conditions['conditions'][$searchedValue]['attribute'] = $this->processContextAttribute;
                    $conditions['conditions'][$searchedValue]['operator'] = $mappedOperators[$conditions['conditions'][$searchedValue]['operator']];
                    $conditions['conditions'][$searchedValue]['value'] = $mappedProcessContexts[$conditions['conditions'][$searchedValue]['value']];
                }

                if (($searchedValue = array_search($this->processContextType, array_column($conditions['conditions'], 'type'))) !== false) {
                    if (is_int($conditions['conditions'][$searchedValue]['value'])) {
                        $conditions['conditions'][$searchedValue]['value'] = [$conditions['conditions'][$searchedValue]['value']];
                    }
                }
            }
            // serialize them back in order to update the table
            $serializedConditions = serialize($conditions);
            $this->log("Update sales rule #" . $salesRule['rule_id']);
            //update the sales rule
            $this->writeConnection->update($this->table,
                array('conditions_serialized' => $serializedConditions),
                array('rule_id =?' => $salesRule['rule_id']));
        }

        $this->log("Stop script.");
    }

    /**
     * Get broken records
     * @return mixed
     */
    private function getAllSalesRules()
    {
        $this->log("Get all the sales rules from DB");
        $sql = "SELECT `rule_id`, `conditions_serialized` FROM `salesrule`";
        return $this->readConnection->fetchAll($sql);
    }
}