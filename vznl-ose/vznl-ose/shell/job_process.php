<?php

require_once 'abstract.php';

/**
 * Class Vznl_Job_Process
 */
class Vznl_Job_Process extends Mage_Shell_Abstract
{
    protected $_lastRun = 0;

    public function __construct()
    {
        parent::__construct();

        register_shutdown_function(array($this, 'shutdown'));
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        /** @var Vznl_Job_Model_RedisProcessor $processor */
        $processor = Mage::getSingleton('vznl_job/redisProcessor');
        try {
            $processor->process();
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            echo $e->getMessage() . PHP_EOL;
        }
        $this->_lastRun = $processor->getProcessed();
    }

    public function shutdown()
    {
        usleep($this->_lastRun ? 0 : 700000); //let it rest for 0.7s only if queues were empty at last run
        /** @var Vznl_Job_Helper_Console $helper */
        $helper = Mage::helper('vznl_job/console');
        $helper->persistConsumers();
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  help							This help

USAGE;
    }
}

$processor = new Vznl_Job_Process();
$processor->run();
