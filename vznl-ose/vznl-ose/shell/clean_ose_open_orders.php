<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
require_once 'abstract.php';

/**
 * Class Dyna_CleanOseOpenOrders_Cli
 */
class Dyna_CleanOseOpenOrders_Cli extends Mage_Shell_Abstract
{
    const allCustomers = 'ALL';
    /**
     * Run database cleanup process
     */
    public function run()
    {
        $customer = $this->getArg('customer');
        if(!$customer){
            echo $this->usageHelp();
            exit(1);
        }

        $binds = array();
        $query = 'UPDATE superorder s INNER JOIN catalog_package p ON p.order_id = s.entity_id '
            . 'INNER JOIN sales_flat_order sfo ON sfo.superorder_id = s.entity_id SET s.customer_number = NULL, p.parent_account_number = NULL, sfo.customer_number = NULL';
        if($customer != self::allCustomers) {
            $query .= ' WHERE s.customer_number = :customer_number OR p.parent_account_number = :customer_number OR sfo.customer_number = :customer_number';
            $binds = array('customer_number' => $customer);
        }
        Mage::getSingleton('core/resource')->getConnection(Mage_Core_Model_Resource::DEFAULT_WRITE_RESOURCE)->query($query, $binds);

        echo 'Open orders cleaned' . ($customer != self::allCustomers ? " for customer number $customer" : '') . PHP_EOL;
    }

    /**
     * Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php file      
  help              This help
  customer              Customer for which the open orders will be removed (either a specific customer number or "ALL")
  
  Example: php clean_ose_open_orders.php --customer ALL 
  Example: php clean_ose_open_orders.php --customer 12345678
  
USAGE;
    }

}

$import = new Dyna_CleanOseOpenOrders_Cli();
$import->run();
