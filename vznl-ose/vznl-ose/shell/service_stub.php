<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 * Class Vznl_Service_Stub
 * 
 * To be executed only from shell
 */

require_once 'abstract.php';

class Vznl_Service_Stub extends Mage_Shell_Abstract
{
    /** @var  Vznl_Esb_Helper_Data */
    protected $_stubHelper;

    /**
     * Run script
     */
    public function run()
    {
    	if (!Mage::getStoreConfig(Vznl_Utility_Helper_Stubs::XML_PATH_PHP_STUB_ENABLE, Mage::app()->getStore())) {
    		throw new Exception('Unauthorized');
    	}
    	
    	$this->_stubHelper = Mage::helper('vznl_utility/stubs');
        
        $requestData = array(
        	'stub' => $this->getArg('s'),
        	'orderNumber' => $this->getArg('o'),
        	'packageId' => $this->getArg('p'),
        	'uri' => $this->getArg('u'),
        	'additionalData' => $this->getArg('a')
        );
        
        $response = $this->_stubHelper->execute($requestData);
        
        echo $response;
    }
    
    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
    	if (!Mage::getStoreConfig(Vznl_Utility_Helper_Stubs::XML_PATH_PHP_STUB_ENABLE, Mage::app()->getStore())) {
    		throw new Exception('Unauthorized');
    	}
    	
    	$this->_stubHelper = Mage::helper('vznl_utility/stubs');
    	return $this->_stubHelper->usageHelp();
    }
}

$cli = new Vznl_Service_Stub();
$cli->run();
