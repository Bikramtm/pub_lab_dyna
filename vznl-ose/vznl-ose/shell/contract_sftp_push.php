<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

require_once 'abstract.php';

/**
 * Class Vznl_Contract_Sftp_Push_Cli
 * Push the contracts generated to SFTP
 */
class Vznl_Contract_Sftp_Push_Cli extends Mage_Shell_Abstract
{
    /**
     * Run script
     */
    public function run()
    {
        try {
	    $cron = new Vznl_Contract_Model_Cron();
            $cron->processSftpJobs();
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * Display help on CLI
     * @return string
     */
    public function usageHelp()
    {
        $f = basename(__FILE__);
        return <<< USAGE
Usage: php ${f} [options]

  -q            Quiet, do not output to cli
  -h            Short alias for help
  help          This help
USAGE;
    }
}

$shell = new Vznl_Contract_Sftp_Push_Cli();
$shell->run();
