<?php
require_once 'abstract.php';

class Sandbox_Release_Publish_Scheduler extends Mage_Shell_Abstract
{
    // Debug mode
    const DEBUG = false;
    // minutes measurement unit
    const UNIT_MINUTES = "minutes";
    // hours measurement unit
    const UNIT_HOURS = "hours";

    // number of units
    private $_offset = 0;
    // measurement unit to be used
    private $_unit = self::UNIT_MINUTES;
    // how many seconds a unit contains
    private $_unitInterval = 0;
    private $scheduleTypeOptions;

    /** @var Omnius_Sandbox_Model_Publisher $_publisher */
    private $_publisher = null;

    protected $units = [
        self::UNIT_MINUTES,
        self::UNIT_HOURS
    ];

    /**
     * Run script
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set("Europe/Amsterdam");
        $this->scheduleTypeOptions = Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::getExecutionDateOneTimeOptions()
            + Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::getExecutionDatePeriodicOptions();
        if ($offset = $this->getArg("offset")) {
            $this->setOffset($offset);
        }
        if ($unit = $this->getArg("unit")) {
            $this->setUnit($unit);
        }
        $this->setUnitInterval();

        $this->_publisher = Mage::getSingleton('dyna_sandbox/publisher');

        try {
            /** @var Omnius_Sandbox_Model_Mysql4_Release_Collection $releaseCollection */
            $releaseCollection = Mage::getResourceModel('sandbox/release_collection')
                ->addFieldToFilter('status', array('neq' => Omnius_Sandbox_Model_Release::STATUS_SUCCESS))
                ->addFieldToFilter('deadline', array('gteq' => date('Y-m-d H:i:s')))
                ->addFieldToFilter('schedule_type', array('in' => array_keys($this->scheduleTypeOptions)))
                ->addFieldToFilter('tries', array('lt' => Mage::helper('sandbox')->getTriesLimit()))
                ->load();

            foreach ($releaseCollection->getItems() as $item) {
                if ($item->getData('schedule_type') == Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::SCHEDULE_TYPE_ONE_TIME) {
                    $this->_handleOneTimeScheduler($item);
                } else if ($item->getData('schedule_type') == Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::SCHEDULE_TYPE_PERIODIC) {
                    $this->_handlePeriodicScheduler($item);
                }
            }
        } catch (Exception $e) {
            $fh = fopen('php://stderr', 'w');
            fputs($fh, $e->__toString());
            fclose($fh);
            exit(255);
        }
    }

    /**
     * Sets the offset in minutes which can be considered when
     *
     * @param $offset Offset in minutes
     */
    public function setOffset($offset) {
        $this->_offset = $offset;

        if (is_numeric($offset)) {
            $this->_offset = $offset;
        } else {
            $this->_writeLine('Invalid parameter value for offset. Should be numeric. No offset applied');
        }
    }

    /**
     * Sets the offset in minutes which can be considered when
     *
     * @param $offset Offset in minutes
     */
    public function setUnit($unit) {
        if (in_array($unit, $this->units)) {
            $this->_unit = $unit;
        } else {
            $this->_writeLine('Invalid parameter value for unit. Should be [minutes/hours]. Default unit used: ['.$this->_unit.']');
        }
    }

    /**
     * Sets the interval in seconds for a unit
     */
    public function setUnitInterval() {
        if ($this->_unit == self::UNIT_MINUTES) {
            $this->_unitInterval = 60;
        } else if ($this->_unit == self::UNIT_HOURS) {
            $this->_unitInterval = 60*60;
        }
    }

    /**
     * Handles one time scheduler for release publish
     *
     * @param $item
     */
    private function _handleOneTimeScheduler($item) {
        $nowTimestamp = time();
        $oneTimeTimestamp = $this->_getTimestampRelativeToUnit(strtotime($item->getData('one_time_date')));

        if (
            ((int)($nowTimestamp/$this->_unitInterval) >= (int)($oneTimeTimestamp/$this->_unitInterval) &&
                isset($this->_publisher)
            ) || self::DEBUG
        ) {
            $this->_logReleaseData($item);
            $this->_publisher->publish($item);
            $this->_logReleasePublishResult($item);
        }
    }

    /**
     * Handles periodic scheduler for release publish
     *
     * @param $item
     */
    private function _handlePeriodicScheduler($item) {
        $proceedToPublish = true;
        $timeParts = explode(':', $item->getData('time'));
        $timestampNow = time();
        $currentTimeInSeconds = (int)date('G', $timestampNow) * 3600 + (int)date('i', $timestampNow) * 60;
        $targetTimeInSeconds = $timeParts[0] * 3600 + $timeParts[1] * 60;
        $offsetInSeconds = $this->_getOffsetSecondsRelativeToUnit();

        $isDaily = ($item->getData('period') == Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::PERIODIC_DAILY);
        $isWeekly = ($item->getData('period') == Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::PERIODIC_WEEKLY);
        $isTargetWeekDay = ($item->getData('week_day') == date('N', $timestampNow));
        $isMonthly = ($item->getData('period') == Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::PERIODIC_MONTHLY);
        $isTargetMonthDay = ($item->getData('month_day') == date('j', $timestampNow));

        if ((!($isWeekly && $isTargetWeekDay) && !($isMonthly && $isTargetMonthDay) && !$isDaily) ||
            !((int)($targetTimeInSeconds/$this->_unitInterval) <= (int)($currentTimeInSeconds/$this->_unitInterval)) ||
            !((int)($currentTimeInSeconds/$this->_unitInterval) <= (int)(($targetTimeInSeconds + $offsetInSeconds)/$this->_unitInterval)) ||
            !isset($this->_publisher)
        ) {
            $proceedToPublish = false;
        }

        if ($proceedToPublish || self::DEBUG) {
            $this->_logReleaseData($item);
            $this->_publisher->publish($item);
            $this->_logReleasePublishResult($item);
        }
    }

    /**
     * Handles one time scheduler for release publish
     *
     * @param $item
     */
    private function _getTimestampRelativeToUnit($timestamp) {
        $result = $timestamp;

        if ($this->_unit == self::UNIT_MINUTES) {
            $result = $timestamp - (int)date('s', $timestamp);
        } else if ($this->_unit == self::UNIT_HOURS) {
            $result = $timestamp - ((int)date('s', $timestamp) - (int)date('i', $timestamp)*60);
        }

        return $result;
    }

    /**
     * Handles one time scheduler for release publish
     *
     * @param $item
     */
    private function _getOffsetSecondsRelativeToUnit() {
        $offsetInSeconds = $this->_offset;

        switch ($this->_unit) {
            case self::UNIT_MINUTES:
                $offsetInSeconds = 60 * $offsetInSeconds;
                break;
            case self::UNIT_HOURS:
                $offsetInSeconds = 3600 * $offsetInSeconds;
                break;
            default:
                break;
        }

        return $offsetInSeconds;
    }


    /**
     * Echo log message to console when release is published
     * @param $msg
     */
    private function _logReleaseData($item)
    {
        $msg = '';
        $scheduleTypeDetails = '';


        if ($item->getData('schedule_type') == Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::SCHEDULE_TYPE_ONE_TIME) {
            $scheduleTypeDetails .= 'executed at ' . $item->getData('one_time_date') . ' ';
        } else if ($item->getData('schedule_type') == Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::SCHEDULE_TYPE_PERIODIC) {
            $periodOptions = Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::getPeriodicOptions();

            $scheduleTypeDetails .= 'executed ' . $periodOptions[$item->getData('period')] . ', ';
            if ($item->getData('period') == Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::PERIODIC_WEEKLY) {
                $weekDayOptions = Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::getWeekDays();

                $scheduleTypeDetails .= 'on ' . $weekDayOptions[$item->getData('week_day')] . ', ';
            } else if ($item->getData('period') == Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form::PERIODIC_MONTHLY) {
                $scheduleTypeDetails .= 'on ' . $item->getData('month_day') . ', ';
            }
            $scheduleTypeDetails .= 'at ' . $item->getData('time') . ' ';
        }

        $msg .= 'Scheduled release of ';
        $msg .= 'replica ' . $item->getData('replica') . ', ' ;
        $msg .= 'version ' . $item->getData('version') . ', ' ;
        $msg .= 'schedule type: ' . $this->scheduleTypeOptions[$item->getData('schedule_type')] . ', ' ;
        $msg .= $scheduleTypeDetails;
        $msg .= 'is being processed... ';

        $this->_writeLine($msg);
    }

    /**
     * Echo log message to console when release is published
     * @param $msg
     */
    private function _logReleasePublishResult($item)
    {
        $msg = '';

        if ($item->getData('status') == Omnius_Sandbox_Model_Release::STATUS_ERROR) {
            $msg .= 'failed!' . PHP_EOL;
        } else if ($item->getData('status') == Omnius_Sandbox_Model_Release::STATUS_SUCCESS) {
            $msg .= 'successful!' . PHP_EOL;
        }

        $this->_writeLine($msg);
    }

    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $availableUnits = implode('/', $this->units);
        return <<<USAGE
Usage:  php file -- [options]

  help							This help
  unit							Measurement units for time comparison [$availableUnits]
  offset						Range for time comparison as number of units

USAGE;
    }
}

$shell = new Sandbox_Release_Publish_Scheduler();
$shell->run();
