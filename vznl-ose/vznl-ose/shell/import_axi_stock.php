<?php

// Enable errors to make sure we get all the details in cron logs
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'vznl_abstract.php';
ini_set('memory_limit', '2G');

class Vznl_Import_Axi_Stock_Cli extends Vznl_Shell_Abstract
{

    public function __construct()
    {
        $this->_lockExpireInterval = 3600; // 3m
        parent::__construct();
        Mage::getSingleton('customer/session');
        Mage::getSingleton('core/session');
    }

    protected $_files = array();
    protected $_store_ids = array();

    private function _writeLine($msg)
    {
        Mage::log($msg, null, 'axi_stock.log');
    }

    protected function connect()
    {
        $connectionDetails['host'] = Mage::getStoreConfig('vodafone_service/axi_stock_sftp/hostname');
        $connectionDetails['username'] = Mage::getStoreConfig('vodafone_service/axi_stock_sftp/username');
        $connectionDetails['password'] = Mage::getStoreConfig('vodafone_service/axi_stock_sftp/password');

        $sftp = new Varien_Io_Sftp();
        $sftp->open(array(
            'host' => $connectionDetails['host'],
            'username' => $connectionDetails['username'],
            'password' => $connectionDetails['password'],
        ));

        return $sftp;
    }

    protected function _flushVarnish()
    {
        if ($store_ids = array_unique($this->_store_ids)) {
            try {
                $servers = array_filter(array_map('trim', explode(PHP_EOL, Mage::getStoreConfig('dev/debug/varnish_ip'))), function ($serverIp) {
                    return $serverIp;
                });

                $cmd = join('; ', array_map(function ($server) use ($store_ids) {
                        return sprintf('(curl --noproxy "*" -X BAN -H %s %s > /dev/null 2>&1 &)', escapeshellarg('X-Ban-Url: .*(getDeviceStock|getAccessoriesStock|getStock)(.*)(axi_store_id=(' . implode('|', $store_ids) . ')).*'), escapeshellarg($server));
                    }, $servers)) . ';';
                $this->_writeLine('Clearing varnish with command: ' . $cmd);
                if (strlen($output = exec($cmd))) {
                    throw new Exception(sprintf('Could not clear Varnish cache. Output: %s', $output));
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
            }
        }
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        $this->_store_ids = array();
        try {
            if(!$this->hasLock()) {
                $this->createLock();

                $start = time();
                $sftp = $this->connect();
                $sftp->cd(Mage::getStoreConfig('vodafone_service/axi_stock_sftp/path'));
                $files = $sftp->ls();

                // Copy remote files to local filesystem
                foreach ($files as $file) {
                    $localFile = tempnam('/tmp', 'mage_axi_stock_');
                    if ($sftp->read($file['text'], $localFile)) {
                        $this->_writeLine('File "' . $file['text'] . '" loaded to local filesystem');
                        $this->_files[] = array('remote' => $file['text'], 'local' => $localFile);
                    } else {
                        $this->_writeLine('File "' . $file['text'] . '" skipped. Unable to read');
                    }
                }
                $sftp->close();

                // Import files
                $handledFiles = array();
                foreach ($this->_files as $file) {
                    $this->_writeLine('Starting import file "' . $file['local'] . '"');
                    if ($this->_handleAxiStockFile($file['local'])) {
                        $this->_writeLine('Import file "' . $file['local'] . '" done');
                        $handledFiles[] = $file;
                    } else {
                        $this->_writeLine('Could not import local file "' . $file['local'] . '"');
                    }
                    $this->_writeLine('Deleting local file "' . $file['local'] . '"');
                    unlink($file['local']);
                }

                // Delete remote files
                $sftp = $this->connect();
                foreach ($handledFiles as $file) {
                    $sftp->rm(Mage::getStoreConfig('vodafone_service/axi_stock_sftp/path') . '/' . $file['remote']);
                    $this->_writeLine('Remove remote file: "' . Mage::getStoreConfig('vodafone_service/axi_stock_sftp/path') . '/' . $file['remote'] . '"');
                }
                $sftp->close();

                /**
                 * Flush Varnish cache
                 */
                $this->_flushVarnish();

                Mage::getSingleton('core/resource')->getConnection('core_write')->exec("INSERT INTO dyna_job_runs (`job_name`, `start_date`, `end_date`) VALUES ('" . __CLASS__ . "', '" . $start . "', '" . time() . "')");
                $this->removeLock();
            }else{
                throw new Exception('Cannot start script due to existing lock: '.$this->getLockPath());
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            Zend_Debug::dump($e->getMessage());
        }
    }

    private function _handleAxiStockFile($path)
    {

        try {
            $stockStoreArray = array();
            $file = fopen($path, 'r');
            $lc = 0;
            $isFullDump = false;
            while (($line = fgetcsv($file, 0, ';')) !== false) {
                $lc++;
                if ($lc == 1) {
                    $isFullDump = ($line[0] === 'STOCK' && $line[1] === 'FULL');
                    continue;
                }
                if ($line[0] == 'END') {
                    break;
                }

                $store = $line[0];
                $sku = $line[1];
                $qty = $line[2];
                $backorderQty = $line[3];

                $changed = time();
                $this->_writeLine("Handling $sku for store $store");
                $productId = Mage::getModel("catalog/product")->getIdBySku($sku);

                if ($isFullDump) {
                    $stockStoreArray[$store][] = $productId;
                }

                if ($productId) {
                    $this->_store_ids[] = $store;
                    $this->_writeLine("Set $productId to $qty");
                    $stock = Mage::getModel('stock/storestock')->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToFilter('store_id', $store)->getLastItem();

                    // Check if stocks are changed and save into DB only if it did
                    if (!empty($stock->getId()) && $stock->getQty() == $qty && $stock->getBackorderQty() == $backorderQty) {
                        $this->_writeLine("No change $productId to $qty");
                    } else {
                        $this->_writeLine("Set $productId to $qty");
                        $stock->setProductId($productId);
                        $stock->setStoreId($store);
                        $stock->setQty($qty);
                        $stock->setBackorderQty($backorderQty);
                        $stock->setLastModified($changed);
                        $stock->save();
                    }
                }
            }
            fclose($file);

            if ($isFullDump) {
                $this->_removeDissapearingStockEntries($stockStoreArray);
            }

            return true;
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);

            return false;
        }

    }

    private function _removeDissapearingStockEntries($stockStoreArray)
    {
        // Get all SKUs known for stock store
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

        $this->_writeLine("Handling removed stocks");
        $knownStockQuery = 'SELECT store_id, product_id FROM dyna_store_stock';
        $knownStock = $read->fetchAll($knownStockQuery);
        foreach ($knownStock as $stockEntry) {
            $productId = $stockEntry['product_id'];
            $storeId = $stockEntry['store_id'];

            if (!array_key_exists($storeId, $stockStoreArray) || !in_array($productId, $stockStoreArray[$storeId])) {
                // Remove stores and SKUs that are no longer in full dump
                $this->_writeLine("Removing $productId from store $storeId");
                $write->exec("DELETE FROM dyna_store_stock WHERE product_id = '" . $productId . "' AND store_id = '" . $storeId . "'");
            }
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);

        return <<<USAGE
Usage:  php -f $file -- [options]

  help                            This help

USAGE;
    }
}

$import = new Vznl_Import_Axi_Stock_Cli();
$import->run();

