<?php
require_once 'abstract.php';

/**
 * Class Agent_Import_Cli
 */
class Agent_Import_Cli extends Mage_Shell_Abstract
{

    /**
     * Run script
     *
     * @return void
     */
    public function run()
    {
        $file = $this->getArg('file');
        $path = $this->getImportDir($file);
        if (!$file || !file_exists($path)) {
            $this->_writeLine('Import file not found, should be in ' . $path);
            return;
        }

        try {
            /** @var Dyna_AgentDE_Model_Importer $importer */
            $importer = Mage::getSingleton('agentde/Importer');
            $importer->setPathToFile($path);

            // Display CLI header
            $this->_writeLine('Importing agent list ' . $path);
            // log header
            $importer->logHeader();
            // import data
            $importer->import($path, Mage::getModel('agent/mapper_agent'));
            // log header
            $importer->logFooter();
            // Display CLI footer
            $this->_writeLine('Finished importing Agents ' . $path);
        } catch (Exception $e) {
            $fh = fopen('php://stderr', 'w');
            fputs($fh, $e->__toString());
            fclose($fh);
            exit(255);
        }
    }

    /**
     * Return path for import dir (if not exist it will create it)
     * @return string
     */
    private function getImportDir($filePath)
    {
        $relativePath = DS . trim($filePath, DS);
        $importDir = Mage::getBaseDir() . $relativePath;

        return $importDir;
    }

    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php file -- [options]

  help							This help
  file							Filename to import

USAGE;
    }
}

$shell = new Agent_Import_Cli();
$shell->run();
