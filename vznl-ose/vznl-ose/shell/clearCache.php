<?php

require_once 'abstract.php';

class Clear_Cache extends Mage_Shell_Abstract
{
    public function run()
    {
        Mage::dispatchEvent('adminhtml_cache_flush_all_caches');
        try {
            // Flush system
            Mage::app()->cleanCache();
            Mage::app()->getCacheInstance()->getFrontend()->clean();

            // Flush js/css
            Mage::getModel('core/design_package')->cleanMergedJsCss();
            $localBase = rtrim(Mage::getBaseDir(), DS);
            $mediaCssJsFolders = ['css','js','css_secure'];
            foreach($mediaCssJsFolders as $mediaCssJsFolder){
                $mediaCssJsFolder = $localBase . DS . 'media' . DS . $mediaCssJsFolder;
                exec("chmod 777 -R $mediaCssJsFolder");
            }

        } catch(Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        $dataClear = implode(" ", $this->dataToBeCleared);

        return <<<USAGE
        
    Usage:  \e[1;32mphp clearCache.php \e[0m
    
    No arguments are needed. Script will clear all Omnius caches

USAGE;
    }
}


$shell = new Clear_Cache();
$shell->run();
