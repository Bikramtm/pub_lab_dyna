<?php
ini_set('memory_limit', '2048M');
/***********************
 * Export Script to run Export profile
 * from command line or cron. Cleans entries from dataflow_batch_export table
 ***********************/

$mageapp  = realpath(dirname(__FILE__)).'/../app/Mage.php';       // Mage app location
$logfile  = 'export_data.log';      // Export log file
$env = shell_exec('readlink -f ../media | cut -d/ -f5 | cut -d"_" -f1');

/* uncomment following block when moved to server - to ensure this page is
 * not accessed from anywhere else
 */

/* System -> Import/Export -> Profiles get profile ID from
 * Magento Import/Export Profiles
 */
$profiles = array(
    '0' => 'default',
    '2' => 'telesales', // store_id => profile name
    '3' => 'belcompany',
    '4' => 'indirect',  
    '5' => 'vodafone_webshop',  
    '6' => 'belcompany_webshop',
    '12' => 'friendsfamily', 
    '13' => 'mobile_move'
);

/* Post run housekeeping table bloat removal
 * exports use "dataflow_batch_export" table
 */
$table = 'dataflow_batch_export';

/* Initialize profile to be run as Magento Admin and log start of export */

require_once $mageapp;
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$profileModel = Mage::getModel('dataflow/profile');

if (! count($profiles)) {
    Mage::log('There are no export $profiles defined in the export_producs.php file.', null, $logfile);
    echo "There are no export $profiles defined in the export_producs.php file.\n";
    die;
}

foreach ($profiles as $store_id => $profile_name) {

    $profileDb = $profileModel->getCollection()
        ->addFieldToFilter('name', $profile_name)
        ->getFirstItem();

    if (! $profileDb->getId()) { // Create the profile if it does not exist
        $data = array(
            'name' => $profile_name,
            'actions_xml' => '<action type="catalog/convert_adapter_product" method="load">
    <var name="store"><![CDATA['. $store_id .']]></var>
</action>
<action type="catalog/convert_parser_product" method="unparse">
    <var name="store"><![CDATA['. $store_id .']]></var>
    <var name="url_field"><![CDATA[0]]></var>
</action>
<action type="dataflow/convert_mapper_column" method="map">
</action>
<action type="dataflow/convert_parser_csv" method="unparse">
    <var name="delimiter"><![CDATA[;]]></var>
    <var name="enclose"><![CDATA["]]></var>
    <var name="fieldnames">true</var>
</action>
<action type="dataflow/convert_adapter_io" method="save">
    <var name="type">file</var>
    <var name="path">var/export/mail</var>
    <var name="filename"><![CDATA[export_all_products_'. $store_id .'.csv]]></var>
</action>');

        if (isset($data)) {
            $profileModel = Mage::getModel('dataflow/profile');
            $profileModel->addData($data);
        }
        try {
            $profileModel->save();
        } catch (Exception $e){
            Mage::log($e->getMessage(), null, $logfile);
            echo $e->getMessage() . "\n";
            die;
        }

        $profileIds[$store_id] = $profileModel->getId();
        Mage::log('Created ProfileID: ' . $profileModel->getId(), null, $logfile);
    } else {
        $profileIds[$store_id] = $profileDb->getId();
    }

}

// Make sure the we have identified all the profiles IDs
if (count($profileIds) != count($profiles)) {
    Mage::log('Could not find all the profile IDs', null, $logfile);
    echo 'Could not find all the profile IDs';
    die;
}

$userModel = Mage::getModel('admin/user');
$userModel->setUserId(0);
Mage::getSingleton('admin/session')->setUser($userModel);
$writeAdapter = Mage::getSingleton('core/resource')->getConnection('core_write');
$table = Mage::getSingleton('core/resource')->getTableName($table);
$failedEmail = false;

foreach ($profileIds as $store_id => $profileId) {
    $profileModel = Mage::getModel('dataflow/profile');
    $profileModel->load($profileId);

    if (!$profileModel->getId()) {
        Mage::log('ERROR: Incorrect profile id', null, $logfile);
        echo "ERROR: Incorrect profile id\n";
    }
    $actionsXml = $profileModel->getActionsXml();
    $actionsXml = preg_replace('/\>(var\/export\/mail)(.*)?\</', '>${1}/' . date('d-m-Y') . '<', $actionsXml);
    $profileModel->setActionsXml($actionsXml);

    Mage::log('Export ProfileID: ' . $profileId . ' Started.', null, $logfile);

    Mage::register('current_convert_profile', $profileModel);
    $profileModel->run();

    $batchModel = Mage::getSingleton('dataflow/batch');

    Mage::log('Export ProfileID: ' . $profileId . ' Complete. BatchID: ' . $batchModel->getId(), null, $logfile);
    echo "Export Complete. ProfileID: " . $profileId . "\n";
    Mage::unregister('current_convert_profile');

    sleep(10);

    /* Truncate dataflow_batch_export table for housecleaning */
    $querystring = "TRUNCATE " . $table;
    $writeAdapter->query($querystring);

    sleep(5);

    // Send emails
    $mail = Mage::getModel('communication/smtpemail');
    $mail->setToEmail(array('U-BUY_Productconfiguration@vodafoneziggo.com'));
    $mail->setFromEmail('no-reply@ubuy.vodafone.com');
    $mail->setSubject('Productexport ' .  date('d-m-Y') . ' ' . $profiles[$store_id] . ' ' . str_replace(' ', '_', $profileModel->getName()) . ' ' . $env);
    $mail->setBody('');
    $csvFile = realpath(dirname(__FILE__)) . '/../var/export/mail/' .  date('d-m-Y') . '/export_all_products_' . $store_id . '.csv';
    if (file_exists($csvFile)) {
        $compressed = gzcompressfile($csvFile);

        if ($compressed) {
            $gz = file_get_contents($compressed);
            $at = new Zend_Mime_Part($gz);
            $at->type = 'application/x-gzip';
            $at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
            $at->filename = 'export_all_products_' . $store_id . '_' . str_replace(' ', '_', $profileModel->getName()) . '_' . $env . '.csv.gz';
            $at->encoding = Zend_Mime::ENCODING_BASE64;
            $mail->setAttachments($at);
        }
    }

    try {
        $mail->send();
        echo "Email sent for ProfileID: " . $profileId . "\n";
    } catch (Exception $e) {
        Mage::helper('communication/smtp')->log($e->getMessage());
        $failedEmail = true;
    }

}

if (! $failedEmail) {
    // delete files from server
    recursiveRemoveDirectory(realpath(dirname(__FILE__)) . '/../var/export/mail/' .  date('d-m-Y'));
}


function gzcompressfile($source,$level=false){
    $dest=$source.'.gz';
    $mode='wb'.$level;
    $error=false;
    if($fp_out=gzopen($dest,$mode)){
        if($fp_in=fopen($source,'rb')){
            while(!feof($fp_in))
                gzwrite($fp_out,fread($fp_in,1024*512));
            fclose($fp_in);
        }
        else $error=true;
        gzclose($fp_out);
    }
    else $error=true;
    if($error) return false;
    else return $dest;
}

function recursiveRemoveDirectory($directory)
{
    foreach(glob("{$directory}/*") as $file)
    {
        if(is_dir($file)) {
            recursiveRemoveDirectory($file);
        } else {
            unlink($file);
        }
    }
    rmdir($directory);
}