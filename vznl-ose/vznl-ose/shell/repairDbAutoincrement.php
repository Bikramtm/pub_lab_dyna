<?php
require_once 'repairDbClass.php';

/*
 * Increases the PK-field in case the AI-sequence becomes too big
 * */
class Tools_Db_Repair_Autoincrement extends Tools_Db_Repair
{
    const LIMITS         = [
        'tinyint-signed'     => 127,
        'tinyint-unsigned'   => 255,
        'smallint-signed'    => 32767,
        'smallint-unsigned'  => 65535,
        'mediumint-signed'   => 8388607,
        'mediumint-unsigned' => 16777215,
        'int-signed'         => 2147483647,
        'int-unsigned'       => 4294967295,
        'bigint-signed'      => 9223372036854775807,
        'bigint-unsigned'    => 18446744073709551615
    ];
    const PERCENT        = 75; // (75%) to be updated
    const LOG_TEXT       = "PK %s(table: %s) it's reaching the limit: %s of %s(%s%%)";
    const SQL_SHOW       = "SHOW CREATE TABLE `%s`;";
    const SQL_UPDATE     = "ALTER TABLE `%s` CHANGE COLUMN `%s` `%s` BIGINT(10) UNSIGNED NOT NULL;";
    const SQL_DROP       = "ALTER TABLE `%s` DROP FOREIGN KEY `%s`;";
    const SQL_ADD        = "ALTER TABLE `%s` ADD `%s`;";
    const SQL_TABLES     = "SHOW TABLE STATUS WHERE `Engine` IN ('InnoDB', 'MyISAM') AND `Auto_increment` > 0;";
    const SQL_TABLE_INFO = "SHOW FULL COLUMNS FROM `%s` WHERE `Key` = 'PRI';";
    const SQL_FOREIGN    = "SELECT TABLE_NAME, COLUMN_NAME, CONSTRAINT_NAME 
                            FROM information_schema.KEY_COLUMN_USAGE 
                            WHERE REFERENCED_TABLE_NAME = '%s' AND 
                            REFERENCED_COLUMN_NAME = '%s' AND 
                            TABLE_SCHEMA = '%s';";

    protected $tableResults;

    /**
     * Tools_Db_Repair_Autoincrement constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->logFile      = 'repairdb_fix_autoincrement_'.date('Y_m_d').'.log';
        $this->tableResults = $this->readConnection->fetchAll(self::SQL_TABLES);
    }

    /**
     * Run script
     */
    public function run()
    {
        $this->log("Start script.");
        $requiredActions = array();

        // parse each table having a column with autoincrement
        foreach ($this->tableResults as $table) {
            // get primary column name and data type
            $primaryCol = $this->getPrimaryCols($table);
            $columnType = $this->getPrimaryType($primaryCol['Type']);

            if ($columnType != 'bigint-unsigned') {
                // it's the column near the limit?
                $percent = $this->getPercent($table['Auto_increment'], self::LIMITS[$columnType]);

                if ($percent > self::PERCENT) {
                    $actions           = [];
                    $actions_end       = [];
                    $actions_start     = [];
                    $foreignKeys       = $this->searchForeignKeys($table['Name'], $primaryCol['Field']);
                    $requiredActions[] = $this->log(sprintf(self::LOG_TEXT, $primaryCol['Field'], $table['Name'], $table['Auto_increment'], self::LIMITS[$columnType], $percent));

                    // if used as FK, drop the key, update record and create the key back
                    if ($foreignKeys) {
                        foreach ($foreignKeys as $foreignKey) {
                            $createQuery     = $this->getSqlCreateQuery($foreignKey['TABLE_NAME']);
                            $constraintQuery = $this->getConstraintQuery($createQuery, $foreignKey['CONSTRAINT_NAME']);

                            if ($constraintQuery) {
                                $actions_start[] = sprintf(self::SQL_DROP, $foreignKey['TABLE_NAME'], $foreignKey['CONSTRAINT_NAME']);
                                $actions_start[] = sprintf(self::SQL_UPDATE, $foreignKey['TABLE_NAME'], $foreignKey['COLUMN_NAME'], $foreignKey['COLUMN_NAME']);
                                $actions_end[]   = sprintf(self::SQL_ADD, $foreignKey['TABLE_NAME'], $constraintQuery);
                            }
                        }
                    }

                    // add to alter - parent table
                    $actions = array_merge(
                        $actions_start,
                        array(sprintf(self::SQL_UPDATE, $table['Name'], $primaryCol['Field'], $primaryCol['Field']). " AUTO_INCREMENT COMMENT '{$primaryCol['Comment']}'"),
                        $actions_end
                    );

                    // Run queries only if it's not readonly mode
                    if ($this->getReadOnly() == false) {
                        foreach ($actions as $action) {
                            $this->log("Running: ".$action);
                            $this->writeConnection->exec($action);
                        }
                    }
                }
            }
        }

        $this->log("Stop script.");

        // if in readonly mode, stop here and return actions required
        if ($this->getReadOnly()) {
            return $requiredActions;
        }

        return $this;
    }

    /**
     * Get all tables having primary keys with autoincrement
     * @param $table
     * @return bool
     */
    private function getPrimaryCols($table)
    {
        return $this->readConnection->fetchRow(sprintf(self::SQL_TABLE_INFO, $table['Name']));
    }

    /**
     * Return column type
     * @param $type
     * @return string
     */
    private function getPrimaryType($type)
    {
        $type = str_replace("(", " ", strtolower($type));
        $type = explode(" ", str_replace(")", "", $type));

        return (isset($type[2])) ? $type[0]."-".$type[2] : $type[0]."-signed";
    }

    /**
     * Determine percent
     * @param $autoIncrement
     * @param $maxValue
     * @return string
     */
    private function getPercent($autoIncrement, $maxValue)
    {
        return number_format((($autoIncrement/$maxValue) * 100), 2);
    }

    /**
     * Search if column is used as foreign key
     * @param $tableName
     * @param $columnName
     * @return mixed
     */
    private function searchForeignKeys($tableName, $columnName)
    {
        return $this->readConnection->fetchAll(sprintf(self::SQL_FOREIGN, $tableName, $columnName, $this->dbName));
    }

    /**
     * Get sql creation query
     * @param $table
     * @return mixed
     */
    private function getSqlCreateQuery($table)
    {
        $res = $this->readConnection->fetchRow(sprintf(self::SQL_SHOW, $table));
        $res = array_values($res);
        return $res[1];
    }

    /**
     * Get constraint query
     * @param $createQuery
     * @return string
     */
    private function getConstraintQuery($createQuery, $fk)
    {
        $regExp  = '#\s+CONSTRAINT `([^`]*)` FOREIGN KEY \(`([^`]*)`\) '
            . 'REFERENCES (`[^`]*\.)?`([^`]*)` \(`([^`]*)`\)'
            . '( ON DELETE (RESTRICT|CASCADE|SET NULL|NO ACTION))?'
            . '( ON UPDATE (RESTRICT|CASCADE|SET NULL|NO ACTION))?#';
        $matches = array();
        preg_match_all($regExp, $createQuery, $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            if (strpos(trim($match[0]), $fk) !== false) {
                return trim($match[0]);
            }
        }

        return false;
    }
}
