#!/bin/bash

SCRIPTROOT=`dirname $0`/..
REMOTEPATH=$1
LOCALPATH=$2

if [ -z "$REMOTEPATH" ] ; then
    echo "Please specify the remote path to copy" 1>&2
    echo
    echo "Example: $0 /vagrant/report report/"
    exit 1
fi


if [ -z "$LOCALPATH" ] ; then
    echo "Please specify the local path to copy" 1>&2
    echo
    echo "Example: $0 /vagrant/report report/"
    exit 1
fi

rsync -avz -e "ssh -p 2222 -i $SCRIPTROOT/.vagrant/machines/default/virtualbox/private_key" vagrant@localhost:$REMOTEPATH $SCRIPTROOT/$LOCALPATH
