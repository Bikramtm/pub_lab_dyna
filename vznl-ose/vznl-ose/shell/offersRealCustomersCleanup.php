<?php

/**
 * Shell script to delete the old offers for real customers permanently based on the number of days that have passed since the offer creation
 * (configurable admin setting)
 */

require_once 'abstract.php';

/**
 * Magento Log Shell Script
 *
 * @category    Dyna
 * @author      Dyna
 */
class Dyna_Offers_Real_Customers_Cleaner extends Mage_Shell_Abstract
{
    private $cliDebug = false;

    private function log($message)
    {
        if(!Mage::getStoreConfig('omnius_general/cleanup_settings/log_file_debug')) {
            return;
        }
        Mage::log($message, null, "cart-real-customers-cleanup.log");
    }

    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }
    /**
     * Run script
     *
     */
    public function run()
    {
        if ($noOfDaysForCleanup = Mage::getStoreConfig('omnius_general/cleanup_settings/no_of_days_real_customer_offer_alive')) {
            $date = date('Y-m-d H:i:s', strtotime('-'.$noOfDaysForCleanup.' day', strtotime(now())));

            $salesQuotesCustomerIds = Mage::getModel('sales/quote')->getCollection()
                ->addFieldToSelect('customer_id')
                ->addFieldToFilter('created_at', array('lteq' => $date))
                ->addFieldToFilter(
                    array('is_offer', 'cart_status'),
                    array(
                        array('eq' => 1),
                        array('eq' => Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED)
                    )
                );

            if(!$salesQuotesCustomerIds) {
                $msg = "No offers were found older than ". $date;
                //write cli log
                if ($this->cliDebug) {
                    $this->_writeLine($msg);
                }
                $this->log($msg);
                return;
            }

            $salesQuotesCustomerIdsArr = [];
            foreach($salesQuotesCustomerIds as $salesQuotesCustomerId){
                if($salesQuotesCustomerId->getCustomerId()) {
                    $salesQuotesCustomerIdsArr[] = $salesQuotesCustomerId->getCustomerId();
                }
            }

            if(!$salesQuotesCustomerIdsArr) {
                $msg = "No offers were found with real customers set older than ". $date;
                //write cli log
                if ($this->cliDebug) {
                    $this->_writeLine($msg);
                }
                $this->log($msg);
                return;
            }

            $realCustomersIdsWithOffers = Mage::getModel('customer/customer')->getCollection()
                ->addAttributeToFilter('is_prospect', 0)
                ->addFieldToFilter('entity_id', array('in' => $salesQuotesCustomerIdsArr));

            if(!$realCustomersIdsWithOffers) {
                $msg = "No offers for real customers were found older than ". $date;
                //write cli log
                if ($this->cliDebug) {
                    $this->_writeLine($msg);
                }
                $this->log($msg);
                return;
            }

            $realCustomersIdsWithOffersArr = [];
            foreach ($realCustomersIdsWithOffers as $realCustomer) {
                $realCustomersIdsWithOffersArr[] = $realCustomer->getId();
            }

            $salesQuotesForRealCustomers = Mage::getModel('sales/quote')->getCollection()
                ->addFieldToFilter('customer_id', array('in' => $realCustomersIdsWithOffersArr));

            $msg = "Starting deleting offers older than ".$noOfDaysForCleanup." days, meaning older than: " . $date;
            if ($this->cliDebug) {
                $this->_writeLine($msg);
            }
            $this->log($msg);

            foreach($salesQuotesForRealCustomers as $offer)
            {
                $id = $offer->getId();

                try{
                    $offer->delete();
                    $msg = "sales quote #".$id." is removed";
                    if ($this->cliDebug) {
                        $this->_writeLine($msg);
                    }
                    $this->log($msg);
                } catch(Exception $e){
                    $msg = "sales quote #".$id." could not be removed: ".$e->getMessage();
                    if ($this->cliDebug) {
                        $this->_writeLine($msg);
                    }
                    $this->log($msg);
                }
            }

            $msg = "Finished offers deletion... ";
            if ($this->cliDebug) {
                $this->_writeLine($msg);
            }
            $this->log($msg);
        }
        else {
            $msg = "The number of days should be set in Admin. Only the offers older than that number of days will be deleted";
            if ($this->cliDebug) {
                $this->_writeLine($msg);
            }
            $this->log($msg);
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php offersRealCustomersCleanup.php 
  help              This help

USAGE;
    }
}

$shell = new Dyna_Offers_Real_Customers_Cleaner();
$shell->run();