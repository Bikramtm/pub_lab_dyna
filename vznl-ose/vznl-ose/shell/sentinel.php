<?php

require_once 'abstract.php';

/**
 * Class Vznl_Job_Sentinel
 */
class Vznl_Job_Sentinel extends Mage_Shell_Abstract
{
    /** @var Vznl_Job_Helper_Console */
    protected $_helper;

    /**
     * Run script
     */
    public function run()
    {
        $watch = $this->getArg('watch');
        $stop = $this->getArg('stop');
        $start = $this->getArg('start');
        $restart = $this->getArg('restart');
        $status = $this->getArg('status');

        if ($watch) {
            $this->_watch();
        } elseif ($stop) {
            $this->_stop();
        } elseif ($start) {
            $this->_start();
        } elseif ($restart) {
            $this->_restart();
        } elseif ($status) {
            $this->_status();
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Assures that at any moment, the correct
     * number of consumers are running
     */
    protected function _watch()
    {
        $start = time();
        $this->_h()->persistConsumers();
        Mage::getSingleton('core/resource')->getConnection('core_write')->exec("INSERT INTO vznl_job_runs (`job_name`, `start_date`, `end_date`) VALUES ('" . __CLASS__ . "', '" . $start . "', '" . time() . "')");
    }

    /**
     * Stops all consumers
     *
     * @throws Exception
     */
    protected function _stop()
    {
        $this->_h()->stopConsumers();
    }

    /**
     * Restarts all consumers
     */
    protected function _restart()
    {
        $this->_h()->restartConsumers();
    }

    /**
     * Starts the consumers
     */
    protected function _start()
    {
        $this->_h()->startConsumers();
    }

    /**
     * Prints a short status of
     * the current working consumers
     */
    protected function _status()
    {
        /** @var Vznl_Job_Model_Queue $q */
        $q = Mage::helper('vznl_job')->getQueue();
        $load = sys_getloadavg();
        echo sprintf(
            PHP_EOL.'There are %s running consumers with an average load in the last minute of %s (%s). Items waiting to be processed: %s'.PHP_EOL,
            $this->_h()->getConsumerCount(),
            $load[0],
            join(', ', $load),
            $q->getWaitingItems()
        );
    }

    /**
     * @return Vznl_Job_Helper_Console
     */
    protected function _h()
    {
        if ( ! $this->_helper) {
            return $this->_helper = Mage::helper('vznl_job/console');
        }
        return $this->_helper;
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  help                                      This help

  watch                                     Assures that the correct number of consumers are running
  stop                                      Stops all consumers
  restart                                   Restarts all consumers
  start                                     Start all consumers
  status                                    Show status

USAGE;
    }
}

$sentinel = new Vznl_Job_Sentinel();
$sentinel->run();