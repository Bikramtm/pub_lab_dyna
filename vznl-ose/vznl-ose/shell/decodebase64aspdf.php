<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'abstract.php';
require_once 'stopwatch.php';

/**
 * Class Dyna_Product_Visibility
 */
class Dyna_DecodeBase64ToPDF_Cli extends Mage_Shell_Abstract
{
    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    /**
     * Run appropriate import script based on the --type param
     *
     */
    public function run()
    {
        $base64pdf =  $this->getArg('base64');
        $this->base64_to_pdf($base64pdf);
    }

    function base64_to_pdf($data)
    {
        try
        {
            $this->_writeLine('Converting base64 string');
            StopWatch::start();

            $pdf_decoded = base64_decode ($data);
            $filename = "PDF_" . date("Y-m-d-H-i-s");
            $pdf = fopen ($filename . 'pdf','w');
            fwrite ($pdf,$pdf_decoded);
            fclose ($pdf);
            $this->_writeLine(sprintf("Base64 string decoded to PDF %s", StopWatch::elapsed()));
        }
        catch (Exception $exception)
        {
            $this->_writeLine(sprintf('Base64 string could not be converted to a PDF %s', $exception->getMessage()));
        }
        finally {
            $this->_writeLine(sprintf('Time taken for convert to PDF %s', StopWatch::elapsed()));
        }
        return false;
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php file

  help						This help
  base64					base64 string to decode and save as PDF

USAGE;
    }
}

// Example
// php decodebase64aspdf.php -base64 TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2Yg

$decode = new Dyna_DecodeBase64ToPDF_Cli();
$decode->run();
