<?php

require_once 'abstract.php';

class Dyna_CacheWarmer extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        $verbose = !$this->getArg('disable-output');
        $includeHeavy = $this->getArg('include-heavy');

        try {
            /** @var Dyna_Warmer_Model_Builder $builder */
            $builder = Mage::getModel('warmer/builder');
            $builder->buildCache($verbose, $includeHeavy);
            exit(0);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            exit(1);
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  help							This help
  disable-output                Disables output
  include-heavy                 Includes heavy cache warmers (resource and memory intensive)

USAGE;
    }
}

$import = new Dyna_CacheWarmer();
$import->run();

