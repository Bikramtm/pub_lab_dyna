<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'abstract.php';

class Dyna_Opcache_CLI extends Mage_Shell_Abstract
{
    public function run()
    {
        $this->clearOpcache();
    }

    function clearOpcache()
    {
       $result = opcache_reset();
        if ($result) {
            echo "Opcache was reset!";
        } else {
            echo "Could not reset Opcache";
        }
        return $result;
    }
}

// Example
//php flushopcache.php

$clearcache = new Dyna_Opcache_CLI();
$clearcache->run();