<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'catalogtables.php';

class Dyna_CatalogExport_Cli extends Dyna_CatalogTables_Cli
{
    private $hostname = null;
    private $username = null;
    private $password = null;
    private $databaseName = null;
    private $exportmultimapper = true;
    private $temporaryDirectory = "/tmp"; #Temporary linux folder
    private $destinationDirectory = "~/"; #Home directory of current user

    public function run()
    {
        $type = $this->getArg('nomm');
        $tempFolder = $this->getArg('temp');
        $destinationFolder = $this->getArg('destination');

        if ($tempFolder) {
            $this->temporaryDirectory = $tempFolder;
        }

        if ($destinationFolder) {
            $this->destinationDirectory = $destinationFolder;
        }

        if ($type) {
            $this->exportmultimapper = false;
        }

        if (empty(parent::CATALOGTABLES)) {
            $this->_writeLine('The catalogTables constant is empty, cannot continue the script');
            exit;
        }

        if ($this->exportmultimapper) {
            if (empty(parent::MULTIMAPPERTABLES)) {
                $this->_writeLine('The multimapperTables constant is empty, cannot continue the script');
                exit;
            }
        }

        $config = Mage::getConfig()->getResourceConnectionConfig("default_setup");

        parent::_writeLine('Connecting to database');

        $databaseInfo = array(
            "host" => $config->host,
            "user" => $config->username,
            "pass" => $config->password,
            "dbname" => $config->dbname
        );

        $hostname = $databaseInfo["host"];
        $username = $databaseInfo["user"];
        $password = $databaseInfo["pass"];
        $databaseName = $databaseInfo["dbname"];

        if ($databaseName == "magento") {
            parent::_writeLine('Database could not be found, cannot continue script');
            exit;
        }

        $tablesToExport = $this->exportmultimapper ? parent::getFullCatalog() : parent::CATALOGTABLES;

        if ($this->exportmultimapper) {
            parent::_writeLine('Starting export of catalog from ' . $databaseName);
        } else {
            parent::_writeLine('Starting export of catalog WITHOUT multimapper from ' . $databaseName);
        }
        $this->export_tables($hostname, $username, $password, $databaseName, $tablesToExport);
    }

    function export_tables($host, $user, $pass, $name, $tables = false, $backup_name = false)
    {
        $archiveFileName = ($backup_name ? $backup_name : "catalogexport") . '_' . date("Y-m-d-H-i-s") . ($this->exportmultimapper ? '' : "_nomultimapper") . '_' . $host . '_' . $name . '.tar.gz';
        $databaseFilename = "database_" . date("Ymd-His") . ($this->exportmultimapper ? '' : "_nomultimapper") . ".gz";
        $tableNames = "";

        if ($tables) {
            foreach ($tables as $exportTable) {
                if (parent::tableExist($exportTable)) {
                    $tableNames .= $exportTable . " ";
                }
            }
        }

        if ($onlyExportTables = strlen($tableNames) > 0) {
            $command = "mysqldump -h$host -u$user --default-character-set=utf8  -p'$pass' $name $tableNames| gzip > $this->temporaryDirectory" . DIRECTORY_SEPARATOR . "$databaseFilename";
        } else {
            $command = "mysqldump -h$host -u$user --default-character-set=utf8  -p'$pass' $name | gzip > $this->temporaryDirectory" . DIRECTORY_SEPARATOR . "$databaseFilename";
        }

        system($command, $returnvalue);

        if ($returnvalue == 0) {
            $archive[0] = $this->temporaryDirectory . DIRECTORY_SEPARATOR . "$databaseFilename";
            parent::_writeLine(($onlyExportTables ? "Only the selected" : "All") . " tables exported ");

            $cableArtifactDestination = $this->temporaryDirectory . DIRECTORY_SEPARATOR . "cable-rules-plugin.phar";
            if ($this->copyCableArtifact($cableArtifactDestination)) {
                $archive[] = $cableArtifactDestination;
                parent::_writeLine("Cable Artifact copied and will also be added to the archive file");
            }

            $catalogVersionDestination = $this->temporaryDirectory . DIRECTORY_SEPARATOR . "APP_VERSION.php";
            if ($this->copyCatalogVersion($catalogVersionDestination)) {
                $archive[] = $catalogVersionDestination;
                parent::_writeLine("Catalog version copied and will also be added to the archive file");
            }

            $this->createCatalogArchive($archive, $archiveFileName);
        } else {
            parent::_writeLine("Could not create backup -> command failed");
        }
    }

    public function createCatalogArchive($archive, $archiveFileName)
    {
        $toBeTarredFiles = "";
        foreach ($archive as $currentFile)
        {
            $toBeTarredFiles .= $currentFile . " ";
        }

        $command = "tar -czf $this->destinationDirectory" . DIRECTORY_SEPARATOR . "$archiveFileName $toBeTarredFiles";
        system($command, $returnvalue);

        if ($returnvalue == 0)
        {
            foreach ($archive as $currentFile)
            {
                $command = "rm $currentFile";
                system($command, $returnvalue);
            }
            parent::_writeLine("Catalog archive created" . ($this->exportmultimapper ? '' : ' without multimapper !'));
        }
        else
        {
            parent::_writeLine("Catalog archive could not be created !!!");
        }
    }

    public function copyCatalogVersion($destination)
    {
        $result = false;
        $catalogVersionFile = Mage::getBaseDir('media') . DIRECTORY_SEPARATOR . "APP_VERSION.php";

        if (file_exists($catalogVersionFile))
        {
            copy($catalogVersionFile, $destination);
            return true;
        }
        return $result;
    }

    public function copyCableArtifact($destination)
    {
        $result = false;
        $cableArtifactFile = Mage::getBaseDir('lib') . DIRECTORY_SEPARATOR . "cable-rules-plugin.phar";

        if (file_exists($cableArtifactFile))
        {
            copy($cableArtifactFile, $destination);
            return true;
        }
        return $result;
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php file
  help				This help
  nomm              Export catalog without the multimapper
  destination       Export fullbackup to specified destination          (Default: /~  [USER HOME DIRECTORY])
  temp              Use specified temporary directory for temp files    (Default: /tmp)

  Example: php exportcatalogfromdatabase.php destination /~/SIT temp /~/tmp     [Exports with multimapper]
  Example: php exportcatalogfromdatabase.php nomm destination /~/SIT temp /~/tmp     [Exports without multimapper]
USAGE;
    }
}

// Example
//php exportcatalogfromdatabase.php

$import = new Dyna_CatalogExport_Cli();
$import->run();
