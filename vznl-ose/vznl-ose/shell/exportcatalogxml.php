<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'abstract.php';

class Dyna_Export_Catalog_xml extends Mage_Shell_Abstract
{
    protected $field_array = [];
    protected $_exportFile ='';
    protected $_xsdFile ='';
    protected $_sequence ='';
    protected $_removeTimestamp = false;

    protected $_generateXml = false;
    protected $_generateCsv = true;

    protected $_currentAttributeSet ='';
    protected $_currentAttributesArray = [];
    protected $_missingAttributesArray = [];

    protected $_valueAttributesArray = [];
    protected $_multiValueAttributesArray = [];
    protected $_booleanValueAttributesArray = [];
    protected $_numericValueAttributesArray = [];
    protected $_decimalValueAttributesArray = [];
    protected $_taxValueAttributesArray = [];
    protected $_textAttributesArray = [];

    protected $_csvDelimiter = ';';

    protected $_logFileName = "catalog_report_export";
    protected $_logFileExtension = "log";

    public function __construct()
    {
        parent::__construct();
        $this->setMissingAttributes();
        $this->setOptionAttributes();
        $this->setTextAttributes();
        $this->setDecimalAttributes();
    }
 
    /**
     * Run the export script
     *
     */
    public function run()
    {
        if ($this->getArg('xml')) {
            $this->_generateXml = true;
        }

        $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection');
        $attributeSetCollection->setEntityTypeFilter('4'); // 4 is Catalog Product Entity Type ID
        foreach ($attributeSetCollection as $attributeSet) {
            $this->_currentAttributeSet = $attributeSet->getAttributeSetName();
            $this->exportset();
        }
    }

    /**
     * run the export for an attrbute set
     */
    public function exportset()
    {
        try {
            $this->setMissingAttributes();
            $this->setOptionAttributes();

            switch ($this->_currentAttributeSet) {
                case 'KD_Cable_Products':
                    $this->_exportFile = 'KD_Cable_Products';
                    $this->_xsdFile = 'Cable.xsd';
                    $this->removeFromArray($this->_taxValueAttributesArray, 'tax_class_id');
                    break;
                case 'Mobile_Additional_Service':
                    $this->_exportFile = 'Mobile_Additional_Service';
                    $this->_xsdFile = 'CAT-DataSet_Mobile_Mobile_Additional_Service.xsd';
                    break;
                case 'Mobile_Data_Tariff':
                    $this->_exportFile = 'Mobile_Data_Tariff';
                    $this->_xsdFile = 'CAT-DataSet_Mobile_Mobile_Data_Tariff.xsd';
                    break;
                case 'Mobile_Device':
                    $this->_exportFile = 'Mobile_Device';
                    $this->_xsdFile = 'CAT-DataSet_Mobile_Mobile_Device.xsd';
                    unset($this->_missingAttributesArray['vat']);
                    $this->removeFromArray($this->_taxValueAttributesArray, 'tax_class_id');
                    break;
                case 'Mobile_Footnote':
                    $this->_exportFile = 'Mobile_Footnote';
                    $this->_xsdFile = 'CAT-DataSet_Mobile_Mobile_Footnote.xsd';
                    unset($this->_missingAttributesArray['vat']);
                    $this->removeFromArray($this->_taxValueAttributesArray, 'tax_class_id');
                    break;
                case 'Mobile_Prepaid_Tariff':
                    $this->_exportFile = 'Mobile_Prepaid_Tariff';
                    $this->_xsdFile = 'CAT-DataSet_Mobile_Mobile_Prepaid_Tariff.xsd';
                    $this->removeFromArray($this->_taxValueAttributesArray, 'tax_class_id');
                    break;
                case 'Mobile_Voice_Data_Tariff':
                    $this->_exportFile = 'Mobile_Voice_Data_Tariff';
                    $this->_xsdFile = 'CAT-DataSet_Mobile_Mobile_Voice_Data_Tariff.xsd';
                    $this->removeFromArray($this->_taxValueAttributesArray, 'tax_class_id');
                    break;
                case 'FN_Accessory':
                    $this->_exportFile = 'FN_Accessory';
                    $this->_xsdFile = 'CAT-DataSet_FN_Accessory.xsd';
                    break;
                case 'FN_Hardware':
                    $this->_exportFile = 'FN_Hardware';
                    $this->_xsdFile = 'CAT-DataSet_FN_Hardware.xsd';
                    break;
                case 'FN_Options':
                    $this->_exportFile = 'FN_Options';
                    $this->_xsdFile = 'CAT-DataSet_FN_Options.xsd';
                    break;
                case 'FN_Promotion':
                    $this->_exportFile = 'FN_Promotion';
                    $this->_xsdFile = 'CAT-DataSet_FN_Promotion.xsd';
                    $this->removeFromArray($this->_taxValueAttributesArray, 'tax_class_id');
                    $this->_taxValueAttributesArray[] = 'vat';
                    break;
                case 'FN_SalesPackage':
                    $this->_exportFile = 'FN_SalesPackage';
                    $this->_xsdFile = 'CAT-DataSet_FN_SalesPackage.xsd';
                    $this->removeFromArray($this->_taxValueAttributesArray, 'tax_class_id');
                    $this->_taxValueAttributesArray[] = 'vat';
                    break;
                default:
                    return 2;
                    break;
            }
        } catch (Exception $e) {
            $this->_hasError = true;
            fwrite(STDERR, 'Skipped attribute set '.$this->_currentAttributeSet.' because of '.$e->getMessage());
        }
        $this->getCurrentAttributes();
        $this->generatexml();
    }

    /**
     * generating the xml for the attribute set
     */
    public function generatexml()
    {
        $attributeSetId = Mage::getModel('eav/entity_attribute_set')
            ->load($this->_currentAttributeSet, 'attribute_set_name')
            ->getAttributeSetId();

        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', $attributeSetId);

        $header_data = array();
        $csv_data = array();
        $output_csv_data = '';

        $padd = '    '; //4 spaces for indentation
        $eol = "\n"; //end of line
        $xml = '<?xml version="1.0" encoding="UTF-8"?>'.$eol;
        $xml .= '<products xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="'.$this->_xsdFile.'">'.$eol;
        $cnt = 1;
        if ($collection) {
            /** @var Dyna_Catalog_Model_Product $product */
            foreach ($collection as $product) {
                $product_data = array();
                $xml .= $padd.'<product>'.$eol;
                foreach($this->_currentAttributesArray as $field){
                    if($cnt == 1){
                        $header_data[] = $field['definition'];
                    }

                    $fieldCodeDb = $field['code_db'];
                    if (array_key_exists($field['code'], $this->_missingAttributesArray) && in_array($field['code'], $this->_taxValueAttributesArray)) {
                        $fieldCodeDb = $field['code'];
                    }

                    $attribute_field_type = $this->getAttributeFieldType($fieldCodeDb);
                    switch ($attribute_field_type) {
                        case 'option':
                            $field_data = $product->getAttributeText($field['code_db']);
                            break;
                        case 'multi':
                            $field_data_array = [];
                            $optionIds  = explode(',', $product->getData($field['code_db']));
                            foreach ($optionIds as $optionId) {
                                $field_data_array[] = $product->getResource()
                                    ->getAttribute($field['code_db'])
                                    ->getSource()
                                    ->getOptionText($optionId);
                            }
                            $field_data = count($field_data_array)>1 ? implode(',', $field_data_array) : $field_data_array[0];
                            break;
                        case 'boolean':
                            $field_val = $product->getAttributeText($field['code_db']);
                            $field_data = ($field_val && in_array(strtolower($field_val), ['true', 'yes', 'enabled'])) ? 1 : 0;
                            break;
                        case 'text':
                            $field_val = $product->getData($field['code_db']);
                            $field_data_csv = iconv(mb_detect_encoding($field_val, mb_detect_order(), true), "UTF-8", $field_val);
                            $field_data = '<![CDATA[' . $field_data_csv . ']]>';
                            break;
                        case 'numeric':
                            $field_data = (int) $product->getData($field['code_db']);
                            break;
                        case 'decimal':
                            $decimalValue = !empty($product->getData($field['code_db'])) ? $product->getData($field['code_db']) : 0;
                            $field_data = number_format($decimalValue, 4, '.', '');
                            break;
                        case 'tax':
                            $store = Mage::app()->getStore();
                            $taxCalculation = Mage::getSingleton('tax/calculation')
                                ->getRateRequest(null, null, null, $store);
                            $taxClassId = $product->getData($field['code_db']);
                            $field_data = (int) Mage::getSingleton('tax/calculation')
                                ->getRate($taxCalculation->setProductClassId($taxClassId));
                            break;
                        default:
                            $field_data = $product->getData($field['code_db']);
                            break;
                    }

                    $product_data[] = isset($field_data_csv) ? $field_data_csv : $field_data;
                    unset($field_data_csv);
                    $xml .= str_repeat($padd, 2).'<'.$field['definition'].'>'.$field_data.'</'.$field['definition'].'>'.$eol;
                }
                $csv_data[] = $product_data;
                $xml .= $padd.'</product>'.$eol;
                $cnt++;
            }
        }
        $xml .= '</products>'.$eol;

        $this->deleteOldFile();

        $timestamp = $this->_removeTimestamp ?: date('Y-m-d') . "_" . time();
        $pathToFile = $this->getExportDir(). DS . $this->_exportFile . $timestamp . '.xml';
        $pathToCsvFile = $this->getExportDir(). DS . $this->_exportFile . $timestamp . '.csv';
        $exportedxml = false;
        try {
            //save export file
            $xml = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $xml);
            !$this->_generateXml ?: $exportedxml = file_put_contents($pathToFile, $xml);

            if ($this->_generateCsv) {
                $csvFile = fopen($pathToCsvFile, 'w');
                fputcsv($csvFile, $header_data, $this->_csvDelimiter);
                foreach ($csv_data as $csv_row) {
                    fputcsv($csvFile, $csv_row, $this->_csvDelimiter);
                }
                fclose($csvFile);
                $exportedcsv = true;
            } else {
                $exportedcsv = false;
            }
        } catch (Exception $e) {
            $this->_writeLine("ERR: error writing , ". $e->getMessage());
        }

        !$this->_generateXml ?: $this->_writeLine('Export of "'.$this->_exportFile . $timestamp .'.xml" file was: ' . ($exportedxml ? 'Successful (find the exported file in var/export)' : 'Unsuccessful' ));
        !$this->_generateCsv ?: $this->_writeLine('Export of "'.$this->_exportFile . $timestamp .'.csv" file was: ' . ($exportedcsv ? 'Successful (find the exported file in var/export)' : 'Unsuccessful' ));
    }


    public function array_to_xml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            if( is_array($value) ) {
                $subnode = $xml_data->addChild($key);
                $this->array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
         }
    }


    public function validateXml($pathToFile){
        $schemaPath .= DIRECTORY_SEPARATOR . $this->validationSchemas[$type];
        if (!file_exists($schemaPath)) {
            $this->_writeLine('Validation schema file not found, should be in ' . $schemaPath);
            return false;
        }

        libxml_use_internal_errors(true);
        $document = new DOMDocument();
        $document->load($pathToFile);
        if (!$document->schemaValidate($schemaPath)) {
            $this->_hasError = true;
            $xmlErrors = libxml_get_errors();

            foreach ($xmlErrors as $xmlError) {
                $this->_writeLine($this->libxmlDisplayError($xmlError));
            }
            $this->_writeLine('The provided XML is not valid according to schema');
            libxml_clear_errors();
            return false;
        }
    }


    /**
     * function to get all the attributes of the current attribute set
     */
    public function getCurrentAttributes(){

        $this->_currentAttributesArray = [];
        $pathToxsdFile = $this->getXsdDir(). DS . $this->_xsdFile;
        if (!file_exists($pathToxsdFile)) {
            $this->_writeLine('Validation schema file not found, should be in ' . $pathToxsdFile);
            return false;
        }

        $pathToconvertedxmlFile = $this->getExportDir(). DS . $this->_currentAttributeSet.'.xml';

        $XSDDOC = new DOMDocument(); 
        $XSDDOC->preserveWhiteSpace = false;
        $XSDDOC->load($pathToxsdFile);
        $XSDDOC->save($pathToconvertedxmlFile);
        $myxmlfile = file_get_contents($pathToconvertedxmlFile);
        $parseObj = str_replace($XSDDOC->lastChild->prefix.':',"",$myxmlfile);
        $xml_string = simplexml_load_string($parseObj);
        $json  = json_encode($xml_string->element->complexType->sequence->element->complexType->sequence);
        $data = json_decode($json, true);

        foreach($data['element'] as $field){
            $field_node_definition = $field['@attributes']['name'];
            $field_type = $field['@attributes']['type'];
            $field_code = $this->camelCaseToUs($field['@attributes']['name']);
            $field_code_db = $this->replaceMissingAttributes($field_code);
            $attr = Mage::getResourceModel('catalog/eav_attribute')->loadByCode('catalog_product',$field_code_db);

            if($attr->getId()){
                $field_data_array = array('definition' => $field_node_definition, 'code' => $field_code, 'code_db' => $field_code_db, 'type' => $field_type);
                $this->_currentAttributesArray[] = $field_data_array;
            } else {
                $this->_writeLine($field_code." attribute could not be found for the attribute set ".$this->_currentAttributeSet);
            }
        }
        array_map('unlink', glob($pathToconvertedxmlFile));
    }

    /**
     * Replacing the attribute name in the xsd with the actual database fields
     * @param $field_code
     * @return $field_code
     */
    public function replaceMissingAttributes($field_code){
        if(array_key_exists($field_code, $this->_missingAttributesArray)){
            $field_code = $this->_missingAttributesArray[$field_code];
        } 
        return $field_code;
    }

    /**
     * Defining the Attributes with different name in the XSD
     */
    public function setMissingAttributes(){
        $this->_missingAttributesArray = array(
            'display_name_basket' => 'display_name_cart',
            'display_name_overview' => 'display_name',
            'display_name_printout' => 'display_name_communication',
            'package_sub_type_id' => 'package_subtype',
            'package_subtype_id' => 'package_subtype',
            'package_type_id' => 'package_type',
            'product_family_id' => 'product_family',
            'product_version_id' => 'product_version',
            'allow_for_unsubsidized_sales' => 'allowed_unsubsidized_sales',
            'position' => 'product_position',
            'vat' => 'tax_class_id'
        );
    }


    /**
     * Defining the Attributes for which option values are required
     */
    public function setOptionAttributes(){
        $this->_valueAttributesArray = array(
            'lifecycle_status',
            'package_subtype',
            'visibility',
            'tax_code',
            'market_code',
            'redplus_role',
            'type',
            'service_category',
            'contract_code',
            'product_segment',
            'price_billing_frequency',
            'tariff_change_action',
            'premium_class',
            'service_item_type',
        );

        $this->_multiValueAttributesArray = array(
            'product_visibility',
            'product_type',
            'product_family',
            'product_version',
        );

        $this->_booleanValueAttributesArray = array(
            'dtc_restrict_ind',
            'preselect',
            'use_service_value',
            'status',
            'ask_confirmation_first',
            'preselect',
            'ident_needed',
            'sim_only',
            'bank_collection_mandatory',
            'allowed_for_porting',
            'allowed_for_activation',
            'allowed_for_add',
            'prepaid',
            'checkout_product',
            'initial_selectable',
            'is_deleted',
            'marketable',
            'droppable',
            'selectable',
            'channels_overview',
            'hide_summary',
            'hide_inventory',
            'zuhause_address_needed',
            'phonebook_entry_allowed',
            'ultracard_allowed',
            'kassenzettel_ind',
            'port_all_numbers',
        );

        $this->_taxValueAttributesArray = [
            'tax_class_id'
        ];
    }

    public function setTextAttributes() {
        $this->_textAttributesArray = array(
            'service_description',
        );

        $this->_numericValueAttributesArray = [
            'vat',
        ];
    }

    public function setDecimalAttributes() {
        $this->_decimalValueAttributesArray = [
            'price',
            'display_price',
            'new_price',
            'maf_discount',
            'maf',
            'discount_amount',
            'price_discount',
            'hwi_amount_gross',
            'net_material_price',
            'hwi_onetime_payment_net',
            'sub_base_gross_price',
            'hwi_onetime_payment_gross',
            'pricelist',
            'gross_material_price',
            'sub_discount_net',
            'sub_discount_gross',
            'contract_value',
        ];
    }

    public function getAttributeFieldType($field_code){
        if(in_array($field_code, $this->_valueAttributesArray)){
            return 'option';
        } else if(in_array($field_code, $this->_multiValueAttributesArray)){
            return 'multi';
        } else if(in_array($field_code, $this->_booleanValueAttributesArray)){
            return 'boolean';
        } else if(in_array($field_code, $this->_textAttributesArray)){
            return 'text';
        } else if(in_array($field_code, $this->_taxValueAttributesArray)){
            return 'tax';
        } else if(in_array($field_code, $this->_numericValueAttributesArray)){
            return 'numeric';
        } else if(in_array($field_code, $this->_decimalValueAttributesArray)){
            return 'decimal';
        } else {
            return 'data';
        }
    }

    /**
     * clear all the old file that starts with the attribute set name.
     */
    public function deleteOldFile(){
        $folder_path = $this->getExportDir(). DS . $this->_exportFile.'*.csv';
        array_map('unlink', glob($folder_path));
    }

    protected function libxmlDisplayError($xmlError)
    {
        switch ($xmlError->level) {
            case LIBXML_ERR_WARNING:
                $return = "[Warning] $xmlError->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return = "[Error] $xmlError->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return = "[Fatal] $xmlError->code: ";
                break;
        }

        $return .= trim($xmlError->message);

        if ($xmlError->file) {
            $return .=    " in $xmlError->file";
        }

        $return .= " on line $xmlError->line";

        return $return;
    }

    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    /**
     * Get path to export dir
     * @return string
     */

    private function getExportDir()
    {
        $exportDir = Mage::getBaseDir('export');
        if(!is_writable($exportDir)) {
            print_r('Cannot write to the export directory var/export.');
            exit;
        }
        return $exportDir;
    }

    /**
     * Get path to export dir
     * @return string
     */
    private function getXsdDir()
    {
        $xsdDir = Mage::getBaseDir('var'). '/xsd';
        if (!is_dir($xsdDir)) {
            print_r('"xsd" directory does not exist.');
            exit;
        }
        return $xsdDir;
    }

    // function to change words with underscores to Camel case.
    function usToCamelCase($string) 
    {
        $str = str_replace('_', '', ucwords($string, '_'));
        return $str;
    }

    // function to change words in camel case to underscores.
    function camelCaseToUs($input) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    protected function removeFromArray(&$array, $searchKey)
    {
        if (($key = array_search($searchKey, $array)) !== false) {
            unset($array[$key]);
        }
    }
}

//calling the run function the Dyna_Export_Catalog_xml class
$export = new Dyna_Export_Catalog_xml();
$export->run();