#!/bin/bash
DOCUMENT_ROOT="$(dirname "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" )"
DATE=$(date +"%Y-%m-%d")
command -v xml2 >/dev/null 2>&1 || { echo >&2 "xml2 is required but it's not installed. Please hardcode MYSQL connection parameters. Aborting."; exit 1; }
#maybe prompt user input for MySQL and Redis params if xml2 is not available?
MYSQL_HOST="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/resources/default_setup/connection/host | cut -d"=" -f2)"
MYSQL_USERNAME="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/resources/default_setup/connection/username | cut -d"=" -f2)"
MYSQL_PASSWORD="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/resources/default_setup/connection/password | cut -d"=" -f2)"
MYSQL_DATABASE="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/resources/default_setup/connection/dbname | cut -d"=" -f2)"
REDIS_SESSION_HOST="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/redis_session/host | cut -d"=" -f2)"
REDIS_SESSION_DATABASE="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/redis_session/db | cut -d"=" -f2)"
REDIS_CACHE_SERVER="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/cache/backend_options/server | cut -d"=" -f2)"
REDIS_CACHE_DATABASE="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/cache/backend_options/database | cut -d"=" -f2)"

#DO NOT EDIT BELOW THIS LINE
if [ "$1" == "export" ]; then
    echo "Creating DB backup of $DOCUMENT_ROOT.";
    echo "Starting full backup of $MYSQL_DATABASE"
    set -o pipefail
    mysqldump -h$MYSQL_HOST -u$MYSQL_USERNAME  --default-character-set=utf8 -p$MYSQL_PASSWORD $MYSQL_DATABASE | gzip > "$DOCUMENT_ROOT/var/export/fullbackup_-$DATE-_-$MYSQL_DATABASE-.sql.gz"
    if [ "$?" -eq 0 ]; then
        echo "Full backup created of $MYSQL_DATABASE"
    else
        echo "Could not create database backup -> command failed"
    fi
else
    if [ "$1" == "import" ]; then
        SECURE_BASE_URL="$(mysql -h"$MYSQL_HOST" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" $MYSQL_DATABASE -s -N -e "SELECT VALUE FROM core_config_data WHERE path = 'web/secure/base_url' LIMIT 1;")";
        UNSECURE_BASE_URL="$(mysql -h"$MYSQL_HOST" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" $MYSQL_DATABASE -s -N -e "SELECT VALUE FROM core_config_data WHERE path = 'web/unsecure/base_url' LIMIT 1;")";
        echo "Importing new database for $DOCUMENT_ROOT.";
        set -o pipefail
        echo "First we empty database $MYSQL_DATABASE";
        mysql -h$MYSQL_HOST -u$MYSQL_USERNAME -p$MYSQL_PASSWORD -Nse 'show tables' $MYSQL_DATABASE | while read table; do mysql -h$MYSQL_HOST -u$MYSQL_USERNAME -p$MYSQL_PASSWORD -e "SET FOREIGN_KEY_CHECKS = 0; drop table $table;" $MYSQL_DATABASE; done
        mysql -h$MYSQL_HOST -u$MYSQL_USERNAME -p$MYSQL_PASSWORD -e "SET FOREIGN_KEY_CHECKS = 1;" $MYSQL_DATABASE
        if [ "$?" -eq 0 ]; then
            echo "Database $MYSQL_DATABASE is now empty and ready for import"
        else
            echo "Could not empty database $MYSQL_DATABASE -> command failed"
        fi
        echo "Starting database import of $2 into $MYSQL_DATABASE database"
        if [[ $2 == *.gz ]]; then
            zcat $DOCUMENT_ROOT/var/import/$2 | mysql -h$MYSQL_HOST -u$MYSQL_USERNAME -p$MYSQL_PASSWORD $MYSQL_DATABASE
        else
            if [[ $2 == *.sql ]]; then
                mysql -h$MYSQL_HOST -u$MYSQL_USERNAME -p$MYSQL_PASSWORD $MYSQL_DATABASE < $DOCUMENT_ROOT/var/import/$2
            fi
        fi
        if [ "$?" -eq 0 ]; then
            echo "Restoring latest configuration in core_config_data"
            mysql -h"$MYSQL_HOST" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" $MYSQL_DATABASE -e "UPDATE core_config_data SET VALUE = 1 WHERE path LIKE '%use_stubs%';";
            mysql -h"$MYSQL_HOST" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" $MYSQL_DATABASE -e "UPDATE core_config_data SET VALUE = '$UNSECURE_BASE_URL' WHERE path = 'web/unsecure/base_url';";
            mysql -h"$MYSQL_HOST" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" $MYSQL_DATABASE -e "UPDATE core_config_data SET VALUE = '$SECURE_BASE_URL' WHERE path = 'web/secure/base_url';";
            mysql -h"$MYSQL_HOST" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" $MYSQL_DATABASE -e "UPDATE core_config_data SET VALUE = 0 WHERE path LIKE '%post_to_slack%';";
            echo "Flushing Redis cache to start fresh with the new DB"
            redis-cli -h $REDIS_SESSION_HOST -n $REDIS_SESSION_DATABASE flushdb && redis-cli -h $REDIS_CACHE_SERVER -n $REDIS_CACHE_DATABASE flushdb
            echo "Full import of $2 into $MYSQL_DATABASE database finished"
        else
            echo "Could not import file $2 into $MYSQL_DATABASE database -> command failed"
        fi
    else
        echo "Usage:";
        echo "-------------";
        echo "./database_ops.sh import <filename>";
        echo "<filename> can be of type .sql or .gz (gzipped .sql file, like the one exported by this script)";
        echo "<filename> must reside inside $DOCUMENT_ROOT/var/import";
        echo "-------------";
        echo "./database_ops.sh export";
        echo "Database will be exported to $DOCUMENT_ROOT/var/export/fullbackup_-<date>-_-<database_name>-.sql.gz";
    fi
fi
