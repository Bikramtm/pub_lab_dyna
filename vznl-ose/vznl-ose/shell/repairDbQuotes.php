<?php
require_once 'repairDbClass.php';

/*
 * Fix relationship between shopping cart, order and catalogue
 * */
class Tools_Db_Repair_Quotes extends Tools_Db_Repair
{
    const LIMIT    = 10;

    /**
     * Tools_Db_Repair_Orders constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->logFile = "repairdb_fix_quotes_".date('Y_m_d').".log";
    }

    /**
     * Run script
     */
    public function run()
    {
        $this->log("Start script.");
        $results = $this->getBrokenData();

        // if in readonly mode, just return the required actions
        if ($this->getReadOnly() == true) {
            $this->log("Stop script.");
            return (!empty($results)) ? ["Found broken relationship between shopping cart, order and catalogue."] : [];
        }

        // update db records
        while (!empty($results)) {
            foreach ($results as $data) {
                $sql = "UPDATE sales_flat_quote_item 
                        SET product_id=(SELECT entity_id FROM catalog_product_entity WHERE sku='".$data['sku']."' limit 1) 
                        WHERE item_id=".$data['item_id'] . " AND product_id=".$data['product_id'];

                $this->log("Running: ".$sql);
                $this->writeConnection->exec($sql);
            }

            $this->step += self::LIMIT;
            usleep(500000);
            $results = $this->getBrokenData();
        }

        $this->log("Stop script.");
    }

    /**
     * Get broken records
     * @return mixed
     */
    private function getBrokenData()
    {
        $sql = "SELECT qi.item_id, qi.sku, qi.product_id 
              FROM sales_flat_quote_item AS qi 
              INNER JOIN sales_flat_quote AS q ON q.`entity_id`=qi.`quote_id` 
              WHERE 
                product_id != (SELECT entity_id FROM catalog_product_entity WHERE catalog_product_entity.sku=qi.sku) AND 
                (q.cart_status >= 1 or q.is_offer=1) 
              ORDER BY qi.item_id DESC 
              LIMIT ".self::LIMIT;

        return $this->readConnection->fetchAll($sql);
    }
}
