<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'abstract.php';

class Dyna_MagentoConfigCache_CLI extends Mage_Shell_Abstract
{
    public function run()
    {
        $this->clearConfigCache();
    }

    function clearConfigCache()
    {
        $result = Mage::app()->getCacheInstance()->cleanType('config');
        if ($result) {
            echo "Magento Config Cache was reset!";
        } else {
            echo "Could not reset Magento Config Cache";
        }
        return $result;
    }
}

// Example
//php flushconfigcache.php

$clearcache = new Dyna_MagentoConfigCache_CLI();
$clearcache->run();