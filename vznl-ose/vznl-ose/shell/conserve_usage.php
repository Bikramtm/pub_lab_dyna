<?php

require_once 'abstract.php';

/**
 * Class Dyna_Update_ReleaseStatus
 */
class Dyna_Conserve_Usage extends Mage_Shell_Abstract
{
    protected $_connection = null;

    protected $_retainTableColumns = array(
        array(
            'table' => 'salesrule',
            'fields' => array(
                'uses_per_customer',
                'times_used',
                'uses_per_coupon'
            ),
            'entity_id' => 'rule_id'
        ),
        array(
            'table' => 'salesrule_coupon',
            'fields' => array(
                'usage_per_customer',
                'times_used'
            ),
            'entity_id' => 'coupon_id'
        )
    );

    /**
     * @return null|Varien_Db_Adapter_Interface
     */
    protected function getConnection()
    {
        if ($this->_connection == null) {
            $this->_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        }
        return $this->_connection;
    }

    /**
     * @return bool|string
     */
    public function generateRetainColumn()
    {
        $connection = $this->getConnection();
        $updateStmt = '';
        foreach ($this->_retainTableColumns as $table) {
            $wrapped = array_map(
                function ($el) {
                    return '`' . $el . '`';
                },
                $table['fields']
            );

            $query = "SELECT `" . $table['entity_id'] . '`,' . implode(', ',
                    $wrapped) . "FROM " . $table['table'] . ";";
            $rows = $connection->fetchAssoc($query);

            foreach ($rows as $row) {
                $updateArray = [];
                foreach ($row as $name => $value) {
                    if ($name == $table['entity_id']) {
                        continue;
                    }
                    $updateArray[] = $name . '=' . ($value == null ? 0 : $value);
                }
                $updateStmt .= 'UPDATE ' . $table['table'] . ' SET ' . implode(', ',
                        $updateArray) . ' WHERE ' . $table['entity_id'] . '=' . $row[$table['entity_id']] . ';';
            }
        }
        return $updateStmt;
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        $releaseId = $this->getArg('release_id');
        $releaseVersion = $this->getArg('release_version');
        $status = $this->getArg('status');

        $filename = sprintf('publish_update_%s_%s.sql', $releaseVersion, $releaseId);
        $path = DS . trim(Mage::getBaseDir('var'), DS) . DS . 'releases';

        if ($status == Dyna_Sandbox_Model_Publisher::UPDATE_SCRIPT_STATUS_START) {
            $updateStmt = $this->generateRetainColumn();
            if ($this->isWritable($path)) {
                if (@file_put_contents($path . DS . $filename, $updateStmt) === false) {
                    Mage::throwException(sprintf('Failed to create update usage script %s', $path . DS . $filename));
                }
            } else {
                Mage::throwException(sprintf('Failed to create update usage script directory at path %s', $path));
            }
        } elseif ($status == Dyna_Sandbox_Model_Publisher::UPDATE_SCRIPT_STATUS_END) {
            $updateStmt = @file_get_contents($path . DS . $filename);
            if ($updateStmt === false) {
                Mage::throwException(sprintf('Failed to retrieve update usage script at path %s',
                    $path . DS . $filename));
            } else {
                $this->getConnection()->query($updateStmt);
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * @param $path string
     * @return bool
     */
    protected function isWritable($path)
    {
        if (!realpath($path)) {
            if (!@mkdir($path, 0775, true)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  help							This help
USAGE;
    }
}

$processor = new Dyna_Conserve_Usage();
$processor->run();