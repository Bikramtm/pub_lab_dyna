<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'abstract.php';

class Dyna_CreateDatabaseBackup_Cli extends Mage_Shell_Abstract
{
    private $hostname = null;
    private $username = null;
    private $password = null;
    private $databaseName = null;
    private $temporaryDirectory = "/tmp"; #Temporary linux folder
    private $destinationDirectory = "~/"; #Home directory of current user

    public function run()
    {
        $tempFolder = $this->getArg('temp');
        $destinationFolder = $this->getArg('destination');

        if ($tempFolder) {
            $this->temporaryDirectory = $tempFolder;
        }

        if ($destinationFolder) {
            $this->destinationDirectory = $destinationFolder;
        }

        $config  = Mage::getConfig()->getResourceConnectionConfig("default_setup");

        $this->_writeLine('Connecting to database');

        $databaseInfo = array(
            "host" => $config->host,
            "user" => $config->username,
            "pass" => $config->password,
            "dbname" => $config->dbname
        );

        $hostname = $databaseInfo["host"];
        $username = $databaseInfo["user"];
        $password = $databaseInfo["pass"];
        $databaseName = $databaseInfo["dbname"];

        if ($databaseName == "magento")
        {
            $this->_writeLine('Database could not be found, cannot continue script');
            exit;
        }

        $this->_writeLine('Starting full backup of ' . $databaseName);
        $this->full_backup($hostname, $username, $password, $databaseName);
    }

    private function full_backup($host,$user,$pass,$name)
    {
        $archiveFileName = "fullbackup" . '_' . date("Y-m-d-H-i-s") . '_' . $host . '_' . $name . '.tar.gz';
        $databaseFilename = "fulldatabase_" . date("Ymd-His") . ".gz";
        $command = "mysqldump -h$host -u$user --default-character-set=utf8 -p$pass $name | gzip > $this->temporaryDirectory" . DIRECTORY_SEPARATOR . "$databaseFilename";

        system($command, $returnvalue);
        if ($returnvalue == 0) {
            $archive[0] = $this->temporaryDirectory . DIRECTORY_SEPARATOR . "$databaseFilename";
            $this->_writeLine("Full backup created of " . $name);

            $catalogDestination = $this->temporaryDirectory . DIRECTORY_SEPARATOR . "APP_VERSION.php";
            $softwareDestination = $this->temporaryDirectory . DIRECTORY_SEPARATOR . "SOFTWARE_VERSION.php";

            # Copy the software version so that we always know which OSE build was deployed/used for the current backup
            if ($this->copySoftwareVersion($softwareDestination)) {
                $archive[1] = $softwareDestination;
                $this->_writeLine("Software version copied and will also be added to the archive file");
            }

            if ($this->copyCatalogVersion($catalogDestination)) {
                $archive[2] = $catalogDestination;
                $this->_writeLine("Catalog version copied and will also be added to the archive file");
            }

            $this->createFullBackupArchive($archive, $archiveFileName);
        } else {
            $this->_writeLine("Could not create full backup -> command failed");
        }
    }

    public function createFullBackupArchive($archive, $archiveFileName)
    {
        $toBeTarredFiles = "";
        foreach ($archive as $currentFile)
        {
            $toBeTarredFiles .= $currentFile . " ";
        }

        $command = "tar -czf $this->destinationDirectory" . DIRECTORY_SEPARATOR . "$archiveFileName $toBeTarredFiles";
        system($command, $returnvalue);

        if ($returnvalue == 0)
        {
            foreach ($archive as $currentFile)
            {
                $command = "rm $currentFile";
                system($command, $returnvalue);
            }
            $this->_writeLine("Full backup archive created");
        }
        else
        {
            $this->_writeLine("Full backup archive could not be created !!!");
        }
    }

    public function copyCatalogVersion($destination)
    {
        $result = false;
        $catalogVersionFile = Mage::getBaseDir('media') . DIRECTORY_SEPARATOR . "APP_VERSION.php";

        if (file_exists($catalogVersionFile))
        {
            copy($catalogVersionFile, $destination);
            $result = true;
        }
        return $result;
    }

    public function copySoftwareVersion($destination)
    {
        $result = false;
        $softwareVersionFile = Mage::getBaseDir('etc') . DIRECTORY_SEPARATOR . "SOFTWARE_VERSION.php";

        if (file_exists($softwareVersionFile))
        {
            copy($softwareVersionFile, $destination);
            $result = true;
        }
        return $result;
    }

    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $availableTypes = implode('/', $this->exclude);
        return <<<USAGE
  Usage:  php file      
  help				This help
  destination       Export fullbackup to specified destination          (Default: /~  [USER HOME DIRECTORY])
  temp              Use specified temporary directory for temp files    (Default: /tmp)
  
  Example: php createdatabasebackup.php destination /~/SIT temp /~/tmp     [Creates a backup to specified destinaion]
                                                                            and uses temp folder]
 
USAGE;
    }
}

// Example
//php createdatabasebackup.php

$import = new Dyna_CreateDatabaseBackup_Cli();
$import->run();