<?php
require_once 'abstract.php';

/**
 * Class Dyna_Import_Artifact
 */
class Dyna_Import_Artifact extends Mage_Shell_Abstract
{
    public function run()
    {
        $artifactPharPath = Mage::helper('dyna_configurator')->getArtifactPath();

        try {

            if ($artifactPath = $this->getArg('path')) {

                if (!is_readable($artifactPath)) {
                    throw new InvalidArgumentException('Invalid path or path not readable');
                }

                $phar = new \Phar($artifactPath);
                unset($phar);

                $fileUpdated = copy($artifactPath, $artifactPharPath);
                if (!$fileUpdated) {
                    throw new RuntimeException(sprintf('Could not write to artifact file %s', $artifactPharPath));
                }

                print('Artifact file updated' . PHP_EOL);
                return;
            }

        } catch (PharException $e) {
            print('Invalid source phar file' . PHP_EOL);
        } catch (Exception $e) {
            print($e->getMessage() . PHP_EOL);
            return;
        }

        echo $this->usageHelp();
    }

    /**
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php import_artifact.php -- [options]

  --path <path_to_artifact.phar>            - The path to the artifact file to be imported
  help                                      - This help

USAGE;
    }
}

$artifactImporter = new Dyna_Import_Artifact();
$artifactImporter->run();
