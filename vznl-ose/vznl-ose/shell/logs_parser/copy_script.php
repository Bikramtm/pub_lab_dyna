<?php
$expireInterval = 14400; // seconds
$lockPath = '/tmp/copy_script.lock';
$apphostname = isset($argv[1]) ? $argv[1] : null;
$logdirname = isset($argv[2]) ? $argv[2] : null;

if($apphostname === ''){
    throw new Exception('App hostname not set in argument');
    exit;
}
if($logdirname === ''){
    throw new Exception('Log dirname not set in argument');
    exit;
}

// Purge lock if expired
if( file_exists($lockPath) && ((filemtime($lockPath) + $expireInterval) < time())){
    unlink($lockPath);
}
if(!file_exists($lockPath)){
    file_put_contents($lockPath, 'lock');

    date_default_timezone_set('Europe/Amsterdam');
    $serverIdentifier = getHostName();
    $sourceLogsFolder = realpath('/usr/local/zend/var/apps/http/'.$apphostname.'/80/active/var/log');
    $customerImportFolder = realpath('/usr/local/zend/var/apps/http/'.$apphostname.'/80/active/var/old_customer_import');
    $destinationLogsFolder = realpath('/var/SP/Shared/'.$logdirname);
    $errorLogFile = fopen('copy_script.log', 'a');

    $log = function ($level, $message) use ($errorLogFile) {
        fputs($errorLogFile,
            sprintf('%s [%s]: %s%s', date('Y-m-d H:i:s'), $level, $message, PHP_SAPI === 'cli' ? PHP_EOL : '<br/>'));
    };

    function moveFolder($from, $to, $log, $suffix = '', $types = false)
    {
        if (realpath($to) === false) {
            mkdir($to);
        }
        $log('INFO', 'Started parsing folder: ' . $from);
        $list = array();
        // Create a list of current files and folders
        if ($handle = opendir($from)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $list[$entry] = $from . DIRECTORY_SEPARATOR . $entry;
                }
            }
            closedir($handle);
        }

        // Move the files to the new folder and new name
        $files = array_filter($list, 'is_file');

        foreach ($files as $fileName => $filePath) {
            if ($types !== false && is_array($types)) {
                // Only process extensions provided
                $extn = explode('.', $fileName);
                $extn = array_pop($extn);
                if (!in_array($extn, $types)) {
                    continue;
                }
            }

            if ($suffix) {
                $newName = $to . DIRECTORY_SEPARATOR . str_replace('.log', sprintf(".%s.log", $suffix), $fileName,
                        $count);
                if ($count === 0) {
                    $newName = $to . DIRECTORY_SEPARATOR . sprintf("%s-%s", $fileName, $suffix);
                }
            } else {
                $newName = $to . DIRECTORY_SEPARATOR . $fileName;
            }

            // In case of error, log it
            if (copy($filePath, $newName) === false) {
                $log('ERROR', sprintf('Failed copying %s to %s', $filePath, $newName));
            } else {
                if (unlink($filePath) === false) {
                    $log('ERROR', sprintf('Failed deleting %s', $filePath));
                }
            }
        }
        $log('INFO', 'Done parsing folder: ' . $from);

        // For each subdirectory copy recursively
        $dirs = array_filter($list, 'is_dir');
        foreach ($dirs as $dirName => $dirPath) {
            moveFolder($from . DIRECTORY_SEPARATOR . $dirName, $to . DIRECTORY_SEPARATOR . $dirName, $log, $suffix);
        }
    }

    if ($sourceLogsFolder === false || $destinationLogsFolder === false) {
        $log('ERROR', 'Source and destination folders must exist');
        fclose($errorLogFile);
        die;
    }

    $log('INFO', 'Starting copying');
    $folderName = $destinationLogsFolder . DIRECTORY_SEPARATOR . date('Y-m-d', strtotime('-1 days'));
    // Create the folder for the past day in case it does not exists
    if (realpath($folderName) === false) {
        mkdir($folderName);
    }

    // Move all the files from the logs folder and append the server ip to them
    moveFolder($sourceLogsFolder, $folderName, $log, $serverIdentifier);

    // Move all the files from the customer import folder and append the server ip to them
    if ($customerImportFolder !== false) {
        $log('INFO', 'Starting copying customer import logs');
        moveFolder($customerImportFolder, $folderName . DIRECTORY_SEPARATOR . 'old_customer_import', $log,
            $serverIdentifier);
        $log('INFO', 'Done copying customer import logs');
    }

    $log('INFO', 'Copying done');

    fclose($errorLogFile);

    unlink($lockPath); // Remove lock
} else {
    echo 'Could not start script due existing lock: '.$lockPath.PHP_EOL;
}
