<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'catalogtables.php';

/**
 * Class Dyna_CleanCatalogFromDatabase_Cli
 */
class Dyna_CleanCatalogFromDatabase_Cli extends Dyna_CatalogTables_Cli
{
    /**
     * Run database cleanup process
     */
    public function run()
    {
        if (empty(parent::CATALOGTABLES))
        {
            parent::_writeLine('The catalogTables constant is empty, cannot continue the script');
            exit;
        }

        if (empty(parent::MULTIMAPPERTABLES))
        {
            parent::_writeLine('The multimapperTables constant is empty, cannot continue the script');
            exit;
        }

        parent::_writeLine('Connecting to database');
        $connection = parent::getConnection();
        if (is_null($connection))
        {
            parent::_writeLine('Database could not be found, cannot continue script');
            exit;
        }

        $config = Mage::getConfig()->getResourceConnectionConfig('default_setup');
        $databaseName = $config->dbname;

        parent::_writeLine('Starting cleaning of catalog from ' . $databaseName);
        $cleanupTables = array_diff(parent::getFullCatalog(), parent::SKIPCATALOGTABLESFORCLEANUP);

        // Intersecting again with database tables
        $allTables = parent::getConnection(Mage_Core_Model_Resource::DEFAULT_WRITE_RESOURCE)->fetchCol("show tables");
        $cleanupTables = array_intersect($cleanupTables, $allTables);

        if ($this->cleanTables($cleanupTables))
        {
            parent::_writeLine('Cleaning succeeded for catalog of ' . $databaseName);
        }
        else
        {
            parent::_writeLine('Cannot clean catalog of ' . $databaseName);
        }
    }

    /**
     * Truncate tables and recreate default categories in one step
     *
     * @param array $tables
     *
     * @return bool|mixed
     */
    protected function cleanTables(array $tables = [])
    {
        if (!empty($tables))
        {
            $truncateQuery = 'SET FOREIGN_KEY_CHECKS = 0;' . PHP_EOL;
            foreach ($tables as $exportTable)
            {
                $truncateQuery .= sprintf('TRUNCATE TABLE %s;' . PHP_EOL, $exportTable);
            }
            $truncateQuery .= implode(PHP_EOL, parent::DEFAULT_CATEGORIES_QUERIES) . PHP_EOL;
            $truncateQuery .= 'SET FOREIGN_KEY_CHECKS = 1;' . PHP_EOL;
            echo $truncateQuery;
            return parent::executeSql($truncateQuery);
        }
        return false;
    }
}

// Example
//php cleancatalogfromdatabase.php

$import = new Dyna_CleanCatalogFromDatabase_Cli();
$import->run();