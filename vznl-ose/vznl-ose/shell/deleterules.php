<?php

/**
 * Shell script to clear compatibility rules by package type
 */

require_once 'abstract.php';

/**
 * Magento Log Shell Script
 *
 * @category    Dyna
 * @package     Dyna_ProductMatchRule
 * @author      Dyna
 */
class Dyna_ProductMatchRules_Cleaner extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        if ($packageType = $this->getArg('packageType')) {
            //Display message that cleaning starts for requested package type
            echo "Starting clearing product combination rules for requested package type: " . $packageType . PHP_EOL;

            //Load package type model by provided package code
            $packageTypeModel = Mage::getModel('dyna_package/packageType')->loadByCode(strtoupper($packageType));
            if (!$packageTypeModel->getId()) {
                //If not found, advise on checking package type code or importing package types
                echo "Cannot find this package type in database. Please check spelling or import package types file. " . PHP_EOL;
                echo "Nothing to do, exiting...";
                exit;
            } else {
                //If found, do clear all product match rules that are related to this package type
                $productMatchCollection = Mage::getModel('productmatchrule/rule')
                    ->getCollection()
                    ->addFieldToFilter('package_type', ['eq' => $packageTypeModel->getId()]);
                foreach ($productMatchCollection as $rule) {
                    $rule->delete();
                }
            }
            //Let them know that rules are cleared
            echo "Rules have been deleted. " . PHP_EOL;
            echo "Exiting... " . PHP_EOL;
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php deleterules.php --packageType [package_type_code]
  packageType       The package type code (CABLE_TV, CABLE_INTERNET_PHONE, MOBILE, PREPAID, DSL, LTE etc) for which the product match rules need to be removed from database
  help              This help

USAGE;
    }
}

$shell = new Dyna_ProductMatchRules_Cleaner();
$shell->run();
