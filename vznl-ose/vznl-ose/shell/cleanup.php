<?php

require_once 'abstract.php';

/**
 * Class Dyna_Clean_Quotes
 */
class Dyna_Cleanup extends Mage_Shell_Abstract
{
    /** @var float */
    protected $_startTime = 0.0;
    protected $_axiClient = null;
    protected $_cleanupCustomerClient = null;

    const COUPON_SEPARATOR = '$@$';
    const AGENT = 'SUPPORT';
    const THRESHOLD = 10;

    /**
     * Initialize application and parse input parameters
     */
    public function __construct()
    {
        $this->_lockExpireInterval = 14400;
        parent::__construct();
    }

    /**
     * @param $data
     * @return array
     */
    protected function toArray($data)
    {
        $items = array();
        if (is_null($data)) {
            return $data;
        } elseif (is_scalar($data)) {
            return $data;
        } elseif (is_array($data)) {
            foreach ($data as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif ($data instanceof Varien_Data_Collection) {
            foreach ($data as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif ($data instanceof Varien_Object) {
            foreach ($data->getData() as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif (is_object($data)) {
            foreach (get_object_vars($data) as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        }

        return $items;
    }

    /**
     * Run script
     */
    public function run()
    {
        try {
            if (!$this->hasLock()) {
                $this->createLock();

                // Validate interval
                if (0 === ($interval = (int)trim(Mage::getStoreConfig('cleanup_configuration/customer_data_cleanup/hours')))) {
                    throw new Exception('Invalid setting for the cleanup interval. Must be a valid digit bigger than 0.');
                }

                // Validate ban(s)
                $bans = array_map(function ($el) {
                    return trim($el);
                }, array_filter(explode(',', Mage::getStoreConfig('cleanup_configuration/customer_data_cleanup/bans')),
                    function ($el) {
                        if (is_numeric(trim($el))) {
                            return true;
                        }

                        return null;
                    }));

                if (count($bans)) {
                    /** @var Dyna_Customer_Model_Customer_Resource_Customer_Collection $c */
                    $c = Mage::getModel('customer/customer')
                        ->getCollection()
                        ->addFieldToFilter('ban', ['in' => $bans]);
                    if (!count($c)) {
                        throw new Exception('No existing customers provided');
                    }
                    $ids = [];
                    foreach ($c as $cu) {
                        $ids[] = $cu->getId();
                    }
                } else {
                    throw new Exception('No customers provided');
                }
                // Loop while there are Superorders and Quotes that should be deleted
                while (true) {
                    $continue = true;
                    // Calculate date
                    $date = new DateTime(sprintf('-%s hours', $interval));
                    /** @var Mage_Sales_Model_Entity_Quote_Collection $quoteCollection */
                    $quoteCollection = Mage::getResourceModel('sales/quote_collection')
                        ->addFieldToFilter('created_at', array('lt' => $date->format('Y-m-d H:i:s')))
                        ->addFieldToFilter('customer_id', ['in' => $ids])
                        ->setPageSize(self::THRESHOLD)
                        ->setCurPage(1);
                    if (count($quoteCollection)) {
                        $this->deleteQuotes($quoteCollection);
                    } else {
                        $continue = false;
                    }
                    $ordersCollection = Mage::getResourceModel('superorder/superorder_collection')
                        ->addFieldToFilter('created_at', array('lt' => $date->format('Y-m-d H:i:s')))
                        ->addFieldToFilter('customer_id', ['in' => $ids])
                        ->setPageSize(self::THRESHOLD)
                        ->setCurPage(1);
                    if (count($ordersCollection)) {
                        $this->deleteSuperorders($ordersCollection);
                        $continue = true;
                    }
                    if (!$continue) {
                        break;
                    }
                    unset($date);
                    unset($quoteCollection);
                    unset($ordersCollection);
                }
                // Once all superorders and quotes have been deleted, cleanup esb database
                $this->cleanupCustomers($bans);

                $this->removeLock();
                exit(0);
            } else {
                throw new Exception('Cannot start script due to existing lock: ' . $this->getLockPath());
            }
        } catch (Exception $e) {
            $this->printLn($e->getMessage());
            $this->printLn($e->getTraceAsString());
            exit(1);
        }
    }

    /**
     * Parse the quotes in the collection, and remove coupons/sales rules usages
     *
     * @param $quoteCollection
     */
    private function deleteQuotes($quoteCollection)
    {
        $this->_startTime = microtime(true);
        $quoteNr = 0;
        $quoteIds = array();
        /** @var Mage_Sales_Model_Quote $quote */
        foreach ($quoteCollection->getItems() as $quote) {
            $quoteIds[] = $quote->getId();
            $data = array();
            $data['quote'] = $quote->toArray();
            /** @var Dyna_Checkout_Model_Sales_Quote_Address $address */
            foreach ($quote->getAllAddresses() as $address) {

                $data['address'][$address->getAddressType()] = $this->toArray($address);
                try {
                    $data['address'][$address->getAddressType() . '_items'] = $this->toArray($address->getAllItems());
                } catch (Exception $e) {
                    $this->printLn($e->getMessage());
                    $this->printLn($e->getTraceAsString());
                }
            }
            $data['items'] = $this->toArray($quote->getItemsCollection());
            $quoteNr++;
        }

        $coreResource = Mage::getSingleton('core/resource');
        $conn = $coreResource->getConnection('core_read');

        $select = $conn->select()
            ->from(array('main_table' => $coreResource->getTableName('sales/quote_item')), "GROUP_CONCAT(main_table.applied_rule_ids SEPARATOR ', ') as rules")
            ->join(array('quotes' => $coreResource->getTableName('sales/quote')), "main_table.quote_id = quotes.entity_id", array("quotes.customer_id"))
            ->group('quotes.customer_id')
            ->where('quote_id in (?)', $quoteIds)
            ->where('super_order_edit_id is NULL')
            ->where('super_quote_id is NULL');

        $rows = $conn->fetchAll($select);

        $this->decrementSalesRules($rows);

        $select = $conn->select()
            ->from(array('main_table' => $coreResource->getTableName('package/package')), "GROUP_CONCAT(main_table.coupon SEPARATOR '" . self::COUPON_SEPARATOR . "') as coupons")
            ->join(array('quotes' => $coreResource->getTableName('sales/quote')), "main_table.quote_id = quotes.entity_id", array("quotes.customer_id"))
            ->group('quotes.customer_id')
            ->where('quote_id in (?)', $quoteIds)
            ->where('super_order_edit_id is NULL')
            ->where('super_quote_id is NULL');

        $rows = $conn->fetchAll($select);

        $this->decrementCoupons($rows);
        foreach ($quoteCollection as $quote) {
            $quote->delete();
        }
        if (!$this->getArg('q')) {
            $this->printLn(sprintf('Deleted %s quotes in %ss', $quoteNr, number_format(microtime(true) - $this->_startTime, 3)));
        }
        unset($rows);
        unset($coreResource);
        unset($conn);
        unset($select);
        unset($quote);
    }

    /**
     * @param $rows
     * @throws Exception
     */
    private function decrementSalesRules($rows)
    {
        $priceRulesHelper = Mage::helper('pricerules');
        $customerRule = array();
        $basicRules = array();
        foreach ($rows as &$row) {
            if (isset($row['customer_id']) && !empty($row['customer_id'])) {
                $itemsToDecrement = $priceRulesHelper->explodeTrimmed($row['rules']);
                foreach ($itemsToDecrement as $rule) {
                    $rule = trim($rule);
                    if (isset($customerRule[$rule][$row['customer_id']])) {
                        $customerRule[$rule][$row['customer_id']] = $customerRule[$rule][$row['customer_id']] + 1;
                    } else {
                        $customerRule[$rule][$row['customer_id']] = 1;
                    }

                    if (isset($basicRules[$rule])) {
                        $basicRules[$rule] = $basicRules[$rule] + 1;
                    } else {
                        $basicRules[$rule] = 1;
                    }
                }
            } else {
                $itemsToDecrement = $priceRulesHelper->explodeTrimmed($row['rules']);
                foreach ($itemsToDecrement as $rule) {
                    $rule = trim($rule);
                    if (isset($basicRules[$rule])) {
                        $basicRules[$rule] = $basicRules[$rule] + 1;
                    } else {
                        $basicRules[$rule] = 1;
                    }
                }
            }
        }
        unset($row);

        foreach ($basicRules as $id => &$decrement) {
            $ruleObject = Mage::getModel('salesrule/rule')->load($id);
            if ($ruleObject->getId()) {
                if ($ruleObject->getTimesUsed() < $decrement) {
                    $ruleObject->setTimesUsed(0)->save();
                } else {
                    $ruleObject->setTimesUsed($ruleObject->getTimesUsed() - $decrement)->save();
                }

                if (isset($customerRule[$id])) {
                    foreach ($customerRule[$id] as $customerId => $customerTimesUsed) {
                        $ruleCustomerModel = Mage::getModel('salesrule/rule_customer');
                        $ruleCustomerModel->loadByCustomerRule($customerId, $ruleObject->getId());
                        if ($ruleCustomerModel->getId()) {
                            if ($ruleCustomerModel->getTimesUsed() < $customerTimesUsed) {
                                $ruleCustomerModel->delete();
                            } else {
                                $ruleCustomerModel->setTimesUsed($ruleCustomerModel->getTimesUsed() - $customerTimesUsed)->save();
                            }
                        }
                    }
                }
            }
        }
        unset($decrement);

    }

    private function decrementCoupons($rows)
    {
        $priceRulesHelper = Mage::helper('pricerules');
        $customerRule = array();
        $basicRules = array();
        foreach ($rows as &$row) {
            if (!empty($row['customer_id'])) {
                $itemsToDecrement = $priceRulesHelper->explodeTrimmed($row['coupons'], self::COUPON_SEPARATOR);
                foreach ($itemsToDecrement as &$rule) {
                    $rule = trim($rule);
                    if (isset($customerRule[$rule][$row['customer_id']])) {
                        $customerRule[$rule][$row['customer_id']] = $customerRule[$rule][$row['customer_id']] + 1;
                    } else {
                        $customerRule[$rule][$row['customer_id']] = 1;
                    }

                    if (isset($basicRules[$rule])) {
                        $basicRules[$rule] = $basicRules[$rule] + 1;
                    } else {
                        $basicRules[$rule] = 1;
                    }
                }
                unset($rule);
            } else {
                $itemsToDecrement = $priceRulesHelper->explodeTrimmed($row['coupons'], self::COUPON_SEPARATOR);
                foreach ($itemsToDecrement as &$rule) {
                    $rule = trim($rule);
                    if (isset($basicRules[$rule])) {
                        $basicRules[$rule] = $basicRules[$rule] + 1;
                    } else {
                        $basicRules[$rule] = 1;
                    }
                }
                unset($rule);
            }
        }
        unset($row);

        foreach ($basicRules as $couponCode => &$decrement) {
            $coupon = Mage::getModel('salesrule/coupon');
            $coupon->load($couponCode, 'code');
            if ($coupon->getId()) {
                if ($coupon->getTimesUsed() < $decrement) {
                    $coupon->setTimesUsed(0)->save();
                } else {
                    $coupon->setTimesUsed($coupon->getTimesUsed() - $decrement)->save();
                }

                if (isset($customerRule[$couponCode])) {
                    foreach ($customerRule[$couponCode] as $customerId => $customerTimesUsed) {
                        $priceRulesHelper->reduceCustomerCouponTimesUsed($customerId, $coupon->getId(), $customerTimesUsed);
                    }
                }
            }
        }
        unset($decrement);
    }

    private function deleteSuperorders($ordersCollection)
    {
        $this->_startTime = microtime(true);
        $orderNr = 0;
        /** @var Dyna_Superorder_Model_Superorder $superOrder */
        foreach ($ordersCollection as $superOrder) {
            // If order is not cancelled, cancel it
            if (!$superOrder->isCancelled()) {
                // perform axi call
                $this->getAxiClient()->cancelSaleOrder($superOrder->getOrderNumber(), self::AGENT);
            }
            $coreResource = Mage::getSingleton('core/resource');
            $conn = $coreResource->getConnection('core_read');
            // Delete all orders belonging to that superorder
            $conn->query(sprintf("DELETE FROM `sales_flat_order` WHERE (`superorder_id` = '%s')", $superOrder->getId()));
            // Delete history
            $conn->query(sprintf("DELETE FROM `superorder_change_history` WHERE (`superorder_id` = '%s')", $superOrder->getId()));
            // Delete snapshots
            $conn->query(sprintf("DELETE FROM `audit_snapshot` WHERE (`object_id` = '%s') AND (`class` = '%s')", $superOrder->getId(), get_class($superOrder)));
            // Delete superorder
            $superOrder->delete();
            ++$orderNr;
        }

        if (!$this->getArg('q')) {
            $this->printLn(sprintf('Deleted %s superorders in %ss', $orderNr, number_format(microtime(true) - $this->_startTime, 3)));
        }
    }

    /**
     * @param $ids
     * @return mixed
     */
    private function cleanupCustomers($ids)
    {
        return $this->getCustomerCleanupClient()->cleanCustomer($ids);
    }

    /**
     * @param $message
     */
    protected function printLn($message)
    {
        if (is_array($message)) {
            foreach ($message as $line) {
                echo sprintf("%s\n\r", $line);
            }
        } else {
            echo sprintf("%s\n\r", $message);
        }
    }

    /**
     * @return Dyna_Service_Model_Client_AxiClient
     */
    private function getAxiClient()
    {
        if (!$this->_axiClient) {
            $this->_axiClient = Mage::helper('dyna_service')->getClient('axi');
        }

        return $this->_axiClient;
    }

    /**
     * @return Dyna_Service_Model_Client_CleanupCustomerClient
     */
    private function getCustomerCleanupClient()
    {
        if (!$this->_cleanupCustomerClient) {
            $this->_cleanupCustomerClient = Mage::helper('dyna_service')->getClient('cleanupCustomer');
        }

        return $this->_cleanupCustomerClient;
    }

    /**
     * Display help on CLI
     * @return string
     */
    public function usageHelp()
    {
        $f = basename(__FILE__);

        return <<< USAGE
Usage: php ${f} [options]

  -q            Quiet, do not output to cli
  -h            Short alias for help
  help          This help
USAGE;
    }
}

$shell = new Dyna_Cleanup();
$shell->run();