<?php

require_once 'vznl_abstract.php';

/**
 * Class Vznl_Clean_Ilt
 */
class Vznl_Clean_Ilt extends Vznl_Shell_Abstract
{
    /** @var integer */
    protected $deleteOlderThanDays = 7;

    /**
     * Initialize application and parse input parameters
     */
    public function __construct()
    {
        $this->_lockExpireInterval = 14400;
        $this->_startTime = microtime(true);
        parent::__construct();
    }

    /**
     * Run script
     */
    public function run()
    {
        try {
            if(!$this->hasLock()) {
                $this->createLock();

                $date = new DateTime('-'.$this->deleteOlderThanDays.' days');
                $coreResource = Mage::getSingleton('core/resource');
                $conn = $coreResource->getConnection('core_read');

                $deleteStatement = sprintf("DELETE FROM `superorder_ilt` WHERE (`created_at` < '%s')", $date->format('Y-m-d H:i:s'));
                $output = $conn->exec($deleteStatement);

                $this->printLn('Cleaned: '.$output.' row(s)');

                $this->removeLock();
                exit(0);
            }else{
                throw new Exception('Cannot start script due to existing lock: '.$this->getLockPath());
            }
        } catch (Exception $e) {
            $this->printLn($e->getMessage());
            $this->printLn($e->getTraceAsString());
            exit(1);
        }
    }

    /**
     * @param $message
     */
    protected function printLn($message)
    {
        if (is_array($message)) {
            foreach ($message as $line) {
                echo sprintf("%s\n\r", $line);
            }
        } else {
            echo sprintf("%s\n\r", $message);
        }
    }

    /**
     * Display help on CLI
     * @return string
     */
    public function usageHelp()
    {
        $f = basename(__FILE__);
        return <<< USAGE
Usage: php ${f} [options]

  -h            Short alias for help
  help          This help
USAGE;
    }
}

$shell = new Vznl_Clean_Ilt();
$shell->run();
