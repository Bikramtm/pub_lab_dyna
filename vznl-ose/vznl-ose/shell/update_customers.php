<?php
require_once 'vznl_abstract.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class Vznl_Import_Customers_Cli extends Vznl_Shell_Abstract
{
    const API_IMPORT = 'import';

    protected $customerDumpLog;
    protected $customerImportLog;
    protected $customerDumpLogInitiated = false;
    protected $customerImportLogInitiated = false;

    public function __construct()
    {
        $this->_lockExpireInterval = 14400; // 4h
        $sessionDateTime = date('dmY_His');
        $this->customerDumpLog = 'customer_dump_' . $sessionDateTime . '.log';
        $this->customerImportLog = 'customer_import_' . $sessionDateTime . '.log';

        parent::__construct();
    }

    private function _writeLine($msg)
    {
        Mage::log($msg, null, $this->ensureLogPresence($this->customerImportLog, $this->customerImportLogInitiated));
    }

    /**
     * Adding Space after 4 characters for NL postcode
     * @return string
     */
    protected function nl_postcode_fileter($postcode) : string
    {
        if (strlen($postcode) == 6 && is_string(substr($postcode, -2))) {
            return wordwrap($postcode, 4, " ", true);
        } else {
            return $postcode;
        }
    }

    // copy_script.php moves logs at x hrs. This function ensures log writing is continued after a log file is moved.
    protected function ensureLogPresence($logFile, $logExisted)
    {
        $logBaseDir = $this->evalSlash(Mage::getBaseDir('log'));

        if(!file_exists($logBaseDir . $logFile) && $logExisted == true){
            $logFile = explode('.', $logFile)[0] . '_continued.' . explode('.',$logFile)[1];
        }

        return $logFile;
    }

    protected function evalSlash($path)
    {
        $length = strlen("/");

        if(substr($path, -$length) === "/"){
            return $path;
        }

        return $path . "/";
    }

    protected function convertUnixTimeToTime($unixTime)
    {
        $dateTime = date("H:i:s", substr($unixTime, 0, 10));

        return $dateTime;
    }

    private function customerMapping($data, $format)
    {
        $customer = array();
        /*
        * Vodafone
        * 0-CTN 1-BAN 2-SEGMENT_NAME 3 - PRICEPLAN_SEGMENT1_NAME 4-VALUE_TIER_7 5-ORG_NAME 6-COC_NUMBER 7-INITIALS 8-NAME_PREFIX_TXT 9-FAMILY_NAME 10-STREET 11-HOUSE_NUM 12-HOUSE_NUM_DETAIL 13-POSTAL_CD 14-CITY_NAME 15-VFMAIL 16-BIRTH_DT 17-GENDER
        */
        if($format == 'VF')
        {
            if($data[17] == 'M') {
                $salutation='Dhr.';
            } else if($data[17] == 'F') {
                $salutation='Mevr.';
            } else {
                $salutation='Aan.';
            }

            $customer['orderNumbers'] = array();
            $customer['accountNumber'] = $data[1];
            $customer['customerType'] = $data[2] == 'Enterprise' ? 'organization' : 'individual';
            $customer['metaData'] = (object) [];
            $customer['salutation'] = $salutation;
            if ($data[0] != "") {
                $customer['contactDetails'][]=
                    [
                        'contactType'=>'phone',
                        'type'=>'mobile',
                        'value'=>$data[0]
                    ];
            }

            if(($data[10] != "") && ($data[13] != "") && ($data[14] != "")) {
                $customer['contactDetails'][] = [
                    'contactType' => 'address',
                    'type' => 'billing_address',
                    'street' => $data[10],
                    'houseNumber' => $data[11] . ($data[12] != "" ? "-" . $data[12] : ""),
                    'postalCode' => $this->nl_postcode_fileter($data[13]),
                    'city' => $data[14],
                    'country' => 'Nederlands'
                ];
            }
            $customer['roleType'] = 'default';
            $customer['lineOfBusiness'] = 'mobile';
            $customer['accountStatus'] = 'active';

            //dob
            if($data[16] != "")
                $customer = $customer + [
                        'dateOfBirth' => date("Y-m-d",strtotime($data[16]))
                    ];

            if($data[2] == 'Enterprise') {
                $customer = $customer + [
                        'organizationName' => $data[5],
                        'chamberOfCommerce' => $data[6]
                    ];
            } else {
                $customer = $customer + [
                        'firstName' => $data[7],
                        'lastName' => $data[8] . " " . $data[9],
                        'gender' => $data[17] == 'M' ? 'male' : 'female'
                    ];
            }
        }

        /*
        * Ziggo
        * 0-TELEPHONECONTACTNUMBER; 1-CUSTOMERID; 2-SEGMENT_NAME; 3-PRICEPLAN_SEGMENT1_NAME; 4-VALUE_TIER_7; 5-ORG_NAME; 6-COC_NUMBER; 7-INITIALS; 8-NAME_PREFIX_TXT; 9-FAMILY_NAME; 10-STREET; 11-HOUSE_NUM; 12-HOUSE_NUM_DETAIL; 13-POSTAL_CD; 14-CITY_NAME; 15-MAIL; 16-BIRTH_DT; 17-GENDER; 18-STATUS
        */
        if($format == 'Ziggo')
        {
            if($data[17] == 'Male') {
                $salutation='Dhr.';
            } else if($data[17] == 'Female') {
                $salutation='Mevr.';
            } else {
                $salutation='Aan';
            }

            $gender = strtolower($data[17]);

            $customer['orderNumbers'] = array();
            $customer['accountNumber'] = $data[1];
            $customer['customerType'] = $data[2] == 'Residential' ? 'individual' : 'organization';
            $customer['metaData'] = (object) [];
            $customer['salutation'] = $salutation;
            if ($data[0] != "") {
                $customer['contactDetails'][]=
                    [
                        'contactType'=>'phone',
                        'type'=>'mobile',
                        'value'=>$data[0]
                    ];
            }
            if(($data[10] != "") && ($data[13] != "") && ($data[14] != "")) {
                $customer['contactDetails'][]=
                    [
                        'contactType' => 'address',
                        'type' => 'billing_address',
                        'street' => $data[10],
                        'houseNumber' => $data[11] . ($data[12] != "" ? "-" . $data[12] : ""),
                        'postalCode' => $this->nl_postcode_fileter($data[13]),
                        'city' => $data[14],
                        'country' => 'Nederlands'
                    ];
            }
            //email
            if($data[15] != "") {
                $customer['contactDetails'][] = [
                    "contactType" => "email",
                    "value" => $data[15]
                ];
            }
            $customer['roleType'] = 'default';
            $customer['lineOfBusiness'] = 'fixed';
            $customer['accountStatus'] = 'active';

            //dob
            if($data[16] != "") {
                $customer = $customer + [
                        'dateOfBirth' => date("Y-m-d",strtotime($data[16]))
                    ];
            }

            if($data[2] != 'Residential') {
                if ($data[5] != "") {
                    $orgName = $data[5];
                } else {
                    $orgName = $data[8] . " " . $data[9];
                }
                $customer = $customer + [
                        'organizationName' => $orgName,
                        'chamberOfCommerce' => $data[6]
                    ];
            } else {
                $customer = $customer + [
                        'firstName' => $data[7],
                        'lastName' => $data[8] . " " . $data[9],
                        'gender' => $gender
                    ];
            }
        }

        return $customer;
    }

    private function _runFile($file, $type)
    {
        try {
            ini_set('max_execution_time', 0);
            $time_start = time();
            $this->_writeLine('Reading File :'.$file);

            if(($handle = fopen($file, 'r')) !== false)
            {
                $customerBulkData = array();
                $header = fgetcsv($handle, 0, ';', chr(8));
                $customerProcessed = 0;
                $timeStartBatch = time();

                $arraysize = Mage::getStoreConfig('vodafone_service/customer_sync_sftp/arraysize') ?? 2;
                $endpoint = Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/rest_api_end_point');
                $api_url = $endpoint . DS . Vznl_Import_Customers_Cli::API_IMPORT;
                $verify = Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/verify') ? true : false;
                $proxy = Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/proxy') ? true : false;
                //          $this->_writeLine( 'Total Record:'. count($customerBulkData));

                $this->_writeLine('Processing the CSV file:' . $file);
                $this->_writeLine('###########################################');

                $i=0;
                $j=0;
                $k=0;
                $delay = $this->getArg('delay');
                if(!isset($delay))
                {
                    $delay = 0;
                }
                while(($data = fgetcsv($handle, 0, ';', chr(8))) !== false)
                {
                    //for vodafone upload only for Postpaid
                    $data = array_map('utf8_encode', $data);
                    if ($type == 'VF' && $data[3] == "Postpaid") {
                        $customerData = $this->customerMapping($data,$type);

                        if(isset($customerBulkData[$customerData["accountNumber"]])) {
                            $contactDetails = array_merge($customerBulkData[$customerData["accountNumber"]]["contactDetails"], $customerData["contactDetails"]);
                            $contactDetailsUnique = array_unique($contactDetails, SORT_REGULAR);
                            $customerData["contactDetails"] = $contactDetailsUnique;
                        }
                        $customerBulkData[$customerData["accountNumber"]] = $customerData;
                    } else {
                        $customerData = $this->customerMapping($data,$type);
                        $customerBulkData[] = $customerData;
                    }
                    $i++;
                    $j++;
                    $isExecuted = false;

                    if($i == $arraysize) {
                        $i=0;
                        $postData = array();

                        foreach ($customerBulkData as $key => $value) {
                            $postData[] = $value;
                        }

                        $startTime = time();
                        $options = [
                            'verify' => $verify,
                            'proxy' => $proxy,
                            'headers' => [
                                'Content-Type' => 'application/json'
                            ],
                            'body' => json_encode($postData),
                        ];
                        if(json_encode($postData) == '' || json_encode($postData) == null)
                        {
                            $this->_writeLine('Empty body');
                            $this->_writeLine(json_encode($postData));
                        }
                        else {
                            try {
                                $startCSTime = time();
                                Mage::log("Request :\n\n" . json_encode($postData) . "\n\n", null, $this->ensureLogPresence($this->customerDumpLog, $this->customerDumpLogInitiated));
                                $client = new Client();
                                $res = $client->request('POST', $api_url, $options);
                                $response = json_decode($res->getBody());
                                Mage::log("Response :\n\n" . $res->getBody() . "\n\n", null, $this->ensureLogPresence($this->customerDumpLog, $this->customerDumpLogInitiated));
                                $endCSTime = time();
                            } catch (Exception $e) {
                                Mage::log("Error : \n\n" . $e->getMessage() . "\n\n", null, $this->ensureLogPresence($this->customerDumpLog, $this->customerDumpLogInitiated));
                            }

                            $this->customerDumpLogInitiated = true;
                        }
                        $k++;
                        $endTime = time();
                        $this->_writeLine('Delay value: ' . ($delay == 0 ? 'No delay set' : $delay . ' second(s)'));
                        $this->_writeLine('Set batch size: ' . $arraysize);
                        $this->_writeLine('Current batch number: ' . $k);
                        $this->_writeLine('Batch start time: ' . $this->convertUnixTimeToTime($startTime));
                        $this->_writeLine('Batch end time: ' . $this->convertUnixTimeToTime($endTime));
                        $this->_writeLine('Batch processing time: ' . (($endTime - $startTime) < 1 ? 'less than 1' : ($endTime - $startTime)) . ' second(s)');
                        $this->_writeLine('CS API call start time: ' . $this->convertUnixTimeToTime($startCSTime));
                        $this->_writeLine('CS API call end time: ' . (!isset($endCSTime) ? 'No time set' : $this->convertUnixTimeToTime($endCSTime)));
                        $this->_writeLine('CS API call duration: ' . (!isset($endCSTime) ? 'No CS API call end time set' : (($endCSTime - $startCSTime) < 1 ? 'less than 1' : ($endCSTime - $startCSTime)) . ' second(s)'));
                        $this->_writeLine('Total number of records processed: ' . $j);
                        $this->_writeLine('###########################################');
                        $customerBulkData = [];
                        $isExecuted = true;
                        sleep($delay);
                    }
                }

                if (!$isExecuted) {
                    $postData = array();
                    $remainder = 0;

                    foreach ($customerBulkData as $key => $value) {
                        $postData[] = $value;
                        $remainder++;
                    }

                    $startTime = time();
                    $options = [
                        'verify' => $verify,
                        'proxy' => $proxy,
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ],
                        'body' => json_encode($postData),
                    ];
                    if(json_encode($postData) == '' || json_encode($postData) == null)
                    {
                        $this->_writeLine('Empty body');
                        $this->_writeLine(json_encode($postData));
                    }
                    else {
                        try {
                            $startCSTime = time();
                            Mage::log("Request :\n\n" . json_encode($postData) . "\n\n", null, $this->ensureLogPresence($this->customerDumpLog, $this->customerDumpLogInitiated));
                            $client = new Client();
                            $res = $client->request('POST', $api_url, $options);
                            $response = json_decode($res->getBody());
                            Mage::log("Response :\n\n" . $res->getBody() . "\n\n", null, $this->ensureLogPresence($this->customerDumpLog, $this->customerDumpLogInitiated));
                            $endCSTime = time();
                        } catch (Exception $e) {
                            Mage::log("Error : \n\n" . $e->getMessage() . "\n\n", null, $this->ensureLogPresence($this->customerDumpLog, $this->customerDumpLogInitiated));
                        }

                        $this->customerDumpLogInitiated = true;
                    }
                    $k++;
                    $endTime = time();
                    $this->_writeLine('Delay value: ' . ($delay == 0 ? 'No delay set' : $delay . ' second(s)'));
                    $this->_writeLine('Set batch size: ' . $arraysize);
                    $this->_writeLine('Actual batch size: ' . $remainder);
                    $this->_writeLine('Current batch number: ' . $k);
                    $this->_writeLine('Batch start time: ' . $this->convertUnixTimeToTime($startTime));
                    $this->_writeLine('Batch end time: ' . $this->convertUnixTimeToTime($endTime));
                    $this->_writeLine('Batch processing time: ' . (($endTime - $startTime) < 1 ? 'less than 1' : ($endTime - $startTime)) . ' second(s)');
                    $this->_writeLine('CS API call start time: ' . $this->convertUnixTimeToTime($startCSTime));
                    $this->_writeLine('CS API call end time: ' . (!isset($endCSTime) ? 'No time set' : $this->convertUnixTimeToTime($endCSTime)));
                    $this->_writeLine('CS API call duration: ' . (!isset($endCSTime) ? 'No CS API call end time set' : (($endCSTime - $startCSTime) < 1 ? 'less than 1' : ($endCSTime - $startCSTime)) . ' second(s)'));
                    $this->_writeLine('Total number of records processed: ' . $j);
                    $this->_writeLine('###########################################');
                    $isExecuted = true;
                    $customerBulkData = [];
                    sleep($delay);
                }
                $this->_writeLine('File processed');

                fclose($handle);
            }
        } catch (Exception $e) {
            $this->_writeLine('Cannot read file: '.$file);
            Mage::getSingleton('core/logger')->logException($e);
            Zend_Debug::dump($e->getMessage());
        }
        $time_end = time();
        $time = $time_end - $time_start;

        $this->_writeLine('File processing time: ' . ($time < 1 ? 'less than 1' : $time) . ' second(s)');
        $this->_writeLine('###########################################');
    }

    private function _getRemoteFiles()
    {
        $localFiles = array();

        // Connect
        $connectionDetails['host'] = Mage::getStoreConfig('vodafone_service/customer_sync_sftp/hostname');
        $connectionDetails['username'] = Mage::getStoreConfig('vodafone_service/customer_sync_sftp/username');
        $connectionDetails['password'] = Mage::getStoreConfig('vodafone_service/customer_sync_sftp/password');
        $connectionDetails['path'] = Mage::getStoreConfig('vodafone_service/customer_sync_sftp/path');

        $remotePath = $connectionDetails['path'];
        $sftp = new Varien_Io_Sftp();

        $this->_writeLine('Trying to connect to remote server');

        $sftp->open(array(
            'host' => $connectionDetails['host'],
            'username' => $connectionDetails['username'],
            'password' => $connectionDetails['password']
        ));

        $this->_writeLine('Connected to remote server');
        $this->_writeLine('Changing path on remote server');
        $sftp->cd($remotePath);
        $this->_writeLine('Changed path on remote server');
        $this->_writeLine('Fetching file list on remote server');
        $files = $sftp->ls();
        $this->_writeLine('Fetched file list on remote server');
        foreach($files as $file) {
            $this->_writeLine('Handling file ' . $file['text'] . ' on remote server');
            if (substr(strtolower($file['text']), -strlen(CSV_EXT . GUNZIP_EXT)) === CSV_EXT . GUNZIP_EXT && (strpos($file['text'], 'delta_CustomerImport') === 0)) {
                // Handle gunzipped CSV files
                // Skip full until first can be planned, structure changed, so will reimport all, this mus be planned and monitorred

                $localFile = tempnam('/tmp', 'mage_customer_dump_');
                $localDecompressedFile = tempnam('/tmp', 'mage_customer_dump_');
                $this->_writeLine('Created local file ' . $file['text'] . ' to handle ' . $localFile);
                $this->_writeLine('Downloading file ' . $file['text'] . ' to local file ' . $localFile);

                if ($sftp->read($file['text'], $localFile)) { // Download to filesystem
                    $this->_writeLine('Downloaded file ' . $localFile . ' to local file ' . $file['text']);
                    $this->_writeLine('Removing remote file ' . $file['text']);
                    $sftp->mv($remotePath . '/' . $file['text'], $remotePath . '/done/' . date('YmdHis_') . $file['text']); // Move to done folder
                    $this->_writeLine('Moved remote file ' . $file['text']);

                    // Decompress local file
                    $this->_writeLine('Decompressing ' . $localFile);
                    exec('gunzip -c ' . $localFile . ' > ' . $localDecompressedFile);
                    $this->_writeLine('Decompressed local file as ' . $localDecompressedFile);
                    unlink($localFile);

                    $localFiles[] = $localDecompressedFile; // Add if this is a CSV
                } else {
                    $this->_writeLine('Downloading of file ' . $file['text'] . ' to local file ' . $localFile . ' failed!');
                }
            } else {
                $this->_writeLine('Skipped file ' . $file['text'] . ' on remote server, irrelevant file');
            }
        }
        $sftp->close();

        return $localFiles;
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        define('GUNZIP_EXT', '.gz');
        define('CSV_EXT', '.csv');
        define('VERSION', '1.4'); // Change if fullimport needs to be forced

        $this->removeLock();
        try {
            if(!$this->hasLock()) {
                $this->createLock();

                $files = array();
                if ($this->getArg('file')) {
                    $this->_writeLine('Doing local file ' . $this->getArg('file'));
                    $files[] = $this->getArg('file'); // Run specific file locally for testing or test data import
                } elseif ($this->getArg('directory')) {
                    $this->_writeLine('Doing local directory ' . $this->getArg('directory'));
                    $files = array_slice(scandir($this->getArg('directory')),
                        2); // Run specific directory locally for testing or test data import
                    array_walk($files, function (&$value, $key) {
                        $value = $this->getArg('directory') . DIRECTORY_SEPARATOR . $value;
                    });
                } else {
                    try {
                        $this->_writeLine('Fetching remote files');
                        $this->customerImportLogInitiated = true;
                        $files = $this->_getRemoteFiles(); // Get files from server
                    } catch (Exception $ex) {
                        $this->_writeLine('Failed to fetch remote files: ' . $ex->getMessage());
                    }
                }

                foreach ($files as $file) {
                    try {
                        $this->_writeLine('Handling file ' . $file);

                        //checking import type (VF/Ziggo)
                        $type = $this->getArg('type');
                        if(strpos($file, 'VF') !== false)
                            $type = 'VF';
                        if(strpos($file, 'Ziggo') !== false)
                            $type = 'Ziggo';

                        if($type == "") {
                            $this->_writeLine('Invalid import file type (VF/Ziggo)!');
                            continue;
                        }

                        $this->_runFile($file, $type); // Synchronize with file
                        $splitFile = explode(DS, $file);
                        $fileNoPath = array_pop($splitFile);
                        rename($file, Mage::helper("vznl_data/data")->getCustomerImportOldFiles() . $fileNoPath);
                    } catch (Exception $ex) {
                        $this->_writeLine('Failed to handle file ' . $file . ' with message: ' . $ex->getMessage());
                    }
                }
                $this->removeLock();
            } else{
                throw new Exception('Cannot start script due to existing lock: '.$this->getLockPath());
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            Zend_Debug::dump($e->getMessage());
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  help                            This help
  file                            File to read

USAGE;
    }
}

$import = new Vznl_Import_Customers_Cli();
$import->run();