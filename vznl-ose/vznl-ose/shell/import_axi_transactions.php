<?php

// Enable errors to make sure we get all the details in cron logs
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'vznl_abstract.php';

class Vznl_Import_Axi_Transactions_Cli extends Vznl_Shell_Abstract
{

    private $packageStatusTypes = array();
    private $write;
    private $orderExists;
    private $removeNotExitsintgTransaction;
    private $searchBeforeDeleLimit = 10;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_lockExpireInterval = 180; //3m
        parent::__construct();

        // Init NL translations
        Mage::getSingleton('customer/session');
        Mage::getSingleton('core/session');
        Mage::getSingleton('core/translate')->setLocale('nl_NL')->init('frontend');
        $this->write = Mage::getSingleton('core/resource')->getConnection('core_write');
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        $this->packageStatusTypes = array(
            Vznl_Superorder_Model_StatusHistory::PACKAGE_STATUS,
            Vznl_Superorder_Model_StatusHistory::PACKAGE_CC_STATUS,
            Vznl_Superorder_Model_StatusHistory::PACKAGE_ESB_STATUS_CODE,
            Vznl_Superorder_Model_StatusHistory::PACKAGE_PORTING_STATUS,
            Vznl_Superorder_Model_StatusHistory::PACKAGE_VF_STATUS_CODE,
            Vznl_Superorder_Model_StatusHistory::PACKAGE_STOCK_STATUS,
            Vznl_Superorder_Model_StatusHistory::PACKAGE_CREDIT_CODE
        );

        try {
            if (!$this->hasLock()) {
                $this->createLock();

                $start = time();
                $this->_writeLine('Processing AXI transactions');
                $connectionDetails['host'] = Mage::getStoreConfig('vodafone_service/axi_transactions_sftp/hostname');
                $connectionDetails['username'] = Mage::getStoreConfig('vodafone_service/axi_transactions_sftp/username');
                $connectionDetails['password'] = Mage::getStoreConfig('vodafone_service/axi_transactions_sftp/password');

                $sftp = new Varien_Io_Sftp();
                $sftp->open(array(
                    'host' => $connectionDetails['host'],
                    'username' => $connectionDetails['username'],
                    'password' => $connectionDetails['password']
                ));
                $remotePath = Mage::getStoreConfig('vodafone_service/axi_transactions_sftp/path');
                $sftp->cd($remotePath);
                $files = $sftp->ls();
                foreach ($files as $file) {
                    $localFile = tempnam('/tmp', 'mage_axi_transaction_');
                    if ($sftp->read($file['text'], $localFile)) {
                        try {
                            $this->orderExists = false;
                            $this->removeNotExitsintgTransaction = false;
                            $this->_handleAxiTransactionFile($localFile);
                        } catch (Exception $ex) {
                            $this->_writeLine('Error processing transaction ' . $ex->getMessage());
                            unlink($localFile);
                            continue; // Don't remove transaction
                        }
                        if ($this->orderExists || $this->removeNotExitsintgTransaction) {
                            $sftp->rm($remotePath . '/' . $file['text']);
                        }
                    }
                    unlink($localFile);
                }
                $sftp->close();

                Mage::getSingleton('core/resource')->getConnection('core_write')->exec("INSERT INTO dyna_job_runs (`job_name`, `start_date`, `end_date`) VALUES ('" . __CLASS__ . "', '" . $start . "', '" . time() . "')");
                $this->_writeLine('Finished AXI transactions');
                $this->removeLock();
            } else {
                throw new Exception('Cannot start script due to existing lock: ' . $this->getLockPath());
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            Zend_Debug::dump($e->getMessage());
        }
    }

    private function _writeLine($msg)
    {
        Mage::log($msg, null, 'axi_transactions.log');
    }

    private function _handleAxiTransactionFile($file)
    {
        $rawXml = file_get_contents($file);
        if (!empty($rawXml)) {
            $ns = 'http://DynaLeanProcessTransactions/v1-0';
            $dom = new DOMDocument();
            $dom->loadXML($rawXml);
            $transactions = $dom->getElementsByTagNameNS($ns, 'OrderTransactionRequest');
            foreach ($transactions as $transaction) {
                $saleOrderNumber = $this->_getSubNodeVal($transaction, $ns, 'SalesOrdernumber');
                $this->_writeLine("Handling $saleOrderNumber");
                $saleOrderStatus = $this->_getSubNodeVal($transaction, $ns, 'OrderStatus');

                $packages = $transaction->getElementsByTagNameNS($ns, 'Package');

                $so = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('order_number', $saleOrderNumber)->getLastItem();
                if (!$so || !$so->getId()) {
                    $this->_writeLine('Error: Order does not exists');
                    $this->_handleNotExistingOrderTransaction($saleOrderNumber);
                    return;
                }

                $superorderId = $so->getId();

                $this->orderExists = true;
                $newIlsSim = array();
                foreach ($packages as $package) {
                    $packageState = $this->_getSubNodeVal($package, $ns, 'State');
                    $packageId = $this->_getSubNodeVal($package, $ns, 'PackageID');

                    if (empty($packageId)) {
                        $this->_writeLine("Error! Missing PackageID for $saleOrderNumber");
                    }

                    $this->_writeLine("Handling $saleOrderNumber package $packageId");
                    $packageStatus = $this->_getSubNodeVal($package, $ns, 'PackageStatus');
                    $packageLines = $package->getElementsByTagNameNS($ns, 'PackageLine');
                    $saleOrderId = $so->getId();
                    foreach ($packageLines as $packageLine) {
                        $packageLineState = $this->_getSubNodeVal($packageLine, $ns, 'State');
                        $packageLineID = $this->_getSubNodeVal($packageLine, $ns, 'LineID');
                        $packageLineStatus = $this->_getSubNodeVal($packageLine, $ns, 'LineStatus');
                        $packageLineDiscount = $this->_getSubNodeVal($packageLine, $ns, 'Discount');
                        // TODO register discount to orderline
                        $packageLineDeliveryDate = $this->_getSubNodeVal($packageLine, $ns, 'DeliveryDate');
                        $references = $packageLine->getElementsByTagNameNS($ns, 'ProductReference');
                        foreach ($references as $reference) {
                            $referenceID = $this->_getSubNodeVal($reference, $ns, 'ReferenceID');
                            $referenceType = $this->_getSubNodeVal($reference, $ns, 'ReferenceType');

                            $orderCollection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('edited', 0)->addFieldToFilter('superorder_id', $so->getId());
                            foreach ($orderCollection as $order) {
                                // Set reference
                                if ($referenceType == 'msisdn')
                                    $this->_updateOrderLineReferenceData($saleOrderId, $packageId, 'tel_number', $referenceID);
                                if ($referenceType == 'imei')
                                    $this->_updateOrderLineReferenceData($saleOrderId, $packageId, 'imei', $referenceID, $so);
                                if ($referenceType == 'sim' || $referenceType == 'simhn') {
                                    $this->_updateOrderLineReferenceData($saleOrderId, $packageId, 'sim_number', $referenceID);
                                    if ($so->getIsIls()) {
                                        $newIlsSim[] = $referenceID;
                                    }
                                }
                            }
                        }
                    }

                    if ($packageState == Dyna_Package_Model_Package::PACKAGE_STATE_AVAILABLE) {
                        $this->_updateOrderLineReferenceData($saleOrderId, $packageId, 'stock_status', Dyna_Package_Model_Package::ESB_PACKAGE_STOCK_STATUS_RESERVED);
                    }
                }

                /**
                 * InLifeSales special cases:
                 * Perform changeSim and multiSim calls, that require hardware reservation,
                 * only after the hardware (sim-card) has been delivered
                 */
                if ($so->getIsIls() && ($ilsCall = $so->getXmlToBeSent())) {
                    try {
                        $ilsCall = unserialize($ilsCall);
                        $ilsClient = Mage::helper('vznl_inlife/data')->getClient('inlife');
                        Mage::register('job_additional_information', array(
                            'dealer_code' => $ilsCall['dealer_code'],
                            'hardware_order_id' => $ilsCall['hardware_order_id'],
                        ));

                        // Add new simcard to the changeInlifeSim call
                        if ($ilsCall['method'] == 'changeInlifeSim' && $newIlsSim) {
                            $ilsCall['arguments'][2] = reset($newIlsSim);
                        }

                        // Add new simcard to the orderInlifeSim call
                        if ($ilsCall['method'] == 'orderInlifeSim' && $newIlsSim) {
                            $ilsCall['arguments'][1] = reset($newIlsSim);
                        }

                        // Add new simcard to the addInlifeAddon call
                        if ($ilsCall['method'] == 'addInlifeAddon' && $newIlsSim) {
                            foreach ($newIlsSim as $key => $simCard) {
                                if (isset($ilsCall['arguments'][4][$key])) {
                                    $componentSettings = array(
                                        'RLC_SIM card number' => $simCard
                                    );
                                    $ilsCall['arguments'][4][$key]['component_settings'] = $componentSettings;
                                }
                            }
                        }

                        if (in_array($ilsCall['method'], array('changeInlifeSim', 'addInlifeAddon', 'orderInlifeSim'))) {
                            call_user_func_array(array(&$ilsClient, $ilsCall['method']), $ilsCall['arguments']);
                            if ($so->getId()) {
                                $this->write->exec("update superorder set xml_to_be_sent=NULL, error_detail=NULL  where entity_id=" . $so->getId());
                            }
                        }
                    } catch (Exception $ex) {
                        if ($ex->getMessage()) {
                            $this->write->query("update superorder set error_detail=:message where entity_id=:superorder_id", [
                                'message' => $ex->getMessage(),
                                'superorder_id' => $so->getId(),
                            ]);
                        }
                        Mage::log($ex->getMessage(), null, 'ils_failed_async_calls.log');
                        Mage::log($so->getXmlToBeSent(), null, 'ils_failed_async_calls.log'); // Catch and handle the exception, and don't retry every iteration, this is a nice DDoS feature for BSL, but lets not do that for now
                    }
                }

                $payments = $transaction->getElementsByTagNameNS($ns, 'Payment');
                foreach ($payments as $payment) {
                    $paymentID = $this->_getSubNodeVal($payment, $ns, 'PaymentID');
                    $paymentAmount = $this->_getSubNodeVal($payment, $ns, 'Amount');
                    // TODO register payment
                }

                // Confirm superorder was processed in axi
                $this->write->exec("UPDATE superorder SET axi_processed=1 WHERE entity_id=" . $superorderId);
                $this->_writeLine('Setting AXI-processed flag for SO: '.$superorderId);

            }
        }
    }

    private function _getSubNodeVal($node, $ns, $subNodeName)
    {
        return $node->getElementsByTagNameNS($ns, $subNodeName)->item(0)->nodeValue;
    }

    /**
     * Delete transactions for not-existing orders
     * @param string $file
     * @param string $saleOrderNumber
     * @return void
     */
    private function _handleNotExistingOrderTransaction(
        string $saleOrderNumber
    ): void
    {
        $duplicatedLog = Mage::getBaseDir('base') . DS . 'var/log/axi_transactions_duplicates.log';
        $contents = @file_get_contents($duplicatedLog);
        $duplicatesList = [];
        if (!empty($contents)) {
            $duplicatesList = json_decode($contents, true);
        }
        if (!isset($duplicatesList[$saleOrderNumber])) {
            $duplicatesList[$saleOrderNumber] = 0;
        }
        $duplicatesList[$saleOrderNumber]++;
        if ($duplicatesList[$saleOrderNumber] > $this->searchBeforeDeleLimit) {
            $this->removeNotExitsintgTransaction = true;
            $this->_writeLine('Deleting transaction file for order ' . $saleOrderNumber . ' after ' . $this->searchBeforeDeleLimit . ' search tries');
            unset($duplicatesList[$saleOrderNumber]);
        }
        file_put_contents($duplicatedLog, json_encode($duplicatesList));
    }

    /**
     * Update order reference data
     * @param string $saleOrderId
     * @param int $packageNumber
     * @param string $reference
     * @param string $value
     * @param Vznl_Superorder_Model_Superorder|null $so
     * @return void
     */
    private function _updateOrderLineReferenceData(
        string $saleOrderId,
        int $packageNumber,
        $reference,
        $value,
        $so = null
    ): void
    {
        $this->_writeLine('Setting ' . $reference . ' value to ' . $value);
        $this->write->exec("UPDATE catalog_package SET updated_at='" . now() . "', " . $reference . " = '" . addslashes($value) . "' WHERE order_id = '" . $saleOrderId . "' AND package_id = '" . $packageNumber . "'");
        // Send e-mail only in case of IMEI
        if (strtolower($reference) == 'imei' && $so && $so->getId()) {
            try {
                Mage::helper('vznl_esb')->sendGarantMail($so, $packageNumber);
            } catch (Exception $ex) {
                $this->_writeLine('There was an error sending warranty e-mail for order ' . $saleOrderId . ': ' . $ex->getMessage());
                $this->orderExists = false; // do not delete order, just allow to retry
            }
        }
        // Save status change in DB
        if (in_array($reference, $this->packageStatusTypes)) {
            try {
                $package = Mage::getModel('package/package')->getCollection()->addFieldToFilter('package_id', $packageNumber)->addFieldToFilter('order_id', $saleOrderId)->getFirstItem();
                Mage::getModel('superorder/statusHistory')
                    ->setPackageId($package->getId())
                    ->setStatus($value)
                    ->setType($reference)
                    ->setCreatedAt(now())
                    ->save();
            } catch (Exception $ex) {
                $this->_writeLine('There was an error saving status change for order ' . $saleOrderId . ': ' . $ex->getMessage());
                $this->orderExists = false; // do not delete order, just allow to retry
            }
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  help                            This help

USAGE;
    }
}

$import = new Vznl_Import_Axi_Transactions_Cli();
$import->run();
