<?php
/**
 * Php script for getting configuration data from service response
 * Copyright (c) 2017. Dynacommerce B.V.
 */

require_once '../abstract.php';

class Dyna_Cron_GetConfigurationData_Cli extends Mage_Shell_Abstract
{
    public function run()
    {
        echo "[" . date("Y-m-d H:i:s") .  "] Started getConfigurationData call\n";
        /** @var Dyna_Configurator_Model_Configuration $updater */
        $updater = Mage::getModel('dyna_configurator/configuration');
        $updater->updateConfigurationData();
        echo "[" . date("Y-m-d H:i:s") .  "] Executed successfully\n";
    }
}

$convert = new Dyna_Cron_GetConfigurationData_Cli();
$convert->run();
