<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'catalogtables.php';

/**
 * Class Dyna_CleanMultiMapperFromDatabase_Cli
 */
class Dyna_CleanMultiMapperFromDatabase_Cli extends Dyna_CatalogTables_Cli
{
    /**
     * Run database cleanup process
     */
    public function run()
    {
        if (empty(parent::MULTIMAPPERTABLES))
        {
            parent::_writeLine('The multimapperTables constant is empty, cannot continue the script');
            exit;
        }

        $config  = Mage::getConfig()->getResourceConnectionConfig("default_setup");

        parent::_writeLine('Connecting to database');

        $databaseName = $config->dbname;
        if ($databaseName == "magento")
        {
            parent::_writeLine('Database could not be found, cannot continue script');
            exit;
        }

        parent::_writeLine('Starting cleaning of multimapper from ' . $databaseName);
        $cleanupTables = parent::MULTIMAPPERTABLES;

        // Intersecting again with database tables
        $allTables = parent::getConnection()->fetchCol("show tables");
        $cleanupTables = array_intersect($cleanupTables, $allTables);

        if ($this->cleanTables($cleanupTables))
        {
            parent::_writeLine('Cleaning succeeded for multimapper of ' . $databaseName);
        }
        else
        {
            parent::_writeLine('Cannot clean multimapper of ' . $databaseName);
        }
    }

    /**
     * Truncate tables and recreate default categories in one step
     *
     * @param array $tables
     *
     * @return bool|mixed
     */
    protected function cleanTables(array $tables = [])
    {
        if (!empty($tables))
        {
            $truncateQuery = 'SET FOREIGN_KEY_CHECKS = 0;' . PHP_EOL;
            foreach ($tables as $exportTable)
            {
                $truncateQuery .= sprintf('TRUNCATE TABLE %s;' . PHP_EOL, $exportTable);
            }
            $truncateQuery .= 'SET FOREIGN_KEY_CHECKS = 1;' . PHP_EOL;
            echo $truncateQuery;
            return parent::executeSql($truncateQuery);
        }
        return false;
    }
}

// Example
//php cleanmultimapperfromdatabase.php

$import = new Dyna_CleanMultiMapperFromDatabase_Cli();
$import->run();
