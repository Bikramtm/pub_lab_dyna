<?php

require_once 'abstract.php';

class Clear_Test_Data extends Mage_Shell_Abstract
{
    protected $params = array();

    protected $colors = array(
        'green' => "\e[1;32m",
        'yellow' => "\e[1;33m",
        'red' => "\e[1;31m",
        'white' => "\e[0m",
    );

    protected $dataToBeCleared = array(
        'all',
        'agents',
        'dealers',
        'dealerGroups',
        'customers',
        'quotes',
        'packages',
        'offers',
        'shoppingCarts',
        'orders',
        'superorders',
        'sapOrders',
    );

    public function run()
    {
        if (empty($this->_args)) {
            echo $this->usageHelp();
        } else {
            // Getting args from cli
            $args = array_keys($this->_args);

            if (strtolower(current($args)) == "all") {
                // Removing "all" entry
                $toBeCleared = array_diff($this->dataToBeCleared, array("all"));
            } else {
                // If not empty, matching args against predefined options
                $toBeCleared = array_intersect($args, $this->dataToBeCleared);
                // If args not found in predefined, noticing frontend that irrelevant args have been passed
                $irrelevantArgs = array_diff($args, $this->dataToBeCleared);
                if (!empty($irrelevantArgs)) {
                    foreach ($irrelevantArgs as $arg) {
                        echo "Passed option that contains typo or is not yet implemented: \e[1;31m" . $arg . "\e[0m", "\n";
                    }
                }
            }

            // Proceeding with clearing
            foreach ($toBeCleared as $option) {
                switch ($option) {
                    case "agents" :
                        $this->removeAgents();
                        break;
                    case "dealers" :
                        $this->removeDealers();
                        break;
                    case "dealerGroups" :
                        $this->removeDealerGroups();
                        break;
                    case "customers" :
                        $this->removeCustomers();
                        break;
                    case "quotes" :
                        $this->removeQuotes();
                        break;
                    case "offers" :
                        $this->removeOffers();
                        break;
                    case "shoppingCarts" :
                        $this->removeShoppingCarts();
                        break;
                    case "packages" :
                        $this->removePackages();
                        break;
                    case "orders" :
                        $this->removeOrders();
                        break;
                    case "superorders" :
                        $this->removeSuperorders();
                        break;
                    case "sapOrders" :
                        $this->removeSaporders();
                        break;
                    default :
                        // Unreachable statement
                        break;
                }
            }
        }
    }

    /**
     * Removing agents from database except the default added one
     * @return $this;
     */
    protected function removeAgents()
    {
        $this->echoMessage("Removing all agents from database except demo-agent.");
        $this->getConnection()->query("delete from agent where username<>:userName",
            array('userName' => 'demo-agent'));
        return $this;
    }

    /**
     * Removing agents from database except the default added one
     * @return $this;
     */
    protected function removeDealers()
    {
        $dealerId = $this->getConnection()->fetchOne("select dealer_id from agent where username=:userName",
            array('userName' => 'demo-agent'));
        $this->echoMessage("Removing all dealers from database except default dealer id: " . $dealerId . ". Otherwise default agent cannot log in.");
        $this->getConnection()->query("delete from dealer where dealer_id<>:dealerId",
            array('dealerId' => $dealerId));
        return $this;
    }

    /**
     * Removing dealer groups from database except the default added one
     * @return $this;
     */
    protected function removeDealerGroups()
    {
        $this->echoMessage("Removing all dealers groups and their links to dealer from database.");
        $this->getConnection()->query("truncate table dealer_group_link", array());
        $this->getConnection()->query("delete from dealer_group", array());
        $this->getConnection()->query("alter table dealer_group auto_increment = 1;", array());
        return $this;
    }

    /**
     * Removing customers from database
     * @return $this;
     */
    protected function removeCustomers()
    {
        $this->echoMessage("Removing all customers from database with foreign keys on.");
        $this->getConnection()->query("delete from customer_entity", array());
        $this->getConnection()->query("alter table customer_entity auto_increment = 1;", array());
        return $this;
    }

    /**
     * Removing offers from database
     * @return $this;
     */
    protected function removeOffers()
    {
        $this->echoMessage("Removing all saved offers from database with foreign keys on.");
        $this->getConnection()->query("delete from sales_flat_quote where is_offer=:isOffer", array('isOffer' => 1));

        return $this;
    }

    /**
     * Removing offers from database
     * @return $this;
     */
    protected function removePackages()
    {
        $this->echoMessage("Removing all packages from database with foreign keys on.");
        $this->getConnection()->query("delete from catalog_package", array('isOffer' => 1));
        $this->getConnection()->query("alter table catalog_package auto_increment = 1;", array());

        return $this;
    }

    /**
     * Removing shopping carts from database
     * @return $this;
     */
    protected function removeShoppingCarts()
    {
        $this->echoMessage("Removing all saved shopping carts from database with foreign keys on.");
        $this->getConnection()->query("delete from sales_flat_quote where cart_status is not null", array());

        return $this;
    }

    /**
     * Removing quotes from database
     * @return $this;
     */
    protected function removeQuotes()
    {
        $this->echoMessage("Removing all saved quotes from database with foreign keys on.");
        $this->getConnection()->query("delete from sales_flat_quote", array());
        $this->getConnection()->query("alter table sales_flat_quote auto_increment = 1;", array());

        return $this;
    }

    /**
     * Removing orders from database
     * @return $this;
     */
    protected function removeOrders()
    {
        $this->echoMessage("Removing orders from database with foreign keys on.");
        $this->getConnection()->query("delete from sales_flat_order", array());
        $this->getConnection()->query("alter table sales_flat_order auto_increment = 1;", array());

        return $this;
    }

    /**
     * Removing superorders from database
     * @return $this;
     */
    protected function removeSuperorders()
    {
        $this->echoMessage("Removing superorders from database with foreign keys on.");
        $this->getConnection()->query("delete from superorder", array());
        $this->getConnection()->query("alter table superorder auto_increment = 1;", array());

        return $this;
    }

    /**
     * Removing SAP orders from database
     * @return $this;
     */
    protected function removeSaporders()
    {
        $this->echoMessage("Removing SAP orders from database with foreign keys on.");
        $this->getConnection()->query("delete from sap_orders", array());
        $this->getConnection()->query("alter table sap_orders auto_increment = 1;", array());

        return $this;
    }

    protected function getConnection()
    {
        return Mage::getSingleton('core/resource')
            ->getConnection("core_write");
    }
    
    protected function echoMessage(string $message, $color = "green")
    {
        echo $this->colors[$color], $message, $this->colors['white'], PHP_EOL;
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        $dataClear = implode(" ", $this->dataToBeCleared);

return <<<USAGE
        
Usage:  \e[1;32mphp clearTestData.php -- [options]\e[0m
    Options can be one or more separated by space of the following: \e[1;33m{$dataClear}\e[0m

    \e[1;31mIf option <all> is provided, all above data will be cleared\e[0m\n

USAGE;
    }
}

$shell = new Clear_Test_Data();
$shell->run();