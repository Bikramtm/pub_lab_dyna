<?php

require_once Mage::getModuleDir('controllers', 'Omnius_Bundles') . DS . '/Adminhtml/BundlesController.php';

class Dyna_Bundles_Adminhtml_BundlesController extends Omnius_Bundles_Adminhtml_BundlesController
{
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                list($model, $data, $deleteImage) = $this->_updateBundleData($data);
                if ($model->getId()) {
                    // When an existing item is changed, only update image if changes have been performed
                    if ($deleteImage) {
                        $model->deleteImage(null);
                    } elseif (isset($_FILES['image']['name']) && file_exists($_FILES['image']['tmp_name']) && $_FILES['image']['error'] == 0) {
                        // When a new image request is received
                        $model->uploadImage();
                    }
                } else {
                    // Image is not mandatory, only upload if present in the request
                    if (isset($_FILES['image']['name']) && file_exists($_FILES['image']['tmp_name']) && $_FILES['image']['error'] == 0) {
                        $model->uploadImage();
                    }
                }
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('The bundle template was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['entity_id' => $model->getEntityId()]);

                    return;
                }
            } catch (Exception $e) {
                $this->_throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     */
    private function _throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
    }
}
