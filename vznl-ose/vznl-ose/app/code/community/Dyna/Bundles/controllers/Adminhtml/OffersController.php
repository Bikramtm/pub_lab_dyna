<?php

require_once Mage::getModuleDir('controllers', 'Omnius_Bundles') . DS . 'Adminhtml' . DS . 'BundlesController.php';

class Dyna_Bundles_Adminhtml_OffersController extends Omnius_Bundles_Adminhtml_BundlesController
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('bundles/offers')
            ->_addBreadcrumb(Mage::helper('bundles')->__('Manage Offers'),
                Mage::helper('bundles')->__('Manage Offers'));

        return $this;
    }

    /**
     * Display the admin grid (table) with all the announcements
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Initialize the addition of a new record
     */
    public function newAction()
    {
        $this->_title(Mage::helper('bundles')->__('New offer'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('dyna_bundles/campaignOffer')->load($id);
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('bundle_offers_data', $model);
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('dyna_bundles/adminhtml_offers_edit'))
            ->_addLeft($this->getLayout()->createBlock('dyna_bundles/adminhtml_offers_edit_tabs'));

        $this->renderLayout();
    }

    /**
     * Trigger edit mode for a record
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $offer = Mage::getModel('dyna_bundles/campaignOffer')->load($id);

        if ($offer->getId()) {
            $relationIds = $this->checkRelationWithCampaigns($offer->getId());

            $offer->setData('campaign_ids',$relationIds);
            Mage::register('bundle_offers_data', $offer);
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addLeft($this->getLayout()->createBlock('dyna_bundles/adminhtml_offers_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Item does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                unset($data['form_key']);
                $offerId = $this->getRequest()->getParam('id');
                /** @var Dyna_Bundles_Model_CampaignOffer $model */
                $offer = Mage::getModel('dyna_bundles/campaignOffer')
                    ->addData($data)
                    ->setId($offerId)
                    ->save();

                if(!empty($data['campaign_ids'])){
                    $featureRelations = $data['campaign_ids'];
                    $actualRelations = $this->checkRelationWithCampaigns($offer->getId());

                    $toAdd = array_diff($featureRelations, $actualRelations);
                    $toRemove = array_diff($actualRelations, $featureRelations);

                    foreach ($toAdd as $campaignId){
                        Mage::getModel('dyna_bundles/campaignOfferRelation')->addData([
                            'campaign_id' => $campaignId,
                            'offer_id' => $offer->getId()
                        ])->save();
                    }

                    foreach ($toRemove as $campaignId){
                        if(($key = array_search($campaignId, $actualRelations)) !== false) {
                            Mage::getModel('dyna_bundles/campaignOfferRelation')->load($key)->delete();
                        }
                    }
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('The offer was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $offer->getId()]);

                    return;
                }
            } catch (Exception $e) {
                $this->_throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id")) {
            $option = Mage::getModel("dyna_bundles/campaignOffer")->load($this->getRequest()->getParam("id"));
            $option->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('Offer successfully deleted.'));
            Mage::getSingleton('adminhtml/session')->setFormData(false);
            Mage::unregister("bundle_offer_data");
        }

        $this->_redirect("*/*/");
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     */
    private function _throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
    }

    /**
     * @param $offerId
     * @param $campaignId
     * @return null
     */
    private function checkRelationWithCampaigns($offerId, $campaignId = null)
    {
        $relationModel = Mage::getModel('dyna_bundles/campaignOfferRelation');
        $ids = [];
        $relations = $relationModel->getCollection()
            ->addFieldToFilter('offer_id', $offerId);
        if(!empty($campaignId)){
            $relations->addFieldToFilter('campaign_id', $campaignId);
        }
        $relations->load();
        if(!empty($relations)){

            foreach ($relations as $relation){
                $ids[$relation->getId()] = $relation->getCampaignId();
            }
        }

        return $ids;
    }
}
