<?php

/**
 * Class Dyna_Bundles_IndexController
 */
class Dyna_Bundles_IndexController extends Mage_Core_Controller_Front_Action
{

    /**
     * Create bundle and updated cart with customer install base products
     * configurator/cart/createBundle
     */
    public function createAction()
    {
        if ($this->getRequest()->isPost()) {
            // get post data
            $bundleId = $this->getRequest()->getPost('bundleId');
            $parentAccountNumber = $this->getRequest()->getPost('subscriptionNumber') ?: null;
            $serviceLineId = $this->getRequest()->getPost('productId') ?: null;
            // this tells us whether or not we should create an inter stack bundle or not
            $interStackPackageId = $this->getRequest()->getPost('interStackPackageId') ?: null;

            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = $this->getCart()->getQuote();

            // if no interstack package id received through request, than try to determine based on parent account number and service line id
            if (!$interStackPackageId && $package = $quote->getInstallBasePackage($parentAccountNumber, $serviceLineId)) {
                $interStackPackageId = $package->getPackageId();
            }

            /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
            $bundlesHelper = Mage::helper('dyna_bundles');

            // preserving initial active package in order to notify frontend that active package has changed and trigger initConfigurator
            $initialPackageId = $quote->getActivePackageId();

            // if no valid bundle id received, notify frontend
            /** @var Dyna_Bundles_Model_BundleRule $bundle */
            $bundle = Mage::getModel('dyna_bundles/bundleRule')->load($bundleId);
            if (!$bundle->getId()) {
                $response = [
                    'error' => true,
                    'url' => $this->__("There was an error loading requested bundle"),
                ];

                $this->jsonResponse($response);
            }

            try {
                switch (true) {
                    case $bundle->isRedPlus():
                        $activePackageId = $bundle->createRedPlusBundle($parentAccountNumber, $serviceLineId);
                        break;
                    case $bundle->isSuso():
                        $activePackageId = $bundle->createSusoBundle();
                        break;
                    case $interStackPackageId != null:
                        $activePackageId = $bundle->createInterStackBundle($interStackPackageId);
                        break;
                    default:
                        // Fall back in creating the wave1 bundle with an install base product
                        $activePackageId = $bundle->createLegacyBundle($parentAccountNumber, $serviceLineId);
                        break;
                }

                Mage::getSingleton('checkout/cart')->save();

                $response = [
                    'error' => false,
                    'rightBlock' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml(),
                    'initNewPackage' => $activePackageId !== $initialPackageId ? $activePackageId : 0,
                    'eligibleBundles' => Mage::helper('dyna_bundles')->getFrontendEligibleBundles(),
                ];
                $response['optionsPanel'] = [];
                if ($bundlesHelper->isBundleChoiceFlow()) {
                    $response['optionsPanel'] = $this->getLayout()->createBlock('dyna_bundles/options')->setTemplate('bundles/options.phtml');
                }

                $this->jsonResponse($response);
            } catch (Mage_Core_Exception $e) {
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $e->getMessage(),
                ));
            }
        }
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart|Mage_Core_Model_Abstract
     */
    protected function getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = Mage::helper('omnius_core')->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);
    }

    /**
     */
    public function hintAction()
    {
        if ($this->getRequest()->isPost()) {

            try {
                $eligibleBundles = Mage::helper('bundles')->getFrontendEligibleBundles(true);

                $html = Mage::getSingleton('core/layout')
                    ->createBlock('dyna_bundles/hint')
                    ->setEligibleBundles($eligibleBundles)
                    ->setTemplate('bundles/hint_content.phtml')
                    ->toHtml();

                $response = [
                    'html' => $html,
                    // this has to be rebuilt
                    'bundleProductsIds' => array()
                ];

                return $this->jsonResponse($response);
            } catch (Exception $e) {
                return $this->jsonResponse(array(
                    'error' => true,
                    'message' => $e->getMessage(),
                ));
            }
        }
    }

    public function getAdditionalBundleInfoAction()
    {
        $selectedBundle = $this->getRequest()->get('bundle_id');
        // this tells us whether or not we should create an inter stack bundle or not
        $productId = $this->getRequest()->get('product_id') ?: null;
        // ctn/customernumber
        $customerNumber = $this->getRequest()->get('customer_number') ?: null;

        $selectedInstalledBaseProductEntityId = $this->getRequest()->get('entity_id') ?: null;

        $interStackPackageId = $this->getRequest()->getParam('inter_stack_package_id') ?: null;

        /** @var Dyna_Bundles_Model_BundleRule $bundle */
        $bundle = Mage::getModel('dyna_bundles/bundleRule')->load($selectedBundle);
        if (!$bundle->getId()) {
            $response = [
                'error' => true,
                'url' => $this->__("There was an error loading requested bundle"),
            ];

            $this->jsonResponse($response);
        }

        $bundleProductsSummary = $bundle->getSummaryBundleDetails($customerNumber, $productId, $selectedInstalledBaseProductEntityId, $interStackPackageId);

        $packagesModelArray = Mage::getModel('dyna_package/packageType')->getCollection()->toArray()['items'];
        $packagesNames = [];

        foreach ($packagesModelArray as $package) {
            if (in_array(strtolower($package['package_code']), array_keys(!empty($bundleProductsSummary['products']) ? $bundleProductsSummary['products'] : array()))) {
                $packagesNames[strtolower($package['package_code'])] = $package['front_end_name'];
            }
        }

        $response = [
            'bundleProducts' => $bundleProductsSummary['products'],
            //'totals' => $bundleProductsSummary['totals'],
            'packageNames' => $packagesNames
        ];

        $this->jsonResponse($response);
    }

    private function getInstallBaseProductsForEligibleBundles($eligibleBundles, $allInstallBaseProducts)
    {
        // build install base packages based on eligible bundles
        $installedBasePackages = array();

        // check if any of the customer's subscriptions are present in the list of eligible bundles. save those that are
        foreach ($eligibleBundles as $eligibleBundle) {
            // only keep "legacy" bundles
            if ($eligibleBundle['addButtonInSection'] == strtolower(Dyna_Bundles_Model_BundleRule::LOCATION_DRAW)) {
                foreach ($allInstallBaseProducts as &$installBaseProduct) {
                    // identifiers of install base subscription
                    $customerNumber = $installBaseProduct['contract_id'];
                    $productId = $installBaseProduct['product_id'];

                    foreach ($eligibleBundle['targetedSubscriptions'] as $targetedSubscription) {
                        if ($targetedSubscription['customerNumber'] == $customerNumber
                            && $targetedSubscription['productId'] == $productId
                        ) {
                            $installBaseProduct['bundle_ids'][] = $eligibleBundle['bundleId'];
                        }
                    }
                }
            }
        }

        foreach ($allInstallBaseProducts as $installBaseProduct) {
            if (!empty($installBaseProduct['bundle_ids'])) {
                $installedBasePackages[] = $installBaseProduct;
            }
        }

        return $installedBasePackages;
    }

    /**
     * The package that is not the active package will be considered the "base products" package.
     * We will retrieve the subscription product from this package to be shown as the "base" product
     * The "base" product is displayed in the left of the hint drawer
     */
    private function getInterStackBaseProducts($eligibleBundles, $quote)
    {
        // build install base packages based on eligible bundles
        $installedBasePackages = array();

        $activePackageId = $quote->getActivePackageId();
        foreach ($eligibleBundles as $eligibleBundle) {
            if ($eligibleBundle['addButtonInSection'] == strtolower(Dyna_Bundles_Model_BundleRule::LOCATION_DRAW) &&
                $eligibleBundle['targetedPackagesInCart']) {
                foreach ($eligibleBundle['targetedPackagesInCart'] as $bundleCartPackage) {
                    if ($bundleCartPackage['packageId'] == $activePackageId) {
                        continue;
                    }
                    $items = $quote->getPackageItems($bundleCartPackage['packageId']);
                    // Determine the Tariff product in quote package
                    foreach ($items as $item) {
                        $subscriptionProduct['product_sku'] = $item->getSku();
                        $subscriptionProduct['product_package_subtype'] = $item->getProduct()->getType();
                        $subscriptionProduct['is_subscription'] = $item->getProduct()->isSubscription();
                        $subscriptionProduct['title'] = $item->getName();
                        $installedBasePackage['products'][] = $subscriptionProduct;
                        $installedBasePackage['bundle_ids'][] = $eligibleBundle['bundleId'];
                        $installedBasePackage['package_id'] = $installedBasePackage['index_id'] = $item->getPackageId();
                        $installedBasePackage['package_type'] = $item->getPackageType();
                        $installedBasePackage['product_id'] = $item->getProductId();
                        $installedBasePackages[] = $installedBasePackage;
                        break;
                    }
                }
            }
        }

        return $installedBasePackages;
    }

    public function fetchMandatoryFamiliesAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $mandatoryFamilies = array_values(Mage::helper("bundles")->getMandatoryFamilies());
        return $this->jsonResponse($mandatoryFamilies);
    }

    /**
     * Saves the selected bundle choices
     *
     * Previously selected bundle choices are removed
     * Promos on previously selected choices are removed
     * Promo bundle rules are reevaluated to add promos on the new bundle choices.
     */
    public function updateBundleChoiceAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this;
        }

        $selectedBundleChoiceProducts = $this->getRequest()->get('products');
        $bundleId = $this->getRequest()->get('bundleId');
        $skus = array_values($selectedBundleChoiceProducts);

        if (!empty($skus)) {
            /** @var Dyna_Package_Model_Package $package */
            $package = $this->getCart()->getQuote()->getCartPackage();
            /** @var Dyna_Bundles_Model_Expression_QuotePackageScope $quoteScope */
            $quoteScope = Mage::getModel("dyna_bundles/expression_quotePackageScope", ['package' => $package, 'bundle' => $package->getBundleById($bundleId)]);
            $items = $package->getItems();

            try {
                $this->validateBundleChoice();
            } catch (Exception $exception) {
                $exceptionMessage = [
                    'error' => true,
                    'message' => $exception->getMessage(),
                    'currentBundleChoices' => $exception->bundleChoiceProducts
                ];
                return $this->jsonResponse($exceptionMessage);
            }
            // Remove old bundle choices
            $targetProductIds = array();
            foreach ($items as $item) {
                if ($item->getBundleChoice()) {
                    $this->getCart()->removeItem($item->getId());
                    $targetProductIds[] = $item->getProductId();
                }
            }

            // Remove all promo items that were added by bundle rules when the parent product is removed
            if (count($targetProductIds)) {
                foreach ($items as $item) {
                    if (in_array($item->getTargetId(), $targetProductIds) && $item->isBundlePromo()) {
                        $this->getCart()->removeItem($item->getId());
                    }
                }
            }

            $quoteScope->add(...$skus);
            foreach ($skus as $sku) {
                $productId = Mage::getModel('catalog/product')->getResource()->getIdBySku($sku);
                $product = Mage::getModel('catalog/product')->load($productId);
                $quoteScope->markProductAsBundleChoice($product);
            }

            // Reevaluates addPromoProduct rules, to support adding promos on bundle choices
            $activeBundleRule = $package->getBundleById($bundleId);

            foreach ($package->getPackagesInSameBundle($bundleId, true) as $sibling) {
                if ($sibling->getEditingDisabled()) {
                    $ruleParser = Mage::helper('bundles')->buildBundlePromoRuleParserFromBundleRule($activeBundleRule, $sibling, $package);
                    $ruleParser->parse();
                }
            }

            $this->getCart()->save();
        }

        $quote = $this->getCart()->getQuote();
        $packageId = $quote->getActivePackageId();
        $cartStatusResponse = $this->getCartStatusResponse($quote->getId(), $packageId);

        return $this->jsonResponse($cartStatusResponse);
    }

    protected function validateBundleChoice()
    {
        $websiteId = Mage::app()->getWebsite()->getId();
        $selectedBundleChoiceProducts = $this->getRequest()->get('products');
        $skus = array_values($selectedBundleChoiceProducts);
        $package = $this->getCart()->getQuote()->getCartPackage();
        $items = $package->getAllItems();

        foreach ($items as $item) {
            if (!$item->getBundleChoice()) {
                $productIdsWithoutBundleChoice[] = $item->getProductId();
            } else {
                $bundleChoiceProducts[] = $item->getProductId();
            }
        }
        /** @var Dyna_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel('catalog/product');
        $idsToBeAdded = array_values($productModel->getIdsBySkus($skus));
        $validProducts = Mage::helper('dyna_configurator/cart')->getCartRules(array_merge($productIdsWithoutBundleChoice, $idsToBeAdded), [], $websiteId, false, $package->getType());
        $invalidProducts = array_diff($idsToBeAdded, $validProducts);
        /** @var Mage_Catalog_Model_Resource_Product_Collection $invalidProductsCollection */
        $invalidProductsCollection = $productModel
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('display_name_configurator')
            ->addAttributeToFilter('entity_id', array('finset' => $invalidProducts))
        ;
        if (count($invalidProductsCollection)) {
            $invalidProductSkuNames = [];
            foreach ($invalidProductsCollection->getItems() as $product) {
                $invalidProductSkuNames[] = '[' . $product->getSku() . '] ' . $product->getDisplayNameConfigurator() ?: $product->getName();
            }
            $exception = new Exception(sprintf(
                '%s.<br/><strong>%s:</strong> %s',
                $this->__("The following products cannot be added to the cart because they don't have allowed rules"),
                $this->__("Invalid"),
                join(', ', $invalidProductSkuNames)
            ));
            $exception->bundleChoiceProducts = $bundleChoiceProducts;
            $exception->bundleChoiceProducts = $bundleChoiceProducts;
            throw $exception;
        }


    }

    /**
     * @TODO: refactor out
     * @param $quoteId
     * @param $activePackageId
     * @return array
     */
    protected function getCartStatusResponse($quoteId, $activePackageId)
    {
        $rightBlockCollapsed = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml');

        $rightBlock = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml');
        $bodyResponse = array(
            'error' => false,
            'message' => $this->__('Bundle choice updated.'),
            'totals' => $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
            'rightBlock' => $rightBlock->setCurrentPackageId($activePackageId)->toHtml(),
            'rightBlockCollapsed' => $rightBlockCollapsed->setCurrentPackageId($activePackageId)->toHtml(),
            'activePackageId' => $activePackageId
        );

        return $bodyResponse;
    }
}
