<?php

require_once Mage::getModuleDir('controllers', 'Omnius_Bundles') . DS . 'Adminhtml' . DS . 'BundlesController.php';

class Dyna_Bundles_Adminhtml_RulesController extends Omnius_Bundles_Adminhtml_BundlesController
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('bundles/rules')
            ->_addBreadcrumb(Mage::helper('bundles')->__('Manage Bundle Rules'),
                Mage::helper('bundles')->__('Manage Bundle Rules'));

        return $this;
    }

    /**
     * Display the admin grid (table) with all the announcements
     */
    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Initialize the addition of a new record
     */
    public function newAction()
    {
        $this->_title(Mage::helper('bundles')->__('New bundle rule'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('dyna_bundles/bundleRule')->load($id);
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('bundle_rules_data', $model);
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('dyna_bundles/adminhtml_rules_edit'))
            ->_addLeft($this->getLayout()->createBlock('dyna_bundles/adminhtml_rules_edit_tabs'));

        $this->renderLayout();
    }


    /**
     * Trigger edit mode for a record
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('dyna_bundles/bundleRule')->load($id);

        if ($model->getId()) {
            Mage::register('bundle_rules_data', $model);
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addLeft($this->getLayout()->createBlock('dyna_bundles/adminhtml_rules_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Item does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                unset($data['form_key']);

                /** @var Dyna_Bundles_Model_Campaign $model */
                $model = Mage::getModel('dyna_bundles/bundleRule')
                    ->addData($data)
                    ->setId($this->getRequest()->getParam('id', null))
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('The bundle rule was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                Mage::unregister("bundle_rules_data");

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $model->getId()]);
                    return;
                }
            } catch (Exception $e) {
                $this->_throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Unable to find rule to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id")) {
            $option = Mage::getModel("dyna_bundles/bundleRule")->load($this->getRequest()->getParam("id"));
            $option->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('Bundle rule successfully deleted.'));
            Mage::getSingleton('adminhtml/session')->setFormData(false);
            Mage::unregister("bundle_rules_data");
        }

        $this->_redirect("*/*/");
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     */
    private function _throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
    }
}
