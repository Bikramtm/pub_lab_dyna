<?php

class Dyna_Bundles_Helper_Data extends Omnius_Bundles_Helper_Data
{
    CONST INSTALL_BASE_CUSTOMER_NUMBER = "customerNumber";
    CONST INSTALL_BASE_PRODUCT_ID = "productId";
    CONST INSTALL_BASE_NR_MEMBERS = "numberOfMembers";
    // constants used for identifying expression language evaluation
    CONST EXPRESSION_INSTALL_BASE_PRODUCT = "installBaseProduct";
    CONST EXPRESSION_PACKAGES_RESULT = "validPackages";
    CONST EXPRESSION_PACKAGES_STACK = "filterStack";
    CONST EXPRESSION_REQUIRED_PRODUCTS = "expressionRequiredProducts";
    CONST EXPRESSION_HIGHLIGHT_PACKAGES = "highlightPackages";

    CONST EXPRESSION_SCOPE_INSTALL_BASE = "installBase";
    const PROVIS_STATUS_AK = 'AK';
    protected $activeBundles = false;
    /** @var Dyna_Bundles_Model_BundleRule */
    protected $currentBundle = null;
    protected $simulation = false;

    // store salesId error
    protected $salesIdError = '';

    /**
     * Return current the bundle that is currently evaluating
     * @return Dyna_Bundles_Model_BundleRule
     */
    public function getCurrentBundle()
    {
        return $this->currentBundle;
    }

    /**
     * Set current the bundle on this helper
     * @return $this
     */
    public function setCurrentBundle(Dyna_Bundles_Model_BundleRule $bundleRule)
    {
        $this->currentBundle = $bundleRule;
        return $this;
    }

    public function isSimulation()
    {
        return $this->simulation;
    }

    /**
     * @param $transactionid
     * @return array
     */
    public function getCallerData($transactionid)
    {
        $data = Mage::getSingleton('dyna_customer/session')->getUctParams();
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $carts = $customer->getCustomerNumber() ? $customer->getShoppingCartsForCustomer(null, $customer->getCustomerNumber()) : array();
        $offer_id = null;
        foreach ($carts as $key => $cart) {
            if ($cart['cart'][0]->getIsOffer() || $cart['cart'][0]->getCartStatus() == Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED) {
                $offer_id = $key;
                break;
            }
        }
        $campaignId = $data['campaignId'];
        $callingNumber = $data['callingNumber'];
        $Idwithtrans = $data['Idwithtrans'];

        $campaign = Mage::getModel('dyna_bundles/campaign')->load($campaignId, 'campaign_id');
        $dateModel = Mage::getSingleton('core/date');
        $timeNow = date('Y-m-d', $dateModel->timestamp(time()));
        $timeFrom = date('Y-m-d', $dateModel->timestamp(time()));
        $timeTo = date('Y-m-d', $dateModel->timestamp(time()));
        if ($campaign && $campaign->getId()) {
            $timeFrom = date('Y-m-d', $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ? $campaign->getValidFrom() : time())));
            $timeTo = date('Y-m-d', $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ? $campaign->getValidTo() : time())));
        }

        if (!$campaign || ($timeNow < $timeFrom || $timeNow > $timeTo)) {
            $campaign = $this->retrieveDefaultCampaign();
            if($campaign){
                $campaignId = $campaign->getId();
                $timeFrom = date('Y-m-d', $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ? $campaign->getValidFrom() : time())));
                $timeTo = date('Y-m-d', $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ? $campaign->getValidTo() : time())));
            }
        }

        Mage::getSingleton('dyna_customer/session')->setUctParams(array(
            'callingNumber' => $callingNumber,
            'campaignId' => $campaignId,
            'transactionId' => $transactionid,
            'Idwithtrans' => $Idwithtrans,
            'success' => true
        ));

        if ($campaign && $campaign->getId() && $timeNow >= $timeFrom && $timeNow <= $timeTo) {
            $data = [
                'ctn' => $callingNumber,
                'campaign_id' => $campaign->getId() ?: 0,
                'offer_id' => $offer_id,
                'cluster_category' => $campaign->getData('cluster_category'),
                'cluster' => $campaign->getData('cluster'),
                'campaign_hint' => $campaign->getData('campaign_hint'),
                'offer_hint' => $campaign->getData('offer_hint'),
                'timeFrom' => $timeFrom,
                'timeTo' => $timeTo,
                'promotion_hint' => $campaign->getData('promotion_hint'),
            ];
        } else {
            $data = [
                'ctn' => $callingNumber,
                'campaign_id' => 0,
            ];
        }
        return $data;
    }

    /**
     * @param $campaignId
     * @param $callerNumber
     * @return array
     */
    public function getCampaignData($campaignId, $callingNumber, $page = 1, $pageSize = 2)
    {
        /** @var Dyna_Bundles_Model_Campaign $campaign */
        $uctParams = Mage::getSingleton('dyna_customer/session')->getUctParams();
        $displayDefault = false;
        $displayChange = false;

        $campaign = Mage::getModel('dyna_bundles/campaign')->load($campaignId, 'campaign_id');
        $dateModel = Mage::getSingleton('core/date');
        $timeNow = date('Y-m-d', $dateModel->timestamp(time()));
        if ($campaign && $campaign->getId()) {
            $timeFrom = date('Y-m-d', $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ? $campaign->getValidFrom() : time())));
            $timeTo = date('Y-m-d', $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ? $campaign->getValidTo() : time())));
        }
        $campaigns = [];
        if (!$campaign || (isset($timeFrom) && isset($timeTo) && ($timeNow < $timeFrom || $timeNow > $timeTo))) {
            $campaign = $this->retrieveDefaultCampaign();
            if($campaign){
                $timeFrom = date('Y-m-d', $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ? $campaign->getValidFrom() : time())));
                $timeTo = date('Y-m-d', $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ? $campaign->getValidTo() : time())));
            }
        }

        if (!$campaign || !$campaign->getId() || ($timeNow < $timeFrom || $timeNow > $timeTo)) {
            return ['campaigns' => $campaigns];
        }

        if ($uctParams["campaignId"]!=$uctParams["Idwithtrans"]) {
            $displayDefault = true;
            $defaultCampaign_Count = $this->getDefaultCampaignCount();

            if ($defaultCampaign_Count > 1) {
                $displayChange = true;
            }
        }
        /** @var Omnius_Package_Model_Mysql4_Package $packageCollection */
        $packageCollection = Mage::getModel('package/package')->getPackages(null, Mage::getSingleton('checkout/session')->getQuote()->getId());
        $packageTypes = [];
        /** @var Dyna_Configurator_Helper_Data $helper */
        $helper = Mage::helper('dyna_configurator');
        foreach ($packageCollection as $package) {
            $packageTypes[] = $package->getType();
        }

        $serviceability_items = Mage::getSingleton('dyna_address/storage')->getData('service_ability');

        $result = $campaign->getOffers($page, $pageSize);
        $showServiceabilityWarning = 1;

        $customerLoggedIn = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $customerLoggedIn = $customerLoggedIn->isCustomerLoggedIn();

        /** @var Dyna_Bundles_Model_CampaignOffer $campaignOffer */
        foreach ($result['campaignOffers'] as $campaignOffer) {
            $offerProductSKUs = explode(',', $campaignOffer->getProductIds());
            $packageType = null;
            $offerProducts = [];
            $availability = 0;
            $hasInvalidProducts = 0;

            if ($offerProductSKUs) {
                $offerProductNames = $packageProducts = [];
                /** @var Dyna_Catalog_Model_Product $item */
                foreach ($offerProductSKUs as &$offerProductSKU) {
                    $productModel = Mage::getModel('catalog/product');
                    $offerProductSKU = trim($offerProductSKU);
                    $productId = $productModel->getIdBySku($offerProductSKU);
                    if ($productId) {
                        $item = $productModel->load($productId);
                        if ($item->getMarketable() === false) {
                            $hasInvalidProducts = 1;
                        }
                        $offerProductNames[] = $item->getDisplayNameConfigurator() ?: $item->getName();
                        $attribute = $item->getResource()->getAttribute(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
                        $packageTypeObject = json_decode(Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, $item->getData(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR)));
                        $packageType = strtolower(current((array)$packageTypeObject));
                        if (!isset($packageProducts[$packageType])) {
                            $packageProducts[$packageType] = [];
                        }
                        if (in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages()) || ($serviceability_items
                                && (in_array($packageType, Dyna_Catalog_Model_Type::getCablePackages())
                                    && $serviceability_items['top_bar_available_services']['KIP']['bandwidth'] >= $item->getData('required_bandwidth')
                                ) || ($serviceability_items
                                    && in_array($packageType, Dyna_Catalog_Model_Type::getDslPackages())
                                    && $serviceability_items['top_bar_available_services']['(V)DSL']['bandwidth'] >= $item->getData('bandwidth'))
                            )
                        ) {
                            $availability += 1;
                        }
                        if (!in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages())) {
                            $showServiceabilityWarning = !empty($serviceability_items) ? 1 : 0;
                        }

                        if ($packageType == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
                            $packageTypeForCreation = Dyna_Catalog_Model_Type::CREATION_TYPE_MOBILE;
                        } else {
                            $packageTypeForCreation = $packageType;
                        }
                        $packageCreationTypeId = Mage::helper('dyna_configurator/cart')->getCreationTypeId($packageTypeForCreation) ?? null;

                        $offerProducts[] = $item->getId();
                        $packageProducts[$packageType]['products'][] = $item->getId();
                        $packageProducts[$packageType]['packageCreationTypeId'] = $packageCreationTypeId;
                    } else {
                        $hasInvalidProducts = 1;
                        $offerProducts[] = $offerProductSKU;
                    }
                }

                $offerProductList = implode(',', $offerProducts);
                if (!$hasInvalidProducts && empty($offerProductList)) {
                    $hasInvalidProducts = 1;
                }
                //check if all offer products meet bandwidth requirements from serviceability check
                if ($availability !== count($offerProducts)) {
                    $status = 0;
                } else {
                    $status = 1;
                }

                $packages = [];
                $defaultEnabled = true;
                foreach ($packageProducts as $packageType => $package) {
                    //check if there are already packages of the same type in the cart
                    if (
                        count($packageTypes) &&
                        (in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages())
                            || in_array($packageType, Dyna_Catalog_Model_Type::getDslPackages())
                            || (in_array($packageType, Dyna_Catalog_Model_Type::getCablePackages())
                                && (count(array_diff($packageTypes, Dyna_Catalog_Model_Type::getCablePackages()))
                                    || in_array($packageType, $packageTypes)))
                        )
                    ) {
                        $status = 0;
                    }
                    //check if there is KIP or KUD in the install base
                    if (($helper->customerHasServiceInstalled(Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_KIP)
                            && $packageType == strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE))
                        || ($helper->customerHasServiceInstalled(Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_CLS)
                            && $packageType == strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT))) {
                        $status = 0;
                    }
                    $packages[] = [
                        'type' => $packageType,
                        'currentProducts' => implode(',', $package['products']),
                        'packageCreationTypeId' => $package['packageCreationTypeId'],
                    ];
                    if ($packageType != strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
                        $defaultEnabled = false;
                    }
                }

                if ($defaultEnabled) {
                    foreach ($packages as $package) {
                        if ($defaultEnabled = Mage::helper('dyna_configurator/cart')->getCampaignOfferCompatibility($package['type'], explode(',', $package['currentProducts']))) {
                            break;
                        }
                    }
                }

                $data = [
                    'identifier' => $campaignOffer->getData('offer_id'),
                    'discounted_months' => $campaignOffer->getDiscountedMonths(),
                    'discounted_price_per_month' => $campaignOffer->getDiscountedPricePerMonth(),
                    'undiscounted_from_month' => $campaignOffer->getUndiscountedFromMonth(),
                    'undiscounted_price_per_month' => $campaignOffer->getUndiscountedPricePerMonth(),
                    'title_description' => $campaignOffer->getTitleDescription(),
                    'usp1' => $campaignOffer->getData('usp1'),
                    'usp1pakagetype' => $campaignOffer->getData('usp1pakagetype'),
                    'usp2pakagetype' => $campaignOffer->getData('usp2pakagetype'),
                    'usp3pakagetype' => $campaignOffer->getData('usp3pakagetype'),
                    'usp4pakagetype' => $campaignOffer->getData('usp4pakagetype'),
                    'usp5pakagetype' => $campaignOffer->getData('usp5pakagetype'),
                    'usp2' => $campaignOffer->getData('usp2'),
                    'usp3' => $campaignOffer->getData('usp3'),
                    'usp4' => $campaignOffer->getData('usp4'),
                    'usp5' => $campaignOffer->getData('usp5'),
                    'subtitle_description' => $campaignOffer->getData('subtitle_description'),
                    'status' => $status,
                    'skus' => implode('<br>', $offerProductNames),
                    'initConfiguratorData' => [
                        'packages' => $packages,
                        'packagesStringified' => rawurlencode(json_encode($packages)),
                        'packageId' => null,
                        'saleType' => Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                        'ctn' => $callingNumber,
                        'hasInvalidProducts' => $hasInvalidProducts,
                        'disabledByPermission' => (!$this->hasPermission($packageType)) ? true : false,
                        'hasOpenOrder' => $customerLoggedIn ? Mage::helper('dyna_customer')->hasOpenOrders(null,$packageType) : false
                    ],
                    'defaultEnabled' => $defaultEnabled,
                ];
                $campaigns[] = $data;
            }
        }
        $prevPage = $page - 1;
        $nextPage = $result['totalCampaignOffers'] / $pageSize > $page ? $page + 1 : 0;
        $noofdots = ceil($result['totalCampaignOffers'] / $pageSize) ;
        for($i=1;$i<=$noofdots;$i++)
        {
            $colour=false;
            if($page==$i)
            {
                $colour=true;
            }
            $dotsarr[$i]=array("page"=>$i, "colour"=>$colour);
        }
        return [
            'campaignData' => $campaign->getData(),
            'campaigns' => $campaigns,
            'nextPage' => $nextPage,
            'prevPage' => $prevPage,
            'serviceability' => $showServiceabilityWarning,
            'dotsarr'=>$dotsarr,
            'page'=>$page,
            'displaydefault'=>$displayDefault,
            'display_Change'=>$displayChange
        ];
    }

    /**
     * @param
     * @return integer
     */
    public function getDefaultCampaignCount()
    {
        $defaultCampaignCount = 0;
        $dateModel = Mage::getSingleton('core/date');

        $timeNow = date('Y-m-d', $dateModel->timestamp(time()));
        $defaultCampaigns = Mage::getModel('dyna_bundles/campaign')
            ->getCollection()
            ->addFieldToFilter('default', 1);

        foreach ($defaultCampaigns as $campaign) {
            $timeFrom = date('Y-m-d',
                $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ? $campaign->getValidFrom() : time())));
            $timeTo = date('Y-m-d',
                $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ? $campaign->getValidTo() : time())));

            if ($campaign->getId() && $timeNow >= $timeFrom && $timeNow <= $timeTo) {
                $campaignId = $campaign->getData('campaign_id');
                $defaultCampaign = Mage::getModel('dyna_bundles/campaign')->load($campaignId, 'campaign_id');

                if (!$defaultCampaign) {
                    continue;
                }

                $offerDetails = $defaultCampaign->getOffers(1, 199);
                if ($offerDetails['campaignOffers']) {
                    $defaultCampaignCount++;
                }
            }
        }
        return $defaultCampaignCount;
    }

    /**
     * post data has the search key like 'abc'
     * @param $postData
     * @return array
     */
    public function getDefaultCampaignData($postData)
    {
        $defaultCampaignsResult = array();
        $dateModel = Mage::getSingleton('core/date');

        $customerData = Mage::getSingleton('dyna_customer/session')->getUctParams();
        $callingNumber = $customerData['callingNumber'];
        $selectedCampaignId = $customerData['campaignId'];
        $serviceabilityItems = Mage::getSingleton('dyna_address/storage')->getData('service_ability');

        /** @var Omnius_Package_Model_Mysql4_Package $packageCollection */
        $packageCollection = Mage::getModel('package/package')->getPackages(null, Mage::getSingleton('checkout/session')->getQuote()->getId());

        $packageTypes = [];
        /** @var Dyna_Configurator_Helper_Data $helper */
        $helper = Mage::helper('dyna_configurator');
        foreach ($packageCollection as $package) {
            $packageTypes[] = $package->getType();
        }

        $terms = $postData && $postData['search'] ? explode(' ', $postData['search']) : "";

        $timeNow = date('Y-m-d', $dateModel->timestamp(time()));
        $defaultCampaigns = Mage::getModel('dyna_bundles/campaign')
            ->getCollection()
            ->addFieldToFilter('default', 1);

        foreach ($terms as $term) {
            $defaultCampaigns->addFieldToFilter(
                array('campaign_title'),
                array(array('like' => "%{$term}%"))
            );
        }

        foreach ($defaultCampaigns as $campaign) {
            $timeFrom = date('Y-m-d', $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ? $campaign->getValidFrom() : time())));
            $timeTo = date('Y-m-d', $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ? $campaign->getValidTo() : time())));

            if ($campaign->getId() && $timeNow >= $timeFrom && $timeNow <= $timeTo) {
                $campaign_id = $campaign->getData('campaign_id');
                $offerData = array();
                /** @var Dyna_Bundles_Model_Campaign $defaultCampaign */
                $defaultCampaign = Mage::getModel('dyna_bundles/campaign')->load($campaign_id, 'campaign_id');

                if(!$defaultCampaign){
                    continue;
                }

                $offerDetails = $defaultCampaign->getOffers(1, 199);

                if($offerDetails['campaignOffers']){
                    foreach ($offerDetails['campaignOffers'] as $campaignOffer) {
                        $offerProductSKUs = explode(',', $campaignOffer->getProductIds());
                        $packageType = null;
                        $offerProducts = [];
                        $availability = 0;
                        $hasInvalidProducts = 0;

                        if ($offerProductSKUs) {
                            $productModel = Mage::getModel('catalog/product');
                            $offerProductNames = [];
                            /** @var Dyna_Catalog_Model_Product $item */
                            foreach ($offerProductSKUs as &$offerProductSKU) {
                                $offerProductSKU = trim($offerProductSKU);
                                $productId = $productModel->getIdBySku($offerProductSKU);
                                if ($productId) {
                                    $item = $productModel->load($productId);
                                    if ($item->getMarketable() === false) {
                                        $hasInvalidProducts = 1;
                                    }
                                    $offerProductNames[] = $item->getDisplayNameConfigurator() ?: $item->getName();
                                    $attribute = $item->getResource()->getAttribute(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
                                    $packageTypeObject = json_decode(Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, $item->getData(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR)));
                                    $packageType = strtolower(current((array)$packageTypeObject));
                                    if (in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages()) || ($serviceabilityItems
                                            && (in_array($packageType, Dyna_Catalog_Model_Type::getCablePackages())
                                                && $serviceabilityItems['top_bar_available_services']['KIP']['bandwidth'] >= $item->getData('required_bandwidth')
                                            ) || ($serviceabilityItems
                                                && in_array($packageType, Dyna_Catalog_Model_Type::getDslPackages())
                                                && $serviceabilityItems['top_bar_available_services']['(V)DSL']['bandwidth'] >= $item->getData('bandwidth'))
                                        )
                                    ) {
                                        $availability += 1;
                                    }

                                    $offerProducts[] = $item->getId();
                                } else {
                                    $hasInvalidProducts = 1;
                                    $offerProducts[] = $offerProductSKU;
                                }
                            }

                            $offerProductList = implode(',', $offerProducts);
                            if (!$hasInvalidProducts && empty($offerProductList)) {
                                $hasInvalidProducts = 1;
                            }
                            //check if all offer products meet bandwidth requirements from serviceability check
                            if ($availability !== count($offerProducts)) {
                                $status = 0;
                            } else {
                                $status = 1;
                            }
                            //check if there are already packages of the same type in the cart
                            if (
                                count($packageTypes) &&
                                (in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages())
                                    || in_array($packageType, Dyna_Catalog_Model_Type::getDslPackages())
                                    || (in_array($packageType, Dyna_Catalog_Model_Type::getCablePackages())
                                        && (count(array_diff($packageTypes, Dyna_Catalog_Model_Type::getCablePackages()))
                                            || in_array($packageType, $packageTypes)))
                                )
                            ) {
                                $status = 0;
                            }

                            $offerData[] = [
                                'identifier' => $campaignOffer->getData('offer_id'),
                                'discounted_months' => $campaignOffer->getDiscountedMonths(),
                                'discounted_price_per_month' => $campaignOffer->getDiscountedPricePerMonth(),
                                'undiscounted_from_month' => $campaignOffer->getUndiscountedFromMonth(),
                                'undiscounted_price_per_month' => $campaignOffer->getUndiscountedPricePerMonth(),
                                'title_description' => $campaignOffer->getTitleDescription(),
                                'usp1' => $campaignOffer->getData('usp1'),
                                'usp1pakagetype' => $campaignOffer->getData('usp1pakagetype'),
                                'usp2pakagetype' => $campaignOffer->getData('usp2pakagetype'),
                                'usp3pakagetype' => $campaignOffer->getData('usp3pakagetype'),
                                'usp4pakagetype' => $campaignOffer->getData('usp4pakagetype'),
                                'usp5pakagetype' => $campaignOffer->getData('usp5pakagetype'),
                                'usp2' => $campaignOffer->getData('usp2'),
                                'usp3' => $campaignOffer->getData('usp3'),
                                'usp4' => $campaignOffer->getData('usp4'),
                                'usp5' => $campaignOffer->getData('usp5'),
                                'subtitle_description' => $campaignOffer->getData('subtitle_description'),
                                'status' => $status,
                                'skus' => implode('<br>', $offerProductNames),
                                'initConfiguratorData' => [
                                    'type' => $packageType,
                                    'packageId' => null,
                                    'saleType' => Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                                    'ctn' => $callingNumber,
                                    'currentProducts' => $offerProductList,
                                    'hasInvalidProducts' => $hasInvalidProducts,
                                    'disabledByPermission' => (!$this->hasPermission($packageType)) ? true : false
                                ],
                            ];
                        }
                    }
                }
                $data = [
                    'id' => $campaign->getId() ?: 0,
                    'campaign_id' => $campaign->getData('campaign_id'),
                    'promotion_hint' => $campaign->getData('promotion_hint'),
                    'campaign_title' => $campaign->getData('campaign_title'),
                    'cluster_category' => $campaign->getData('cluster_category'),
                    'cluster' => $campaign->getData('cluster'),
                    'campaign_hint' => $campaign->getData('campaign_hint'),
                    'offer_hint' => $campaign->getData('offer_hint'),
                    'promotion_hint1' => $campaign->getData('promotion_hint1'),
                    'promotion_hint2' => $campaign->getData('promotion_hint2'),
                    'promotion_hint3' => $campaign->getData('promotion_hint3'),
                    'timeFrom' => $timeFrom,
                    'timeTo' => $timeTo,
                    'selectedCampaignId' => $selectedCampaignId,
                    'offerdetails' => $offerData
                ];

                $defaultCampaignsResult[] = $data;
            }
        }
        return $defaultCampaignsResult;
    }
    /**
    * postdata has the @param $campaign_id
    */
    public function setDefaultCampaignData($postdata)
    {
        $uctParams = Mage::getSingleton('dyna_customer/session')->getUctParams();
        $uctParams['campaignId'] = $postdata["campaign_id"];

        Mage::getSingleton('dyna_customer/session')->setUctParams($uctParams);

        $result["success"]=true;
        return $result;
    }
    /**
     * @param $campaignId
     * @return array
     */
    public function getCampaignProducts($campaignId)
    {
        /** @var Dyna_Bundles_Model_Campaign $campaign */
        $campaign = Mage::getModel('dyna_bundles/campaign')->load($campaignId, 'campaign_id');

        $result = $campaign->getOffers(1, 999);

        $campaignProducts = [];

        $dateModel = Mage::getSingleton('core/date');
        $timeNow = date('Y-m-d', $dateModel->timestamp(time()));
        $timeFrom = date('Y-m-d', $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ?: time())));
        $timeTo = date('Y-m-d', $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ?: time())));
        if ($timeNow < $timeFrom || $timeNow > $timeTo) {
            return '';
        }

        /** @var Dyna_Bundles_Model_CampaignOffer $campaignOffer */
        if(isset($result['campaignOffers']) && is_array($result['campaignOffers'])){
            foreach ($result['campaignOffers'] as $campaignOffer) {
                $offerProductSKUs = explode(',', $campaignOffer->getProductIds());
                $offerProducts = [];

                if ($offerProductSKUs) {
                    $productModel = Mage::getModel('catalog/product');
                    /** @var Dyna_Catalog_Model_Product $item */
                    foreach ($offerProductSKUs as $offerProductSKU) {
                        $offerProductSKU = trim($offerProductSKU);
                        $productId = $productModel->getIdBySku($offerProductSKU);
                        if ($productId) {
                            $item = $productModel->load($productId);
                            $offerProducts[] = $item->getId();
                        }
                    }
                }

            $campaignProducts = array_merge($campaignProducts, $offerProducts);
            }
        }

        return array_unique($campaignProducts);
    }

    /**
     * @param Dyna_Bundles_Model_BundleRule $bundle
     * @param $activePackage
     * @param $bundleCreatedPackage
     * @return Dyna_Bundles_Model_Expression_Parser|Mage_Core_Model_Abstract
     * @internal param int $installPackageId
     * @internal param int $quotePackageId
     */
    public function buildRuleParserFromBundleRule(Dyna_Bundles_Model_BundleRule $bundle, $activePackage, $bundleCreatedPackage) : Dyna_Bundles_Model_Expression_Parser
    {
        /** @var Dyna_Bundles_Model_Service_Logger $logger */
        $logger = Mage::getModel("dyna_bundles/service_logger", ['logFile' => 'bundleRules.log']);

        $availableScopes = [
            'installBase' => Mage::getModel("dyna_bundles/expression_packageScope", array(
                'package' => $bundleCreatedPackage,
                'bundle' => $bundle,
            )),
            'quote' => Mage::getModel("dyna_bundles/expression_quotePackageScope", array(
                'package' => $activePackage,
                'bundle' => $bundle,
                'interStackPackageId' => isset($bundleCreatedPackage) ? $bundleCreatedPackage->getPackageId() : null,
            )),
        ];

        $data = [
            'scopes' => $availableScopes,
            'bundle' => $bundle,
            'logger' => $logger,
        ];
        return Mage::getModel("dyna_bundles/expression_parser", $data);
    }

    /**
     * @param Dyna_Bundles_Model_BundleRule $bundle
     * @param $activePackage
     * @param $bundleCreatedPackage
     * @return Dyna_Bundles_Model_Expression_Parser
     */
    public function buildRuleParserFromBundleRuleForDummyScope(Dyna_Bundles_Model_BundleRule $bundle, $activePackage, $bundleCreatedPackage, $interStackPackageId) : Dyna_Bundles_Model_Expression_Parser
    {
        /** @var Dyna_Bundles_Model_Service_Logger $logger */
        $logger = Mage::getModel("dyna_bundles/service_logger", ['logFile' => 'bundleRules.log']);

        $availableScopes = [
            'installBase' => Mage::getModel("dyna_bundles/expression_packageDummyScope", array(
                'package' => $bundleCreatedPackage,
                'bundle' => $bundle,
            )),
            'quote' => Mage::getModel("dyna_bundles/expression_quotePackageDummyScope", array(
                'package' => $activePackage,
                'bundle' => $bundle,
                'interStackPackageId' => $interStackPackageId,
            )),
        ];

        $data = [
            'scopes' => $availableScopes,
            'bundle' => $bundle,
            'logger' => $logger,
        ];
        return Mage::getModel("dyna_bundles/expression_parser", $data);
    }

    public function buildBundlePromoRuleParserFromBundleRule(Dyna_Bundles_Model_BundleRule $bundle, $installBasePackage, $quotePackage): Dyna_Bundles_Model_Expression_Parser
    {
        /** @var Dyna_Bundles_Model_Service_Logger $logger */
        $logger = Mage::getModel("dyna_bundles/service_logger", ['logFile' => 'bundleRules.log']);

        $availableScopes = [
            'installedBase' => Mage::getModel("dyna_bundles/expression_packageScope", ['package' => $installBasePackage, 'bundle' => $bundle]),
            'quote' => Mage::getModel("dyna_bundles/expression_promoOnlyQuotePackageScope", ['package' => $quotePackage, 'bundle' => $bundle]),
        ];

        $data = [
            'scopes' => $availableScopes,
            'bundleActions' => $bundle->getActions(),
            'logger' => $logger,
            'bundle' => $bundle
        ];
        return Mage::getModel("dyna_bundles/expression_parser", $data);
    }

    public function hasMandatoryFamiliesCompleted()
    {
        return empty($this->getMandatoryFamilies());
    }

    /**
     * Returns the missing mandatory families
     * @return array|mixed
     */
    public function getMandatoryFamilies()
    {
        /** @var Dyna_Bundles_Model_Expression_ParserResponse $parserResponse */
        $parserResponse = Mage::getSingleton('dyna_customer/session')->getBundleRuleParserResponse();

        $mandatoryFamilies = [];

        if ($parserResponse) {
            if ($this->savedParserResponseAppliesToQuote($parserResponse)) {
                $mandatoryFamilies = $parserResponse->getMandatoryChooseFromFamiliesFormat();
                $mandatoryFamilies = $this->getMissingMandatoryFamilies($mandatoryFamilies);
            } else {
                Mage::getSingleton('dyna_customer/session')->unsBundleRuleParserResponse();
                $mandatoryFamilies = [];
            }

        }

        return $mandatoryFamilies;
    }

    public function savedParserResponseAppliesToQuote($parserResponse)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packages = $quote->getCartPackages();

        $applies = false;

        foreach ($packages as $package) {
            foreach ($package->getBundles() as $bundlePackage) {
                if ($bundlePackage->getId() == $parserResponse->getBundleRuleId()) {
                    $applies = $bundlePackage->getId();
                    break;
                }
            }
        }

        return $applies;
    }

    /**
     * Filter mandatory choose families by products in cart
     * @param $mandatoryFamilies
     * return $this
     */
    protected function getMissingMandatoryFamilies($mandatoryFamilies)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        foreach ($mandatoryFamilies as $entityId => &$families) {
            foreach ($families as $key => $family) {
                $package = $quote->getCartPackageByEntityId($family['packageId']);
                if (!$package) {
                    continue;
                }
                foreach ($package->getAllItems() as $item) {
                    if (in_array($family['familyId'], $item->getProduct()->getCategoryIds())) {
                        unset ($families[$key]);
                    }
                }
            }

            if (empty($mandatoryFamilies[$entityId])) {
                unset($mandatoryFamilies[$entityId]);
            }
        }

        return $mandatoryFamilies;
    }

    public function isBundleChoiceFlow()
    {
        /** @var Dyna_Bundles_Model_Expression_ParserResponse $parserResponse */
        $parserResponse = $this->getBundleRuleParser();
        if ($parserResponse) {
            return $this->savedParserResponseAppliesToQuote($parserResponse) && !empty($parserResponse->getMandatoryChooseFromFamilies());
        }
        return false;
    }

    /**
     * @return array|false|Mage_Core_Model_Abstract|null
     */
    public function getBundleRuleParser()
    {
        $activePackage = Mage::getSingleton('checkout/session')->getQuote()->getActivePackage();
        $packageId = $activePackage ? $activePackage->getEntityId() : null;
        $result = null;
        if ($packageId) {
            /** @var Dyna_Bundles_Model_BundlePackages $bundleRelation */
            $bundleRelation = Mage::getModel('dyna_bundles/bundlePackages')
                ->getCollection()
                ->addFieldToFilter('package_id', $packageId)
                ->addFieldToFilter('mandatory_families', array('neq' => 'NULL'))
                ->getLastItem();
            $result = $bundleRelation ? Mage::getModel("dyna_bundles/expression_parserResponse", ['mandatoryChooseFromFamilies' => $bundleRelation->getMandatoryFamilies(), 'bundleRuleId' => $bundleRelation->getBundleRuleId()]) : [];
        }

        return $result;
    }

    public function choiceAppliesToActivePackage()
    {
        /** @var Dyna_Bundles_Model_Expression_ParserResponse $parserResponse */
        $parserResponse = Mage::getSingleton('customer/session')->getBundleRuleParserResponse();
        $activePackage = $this->getQuote()->getCartPackage();
        $applies = false;

        if ($parserResponse && !empty($parserResponse->getMandatoryChooseFromFamilies())) {
            foreach ($parserResponse->getMandatoryChooseFromFamilies() as $family) {
                foreach ($family as $key => $familyId) {
                    if ($activePackage->getId() == $key) {
                        $applies = true;
                    }
                }
                // preserve by entity id as package index will change on package deletion
            }
        }

        return $applies;
    }

    /**
     *  Returns all mandatory families
     */
    public function getAllMandatoryFamilies()
    {
       /** @var Dyna_Bundles_Model_Expression_ParserResponse $parserResponse */
        $parserResponse = $this->getBundleRuleParser();

        $mandatoryFamilies = [];

        if ($parserResponse) {
            if ($this->savedParserResponseAppliesToQuote($parserResponse)) {
                $mandatoryFamilies = $parserResponse->getMandatoryChooseFromFamiliesFormat();
            }
        }

        return $mandatoryFamilies;
    }

    /**
     * @param $finalProducts
     * @param $offerId
     * @return bool
     */
    public function productsInOffer($finalProducts, $offerId)
    {
        $offerIds = $this->getOfferProducts($offerId);
        return !empty(array_intersect($offerIds, $finalProducts));
    }

    /**
     * Returns ids of products from a specific offer
     * @param $offerId
     * @return array
     */
    protected function getOfferProducts($offerId)
    {
        /** @var Dyna_Bundles_Model_Campaign $campaign */
        $offer = Mage::getModel('dyna_bundles/campaignOffer')
            ->getCollection()
            ->addFieldToFilter('offer_id', $offerId)
            ->getFirstItem();

        $offerSkus = explode(',', $offer->getData('product_ids'));
        $offerIds = [];
        foreach ($offerSkus as $offerSku) {
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product');
            $offerIds[] = $product->getIdBySku(trim($offerSku));
        }

        return $offerIds;
    }

    /**
     * Get Red Sales Id
     * @param null $cluster_category
     * @return array
     */
    public function getRedSalesId($cluster_category = null)
    {
        /* If we have a campaign, every quote must have set the data campaign*/
        /** @var Dyna_Customer_Model_Session $session */
        $session = Mage::getSingleton('dyna_customer/session');

        /** @var Dyna_Agent_Model_Dealer $agent */
        $agent = $session->getAgent();
        $uctParams = $session->getUctParams();
        $redSalesId = array();

        // if there is a valid campaign id, retrieve red_sales_id from bundle_campaign_dealercode (Telesales agent)
        if ($uctParams && !empty($uctParams['transactionId']) && !empty($uctParams['campaignId'])) {
            $this->getTelesalesAgentVoids($session, $uctParams, $redSalesId, $cluster_category);
        } else {
            // else, mapping of red_sales_id is made from agent->getRedSalesId() (alternative of wave's 2 COPS agent)
            $redSalesId = $this->getProvisRedSalesId(array($agent->getRedSalesId()));
        }

        return $redSalesId == array() ? null : $redSalesId;
    }

    /**
     * Get voids for Telesales agents
     * @param $session
     * @param $uctParams
     * @param $redSalesId
     * @param null $cluster_category
     */
    private function getTelesalesAgentVoids($session, $uctParams, &$redSalesId, $cluster_category = null)
    {
        /** @var Dyna_Agent_Model_Dealer $dealer */
        $dealer = $session->getAgent()->getDealer();
        $agency = Mage::getSingleton('agent/dealergroup')->load($dealer->getGroupId())->getName();
        $city = $dealer->getCity();
        if (is_null($cluster_category)) {
            $cluster_category = $this->getCampaignData($uctParams['campaignId'], $uctParams['callingNumber'])['campaignData']['cluster_category'];
        }
        $marketing_code = $this->getCampaignData($uctParams['campaignId'], $uctParams['callingNumber'])['campaignData']['marketing_code'];

        // Determine the agent VOID (Red Sales Id)

        /** @var Dyna_Bundles_Model_CampaignDealercode $dealerCodeModel */
        $dealerCodeModel = Mage::getModel('dyna_bundles/campaignDealercode')
            ->getCollection()
            ->addFieldToFilter('agency', array('in' => [$agency, '*']))
            ->addFieldToFilter('city', array('in' => [$city, '*']))
            ->addFieldToFilter('cluster_category', array('in' => [$cluster_category, '*']))
            ->addFieldToFilter('marketing_code', array('in' => [$marketing_code, '*']));

        if(count($dealerCodeModel)){
            foreach($dealerCodeModel as $dealerCodeRow){
                $redSalesId[] = $dealerCodeRow->getRedSalesId();
            }
        }

        $redSalesId = $this->getProvisRedSalesId($redSalesId);
    }

    public function getSelectSalesIDUrl() {
        return $this->_getUrl('bundles/index/selectSalesId');
    }

    /**
     * Get the right red_sales_id from Provis import
     * @param $redSalesIds
     * @return boolean
     */
    public function getProvisRedSalesId($redSalesIds)
    {
        if (empty($redSalesIds) || (!$redSalesIds[0])) {
            $message = $this->__('Please be aware! Unable to submit an order due to unavailable Sales ID');
        } else {
            /** @var Dyna_AgentDE_Model_ProvisSales $agentProvisSales */
            $agentProvisSales = Mage::getModel('agentde/provisSales')
                ->getCollection()
                ->addFieldToFilter('red_sales_id', array('in' => $redSalesIds));

            if (count($agentProvisSales)) {
                foreach ($agentProvisSales as $provisSalesId) {
                    if ($provisSalesId->getStatus() != 'AK') {
                        $message = $this->__('Please be aware! Unable to submit an order due to an inactive Sales ID');
                    } elseif(!$provisSalesId->getBlueSalesId() || !$provisSalesId->getYellowSalesId()) {
                        $message = $this->__('Please be aware! Unable to submit an order due to an invalid Sales ID');
                    } else {
                        return $provisSalesId->getRedSalesId();
                    }
                }
            } else {
                $message = $this->__('Please be aware! Unable to submit an order due to an invalid Sales ID');
            }
        }

        $this->salesIdError = $message;
        return false;
    }

    /**
     * Get data from Provis import by red_sales_id
     */
    public function getProvisDataByRedSalesId($quoteRedSalesId = null)
    {
        if($quoteRedSalesId){
            // check if quote salesId is a valid one, as an extra safety step
            $redSalesId = $this->getProvisRedSalesId(array($quoteRedSalesId));
        } else {
            $redSalesId = $this->getRedSalesId();
        }

        if(!$redSalesId){
            return array(
                'error' => true,
                'message' => $this->salesIdError
            );
        }

        /** @var Dyna_AgentDE_Model_ProvisSales $agentProvisSales */
        $agentProvisSales = Mage::getModel('agentde/provisSales')
            ->getCollection()
            ->addFieldToFilter('red_sales_id', $redSalesId)
            ->getFirstItem();

        return array(
            'kias' => $agentProvisSales->getRedSalesId(),
            'fn' => $agentProvisSales->getYellowSalesId(),
            'kd' => $agentProvisSales->getBlueSalesId()
        );
    }

    /**
     * Return a list of eligible bundles that will be sent forward to frontend (has a special structure)
     * @return array ()
     */
    public function getFrontendEligibleBundles($skipSimulation = false, $filterLinkedAccounts = true, $forTariffs = null)
    {
        $eligible = array();
        $hints = array();

        // get quote
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        // get quote active package
        $activePackage = $quote->getActivePackage();

        /** @var Dyna_Package_Model_PackageType $packageType */
        $packageType = $activePackage ? Mage::getModel('dyna_package/packageType')->loadByCode($activePackage->getType()) : null;

        $relatedRedPlusTariffSKU = '';
        if($activePackage) {
            // check if current package is redPlus related; if so, send its tariff's sku to addRedPlusMember button
            foreach($activePackage->getAllItems() as $item) {
                if(strtolower($item->getProductType()) == strtolower(Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION)) {
                    $relatedRedPlusTariffSKU = $item->getSku();
                }
            }
        }

        // holds section products for each section visible in configurator
        $allSectionProducts = array();

        // get bundles collection
        /** @var Dyna_Bundles_Model_Mysql4_BundleRule_Collection $bundles */
        $bundles = Mage::getModel('dyna_bundles/bundleRule')
            ->getCollection();

        if (!$bundles->getSize()) {
            return $eligible;
        }

        // loading products from catalog for all sections
        if ($packageType && !$skipSimulation && $forTariffs) {
            $allSectionProducts = Mage::getModel('catalog/product')
                ->getCollection()
                ->addFieldToFilter('entity_id', array('in' => $forTariffs));
        }

        // get expression language instance
        /** @var Dyna_Bundles_Helper_ExpressionLanguage $expressionLanguage */
        $expressionLanguage = Mage::helper('dyna_bundles/expressionLanguage');
        /** @var Dyna_Configurator_Helper_Expression $bundlesSimulatorHelper */
        $bundlesSimulatorHelper = Mage::helper('dyna_bundles/expression');
        $cartObject = Mage::getModel('dyna_configurator/expression_cart')
            ->setBundlesHelper($bundlesSimulatorHelper);

        /** @var Dyna_Configurator_Model_Expression_InstallBase $installBaseEvaluator */
        $installBaseEvaluator = Mage::getModel('dyna_configurator/expression_installBase');
        $installBaseEvaluator->setBundlesHelper($this);

        if (!$filterLinkedAccounts) {
            $installBaseEvaluator->removeLinkedAccountsFilter();
        }

        $expressionObjects = array(
            'customer' => Mage::getModel('dyna_configurator/expression_customer'),
            'cart' => $cartObject,
            'availability' => Mage::getModel('dyna_configurator/expression_availability'),
            'installBase' => $installBaseEvaluator,
            'agent' => Mage::getModel('dyna_configurator/expression_agent'),
        );

        // used for returning post evaluation
        $dummyScope = Mage::getModel('dyna_bundles/expression_scope');

        $parentPackageId = $this->getParentPackageId();

        // holds an array consisting of the bundle id as key and the package type that it allows as value
        $packagesAltered = array();
        // holds a list of bundle ids to which current cart satisfies allowed to condition (don't need to evaluate these bundles, the hint action will cover it)
        $currentCartAllowedBundles = array();

        // getting the list of bundles that enable packages and that are eligible
        /** @var Dyna_Bundles_Model_BundleRule $bundle */
        foreach ($bundles as $bundle) {
            $bundlesSimulatorHelper->setCurrentBundle($bundle);
            $this->currentBundle = $bundle;

            // reset expression evaluation result
            // will be later used for returning eligible RedPlus subscriptions
            $expressionLanguage->resetResponse();
            /** @var Dyna_Bundles_Model_BundleAction $action */
            foreach ($bundle->getActions() as $action) {
                // checking whether this bundle enables a certain package
                // if it contains the allow package string, than we evaluate the expression with a dummy object that returns the sent parameter
                if (strpos($action->getAction(), Dyna_Bundles_Model_Expression_Scope::ALLOW_PACKAGE_TYPE_STRING) !== false) {
                    $packageCreationType = strtolower($expressionLanguage->evaluate($action->getAction(), array('scope' => $dummyScope)));

                    // Load the package type information and build an array of enabled packages

                    /** @var Dyna_Package_Model_PackageCreationTypes $typeModel */
                    $typeModel = Mage::getModel('dyna_package/packageCreationTypes');
                    $creationType = $typeModel->getTypeByPackageCode($packageCreationType);
                    $type = $creationType->getReferencedPackageType();

                    $packagesAltered[$bundle->getId()][] = array(
                        'packageCode' => $type->getPackageCode(true),
                        'creationTypeId' => $creationType->getId(),
                        'creationTypeCode' => $creationType->getPackageTypeCode(),
                        'filterAttributes' => $creationType->getFilterAttributes()
                    );
                } else if (strpos($action->getAction(), Dyna_Bundles_Model_Expression_Scope::ALLOW_PACKAGE_CREATION_TYPE_ID_STRING) !== false) {
                    $packageCreationType = strtolower($expressionLanguage->evaluate($action->getAction(), array('scope' => $dummyScope)));

                    // Load the package type information and build an array of enabled packages

                    /** @var Dyna_Package_Model_PackageCreationTypes $typeModel */
                    $typeModel = Mage::getModel('dyna_package/packageCreationTypes');
                    $creationType = $typeModel->getTypeByPackageCode($packageCreationType);
                    $type = $creationType->getReferencedPackageType();

                    $packagesAltered[$bundle->getId()][] = array(
                        'packageCode' => $type->getPackageCode(true),
                        'creationTypeId' => $creationType->getId(),
                        'creationTypeCode' => $creationType->getPackageTypeCode(),
                        'filterAttributes' => $creationType->getFilterAttributes()
                    );
                }
            }

            // skip if one of the conditions fail
            foreach ($bundle->getConditions() as $bundleCondition) {
                // skip if one of the conditions fail
                if (!$expressionLanguage->evaluate($bundleCondition->getCondition(), $expressionObjects)) {
                    // be sure to clear the registered responses
                    Mage::unregister('install_base_expression_result');
                    continue 2;
                }
            }

            // if this bundles enables a create package button, that add it to response
            $evaluationResponse = $expressionLanguage->getResponse();
            // be sure to clear the registered responses
            Mage::unregister('install_base_expression_result');

            // might be just an install base bundle with cart active package
            $alteredPackageType = isset($packagesAltered[$bundle->getId()]) ? current($packagesAltered[$bundle->getId()])['packageCode'] : "";

            $packageCodes = [];
            if (isset($packagesAltered[$bundle->getId()])) {
                foreach ($packagesAltered[$bundle->getId()] as $packageInfo) {
                    $packageCodes[] = $packageInfo['packageCode'];
                }
            }

            // skip Red+ bundles if no creation type is found
            if ($bundle->isRedPlus() && (!isset($packagesAltered[$bundle->getId()]) || !isset(current($packagesAltered[$bundle->getId()])['creationTypeId']))) {
                continue;
            }

            // check for packages to have valid data
            foreach ($evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] ?? [] as $package) {
                if (!$package['eligibleProductSku']) {
                    continue;
                }
            }
            // check for packages to have valid data
//            foreach ($evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] ?? array() as $package) {
//                if (!$package['eligibleProductSku']) {
//                    continue 2;
//                }
//            }

            $eligible[$bundle->getId()] = array(
                'bundleId' => (int)$bundle->getId(),
                'susoBundle' => $bundle->isSuso(),
                'redPlusBundle' => $bundle->isRedPlus(),
                'lowEntryBundle' => $bundle->isLowEntry(),
                'packageName' => Mage::getModel('dyna_package/packageType')->loadByCode($alteredPackageType)->getFrontEndName(),
                'addButtonTitle' => $bundle->getHintWithLocation(Dyna_Bundles_Model_BundleRule::LOCATION_OPTION) ? $bundle->getHintWithLocation(Dyna_Bundles_Model_BundleRule::LOCATION_OPTION)->getText() : null,
                'parentPackageId' => $parentPackageId,
                'activatePackages' => isset($packagesAltered[$bundle->getId()]) ? $packageCodes : [],
                'creationTypeId' => isset($packagesAltered[$bundle->getId()]) ? current($packagesAltered[$bundle->getId()])['creationTypeId'] : null,
                'filterAttributes' => isset($packagesAltered[$bundle->getId()]) ? current($packagesAltered[$bundle->getId()])['filterAttributes'] : null,
                'targetedProducts' => $evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_REQUIRED_PRODUCTS] ?? array(),
                'addButtonInSection' => $bundle->getHintLocations(),// $bundle->getHintLocation(),
                // an array containing a list of combinations subscription number (aka customer number) - product id in order to have a clear reference
                // to which what install base product the bundle will be created
                'targetedSubscriptions' => array_values($evaluationResponse[static::EXPRESSION_INSTALL_BASE_PRODUCT] ?? []),
                // an array of packages in the current cart that contain a tariff that would make the package eligible to be the parent of red+ packages
                'targetedPackagesInCart' => $evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] ?? [],
                'highlightPackages' => $bundle->hasLocationCart() ? ($evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_HIGHLIGHT_PACKAGES] ?? []) : [],
                'relatedProductSku' => $relatedRedPlusTariffSKU
            );

            if (!isset($packagesAltered[$bundle->getId()])) {
                // if not, remove it from bundling list as it will be covered later by hint panel
                $currentCartAllowedBundles[] = $bundle->getId();
            }

            if ($cartHint = $bundle->getHintWithLocation(Dyna_Bundles_Model_BundleRule::LOCATION_SLIDE_IN)) {
                $hints[$bundle->getId()] = array(
                    'hintEnabled'  => $cartHint->getHintEnabled(),
                    'hintTitle' => $cartHint->getTitle(),
                    'hintText'     => $cartHint->getText(),
                    'hintLocation' => $cartHint->getLocation(),
                    'bundleId' => $bundle->getId(),
                );
            }

            $bundlesSimulatorHelper->clearCurrentBundle();
        }
        $this->currentBundle = null;

        if (!$skipSimulation) {
            $this->simulation = true;
            $expressionLanguage->updateSimulationStatus(true);
            // there goes nothing
            foreach ($allSectionProducts as $product) {
                /** @var Dyna_Catalog_Model_Product $product */
                // starting new product simulation
                $bundlesSimulatorHelper
                    ->takeSnapshot()
                    ->simulateProduct($product);
                /** @var Dyna_Bundles_Model_BundleRule $bundle */
                foreach ($bundles as $bundle) {
                    $this->currentBundle = $bundle;
                    $bundlesSimulatorHelper->setCurrentBundle($bundle);
                    $expressionLanguage->resetResponse();
                    // skip bundles that allow packages and bundles that are allowed for current cart (these will be evaluated by hints action)
                    if (isset($packagesAltered[$bundle->getId()]) || in_array($bundle->getId(), $currentCartAllowedBundles)) {
                        continue;
                    }

                    // skip further processing if one of the conditions fail
                    foreach ($bundle->getConditions() as $bundleCondition) {
                        if (!$expressionLanguage->evaluate($bundleCondition->getCondition(), $expressionObjects)) {
                            continue 2;
                        }
                    }

                    $evaluationResponse = $expressionLanguage->getResponse();
                    // not yet added to response?
                    if (!isset($eligible[$bundle->getId()])) {
                        $eligible[$bundle->getId()] = array(
                            'bundleId' => (int)$bundle->getId(),
                            'susoBundle' => $bundle->isSuso(),
                            'redPlusBundle' => $bundle->isRedPlus(),
                            'packageName' => "",
                            'addButtonTitle' => $bundle->getHintText(),
                            'parentPackageId' => null,
                            'activatePackages' => array(),
                            'targetedProducts' => array(),
                            'addButtonInSection' => $bundle->getHintLocations(), // option|cart|draw
                            // an array containing a list of combinations subscription number (aka customer number) - product id in order to have a clear reference
                            // to which what install base product the bundle will be created
                            'targetedSubscriptions' => array_values($evaluationResponse[static::EXPRESSION_INSTALL_BASE_PRODUCT] ?? []),
                        );
                    }
                    $eligible[$bundle->getId()]['targetedProducts'][] = $product->getId();
                    $bundlesSimulatorHelper->clearCurrentBundle();
                }

                // reverting helper instance to cart's state
                $bundlesSimulatorHelper->clearSnapshot();
            }
        }
        $this->currentBundle = null;

        // save bundle hints
        Mage::unregister('bundleHints');
        Mage::register('bundleHints', $hints);

        // resetting response keys (so array arrives in javascript response)
        return array_values($eligible);
    }

    /**
     * Determine parent package by the following logic:
     * If active package has parent, then get parent package id, otherwise return active package id
     * @return null
     */
    public function getParentPackageId()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackage = $quote->getActivePackage();
        switch (true) {
            case !$activePackage:
                $activePackageId = null;
                break;
            case $activePackage->getPackageId():
                $activePackageId = null;
                foreach ($quote->getCartPackages() as $package) {
                    if ($package->getId() == $activePackage->getParentId()) {
                        $activePackageId = $package->getPackageId();
                    }
                }
                break;
            default:
                $activePackageId = $activePackage->getPackageId();
        }

        return $activePackageId;
    }

    /**
     * Return current active bundles to frontend
     * @return array
     */
    public function getActiveBundles()
    {
        if ($this->activeBundles === false) {
            $tempPackages = array();
            $cartPackages = $this->getQuote()->getCartPackages();
            if($cartPackages) {
                foreach ($cartPackages as $package) {
                    foreach ($package->getBundles() as $bundle) {
                        // if bundled and already added to our response list, update list to include this package
                        if (!empty($package->getBundles()) && isset($tempPackages[$bundle->getId()])) {
                            $tempPackages[$bundle->getId()]['packages'][] = array(
                                'packageId' => (int)$package->getPackageId(),
                                'relatedProductId' => $this->activeBundles[$bundle->getId()]['redPlusBundle'] ? array($package->getRelatedProductId()) : $package->getBundleComponents(),
                                // returning install base targeted products from package (if any)
                                'targetedSubscriptions' => array(
                                    static::INSTALL_BASE_CUSTOMER_NUMBER => $package->getParentAccountNumber(),
                                    static::INSTALL_BASE_PRODUCT_ID => $package->getServiceLineId(),
                                )
                            );
                        } elseif (!empty($package->getBundles())) {
                            // load bundle
                            $tempPackages[$bundle->getId()] = array(
                                'bundleId' => (int)$bundle->getId(),
                                'susoBundle' => $bundle->isSuso(),
                                'redPlusBundle' => $bundle->isRedPlus()
                            );
                            $tempPackages[$bundle->getId()]['packages'][] = array(
                                'packageId' => (int)$package->getPackageId(),
                                'packageName' => $package->getTitle(),
                                'relatedProductId' => $bundle->isRedPlus() ? array($package->getRelatedProductId()) : $package->getBundleComponents(),
                                // returning install base targeted products from package (if any)
                                'targetedSubscriptions' => array(
                                    static::INSTALL_BASE_CUSTOMER_NUMBER => $package->getParentAccountNumber(),
                                    static::INSTALL_BASE_PRODUCT_ID => $package->getServiceLineId(),
                                )
                            );
                        }
                    }
                }
            }
            $this->activeBundles = $tempPackages;
        }

        return array_values($this->activeBundles);
    }

    /**
     * Clear cached active bundles on this instance
     * @return $this
     */
    public function clearCachedActiveBundles()
    {
        $this->activeBundles = false;

        return $this;
    }

    /**
     * Get session quote
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    /**
     * Returns whether the user is eligible to add red+ packages
     * from the shopping cart
     * @return bool
     */
    public function cartIsRedPlusEligible()
    {
        $eligibleBundles = $this->getFrontendEligibleBundles(true);
        $this->filterBundlesBySubscriptionStatus($eligibleBundles);

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackage = $quote->getCartPackage();
        $banSelected = $activePackage ? $activePackage->getParentAccountNumber() : false;

        foreach ($eligibleBundles as &$bundle) {
            if ($bundle['redPlusBundle'] && (!empty($bundle['targetedSubscriptions']) || !empty($bundle['targetedPackagesInCart']))) {
                if ($banSelected) {
                    foreach ($bundle['targetedSubscriptions'] as &$targetedSubscription) {
                        if ($targetedSubscription['customerNumber'] == $banSelected) {
                            return true;
                        }
                    }
                    if (!empty($bundle['targetedPackagesInCart'])) {
                        return true;
                    }
                } else if($bundle['creationTypeId']){
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Return product ids that are bundle component in order to break the bundles if needed
     * @return array
     */
    public function getBundleComponents()
    {
        $activePackageId = $this->getQuote()->getActivePackageId();
        $result = [];

        foreach ($this->getQuote()->getAllItems() as $quoteItem) {
            if ($quoteItem->getPackageId() != $activePackageId || !$quoteItem->getBundleComponent()) {
                continue;
            }

            $result[] = array(
                'productId' => $quoteItem->getProductId(),
                'bundleId' => $quoteItem->getBundleComponent(),
            );
        }

        return array_values($result);
    }

    /**
     * Return bundle hints for eligible bundles
     * @return array
     */
    public function getBundleHints()
    {
        $hints = Mage::registry('bundleHints');

        $result = array();
        if (!empty($hints)) {
            foreach ($hints as $bundleHint) {
                $result[] = array(
                    'title' => $bundleHint['hintTitle'],
                    'description' => $bundleHint['hintText'],
                    'bundleId' => (int)$bundleHint['bundleId'],
                );
            }
        }

        return $result;
    }

    /**
     * Filters the eligible bundles and removes the subscriptions with the wrong status
     * @param $eligibleBundles
     */
    public function filterBundlesBySubscriptionStatus(&$eligibleBundles)
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $allInstallBaseProducts = $customer->getAllInstallBaseProducts();

        foreach ($eligibleBundles as &$eligibleBundle) {
            foreach ($eligibleBundle['targetedSubscriptions'] as $key => $targetedSubscription) {
                foreach ($allInstallBaseProducts as $installBaseProduct) {
                    if ( $targetedSubscription['productId'] != null
                        && $targetedSubscription['customerNumber'] != null
                        && $installBaseProduct['product_id'] == $targetedSubscription['productId']
                        && $installBaseProduct['contract_id'] == $targetedSubscription['customerNumber']
                        && isset($installBaseProduct['product_status'])
                        && $installBaseProduct['product_status'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_R)
                    {
                        unset($eligibleBundle['targetedSubscriptions'][$key]);
                        break;
                    }
                }
            }
            $eligibleBundle['targetedSubscriptions'] = array_values($eligibleBundle['targetedSubscriptions']);
        }
    }

    public function calculateTotalsForInstalledBase($skus = [])
    {
        /** @var Dyna_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel("catalog/product");
        $totals = 0;
        foreach ($skus as $sku) {
            $product = $productModel->loadByAttribute('sku', $sku);
            if($product->getId()){
                $mafInclTax =  Mage::helper('tax')->getPrice($product,$product->getMaf(),false);
                $totals += $mafInclTax;
            }
        }

        return $totals;
    }

    /**
     * TODO: Calculate price slide and repeat for discounts
     * @param $products
     * @param int $oldPrice
     * @return int
     */
    public function calculateTotalsForProducts($products, $oldPrice = 0)
    {
        foreach ($products as $product) {
            if($product['maf']){
                if($product['type'] == 'remove'){
                    $oldPrice -= $product['maf'];
                } else {
                    $oldPrice += $product['maf'];
                }
            }else{
                $oldPrice -= $product['promoDiscount'];
            }
        }

        return $oldPrice;
    }

    /** @return Dyna_Bundles_Model_Campaign */
    protected function retrieveDefaultCampaign() {
        $dateModel = Mage::getSingleton('core/date');
        $timeNow = date('Y-m-d', $dateModel->timestamp(time()));
        $defaultCampaigns = Mage::getModel('dyna_bundles/campaign')->getCollection()->addFieldToFilter('default', 1);
        foreach ($defaultCampaigns as $campaign) {
            $timeFrom = date('Y-m-d', $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ? $campaign->getValidFrom() : time())));
            $timeTo = date('Y-m-d', $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ? $campaign->getValidTo() : time())));
            if($campaign->getId() && $timeNow >= $timeFrom && $timeNow <= $timeTo) {
                $uctParams = Mage::getSingleton('dyna_customer/session')->getUctParams();
                $uctParams['campaignId'] = $campaign->getCampaignId();
                Mage::getSingleton('dyna_customer/session')->setUctParams($uctParams);
                return $campaign;
            }
        }
        return null;
    }

    /**
     * Creates a new relation in bundle_packages
     *
     * @param $bundleId
     * @param $packageId
     */
    public function addBundlePackageEntry($bundleId, $packageId)
    {
        if ($bundleId && $packageId) {
            $existingEntries = Mage::getModel('dyna_bundles/bundlePackages')
                ->getCollection()
                ->addFieldToFilter('bundle_rule_id', $bundleId)
                ->addFieldToFilter('package_id', $packageId);

            if ($existingEntries->getSize() == 0) {
                Mage::getModel('dyna_bundles/bundlePackages')
                    ->setBundleRuleId($bundleId)
                    ->setPackageId($packageId)
                    ->save();
            }
        }
    }

    /**
     * Removes an entry from bundle_packages
     *
     * @param $bundleId
     * @param $packageId
     */
    public function removeBundlePackageEntry($bundleId, $packageId)
    {
        /** @var Dyna_Bundles_Model_BundlePackages $existingEntry */
        $existingEntry = Mage::getModel('dyna_bundles/bundlePackages')
            ->getCollection()
            ->addFieldToFilter('bundle_rule_id', $bundleId)
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();

        $existingEntry->delete();
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     */
    public function recreateBundles(Dyna_Checkout_Model_Sales_Quote $quote)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $parentQuote */
        $parentQuote = Mage::getModel('sales/quote')
            ->load($quote->getQuoteParentId());
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');

        foreach ($parentQuote->getCartPackages() as $oldPackage) {
            // get the corresponding package from the new quote
            $newPackage = $quote->getCartPackage($oldPackage->getPackageId());
            if ($newPackage) {
                $bundlePackages = Mage::getModel('dyna_bundles/bundlePackages')
                    ->getCollection()
                    ->addFieldToFilter("package_id", $oldPackage->getId());

                foreach ($bundlePackages as $bundle) {
                    $bundlesHelper->addBundlePackageEntry($bundle->getBundleRuleId(), $newPackage->getId());
                }
            }
        }
     }

     protected function hasPermission($packageType)
     {
         $customerSession = Mage::getSingleton('dyna_customer/session');
         $agent = $customerSession->getAgent();

         switch (strtolower($packageType)) {
             case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
             case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
             case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
                 return $agent->isGranted('ALLOW_CAB');
             case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
             case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
                 return $agent->isGranted('ALLOW_MOB');
             case strtolower(Dyna_Catalog_Model_Type::TYPE_LTE):
             case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
                 return $agent->isGranted('ALLOW_DSL');
             default:
                 return false;

         }
     }

    /**
     * @param null $ruleParser
     */
    public function setBundleRuleParser($ruleParser = null)
    {
        if (!$ruleParser) {
            return;
        }
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packagesQuote = $quote->getCartPackages();
        $packages = [];
        foreach ($packagesQuote as $package) {
            $packages[] = $package->getId();
        }
        $ruleParserArray = $ruleParser->toArray();
        /** @var Dyna_Bundles_Model_BundlePackages $existingEntry */
        $bundleRelations = Mage::getModel('dyna_bundles/bundlePackages')
            ->getCollection()
            ->addFieldToFilter('bundle_rule_id', $ruleParserArray['bundleRuleId'])
            ->addFieldToFilter('package_id', ['in' => $packages]);


        if ($bundleRelations && $ruleParserArray['mandatoryChooseFromFamilies']) {
            foreach ($bundleRelations as $bundleRelation) {
                $this->setMandatoryFamiliesForBundleRuleParser($bundleRelation, $ruleParserArray);
            }
        }
    }

    protected function setMandatoryFamiliesForBundleRuleParser($bundleRelation, $ruleParserArray)
    {
        $bundleRelation->setMandatoryFamilies($ruleParserArray['mandatoryChooseFromFamilies'])->save();
    }
}

