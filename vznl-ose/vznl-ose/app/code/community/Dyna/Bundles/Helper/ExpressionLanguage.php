<?php
/**
 * Wrapper over Symfony Expression Language used for gathering evaluation responses into a single response object used later for determining what sources
 * evaluate conditions to true
 * Class Dyna_Bundles_Helper_ExpressionLanguage
 */

class Dyna_Bundles_Helper_ExpressionLanguage extends Symfony\Component\ExpressionLanguage\ExpressionLanguage
{
    protected $response = array();
    /** @var Dyna_Checkout_Model_Sales_Quote */
    protected $quote = null;
    protected $simulation = false;

    /**
     * Evaluate condition against certain objects
     * Returned values are merged into $response property which can later be reset if needed
     * @param string|\Symfony\Component\ExpressionLanguage\Expression $expression
     * @param array $values
     * @return string
     */
    public function evaluate($expression, $values = array())
    {
        // forwarding not operator to expression language evaluation object as parameter (@see OVG-2301)
        $expression = preg_replace("/(![^(]+)\(([^)]+)\)/","$1($2, true)",$expression);
        /** @var Dyna_Core_Helper_Data $coreHelper */
        $coreHelper = Mage::helper('dyna_core');

        $result = $coreHelper->evaluateExpressionLanguage($expression, $values);

        // merge all results, if not empty array, into one big array
        // can be later accessed by keys ex: subscription => array(value1, value2, value3) etc
        if ($result && is_array($result)) {
            $this->response = Mage::helper('dyna_core')->arrayRecursiveUnique(array_merge_recursive($this->response, $result));
        } elseif ($result && is_bool($result)) {
            // try to get last response from registry (evaluating with "and" and "or" will result in boolean response)
            if (($newResult = Mage::registry('install_base_expression_result')) && is_array($newResult)) {
                $this->response = Mage::helper('dyna_core')->arrayRecursiveUnique(array_merge_recursive($this->response, $newResult));
            }
        }

        return $result;
    }

    /**
     * Updating this instance's simulation in order to prevent unneeded evaluations
     * @param $status
     * @return $this
     */
    public function updateSimulationStatus($status)
    {
        $this->simulation = $status;

        return $this;
    }

    /*
     * Method that resets current response after evaluation
     * Response is kept on instance until this method is called
     * @return $this
     */
    public function resetResponse()
    {
        $this->response = array();

        return $this;
    }

    /**
     * Get the entire built up response after evaluating certain rules
     * @return array
     */
    public function getResponse()
    {
        // appending RedPlus related products to response for installBase evaluation
        $this
            ->updateResponseForRedPlus()
            ->updateResponseForTargetedPackages()
            ->updateRequiredProducts();

        return $this->response;
    }

    /**
     * Updating related product for each package evaluated to true
     * @return $this
     */
    public function updateResponseForTargetedPackages()
    {
        $targetedPackages = array();

        if (!empty($this->response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT])) {
            $validatedPackages = array_unique($this->response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT]);
            foreach ($validatedPackages as $packageId) {
                $eligibleProductSku = null;
                $cartPackage = $this->getQuote()->getCartPackage($packageId);
                if ($cartPackage) {
                    foreach ($cartPackage->getAllItems() as $item) {
                        if ($item->getProduct()->isRedPlusOwner() || $item->getProduct()->isSubscription()) {
                            $eligibleProductSku = $item->getSku();
                            // break loop otherwise it will get overridden
                            break;
                        }
                    }

                    $targetedPackages[] = array(
                        'packageId' => (int)$packageId,
                        'eligibleProductSku' => $eligibleProductSku,
                    );
                }
            }
        }

        $this->response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = $targetedPackages;
        $this->response[Dyna_Bundles_Helper_Data::EXPRESSION_HIGHLIGHT_PACKAGES] = $targetedPackages;

        return $this;
    }

    /**
     * Appending RedPlus related products to response for installBase evaluation
     * @return $this
     */
    public function updateResponseForRedPlus()
    {
        // If in simulation, stop processing this flow as it is irrelevant
        if ($this->simulation) {
            return $this;
        }

        if (!empty($this->response[Dyna_Bundles_Helper_Data::EXPRESSION_INSTALL_BASE_PRODUCT])) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $bundlesHelper = $this->getBundlesHelper();
            foreach ($this->response[$bundlesHelper::EXPRESSION_INSTALL_BASE_PRODUCT] as &$evaluationResponse) {
                $evaluationResponse['relatedProductSku'] = null;
                $evaluationResponse['relatedProductId'] = null;
                foreach ($customer->getAllInstallBaseProducts() as $installBaseProduct) {
                    if ($evaluationResponse[$bundlesHelper::INSTALL_BASE_CUSTOMER_NUMBER] == $installBaseProduct['contract_id']
                        && $evaluationResponse[$bundlesHelper::INSTALL_BASE_PRODUCT_ID] == $installBaseProduct['product_id']
                        && !empty($installBaseProduct['products'])
                    ) {
                        $evaluationResponse['sharingGroupReservedStatus'] = $installBaseProduct['sharing_group_reserved_status'] ?? false;
                        // let's determine the related product sku and id
                        foreach ($installBaseProduct['products'] as $productData) {
                            // maybe multimapper has not been imported
                            if (empty($productData['product_id'])) {
                                continue;
                            }
                            $product = Mage::getModel('catalog/product')->load($productData['product_id']);
                            if ($product->isRedPlusOwner()) {
                                $evaluationResponse['relatedProductSku'] = $product->getSku();
                                $evaluationResponse['relatedProductId'] = $product->getId();
                                $evaluationResponse['sharingGroupId'] = $installBaseProduct['sharing_group_id'] ?? "0";
                                $evaluationResponse['sharingGroupReservedStatus'] = $installBaseProduct['sharing_group_reserved_status'] ?? false;
                                $evaluationResponse['subscriptionProductStatus'] = $installBaseProduct['product_status'] ?? null;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Return the bundles helper instance
     * @return Dyna_Bundles_Helper_Data|Mage_Core_Helper_Abstract
     */
    protected function getBundlesHelper()
    {
        return Mage::helper('dyna_bundles');
    }

    /**
     * Return current session quote
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        if (!$this->quote) {
            $this->quote = Mage::getSingleton('checkout/cart')->getQuote();
        }

        return $this->quote;
    }

    /**
     * Prepare response after evaluating expression for determining the products that can lead to activating a bundle
     * @return $this
     */
    protected function updateRequiredProducts()
    {
        // get targeted products from Magento registry
        $targetedProducts = Mage::registry(Dyna_Bundles_Helper_Data::EXPRESSION_REQUIRED_PRODUCTS);
        // ensure register key is set to null
        Mage::unregister(Dyna_Bundles_Helper_Data::EXPRESSION_REQUIRED_PRODUCTS);
        $productsIdsArray = array();
        if ($targetedProducts !== null) {
            /** @var Dyna_Catalog_Model_Product $productModel */
            $productModel = Mage::getModel('catalog/product');
            // convert all skus to product ids
            $productsIdsArray = $productModel->getIdsBySkus($targetedProducts);
        }

        // no association between skus and ids needed to be sent in frontend
        $this->response[Dyna_Bundles_Helper_Data::EXPRESSION_REQUIRED_PRODUCTS] = array_values($productsIdsArray);

        return $this;
    }
}
