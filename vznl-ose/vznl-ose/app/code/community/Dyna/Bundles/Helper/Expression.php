<?php

class Dyna_Bundles_Helper_Expression extends Dyna_Configurator_Helper_Expression
{
    protected $categoryPathSeparator = "/";

    // holds all properties before simulating a product added to cart
    protected $snapshot = array();

    protected $simulatingProduct = null;

    /**
     * Update helper instance simulating as if a product was added to cart in order to anticipate which products are eligible for bundling
     * @param Dyna_Catalog_Model_Product $product
     * @return $this
     */
    public function simulateProduct(Dyna_Catalog_Model_Product $product)
    {
        // If already present in quote, quote instance is up to date
        if (in_array($product->getSku(), $this->activePackageProducts)) {
            return $this;
        }

        $this->cartEmpty = false;

        $this->simulatingProduct = $product->getId();

        // proceeding further with product in quote simulation
        $sku = $product->getSku();
        // add sku to quoteProducts array
        $this->quoteProducts[$sku] = $sku;
        $this->activePackageProducts[$sku] = $sku;
        // Add sku to quote list of products
        $this->packages[$this->activePackageId][$sku] = $sku;
        // Build categories for current sku
        $this->packages[$this->activePackageId]['categories'] = array();
        foreach ($product->getCategoryIds() as $categoryId) {
            if (isset($this->allCategories[$categoryId])) {
                $this->packages[$this->activePackageId]['categories'][$categoryId] = $this->getCategoryPath($categoryId);
                // Add category id and name to the list of categories for current sku
                $this->quoteProductsCategories[$sku][$categoryId] = $this->getCategoryPath($categoryId);
                // Add category id and name to the list of categories for current sku
                $this->packages[$this->activePackageId]['categories'][$categoryId] = $this->activePackageProductsCategories[$sku][$categoryId] = $this->getCategoryPath($categoryId);
            }
        }

        // Build quote categories
        $this->quoteCategories = array_unique(array_reduce($this->quoteProductsCategories, "array_merge", array()));

        // Build active package categories
        $this->activePackageCategories = array_unique(array_reduce($this->activePackageProductsCategories, "array_merge", array()));

        return $this;
    }

    /**
     * Saves current instance properties in order to simulate a product added to cart
     * @return $this
     */
    public function takeSnapshot()
    {
        $this->snapshot = array(
            'cartEmpty' => $this->cartEmpty,
            'quoteProducts' => $this->quoteProducts,
            'quoteCategories' => $this->quoteCategories,
            'quoteProductsCategories' => $this->quoteProductsCategories,
            'activePackageCategories' => $this->activePackageCategories,
            'activePackageProducts' => $this->activePackageProducts,
            'activePackageProductsCategories' => $this->activePackageProductsCategories,
            'packages' => $this->packages,
        );

        return $this;
    }

    /**
     * Reverts to previous state and clears this snapshot
     * @return $this
     */
    public function clearSnapshot()
    {
        if (!empty($this->snapshot)) {
            $this->cartEmpty = $this->snapshot['cartEmpty'];
            $this->quoteProducts = $this->snapshot['quoteProducts'];
            $this->quoteCategories = $this->snapshot['quoteCategories'];
            $this->quoteProductsCategories = $this->snapshot['quoteProductsCategories'];
            $this->activePackageCategories = $this->snapshot['activePackageCategories'];
            $this->activePackageProducts = $this->snapshot['activePackageProducts'];
            $this->activePackageProductsCategories = $this->snapshot['activePackageProductsCategories'];
            $this->packages = $this->snapshot['packages'];

            unset ($this->snapshot);
            $this->snapshot = array();
        }

        $this->simulatingProduct = null;

        return $this;
    }
}
