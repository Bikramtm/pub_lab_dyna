<?php
/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$tables = [
    $installer->getTable('sales/quote_item'),
    $installer->getTable('sales/order_item'),
];

foreach ($tables as $table) {
    $installer->getConnection()
        ->addColumn($table, 'bundle_choice', [
            'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
            'comment' => 'Is the item added by a bundle choose rule',
            'nullable' => false,
            'default' => false,
        ]);
}


