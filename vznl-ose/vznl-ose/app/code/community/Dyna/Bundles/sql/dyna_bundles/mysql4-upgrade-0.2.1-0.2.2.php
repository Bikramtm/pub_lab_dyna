<?php
/**
 * Updating products_category_in_installed_base and products_category_in_current_quote columns from bundle_rules table to int
 * Adding two columns to save category path products_category_path_in_installed_base and products_category_path_in_current_quote
 * Adding column to save initial installed base products list, as it will be altered by rules
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

$bundleRulesTable = $this->getTable("dyna_bundles/bundle_rules");

// Changing column definitions
$this->getConnection()->changeColumn($bundleRulesTable, "products_category_in_installed_base", "products_category_in_installed_base", array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER
));

$this->getConnection()->changeColumn($bundleRulesTable, "products_category_in_current_quote", "products_category_in_current_quote", array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER
));

// Adding foreign key to catalog_category_entity
$this->getConnection()->addForeignKey(
    $this->getConnection()->getForeignKeyName($bundleRulesTable, "products_category_in_installed_base", $this->getTable("catalog/category"), "entity_id"),
    $bundleRulesTable,
    "products_category_in_installed_base",
    $this->getTable("catalog/category"),
    "entity_id"
);

$this->getConnection()->addForeignKey(
    $this->getConnection()->getForeignKeyName($bundleRulesTable, "products_category_in_current_quote", $this->getTable("catalog/category"), "entity_id"),
    $bundleRulesTable,
    "products_category_in_current_quote",
    $this->getTable("catalog/category"),
    "entity_id"
);

// Adding columns for saving paths to categories
$this->getConnection()->addColumn($bundleRulesTable, "products_category_path_in_installed_base", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'after' => "products_category_in_installed_base",
    'comment' => 'Column for saving path to installed based category condition'
));

$this->getConnection()->addColumn($bundleRulesTable, "products_category_path_in_current_quote", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'after' => "products_category_in_current_quote",
    'comment' => 'Column for saving path to installed based category condition',
));

// Adding columns for initial installed base products to package
$packageResourceTable = $this->getTable('package/package');

$this->getConnection()->addColumn($packageResourceTable, 'initial_installed_base_products', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'comment' => 'Column used for setting customer initial install base products'
));

$this->endSetup();
