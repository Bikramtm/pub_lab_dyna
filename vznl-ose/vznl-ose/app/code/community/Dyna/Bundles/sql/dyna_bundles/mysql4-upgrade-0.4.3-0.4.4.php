<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 *
 * Rename column:
 * package_type_id => package_creation_type_id
 */

/** @var Mage_Sales_Model_Entity_Setup $this */

$this->startSetup();

$bundleActionsTable = 'bundle_actions';

if ($this->getConnection()->tableColumnExists($bundleActionsTable, 'package_type_id')) {
    $this->getConnection()->changeColumn($bundleActionsTable, 'package_type_id', 'package_creation_type_id', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 100,
        'comment' => 'Used for evaluating the bundle actions on the desired package creation type id',
        'nullable' => true,
        'default' => null,
        'after' => 'condition'
    ]);
}

$this->endSetup();
