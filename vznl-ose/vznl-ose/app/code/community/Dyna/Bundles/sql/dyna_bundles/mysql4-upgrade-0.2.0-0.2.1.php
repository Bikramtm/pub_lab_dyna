<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
/**
 * Installer that creates the campaign fields resource table
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;

$installer->startSetup();

$bundleRulesTable = new Varien_Db_Ddl_Table();
$bundleRulesTable->setName($this->getTable("dyna_bundles/bundle_rules"));

$bundleRulesTable
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
        "unsigned" => true,
        "primary" => true,
        'nullable'  => false,
        "auto_increment" => true,
    ], 'ID')
    ->addColumn("bundle_rule_name", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], 'Name')
    ->addColumn("bundle_gui_name", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], 'GUI Name')
    ->addColumn("description", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], 'description')
    ->addColumn("products_category_in_installed_base", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], 'products_category_in_installed_base')
    ->addColumn("products_category_in_current_quote", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], 'products_category_in_current_quote')
    ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Website Id')
    ->addColumn("based_on_a_lifecycle", Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, [
    ], 'based_on_a_lifecycle')
    ->addColumn("display_bundle_benefits1", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], 'display_bundle_benefits1')
    ->addColumn("display_bundle_benefits2", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], 'display_bundle_benefits2')
    ->addForeignKey(
        $this->getFkName($bundleRulesTable->getName(), 'website_id', 'core/website', 'website_id'),
        'website_id', $this->getTable('core/website'), 'website_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
;
if (!$installer->tableExists($this->getTable("dyna_bundles/bundle_rules"))) {
    $this->getConnection()->createTable($bundleRulesTable);
}


$bundlesActionsTable = new Varien_Db_Ddl_Table();
$bundlesActionsTable->setName($this->getTable("dyna_bundles/bundle_actions"));

$bundlesActionsTable
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
        "unsigned" => true,
        "primary" => true,
        "auto_increment" => true,
        'nullable'  => false,
    ], 'id')
    ->addColumn('bundle_rule_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
        "unsigned" => true,
        'nullable'  => false,
    ], 'bundle_rule_id')
    ->addColumn("rule_type", Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
    ], 'rule_type')
    ->addColumn("scope", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], 'scope')
    ->addColumn("condition", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], 'condition')
    ->addColumn("action", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], 'product')
    ->addColumn("priority", Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'default' => '1'
    ], 'action')
    ->addForeignKey(
        $this->getFkName($bundleRulesTable->getName(), 'bundle_rule_id', 'dyna_bundles/bundle_rules', 'id'),
        'bundle_rule_id', $this->getTable('dyna_bundles/bundle_rules'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
;
if (!$installer->tableExists($this->getTable("dyna_bundles/bundle_actions"))) {
    $this->getConnection()->createTable($bundlesActionsTable);
}


$installer->endSetup();
