<?php
/**
 * Adding unique indexes to campaign_id, offer_id and bundle_rule_name
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

$this->getConnection()->changeColumn($this->getTable("dyna_bundles/campaign"), "campaign_id", "campaign_id", array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'length' => 255
));
$this->getConnection()->changeColumn($this->getTable("dyna_bundles/campaign_offer"), "offer_id", "offer_id", array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'length' => 255
));
$this->getConnection()->changeColumn($this->getTable("dyna_bundles/bundle_rules"), "bundle_rule_name", "bundle_rule_name", array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'length' => 255
));

$this->getConnection()
  ->addIndex(
    $this->getTable("dyna_bundles/campaign"),
    $this->getIdxName('campaign_id', array('campaign_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
    array('campaign_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);
$this->getConnection()
  ->addIndex(
    $this->getTable("dyna_bundles/campaign_offer"),
    $this->getIdxName('offer_id', array('offer_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
    array('offer_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);
$this->getConnection()
  ->addIndex(
    $this->getTable("dyna_bundles/bundle_rules"),
    $this->getIdxName('bundle_rule_name', array('bundle_rule_name'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
    array('bundle_rule_name'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$this->endSetup();
