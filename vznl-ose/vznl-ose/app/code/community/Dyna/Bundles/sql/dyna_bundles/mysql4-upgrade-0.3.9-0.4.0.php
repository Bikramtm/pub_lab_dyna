<?php
/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$tables = [
    $installer->getTable('sales/quote_item'),
    $installer->getTable('sales/order_item'),
];

foreach ($tables as $table) {
    $installer->getConnection()
        ->changeColumn(
            $table,
            'bundle_component',
            'bundle_component',
            [
                'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
                'comment' => 'Is the item part of a bundle',
                'nullable' => false,
                'default' => 0,
            ]
        );
}