<?php
/**
 * Installer that changes structure for bundles in wave 2
 * Dropping columns from bundle_rules as condition moves to source expression
    products_category_in_current_quote
    products_category_path_in_current_quote
    products_expression_in_installed_base
 * Updating bundle rules with other columns
 */


/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$bundleRulesTable = $installer->getTable('dyna_bundles/bundle_rules');

// Removing product in category condition columns as conditioning is moving to source expression evaluation
$installer->getConnection()->dropColumn($bundleRulesTable, "products_category_in_current_quote");
$installer->getConnection()->dropColumn($bundleRulesTable, "products_category_path_in_current_quote");
$installer->getConnection()->dropColumn($bundleRulesTable, "products_expression_in_installed_base");

// Updating bundles with new columns according to OVG-675
$installer->getConnection()->addColumn($bundleRulesTable, "bundle_type", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 45,
    'comment' => 'Used for determining what type of bundle it is: RedPlus / MarketingBundle / SurfSofortDSL / SurfSofortKIP / Other',
    'nullable' => true,
    'default' => null,
));

$installer->getConnection()->addColumn($bundleRulesTable, "ogw_orchestration_type", array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'length' => 1,
    'comment' => '0, 1, 2 3 or 4',
    'nullable' => true,
    'default' => null,
));

$installer->getConnection()->addColumn($bundleRulesTable, "hint_enabled", array(
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'comment' => 'Hint enabled or no',
    'nullable' => false,
    'default' => 0,
));

$installer->getConnection()->addColumn($bundleRulesTable, "hint_text", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'comment' => 'Text to be displayed in hint section',
    'nullable' => true,
    'default' => null,
));

$installer->getConnection()->addColumn($bundleRulesTable, "hint_location", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 20,
    'comment' => 'Location where the hint should be displayed (package subtype code, draw or cart)',
    'nullable' => true,
    'default' => null,
));

// Creating bundle conditions table
$bundlesConditionsTable = new Varien_Db_Ddl_Table();
$bundlesConditionsTable->setName($this->getTable("dyna_bundles/bundle_conditions"));

$bundlesConditionsTable
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, array(
        "unsigned" => true,
        "primary" => true,
        "auto_increment" => true,
        'nullable' => false,
    ), 'Bundle condition id')
    ->addColumn('bundle_rule_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, array(
        "unsigned" => true,
        'nullable' => false,
    ), 'Reference to the bundle rule to which this condition is related')
    ->addColumn("condition", Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'The condition which will be evaluated by source expression')
    ->addForeignKey(
        $installer->getFkName($bundlesConditionsTable->getName(), 'bundle_rule_id', $bundleRulesTable, 'id'),
        'bundle_rule_id', $bundleRulesTable, 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    );

if (!$installer->tableExists($this->getTable("dyna_bundles/bundle_conditions"))) {
    $this->getConnection()->createTable($bundlesConditionsTable);
}

$installer->endSetup();
