<?php

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$bundleRulesTable = $installer->getTable('dyna_bundles/bundle_rules');


if ($installer->tableExists($bundleRulesTable)) {
    if (!$installer->getConnection()->tableColumnExists($bundleRulesTable, "hint_title")) {
        $installer->getConnection()->addColumn($bundleRulesTable, "hint_title", array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 200,
            'comment' => 'Used for displaying the title of the bundle hint',
            'nullable' => true,
            'default' => null,
            'after' => 'hint_enabled'
        ));
    }
}

$this->endSetup();
