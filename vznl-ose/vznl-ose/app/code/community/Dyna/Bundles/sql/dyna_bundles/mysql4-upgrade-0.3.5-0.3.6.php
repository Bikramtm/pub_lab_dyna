<?php

/** @var Mage_Sales_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$bundleRulesTable = $installer->getTable('dyna_bundles/bundle_rules');
$packagesTable = $installer->getTable('package/package');

// Create bundle_packages table
if (!$installer->getConnection()->isTableExists($installer->getTable('dyna_bundles/bundle_packages'))) {
    $bundlePackagesTable = new Varien_Db_Ddl_Table();
    $bundlePackagesTable->setName($installer->getTable('dyna_bundles/bundle_packages'));

    $bundlePackagesTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            "unsigned" => true,
            "primary" => true,
            "auto_increment" => true,
            "nullable" => false,
        ), 'Bundle package link id')
        ->addColumn('bundle_rule_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            "unsigned" => true,
            "nullable" => false
        ), 'Reference to the bundle rule')
        ->addColumn('package_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            "unsigned" => true,
            "nullable" => false
        ), 'Reference to the entity id of catalog package')
        ->addForeignKey($installer->getFkName($bundlePackagesTable->getName(), 'bundle_rule_id', $bundleRulesTable, 'id'),
            'bundle_rule_id', $bundleRulesTable, 'id')
        ->addForeignKey($installer->getFkName($bundlePackagesTable->getName(), 'package_id', $packagesTable, 'entity_id'),
            'package_id', $packagesTable, 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

    $installer->getConnection()->createTable($bundlePackagesTable);
}

// remove the bundle_id from catalog_package
if ($installer->getConnection()->tableColumnExists($packagesTable, 'bundle_id')) {
    $installer->getConnection()->dropColumn($packagesTable, 'bundle_id');
}

$installer->endSetup();

