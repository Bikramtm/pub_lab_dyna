<?php
/**
 * Installer that updates bundle_campaign_dealercode
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;

$installer->startSetup();

$table = $this->getTable('dyna_bundles/campaign_dealercode');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

// Rename primary_dealer_code to red_sales_id
if ($connection->isTableExists($table) && $connection->tableColumnExists($table,"primary_dealer_code")) {
    $connection->changeColumn($table, "primary_dealer_code", "red_sales_id", array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'length' => 50
    ));
}

// Remove package_type, scenario and secondary_dealer_code as these attributes are not used anymore on Wave 3 for getting the proper red sales id in an uct campaign flow
$fields = [
    'package_type',
    'scenario',
    'secondary_dealer_code'
];
foreach ($fields as $field) {
    if ($connection->tableColumnExists($table,$field)) {
        $connection->dropColumn($table, $field);
    }
}


$installer->endSetup();
