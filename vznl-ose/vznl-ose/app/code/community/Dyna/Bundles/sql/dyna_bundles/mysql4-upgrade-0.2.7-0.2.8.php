<?php
/**
 * Adding product ids to campaign offer table
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_offer"), "product_ids", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => 'Offer Product IDs'
));

$this->run("DROP TABLE IF EXISTS `bundle_campaign_offer_product`;");

$this->endSetup();
