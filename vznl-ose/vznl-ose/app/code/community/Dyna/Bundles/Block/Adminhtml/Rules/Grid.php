<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Rules_Grid
 */
class Dyna_Bundles_Block_Adminhtml_Rules_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('bundleRulesGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        /** @var Dyna_Bundles_Model_BundleAction $collection */
        $collection = Mage::getModel('dyna_bundles/bundleRule')->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Create the admin grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('id', array(
            'header' => $this->_getColumnTitle('ID'),
            'align' => 'left',
            'width' => '30px',
            'type' => 'number',
            'index' => 'id',
        ));

        $this->addColumn('bundle_rule_name', array(
            'header' => Mage::helper('bundles')->__('Bundle Rule Name'),
            'align' => 'left',
            'width' => '30px',
            'index' => 'bundle_rule_name',
        ));

        $this->addColumn('bundle_gui_name', array(
            'header' => $this->_getColumnTitle('Bundle Gui Name'),
            'align' => 'left',
            'width' => '30px',
            'index' => 'bundle_gui_name',
        ));

        $this->addColumn('description', array(
            'header' => $this->_getColumnTitle('Description'),
            'align' => 'left',
            'index' => 'description',
        ));

        $this->addColumn('website_id', array(
            'header' => $this->_getColumnTitle('Website'),
            'align' => 'left',
            'index' => 'website_id',
            'type' => 'options',
            'options' => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(false),
        ));

        return $this;
    }


    function _getColumnTitle($text){
        return Mage::helper('bundles')->__($text);
    }

    /**
     * Get edit url for a record
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
