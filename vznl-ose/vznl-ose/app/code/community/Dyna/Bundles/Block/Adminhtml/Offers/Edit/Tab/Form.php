<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Offers_Edit_Tab_Form
 */
class Dyna_Bundles_Block_Adminhtml_Offers_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Class constructor
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setDestElementId('edit_form');
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldSet = $form->addFieldset("bundle_offers_form", array("legend" => Mage::helper("bundles")->__("Offer Information")));
        $offerData = null;

        if (Mage::getSingleton("adminhtml/session")->getOffersData()) {
            $offerData = Mage::getSingleton("adminhtml/session")->getOffersData();
            Mage::getSingleton("adminhtml/session")->getOffersData(null);
        } elseif (Mage::registry("bundle_offers_data")) {
            $offerData = Mage::registry("bundle_offers_data")->getData();
        }

        $fieldSet->addField("offer_id", "text", array(
            "label" => Mage::helper("bundles")->__("Offer Identification"),
            "class" => "required-entry",
            "required" => true,
            "name" => "offer_id",
        ));

        $fieldSet->addField("campaign_ids", "multiselect", array(
            "label" => Mage::helper("bundles")->__("Campaign"),
            "required" => true,
            "name" => "campaign_ids[]",
            "values" => $this->getCampaigns()
        ));

        $fieldSet->addField("title_description", "text", array(
            "label" => Mage::helper("bundles")->__("Title description"),
            "name" => "title_description"
        ));

        $fieldSet->addField("usp1", "text", array(
            "label" => Mage::helper("bundles")->__("USP 1"),
            "name" => "usp1"
        ));

        $fieldSet->addField("usp2", "text", array(
            "label" => Mage::helper("bundles")->__("USP 2"),
            "name" => "usp2"
        ));

        $fieldSet->addField("discounted_months", "text", array(
            "label" => Mage::helper("bundles")->__("Discounted months"),
            "name" => "discounted_months"
        ));

        $fieldSet->addField("discounted_price_per_month", "text", array(
            "label" => Mage::helper("bundles")->__("Discounted price per month"),
            "name" => "discounted_price_per_month"
        ));

        $fieldSet->addField("undiscounted_price_per_month", "text", array(
            "label" => Mage::helper("bundles")->__("Undiscounted price per month"),
            "name" => "undiscounted_price_per_month"
        ));

        $fieldSet->addField('product_ids', 'text', array(
            'label' => Mage::helper("bundles")->__('Product IDs'),
            'name' => 'product_ids'
        ));

        $form->setValues($offerData);
        
        return parent::_prepareForm();
    }


    protected function getCampaigns()
    {
        /** @var Dyna_Bundles_Model_Campaign $categoriesCollection */
        $campaigns = Mage::getModel('dyna_bundles/campaign')->getCollection();
        $selectable_collection = [];
        foreach ($campaigns as $campaign) {
            $selectable_collection[] = [
                'label' => $campaign->getCampaignId(),
                'value' => $campaign->getId()
            ];
        }

        return $selectable_collection;
    }
}
