<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Actions_Grid
 */
class Dyna_Bundles_Block_Adminhtml_Actions_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('bundleActionGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        /** @var Dyna_Bundles_Model_BundleAction $collection */
        $collection = Mage::getModel('dyna_bundles/bundleAction')->getCollection();
        $collection->getSelect()->joinLeft( array('rules'=>'bundle_rules'),'main_table.bundle_rule_id = rules.id',['bundle_rule_name'=>'rules.bundle_rule_name']);

        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Create the admin grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */

    protected function _prepareColumns()
    {

        parent::_prepareColumns();

        $this->addColumn('id', array(
            'header' => $this->_getColumnTitle('ID'),
            'align' => 'left',
            'width' => '30px',
            'type' => 'number',
            'index' => 'id',
        ));

        $this->addColumn('bundle_rule_name', array(
            'header' => Mage::helper('bundles')->__('Bundle Rule Name'),
            'align' => 'left',
            'index' => 'bundle_rule_name',
        ));

        $this->addColumn('scope', array(
            'header' => $this->_getColumnTitle('Scope'),
            'align' => 'left',

            'index' => 'scope',
        ));

        $this->addColumn('condition', array(
            'header' => $this->_getColumnTitle('Condition'),
            'align' => 'left',

            'index' => 'condition',
        ));

        $this->addColumn('action', array(
            'header' => $this->_getColumnTitle('Action'),
            'align' => 'left',

            'index' => 'action',
        ));

        $this->addColumn('priority', array(
            'header' => $this->_getColumnTitle('Priority'),
            'align' => 'left',
            'index' => 'priority',
            'width' => '20px'
        ));

        return $this;
    }
    /**
     * Get edit url for a record
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    function _getColumnTitle($text){
        return Mage::helper('bundles')->__($text);
    }
}
