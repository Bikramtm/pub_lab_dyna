<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Rules
 */
class Dyna_Bundles_Block_Adminhtml_Rules extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Dyna_Bundles_Block_Adminhtml_Rules constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_rules';
        $this->_blockGroup = 'dyna_bundles';
        $this->_headerText = Mage::helper('bundles')->__('Manage Bundle Rules');
        
        parent::__construct();
    }
}
