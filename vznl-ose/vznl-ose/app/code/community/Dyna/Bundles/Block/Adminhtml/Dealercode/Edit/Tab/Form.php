<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Dealercode_Edit_Tab_Form
 */
class Dyna_Bundles_Block_Adminhtml_Dealercode_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Class constructor
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setDestElementId('edit_form');
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldSet = $form->addFieldset("bundle_dealercode_form", array("legend" => Mage::helper("bundles")->__("Dealer code Information")));

        $fieldSet->addField("agency", "text", array(
            "label" => Mage::helper("bundles")->__("Agency"),
            "class" => "required-entry",
            "name" => "agency"
        ));

        $fieldSet->addField("city", "text", array(
            "label" => Mage::helper("bundles")->__("City"),
            "name" => "city"
        ));

        $fieldSet->addField("marketing_code", "text", array(
            "label" => Mage::helper("bundles")->__("Marketing code"),
            "name" => "marketing_code",
        ));

        $fieldSet->addField("cluster_category", "text", array(
            "label" => Mage::helper("bundles")->__("Cluster category"),
            "name" => "cluster_category",
        ));

        $fieldSet->addField("red_sales_id", "text", array(
            "label" => Mage::helper("bundles")->__("Red Sales Id"),
            "name" => "red_sales_id",
            "required" => true
        ));

        $fieldSet->addField("comment1", "textarea", array(
            "label" => Mage::helper("bundles")->__("First Comment"),
            "name" => "comment1"
        ));

        $fieldSet->addField("comment2", "textarea", array(
            "label" => Mage::helper("bundles")->__("Second comment"),
            "name" => "comment2"
        ));

        if (Mage::getSingleton("adminhtml/session")->getDealercodeData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getDealercodeData());
            Mage::getSingleton("adminhtml/session")->getDealercodeData(null);
        } elseif (Mage::registry("bundle_dealercode_data")) {
            $form->setValues(Mage::registry("bundle_dealercode_data")->getData());
        }
        
        return parent::_prepareForm();
    }
}
