<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Dealercode
 */
class Dyna_Bundles_Block_Adminhtml_Dealercode extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Dyna_Bundles_Block_Adminhtml_Dealercode constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_dealercode';
        $this->_blockGroup = 'dyna_bundles';
        $this->_headerText = Mage::helper('bundles')->__('Manage Dealer Codes');

        parent::__construct();
    }
}
