<?php

class Dyna_Bundles_Block_Options extends Mage_Core_Block_Template
{
    /**
     * @var Dyna_Bundles_Helper_Data|Mage_Core_Helper_Abstract|Omnius_Bundles_Helper_Data
     */
    protected $bundleHelper;

    /**
     * Dyna_Bundles_Block_Options constructor.
     */
    public function __construct(array $args = array())
    {
        $this->bundle = null;
        $this->bundleHelper = Mage::helper('bundles');

        parent::__construct();
    }

    /**
     * @return bool
     */
    public function hasBundleOptions()
    {
        return $this->bundleHelper->isBundleChoiceFlow();
    }

    public function getBundleChoiceId()
    {
        /** @var Dyna_Bundles_Helper_Data $checkoutCartHelper */
        $bundleHelper = Mage::helper('dyna_bundles');
        $parserResponse = $bundleHelper->getBundleRuleParser();
        unset($bundleHelper);
        if ($parserResponse) {
            return $this->bundleHelper->savedParserResponseAppliesToQuote($parserResponse);
        }

        return null;
    }

    /**
     * @return array
     */
    public function getBundleOptions()
    {
        $families = $this->bundleHelper->getAllMandatoryFamilies();
        $families = $this->getFamiliesFormat($families);
        $products = $this->getProductsFromFamilies($families);

        return [
            'families' => $families,
            'products' => $products,
            'familyKey' => key($families),
            'activePackageId' => Mage::getSingleton('checkout/cart')->getQuote()->getActivePackage()->getEntityId(),
            'handleDummyPackage' => $this->checkBundleForDummy($products)
        ];
    }

    /**
     * @param $products
     * @return bool|mixed
     */
    public function checkBundleForDummy($products)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packages = $quote->getCartPackages();

        foreach ($packages as $package) {
            if ($package['editing_disabled'] && !empty($products[$package['entity_id']])) {
                return $package['entity_id'];
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getBundleSelectedProductIds()
    {
        $selectedProducts = [];
        $packageId = Mage::getSingleton('checkout/cart')->getQuote()->getCartPackage()->getId();
        $options = $this->getBundleOptions();
        $products = $options['products'][$packageId] ?? [];
        if (is_array($products)) {
            foreach ($products as $productsList) {
                foreach ($productsList as $product) {
                    if ($product['selected'] === 'true') {
                        $selectedProducts[] = $product['productID'];
                    }
                }
            }
        }

        return $selectedProducts;
    }

    /**
     * @param $allMandatoryFamilies
     * @return array
     */
    protected function getFamiliesFormat($allMandatoryFamilies)
    {
        $familiesResult = [];
        foreach ($allMandatoryFamilies as $packageId => $families) {
            foreach ($families as $family) {
                $familiesResult[$packageId][$family['familyId']] = [
                    'name' => $family['familyName'],
                    'packageId'=>$family['packageId']
                ];
            }
        }


        return $familiesResult;
    }

    /**
     * Returns a list of products for each family
     * @param $formattedFamilies
     * @return array
     */
    protected function getProductsFromFamilies($formattedFamilies)
    {
        $products = [];
        $categoryModel = Mage::getModel("dyna_catalog/category");
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packages = $quote->getCartPackages();
        $quoteItems = $quote->getAllItems();

        $cartItems = [];
        $mappedPackages = [];

        foreach ($packages as $package) {
            $mappedPackages[$package['package_id']] =  $package['entity_id'];
        }

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItem */
        foreach ($quoteItems as $quoteItem) {
            $cartItems[$mappedPackages[$quoteItem->getPackageId()]][] = $quoteItem->getProductId();
        }

        unset($packages, $package, $quoteItems);

        foreach ($formattedFamilies as $packageID => $formattedFamily) {
            foreach ($formattedFamily as $familyId => $family) {
                $currentProducts = [];
                $prods = $categoryModel->load($familyId)->getProductCollection()->addAttributeToSelect('name');

                foreach ($prods as $prod) {
                    $selected = in_array($prod->getId(), $cartItems[$packageID]) ? 'true' : 'false';
                    $currentProducts[] = [
                        'ID' => $prod->getSku(),
                        'name' => $prod->getName(),
                        'selected' => $selected,
                        'productID' => $prod->getId(),
                    ];
                }

                $products[$packageID][$familyId] = $currentProducts;
            }
        }

        return $products;
    }

    public function createDataBundleFromBundleOptions($dataBundle, $bundleOptions, $packageId, $block)
    {
        if (!empty($bundleOptions['products'][$packageId]) && !empty($bundleOptions['families'][$packageId]) && !empty($block->getBundleSelectedProductIds($packageId))) {
            $dataBundle[$packageId] = [
                'productsContainer' => $bundleOptions['products'][$packageId],
                'families' => $bundleOptions['families'][$packageId],
                'selectedProducts' => $block->getBundleSelectedProductIds($packageId)
            ];
        }

        return $dataBundle;
    }
}
