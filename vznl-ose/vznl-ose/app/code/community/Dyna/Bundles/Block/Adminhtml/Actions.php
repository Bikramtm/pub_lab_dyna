<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Actions
 */
class Dyna_Bundles_Block_Adminhtml_Actions extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Dyna_Bundles_Block_Adminhtml_Actions constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_actions';
        $this->_blockGroup = 'dyna_bundles';
        $this->_headerText = Mage::helper('bundles')->__('Manage Bundle Actions');

        parent::__construct();
    }
}
