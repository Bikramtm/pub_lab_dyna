<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Campaigns_Edit_Tab_Form
 */
class Dyna_Bundles_Block_Adminhtml_Campaigns_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Class constructor
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setDestElementId('edit_form');
    }

    /**
     * @return Dyna_Bundles_Block_Adminhtml_Campaigns_Edit_Tab_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldSet = $form->addFieldset("bundle_campaigns_form", array("legend" => Mage::helper("bundles")->__("Bundle Campaigns Information")));

        $fields = [
            array(
                "name" => "campaign_id",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("bundles")->__("Campaign ID"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "campaign_id",
                )
            ),
            array(
                "name" => "cluster",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("bundles")->__("Cluster"),
                    "name" => "cluster",
                )
            ),
            array(
                "name" => "cluster_category",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("bundles")->__("Cluster category"),
                    "name" => "cluster_category",
                )
            ),
            array(
                "name" => "advertising_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("bundles")->__("Advertising code"),
                    "name" => "advertising_code",
                )
            ),
            array(
                "name" => "campaign_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("bundles")->__("Campaign code"),
                    "name" => "campaign_code",
                )
            ),
            array(
                "name" => "marketing_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("bundles")->__("Marketing code"),
                    "name" => "marketing_code",
                )
            ),
            array(
                "name" => "promotion_hint",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("bundles")->__("Promotion hint"),
                    "name" => "promotion_hint",
                )
            ),
            array(
                "name" => "valid_from",
                "type" => "date",
                "parameters" => array(
                    'name'   => 'valid_from',
                    'label'  => Mage::helper('bundles')->__('Valid from'),
                    'title'  => Mage::helper('bundles')->__('Valid from'),
                    'image'  => $this->getSkinUrl('images/grid-cal.gif'),
                    'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
                    'format'       => Varien_Date::DATE_INTERNAL_FORMAT
                )
            ),
            array(
                "name" => "valid_to",
                "type" => "date",
                "parameters" => array(
                    'name'   => 'valid_to',
                    'label'  => Mage::helper('bundles')->__('Valid to'),
                    'title'  => Mage::helper('bundles')->__('Valid to'),
                    'image'  => $this->getSkinUrl('images/grid-cal.gif'),
                    'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
                    'format'       => Varien_Date::DATE_INTERNAL_FORMAT
                )
            ),
            array(
                "name" => "offer_hint",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("bundles")->__("Offer hint"),
                    "name" => "offer_hint",
                )
            ),
            array(
                "name" => "campaign_hint",
                "type" => "textarea",
                "parameters" => array(
                    "label" => Mage::helper("bundles")->__("Campaign hint"),
                    "name" => "campaign_hint",
                )
            ),
        ];

        foreach ($fields as $field) {
            $fieldSet->addField($field["name"], $field["type"], $field["parameters"]);
        }

        if (Mage::getSingleton("adminhtml/session")->getCampaignsData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getCampaignsData());
            Mage::getSingleton("adminhtml/session")->getCampaignsData(null);
        } elseif (Mage::registry("bundle_campaigns_data")) {
            $form->setValues(Mage::registry("bundle_campaigns_data")->getData());
        }

        return parent::_prepareForm();
    }
}
