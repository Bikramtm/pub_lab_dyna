<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Bundles_Block_Adminhtml_Campaigns_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Omnius_MultiMapper_Block_Adminhtml_Mapper_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId("bundles_campaigns_edit_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("bundles")->__("Campaign Information"));
    }

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab("edit_form", array(
            "label" => Mage::helper("bundles")->__("Campaign Information"),
            "title" => Mage::helper("bundles")->__("Campaign Information"),
            'content' => $this->getLayout()->createBlock('dyna_bundles/adminhtml_campaigns_edit_tab_form')->toHtml(),
        ));

        $this->addTab("edit_form_actions_view", array(
            "label" => Mage::helper("bundles")->__("See linked offers"),
            "title" => Mage::helper("bundles")->__("See linked offers"),
            'content' => $this->getLayout()->createBlock('dyna_bundles/adminhtml_campaigns_offers')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}
