<?php

/**
 * Class Dyna_Bundles_Model_Import_Campaign
 */
class Dyna_Bundles_Model_Import_CampaignDealercode extends Dyna_Bundles_Model_Import_Abstract
{
    /** @var string $_logFileName */
    protected $_logFileName = "bundle_campaign_dealercode_import";

    /**
     * Import campaign
     *
     * @param array $data
     *
     * @return void
     */
    public function import($data)
    {
        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;
        $mandatoryAttributes = ['RedSalesId', 'ClusterCategory'];
        foreach ($mandatoryAttributes as $attribute) {
            if (!isset($data[$attribute]) || empty($data[$attribute])) {
                $this->_logError('Skipping line without ' . $attribute);
                $skip = true;
            }
        }
        $data = $this->_setDataMapping($data);

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        /**
         * OMNVFDE-4580
         * For the campaign dealercode import, we must first check for data entries
         * that are separated by commas. Where such entries are found, multiple import rows
         * will be created, each having one single value of the set
         */
        $splitRows = $this->buildImportLines($data);

        foreach ($splitRows as $data) {
            $this->_log('Start importing ' . $data['comment1'] . ' ' . $data['comment2'], false);

            try {
                /** @var Dyna_Bundles_Model_CampaignDealercode $campaignDealercode */
                $campaignDealercode = Mage::getModel('dyna_bundles/campaignDealercode');

                $params = [
                    'agency',
                    'city',
                    'marketing_code',
                    'cluster_category',
                    'red_sales_id',
                    'comment1',
                    'comment2',
                ];

                // all parameters are default null
                $defaultParams = array_combine($params, array_fill(0, count($params), null));

                $this->_setDataWithDefaults($campaignDealercode, $data, $defaultParams);

                if (!$this->getDebug()) {
                    $campaignDealercode->save();
                }

                unset($campaignDealercode);
                $this->_qties++;

                $this->_log('Finished importing ' . $data['comment1'] . ' ' . $data['comment2'], false);
            } catch (Exception $ex) {
                echo $ex->getMessage();
                $this->_logEx($ex);
            }
        }
    }

    /**
     * Process custom data mapping for cable products
     *
     * @param array $data
     *
     * @return array
     */
    protected function _setDataMapping($data)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }
        return [
          'agency' => $data['Agency'] ?? null,
          'city' => $data['City'] ?? null,
          'marketing_code' => $data['MarketingCode'] ?? null,
          'cluster_category' => $data['ClusterCategory'] ?? null,
          'red_sales_id' => $data['RedSalesId'] ?? null,
          'comment1' => $data['Comment1'] ?? null,
          'comment2' => $data['Comment2'] ?? null,
        ];
    }

    /**
     * Splits the import line into multiple unique lines
     *
     * @param $importLine
     * @return array
     */
    protected function splitImportLine($importLine) {
        $result = array(
            array()
        );
        foreach ($importLine as $key => $values) {
            $newLine = array();
            foreach ($result as $resultItem) {
                foreach ($values as $value) {
                    $newLine[] = array_merge($resultItem, array($key => $value));
                }
            }
            $result = $newLine;
        }
        return $result;
    }

    /**
     * Method used to create separate import lines for each field that contains
     * data separated by commas
     *
     * @param $importLine
     * @return array
     */
    public function buildImportLines($importLine)
    {
        $multiDataRows = [
            'agency',
            'city',
            'marketing_code',
            'cluster_category',
            'package_type',
            'scenario'
        ];

        // first map the multiple values as arrays
        foreach ($importLine as $rowName => $data) {
            if (in_array($rowName, $multiDataRows) && strpos($data, ',') !== false) {
                $rowValues = array_map('trim', explode(',', $data));
                $importLine[$rowName] = $rowValues;
            } else {
                $importLine[$rowName] = [$data];
            }
        }

        // build multiple import lines by splitting the multiple values
        $lines = $this->splitImportLine($importLine);

        return $lines;
    }
}
