<?php

/**
 * Class Dyna_Bundles_Model_BundlePackages
 * @method getBundleRuleId()
 * @method getPackageId()
 */
class Dyna_Bundles_Model_BundlePackages extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_bundles/bundlePackages');
    }

    public function setMandatoryFamilies($value = null)
    {
        $this->setData('mandatory_families', $value == null ? null : serialize($value));
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getMandatoryFamilies()
    {
        return $this->getData('mandatory_families') == null ? null : unserialize($this->getData('mandatory_families'));
    }
}
