<?php

/**
 * Class AbstractCampaign
 */
abstract class Dyna_Bundles_Model_Import_Abstract extends Omnius_Import_Model_Import_ImportAbstract
{
    /** @var Dyna_Import_Helper_Data $_helper */
    public $_helper;

    /**
     * Dyna_Cable_Model_Import_CableProduct constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper->setImportLogFile($this->_logFileName . '.log');
        $this->_qties = 0;
    }

    /**
     * Process custom data mapping for cable products
     *
     * @param array $data
     *
     * @return array
     */
    protected function _setDataMapping($data)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }

        foreach ($data as &$value) {
            if (is_array($value) && empty($value)) {
                $value = null;
            }
        }
        return $data;
    }

    /**
     * Set product model default values
     *
     * @param Mage_Core_Model_Abstract $modelInstance
     * @param array $data
     * @param array $defaults
     */
    protected function _setDataWithDefaults($modelInstance, $data, $defaults)
    {
        $modelInstance->addData(array_replace($defaults, $data));
    }

    /**
     * @param $date
     *
     * @return bool|null|string
     */
    protected function formatDate($date)
    {
        /**
         * if date is set strip the character "-" from date
         * this is needed since the csv can contain instead of an empty cell the character "-"
         */
        if (trim($date) == "-" || !$date) {
            return null;
        }
        return date("Y-m-d", strtotime($date));
    }

    /**
     * @param string $price
     *
     * @return mixed
     */
    protected function cleanPriceString($price)
    {
        return str_replace(',', '.', preg_replace('/[^\d\,]/', '', $price));
    }

    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }
    /**
     * Log exception
     *
     * @param $ex Mage_Exception
     */
    protected function _logEx($ex)
    {
        $this->_logError('Import threw an exception: ');
        $this->_helper->logMsg($ex->getMessage());
    }

    abstract public function import($data);
}
