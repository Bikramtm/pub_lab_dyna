<?php

/**
 * Class Dyna_Bundles_Model_Expression_PromoOnlyPackageScope
 * Reevaluate bundle rules but execute addPromoProduct rules only, all other actions must return true
 * This should probably be supported at the parser level
 * Other actions ar
 */
class Dyna_Bundles_Model_Expression_PromoOnlyPackageScope extends Dyna_Bundles_Model_Expression_PackageScope
{
    /**
     * @param array ...$skus
     * @return bool
     */
    public function add(...$skus)
    {
        return true;
    }

    /**
     * @param $skuToReplace
     * @param $skusToReplaceWith
     * @return bool
     */
    public function replace($skuToReplace, $skusToReplaceWith)
    {
        return true;
    }

    /**
     * @param array ...$skusToDelete
     * @return bool
     */
    public function delete(...$skusToDelete)
    {
        return true;
    }

    /**
     * @param $category
     * @param null $default
     * @return bool
     */
    public function chooseFromCategory($category, $default = null)
    {
        return false;
    }

    /**
     * @param $category
     * @param null $default
     * @return bool
     */
    public function chooseFromCategoryId($category, $default = null)
    {
        return true;
    }

    /**
     * @param $categoryName
     * @return bool
     */
    public function containsProductInCategory($categoryName): bool
    {
        return true;
    }
}
