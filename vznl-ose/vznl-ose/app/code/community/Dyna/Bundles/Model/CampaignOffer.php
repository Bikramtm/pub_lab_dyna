<?php

/**
 * Class Dyna_Bundles_Model_CampaignOffer
 */
class Dyna_Bundles_Model_CampaignOffer extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_bundles/campaignOffer');
    }

    public function getUndiscountedFromMonth() {
        return $this->getDiscountedMonths() + 1;
    }
}
