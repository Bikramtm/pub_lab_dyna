<?php

class Dyna_Bundles_Model_Expression_QuotePackageDummyScope extends Dyna_Bundles_Model_Expression_QuotePackageScope
{
    /** @var Dyna_Checkout_Model_Sales_Quote|Mage_Sales_Model_Quote */
    protected $quote;
    /** @var  Dyna_Bundles_Model_BundleRule */
    protected $bundle;
    /** @var mixed */
    protected $categories;
    /** @var array|Omnius_Checkout_Model_Sales_Quote_Item[] */
    protected $quoteItems;
    /** @var Dyna_Configurator_Helper_Cart */
    protected $cartHelper = null;
    /** @var Dyna_Core_Helper_Data */
    protected $dynaCoreHelper = null;

    public $bundleProductsSummary;
    protected $chooseCategory;


    public function __construct($args)
    {
        parent::__construct($args);
        if (empty($args['bundle']) || !($args['bundle'] instanceof Dyna_Bundles_Model_BundleRule)) {
            throw new InvalidArgumentException("invalid or no bundle received for quote package scope");
        }

        $this->bundle = $args['bundle'];
        $this->quote = $this->getQuote();
        $this->quoteItems = $this->package->getAllItems();

        if (!$this->categories) {
            if (Mage::registry('allcategories')) {
                $this->categories = Mage::registry('allcategories');
            } else {
                $allCategories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToSelect('name');
                foreach ($allCategories as $category) {
                    $this->categories[$category->getId()] = $category->getName();
                }
                Mage::register('allcategories', $this->categories);
            }
        }
    }

    protected function setProductsPerPackageType($productData, $bundleProduct)
    {
        if (!empty($productData['packageType'])) {
            $multipleTypes = explode(',', $productData['packageType']);

            if (count($multipleTypes) > 1) {
                foreach ($multipleTypes as $type) {
                    $this->bundleProductsSummary[trim($type)][] = $bundleProduct;
                }
            } else {
                $this->bundleProductsSummary[$productData['packageType']][] = $bundleProduct;
            }
        }
    }

    public function add(...$skus)
    {
        if (Mage::registry('bundleProductsSummary')) {
            $this->bundleProductsSummary = Mage::registry('bundleProductsSummary');
            Mage::unregister('bundleProductsSummary');
        }

        foreach ($skus as $sku) {
            try {
                $productData = $this->getProductData($sku);
                $bundleProduct = $productData;
                unset($bundleProduct['packageType']);
                $bundleProduct['type'] = 'add';
                if($this->chooseCategory){
                    $bundleProduct['choose'] = 1;
                    $this->chooseCategory = null;
                }

                $this->setProductsPerPackageType($productData, $bundleProduct);

            } catch (Exception $e) {
                Mage::log(sprintf("Unable to find sku for add : %s", $sku, $e->getMessage()), null, "bundleRules.log");
            }
        }
        Mage::register('bundleProductsSummary', $this->bundleProductsSummary);

        return true;
    }

    public function delete(...$skusToDelete)
    {
        $packageProductSkus = [];
        foreach ($this->quoteItems as $quoteItem) {
            $packageProductSkus[] = $quoteItem->getSku();
        }
        if (Mage::registry('bundleProductsSummary')) {
            $this->bundleProductsSummary = Mage::registry('bundleProductsSummary');
            Mage::unregister('bundleProductsSummary');
        }

        foreach ($skusToDelete as $sku) {
            try {
                if (in_array($sku, $packageProductSkus)) {
                    $productData = $this->getProductData($sku);
                    $bundleProduct = $productData;
                    unset($bundleProduct['packageType']);
                    $bundleProduct['type'] = 'remove';

                    $this->setProductsPerPackageType($productData, $bundleProduct);
                }
            } catch (Exception $e) {
                Mage::log(sprintf("Unable to find sku for remove :%s", $sku, $e->getMessage()), null, "bundleRules.log");
            }
        }
        Mage::register('bundleProductsSummary', $this->bundleProductsSummary);

        return true;
    }


    public function addPromoProduct($skus, $target)
    {
        if (Mage::registry('bundleProductsSummary')) {
            $this->bundleProductsSummary = Mage::registry('bundleProductsSummary');
            Mage::unregister('bundleProductsSummary');
        }

        $aSkus = explode(',', $skus);
        foreach ($aSkus as $sku) {
            if ($sku) {
                $product = Mage::getModel("catalog/product")->loadByAttribute('sku', $sku);
                if (!$product) {
                    Mage::log(sprintf("Unable to find sku for add : %s", $sku), null, "bundleRules.log");
                } elseif (!$this->exists($product)) {
                    $packageTypes = explode(',', $product->getPackageType());

                    if (count($packageTypes) > 1) {
                        foreach ($packageTypes as $type) {
                            $this->bundleProductsSummary[trim($type)][] = [
                                'name' => $product->getDisplayNameConfigurator() ?: $product->getName(),
                                'type' => 'add',
                                'productId' => (int)$product->getId(),
                                'promoDiscount' => $product->getMafDiscount() ?? 0,
                                'discountFormatted' => $product->getMafDiscount() ? "-" . Mage::app()->getStore()->formatPrice($product->getMafDiscount(), 1) : null,
                                'maf' => Mage::helper('tax')->getPrice($product, $product->getMaf(), false)
                            ];
                        }
                    } else {
                        $this->bundleProductsSummary[$product->getPackageType()][] = [
                            'name' => $product->getDisplayNameConfigurator() ?: $product->getName(),
                            'type' => 'add',
                            'productId' => (int)$product->getId(),
                            'promoDiscount' => $product->getMafDiscount() ?? 0,
                            'discountFormatted' => $product->getMafDiscount() ? "-" . Mage::app()->getStore()->formatPrice($product->getMafDiscount(), 1) : null,
                            'maf' => Mage::helper('tax')->getPrice($product, $product->getMaf(), false)
                        ];
                    }

                }
            }
        }
        Mage::register('bundleProductsSummary', $this->bundleProductsSummary);

        return true;
    }

    public function addPackage(string $packageType)
    {
        return true;
    }

    private function getProductData($sku)
    {
        $product = Mage::getModel("catalog/product")->loadByAttribute('sku', $sku);

        if ($product) {
            return [
                'packageType' => $product->getPackageType(),
                'name' => $product->getDisplayNameConfigurator() ?: $product->getName(),
                'productId' => (int)$product->getId(),
                'promoDiscount' => $product->getMafDiscount() ?? 0,
                'discountFormatted' => $product->getMafDiscount() ? "-" . Mage::app()->getStore()->formatPrice($product->getMafDiscount(), 1) : null,
                'maf' => Mage::helper('tax')->getPrice($product, $product->getMaf(), false)
            ];
        }
        return [];
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool|Mage_Sales_Model_Quote_Item
     */
    protected function exists(Mage_Catalog_Model_Product $product)
    {
        /* Re-fetch quote items */
        $this->quoteItems = $this->package->getAllItems();
        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        foreach ($this->quoteItems as $quoteItem) {
            if ($quoteItem->getProductId() == $product->getId()) {
                return $quoteItem;
            }
        }

        return false;
    }

    /**
     * Override parent method to prevent changing the cart when simulating
     * @param $category
     * @param null $default
     * @return bool
     */
    public function chooseFromCategory($category, $default = null)
    {
        $this->chooseCategory = true;
        $this->add([$default]);
        return true;
    }

    /**
     * Override parent method to prevent changing the cart when simulating
     * @param $category
     * @param null $default
     * @return bool
     */
    public function chooseFromCategoryId($category, $default = null)
    {
        $this->chooseCategory = true;
        $this->add([$default]);
        return true;
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    public function containsProductInCategoryId($categoryId)
    {
        if (($category = $this->getCategoryIdByUniqueId($categoryId)) && $category->getId()) {
            return $this->containsProductInCategory((int)$category->getId());
        }

        return false;
    }

    /**
     * @param $categoryName
     * @return bool
     */
    public function containsProductInCategory($categoryName): bool
    {
        $categoryId = is_int($categoryName) ? $categoryName : $this->getCategoryIdFromPath($categoryName);

        foreach ($this->quoteItems as $item) {
            if (in_array($item->getPackageId(), $this->bundlePackageIds)) {
                $cats = $item->getProduct()->getCategoryIds();
                if(in_array($categoryId, $cats)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $categoryName
     * @return bool
     */
    public function deleteProductsFromCategory($categoryName)
    {
        if (Mage::registry('bundleProductsSummary')) {
            $this->bundleProductsSummary = Mage::registry('bundleProductsSummary');
            Mage::unregister('bundleProductsSummary');
        }

        $categoryId = is_int($categoryName) ? $categoryName : $this->getCategoryIdFromPath($categoryName);

        foreach ($this->quoteItems as $item) {
            if (in_array($item->getPackageId(), $this->bundlePackageIds)) {
                $cats = $item->getProduct()->getCategoryIds();
                if(in_array($categoryId, $cats)) {
                    $productData = $this->getProductData($item->getSku());
                    $productData['type'] = 'remove';
                    $bundleProduct = $productData;

                    $this->setProductsPerPackageType($productData, $bundleProduct);
                }
            }
        }

        Mage::register('bundleProductsSummary', $this->bundleProductsSummary);

        return true;
    }
}
