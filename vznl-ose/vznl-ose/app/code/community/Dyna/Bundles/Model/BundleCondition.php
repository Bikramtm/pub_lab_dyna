<?php

/**
 * Class Dyna_Bundles_Model_BundleCondition
 * @method int getBundleRuleId()
 * @method string|null getCondition()
 */
class Dyna_Bundles_Model_BundleCondition extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_bundles/bundleCondition');
    }

}
