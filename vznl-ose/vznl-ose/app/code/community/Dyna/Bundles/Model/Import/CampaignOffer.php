<?php

/**
 * Class Dyna_Bundles_Model_Import_CampaignOffer
 */
class Dyna_Bundles_Model_Import_CampaignOffer extends Dyna_Bundles_Model_Import_Abstract
{
    /** @var string $_logFileName */
    protected $_logFileName = "bundle_campaignoffer_import";

    /**
     * Import campaign
     *
     * @param array $data
     *
     * @return void
     */
    public function import($data)
    {

        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;

        $data = $this->_setDataMapping($data);

        $mandatoryAttributes = ['offer_id'];
        foreach ($mandatoryAttributes as $attribute) {
            if (!isset($data[$attribute])) {
                $this->_logError('Skipping line without ' . $attribute);
                $skip = true;
            }
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        $this->_log('Start importing ' . $data['offer_id']['value'], false);

        try {
            /** @var Dyna_Bundles_Model_CampaignOffer $offer */
            $offer = Mage::getModel('dyna_bundles/campaignOffer');
            $existingOffer = $offer->load($data['offer_id']['value'], 'offer_id');

            if ($existingOffer->getEntityId()) {
                $offer = $existingOffer;
                $this->_log('Loading existing offer"' . $data['offer_id']['value'] . '" with ID ' . $offer->getEntityId(), false);
            }

            $formattedData = array();

            if (isset($data['offer_id'])) {
                $formattedData['offer_id'] = $data['offer_id']['value'];
            }
            if (isset($data['title_description'])) {
                $formattedData['title_description'] = $data['title_description']['value'];
            }
            if (isset($data['subtitle_description'])) {
                $formattedData['subtitle_description'] = $data['subtitle_description']['value'];
            }
            if (isset($data['usp1'])) {
                $formattedData['usp1'] = $data['usp1']['value'];
            }
            if (isset($data['usp1']['attributes'])) {
                $formattedData['usp1pakagetype'] = $data['usp1']['attributes']['package_type'];
            }
            if (isset($data['usp2'])) {
                $formattedData['usp2'] = $data['usp2']['value'];
            }
            if (isset($data['usp2']['attributes'])) {
                $formattedData['usp2pakagetype'] = $data['usp2']['attributes']['package_type'];
            }
            if (isset($data['usp3'])) {
                $formattedData['usp3'] = $data['usp3']['value'];
            }
            if (isset($data['usp3']['attributes'])) {
                $formattedData['usp3pakagetype'] = $data['usp3']['attributes']['package_type'];
            }
            if (isset($data['usp4'])) {
                $formattedData['usp4'] = $data['usp4']['value'];
            }
            if (isset($data['usp4']['attributes'])) {
                $formattedData['usp4pakagetype'] = $data['usp4']['attributes']['package_type'];
            }
            if (isset($data['usp5'])) {
                $formattedData['usp5'] = $data['usp5']['value'];
            }
            if (isset($data['usp5']['attributes'])) {
                $formattedData['usp5pakagetype'] = $data['usp5']['attributes']['package_type'];
            }
            if (isset($data['discounted_months'])) {
                $formattedData['discounted_months'] = $data['discounted_months']['value'];
            }
            if (isset($data['discounted_price_per_month'])) {
                $formattedData['discounted_price_per_month'] = $data['discounted_price_per_month']['value'];
            }
            if (isset($data['undiscounted_price_per_month'])) {
                $formattedData['undiscounted_price_per_month'] = $data['undiscounted_price_per_month']['value'];
            }
            if (isset($data['product_ids'])) {
                $formattedData['product_ids'] = $data['product_ids']['value'];
            }

            $data = $this->_filterData($formattedData);
            $params = [
                'offer_id',
                'title_description',
                'subtitle_description',
                'usp1',
                'usp1pakagetype',
                'usp2',
                'usp2pakagetype',
                'usp3',
                'usp3pakagetype',
                'usp4',
                'usp4pakagetype',
                'usp5',
                'usp5pakagetype',
                'discounted_months',
                'discounted_price_per_month',
                'undiscounted_price_per_month',
                'product_ids'
            ];

            // all parameters are default null
            $defaultParams = array_combine($params, array_fill(0, count($params), null));

            $this->_setDataWithDefaults($offer, $data, $defaultParams);

            if (!$this->getDebug()) {
                $offer->save();
            }

            unset($offer);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['offer_id'], false);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }

    /**
     * Filter and process invalid data types
     *
     * @param array $data
     *
     * @return array
     */
    protected function _filterData($data)
    {
        $data['discounted_price_per_month'] = $this->cleanPriceString($data['discounted_price_per_month'] ?? '');
        $data['undiscounted_price_per_month'] = $this->cleanPriceString($data['undiscounted_price_per_month'] ?? '');
        foreach ($data as $key => $column) {
            if (is_null($column) || is_array($column)) {
                $filtered_data[$key] = null;
            } else {
                $filtered_data[$key] = preg_replace('/[^A-Za-z0-9 \-,:_!@#$%^&*().]/u', '', $column);
            }
            if (strlen($filtered_data[$key]) == 0) {
                $filtered_data[$key] = null;
            }
        }
        $data = $filtered_data;

        return $data;
    }

    /**
     * Replace comma with dot and make sure the price does not contain invalid characters
     *
     * @param string $price
     * @return mixed
     */
    protected function cleanPriceString($price)
    {
        return preg_replace('/[^\d\.]/', '', str_replace(',', '.', $price));
    }
}
