<?php

/**
 * This class will be used for reporting what will bundles actions target
 * Class Dyna_Configurator_Model_Expression_Scope
 */

class Dyna_Bundles_Model_Expression_Scope extends Varien_Object
{
    const ALLOW_PACKAGE_TYPE_STRING = "allowPackageTypeId";
    const ALLOW_PACKAGE_CREATION_TYPE_ID_STRING = "allowPackageCreationTypeId";

    public function allowPackageTypeId($packageType)
    {
        return $packageType;
    }

    public function allowPackageCreationTypeId($packageType)
    {
        return $packageType;
    }
}
