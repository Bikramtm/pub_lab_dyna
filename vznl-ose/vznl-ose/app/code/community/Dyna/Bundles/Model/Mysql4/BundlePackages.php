<?php

/**
 * Class Dyna_Bundles_Model_Mysql4_BundlePackages
 */
class Dyna_Bundles_Model_Mysql4_BundlePackages extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_bundles/bundle_packages', 'entity_id');
    }
}
