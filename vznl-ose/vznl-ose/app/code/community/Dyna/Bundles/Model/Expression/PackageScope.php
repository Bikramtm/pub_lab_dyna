<?php

class Dyna_Bundles_Model_Expression_PackageScope extends Dyna_Bundles_Model_Expression_AbstractScope implements Dyna_Bundles_Model_Expression_ScopeInterface
{
    /**
     * @var Dyna_Package_Model_Package
     */
    protected $package = null;
    /**
     * @var
     */
    protected $packageItems;
    /**
     * @var
     */
    /*protected $packagePromoItems;*/
    /**
     * @var mixed
     */
    protected $categories;
    /** @var  Dyna_Bundles_Model_BundleRule */
    protected $bundle;
    /** @var Dyna_Checkout_Model_Sales_Quote|Mage_Sales_Model_Quote */
    protected $quote;

    public function __construct($args)
    {
        if ($args['package']) {
            $this->package = $args['package'];
            $this->packageItems = explode(',', $this->package->getData('installed_base_products'));
            $this->bundlePackageIds[] = $args['package']->getPackageId();
        }
        if (empty($args['bundle']) || !($args['bundle'] instanceof Dyna_Bundles_Model_BundleRule)) {
            throw new InvalidArgumentException("invalid or no bundle received for installbase package scope");
        }

        $this->quote = $this->getQuote();
        $this->bundle = $args['bundle'];

        if (!$this->categories) {
            if (Mage::registry('allcategories')) {
                $this->categories = Mage::registry('allcategories');
            } else {
                $allCategories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToSelect('name');
                foreach ($allCategories as $category) {
                    $this->categories[$category->getId()] = $category->getName();
                }
                Mage::register('allcategories', $this->categories);
            }
        }
    }

    /**
     * @param $category
     * @param null $defaultSku
     * @return bool
     */
    public function chooseFromCategory($category, $defaultSku = null)
    {
        if (!($category instanceof Dyna_Catalog_Model_Category)) {
            $category = $this->getCategoryIdFromPath($category);
        } else {
            // keep only category id from category instance
            $category = $category->getId();
        }

        if ($category && !in_array($category, $this->chooseFromCategories)) {
            if($defaultSku){
                $item = $this->addSkuToCart($defaultSku);
                $item->setBundleChoice(true);
            }
            $this->chooseFromCategories[$this->package->getEntityId()][$this->package->getEntityId()] = $category;
        }
        return true;
    }

    /**
     * @param $categoryId
     * @param null $default
     * @return bool
     */
    public function chooseFromCategoryId($categoryId, $default = null)
    {
        if (($category = $this->getCategoryIdByUniqueId($categoryId)) && $category->getId()) {
            return $this->chooseFromCategory($category, $default);
        }

        return true;
    }

    /**
     * @param $sku
     * @return bool
     */
    public function containsProduct($sku): bool
    {
        if (!$this->package) {
            return false;
        }
        $allItems = $this->package->getInstalledBaseProducts();

        foreach ($allItems as $item){
            if($item->getSku() == $sku){
                return true;
            }
        }

        return false;
    }

    /**
     * @param $categoryName
     * @return bool
     */
    public function containsProductInCategory($categoryName): bool
    {
        if (!$this->package) {
            return false;
        }

        $categoryId = is_int($categoryName) ? $categoryName : $this->getCategoryIdFromPath($categoryName);

        foreach ($this->packageItems as $sku) {
            $product = Mage::getModel("catalog/product")->loadByAttribute("sku", $sku);
            if ($product) {
                $cats = $product->getCategoryIds();
                if (in_array($categoryId, $cats)) {
                    return true;
                }

            }
        }

        return false;
    }

    /**
     * @param $skuToReplace
     * @param $skusToReplaceWith
     * @return bool
     */
    public function replace($skuToReplace, $skusToReplaceWith)
    {
        if (!$this->package) {
            return false;
        }

        $skusToReplaceList = explode(',', $skuToReplace);
        $skusToReplaceList = array_map('trim', $skusToReplaceList);

        $skusToReplaceWithList = explode(',', $skusToReplaceWith);
        $skusToReplaceWithList = array_map('trim', $skusToReplaceWithList);

        $this->delete(...$skusToReplaceList);
        $this->add(...$skusToReplaceWithList);

        return true;
    }

    /**
     * @param array ...$skusToDelete
     * @return bool
     */
    public function delete(...$skusToDelete)
    {
        if (!$this->package) {
            return false;
        }

        $productsArray = array_flip($this->packageItems);
        foreach ($skusToDelete as $sku) {
            if (in_array($sku, $productsArray)) {
                unset($this->packageItems[$productsArray[$sku]]);
            }
        }
        $product = Mage::getModel('catalog/product');

        $productId = [];
        foreach ($skusToDelete as $sku) {
            $productId[] = $product->getIdBySku($sku);
        }
        if (count($productId) == 1) {
            $productId = $productId[0];
        }

        if (is_array($productId)) {
            $ids = array_filter(array_filter($productId, 'trim'));
            foreach ($ids as $id) {
                $item = $this->getQuote()->getItemsCollection()->getItemsByColumnValue('product_id', $id);
                $item = reset($item);
                if ($item) {
                    $this->getCart()
                        ->removeItem($item->getId());
                }
            }
        } else {
            $id = (int)$productId;
            $item = $this->getQuote()->getItemsCollection()->getItemsByColumnValue('product_id', $id);
            $item = reset($item);
            if ($item) {
                $this->getCart()->removeItem($item->getId());
            }
        }

        return true;
    }

    /**
     * @param array ...$skus
     * @return bool
     */
    public function add(...$skus)
    {
        if (!$this->package) {
            return false;
        }
        foreach ($skus as $sku) {
            //add the product as a quote item
            try {
                $this->addSkuToCart($sku);
            } catch (Exception $e) {
                Mage::log(sprintf("Unable to add %s to cart : %s", $sku, $e->getMessage()), Zend_Log::WARN, "bundleRules.log");
            }
        }

        return true;
    }

    /**
     * @param $sku
     * @return Dyna_Checkout_Model_Sales_Quote_Item|Mage_Checkout_Model_Cart|Mage_Sales_Model_Quote_Item|Omnius_Checkout_Model_Sales_Quote_Item
     */
    protected function addSkuToCart($sku)
    {
        $websiteId = Mage::app()->getWebsite()->getId();
        if (($emulatedWebsiteId = Mage::helper('superorder')->shouldEmulateStore($websiteId)) != $websiteId) {
            // If a product from another shop should be added, emulate that store
            $storeId = Mage::app()->getWebsite($emulatedWebsiteId)->getDefaultStore()->getId();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }

        $cart = $this->getCart();
        $product = null;
        if ($sku) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($storeId);
            $product->load($product->getIdBySku($sku));
            if (!$product->getId()) {
                Mage::throwException('Invalid product ID given');
            }
        } else {
            Mage::throwException('Invalid product ID given');
        }

        $numberCtns = count($this->getQuote()->getPackageCtns());
        $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

        $result = false;
        if (!$this->exists($product)) {
            $result = $cart->addProduct($product, array(
                    'qty' => $numberCtns ?: 1,
                    'one_of_deal' => ($isOneOfDeal && $numberCtns) ? 1 : 0,
                )
            );

            if (is_string($result)) {
                Mage::throwException($result);
            } else {
                $result
                    ->setPackageType($this->package->getType())
                    ->setBundleComponent($this->bundle->getId())
                    ->setPackageId($this->package->getPackageId());
            }
        } else {
            $this->markProductAsBundleComponent($product);
        }

        return $result;
    }

    /**
     * @param $skus
     * @param $target
     * @return bool
     */
    public function addPromoProduct($skus, $target)
    {
        $websiteId = Mage::app()->getWebsite()->getId();
        if (($emulatedWebsiteId = Mage::helper('superorder')->shouldEmulateStore($websiteId)) != $websiteId) {
            // If a product from another shop should be added, emulate that store
            $storeId = Mage::app()->getWebsite($emulatedWebsiteId)->getDefaultStore()->getId();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }

        $aSkus = explode(',', $skus);
        foreach ($aSkus as $sku) {
            $cart = $this->getCart();
            $product = null;
            if ($sku) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId($storeId);
                $product->load($product->getIdBySku($sku));
                if (!$product->getId()) {
                    Mage::throwException('Invalid product ID given');
                }
            } else {
                Mage::throwException('Invalid product ID given');
            }

            $numberCtns = count($this->getQuote()->getPackageCtns());
            $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

            $targetProductId = null;
            if (!$this->exists($product)) {
                $allItems = $this->quote->getAllItems();
                foreach ($allItems as $targetItem) {
                    if ($targetItem->getSku() == $target) {
                        $targetProductId = $targetItem->getProductId();
                        break;
                    }
                }

                // adding a product to a target that doesn't exist should never happen as the action should have an existence condition for this
                // but still returning evaluation response to true to prevent logging
                if (!$targetProductId) {
                    return true;
                }

                $result = $cart->addProduct($product, array(
                        'qty' => $numberCtns ?: 1,
                        'one_of_deal' => ($isOneOfDeal && $numberCtns) ? 1 : 0,
                    )
                );

                if (is_string($result)) {
                    Mage::throwException($result);
                } else {
                    $result
                        ->setPackageType($this->package->getType())
                        ->setBundleComponent($this->bundle->getId())
                        ->setTargetId($targetProductId)
                        ->setPackageId($this->package->getPackageId())
                        ->setIsBundlePromo(1);
                }
            } else {
                $this->markProductAsBundleComponent($product);
            }

        }

        return true;
    }

    /**
     * @return bool
     */
    public function onParsingDone(): bool
    {
        if (!$this->package || !$this->package->getEditingDisabled()) {
            return false;
        }

        $this->package->setInstalledBaseProducts(implode(',', $this->packageItems));
        $this->package->save();

        return true;
    }

    public function getId()
    {
        if (!$this->package) {
            return false;
        }

        return $this->package->getPackageId();
    }

    public function getPackageItems()
    {
        return $this->packageItems;
    }

    /**
     * Get cart instance from session
     * @return Dyna_Checkout_Model_Cart|Mage_Core_Model_Abstract
     */
    protected function getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * Get current session quote
     * @return Dyna_Checkout_Model_Sales_Quote|Mage_Sales_Model_Quote
     */
    protected function getQuote()
    {
        return $this->getCart()->getQuote();
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool|Mage_Sales_Model_Quote_Item
     */
    protected function exists(Mage_Catalog_Model_Product $product)
    {
        /* Re-fetch quote items */
        $quoteItems = $this->quote->getAllItems();
        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        foreach ($quoteItems as $quoteItem) {
            if (($quoteItem->getPackageId() == $this->package->getId() || $quoteItem->getPackageId() == $this->package->getPackageId()) &&
                $quoteItem->getProductId() == $product->getId()
            ) {
                return $quoteItem;
            }
        }
        return false;
    }

    /**
     * If a product is present in the quote scope, mark it as a bundle component
     * @param Mage_Catalog_Model_Product $product
     * @param bool $isPartOfBundle
     * @return bool
     */
    public function markProductAsBundleComponent(Mage_Catalog_Model_Product $product, $isPartOfBundle = true)
    {
        $isPartOfBundle = $isPartOfBundle ? $this->bundle->getId() : 0;
        $marked = false;
        $quoteItem = $this->exists($product);
        if ($quoteItem) {
            $quoteItem->setBundleComponent($isPartOfBundle);
            $marked = true;
        }
        return $marked;
    }

    /**
     * @param $product
     * @param bool $isBundleChoice
     * @return bool
     */
    public function markProductAsBundleChoice($product, $isBundleChoice = true)
    {
        $marked = false;
        $quoteItem = $this->exists($product);
        if ($quoteItem) {
            $quoteItem->setBundleChoice($isBundleChoice);
            if ($isBundleChoice) {
                $quoteItem->setBundleComponent($this->bundle->getId());
            }
            $marked = true;
        }
        return $marked;
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    public function containsProductInCategoryId($categoryId)
    {
        if (($category = $this->getCategoryIdByUniqueId($categoryId)) && $category->getId()) {
            return $this->containsProductInCategory((int)$category->getId());
        }

        return false;
    }

    /**
     * Return package
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * Deletes every product of "$categoryName" in the corresponding package
     *
     * @param $categoryName
     * @return bool
     */
    public function deleteProductsFromCategory($categoryName)
    {
        $categoryId = is_int($categoryName) ? $categoryName : $this->getCategoryIdFromPath($categoryName);

        foreach ($this->packageItems as $sku) {
            $product = Mage::getModel("catalog/product")->loadByAttribute("sku", $sku);
            if ($product) {
                $cats = $product->getCategoryIds();
                if (in_array($categoryId, $cats)) {
                    $item = $this->quote->getItemByProduct($product);
                    if ($item) {
                        $this->getCart()
                            ->removeItem($item->getId());
                    }
                }
            }
        }

        return true;
    }
}
