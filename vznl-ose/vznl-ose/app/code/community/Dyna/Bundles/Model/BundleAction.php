<?php

/**
 * Class Dyna_Bundles_Model_BundleAction
 * @method int getBundleRuleId()
 * @method string|null getRuleType()
 * @method string|null getCondition()
 * @method string|null getAction()
 * @method string|null getPriority()
 *
 */
class Dyna_Bundles_Model_BundleAction extends Mage_Core_Model_Abstract
{
    private $_maxPriority = 0;

    public function _construct()
    {
        $this->_init('dyna_bundles/bundleAction');
    }
    /**
     * Fetches the highest priority from the product_match_rule table.
     * @return int
     */
    public function getMaxPriority()
    {
        if (!$this->_maxPriority) {
            $query = $this->getCollection()->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('MAX(priority) as max');
            $max = $query->query()->fetchAll();
            if (is_array($max) && count($max) > 0) {
                return $this->_maxPriority = (int) $max[0]['max'];
            } else {
                return $this->_maxPriority = 0;
            }
        }

        return $this->_maxPriority + 1;
    }
}
