<?php

/**
 * Class Dyna_Bundles_Model_BundleProcessContext
 */
class Dyna_Bundles_Model_BundleProcessContext extends Mage_Core_Model_Abstract {
    public function _construct()
    {
        $this->_init('dyna_bundles/bundleProcessContext');
    }
}
