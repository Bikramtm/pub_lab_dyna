<?php

/**
 * Class Dyna_Bundles_Model_Mysql4_CampaignOfferRelation_Collection
 */
class Dyna_Bundles_Model_Mysql4_CampaignOfferRelation_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Dyna_Bundles_Model_Mysql4_CampaignOfferRelation_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('dyna_bundles/campaignOfferRelation');
    }
}