<?php

/**
 * Class Dyna_Bundles_Model_CampaignOfferRelation
 */
class Dyna_Bundles_Model_CampaignOfferRelation extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_bundles/campaignOfferRelation');
    }
}
