<?php

/**
 * Class Dyna_Bundles_Model_Mysql4_CampaignOffer
 */
class Dyna_Bundles_Model_Mysql4_CampaignOffer extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_Bundles_Model_Mysql4_CampaignOffer constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_bundles/campaign_offer', 'entity_id');
    }
}
