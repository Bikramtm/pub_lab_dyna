<?php
/**
 * Update attributes types for discounts related attributes
 */

$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Update attributes
$attributes = [
    'maf_discount' => [
        'frontend_input' => 'price',
        'backend_type' => 'decimal',
    ],
    'maf_discount_percent' => [
        'frontend_input' => 'text',
        'backend_type' => 'int',
    ],
    'price_discount' => [
        'frontend_input' => 'price',
        'backend_type' => 'decimal',
    ],
    'price_discount_percent' => [
        'frontend_input' => 'text',
        'backend_type' => 'int',
    ],
];

foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttribute($entityTypeId, $code, 'attribute_id');
    if ($attributeId) {
        $installer->updateAttribute($entityTypeId, $attributeId, $options);
    }
}

$installer->endSetup();
