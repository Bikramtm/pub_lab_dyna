<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn('package_creation_types','requires_serviceability', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => false,
        'length'    => 1,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Determine if this package type requires serviceability'
    ));
$installer->endSetup();
