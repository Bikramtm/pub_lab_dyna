<?php
/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

// Get catalog_product entity id
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$groupName = 'General';

$newAttributes = [
    'ask_confirmation_first' => [
        'label' => 'Ask confirmation first',
        'type' => 'varchar',
        'input' => 'select',
        'option' => [
            'values' => [
                'true' => 'True',
                'false' => 'False'
            ]
        ]
    ],
    'confirmation_text' => [
        'label' => 'Confirmation text',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'contract_value' => [
        'label' => 'Contract value',
        'input' => 'price',
        'type' => 'decimal',
    ],
    'dtc_restrict_ind' => [
        'label' => 'DTC Restrict Ind',
        'type' => 'varchar',
        'input' => 'select',
        'option' => [
            'values' => [
                'true' => 'True',
                'false' => 'False'
            ]
        ]
    ],
    'preselect' => [
        'label' => 'Preselect',
        'type' => 'varchar',
        'input' => 'select',
        'option' => [
            'values' => [
                'true' => 'True',
                'false' => 'False'
            ]
        ]
    ],
    'related_slave_sku' => [
        'label' => 'Related Slave SKU',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'service_xpath' => [
        'label' => 'Service xpath',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'sorting' => [
        'label' => 'Sorting',
        'input' => 'text',
        'type' => 'int',
    ],
    'vat' => [
        'label' => 'VAT',
        'input' => 'text',
        'type' => 'int',
    ],
    'use_service_value' => [
        'label' => 'Use service value',
        'type' => 'varchar',
        'input' => 'select',
        'option' => [
            'values' => [
                'true' => 'True',
                'false' => 'False'
            ]
        ]
    ]
];

// In with the new
$sortOrder = 200;
foreach ($newAttributes as $attributeCode => $options) {
    $attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);

    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $attributeCode, $options);
        $sortOrder++;
    }
}

$installer->endSetup();

