<?php
// Remove invalid `Cable` and `Fixed` package types
/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeValues = Dyna_Catalog_Model_Type::getPackageTypes(false);

// Update package_type attribute options
$attribute_code = 'package_type';

$attribute = Mage::getModel('eav/config')->getAttribute($entityTypeId, $attribute_code);
$options = $attribute->getSource()->getAllOptions(false);

foreach ($options as $option) {
    if (!in_array($option['label'], $attributeValues)) {
        $existingOption['delete'][$option['value']] = true;
        $existingOption['value'][$option['value']] = true;
        $installer->addAttributeOption($existingOption);
    }
}

$installer->endSetup();
