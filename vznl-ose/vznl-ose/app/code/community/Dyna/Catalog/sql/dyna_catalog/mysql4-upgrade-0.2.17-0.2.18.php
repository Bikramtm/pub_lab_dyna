<?php
/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$this->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'family_position', array(
    'group' => 'General Information',
    'sort_order' => 3,
    'input' => 'text',
    'type' => 'int',
    'label' => 'Family position in configurator',
    'backend' => '',
    'visible' => true,
    'required' => false,
    'visible_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$installer->endSetup();