<?php
/**
 * Add package_subtype option "ConfigurableCheckoutOptions"
 */

/* @var $this Dyna_Mobile_Model_Resource_Setup */

$this->startSetup();

/** @var Mage_Eav_Model_Attribute $attribute */
$attributeModel = Mage::getModel('eav/entity_attribute');
/** @var Mage_Eav_Model_Entity_Attribute_Source_Table $attributeOptionModel */
$attributeOptionModel = Mage::getModel('eav/entity_attribute_source_table') ;

// Add package_subtype options
$attributeId = $this->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'package_subtype', 'attribute_id');
/** @var Mage_Eav_Model_Entity_Attribute_Source_Table $attributeOptionModel */
$attributeOptionModel = Mage::getModel('eav/entity_attribute_source_table') ;
$attribute = $attributeModel->load($attributeId);
$attributeOptionModel->setAttribute($attribute);
$options = $attributeOptionModel->getAllOptions(false);

$newPackageSubtypes = "ConfigurableCheckoutOptions";

$addNew = true;
foreach ($options as $option) {
    if (strtolower($option['label']) ==  strtolower($newPackageSubtypes)) {
        $addNew = false;
    }
}

if ($addNew) {
    $subtypeOptions = [
        'attribute_id' => $attributeId,
        'values' => $newPackageSubtypes,
    ];
    $this->addAttributeOption($subtypeOptions);
}

unset($options);

$this->endSetup();
