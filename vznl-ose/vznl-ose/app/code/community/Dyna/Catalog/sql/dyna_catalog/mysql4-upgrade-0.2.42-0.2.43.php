<?php
/**
 * Remove attribute debit_to_credit for mobile products. This attribute was used to mark products that were
 * available in the configurator for debit_to_credit flows (Prepaid to Postpaid)
 */
/* @var $installer Mage_Sales_Model_Entity_Setup */

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$attributeCode = "debit_to_credit";

$attributeSets = [
    "Mobile_Accessory",
    "Mobile_Additional_Service",
    "Mobile_Data_Tariff",
    "Mobile_Device",
    "Mobile_Footnote",
    "Mobile_Prepaid_Tariff",
    "Mobile_Simcard",
    "Mobile_Voice_Data_Tariff"
];

$attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
if ($attributeId) {
    /** @var Omnius_Catalog_Helper_Setup $helper */
    $helper = Mage::helper('omnius_catalog/setup');
    foreach ($attributeSets as $attributeSetCode) {
        $helper->removeAttributeFromAttributeSet($attributeSetCode, $attributeCode);
    }

    $installer->removeAttribute($entityTypeId, $attributeCode);
}

unset($attributeSets);
$installer->endSetup();
