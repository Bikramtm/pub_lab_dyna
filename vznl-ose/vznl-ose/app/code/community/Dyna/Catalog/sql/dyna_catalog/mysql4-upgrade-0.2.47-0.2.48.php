<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Dyna_Cable_Model_Resource_Setup $this */
$this->startSetup();

$defaultValues = array();
$cableCreationTypeCodes = array(
    Dyna_Catalog_Model_Type::CREATION_TYPE_CABLE_INDEPENDENT,
    Dyna_Catalog_Model_Type::CREATION_TYPE_CABLE_INTERNET_PHONE,
    Dyna_Catalog_Model_Type::CREATION_TYPE_CABLE_TV
);

foreach (Mage::getModel('dyna_package/packageCreationTypes')->getCollection() as $packageCreationType) {
    if (!in_array(strtolower($packageCreationType->getPackageTypeCode()), array_map('strtolower', $cableCreationTypeCodes))) {
        $defaultValues[] = $packageCreationType->getId();
    }
}

Mage::getConfig()->saveConfig('catalog/product_dependencies/option_of_option_removal', implode(',', $defaultValues));

$this->endSetup();
