<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();
$entityTypeId = $installer->getEntityTypeId('catalog_product');

$attributes = [
    Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR => [
        'group' => 'General',
        'label' => 'Product frontend visibility',
        'input' => 'multiselect',
        'type' => 'varchar',
        'backend' => 'eav/entity_attribute_backend_array',
        'option' => [
            'values' => array(
                Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CONFIGURATOR,
                Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_BASKET,
                Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_OVERVIEW,
                Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_PRINTOUT,
                Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY,
            ),
        ],
    ]
];

foreach ($attributes as $attributeCode => $options) {
    $options['user_defined'] = 1;
    if (!$installer->getAttribute($entityTypeId, $attributeCode)) {
        $installer->addAttribute($entityTypeId, $attributeCode, $options);
    }
}

$allAttributeSetIds=$installer->getAllAttributeSetIds('catalog_product');
foreach ($allAttributeSetIds as $attributeSetId) {
    try{
        $attributeGroupId=$installer->getAttributeGroup('catalog_product',$attributeSetId,'General');
    }
    catch(Exception $e)
    {
        $attributeGroupId=$installer->getDefaultAttributeGroupId('catalog/product',$attributeSetId);
    }
    $installer->addAttributeToSet('catalog_product',$attributeSetId,$attributeGroupId,$attributeId);
}

$this->addAttributeToSet($entityTypeId, $attributeSetId, 'General', 'product_visibility', 10);


$installer->endSetup();
?>
