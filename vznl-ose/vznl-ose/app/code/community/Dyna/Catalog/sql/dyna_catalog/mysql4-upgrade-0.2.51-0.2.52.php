<?php
/* @var $installer remove the option_code, add new attribute in place and add the reference table */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$groupName = 'General';
$sortOrder = 11;
$attributeCode = 'option_type';
$checkoutoptionattributeSetName = 'CheckoutOption';

//deleting the old option_type attribute
$attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
if($attributeId) $installer->removeAttribute($entityTypeId, $attributeCode);

//creating a new attribute with the same code
$installer->addAttribute($entityTypeId, $attributeCode, array(
    'label' => 'Option Type',
    'input' => 'text',
    'type' => 'varchar',
    'visible' => true,
    'visible_on_front' => true,
    'required' => false,
    'default' => 0,
    'user_defined' => 1,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'sort_order' => $sortOrder,
));

$attrSetId = $installer->getAttributeSetId($entityTypeId, $checkoutoptionattributeSetName);
$installer->addAttributeToSet($entityTypeId, $attrSetId, 'General', $attributeCode, $sortOrder);

// Creating a new table checkout_option_type_referencetable if not exists
if($installer->getConnection()->isTableExists('checkout_option_type_referencetable') != true) {
    $table = $installer->getConnection()
        ->newTable('checkout_option_type_referencetable')
        ->addColumn(
            'reference_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            array(
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true,
                'auto_increment' => true,
            ),
            'Entity Id'
        )
        ->addColumn('option_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(), 'Option Type')
        ->addColumn('display_label_checkout', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(), 'Display Label Checkout');
    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
