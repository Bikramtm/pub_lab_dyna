<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$attributeCode = 'package_subtype';
$attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);
$hasAlready = array('CheckoutOption' => false, 'Fee' => false, 'hidden' => false);
$options =  Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode)->getSource()->getAllOptions(false);
foreach($options as $option){
    if(strtolower($option['label']) == strtolower('CheckoutOption')){
        $hasAlready['CheckoutOption'] = true;
    }
    if(strtolower($option['label']) == strtolower('Fee')){
        $hasAlready['Fee'] = true;
    }
    if(strtolower($option['label']) == strtolower('hidden')){
        $hasAlready['hidden'] = true;
    }
}

if($attribute->getId() && $attribute->getFrontendInput()=='select') {
    $option['attribute_id'] = $attribute->getId();
    if(!$hasAlready['CheckoutOption']) {
        $option['value'] = [[0 => 'CheckoutOption']];
        $installer->addAttributeOption($option);
    }
    if(!$hasAlready['Fee']) {
        $option['value'] = [[0 => 'Fee']];
        $installer->addAttributeOption($option);
    }
    if(!$hasAlready['hidden']) {
        $option['value'] = [[0 => 'hidden']];
        $installer->addAttributeOption($option);
    }
}

$installer->endSetup();
?>