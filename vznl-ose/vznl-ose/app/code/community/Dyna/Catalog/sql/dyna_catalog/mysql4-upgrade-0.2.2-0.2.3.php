<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
// Re-Remove not needed attributes added by Omnius_Core

/** @var Mage_Eav_Model_Entity_Setup $this */
$this->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$this->removeAttribute($entityTypeId, 'identifier_commitment_months');
$this->removeAttribute($entityTypeId, 'identifier_package_type');
$this->removeAttribute($entityTypeId, 'identifier_package_subtype');
$this->removeAttribute($entityTypeId, 'terms_and_conditions');

$this->endSetup();
