<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Omnius
 * @package     Omniu_MixMatch
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table for Product family
 */
if (!$installer->tableExists('product_family')) {
    $table = $installer->getConnection()
        ->newTable("product_family")
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'ID')
        ->addColumn('product_family_id', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'default' => "",
        ), 'Number of the family where the product is part')
        ->addColumn('lifecycle_status', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
            'default' => "",
        ), 'Indicates the status of the product and if the product is currently in use or if the product will be used in the future')
        ->addColumn('product_family_name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'default' => "",
        ), 'Name of the family where to product belong to')
        ->addColumn('product_version_id', Varien_Db_Ddl_Table::TYPE_DECIMAL, 11, array(
            'length' => '12,4'), 'Number of the version of the product')
        ->addColumn('package_type_id', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Package Type where the productFamily  is mapped to. ')
        ->addColumn('package_subtype_id', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Account subtype')
        ->setComment('Product family table');

    $installer->getConnection()->createTable($table);
}


/**
 * Create table for Product version
 */
if (!$installer->tableExists('product_version')) {
    $table = $installer->getConnection()
        ->newTable("product_version")
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'ID')
        ->addColumn('product_version_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(), 'Number of the version of the product')
        ->addColumn('lifecycle_status', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Indicates the status of the product and if the product is currently in use or if the product will be used in the future ')
        ->addColumn('product_family_id', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Number of the family where the product is part ')
        ->addColumn('product_version_name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Name/description of the productVersion')
        ->addColumn('package_type_id', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Package Type where the productVersion is mapped to. ')
        ->setComment('Product version table');

    $installer->getConnection()->createTable($table);
}


/**
 * Add new columns for packageType and packageSubType
 */
$packageTypesTable = 'catalog_package_types';
$packageSubTypesTable = 'catalog_package_subtypes';

if (!$installer->getConnection()->tableColumnExists($packageTypesTable, 'lifecycle_status')) {
    $installer->getConnection()
        ->addColumn($packageTypesTable, 'lifecycle_status',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Indicates the status of the package type and if the package type is currently in use or it will be used in the future',
                'nullable' => true,
                'default' => null,
                'length' => 32
            )
        );
}

if (!$installer->getConnection()->tableColumnExists($packageSubTypesTable, 'manual_override_allowed')) {
    $installer->getConnection()
        ->addColumn($packageSubTypesTable, 'manual_override_allowed',
            array(
                'nullable' => false,
                'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
                'default' => 0,
                'comment' => 'Determines whether products are allowed to be manually added after they have been automatically removed.',
            )
        );
}

if (!$installer->getConnection()->tableColumnExists($packageSubTypesTable, 'package_subtype_lifecycle_status')) {
    $installer->getConnection()
        ->addColumn($packageSubTypesTable, 'package_subtype_lifecycle_status',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'length' => 32,
                'after' => null, // column name to insert new column after
                'comment' => 'Indicates the status of the package subtype and if the package subtype is currently in use or it will be used in the future'
            )
        );
}


$installer->endSetup();
