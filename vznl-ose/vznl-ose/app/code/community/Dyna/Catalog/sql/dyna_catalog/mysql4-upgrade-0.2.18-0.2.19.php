<?php
/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$groupName = 'General';
$sortOrder = 11;
$attributeCode = 'initial_selectable';

$createAttributeSets = [];
$attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')->load();

$attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
if (!$attributeId) {
    $installer->addAttribute($entityTypeId, $attributeCode, array(
        'label' => 'Initial Selectable',
        'input' => 'boolean',
        'type' => 'int',
        'visible' => true,
        'visible_on_front' => true,
        'required' => false,
        'default' => 1,
        'user_defined' => 1,
        'source' => 'eav/entity_attribute_source_boolean',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'sort_order' => $sortOrder,
    ));

    foreach ($attributeSetCollection as $attributeSet) {
        $name = $attributeSet->getAttributeSetName();
        $attrSetId = $installer->getAttributeSetId($entityTypeId, $name);
        $installer->addAttributeToSet($entityTypeId, $attrSetId, $groupName, $attributeCode, $sortOrder);
    }

    /** @var Mage_Catalog_Model_Product $products */
    $products = Mage::getModel('catalog/product')->getCollection();
    foreach($products as $product)
    {
        $product->setInitialSelectable(1);
        $product->setStoreId(0);
        $product->getResource()->saveAttribute($product, 'initial_selectable');
    }
}
$installer->endSetup();
