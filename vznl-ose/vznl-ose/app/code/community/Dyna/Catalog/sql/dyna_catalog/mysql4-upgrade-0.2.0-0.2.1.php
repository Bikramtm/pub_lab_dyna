<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
// Remove not needed attributes added by Omnius_Core

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$installer->removeAttribute($entityTypeId, 'identifier_commitment_months');
$installer->removeAttribute($entityTypeId, 'identifier_package_type');
$installer->removeAttribute($entityTypeId, 'identifier_package_subtype');
$installer->removeAttribute($entityTypeId, 'terms_and_conditions');

$installer->endSetup();
