<?php

/**
 * Add new table process_context
 * Columns:
 * entity_id (FK), code, name, description
 *
 * Index:
 * Unique(code)
 */
$installer = $this;

$installer->startSetup();

$processContextTable = new Varien_Db_Ddl_Table();
$processContextTable->setName("process_context");

$processContextTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
        "unsigned" => true,
        "primary" => true,
        "auto_increment" => true,
        "nullable" => false,
    ], "Primary key for process context table")
    ->addColumn('code',Varien_Db_Ddl_Table::TYPE_TEXT, 200, [
        "nullable" => false,
    ], "Process context code which is unique")
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 200)
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT)
;

if(!$installer->getConnection()->isTableExists($processContextTable->getName())){
    $installer->getConnection()->createTable($processContextTable);
}

$installer->getConnection()
    ->addIndex(
        $installer->getTable('process_context'),
        $installer->getIdxName('process_context', array('code'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        array('code'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    );

$installer->endSetup();
