<?php
/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

// Re-add custom_design_from core attribute deleted in OMNVFDE-885

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$groupName = 'Custom Design';
$sortOrder = 30;
$attributeCode = 'custom_design_from';

$createAttributeSets = [];
$attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')->load();

$attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
if (!$attributeId) {
    $installer->addAttribute($entityTypeId, $attributeCode, array(
        'label' => 'Active From',
        'input' => 'date',
        'type' => 'datetime',
        'required' => false,
        'default' => 1,
        'user_defined' => false,
        'source' => 'eav/entity_attribute_backend_datetime',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'sort_order' => $sortOrder,
    ));
}
$installer->endSetup();
