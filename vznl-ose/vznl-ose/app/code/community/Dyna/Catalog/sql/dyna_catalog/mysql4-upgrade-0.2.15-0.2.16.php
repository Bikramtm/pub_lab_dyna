<?php
/* @var $this Mage_Sales_Model_Entity_Setup */
$this->startSetup();
$this->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'hide_not_selected_products', array(
    'group' => 'General Information',
    'sort_order' => 3,
    'input' => 'select',
    'source' => 'eav/entity_attribute_source_boolean',
    'type' => 'int',
    'label' => 'Hide products from the family category if at least one is selected',
    'backend' => '',
    'visible' => true,
    'required' => false,
    'visible_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->endSetup();
