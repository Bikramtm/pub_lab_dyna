<?php
/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
// Get catalog_product entity id
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$attributeToRename = 'display_name_short';
$renameAttributeTo = 'display_name_cart';
$attributeId = $installer->getAttributeId($entityTypeId, $attributeToRename);
if ($attributeId) {
    $installer->updateAttribute($entityTypeId, $attributeToRename, array('attribute_code' => $renameAttributeTo));
}
$installer->endSetup();
