<?php
$installer = $this;

// catalog package table db
$packageTable = $installer->getTable('catalog_package');
$connection = $installer->getConnection();

if (!$connection->tableColumnExists($packageTable, 'kias_linked_account')) {
    $connection
        ->addColumn($packageTable, 'kias_linked_account',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'KIAS linked account number',
                'nullable' => true,
                'default' => null,
                'length' => 255
            )
        );
}

//end setup installer
$installer->endSetup();
