<?php

/* @var $installer remove the option_code, add new attribute in place and add the reference table */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$groupName = 'General';
$sortOrder = 11;
$attributeCode = 'option_type';

//deleting the old option_type attribute
$attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
if($attributeId) $installer->removeAttribute($entityTypeId, $attributeCode);

$defaultSetName = 'CheckoutOption';


// Create default attributes
$generalAttributes = [
    'option_type' => [
        'label'     => 'Option Type',
        'input'     => 'multiselect',
        'type'      => 'varchar',
        'backend'   => 'eav/entity_attribute_backend_array',
        'option'    => [
            'values' => [
                "number_porting_one",
                "number_porting_more",
                "number_porting_all",
                "phone_book_entry1",
                "phone_book_entry2",
                "phone_book_entry3",
                "technician_needed_yes",
                "technician_needed_no",
                "billing_paper",
                "billing_online",
                "payment_debit",
                "payment_transfer"
            ]
        ],
    ]
];

foreach ($generalAttributes as $code => $options) {
    $attributeId = $installer->getAttributeId($entityTypeId, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
    }
}

$attrSetId = $installer->getAttributeSetId($entityTypeId, $defaultSetName);
$installer->addAttributeToSet($entityTypeId, $attrSetId, 'General', $attributeCode, $sortOrder);

unset($generalAttributes);

$installer->endSetup();
