<?php

$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$defaultSetName = 'CheckoutOption';
$installer->addAttributeSet($entityTypeId, $defaultSetName, 10);

// Create default attributes
$generalAttributes = [
    'option_type' => [
        'label'     => 'Option Type',
        'input'     => 'multiselect',
        'type'      => 'varchar',
        'backend'   => 'eav/entity_attribute_backend_array',
        'option'    => [
            'values' => [
                "number_porting_one",
                "number_porting_more",
                "phonebook_entry1",
                "phonebook_entry2",
                "phonebook_entry3",
                "ready_to_view_yes",
                "ready_to_view_no",
                "billing_paper",
                "billing_online",
                "payment_debit",
                "payment_transfer"
            ]
        ],
    ],
    'display_label_checkout' => [
        'label' => 'Display label checkout',
        'input' => 'text',
        'type' => 'text',
    ],

    'option_ranking' => [
        'label' => 'Option ranking',
        'input' => 'text',
        'type' => 'int',
    ]

];

foreach ($generalAttributes as $code => $options) {
    $attributeId = $installer->getAttributeId($entityTypeId, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
    }
}

$defaultSet = [
    'sku',
    'name',
    'price',
    'maf',
    'package_type',
    'product_visibility',
    'option_type',
    'display_label_checkout',
    'option_ranking',
    'status',
    'visibility',
    'tax_class_id',
    'vat'
];

$attrSetId = $installer->getAttributeSetId($entityTypeId, $defaultSetName);

$sortOrder = 10;
foreach ($defaultSet as $attrCode) {
    $installer->addAttributeToSet($entityTypeId, $attrSetId, 'General', $attrCode, $sortOrder);
    $sortOrder++;
}
unset($generalAttributes, $defaultSet);
$installer->endSetup();
