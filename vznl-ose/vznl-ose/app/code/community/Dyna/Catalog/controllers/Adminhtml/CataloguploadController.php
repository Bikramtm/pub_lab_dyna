<?php

class Dyna_Catalog_Adminhtml_CataloguploadController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Catalog upload'));
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('dyna_catalog/adminhtml_catalogupload_edit'));
        $this->renderLayout();
    }

    public function uploadAction()
    {
        try {
            if ($postData = $this->getRequest()->getPost()) {
                $uploader = Mage::getModel('core/file_uploader', 'file');
                $uploader->setAllowedExtensions(['zip']);
                $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';

                // Empty directory first
                $file = new Varien_Io_File();
                $file->open(array('path' => $path));
                foreach ($file->ls() as $ref) {
                    if ($ref['leaf']) {
                        $file->rm($ref['text']);
                    }
                }
                $uploader->save($path);
                if ($uploadFile = $uploader->getUploadedFileName()) {
                    $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
                    $file->mv($path . $uploadFile, $path.$newFilename);

                    // Unzip the file
                    $zip = new ZipArchive;
                    $zip->open($path . $newFilename);
                    $zip->extractTo($path);
                    $zip->close();

                    $file->rm($path.$newFilename);
                }
                if (isset($newFilename) && $newFilename) {
                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        $this->__('Files are uploaded succesfully and will be processed on the next run')
                    );
                    $this->_redirect('*/*');
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
                );
                $this->_redirect('*/*');
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(
                $e->getMessage()
            );
            $this->_redirect('*/*');
        }
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }
}