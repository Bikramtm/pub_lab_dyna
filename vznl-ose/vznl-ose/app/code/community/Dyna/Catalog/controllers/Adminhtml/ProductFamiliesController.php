<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Catalog_Adminhtml_ProductFamiliesController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("catalog")->_addBreadcrumb(Mage::helper("adminhtml")->__("Product Families"), Mage::helper("adminhtml")->__("Product Families"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Product Families"));
        $this->_title($this->__("Manage Product Versions"));

        $this->_initAction();
        $this->renderLayout();
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("dyna_catalog/productFamily")->load($id);

        if ($model->getId()) {
            Mage::register("product_family_data", $model);

            $this->_title($this->__("Product family"));
            $this->_title($this->__("Edit product family"));

            $this->_initAction();
            $this->renderLayout();
        } else {
            $this->_initAction();
            $this->renderLayout();
        }
    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            $model = Mage::getModel("dyna_catalog/productFamily");
            $postData["package_type_id"] = $postData["package_type_id"] !== "" ? $postData["package_type_id"] : null;
            $postData["package_subtype_id"] = $postData["package_subtype_id"] !== "" ? $postData["package_subtype_id"] : null;

            if ($postData["package_type_id"] && $postData["package_subtype_id"] && Mage::getModel('dyna_package/packageSubtype')->load($postData["package_subtype_id"])->getTypePackageId() !== $postData["package_type_id"]){
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__("The selected package subtype doesn't belong to the selected package type."));
                Mage::getSingleton('adminhtml/session')->setFormData($postData);
                $this->_redirect('*/*/edit', array('id' => $postData["id"]));
                return;
            }

            if ($postData["id"]) {
                $existingOption = $model->load($postData["id"]);
                $existingOption->addData($postData);

                $existingOption->save();
            } else {
                $model->setData($postData);
                $model->save();
            }
        }

        $this->_redirect("*/*/");
    }

    /**
     * Method that handles deletion of package type
     */
    public function deleteAction()
    {
        $params = $this->getRequest()->getParam("id");
        Mage::getModel('dyna_catalog/productFamily')
            ->load($params)
            ->delete();

        $this->_redirect("*/*/");
    }
}
