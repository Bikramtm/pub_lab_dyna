<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Catalog_Adminhtml_ProductVersionsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("catalog")->_addBreadcrumb(Mage::helper("adminhtml")->__("Product Versions"), Mage::helper("adminhtml")->__("Product Versions"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Product Versions"));
        $this->_title($this->__("Manage Product Versions"));

        $this->_initAction();
        $this->renderLayout();
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("dyna_catalog/productVersion")->load($id);

        if ($model->getId()) {
            Mage::register("product_version_data", $model);

            $this->_title($this->__("Product version"));
            $this->_title($this->__("Edit product version"));

            $this->_initAction();
            $this->renderLayout();
        } else {
            $this->_initAction();
            $this->renderLayout();
        }
    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            $model = Mage::getModel("dyna_catalog/productVersion");

            if ($this->getRequest()->getParam("id")) {
                $existingOption = $model->load($this->getRequest()->getParam("id"));
                $existingOption->addData($postData);

                $existingOption->save();
            } else {
                $model->setData($postData);
                $model->save();
            }
        }

        $this->_redirect("*/*/");
    }

    /**
     * Method that handles deletion of package type
     */
    public function deleteAction()
    {
        $params = $this->getRequest()->getParam("id");
        Mage::getModel('dyna_catalog/productVersion')
            ->load($params)
            ->delete();

        $this->_redirect("*/*/");
    }
}
