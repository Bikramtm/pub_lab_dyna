<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Catalog_Helper_Data extends Omnius_Catalog_Helper_Data
{
    /** @var array */
    protected $attributeSetNames = array();
    protected $attributeAdminLabel = array();
    protected $productFilters = array();
    protected $productVisibilityEnabled;
    protected $packageHelper;

    /**
     * Dyna_Catalog_Helper_Data constructor.
     */
    public function __construct()
    {
        $this->productVisibilityEnabled = (bool)Mage::getStoreConfig('catalog/frontend/product_visibility_enabled');
        $this->packageHelper = Mage::helper('dyna_package');
    }

    /**
     * @param $websiteId
     * @param $subtype
     * @param $cache
     * @param $product
     * @return mixed
     */
    public function getProductsForStock($websiteId, $subtype, $cache, $product)
    {
        $productsCacheKey = 'catalog_products_' . $subtype . '_stock_call_' . $websiteId;
        if ($productsCached = $cache->load($productsCacheKey)) {
            $products = unserialize($productsCached);
        } else {
            $productsColl = Mage::getResourceModel('catalog/product_collection')
                ->addWebsiteFilter($websiteId)
                ->addAttributeToFilter('is_deleted', array('neq' => 1))
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, array('eq' => $product));

            $products = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAssoc($productsColl->getSelect());
            unset($productsColl);
            $cache->save(serialize($products), $productsCacheKey, array(), $cache->getTtl());
            unset($productsCacheKey);
        }

        return $products;
    }

    /**
     * @param $websiteId
     * @param $subtype
     * @param $cache
     * @return mixed
     */
    public function getProductForStock($websiteId, $subtype, $cache)
    {
        $attributeCacheKey = $subtype . '_attribute_stock_admin_labels_' . $websiteId;
        if ($attributeCached = $cache->load($attributeCacheKey)) {
            $product = unserialize($attributeCached);
        } else {
            $attribute = Mage::getSingleton("eav/config")
                ->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
            $product = $this->getAttributeAdminLabel(
                $attribute,
                null,
                $subtype
            );

            unset($attribute);
            $cache->save(serialize($product), $attributeCacheKey, array(), $cache->getTtl());
        }

        return $product;
    }

    /**
     * @param $attribute
     * @param $attributeValueId
     * @param null $attributeValueLabel
     * @return mixed|string
     */
    public function getAttributeAdminLabel($attribute, $attributeValueId, $attributeValueLabel = null)
    {
        $key = $attribute->getAttributeCode() . '_values';
        if (empty($this->attributeAdminLabel[$key])) {
            $cachedValues = $this->getCache()->load($key);
            // if not already cached, retrieve all attribute admin values
            if ($cachedValues === false) {
                $result = [];
                $_collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                    ->setStoreFilter(0)//to get the admin values
                    ->setAttributeFilter($attribute->getId());
                $options = Mage::getSingleton('core/resource')
                    ->getConnection('core_read')
                    ->fetchAll($_collection->getSelect());
                unset($_collection);
                // store as value=>label array for easy access
                foreach ($options as $option) {
                    $result[$option['option_id']] = $option['value'];
                }

                $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
            } else {
                $result = unserialize($cachedValues);
            }

            $this->attributeAdminLabel[$key] = $result;
        } else {
            $result = $this->attributeAdminLabel[$key];
        }

        // if an attribute label is provided, return his value(id)
        if ($attributeValueLabel) {
            return array_search(strtolower($attributeValueLabel), array_map('strtolower', $result));
        }

        if ($result && is_array($result)) {
            // For multiselects, the values may come as a comma separate string (e.g: 1,2,3)
            $attributeValues = explode(',', $attributeValueId);
            foreach ($result as $attributeValue => $attributeLabel) {
                if (!in_array($attributeValue, $attributeValues)) {
                    unset($result[$attributeValue]);
                }
            }
        }

        return json_encode($result);
    }

    /**
     * Get VAT tax class
     * @param int $vatValue
     * @return string
     */
    public function getVatTaxClass($vatValue)
    {
        $vatTaxClass = '';

        switch ($vatValue) {
            case '19':
                $vatTaxClass = 'TAX_19';
                break;
            case '6':
                $vatTaxClass = 'TAX_6';
                break;
            case '0':
                $vatTaxClass = 'TAX_0';
                break;
        }

        return $vatTaxClass;
    }

    /**
     * @param $product
     * @param $section
     * @param $productAttributeIds
     * @return boolean
     */
    public function getProductVisibility($product, $section, $productAttributeIds = false)
    {
        if (!$this->productVisibilityEnabled) {
            return true;
        } else {
            if ($productAttributeIds == false) {
                $productAttributeIds = [];

                if ($product) {
                    $productVisibility = $product->getData('product_visibility');
                    $productAttributeIds = explode(",", $productVisibility);
                }
            }
        }

        $productVisibilityAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'product_visibility');
        $sectionId = $this->getAttributeAdminLabel($productVisibilityAttribute, null, $section);

        return in_array($sectionId, $productAttributeIds);
    }

    /**
     * Get the product visibility on a section:
     * -  the product_visibility attribute must be set to be visible in that section
     * AND
     * - the package_subtype_visibility property must be set to be visible in that section
     *
     * @param string $productSectionVisibility
     * @param string $productSku
     * @param Dyna_Catalog_Model_Product $product
     * @param array|bool $productAttributeIds
     * @return bool
     */
    public function getVisibility($productSectionVisibility, $productSku, $product = null, $productAttributeIds = null)
    {
        // return false if neither product sku nor sku are passed
        if (!$productSku && !$product) {
            return false;
        }
        $key = md5(sprintf("%s|%s", $productSku, $productSectionVisibility));

        $foundInCache = $this->getCache()->load($key);

        if ($foundInCache !== false) {
            $result = unserialize($foundInCache);
            return (bool)$result;
        }

        if (!$product) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);
            if (!$productAttributeIds) {
                // if attributes are not passed, get them from this product instance
                $productAttributeIds = $product->getProductVisibility();
            } elseif (is_string($productAttributeIds)) {
                // ensure these attribute values are array
                $productAttributeIds = explode(',', $productAttributeIds);
            }
        } elseif (!$productAttributeIds) {
            $productAttributeIds = explode(",", $product->getProductVisibility());
        }

        $applyVisibilityRules = false;
        $productVisibility = true;

        $applyVisibilityRules = (bool)Mage::getStoreConfig('catalog/frontend/product_visibility_enabled');
        if ($applyVisibilityRules) {
            $productVisibility = $this->getProductVisibility($product, $productSectionVisibility, $productAttributeIds);
        }

        $packageType = null;
        $packageTypeFromRequest = Mage::registry('products_visibility_package_type');

        $packageTypeCodes = explode(', ', $product->getPackageType());

        if ($packageTypeFromRequest) {
            foreach ($packageTypeCodes as $packageTypeCode) {
                if ($packageTypeCode == $packageTypeFromRequest) {
                    $packageType = $packageTypeCode;
                    break;
                }
            }
        } else {
            $packageType = array_shift($packageTypeCodes);
        }

        $packageType = $product->getPackageType();
        $productTypeArray = $product->getType();
        $packageSubtypeCode = array_shift($productTypeArray);

        switch ($productSectionVisibility) {
            case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY:
                $subTypePackageSectionVisibility = Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_INVENTORY;
                break;
            case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CONFIGURATOR:
                $subTypePackageSectionVisibility = Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_CONFIGURATOR;
                break;
            case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_SHOPPING_CART:
                $subTypePackageSectionVisibility = Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_SHOPPING_CART;
                break;
            case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_EXTENDED_SHOPPING_CART:
                $subTypePackageSectionVisibility = Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_EXTENDED_SHOPPING_CART;
                break;
            case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_ORDER_OVERVIEW:
                $subTypePackageSectionVisibility = Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_ORDER_OVERVIEW;
                break;
            case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_VOICELOG:
                $subTypePackageSectionVisibility = Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_VOICELOG;
                break;
            case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_OFFER_PDF:
                $subTypePackageSectionVisibility = Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_OFFER_PDF;
                break;
            case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CALL_SUMMARY:
                $subTypePackageSectionVisibility = Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_CALL_SUMMARY;
                break;
            default:
                $subTypePackageSectionVisibility = null;
                break;
        }

        /**
         * @var Dyna_Package_Helper_Data $packageHelper
         */
        $packageHelper = Mage::helper('dyna_package');
        $packageSubtypeVisibility = $packageHelper->getPackageSubtypeVisibilityForSection($packageType, $packageSubtypeCode, $subTypePackageSectionVisibility);
        $this->getCache()->save(serialize((int)($productVisibility && $packageSubtypeVisibility)), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

        return $productVisibility && $packageSubtypeVisibility;
    }

    /**
     * Return only the fees from a list of product ids
     * Used by defaulted rules to fall back to previously added fee in case it was removed later by another fee
     * Using raw query for speeding purposes
     * @param array $productIds
     * @return array
     */
    public function filterFeeProductIds(array $productIds)
    {
        if (empty($productIds = array_filter($productIds))) {
            return array();
        }

        $cacheKey = 'ALL_FEE_PRODUCTS';

        $cachedValues = $this->getCache()->load($cacheKey);
        // if not already cached, retrieve all attribute admin values
        // cache for performance all fee products
        if ($cachedValues === false) {
            /** @var Magento_Db_Adapter_Pdo_Mysql $connection */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $feeSubtypes = implode("\",\"", Dyna_Catalog_Model_Type::getFeesSubtypes());
            $query = "SELECT entity_id FROM catalog_product_entity_text WHERE value IN (\"" . $feeSubtypes . "\")";

            $allFeeProducts = $connection->fetchAll($query, [], Zend_Db::FETCH_COLUMN);

            $this->getCache()->save(serialize($allFeeProducts), $cacheKey, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

        } else {
            $allFeeProducts = unserialize($cachedValues);
        }

        return array_intersect($productIds, $allFeeProducts);
    }

    /**
     * Get attribute set name by id
     * Used for product filtering, instead of calling product method in omnius, using this singleton helper for performance boost
     * @param string $attributeSetId
     */
    public function getAttributeSet(string $attributeSetId = null)
    {
        if (!$attributeSetId) {
            return "";
        }

        if (empty($this->attributeSetNames[$attributeSetId])) {
            // Load a dummy product
            $attributeSetName = Mage::getModel('eav/entity_attribute_set')
                ->load($attributeSetId)
                ->getAttributeSetName();
            $this->attributeSetNames[$attributeSetId] = $attributeSetName;
        }

        return $this->attributeSetNames[$attributeSetId];
    }

    /**
     * Overriding omnius addFilter for passing reference to filter array
     * Speeds up configurator by 10 seconds
     * @param $attr
     * @param $filters
     * @param $store
     * @param $localeCode
     * @param $product
     * @return mixed
     */
    public function addFilter($attr, &$filters, $store, $localeCode, $product)
    {
        $storeLabel = $attr->getStoreLabel(Mage::app()->getStore()->getId());
        $attrCode = $attr->getAttributeCode();
        $attrValue = $attr->getFrontend()->getValue($product);
        $filters[$attrCode]['label'] = $storeLabel;
        if ($attr->getFrontendInput() == 'price') {
            $filters[$attrCode]['options'][$product->getData($attrCode)] = $store->convertPrice($attrValue, true, false);
        } elseif ($attr->getAttributeCode() == 'weight') {
            $filters[$attrCode]['options'][$product->getData($attrCode)] = Zend_Locale_Format::toFloat($attrValue, array('locale' => $localeCode, 'precision' => 0));
        } elseif ($attr->getFrontendInput() == 'multiselect') {
            // get all available labels
            $options = $attr->getSource()->getAllOptions(false, true);
            $labels = array();
            foreach ($options as $option) {
                $labels[$option['value']] = $option['label'];
            }
            // get filters attr ids
            $ids = explode(',', $product->getData($attrCode));
            sort($ids);
            foreach ($ids as $attributeId) {
                $filters[$attrCode]['options'][$attributeId] = isset($labels[$attributeId]) ? $labels[$attributeId] : '';
            }
        } else {
            $filters[$attrCode]['options'][$product->getData($attrCode)] = $attrValue;
        }
        $filters[$attrCode]['position'] = $attr->getPosition();

        return $this;
    }

    /**
     * Returns options for select/multiselect type attributes
     *
     * @param $attributeCode
     * @param $assoc
     *
     * @return mixed|boolean
     */
    public function getAttributeOptions($attributeCode, $assoc = false)
    {
        $options = [];

        $attribute = Mage::getSingleton("eav/config")->getAttribute(Dyna_Catalog_Model_Product::ENTITY, $attributeCode);
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
            if ($assoc) {
                //   array_walk($options, function(&$a, $b) { return $b['value'] = $b['label']; });
                $options = array_reduce(
                    $options,
                    function ($result, $item) {
                        $result[$item['value']] = $item['label'];
                        return $result;
                    },
                    []);
            }
        }

        return $options;
    }

    /**
     * @todo add caching for the products
     * Get the configurable checkout products based on the option_type and package_type
     * @param null $optionType
     * @param null $packageTypeDefault
     * @return mixed
     */
    public function getConfigurableCheckoutProducts($optionType = null, $packageTypeDefault = null) {

        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $cart->getQuote();

        // If packageType is not passed
        // Getting all the package type, which will be used to add only configurable checkout products for which package are added in cart
        $packageTypes = $packageTypeIds = array();
        if ($packageTypeDefault) {
            $packageTypes[] = $packageTypeDefault;
        } else {
            foreach ($quote->getPackages() as $package) {
                if (!in_array($package['type'], $packageTypes))
                    $packageTypes[] = $package['type'];
            }
        }

        //Getting Package Type Ids which will be used to filter the products
        $packageTypeAttribute = Mage::getSingleton("eav/config")
            ->getAttribute(Dyna_Catalog_Model_Product::ENTITY, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
        foreach($packageTypes as $packageType){
            $packageTypeIds[] = $this->getAttributeAdminLabel($packageTypeAttribute, null, $packageType);
        }

        //Getting option_type attribute id which will be used to filter the products
        $optionTypeAttribute = Mage::getSingleton("eav/config")
            ->getAttribute(Dyna_Catalog_Model_Product::ENTITY, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_OPTION_TYPE_ATTR);

        $optionTypeId = $this->getAttributeAdminLabel($optionTypeAttribute, null, $optionType);

        // If multiple package type is selected for the same sku
        foreach($packageTypeIds as $packageTypeId) {
            $findInSet[] = array('finset' => $packageTypeId);
        }

        $products =
            Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_OPTION_TYPE_ATTR, array('finset' => $optionTypeId))
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, $findInSet)
                ->addAttributeToSelect('price')
                ->addAttributeToSelect('maf')
                ->addAttributeToSelect('display_name_communication')
                ->addOrder('option_ranking', Varien_Data_Collection::SORT_ORDER_ASC);

        return $products->load();
    }


    /**
     * @param $attribute
     * @param $attributeValueId
     * @param null $attributeValueLabel
     * @return mixed|string
     */
    public function getAttributeValueIds($attribute, $attributeValueLabel)
    {
        $key = $attribute->getAttributeCode() . '_values';
        if (empty($this->attributeAdminLabel[$key])) {
            $cachedValues = $this->getCache()->load($key);
            // if not already cached, retrieve all attribute admin values
            if ($cachedValues === false) {
                $result = [];
                $_collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                    ->setStoreFilter(0)//to get the admin values
                    ->setAttributeFilter($attribute->getId());
                $options = Mage::getSingleton('core/resource')
                    ->getConnection('core_read')
                    ->fetchAll($_collection->getSelect());
                unset($_collection);
                // store as value=>label array for easy access
                foreach ($options as $option) {
                    $result[$option['option_id']] = $option['value'];
                }

                $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
            } else {
                $result = unserialize($cachedValues);
            }

            $this->attributeAdminLabel[$key] = $result;
        } else {
            $result = $this->attributeAdminLabel[$key];
        }

        $ids = array();

        foreach ($result as $key => $value) {
            if(strpos($value, $attributeValueLabel) !== false) {
                $ids[] = $key;
            }
        }

        return $ids;
    }

    public function getAllCategories()
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql =
<<<SQL
SELECT 
  `main_table`.`entity_id` as `entity_id`,
  `main_table`.`unique_id` as `category_id`,
  `main_table`.`path` as `path`,
  `category_names`.`value` as `name`,
  `category_ids`.`value` as `unique_id`
FROM
  `catalog_category_entity` AS main_table 
  LEFT JOIN eav_attribute `name`
    ON `name`.`entity_type_id` = :entity_type_id 
    AND `name`.`attribute_code` = :name_attribute_code 
  LEFT JOIN `eav_attribute` `uniqueid`
    ON `uniqueid`.`entity_type_id` = :entity_type_id
    AND `uniqueid`.`attribute_code` = :url_key_attribute_code 
  LEFT JOIN `catalog_category_entity_varchar` `category_names` 
    ON `main_table`.`entity_id` = `category_names`.`entity_id` 
    AND `name`.`attribute_id` = `category_names`.`attribute_id` 
  LEFT JOIN `catalog_category_entity_varchar` `category_ids` 
    ON `main_table`.`entity_id` = `category_ids`.`entity_id` 
    AND `uniqueid`.`attribute_id` = `category_ids`.`attribute_id` 
GROUP BY `main_table`.`entity_id` 
ORDER BY `main_table`.entity_id ASC ;
SQL;
        $result = $connection->fetchAll($sql, array(
            ':entity_type_id' => 3,
            ':name_attribute_code' => 'name',
            ':url_key_attribute_code' => 'url_key',
        ));


        // return with category id as key
        return array_combine(array_column($result, 'entity_id'), $result);

    }
    
    /**
     * Determines if a product is of a certain type by comparing it's type to a provided array
     * @param array|string $types
     * @param Dyna_Catalog_Model_Product $product|Mage_Catalog_Model_Product
     * @return bool
     */
    public function is($types, Mage_Catalog_Model_Product $product)
    {
        return $product->is($types);
    }
}
