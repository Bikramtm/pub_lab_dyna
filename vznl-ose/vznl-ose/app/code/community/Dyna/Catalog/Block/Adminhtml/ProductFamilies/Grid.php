<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Catalog_Block_Adminhtml_ProductFamilies_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("productFamiliesGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("dyna_catalog/productFamily")->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return self|Mage_Adminhtml_Block_Widget_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn("entity_id", array(
            "header" => Mage::helper("dyna_catalog")->__("ID"),
            "type" => "number",
            "index" => "entity_id",
        ));
        $this->addColumn("product_family_id", array(
            "header" => Mage::helper("dyna_catalog")->__("Family id"),
            "index" => "product_family_id",
        ));
        $this->addColumn("product_family_name", array(
            "header" => Mage::helper("dyna_catalog")->__("Name"),
            "index" => "product_family_name",
        ));
        $this->addColumn("product_version_id", array(
            "header" => Mage::helper("dyna_catalog")->__("Version ID"),
            "index" => "product_version_id",
        ));
        $this->addColumn("package_type_id", array(
            "header" => Mage::helper("dyna_catalog")->__("Package type"),
            "index" => "package_type_id",
            "type" => "options",
            "searchable" => false,
            "options" => Dyna_Catalog_Block_Adminhtml_ProductFamilies::getPackageTypes(false),
        ));

        $this->addColumn("package_subtype_id", array(
            "header" => Mage::helper("dyna_catalog")->__("Package subtype"),
            "index" => "package_subtype_id",
            "type" => "options",
            "searchable" => false,
            'options' => Dyna_Catalog_Block_Adminhtml_ProductFamilies::getPackageSubTypes(false),
        ));

        $this->addColumn("lifecycle_status", array(
            "header" => Mage::helper("dyna_catalog")->__("Lifecycle status"),
            "index" => "lifecycle_status",
            "searchable" => false,
            "renderer" => "dyna_catalog/adminhtml_lifecycleRenderer",
            'type' => 'options',
            'options' => self::getLifecycleStatuses(),
        ));

        return parent::_prepareColumns();
    }

    public static function getLifecycleStatuses()
    {
        return Dyna_Catalog_Model_Lifecycle::getLifecycleTypes();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);

        return $this;
    }
}
