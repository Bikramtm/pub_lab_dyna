<?php

class Dyna_Catalog_Block_Adminhtml_ProductVersions_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                "id" => "edit_form",
                "action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldSet = $form->addFieldset("general",
            array(
                "legend" => "Product version configuration"
            )
        );

        $fieldSet->addField("product_version_id", "text",
            array(
                "name" => "product_version_id",
                "label" => "Code",
                "input" => "text",
                "required" => true,
                'note' => "Version identifier",
            )
        );

        $fieldSet->addField("product_version_name", "text",
            array(
                "name" => "product_version_name",
                "label" => "Name",
                "input" => "text",
                'note' => "Product version name",
            )
        );

        $fieldSet->addField("lifecycle_status", "select",
            array(
                "name" => "lifecycle_status",
                "label" => "Lifecycle status",
                "input" => "text",
                "values" => Dyna_Catalog_Model_Lifecycle::getLifecycleTypes(),
            )
        );

        $fieldSet->addField("product_family_id", "text",
            array(
                "name" => "product_family_id",
                "label" => "Family id",
                "input" => "text",
                'note' => "Product family id",
            )
        );

        $fieldSet->addField("package_type_id", "select",
            array(
                "name" => "package_type_id",
                "label" => "Package type",
                "input" => "text",
                'note' => "Package type",
                "values" => Mage::helper('dyna_package')->getPackageTypesId()
            )
        );

        $productFamilyData = Mage::registry("product_version_data");
        if ($productFamilyData) {
            $form->addValues($productFamilyData->getData());
        }

        return parent::_prepareForm();
    }
}
