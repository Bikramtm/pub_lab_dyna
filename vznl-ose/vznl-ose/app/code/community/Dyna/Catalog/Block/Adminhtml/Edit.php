<?php

class Dyna_Catalog_Block_Adminhtml_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_blockGroup = "dyna_catalog";
        $this->_controller = "adminhtml_delivery";
        $this->_headerText = Mage::helper("catalog")->__("Add New Delivery Option");
        $this->_mode = "edit_tab";
        $this->_updateButton("save", "label", Mage::helper("catalog")->__("Save Delivery Option"));

        parent::__construct();
    }
}
