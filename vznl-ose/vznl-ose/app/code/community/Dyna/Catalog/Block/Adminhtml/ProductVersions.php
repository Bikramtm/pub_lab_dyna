<?php

class Dyna_Catalog_Block_Adminhtml_ProductVersions extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_productVersions";
        $this->_blockGroup = "dyna_catalog";
        $this->_headerText = Mage::helper("dyna_catalog")->__("Product Versions");
        $this->_addButtonLabel = Mage::helper("dyna_catalog")->__("Add New Product Version");
        parent::__construct();
    }
}
