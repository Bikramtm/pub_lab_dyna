<?php

class Dyna_Catalog_Block_Adminhtml_ProductFamilies_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                "id" => "edit_form",
                "action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldSet = $form->addFieldset("general",
            array(
                "legend" => "Product family configuration"
            )
        );

        $fieldSet->addField("product_family_id", "text",
            array(
                "name" => "product_family_id",
                "label" => "Family ID",
                "input" => "text",
                "required" => true,
                'note' => "Identifier for this family of products",
            )
        );

        $fieldSet->addField("lifecycle_status", "select",
            array(
                "name" => "lifecycle_status",
                "label" => "Family lifecycle",
                "input" => "text",
                "values" => Dyna_Catalog_Model_Lifecycle::getLifecycleTypes(),
            )
        );

        $fieldSet->addField("product_family_name", "text",
            array(
                "name" => "product_family_name",
                "label" => "Family name",
                "input" => "text",
                'note' => "Name of family",
            )
        );

        $fieldSet->addField("product_version_id", "text",
            array(
                "name" => "product_version_id",
                "label" => "Version ID",
                "input" => "text",
                'note' => "Product version ID",
            )
        );

        $fieldSet->addField("package_type_id", "select",
            array(
                "name" => "package_type_id",
                "label" => "Package type",
                "input" => "text",
                "values" => Dyna_Catalog_Block_Adminhtml_ProductFamilies::getPackageTypes()
            )
        );

        $fieldSet->addField("package_subtype_id", "select",
            array(
                "name" => "package_subtype_id",
                "label" => "Package subtype",
                "input" => "text",
                "values" => Dyna_Catalog_Block_Adminhtml_ProductFamilies::getPackageSubTypes()
            )
        );

        $productFamilyData = Mage::registry("product_family_data");
        if ($productFamilyData) {
            $form->addValues($productFamilyData->getData());
        }

        return parent::_prepareForm();
    }
}
