<?php

class Dyna_Catalog_Block_Adminhtml_ProductFamilies extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_productFamilies";
        $this->_blockGroup = "dyna_catalog";
        $this->_headerText = Mage::helper("dyna_catalog")->__("Product Families");
        $this->_addButtonLabel = Mage::helper("dyna_catalog")->__("Add New Product Family");
        parent::__construct();
    }

    /**
     * @param bool $includeDefault
     * @return array
     */
    public static function getPackageTypes($includeDefault = true)
    {
        $options = [];

        if($includeDefault){
            $options = [
                null => Mage::helper('catalog')->__('Please select...')
            ];
        }
        $collectionPackageTypes = Mage::getModel('dyna_package/packageType')->getCollection();
        foreach ($collectionPackageTypes as $packageType) {
            $options[$packageType->getId()] = $packageType->getFrontEndName() . ' (' . $packageType->getPackageCode() . ")";
        }

        return $options;
    }

    /**
     * @param bool $includeDefault
     * @param null $packageTypeId
     * @return array
     */
    public static function getPackageSubTypes($includeDefault = true, $packageTypeId = null)
    {
        $options = [];

        if ($includeDefault) {
            $options = [
                null => Mage::helper('catalog')->__('Please select...')
            ];
        }
        /** @var Dyna_Package_Model_PackageSubtype $collectionPackageSubTypes */
        $collectionPackageSubTypes = Mage::getModel('dyna_package/packageSubtype')->getCollection();
        $collectionPackageSubTypes = $collectionPackageSubTypes->joinCollectionPackageTypes();
        if (isset($packageTypeId)) {
            $collectionPackageSubTypes->addFieldToFilter('package_type_id', $packageTypeId);
        }

        foreach ($collectionPackageSubTypes as $packageSubType) {
            $options[$packageSubType->getId()] = $packageSubType->getFrontEndName() . ' (' . $packageSubType->getPackageTypeCode() . ")";
        }

        return $options;
    }
}
