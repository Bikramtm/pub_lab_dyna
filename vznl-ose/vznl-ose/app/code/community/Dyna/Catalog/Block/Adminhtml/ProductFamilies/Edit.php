<?php

class Dyna_Catalog_Block_Adminhtml_ProductFamilies_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_blockGroup = "dyna_catalog";
        $this->_controller = "adminhtml_productFamilies";
        $this->_headerText = Mage::helper("dyna_catalog")->__("Add Product Family");
        $this->_mode = "edit_tab";
        $this->_updateButton("save", "label", Mage::helper("dyna_catalog")->__("Save family"));

        parent::__construct();
    }
}
