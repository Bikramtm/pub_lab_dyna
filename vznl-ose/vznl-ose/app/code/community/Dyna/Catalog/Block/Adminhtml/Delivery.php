<?php

class Dyna_Catalog_Block_Adminhtml_Delivery extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = "dyna_catalog";
        $this->_controller = "adminhtml_delivery";
        $this->_headerText = Mage::helper("catalog")->__("Delivery Options");
        $this->_addButtonLabel = Mage::helper("catalog")->__("Add New Delivery Option");

        parent::__construct();
    }
}
