<?php


class Dyna_Catalog_Model_Import_Categories extends Dyna_Import_Model_Adapter_Xml
{
    const CATEGORY_DISPLAY_MODE = "PRODUCTS";
    const CATEGORY_IS_ACTIVE = 1;
    const IS_FAMILY_NO = 'No';
    const IS_FAMILY_YES = 'DisplayFamily';
    const IS_FAMILY_MUTUAL = 'MutuallyExclusiveFamily';
    const DEFAULT_ID_CATEGORY = 2;

    public function import()
    {
        // retrieve data
        $rows = $this->getXmlData();
        $parentCategory = Mage::getModel('catalog/category')->load(self::DEFAULT_ID_CATEGORY);

        foreach ($rows['Category'] as $category) {
            $isNew = false;
            $action = 'updated';

            // search category
            /** @var Dyna_Catalog_Model_Category $categoryModel */
            $categoryModel = Mage::getModel('dyna_catalog/category')
                ->getCollection()
                ->addFieldToFilter('unique_id', ['eq' => $category['CommercialIdentifier']])
                ->getFirstItem();

            if (!$categoryModel || !$categoryModel->getId()) {
                // create a new category
                $isNew = true;
                /** @var Dyna_Catalog_Model_Category $categoryModel */
                $categoryModel = Mage::getModel('dyna_catalog/category');
            }

            // set data for new categories
            $categoryName = (isset($category['DisplayName']) && trim($category['DisplayName']) != '') ?
                $category['DisplayName'] : $category['ExternalIdentifier'];

            // set parent if is defined
            if (isset($category['ParentIdentifier']) && trim($category['ParentIdentifier']) != '') {
                $parentCategoryModel = Mage::getModel('dyna_catalog/category')
                    ->getCollection()
                    ->addFieldToFilter('unique_id', ['eq' => $category['ParentIdentifier']])
                    ->getFirstItem();

                if ($parentCategoryModel && $parentCategoryModel->getId()) {
                    $parentCategory = $parentCategoryModel;
                }
            }

            // for a new category set path and name
            if ($isNew) {
                $action = 'added';
                $general['name'] = $categoryName;
                $general['path'] = $parentCategory->getPath();
            }

            // set categories data
            $general['display_mode'] = self::CATEGORY_DISPLAY_MODE;
            $general['is_active'] = self::CATEGORY_IS_ACTIVE;
            $general['unique_id'] = $category['CommercialIdentifier'];
            $general['is_anchor'] = ($category['IsAnchor'] == 'true') ? 1 : 0;
            $general['description'] = $category['Description'] ?? '';
            $general['family_position'] = $category['FamilyCategorySorting'] ?? '';
            $general['external_identifier'] = $category['ExternalIdentifier'] ?? '';
            $general['stack'] = $this->stack;

            switch ($category['IsFamily']) {
                case self::IS_FAMILY_NO:
                    $general['is_family'] = 0;
                    $general['allow_mutual_products'] = 0;
                    break;
                case self::IS_FAMILY_YES:
                    $general['is_family'] = 1;
                    $general['allow_mutual_products'] = 0;
                    break;
                case self::IS_FAMILY_MUTUAL:
                    $general['is_family'] = 1;
                    $general['allow_mutual_products'] = 1;
                    break;
                default:
                    // nothing to do
            }

            try {
                $categoryModel->addData($general);
                $categoryModel->save();
                $this->writeLine("Category ({$categoryModel->getUniqueId()}) was {$action}.");
            } catch (Exception $e) {
                $this->writeLine($e->getMessage());
            }
        }
    }
}
