<?php

/**
 * Class Dyna_Catalog_Model_Product_Import_ProductFamily
 */
class Dyna_Catalog_Model_Product_Import_ProductFamily extends Omnius_Import_Model_Import_ImportAbstract
{
    const NUMBER_OF_COLUMNS = 6;
    protected $_data;
    protected $_headerColumns;
    protected $_csvDelimiter = ';';
    protected $_header;
    protected $_rowNumber = 1;
    protected $_logFileName = "product_family";
    protected $_logFileExtension = "log";

    /** @var Omnius_Import_Helper_Data $_helper */
    public $_helper;
    public $packageHelper = null;
    public $packageTypes = [];
    public $packageTypesId = [];
    public $packageSubTypes = [];

    /**
     * Dyna_Catalog_Model_Product_Import_ProductFamily constructor.
     */
    public function __construct()
    {
        parent::__construct();
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $this->packageHelper = Mage::helper('dyna_package');
        //get package types from db
        $this->packageTypes = $this->packageHelper->getPackageTypes();
        $this->packageTypesId = $this->packageHelper->getPackageTypesId();
        $this->packageSubTypes = [];
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
    }

    /**
     * @param $header
     * @return bool
     */
    public function setHeader($header)
    {
        $this->_header = $header;
        return true;
    }

    public function process($rawData)
    {
        $this->_rowNumber++;
        $rawData = array_combine($this->_header, $rawData);
        if (!array_filter($rawData)) {
            return false;
        }

        try {
            //for future modifications
            $this->_data['product_family_id'] = $rawData['product_family_id'];
            $this->_data['stack'] = $this->stack;

            if (isset($rawData['lifecycle_status']) && in_array($rawData['lifecycle_status'], Dyna_Catalog_Model_Lifecycle::getLifecycleTypes())) {
                $this->_data['lifecycle_status'] = $rawData['lifecycle_status'];
            } elseif (isset($rawData['lifecycle_status'])) {
                $this->_log("[Info] '" . $rawData['lifecycle_status'] . "' is not a valid type for lifecycle status, the supported types are: " . implode('; ', Dyna_Catalog_Model_Lifecycle::getLifecycleTypes()) . '. Lifecycle for this entry will be set to null.');
            }

            $this->_data['product_family_name'] = $rawData['product_family_name'];
            $this->_data['product_version_id'] = $rawData['product_version_id'] ?? '';
            $rawData['package_type_id'] = trim($rawData['package_type_id']);
            $rawData['package_subtype_id'] = trim($rawData['package_subtype_id']);

            if (!empty($this->packageTypes) && $rawData['package_type_id'] !== '') {
                if (in_array($rawData['package_type_id'], array_keys($this->packageTypes))) {
                    $this->_data['package_type_id'] = array_search($this->packageTypes[$rawData['package_type_id']], $this->packageTypesId);
                } else {
                    $this->_log("[Info] '" . $rawData['package_type_id'] . "' is not a valid value for package type, the supported types are: " . implode('; ', $this->packageTypes) . '. package_type_id for this entry will be set to null.');
                }
            }

            if ($rawData['package_subtype_id'] !== '' && isset($this->_data['package_type_id']) && $this->_data['package_type_id'] != null) {
                if ($subtype = $this->packageHelper->getPackageSubtype($rawData['package_type_id'], $rawData['package_subtype_id'], false, [])) {
                    $this->_data['package_subtype_id'] = $subtype['entity_id'];
                } else {
                    $this->_logError("[ERROR] '" . $rawData['package_subtype_id'] . "' is not a valid value for package subtype. package_subtype_id for this entry will be set to null.");
                }
            }

            /** save product family record */
            $model = Mage::getModel('dyna_catalog/productFamily');

            $model->addData($this->_data);
            $model->save();
            if ($this->getDebug()) {
                $this->_log("Successfully imported CSV line " . $this->_rowNumber . " : " . implode('|', $this->_data));
            }

            $this->_totalFileRows++;
        } catch (Exception $e) {
            $this->_skippedFileRows++;
            $this->_logError("[ERROR] Skipped row because :" . $e->getMessage());
            fwrite(STDERR, $e->getMessage());
        }
    }
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }
}
