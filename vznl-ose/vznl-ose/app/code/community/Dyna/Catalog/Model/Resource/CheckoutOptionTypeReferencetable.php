<?php

/**
 * Class Dyna_Catalog_Model_Resource_Checkout_Option_Type_Referencetable
 */
class Dyna_Catalog_Model_Resource_CheckoutOptionTypeReferencetable extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init("dyna_catalog/checkoutOptionTypeReferencetable", "reference_id");
    }
}
