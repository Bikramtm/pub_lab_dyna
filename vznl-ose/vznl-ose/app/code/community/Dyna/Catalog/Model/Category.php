<?php

class Dyna_Catalog_Model_Category extends Omnius_Catalog_Model_Category
{
    protected $categories = null;
    protected static $categoryCache = array();

    /**
     * Get a list of all categories that are family and mutually exclusive (if the current model has an id, that id is not returned)
     *
     * @return array
     */
    public function getAllExclusiveFamilyCategoryIds()
    {
        $key = md5(serialize([strtolower(__METHOD__), $this->getId()]));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $result = [];
            /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
            $collection = $this->getCollection()->addAttributeToFilter('is_family', 1)->addAttributeToFilter('allow_mutual_products', 1)->load();
            foreach ($collection as $category) {
                if ($category->getId() != $this->getId()) {
                    $result[] = $category->getId();
                }
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    public function getCategoryByUrlKey($urlKey)
    {
        return $this->getCollection()
            ->addAttributeToFilter('url_key', $urlKey)
            ->getFirstItem()
            ->getId();
    }

    public function getProductSkus($forOtherCategoryId = null)
    {
        if (!$forOtherCategoryId) {
            $category = $this;
        } else {
            /** @var Dyna_Catalog_Model_Category $category */
            $category = Mage::getModel('catalog/category')->load($forOtherCategoryId);
        }

        $products = $category->getId() ? $category->getProductIdsAndSkus($category->getId()) : array();

        // returns $val[sku] = sku
        return array_combine($products, $products);
    }

    public function getProductSkusForCategoryNames($categoryNames)
    {
        if ($categoryNames === null) {
            $condition = ['eq' => $this->getId()];
        } elseif (is_array($categoryNames)) {
            $condition = ['in' => $categoryNames];
        } else {
            $condition = ['eq' => $categoryNames];
        }
        $key = md5(serialize([strtolower(__METHOD__), serialize($condition)]));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
            $collection = Mage::getModel('catalog/category')->getCollection()
                ->joinField('product_id', 'catalog/category_product', 'product_id', 'category_id=entity_id', null, 'left')
                ->joinField('product_sku', 'catalog/product', 'entity_id', 'entity_id = product_id', null, 'left')
                ->addFieldToFilter('name', $condition);
            $collection->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('at_product_sku.sku');

            $result = [];
            foreach ($collection->load() as $product) {
                if (trim($product['sku'])) {
                    $result[$product['sku']] = $product['sku'];

                }
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Return all categories for a given list of products
     * @param $productIds string|array
     * @return array
     */
    public function getProductCategoryIds($productIds)
    {
        if (is_string($productIds)) {
            $productIds = [$productIds];
        }

        if (empty($productIds)) {
            return array();
        }

        /** @var Varien_Db_Adapter_Interface $adapter */
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $adapter->select()
            ->from('catalog_category_product', array('category_id'))
            ->where('product_id in (?)', $productIds);

        return $adapter->fetchCol($select);
    }

    /**
     * Retrieve category path by names path
     *
     * @param string $path
     * @param string $separator
     *
     * @return array|bool
     */
    public function namePathToIdPath($path, $separator = "/")
    {
        $names = explode($separator, $path);
        // all categories have PRODUCTS category as parent
        $ids = [1, 2];
        foreach ($names as $key => $name) {
            $index = $key + 2;
            $parentId = isset($ids[$index - 1]) ? $ids[$index - 1] : 0;

            if (!isset(self::$categoryCache[$parentId])) {
                self::$categoryCache[$parentId] = Mage::getResourceModel('catalog/category_collection')
                    ->addAttributeToFilter('parent_id', $parentId)
                    ->addAttributeToSelect(['entity_id', 'name']);
            }
            $category = self::$categoryCache[$parentId]->getItemByColumnValue('name', $name);

            if (!$category || !$category->getId()) {
                return false;
            }
            $ids[$index] = (int) $category->getEntityId();
        }
        return implode($separator, $ids);
    }

    /**
     * Retrieve category by path name
     *
     * @param $path
     * @param string $separator
     *
     * @return bool|\Mage_Catalog_Model_Abstract
     */
    public function getCategoryByNamePath($path, $separator = '/')
    {
        $cacheKey = md5("category_by_path_" . $path);

        if (!$result = unserialize($this->getCache()->load($cacheKey))) {
            $idPath = $this->namePathToIdPath($path, $separator);
            $idPath = str_replace($separator, "/", $idPath);
            if ($idPath) {
                $result = $this->getCollection()
                    ->addFieldToFilter('path', array('like' => $idPath))
                    ->getFirstItem();
            }

            $this->getCache()->save(serialize($result), $cacheKey, [$this->getCache()::PRODUCT_TAG], $this->getCache()->getTtl());
        }

        return $result;
    }

    public function getPathAsString($pretty = true, $separator = "->")
    {
        $pathIds = explode('/', $this->getPath());
        // Unsetting root and default categories
        unset($pathIds[0], $pathIds[1]);
        $pathNames = array();
        $allCategories = $this->getAllCategories();


        foreach ($pathIds as $pathId) {
            $pathNames[] = $allCategories[$pathId];
        }

        return $pretty ? $this->getName() . " " . "(" . implode($separator, $pathNames) . ")" : implode($separator, $pathNames);
    }

    public function getAllCategories()
    {
        $cacheKey = md5("all_categories_id_to_name");
        if (!$this->categories) {
            if (!$result = unserialize($this->getCache()->load($cacheKey))) {
                $sql =
<<<SQL
SELECT 
  main_table.`entity_id`,
  category_names.`value` 
FROM
  catalog_category_entity AS main_table 
  LEFT JOIN eav_attribute name
    ON name.`entity_type_id` = 3 
    AND attribute_code = 'name' 
  LEFT JOIN catalog_category_entity_varchar category_names 
    ON main_table.`entity_id` = category_names.`entity_id` 
    AND name.`attribute_id` = `category_names`.`attribute_id` 
GROUP BY main_table.`entity_id` ORDER BY main_table.entity_id ASC;
SQL;
                $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                $result = $connection->fetchPairs($sql);
                $this->getCache()->save($this->serialize($result), $cacheKey, array($this->getCache()::PRODUCT_TAG), $this->getCache()->getTtl());
            }

            $this->categories = $result;
        }

        return $this->categories;
    }

    public function getCategoryByUniqueId($uniqueId)
    {
        $cacheKey = md5("category_by_unique_id_" . $uniqueId);

        if (!$result = unserialize($this->getCache()->load($cacheKey))) {
            /** @var Dyna_Catalog_Model_Category $category */
            $result = Mage::getModel('dyna_catalog/category')
                ->getCollection()
                ->addFieldToFilter('unique_id', ['eq' => $uniqueId])
                ->getFirstItem();

            $this->getCache()->save(serialize($result), $cacheKey, array($this->getCache()::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    public function getCategoryFamilyByName($name)
    {
        $cacheKey = md5("category_by_name_" . $name);

        if (!$result = unserialize($this->getCache()->load($cacheKey))) {
            /** @var Dyna_Catalog_Model_Category $category */
            $result = Mage::getModel('catalog/category')
                ->getCollection()
                ->addFieldToFilter('name', ['eq' => $name])
                ->addFieldToFilter('is_family', 1)
                ->getFirstItem();
            $this->getCache()->save(serialize($result), $cacheKey, array($this->getCache()::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        return $result->getName();
    }
}
