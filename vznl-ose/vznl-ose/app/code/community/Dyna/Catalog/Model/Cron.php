<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_Catalog_Model_Cron
{
    public function updateProductLifecycleStatus()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $futureLifecycle = Dyna_Catalog_Model_Lifecycle::getLifecycleStatusIdByCode(Dyna_Catalog_Model_Lifecycle::TYPE_FUTURE);
        $liveLifecycle = Dyna_Catalog_Model_Lifecycle::getLifecycleStatusIdByCode(Dyna_Catalog_Model_Lifecycle::TYPE_LIVE);
        $legacyLifecycle = Dyna_Catalog_Model_Lifecycle::getLifecycleStatusIdByCode(Dyna_Catalog_Model_Lifecycle::TYPE_LEGACY);
        $now = date('Y-m-d');

        /** @var Dyna_Catalog_Model_Resource_Product_Collection $futureProductsCheck */
        $futureProductsCheck = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('lifecycle_status')
            ->addAttributeToFilter('lifecycle_status', array('eq' => $futureLifecycle))
            ->addAttributeToFilter('effective_date', array('lteq' => $now));
        /** @var Dyna_Catalog_Model_Product $product */
        foreach ($futureProductsCheck as $product) {
            $product->setLifecycleStatus($liveLifecycle);
        }
        $futureProductsCheck->save();

        /** @var Dyna_Catalog_Model_Resource_Product_Collection $liveProductsCheck */
        $liveProductsCheck = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('lifecycle_status')
            ->addAttributeToFilter('lifecycle_status', array('eq' => $liveLifecycle))
            ->addAttributeToFilter('expiration_date', array('lteq' => $now));
        foreach ($liveProductsCheck as $product) {
            $product->setLifecycleStatus($legacyLifecycle);
        }
        $liveProductsCheck->save();
    }
}
