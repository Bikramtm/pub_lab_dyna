<?php

class Dyna_Catalog_Model_Receiver extends Mage_Core_Model_Abstract
{
    /**
     * Return current smartcard serial number
     * Using camelcase data keys for aligning with own hardware service validation response which will be added as is to models
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->getData("EquipmentSerialNumber");
    }

    /**
     * Set serial number on current instance
     * Using camelcase data keys for aligning with own hardware service validation response which will be added as is to models
     * @param string $serialNumber
     * @return $this
     */
    public function setSerialNumber(string $serialNumber)
    {
        $this->setData("EquipmentSerialNumber", $serialNumber);

        return $this;
    }

    /**
     * Return true if current receiver supports HD
     * @return bool
     */
    public function supportsHd()
    {
        foreach ($this->getData("FeatureList") as $feature) {
            if ($feature["AttributeName"] == "HD" && $feature["AttributeValue"] == "true") {
                return true;
            }
        }

        return false;
    }

    /**
     * Return true if current receiver supports Vod
     * @return bool
     */
    public function supportsVod()
    {
        foreach ($this->getData("FeatureList") as $feature) {
            if ($feature["AttributeName"] == "VOD" && $feature["AttributeValue"] == "true") {
                return true;
            }
        }

        return false;
    }

    /**
     * Return true if current receiver supports Vod
     * @return bool
     */
    public function supportsHDFree()
    {
        foreach ($this->getData("FeatureList") as $feature) {
            if ($feature["AttributeName"] == "HDFree" && $feature["AttributeValue"] == "true") {
                return true;
            }
        }

        return false;
    }

    /**
     * Get receiver type from getTvEquipmentFeatures service call
     * @return mixed
     */
    public function getType()
    {
        return $this->getData("EquipmentType");
    }
}
