<?php

/**
 * Class Dyna_Catalog_Model_Product_Import_CheckoutProduct
 */
class Dyna_Catalog_Model_Product_Import_CheckoutProduct extends Dyna_Import_Model_Import_Product_Abstract
{
    /** @var string $_multiselectSplit */
    protected $_multiselectSplit = ",";
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ';';
    /** @var string $_logFileName */
    protected $_logFileName = "checkout_product_import";

    /**
     * Dyna_Catalog_Model_Product_Import_CheckoutProducts constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper->setImportLogFile($this->_logFileName . '.log');
        $this->_qties = 0;
    }

    /**
     * Import cable product
     * sku; The SKU will represent the unique productcode of each CheckoutOption. Based on the serviceability response and related rules, only available SKUs for that address (in combination to the other products in the shopping cart) can be displayed.
     * attribute_set;
     * package_type;  KIP/DSL/LTE/…/*
     * package_subtype; The package_subtype will be 'ConfigurableCheckoutOptions'
     * name;
     * option_type; represents the field in the checkout. This currently contains the following possible values:
     * display_label_checkout;
     * option_ranking;
     * price;
     * product_visibility
     *
     * @param array $data
     * @return void
     */
    public function importProduct($data)
    {
        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;
        $data = $this->_setDataMapping($data);

        if (!$this->hasValidPackageDefined($data)) {
            $this->_skippedFileRows++;
            return;
        }

        if (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (!array_key_exists('name', $data) || empty($data['name'])) {
            $this->_logError('Skipping line without name');
            $skip = true;
        }

        if ($skip) {
            //for logging purposes
            $this->_skippedFileRows++;
            return;
        }

        $data['sku'] = trim($data['sku']);
        $this->_currentProductSku = $data['sku'];
        $this->_log('Start importing ' . trim($data['name']));
        $data = $this->checkPrice($data);

        try {
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product');
            $id = $product->getIdBySku($data['sku']);
            $new = ($id == 0);
            if (!$new) {
                $this->_log('Loading existing product "' . $data['sku'] . '" with ID ' . $id);
                $product->load($id);
            }

            // check vat value and tax
            $vatValue = isset($data['vat']) ? $data['vat'] : 0;
            $vatClass = Mage::helper('dyna_catalog')->getVatTaxClass($vatValue);

            $skip = false;
            if (!$this->_setAttributeSetByCode($product, $data)) {
                $this->_logError('Unknown attribute set "' . $data['attribute_set'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->_setWebsitesByCsvString($product, $data)) {
                $this->_logError('Unknown website "' . $data['_product_websites'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->setTaxClassIdByName($product, $vatClass)) {
                $this->_logError((empty($vatClass) ? 'Empty vat value' : 'Unknown vat value "' . $vatClass . '"') . ' for product ' . $data['sku']);
                $skip = true;
            }

            if ($skip) {
                //for logging purposes
                $this->_skippedFileRows++;
                return;
            }

            $this->_setDefaults($product, $data);
            $this->_setStatus($product, $data);
            $this->_setProductData($product, $data);

            if ($new) {
                $this->_createStockItem($product);
            }

            if (!$this->getDebug()) {
                $product->save();
            }

            unset($product);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['name']);
        } catch (Exception $ex) {
            //for logging purposes
            $this->_skippedFileRows++;
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }

    /**
     * Process custom data mapping for cable products
     *
     * @param array $data
     * @return array
     */
    protected function _setDataMapping($data)
    {
        // Default telesales website
        $data['_product_websites'] = 'telesales';

        // If tax class id is not provided, we set it to Taxable Goods by default
        if (!isset($data['tax_class_id'])) {
            $data['tax_class_id'] = 'Taxable Goods';
        } elseif ($data['tax_class_id'] == 'None') {
            $data['tax_class_id'] = 0;
        }

        // Format price decimals
        $data['price'] = $this->getFormattedPrice($data['price']);

        // determine the name based on the gui description or sku
        $data['name'] = $this->determineName($data);
        // if there is no gui description or sku -> unset the name
        if (!$data['name']) {
            unset($data['name']);
        }

        /** Get correct value for product visibility attribute */
        if (isset($data['product_visibility'])) {
            $data['product_visibility'] = $this->parseProductVisibility($data['product_visibility']);
        }

        return $data;
    }

    /**
     * @param $price
     * @return string
     */
    private function getFormattedPrice($price)
    {
        $price = str_replace(',', '.', $price);

        return number_format($price, 4, '.', null);
    }

    /**
     * Determine the name based on gui description or sku
     *
     * @param array $data
     * @return string
     */
    private function determineName($data)
    {
        $productName = null;
        if (!empty($data['name'])) {
            $productName = $data['name'];
        } elseif (isset($data['sku']) && $data['sku']) {
            $productName = $data['sku'];
        }

        return $productName;
    }

    /**
     * Method to parse the value of the product visibility attributes
     * @param $values
     * @return string
     */
    protected function parseProductVisibility($values)
    {
        $markedOptions = [];
        $markedOptionIds = [];
        $options = [];

        $attribute = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY,
                Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        if ('*' == $values) {
            $markedOptionIds = array_column($options, 'label');
        } else {
            $values = explode(',', $values);
            foreach ($values as $value) {
                $markedOptions[] = strtolower(str_replace(' ', '', $value));
            }
            foreach ($options as $option) {
                if (in_array(strtolower(str_replace(' ', '', $option['label'])), $markedOptions)) {
                    $markedOptionIds[] = $option['label'];
                }
            }
        }

        return implode(',', $markedOptionIds);
    }

    /**
     * Checks whether or not product data has both package type and package subtype valid
     * @return bool
     */
    public function hasValidPackageDefined($productData)
    {
        $valid = true;

        $packageTypes = explode(',', $productData['package_type']);
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');
        //get package types from db
        $validPackageTypes = $packageHelper->getPackageTypes();

        if (!empty($validPackageTypes)) {
            $validPackageTypes = array_keys($validPackageTypes);
        }

        $invalidPackageType = null;
        $invalidPackageSubType = null;
        foreach ($packageTypes as $packageType) {
            if (!in_array($packageType, $validPackageTypes)) {
                $invalidPackageType = $packageType;
                break;
            }

            if (empty($packageHelper->getPackageSubtype($packageType, $productData['package_subtype'], false, []))) {
                $invalidPackageSubType = $productData['package_subtype'];
                break;
            }
        }

        if (empty($productData['package_type']) || $invalidPackageType) {
            $productData['package_type'] = !empty($productData['package_type']) ? $productData['package_type'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['sku'] . "\e[0m) Skipped entire product because of invalid package \e[1;33mtype\e[0m value: \e[1;31m" . $invalidPackageType, "\033[0m (case sensitive)\n";
            $this->_log("[ERROR] (SKU " . $productData['sku'] . ") Skipped entire product because of invalid package type value: " . $invalidPackageType . " (case sensitive)");
            $valid = false;
        }

        if (empty($productData['package_subtype']) || $invalidPackageSubType) {
            $productData['package_subtype'] = !empty($productData['package_subtype']) ? $productData['package_subtype'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['sku'] . "\e[0m) Skipped entire product because of invalid package \e[1;33msubType\e[0m value: \e[1;31m" . $productData['package_subtype'], "\033[0m (case sensitive)\n";
            $this->_log("[ERROR] (SKU " . $productData['sku'] . ") Skipped entire product because of invalid package subType value: " . $productData['package_subtype'] . " (case sensitive)");
            $valid = false;
        }

        return $valid;
    }

    /**
     * Checks whether or not price is listed in import file (can be 0, but needs to exists)
     */
    protected function checkPrice($data)
    {
        if (!isset($data['price']) || $data['price'] === "") {
            $data['price'] = 0;
            $this->_log("[NOTICE] Did not receive any value for price column. Setting price to 0");
        }

        return $data;
    }

    /**
     * Set product class id based on the provided tax_class_id in the CSV file
     *
     * @param $product
     * @param string $className
     * @return bool
     */
    public function setTaxClassIdByName($product, $className)
    {
        $productTaxClass = Mage::getModel('tax/class')
            ->getCollection()
            ->addFieldToFilter('class_name', $className)
            ->load()
            ->getFirstItem();
        if (!$productTaxClass || !$productTaxClass->getId()) {
            return false;
        }

        $product->setTaxClassId($productTaxClass->getId());

        return true;
    }

    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        // This will format itself trough observer
        $product->setUrlKey($data['name']);
        $product->setProductVisibilityStrategy('all');
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setIsDeleted('0');
    }

    /**
     * Set product status
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setStatus($product, $data)
    {
        if (array_key_exists('status', $data) && $data['status']) {
            switch ($data['status']) {
                case 'Enabled':
                case 'enabled':
                    $product->setStatus(1);
                    break;
                case 'Disabled':
                case 'disabled':
                default:
                    $product->setStatus(2);
                    break;
            }
        } else {
            $product->setStatus(2);
        }
    }
}
