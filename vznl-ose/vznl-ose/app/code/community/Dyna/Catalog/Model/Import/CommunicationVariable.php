<?php

/**
 * Class Dyna_Catalog_Model_Import_CommunicationVariable
 */
class Dyna_Catalog_Model_Import_CommunicationVariable extends Omnius_Import_Model_Import_ImportAbstract
{
    protected $_data;
    protected $_headerColumns;
    protected $_csvDelimiter = ';';
    protected $_header;
    protected $_websites;
    protected $_importType = '';
    protected $_rawData;
    protected $_currentCondition = 1;
    protected $_currentAction = 1;
    protected $_error;
    protected $_logFileName = "communication_variable_import";
    protected $_logFileExtension = "log";

    /** @var Omnius_Import_Helper_Data $_helper */
    public $_helper;


    /**
     * Dyna_Catalog_Model_Import_CommunicationVariable constructor.
     */
    //to edit
    public function __construct()
    {
        parent::__construct();
        $this->setDefaultWebsites();
    }

    /**
     * Reset communication_variable information
     */
    public function clearData()
    {
        $this->_data = array(
            'reference_id' => null,
            'communication_variable' => '',
            'communication_variable_reference' => ''
        );
    }

    /**
     * @return $this
     */
    public function saveCommunicationVariable()
    {
        /** @var Dyna_catalog_Model_CommunicationVariableReference $model */
        try {
            $model = Mage::getModel('dyna_catalog/communicationVariableReference');
            $data = $this->_data;

            $model->setData($data);
            $model->save();

        }catch (Exception $e) {
            $this->_log("[ERROR] Skipped row because: " . $e->getMessage());
            fwrite(STDERR, "[ERROR] Skipped row because: " . $e->getMessage());
        }

        return $this;
    }

    /**
     * @param $rawData
     * @return bool
     * processing the raw data.
     */
    public function process($rawData)
    {
        $this->_totalFileRows++;
        $rawData = array_combine($this->_header, $rawData);
        if (!array_filter($rawData)) {
            $this->_skippedFileRows++;
            return false;
        }

        $communicationVariable = trim($rawData['communication_variable']);
        
        $existing = Mage::getModel('dyna_catalog/communicationVariableReference')->load($communicationVariable, 'communication_variable');

        if ($existing->getId()) {
            $this->_log("Updating communication variable: " . $rawData['communication_variable'], true);
        } else {
            $this->_log("Saving communication variable: " . $rawData['communication_variable'], true);
        }

        try {
            /** process basic data like name, start date, end date etc */
            $this->clearData();
            $this->_data['reference_id'] = $existing->getId() ?: null;
            $this->_data['communication_variable'] = trim($rawData['communication_variable']);
            $this->_data['communication_variable_reference'] = trim($rawData['message']);

            $this->setRawData($rawData);
            $this->saveCommunicationVariable();
            
        } catch (Exception $e) {
            $this->_skippedFileRows++;
            $this->_log("[ERROR] Skipped row because: " . $e->getMessage());
            fwrite(STDERR, "[ERROR] Skipped row because: " . $e->getMessage());
        }
    }


    /**
     * @param $header
     * @return $this
     */
    public function setHeader($header)
    {
        $this->_header = $header;

        return $this;
    }

    /**
     * Try to set the websites for the rule;
     * Go through each website id or website code provided
     * (the list provided in the csv can be of the form "id1,id2" or "code1,code2"
     * and see if the code/id provided is in the list of websites codes/ids that are possible.
     * If one id/code is not found in the possible list => skip the row, error
     *
     * @param $websites
     * @return array
     */
    protected function setWebsitesIds($websites)
    {
        $websitesData = explode(',', strtolower(trim($websites)));
        $websitesToAdd = [];
        foreach ($websitesData as $key => $possibleWebsite) {
            // if one of the possible websites provided in the csv is "*"
            // the rule will be available for all websites; skip the rest of the code
            if ($possibleWebsite == self::ALL_WEBSITES) {
                return array_keys($this->_websites);
            }
            // the website provided is the code and we should store the id
            if ($validWebsite = array_search($possibleWebsite, $this->_websites)) {
                $websitesToAdd[$key] = $validWebsite;
            } elseif (array_key_exists($possibleWebsite, $this->_websites)) {
                // the website provided is id and we should store it
                $websitesToAdd[$key] = $possibleWebsite;
            } else {
                // not match can be found between the given website and a code or an id
                $this->_log('[ERROR]The website ' . print_r($websitesData, true) . ' is not present/valid. skipping row');
                return false;
            }
        }

        return $websitesToAdd;
    }

    /**
     * Get all DB websites and assign them to local variable
     * @return $this
     */
    protected function setDefaultWebsites()
    {
        $websites = Mage::app()->getWebsites();
        foreach ($websites as $website) {
            $this->_websites[$website->getId()] = $website->getCode();
        }

        return $this;
    }

    
    /**
     * @param $type
     * @return $this
     */
    public function setImportType($type)
    {
        $this->_importType = $type;
        $this->setImportLogFile();

        return $this;
    }

    /**
     * @return string
     */
    public function getImportType()
    {
        return $this->_importType;
    }

    /**
     * Moved from the constructor, because we need the type to be set
     * This function will set te log file name
     * @return $this
     */
    protected function setImportLogFile()
    {
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
        return $this;
    }


    /**
     * @param $data
     * @return $this
     */
    public function setRawData($data)
    {
        $this->_rawData = $data;
        return $this;
    }

    /**
     * @param $helper
     * @return $this
     */
    public function setHelper($helper)
    {
        $this->_helper = $helper;
        return $this;
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg($msg);
    }

    protected function _logError($msg) {
        $this->_helper->logMsg($msg);
    }
}