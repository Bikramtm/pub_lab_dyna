<?php

class Dyna_Catalog_Model_Resource_Delivery_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init("dyna_catalog/delivery");
    }
}
