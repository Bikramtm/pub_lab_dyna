<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/** Class Dyna_Catalog_Model_CommunicationVariableReference */
class Dyna_Catalog_Model_CommunicationVariableReference extends Mage_Core_Model_Abstract
{
    /**
     * Dyna_Catalog_Model_CommunicationVariableReference constructor.
     */
    protected function _construct() {
        $this->_init('dyna_catalog/communicationVariableReference');
    }
}
