<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/** Class Dyna_Catalog_Model_ProductVersion */
class Dyna_Catalog_Model_ProductVersion extends Mage_Core_Model_Abstract
{
    /** @var array */
    protected static $productVersions = false;

    /**
     * Dyna_Catalog_Model_ProductVersion constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_catalog/productVersion');
    }

    /*
     * Return product version id based on version code
     * @return int
     */
    public static function getProductVersionIdByCode(string $versionCode)
    {
        if (self::$productVersions === false) {
            self::cacheVersions();
        }

        return self::$productVersions[$versionCode] ?? null;
    }

    /**
     * Return external version id based on internally represented version id
     * @param $versionId
     * @return string
     */
    public static function getProductVersionCodeById($versionId)
    {
        if (!$versionId) {
            return null;
        }
        if (self::$productVersions === false) {
            self::cacheVersions();
        }

        return array_search($versionId, self::$productVersions);
    }

    public static function cacheVersions()
    {
        /** @var Dyna_Catalog_Model_Resource_ProductVersion_Collection $collection */
        $collection = (new self())
            ->getCollection();
        $collection->getSelect()
            ->reset(Varien_Db_Select::COLUMNS)
            ->columns(array('product_version_id', 'entity_id'));
        self::$productVersions = $collection->getConnection()->fetchPairs($collection->getSelect()->assemble());
    }
}
