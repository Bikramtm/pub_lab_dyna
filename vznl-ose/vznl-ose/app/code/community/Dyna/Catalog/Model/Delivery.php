<?php

class Dyna_Catalog_Model_Delivery extends Mage_Core_Model_Abstract
{
    const DELIVERY_DELIVER_TO_VF_STORE = "Deliver to VF-Store";
    const DELIVERY_DELIVER_TO_ADDRESS = "Deliver to an address";
    const DELIVERY_SAME_DAY_DELIVERY = "Same Day Delivery";

    protected function _construct()
    {
        $this->_init("dyna_catalog/delivery");
    }

    public function getDeliveryTypes() {
        return [
            strtolower(self::DELIVERY_DELIVER_TO_VF_STORE),
            strtolower(self::DELIVERY_DELIVER_TO_ADDRESS),
            strtolower(self::DELIVERY_SAME_DAY_DELIVERY)
        ];
    }
}
