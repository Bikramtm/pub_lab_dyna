<?php


class Dyna_Catalog_Model_Import_Products extends Dyna_Import_Model_Adapter_Xml
{
    protected $importHelper;
    protected $importFn;
    protected $importMobile;

    /**
     * Dyna_Catalog_Model_Import_Products constructor.
     * @param $args
     */
    public function __construct($args)
    {
        parent::__construct($args);

        $this->importHelper = Mage::helper("dyna_import/data");

        $this->importFn = Mage::getModel('dyna_fixed/import_fixedProduct');
        $this->importFn->setImportHelper($this->importHelper);
        $this->importFn->setDebug(false);

        $this->importMobile = Mage::getModel('dyna_mobile/import_mobileProduct');
        $this->importMobile->setImportHelper($this->importHelper);
        $this->importMobile->setDebug(false);
    }

    public function import()
    {
        // retrieve data
        $rows = $this->getXmlData();

        $this->writeLine('Importing product file ' . $this->path);

        // parse products
        foreach ($rows as $key => $productRows) {
            switch ($key) {
                // FN
                case 'FnAccessory':
                    $this->processFnAccessory($productRows);
                    break;
                case 'FnHardware':
                    $this->processFnHardware($productRows);
                    break;
                case 'FnOption':
                    $this->processFnOption($productRows);
                    break;
                case 'FnPromotion':
                    $this->processFnPromotion($productRows);
                    break;
                case 'FnTariffoption':
                    $this->processFnTariffOption($productRows);
                    break;
                case 'FnSalespackage':
                    $this->processFnSalesPackage($productRows);
                    break;
                // Mobile
                case 'Footnote':
                    $this->processFootnote($productRows);
                    break;
                case 'MobileTariff':
                    $this->processMobileTariff($productRows);
                    break;
                case 'MobileHardware':
                    $this->processMobileHardware($productRows);
                    break;
                case 'MobileService':
                    $this->processMobileService($productRows);
                    break;
                default:
                    // Skip row
            }
        }
    }

    protected function processFnAccessory($products)
    {
        foreach ($products as $product) {
            $this->camelToSnakeCase($product);
            $product['category'] = $this->getCategoriesIds($product['category']);
            $product['attribute_set'] = 'FN_Accessory';
            $this->importFn->importProduct($product);
        }
    }

    protected function processFnHardware($products)
    {
        foreach ($products as $product) {
            $this->camelToSnakeCase($product);
            $product['category'] = $this->getCategoriesIds($product['category']);
            $product['attribute_set'] = 'FN_Hardware';
            $this->importFn->importProduct($product);
        }
    }

    protected function processFnOption($products)
    {
        foreach ($products as $product) {
            $this->camelToSnakeCase($product);
            $product['category'] = $this->getCategoriesIds($product['category']);
            $product['attribute_set'] = 'FN_Options';
            $this->importFn->importProduct($product);
        }
    }

    protected function processFnPromotion($products)
    {
        foreach ($products as $product) {
            $this->camelToSnakeCase($product);
            $product['category'] = $this->getCategoriesIds($product['category']);
            $product['attribute_set'] = 'FN_Promotion';
            $this->importFn->importProduct($product);
        }
    }

    protected function processFnTariffOption($products)
    {
        foreach ($products as $product) {
            $this->camelToSnakeCase($product);
            $product['category'] = $this->getCategoriesIds($product['category']);
            $product['attribute_set'] = 'FN_Tariff_Options';
            $this->importFn->importProduct($product);
        }
    }

    protected function processFnSalesPackage($products)
    {
        foreach ($products as $product) {
            $this->camelToSnakeCase($product);
            $product['category'] = $this->getCategoriesIds($product['category']);
            $product['attribute_set'] = 'FN_SalesPackage';
            $this->importFn->importProduct($product);
        }
    }

    protected function processFootnote($products)
    {
        foreach ($products as $product) {
            $this->camelToSnakeCase($product);
            $product['category'] = $this->getCategoriesIds($product['category']);
            $product['attribute_set'] = 'mobile_footnote';
            $this->importMobile->importProduct($product);
        }
    }

    protected function processMobileTariff($products)
    {
        foreach ($products as $product) {
            $this->camelToSnakeCase($product);
            $product['category'] = $this->getCategoriesIds($product['category']);
            $product['attribute_set'] = 'Mobile_Voice_Data_Tariff';
            $this->importMobile->importProduct($product);
        }
    }

    protected function processMobileHardware($products)
    {
        foreach ($products as $product) {
            $this->camelToSnakeCase($product);
            $product['category'] = $this->getCategoriesIds($product['category']);
            $product['attribute_set'] = 'mobile_device';
            $this->importMobile->importProduct($product);
        }
    }

    protected function processMobileService($products)
    {
        foreach ($products as $product) {
            $this->camelToSnakeCase($product);
            $product['category'] = $this->getCategoriesIds($product['category']);
            $product['attribute_set'] = 'mobile_additional_service';
            $this->importMobile->importProduct($product);
        }
    }

    /**
     * @param $categories
     * @return array
     */
    protected function getCategoriesIds($categories)
    {
        $categoryIds = [];
        $categoriesModel = Mage::getModel('dyna_catalog/category')
            ->getCollection()
            ->addFieldToFilter('unique_id', $categories)
            ->getItems();

        foreach ($categoriesModel as $categoryModel) {
            $categoryIds[] = $categoryModel->getId();
        }

        return $categoryIds;
    }
}

