<?php

/**
 * Class Dyna_Catalog_Model_Resource_CommunicationVariableReference_Collection
 */
class Dyna_Catalog_Model_Resource_CommunicationVariableReference_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Dyna_Catalog_Model_Resource_CommunicationVariableReference_Collection constructor
     */
    protected function _construct()
    {
        $this->_init('dyna_catalog/communicationVariableReference');
    }
}
