<?php

/**
 * Class Dyna_Catalog_Model_Import_CategoriesTree
 */
class Dyna_Catalog_Model_Import_CategoriesTree extends Omnius_Import_Model_Import_ImportAbstract
{
    const CATEGORY_DISPLAY_MODE = "PRODUCTS";
    const CATEGORY_IS_ACTIVE = 1;
    const DEFAULT_ID_CATEGORY = 2;

    /** @var string $_multiselectSplit */
    protected $_multiselectSplit = ",";
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ';';
    /** @var string $_logFileName */
    protected $_logFileName = "categories_tree";
    /** @var null $_header */
    protected $_header = null;
    /** @var array */
    protected $_categories = [];
    /** @var array */
    protected $_products = [];

    /**
     * Dyna_Catalog_Model_Product_Import_CheckoutProducts constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper->setImportLogFile($this->_logFileName . '.log');
        $this->_qties = 0;
        $this->getCategories();
    }

    /**
     * Import FN category from csv file
     * @param $data
     */
    public function importCategoryTree($data)
    {
        $data = array_combine($this->_header, $data);
        if (!array_filter($data)) {
            return false;
        }

        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;

        if (!array_key_exists('external_category_id', $data) || empty($data['external_category_id'])) {
            $this->_logError('Skipping line without external category id');
            $skip = true;
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }
        $this->_log('Start importing the category tree ' . $data['external_category_id'], true);

        try {
            // search category
            /** @var Dyna_Catalog_Model_Category $categoryModel */
            $categoryModel = Mage::getModel('dyna_catalog/category')
                ->getCollection()
                ->addFieldToFilter('unique_id', ['eq' => $data['external_category_id']])
                ->getFirstItem();
            $isNew = false;
            if (!$categoryModel || !$categoryModel->getId()) {
                // create a new category
                $isNew = true;
                /** @var Dyna_Catalog_Model_Category $categoryModel */
                $categoryModel = Mage::getModel('dyna_catalog/category');
            }

            // set data for new categories
            $categoryName = (isset($data['category_name']) && trim($data['category_name']) != '') ?
                $data['category_name'] : $data['external_category_id'];

            // set parent if is defined
            if (isset($data['external_parent_category_id']) && trim($data['external_parent_category_id']) != '') {
                $parentCategoryModel = Mage::getModel('dyna_catalog/category')
                    ->getCollection()
                    ->addFieldToFilter('unique_id', ['eq' => $data['external_parent_category_id']])
                    ->getFirstItem();

                if ($parentCategoryModel && $parentCategoryModel->getId()) {
                    $parentCategory = $parentCategoryModel;
                } else {
                    $this->_logError('Skipped category ' . $data['category_name'] . ' with unique id ' . $data['external_category_id'] . ' because parent category with unique id ' . $data['external_parent_category_id'] . ' doesn\'t exists');
                    $this->_skippedFileRows++;
                    return;
                }
            } else {
                $parentCategory = Mage::getModel('catalog/category')->load(self::DEFAULT_ID_CATEGORY);
            }

            // for a new category set path and name
            if ($isNew) {
                $general['name'] = $categoryName;
                $general['path'] = $parentCategory->getPath();
            }

            // set categories data
            $general['display_mode'] = self::CATEGORY_DISPLAY_MODE;
            $general['is_active'] = self::CATEGORY_IS_ACTIVE;
            $general['unique_id'] = $data['external_category_id'];
            $general['is_anchor'] = ($data['is_anchor'] == 'yes') ? 1 : 0;
            $general['is_family'] = ($data['is_family'] == 'yes') ? 1 : 0;
            $general['allow_mutual_products'] = ($data['mutually_exclusive'] == 'yes') ? 1 : 0;
            $general['description'] = $data['description'] ?? '';
            $general['family_position'] = $data['family_category_sorting'] ?? '';
            $general['external_identifier'] = $data['external_category_id'] ?? '';
            $general['hide_not_selected_products'] = ($data['hide_not_selected_products'] == 'yes') ? 1 : 0;

            $categoryModel->addData($general);
            $categoryModel->save();
            $this->_qties++;
            $this->_log('Finished importing the category tree ' . $data['category_name'] . '. Action: ' . ($isNew ? 'new' : 'modified'), true);
        } catch (Exception $ex) {
            fwrite(STDERR, $ex->getMessage());
            $this->_logError($ex->getMessage());
        }
    }

    /**
     * Import FN category from csv file
     * @param $data
     */
    public function importCategoryProducts($data)
    {
        $data = array_combine($this->_header, $data);
        if (!array_filter($data)) {
            return false;
        }

        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;

        if (!array_key_exists('external_category_id', $data) || empty($data['external_category_id'])) {
            $this->_logError('Skipping line without category tree unique Id');
            $skip = true;
        } elseif (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (in_array($data['external_category_id'], $this->_categories)) {
            $this->_logError('Skipping line without valid category for identifier ' . $data['external_category_id']);
            $skip = true;
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }
        $this->_log('Start importing the product ' . $data['sku'] . ' for category' . $data['external_category_id'], true);

        try {
            if (!in_array($data['sku'], $this->_products)) {
                $productId = Mage::getModel("catalog/product")->getIdBySku($data['sku']);
                $this->_products[$data['sku']] = $productId;
            } else {
                $productId = $this->_products[$data['sku']];
            }

            if (!$productId) {
                $this->_logError('Skipping line because of invalid product with SKU: ' . $data['sku']);
                $this->_skippedFileRows++;
                return;
            }

            //add new connection between product and category
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            $query = "INSERT IGNORE INTO catalog_category_product (category_id, product_id, position) VALUES (:category_id, :product_id, :position)";
            $binds = array(
                'category_id' => $this->_categories[$data['external_category_id']],
                'product_id' => $productId,
                'position' => isset($data['position']) ? $data['position'] : 1,
            );
            $write->query($query, $binds);

            $this->_qties++;
            $this->_log('Finished importing the product with sku ' . $data['sku'] . ' for category ' . $data['external_category_id'], true);
        } catch (Exception $ex) {
            fwrite(STDERR, $ex->getMessage());
            $this->_logError($ex->getMessage());
        }
    }

    /**
     * Get array of categories with structutre  [unique_id => entity_id]
     */
    private function getCategories()
    {
        /** @var Dyna_Catalog_Model_Category $categories */
        $categories = Mage::getModel('dyna_catalog/category')->getCollection();
        foreach ($categories as $category) {
            $this->_categories[$category->getUniqueId()] = $category->getId();
        }

    }

    /**
     * @param $header
     * @return bool
     */
    public function setHeader($header)
    {
        $this->_header = $header;
        return true;
    }

    /**
     * Log function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }
}
