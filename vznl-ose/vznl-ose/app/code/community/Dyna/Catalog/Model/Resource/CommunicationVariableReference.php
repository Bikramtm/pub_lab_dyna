<?php

/**
 * Class Dyna_Catalog_Model_Resource_CommunicationVariableReference
 */
class Dyna_Catalog_Model_Resource_CommunicationVariableReference extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init("dyna_catalog/communicationVariableReference", "reference_id");
    }
}
