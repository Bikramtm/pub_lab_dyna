<?php
class Dyna_CatalogLog_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getLatestVersions() {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $entries = $connection->fetchAll("
            SELECT cil.*
            FROM catalog_import_log cil
            INNER JOIN
                (SELECT file_name, MAX(date) AS MaxDateTime
                FROM catalog_import_log
                GROUP BY file_name) grouped 
            ON cil.file_name = grouped.file_name 
            AND cil.date = grouped.MaxDateTime
            GROUP BY file_name
            ");


        $resultString = '
        <table id="cataloglog">
            <tr>
                <th>Date</th>
                <th>Log</th>
                <th>Author</th>
                <th>Filename</th>
                <th>Stack</th>
                <th>Interface version</th>
                <th>Reference Data Build</th>
            </tr>';

        foreach ($entries as $entry) {

            $data = array(
                $entry["date"],
                $entry["log"],
                $entry["author"],
                $entry["file_name"],
                $entry["stack"],
                $entry["interface_version"],
                $entry["reference_data_build"]
            );

            $resultString .= "<tr>";
            foreach ($data as $d) {
                $resultString .= "<td>" . htmlentities(addslashes($d)) . "</td>" ;
            }
            $resultString .= "</tr>";
        }

        $resultString .=  "</table>";

        return strtr($resultString, array('\n' => '', '\r\n' => ''));
    }
}