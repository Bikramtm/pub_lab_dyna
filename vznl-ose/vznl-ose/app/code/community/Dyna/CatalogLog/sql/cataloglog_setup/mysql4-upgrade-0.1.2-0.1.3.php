<?php
/** @var Mage_Sales_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$actions = array(
    'sales_flat_order' => array (
        'superorder_id',
        'billing_address_id'
    ),
    'superorder' => array(
        'created_agent_id',
        'created_dealer_id',
        'updated_at'
    ),
    'catalog_package' => array(
        'campaign_id',
        'package_id'
    ),
    'sales_flat_order_address' => array(
        'address_type'
    ),
    'catalog_product_entity_int' => array(
        'value'
    ),
    'sales_flat_order_item' => array(
        'sku'
    ),
    'sales_flat_quote' => array(
        'active_package_id',
        'created_at',
        'updated_at',
    ),
    'sales_flat_quote_item' => array(
        'sku'
    )
);

$connection = $installer->getConnection();

foreach ($actions as $table => $columns) {
    foreach ($columns as $column) {
        try {
            $connection->addIndex(
                $table
                , 'IDX_' . strtoupper($table) . '_' . strtoupper($column)
                , $column
            );
        } catch (Exception $e) {
            Mage::throwException('Error while adding index to ' . $table . '.' . $column . PHP_EOL . $e->getMessage());
        }
    }
}
$installer->endSetup();