<?php
/** @var Mage_Sales_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

// create new table for registering the catalog imports
if (!$installer->getConnection()->isTableExists($installer->getTable('catalog_import_log'))) {
    $catalogTable = new Varien_Db_Ddl_Table();
    $catalogTable->setName($installer->getTable('catalog_import_log'));

    $catalogTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, array(
            'unsigned' => true,
            'primary' => true,
            'auto_increment' => true,
            'nullable' => false,
        ))

        ->addColumn('import_date', Varien_Db_Ddl_Table::TYPE_DATE, 5)

        ->addColumn('import_log', Varien_Db_Ddl_Table::TYPE_DATE, 5, array(
            'nullable' => true
        ))

        ->addColumn('import_author', Varien_Db_Ddl_Table::TYPE_DATE, 5, array(
            'nullable' => true
        ))

        ->addColumn('file_name', Varien_Db_Ddl_Table::TYPE_TEXT)

        ->addColumn('file_data_version', Varien_Db_Ddl_Table::TYPE_TEXT)

        ->addColumn('file_date', Varien_Db_Ddl_Table::TYPE_DATE, 5)

        ->addColumn('file_log', Varien_Db_Ddl_Table::TYPE_TEXT)

        ->addColumn('file_author', Varien_Db_Ddl_Table::TYPE_TEXT)

        ->addColumn('schema_version', Varien_Db_Ddl_Table::TYPE_TEXT)

        ->addColumn('data_version', Varien_Db_Ddl_Table::TYPE_TEXT);

    $installer->getConnection()->createTable($catalogTable);
}

$installer->endSetup();