<?php
/** @var Mage_Sales_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$catalogTable = $installer->getTable('catalog_import_log');
$installer->getConnection()->modifyColumn($catalogTable, 'import_date', 'DATETIME');
$installer->getConnection()->modifyColumn($catalogTable, 'file_date', 'DATETIME');
$installer->getConnection()->modifyColumn($catalogTable, 'import_log', 'TEXT');
$installer->getConnection()->modifyColumn($catalogTable, 'import_author', 'TEXT');

$installer->endSetup();