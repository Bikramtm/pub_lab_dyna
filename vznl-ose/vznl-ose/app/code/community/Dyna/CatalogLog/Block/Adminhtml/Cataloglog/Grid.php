<?php

class Dyna_CatalogLog_Block_Adminhtml_Cataloglog_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("cataloglogGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("cataloglog/cataloglog")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn("entity_id", array(
            "header" => Mage::helper("cataloglog")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "entity_id",
        ));

        $this->addColumn('date', array(
            'header' => Mage::helper('cataloglog')->__('Import Date'),
            'index' => 'date',
            'type' => 'datetime',
        ));

        $this->addColumn("log", array(
            "header" => Mage::helper("cataloglog")->__("Import Comment"),
            "index" => "log",
        ));

        $this->addColumn("author", array(
            "header" => Mage::helper("cataloglog")->__("Import Author"),
            "index" => "author",
        ));

        $this->addColumn("file_name", array(
            "header" => Mage::helper("cataloglog")->__("Filename"),
            "index" => "file_name",
        ));

        $this->addColumn("stack", array(
            "header" => Mage::helper("cataloglog")->__("Stack"),
            "index" => "stack",
        ));

        $this->addColumn("interface_version", array(
            "header" => Mage::helper("cataloglog")->__("Interface version"),
            "index" => "interface_version",
        ));

        $this->addColumn('reference_data_build', array(
            'header' => Mage::helper('cataloglog')->__('Reference data build'),
            'index' => 'reference_data_build',
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_cataloglog', array(
            'label' => Mage::helper('cataloglog')->__('Remove Cataloglog'),
            'url' => $this->getUrl('*/adminhtml_cataloglog/massRemove'),
            'confirm' => Mage::helper('cataloglog')->__('Are you sure?')
        ));
        return $this;
    }
}