<?php

$changeData = new Mage_Core_Model_Config();
$changeData->saveConfig('form_validations/form_fields/email_address', "/^[a-zA-Z0-9_.+-äöüÄÖÜß]+@[a-zA-Z0-9-äöüÄÖÜß]+\.[a-zA-Z0-9-.äöüÄÖÜß]+$/", 'default', 0);
