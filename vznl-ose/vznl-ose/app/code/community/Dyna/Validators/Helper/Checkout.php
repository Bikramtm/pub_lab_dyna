<?php

/**
 * Class Dyna_Validators_Helper_Checkout
 */
class Dyna_Validators_Helper_Checkout extends Mage_Core_Helper_Abstract
{
    /**
     * @param $data
     * @param $prefix
     * @param $suffix
     * @param $values
     * @param $errors
     * @param $key
     */
    protected function validateRules($data, $prefix, $suffix, &$values, &$errors, $key)
    {
        foreach ($values['validation'] as $rule) {
            // If another error is already set, don't validate
            if (isset($errors[$prefix . $key . $suffix])) {
                continue;
            }
            if (!Mage::helper('dyna_validators/data')->{$rule}($data[$key])) {
                if (!empty($values['message'][$rule])) {
                    $errors[$prefix . $key . $suffix] = $this->__($values['message'][$rule]);
                } else {
                    $errors[$prefix . $key . $suffix] = $this->__('Field has invalid data');
                }
                continue;
            }
        }
    }


    /**
     * @param array $data
     * @param array $validations
     * @param null /string $prefix
     * @param null /string $suffix
     * @return array
     * @throws Mage_Customer_Exception
     */
    public function _performValidate($data, $validations, $prefix = null, $suffix = null)
    {
        $errors = [];
        foreach ($validations as $key => $values) {
            if ($values['required'] && ((!isset($data[$key])) || (empty($data[$key]) && strlen($data[$key]) == 0))) {
                $errors[$prefix . $key . $suffix] = $this->__('This is a required field.');
                continue;
            }

            if (!$values['required'] && (!isset($data[$key]) || empty($data[$key]))) {
                continue;
            }

            if (isset($values['validation']) && is_array($values['validation'])) {
                $this->validateRules($data, $prefix, $suffix, $values, $errors, $key);
            }
        }

        return $errors;
    }

    /**
     * Returns whether the current quote has a student discount product
     * @return bool
     */
    public function hasStudentDiscount()
    {
        $packages = $this->getQuote()->getPackages();

        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            $items = $package['items'];

            /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
            foreach ($items as $item) {
                if ($item->getSku() == Dyna_Catalog_Model_Type::getMobileStudentDiscount()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns whether the current quote has a young discount product
     * @return bool
     */
    public function hasYoungDiscount()
    {
        $packages = $this->getQuote()->getPackages();

        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            $items = $package['items'];

            /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
            foreach ($items as $item) {
                if ($item->getSku() == Dyna_Catalog_Model_Type::getMobileYoungDiscount()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns whether the current quote has a disabled discount product
     * @return bool
     */
    public function hasDisabledDiscount()
    {
        $packages = $this->getQuote()->getPackages();

        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            $items = $package['items'];

            /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
            foreach ($items as $item) {
                if ($item->getSku() == Dyna_Catalog_Model_Type::getMobileDisabledDiscount()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    /**
     * Validates delivery data
     * @param $data
     * @return array
     */
    public function validateDeliveryData($data)
    {
        $errors = array();

        if (array_key_exists('method', $data)) {
            switch ($data['method']) {
                case 'deliver':
                case 'pickup':
                    break;
                case 'other_address':
                    if ($data['other_address']['gender'] == 3) {
                        $rules = array(
                            'company_name' => array(
                                'required' => true,
                                'validation' => array()
                            ),
                            'co_name' => array(
                                'required' => true,
                                'validation' => array()
                            )
                        );
                        $errors += $this->_performValidate($data['other_address'], $rules, 'delivery[other_address][',']');
                    } else {
                        $rules = array(
                            'gender' => array(
                                'required' => true,
                                'validation' => array()
                            ),
                            'firstname' => array(
                                'required' => true,
                                'validation' => array(
                                    'validateName'
                                )
                            ),
                            'lastname' => array(
                                'required' => true,
                                'validation' => array(
                                    'validateName'
                                )
                            )
                        );
                        $errors += $this->_performValidate($data['other_address'], $rules, 'delivery[other_address][',']');
                    }
                    $rules = array(
                        'postcode' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'street' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                        'city' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                        'houseno' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        )
                    );
                    $errors += $this->_performValidate($data['other_address'], $rules, 'delivery[other_address][', ']');
                    break;
                default:

                    break;
            }
        }

        if (isset($data['date_choice']) && $data['date_choice'] == 2) {
            $rules = array(
                'date' => array(
                    'required' => true,
                    'validation' => array(
                        'validateIsDate'
                    )
                )
            );
            $errors += $this->_performValidate($data, $rules, 'delivery[', ']');
        }

        if (isset($data['same_day']) && $data['same_day'] == 1) {
            $rules = array(
                'same_day_schedule' => array(
                    'required' => true,
                    'validation' => array()
                )
            );
            $errors += $this->_performValidate($data, $rules, 'delivery[', ']');
        }

        return $errors;
    }

    /**
     * Validates porting data for cable packages
     * @param $data
     * @return array
     */
    protected function validateCablePortingData($data)
    {
        $errors = array();
        if (!empty($data['provider']['cable_internet_phone']['change_provider'])
            && $data['provider']['cable_internet_phone']['change_provider'] == Dyna_Checkout_Helper_Fields::CHANGE_PROVIDER_YES) {
            foreach ($data['provider']['cable_internet_phone']['phone_transfer_list']['telephone_prefix'] as $key => $prefix) {
                $rules = array(
                    $key => array(
                        'required' => true,
                        'validation' => array(
                            'validatePhoneNumberPrefix'
                        )
                    )
                );
                $errors += $this->_performValidate($data['provider']['cable_internet_phone']['phone_transfer_list']['telephone_prefix'], $rules, 'provider[cable_internet_phone][phone_transfer_list][telephone_prefix][', ']');
            }

            foreach ($data['provider']['cable_internet_phone']['phone_transfer_list']['telephone'] as $key => $phone) {
                $rules = array(
                    $key => array(
                        'required' => true,
                        'validation' => array(
                            'validatePhoneNumber'
                        )
                    )
                );
                $errors += $this->_performValidate($data['provider']['cable_internet_phone']['phone_transfer_list']['telephone'], $rules, 'provider[cable_internet_phone][phone_transfer_list][telephone][', ']');
            }

            if ((false === empty($data['provider']['cable_internet_phone']['owner_of_terminal'])) && ($data['provider']['cable_internet_phone']['owner_of_terminal'] != 1)) {
                $rules = array(
                    'firstname' => array(
                        'required' => true,
                        'validation' => array(
                            'validateName'
                        )
                    ),
                    'lastname' => array(
                        'required' => true,
                        'validation' => array(
                            'validateName'
                        )
                    )
                );
                $errors += $this->_performValidate($data['provider']['cable_internet_phone']['other_owner'], $rules, 'provider[cable_internet_phone][other_owner][', ']');
            }

            if ($data['provider']['cable_internet_phone']['other_address']['selected'] == 2) {
                $rules = array(
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'street' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'houseno' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array()
                    )
                );
                $errors += $this->_performValidate($data['provider']['cable_internet_phone']['other_address'], $rules, 'provider[cable_internet_phone][other_address][', ']');
            }

            if ($data['provider_notice_date_radio']['cable_internet_phone'] == 1) {
                $rules = array(
                    'notice_date' => array(
                        'required' => true,
                        'validation' => array(
                            'validateIsDate'
                        )
                    )
                );
                $errors += $this->_performValidate($data['provider']['cable_internet_phone'], $rules, 'provider[cable_internet_phone][', ']');
            }

            if (true === empty($data['provider']['cable_internet_phone']['change_provider_later'])) {
                $rules = array(
                    'current_provider_type' => array(
                        'required' => true,
                        'validation' => array()
                    )
                );

                $errors += $this->_performValidate($data['provider']['cable_internet_phone'], $rules, 'provider[cable_internet_phone][', ']');
            }
        }
        return $errors;
    }

    /**
     * Validates porting data for DSL packages
     * @param $data
     * @return array
     */
    protected function validateDSLPortingData($data)
    {
        $errors = array();

        if (isset($data['provider']['dsl']['change_provider']) && $data['provider']['dsl']['change_provider'] == Dyna_Checkout_Helper_Fields::CHANGE_PROVIDER_YES) {
            foreach ($data['provider']['dsl']['phone_transfer_list']['telephone_prefix'] as $key => $prefix) {
                $rules = array(
                    $key => array(
                        'required' => true,
                        'validation' => array(
                            'validatePhoneNumberPrefix'
                        )
                    )
                );
                $errors += $this->_performValidate($data['provider']['dsl']['phone_transfer_list']['telephone_prefix'], $rules, 'provider[dsl][phone_transfer_list][telephone_prefix][', ']');
            }

            foreach ($data['provider']['dsl']['phone_transfer_list']['telephone'] as $key => $phone) {
                $rules = array(
                    $key => array(
                        'required' => true,
                        'validation' => array(
                            'validatePhoneNumber'
                        )
                    )
                );
                $errors += $this->_performValidate($data['provider']['dsl']['phone_transfer_list']['telephone'], $rules, 'provider[dsl][phone_transfer_list][telephone][', ']');
            }

            if (isset($data['provider']['dsl']['owner_of_terminal']) && $data['provider']['dsl']['owner_of_terminal'] != 1) {
                $rules = array(
                    'firstname' => array(
                        'required' => true,
                        'validation' => array(
                            'validateName'
                        )
                    ),
                    'lastname' => array(
                        'required' => true,
                        'validation' => array(
                            'validateName'
                        )
                    )
                );
                $errors += $this->_performValidate($data['provider']['dsl']['other_owner'], $rules, 'provider[dsl][other_owner][', ']');
            }

            if (isset($data['provider']['dsl']['other_address']['selected']) && $data['provider']['dsl']['other_address']['selected'] == 2) {
                $rules = array(
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'street' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'houseno' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array()
                    )
                );
                $errors += $this->_performValidate($data['provider']['dsl']['other_address'], $rules, 'provider[dsl][other_address][', ']');
            }

            $rules = array(
                'phone_socket_no' => array(
                    'required' => false,
                    'validation' => array()
                )
            );

            $errors += $this->_performValidate($data['provider'], $rules, 'provider[', ']');
        } else {
            $rules = array(
                'landlord_firstname' => array(
                    'required' => false,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'landlord_lastname' => array(
                    'required' => false,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'landlord_prefix' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumberPrefix'
                    )
                ),
                'landlord_phone' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumber'
                    )
                ),
                'phone_socket_no' => array(
                    'required' => false,
                    'validation' => array()
                )
            );
            $errors += $this->_performValidate($data['provider'], $rules, 'provider[', ']');
        }

        if (isset($data['provider']['wish_date_option']) && $data['provider']['wish_date_option'] == 1) {
            $rules = array(
                'wish_date' => array(
                    'required' => true,
                    'validation' => array(
                        'validateIsDate'
                    )
                )
            );
            $errors += $this->_performValidate($data['provider'], $rules, 'provider[', ']');
        }

        if ($data['provider']['home_type'] == Dyna_Checkout_Helper_Fields::CHANGE_PROVIDER_MULTI_FAMILY_HOUSE) {
            $rules = array(
                'home_entrance' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'home_floor' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'home_location' => array(
                    'required' => true,
                    'validation' => array()
                )
            );

            $errors += $this->_performValidate($data['provider'], $rules, 'provider[', ']');
        }

        return $errors;
    }

    /**
     * Validates porting data for LTE packages
     * @param $data
     * @return array
     */
    protected function validateLTEPortingData($data)
    {
        $errors = array();

        if ($data['provider']['lte']['change_provider'] == 1) {
            $rules = array(
                'current_provider_type' => array(
                    'required' => true,
                    'validation' => array()
                )
            );

            if ($data['provider']['lte']['change_provider_later'] == 2) {
                $rules['current_provider_type']['required'] = false;
            }

            $errors += $this->_performValidate($data['provider']['lte'], $rules, 'provider[lte][', ']');

            foreach ($data['provider']['lte']['phone_transfer_list']['telephone_prefix'] as $key => $prefix) {
                $rules = array(
                    $key => array(
                        'required' => true,
                        'validation' => array(
                            'validatePhoneNumberPrefix'
                        )
                    )
                );
                $errors += $this->_performValidate($data['provider']['lte']['phone_transfer_list']['telephone_prefix'], $rules, 'provider[lte][phone_transfer_list][telephone_prefix][', ']');
            }

            foreach ($data['provider']['lte']['phone_transfer_list']['telephone'] as $key => $phone) {
                $rules = array(
                    $key => array(
                        'required' => true,
                        'validation' => array(
                            'validatePhoneNumber'
                        )
                    )
                );
                $errors += $this->_performValidate($data['provider']['lte']['phone_transfer_list']['telephone'], $rules, 'provider[lte][phone_transfer_list][telephone][', ']');
            }

            if (isset($data['provider']['lte']['owner_of_terminal']) && $data['provider']['lte']['owner_of_terminal'] != 1) {
                $rules = array(
                    'firstname' => array(
                        'required' => true,
                        'validation' => array(
                            'validateName'
                        )
                    ),
                    'lastname' => array(
                        'required' => true,
                        'validation' => array(
                            'validateName'
                        )
                    )
                );
                $errors += $this->_performValidate($data['provider']['lte']['other_owner'], $rules, 'provider[lte][other_owner][', ']');
            }
            if (isset($data['provider']['lte']['owner_of_terminal']) && $data['provider']['lte']['other_address']['selected'] == 1) {
                $rules = array(
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'street' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'houseno' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array()
                    )
                );
                $errors += $this->_performValidate($data['provider']['lte']['other_address'], $rules, 'provider[lte][other_address][', ']');
            }
        }

        return $errors;
    }

    protected function validateMobileCustomerData($data)
    {
        $errors = array();

        $rules = array(
            'gender' => array(
                'required' => true,
                'validation' => array()
            ),
            'prefix' => array(
                'required' => false,
                'validation' => array()
            ),
            'firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'dob' => array(
                'required' => true,
                'validation' => array(
                    'validateIs18YearsOld'
                ),
                'message' => array(
                    'validateIs18YearsOld' => $this->__('We are afraid that legal requirements demand us you to be at least 18 years old.')
                )
            ),
            'nationality' => array(
                'required' => true,
                'validation' => array()
            ),
            'account_password' => array(
                'required' => true,
            )
        );

        $sale_types = [];
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $checkoutLayout = Mage::helper("dyna_checkout/layout");

        if (true === $checkoutLayout->customerIsKnownInAnyStackFromCart()) {
            unset($rules['account_password']);
        }

        foreach ($quote->getCartPackages(true) as $package) {
            if (!in_array($package['sale_type'], $sale_types)) {
                $sale_types[] = $package['sale_type'];
            }
        }

        if (in_array(Dyna_Catalog_Model_ProcessContext::ACQ, $sale_types)) {
            $rules['id_number']['required'] = true;
            $rules['id_number']['validation'] = array('validateIDNumber');
        } else {
            $rules['id_number']['required'] = false;
            $rules['id_number']['validation'] = array();
        }
        $errors += $this->_performValidate($data['customer'], $rules, 'customer[', ']');

        if (isset($data['customer']['other_identity']['selected']) && $data['customer']['other_identity']['selected'] == 1) {
            $rules = array(
                'card_no' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'exp_date' => array(
                    'required' => true,
                    'validation' => array(
                        'validateExpirationDate'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['other_identity'], $rules, 'customer[other_identity][', ']');
        }

        $rules = array(
            'postcode' => array(
                'required' => true,
                'validation' => array(
                    'validateNumber'
                )
            ),
            'city' => array(
                'required' => true,
                'validation' => array()
            ),
            'street' => array(
                'required' => true,
                'validation' => array()
            ),
            'houseno' => array(
                'required' => true,
                'validation' => array(
                    'validateNumber'
                )
            )
        );
        $errors += $this->_performValidate($data['customer']['address'], $rules, 'customer[address][', ']');

        $rules = array(
            'email' => array(
                'required' => false,
                'validation' => array(
                    'validateEmailSyntax'
                )
            )
        );
        $errors += $this->_performValidate($data['customer']['contact_details'], $rules, 'customer[contact_details][', ']');

        /** Validate phone numbers */
        foreach ($data['customer']['contact_details']['phone_list'] as $key => $value) {
            $rules = array(
                'telephone_prefix' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumberPrefix'
                    )
                ),
                'telephone' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumber'
                    )
                )
            );

            $errors += $this->_performValidate($data['customer']['contact_details']['phone_list'][$key], $rules, 'customer[contact_details][phone_list][' . $key . '][', ']');
        }

        /** Validate contact person (Anschprepartner) */
        if (isset($data['contact_person']) && $data['contact_person'] == 'on') {
            $rules = array(
                'salutation' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'lastname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'firstname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'telephone_prefix' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumberPrefix'
                    )
                ),
                'telephone' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumber'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['contact_person_details'][0], $rules, 'customer[contact_person_details][0][', ']');
        }

        /** Validate order status */
        $rules = array(
            'telephone' => array(
                'required' => false,
                'validation' => array()
            ),
            'email' => array(
                'required' => false,
                'validation' => array(
                    'validateEmailSyntax'
                )
            )
        );

        if (!empty($data['order_status'])) {
            $errors += $this->_performValidate($data['order_status'], $rules, 'order_status[', ']');
        }

        /** Validate billing details */
        if (isset($data['billing']['other_address']['selected']) && $data['billing']['other_address']['selected'] == 2) {
            switch ($data['billing']['other_address']['type']) {
                case 'address':
                    $rules = array(
                        'gender' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                        'postcode' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'city' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                        'street' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                        'houseno' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        )
                    );
                    if (strtoupper($data['customer']['type']) == 'SOHO' && $data['billing']['other_address']['gender'] == 3) {
                        $rules += array(
                            'company_name' => array(
                                'required' => true,
                                'validation' => array()
                            ),
                            'company_additional_name' => array(
                                'required' => true,
                                'validation' => array()
                            )
                        );
                    } else {
                        $rules += array(
                            'firstname' => array(
                                'required' => true,
                                'validation' => array(
                                    'validateName'
                                )
                            ),
                            'lastname' => array(
                                'required' => true,
                                'validation' => array(
                                    'validateName'
                                )
                            )
                        );
                    }
                    $errors += $this->_performValidate($data['billing']['other_address'], $rules, 'billing[other_address][', ']');
                    break;
                case 'pobox':
                    $rules = array(
                        'pobox' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'postcode' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'city' => array(
                            'required' => true,
                            'validation' => array()
                        )
                    );
                    $errors += $this->_performValidate($data['billing']['other_address'], $rules, 'billing[other_address][', ']');
                    break;
                default:
                    break;
            }
        }

        if (isset($data['billing'])) {
            $rules = array(
                'email_address' => array(
                    'required' => false,
                    'validation' => array(
                        'validateEmailSyntax'
                    )
                )
            );
            $errors += $this->_performValidate($data['billing'], $rules, 'billing[', ']');
        }

        return $errors;
    }

    protected function validatePrepaidCustomerData($data)
    {
        $errors = array();

        $rules = array(
            'gender' => array(
                'required' => true,
                'validation' => array()
            ),
            'prefix' => array(
                'required' => false,
                'validation' => array()
            ),
            'firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'dob' => array(
                'required' => true,
                'validation' => array(
                    'validateIs7YearsOld'
                )
            ),
            'id_number' => array(
                'required' => true,
                'validation' => array(
                    'validateIDNumber'
                )
            ),
            'nationality' => array(
                'required' => true,
                'validation' => array()
            )
        );

        $errors += $this->_performValidate($data['customer'], $rules, 'customer[', ']');

        if (isset($data['customer']['other_identity']['selected']) && $data['customer']['other_identity']['selected'] == 1) {
            $rules = array(
                'card_no' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'exp_date' => array(
                    'required' => true,
                    'validation' => array()
                )
            );
            $errors += $this->_performValidate($data['customer']['other_identity'], $rules, 'customer[other_identity][', ']');
        }

        $rules = array(
            'postcode' => array(
                'required' => true,
                'validation' => array(
                    'validateNumber'
                )
            ),
            'city' => array(
                'required' => true,
                'validation' => array()
            ),
            'street' => array(
                'required' => true,
                'validation' => array()
            ),
            'houseno' => array(
                'required' => true,
                'validation' => array(
                    'validateNumber'
                )
            )
        );
        $errors += $this->_performValidate($data['customer']['address'], $rules, 'customer[address][', ']');

        $rules = array(
            'email' => array(
                'required' => false,
                'validation' => array(
                    'validateEmailSyntax'
                )
            )
        );
        $errors += $this->_performValidate($data['customer']['contact_details'], $rules, 'customer[contact_details][', ']');

        /** Validate phone numbers */
        foreach ($data['customer']['contact_details']['phone_list'] as $key => $value) {
            $rules = array(
                'telephone_prefix' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumberPrefix'
                    )
                ),
                'telephone' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumber'
                    )
                )
            );

            $errors += $this->_performValidate($data['customer']['contact_details']['phone_list'][$key], $rules, 'customer[contact_details][phone_list][' . $key . '][', ']');
        }

        /** Validate contact person (Anschprepartner) */
        if (isset($data['contact_person']) && $data['contact_person'] == 'on') {
            $rules = array(
                'salutation' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'lastname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'firstname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'telephone_prefix' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumberPrefix'
                    )
                ),
                'telephone' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumber'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['contact_person_details'][0], $rules, 'customer[contact_person_details][0][', ']');
        }

        /** Validate order status */
        $rules = array(
            'telephone' => array(
                'required' => false,
                'validation' => array()
            ),
            'email' => array(
                'required' => false,
                'validation' => array(
                    'validateEmailSyntax'
                )
            )
        );

        $errors = $this->perFormValidateForOrderStatus($errors, $data, $rules);

        /** Validate billing details */
        if (isset($data['billing']['other_address']['selected']) && $data['billing']['other_address']['selected'] == 2) {
            switch ($data['billing']['other_address']['type']) {
                case 'address':
                    $rules = array(
                        'gender' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                        'firstname' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'lastname' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'postcode' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'city' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                        'street' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                        'houseno' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        )
                    );
                    if (strtoupper($data['customer']['type']) == 'SOHO' && $data['billing']['other_address']['gender'] == 3) {
                        $rules += array(
                            'company_name' => array(
                                'required' => true,
                                'validation' => array()
                            ),
                            'company_additional_name' => array(
                                'required' => true,
                                'validation' => array()
                            )
                        );
                    }
                    $errors += $this->_performValidate($data['billing']['other_address'], $rules, 'billing[other_address][', ']');
                    break;
                case 'pobox':
                    $rules = array(
                        'pobox' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'postcode' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'city' => array(
                            'required' => true,
                            'validation' => array()
                        )
                    );
                    $errors += $this->_performValidate($data['billing']['other_address'], $rules, 'billing[other_address][', ']');
                    break;
                default:
                    break;
            }
        }

        if (isset($data['billing'])) {
            $rules = array(
                'email_address' => array(
                    'required' => false,
                    'validation' => array(
                        'validateEmailSyntax'
                    )
                )
            );
            $errors += $this->_performValidate($data['billing'], $rules, 'billing[', ']');
        }

        return $errors;
    }

    protected function validateFixedCustomerData($data)
    {
        $errors = array();

        if (strtoupper($data['customer']['type']) != 'SOHO') {
            $rules = array(
                'gender' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'prefix' => array(
                    'required' => false,
                    'validation' => array()
                ),
                'firstname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'lastname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'dob' => array(
                    'required' => true,
                    'validation' => array(
                        'validateIs18YearsOld'
                    )
                ),
                'account_password' => array(
                    'required' => true,
                    'validation' => array(
                        'validateCustomerPassword'
                    ),
                    'message' => array(
                        'validateCustomerPassword' => $this->__("Password must be min. 10 characters – alpha & numeric")
                    )
                )
            );
            $errors += $this->_performValidate($data['customer'], $rules, 'customer[', ']');

            $rules = array(
                'postcode' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                ),
                'city' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'street' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'houseno' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['address'], $rules, 'customer[address][', ']');

            $rules = array(
                'email' => array(
                    'required' => false,
                    'validation' => array(
                        'validateEmailSyntax'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['contact_details'], $rules, 'customer[contact_details][', ']');

            /** Validate phone numbers */
            foreach ($data['customer']['contact_details']['phone_list'] as $key => $value) {
                $rules = array(
                    'telephone_prefix' => array(
                        'required' => ($key == 0) ? true : false,
                        'validation' => array(
                            'validatePhoneNumberPrefix'
                        )
                    ),
                    'telephone' => array(
                        'required' => ($key == 0) ? true : false,
                        'validation' => array(
                            'validatePhoneNumber'
                        )
                    )
                );

                $errors += $this->_performValidate($data['customer']['contact_details']['phone_list'][$key], $rules, 'customer[contact_details][phone_list][' . $key . '][', ']');
            }
        } else {
            /** Validate Fixed Soho fields */
            $errors += $this->validateFixedCustomerDataForSOHO($data);
        }

        /** Validate billing details */
        if (isset($data['billing']['other_address']['selected']) && $data['billing']['other_address']['selected'] == 2) {
            $rules = array(
                'gender' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'firstname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'lastname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'postcode' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                ),
                'city' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'street' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'houseno' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                )
            );
            if ($data['billing']['other_address']['gender'] == 3) {
                $rules += array(
                    'company_name' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'company_additional_name' => array(
                        'required' => false,
                        'validation' => array()
                    )
                );

                $rules['firstname'] = array(
                    'required' => false,
                    'validation' => array()
                );

                $rules['lastname'] = array(
                    'required' => false,
                    'validation' => array()
                );
            }

            $errors += $this->_performValidate($data['billing']['other_address'], $rules, 'billing[other_address][', ']');
        }
        return $errors;
    }

    protected function validateFixedCustomerDataForSOHO($data)
    {
        $errors = array();

        if (false === empty($data['customer']['business']['commercial_register'])
            || $data['customer']['business']['commercial_register'] == 2
            || strtolower($data['customer']['business']['commercial_register'] == 'on')
        ) {
            $data['customer']['business']['commercial_register'] = 2;
        } else {
            $data['customer']['business']['commercial_register'] = 1;
        }

        $rules = array(
            'commercial_register' => array(
                'required' => true,
                'validation' => array()
            )
        );
        $errors += $this->_performValidate($data['customer']['business'], $rules, 'customer[business][', ']');

        $rules = array(
            'account_password' => array(
                'required' => true,
                'validation' => array(
                    'validateCustomerPassword'
                ),
                'message' => array(
                    'validateCustomerPassword' => $this->__("Password must be min. 10 characters – alpha & numeric")
                )
            )
        );
        $errors += $this->_performValidate($data['customer'], $rules, 'customer[', ']');

        /** Commercial register = yes */
        if ($data['customer']['business']['commercial_register'] == 2) {
            $rules = array(
                'company_name' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'company_additional_name' => array(
                    'required' => false,
                    'validation' => array()
                ),
                'commercial_register_type' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'company_registration_number' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                ),
                'company_trade_location' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'postcode' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                ),
                'city' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'street' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'houseno' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['address'], $rules, 'customer[address][', ']');

            $rules = array(
                'gender' => array(
                    'required' => false,
                    'validation' => array()
                ),
                'prefix' => array(
                    'required' => false,
                    'validation' => array()
                ),
                'first_name' => array(
                    'required' => false,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'last_name' => array(
                    'required' => false,
                    'validation' => array(
                        'validateName'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['contact_person'], $rules, 'customer[contact_person][', ']');

        } else {
            /** Commercial register = no */
            $rules = array(
                'gender' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'firstname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'lastname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'dob' => array(
                    'required' => true,
                    'validation' => array(
                        'validateIs18YearsOld'
                    )
                )
            );

            $errors += $this->_performValidate($data['customer'], $rules, 'customer[', ']');

            $rules = array(
                'company_name' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'postcode' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                ),
                'city' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'street' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'houseno' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['address'], $rules, 'customer[address][', ']');

            $rules = array(
                'postcode' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                ),
                'city' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'street' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'houseno' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['address'], $rules, 'customer_soho[address][', ']');
        }

        /** Validate contact details */
        $rules = array(
            'email' => array(
                'required' => false,
                'validation' => array(
                    'validateEmailSyntax'
                )
            )
        );
        $errors += $this->_performValidate($data['customer']['contact_details'], $rules, 'customer[contact_details][', ']');

        /** Validate phone numbers */
        foreach ($data['customer']['contact_details']['phone_list'] as $key => $value) {
            $rules = array(
                'telephone_prefix' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumberPrefix'
                    )
                ),
                'telephone' => array(
                    'required' => false,
                    'validation' => array(
                        'validatePhoneNumber'
                    )
                )
            );

            $errors += $this->_performValidate($data['customer']['contact_details']['phone_list'][$key], $rules, 'customer[contact_details][phone_list][' . $key . '][', ']');
        }
        return $errors;
    }

    protected function validateCableCustomerData($data)
    {
        $errors = array();

        if (strtoupper($data['customer']['type']) != 'SOHO') {
            $rules = array(
                'gender' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'prefix' => array(
                    'required' => false,
                    'validation' => array()
                ),
                'firstname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'lastname' => array(
                    'required' => true,
                    'validation' => array(
                        'validateName'
                    )
                ),
                'dob' => array(
                    'required' => true,
                    'validation' => array(
                        'validateIs18YearsOld'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer'], $rules, 'customer[', ']');

            $rules = array(
                'postcode' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                ),
                'city' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'street' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'houseno' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['address'], $rules, 'customer[address][', ']');

            $rules = array(
                'email' => array(
                    'required' => false,
                    'validation' => array(
                        'validateEmailSyntax'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['contact_details'], $rules, 'customer[contact_details][', ']');

            /** For cable, the first phone number is mandatory */
            $rules = array(
                'telephone_prefix' => array(
                    'required' => true,
                    'validation' => array(
                        'validatePhoneNumberPrefix'
                    )
                ),
                'telephone' => array(
                    'required' => true,
                    'validation' => array(
                        'validatePhoneNumber'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['contact_details']['phone_list'][0], $rules, 'customer[contact_details][phone_list][0][', ']');

            /** Validate phone numbers */
            foreach ($data['customer']['contact_details']['phone_list'] as $key => $value) {
                $rules = array(
                    'telephone_prefix' => array(
                        'required' => false,
                        'validation' => array(
                            'validatePhoneNumberPrefix'
                        )
                    ),
                    'telephone' => array(
                        'required' => false,
                        'validation' => array(
                            'validatePhoneNumber'
                        )
                    )
                );

                $errors += $this->_performValidate($data['customer']['contact_details']['phone_list'][$key], $rules, 'customer[contact_details][phone_list][' . $key . '][', ']');
            }
        } else {
            $rules = array(
                'company_name' => array(
                    'required' => true,
                    'validation' => array()
                ),
            );
            $errors += $this->_performValidate($data['customer']['address'], $rules, 'customer[address][', ']');

            $rules = array(
                'postcode' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                ),
                'city' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'street' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'houseno' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer_soho']['address'], $rules, 'customer_soho[address][', ']');

            /** For cable, the first phone number is mandatory */
            $rules = array(
                'telephone_prefix' => array(
                    'required' => true,
                    'validation' => array(
                        'validatePhoneNumberPrefix'
                    )
                ),
                'telephone' => array(
                    'required' => true,
                    'validation' => array(
                        'validatePhoneNumber'
                    )
                )
            );
            $errors += $this->_performValidate($data['customer']['contact_details']['phone_list'][0], $rules, 'customer[contact_details][phone_list][0][', ']');

            /** Validate phone numbers */
            foreach ($data['customer']['contact_details']['phone_list'] as $key => $value) {
                $rules = array(
                    'telephone_prefix' => array(
                        'required' => false,
                        'validation' => array(
                            'validatePhoneNumberPrefix'
                        )
                    ),
                    'telephone' => array(
                        'required' => false,
                        'validation' => array(
                            'validatePhoneNumber'
                        )
                    )
                );

                $errors += $this->_performValidate($data['customer']['contact_details']['phone_list'][$key], $rules, 'customer[contact_details][phone_list][' . $key . '][', ']');
            }
        }

        /** Validate billing details */
        if (isset($data['billing']['other_address']['selected']) && $data['billing']['other_address']['selected'] == 2) {
            $rules = array(
                'gender' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'postcode' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                ),
                'city' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'street' => array(
                    'required' => true,
                    'validation' => array()
                ),
                'houseno' => array(
                    'required' => true,
                    'validation' => array(
                        'validateNumber'
                    )
                )
            );
            if ($data['billing']['other_address']['gender'] == 3) {
                $rules += array(
                    'company_name' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'company_additional_name' => array(
                        'required' => false,
                        'validation' => array()
                    )
                );
            } else {
                $rules += array(
                    'firstname' => array(
                        'required' => true,
                        'validation' => array(
                            'validateName'
                        )
                    ),
                    'lastname' => array(
                        'required' => true,
                        'validation' => array(
                            'validateName'
                        )
                    )
                );

            }

            $errors += $this->_performValidate($data['billing']['other_address'], $rules, 'billing[other_address][', ']');
        }
        return $errors;
    }

    public function validateDeliveryAndPaymentData($postData)
    {
        $errors = array();
        $errors += $this->validateDeliveryData($postData['delivery']);

        return $errors;
    }

    public function validateChangeProviderData($data)
    {
        $errors = array();
        $allPackages = $this->getQuote()->getCartPackages();

        foreach ($allPackages as $package) {
            $packageType = $package->getType();

            if (in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getCablePackages())) {
                $errors += $this->validateCablePortingData($data);
            } elseif (strtolower($packageType) == strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL)) {
                $errors += $this->validateDSLPortingData($data);
            } elseif (strtolower($packageType) == strtolower(Dyna_Catalog_Model_Type::TYPE_LTE)) {
                $errors += $this->validateLTEPortingData($data);
            }
        }

        return $errors;
    }

    public function validateMobileYoubiageData($data)
    {
        $errors = [];
        foreach ($this->getQuote()->getPackages() as $packageId => $package) {
            if ($package['type'] == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) &&
            isset($data['young'][$packageId][$package['type']]['sub_user_dob'])) {
               $rules = array(
                   'sub_user_dob' => array(
                       'required' => true,
                       'validation' => ['validateAgeBetween10And18']
                   )
               );

               $errors += $this->_performValidate($data['young'][$packageId][$package['type']], $rules, 'young['.$packageId.']['.$package['type'].'][', ']');
            }
        }

        return $errors;
    }

    public function validateOtherData($otherData)
    {
        $errors = array();

        $allPackages = $this->getQuote()->getCartPackages();

        foreach ($allPackages as $package) {
            $packageType = $package->getType();
            $packageId = $package->getPackageId();

            if (!empty($otherData['other']['phonebook_choice'][$packageId][$packageType]) && ($otherData['other']['phonebook_choice'][$packageId][$packageType] == 2)) {
                if (!isset($otherData['other']['extended'][$packageId][$packageType]['directories_entry']['electronic']) || $otherData['other']['extended'][$packageId][$packageType]['directories_entry']['electronic'] != 'on') {
                    $rules = array(
                        'printed' => array(
                            'required' => true,
                            'validation' => array()
                        )
                    );
                    $errors = $this->perFormValidationForExtended(
                        $errors,
                        $otherData,
                        $packageId,
                        $packageType,
                        $rules
                    );
                }
                if (!isset($otherData['other']['extended'][$packageId][$packageType]['directories_entry']['printed']) || $otherData['other']['extended'][$packageId][$packageType]['directories_entry']['printed'] != 'on') {
                    $rules = array(
                        'electronic' => array(
                            'required' => true,
                            'validation' => array()
                        )
                    );
                    $errors = $this->perFormValidationForExtended(
                        $errors,
                        $otherData,
                        $packageId,
                        $packageType,
                        $rules
                    );
                }

                if (!in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getCablePackages())) {
                    $rules = array(
                        'firstname' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'lastname' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                    );
                    $errors = $this->perFormValidationForOtherExtended(
                        $errors,
                        $otherData,
                        $packageId,
                        $packageType,
                        $rules
                    );
                    if (in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getFixedPackages())) {
                        $rules = array(
                            'firstname' => array(
                                'required' => true,
                                'validation' => array(
                                    'validateName'
                                )
                            ),
                            'lastname' => array(
                                'required' => true,
                                'validation' => array(
                                    'validateName'
                                )
                            )
                        );
                        $errors = $this->perFormValidationForOtherExtendedCoUsers(
                            $errors,
                            $otherData,
                            $packageId,
                            $packageType,
                            $rules
                        );
                    }
                }
            }
            if (in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getMobilePackages())) {
                if ($this->hasStudentDiscount()) {
                    $rules = array(
                        'matriculation_nr' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                        'institution_location' => array(
                            'required' => true,
                            'validation' => array()
                        )
                    );
                    $errors += $this->_performValidate($otherData['other']['student'], $rules, 'other[student][', ']');
                } elseif ($this->hasDisabledDiscount()) {
                    $rules = array(
                        'disabled_id' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                        'disabled_degree' => array(
                            'required' => true,
                            'validation' => array(
                                'validatePercentage'
                            )
                        )
                    );
                    $errors += $this->_performValidate($otherData['other']['disabled'], $rules, 'other[disabled][', ']');
                }
            }
        }

        return $errors;
    }

    public function validateCustomerData($data)
    {
        $errors = array();
        $allPackages = $this->getQuote()->getCartPackages(true);

        foreach ($allPackages as $package) {
            $packageType = $package->getType();
            if (strtolower($packageType) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
                $errors += $this->validateMobileCustomerData($data);
            } elseif (strtolower($packageType) == strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID)) {
                $errors += $this->validatePrepaidCustomerData($data);
            } elseif (in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getFixedPackages())) {
                $errors += $this->validateFixedCustomerData($data);
            } elseif (in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getCablePackages())) {
                $errors += $this->validateCableCustomerData($data);
            }
        }

        return $errors;
    }

    protected function _performVoicelogValidate( $data, $validations )
    {
        $errors = [];
        foreach ($validations as $key => $values) {
            if ($values['required'] && ((!isset($data[$key])) || (empty($data[$key]) && strlen($data[$key]) == 0))) {
                switch($key) {
                    case 'terms1':
                        $errors[$key] = $this->__('You must agree to the required terms and conditions');
                        break;
                }
                continue;
            }
        }

        return $errors;
    }

    public function validateVoicelogData( $data )
    {
        $errors = array();

        $agent = Mage::getSingleton('dyna_customer/session')->getAgent();
        //The permission controls for which agent the voicelog shall be a required part of the checkout section.
        //Agents without this permission do not have to consider the voicelog questions when submitting an order.
        if ( $agent->isGranted('VOICELOG_REQUIRED') ) {
            $rules = array(
                'terms1' => array(
                    'required' => true
                )
            );

            $checkoutLayout = Mage::helper("dyna_checkout/layout");
            $checkoutLayout->setupFormLayout();

            if ($checkoutLayout->isAvailableSection('payments_monthly')) {
                $rules = array_merge($rules, array(
                    'terms2' => array(
                        'required' => true
                    )
                ));
            }

            if ($checkoutLayout->isAvailableSection('payments_onetime')) {
                $rules = array_merge($rules, array(
                    'terms3' => array(
                        'required' => true
                    )
                ));
            }

            if($checkoutLayout->isDeliveryOptionsSectionAvailable()) {
                $packages = $this->getQuote()->getCartPackages(true);

                $isActivationOrder = false;
                $isMpo = count($packages) > 1 ? true : false;

                foreach ($packages as $package) {
                    if ($package['sale_type'] == Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION) {
                        $isActivationOrder = true;
                        break;
                    }
                }

                if ($isActivationOrder && $isMpo) {
                    $rules = array_merge($rules, array(
                        'terms4' => array(
                            'required' => true
                        )
                    ));
                }
            }

            $errors = $this->_performVoicelogValidate($data['voicelog'], $rules);
        }

        return $errors;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To perform validation for the directories entries
     * @param $errors
     * @param $otherData
     * @param $packageId
     * @param $packageType
     * @param $rules
     * @return array
     */
    protected function perFormValidationForExtended($errors, $otherData, $packageId, $packageType, $rules)
    {
        if (isset($otherData['other']['extended'][$packageId][$packageType]['directories_entry'])) {
            $errors += $this->_performValidate($otherData['other']['extended'][$packageId][$packageType]['directories_entry'], $rules, 'other[extended][directories_entry][', ']');
        }

        return $errors;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To perform validation for the other extended
     * @param $errors
     * @param $otherData
     * @param $packageId
     * @param $packageType
     * @param $rules
     * @return array
     */
    protected function perFormValidationForOtherExtended($errors, $otherData, $packageId, $packageType, $rules)
    {
        if (isset($otherData['other']['extended'][$packageId][$packageType])) {
            $errors += $this->_performValidate($otherData['other']['extended'][$packageId][$packageType], $rules, 'other[extended][', ']');
        }

        return $errors;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To perform validation for the other extended co users
     * @param $errors
     * @param $otherData
     * @param $packageId
     * @param $packageType
     * @param $rules
     * @return array
     */
    protected function perFormValidationForOtherExtendedCoUsers($errors, $otherData, $packageId, $packageType, $rules)
    {
        if (isset($otherData['other']['extended'][$packageId][$packageType]['co_users'])) {
            $errors += $this->_performValidate($otherData['other']['extended'][$packageId][$packageType]['co_users'], $rules, 'other[extended][co_users][', ']');
        }

        return $errors;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To perform validation for the other status
     * @param $errors
     * @param $data
     * @param $rules
     * @return array
     */
    protected function perFormValidateForOrderStatus($errors, $data, $rules)
    {
        if (isset($data['order_status'])) {
            $errors += $this->_performValidate($data['order_status'], $rules, 'order_status[', ']');
        }

        return $errors;
    }
}
