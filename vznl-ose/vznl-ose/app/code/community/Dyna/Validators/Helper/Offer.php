<?php

/**
 * Class Dyna_Validators_Helper_Offer
 */
class Dyna_Validators_Helper_Offer extends Mage_Core_Helper_Data
{
    /** @var  Dyna_Validators_Model_Offer_Interface[] */
    protected $validators = null;

    public function __construct()
    {
        $this->validators = [
            Mage::getModel('dyna_validators/offer_products'),
            Mage::getModel('dyna_validators/offer_openOrders'),
            Mage::getModel('dyna_validators/offer_campaign'),
            Mage::getModel('dyna_validators/offer_duplicatedOffer')
        ];
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @return Dyna_Validators_Model_Offer_ValidationResult[]
     */
    public function validate(Dyna_Checkout_Model_Sales_Quote $quote)
    {
        $validationResults = [];

        foreach ($this->validators as $validator) {
            $validationResults[] = $validator->validate($quote);
        }

        return $validationResults;
    }
}