<?php

/**
 * Class Dyna_Validators_Model_Offer_OpenOrders
 */
class Dyna_Validators_Model_Offer_OpenOrders implements Dyna_Validators_Model_Offer_Interface
{
    protected static $message = "The offer is invalid due to an open order.";
    /** @var Dyna_Customer_Helper_Data */
    protected $customerHelper;

    public function __construct()
    {
        $this->customerHelper = Mage::helper('dyna_customer');
    }

    /**
     * Checks whether the customer has an open order that prevents this
     * offer from being submitted
     *
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @return Dyna_Validators_Model_Offer_ValidationResult
     */
    public function validate(Dyna_Checkout_Model_Sales_Quote $quote): Dyna_Validators_Model_Offer_ValidationResult
    {
        $offerPackageTypes = array_unique($this->getPackageTypes($quote));
        $openOrders = $this->customerHelper->getOSFOpenOrders(null, $offerPackageTypes,null,null);

        // Validate OSF orders
        $isValid = $this->validateOSFOrders($quote->getCartPackages(), $this->getOrderedPackages($openOrders));
        // Validate Non-OSF orders
        $isValid = $isValid && !$this->customerHelper->hasNonOSFOrders($this->getCustomerNumber(), $offerPackageTypes);

        /** @var Dyna_Validators_Model_Offer_ValidationResult $validationResult */
        $validationResult = Mage::getModel('dyna_validators/offer_validationResult');
        $validationResult
            ->setIsValid($isValid)
            ->setShowMessage(true)
            ->setMessage(self::$message);

        return $validationResult;
    }

    /**
     * Get the package types in the saved offer
     *
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @return array
     */
    protected function getPackageTypes(Dyna_Checkout_Model_Sales_Quote $quote)
    {
        $packageTypes = [];
        foreach ($quote->getCartPackages() as $package) {
            $packageTypes[] = strtolower($package->getType());
        }

        return array_unique($packageTypes);
    }

    /**
     * Get the customer number of the logged in customer
     *
     * @return mixed
     */
    protected function getCustomerNumber()
    {
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');

        return $customerSession->getCustomer()->getCustomerNumber();
    }

    /**
     * @param $superOrders
     * @return mixed
     */
    protected function getOrderedPackages($superOrders)
    {
        $orderIds = [];
        foreach ($superOrders as $superOrder) {
            $orderIds[] = $superOrder->getId();
        }

        return Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('order_id', array('in' => $orderIds));
    }

    /**
     * Checks the packages in the offer against the ones in previous open orders
     *
     * @param Dyna_Package_Model_Package[] $offerPackages
     * @param Dyna_Package_Model_Package[] $orderPackages
     *
     * @return bool
     */
    protected function validateOSFOrders($offerPackages, $orderPackages)
    {
        $isValid = true;

        foreach ($offerPackages as $offerPackage) {
            $packageType = strtolower($offerPackage->getType());
            if (in_array($packageType, Dyna_Catalog_Model_Type::getFixedPackages())) {
                foreach ($orderPackages as $orderPackage) {
                    if (in_array(strtolower($orderPackage->getType()), Dyna_Catalog_Model_Type::getFixedPackages())) {
                        $isValid = false;
                        break 2;
                    }
                }
            } elseif (in_array($packageType, Dyna_Catalog_Model_Type::getCablePackages())) {
                foreach ($orderPackages as $orderPackage) {
                    if ($packageType == strtolower($orderPackage->getType())) {
                        $isValid = false;
                        break 2;
                    }
                }
            }
            // no validations exist for mobile packages
        }

        return $isValid;
    }
}