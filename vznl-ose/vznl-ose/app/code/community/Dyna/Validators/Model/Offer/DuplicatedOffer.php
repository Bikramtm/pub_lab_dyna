<?php

/**
 * Class Dyna_Validators_Model_Offer_DuplicatedOffer
 */
class Dyna_Validators_Model_Offer_DuplicatedOffer implements Dyna_Validators_Model_Offer_Interface
{
    /**
     * Checks whether the offer is already loaded in the current session quote
     *
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @return Dyna_Validators_Model_Offer_ValidationResult
     */
    public function validate(Dyna_Checkout_Model_Sales_Quote $quote): Dyna_Validators_Model_Offer_ValidationResult
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $sessionQuote */
        $sessionQuote = Mage::getSingleton('checkout/cart')->getQuote();
        $parentQuoteId = $sessionQuote->getQuoteParentId();

        /** @var Dyna_Validators_Model_Offer_ValidationResult $validationResult */
        $validationResult = Mage::getModel('dyna_validators/offer_validationResult');

        if ($parentQuoteId && $parentQuoteId == $quote->getId()) {
            $validationResult
                ->setIsValid(false);
        } else {
            $validationResult
                ->setIsValid(true);
        }

        return $validationResult;
    }
}