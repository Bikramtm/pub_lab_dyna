<?php

/**
 * Class Dyna_Validators_Model_Offer_Campaign
 */
class Dyna_Validators_Model_Offer_Campaign implements Dyna_Validators_Model_Offer_Interface
{
    protected static $message = "Campaign is invalid.";

    /**
     * Checks whether the campaign that was used to create the offer
     * is still valid in the catalogue
     *
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @return Dyna_Validators_Model_Offer_ValidationResult
     */
    public function validate(Dyna_Checkout_Model_Sales_Quote $quote): Dyna_Validators_Model_Offer_ValidationResult
    {
        $isValid = true;

        if ($campaignCode = $quote->getCampaignCode()) {
            /** @var Dyna_Bundles_Model_Campaign $campaignModel */
            $campaignModel = Mage::getModel('dyna_bundles/campaign');
            if (!($campaignId = $campaignModel::getIdByCampaignCode($campaignCode))
                || !$campaignModel->load($campaignId)->isValid()) {
                $isValid = false;
            }
        }

        /** @var Dyna_Validators_Model_Offer_ValidationResult $validationResult */
        $validationResult = Mage::getModel('dyna_validators/offer_validationResult');

        $validationResult
            ->setMessage(self::$message)
            ->setShowMessage(true)
            ->setIsValid($isValid);

        return $validationResult;
    }
}