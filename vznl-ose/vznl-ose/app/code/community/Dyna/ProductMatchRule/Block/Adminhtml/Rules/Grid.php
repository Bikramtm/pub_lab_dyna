<?php

/**
 * Admin list Customer images grid
 */
class Dyna_ProductMatchRule_Block_Adminhtml_Rules_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $packageTypes = [];

    public function __construct()
    {
        parent::__construct();
        $this->setId('rulesGrid');
        $this->setDefaultSort('product_match_rule_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        /** @var Dyna_ProductMatchRule_Model_Rule $collection */
        $collection = Mage::getModel('productmatchrule/rule')->getCollection();

        $collection->getSelect()
            ->joinLeft(
                array('product_match_rule_website' => 'product_match_rule_website'),
                'product_match_rule_website.rule_id = main_table.product_match_rule_id',
                array('ruleID' => 'product_match_rule_website.rule_id', 'websiteID' => 'product_match_rule_website.website_id')
            )
            ->joinLeft(
                array('core_website' => 'core_website'),
                'core_website.website_id = product_match_rule_website.website_id',
                array('name' => 'core_website.name')
            )
            ->joinLeft(
                array('product_match_rule_process_context' => 'product_match_rule_process_context'),
                'product_match_rule_process_context.rule_id = main_table.product_match_rule_id',
                array('processContextRuleID' => 'product_match_rule_process_context.rule_id', 'processContextID' => 'product_match_rule_process_context.process_context_id')
            )
            ->joinLeft(
                array('process_context' => 'process_context'),
                'process_context.entity_id = product_match_rule_process_context.process_context_id',
                array('name' => 'process_context.code')
            )
            ->group('main_table.product_match_rule_id');

        $this->setCollection($collection);
        if ($this->getCollection()) {
            $this->_preparePage();

            $columnId = $this->getParam($this->getVarNameSort(), $this->_defaultSort);
            $dir = $this->getParam($this->getVarNameDir(), $this->_defaultDir);
            $filter = $this->getParam($this->getVarNameFilter(), null);

            if (is_null($filter)) {
                $filter = $this->_defaultFilter;
            }

            if (is_string($filter)) {
                $data = $this->parseData($filter);
                $this->_setFilterValues($data);
            } else {
                if ($filter && is_array($filter)) {
                    $this->_setFilterValues($filter);
                } else {
                    if (0 !== sizeof($this->_defaultFilter)) {
                        $this->_setFilterValues($this->_defaultFilter);
                    }
                }
            }

            if (isset($this->_columns[$columnId]) && $this->_columns[$columnId]->getIndex()) {
                $dir = (strtolower($dir) == 'desc') ? 'desc' : 'asc';
                $this->_columns[$columnId]->setDir($dir);
                $this->_setCollectionOrder($this->_columns[$columnId]);
            }

            if (!$this->_isExport) {
                $this->getCollection()->load();
                $this->_afterLoadCollection();
            }
        }

        return $this;
    }

    /**
     * Get defined package types for displaying combination rules
     */
    public function getPackageTypes()
    {
        if (!$this->packageTypes) {
            $packageTypesCollection = Mage::getModel("dyna_package/packageType")
                ->getCollection();
            foreach ($packageTypesCollection as $packageType) {
                $this->packageTypes[$packageType->getId()] = $packageType->getPackageCode();
            }
        }

        return $this->packageTypes;
    }

    /**
     * Create the admin grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */

    protected function _prepareColumns()
    {
        /** @var Dyna_ProductMatchRule_Helper_Data $helper */
        $helper = Mage::helper('productmatchrule');

        parent::_prepareColumns();

        if (!$this->_isExport) {
            $this->addColumn('product_match_rule_id', array(
                'header' => $this->_getColumnTitle('ID'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'product_match_rule_id',
            ));
        }

        $this->addColumn('package_type', array(
            'header' => $this->_getColumnTitle('Package type'),
            'align' => 'left',
            'type' => 'options',
            'options' => $this->getPackageTypes(),
            'index' => 'package_type',
        ));

        $this->addColumn('rule_title', array(
            'header' => $this->_getColumnTitle('Rule title'),
            'align' => 'left',
            'index' => 'rule_title',
        ));

        $this->addColumn('left_id', array(
            'header' => $this->_getColumnTitle('Source'),
            'align' => 'left',
            'index' => 'left_id',
            'renderer' => 'Dyna_ProductMatchRule_Block_Adminhtml_Rules_ColumnRenderer',
        ));

        $this->addColumn('operation', array(
            'header' => $this->_getColumnTitle('Operation'),
            'align' => 'left',
            'index' => 'operation',
            'type' => 'options',
            'options' => Mage::helper('productmatchrule')->getOperations()
        ));

        $this->addColumn('operation_value', array(
            'header' => $this->_getColumnTitle('Operation value'),
            'align' => 'left',
            'type' => 'text',
            'index' => 'operation_value',
        ));

        $this->addColumn('right_id', array(
            'header' => $this->_getColumnTitle('Target'),
            'align' => 'left',
            'index' => 'right_id',
            'renderer' => 'Dyna_ProductMatchRule_Block_Adminhtml_Rules_ColumnRenderer',
        ));

        $this->addColumn('operation_type', array(
            'header' => $this->_getColumnTitle('Operation type'),
            'align' => 'left',
            'width' => '30px',
            'index' => 'operation_type',
            'type' => 'options',
            'options' => Mage::helper('productmatchrule')->getOperationTypes()
        ));

        $this->addColumn('process_context', array(
            'header' => $this->_getColumnTitle('Process context'),
            'align' => 'left',
            'type' => 'options',
            'options' => Mage::getModel("dyna_catalog/processContext")->getAllProcessContextIdCodePairs(),
            'index' => 'process_context_id',
            'renderer' => 'Dyna_ProductMatchRule_Block_Adminhtml_Rules_ContextRenderer',
        ));

        $this->addColumn('priority', array(
            'header' => $this->_getColumnTitle('Priority'),
            'align' => 'left',
            'width' => '30px',
            'index' => 'priority',
        ));

        $this->addColumn('source_collection', array(
            'header' => $this->_getColumnTitle('Source collection'),
            'align' => 'left',
            'width' => '30px',
            'index' => 'source_collection',
            'type' => 'options',
            'options' => $helper->getSourceCollections()
        ));

        $this->addColumn('website_id', array(
            'header' => $this->_getColumnTitle('Website'),
            'index' => 'product_match_rule_website.website_id',
            'type' => 'options',
            'options' => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(false),
            'renderer' => 'Dyna_ProductMatchRule_Block_Adminhtml_Rules_WebsiteRenderer',
        ));

        if ($this->_isExport) {
            $this->addColumn('rule_origin', array(
                'header' => $this->_getColumnTitle('Rule origin'),
                'index' => 'rule_origin',
            ));
        }

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));

        return $this;
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('product_match_rule_id');
        $this->getMassactionBlock()->setFormFieldName('rules');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('productmatchrule')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('productmatchrule')->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('lock', array(
            'label' => Mage::helper('productmatchrule')->__('Lock'),
            'url' => $this->getUrl('*/*/massLock'),
            'confirm' => Mage::helper('productmatchrule')->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('unlock', array(
            'label' => Mage::helper('productmatchrule')->__('Unlock'),
            'url' => $this->getUrl('*/*/massUnlock'),
            'confirm' => Mage::helper('productmatchrule')->__('Are you sure?')
        ));

        return $this;
    }

    protected function _getColumnTitle($text)
    {
        if ($this->_isExport) {
            return str_replace(' ', '_', strtolower($text));
        }
        return Mage::helper('productmatchrule')->__($text);
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('product_match_rule_id' => $row->getProductMatchRuleId()));
    }

    /**
     * @param $data
     * @param $column
     */
    protected function processLeft($data, $column)
    {
        $productLeftFilters = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('name')
            ->addFieldToFilter(array(
                array('attribute' => 'sku', 'like' => '%' . $data['left_id'] . '%')
            ));

        $categoryLeftFilters = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('name')
            ->addFieldToFilter(array(
                array(
                    'attribute' => 'name',
                    'like' => '%' . $data['left_id'] . '%'
                )
            ));

        $column->getFilter()->setValue($data['left_id']);
        $left_array = array();
        foreach ($productLeftFilters as $prod) {
            $left_array[] = $prod->getId();
        }

        foreach ($categoryLeftFilters as $prod) {
            $left_array[] = $prod->getId();
        }

        if (!empty($left_array)) {
            $this->getCollection()->addFieldToFilter('left_id', $left_array);
        } else {
            $this->getCollection()->addFieldToFilter('left_id', false);
        }
    }

    /**
     * @param $data
     * @param $column
     */
    protected function processRight($data, $column)
    {
        $productRightFilters = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('name')
            ->addFieldToFilter(array(
                array('attribute' => 'sku', 'like' => '%' . $data['right_id'] . '%')
            ));

        $categoryRightFilters = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('name')
            ->addFieldToFilter(array(
                array('attribute' => 'name', 'like' => '%' . $data['right_id'] . '%')
            ));
        $column->getFilter()->setValue($data['right_id']);
        $right_array = array();
        foreach ($productRightFilters as $prod) {
            $right_array[] = (int)$prod->getId();
        }

        foreach ($categoryRightFilters as $prod) {
            $right_array [] = (int)$prod->getId();
        }

        if (!empty($right_array)) {
            $this->getCollection()->addFieldToFilter('right_id', $right_array);
        } else {
            $this->getCollection()->addFieldToFilter('right_id', false);
        }
    }

    /**
     * @param $filter
     * @return array
     */
    protected function parseData($filter)
    {
        $data = $this->helper('adminhtml')->prepareFilterString($filter);
        if (isset($data['left_id']) || isset($data['right_id'])) {
            foreach ($this->getColumns() as $columnId => $column) {
                if (isset($data['left_id']) && $columnId == 'left_id') {
                    $this->processLeft($data, $column);
                }

                if (isset($data['right_id']) && $columnId == 'right_id') {
                    $this->processRight($data, $column);
                }
            }
        }
        unset($data['left_id']);
        unset($data['right_id']);

        return $data;
    }
}
