<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_ProductMatchRule_Block_Adminhtml_Rules_Edit extends Omnius_ProductMatchRule_Block_Adminhtml_Rules_Edit
{
    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->addJs('mage/adminhtml/rules.js');
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
        $this->getLayout()->getBlock('head')->addJs('omnius/adminhtml/productmatchrule_edit.js');
        $this->getLayout()->getBlock('head')->removeItem('js', 'omnius/productmatchrule_edit.js');

        return parent::_prepareLayout();
    }
}
