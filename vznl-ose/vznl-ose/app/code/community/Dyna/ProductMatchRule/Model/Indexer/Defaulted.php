<?php

/**
 * Class Dyna_ProductMatchRule_Model_Indexer_Defaulted
 */
class Dyna_ProductMatchRule_Model_Indexer_Defaulted extends Omnius_ProductMatchRule_Model_Indexer_Defaulted
{
    protected $args = [];
    protected $packageTypes = [];
    protected $_inserted = 0;
    /** @var Dyna_ProductMatchRule_Model_Indexer_Productmatch */
    protected $whitelistIndexer = false;
    protected $rulesContexts;
    protected $sourceCollectionId = null;

    public function __construct()
    {
        parent::__construct();
        $this->initMemoryLimits();

        $this->_init('omnius_productmatchrule/indexer_defaulted');
    }

    /**
     * Empty index and generate whitelist based on rules
     */
    public function reindexAll()
    {
        $this->productMatchRuleHelper = Mage::helper('productmatchrule');

        // Package types can be sent as params from cli indexing
        if (!empty($this->args)) {
            // Expecting args to contain a list of package type codes based on which we will load their ids
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getModel("dyna_package/packageType");
            foreach ($this->args as $packageCode) {
                $package = $packageTypeModel->loadByCode($packageCode);
                if ($package->getId()) {
                    $this->packageTypes[$package->getPackageCode()] = $package->getId();
                } else {
                    $this->productMatchRuleHelper->logIndexer("[Defaulted] Skipping argument package type: " . $packageCode . " because cannot be loaded (check Admin / Catalog / Packages Configuration / Package Types)");
                }
            }
        } else {
            // Loading all package types an executing indexer consecutive for each rule
            $packageTypeCollection = Mage::getModel("dyna_package/packageType")->getCollection();
            foreach ($packageTypeCollection as $package) {
                $this->packageTypes[$package->getPackageCode()] = $package->getId();
            }
        }

        if (empty($this->packageTypes)) {
            $this->productMatchRuleHelper->logIndexer("[Defaulted] Exiting indexer because there are no package types defined. Please run package types import and execute indexer later.");
            exit;
        }

        $this->_reindexAll();
    }

    /**
     * Get the whitelist indexer model
     * @return Dyna_ProductMatchRule_Model_Indexer_Productmatch
     */
    public function getWhitelistIndexer()
    {
        if ($this->whitelistIndexer === false) {
            $this->whitelistIndexer = Mage::getSingleton('dyna_productmatchrule/indexer_productmatch');
            $this->whitelistIndexer->start();
        }

        return $this->whitelistIndexer;
    }

    /**
     * Clear data per package type if package type is provided as argument
     * @param $packageId
     */
    protected function clearDataPerPackageType($packageId)
    {
        if (!empty($this->args) || (empty($this->args) && !empty($this->website))) {
            $this->productMatchRuleHelper->logIndexer("[Defaulted] Removing indexer entries for package type: " . $packageId . ".");
            $deleteStartTime = microtime(true);
            $conditions = '';
            $deletedRows = 0;

            if (!empty($this->website)) {
                $websiteId = $this->allWebsites[strtolower($this->website)];
                $conditions .= $this->getConnection()->prepareSqlCondition("package_type", $packageId);
                $conditions .= " AND " . $this->getConnection()->prepareSqlCondition("website_id", $websiteId);
            } else {
                $conditions .= $this->getConnection()->prepareSqlCondition("package_type", $packageId);
            }

            $select = "SELECT COUNT(entity_id) FROM $this->_table WHERE :cond";
            $binds = array(
                ':cond' => $conditions
            );
            $noDbEntries = (int)$this->getConnection()->fetchOne($select, $binds);

            if ($noDbEntries > 0) {
                do {
                    $this->_query = $this->getConnection()
                        ->query('DELETE FROM ' . $this->_table . ' WHERE :cond LIMIT :limit',
                            array(
                                ':cond' => $conditions,
                                ':limit' => self::BATCH_SIZE * 100
                            )
                        );
                    $deletedRows += $this->_query->rowCount();
                    $this->closeConnection();
                    $noDbEntries -= self::BATCH_SIZE;
                } while ($noDbEntries >= 0);
            }


            $deleteResultTime = gmdate('H:i:s', microtime(true) - $deleteStartTime);
            if ($deleteResultTime <= '00:01:00') {
                $deleteResultTime = null;
            } else {
                $this->productMatchRuleHelper->logIndexer("Deleted " . $deletedRows . " rows" . (empty($deleteResultTime) ? "." : " in " . $deleteResultTime . "."));
            }
        }
    }

    /**
     * If no parameters were sent to the reindex command, we truncate the entire table
     */
    protected function tryTruncateTable()
    {
        if (empty($this->args) && empty($this->website)) {
            $this->productMatchRuleHelper->logIndexer("[Defaulted] Truncating table " . $this->_table . ".");
            $deleteStartTime = microtime(true);

            $this->getConnection()->query(sprintf('TRUNCATE TABLE `%s`', $this->_table));

            $deleteResultTime = gmdate('H:i:s', microtime(true) - $deleteStartTime);

            if ($deleteResultTime <= '00:01:00') {
                $deleteResultTime = null;
            } else {
                $this->productMatchRuleHelper->logIndexer("Truncated table" . (empty($deleteResultTime) ? "." : " in " . $deleteResultTime . "."));
            }
        }
    }
    /**
     * Reindex all
     */
    protected function _reindexAll()
    {
        $this->tryTruncateTable();

        $this->start();
        $this->getConnection()->query(sprintf('ALTER TABLE %s DISABLE KEYS', $this->_table));

        foreach ($this->packageTypes as $packageCode => $packageId) {
            $this->_inserted = 0;
            $this->productMatchRuleHelper->logIndexer("[Defaulted] Processing indexer entries for package type: " . $packageCode . ".");

            $this->clearDataPerPackageType($packageId);

            $sourceCollections = $this->productMatchRuleHelper->getSourceCollections();
            foreach ($sourceCollections as $sourceCollectionId => $sourceCollectionName) {
                $this->sourceCollectionId = $sourceCollectionId;
                $this->getWhitelistIndexer()->buildNotAllowedCache($packageId, $sourceCollectionId);
                $this->productMatchRuleHelper->logIndexer("[Defaulted] Building indexer entries for source collection: " . $sourceCollectionName . ".");
                $this->buildIndex($packageId, $sourceCollectionId);
            }

            $this->productMatchRuleHelper->logIndexer("Total Inserted rows " . $this->_inserted . " for package type " . $packageCode);
        }

        $this->end();
    }

    /**
     * @param $packageId
     * @param $sourceCollectionId
     */
    protected function buildIndex($packageId, $sourceCollectionId)
    {
        /** @var Omnius_ProductMatchRule_Model_Resource_Rule_Collection $collection */
        $collection = Mage::getResourceModel('dyna_productmatchrule/rule_collection')
            ->addFieldToFilter('operation_type', ['in' => [
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P
            ]])
            ->addFieldToFilter('operation',
                array(
                    array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED),
                    array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED),
                    array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_OBLIGATED)
                )
            )
            ->addFieldToFilter('package_type', ['eq' => $packageId])
            ->addFieldToFilter('source_collection', ['eq' => $sourceCollectionId]);
        $collection->setPageSize(self::BATCH_SIZE);

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();
        /**
         * Process the rules within batches to remove
         * the risk using all the allocated memory
         * only to load the rule collections
         * This way, we never load the whole
         * collection into the current memory
         */
        do {
            $collection->setCurPage($currentPage);
            $collection->load();

            $this->getWhitelistIndexer()->associateRulesToContexts($collection);

            $this->rulesContexts = $this->getWhitelistIndexer()->getRulesContexts();
            foreach ($collection as $rule) {
                $this->processItem($rule);
            }
            $currentPage++;
            $collection->clear();
        } while ($currentPage <= $pages);

        $this->_applyChanges();
    }

    /**
     * Executed after processing the collection
     */
    public function end()
    {
        $this->_applyChanges();
        $this->getConnection()->query(sprintf('ALTER TABLE %s ENABLE KEYS', $this->_table));
        Omnius_ProductMatchRule_Model_Indexer_Abstract::end();
    }

    /**
     * Params setter called from shell/deindexer.php to filter indexing for a certain package type
     * @param array $args
     * @return $this
     */
    public function setParams($args = [])
    {
        $this->args = $args;

        return $this;
    }

    /**
     * @param $rule Dyna_ProductMatchRule_Model_Rule
     */
    protected function processItem($rule)
    {
        $websiteIds = $rule->getWebsiteIds();

        switch ($rule['operation_type']) {
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allProductIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    $this->addDynaConditionalCategoryOpTypeP2P($rule, $websiteId);
                }
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (!in_array($rule['right_id'], $this->_allProductIds)
                    || !in_array($rule['left_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    $this->addDynaConditionalCategoryOpTypeC2P($rule, $websiteId);
                }
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) { break; }
                foreach ($websiteIds as $websiteId) {
                    $this->addConditionalCategoryOpTypeP2C($rule, $websiteId);
                }
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (!in_array($rule['left_id'], $this->_allCategoryIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) { break; }
                foreach ($websiteIds as $websiteId) {
                    $this->addConditionalCategoryOpTypeC2C($rule, $websiteId);
                }
                break;
            default:
                break;
        }
    }

    /**
     * @param $rule
     * @param $websiteId
     */
    protected function addDynaConditionalCategoryOpTypeC2C($rule, $websiteId)
    {
        if (($rule['operation'] == Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED)
            && isset($this->_productCategories[$websiteId][$rule['left_id']])
            && isset($this->_productCategories[$websiteId][$rule['right_id']])
        ) {
            foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$rightProdId) {
                $this->_addDynaConditionalCategory($rule['product_match_rule_id'], $rule['package_type'], $rightProdId, $rule['right_id'], $websiteId);
            }
            unset($rightProdId);
        }
    }

    /**
     * @param $rule
     * @param $websiteId
     */
    protected function addDynaConditionalCategoryOpTypeP2C($rule, $websiteId)
    {
        if (($rule['operation'] == Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED)
            && isset($this->_productCategories[$websiteId][$rule['right_id']])
        ) {
            $this->_addDynaConditionalCategory($rule['product_match_rule_id'], $rule['package_type'], $rule['left_id'], $rule['right_id'], $websiteId);
        }
    }

    /**
     * @param $ruleId
     * @param $packageId
     * @param $left
     * @param $right
     * @param $websiteId
     */
    protected function _addDynaConditionalCategory($ruleId, $packageId, $left, $right, $websiteId)
    {
        $this->_matches[] = array(self::ADD_MANDATORY, array($ruleId, $packageId, $websiteId, $left, $right));
        $this->_assertMemory();
    }

    /**
     * @param $rule
     * @param $websiteId
     */
    protected function addDynaConditionalCategoryOpTypeP2P($rule, $websiteId)
    {
        $left = $rule['left_id'];
        $right = $rule['right_id'];

        if ($rule['operation'] == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED
            || $rule['operation'] == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED
        ) {
            $appendContexts = [];
            $noProcessContext = false;
            if (isset($this->rulesContexts[$rule['product_match_rule_id']])) {
                foreach ($this->rulesContexts[$rule['product_match_rule_id']] as $ruleContext) {
                    if ($this->getWhitelistIndexer()->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], $ruleContext)) {
                        $appendContexts[] = $ruleContext;
                    }
                }
            } else {
                if ($this->getWhitelistIndexer()->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], null)) {
                    $noProcessContext = true;
                }
            }

            if (count($appendContexts) || $noProcessContext) {
                $contexts = $noProcessContext ? null : implode(',', $appendContexts);
                $this->_addDynaConditional($rule['product_match_rule_id'], $rule['package_type'], $left, $right, $websiteId, $rule['operation'], $contexts);
            }
        }
    }

    /**
     * @param $rule
     * @param $websiteId
     */
    protected function addDynaConditionalCategoryOpTypeC2P($rule, $websiteId)
    {
        if (($rule['operation'] == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED
                || $rule['operation'] == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED)
            && isset($this->_productCategories[$websiteId][$rule['left_id']])
        ) {
            foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$catProdId) {
                $left = $catProdId;
                $right = $rule['right_id'];

                $appendContexts = [];
                $noProcessContext = false;
                if (isset($this->rulesContexts[$rule['product_match_rule_id']])) {
                    foreach ($this->rulesContexts[$rule['product_match_rule_id']] as $ruleContext) {
                        if ($this->getWhitelistIndexer()->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], $ruleContext)) {
                            $appendContexts[] = $ruleContext;
                        }
                    }
                } else {
                    if ($this->getWhitelistIndexer()->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], null)) {
                        $noProcessContext = true;
                    }
                }

                if (count($appendContexts) || $noProcessContext) {
                    $contexts = $noProcessContext ? null : implode(',', $appendContexts);
                    $this->_addDynaConditional($rule['product_match_rule_id'], $rule['package_type'], $left, $right, $websiteId, $rule['operation'], $contexts);
                }
            }
            unset($catProdId);
        }
    }

    /**
     * @param $ruleId
     * @param $packageId
     * @param $left
     * @param $right
     * @param $websiteId
     * @param bool $operationId
     */
    protected function _addDynaConditional($ruleId, $packageId, $left, $right, $websiteId, $operationId = false, $processContext = '')
    {
        if ($left != $right) {
            $this->_matches[] = array(self::ADD_ACTION, array($websiteId, $ruleId, $packageId, $left, $right, $operationId, $processContext));
            $this->_assertMemory();
        }
    }

    /**
     * Insert multiple categories
     */
    protected function _insertMultipleCategories()
    {
        $values = '';

        $add = array();

        foreach ($this->_prev as $_key => &$_row) {
            /**
             * $_row [0] = add_action
             * $_row [1][0] = website_id
             * $_row [1][1] = rule_id
             * $_row [1][2] = package_id
             * $_row [1][3] = product_id
             * $_row [1][4] = category_id
             * $_row [1][5] = operation
             * $_row [1][6] = process_context_ids
             *
             */
            $add[$_row[1][0]][] = array($_row[1][3], $_row[1][4], $_row[1][1], $_row[1][6]);
            unset($this->_prev[$_key]);
        }
        unset($_row);

        foreach ($add as $websiteId => &$combinations) {

            foreach ($combinations as $key => &$combination) {
                $values .= sprintf('("%s","%s","%s","%s"),', $websiteId, $combination[0], $combination[1], $combination[3]);

                if (strlen($values) >= $this->_maxPacketsLength) {
                    $sql = sprintf(
                        'INSERT INTO `%s` (`website_id`,`product_id`,`category_id`, `process_context_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                        $this->_mandatoryTable,
                        trim($values, ',')
                    );

                    $this->_query = $this->getConnection()->query($sql);

                    $this->_inserted += $this->_query->rowCount();
                    $this->closeConnection();

                    unset($sql);
                    $values = '';
                }
                unset($combinations[$key]);
            }
            unset($combination);
            unset($add[$websiteId]);
        }
        unset($combinations);
        unset($add);

        if ($values) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`product_id`,`category_id`, `process_context_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                $this->_mandatoryTable,
                trim($values, ',')
            );

            unset($values);
            $this->_query = $this->getConnection()->query($sql);

            $this->_inserted += $this->_query->rowCount();
            $this->closeConnection();
            unset($sql);
        }
    }

    /**
     * Builds INSERT statements and executes them
     * Iterates over the items withing the $_prev array
     * and builds the INSERT statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function _insertMultiple()
    {
        $values = '';

        $add = array();
        foreach ($this->_prev as $_key => &$_row) {
            /**
             * $_row [0] = add_action
             * $_row [1][0] = website_id
             * $_row [1][1] = rule_id
             * $_row [1][2] = package_id
             * $_row [1][3] = product_id
             * $_row [1][4] = category_id
             * $_row [1][5] = operation
             * $_row [1][6] = process_context_ids
             *
             */
            $add[$_row[1][0]][] = array($_row[1][1], $_row[1][2], $_row[1][3], $_row[1][4], $_row[1][5], $_row[1][6]);
            unset($this->_prev[$_key]);
        }
        unset($_row);

        foreach ($add as $websiteId => &$combinations) {
            foreach ($combinations as $key => &$combination) {
                /**
                 * $combination[0 = rule_id]
                 * $combination[1 = package_type]
                 * $combination[2 = source_product_id ]
                 * $combination[3 = target_product_id]
                 * $combination[4 = operation]
                 * $combination[5 = process_context_id]
                 */
                /**
                 * $values[0 = websiteId]
                 * $values[1 = rule_id]
                 * $values[2 = package_type ]
                 * $values[3 = source_product_id]
                 * $values[4 = target_product_id]
                 * $values[5 = operation]
                 * $values[5 = source_collection]
                 */
                $obligated = in_array($combination[4], [Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED, Dyna_ProductMatchRule_Model_Rule::OP_OBLIGATED]) ? 1 : 0;
                $values .= sprintf('("%s","%s","%s","%s","%s", %d, "%s", %d),', $websiteId, $combination[2], $combination[3], $combination[0], $combination[1], $obligated, $combination[5], $this->sourceCollectionId);

                if (strlen($values) >= $this->_maxPacketsLength) {
                    $sql = sprintf(
                        'INSERT INTO `%s` (`website_id`,`source_product_id`,`target_product_id`,`rule_id`,`package_type`, `obligated`, `process_context_id`, `source_collection`) 
                                VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                        $this->_table,
                        trim($values, ',')
                    );
                    $this->_query = $this->getConnection()->query($sql);

                    $this->_inserted += $this->_query->rowCount();
                    $this->closeConnection();
                    unset($sql);
                    $values = '';
                }
                unset($combinations[$key]);
            }
            unset($combination);
            unset($add[$websiteId]);
        }
        unset($combinations);
        unset($add);

        if ($values) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`source_product_id`,`target_product_id`,`rule_id`,`package_type`,`obligated`, `process_context_id`, `source_collection`) 
                        VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                $this->_table,
                trim($values, ',')
            );

            unset($values);
            $this->_query = $this->getConnection()->query($sql);

            $this->_inserted += $this->_query->rowCount();
            $this->closeConnection();
            unset($sql);
        }
    }

    /**
     * Invalidate the product match rules and warn the agent the match rules might be outdated
     *
     * @return $this
     */
    protected function invalidateMatchRuleIndex()
    {
        Mage::getSingleton('index/indexer')
            ->getProcessByCode('product_match_whitelist')
            ->changeStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);

        return $this;
    }
}
