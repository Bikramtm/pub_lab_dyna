<?php

class Dyna_ProductMatchRule_Model_ImporterXML extends Dyna_Import_Model_Adapter_Xml
{
    const ALL_WEBSITES = "*";
    const PARAM_CATEGORY_ID = "categoryID";
    const PARAM_CATEGORY_PATH = "categoryPath";
    const ALL_PROCESS_CONTEXTS = "*";
    const OP_TYPE_PC = [
        Dyna_ProductMatchRule_Helper_Data::OPERATION_TYPE_PRODUCT,
        Dyna_ProductMatchRule_Helper_Data::OPERATION_TYPE_CATEGORY
    ];

    protected $data = [];
    protected $opType;
    protected $operationsTypes = [];
    protected $operationsWithValues = [];
    protected $allowedOperationTypeCombinations = [];
    protected $operations = [];
    protected $line = [];
    protected $websites = [];
    protected $processContexts = [];
    /** @var  array[websiteID[productID, productID]] $websiteProducts */
    protected $websiteProducts = [];
    /** @var  array[websiteID[$categoryID => [productID, productID]]] $websiteCategoryProducts */
    protected $websiteCategoryProducts = [];
    protected $sourceCollection = [];
    protected $sourceType = [];
    protected $targetType = [];
    protected $ruleOrigin = [];
    protected $noImports = 0;
    protected $lineNo = 0;

    // Used for building a package types mapped to package ids stack
    protected $packageTypes = [];

    /** @var Dyna_ProductMatchRule_Helper_Data $helper */
    protected $helper;

    /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
    protected $processContextHelper;

    /**
     * Dyna_PriceRules_Model_Import_SalesRules constructor.
     * @param $args
     */
    public function __construct($args)
    {
        // set log file name
        $this->logFile = "import_match_rules.log";

        /** @var Dyna_ProductMatchRule_Helper_Data $helper */
        $this->helper = Mage::helper('productmatchrule');

        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $this->processContextHelper = Mage::helper("dyna_catalog/processContext");

        parent::__construct($args);
    }

    public function import()
    {
        // retrieve data
        $xmlRows = $this->getXmlData(true, false);

        // set default values
        $this->helper->importHelper = $this->_helper;
        $this->setDefaultOperationTypes();
        $this->operationsWithValues = $this->helper->getOperationsWithValues();
        $this->setDefaultOperations();
        $this->allowedOperationTypeCombinations = $this->helper->getAllowedOperationTypeCombinations();
        $this->setDefaultProcessContexts();
        $this->setDefaultSourceCollections();
        $this->setDefaultSourceType();
        $this->setDefaultTargetType();
        $this->setDefaultRuleOrigin();
        $this->setAllDefaultWebsites();

        foreach ($this->websites as $websiteId => $websiteCode) {
            /** @var Dyna_Catalog_Model_Category $categoryModel */
            $categoryModel = Mage::getModel('catalog/category');
            $this->websiteProducts[$websiteId] = $categoryModel->getWebsitesProductsId($websiteId);

            /** @var Dyna_ProductMatchRule_Model_Rule $productMatchRuleModel */
            $productMatchRuleModel = Mage::getModel('productmatchrule/rule');
            $productCategories = $productMatchRuleModel->getAllCategoryProducts($websiteId, true);
            $this->websiteCategoryProducts[$websiteId] = $productCategories;
            unset($productCategories);
        }

        $this->helper->setWebsiteProducts($this->websiteProducts);
        $this->helper->setWebsiteCategoryProducts($this->websiteCategoryProducts);

        // parse rules
        foreach ($xmlRows->CompatibilityRule as $compatibilityRule) {
            // set current rule
            $this->line = $compatibilityRule;
            $this->lineNo++;

            // clear old data
            $this->data = [];
            $this->data['stack'] = $this->stack;
            $this->opType = [];
            $foundLeftIdOrSourceExpressionCollection = false;
            $foundRightIdOrTargetExpressionCollection = false;

            // get the rule title; trim it
            if (isset($this->line->Title)) {
                $this->data['rule_title'] = trim((string)$this->line->Title);
            }

            // determine the id of the operation type based on the string(PRODUCT-PRODUCT)
            if (isset($this->line->OperationType) && !$this->setOperationType()) {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] operation type cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping element no. ' . $this->lineNo);
                continue;
            }

            // determine the operation
            if (isset($this->line->Operation)) {
                // determine the id of the operation based on the string (allowed, min)
                if (!$this->setOperation()) {
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] operation cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping element no. ' . $this->lineNo);
                    continue;
                }

                if (!$this->allowedOperationTypeCombination()) {
                    $this->_logError('Operation "' . $this->operations[$this->data['operation']] . '" cannot have type "' . $this->operationsTypes[$this->data['operation_type']] . '". Skipping row no ' . $this->_rowNumber);
                    $this->_skippedFileRows++;
                    return false;
                }

                // determine the the operation value for operations min, max, equal
                if (!$this->setOperationValue()) {
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] Skipping element no. ' . $this->lineNo);
                    continue;
                }
            }

            // determine the override_initial_selectable
            if (in_array($this->data['operation'], $this->helper->getOperationsWithOverrideInitialSelectable())) {
                $this->setOverrideInitialSelectable();
            }

            // determine the service_source_expression
            if ((string)$this->line->SourceType == Dyna_ProductMatchRule_Helper_Data::SOURCE_TYPE_SERVICE) {
                try {
                    /** @var Dyna_Import_Model_ExpressionLanguage_Source_Conditions $sourceCondition */
                    $sourceCondition = Mage::getModel('dyna_import/expressionLanguage_source_conditions');
                    $this->searchAndTransformCategoryIdToPath($compatibilityRule->SourceConditions, 'Source');
                    $sourceCondition->parseConditions($compatibilityRule->SourceConditions, 'Source');

                    $this->data['service_source_expression'] = $sourceCondition->asString();
                } catch (Exception $exception) {
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] Invalid service source expression: ' . $exception->getMessage() . ' ( rule tile = ' . $this->data['rule_title'] . ') Skipping element no. ' . $this->lineNo);
                    continue;
                }

                // validate service source expression
                if ($this->data['service_source_expression']) {
                    //validate the service source expression
                    if (!$this->validateServiceExpression($this->data['service_source_expression'])) {
                        $this->_skippedFileRows++;
                        $this->_logError('[ERROR] service_source_expression is not valid ( rule tile = ' . $this->data['rule_title'] . ') Skipping element no. ' . $this->lineNo);
                        continue;
                    }

                    $foundLeftIdOrSourceExpressionCollection = true;
                } else {
                    // the service source expression is mandatory if the source type is service OMNVFDE-408
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] service_source_expression cannot be determined and is mandatory when the source type is Service( rule tile = ' . $this->data['rule_title'] . ') Skipping element no. ' . $this->lineNo);
                    continue;
                }
            }

            // if operation Target is Service, set the service target expression
            if ($this->opType[1] == Dyna_ProductMatchRule_Helper_Data::OPERATION_TYPE_SERVICE) {
                try {
                    /** @var Dyna_Import_Model_ExpressionLanguage_Source_Conditions $targetCondition */
                    $targetCondition = Mage::getModel('dyna_import/expressionLanguage_source_conditions');
                    $this->searchAndTransformCategoryIdToPath($compatibilityRule->TargetConditions, 'Target');
                    $targetCondition->parseConditions($compatibilityRule->TargetConditions, 'Target');

                    $this->data['service_target_expression'] = $targetCondition->asString();
                } catch (Exception $exception) {
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] Invalid service target expression: ' . $exception->getMessage() . ' ( rule tile = ' . $this->data['rule_title'] . ') Skipping element no. ' . $this->lineNo);
                    continue;
                }

                // validate target source expression
                if ($this->data['service_target_expression']) {
                    //validate the target source expression
                    if (!$this->validateServiceExpression($this->data['service_target_expression'])) {
                        $this->_skippedFileRows++;
                        $this->_logError('[ERROR] service_target_expression is not valid ( rule tile = ' . $this->data['rule_title'] . ') Skipping element no. ' . $this->lineNo);
                        continue;
                    }

                    $foundRightIdOrTargetExpressionCollection = true;
                } else {
                    // the service target expression is mandatory if the operation type is service-service
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] service_target_expression cannot be determined and is mandatory when the operation type is *-Service( rule tile = ' . $this->data['rule_title'] . ') Skipping element no. ' . $this->lineNo);
                    continue;
                }
            }

            // Determine left_id(product selection)
            if (!$foundLeftIdOrSourceExpressionCollection && $this->setLeftId()) {
                $foundLeftIdOrSourceExpressionCollection = true;
            }

            // determine the source_type; must be a string that can be associated with an id or a valid id; else error
            if (!$this->setSourceType()) {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] source type cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping element no. ' . $this->lineNo);
                continue;
            }

            // determine the target_type; must be a string that can be associated with an id or a valid id; else error
            if (!$this->setTargetType()) {
                // @todo uncomment these lines(skip element) after XSD and import file is updated to include TargetType
                // $this->_logError('[ERROR] target type cannot be determined for rule title: '. $this->data['rule_title'] . '. Skipping element no. ' . $this->lineNo);
                // $this->_skippedFileRows++;
                // continue;
            }

            // validate if the operation_type has the required values (service source or left_id)
            if (!$this->validateRequiredLeftIdOrServiceSourceExpression($foundLeftIdOrSourceExpressionCollection)) {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] Skipping element no. ' . $this->lineNo);
                continue;
            }

            // determine the the right id value for the given sku and op type
            if (in_array($this->opType[1], self::OP_TYPE_PC) &&
                !$foundRightIdOrTargetExpressionCollection && !$this->setRightId()) {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] Skipping element no. ' . $this->lineNo);
                continue;
            }

            // determine the rule description
            if (isset($this->line->RuleDescription)) {
                $this->data['rule_description'] = trim((string)$this->line->RuleDescription);
            }

            // determine the priority
            if (isset($this->line->Priority)) {
                if (!$this->setPriority()) {
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] Skipping element no. ' . $this->lineNo);
                    continue;
                }
            }

            // determine if is locked or not; this is not a required field;
            // it will be locked only when the value is locked or 1, else it will be unlocked
            if (isset($this->line->Locked)) {
                $this->setIsLocked();
            }

            // determine the website ids (if is provided a code it must be mapped to a possible id;
            // if is an id it must be a valid one, else -> error)
            if (isset($this->line->Channels)) {
                if (!$this->setWebsiteIds()) {
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] Skipping element no. ' . $this->lineNo);
                    continue;
                }
            } else {
                foreach (Mage::app()->getWebsites() as $website) {
                    /** @var $website Mage_Core_Model_Website */
                    $this->data['website_id'][] = $website->getId();
                }
            }

            // determine the rule origin, if is set we will use that value, else 1 = import
            if (isset($this->line->RuleOrigin)) {
                $this->setRuleOrigin();
            } else {
                $this->_log('rule_origin was set to the default value for import 1', true);
                $this->data['rule_origin'] = 1;
            }

            // determine the process context. if is a string it must be a valid string that can be mapped to an id, if is an id it must be a valid one, else -> error)
            if (isset($this->line->ProcessContexts)) {
                if (!$this->setProcessContextId()) {
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] Skipping element no. ' . $this->lineNo);
                    continue;
                }
            }

            // determine the effective date, if it's a string it must be a valid date string, else null is inserted
            if (isset($this->line->EffectiveDate)) {
                $this->setEffectiveDate();
            }

            // determine the expiration date, if it's a string it must be a valid date string, else null is inserted
            if (isset($this->line->ExpirationDate)) {
                $this->setExpirationDate();
            }

            // determine to which package type this rule belongs
            if (isset($this->line->PackageType)) {
                $this->line->PackageTypeId = (string)$this->line->PackageType;
            }

            if (isset($this->line->PackageTypeId)) {
                try {
                    $this->setPackageType();
                } catch (Exception $e) {
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR]' . $e->getMessage());
                    $this->_logError('[ERROR] Trying to import a rule with an invalid package type (' .
                        $this->position['package_type_id'] . '): ' . $this->data['rule_title'] .
                        '. Skipping element no. ' . $this->lineNo);
                    continue;
                }
            }

            // determine the source_collection; must be a string that can be associated with an id or a valid id; else error
            // the source collection is mandatory only if source type is product selection OMNVFDE-408
            if (isset($this->line->SourceCollection)) {
                if ((
                        !$this->setSourceCollection() ||
                        (isset($this->data['source_collection']) && $this->data['source_collection'] == 0) ||
                        !isset($this->data['left_id'])
                    ) &&
                    $this->data['source_type'] == 0) {
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] source_collection and left_id are required for Product source type
                        ( rule tile = ' . $this->data['rule_title'] . ') Skipping element no. ' . $this->lineNo);
                    continue;
                }
            }

            // set the left_id as a condition
            if (isset($this->data['sourceSku'])) {
                $this->data['conditions'] = [
                    '1' => [
                        'type' => 'catalogrule/rule_condition_combine',
                        'aggregator' => 'all',
                        'value' => '1',
                        'new_child' => '',
                    ],
                    '1--2' => [
                        'type' => 'catalogrule/rule_condition_product',
                        'attribute' => 'sku',
                        'operator' => '==',
                        'value' => $this->data['sourceSku']
                    ]
                ];
            }

            if (isset($this->data['sourceCategoryId'])) {
                $this->data['conditions'] = [
                    '1' => [
                        'type' => 'catalogrule/rule_condition_combine',
                        'aggregator' => 'all',
                        'value' => '1',
                        'new_child' => '',
                    ],
                    '1--2' => [
                        'type' => 'catalogrule/rule_condition_product',
                        'attribute' => 'category_ids',
                        'operator' => '==',
                        'value' => $this->data['sourceCategoryId']
                    ]
                ];
            }

            // Parse POST data to Dyna_ProductMatchRule_Model_MatchRule model
            $response = $this->helper->setRuleData($this->data, true);

            if (!is_array($response)) {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] ( rule tile = ' . $this->data['rule_title'] . ')' . $response);
                $this->_logError('[ERROR] Skipping element no. ' . $this->lineNo);
                // Error occurred
                continue;
            }

            try {
                foreach ($response as $model) {
                    $model->save();
                    $model->setWebsiteIds($this->data['website_id']);
                    $model->setProcessContextsIds($this->data['process_context']);
                }
            } catch (Exception $e) {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] Exception ( rule tile = ' . $this->data['rule_title'] . ')' . $e->getMessage());
                $this->_logError('[ERROR] Skipping element no. ' . $this->lineNo);
                fwrite(STDERR, '[ERROR] Exception ( rule tile = ' . $this->data['rule_title'] . ')' . $e->getMessage());
                // Error occurred
                continue;
            }

            $this->noImports++;
            $this->_log('Imported row ' . print_r($this->line, true), true);
            unset($this->line);

        }

        $this->_totalFileRows = $this->lineNo;

        // also invalidate indexers
        foreach (array('product_match_whitelist', 'product_match_defaulted') as $indexName) {
            $indexer = Mage::getSingleton('index/indexer')->getProcessByCode($indexName);
            if ($indexer) {
                $indexer->setStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX)->save();
            }
        }
    }

    /**
     * Set the source_collection id
     * if it is a string it must be mapped to one of the possible strings for source_collection (it string must be contained in one of the default source_collection) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for source_collection
     * else -> skip row, error
     * @return bool
     */
    protected function setSourceCollection()
    {
        if ($this->data['source_type'] == 0) {
            $valueToLookUp = strtolower(trim((string)$this->line->SourceCollection));

            return $this->lookupInKeyOrValueOfArray(
                $valueToLookUp,
                'source_collection',
                $this->sourceCollection,
                true
            );
        } else {
            return true;
        }
    }

    /**
     * Set the priority
     * If the priority is not determined => skip row, error
     *
     * @return bool
     */
    protected function setPriority()
    {
        $this->data['priority'] = (int)$this->line->Priority;
        if (!isset($this->data['priority'])) {
            $this->_logError('[ERROR] The priority is not present/valid (rule title = ' . $this->data['rule_title'] . '). Skipping element.');

            return false;
        }

        return true;
    }

    /**
     * Set effective date
     */
    protected function setEffectiveDate()
    {
        $lockedData = trim((string)$this->line->EffectiveDate);
        if (!empty($lockedData)) {
            $date = new DateTime($lockedData);
            $date = $date->format('Y-m-d');
            $errors = DateTime::getLastErrors();

            if (!empty($errors['warning_count'])) {
                $this->_logError('[ERROR] Invalid format for effective_date ' . $lockedData);
            } else {
                $this->data['effective_date'] = $date;
            }
        } else {
            $this->data['effective_date'] = null;
        }
    }

    /**
     * Set expiration date
     */
    protected function setExpirationDate()
    {
        $lockedData = trim((string)$this->line->ExpirationDate);
        if (!empty($lockedData)) {
            $date = new DateTime($lockedData);
            $date = $date->format('Y-m-d');
            $errors = DateTime::getLastErrors();

            if (!empty($errors['warning_count'])) {
                $this->_logError('Invalid format for expiration_date ' . $lockedData);
            } else {
                $this->data['expiration_date'] = $date;
            }
        } else {
            $this->data['expiration_date'] = null;
        }
    }

    /**
     * Set operation type
     * If the type cannot be mapped to an ID (with the given name or the given id) => error, skip row
     *
     * @return bool
     */
    protected function setOperationType()
    {
        $this->opType = explode('-', trim(strtoupper((string)$this->line->OperationType)));
        $valueToLookUp = strtolower(trim((string)$this->line->OperationType));

        return $this->lookupInKeyOrValueOfArray(
            $valueToLookUp,
            'operation_type',
            $this->operationsTypes,
            true
        );
    }

    /**
     * Set operation
     * If the operation cannot be mapped to an ID => error, skip row
     *
     * @return bool
     */
    protected function setOperation()
    {
        // replace the value of the operation if is a min, max, eq operation with (n) in order to identify the operation
        $operation = strtolower(preg_replace('#\(.*?\)#s', '(n)', trim((string)$this->line->Operation)));

        return $this->lookupInKeyOrValueOfArray($operation, 'operation', $this->operations, true);
    }

    /**
     * Check Operation X OperationType combinations
     * For example defaulted* rules can't have target as SERVICE
     * Skip row if combination is not supported
     *
     * @return bool
     */
    protected function allowedOperationTypeCombination() {
        return isset($this->allowedOperationTypeCombinations[$this->operations[$this->data['operation']]][$this->data['operation_type']]);
    }

    /**
     * Set the operation value
     * The operation value is required and must be numeric for operations: min, max, equal;
     * If the operation is min,max, eqal and the operation value is not numeric or is not present => skip row, error
     * @return bool
     */
    protected function setOperationValue()
    {
        // determine also the operation value if the operation is min, max, equal
        if (in_array($this->data['operation'], $this->operationsWithValues)) {
            // the operation value is provided between parentheses; if in parentheses is a number that number is the value; else it will be 0
            preg_match('#\((.*?)\)#', (string)$this->line->Operation, $match);
            // error skip row
            if (!is_numeric($match[1])) {
                $this->_logError('[ERROR] the operation_value is not valid (rule title = ' . $this->data['rule_title'] . '). skipping row');

                return false;
            }
            $this->data['operation_value'] = $match[1];
        }

        return true;
    }

    /**
     * For a given value try the following match:
     * - if the given value is numeric try to match it with an ID - if is not possible error, skip row; if is found the ID is retained to be used for the import
     * - if the given value is not numeric try to match it with the value of an attribute (as it appear on the front end);
     * here we will 2 types of matches: exact name or if the name is contained in one of the attributes values
     * (for example for process_context in the csv can be the value '*' that will be matched with '* (all)')
     * if a match is found (exact or partial, depending on the parameter $exactLookup) than the ID is retained to be used for the import
     *
     * @param string $valueToLookUp (this is the value that we are trying to match)
     * @param string $keyValueToLookUp (this is the name of the key that will help us for the logs)
     * @param $arrayWhereToLookUp (this is the array with possible values as ID->name where we will try to find the  $valueToLookUp)
     * @param bool $exactLookup (this specifies if we should do a strpos or an exact match of string for the name)
     * @param bool $logMessage
     * @return bool
     */
    protected function lookupInKeyOrValueOfArray(
        $valueToLookUp,
        $keyValueToLookUp,
        $arrayWhereToLookUp,
        $exactLookup = true,
        $logMessage = true
    ) {
        if (is_numeric($valueToLookUp)) {
            if (array_key_exists($valueToLookUp, $arrayWhereToLookUp)) {
                $this->data[$keyValueToLookUp] = $valueToLookUp;
                return true;
            } else {
                if ($logMessage) {
                    $this->_logError('[ERROR] the ' . $keyValueToLookUp . ' =  ' . $valueToLookUp . ' is not valid (rule title = ' . $this->data['rule_title'] . '). skipping row');
                }

                return false;
            }
        }

        foreach ($arrayWhereToLookUp as $key => $arrayValueWhereToLookUp) {
            if ($exactLookup) {
                if ($arrayValueWhereToLookUp == $valueToLookUp) {
                    $this->data[$keyValueToLookUp] = $key;
                    return true;
                }

            } else {
                if (strpos($arrayValueWhereToLookUp, $valueToLookUp) !== false) {
                    $this->data[$keyValueToLookUp] = $key;
                    return true;
                }
            }
        }

        if ($logMessage) {
            $this->_logError('[ERROR] the ' . $keyValueToLookUp . ' =  ' . $valueToLookUp . ' is not valid (rule title = ' . $this->data['rule_title'] . ').  skipping row');
            $this->_log(var_export($arrayWhereToLookUp, true));
        }

        return false;
    }

    /**
     * Set the default operation types [id->name(strtolower)]
     */
    protected function setDefaultOperationTypes()
    {
        if (!$this->operationsTypes) {
            $defaultOperationTypes = $this->helper->getOperationTypes();
            foreach ($defaultOperationTypes as $id => $name) {
                $this->operationsTypes[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the default operation [id->name(strtolower)]
     */
    protected function setDefaultOperations()
    {
        if (!$this->operations) {
            $defaultOperations = $this->helper->getOperations();
            foreach ($defaultOperations as $id => $name) {
                $this->operations[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the default process contexts [id->code]
     */
    protected function setDefaultProcessContexts()
    {
        if (!$this->processContexts) {
            $this->processContexts = $this->processContextHelper->getProcessContextsByCodes();
        }
    }

    /**
     * Set the default sourceCollections [id->name(strtolower)]
     */
    protected function setDefaultSourceCollections()
    {
        if (!$this->sourceCollection) {
            $defaultSourceCollections = $this->helper->getSourceCollections();
            foreach ($defaultSourceCollections as $id => $name) {
                $this->sourceCollection[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the default sourceTypes [id->name(strtolower)]
     */
    protected function setDefaultSourceType()
    {
        if (!$this->sourceType) {
            $defaultSourceTypes = $this->helper->getSourceTypes();
            foreach ($defaultSourceTypes as $id => $name) {
                $this->sourceType[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the default targetTypes [id->name(strtolower)]
     */
    protected function setDefaultTargetType()
    {
        if (!$this->targetType) {
            $this->targetType[] = "";
            $defaultTargetTypes = $this->helper->getTargetTypes();
            foreach ($defaultTargetTypes as $id => $name) {
                $this->targetType[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the default rule_origin [id->name(strtolower)]
     */
    protected function setDefaultRuleOrigin()
    {
        if (!$this->ruleOrigin) {
            $defaultRuleOrigin = $this->helper->getRuleOrigin();
            foreach ($defaultRuleOrigin as $id => $name) {
                $this->ruleOrigin[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the websites that are available as an array of [websiteID => websiteCode]
     */
    protected function setAllDefaultWebsites()
    {
        if (!$this->websites) {
            $presentWebsites = Mage::app()->getWebsites();

            /** @var Mage_Core_Model_Website $website */
            foreach ($presentWebsites as $website) {
                $this->websites[$website->getId()] = strtolower($website->getCode());
            }
        }
    }

    /**
     * Set override initial selectable value for Allowed rules
     *
     * @return bool
     */
    protected function setOverrideInitialSelectable()
    {
        $this->data['override_initial_selectable'] =
            (isset($this->line->Operation->attributes()->overrideinitialselectable) && $this->line->Operation->attributes()->overrideinitialselectable == "true") ? 1 : 0;

        return true;
    }

    /**
     * @param $type
     * @param $value
     * @param $attributeName
     * @return null
     */
    protected function _getEntity($type, $value, $attributeName = false)
    {
        switch (strtoupper($type)) {
            case Dyna_ProductMatchRule_Helper_Data::OPERATION_TYPE_PRODUCT:
                /**@var $productModel Dyna_Catalog_Model_Product */
                $productModel = Mage::getModel("catalog/product");
                return $productModel->getIdBySku($value);  //@todo - change this
            case Dyna_ProductMatchRule_Helper_Data::OPERATION_TYPE_CATEGORY:
                $categoryModel = Mage::getModel('dyna_catalog/category');
                /** @var Dyna_Catalog_Model_Category $category */
                if ($attributeName == self::PARAM_CATEGORY_PATH) {
                    $category = $categoryModel
                        ->getCategoryByNamePath(str_replace('->', '/', $value), '/');
                } else {
                    $category = $categoryModel
                        ->getCollection()
                        ->addFieldToFilter('unique_id', ['eq' => $value])
                        ->getFirstItem();
                }

                if ($category) {
                    return $category->getId();
                }
                return false;
            default:
                // Unknown entity
                break;
        }

        return false;
    }

    protected function getChildCount($elements)
    {
        $counter = 0;
        foreach ($elements as $element) {
            $counter++;
        }
        return $counter;
    }

    /**
     * Set the left id based on the operation type and sku
     *
     * @return bool
     */
    protected function setLeftId()
    {
        $serviceArray = array('Service-Product', 'Service-Category', 'Service-Service');
        $elementSourceType = (string)$this->line->SourceType;

        if ($elementSourceType != Dyna_ProductMatchRule_Helper_Data::SOURCE_TYPE_SERVICE &&
            in_array((string)$this->line->OperationType, $serviceArray)) {
            $this->_logError('[ERROR] "' . $this->data['rule_title'] . '" rule has invalid SourceType ' . (string)$this->line->SourceType . ' for Operation Type ' . (string)$this->line->OperationType);
            return false;
        }

        if ($elementSourceType != Dyna_ProductMatchRule_Helper_Data::SOURCE_TYPE_SERVICE &&
            $this->getChildCount($this->line->SourceConditions->SourceCondition) > 1) {
            $this->_logError('[ERROR] "' . $this->data['rule_title'] . '" rule with Operation Type . ' . (string)$this->line->OperationType . ' can only have one SourceCondition. Multiple found.');
            return false;
        }

        $attributeName = $this->getAttributeName($this->line->SourceConditions->SourceCondition->Params->Param);
        $sourceConditionValue = trim((string)$this->line->SourceConditions->SourceCondition->Params->Param);

        if ($this->getChildCount($this->line->SourceConditions->SourceCondition->Params->Param) == 1) {
            $this->data['left_id'] = $this->_getEntity($this->opType[0], $sourceConditionValue, $attributeName);
        } else {
            $this->_logError('[ERROR] Only one Param is expected in SourceCondition for rule: ' . $this->data['rule_title'] . '. Multiple found.');
            return false;
        }

        // determine the source sku or source category id that will be used to create the source condition
        switch ($this->opType[0]) {
            case Dyna_ProductMatchRule_Helper_Data::OPERATION_TYPE_PRODUCT:
                $this->data['sourceSku'] = $sourceConditionValue;
                break;
            case Dyna_ProductMatchRule_Helper_Data::OPERATION_TYPE_CATEGORY:
                $this->data['sourceCategoryId'] = $this->data['left_id'];
                break;
        }

        return true;
    }

    /**
     * Set the right id based on the operation type and sku
     * If it cannot be determined => error, skip row
     *
     * @return bool
     */
    protected function setRightId()
    {
        if ($this->getChildCount($this->line->TargetConditions->TargetCondition->Params->Param) > 1) {
            $this->_logError('[ERROR] Invalid target - Expected only one Param in TargetCondition for rule title: ' . $this->data['rule_title']);
            return false;
        }

        $attributeName = $this->getAttributeName($this->line->TargetConditions->TargetCondition->Params->Param);
        $targetConditionValue = (string)$this->line->TargetConditions->TargetCondition->Params->Param;
        if (is_string($targetConditionValue)) {
            $this->data['right_id'] = $this->_getEntity($this->opType[1], trim($targetConditionValue), $attributeName);
            if (!$this->data['right_id']) {
                $this->_logError('[ERROR] Invalid target - product or category not found for rule title: ' . $this->data['rule_title']);
                return false;
            }
        } else {
            $this->_logError('[ERROR] Missing target for rule title: ' . $this->data['rule_title']);
            return false;
        }

        return true;
    }

    /**
     * Set the source_type id
     * if it is a string it must be mapped to one of the possible strings for source_type (it string must be contained in one of the default source_type) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for source_type
     * else -> skip row, error
     * @return bool
     */
    protected function setSourceType()
    {
        $valueToLookUp = strtolower(trim((string)$this->line->SourceType));

        return $this->lookupInKeyOrValueOfArray($valueToLookUp, 'source_type', $this->sourceType, true);
    }

    /**
     * Set the target_type id
     * if it is a string it must be mapped to one of the possible strings for target_type (it string must be contained in one of the default target_type) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for target_type
     * else -> skip row, error
     * @return bool
     */
    protected function setTargetType()
    {
        $valueToLookUp = strtolower(trim((string)$this->line->TargetType));

        // @todo remove last param(logMessage), used only as a temporary solution because TargetType is missing in import files
        return $this->lookupInKeyOrValueOfArray($valueToLookUp, 'target_type', $this->targetType, true, false);
    }

    /**
     * Validate the presence of left_id or service_source_expression
     * If the operation_type is SERVICE-PRODUCT the service_source_expression must be present
     * If the operation_type is NOT SERVICE-PRODUCT the left_id must be present
     *
     * @param bool $foundLeftIdOrSourceExpressionCollection
     * @return bool
     */
    protected function validateRequiredLeftIdOrServiceSourceExpression($foundLeftIdOrSourceExpressionCollection)
    {

        // if no left_id nor service_source_expression => error, skip row
        if (!$foundLeftIdOrSourceExpressionCollection) {
            $this->_logError('[ERROR] No source nor service_source_expression can be determined (rule title = ' . $this->data['rule_title'] . '). Skipping element.');
            return false;
        }

        if (in_array($this->data['operation_type'], Dyna_ProductMatchRule_Model_Rule::$serviceOperationTypes)) {
            if (!isset($this->data['service_source_expression'])) {
                $this->_logError('[ERROR] When the operation type is SERVICE-PRODUCT or SERVICE-CATEGORY the service_source_expression must be present(rule title = ' . $this->data['rule_title'] . '). Skipping element.');
                return false;
            }
        } else {
            if (!$this->data['left_id']) {
                $this->_logError('[ERROR] when the operation type is not SERVICE-PRODUCT the left_id must be present (rule title = ' . $this->data['rule_title'] . '). Skipping element');
                return false;
            }
        }

        return true;
    }

    private function validateServiceExpression($serviceExpression)
    {
        /** @var $dynaCoreHelper Dyna_Core_Helper_Data */
        $dynaCoreHelper = Mage::helper('dyna_core');

        $result = $dynaCoreHelper->parseExpressionLanguage(
            $serviceExpression,
            array(
                'installBase',
                'catalog',
                'customer',
                'availability',
                'cart',
                'order',
                'agent'
            ),
            $this->_helper->getImportLogFile()
        );

        if ($result === null) {
            return false;
        }

        return true;
    }

    /**
     * Set locked
     * If the values = locked or 1 => than is locked (meaning 1)
     * Else is 0, meaning unlocked
     */
    protected function setIsLocked()
    {
        $lockedData = trim((string)$this->line->Locked);
        if (is_numeric($lockedData)) {
            $this->data['locked'] = ($lockedData == 1) ? 1 : 0;
        } elseif (strtolower(trim((string)$this->line->Locked)) == 'locked') {
            $this->data['locked'] = 1;
        } else {
            $this->data['locked'] = 0;
        }
    }

    /**
     * Try to set the websites for the rule;
     * Go through each website id or website code provided (the list provided in the csv can be of the form "id1,id2" or "code1,code2"
     * and see if the code/id provided is in the list of websites codes/ids that are possible.
     * If one id/code is not found in the possible list => skip the row, error
     *
     * @return bool
     */
    protected function setWebsiteIds()
    {
        foreach ($this->line->Channels->Channel as $channel)
        {
            $websitesData = explode(',', strtolower(trim((string)$channel)));
            foreach ($websitesData as $key => $possibleWebsite) {
                // if one of the possible websites provided in the csv is "*" the rule will be available for all websites;
                // skip the rest of the code
                if ($possibleWebsite == self::ALL_WEBSITES) {
                    $this->data['website_id'] = array_keys($this->websites);
                    return true;
                }

                // the website provided is the code and we should store the id
                if ($validWebsite = array_search($possibleWebsite, $this->websites)) {
                    $this->data['website_id'][] = $validWebsite;
                } // the website provided is id and we should store it
                elseif (array_key_exists($possibleWebsite, $this->websites)) {
                    $this->data['website_id'][] = $possibleWebsite;
                } // not match can be found between the given website and a code or an id
                else {
                    $this->_logError('[ERROR] The website_id ' . print_r($websitesData, true) .
                        ' is not present/valid (rule title = ' . $this->data['rule_title'] . '). Skipping element.');

                    continue;
                }
            }
        }

        return true;
    }

    /**
     * Set the rule_origin data
     * @return bool
     */
    protected function setRuleOrigin()
    {
        $valueToLookUp = strtolower(trim((string)$this->line->RuleOrigin));
        if (!$this->lookupInKeyOrValueOfArray(
            $valueToLookUp,
            'rule_origin',
            $this->ruleOrigin,
            true
        )) {
            $this->_log('rule_origin was set to the default value for import 1', true);
            $this->data['rule_origin'] = 1;
        }
    }

    /**
     * Set the process context id
     * if it is a string it must be mapped to one of the possible strings for process context (it string must be contained in one of the default process context) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for process context
     * else -> skip row, error
     * @return bool
     */
    protected function setProcessContextId()
    {
        foreach ($this->line->ProcessContexts->ProcessContext as $processContext) {
            $processContextsForRule = strtoupper(trim((string)$processContext));

            if ($processContextsForRule == self::ALL_PROCESS_CONTEXTS) {
                $this->data['process_context'] = $this->processContexts;
                return true;
            }

            $processContextsForRuleArray = explode(',', $processContextsForRule);
            foreach ($processContextsForRuleArray as $processRuleContextForRule) {
                if (isset($this->processContexts[trim($processRuleContextForRule)])) {
                    $this->data['process_context'][] = $this->processContexts[trim($processRuleContextForRule)];
                } else {
                    $this->_logError('[ERROR] The process_context "' . trim($processRuleContextForRule) .
                        '" is not valid (rule title = ' . $this->data['rule_title'] . '). Skipping element.');
                    continue;
                }
            }
        }

        return true;
    }

    /**
     * Set package type for current entry
     */
    public function setPackageType()
    {
        /** @var Dyna_Package_Model_PackageType $packageTypeModel */
        $packageTypeModel = Mage::getModel("dyna_package/packageType");

        $packageTypes = array_map(function ($packageType) {
            return trim($packageType);
        }, explode(",", $this->line->PackageTypeId));

        foreach ($packageTypes as $packageType) {
            // If not previously loaded, load and cache it
            if (empty($this->packageTypes[$packageType])) {
                $packageTypeObj = $packageTypeModel->loadByCode($packageType);
                if (!$packageTypeObj->getId()) {
                    $errorMessage = "Trying to import combination rules for an invalid package code: " . $packageType .
                        ". Have all the package types importers been run?";
                    $packageTypeCollection = Mage::getModel("dyna_package/packageType")
                        ->getCollection();
                    $packageTypeCodes = [];

                    foreach ($packageTypeCollection as $packageTypeModel) {
                        $packageTypeCodes[] = $packageTypeModel->getPackageCode();
                    }
                    $errorMessage .= " Valid backend package types are: " . implode(",", $packageTypeCodes);

                    Mage::throwException($errorMessage);
                }

                // Deleting old rules has been moved to shell script
                $this->packageTypes[$packageType] = $packageTypeObj->getId();
            }

            $this->data["package_type"][] = $this->packageTypes[$packageType];
        }

        return true;
    }

    /**
     * @param $element
     * @return bool|string
     */
    protected function getAttributeName($element)
    {
        if ((string)$element->attributes()->name == self::PARAM_CATEGORY_ID) {
            return (string)$element->attributes()->name;
        } else {
            if ((string)$element->attributes()->name == self::PARAM_CATEGORY_PATH) {
                return (string)$element->attributes()->name;
            } else {
                return false;
            }
        }
    }

    protected function getCategoryPath($categoryUniqueID)
    {
        /** @var Dyna_Catalog_Model_Category $category */
        $category = Mage::getModel('dyna_catalog/category')
            ->getCollection()
            ->addFieldToFilter('unique_id', ['eq' => $categoryUniqueID])
            ->getFirstItem();

        if ($category) {
            /** @var Dyna_Configurator_Helper_Expression $categoryHelper */
            $categoryHelper = Mage::helper('dyna_configurator/expression');
            $categoryHelper->setCategories();
            return $categoryHelper->getCategoryPath($category->getEntityId());
        }

        return $categoryUniqueID;
    }

    protected function searchAndTransformCategoryIdToPath(&$conditions, $type = 'Source')
    {
        $conditionType = ($type == 'Target') ? 'TargetCondition' : 'SourceCondition';
        $conditionsType = ($type == 'Target') ? 'TargetConditions' : 'SourceConditions';

        foreach ($conditions->$conditionType as $condition) {
            if ($condition->$conditionsType) {
                $this->searchAndTransformCategoryIdToPath($condition->$conditionsType, $type);
            } else {
                foreach ($condition->Params->Param as $param) {
                    if (property_exists($param, $conditionsType)) {
                        $this->searchAndTransformCategoryIdToPath($param->$conditionsType, $type);
                    } else {
                        foreach ($param->attributes() as $attr) {
                            if (strtolower((string)$attr) == strtolower(self::PARAM_CATEGORY_ID)) {
                                $param[0] = $this->getCategoryPath((string)$param);
                            }
                        }
                    }
                }
            }
        }
    }
}
