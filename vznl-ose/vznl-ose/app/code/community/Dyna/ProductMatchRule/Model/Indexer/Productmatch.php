<?php

/**
 * Class Dyna_ProductMatchRule_Model_Indexer_Productmatch
 */
class Dyna_ProductMatchRule_Model_Indexer_Productmatch extends Omnius_ProductMatchRule_Model_Indexer_Productmatch
{
    const CACHE_TAG = 'product_match_whitelist_indexer';
    const RULE_CONTEXTS_CACHE_TAG = 'product_match_whitelist_indexer_rule_contexts';

    protected $args = [];
    protected $website = null;
    protected $allWebsites = [];
    protected $packageTypes = [];
    protected $_inserted = 0;
    protected $_deleted = 0;
    protected $_cache = null;
    protected $redisClient;
    protected $rulesContexts;
    /** @var Dyna_ProductMatchRule_Helper_ArrayRuleCache */
    protected $ruleCache;
    protected $sourceCollectionId = null;
    protected $packageCode = null;
    /** @var string */
    protected $_pcTable = "product_match_whitelist_index_process_context_set";
    protected $tempPcs = array();

    /**
     * Initiate indexer
     */
    public function __construct()
    {
        parent::__construct();
        $this->ruleCache = Mage::helper('dyna_productmatchrule/arrayRuleCache');

        $this->initMemoryLimits();
        $this->_init('dyna_productmatchrule/indexer_productmatch');
    }

    /**
     * Clear data per package type if package type is provided as argument
     * @param $packageId
     */
    protected function clearDataPerPackageType($packageId)
    {
        if (!empty($this->args) || (empty($this->args) && !empty($this->website))) {
            $deleteStartTime = microtime(true);
            $conditions = $this->getConnection()->prepareSqlCondition("package_type", $packageId);
            $deletedRows = 0;

            if (!empty($this->website)) {
                $websiteId = $this->allWebsites[strtolower($this->website)];
                $conditions .= " AND " . $this->getConnection()->prepareSqlCondition("website_id", $websiteId);
            }

            $select = "SELECT COUNT(*) FROM ". $this->_table ." WHERE " . $conditions;
            $noDbEntries = (int)$this->getConnection()->fetchOne($select);

            if ($noDbEntries > 0) {
                do {
                    $batchSize = self::BATCH_SIZE * 100;
                    $limit = $batchSize > $noDbEntries ? $batchSize : $noDbEntries;
                    $this->_query = $this->getConnection()->query(sprintf('DELETE FROM `%s` WHERE %s LIMIT %d', $this->_table, $conditions, $limit));

                    $deletedRows += $this->_query->rowCount();
                    $this->closeConnection();
                    $noDbEntries = $noDbEntries - self::BATCH_SIZE;
                } while ($noDbEntries >= 0);
            }


            $deleteResultTime = gmdate('H:i:s', microtime(true) - $deleteStartTime);
            if ($deleteResultTime <= '00:01:00') {
                $deleteResultTime = null;
            } else {
                $this->productMatchRuleHelper->logIndexer("Deleted " . $deletedRows . " rows" . (empty($deleteResultTime) ? "." : " in " . $deleteResultTime . "."));
            }
        }
    }

    /**
     * Executed after processing the collection
     */
    public function end()
    {
        $this->_applyChanges();
        if ($this->tempPcs) {
            $pcIds = "";
            foreach ($this->tempPcs as $ids => $id) {
                $pcIds .= ($pcIds ? "," : "") . "(" . $id . ",'" . $ids . "')";
            }

            $this->getConnection()->query(sprintf("INSERT IGNORE INTO %s(entity_id, process_context_ids) VALUES%s", $this->_pcTable, $pcIds));
        }
        $this->getConnection()->query(sprintf('ALTER TABLE %s DROP INDEX IDX_PRD_MATCH_WHITELIST_UNIQUE_ID_ENTRY', $this->_table));
        $this->getConnection()->query(sprintf('ALTER TABLE %s ENABLE KEYS', $this->_table));
        Omnius_ProductMatchRule_Model_Indexer_Abstract::end();
    }

    /**
     * Empty index and generate whitelist based on rules
     * args can only be package types (see shell/deindexer.php)
     */
    public function reindexAll()
    {
        // Package types can be sent as params from cli indexing
        if (!empty($this->args)) {
            // Expecting args to contain a list of package type codes based on which we will load their ids
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getModel("dyna_package/packageType");
            foreach ($this->args as $packageCode) {
                $package = $packageTypeModel->loadByCode($packageCode);
                if ($package->getId()) {
                    $this->packageTypes[$package->getPackageCode()] = $package->getId();
                } else {
                    $this->productMatchRuleHelper->logIndexer("[Productmatch] Skipping argument package type: " . $packageCode . " because cannot be loaded (check Admin / Catalog / Packages Configuration / Package Types)");
                }
            }
        } else {
            // Loading all package types an executing indexer consecutive for each rule
            $packageTypeCollection = Mage::getModel("dyna_package/packageType")->getCollection();
            /** @var Dyna_Package_Model_Package $package */
            foreach ($packageTypeCollection as $package) {
                $this->packageTypes[$package->getPackageCode()] = $package->getId();
            }
        }

        if (empty($this->packageTypes)) {
            $this->productMatchRuleHelper->logIndexer("[Productmatch] Exiting indexer because there are no package types defined. Please run package types import and execute indexer later.");
            exit;
        }

        $this->_reindexAll();
    }

    /**
     * Create Redis cache for not allowed rules that will be used when creating the allowed rules
     * @param $packageId
     */
    public function buildNotAllowedCache($packageId, $sourceCollectionId)
    {
        $processStartTime = microtime(true);
        $notAllowedCollection = Mage::getResourceModel('dyna_productmatchrule/rule_collection')
            ->addFieldToSelect(['product_match_rule_id', 'package_type', 'operation_type', 'priority', 'left_id', 'right_id', 'operation'])
            ->addFieldToFilter('package_type', ['eq' => (int)$packageId])
            ->addFieldToFilter('operation_type', ['in' => [
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P
            ]])
            ->addFieldToFilter('operation', ['eq' => Dyna_ProductMatchRule_Model_Rule::OP_NOTALLOWED])
            ->addFieldToFilter('source_collection', ['eq' => $sourceCollectionId])
            ->setPageSize(self::BATCH_SIZE);

        $notAllowedCollection->getSelect()
            ->columns('(SELECT GROUP_CONCAT(website_id) FROM product_match_rule_website r WHERE main_table.product_match_rule_id = r.rule_id) AS website_ids')
            ->group('main_table.product_match_rule_id')
            ->order('main_table.priority', 'asc')
            ->order('main_table.product_match_rule_id', 'asc');

        $totalNotAllowedRecords = $notAllowedCollection->getSize();

        $this->productMatchRuleHelper->logIndexer("[Productmatch] Caching Not Allowed rules: " . $totalNotAllowedRecords . " Not Allowed rules.");
        $notAllowedCurrentPage = 1;
        $notAllowedPages = $notAllowedCollection->getLastPageNumber();

        do {
            $pageStartTime = microtime(true);
            $notAllowedCollection->setCurPage($notAllowedCurrentPage);
            $this->associateRulesToContexts($notAllowedCollection);

            foreach ($notAllowedCollection as $notAllowedRule) {
                $this->processNotAllowedRule($notAllowedRule);
            }

            unset($notAllowedRule);
            $pageResultTime = gmdate('H:i:s', microtime(true) - $pageStartTime);
            if ($pageResultTime >= '00:01:00') {
                $this->productMatchRuleHelper->logIndexer("Not Allowed Page " . $notAllowedCurrentPage . "/" . $notAllowedPages . " took " . $pageResultTime);
            }
            $notAllowedCurrentPage++;
            $notAllowedCollection->clear();
        } while ($notAllowedCurrentPage <= $notAllowedPages);

        $processResultTime = gmdate('H:i:s', ceil(microtime(true) - $processStartTime));
        $this->productMatchRuleHelper->logIndexer("Process Not Allowed Items took: " . $processResultTime);

        $notAllowedCollection->clear();
        unset($notAllowedCollection, $notAllowedCurrentPage);
    }

    /**
     * @param $websiteId
     * @param $left
     * @param $right
     * @param $priority
     */
    protected function storeRule($websiteId, $left, $right, $priority)
    {
        $key = $this->ruleCache->buildKey($websiteId, $left, $right);

        $prioNotAllowed = $this->ruleCache->get($key);
        if ($prioNotAllowed === false || ((int)$prioNotAllowed < (int)$priority)) {
            $this->ruleCache->set($key, $priority);
        }
    }

    /**
     * @param $websiteId
     * @param $left
     * @param $right
     * @param $priority
     * @param $contextId
     * @return bool
     */
    public function canInsertAllowedRule($websiteId, $left, $right, $priority, $contextId)
    {
        $key = $this->ruleCache->buildKey($websiteId, $left, $right);
        $ruleInfo = $this->ruleCache->get($key);
        $prioNotAllowed = false;

        if (($contextId && strpos('.' . $contextId . '.', $ruleInfo) !== false) || $ruleInfo) {
            $prioNotAllowed = (int)strtok($ruleInfo, ".");
        }

        return $prioNotAllowed === false || $priority > $prioNotAllowed;
    }

    /**
     * @param $rule Dyna_ProductMatchRule_Model_Rule
     */
    public function processNotAllowedRule($rule)
    {
        $processStartTime = microtime(true);
        $websiteIds = explode(',', $rule['website_ids']);

        switch ($rule['operation_type']) {
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allProductIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {

                    $left = $rule['left_id'];
                    $right = $rule['right_id'];
                    if (!$rule['override_initial_selectable']) {
                        $left = min($rule['left_id'], $rule['right_id']);
                        $right = max($rule['left_id'], $rule['right_id']);
                    }

                    if (isset($this->rulesContexts[$rule['product_match_rule_id']])) {
                        $ruleInfo = $rule['priority'] . '.' . implode('.', $this->rulesContexts[$rule['product_match_rule_id']]) . '.';
                    } else {
                        $ruleInfo = $rule['priority'] . '.';
                    }
                    $this->storeRule($websiteId, $left, $right, $ruleInfo);
                    unset($left, $right);
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    if (isset($this->_productCategories[$websiteId][$rule['right_id']])) {
                        foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$catProdId) {

                            $left = $rule['left_id'];
                            $right = $catProdId;
                            if (!$rule['override_initial_selectable']) {
                                $left = min($rule['left_id'], $catProdId);
                                $right = max($rule['left_id'], $catProdId);
                            }

                            if (isset($this->rulesContexts[$rule['product_match_rule_id']])) {
                                $ruleInfo = $rule['priority'] . '.' . implode('.', $this->rulesContexts[$rule['product_match_rule_id']]) . '.';
                            } else {
                                $ruleInfo = $rule['priority'] . '.';
                            }
                            $this->storeRule($websiteId, $left, $right, $ruleInfo);
                        }
                        unset($catProdId);
                    }
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (!in_array($rule['right_id'], $this->_allProductIds)
                    || !in_array($rule['left_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    if (isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                        foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$catProdId) {

                            $left = $catProdId;
                            $right = $rule['right_id'];
                            if (!$rule['override_initial_selectable']) {
                                $left = min($catProdId, $rule['right_id']);
                                $right = max($catProdId, $rule['right_id']);
                            }

                            if (isset($this->rulesContexts[$rule['product_match_rule_id']])) {
                                $ruleInfo = $rule['priority'] . '.' . implode('.', $this->rulesContexts[$rule['product_match_rule_id']]) . '.';
                            } else {
                                $ruleInfo = $rule['priority'] . '.';
                            }
                            $this->storeRule($websiteId, $left, $right, $ruleInfo);
                        }
                        unset($catProdId);
                    }
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (!in_array($rule['left_id'], $this->_allCategoryIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    if (isset($this->_productCategories[$websiteId][$rule['right_id']]) && isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                        foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$rightProdId) {
                            foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$leftProdId) {

                                $left = $leftProdId;
                                $right = $rightProdId;
                                if (!$rule['override_initial_selectable']) {
                                    $left = min($leftProdId, $rightProdId);
                                    $right = max($leftProdId, $rightProdId);
                                }

                                if (isset($this->rulesContexts[$rule['product_match_rule_id']])) {
                                    $ruleInfo = $rule['priority'] . '.' . implode('.', $this->rulesContexts[$rule['product_match_rule_id']]) . '.';
                                } else {
                                    $ruleInfo = $rule['priority'] . '.';
                                }
                                $this->storeRule($websiteId, $left, $right, $ruleInfo);
                            }
                            unset($rightProdId);
                        }
                        unset($leftProdId);
                    }
                }
                break;
            default :
                break;
        }

        $processResultTime = gmdate('H:i:s', microtime(true) - $processStartTime);
        if ($processResultTime >= '00:01:00') {
            $this->productMatchRuleHelper->logIndexer("Processing Not Allowed rule " . $rule['product_match_rule_id'] . " took " . $processResultTime);
        }

        unset($rule, $websiteIds);
    }


    /**
     * Reindex all
     */
    protected function _reindexAll()
    {
        if (empty($this->args) && empty($this->website)) {
            $this->productMatchRuleHelper->logIndexer("[Productmatch] Truncating table " . $this->_table . ".");
            $this->productMatchRuleHelper->logIndexer("[Productmatch] Truncating table " . $this->_pcTable . ".");
            $deleteStartTime = microtime(true);

            $this->getConnection()->query(sprintf('TRUNCATE TABLE `%s`', $this->_table));
            $this->getConnection()->query(sprintf('TRUNCATE TABLE `%s`', $this->_pcTable));

            $deleteResultTime = gmdate('H:i:s', microtime(true) - $deleteStartTime);

            if ($deleteResultTime <= '00:01:00') {
                $deleteResultTime = null;
            } else {
                $this->productMatchRuleHelper->logIndexer("Truncated table" . (empty($deleteResultTime) ? "." : " in " . $deleteResultTime . "."));
            }
        } else {
            // cannot truncate process context table because there this is a partial index so loading all process_context_ids into tempPcs
            $processContextTable = $this->getConnection()->query(sprintf("select entity_id, process_context_ids from %s order by entity_id asc", $this->_pcTable))->fetchAll();
            $this->tempPcs = array_combine(array_column($processContextTable, "process_context_ids"), array_column($processContextTable, "entity_id"));
        }

        // temporarily add unique key on columns
        $indexes = $this->getConnection()->query(sprintf("SHOW INDEX FROM %s;", $this->_table))->fetchAll(\PDO::FETCH_ASSOC);
        if (!in_array("IDX_PRD_MATCH_WHITELIST_UNIQUE_ID_ENTRY", array_column($indexes, "Key_name"))) {
            $this->getConnection()->query(sprintf('ALTER TABLE %s ADD UNIQUE KEY `IDX_PRD_MATCH_WHITELIST_UNIQUE_ID_ENTRY` (`website_id`,`package_type`,`source_product_id`,`target_product_id`,`override_initial_selectable`,`product_match_whitelist_index_process_context_set_id`,`source_collection`);', $this->_table));
        }

        $this->start();
        $this->getConnection()->query(sprintf('ALTER TABLE %s DISABLE KEYS', $this->_table));

        foreach ($this->packageTypes as $packageCode => $packageId) {
            $this->ruleCache->clear();
            $this->_inserted = 0;
            $this->rulesContexts = [];
            $packageTypeStartTime = microtime(true);
            $this->productMatchRuleHelper->logIndexer("[Productmatch] Removing indexer entries for package type: " . $packageCode . ".");
            $this->packageCode = $packageCode;

            $this->clearDataPerPackageType($packageId);

            $this->productMatchRuleHelper->logIndexer("[Productmatch] Building indexer entries for package type: " . $this->packageCode);

            $sourceCollections = $this->productMatchRuleHelper->getSourceCollections();
            foreach ($sourceCollections as $sourceCollectionId => $sourceCollectionName) {
                $this->ruleCache->clear();
                $this->sourceCollectionId = $sourceCollectionId;
                $this->buildNotAllowedCache($packageId, $sourceCollectionId);
                $this->productMatchRuleHelper->logIndexer("[Productmatch] Processing source collection: " . $sourceCollectionName . ".");
                $this->buildIndex($packageId, $sourceCollectionId);
            }

            $packageTypeResultTime = gmdate('H:i:s', ceil(microtime(true) - $packageTypeStartTime));

            $this->productMatchRuleHelper->logIndexer("Package Type " . $packageCode . " took " . $packageTypeResultTime);
            $this->productMatchRuleHelper->logIndexer("Total Inserted rows " . $this->_inserted . " for package type " . $packageCode);

            $this->_cache = null;
            $this->closeConnection();
        }

        $this->end();
    }

    /**
     * @param $packageId
     * @param $sourceCollectionId
     */
    public function buildIndex($packageId, $sourceCollectionId)
    {
        /** @var Omnius_ProductMatchRule_Model_Resource_Rule_Collection $collection */
        $collection = Mage::getResourceModel('dyna_productmatchrule/rule_collection')
            ->addFieldToSelect(['product_match_rule_id', 'package_type', 'operation_type', 'priority', 'left_id', 'right_id', 'operation', 'override_initial_selectable'])
            ->addFieldToFilter('package_type', ['eq' => (int)$packageId])
            ->addFieldToFilter('operation_type', ['in' => [
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P]])
            ->addFieldToFilter('operation', ['in' => [
                Dyna_ProductMatchRule_Model_Rule::OP_ALLOWED,
                Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED,
                Dyna_ProductMatchRule_Model_Rule::OP_OBLIGATED,
                Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED]])
            ->addFieldToFilter('source_collection', ['eq' => $sourceCollectionId])
            ->setPageSize(self::BATCH_SIZE);

        $collection->getSelect()
            ->columns('(SELECT GROUP_CONCAT(website_id) FROM product_match_rule_website r WHERE main_table.product_match_rule_id = r.rule_id) AS website_ids')
            ->group('main_table.product_match_rule_id')
            ->order('main_table.priority', 'asc')
            ->order('main_table.product_match_rule_id', 'asc');

        $totalRecords = $collection->getSize();

        $this->productMatchRuleHelper->logIndexer("[Productmatch] Found a number of: ". $totalRecords . " rules.");
        if (!$totalRecords) {
            $this->productMatchRuleHelper->logIndexer("[Productmatch] Nothing to do, returning.");
            return;
        }

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();

        /**
         * Process the rules within batches to remove the risk using all the allocated memory
         * This way, we never load the whole collection into the current memory
         */
        $processStartTime = microtime(true);
        do {
            $this->productMatchRuleHelper->logIndexer("Start Process Page " . $currentPage . "/" . $pages);
            $pageStartTime = microtime(true);
            $collection->setCurPage($currentPage);
            $collection->load();

            $this->associateRulesToContexts($collection);

            foreach ($collection as $rule) {
                $this->processItem($rule);
            }

            $pageResultTime = gmdate('H:i:s', ceil(microtime(true) - $pageStartTime));
            if ($pageResultTime >= '00:01:00') {
                $this->productMatchRuleHelper->logIndexer("Page " . $currentPage . "/" . $pages . " took " . $pageResultTime . " insert index at " . $this->_inserted);
            }

            $currentPage++;
            $collection->clear();
        } while ($currentPage <= $pages);

        //Mage::getModel('core/resource_iterator')->walk($collection->getSelect(), array(array($this, 'processItem')));

        $collection->clear();
        $collection = null;
        unset($collection);

        $processResultTime = gmdate('H:i:s', microtime(true) - $processStartTime);
        if ($processResultTime >= '00:01:00') {
            $this->productMatchRuleHelper->logIndexer("Build index took " . $processResultTime);
        }

        $this->_applyChanges();

    }

    /**
     * @param $rule Dyna_ProductMatchRule_Model_Rule
     */
    public function processItem($rule)
    {
        $websiteIds = explode(',', $rule['website_ids']);

        if (!empty($this->website)) {
            $websiteId = $this->allWebsites[strtolower($this->website)];
            if (!in_array($websiteId, $websiteIds)) {
                return;
            }
            $websiteIds = [$websiteId];
        }

        switch ($rule['operation_type']) {
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allProductIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    $left = $rule['left_id'];
                    $right = $rule['right_id'];
                    if (!$rule['override_initial_selectable']) {
                        $left = min($rule['left_id'], $rule['right_id']);
                        $right = max($rule['left_id'], $rule['right_id']);
                    }

                    $appendContexts = [];
                    $noProcessContext = false;
                    if (isset($this->rulesContexts[$rule['product_match_rule_id']])) {
                        foreach ($this->rulesContexts[$rule['product_match_rule_id']] as $ruleContext) {
                            if ($this->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], $ruleContext)) {
                                $appendContexts[] = $ruleContext;
                            }
                        }
                    } else {
                        if ($this->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], null)) {
                            $noProcessContext = true;
                        }
                    }

                    if (count($appendContexts) || $noProcessContext) {
                        $contexts = $noProcessContext ? null : implode(',', $appendContexts);
                        $this->_addDynaConditional($rule['product_match_rule_id'], $rule['package_type'], $left, $right, $websiteId, $rule['override_initial_selectable'], $contexts);
                    }
                    unset($prioNotAllowed, $left, $right, $key);
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    if (isset($this->_productCategories[$websiteId][$rule['right_id']])) {
                        foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$rightProdId) {
                            $left = $rule['left_id'];
                            $right = $rightProdId;
                            if (!$rule['override_initial_selectable']) {
                                $left = min($rule['left_id'], $rightProdId);
                                $right = max($rule['left_id'], $rightProdId);
                            }

                            $appendContexts = [];
                            $noProcessContext = false;
                            if (isset($this->rulesContexts[$rule['product_match_rule_id']])) {
                                foreach ($this->rulesContexts[$rule['product_match_rule_id']] as $ruleContext) {
                                    if ($this->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], $ruleContext)) {
                                        $appendContexts[] = $ruleContext;
                                    }
                                }
                            } else {
                                if ($this->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], null)) {
                                    $noProcessContext = true;
                                }
                            }

                            if (count($appendContexts) || $noProcessContext) {
                                $contexts = $noProcessContext ? null : implode(',', $appendContexts);
                                $this->_addDynaConditional($rule['product_match_rule_id'], $rule['package_type'], $left, $right, $websiteId, $rule['override_initial_selectable'], $contexts);
                            }

                            unset($prioNotAllowed, $left, $right, $key);
                        }
                        unset($rightProdId);
                    }
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (!in_array($rule['right_id'], $this->_allProductIds)
                    || !in_array($rule['left_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    if (isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                        foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$leftProdId) {
                            $left = $leftProdId;
                            $right = $rule['right_id'];
                            if (!$rule['override_initial_selectable']) {
                                $left = min($leftProdId, $rule['right_id']);
                                $right = max($leftProdId, $rule['right_id']);
                            }

                            $appendContexts = [];
                            $noProcessContext = false;
                            if (isset($this->rulesContexts[$rule['product_match_rule_id']])) {
                                foreach ($this->rulesContexts[$rule['product_match_rule_id']] as $ruleContext) {
                                    if ($this->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], $ruleContext)) {
                                        $appendContexts[] = $ruleContext;
                                    }
                                }
                            } else {
                                if ($this->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], null)) {
                                    $noProcessContext = true;
                                }
                            }

                            if (count($appendContexts) || $noProcessContext) {
                                $contexts = $noProcessContext ? null : implode(',', $appendContexts);
                                $this->_addDynaConditional($rule['product_match_rule_id'], $rule['package_type'], $left, $right, $websiteId, $rule['override_initial_selectable'], $contexts);
                            }
                            unset($prioNotAllowed, $left, $right, $key);
                        }
                        unset($leftProdId);
                    }
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (!in_array($rule['left_id'], $this->_allCategoryIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    if (isset($this->_productCategories[$websiteId][$rule['right_id']]) && isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                        foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$rightProdId) {
                            foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$leftProdId) {
                                $left = $leftProdId;
                                $right = $rightProdId;
                                if (!$rule['override_initial_selectable']) {
                                    $left = min($leftProdId, $rightProdId);
                                    $right = max($leftProdId, $rightProdId);
                                }

                                $appendContexts = [];
                                $noProcessContext = false;
                                if (isset($this->rulesContexts[$rule['product_match_rule_id']])) {
                                    foreach ($this->rulesContexts[$rule['product_match_rule_id']] as $ruleContext) {
                                        if ($this->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], $ruleContext)) {
                                            $appendContexts[] = $ruleContext;
                                        }
                                    }
                                } else {
                                    if ($this->canInsertAllowedRule($websiteId, $left, $right, $rule['priority'], null)) {
                                        $noProcessContext = true;
                                    }
                                }

                                if (count($appendContexts) || $noProcessContext) {
                                    $contexts = $noProcessContext ? null : implode(',', $appendContexts);
                                    $this->_addDynaConditional($rule['product_match_rule_id'], $rule['package_type'], $left, $right, $websiteId, $rule['override_initial_selectable'], $contexts);
                                }
                                unset($prioNotAllowed, $left, $right, $key);
                            }
                            unset($leftProdId);
                        }
                        unset($rightProdId);
                    }
                }
                break;
            default :
                break;
        }
    }

    /**
     * @param $left
     * @param $right
     * @param $websiteId
     */
    public function _addDynaConditional($ruleId, $packageId, $left, $right, $websiteId, $override_initial_selectable = 0, $processContext = null)
    {
        $startTime = microtime(true);
        if ($left != $right) {
            if (!$override_initial_selectable) {
                $leftId = min($left, $right);
                $rightId = max($left, $right);
            } else {
                $leftId = $left;
                $rightId = $right;
            }

            $this->_matches[] = array(self::ADD_ACTION, array($websiteId, $ruleId, $packageId, $leftId, $rightId, $override_initial_selectable, $processContext));
            $this->_assertMemory();
        }

        $resultTime = gmdate('H:i:s', microtime(true) - $startTime);
        if ($resultTime >= '00:01:00') {
            $this->productMatchRuleHelper->logIndexer("Process ADD for " . $ruleId . " with left " . $left . " and right " . $right . " took " . $resultTime);
        }
    }

    /**
     * Groups gathered items together by statement (INSERT/DELETE)
     * to decrease the number of statements executed on the database
     */
    protected function _applyChanges()
    {

        /**
         * If something remains unprocessed in the _matches array
         */
        if (count($this->_matches)) {
            if ($this->_matches[0][0] === self::ADD_ACTION) {
                $start = microtime(true);
                $this->_insertMultiple();
                $this->timeToInsert += microtime(true) - $start;
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        $this->_matches = [];
    }

    /**
     * Builds INSERT statements and executes them
     * Iterates over the items withing the $_prev array
     * and builds the INSERT statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function _insertMultiple()
    {
        $values = '';

        $add = array();
        $this->_matches = array_reverse($this->_matches);
        while (($_row = array_pop($this->_matches)) !== null) {
            $add[$_row[1][0]][] = array($_row[1][1], $_row[1][2], $_row[1][3], $_row[1][4], $_row[1][5], $_row[1][6]);
        }
        unset($_row);

        foreach ($add as $websiteId => &$combinations) {
            foreach ($combinations as $key => &$combination) {
                !isset($this->tempPcs[$combination[5]]) && ($this->tempPcs[$combination[5]] = end($this->tempPcs) + 1);
                $processContextIds = $this->tempPcs[$combination[5]];
                $values .= sprintf('("%s","%s","%s","%s","%s","%s","%s", %d),', $websiteId, $combination[1], $combination[2], $combination[3], $combination[4], $processContextIds, $combination[0], $this->sourceCollectionId);

                if (strlen($values) >= $this->_maxPacketsLength) {
                    $sql = sprintf(
                        'INSERT INTO `%s` (`website_id`,`package_type`,`source_product_id`,`target_product_id`,`override_initial_selectable`,`product_match_whitelist_index_process_context_set_id`,`rule_id`,`source_collection`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                        $this->_table,
                        trim($values, ',')
                    );

                    $this->_query = $this->getConnection()->query($sql);

                    $this->_inserted += $this->_query->rowCount();
                    $this->closeConnection();

                    unset($sql, $values);

                    $values = '';
                }
                unset($combinations[$key]);
            }
            unset($combination, $add[$websiteId]);
        }

        unset($combinations, $add);

        if ($values != '') {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`package_type`,`source_product_id`,`target_product_id`,`override_initial_selectable`,`product_match_whitelist_index_process_context_set_id`,`rule_id`,`source_collection`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                $this->_table,
                trim($values, ',')
            );

            $this->_query = $this->getConnection()->query($sql);

            $this->_inserted += $this->_query->rowCount();
            $this->closeConnection();

            unset($sql, $values);
        }
    }

    /**
     * Params setter called from shell/deindexer.php to filter indexing for a certain package type
     * @param array $args
     * @return $this
     */
    public function setParams($args = [])
    {
        $this->args = $args;

        return $this;
    }

    /**
     * Website setter called from shell/deindexer.php to filter indexing for a certain website
     * @param string $website
     * @return $this
     */
    public function setWebsite($website = null)
    {
        $this->website = $website;

        $allWebsites = Mage::app()->getWebsites();
        foreach ($allWebsites as $eachWebsite) {
            $this->allWebsites[$eachWebsite->getCode()] = strtolower($eachWebsite->getId());
        }

        return $this;
    }


    public function associateRulesToContexts($collection)
    {
        if ($collection->getIds()) {

            $ruleIds = implode(",", $collection->getIds());
            $sql = "SELECT * FROM `product_match_rule_process_context` WHERE `rule_id` IN ($ruleIds)";

            $processContextIds = $this->getConnection()->fetchAll($sql);
            $this->closeConnection();
            $pageContexts = $this->rulesContexts;

            foreach ($processContextIds as $processCtx) {
                $pageContexts[$processCtx['rule_id']][] = $processCtx['process_context_id'];
            }
            unset($processContextIds);

            $this->rulesContexts = $pageContexts;
        }
    }

    public function getRulesContexts()
    {
        return $this->rulesContexts;
    }
}
