<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_ProductMatchRule_Model_Indexer_RemovalNotAllowed extends Omnius_ProductMatchRule_Model_Indexer_Abstract
{
    const LOG_PREFIX = "REMOVAL NOT ALLOWED";
    const NAME = "Product removal not allowed";
    const DESCRIPTION = "Index products that are not allowed for removal based on cart combination";
    const ADD_ACTION = 1;

    protected $args = [];
    protected $packageTypes = [];

    /** @var string */
    protected $_table;

    /** @var array */
    protected $_matches = array();

    /** @var array */
    protected $_mandatory = array();

    /** @var array */
    protected $_prev = array();

    /** @var array */
    protected $_allProductIds = array();

    /** @var array */
    protected $_allCategoryIds = array();

    /** @var array */
    protected $_productCategories = array();

    /** @var array */
    protected $_websites = array();

    /** @var array */
    protected $_websiteIds = array();

    public function __construct()
    {
        parent::__construct();

        $this->_table = (string) Mage::getResourceSingleton('dyna_productmatchrule/removalNotAllowed')->getMainTable();
        $this->initMemoryLimits();

        $this->_init('dyna_productmatchrule/indexer_removalNotAllowed');
    }

    /**
     * Register indexer required data inside event object
     * @param   Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        // no-op
    }

    /**
     * Process event based on event state data
     * @param   Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        // no-op
    }

    /**
     * Return indexer name
     * @return string
     */
    public function getName()
    {
        return static::NAME;
    }

    /**
     * Get Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return static::DESCRIPTION;
    }

    /**
     * Params setter called from shell/deindexer.php to filter indexing for a certain package type
     * @param array $args
     * @return $this
     */
    public function setParams($args = [])
    {
        $this->args = $args;

        return $this;
    }

    /**
     * Empty index and generate removal not allowed index entries
     */
    public function reindexAll()
    {
        $this->productMatchRuleHelper = Mage::helper('productmatchrule');

        // Package types can be sent as params from cli indexing
        if (!empty($this->args)) {
            // Expecting args to contain a list of package type codes based on which we will load their ids
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getModel("dyna_package/packageType");
            foreach ($this->args as $packageCode) {
                $package = $packageTypeModel->loadByCode($packageCode);
                if ($package->getId()) {
                    $this->packageTypes[$package->getPackageCode()] = $package->getId();
                } else {
                    $this->productMatchRuleHelper->logIndexer("[" . static::LOG_PREFIX . "] Skipping argument package type: " . $packageCode . " because cannot be loaded (check Admin / Catalog / Packages Configuration / Package Types)");
                }
            }
        } else {
            // Loading all package types an executing indexer consecutive for each rule
            $packageTypeCollection = Mage::getModel("dyna_package/packageType")->getCollection();
            foreach ($packageTypeCollection as $package) {
                $this->packageTypes[$package->getPackageCode()] = $package->getId();
            }
        }

        if (empty($this->packageTypes)) {
            $this->productMatchRuleHelper->logIndexer("[" . static::LOG_PREFIX . "] Exiting indexer because there are no package types defined. Please run package types import and execute indexer later.");
            exit;
        }

        $this->_reindexAll();
    }

    /**
     * Reindex all not allowed rules
     */
    protected function _reindexAll()
    {
        // Make sure we have keys enabled before we start deleting/truncating
        $this->getConnection()->query('ALTER TABLE ' . $this->_table . ' ENABLE KEYS;');
        $this->getConnection()->query('SET UNIQUE_CHECKS = 1;');
        $this->getConnection()->query('SET AUTOCOMMIT = 1;');
        $this->start();
        if (empty($this->args)) {
            $this->productMatchRuleHelper->logIndexer("[" . static::LOG_PREFIX . "] Truncating table " . $this->_table . ".");
            $deleteStartTime = microtime(true);
            $this->getConnection()->query(sprintf('TRUNCATE TABLE `%s`', $this->_table));
            $this->closeConnection();
            $deleteResultTime = gmdate('H:i:s', microtime(true) - $deleteStartTime);
            if ($deleteResultTime <= '00:01:00') {
                $deleteResultTime = null;
            }
            $this->productMatchRuleHelper->logIndexer("[" . static::LOG_PREFIX . "] Truncated table" . (empty($deleteResultTime) ? "." : " in " . $deleteResultTime . "."));
        }

        foreach ($this->packageTypes as $packageCode => $packageId) {
            if (!empty($this->args)) {
                $this->productMatchRuleHelper->logIndexer("[" . static::LOG_PREFIX . "] Removing indexer entries for package type: " . $packageCode . ".");
                $this->getConnection()->query(sprintf('delete from `%s` where package_type=%s;', $this->_table, $packageId));
            }
            $this->productMatchRuleHelper->logIndexer("[" . static::LOG_PREFIX . "] Building indexer entries for package type: " . $packageCode . ".");

            /** @var Omnius_ProductMatchRule_Model_Resource_Rule_Collection $collection */
            $collection = Mage::getResourceModel('dyna_productmatchrule/rule_collection')
                ->addFieldToFilter('operation_type', ['in' => [
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P,
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C,
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C,
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P,
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P,
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C,
                ]])
                ->addFieldToFilter('operation',
                    array(
                        array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_REMOVAL_NOTALLOWED)
                    )
                )
                ->addFieldToFilter('package_type', ['eq' => $packageId]);
            $collection->setPageSize(self::BATCH_SIZE);
            $currentPage = 1;
            $pages = $collection->getLastPageNumber();
            // Disabling indexing on mysql table to speed up import
            $this->getConnection()->query('ALTER TABLE ' . $this->_table . ' DISABLE KEYS;');
            $this->getConnection()->query('SET UNIQUE_CHECKS = 0;');
            $this->closeConnection();

            /**
             * Process the rules within batches to remove
             * the risk using all the allocated memory
             * only to load the rule collections
             * This way, we never load the whole
             * collection into the current memory
             */
            do {
                $collection->setCurPage($currentPage);
                $collection->load();
                /** @var Dyna_ProductMatchRule_Model_Rule $rule */
                foreach ($collection as $rule) {
                    $this->processItem($rule);
                }
                $currentPage++;
                $collection->clear();
            } while ($currentPage <= $pages);

            $this->_applyChanges();
            //reenabling indexing on mysql table after import
            $this->getConnection()->query('ALTER TABLE ' . $this->_table . ' ENABLE KEYS;');
            $this->getConnection()->query('SET UNIQUE_CHECKS = 1;');
            $this->closeConnection();
        }

        $this->end();
    }

    /**
     * Executed before processing the collection
     */
    public function start()
    {
        $this->_websites = array();
        $this->_websiteIds = array_keys(Mage::app()->getWebsites());

        foreach ($this->_websiteIds as $websiteId) {
            $this->_websites[$websiteId] = array();
            $this->_productCategories[$websiteId] = Mage::getModel('productmatchrule/rule')->getAllCategoryProducts($websiteId);
        }

        //get id of all products to check if these are still available
        $this->_allProductIds = Mage::getResourceModel('catalog/product_collection')->getAllIds();
        $this->_allCategoryIds = Mage::getResourceModel('catalog/category_collection')->getAllIds();
    }

    /**
     * Process compatibility rule by its definition
     * @param $rule Dyna_ProductMatchRule_Model_Rule
     */
    protected function processItem($rule)
    {
        $websiteIds = implode(",", $rule->getWebsiteIds());

        switch ($rule->getOperationType()) {
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (!$this->validateProductId($rule->getLeftId(), $rule->getRightId())) {
                    break;
                }
                foreach (explode(",", $websiteIds) as $websiteId) {
                    $this->_addConditional($rule->getId(), $rule->getPackageType(), $rule->getLeftId(), $rule->getRightId(), $websiteId, $rule->getSourceCollection());
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (!$this->validateProductId($rule->getLeftId()) || $this->validateCategoryId($rule->getRightId())) {
                    break;
                }
                foreach (explode(",", $websiteIds) as $websiteId) {
                    // check whether or not category belongs to this website
                    if (!isset($this->_productCategories[$websiteId][$rule->getRightId()])) {
                        break;
                    }

                    foreach ($this->_productCategories[$websiteId][$rule->getRightId()] as &$catProdId) {
                        $this->_addConditional($rule->getId(), $rule->getPackageType(), $rule->getLeftId(), $catProdId, $websiteId, $rule->getSourceCollection());
                    }
                    unset($catProdId);
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (!$this->validateCategoryId($rule->getLeftId()) || !$this->validateProductId($rule->getRightId())) {
                    break;
                }
                foreach (explode(",", $websiteIds) as $websiteId) {
                    // check whether or not category belongs to this website
                    if (!isset($this->_productCategories[$websiteId][$rule->getLeftId()])) {
                        break;
                    }
                    foreach ($this->_productCategories[$websiteId][$rule->getLeftId()] as &$catProdId) {
                        $this->_addConditional($rule->getId(), $rule->getPackageType(), $catProdId, $rule->getRightId(), $websiteId, $rule->getSourceCollection());
                    }
                    unset($catProdId);
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (!$this->validateCategoryId($rule->getLeftId(), $rule->getRightId())) {
                    break;
                }
                foreach (explode(",", $websiteIds) as $websiteId) {
                    $this->handleConditionals($rule, $websiteId, $leftProdId, $rightProdId, $rule->getSourceCollection());
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P:
                if (!$this->validateProductId($rule->getRightId())) {
                    break;
                }
                foreach (explode(",", $websiteIds) as $websiteId) {
                    $this->_addConditional($rule->getId(), $rule->getPackageType(), null, $rule->getRightId(), $websiteId, $rule->getSourceCollection());
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C:
                if (!$this->validateCategoryId($rule->getRightId())) {
                    break;
                }
                foreach (explode(",", $websiteIds) as $websiteId) {
                    // check whether or not category belongs to this website
                    if (!isset($this->_productCategories[$websiteId][$rule->getRightId()])) {
                        break;
                    }

                    foreach ($this->_productCategories[$websiteId][$rule->getRightId()] as &$catProdId) {
                        $this->_addConditional($rule->getId(), $rule->getPackageType(), null, $catProdId, $websiteId, $rule->getSourceCollection());
                    }
                    unset($catProdId);
                }
                break;
            default:
                break;
        }
    }

    /**
     * Determine whether or not current product ids are still present in products table
     * @param array ...$productIds
     * @return bool
     */
    protected function validateProductId(...$productIds)
    {
        foreach ($productIds as $productId) {
            if (!in_array($productId, $this->_allProductIds)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether or not current category ids are still present in products table
     * @param array ...$categoryIds
     * @return bool
     */
    public function validateCategoryId(...$categoryIds)
    {
        foreach ($categoryIds as $categoryId) {
            if (!in_array($categoryId, $this->_allCategoryIds)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Executed after processing the collection
     */
    public function end()
    {
        $this->_applyChanges();
        parent::end();
    }

    /**
     * Groups gathered items together by statement (INSERT/DELETE)
     * to decrease the number of statements executed on the database
     */
    protected function _applyChanges()
    {
        $this->_matches = array_reverse($this->_matches);
        while (($_row = array_pop($this->_matches)) !== null) {
            if (!count($this->_prev) || $_row[0] === $this->_prev[0][0]) {
                $this->_prev[] = $_row;
            } elseif ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultiple();
                $this->_prev = array($_row);
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        unset($_row);

        /**
         * If something remains unprocessed in the _prev array
         */
        if (count($this->_prev)) {
            if ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultiple();
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        $this->_matches = [];
        $this->_prev = [];
    }

    /**
     * Builds INSERT statements and executes them
     * Iterates over the items withing the $_prev array
     * and builds the INSERT statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function _insertMultiple()
    {
        $values = '';

        $add = array();
        $this->_prev = array_reverse($this->_prev);
        while (($_row = array_pop($this->_prev)) !== null) {
            $add[$_row[1][0]][] = array($_row[1][1], $_row[1][2], $_row[1][3], $_row[1][4], $_row[1][5]);
        }
        unset($_row);
        foreach ($add as $websiteId => &$combinations) {
            foreach ($combinations as $key => &$combination) {
                $values .= "(" . $websiteId . "," . $combination[0] . "," . $combination[1] . "," . (!is_null($combination[2]) ? $combination[2] : "NULL") . "," . $combination[3] . "," . ($combination[4] ?: "NULL") . "),";
                if (strlen($values) >= $this->_maxPacketsLength) {
                    $sql = sprintf(
                        'INSERT INTO `%s` (`website_id`,`rule_id`,`package_type`,`source_product_id`,`target_product_id`,`source_collection`) VALUES %s ON DUPLICATE KEY 
                        UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                        $this->_table,
                        trim($values, ',')
                    );
                    $this->getConnection()->query($sql);
                    $this->closeConnection();
                    unset($sql, $values);
                    $values = '';
                }
                unset($combinations[$key]);
            }
            unset($combination);
            unset($add[$websiteId]);
        }
        unset($combinations);
        unset($add);

        if ($values) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`rule_id`,`package_type`,`source_product_id`,`target_product_id`,`source_collection`) VALUES %s ON DUPLICATE KEY 
                        UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                $this->_table,
                trim($values, ',')
            );

            $this->getConnection()->query($sql);
            $this->closeConnection();
            unset($sql, $values);
        }
    }

    /**
     * @param $left
     * @param $right
     * @param $websiteId
     */
    public function _addConditional($ruleId, $packageType, $left, $right, $websiteId, $collectionSource)
    {
        $this->_matches[] = array(self::ADD_ACTION, array($websiteId, $ruleId, $packageType, $left, $right, $collectionSource));
        $this->_assertMemory();
    }

    /**
     * @param $rule Dyna_ProductMatchRule_Model_Rule
     * @param $websiteId
     * @param $leftProdId
     * @param $rightProdId
     */
    protected function handleConditionals($rule, $websiteId, &$leftProdId, &$rightProdId, $sourceCollection)
    {
        if (isset($this->_productCategories[$websiteId][$rule['right_id']]) && isset($this->_productCategories[$websiteId][$rule['left_id']])) {
            foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$rightProdId) {
                foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$leftProdId) {
                    $this->_addConditional($rule->getId(), $rule->getPackageType(), $leftProdId, $rightProdId, $websiteId, $sourceCollection);
                }
                unset($rightProdId);
            }
            unset($leftProdId);
        }

        return;
    }

}
