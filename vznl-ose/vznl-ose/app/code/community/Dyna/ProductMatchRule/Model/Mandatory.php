<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Model_Mandatory
 * @package MatchRule
 */
class Dyna_ProductMatchRule_Model_Mandatory extends Omnius_ProductMatchRule_Model_Mandatory
{
    /**
     * @param $productId
     * @param $websiteId
     * @return array|mixed
     */
    public function getMandatoryFamilies($productId, $websiteId)
    {
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $processContextId = $processContextHelper->getProcessContextId();

        $binds = array(
            ':product' => $productId,
            ':website' => $websiteId,
            ':procCont' => $processContextId,
            ':procCont2' => "%,".$processContextId. ",%",
            ':procCont3' => "%,".$processContextId,
            ':procCont4' => $processContextId.",%"
        );


        $cacheKey = sprintf('mandatory_categories_cache_%s_%s_%s', $productId, $websiteId, $processContextId);
        if ($result = $this->getCache()->load($cacheKey)) {
            return unserialize($result);
        } else {
            $sqlString = 'SELECT DISTINCT(`product_match_mandatory_index`.`category_id`)  
                            FROM `product_match_mandatory_index` 
                            WHERE `product_id` = :product AND `website_id` = :website ';

            if($processContextId) {
                $sqlString .= '  AND (`process_context_id` = :procCont OR
                                `process_context_id` LIKE :procCont2 OR
                                `process_context_id` LIKE :procCont3  OR
                                `process_context_id` LIKE :procCont4)';
            }

            $adapter = Mage::getSingleton('core/resource');
            $connection = $adapter->getConnection('core_read');
            $categories = $connection->fetchAll($sqlString, $binds, \PDO::FETCH_ASSOC);
            $result = [];
            foreach ($categories as $category) {
                $result[] = $category['category_id'];
            }
            $result = array_unique($categories);
        }

        $this->getCache()->save(
            serialize($result),
            $cacheKey,
            [Dyna_Cache_Model_Cache::CACHE_TAG],
            $this->getCache()->getTtl()
        );

        return $result;
    }
}