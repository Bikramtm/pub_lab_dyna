<?php

/**
 * Class Dyna_ProductMatchRule_Model_Rule
 * @method string getPackageType()
 * @method $this setPackageType(string $type)
 * @method int getOperationType()
 * @method $this setOperationType(int $type)
 * @method int getLeftId()
 * @method $this setLeftId(int $value)
 * @method int getRightId()
 * @method int getOverrideInitialSelectable()
 * @method string getServiceSourceExpression()
 * @method string getTargetSourceExpression()
 * @method string getOperation()
 * @method int getSourceCollection()
 * @method $this setRightId($value)
 */
class Dyna_ProductMatchRule_Model_Rule extends Dyna_ProductMatchRule_Model_CatalogRule
{
    const OP_TYPE_P2P = 1;
    const OP_TYPE_P2C = 2;
    const OP_TYPE_C2C = 3;
    const OP_TYPE_C2P = 4;
    const OP_TYPE_S2P = 5;
    const OP_TYPE_S2C = 6;
    const OP_TYPE_M2P = 7;
    const OP_TYPE_M2C = 8;
    const OP_TYPE_S2S = 9;
    const OP_TYPE_P2S = 10;
    const OP_TYPE_C2S = 11;

    const OP_NOTALLOWED = 0;
    const OP_ALLOWED = 1;
    const OP_DEFAULTED = 2;
    const OP_OBLIGATED = 3;
    const OP_DEFAULTED_OBLIGATED = 4;
    const OP_MIN_N = 5;
    const OP_MAX_N = 6;
    const OP_EQL_N = 7;
    const OP_REMOVAL_NOTALLOWED = 8;

    const ORIGIN_USER = 0;
    const ORIGIN_IMPORT = 1;

    // added to remove Dyna_Sandbox_Model_Sandbox dependency
    const UNIQUE_ID_REFERENCE = 'unique_id_replication_reference';

    public static $serviceOperationTypes = [
        self::OP_TYPE_S2P,
        self::OP_TYPE_S2C,
        self::OP_TYPE_S2S,
    ];

    protected $_maxPriority = 0;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    protected $currentRule = '';

    protected function _construct()
    {
        $this->_init('productmatchrule/rule');
    }

    /**
     * Fetches the highest priority from the product_match_rule table.
     * @return int
     */
    public function getMaxPriority()
    {
        if (!$this->_maxPriority) {
            $query = $this->getCollection()->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('MAX(priority) as max');
            $max = $query->query()->fetchAll();
            if (is_array($max) && count($max) > 0) {
                return $this->_maxPriority = (int)$max[0]['max'];
            } else {
                return $this->_maxPriority = 0;
            }
        }

        return $this->_maxPriority + 1;
    }

    /**
     * Get all service expression applicable rules
     *
     * Because the serviceAbility service returns multiple streams and that the rules need to
     * be evaluated per stream and not a collection of streams, we build one "availability" variable
     * per stream, restraining the rule context to one stream at a time, allowing conditions like
     *
     *      availability.hasTechnology('test', '1') AND availability.hasOption('option', 'test')
     *
     * that will evaluate to true ONLY if the that one stream has BOTH conditions true and avoid the case when we
     * have two streams in the context and the first condition evaluates as true on the first stream and the second
     * condition evaluates as true on the second stream, making the whole rule pass as true while it should not
     *
     * @param array $productIds
     * @param $websiteId
     * @param $packageType
     * @return array
     */
    public function loadAllowedServiceRules($productIds, $websiteId, $packageType, $returnNotAllowed = false)
    {
        $notInitiallySelectable = Mage::helper('dyna_configurator/cart')->getNotInitiallySelectableProducts($packageType);
        /** @var Dyna_Configurator_Model_Expression_Cart $cartModel */
        $cartModel = Mage::getModel('dyna_configurator/expression_cart');
        $cartModel->appendCurrentProducts($productIds ?: array());
        $debug = Mage::helper('dyna_productmatchrule')->isDebugMode();

        $sourceAllowed = [self::OP_TYPE_S2S, self::OP_TYPE_S2P, self::OP_TYPE_S2C];
        $targetAllowed = [self::OP_TYPE_P2S, self::OP_TYPE_C2S];

        $inputParams = Mage::helper('dyna_productmatchrule')->getServiceCompRulesObjects();
        $inputParams['cart'] = $cartModel;

        // separated from above object because it is needed in targeting products, not for evaluation rules
        /** @var Dyna_Configurator_Model_Expression_Catalog $catalogObject */
        $catalogObject = Mage::getModel('dyna_configurator/expression_catalog', $packageType);

        /**
         * @var $dynaCoreHelper Dyna_Core_Helper_Data
         */
        $dynaCoreHelper = Mage::helper('dyna_core');

        // split rule collections due to caching issues
        $sourceCollectionS2S = $this->getCollectionData($productIds, $websiteId, $packageType, 'source', [self::OP_TYPE_S2S]);
        $sourceCollectionS2P = $this->getCollectionData($productIds, $websiteId, $packageType, 'source', [self::OP_TYPE_S2P]);
        $sourceCollectionS2C = $this->getCollectionData($productIds, $websiteId, $packageType, 'source', [self::OP_TYPE_S2C]);

        if ($productIds) {
            $targetCollectionP2S = $this->getCollectionData($productIds, $websiteId, $packageType, 'target', [self::OP_TYPE_P2S]);
            $targetCollectionC2S = $this->getCollectionData($productIds, $websiteId, $packageType, 'target', [self::OP_TYPE_C2S]);

            $mergedCollectionsIds = array_merge(
                $sourceCollectionS2S->getAllIds(),
                $sourceCollectionS2P->getAllIds(),
                $sourceCollectionS2C->getAllIds(),
                $targetCollectionP2S->getAllIds(),
                $targetCollectionC2S->getAllIds()
            );
        } else {
            $mergedCollectionsIds = array_merge($sourceCollectionS2S->getAllIds(), $sourceCollectionS2P->getAllIds(), $sourceCollectionS2C->getAllIds());
        }

        $finalCollection = $this->getRulesByIds($mergedCollectionsIds);

        $errors = [];
        $allowed = [];
        $notAllowed = [];

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        if ($debug) {
            $this->log('===== Processing quote: ' . $quote->getId());
        }

        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");

        // Assume all cart products as allowed with prio 1
        $prioList = [
            strtolower(Dyna_Catalog_Model_ProcessContext::ACQ),
            strtolower(Dyna_Catalog_Model_ProcessContext::MIGCOC)
        ];
        if (in_array(strtolower($processContextHelper->getProcessContextCode()), $prioList) && !$quote->getIsOffer()) {
            foreach ($quote->getPackageItems($quote->getActivePackageId()) as $item) {
                $allowed[$item->getProductId()] = 1;
            }
        }

        foreach ($finalCollection as $rule) {
            try {
                if (in_array($rule->getOperationType(), $targetAllowed)) {
                    $passed = true;
                } else {
                    $ssExpression = $rule->getServiceSourceExpression();
                    $passed = false;
                    if ($dynaCoreHelper->evaluateExpressionLanguage($ssExpression, $inputParams)) {
                        $passed = true;
                    }
                }

                if ($passed) {
                    $ruleId = $rule->getId();
                    if ($debug) {
                        $type = $rule->getOperation() == static::OP_NOTALLOWED ? 'not allowed' : 'allowed';
                        $this->log(sprintf('Service %s rule evaluated to true. Id: %s, Name: %s', $type, $ruleId,
                            $rule->getRuleTitle()));
                    }
                    $products = [];
                    if (in_array($rule->getOperationType(), array_merge([static::OP_TYPE_S2S], $targetAllowed))) {
                        $targetedProductsExpression = trim($rule->getServiceTargetExpression());

                        $indexed = $dynaCoreHelper->evaluateExpressionLanguage($targetedProductsExpression, array(
                            // let the evaluator know what kind of operation is evaluating
                            'catalog' => $catalogObject,
                            'cart' => $cartModel,
                        ));
                    } else {
                        $indexed = $this->getIndexedServiceRules($ruleId, $websiteId);
                    }

                    if ($indexed && is_array($indexed)) {
                        foreach ($indexed as $product) {
                            if ($rule->getOverrideInitialSelectable() == 1 || !in_array($product,
                                    $notInitiallySelectable)) {
                                $products[$product] = $rule->getPriority();
                            }
                        }
                    }

                    if ($debug) {
                        $this->log('Products affected: ' . json_encode($products));
                    }
                    switch ($rule->getOperation()) {
                        case static::OP_ALLOWED:
                        case static::OP_DEFAULTED:
                        case static::OP_DEFAULTED_OBLIGATED:
                        case static::OP_OBLIGATED:
                            $allowed = self::mergeArray($allowed, $products);
                            break;
                        case static::OP_NOTALLOWED:
                            $notAllowed = self::mergeArray($notAllowed, $products);
                            break;
                        default:
                            continue;
                    }
                }
            } catch (Exception $e) {
                // Error
                $errors[$ssExpression] = '[Rule ID = ' . $rule->getId() . '] ' . $e->getMessage();
            }
        }

        if (count($errors) > 0) {
            $this->log('[ERROR] Found ' . count($errors) . ' errors within the rules', Zend_Log::NOTICE);
        }

        $messages = '';
        foreach ($errors as $key => $value) {
            $messages = sprintf("%s [RULE] \n Expression: %s \n Message: %s \n", $messages, $key, $value);
        }

        if (strlen($messages) > 0) {
            $this->log($messages, Zend_Log::NOTICE);
        }

        ksort($allowed);
        ksort($notAllowed);
        if ($debug) {
            $this->log('Products allowed by service rules: ' . json_encode($allowed));
            $this->log('Products not allowed by service rules: ' . json_encode($notAllowed));
        }

        if ($returnNotAllowed) {
            return $notAllowed;
        } else {
            foreach ($notAllowed as $id => $prio) {
                if (isset($allowed[$id]) && $allowed[$id] <= $prio) {
                    // remove not allowed with higher prio
                    unset($allowed[$id]);
                    continue;
                }
            }
            return $allowed;
        }
    }

    protected function getRulesByIds(array $ruleIds)
    {
        $ruleIds = array_unique($ruleIds);
        sort($ruleIds);
        $cacheKey = "COMPATIBILITY_RULES_" . md5(serialize($ruleIds));
        if (!$resultSerialised = $this->getCache()->load($cacheKey)) {
            $finalCollection = $this->getCollection()
                ->addFieldToFilter('product_match_rule_id', array('in' => $ruleIds))
                ->load();

            $result = new Varien_Data_Collection();
            foreach ($finalCollection as $item) {
                $result->addItem($item);
            }
            $this->getCache()->save(serialize($result), $cacheKey, array($this->getCache()::PRODUCT_TAG), $this->getCache()->getTtl());
        } else {
            $result = unserialize($resultSerialised);
        }

        return $result;

    }

    /**
     * @param $productIds
     * @param $websiteId
     * @param $packageType
     * @param $type
     * @param $operationType
     * @return mixed
     * @throws Mage_Core_Exception
     */
    protected function getCollectionData($productIds, $websiteId, $packageType, $type, $operationType)
    {
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $processContextId = $processContextHelper->getProcessContextId();

        // no process context found if there is no package in the cart (campaign flow). default ACQ
        if ($processContextId === null) {
            $processContextId = Mage::getModel('dyna_catalog/processContext')->getProcessContextIdByCode(Dyna_Catalog_Model_ProcessContext::ACQ);
        }

        static $cache = null;
        $cacheKey = "CACHED_SERVICE_RULES_" . md5(serialize(array(
                $productIds,
                $websiteId,
                $packageType,
                $type,
                $operationType,
                $processContextId
            )));
        if (isset($cache[$cacheKey])) {
            return $cache[$cacheKey];
        }

        // caching collection in redis for avoiding heavy db usage (this collection will not change)
        if (!$finalCollectionSerialized = $this->getCache()->load($cacheKey)) {
            $ruleCollection = $this->getCollection()
                ->addFieldToFilter('operation_type', ['in' => $operationType])
                ->addFieldToFilter('main_table.operation', ['in' => [
                    static::OP_ALLOWED,
                    static::OP_DEFAULTED,
                    static::OP_DEFAULTED_OBLIGATED,
                    static::OP_OBLIGATED,
                    static::OP_NOTALLOWED
                ]]);

            if ($packageType) {
                /** @var Dyna_Package_Model_PackageType $packageTypeModel */
                $packageTypeModel = Mage::getSingleton("dyna_package/packageType")
                    ->loadByCode($packageType);
                if ($packageTypeModel->getId()) {
                    $ruleCollection->addFieldToFilter("main_table.package_type",
                        array('eq' => $packageTypeModel->getId()));
                } else {
                    Mage::throwException("Got package type value but cannot find resource in Allowed Service Rules call.");
                }
            } else {
                Mage::throwException("Empty package type in Allowed Service Rules call");
            }

            $ruleCollection->getSelect()
                ->join(array('rule_website' => 'product_match_rule_website'),
                    'rule_website.rule_id = main_table.product_match_rule_id')
                ->where('rule_website.website_id = ?', $websiteId)
                ->order('main_table.priority ASC');


            if ($processContextId) {
                $ruleCollection->getSelect()
                    ->join(array('product_match_rule_process_context' => 'product_match_rule_process_context'),
                        'product_match_rule_process_context.rule_id = main_table.product_match_rule_id')
                    ->where('product_match_rule_process_context.process_context_id = ?', $processContextId);
            }

            if ($type == 'target') {
                $ruleCollection->getSelect()
                    ->join(
                        array('pmsi' => 'product_match_service_index'),
                        'main_table.operation_type IN (' . implode(',', $operationType) . ') AND
                   main_table.product_match_rule_id = pmsi.rule_id ' .
                        (count($productIds) > 0 ? ' AND pmsi.source_product_id IN
                       (' . implode(',', $productIds) . ')' : ''),
                        ['pmsi.source_product_id' => 'source_product_id']

                    )
                    ->where('main_table.operation_type IN (' . implode(',', $operationType) . ')')
                    ->group('main_table.product_match_rule_id');
            }

            $finalCollection = new Varien_Data_Collection();
            foreach ($ruleCollection->getItems() as $item) {
                $finalCollection->addItem($item);
            }
            $cache[$cacheKey] = $finalCollection;

            $this->getCache()->save(serialize($finalCollection), $cacheKey, array($this->getCache()::PRODUCT_TAG),
                $this->getCache()->getTtl());
        } else {
            $finalCollection = unserialize($finalCollectionSerialized);
        }

        return $finalCollection;
    }

    /**
     * @param $array1
     * @param $array2
     * @return mixed
     */
    protected static function mergeArray($array1, $array2)
    {
        foreach ($array2 as $id => $prio) {
            if (!isset($array1[$id])) {
                $array1[$id] = $prio;
            } else {
                if ($array1[$id] < $prio) {
                    $array1[$id] = $prio;
                }
            }

        }
        return $array1;
    }

    /**
     * @param $ruleId
     * @param $websiteId
     * @return array
     * @internal param $hashes
     * @todo Create cache mechanism
     */
    public function getIndexedServiceRules($ruleId, $websiteId)
    {
        $products = [];
        /** @var Zend_Db_Adapter_Abstract $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $tableName = 'product_match_service_index';

        if ($ruleId) {
            $sql = "SELECT DISTINCT(target_product_id)
                FROM " . $tableName . " 
                WHERE rule_id=:ruleId AND website_id = :id ORDER BY priority DESC";

            $products = $connection->fetchAll($sql, ['ruleId' => $ruleId, 'id' => $websiteId], Zend_Db::FETCH_COLUMN);
        }

        return $products;
    }

    /**
     * @param array $websiteIds
     * @return Mage_CatalogRule_Model_Rule|
     */
    public function setWebsiteIds($websiteIds)
    {
        $currentIds = $this->getWebsiteIds();
        $remove = array_diff($currentIds, $websiteIds);

        $add = array_diff($websiteIds, $currentIds);

        if ($remove || $add) {
            /** @var Zend_Db_Adapter_Abstract $connection */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $tableName = 'product_match_rule_website';

            if ($remove) {
                $removeIds = implode(',', $remove);
                $sql = "DELETE FROM `$tableName` WHERE rule_id = :id AND website_id IN ($removeIds)";
                $connection->query($sql, ["id" => $this->getId()]);
            }
            if ($add) {

                $str = '';
                $map["id"] = $this->getId();

                foreach ($add as $websiteId) {
                    $str .= "(:id, :website_id$websiteId), ";
                    $map["website_id$websiteId"] = $websiteId;
                }

                $str = trim($str, ', ');
                $sql = "INSERT INTO `$tableName` VALUES " . $str;
                $connection->query($sql, $map);
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getWebsiteIds()
    {
        /** @var Zend_Db_Adapter_Abstract $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $tableName = 'product_match_rule_website';

        $sql = "SELECT website_id FROM `$tableName` WHERE rule_id = :id;";

        return array_column($connection->fetchAll($sql, ['id' => $this->getId()], Zend_Db::FETCH_NUM), 0);
    }

    /**
     * @param array $contextIds
     * @return Dyna_ProductMatchRule_Model_Rule
     */
    public function setProcessContextsIds($contextIds)
    {
        $currentIds = $this->getProcessContextIds();
        $remove = array_diff($currentIds, $contextIds);
        $add = array_diff($contextIds, $currentIds);

        if ($remove || $add) {
            /** @var Zend_Db_Adapter_Abstract $connection */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $tableName = 'product_match_rule_process_context';

            if ($remove) {
                $remove = array_map(function ($n) {
                    return (int)$n;
                }, $remove);
                $removeIds = implode(',', $remove);

                $sql = "DELETE FROM `$tableName` WHERE rule_id = :id AND process_context_id IN ($removeIds)";
                $connection->query($sql, ["id" => $this->getId()]);
            }

            if ($add) {
                $str = '';

                $map["id"] = $this->getId();

                foreach ($add as $contextId) {
                    $str .= "(:id, :process_context_id$contextId), ";
                    $map["process_context_id$contextId"] = $contextId;
                }

                $str = trim($str, ', ');
                $sql = "INSERT INTO `$tableName` VALUES " . $str;

                $connection->query($sql, $map);
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getProcessContextIds()
    {
        /** @var Zend_Db_Adapter_Abstract $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $tableName = 'product_match_rule_process_context';

        $sql = "SELECT process_context_id FROM `$tableName` WHERE rule_id = :id;";

        return array_column($connection->fetchAll($sql, ['id' => $this->getId()], Zend_Db::FETCH_NUM), 0);
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * Gets a list of categories and associated products
     * @param int|null $websiteId
     * @param bool $withPaths
     * @return array
     */
    public function getAllCategoryProducts($websiteId, $withPaths = false)
    {
        $website = Mage::getModel('core/website')->load($websiteId);
        // Add store id condition to handle correctly indexer for anchor categories
        // This will get only the default store, for all stores @todo
        $storeId = $website->getDefaultStore()->getStoreId();

        $productCategories = $categoryNames = array();
        $categories = Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('*');
        foreach ($categories as $category) {
            $categoryProductIds = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId($storeId)
                ->addWebsiteFilter(array($websiteId))
                ->addCategoryFilter($category)
                ->getAllIds();
            $productCategories[$category->getId()] = $categoryProductIds;
            $path = substr($category->getPath(), 4);
            if ($withPaths && !empty($path)) {
                $categoryNames[$category->getId()] = $category->getName();
                $cats = explode('/', $path);
                unset($path);
                foreach ($cats as &$cat) {
                    $cat = $categoryNames[$cat];
                }
                $productCategories['paths'][implode('/', $cats)] = $category->getId();
                unset($cats);
            }
        }

        return $productCategories;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        if (!$this->getData('unique_id')) {
            $this->setData('unique_id', hash('sha256', (time() . uniqid(rand(0, 1000)) . __FILE__)));
        }

        return parent::_beforeSave();
    }

    /**
     * Processing object after save data
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        /**
         * Do not clean cache (heavy I/O) after each save
         */
        return $this;
    }

    /**
     *
     * @param int $id
     * @return Mage_Core_Model_Abstract
     */
    public function loadAll($id = null)
    {
        $id = $id ?: $this->getProductMatchRuleId();
        $model = $this->load($id);
        $ruleCollection = $this->getCollection()
            ->addFieldToFilter('operation_type', $model->getData('operation_type'))
            ->addFieldToFilter('right_id', $model->getData('right_id'))
            ->addFieldToFilter('operation', $model->getData('operation'))
            ->addFieldToFilter('priority', $model->getData('priority'));

        if ($model->getSourceType()) {
            // Service type expression
            $ruleCollection->addFieldToFilter('source_type', $model->getData('source_type'))
                ->addFieldToFilter('source_collection', $model->getData('source_collection'))
                ->addFieldToFilter('service_source_expression', $model->getData('service_source_expression'));
        } else {
            // Product selection
            $ruleCollection->addFieldToFilter('left_id', $model->getData('left_id'));
        }

        $ruleCollection->load();

        return $ruleCollection;
    }

    /**
     * @return int
     */
    public function getRuleOrigin()
    {
        return (int)parent::getRuleOrigin();
    }

    /**
     * Log messages to custom file: var/log/ServiceExpressionRules.log
     * @param $message
     */
    public function log($message, $level = null)
    {
        Mage::log($message, $level, 'ServiceExpressionRules.log', true);
    }

    /**
     * Returns the priorities for the specified rule ids
     * @param array $productIds
     * @return array
     */
    public function getRulesPriority($ruleIds)
    {
        $result = array();

        $rules = $this->getCollection()
            ->addFieldToFilter('product_match_rule_id', array('in' => $ruleIds))
            ->load();

        foreach ($rules as $rule) {
            $result[$rule->getProductMatchRuleId()] = $rule->getPriority();
        }

        return $result;
    }

    /**
     * Returns the value of the conditions_serialized field
     * @return string
     */
    public function getSerializedConditions()
    {
        return serialize($this->getConditions()->asArray());
    }
}
