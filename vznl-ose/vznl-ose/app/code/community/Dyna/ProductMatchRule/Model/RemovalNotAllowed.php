<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_ProductMatchRule_Model_RemovalNotAllowed extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('dyna_productmatchrule/removalNotAllowed');
    }
}
