<?php

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $installer->getTable('sales/quote_item');

$connection->addColumn($tableName, 'is_contract', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'default'   => false,
    'nullable'  => false,
    'comment'   => 'Current service line installed base flag'
));

$installer->endSetup();
