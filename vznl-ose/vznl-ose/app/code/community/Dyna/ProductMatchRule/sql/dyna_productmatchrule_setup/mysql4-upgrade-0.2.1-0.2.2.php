<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$tableName = $installer->getTable('product_match_service_index');

if (! $installer->getConnection()->isTableExists($tableName)) {
// Defining table structure via object
    $table = $installer->getConnection()
        ->newTable($tableName)
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'auto_increment' => true
            ))
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
            array(
                'unsigned' => true,
                'nullable' => false,
            ))
        ->addColumn('service_hash', Varien_Db_Ddl_Table::TYPE_CHAR, 32,
            array(
                'nullable' => false,
            ))
        ->addColumn('target_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => false,
            ))
        ->addColumn('priority', Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
            array(
                'unsigned' => true,
                'nullable' => false,
            ))
        ->addColumn('operation', Varien_Db_Ddl_Table::TYPE_SMALLINT, 2,
            array(
                'unsigned' => true,
                'nullable' => false,
            ))
        ->addColumn('operation_value', Varien_Db_Ddl_Table::TYPE_INTEGER, 10,
            array(
                'unsigned' => true,
                'nullable' => true,
            ), 'Operation value for operations min,max,eq')
        ->addForeignKey(
            $installer->getFkName('product_match_service_index_website', 'website_id', 'core/website', 'website_id'),
            'website_id', $installer->getTable('core/website'), 'website_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('product_match_service_index_product_target', 'target_product_id', 'catalog/product', 'entity_id'),
            'target_product_id', $installer->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addIndex(
            $installer->getIdxName($tableName, array('service_hash')),
            array('service_hash')
        );
    // Executing table creation object
    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
