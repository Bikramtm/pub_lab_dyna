<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$removalNotAllowed = $installer->getTable('dyna_productmatchrule/removalNotAllowed');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$connection->addColumn($removalNotAllowed, 'source_collection', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'default' => 0,
    'comment' => 'Source collection for removal not allowed rules'
));

// Drop old defaulted index and add new one with source_collection
$indexName = $connection->getIndexName($removalNotAllowed, ['website_id', 'rule_id','source_product_id', 'target_product_id'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
$connection->dropIndex($removalNotAllowed, $indexName);
$connection->addIndex($removalNotAllowed, $indexName, ['website_id', 'rule_id','source_product_id', 'target_product_id', 'source_collection'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);

$installer->endSetup();
