<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */
$this->startSetup();

/** @var Magento_Db_Adapter_Pdo_Mysql  $connection */
$connection = $this->getConnection();

$tableName = 'product_match_service_index';
$indexList = $connection->getIndexList($tableName);
$foreignKeys = $connection->getForeignKeys($tableName);
if ($connection->isTableExists($tableName) && isset($indexList['FK_PRD_MATCH_SERVICE_IDX_WS_WS_ID_CORE_WS_WS_ID'])
&& isset($indexList['FK_EC51F77D2BBCA33686678F4C37170EA4'])
&& isset($indexList['FK_8322FC0CE9AE0E0C4C6AC8390E401E6E'])
&& isset($indexList['FK_PRD_MATCH_SERVICE_IDX_PACKAGE_TYPE_CAT_PACKAGE_TYPES_ENTT_ID'])
&& !isset($indexList['RULE_ID_WEBSITE_ID'])
&& isset($foreignKeys['FK_8322FC0CE9AE0E0C4C6AC8390E401E6E'])
&& isset($foreignKeys['FK_EC51F77D2BBCA33686678F4C37170EA4'])
&& isset($foreignKeys['FK_PRD_MATCH_SERVICE_IDX_PACKAGE_TYPE_CAT_PACKAGE_TYPES_ENTT_ID'])
&& isset($foreignKeys['FK_PRD_MATCH_SERVICE_IDX_WS_WS_ID_CORE_WS_WS_ID'])) {
    // alter the service rules indexer
        $sql =
        <<<SQL
    ALTER TABLE `product_match_service_index`
      DROP INDEX `FK_PRD_MATCH_SERVICE_IDX_WS_WS_ID_CORE_WS_WS_ID`,
      DROP INDEX `FK_EC51F77D2BBCA33686678F4C37170EA4`,
      DROP INDEX `FK_8322FC0CE9AE0E0C4C6AC8390E401E6E`,
      DROP INDEX `FK_PRD_MATCH_SERVICE_IDX_PACKAGE_TYPE_CAT_PACKAGE_TYPES_ENTT_ID`,
      ADD  INDEX `RULE_ID_WEBSITE_ID` (`rule_id`, `website_id`),
      DROP FOREIGN KEY `FK_8322FC0CE9AE0E0C4C6AC8390E401E6E`,
      DROP FOREIGN KEY `FK_EC51F77D2BBCA33686678F4C37170EA4`,
      DROP FOREIGN KEY `FK_PRD_MATCH_SERVICE_IDX_PACKAGE_TYPE_CAT_PACKAGE_TYPES_ENTT_ID`,
      DROP FOREIGN KEY `FK_PRD_MATCH_SERVICE_IDX_WS_WS_ID_CORE_WS_WS_ID`;
SQL;

    $connection->query($sql);
}

if (!$connection->isTableExists('product_match_whitelist_index_process_context')) {
// create a process context table for whitelist indexer
    $sql =
        <<<SQL
CREATE TABLE `product_match_whitelist_index_process_context` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `process_context_ids` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8
SQL;

    $connection->query($sql);
}

if ($connection->isTableExists('product_match_whitelist_index_process_context')) {
    // populate the process context table with distinct values from whitelist indexer
    $sql =
    <<<SQL
    insert into product_match_whitelist_index_process_context(process_context_ids) SELECT DISTINCT(process_context_id) FROM product_match_whitelist_index; 
SQL;

    $connection->query($sql);
}

if ($connection->isTableExists('product_match_whitelist_index') && !$connection->tableColumnExists('product_match_whitelist_index', 'process_context_ids')) {
// add the combination reference table to whitelist
    $sql =
        <<<SQL
ALTER TABLE `product_match_whitelist_index` ADD COLUMN `process_context_ids` TINYINT(1) NULL AFTER `process_context_id`;  
SQL;

    $connection->query($sql);
}

// move process context data from whitelist to the referenced table
$sql =
<<<SQL
UPDATE product_match_whitelist_index ix
LEFT JOIN product_match_whitelist_index_process_context pc
ON ix.`process_context_ids` = pc.`process_context_ids`
SET ix.`process_context_ids` = pc.`entity_id`
SQL;

$connection->query($sql);

$tableName = 'product_match_whitelist_index';
$indexList = $connection->getIndexList($tableName);
if ($connection->isTableExists($tableName)
&& $connection->tableColumnExists($tableName, 'whitelist_id')
&& $connection->tableColumnExists($tableName, 'process_context_id')
&& isset($indexList['FK_EC51F77D2BBCA33686678F4C37170EA4'])
&& isset($indexList['IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_SOURCE_PRD_ID'])
&& isset($indexList['IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_TARGET_PRD_ID'])) {
// drop unnecessarily columns and indexes and add process_context_ids in index
    $sql =
        <<<SQL
ALTER TABLE `product_match_whitelist_index`
  DROP COLUMN `whitelist_id`,
  DROP COLUMN `process_context_id`,
  DROP PRIMARY KEY,
  DROP INDEX `IDX_PRD_MATCH_WHITELIST_UNIQUE_ID_ENTRY`,
  DROP INDEX `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_SOURCE_PRD_ID`,
  DROP INDEX `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_TARGET_PRD_ID`;
SQL;

    $connection->query($sql);
}
$indexList = $connection->getIndexList($tableName);
if ($connection->isTableExists($tableName)
    && !isset($indexList['IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_SOURCE_PRD_ID'])
    && !isset($indexList['IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_TARGET_PRD_ID'])) {
    $sql =
        <<<SQL
ALTER TABLE `product_match_whitelist_index`
  ADD  INDEX `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_SOURCE_PRD_ID` (`website_id`, `package_type`, `source_product_id`, `process_context_ids`, `source_collection`),
  ADD  INDEX `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_TARGET_PRD_ID` (`website_id`, `package_type`, `target_product_id`, `process_context_ids`, `source_collection`);
SQL;

    $connection->query($sql);
}

$this->endSetup();
