<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$defaultedIndex = $installer->getTable('productmatchrule/defaulted');
$whitelistIndex = $installer->getTable('productmatchrule/matchRule');
/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

try {
// Drop old defaulted index and add new one with source_collection
    $indexName = "IDX_DEFAULTED_WEBSITE_LEFT_RIGHT_UNIQUE_INDEXER";
    $connection->dropIndex($defaultedIndex, $indexName);

// Add new index to contain source_collection
    $indexName = $connection->getIndexName($defaultedIndex, ['website_id', 'source_product_id', 'target_product_id'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
    $connection->addIndex($defaultedIndex, $indexName, ['website_id', 'source_product_id', 'target_product_id', 'source_collection'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);

// Drop old whitelist index and add new one with source_collection
    $indexName = "IDX_WEBSITE_PACKAGETYPE_LEFT_RIGHT_OVERRIDE_UNIQUE_INDEXER";
    $connection->dropIndex($whitelistIndex, $indexName);
} catch (Exception $e) {
    Mage::logException($e);
    echo 'An error occurred while performing upgrade script mysql4-upgrade-0.2.25-0.2.26.php';
}
$installer->endSetup();
