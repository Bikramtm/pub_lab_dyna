<?php
// Adds indexes to product_match_whitelist_index to speed it up

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $installer->getTable('productmatchrule/matchRule');

// Get all existing indexes to avoid duplicates
$indexList = $connection->getIndexList($tableName);

$oldIndexName = 'IDX_WEBSITE_LEFT_RIGHT_UNIQUE_INDEXER';

$indexName = 'IDX_WEBSITE_LEFT_RIGHT_OVERRIDE_UNIQUE_INDEXER';

if (isset($indexList[$oldIndexName])) {
    $connection->dropIndex($tableName, $oldIndexName);
}
if (!isset($indexList[$indexName])) {
    $connection->addIndex($tableName,
        $indexName,
        array('website_id', 'source_product_id', 'target_product_id', 'override_initial_selectable'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    );
}

$installer->endSetup();
