<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */
$this->startSetup();

/** @var Magento_Db_Adapter_Pdo_Mysql  $connection */
$connection = $this->getConnection();

$tableName = 'product_match_whitelist_index_process_context';
if ($connection->isTableExists($tableName) && !$connection->isTableExists('product_match_whitelist_index_process_context_set')) {
    $sql =
        <<<SQL
    RENAME TABLE `product_match_whitelist_index_process_context` TO `product_match_whitelist_index_process_context_set`;
SQL;

    $connection->query($sql);
}

$tableName = 'product_match_whitelist_index';
if ($connection->isTableExists($tableName) && $connection->tableColumnExists($tableName, 'process_context_ids')) {
    $sql =
        <<<SQL
    ALTER TABLE `product_match_whitelist_index` CHANGE `process_context_ids` `product_match_whitelist_index_process_context_set_id` TINYINT(1);
SQL;

    $connection->query($sql);
}

$tableName = 'product_match_whitelist_index';
$indexList = $connection->getIndexList($tableName);
if ($connection->isTableExists($tableName)
    && isset($indexList['IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_SOURCE_PRD_ID'])
    && isset($indexList['IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_TARGET_PRD_ID'])) {
    $sql =
        <<<SQL
ALTER TABLE `product_match_whitelist_index`
  DROP INDEX `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_SOURCE_PRD_ID`,
  DROP INDEX `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_TARGET_PRD_ID`;
SQL;

    $connection->query($sql);
}
$indexList = $connection->getIndexList($tableName);
if ($connection->isTableExists($tableName)
    && !isset($indexList['IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_SOURCE_PRD_ID'])
    && !isset($indexList['IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_TARGET_PRD_ID'])) {
    $sql =
        <<<SQL
ALTER TABLE `product_match_whitelist_index`
  ADD  INDEX `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_SOURCE_PRD_ID` (`website_id`, `package_type`, `source_product_id`, `product_match_whitelist_index_process_context_set_id`, `source_collection`),
  ADD  INDEX `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_TARGET_PRD_ID` (`website_id`, `package_type`, `target_product_id`, `product_match_whitelist_index_process_context_set_id`, `source_collection`);
SQL;

    $connection->query($sql);
}

$this->endSetup();
