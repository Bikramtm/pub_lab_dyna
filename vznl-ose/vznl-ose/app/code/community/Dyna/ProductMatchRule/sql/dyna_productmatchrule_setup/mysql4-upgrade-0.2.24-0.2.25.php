<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$defaultedIndex = $this->getTable('productmatchrule/defaulted');
$whitelistIndex = $this->getTable('productmatchrule/matchRule');
/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

// Change defaulted index table ENGINE from INNODB to MyISAM
$result = $connection->fetchRow(sprintf("SHOW TABLE STATUS WHERE name='%s'", $defaultedIndex));
if ($result['Engine'] !== 'MyISAM') {
    $foreignKeys = $connection->getForeignKeys($defaultedIndex);
    $installer->run(sprintf("TRUNCATE TABLE %s", $defaultedIndex));

    foreach ($foreignKeys as $foreignKey) {
        $connection->dropForeignKey($defaultedIndex, $foreignKey['FK_NAME']);
    }

    // Indexer is readonly, MyIsam has better insert performance
    $installer->run(sprintf("ALTER TABLE %s ENGINE=MyISAM;", $defaultedIndex));
}


// Drop old defaulted index and add new one with source_collection
$indexName = $connection->getIndexName($defaultedIndex, ['website_id', 'source_product_id', 'target_product_id'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
$connection->dropIndex($defaultedIndex, $indexName);
$connection->addIndex($defaultedIndex, $indexName, ['website_id', 'source_product_id', 'target_product_id', 'source_collection'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);

// Drop old whitelist index and add new one with source_collection
$indexName = $connection->getIndexName($whitelistIndex, ['website_id', 'package_type', 'source_product_id', 'target_product_id', 'override_initial_selectable'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
$connection->dropIndex($whitelistIndex, $indexName);
$connection->addIndex($whitelistIndex, $indexName, ['website_id', 'package_type', 'source_product_id', 'target_product_id', 'override_initial_selectable', 'source_collection'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);

$installer->endSetup();
