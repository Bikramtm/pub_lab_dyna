<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$tableName = $this->getTable('dyna_productmatchrule/removalNotAllowed');

if (!$this->getConnection()->isTableExists($tableName)) {
    $table = $this->getConnection()
        ->newTable($this->getTable($tableName));
    $table->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'auto_increment' => true
            ))
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ))
        ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'nullable' => true,
                'default' => null,
            ), 'The rule id from which this entry comes')
        ->addColumn('package_type', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'nullable' => true,
                'default' => null,
            ), 'The package type to which current rule applies')
        ->addColumn('source_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => true,
                'primary' => false
            ))
        ->addColumn('target_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => true,
                'primary' => false
            ))
        ->addForeignKey(
            // $priTableName, $priColumnName, $refTableName, $refColumnName
            $this->getFkName($table->getName(), 'website_id', 'core/website', 'website_id'),
            'website_id', $this->getTable('core/website'), 'website_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $this->getFkName($table->getName(), 'source_product_id', 'catalog/product', 'entity_id'),
            'source_product_id', $this->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $this->getFkName($table->getName(), 'target_product_id', 'catalog/product', 'entity_id'),
            'target_product_id', $this->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $this->getFkName($table->getName(), 'rule_id', $this->getTable("productmatchrule/rule"), 'product_match_rule_id'),
            'rule_id', $this->getTable('productmatchrule/rule'), 'product_match_rule_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $this->getFkName($table->getName(), 'package_type', $this->getTable("package/type"), 'entity_id'),
            'package_type', $this->getTable('package/type'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addIndex(
            $this->getIdxName($table->getName(), array('website_id', 'rule_id', 'source_product_id', 'target_product_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('website_id', 'rule_id', 'source_product_id', 'target_product_id'),
            array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
        )
    ;

    //Executing table creation object
    $this->getConnection()->createTable($table);
}
