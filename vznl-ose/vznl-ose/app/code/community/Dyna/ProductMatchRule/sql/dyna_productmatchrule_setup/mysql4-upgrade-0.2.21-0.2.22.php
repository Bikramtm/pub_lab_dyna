<?php

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $installer->getTable('productmatchrule/matchRule');

$result = $connection->fetchRow(sprintf("SHOW TABLE STATUS WHERE name='%s'", $tableName));
if ($result['Engine'] !== 'MyISAM') {
    $foreignKeys = $connection->getForeignKeys($tableName);
    $installer->run(sprintf("TRUNCATE TABLE %s", $tableName));

    foreach ($foreignKeys as $foreignKey) {
        $connection->dropForeignKey($tableName, $foreignKey['FK_NAME']);
    }

    // Indexer is readonly, MyIsam has better insert performance
    $installer->run(sprintf("ALTER TABLE %s ENGINE=MyISAM;", $tableName));
}

$installer->endSetup();
