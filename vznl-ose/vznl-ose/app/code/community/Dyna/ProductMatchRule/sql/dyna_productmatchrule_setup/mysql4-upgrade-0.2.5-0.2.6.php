<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$productMatchDefaulted = $this->getTable('productmatchrule/defaulted');
$quoteItem = $this->getTable('sales/quote_item');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$connection->addColumn($productMatchDefaulted, 'obligated', array(
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'default' => 0,
    'comment' => 'Obligated flag for defaulted rules'
));

$connection->addColumn($quoteItem, 'is_obligated', array(
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'default' => 0,
    'comment' => 'Obligated flag for defaulted items'
));

$installer->endSetup();
