<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$defaultedIndex = $this->getTable('productmatchrule/defaulted');
$whitelistIndex = $this->getTable('productmatchrule/matchRule');
/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$connection->addColumn($defaultedIndex, 'source_collection', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'default' => 0,
    'comment' => 'Source collection for defaulted rules'
));


$connection->addColumn($whitelistIndex, 'source_collection', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'default' => 0,
    'comment' => 'Source collection for defaulted rules'
));

$installer->endSetup();
