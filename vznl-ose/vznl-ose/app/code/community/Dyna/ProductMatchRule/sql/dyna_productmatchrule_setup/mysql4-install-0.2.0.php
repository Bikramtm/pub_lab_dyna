<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$table = $this->getTable('product_match_rule');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$connection->addColumn($table, 'source_type', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'unsigned' => true,
    'after' => 'operation',
    'comment' => 'Product selection or Service.',
));
$connection->addColumn($table, 'service_source_expression', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'after' => 'source_type',
    'comment' => 'This expression will be evaluated based on response of the indicated service.',
));
$connection->addColumn($table, 'source_collection', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'after' => 'service_source_expression',
    'unsigned' => true,
    'comment' => 'Inventory, current quote or difference between inventory and current quote.',
));
$connection->addColumn($table, 'lifecycle', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'unsigned' => true,
    'after' => 'source_collection',
    'comment' => 'Determines in which part of the lifecycle (what scenario) the compatibility rule is active.'
));

$installer->endSetup();
