<?php
class Dyna_ProductMatchRule_Helper_ArrayRuleCache
{
    protected $websiteBits= 4;
    protected $maxWebSiteLength;

    protected $cache;

    public function __construct()
    {
        $this->maxWebSiteLength  = 2 * $this->websiteBits - 1;
    }

    public function setWebsiteBits($bits)
    {
        $this->websiteBits = $bits;
        $this->maxWebSiteLength  = 2 * $this->websiteBits - 1;
    }

    public function set($key, $value)
    {
        $this->cache[$key] = $value;
    }

    public function get($key)
    {
        return $this->cache[$key] ?? false;
    }

    /**
     * Generate a 64 bit key from the ids to improve the memory consumption of the array
     * If the key is numeric, PHP won't have to store its string representation
     * @param $websiteId
     * @param $leftID
     * @param $rightID
     * @return int
     * @throws Exception
     */
    public function buildKey($websiteId, $leftID, $rightID)
    {
        $websiteId = (int)$websiteId;
        $leftID = (int)$leftID;
        $rightID = (int)$rightID;
        if($rightID > (PHP_INT_MAX >> $this->websiteBits)) {
            throw new Exception(sprintf('Invalid right id. Max supported value for array backend is %s', PHP_INT_MAX >> $this->websiteBits));
        }
        if($websiteId > $this->maxWebSiteLength ) {
            throw new Exception(sprintf('Invalid website id. Max bits %s', $this->websiteBits));
        }

        return ($leftID << 32) | (($rightID << $this->websiteBits) | $websiteId);
    }

    public function clear()
    {
        $this->cache = [];
    }
}
