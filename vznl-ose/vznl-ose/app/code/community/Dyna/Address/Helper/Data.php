<?php

class Dyna_Address_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Perform serviceability check for migration to dsl at another address
     * @param array $address
     * @return string
     */
    public function getMigrationAvailabilityForAddress(array $address)
    {
        //@todo remove hardcoded check and use services or stubs
        if ($address['city'] == "Berlin") {
            return [
                'cable' => 1,
                'dsl' => 1,
                'addressId' => 22355,
            ];
        } elseif ($address['city'] == "Frankfurt") {
            return [
                'cable' => 0,
                'dsl' => 1,
                'addressId' => 22356,
            ];
        } else {
            return [
                'cable' => 0,
                'dsl' => 0,
                'addressId' => 22352,
            ];
        }
    }

    public function warnAboutAddressChanged($postData)
    {
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');

        /** @var Dyna_Address_Model_Storage $sessionAddress */
        $sessionAddress = Mage::getSingleton('dyna_address/storage');

        $oldServiceAbility = $sessionAddress->getServiceAbility();
        $sameTasiAtNewAddress = $this->sameTasiAtNewAddress($postData, $sessionAddress->getAddress());

        if($packageHelper->hasServiceAbilityRequiredPackages()
            && $oldServiceAbility
            && isset($oldServiceAbility['address_id'])
            && (($oldServiceAbility['address_id'] != $postData['addressId']) || !$sameTasiAtNewAddress)
        ) {
            return true;
        }

        return false;
    }

    /**
     * Checks if Google Api search is enabled
     * @return mixed
     */
    public function hasGoogleAutocomplete()
    {
        /** @var  $googleAutocomplete */
        $googleAutocomplete = Mage::getStoreConfig('omnius_service/autocompletion_address');
        if($googleAutocomplete['enabled'] == 1 && !empty($googleAutocomplete['service_key'] )){
            return $googleAutocomplete;
        }
        return false;
    }

    private function sameTasiAtNewAddress($newAddress, $oldAddress)
    {
        if(!empty($newAddress['tasi']) || !empty($newAddress['component-ctn'])) {
            if(!empty($newAddress['tasi'])) {
                return $newAddress['tasi'] === $oldAddress['tasi'];
            }
            if(!empty($newAddress['component-ctn'])) {
                return $newAddress['component-ctn'] === $oldAddress['component-ctn'];
            }
        }
        if(!empty($oldAddress['tasi']) || !empty($oldAddress['component-ctn'])) {
            return false;
        }

        return true;
    }
}
