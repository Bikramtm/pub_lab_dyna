<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_LoginSecurityDE_Model_Observer extends Dyna_LoginSecurity_Model_Observer
{
    /**
     * Check if the user is blocked
     *
     * @param Varien_Event_Observer $observer
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function authenticate_before(Varien_Event_Observer $observer)
    {
        // check if the Ip is blocked
        $helper = Mage::helper('loginsecurity');
        $enableUserBlocked = Mage::getStoreConfig('security_options/messages/enable_user_blocking');

        $data = $observer->getData();

        $username = isset($data['login']) ? $data['login'] : (isset($data['username']) ? $data['username'] : '');
        $section = isset($data['section']) ? $data['section'] : '';

        $details = ['enableUserBlocked' => $enableUserBlocked, 'username' => $username, 'section' => $section];

        $banMode = Mage::getStoreConfig('security_options/messages/ban_mode'); // 1 = temporary, 2 = permanent
        $isBlocked = $helper->checkUserIsBlocked($details, $banMode);

        if ($isBlocked) {
            if ($banMode == Dyna_LoginSecurity_Model_System_BanMode::TEMPORARY_BAN) {
                $message = Mage::getStoreConfig('security_options/messages/temporary_blocked_account_message');
            } else {
                $message = Mage::getStoreConfig('security_options/messages/permanent_blocked_account_message');
            }

            throw Mage::exception('Mage_Core', Mage::helper('customer')->__($message));
        }
    }

    /**
     * Check if this customer account(username) is disabled from Backend / Customers / Manage Customers / Account information
     *
     * @param Varien_Event_Observer $observer
     * @throws Mage_Core_Exception
     */
    public function check_account(Varien_Event_Observer $observer)
    {
        $data = $observer->getData();
        $username = isset($data['login']) ? $data['login'] : (isset($data['username']) ? $data['username'] : '');
        if ($username) {
            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer = $customer->loadByEmail($username);

            if ($customer->getData('locked_account')) {
                throw Mage::exception(
                    'Mage_Core',
                    Mage::helper('customer')
                        ->__(Mage::getStoreConfig('security_options/messages/temporary_blocked_account_message'))
                );
            }
        }
    }
}
