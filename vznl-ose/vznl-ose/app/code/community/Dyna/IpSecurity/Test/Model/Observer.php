<?php
/**
 * @category   Dyna
 * @package    Dyna_IpSecurity
 */

class Dyna_IpSecurity_Test_Model_Observer extends EcomDev_PHPUnit_Test_Case
{
    /**
     * Search IP in Settings (IP Rules Set) test
     *
     * @test
     * @doNotIndexAll
     * @dataProvider dataProvider
     */
    public function testIsIpInList($searchIp, $ipRulesList, $expectedResult)
    {
        /* @var $testModel Dyna_IpSecurity_Model_Observer*/
        $testModel = Mage::getModel('dynaipsecurity/observer');
        $searchResult = $testModel->isIpInList($searchIp, $ipRulesList);

        $this->assertEquals($expectedResult, $searchResult);
    }

    /**
     * Allow/Deny logic test
     *
     * @test
     * @doNotIndexAll
     * @dataProvider dataProvider
     */
    public function testIsIpAllowed($searchIp, $allowIps, $blockIps, $expectedResult)
    {
        /* @var $testModel Dyna_IpSecurity_Model_Observer*/
        $testModel = Mage::getModel('dynaipsecurity/observer');
        $searchResult = $testModel->IsIpAllowed($searchIp, $allowIps, $blockIps);

        $this->assertEquals($expectedResult, $searchResult);
    }
}