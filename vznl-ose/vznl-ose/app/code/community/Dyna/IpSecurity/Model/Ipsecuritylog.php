<?php
/**
 * @category   Dyna
 * @package    Dyna_IpSecurity
 */

class Dyna_IpSecurity_Model_Ipsecuritylog extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('dynaipsecurity/ipsecuritylog');
    }
}
