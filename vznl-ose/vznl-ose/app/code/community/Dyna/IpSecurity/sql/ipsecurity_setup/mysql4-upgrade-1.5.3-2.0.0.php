<?php
/**
 * @category   Dyna
 * @package    Dyna_IpSecurity
 */

$installer = $this;
/* $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("
  ALTER TABLE `{$this->getTable('ipsecurity_log')}` ADD `last_block_rule` VARCHAR( 255 ) NOT NULL AFTER `blocked_ip`"
);

$installer->endSetup();