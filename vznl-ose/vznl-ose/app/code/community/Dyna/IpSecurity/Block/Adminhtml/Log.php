<?php
/**
 * @category   Dyna
 * @package    Dyna_IpSecurity
 */

class Dyna_IpSecurity_Block_Adminhtml_Log extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_log';
        $this->_blockGroup = 'dynaipsecurity';
        $this->_headerText = Mage::helper('dynaipsecurity')->__('IP Security log table');

        parent::__construct();
        $this->_removeButton('add');
    }
}