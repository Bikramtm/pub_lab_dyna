<?php

use Symfony\Component\ExpressionLanguage\ParsedExpression;
use Symfony\Component\ExpressionLanguage\ParserCache\ParserCacheInterface;

class Dyna_Core_Helper_ParserCache implements ParserCacheInterface
{
    const PARSER_KEY = "service_expression_rules_parser_cache";

    protected $initialKeys = 0;
    protected $cache = array();
    /** @var Dyna_Cache_Model_Cache */
    protected $dynaCache = null;

    public function __construct()
    {
        $this->cache = unserialize($this->getCache()->load($this->getKey())) ?: array();
        $this->initialKeys = $this->cache ? count($this->cache) : $this->initialKeys;

        return $this;
    }

    public function __destruct()
    {
        if ($this->initialKeys !== count($this->cache)) {
            $this->getCache()->save(serialize($this->cache), $this->getKey(), array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        return $this;
    }

    public function getKey()
    {
        return static::PARSER_KEY;
    }

    /**
     * {@inheritdoc}
     */
    public function fetch($key)
    {
        return isset($this->cache[$key]) ? $this->cache[$key] : null;
    }

    /**
     * {@inheritdoc}
     */
    public function save($key, ParsedExpression $expression)
    {
        $this->cache[$key] = $expression;
    }

    /*
     * Return dyna cache for caching parser expressions
     * @return Dyna_Cache_Model_Cache
     */
    public function getCache()
    {
        return $this->dynaCache ?: $this->dynaCache = Mage::getSingleton('dyna_cache/cache');
    }
}
