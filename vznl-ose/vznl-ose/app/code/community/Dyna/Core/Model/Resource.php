<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_Core_Model_Resource extends Mage_Core_Model_Resource
{
    protected static $tableNames = array();

    /**
     * Get resource table name, validated by db adapter
     *
     * @param   string|array $modelEntity
     * @return  string
     */
    public function getTableName($modelEntity)
    {
        $key = is_array($modelEntity) ? md5(serialize($modelEntity)) : $modelEntity;

        if (!isset(static::$tableNames[$key])) {
            static::$tableNames[$key] = parent::getTableName($modelEntity);
        }

        return static::$tableNames[$key];
    }

    /**
     *
     * @return Dyna_Cache_Model_Cache|Mage_Core_Model_Abstract
     */
    protected function getCache()
    {
        return Mage::getSingleton('dyna_cache/cache');
    }
}
