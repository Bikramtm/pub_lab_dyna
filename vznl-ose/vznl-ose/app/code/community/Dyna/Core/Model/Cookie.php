<?php
class Dyna_Core_Model_Cookie extends Omnius_Checkout_Model_Cookie
{
    /**
     * @see Mage_Core_Model_Cookie::isSecure()
     */
    public function isSecure()
    {
        $helper = Mage::helper('dyna_core');
        if ($this->getStore()->isAdmin() || $helper->checkSecureFrontend()) {
            return $this->_getRequest()->isSecure();
        }
        return false;
    }
}