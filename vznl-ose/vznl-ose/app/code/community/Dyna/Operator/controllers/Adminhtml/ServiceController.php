<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once Mage::getModuleDir('controllers', 'Omnius_Operator') . DS . '/Adminhtml/ServiceController.php';

class Dyna_Operator_Adminhtml_ServiceController extends Omnius_Operator_Adminhtml_ServiceController
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu("system/operator")
            ->_addBreadcrumb(Mage::helper("adminhtml")->__("Service Providers"), Mage::helper("adminhtml")->__("Service Providers"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Catalog"));
        $this->_title($this->__("Service Providers"));

        $this->_initAction();
        $this->renderLayout();
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }
}
