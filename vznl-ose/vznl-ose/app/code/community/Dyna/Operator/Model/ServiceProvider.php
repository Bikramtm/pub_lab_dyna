<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Operator_Model_ServiceProvider extends  Omnius_Operator_Model_ServiceProvider
{
    public function validateData($dataObject)
    {
        $response = [
            'success' => true,
            'missingFields' => []
        ];

        if(!$dataObject->getCode()) {
            $response['success'] = false;
            $response['missingFields'][] = 'telcompany';
        }
        if(!$dataObject->getName()) {
            $response['success'] = false;
            $response['missingFields'][] = 'OSF GUI Name';
        }
        if(('' === $dataObject->getCableProvider()) || is_null($dataObject->getCableProvider())) {
            $response['success'] = false;
            $response['missingFields'][] = 'Cableprovider';
        }
        return $response;
    }
}
