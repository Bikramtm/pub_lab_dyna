<?php

/**
 * Class Dyna_Operator_Model_Import_ServiceProviders
 */
class Dyna_Operator_Model_Import_ServiceProvider extends Omnius_Import_Model_Import_ImportAbstract
{
    protected $_data;
    protected $_headerColumns;
    protected $_csvDelimiter = ';';
    protected $_header;
    protected $_rowNumber = 1;
    protected $_logFileName = "service_providers_import";
    protected $_logFileExtension = "log";

    const NUMBER_OF_COLUMNS = 4;

    /** @var Dyna_Import_Helper_Data $_helper */
    public $_helper;

    
    /**
     * Dyna_AgentDE_Model_Import_VodafoneShip2Stores constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
    }

    public static function importFileHeader()
    {
        return [
            "telcompany",
            "osf gui name",
            "cableprovider",
            "haftungsfreistellung_vorh"
        ];
    }

    public static function booleanHeaders()
    {
        return [
            "cableprovider",
            "haftungsfreistellung_vorh",
        ];
    }

    public function process($rawData)
    {
        $this->_rowNumber++;

        try {
            $this->setDataMappings($rawData);

            /** leave rules processing to other method */
            $this->saveServiceProviders();
            $this->_totalFileRows++;
        } catch (Exception $e) {
            $this->_skippedFileRows++;
            $this->_logError("[ERROR] Skipped row because :" . $e->getMessage());
            fwrite(STDERR, "[ERROR] Skipped row because :" . $e->getMessage());
        }
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function saveServiceProviders()
    {
        /** @var Dyna_Operator_Model_ServiceProvider $model */
        $model = Mage::getModel('dyna_operator/serviceProvider');
        $validateResult = $model->validateData(new Varien_Object($this->_data));

        if (!$validateResult['success']) {
            $this->_logError("[ERROR] Skipped row. Missing mandatory fields :" . implode(",", $validateResult['missingFields']) . " at row " . $this->_rowNumber);
        } else {
            $model->addData($this->_data);
            $model->save();
            $this->_log("Successfully imported CSV line " . $this->_rowNumber . " : " . implode('|', $this->_data));
        }

        return $this;
    }

    private function setDataMappings($data) {
        $booleanHeaders = $this->booleanHeaders();
        $mapping = Mage::helper("dyna_import/data")->getMapping('serviceProviders',['mapping']);

        foreach ($mapping as $csvHeader => $dbColumn) {
            $this->_data[$dbColumn] = $data[$csvHeader];
            if (in_array($csvHeader, $booleanHeaders)) {
                $this->_data[$dbColumn] = ('Y' == $this->_data[$dbColumn]) ? 1 : 0;
            }
        }

        return true;
    }

    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }
}
