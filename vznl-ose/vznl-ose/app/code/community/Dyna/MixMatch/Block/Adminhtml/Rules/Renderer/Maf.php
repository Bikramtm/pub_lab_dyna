<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** Class Dyna_MixMatch_Block_Adminhtml_Rules_Renderer_Maf */
class Dyna_MixMatch_Block_Adminhtml_Rules_Renderer_Maf extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Price
{
    /**
     * Render four digits maf
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $maf = $row->getMaf();

        return ($maf === null) ? '' : number_format($maf, 4, null, '');
    }

}
