<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** Class Dyna_MixMatch_Block_Adminhtml_Rules_Renderer_Price */
class Dyna_MixMatch_Block_Adminhtml_Rules_Renderer_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Price
{
    /**
     * Render four digits price
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $price = $row->getPrice();

        return ($price === null) ? '' : number_format($price, 4, null, '');
    }

}
