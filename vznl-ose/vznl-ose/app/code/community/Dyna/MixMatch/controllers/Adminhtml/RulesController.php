<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once Mage::getModuleDir('controllers', 'Omnius_MixMatch') . DS . 'Adminhtml' . DS . 'RulesController.php';

/**
 * Class Dyna_MixMatch_Adminhtml_RulesController
 */
class Dyna_MixMatch_Adminhtml_RulesController extends Omnius_MixMatch_Adminhtml_RulesController
{
    /** wee need to rewrite some methods */

    public function saveAction()
    {
        $postData = new Varien_Object($this->getRequest()->getPost());

        $websiteIds = $postData->getData('website_id');
        $targetSku = $postData->getData('target_sku');
        $sourceSku = $postData->getData('source_sku');
        $targetCategory = $postData->getData('target_category');
        $sourceCategory = $postData->getData('source_category');
        $effectiveDate = $postData->getData('effective_date');
        $expirationDate = $postData->getData('expiration_date');
        $subscriberSegment = implode($postData->getData('subscriber_segment'), ',');
        $redSalesId = $postData->getData('red_sales_id');
        $stream = $postData->getData('stream');
        $deviceSubscriptionSku = $postData->getData('device_subscription_sku');
        if ($targetSku && $sourceSku && $websiteIds) {
            try {
                if ($deviceSubscriptionSku) {
                    if (3 !== count($this->getProductCollection([$targetSku, $sourceSku, $deviceSubscriptionSku]))) {
                        Mage::throwException('Invalid SKUs given. Please make sure that the SKUs belong to existing products');
                    }
                } else {
                    if (2 !== count($this->getProductCollection([$targetSku, $sourceSku]))) {
                        Mage::throwException('Invalid SKUs given. Please make sure that the SKUs belong to existing products.');
                    }
                }
                foreach ($websiteIds as $websiteId) {
                    $model = Mage::getModel('omnius_mixmatch/price')
                        ->setTargetSku($targetSku)
                        ->setSourceSku($sourceSku)
                        ->setTargetCategory($targetCategory)
                        ->setSourceCategory($sourceCategory)
                        ->setPrice($postData->getData('price'))
                        ->setWebsiteId($websiteId)
                        ->setSubscriberSegment($subscriberSegment)
                        ->setEffectiveDate($effectiveDate)
                        ->setExpirationDate($expirationDate)
                        ->setRedSalesId($redSalesId)
                        ->setStream($stream)
                        ->save();
                }

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Rule successfully saved"));
                Mage::getSingleton("adminhtml/session")->setDynaMixMatchData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", ["id" => $model->getId()]);

                    return;
                }
                $this->_redirect("*/*/");

                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setDynaMixMatchData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", ["id" => $this->getRequest()->getParam("id")]);

                return;
            }
        }
        Mage::getSingleton("adminhtml/session")->addError('Please fill all fields');
        Mage::getSingleton("adminhtml/session")->setDynaMixMatchData($this->getRequest()->getPost());
        $this->_redirect("*/*/new");
    }

    public function uploadAction()
    {
        try {
            if ($postData = $this->getRequest()->getPost()) {
                if (!empty($_FILES['file']['tmp_name'])) {
                    $uploader = new Mage_Core_Model_File_Uploader('file');
                    $uploader->setAllowedExtensions(['xlsx','csv']);
                    $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
                    $uploader->save($path);
                    if ($uploadFile = $uploader->getUploadedFileName()) {
                        $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
                        rename($path . $uploadFile, $path . $newFilename);
                    }

                    if (isset($newFilename) && $newFilename) {
                        Mage::getModel('dyna_mixmatch/importer')->importFile($path . $newFilename, $postData['website_id'], $uploader->getFileExtension());
                        Mage::getSingleton('adminhtml/session')->addSuccess(
                            $this->__('Import file successfully parsed and imported into database')
                        );
                        $this->_redirect('*/*');
                    }
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
                );
                $this->_redirect('*/*');
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(
                $e->getMessage()
            );
            $this->_redirect('*/*');
        }
    }
}
