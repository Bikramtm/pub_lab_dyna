<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class MixMatch Helper
 */
class Dyna_MixMatch_Helper_Data extends Omnius_MixMatch_Helper_Data
{

    public function getLastImportedFileName($websiteId)
    {
        return Mage::getStoreConfig(
            Dyna_MixMatch_Model_Importer::CONFIG_PATH,
            Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId()
        );
    }
}
