<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_MixMatch_Model_Indexer_Mixmatch
 */
class Dyna_MixMatch_Model_Indexer_Mixmatch extends Mage_Index_Model_Indexer_Abstract
{
    const BATCH_SIZE = 500;

    protected $logfile = 'mixmatch_indexer.log';

    /** @var int */
    protected $flushOnMemory;

    /** @var int */
    protected $maxPacketsLength;

    /** @var array */
    protected $items = [];

    /** @var Magento_Db_Adapter_Pdo_Mysql */
    protected $connection;

    /** @var array */
    protected $categoryCache;
    /** @var string */
    protected $table;
    /** @var Dyna_Catalog_Model_Category */
    protected $categoryModel = null;


    /**
     * Initiate indexer
     */
    public function __construct()
    {
        parent::__construct();

        $this->table = (string) Mage::getResourceSingleton('dyna_mixmatch/priceIndex')->getMainTable();
        $this->connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        /** @var Dyna_Core_Helper_System $coreHelper */
        $systemHelper = Mage::helper('dyna_core/System');
        $this->flushOnMemory = $systemHelper->getMemoryLimit();
        $this->maxPacketsLength = $systemHelper->getMysqlMaxAllowedPacketSize();

        $this->_init('omnius_mixmatch/indexer_mixmatch');
    }

    /**
     * Get Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return 'Product Mixmatches';
    }

    /**
     * Get Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return 'Mixmatches that determine if a product has a different price when combined with other products';
    }

    /**
     * @throws Exception
     */
    public function reindexAll()
    {
        try {
            $this->connection->query(sprintf('TRUNCATE TABLE `%s`;', $this->table));

            $this->runBefore();

            /** @var Omnius_MixMatch_Model_Resource_Price_Collection $collection */
            $collection = Mage::getResourceModel('omnius_mixmatch/price_collection');
            $collection->setPageSize(self::BATCH_SIZE);

            $currentPage = 1;
            $pages = $collection->getLastPageNumber();
            do {
                $collection->setCurPage($currentPage);
                foreach ($collection as $mixMatch) {
                    $this->processItem($mixMatch);
                }
                $currentPage++;
                $collection->clear();
            } while ($currentPage <= $pages);

            $this->runAfter();
        } catch (Exception $e) {
            $this->log($e->getMessage());
        }
    }

    /**
     * @param $mixMatch Omnius_MixMatch_Model_Price
     */
    protected function processItem($mixMatch)
    {
        try {
            if ($mixMatch->getTargetCategory() || $mixMatch->getSourceCategory()) {
                $sourceProducts = $mixMatch->getSourceCategory() ? $this->getProductsForCategory($mixMatch->getSourceCategory(), $mixMatch->getWebsiteId()) : [$mixMatch->getSourceSku()];
                $targetProducts = $mixMatch->getTargetCategory() ? $this->getProductsForCategory($mixMatch->getTargetCategory(), $mixMatch->getWebsiteId()) : [$mixMatch->getTargetSku()];
                foreach ($sourceProducts as $sourceProduct) {
                    foreach ($targetProducts as $targetProduct) {
                        $item = Mage::getModel('dyna_mixmatch/priceIndex')->addData($mixMatch->getData());
                        $item->setSourceSku($sourceProduct)->setTargetSku($targetProduct);
                        $this->items[] = $this->getEntryData($item);
                    }
                }
                unset($item);
                unset($sourceProducts);
                unset($targetProducts);
            } else {
                $item = Mage::getModel('dyna_mixmatch/priceIndex')->addData($mixMatch->getData());
                $this->items[] = $this->getEntryData($item);
            }

            $this->assertMemory();
        } catch (Exception $e) {
            $this->log($e->getMessage());
        }
    }

    /**
     * @param string $message
     * @param int $level
     */
    protected function log($message, $level = Zend_Log::ERR)
    {
        Mage::log($message, $level, $this->logfile, true);
    }

    protected function _construct()
    {
        $this->_init('dyna_mixmatch/indexer_mixmatch');
    }

    /**
     * Register indexer required data inside event object
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
    }

    /**
     * Process event based on event state data
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
    }

    /**
     * Retrieve the list of products currently in a category (by name)
     *
     * @param string $categoryName
     * @return array
     */
    protected function getProductsForCategory($categoryName, $websiteId)
    {
        $result = [];
        $storeId = Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId();
        if ($categoryName && $storeId) {
            $key = serialize([$categoryName, $storeId]);
            if (isset($this->categoryCache[$key])) {
                $result = $this->categoryCache[$key];
            } else {
                // current separator is ->
                if (($category = $this->getCategoryModel()->getCategoryByNamePath($categoryName, "->")) && $category->getId()) {
                    /** @var Mage_Catalog_Model_Resource_Product_Collection $products */
                    $products = Mage::getResourceModel('catalog/product_collection');
                    $products->addCategoryFilter($category)
                        ->addStoreFilter($storeId);
                    $result = $this->categoryCache[$key] = $products->getColumnValues('sku');
                } else {
                    $result = $this->categoryCache[$key] = [];
                }
            }
        }

        return $result;
    }

    /**
     * Methods that will be ran when a batch of rows is completed
     *
     * @return $this
     */
    protected function runAfter()
    {
        //reenabling indexing on mysql table after import
        $this->applyChanges();
        $this->connection->query('ALTER TABLE ' . $this->table . ' ENABLE KEYS;');
        $this->connection->query('COMMIT;');

        $this->closeConnection();

        return $this;
    }

    /**
     * Methods that will be ran when a batch of rows is completed
     *
     * @return $this
     */
    private function runBefore()
    {
        //disabling indexing on mysql table to speed up import
        $this->connection->query('ALTER TABLE ' . $this->table . ' DISABLE KEYS;');
        return $this;
    }

    /**
     * Invalidate the product match rules and warn the agent the match rules might be outdated
     *
     * @return $this
     */
    protected function invalidateMatchRuleIndex()
    {
        Mage::getSingleton('index/indexer')
            ->getProcessByCode('product_match_whitelist')
            ->changeStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);

        return $this;
    }

    /**
     * @return bool
     */
    protected function isMemoryExceeded()
    {
        return memory_get_usage(true) >= $this->flushOnMemory;
    }

    /**
     * Check is we approach the PHP memory
     * limit. If the current used memory exceeds
     * the current limit, all gathered data until
     * this moment will be flushed to the database
     */
    protected function assertMemory()
    {
        if ($this->isMemoryExceeded()) {
            $this->applyChanges();
        }
    }

    /**
     * Groups gathered items together by statement (INSERT/DELETE)
     * to decrease the number of statements executed on the database
     */
    protected function applyChanges()
    {
        if (count($this->items)) {
            $this->insertMultiple();
        }
        $this->items = [];
    }

    /**
     * Builds INSERT statements and executes them
     * Iterates over the items withing the $matches array
     * and builds the INSERT statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function insertMultiple()
    {
        $values = '';

        foreach ($this->items as $key => &$combination) {
            $values .= vsprintf('(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s),', $combination);
            if (strlen($values) >= $this->maxPacketsLength) {
                $this->persistData($values);
                $values = '';
            }
            unset($this->items[$key]);
        }
        unset($combination);

        if ($values) {
            $this->persistData($values);
            unset($values);
        }
    }

    /**
     * @return float
     */
    protected function getMachineMaxMemory()
    {
        if (!is_readable("/proc/meminfo")) {
            return 1024 * 1024 * 1024;
        }

        $data = explode(PHP_EOL, file_get_contents("/proc/meminfo"));
        $memInfo = [];
        foreach ($data as $line) {
            $values = explode(":", $line);
            if (count($values) == 2) {
                list($key, $val) = $values;
                $memInfo[$key] = trim($val);
            } else {
                $memInfo[$line] = null;
            }
        }
        if (empty($memInfo['MemTotal'])) {
            $memInfo['MemTotal'] = 0;
        }
        $maxMemory = 1024 * ((int) trim(trim($memInfo['MemTotal'], 'kb')));

        return max($maxMemory * 0.2, 1024 * 1024 * 1024);
    }

    /**
     * @param $mixMatch Mage_Core_Model_Abstract
     * @return array
     */
    protected function getEntryData($mixMatch):array
    {
        // Ensure a dates are indexed
        if (!$mixMatch->getEffectiveDate()) {
            $dateTime = new DateTime('now -1 day');
            $mixMatch->setEffectiveDate($dateTime->format('Y-m-d 00:00:00'));
        }
        if (!$mixMatch->getExpirationDate()) {
            $dateTime = new DateTime('now +10years');
            $mixMatch->setExpirationDate($dateTime->format('Y-m-d 00:00:00'));
        }
        unset($dateTime);

        return [
            $mixMatch->getDataForSql('source_sku'),
            $mixMatch->getDataForSql('priority'),
            $mixMatch->getDataForSql('target_sku'),
            $mixMatch->getDataForSql('price'),
            $mixMatch->getDataForSql('maf'),
            $mixMatch->getDataForSql('website_id'),
            $mixMatch->getDataForSql('red_sales_id'),
            $mixMatch->getDataForSql('subscriber_segment'),
            $mixMatch->getDataForSql('effective_date'),
            $mixMatch->getDataForSql('expiration_date'),
            $mixMatch->getDataForSql('entity_id'),
        ];
    }

    /**
     * @param $values
     */
    protected function persistData($values)
    {
        $sql = sprintf(
            'INSERT INTO `%s` (`source_sku`,`priority`,`target_sku`,`price`,`maf`,`website_id`,`red_sales_id`,`subscriber_segment`,`effective_date`,`expiration_date`, `rule_id`) VALUES %s ON DUPLICATE KEY UPDATE `maf`=VALUES(`maf`), `price`=VALUES(`price`)' . PHP_EOL,
            $this->table,
            trim($values, ',')
        );

        $this->connection->query($sql);
        unset($sql);
    }

    /**
     * Get a category instance (used for building category id based on path)
     * @return Dyna_Catalog_Model_Category
     */
    protected function getCategoryModel()
    {
        if (!$this->categoryModel) {
            $this->categoryModel = Mage::getModel('catalog/category');
        }

        return $this->categoryModel;
    }

    protected function closeConnection() {
        $this->connection = null;
    }
}
