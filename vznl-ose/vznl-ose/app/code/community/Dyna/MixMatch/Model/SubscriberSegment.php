<?php

/**
 * Class Dyna_MixMatch_Model_SubscriberSegment
 */
class Dyna_MixMatch_Model_SubscriberSegment extends Mage_Core_Model_Abstract
{
    const SUBSCRIBER_SEGMENT_VALUE_B = 'B';
    const SUBSCRIBER_SEGMENT_VALUE_G = 'G';
    const SUBSCRIBER_SEGMENT_VALUE_P = 'P';
    const SUBSCRIBER_SEGMENT_VALUE_R = 'R';
    const SUBSCRIBER_SEGMENT_VALUE_S = 'S';
    const SUBSCRIBER_SEGMENT_VALUE_T = 'T';
    const SUBSCRIBER_SEGMENT_VALUE_ALL = '*';
    const SUBSCRIBER_SEGMENT_VALUE_EMPTY = 'EMPTY';

    /**
     * Retrieve subscriber segment values for form
     *
     * @return array
     */
    public function getValuesForForm()
    {
        return [
            [
                'value' => self::SUBSCRIBER_SEGMENT_VALUE_B,
                'label' => self::SUBSCRIBER_SEGMENT_VALUE_B
            ],
            [
                'value' => self::SUBSCRIBER_SEGMENT_VALUE_G,
                'label' => self::SUBSCRIBER_SEGMENT_VALUE_G
            ],
            [
                'value' => self::SUBSCRIBER_SEGMENT_VALUE_P,
                'label' => self::SUBSCRIBER_SEGMENT_VALUE_P
            ],
            [
                'value' => self::SUBSCRIBER_SEGMENT_VALUE_R,
                'label' => self::SUBSCRIBER_SEGMENT_VALUE_R
            ],
            [
                'value' => self::SUBSCRIBER_SEGMENT_VALUE_S,
                'label' => self::SUBSCRIBER_SEGMENT_VALUE_S
            ],
            [
                'value' => self::SUBSCRIBER_SEGMENT_VALUE_T,
                'label' => self::SUBSCRIBER_SEGMENT_VALUE_T
            ]
        ];
    }

    /**
     * Retrieve subscriber segment value as array
     *
     * @return array
     */
    public function getAllValues()
    {
        return [
            self::SUBSCRIBER_SEGMENT_VALUE_B,
            self::SUBSCRIBER_SEGMENT_VALUE_G,
            self::SUBSCRIBER_SEGMENT_VALUE_P,
            self::SUBSCRIBER_SEGMENT_VALUE_R,
            self::SUBSCRIBER_SEGMENT_VALUE_S,
            self::SUBSCRIBER_SEGMENT_VALUE_T
        ];
    }
}
