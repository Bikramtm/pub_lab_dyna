<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */
/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();

// not doing it Magento way because it doesn't like tinyints
if (!$this->getConnection()->tableColumnExists($this->getTable('dyna_mixmatch/priceIndex'), "priority")) {
$sql =
<<<SQL
ALTER TABLE `dyna_mixmatch_flat` ADD COLUMN `priority` TINYINT(3) UNSIGNED DEFAULT 0  NOT NULL COMMENT 'Mixmatch priority' AFTER `source_sku`;
SQL;
    $this->getConnection()->query($sql);
}

if (!$this->getConnection()->tableColumnExists($this->getTable('omnius_mixmatch/mixmatch'), "priority")) {
$sql =
<<<SQL
ALTER TABLE `catalog_mixmatch` ADD COLUMN `priority` TINYINT(3) UNSIGNED DEFAULT 0  NOT NULL COMMENT 'Mixmatch priority' AFTER `red_sales_id`;
SQL;
    $this->getConnection()->query($sql);
}

$this->endSetup();
