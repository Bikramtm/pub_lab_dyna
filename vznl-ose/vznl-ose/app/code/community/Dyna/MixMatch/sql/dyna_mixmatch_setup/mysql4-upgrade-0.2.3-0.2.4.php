<?php
/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

$tableName = $this->getTable('dyna_mixmatch/priceIndex');
if ($connection->tableColumnExists($tableName, 'void')) {
    $connection->changeColumn($tableName, 'void', 'void', 'varchar(255) null');
} else {
    $connection->addColumn($tableName, 'void', 'varchar(255) null');
}

$this->endSetup();
