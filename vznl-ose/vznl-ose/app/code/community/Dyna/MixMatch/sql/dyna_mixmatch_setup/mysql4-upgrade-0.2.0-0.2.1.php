<?php
/**
 * Added new columns for EffectiveDate, ExpirationDate and stream
 * See JIRA ticket OMNVFDE-129
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_mixmatch'), 'effective_date', 'datetime default null');
$connection->addColumn($this->getTable('catalog_mixmatch'), 'expiration_date', 'datetime default null');
$connection->addColumn($this->getTable('catalog_mixmatch'), 'stream', 'varchar(64) null');

$installer->endSetup();
