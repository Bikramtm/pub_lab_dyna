<?php
/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

$tableName = $this->getTable('catalog_mixmatch');
if ($connection->tableColumnExists($tableName, 'void') && !$connection->tableColumnExists($tableName, 'red_sales_id'))  {
    $connection->changeColumn($tableName, 'void', 'red_sales_id', 'varchar(255) null');
} elseif (!$connection->tableColumnExists($tableName, 'red_sales_id')) {
    $connection->addColumn($tableName, 'red_sales_id', 'varchar(255) null');
}

$tableName = $this->getTable('dyna_mixmatch_flat');
if ($connection->tableColumnExists($tableName, 'void') && !$connection->tableColumnExists($tableName, 'red_sales_id')) {
    $connection->changeColumn($tableName, 'void', 'red_sales_id', 'varchar(255) null');
} elseif (!$connection->tableColumnExists($tableName, 'red_sales_id')) {
    $connection->addColumn($tableName, 'red_sales_id', 'varchar(255) null');
}

$this->endSetup();
