<?php
/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

$tableName = $this->getTable('dyna_mixmatch/priceIndex');
if (!$this->tableExists($tableName)) {
    $table = $this->getConnection()
        ->newTable($tableName)
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true,], 'ID')
        ->addColumn('source_sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, [], 'Source SKU')
        ->addColumn('target_sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, [], 'Target SKU')
        ->addColumn('price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', ['nullable' => true,], 'Purchase Price')
        ->addColumn('maf', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', ['nullable' => true,], 'Purchase Price')
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, 5, ['unsigned' => true, 'nullable' => false,], 'Website id')
        ->addColumn('void', Varien_Db_Ddl_Table::TYPE_SMALLINT, 5, ['unsigned' => true, 'nullable' => false,])
        ->addColumn('subscriber_segment', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, ['nullable' => true])
        ->addColumn('effective_date', Varien_Db_Ddl_Table::TYPE_DATETIME, 5, ['nullable' => false,])
        ->addColumn('expiration_date', Varien_Db_Ddl_Table::TYPE_DATETIME, 5, ['nullable' => false,])
        ->addIndex($this->getIdxName('dyna_mixmatch/priceIndex', ['source_sku', 'target_sku', 'website_id', 'void', 'subscriber_segment'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            ['source_sku', 'target_sku', 'website_id', 'void', 'subscriber_segment'], ['type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE])
        ->setComment('MixMatch Prices Indexed Table');

    $this->getConnection()->createTable($table);
}

$this->endSetup();
