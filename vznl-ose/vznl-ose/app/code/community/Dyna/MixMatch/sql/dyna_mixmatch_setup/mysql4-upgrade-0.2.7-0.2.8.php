<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */
/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();

$tableName = $this->getTable('dyna_mixmatch/priceIndex');

if (!$this->getConnection()->tableColumnExists($tableName, 'rule_id')) {
    $this->getConnection()->addColumn(
        $tableName,
        'rule_id',
        'int(10) UNSIGNED NOT NULL'
    );
}

$this->endSetup();
