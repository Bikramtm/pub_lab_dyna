<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Superorder_Model_Offer
{
    protected $type = null;
    protected $quote = null;
    protected $customer = null;
    protected $superorder = null;
    protected $countryCode = null;

    /* @var Dyna_Superorder_Helper_Client $clientHelper */
    protected $clientHelper;

    /** @var Omnius_Service_Helper_Data $serviceHelper */
    protected $serviceHelper;

    // For sendDocument party name should always set to mail
    // @see OMNVFDE-1814 and https://dynalean.atlassian.net/wiki/display/OMNVFDE/I1011+-+SendDocument for mapping
    const PARTY_NAME = "mail";

    const STATUS_OFFER_SUBMITTED = 'Submitted';
    const STATUS_OFFER_EXPIRED = 'OutOfDate';
    const STATUS_OFFER_VALID = 'Valid';

    /**
     * Get session customer
     * @return Dyna_Customer_Model_Customer|Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        if (!$this->customer) {
            $this->customer = Mage::getSingleton('customer/session')->getCustomer();
        }

        return $this->customer;
    }

    protected function hasPortingPhoneNumbers($packageId,$packageType)
    {
        $portingData = array();

        // There are maximum three porting options
        for ($i = 0; $i < 3; $i++) {
            // Selected phone number is a reference incremented by one to "provider[phone_transfer_list][telephone][index-1]"
            $phoneNumber = $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][phone_entries][' . $i . '][telephone_no]');
            $phonePrefix = $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][phone_entries][' . $i . '][prefix]');
            $phoneType = $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][phone_entries][' . $i . '][type]');
            // Hardcoded as requested on item
            $phoneCountryCode = 49;
            // If no phone entry selected, do not send this info to submit order
            if (!$phoneNumber || ($phoneNumber == Mage::helper('omnius_customer')->__('New')) || ($phoneType == Dyna_Checkout_Block_Cart_Steps_SaveOther::PHONE_ENTRY_TYPE_NO_ENTRY)) {
                continue;
            }
            // There is a valid phone number so adding this to returned array
            // This is the value saved in checkout fields when New entry is selected
            $portingData[] = array(
                'CountryCode' => $phoneCountryCode,
                'LocalAreaCode' => $phonePrefix,
                'PhoneNumber' => $phoneNumber,
                'Type' => $phoneType,
            );
        }

        return $portingData;
    }

    /**
     * @return array
     */
    protected function extractPhoneBookEntry($packageId,$packageType): array
    {
        $phoneBookEntry = [];
        $phonebookChoice = $this->clientHelper->getCheckout('other[phonebook_choice][' . $packageId . '][' . $packageType . ']');
        $accountType = Dyna_Superorder_Helper_Client::getPackageType(Mage::helper('dyna_customer/customer')->getCustomerDataAccountCategory());
        $personalDetails = $this->clientHelper->getPhoneBookPersonalFields($accountType, $packageType, $packageId);

        if (!empty($phonebookChoice) &&
            $phonebookChoice != Dyna_Checkout_Block_Cart_Steps_SaveOther::PHONEBOOK_CHOICE_NO_ENTRIES &&
            $personalDetails != '') {
            $personalPhone = $this->hasPortingPhoneNumbers($packageId, $packageType);

            $phoneBookEntry = [
                'id' => '',
                'personalInfo' => [
                    'id' => '',
                    'partyType' => '',
                    'person' => [
                        'id' => '',
                        'gender' => '',
                        'initials' => ($personalDetails['FirstName'][0] ?? ''). '' . ($personalDetails['FamilyName'][0] ?? ''),
                        'firstname' => $personalDetails['FirstName'] ?? '',
                        'lastname' => $personalDetails['FamilyName'] ?? '',
                        'title' => $personalDetails['Title'] ?? '',
                        'salutation' => $this->clientHelper->getCheckout('other[extended][' . $packageId . '][' . $packageType . '][profession]'),
                        'dateOfBirth' => '',
                    ],
                    'email' => [
                        'type' => '',
                        'value' => '',
                    ],
                    'telephone' => [
                        'type' => $personalPhone['Type'] ?? '',
                        'phoneNumber' => $personalPhone['PhoneNumber'] ?? '',
                        'countryCode' => $this->countryCode ?? '',
                        'localAreaCode' => '',
                    ],
                    'address' => [
                        "type" => "",
                        "city" => "",
                        "street" => "",
                        "country" => "",
                        "zipcode" => "",
                        "addition" => "",
                        "housenumber" => "",
                    ],
                ],
                'details' => [
                    [
                    ]
                ]
            ];
        }
        return $phoneBookEntry;
    }

    /**
     * @return array
     */
    protected function extractPortingInfo($package): array
    {
        $mobilePortingPhoneNumbers = $this->clientHelper->getPortingPhoneNumbers($package, 'Mobile', true);
        $mobilePortingPhoneNumber = ($mobilePortingPhoneNumbers) ? $mobilePortingPhoneNumbers[0] : null;

        // -- porting information
        $portingInfo = [
            'id' => 0,
            'handInDatalater'       => $this->clientHelper->getCheckout('provider[change_provider]', Dyna_Superorder_Helper_Client::NP_CHANGE_PROVIDER_DATA_LATER),
            'carrierId'             => $this->clientHelper->getCheckout('provider[current_provider_type]') ?: null,
            'carrierCustomerId'     => null,
            'carrierContractCancel' => $this->clientHelper->getCarrierContractCancel($package->getType()),
            'portingDate'           => $this->clientHelper->getCheckout('provider[wish_date]') ? date("Y-m-d", strtotime($this->clientHelper->getCheckout('provider[wish_date]'))) : null,
            'portingOrderDate'      => date("Y-m-d"),
            'portAllNumbers'        => $this->clientHelper->getCheckout('provider[no_phones_transfer]', Dyna_Superorder_Helper_Client::PORTING_ALL_NUMBERS),
            'portingSubscriberAddress' => [
                'zipcode'     => $this->clientHelper->getCheckout('customer[other_address][postcode]'),
                'housenumber' => $this->clientHelper->getCheckout('customer[other_address][houseno]'),
                'addition'    => $this->clientHelper->getCheckout('customer[other_address][addition]'),
                'street'      => $this->clientHelper->getCheckout('customer[other_address][street]'),
                'city'        => $this->clientHelper->getCheckout('customer[other_address][city]'),
                'country'     => $this->countryCode,
                'type'        => '',
            ],
            'portingSubscriberContact' => [
                'id'        => '',
                'partyType' => '',
                'person' => [
                    'id'            => '',
                    'initials'      => '',
                    'firstname'     => $this->clientHelper->getCheckout('provider[other_owner][firstname]'),
                    'lastname'      => $this->clientHelper->getCheckout('provider[other_owner][lastname]'),
                    'title'         => '',
                    'salutation'    => $this->clientHelper->getCheckout('provider[other_owner][gender]'),
                    'gender'        => 1,
                    'dateOfBirth'   => '',
                ],
                'email' => [
                    'type'  => '',
                    'value' => '',
                ],
                'telephone' => [
                    'type'          => '',
                    'phoneNumber'   => '',
                    'countryCode'   => '',
                    'localAreaCode' => '',
                ],
                'address' => [
                    'zipcode'       => '',
                    'housenumber'   => '',
                    'addition'      => '',
                    'street'        => '',
                    'city'          => '',
                    'country'       => '',
                    'type'          => '',
                ],
            ],
            'carrierIdentificationPhoneNumber' => [
                'type'          => '',
                'phoneNumber'   => $mobilePortingPhoneNumber,
                'countryCode'   => $this->countryCode,
                'localAreaCode' => '',
            ],
            'portingPhoneNumber' => [
                'type'          => '',
                'phoneNumber'   => $this->clientHelper->getPortingPhoneNumbers($package),
                'countryCode'   => $this->countryCode,
                'localAreaCode' => '',
            ]
        ];

        if ($this->clientHelper->getCheckout('provider[change_provider]') == Dyna_Superorder_Helper_Client::NP_CHANGE_PROVIDER_YES) {
            // -- nothing
        } elseif ($this->clientHelper->getCheckout('provider[change_provider]') == Dyna_Superorder_Helper_Client::NP_CHANGE_PROVIDER_DATA_LATER) {
            $portingInfo['CarrierContractCancel'] = null;
        } else {
            $portingInfo = [];
        }

        return $portingInfo;
    }

    /**
     * @param $package
     * @return array
     */
    protected function extractOrderLines($package): array
    {
        $orderLines         = [];
        $currentStore       = Mage::app()->getStore();
        $taxCalculation     = Mage::getModel('tax/calculation');
        $taxRequest         = $taxCalculation->getRateRequest(null, null, null, $currentStore);

        foreach ($package->getItems() as $item) {

            /** @var Dyna_Catalog_Model_Product $product */
            $product = $item->getProduct();

            $orderLines[] = [
                'status'    => 'added',
                'defaulted' => (int) $item->getIsDefaulted(),
                'product' => [
                    'id'   => $product->getId(),
                    'type' => [
                        'id'    => $product->getData('package_type'),
                        'name'  => $product->getPackageType(),
                    ],
                    'subtype' => [
                        'id'   => $product->getData('package_subtype'),
                        'name' => $product->getPackageSubtypeName(),
                    ],
                    'commercialId'   => $product->getSku(),
                    'name'           => $product->getName(),
                    'description'    => $product->getDescription(),
                    'additionalInfo' => $product->getServiceDescription(),
                    'productPrice' => [
                        [
                            'type'                   => 'one time',
                            'recurrringChargePeriod' => 'month',
                            'validFor' => [
                                'name'  => '',
                                'value' => '',
                            ],
                            'price' => [
                                'currency' => Mage::app()->getStore()->getCurrentCurrencyCode(),
                                'inclTax'  => $item->getPriceInclTax(),
                                'exclTax'  => $item->getPrice(),
                                'taxClass' => $taxCalculation->getRate($taxRequest->setProductClassId($product->getTaxClassId())),
                            ]
                        ],
                        [
                            'type'                   => 'recurrring',
                            'recurrringChargePeriod' => 'month',
                            'validFor' => [
                                'name'  => '',
                                'value' => '',
                            ],
                            'price' => [
                                'currency' => Mage::app()->getStore()->getCurrentCurrencyCode(),
                                'inclTax'  => $item->getMafInclTax(),
                                'exclTax'  => $item->getMaf(),
                                'taxClass' => $taxCalculation->getRate($taxRequest->setProductClassId($product->getTaxClassId())),
                            ]
                        ]
                    ],
                    'additionalProperties' => $this->extractAttributes($product)
                ],
            ];
        }

        return $orderLines;
    }

    protected function extractAttributes($product)
    {

        $additionalProperties = [];

        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            $property = [
                'id' => $attribute->getAttributeId(),
                'name' => $attribute->getAttributeCode(),
            'value' => $attribute->getFrontend()->getValue($product),
            ];

            $additionalProperties[] = $property;
        }

        return $additionalProperties;
    }

    /**
     * @param $allPackages
     * @param $customer
     * @param $customerNumber
     * @return array
     */
    protected function extractPackages($allPackages, $customer, $customerNumber): array
    {
        $packages = [];

        foreach ($allPackages as $package) {
            $bundle = $package->getBundle();
            $subscriberData = Mage::getModel('dyna_superorder/client_submitOrderClient_packageSubscriber');
            $subscriberInfo = $subscriberData->getSubscriberData($package, $customer, null, $allPackages);
            $serviceAddress = $package->getServiceAddressData();
            $billingInfo = $this->getBillingData($package);


            $orderLines = $this->extractOrderLines($package);

            $packages[] = [
                'id' => $package->getPackageId(),
                'header' => [
                    'packageType' => [
                        'id'   => $package->getId(),
                        'name' => $package->getType(),
                    ],
                ],
                'customerID' => $customerNumber,
                'orderLines' => $orderLines,
                'deliveryOrder' => [
                    'id' => 0,
                ],
                'bundle' => [
                    'id' => ($bundle) ? $bundle->getBundleId() : '',
                    'name' => ($bundle) ? $bundle->getBundleGuiName() : '',
                    'legacyBundelId' => ($bundle) ? $bundle->getLegacyBundleId() : '',
                ],
                'bundleMembership' => !empty($package->getBundles()),
                'subscribers' => [
                    'id' => '',
                    'customerID' => $subscriberInfo['Ctn'],
                    'contactID' => [
                        0 => '',
                    ],
                ],
                'billingAccount' => [
                    'accountNo' => $billingInfo[0]['ID'],
                    'financialInstitutionBranch' => [
                        'id' => $billingInfo[0]['FinancialInstitutionBranch']['ID'],
                        'financialInstitution' => [
                            'id' => $billingInfo[0]['FinancialInstitutionBranch']['FinancialInstitution']['ID'],
                            'name' => $billingInfo[0]['FinancialInstitutionBranch']['FinancialInstitution']['Name'],
                        ],
                    ],
                    'customerAccountID' => !empty($billingInfo[0]['']) ? $billingInfo[0][''] : '',
                    'paymentMethod' => $billingInfo[0]['PaymentMethodList']['PaymentChargeType'][0]['PaymentMethod'],
                    'billingType' => $billingInfo[0]['BillingType'],
                    'ibanValidated' => $billingInfo[0]['IBANValidated'],
                    'mandateMethod' => $billingInfo[0]['MandateMethod'],
                ],
                'serviceAddress' => [
                    'zipcode' => $serviceAddress['postcode'],
                    'housenumber' => $serviceAddress['houseno'],
                    'addition' => $serviceAddress['addition'],
                    'street' => $serviceAddress['street'],
                    'city' => $serviceAddress['city'],
                    'country' => 'DEU',
                    'type' => 'S',
                ],
            ];
        }
        return $packages;
    }

    /**
     * @param $allPackages
     * @return array
     */
    protected function extractDeliverOrders($allPackages): array
    {
        $deliveryOrders = [];
        foreach ($allPackages as $package) {
            $deliveryOrders[] = array(
                'id' => null
            );
        }
        return $deliveryOrders;
    }

    /**
     * @param $quote
     * @param $customerNumber
     * @param $portingInfo
     * @param $phoneBookEntry
     * @return array
     */
    protected function extractCustomer($quote, $customerNumber, $portingInfo, $phoneBookEntry): array
    {
        // -- customer
        $customer           = $quote->getCustomer();
        $customerIdNumber   = $this->clientHelper->getCheckout('customer[id_number]') ? : null;
        $customerLastName   = $this->clientHelper->getCheckout('customer[lastname]') ? : null;
        $customerFirstName  = $this->clientHelper->getCheckout('customer[firstname]') ? : null;
        $customerInitials   = $customerFirstName[0].''.$customerLastName[0];
        $customerTitle      = $this->clientHelper->getCheckout('customer[prefix]') ? : null;
        $customerDob        = $this->clientHelper->getCheckout('customer[dob]');
        $customerGender     = $this->clientHelper->getCheckout('customer[gender]');
        $customerSalutation = $customerGender ? $this->clientHelper->getSalutation('customer[gender]') : null;

        // -- account
        $partyType          = $this->clientHelper->getCustomerType('customer[type]');
        $accountType        = $partyType ?? '';
        $accountCategory    = implode(',', $this->getAccountCategory($quote));

        // -- contact
        $contactEmail       = $this->clientHelper->getCheckout('customer[contact_details][email]');
        $contactFirstName   = $this->clientHelper->getCustomerContactFirstName($customer);
        $contactLastName    = $this->clientHelper->getCustomerContactLastName($customer);
        $contactInitials    = $contactFirstName[0].''.$contactLastName[0];
        $contactSalutation  = $this->clientHelper->getCustomerContactSalutation($customer);
        $contactPhoneType   = $this->clientHelper->getCheckout('customer[contact_details][phone_list][0][telephone_type]');
        $contactPhoneNumber = $this->clientHelper->getCheckout('customer[contact_details][phone_list][0][telephone]');
        $contactPhonePrefix = $this->clientHelper->getCheckout('customer[contact_details][phone_list][0][telephone_prefix]');

        // -- company
        $companyFirst       = $this->clientHelper->getCheckout('customer[firstname]') ? : $this->clientHelper->getCheckout('customer[contact_person][first_name]');
        $companyLast        = $this->clientHelper->getCheckout('customer[lastname]') ? : $this->clientHelper->getCheckout('customer[contact_person][last_name]');

        return [
            [
                'id'                  => $customerNumber,
                'accountCategory'     => $accountCategory,
                'accountType'         => $accountType,
                'parentAccountNumber' => $customer->getGlobalId(),
                'personalDetails' => !$customer->getIsSoho()  || !$this->sohoCustomerIsCompany() ? [
                    'id'          => $customerIdNumber,
                    'initials'    => $customerInitials,
                    'firstname'   => $customerFirstName,
                    'lastname'    => $customerLastName,
                    'title'       => $customerTitle,
                    'salutation'  => $customerSalutation,
                    'gender'      => $customerGender,
                    'dateOfBirth' => $customerDob,
                ] : [],
                'companyDetails' => $customer->getIsSoho() ? [
                    'id'                => $this->clientHelper->getCheckout('customer[id_number]') ? : null,
                    'registrationName'  => $companyFirst . ' ' . $companyLast,
                    'companyID'         => $this->clientHelper->getCheckout('customer[address][company_registration_number]'),
                    'companyLegalForm'  => $this->clientHelper->getCheckout('customer[address][commercial_register_type]'),
                    'registrationCity'  => $this->clientHelper->getCheckout('customer[address][city]'),
                    'registrationAddress' => [
                        'zipcode'     => $this->clientHelper->getCheckout('customer[address][postcode]'),
                        'housenumber' => $this->clientHelper->getCheckout('customer[address][houseno]'),
                        'addition'    => $this->clientHelper->getCheckout('customer[address][addition]'),
                        'street'      => $this->clientHelper->getCheckout('customer[address][street]'),
                        'city'        => $this->clientHelper->getCheckout('customer[address][city]'),
                        'country'     => $this->countryCode,
                        'type'        => '',
                    ]
                ]: [],
                'contact' => [
                    [
                        'id'        => '',
                        'partyType' => $partyType,
                        'person' => [
                            'id'          => '',
                            'title'       => $this->clientHelper->getCheckout('customer[prefix]'),
                            'gender'      => $this->clientHelper->getCheckout('customer[contact_person][gender]'),
                            'initials'    => $contactInitials,
                            'firstname'   => $contactFirstName,
                            'lastname'    => $contactLastName,
                            'salutation'  => $contactSalutation,
                            'dateOfBirth' => date("Y-m-d", strtotime($this->clientHelper->getCheckout('customer[dob]'))),
                        ],
                        'email' => [
                            'type'  => '',
                            'value' => $contactEmail,
                        ],
                        'telephone' => [
                            'type'          => $contactPhoneType,
                            'countryCode'   => $this->countryCode,
                            'localAreaCode' => $contactPhonePrefix,
                            'phoneNumber'   => $contactPhoneNumber,
                        ],
                        'address' => [
                            'city'        => $this->clientHelper->getCheckout('customer[address][city]'),
                            'type'        => '',
                            'street'      => $this->clientHelper->getCheckout('customer[address][street]'),
                            'country'     => $this->countryCode,
                            'zipcode'     => $this->clientHelper->getCheckout('customer[address][postcode]'),
                            'addition'    => $this->clientHelper->getCheckout('customer[address][addition]'),
                            'housenumber' => $this->clientHelper->getCheckout('customer[address][houseno]'),
                        ]
                    ]
                ],
                'portingInfo' => [
                    $portingInfo
                ],
                'phoneBookEntry' => [
                    $phoneBookEntry
                ],
                'attributes' => [
                    [
                    ]
                ],
            ]
        ];
    }

    /**
     * During checkout, a SOHO customer can be a registered company or an individual customer
     */
    protected function sohoCustomerIsCompany()
    {
        return $this->clientHelper->getCheckout('customer[business][commercial_register]') == 2;
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @param $packages
     * @return array
     */
    protected function extractHeaders($quote, $packages)
    {
        // date
        $validity = (int)Mage::getConfig()->getNode('default/checkout/vodafoneoptions/saved_offers_validity_period');
        $validity = ($validity == 0) ? 1 : $validity;

        $creationDate = new \DateTime();
        $creationDate->setTimestamp(Mage::getModel('core/date')->timestamp($quote->getCreatedAt()));

        $expirationDate = new \DateTime();
        $expirationDate->setTimestamp(Mage::getModel('core/date')->timestamp($quote->getCreatedAt()));
        $expirationDate->modify("+ {$validity} days");

        // status
        if ($quote->isOffer() == 0) {
            $headerStatus = self::STATUS_OFFER_SUBMITTED;
        } else {
            $headerStatus = (Mage::helper('dyna_checkout')->isOfferExpired($quote->getCreatedAt())) ?
                self::STATUS_OFFER_EXPIRED : self::STATUS_OFFER_VALID;
        }

        // packages header
        $packagesHeader = [];

        foreach ($packages as $package) {
            $packagesHeader[] = $package['header'];
        }

        $taxClassCalculator = Mage::getModel('tax/calculation');
        $calculationRequest = $taxClassCalculator->getRateRequest(null, null, null);
        $taxPercent = $taxClassCalculator->getRate($calculationRequest->setProductClassId($quote->getCustomerTaxClassId()));

        $headers = [
            'id'                => $quote->getId(),
            'name'              => $this->clientHelper->getCheckout('offerName'),
            'status'            => $headerStatus,
            'description'       => $this->clientHelper->getCheckout('offerDescription'),
            'creationDate'      => $creationDate->format('d-m-Y'),
            'validityEndDate'   => $expirationDate->format('d-m-Y'),
            'packageheaders'    => $packagesHeader,
            'price' => [
                [
                    'type'                   => 'one time',
                    'validFor' => [
                        'name'  => '',
                        'value' => '',
                    ],
                    'price' => [
                        'currency' =>  $quote->getQuoteCurrencyCode(),
                        'inclTax' => $quote->getGrandTotal(),
                        'exclTax' => $quote->getSubtotal(),
                        'taxClass' => $taxPercent,
                    ]
                ],
                [
                    'type'                   => 'recurring',
                    'recurrringChargePeriod' => 'month',
                    'validFor' => [
                        'name'  => 'week',
                        'value' => '',
                    ],
                    'price' => [
                        'currency' => $quote->getQuoteCurrencyCode(),
                        'inclTax' => $quote->getBaseMafGrandTotal(),
                        'exclTax' => $quote->getBaseMafSubtotal(),
                        'taxClass' => 0,
                    ]
                ]
            ]
        ];

        return $headers;
    }

    /**
     * Check if customer products contain Gigakombi bundle
     * @return bool
     */
    protected function isGigacombi()
    {
        //no point in checking if this is a new customer scenario
        if (!Mage::getSingleton('customer/session')->getCustomer()->getCustomerNumber()) {
            return false;
        }

        foreach ($this->quote->getPackages() as $package) {
            if ($bundleId = $package['bundle_id']) {
                $bundleRule = Mage::getSingleton('dyna_bundles/bundleRule')
                    ->getCollection()
                    ->addFieldToFilter('id', ['eq' => $bundleId])
                    ->getFirstItem();

                $bundleType = $bundleRule->getBundleType();
                $bundleGuiName = $bundleRule->getBundleGuiName();

                if (isset($bundleType)) {
                    return $bundleType == Dyna_Bundles_Model_BundleRule::TYPE_KOMBI;
                } else if (isset($bundleGuiName)) {
                    return $bundleGuiName == Dyna_Bundles_Model_BundleRule::TYPE_KOMBI;
                }
            }
        }

        return false;
    }

    /**
     * Retrieve the quote packages
     *
     * @return array
     */
    protected function getPackages($quote)
    {
        if(!$quote || !$quote->getId()) {
            $quote = $this->quote;
        }
        $tempPackages = $quote->getPackages();

        $packages = [];
        foreach ($tempPackages as $id => $package) {
            /** @var Dyna_Package_Model_Package $packageModel */
            $packageModel = $quote->getCartPackage($id);
            $packageModel->setData('items', $package['items']);
            if (!$packageModel->getInstalledBaseProducts()) {
                $packages[] = $packageModel;
            }
        }

        return $packages;
    }

    protected function getAllItems()
    {
        if($this->type == 'Offer') {
            return $this->quote->getAllItems();
        }
        $tempItems = $this->superorder->getOrderedItems(true, false);
        $items = array();
        foreach ($tempItems as $it) {
            $items = array_merge($items, $it);
        }
        return $items;
    }

    /**
     * Return account categories the current customer belongs to based on current packages
     * @param $quote Dyna_Checkout_Model_Sales_Quote
     * @return string
     */
    public function getAccountCategory(Dyna_Checkout_Model_Sales_Quote $quote = null, $requestValue = true)
    {
        $mobilePackages = Dyna_Catalog_Model_Type::getMobilePackages();
        $cablePackages = Dyna_Catalog_Model_Type::getCablePackages();
        $fixedPackages = Dyna_Catalog_Model_Type::getFixedPackages();

        // Get current quote packages
        if (!isset($quote)) {
            $quote = $this->quote;
        }
        $packages = $quote->getCartPackages() ? $quote->getCartPackages() : $this->getPackages();

        $accountCategories = array();

        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            $packageType = strtolower($package->getType());

            if (in_array($packageType, $mobilePackages)) {
                $accountCategories[] = $requestValue ? Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KS : Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS;
            } elseif (in_array($packageType, $cablePackages)) {
                $accountCategories[] = Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD;
            } elseif (in_array($packageType, $fixedPackages)) {
                $accountCategories[] = Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN;
            }

            $accountCategories = array_unique($accountCategories);
        }

        return $accountCategories;
    }

    /**
     * Build the package billing account data
     * @param $package
     * @return array
     */
    protected function getBillingData($package)
    {
        $callDetailType = $callDigitsMask = null;
        switch ($this->clientHelper->getCheckout('other[single_connection]')) {
            case Dyna_Superorder_Helper_Client::CONNECTION_NONE:
                $callDetailType = 'N';
                $callDigitsMask = '0';
                break;
            case Dyna_Superorder_Helper_Client::CONNECTION_FULL:
                $callDetailType = 'S';
                $callDigitsMask = '3';
                break;
            case Dyna_Superorder_Helper_Client::CONNECTION_CONDENSED:
                $callDetailType = 'M';
                $callDigitsMask = '3';
                break;
        }

        $returnData = [];

        foreach ($this->clientHelper->getChargeType( $package ) as $chargeType) {
            $directIban = $this->clientHelper->getIBANFieldValue( $package, $chargeType );
            $returnData[] =         array(
                'ID' => $directIban,//@excel
                'FinancialInstitutionBranch' => array(
                    // send last 10 digits from account number as submit order fails validation (see OMNVFDE-2913)
                    'ID' => substr($this->clientHelper->getCheckout("iban[$directIban][account_number]"), -10),//@excel
                    'Name' => $this->clientHelper->getCheckout("iban[$directIban][credit_provider]"),//@excel
                    'FinancialInstitution' => array(
                        'ID' => $this->clientHelper->getCheckout("iban[$directIban][bank_code]"),//@excel,
                        'Name' => $this->clientHelper->getCheckout("iban[$directIban][usage]"),//@excel,
                    ),
                ),
                'PartyIdentyfication' => null,
                // @TODO Needs update with user as account owner or other person as account owner
                'AccountOwnerName' => $this->clientHelper->getAccountHolderieldValue(),//@excel
                'PaymentMethodList' => $this->clientHelper->getPaymentMethodList($package),//@excel
                'BillingType' => $this->clientHelper->getBillingType($package),
                'CallDetailType' => $callDetailType,//@excel
                'DigitsMaskED' => null,
                'DigitsMaskCD' => $callDigitsMask,//@excel
                'BooIndicator' => null,
                'Purpose' => $this->clientHelper->getCheckout("iban[$directIban][usage]"), //@excel
                'IBANValidated' => 'true', //@toclarify - how can this be invalid?
                'MandateMethod' => $this->clientHelper->getSepaMandate( $chargeType ),
                'ChargesType' => $chargeType
            );
        }
        return $returnData;
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @return array
     */
    public function extractOfferData($quote) : array
    {
        $this->quote = $quote;

        /** @var Dyna_Checkout_Model_Field $orderCheckoutData */
        $orderCheckoutData = Mage::getModel('dyna_checkout/field')
            ->loadQuoteData(array('quote_id' => $quote->getId()));

        // Set checkout data on superorder client
        Mage::helper('dyna_superorder/client')
            ->setClientData($orderCheckoutData, $this->getCustomer());

        $this->clientHelper = Mage::helper('dyna_superorder/client');
        $this->serviceHelper = Mage::helper('omnius_service');

        // get country code
        $this->countryCode = ($this->serviceHelper->getGeneralConfig('country_code') ?
            $this->serviceHelper->getGeneralConfig('country_code') : Dyna_Customer_Model_Customer::COUNTRY_CODE);
        // -- customer
        $customer       = $quote->getCustomer();
        $customerNumber = $customer->getCustomerNumber();
        // -- package
        $allPackages = $this->getPackages($quote);
        if (false === empty($customer) && false === empty($customerNumber)) {
            $packages = $this->extractPackages($allPackages, $customer, $customerNumber);
        }
        // -- porting info
        foreach ($allPackages as $currentPackage) {
            $portingInfo = $this->extractPortingInfo($currentPackage);
            // -- phone book
            $phoneBookEntry[] = $this->extractPhoneBookEntry($currentPackage->getPackageId(),$currentPackage->getType());
            if (!empty($portingInfo)) {
                break;
            }
        }

        // -- delivery
        $deliveryOrders = $this->extractDeliverOrders($allPackages);

        // -- customer
        $customers = $this->extractCustomer($quote, $customerNumber, $portingInfo, $phoneBookEntry);

        // -- headers
        $headers = array();
        if (isset($packages)) {
            $headers = $this->extractHeaders($quote, $packages);
        }
        // -- return data
        $params = [
            'data' => [
                'id'             => $quote->getId(),
                'header'         => $headers,
                'salesOrder'     => [
                    'id' => $quote->getId(),
                    'customers'      => $customers,
                    'packages'       => $packages ?? array(),
                    'deliveryOrders' => $deliveryOrders,
                ]
            ]
        ];
        // determine campaign expiration date
        if($quote->getCampaignCode()) {
            $campaign = Mage::getModel('dyna_bundles/campaign')->load($quote->getCampaignCode(), 'campaign_id');
            if($campaign && $campaign->getValidTo()) {
                $params['campaignValidityDate'] = $campaign->getValidTo();
            }
        }
        return $params;
    }
}
