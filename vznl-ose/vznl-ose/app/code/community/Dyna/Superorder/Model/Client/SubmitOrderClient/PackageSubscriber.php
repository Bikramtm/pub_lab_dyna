<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Superorder_Model_Client_SubmitOrderClient_PackageSubscriber extends Mage_Core_Model_Abstract
{
    /** @var  $clientHelper Dyna_Superorder_Helper_Client */
    protected $clientHelper;

    public function _construct()
    {
        $this->clientHelper = Mage::helper('dyna_superorder/client');
    }

    /**
     * Generate the main Subscriber node data
     * @param Dyna_Package_Model_Package $package
     * @param $customer
     * @return array
     */
    public function getSubscriberData($package, $customer, $addressType = null, $packages)
    {
        //for prolongation, send serial number as received from retrieveCustomerInfo service
        $serialNumber = null;
        if($package->getActivityType() != Dyna_Package_Model_Package::PACKAGE_ACTION_NEW && ($package->getActivityType() != Dyna_Package_Model_Package::PACKAGE_ACTION_ADD_TO_REDPLUS || $package->getSaleType() == Dyna_Catalog_Model_ProcessContext::ILSBLU)){
            foreach (Mage::getSingleton('customer/session')->getCustomer()->getAllInstallBaseProducts() as $subscription) {
                if($subscription['contract_id'] == $package->getParentAccountNumber()){
                    if( !empty( $subscription['serial_number'] )) {
                        $serialNumber = $subscription['serial_number'];
                    } else {
                        $serialNumber = !empty($subscription['sim_number']) ? $subscription['sim_number'] : '';
                    }
                    break;
                }
            }
        }

        $subscriberData = array(
            'Ctn' => $this->getCtnData($package),
            'Address' => $this->getAddressData($package, $addressType, $packages),
            'Contact' => $this->getContactData($package, $customer),
            'PrepaidSubscriberBirthDate' =>  $package->getSaleType() == Dyna_Catalog_Model_ProcessContext::MIGCOC ? $customer->getInstallBaseDataBy($package->getParentAccountNumber(), 'contact_birth_date') : null,
            'Device' => $this->getDeviceData($package),
            'SerialNumber' => $serialNumber
        );

        if(!$subscriberData['PrepaidSubscriberBirthDate']){
            $subscriberData['PrepaidSubscriberBirthDate'] = '1900-01-01';
        }

        return $subscriberData;
    }

    /**
     * Build the address node(s)
     * @$package Dyna_Package_Model_Package
     * @return array
     */
    protected function getAddressData($package, $addressType = null, $packages)
    {
        $this->clientHelper = Mage::helper('dyna_superorder/client');
        $dlAddressType =  $this->clientHelper->getCheckout('delivery[pickup][store_id]') ? 'pickup_address' : 'other_address';
        $dlAddressTypeCamelCased = $dlAddressType == "other_address" ? "otherAddress" : $dlAddressType;

        if ($this->clientHelper->getCheckout('customer[type]') == 'Soho') {
            $hasFixedPackageInOrder = FALSE;
            foreach( $packages as $package ) {
                if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getFixedPackages())) {
                    $hasFixedPackageInOrder = TRUE;
                }
            }
            if ($hasFixedPackageInOrder) {
                if ($this->clientHelper->getCheckout('customer[business][commercial_register]') != Dyna_Checkout_Helper_Fields::HANDELSREGISTEREINTRAG_YES) {
                    $customerDataType = 'LA';
                    $commercialRegistryKey = 'L';
                } else {
                    $customerDataType = null;
                    $commercialRegistryKey = 'L';
                }
            } else {
                $commercialRegistryKey = null;
                $customerDataType = 'L';
            }
        } else {
            $commercialRegistryKey = null;
            $customerDataType = 'L';
        }

        $checkoutAddressData = array(
            'B' =>
                ($this->clientHelper->getCheckout('billing[other_address][selected]') == Dyna_Checkout_Block_Cart_Steps_SaveCustomer::MULTIPLE_BILLING_ADDRESSES)
                && ( $this->clientHelper->getCheckout('billing[pakket][' . $package->getPackageId() . '][other_address][selected]') == Dyna_Checkout_Block_Cart_Steps_SaveCustomer::BILLING_ANOTHER_ADDRESS
                    || $this->clientHelper->getCheckout('billing[pakket][' . $package->getPackageId() . '][other_address][selected]') == Dyna_Checkout_Block_Cart_Steps_SaveCustomer::REUSE_BILLING_ADDRESS
                    || $this->clientHelper->getCheckout('billing[pakket][' . $package->getPackageId() . '][other_address][selected]') == Dyna_Checkout_Block_Cart_Steps_SaveCustomer::BILLING_PO_BOX )
                ? array(
                'ID' => 'billing_split_pakket_' . $package->getPackageId() . '_address_id',
                'PostalZone' => 'billing[pakket][' . $package->getPackageId() . '][other_address][postcode]',
                'CityName' => 'billing[pakket][' . $package->getPackageId() . '][other_address][city]',
                'Street' => 'billing[pakket][' . $package->getPackageId() . '][other_address][street]',
                'HouseNo' => 'billing[pakket][' . $package->getPackageId() . '][other_address][houseno]',
                'CountryName' => 'billing[pakket][' . $package->getPackageId() . '][other_address][nationality]',
                'PostBox' => 'billing[pakket][' . $package->getPackageId() . '][other_address][pobox]',
                'District' => 'billing[pakket][' . $package->getPackageId() . '][other_address][district]',
                'HouseNumberAddition' => 'billing[pakket][' . $package->getPackageId() . '][other_address][addition]',
            )
                : array(
                'ID' => 'billing_other_address_id',
                'PostalZone' => 'billing[other_address][postcode]',
                'CityName' => 'billing[other_address][city]',
                'Street' => 'billing[other_address][street]',
                'HouseNo' => 'billing[other_address][houseno]',
                'CountryName' => 'billing[other_address][nationality]',
                'PostBox' => 'billing[other_address][pobox]',
                'District' => 'billing[other_address][district]',
                'HouseNumberAddition' => 'billing[other_address][addition]',
            ),
            $commercialRegistryKey => $commercialRegistryKey !== null ? array(
                'ID' => 'customer_soho_address_id',
                'PostalZone' => 'customer_soho[address][postcode]',
                'CityName' => 'customer_soho[address][city]',
                'Street' => 'customer_soho[address][street]',
                'HouseNo' => 'customer_soho[address][houseno]',
                'District' => 'customer_soho[address][district]',
                'HouseNumberAddition' => 'customer_soho[address][addition]',
            ) : null,
            $customerDataType => $customerDataType !== null ? array(
                'ID' => 'customer_address_id',
                'PostalZone' => 'customer[address][postcode]',
                'CityName' => 'customer[address][city]',
                'Street' => 'customer[address][street]',
                'HouseNo' => 'customer[address][houseno]',
                'District' => 'customer[address][district]',
                'HouseNumberAddition' => 'customer[address][addition]',
            ) : null,
            'DL' =>
                ($this->clientHelper->getCheckout('delivery[method]') == Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SPLIT_DELIVERY)
                    ? array(
                        'ID' => 'delivery_split_pakket_' . $package->getPackageId() . '_address_id',
                        'PostalZone' => 'delivery[pakket][' . $package->getPackageId() . '][' . $dlAddressTypeCamelCased . '][postcode]',
                        'CityName' => 'delivery[pakket][' . $package->getPackageId() . '][' . $dlAddressTypeCamelCased . '][city]',
                        'Street' => 'delivery[pakket][' . $package->getPackageId() . '][' . $dlAddressTypeCamelCased . '][street]',
                        'HouseNo' => 'delivery[pakket][' . $package->getPackageId() . '][' . $dlAddressTypeCamelCased . '][houseno]',
                        'District' => 'delivery[pakket][' . $package->getPackageId() . '][' . $dlAddressTypeCamelCased . '][district]',
                        'HouseNumberAddition' => 'delivery[pakket][' . $package->getPackageId() . '][' . $dlAddressTypeCamelCased . '][addition]',
                    )
                    : array(
                        'ID' => 'delivery_' . $dlAddressType . '_id',
                        'PostalZone' => 'delivery[' . $dlAddressType . '][postcode]',
                        'CityName' => 'delivery[' . $dlAddressType . '][city]',
                        'Street' => 'delivery[' . $dlAddressType . '][street]',
                        'HouseNo' => 'delivery[' . $dlAddressType . '][houseno]',
                        'District' => 'delivery[' . $dlAddressType . '][district]',
                        'HouseNumberAddition' => 'delivery[' . $dlAddressType . '][addition]',
                    ),
            'PA'  =>
                ($this->clientHelper->getCheckout('payment[monthly][method]') == Dyna_Checkout_Helper_Fields::PAYMENT_SPLIT_PAYMENT)
                    ? array(
                        'ID' => 'payment_monthly_address_id',
                        'PostalZone' => 'payment[pakket][' . $package->getPackageId() . '][alternative][postcode]',
                        'CityName' => 'payment[pakket][' . $package->getPackageId() . '][alternative][city]',
                        'Street' => 'payment[pakket][' . $package->getPackageId() . '][alternative][street]',
                        'HouseNo' => 'payment[pakket][' . $package->getPackageId() . '][alternative][houseno]',
                        'HouseNumberAddition' => 'payment[pakket][' . $package->getPackageId() . '][alternative][addition]',
                    )
                    : array(
                        'ID' => 'payment_monthly_address_id',
                        'PostalZone' => 'payment[monthly][alternative][postcode]',
                        'CityName' => 'payment[monthly][alternative][city]',
                        'Street' => 'payment[monthly][alternative][street]',
                        'HouseNo' => 'payment[monthly][alternative][houseno]',
                        'HouseNumberAddition' => 'payment[monthly][alternative][addition]',
                    ),
            'T' => array(
                'ID' => 'payment_onetime_address_id',
                'PostalZone' => 'payment[one_time][cash][postcode]',
                'CityName' => 'payment[one_time][cash][city]',
                'Street' => 'payment[one_time][cash][street]',
                'HouseNo' => 'payment[one_time][cash][houseno]',
            ),
           'PS' => array(
                'ID' => 'provider_other_address_id',
                'PostalZone' => 'provider['.$package->getType().'][other_address][postcode]',
                'CityName' => 'provider['.$package->getType().'][other_address][city]',
                'Street' => 'provider['.$package->getType().'][other_address][street]',
                'HouseNo' => 'provider['.$package->getType().'][other_address][houseno]',
                'District' => 'provider['.$package->getType().'][other_address][district]',
                'HouseNumberAddition' => 'provider['.$package->getType().'][other_address][addition]',
            )
        );

        if( $this->deliveryAddressIsSameAs($package, Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SPECIFIED_ADDRESS) ) {
            $checkoutAddressData['DL'] = $checkoutAddressData['L'];
        }

        if($this->billingAddressIsSameAsCustomerAddress($package)) {
            $checkoutAddressData['B'] = $checkoutAddressData['L'];
        }

        if( $this->clientHelper->getCheckout('payment[monthly][method]') == Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SPLIT_DELIVERY ) {
            $alternativeAccountHolder = $this->clientHelper->getCheckout('payment[pakket][' . $package->getPackageId() . '][alternative_account_holder]');
        } else {
            $alternativeAccountHolder = $this->clientHelper->getCheckout('payment[monthly][alternative_account_holder]');
        }
        if($alternativeAccountHolder != Dyna_Checkout_Block_Cart_Steps_SaveCustomer::ALTERNATIVE_ACCOUNT_HOLDER){
            $checkoutAddressData['PA'] = $checkoutAddressData['L'];
        }

        $SAPDeliveryType = $this->determineSAPDeliveryType($package);

        $addressData = array();
        foreach ($checkoutAddressData as $type => $field) {
            switch( TRUE ) {
                case $type == 'DL' && $this->deliveryAddressIsSameAs($package, Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_PICKUP_VODAFONE_SHOP):
                    $storeId = $this->clientHelper->getCheckout('delivery[pakket]['. $package->getPackageId() .'][store_id]');
                    $store = Mage::getModel('agentde/vodafoneShip2Stores')->load($storeId);
                    $addressDetails =  array(
                        'ID' => $this->clientHelper->getValue($field, 'ID'),
                        'AddressTypeCode' => $type, //@excel
                        'StreetName' => $store->getStreet(),
                        'MarkAttention' => null,// is null so it doesn't appear in submit order request xml
                        'BuildingNumber' => $store->getHouseNo(),
                        'CityName' => $store->getCity(),
                        'PostalZone' => $store->getPostcode(),
                        'CountrySubentity' => null,
                        'District' => $store->getDistrict(),
                        'Country' => array(
                            'Name' => 'DEU'
                        ),
                        'HouseNumberAddition' => $store->getHouseNo(),
                        'Validated' => 'true',//@toclarify - how can this be invalid?
                    );
                    break;
                case $type == 'DL' && $this->deliveryAddressIsSameAs($package, Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SERVICE_ADDRESS):
                    $serviceAddress = $package->getServiceAddressData();
                    $addressDetails = array(
                        'ID' => $serviceAddress['id'],
                        'AddressTypeCode' => $type,
                        'Postbox' => $serviceAddress['postbox'],
                        'StreetName' => $serviceAddress['street'],
                        'BuildingNumber' => $serviceAddress['houseno'],
                        'CityName' => $serviceAddress['city'],
                        'PostalZone' => $serviceAddress['postcode'],
                        'Location' => $this->clientHelper->getResidentialBuildingInfo(),
                        'CountrySubentity' => null,
                        'District' => $serviceAddress['district'],
                        'Country' => array(
                            'Name' => 'DEU'
                        ),
                        'HouseNumberAddition' => $serviceAddress['addition'],
                        'Validated' => 'true'
                    );
                    break;
                default:
                    $addressDetails =  array(
                        'ID' => $this->clientHelper->getValue($field, 'ID'),//@excel
                        'AddressTypeCode' => $type, //@excel
                        'Postbox' => $this->clientHelper->getValue($field, 'PostBox'),//@excel
                        'StreetName' => $this->clientHelper->getValue($field, 'Street'),//@excel
                        'MarkAttention' => null,// is null so it doesn't appear in submit order request xml
                        'BuildingNumber' => $this->clientHelper->getValue($field, 'HouseNo'),//@excel
                        'CityName' => $this->clientHelper->getValue($field, 'CityName'),//@excel
                        'PostalZone' => $this->clientHelper->getValue($field, 'PostalZone'),//@excel
                        'Location' => isset($field['Location'])?$field['Location']:null,//@excel
                        'CountrySubentity' => null,
                        'District' => $this->clientHelper->getValue($field, 'District'),//@excel,
                        'Country' => array(
                            'Name' => 'DEU'
                        ),
                        'HouseNumberAddition' => $this->clientHelper->getValue($field, 'HouseNumberAddition'),//@todo - from validate address,
                        'Validated' => 'true',//@toclarify - how can this be invalid?
                        'SAPDeliveryType' => $type == 'DL' ? $SAPDeliveryType : null,
                    );
                    break;
            }
            $addressData[] = $addressDetails;

            if (isset($addressType) && (strtolower($addressType) == strtolower($type))) {
                return $addressDetails;
            }
        }

        //add service address data for cable/fixed packages
        if (($package->isCable() || $package->isFixed()) && ($serviceAddress = $package->getServiceAddressData())) {

            $addressData[] = array(
                'ID' => $serviceAddress['id'],
                'AddressTypeCode' => 'S',
                'Postbox' => $serviceAddress['postbox'],
                'StreetName' => $serviceAddress['street'],
                'BuildingNumber' => $serviceAddress['houseno'],
                'CityName' => $serviceAddress['city'],
                'PostalZone' => $serviceAddress['postcode'],
                'Location' => $this->clientHelper->getResidentialBuildingInfo(),
                'CountrySubentity' => null,
                'District' => $serviceAddress['district'],
                'Country' => array(
                    'Name' => 'DEU'
                ),
                'HouseNumberAddition' => $serviceAddress['addition'],
                'Validated' => 'true'
            );
        }

        return $addressData;
    }

    /**
     * Build the contact node data
     * @param Dyna_Package_Model_Package $package
     * @param Dyna_Customer_Model_Customer $customer
     * @return array
     */
    protected function getContactData($package, $customer)
    {
        /** @var Omnius_Service_Helper_Data $serviceHelper */
        $serviceHelper = Mage::helper('omnius_service');

        $sameAsLegal = $this->deliveryAddressIsSameAs($package, Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SPECIFIED_ADDRESS)
            || $this->deliveryAddressIsSameAs($package, Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SERVICE_ADDRESS);
        $registrationName = $this->clientHelper->getCheckout('billing[other_address][company_name]');
        if (empty($registrationName)) {
            $registrationName = $this->clientHelper->getCheckout('customer[address][company_name]');
        }

        $data = array();
        $contactTypes = array(
            'L' => ($contactData = array(
                'Salutation' => 'customer[gender]',
                'Title' => 'customer[prefix]',
                'FirstName' => 'customer[firstname]',
                'LastName' => 'customer[lastname]',
                'BirthDate' => 'customer[dob]',
                'IdentificationCardNumber' => 'customer[id_number]',
                'NationalityID' => 'customer[nationality]',
                'RegistrationName'  => 'customer[address][company_name]',
            )),
            'TC' => array(
                'Salutation' => $this->clientHelper->getCheckout('payment[one_time][cash][gender]') ? 'payment[one_time][cash][gender]' : 'customer[gender]',
                'Title' => $this->clientHelper->getCheckout('payment[one_time][cash][prefix]') ? 'payment[one_time][cash][prefix]' : 'customer[prefix]',
                'FirstName' => $this->clientHelper->getCheckout('payment[one_time][cash][firstname]') ? 'payment[one_time][cash][firstname]' : 'customer[firstname]',
                'LastName' => $this->clientHelper->getCheckout('payment[one_time][cash][lastname]') ? 'payment[one_time][cash][lastname]' : 'customer[lastname]',
                'BirthDate' => 'customer[dob]',
                'NationalityID' => 'customer[nationality]'
            ),
            'DC' => ($sameAsLegal ? $contactData : $this->determineDeliveryContactData($package)),
            'LC' => array(
                'FirstName' => $package->isMobile() ? 'customer[contact_person_details][0][firstname]' : 'customer[firstname]',
                'LastName' => $package->isMobile() ? 'customer[contact_person_details][0][lastname]' : 'customer[lastname]',
                'Title' => $package->isMobile() ? null   : 'customer[prefix]',
                'BirthDate' => $package->isMobile() ? null : 'customer[dob]',
                'Salutation' => $package->isMobile() ? 'customer[contact_person_details][0][salutation]' : 'customer[gender]',
                'PhoneNumber' => $package->isMobile() ? 'customer[contact_person_details][0][telephone]' : null,
                'LocalAreaCode' => $package->isMobile() ? 'customer[contact_person_details][0][telephone_prefix]' : null,
            ),
            'MC' => array(
                'PhoneNumber' => 'order_status[telephone]',
                'RoleElectronicMail' => 'customer[contact_details][email]'
            ),
            'BC' => $this->determineBillingContactData($package, $contactData),
            'PC' => array(
                'Salutation' => 'payment[monthly][alternative][gender]',
                'Title' => 'payment[monthly][alternative][prefix]',
                'FirstName' => 'payment[monthly][alternative][firstname]',
                'LastName' => 'payment[monthly][alternative][lastname]',
                'ElectronicMail' => 'payment[sepa_mandate][email]'
            ),
            'CC' => ($customer->getIsSoho() && $package->isMobile() && $this->clientHelper->getCheckout('customer[contact_details][phone_list][0][telephone]')) ? array(
                'LocalAreaCode' => 'customer[contact_details][phone_list][0][telephone_prefix]',
                'PhoneNumber' => 'customer[contact_details][phone_list][0][telephone]'
            ) : null,
            'PT' => $package->isFixed() ? array(
                'FirstName' => 'provider[landlord_firstname]',
                'LastName' => 'provider[landlord_lastname]',
                'LocalAreaCode' => 'provider[landlord_prefix]',
                'PhoneNumber' => 'provider[landlord_phone]'
            ) : null,
        );

        if(strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL)){
            $contactTypes['IC'] = $contactTypes['LC'];
        }
        if($package->isFixed() || $package->isCable()){
            $this->clientHelper->addConnectionOwnerContacts($contactTypes, $package);
        }

        foreach($contactTypes as $type => $contactData){
            if(!$contactData) {
                continue;
            }
            $data[] = array(
                'PartyIdentification' => null,
                'PartyName' => $this->clientHelper->getValue($contactData, 'PartyName') ? array(
                    'Name' => $this->clientHelper->getValue($contactData, 'PartyName')
                 ) : null,//@excel
                'PartyLegalEntity' => array('RegistrationName' => $this->clientHelper->getValue($contactData, 'RegistrationName')),
                'Contact' => $this->clientHelper->getValue($contactData, 'ElectronicMail') ? array(
                    'Name' => null,
                    'ElectronicMail' => $this->clientHelper->getValue($contactData, 'ElectronicMail')
                ) : null,
                'PartyType' => $type,//@excel
                'ContactPhoneNumber' => $this->clientHelper->getValue($contactData, 'PhoneNumber') ? array(
                    'CountryCode' => ($serviceHelper->getGeneralConfig('country_code') ? $serviceHelper->getGeneralConfig('country_code') : Dyna_Customer_Model_Customer::COUNTRY_CODE),
                    'LocalAreaCode' => $this->clientHelper->getValue($contactData, 'LocalAreaCode'),
                    'PhoneNumber' => $this->clientHelper->getValue($contactData, 'PhoneNumber'),
                    'Type' => isset($contactData['Type']) ? $this->clientHelper->getContactPhoneNumberType($contactData['Type']) : null,
                ) : null,
                'Role' => $type == 'CC' ? null : array(
                    'Person' => array(
                        'FirstName' => $this->clientHelper->getValue($contactData, 'FirstName'),//@excel
                        'FamilyName' => $this->clientHelper->getValue($contactData, 'LastName'),//@excel
                        'Title' => $this->clientHelper->getValue($contactData, 'Title'),//@excel,
                        'NationalityID' => $this->clientHelper->getValue($contactData, 'NationalityID'),//@excel
                        'BirthDate' => $this->clientHelper->getValue($contactData, 'BirthDate')?
                            date("Y-m-d", strtotime($this->clientHelper->getValue($contactData, 'BirthDate'))) : null, //@excel
                        'Contact' => $this->clientHelper->getValue($contactData, 'RoleElectronicMail') ? array(
                            'ElectronicMail' => $this->clientHelper->getValue($contactData, 'RoleElectronicMail')
                        ) : null,
                        'Salutation' => $this->clientHelper->getSalutation($this->clientHelper->getValue($contactData, 'Salutation')),//@excel
                        'IdentificationCardNumber' =>  $this->clientHelper->getValue($contactData, 'IdentificationCardNumber'),//@excel
                    )
                )
            );
        }

        $this->removeEmptyContacts($data);

        return $data;
    }

    /**
     * Build the ctn node data
     * @param $package
     * @return array
     */
    public function getCtnData($package)
    {
        $allMobilePackages = array_map('strtolower', Dyna_Catalog_Model_Type::getMobilePackages());
        $packageIsMobile = in_array(strtolower($package->getType()), $allMobilePackages);
        $installedBase = $package->getInstalledBaseProducts();
        $isPartOfBundle = count($package->getBundles()) > 0;

        if ($packageIsMobile && $installedBase && $isPartOfBundle) {
            $ctn = str_replace('gsm', '', strtolower($package->getServiceLineId()));
        } else {
            $ctn = $package->getCtn();
        }

        if (trim($ctn) != '') {
            //strip country code from begining of ctn
            $ctn_trimmed = trim($ctn);
            $generalCountryCode = Mage::helper('omnius_service')->getGeneralConfig('country_code');
            $country_code_trimmed = str_replace('0', '', $generalCountryCode);
            if (substr($ctn, 0, strlen($country_code_trimmed)) == $country_code_trimmed) {
                $ctn_trimmed = substr($ctn, strlen($country_code_trimmed));
            }
            return [
                'PhoneNumber' => $ctn_trimmed
            ];
        } else {
            return null;
        }
    }

    /**
     * Builds device node data
     * @param Dyna_Package_Model_Package $package
     * @return array
     */
    public function getDeviceData($package) {
        $data = null;

        $items = $package->getItems(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE);
        foreach ($items as $item) {
            $device = array(
                'SerialNumber' => $item->getSku() ? $item->getSku() : null,
                'ID' => $item->getProductId() ? $item->getProductId() : null,
                'Name' => $item->getProduct()->getName() ? $item->getProduct()->getName() : null,
                'DevicePrice' => $item->getItemFinalPriceInclTax()
            );

            if ($data == null) {
                $data = array();
            }
            $data[] = $device;
        }

        return $data;
    }

    /**
     * Removes the empty contact nodes from the array
     *
     * @param $contacts
     */
    protected function removeEmptyContacts(&$contacts)
    {
        if (!$contacts || !is_array($contacts)) {
            return;
        }

        /** @var Dyna_Service_Model_DotAccessor $dot */
        $dot = Mage::getSingleton('dyna_service/dotAccessor');

        $skipFields = [
            'Role.Person.Salutation',
            'PartyType'
        ];

        $checkFields = [
            'PartyIdentification',
            'PartyName.Name',
            'PartyLegalEntity.RegistrationName',
            'Contact.Name',
            'Contact.ElectronicMail',
            'PartyType',
            'ContactPhoneNumber.CountryCode',
            'ContactPhoneNumber.LocalAreaCode',
            'ContactPhoneNumber.PhoneNumber',
            'ContactPhoneNumber.Type',
            'Role.Person.FirstName',
            'Role.Person.FamilyName',
            'Role.Person.Title',
            'Role.Person.NationalityID',
            'Role.Person.BirthDate',
            'Role.Person.Contact.ElectronicMail',
            'Role.Person.Salutation',
            'Role.Person.IdentificationCardNumber'
        ];

        foreach ($contacts as $key => $contact) {
            $removeNode = true;
            foreach ($checkFields as $field) {
                if (!in_array($field, $skipFields) && $dot->getValue($contact, $field) != null) {
                    $removeNode = false;
                }
            }

            if ($removeNode) {
                unset($contacts[$key]);
            }
        }

        $contacts = array_values($contacts);
    }

    /**
     * @param Dyna_Package_Model_Package $package
     * @return int $SAPDeliveryType
     */
    protected function determineSAPDeliveryType($package)
    {
        $SAPDeliveryType = null;
        if($package->isMobile()) {
            $field = $this->clientHelper->getCheckout('delivery[method]') == Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SPLIT_DELIVERY
                ?  'delivery[pakket]['.$package->getPackageId().'][same_day_schedule]' : 'delivery[same_day_schedule]';
            switch ($this->clientHelper->getCheckout($field)) {
                case 1:
                    $SAPDeliveryType = 12;
                    break;
                case 2:
                    $SAPDeliveryType = 13;
                    break;
            }
        }
        return $SAPDeliveryType;
    }

    /**
     * @param Dyna_Package_Model_Package $package
     * @param array $contactData
     * @return array
     */
    protected function determineBillingContactData($package, $contactData)
    {
        switch ($this->clientHelper->getCheckout('billing[other_address][selected]')) {
            case Dyna_Checkout_Block_Cart_Steps_SaveCustomer::MULTIPLE_BILLING_ADDRESSES:
                if($this->clientHelper->getCheckout('billing[pakket][' . $package->getPackageId() . '][other_address][selected]') == Dyna_Checkout_Block_Cart_Steps_SaveCustomer::BILLING_ANOTHER_ADDRESS) {
                    return array(
                        'FirstName' => 'billing[pakket][' . $package->getPackageId() . '][other_address][firstname]',
                        'LastName' => 'billing[pakket][' . $package->getPackageId() . '][other_address][lastname]',
                        'PartyName' => 'billing[pakket][' . $package->getPackageId() . '][other_address][company_additional_name]',
                        'RegistrationName' => 'billing[pakket][' . $package->getPackageId() . '][other_address][company_name]',
                        'Salutation' => 'billing[pakket][' . $package->getPackageId() . '][other_address][gender]',
                        'Title' => 'billing[pakket][' . $package->getPackageId() . '][other_address][prefix]',
                    );
                }
                return $contactData + array('RegistrationName' => 'customer[address][company_name]', 'RoleElectronicMail' => 'billing[email_address]');
            case Dyna_Checkout_Block_Cart_Steps_SaveCustomer::BILLING_ANOTHER_ADDRESS:
                return array(
                    'FirstName' => 'billing[other_address][firstname]',
                    'LastName' => 'billing[other_address][lastname]',
                    'PartyName' => 'billing[other_address][company_additional_name]',
                    'RegistrationName' => 'customer[address][company_name]',
                    'Salutation' => 'billing[other_address][gender]',
                    'Title' => 'billing[other_address][prefix]',
                    'RoleElectronicMail' => 'billing[email_address]',
                );
            default:
                return $contactData + array('RegistrationName' => 'customer[address][company_name]', 'RoleElectronicMail' => 'billing[email_address]');
        }
    }

    /**
     * @param Dyna_Package_Model_Package $package
     * @return array
     */
    protected function determineDeliveryContactData($package)
    {
        switch ($this->clientHelper->getCheckout('delivery[method]')) {
            case Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SPLIT_DELIVERY:
                $dlAddressType =  $this->clientHelper->getCheckout('delivery[pickup][store_id]') ? 'pickup_address' : 'other_address';
                $dlAddressTypeCamelCased = $dlAddressType == "other_address" ? "otherAddress" : $dlAddressType;
                return array(
                    'Salutation' => 'delivery[pakket]['.$package->getPackageId().']['.$dlAddressTypeCamelCased.'][gender]',
                    'Title' => 'delivery[pakket]['.$package->getPackageId().']['.$dlAddressTypeCamelCased.'][prefix]',
                    'FirstName' => 'delivery[pakket]['.$package->getPackageId().']['.$dlAddressTypeCamelCased.'][firstname]',
                    'LastName' => 'delivery[pakket]['.$package->getPackageId().']['.$dlAddressTypeCamelCased.'][lastname]',
                    'RegistrationName'  => 'delivery[pakket]['.$package->getPackageId().']['.$dlAddressTypeCamelCased.'][company_name]',
                );
            default:
                return array(
                    'Salutation' => 'delivery[other_address][gender]',
                    'Title' => 'delivery[other_address][prefix]',
                    'FirstName' => 'delivery[other_address][firstname]',
                    'LastName' => 'delivery[other_address][lastname]',
                    'RegistrationName'  => 'delivery[other_address][company_name]',
                );
        }
    }

    /**
     * @param Dyna_Package_Model_Package $package
     * @return bool
     */
    protected function deliveryAddressIsSameAs($package,$addressType)
    {
        switch($this->clientHelper->getCheckout('delivery[method]')) {
            case $addressType:
                return true;
            case Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SPLIT_DELIVERY:
                return ($this->clientHelper->getCheckout('delivery[pakket]['.$package->getPackageId().'][method]') == $addressType);
        }
        return false;
    }

    /**
     * @param Dyna_Package_Model_Package $package
     * @return bool
     */
    protected function billingAddressIsSameAsCustomerAddress($package)
    {
        switch($this->clientHelper->getCheckout('billing[other_address][selected]')) {
            case Dyna_Checkout_Block_Cart_Steps_SaveCustomer::SAME_AS_CUSTOMER_ADDRESS:
                return true;
            case Dyna_Checkout_Block_Cart_Steps_SaveCustomer::MULTIPLE_BILLING_ADDRESSES:
                return ($this->clientHelper->getCheckout('billing[pakket]['.$package->getPackageId().'][other_address][selected]') == Dyna_Checkout_Block_Cart_Steps_SaveCustomer::SAME_AS_CUSTOMER_ADDRESS);
        }
        return false;
    }



}
