<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

use Dompdf\Dompdf;

/**
 * Class Dyna_Superorder_Model_Client_SendDocumentClient
 */
class Dyna_Superorder_Model_Client_SendDocumentClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "send_document/wsdl_send_document";
    const ENDPOINT_CONFIG_KEY = "send_document/endpoint_send_document";
    const CONFIG_STUB_PATH = 'send_document/use_stubs';

    // For sendDocument party name should allways set to mail @see OMNVFDE-1814 and https://dynalean.atlassian.net/wiki/display/OMNVFDE/I1011+-+SendDocument for mapping
    const PARTY_NAME = "mail";
    const CONTACT_PHONE_NUMBER_TYPE = "CallBack";
    const SOHO_SALUTATION = 'Firma';

    /** @var  Dyna_Customer_Model_Customer */
    protected $customer = null;
    /** @var Dyna_AgentDE_Model_Agent */
    protected $agent = null;

    /** @var Dyna_Superorder_Helper_Client $clientHelper */
    protected $clientHelper;

    protected $type = null;
    protected $quote = null;
    protected $superorder = null;

    /**
     * @param $quote @param Dyna_Checkout_Model_Sales_Quote $quote
     * @param $type
     * @param $superorder
     * @return array
     */
    public function executeSendDocument(
        $quote,
        $type,
        $superorder = null,
        $customerParams = null,
        $checkoutFields = null
    ): array {
        $this->type = $type;
        $this->quote = $quote;
        $this->superorder = $superorder;
        /** @var Dyna_Checkout_Model_Field $orderCheckoutData */
        if ($checkoutFields !== null) {
            $orderCheckoutData = $checkoutFields;
        } else {
            $orderCheckoutData = Mage::getModel('dyna_checkout/field')
                ->loadQuoteData(array('quote_id' => $quote->getId()));
        }

        // Set checkout data on superorder client
        Mage::helper('dyna_superorder/client')->setClientData($orderCheckoutData, $this->getCustomer());
        $this->clientHelper = Mage::helper('dyna_superorder/client');
        $documentName = '';
        $allPackages = $this->getPackages();
        $package = is_array($allPackages) ? reset($allPackages) : $allPackages->getFirstItem();
        $useCase = '';

        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();


        switch ($type) {
            case 'Offer' :
                $useCase = 'OSF-OfferSummary';
                $printOutput = Mage::app()->getLayout()
                    ->createBlock('dyna_checkout/offer')
                    ->init($quote)
                    ->setTemplate('checkout/cart/pdf/offer.phtml')
                    ->toHtml();

                $pdfInstance = new DOMPDF;
                $pdfInstance->loadHtml($printOutput);
                $pdfInstance->render();
                $pdfOutput = $pdfInstance->output();

                //Get path to offers dir for export
                $offersDir = $this->getOffersDir();

                // as stated in VFDED1W3S-3410, naming of pdf's is simplified in Wave 3
                $documentName = 'Ihr Angebot';

                file_put_contents($offersDir . DS . date("Y-m-d-H-i-s") . '_' . $quote->getId() . '_' . $documentName . '.pdf', $pdfOutput);
                break;

            case 'Order' :
                $useCase = 'OSF-CallSummary';
                if ((count($allPackages) == 1) && $this->hasDsl()) {
                    // if the order has only one package of type DSL, do not create Call Summary pdf anymore
                    return array('Success' => true);
                }
                $printOutput = Mage::app()->getLayout()
                    ->createBlock('dyna_checkout/call')
                    ->init($quote, $superorder, $checkoutFields)
                    ->setTemplate('checkout/cart/pdf/call.phtml')
                    ->toHtml();
                $pdfInstance = new DOMPDF;
                $pdfInstance->loadHtml($printOutput);
                $pdfInstance->render();
                $pdfOutput = $pdfInstance->output();

                if (!is_dir(Mage::getBaseDir('media') . DS . 'calls')) {
                    mkdir(Mage::getBaseDir('media') . DS . 'calls', 0777, true);
                }

                $documentName = 'Ihre Bestellung';

                file_put_contents(Mage::getBaseDir('media') . DS . 'calls' . DS . date("Y-m-d-H-i-s") . '_' . $quote->getId() . '_' . $documentName . '.pdf', $pdfOutput);

                break;
            case 'Cart' :
                $useCase = 'OSF-InfoSummary';
                $printOutput = Mage::app()->getLayout()
                    ->createBlock('dyna_checkout/info')
                    ->init($quote)
                    ->setCustomerFirstname($customerParams['firstname'])
                    ->setCustomerLastname($customerParams['lastname'])
                    ->setTemplate('checkout/cart/pdf/info.phtml')
                    ->toHtml();

                $pdfInstance = new DOMPDF;
                $pdfInstance->loadHtml($printOutput);
                $pdfInstance->render();
                $pdfOutput = $pdfInstance->output();

                //Get path to info summary dir for export
                $infoSummaryDir = $this->getInfoSummaryDir();

                $documentName = 'Ihr Angebot';
                file_put_contents($infoSummaryDir . DS . date("Y-m-d-H-i-s") . '_' . $quote->getId() . '_' . $documentName . '.pdf', $pdfOutput);
                break;
        }

        /** @var Dyna_AgentDE_Model_Agent $agent */
        $this->agent = Mage::getSingleton('customer/session')->getAgent();
        $hotlineNumber = $this->agent->getDealer()->getHotlineNumber();
        $hotlineNumberExplode = empty($hotlineNumber) ? array('', '') : explode('/', $hotlineNumber);

        $documentList = array(
            array(
                'DocumentName' => $documentName . '.pdf',
                'DocumentFormat' => 'pdf',
                'DocContentBase64' => base64_encode($pdfOutput),
                'DocPosition' => '1',
            ),
        );

        if ($type == 'Order' && $this->hasMobile()) {
            $docs = array(
                '2' => 'Anlage_Widerrufsrecht_Mobilfunk'
            );
            foreach ($this->getPackages() as $package) {
                if ($package->hasHandset()) {
                    $docs['3'] = 'Beitrittserklaerung_Handy_Versicherung_0916';
                    break;
                }
            }

            foreach ($docs as $position => $doc) {
                $tmpfile = Mage::getBaseDir('media') . '/doc/' . $doc . '.pdf';
                //if file does not exist, skip it
                if (!file_exists($tmpfile)) {
                    continue;
                }
                $handle = fopen($tmpfile, "r");
                $contents = fread($handle, filesize($tmpfile));
                fclose($handle);

                array_push($documentList, array(
                    'DocumentName' => $doc . '.pdf',
                    'DocumentFormat' => 'pdf',
                    'DocContentBase64' => base64_encode($contents),
                    'DocPosition' => $position,
                ));
            }
        }

        $contactPhonePrefix = $this->clientHelper->getCheckout('customer[contact_details][phone_list][0][telephone_prefix]');
        $contactPhoneNumber = $this->clientHelper->getCheckout('customer[contact_details][phone_list][0][telephone]');
        $communicationType = ($this->clientHelper->getCheckout('status_email') == true) ? 'Email' : (($this->clientHelper->getCheckout('status_sms') == true) ? 'SMS' : 'Email');
        $customerTitle = $this->clientHelper->getCheckout('customer[prefix]');

        $partyType = Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SHORT_PRIVATE;

        $smsNumber = null;
        if (Mage::app()->getRequest()->getParam('pushphone')) {
            $smsNumber = Mage::app()->getRequest()->getParam('pushphone');
        }

        switch (true) {
            case $customer->getIsSoho():
                $partyType = Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SHORT_SOHO;
                break;
            case isset($customerParams['companyname']):
                $partyType = Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SHORT_SOHO;
                break;
            case strtoupper($this->clientHelper->getCustomerType('customer[type]')) == Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SOHO:
                $partyType = Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SHORT_SOHO;
                break;
        }

        $customerFirstName = null;
        switch (true) {
            case isset($customerParams['firstname']) :
                $customerFirstName = $customerParams['firstname'];
                break;
            case !empty($this->clientHelper->getCheckout('customer[firstname]')):
                $customerFirstName = $this->clientHelper->getCheckout('customer[firstname]');
                break;
        }

        $customerLastName = null;
        if ($partyType != Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SHORT_SOHO) {
            switch (true) {
                case isset($customerParams['lastname']):
                    $customerLastName = $customerParams['lastname'];
                    break;
                case !empty($this->clientHelper->getCheckout('customer[lastname]')):
                    $customerLastName = $this->clientHelper->getCheckout('customer[lastname]');
                    break;
            }
        }

        $customerCity = 'Dummy';
        switch (true) {
            case (null !== $customer->getCountry()) :
                $customerCity = $customer->getCountry();
                break;
            case !empty($this->clientHelper->getCheckout('customer[address][city]')):
                $customerCity = $this->clientHelper->getCheckout('customer[address][city]');
                break;
        }

        $email = null;
        switch (true) {
            case isset($customerParams['email']):
                $email = $customerParams['email'];
                break;
            case !empty(Mage::app()->getRequest()->getParam('email')) :
                $email = Mage::app()->getRequest()->getParam('email');
                break;
            case !empty($this->clientHelper->getCheckout('customer[contact_details][email]')):
                $email = $this->clientHelper->getCheckout('customer[contact_details][email]');
                break;
        }

        $partyName = null;
        if ($partyType == Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SHORT_SOHO) {
            switch (true) {
                case isset($customerParams['companyname']) :
                    $partyName = $customerParams['companyname'];
                    break;
                case !empty($this->clientHelper->getCheckout('customer[address][company_name]')):
                    $partyName = $this->clientHelper->getCheckout('customer[address][company_name]');
                    break;
                case (($useCase == 'OSF-InfoSummary') && !empty($customer->getCompanyName())):
                    $partyName = $customer->getCompanyName();
                    break;
            }
        }

        // When customer is SOHO but the company name field is not available in checkout send the customer last name in request
        if ($partyType == Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SHORT_SOHO && empty($partyName)) {
            $customerLastName = $this->clientHelper->getCheckout('customer[lastname]');
        }

        $params = array(
            'SalesOrder' => array(
                'ID' => $quote->getId(),
                'Customer' => array(
                    'PartyIdentification' => array(
                        'ID' => $this->clientHelper->getCustomerNumber($this->superorder ? $this->superorder : $this->quote),
                    ),
                    'PartyName' => trim($partyName) ? array(
                        'Name' => $partyName
                    ) : null,
                    'AccountCategory' => $this->getOneAccountCategory($quote, $customer),
                    'Epithet' => $this->clientHelper->getCheckout('customer[address][company_name]'),
                    'AccountClass' => $this->getAccountClass($this->superorder ?: $this->quote),
                    'PartyType' => $partyType ?? '',
                    'ContactPhoneNumber' => array(
                        'CountryCode' => $customerParams['prefix'] ?: '',
                        'PhoneNumber' => $customerParams['telephone'] ?: '',
                        'Type' => self::CONTACT_PHONE_NUMBER_TYPE,
                    ),
                    'Role' => array(
                        'Person' => array(
                            'FirstName' => $customerFirstName,
                            'FamilyName' => $customerLastName,
                            'Title' => $customerTitle == null ? null : $customerTitle,
                            'AdditionalTitle' => '',
                            'Salutation' => $this->getCustomer()->getIsSoho() ? self::SOHO_SALUTATION : ($this->clientHelper->getCheckout('customer[gender]') ? $this->clientHelper->getSalutation('customer[gender]') : null),
                        ),
                    ),
                ),
                'LogicalPackage' => array(
                    'LogicalPackageID' => array(
                        'ID' => $package->getPackageId()
                    ),
                    'PackageType' => $this->getOrderType(),
                    'CustomerSequenceID' => array(
                        'ID' => ''
                    ),
                    'BundleType' => $this->getBundleType(),
                    'OGWOrderLineID' => array(
                        'ID' => $package->getOGWOrderLineIDNode()['ID']
                    ),
                    'Subscriber' => array(
                        'Ctn' => array(
                            'PhoneNumber' => $contactPhonePrefix . $contactPhoneNumber,
                        ),
                        'Address' => array(
                            'StreetName' => $this->clientHelper->getCheckout('customer[address][street]'),
                            'BuildingNumber' => $this->clientHelper->getCheckout('customer[address][houseno]'),
                            'InhouseMail' => '',
                            'Department' => '',
                            'MarkAttention' => '',
                            'MarkCare' => '',
                            'PlotIdentification' => '',
                            'CitySubdivisionName' => '',
                            'CityName' => $customerCity,
                            'PostalZone' => $this->clientHelper->getCheckout('customer[address][postcode]'),
                            'CountrySubentity' => ''
                        ),
                        'Contact' => array(
                            'Contact' => array(
                                'ElectronicMail' => $email
                            )
                        )
                    )
                )
            ),
            'Document' => array(
                'CommunicationType' => $communicationType,
                'SmsNumber' => $smsNumber,
                'OfferSummaryDate' => date("Y-m-d"),
                'Archive' => 'false',
                'UseCase' => $useCase,
                'DocumentList' => $documentList,
                'BounceInfoEmailAddress' => $this->agent->getDealer()->getBounceInfoEmailAddress()
            )
        );

        if ($hotlineNumberExplode[1]) {
            $params['SalesOrder']['Customer']['ContactPhoneNumber'] = array(
                'CountryCode' => str_replace('+', '', $hotlineNumberExplode[0]),
                'LocalAreaCode' => null,
                'PhoneNumber' => $hotlineNumberExplode[1],
                'Type' => self::CONTACT_PHONE_NUMBER_TYPE
            );
        }

        $this->setRequestHeaderInfo($params);

        //To make sure confing from this class is the last one that has been loaded
        $this->loadConfig();

        return $this->SendDocument($params);
    }

    /**
     * Get party_name from the configurable input in Admin
     * @return bool
     */
    public function getPartyName()
    {
        return self::PARTY_NAME;
    }

    /**
     * Get session customer
     * @return Dyna_Customer_Model_Customer|Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        if (!$this->customer) {
            $this->customer = Mage::getSingleton('customer/session')->getCustomer();
        }

        return $this->customer;
    }

    /**
     * Return account categories the current customer belongs to based on current packages
     * @param $quote Dyna_Checkout_Model_Sales_Quote
     * @return string
     */
    public function getAccountCategory(Dyna_Checkout_Model_Sales_Quote $quote = null, $requestValue = true)
    {
        $mobilePostpaidPackages = Dyna_Catalog_Model_Type::getPostPaidMobilePackages();
        $mobilePrepaidPackages = Dyna_Catalog_Model_Type::getPrePaidMobilePackages();
        $cablePackages = Dyna_Catalog_Model_Type::getCablePackages();
        $fixedPackages = Dyna_Catalog_Model_Type::getFixedPackages();

        // Get current quote packages
        if (!isset($quote)) {
            $quote = $this->quote;
        }
        $packages = $quote->getCartPackages() ? $quote->getCartPackages() : $this->getPackages();

        $accountCategories = array();

        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            $packageType = strtolower($package->getType());

            if (in_array($packageType, $mobilePostpaidPackages)) {
                $accountCategories[] = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_POSTPAID;
            } elseif (in_array($packageType, $mobilePrepaidPackages)) {
                $accountCategories[] = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_PREPAID;
            } elseif (in_array($packageType, $cablePackages)) {
                $accountCategories[] = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_CABLE;
            } elseif (in_array($packageType, $fixedPackages)) {
                $accountCategories[] = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_FIXED;
            }

            $accountCategories = array_unique($accountCategories);
        }

        return $accountCategories;
    }

    /**
     * Returns the order action based on install base packages and new packages
     * @return mixed
     */
    public function getOrderType()
    {
        $oldPackageMappings = $this->clientHelper->getOldPackageMappings();
        $newPackageMappings = $this->clientHelper->getNewPackageMappings();

        $newPackages = [];
        $oldPackages = [];

        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $serviceCustomersAccountCategories = array_keys($customerSession->getServiceCustomers() ?? []);
        $cartAccountCategories = $this->getAccountCategory(null, false);
        $migrationOrMoveOffnetState = Mage::getSingleton('checkout/session')->getInMigrationOrMoveOffnetState();

        if ($migrationOrMoveOffnetState) {
            $oldPackages[] = ($migrationOrMoveOffnetState == Dyna_Catalog_Model_Type::MIGRATION) ? Dyna_Superorder_Helper_Client::ORDER_TYPE_CABLE_FROM_DSL : Dyna_Superorder_Helper_Client::ORDER_TYPE_DSL_FROM_CABLE;

            if (in_array(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS, $serviceCustomersAccountCategories)) {
                $oldPackages[] = $oldPackageMappings[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS];
            }
            if (in_array(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS, $cartAccountCategories)) {
                $newPackages[] = $newPackageMappings[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS];
            }
        } else {
            foreach ($serviceCustomersAccountCategories as $key => $accountCategory) {
                $oldPackages[] = $oldPackageMappings[$accountCategory];
            }
            foreach ($cartAccountCategories as $key => $accountCategory) {
                $newPackages[] = $newPackageMappings[$accountCategory];
            }
        }

        //check to make sure the new_old/old_new format is supported
        $orderTypesXmlPath = Mage::getModuleDir('etc', 'Dyna_Superorder') . DS . 'orderTypes.xml';
        $orderTypesXmlContent = file_get_contents($orderTypesXmlPath);
        $orderTypesXmlData = get_object_vars(simplexml_load_string($orderTypesXmlContent, 'Varien_Simplexml_Element'));
        $value = implode('_', array_merge(array_unique($oldPackages), array_unique($newPackages)));
        if (!in_array($value, $orderTypesXmlData['orderType'])) {
            $value = implode('_', array_merge(array_unique($newPackages), array_unique($oldPackages)));
        }

        return $value;
    }

    /**
     * @return string
     */
    protected function getBundleType()
    {
        // todo: check if we should send noGIGAKOMBI in case that no bundle is present is cart
        return $this->isGigacombi() ? 'GIGAKOMBI' : 'noGIGAKOMBI';
    }

    /**
     * Return path for offers dir (if not exist it will create it)
     * @return string
     */
    protected function getOffersDir()
    {
        if (!is_dir(Mage::getBaseDir('media') . DS . 'offers')) {
            mkdir(Mage::getBaseDir('media') . DS . 'offers', 0777, true);
        }

        return Mage::getBaseDir('media') . DS . 'offers';
    }

    /**
     * Return path for info summary dir (if not exist it will create it)
     * @return string
     */
    private function getInfoSummaryDir()
    {
        if (!is_dir(Mage::getBaseDir('media') . DS . 'carts')) {
            mkdir(Mage::getBaseDir('media') . DS . 'carts', 0777, true);
        }

        return Mage::getBaseDir('media') . DS . 'carts';
    }

    /**
     * @return string
     */
    protected function getPackageType()
    {
        if ($this->hasDsl()) {
            $packageType = 'DSL';
        }
        if ($this->hasLte()) {
            $packageType = 'LTE';
        }
        if ($this->hasCable()) {
            $packageType = 'Cable';
        }
        if ($this->hasMobile()) {
            $packageType = 'Mobile';
        }
        return $packageType ?: '';
    }

    /**
     * Check if there is at least a package of type fixed (DSL) in the cart
     *
     * @return bool
     */
    protected function hasDsl()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfCreationType(
            $this->getPackages(),
            Mage::helper('dyna_configurator/cart')->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_DSL)
        );
    }

    /**
     * Check if there is at least a package of type fixed (LTE) in the cart
     *
     * @return bool
     */
    protected function hasLte()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfCreationType(
            $this->getPackages(),
            Mage::helper('dyna_configurator/cart')->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_LTE)
        );
    }

    /**
     * Retrieve the quote packages
     *
     * @return array
     */
    protected function getPackages()
    {
        if ($this->type == 'Order') {
            return $this->superorder->getPackages();
        }
        /** @var Dyna_Checkout_Model_Sales_Quote $quoteModel */
        $quoteModel = Mage::getSingleton('checkout/cart')->getQuote();
        $tempPackages = $quoteModel->getPackages();

        $packages = [];
        foreach ($tempPackages as $id => $package) {
            /** @var Dyna_Package_Model_Package $packageModel */
            $packageModel = $quoteModel->getCartPackage($id);
            $packageModel->setData('items', $package['items']);
            if (!$packageModel->getInstalledBaseProducts()) {
                $packages[] = $packageModel;
            }
        }

        return $packages;
    }

    /**
     * Check if there is atleast a package of type cable in the cart
     *
     * @return bool
     */
    protected function hasCable()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfCreationType(
            $this->getPackages(),
            Dyna_Catalog_Model_Type::getCableCreationTypes(),
            true
        );
    }

    /**
     * Check if there is atleast a package of type mobile (postpaid) in the cart
     *
     * @return bool
     */
    protected function hasMobile()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfCreationType(
            $this->getPackages(),
            Dyna_Catalog_Model_Type::getMobileCreationTypes()
        );
    }

    /**
     * Check if customer products contain Gigakombi bundle
     * @return bool
     */
    protected function isGigacombi()
    {
        //no point in checking if this is a new customer scenario
        if (!Mage::getSingleton('customer/session')->getCustomer()->getCustomerNumber()) {
            return false;
        }
        // check if any package from cart is part of a GigaKombi bundle
        foreach ($this->quote->getCartPackages() as $package) {
            if ($package->isPartOfGigaKombi()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    protected function getAllItems()
    {
        if ($this->type == 'Offer') {
            return $this->quote->getAllItems();
        }
        $tempItems = $this->superorder->getOrderedItems(true, false);
        $items = array();
        foreach ($tempItems as $it) {
            $items = array_merge($items, $it);
        }
        return $items;
    }

    /**
     * Identify the correct account class based on package type
     * @param $quoteOrSuperorder
     * @return null|string
     */
    protected function getAccountClass($quoteOrSuperorder)
    {
        $accountClass = null;

        foreach ($quoteOrSuperorder->getOrderNumber() ? $quoteOrSuperorder->getPackages() : $quoteOrSuperorder->getCartPackages() as $package) {
            if ($package->getInstalledBaseProducts()) {
                continue;
            }

            switch (strtolower($package->getType())) {
                case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_LTE):
                    $accountClass = 'VR';
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
                    $accountClass = 'KD';
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
                    $accountClass = 'MMC';
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
                    $accountClass = 'MMO';
                    break;
                default :
                    $accountClass = null;
                    break;
            }
        }

        return $accountClass;
    }

    /**
     * @param $quote
     * @param $customer
     * @return array
     */
    protected function getOneAccountCategory($quote, $customer)
    {
        $accountCategory = null;
        $accountCategories = $this->getAccountCategory($quote);

        if (true === empty($customer->isCustomerLoggedIn())) {
            $accountCategory = $accountCategories[0];
        } else {
            $packages = $quote->getCartPackages() ? $quote->getCartPackages() : $this->getPackages();
            $mobilePostpaidPackages = Dyna_Catalog_Model_Type::getPostPaidMobilePackages();
            $mobilePrepaidPackages = Dyna_Catalog_Model_Type::getPrePaidMobilePackages();
            $cablePackages = Dyna_Catalog_Model_Type::getCablePackages();
            $fixedPackages = Dyna_Catalog_Model_Type::getFixedPackages();


            foreach ($packages as $package) {
                if ($package->getData('parent_account_number') != '') {

                    $packageType = strtolower($package->getType());

                    if (in_array($packageType, $mobilePostpaidPackages)) {
                        $accountCategory = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_POSTPAID;
                    } elseif (in_array($packageType, $mobilePrepaidPackages)) {
                        $accountCategory = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_PREPAID;
                    } elseif (in_array($packageType, $cablePackages)) {
                        $accountCategory = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_CABLE;
                    } elseif (in_array($packageType, $fixedPackages)) {
                        $accountCategory = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_FIXED;
                    }

                    break;
                }
            }

            if ($accountCategory == null) {
                $accountCategory = $accountCategories[0];
            }
        }

        return $accountCategory;
    }
}
