<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Data
 */
class Dyna_Superorder_Helper_Esb extends Mage_Core_Helper_Abstract
{
    /**
     * @param $msg
     */
    private function _logMsg($msg, $level = null)
    {
        Mage::log($msg, $level, 'soap_esb.log');
    }

    /**
     * @param $data
     * @return string
     */
    public function setDeliveryDetails($data)
    {
        $this->_logMsg('Received setDeliveryDetails with data: ' . $data);

        list(, $param2, $param3, $param4) = explode(';', $data . ";");
        //added extra semicolon due to message inconsistency as sometimes the last param is not sent

        $orderNumber = $param2;
        $order = Mage::getModel('sales/order')->load($orderNumber, 'increment_id');
        if (!$order || !$order->getId()) {
            $this->_logMsg('Order not found: ' . $orderNumber);

            return 'Error: Order not found';
        }
        $plannedDate = trim($param3); // dd-mm-yyyy hh:mm
        $carrierName = trim($param4);
        if (strpos($param4, ',') !== false) {
            $param4Array = explode(',', $param4);
            $trackAndTraceUrl = !empty($param4Array[1]) ? trim($param4Array[1]) : null;
            $carrierName = trim($param4Array[0]);
            // prevent replacing a previously set url with an empty url
            if ($trackAndTraceUrl) {
                $order->setTrackAndTraceUrl($trackAndTraceUrl);
            }
        }
        if ($carrierName) {
            $order->setCarrierName($carrierName);
        }
        if ($plannedDate) {
            $order->setPlannedDate($plannedDate);
        }
        $order->save();

        return 'Success: Changed delivery details';
    }

    public function setTemporaryCustomerId($data)
    {
        $this->_logMsg('Received setTemporaryCustomerId with data: ' . $data);

        list($param1, $param2, $param3) = explode(';', $data . ";");
        $superOrderOgwId = $param1;
        $tempCustomerNo = $param2;
        $packageType = $param3;

        $packages = Mage::getModel('package/package')->getPackagesWithOgwId($superOrderOgwId, $packageType);
        if(!$packages->getData()) {
            $this->_logMsg('No package found');
            return 'Error: No package found';
        }

        try{
            foreach ($packages as $package) {
                $write = Mage::getSingleton('core/resource')->getConnection('core_read');
                $rows = $write->exec("UPDATE catalog_package SET parent_account_number ='" .$tempCustomerNo . "' WHERE entity_id = '" . $package->getId()."'");

                if ($rows <= 0) {
                    return 'Error: Invalid order ID/package_id or ciboodle_cases was already set to this value';
                }
                $this->_logMsg('Package with id = '.$package->getId() .' was updated with customerNo '. $tempCustomerNo);
            }
        }
        catch(\Exception $e) {
            $this->_logMsg('Error '. $e->getTraceAsString());
            $this->_logMsg('Error '. $e->getMessage());
            return 'Error: An error occurred';
        }

        return 'Success: Set Temporary Customer Number';
    }

    public function updateTemporaryCustomerId($data)
    {
        $this->_logMsg('Received updateTemporaryCustomerId with data: ' . $data);

        list($param1, $param2) = explode(';', $data . ";");
        $tempCustomerNo = $param1;
        $finalCustomerNo = $param2;

        $packages = Mage::getModel('package/package')->getPackagesWithCustomerNo($tempCustomerNo);
        if(!$packages->getData()) {
            $this->_logMsg('No package found');
            return 'Error: No package found';
        }

        try{
            foreach ($packages as $package) {
                if($package->getId()) {
                    $package->setParentAccountNumber($finalCustomerNo);
                    $package->save(true);
                    $this->_logMsg('Package with id = '.$package->getId() .' was updated with customerNo '. $finalCustomerNo);
                }
            }
        }
        catch(\Exception $e) {
            $this->_logMsg('Error '. $e->getTraceAsString());
            $this->_logMsg('Error '. $e->getMessage());
            return 'Error: An error occurred';
        }

        return 'Success: Set Final Customer Number';
    }

    /**
     * @param $data
     * @return string
     */
    public function setCiboodleId($data)
    {
        $this->_logMsg('Received setCiboodleId with data: ' . $data);

        list($param1, $param2) = explode(';', $data);

        $packageInfo = explode('.', $param1);
        $orderNumber = $packageInfo[0];
        $superorder = Mage::getModel('superorder/superorder')->load($orderNumber, 'order_number');
        $packageNr = $packageInfo[1]; // Format used is saleOrderNumber.PackageNr, xxxxx.1 xxxxx.2 etc
        $ciBoodleId = $param2;
        $write = Mage::getSingleton('core/resource')->getConnection('core_read');
        $rows = $write->exec("UPDATE catalog_package SET updated_at='" . now() . "', ciboodle_cases = '" . $ciBoodleId . "' WHERE order_id = '" . $superorder->getEntityId() . "' AND package_id = '" . $packageNr . "'");

        if ($rows <= 0) {
            return 'Error: Invalid order ID/package_id or ciboodle_cases was already set to this value';
        }

        return 'Success: Changed ciboodle_cases';
    }

    /**
     * @param $data
     * @return string
     */
    public function setSaleOrderError($data)
    {
        $this->_logMsg('Received setSaleOrderError with data: ' . $data);

        list($param1, $param2, $param3) = explode(';', $data . ";");
        //added extra semicolon due to message inconsistency as sometimes the last param is not sent

        $orderNumber = $param1;
        $errorCode = $param2;
        $errorMessage = $param3;

        if (empty($orderNumber)) {
            return 'Error: Empty order number';
        }
        if (empty($errorCode) || !is_numeric($errorCode)) {
            return 'Error: Incorrect error code';
        }

        $write = Mage::getSingleton('core/resource')->getConnection('core_read');
        $error = Mage::getModel('superorder/errors')->setErrorCode($errorCode);
        if ($errorMessage) {
            $error->setErrorDetail($errorMessage);
            $rows = $write->exec("UPDATE superorder SET error_code = '" . $errorCode . "', error_detail = '" . addslashes($errorMessage) . "' WHERE order_number = '" . $orderNumber . "'");
        } else {
            $rows = $write->exec("UPDATE superorder SET error_code = '" . $errorCode . "' WHERE order_number = '" . $orderNumber . "'");
        }

        // Create a saved shopping cart from the quote of the failed order
        $superOrder = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('order_number', $orderNumber)->getLastItem();
        $error->setSuperorderId($superOrder->getId())->setCreatedAt(now())->save();

        if ($superOrder && $superOrder->getId()) {
            $this->generateQuoteOnFail($superOrder);
        }

        if ($rows <= 0) {
            $message = 'Error: Invalid order ID or status was already set to this value';
        } else {
            $message = 'Success: Changed sales order error code';
        }

        return $message;
    }

    /**
     * @param $data
     * @return string
     * @throws Mage_Core_Exception
     */
    public function setSaleOrderStatus($data)
    {
        $this->_logMsg('Received setSaleOrderStatus with data: ' . $data);

        list($param1, $param2, $param3) = explode(';', $data . ";");

        $orderNumber = trim($param1);
        $status = trim($param2);
        $statusTranslate = trim($param3);
        if ($status == 'Validation Approved') {
            $status = Dyna_Superorder_Model_Superorder::SO_VALIDATED;
        }

        if (empty($orderNumber)) {
            return 'Error: Empty order number';
        }
        if (empty($status)) {
            return 'Error: Empty status';
        }

        $write = Mage::getSingleton('core/resource')->getConnection('core_read');
        $rows = $write->exec("UPDATE superorder SET order_status = '" . $status . "', order_status_translate='" . $statusTranslate . "' WHERE order_number = '" . $orderNumber . "'");

        if ($rows <= 0) {
            $message = 'Error: Invalid order ID or status was already set to this value';
        }
        else {
            $message = 'Success: Changed sales order status';
        }

        return $message;
    }

    /**
     * @param $data
     * @return string
     */
    public function setDeliveryOrderStatus($data)
    {
        $this->_logMsg('Received setDeliveryOrderStatus with data: ' . $data);

        list(, $param1, $param2) = explode(';', $data);

        $orderNumber = $param1;
        $status = $param2;
        $statusCode = str_replace(" ", "_", strtolower($status));
        if (empty($orderNumber)) {
            return 'Error: Empty order number';
        }
        if (empty($status)) {
            return 'Error: Empty status';
        }

        try {
            $order = Mage::getModel('sales/order')->load($orderNumber, 'increment_id');
            if ($order && $order->getId()) {
                $this->_logMsg('Setting status for ' . $orderNumber . ' to: ' . $statusCode);
                $order->setStatus($statusCode, true)->save();
            } else {
                $this->_logMsg('Unable to set status for ' . $orderNumber . ' to: ' . $statusCode . '. Order not found');
            }
        } catch (Exception $ex) {
            // do nothing
        }

        return 'OK';
    }

    /**
     * @param $data
     * @return string
     */
    public function setCustomerBan($data)
    {
        $this->_logMsg('Received setCustomerBan with data: ' . $data);

        //added order number for future purposes although it is not used at this point
        list(, $param1, $param2, $param3) = explode(';', $data . ";");
        //added extra semicolon due to message inconsistency as sometimes the last param is not sent

        $customerId = $param1;
        $customerBan = $param2;
        $customerMmlStatus = $param3;

        $customer = Mage::getModel('customer/customer')->load($customerId);
        if ($customer && $customer->getId()) {
            $customer->setBan($customerBan);
            $customer->setCustomerLabel($customerMmlStatus);
            $customer->save();

            return 'Success';
        }

        return 'Error: Customer does not exists';
    }

    /**
     * @param $data
     * @return string
     */
    public function setVfOrderNr($data)
    {
        $this->_logMsg('Received setVfOrderNr with data: ' . $data);

        list($param1, $param2, $param3) = explode(';', $data);

        $orderNumber = $param1;
        $packageNumber = $param2;
        $vfNumber = $param3;
        $so = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('order_number', $orderNumber)->getLastItem();
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }

        $write = Mage::getSingleton('core/resource')->getConnection('core_read');
        $rows = $write->exec("UPDATE catalog_package SET updated_at='" . now() . "', package_esb_number = '" . $vfNumber . "' WHERE order_id = '" . $so->getId() . "' AND package_id = '" . $packageNumber . "'");
        if ($rows <= 0) {
            return 'Error: Invalid order ID or package ID, or package_esb_number was already set to this value';
        }

        return 'Success';
    }

    /**
     * @param $esbPackageNr
     * @return mixed
     */
    private function _getPackageNr($esbPackageNr)
    {
        $parts = explode('.', $esbPackageNr);

        return ((count($parts) > 1) ? $parts[1] : $parts[0]);
    }

    /**
     * @param $data
     * @return string
     */
    public function setVfStatusCode($data)
    {
        $this->_logMsg('Received setVfStatusCode with data: ' . $data);

        list($param1, $param2, $param3, $param4) = explode(';', $data);

        $orderNumber = $param1;
        $packageNumber = $this->_getPackageNr($param2);
        $vfStatusCode = trim($param3);
        $vfStatusDesc = trim($param4);
        try {

            $so = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('order_number', $orderNumber)->getLastItem();
            if (!$so || !$so->getId()) {
                throw new Exception('Error: Order does not exists');
            }
            $write = Mage::getSingleton('core/resource')->getConnection('core_read');

            $package = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('order_id', $so->getId())
                ->addFieldToFilter('package_id', $packageNumber)
                ->getFirstItem();

            if (strpos($vfStatusCode, 'CRD') === 0) {
                // This is credit case update
                $rows = $write->exec("UPDATE catalog_package SET updated_at='" . now() . "', vf_creditcode = '" . $vfStatusCode . "' WHERE order_id = '" . $so->getId() . "' AND package_id = '" . $packageNumber . "'");

                if ($rows <= 0) {
                    throw new Exception('Error: Invalid order ID or package ID, or vf_creditcode was already set to this value');
                }

                Mage::getModel('superorder/statusHistory')
                    ->setType(Omnius_Superorder_Model_StatusHistory::PACKAGE_CREDIT_CODE)
                    ->setPackageId($package->getId())
                    ->setStatus($vfStatusCode)
                    ->setCreatedAt(now())
                    ->save();
            } elseif ($vfStatusCode != 'VOD-00-000-000') {
                // Create the original quote as shopping cart
                $this->generateQuoteOnFail($so);

                $rows = $write->exec("UPDATE catalog_package SET updated_at='" . now() . "', status = '" . Dyna_Package_Model_Package::ESB_PACKAGE_STATUS_FAILED . "', vf_status_code = '" . $vfStatusCode . "', vf_status_desc = '" . $vfStatusDesc . "' WHERE order_id = '" . $so->getId() . "' AND package_id = '" . $packageNumber . "'");
                // Also set the error on sale order level, temporary fix; otherwise its not visible in failed order screen for telesales

                if ($so->getId()) {
                    $write->exec("update superorder set error_code='" . addslashes($vfStatusCode) . "', error_detail='" . addslashes($vfStatusDesc) . "' where entity_id=" . $so->getId());
                    Mage::getModel('superorder/errors')->setErrorCode($vfStatusCode)->setErrorDetail($vfStatusDesc)->setSuperorderId($so->getId())->setCreatedAt(now())->save();
                }

                if ($rows <= 0) {
                    throw new Exception('Error: Invalid order ID or package ID, or status was already set to this value');
                }

                Mage::getModel('superorder/statusHistory')
                    ->setType(Omnius_Superorder_Model_StatusHistory::PACKAGE_STATUS)
                    ->setPackageId($package->getId())
                    ->setStatus(Dyna_Package_Model_Package::ESB_PACKAGE_STATUS_FAILED)
                    ->setCreatedAt(now())
                    ->save();

                Mage::getModel('superorder/statusHistory')
                    ->setType(Omnius_Superorder_Model_StatusHistory::PACKAGE_VF_STATUS_CODE)
                    ->setPackageId($package->getId())
                    ->setStatus($vfStatusCode)
                    ->setCreatedAt(now())
                    ->save();
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return 'Success';
    }

    /**
     * @param $data
     * @return string
     */
    public function setVfCreditStatus($data)
    {
        $this->_logMsg('Received setVfCreditStatus with data: ' . $data);

        list($param1, $param2, $param3, $param4) = explode(';', $data . ";");
        //added extra semicolon due to message inconsistency as sometimes the last param is not sent

        $orderNumber = $param1;
        $packageNumber = $this->_getPackageNr($param2);
        $vfIdentifyingEntity = $param3;
        $vfId = $param4;
        $so = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('order_number', $orderNumber)->getLastItem();
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }

        $write = Mage::getSingleton('core/resource')->getConnection('core_read');

        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $so->getId())
            ->addFieldToFilter('package_id', $packageNumber)->getFirstItem();

        if (!empty($vfId) && $vfId != 'VOD-00-000-000') {
            $query = "UPDATE catalog_package SET updated_at='" . now() . "', vf_status_code = '" . $vfId . "', creditcheck_status = 'VALIDATION_FAILED', vf_status_desc = '" . $vfIdentifyingEntity . "'";
            if ($package->getCurrentNumber()) {
                $query .= ", porting_status = 'VALIDATION_FAILED'";
            }
            $query .= " WHERE order_id = '" . $so->getId() . "' AND package_id = '" . $packageNumber . "'";
            $rows = $write->exec($query);

            $write->exec("UPDATE superorder SET error_detail = '" . addslashes($vfIdentifyingEntity) . "' WHERE order_number = '" . $so->getOrderNumber() . "'");
            $this->generateQuoteOnFail($so);
        } else {
            $rows = $write->exec("UPDATE catalog_package SET updated_at='" . now() . "', vf_status_code = '" . $vfId . "', vf_status_desc = '" . $vfIdentifyingEntity . "' WHERE order_id = '" . $so->getId() . "' AND package_id = '" . $packageNumber . "'");
        }

        if ($rows <= 0) {
            $message = 'Error: Invalid order ID or package ID, or status was already set to this value';
        } else {
            Mage::getModel('superorder/statusHistory')
                ->setType(Omnius_Superorder_Model_StatusHistory::PACKAGE_VF_STATUS_CODE)
                ->setPackageId($package->getId())
                ->setStatus($vfId)
                ->setCreatedAt(now())
                ->save();

            $message = 'Success';
        }

        return $message;
    }

    /**
     * @param $data
     * @return string
     */
    public function setNetworkStatus($data)
    {
        $this->_logMsg('Received setNetworkStatus with data: ' . $data);

        if (substr_count($data, ';') === 3) {
            list($param1, $param2, $param3, $param4) = explode(';', $data . ";");
            //added extra semicolon due to message inconsistency as sometimes the last param is not sent
        } else {
            list($param1, $param2, $param3) = explode(';', $data . ";");
            //added extra semicolon due to message inconsistency as sometimes the last param is not sent
            $param4 = '';
        }

        $orderNumber = $param1;
        $packageNumber = $param2;
        $status = '';
        $ccStatus = '';
        $packageDescription = '';

        $this->getNetworkCCStatus($param3, $param4, $status, $ccStatus, $packageDescription);
        $npStatus = $this->getNetworkNPStatus($param3, $param4);

        if ($orderNumber) {
            $so = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('order_number', $orderNumber)->getLastItem();
            if (!$so || !$so->getId()) {
                return 'Error: Order does not exists';
            }

            $package = Mage::getModel('package/package')->getPackages($so->getId(), null, $packageNumber)->getFirstItem();
            try {
                if ($ccStatus) {
                    $this->updateNetworkCC($packageDescription, $status, $ccStatus, $so, $packageNumber, $package);
                }
                if ($npStatus) {
                    $this->updateNetworkNP($npStatus, $so, $packageNumber, $package);
                }
            } catch (Exception $e) {
                return $e->getMessage();
            }

            return 'Success';
        }
    }

    /**
     * @param $data
     * @return string
     */
    public function setOrderMsdisn($data)
    {
        $this->_logMsg('Received setOrderMsdisn with data: ' . $data);

        list($orderNr, $param1, , $param3) = explode(';', $data);

        $packageNr = $param1;
        $msisdn = $param3;

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');

        // Get packageID, match to super order, match to order, match to order_item and set MSISDN
        $supOID = $read
            ->fetchOne("select entity_id from superorder where order_number=:orderNr",
                array(':orderNr' => $orderNr));

        $packageId = $read
            ->fetchOne("SELECT entity_id FROM catalog_package WHERE package_id = :packageId and order_id = :superOrder",
                array(
                    ':packageId' => $packageNr,
                    ':superOrder' => $supOID));
        $daUpdaterows = $write->exec("UPDATE catalog_package SET updated_at='" . now() . "', tel_number = '" . $msisdn . "' WHERE entity_id = '" . $packageId . "'");

        // Get packageID, match to super order, match to order to get Customer ID
        $customerId = $read
            ->fetchOne("SELECT DISTINCT(sales_flat_order.customer_id)
             FROM catalog_package, sales_flat_order, sales_flat_order_item
             WHERE catalog_package.entity_id = :packageId
             AND catalog_package.order_id = sales_flat_order.superorder_id
             AND catalog_package.package_id = sales_flat_order_item.package_id
             AND sales_flat_order_item.order_id = sales_flat_order.entity_id",
                array(
                    ':packageId' => $packageId
                ));

        // Insert CTN entry to search for this customer without waiting for dump
        if ($customerId && $msisdn) {
            try {
                // TODO check if not already existing
                $write->exec("INSERT INTO customer_ctn (customer_id, ctn) VALUES (:customerId', :msisdn)  
                ON DUPLICATE KEY UPDATE ctn=VALUES(ctn)",
                    array(
                        ':customerId' => $customerId,
                        ':msisdn' => $msisdn
                    ));
            } catch (Exception $ex) {
                // do nothing
            }
        }

        if ($daUpdaterows <= 0) {
            return 'Error: Invalid or unknown dealer adapter number';
        }

        return 'Success: Changed MSISDN for order';
    }

    public function setPackageStatus($data)
    {
        $this->_logMsg('Received setPackageStatus with data ' . $data);

        list($param1, $param2, $param3) = explode(';', $data);

        $ccStatus = null;
        $orderNumber = $param1;
        $packageNumber = $this->_getPackageNr($param2);
        $status = $param3;

        switch ($param3) {
            case 'Package_Validated':
                $status = 'Package Validated';
                $ccStatus = 'APPROVED';
                break;
            case 'Delivered':
                $status = Dyna_Package_Model_Package::ESB_PACKAGE_STATUS_DELIVERED;
                $ccStatus = 'APPROVED';
                break;
            default:
                // do nothing
                break;
        }

        $message = null;
        if (empty($orderNumber)) {
            $message = 'Error: Empty order number';
        } elseif (empty($packageNumber)) {
            $message = 'Error: Empty package number';
        } elseif (empty($status)) {
            $message = 'Error: Empty status';
        }

        if ($message) {
            return $message;
        }

        $so = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('order_number', $orderNumber)->getLastItem();
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }

        $write = Mage::getSingleton('core/resource')->getConnection('core_read');
        $ccDate = $this->getCcDate();
        if ($ccStatus !== null) {
            $rows = $write->exec("UPDATE catalog_package SET updated_at='" . now() . "', status = '" . $status . "', creditcheck_status = '" . $ccStatus . "', creditcheck_status_updated='" . $ccDate . "' WHERE order_id = '" . $so->getId() . "' AND package_id = '" . $packageNumber . "'");
        } else {
            $rows = $write->exec("UPDATE catalog_package SET updated_at='" . now() . "', status = '" . $status . "' WHERE order_id = '" . $so->getId() . "' AND package_id = '" . $packageNumber . "'");
        }

        if ($rows <= 0) {
            $message = 'Error: Invalid order ID or package ID, or status was already set to this value';
        } else {
            $packages = Mage::getModel('package/package')->getOrderPackages($so->getId());
            foreach ($packages->getItems() as $package) {
                if ($package->getPackageId() == $packageNumber) {
                    if ($ccStatus !== null) {
                        Mage::getModel('superorder/statusHistory')
                            ->setType(Omnius_Superorder_Model_StatusHistory::PACKAGE_CC_STATUS)
                            ->setPackageId($package->getId())
                            ->setStatus($ccStatus)
                            ->setCreatedAt(now())
                            ->save();
                    }

                    Mage::getModel('superorder/statusHistory')
                        ->setType(Omnius_Superorder_Model_StatusHistory::PACKAGE_STATUS)
                        ->setPackageId($package->getId())
                        ->setStatus($status)
                        ->setCreatedAt(now())
                        ->save();
                }
            }

            $message = 'Success: Changed package status';
        }

        return $message;
    }

    /**
     * @return mixed
     */
    private function getCcDate()
    {
        return now();
    }

    public function sendGarantMail($superOrder, $packageId)
    {
        list($packagesWithGarant, $garantProductOrders) = $superOrder->getGarantProductOrders();
        // Skip packages without garant
        $found = false;
        foreach ($packagesWithGarant as $packageIds) {
            if (in_array($packageId, $packageIds)) {
                $found = true;
            }
            if ($found) {
                break;
            }
        }
        if (!$found) {
            return;
        }
        if (count($garantProductOrders)) {
            $payload = Mage::helper('email')->getGarantPolisPayload(
                $superOrder,
                $this->getGarantAttachmentPaths($packagesWithGarant, $garantProductOrders, $superOrder)
            );
            Mage::getSingleton('email/pool')->add(
                $payload,
                Mage::app()->getWebsite($superOrder->getWebsiteId())->getCode()
            );
        }
    }

    public function getGarantAttachmentPaths($packagesWithGarant, $garantProductOrders, $superOrder)
    {
        $attachmentPaths = [];
        $orderCollection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('entity_id', array('in' => $garantProductOrders));
        foreach ($orderCollection as $order) {
            $packageItems = array();
            foreach ($order->getAllItems() as $item) {
                if (isset($packagesWithGarant[$order->getId()]) && in_array($item->getPackageId(), $packagesWithGarant[$order->getId()])) {
                    $packages[$item->getPackageId()] = isset($packages[$item->getPackageId()])
                        ? $packages[$item->getPackageId()]
                        : Mage::getModel('package/package')->getOrderPackages($superOrder->getId(), $item->getPackageId());
                    $package = $packages[$item->getPackageId()];
                    if (!$package->getImei()) {
                        //if IMEI has not yet been filled in, wait for the ESB to update IMEI for the rest of the packages
                        return;
                    }
                    $packageItems[$item->getPackageId()] = isset($packageItems[$item->getPackageId()]) ? $packageItems[$item->getPackageId()] : array();
                    $packageItems[$item->getPackageId()][] = $item;
                }
            }
            $attachmentPaths = array_merge($attachmentPaths, Mage::helper('omnius_checkout/pdf')->saveWarranty($order, $packageItems));
        }

        return $attachmentPaths;
    }

    /**
     * @param $superOrder
     */
    protected function generateQuoteOnFail($superOrder)
    {
        $initialQuote = $superOrder->getOriginalQuote();
        $savedCarts = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('quote_parent_id', $initialQuote->getId())
            ->addFieldToFilter('cart_status', Dyna_Checkout_Model_Sales_Quote::CART_STATUS_SAVED);

        // If the quote is not already saved as a cart, we save it now
        if (count($savedCarts) == 0) {
            $initialQuote->setSaveAsCart(true);
            Mage::helper('omnius_checkout')->createNewCartQuote($initialQuote, $superOrder->getId());
        }
    }

    public function setHomeDeliveryStatus($data)
    {
        $this->_logMsg('Received setHomeDeliveryStatus with data: ' . $data);
        list(, $param1, $param2, $param3) = explode(';', $data . ";");
        //added extra semicolon due to message inconsistency as sometimes the last param is not sent

        $orderNumber = trim($param1);
        $status = trim($param2);
        $reasonCode = trim($param3);

        if (empty($orderNumber)) {
            return 'Error: Empty order number';
        }
        if (empty($status)) {
            return 'Error: Empty status';
        }

        $statusCode = $status == 'CANCEL' ? 'cancelled' : str_replace(" ", "_", strtolower($status));

        try {
            /** @var $order Dyna_Checkout_Model_Sales_Order */
            $order = Mage::getModel('sales/order')->load($orderNumber, 'increment_id');
            if ($order && $order->getId()) {
                $this->_logMsg('Setting home delivery status for ' . $orderNumber . ' to: ' . $statusCode . ', reason code: ' . $reasonCode);
                $order->setEcomStatus($statusCode)
                    ->setReasonCode($reasonCode)
                    ->save();
            } else {
                $this->_logMsg('Unable to set home delivery status for ' . $orderNumber . ' to: ' . $statusCode . ', reason code: ' . $reasonCode . '. Order not found');
            }
        } catch (Exception $ex) {
            $this->_logMsg('Unable to set home delivery status for ' . $orderNumber . ' to: ' . $statusCode . ', reason code: ' . $reasonCode . '. Order not found');
        }

        return 'OK';
    }

    /**
     * Used for setting the approve_order status of the package in case the approve package call is performed from within the process order call.
     * @param $data
     * @return string
     */
    public function setOrderPackageApproval($data)
    {
        $this->_logMsg('Received setOrderPackageApproval with data: ' . $data);
        if (substr_count($data, ';') === 3) {
            list($param1, $param2, $param3, $param4) = explode(';', $data);
        } else {
            list($param1, $param2, $param3) = explode(';', $data);
            $param4 = '';
        }

        $message = null;
        if (empty($param1)) {
            $message = 'Error: Empty order number';
        } elseif (empty($param2)) {
            $message = 'Error: Empty package id';
        } elseif (empty($param3)) {
            $message = 'Error: Empty status';
        }

        if ($message) {
            return $message;
        }

        try {
            $superorder = Mage::getModel('superorder/superorder')->load($param1, 'order_number');
            $package = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('order_id', $superorder->getId())
                ->addFieldToFilter('package_id', $param2)
                ->getFirstItem();

            if ($param3 == 'true') {
                $package->setApproveOrder(1)
                    ->setApproveOrderJobId(null)
                    ->setUpdatedAt(now())
                    ->save();
            } elseif ($param3 == 'false') {
                $superorder->setErrorDetail($param4)
                    ->setUpdatedAt(now())
                    ->save();
            }
        } catch (Exception $e) {
            $this->_logMsg('Unable to set approve order package status for ' . $param1 . '.' . $param2 . ', error: ' . $e->getMessage());
        }

        return 'OK';
    }

    public function setActualPortingDate($data)
    {
        $this->_logMsg('Received setActualPortingDate with data: ' . $data);
        list($orderNumber, $packageId, $portingDate) = explode(';', $data . ";");

        $actualPortingDate = date("Y-m-d H:i:s", strtotime($portingDate));
        $so = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('order_number', $orderNumber)->getLastItem();
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }

        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_write');
        $query = "update catalog_package set actual_porting_date='{$actualPortingDate}' where order_id='{$so->getId()}' and package_id='{$packageId}' order by entity_id desc limit 1";
        $rows = $connection->exec($query);

        if ($rows <= 1) {
            return "Error";
        }

        return "Success";
    }

    public function setManualActivation($data)
    {
        $this->_logMsg('Received setManualActivation with data: ' . $data);

        list($param1, $param2) = explode(';', $data);

        $orderNumber = $param1;
        $packageNumber = $param2;
        $so = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('order_number', $orderNumber)->getLastItem();
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }
        $write = Mage::getSingleton('core/resource')->getConnection('core_read');
        $rows = $write->exec("UPDATE catalog_package SET updated_at='" . now() . "', esb_manual_activation = '1' WHERE order_id = '" . $so->getId() . "' AND package_id = '" . $packageNumber . "'");
        if ($rows <= 0) {
            return 'Error: Invalid order ID or package ID, or package_esb_number was already set to this value';
        }

        return 'Success';
    }

    /**
     * @param $param3
     * @param $param4
     * @return null|string
     */
    protected function getNetworkNPStatus($param3, $param4)
    {
        $npStatus = '';
        if ($param3 == 'NumberPorting') {
            switch ($param4) {
                case 'Pending':
                    $npStatus = 'PENDING';
                    break;
                case 'Approved':
                    $npStatus = 'APPROVED';
                    break;
                case 'Rejected':
                case 'ServiceNotAvailable':
                    $npStatus = 'REJECTED';
                    break;
                case 'Referred':
                    $npStatus = 'REFERRED';
                    break;
                case 'Final Rejected':
                    $npStatus = "REJECTED & CANCELLED";
                    break;
                default:
                    $npStatus = null;
                    break;
            }

            return $npStatus;
        }

        return $npStatus;
    }

    protected function updateNetworkCC($packageDescription, $status, $ccStatus, $so, $packageNumber, $package)
    {
        $ccDate = $this->getCcDate();
        $write = Mage::getSingleton('core/resource')->getConnection('core_read');
        if ($packageDescription) {
            $string = sprintf("UPDATE catalog_package SET updated_at='%s', vf_status_desc = '%s', status = '%s', creditcheck_status = '%s', creditcheck_status_updated='%s' WHERE order_id = '%s' AND package_id = '%s'", now(), $packageDescription, $status, $ccStatus, $ccDate, $so->getId(),
                $packageNumber);
        } else {
            $string = sprintf("UPDATE catalog_package SET updated_at='%s', status = '%s', creditcheck_status = '%s', creditcheck_status_updated='%s' WHERE order_id = '%s' AND package_id = '%s'", now(), $status, $ccStatus, $ccDate, $so->getId(), $packageNumber);
        }

        $rows = $write->exec($string);

        if (!isset($rows) || $rows <= 0) {
            throw new Exception('Error: Invalid order ID or package ID, or status was already set to this value');
        }

        Mage::getModel('superorder/statusHistory')
            ->setType(Omnius_Superorder_Model_StatusHistory::PACKAGE_CC_STATUS)
            ->setPackageId($package->getId())
            ->setStatus($ccStatus)
            ->setCreatedAt(now())
            ->save();

        Mage::getModel('superorder/statusHistory')
            ->setType(Omnius_Superorder_Model_StatusHistory::PACKAGE_STATUS)
            ->setPackageId($package->getId())
            ->setStatus($status)
            ->setCreatedAt(now())
            ->save();
    }

    protected function updateNetworkNP($npStatus, $so, $packageNumber, $package)
    {
        $write = Mage::getSingleton('core/resource')->getConnection('core_read');
        $rows = $write->exec("UPDATE catalog_package SET updated_at='" . now() . "', porting_status = '" . $npStatus . "', porting_status_updated=NOW() WHERE order_id = '" . $so->getId() . "' AND package_id = '" . $packageNumber . "'");

        if (!isset($rows) || $rows <= 0) {
            throw new Exception('Error: Invalid order ID or package ID, or status was already set to this value');
        }

        Mage::getModel('superorder/statusHistory')
            ->setType(Omnius_Superorder_Model_StatusHistory::PACKAGE_PORTING_STATUS)
            ->setPackageId($package->getId())
            ->setStatus($npStatus)
            ->setCreatedAt(now())
            ->save();
    }

    /**
     * @param $param3
     * @param $param4
     * @param $status
     * @param $ccStatus
     * @param $packageDescription
     */
    protected function getNetworkCCStatus($param3, $param4, &$status, &$ccStatus, &$packageDescription)
    {
        if ($param3 == 'CreditCheck' || $param3 == 'ValidateOrder') {
            switch ($param4) {
                case 'Approved':
                case 'Succeeded':
                    // Temp
                    $status = 'Package Validated';
                    $ccStatus = 'APPROVED';
                    break;
                case 'Rejected':
                    $status = 'Cancelled';
                    $ccStatus = 'REJECTED';
                    break;
                case 'AdditionalInfoRequired':
                    $status = 'Additional info required';
                    $ccStatus = Dyna_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED;
                    break;
                case 'Referred':
                    $status = 'Referred';
                    $ccStatus = 'REFERRED';
                    break;
                case 'Pending':
                    $status = 'Pending';
                    $ccStatus = Dyna_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING;
                    break;
                default:
                    $status = Dyna_Package_Model_Package::ESB_PACKAGE_STATUS_FAILED;
                    $ccStatus = Dyna_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED;
                    $packageDescription = $param4;
            }
        }
    }
}
