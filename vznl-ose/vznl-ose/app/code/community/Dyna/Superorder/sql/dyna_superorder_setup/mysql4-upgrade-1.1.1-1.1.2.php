<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$connection = $this->getConnection();

$transactionIdAttributes = [
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => 100,
    'comment'   => "The id of the transaction that was used to create the current package",
];
if (!$connection->tableColumnExists($this->getTable('superorder'), 'transaction_id')) {
    $connection->addColumn($this->getTable('superorder'), 'transaction_id', $transactionIdAttributes);
}
$installer->endSetup();