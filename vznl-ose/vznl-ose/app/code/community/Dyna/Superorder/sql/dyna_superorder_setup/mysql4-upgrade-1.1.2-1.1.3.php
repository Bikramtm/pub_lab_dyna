<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$connection = $this->getConnection();

$orderStatusTranslateAttributes = [
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => 255,
    'comment'   => "The translated order status",
];
if (!$connection->tableColumnExists($this->getTable('superorder'), 'order_status_translate')) {
    $connection->addColumn($this->getTable('superorder'), 'order_status_translate', $orderStatusTranslateAttributes);
}
$installer->endSetup();