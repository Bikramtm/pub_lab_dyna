<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_DynaSecurity_Model_Admin extends Mage_Admin_Model_User
{
    /**
     * @param string $username
     * @param string $password
     * @return bool
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function authenticate($username, $password)
    {
        $config = Mage::getStoreConfigFlag('admin/security/use_case_sensitive_login');
        $result = false;

        try {
            Mage::dispatchEvent('admin_user_authenticate_before', array(
                'username' => $username,
                'user' => $this,
                'section' => 'admin',
                'login' => $username,
            ));
            $this->loadByUsername($username);
            $sensitive = ($config) ? $username == $this->getUsername() : true;

            if ($sensitive && $this->getId() && Mage::helper('core')->validateHash($password, $this->getPassword())) {

                if ($this->getIsActive() != '1') {
                    Mage::throwException(Mage::helper('adminhtml')->__('This account is inactive.'));
                }
                if (!$this->hasAssigned2Role($this->getId())) {
                    Mage::throwException(Mage::helper('adminhtml')->__('Access denied.'));
                }
                $result = true;
            }

            if (!$result) {
                Mage::dispatchEvent('authenticate_failed', array(
                    'model' => $this,
                    'login' => $username,
                    'section' => 'admin'
                ));
            } else {
                Mage::dispatchEvent('authenticate_success', array(
                    'login' => $username,
                    'section' => 'admin'
                ));
            }

            Mage::dispatchEvent('admin_user_authenticate_after', array(
                'username' => $username,
                'password' => $password,
                'user' => $this,
                'result' => $result,
            ));
        } catch (Mage_Core_Exception $e) {
            $this->unsetData();
            throw $e;
        }

        if (!$result) {
            $this->unsetData();
        }

        return $result;
    }

    /**
     * Validate admin password strength if Dyna_PasswordAdminStrentgth is enabled
     * based on System -> Configuration -> Dyna Security -> Admin Password Strength settings
     * For existing customer password + confirmation will be validated only when password is set (i.e. its change is requested)
     *
     * @return bool
     */
    public function validate()
    {
        if (Mage::helper('core')->isModuleEnabled('Dyna_PasswordAdminStrength')) {
            $password = $this->getNewPassword();
            /** @var Dyna_PasswordStrength_Helper_Data $validationHelper */
            $validationHelper = Mage::helper('passwordadminstrength');
            $errors = $validationHelper->validatePassword($this, $password);

            if (empty($errors) || $password == '') {
                return parent::validate();
            }

            return $errors;
        } else {
            parent::validate();
        }

    }

    /**
     * Set the password hash method if the Dyna_PasswordHash module is enabled
     */
    protected function _beforeSave()
    {
        if (Mage::helper('core')->isModuleEnabled('Dyna_PasswordHash')) {
            $hashMethod = Mage::getStoreConfig('passwordhash/pbkdf2/hash_algorithm');

            if ($this->getNewPassword()) {
                // Change user password
                $this->setHashMethod($hashMethod);
            } elseif ($this->getPassword() && $this->getPassword() != $this->getOrigData('password')) {
                // New user password
                $this->setHashMethod($hashMethod);
            }
        }

        parent::_beforeSave();
    }
}
