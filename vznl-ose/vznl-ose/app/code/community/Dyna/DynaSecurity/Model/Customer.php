<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_DynaSecurity_Model_Customer extends Mage_Customer_Model_Customer
{
    /**
     * Overwrite the authenticate method from customer module
     *
     * @param string $login
     * @param string $password
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function authenticate($login, $password)
    {
        Mage::dispatchEvent('authenticate_before', array(
            'model' => $this,
            'login' => $login,
            'section' => 'customer'
        ));

        $this->loadByEmail($login);
        if ($this->getConfirmation() && $this->isConfirmationRequired()) {
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('This account is not confirmed.'),
                self::EXCEPTION_EMAIL_NOT_CONFIRMED
            );
        }

        Mage::dispatchEvent('authenticate_before_validate', array(
            'model' => $this
        ));


        if (!$this->validatePassword($password)) {
            Mage::dispatchEvent('authenticate_failed', array(
                'model' => $this,
                'login' => $login,
                'section' => 'customer'
            ));

            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Invalid login or password.'),
                self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD
            );
        }

        Mage::dispatchEvent('authenticate_success', array(
            'login' => $login,
            'section' => 'customer'
        ));

        Mage::dispatchEvent('customer_customer_authenticated', array(
            'model' => $this,
            'password' => $password,
        ));

        return true;
    }

    /**
     * Validate customer attribute values.
     * For existing customer password + confirmation will be validated only when password is set (i.e. its change is requested)
     *
     * @return bool
     */
    public function validate()
    {
        if (Mage::helper('core')->isModuleEnabled('Dyna_PasswordStrength')) {
            $password = $this->getPassword();
            /** @var Dyna_PasswordStrength_Helper_Data $validationHelper */
            $validationHelper = Mage::helper('passwordstrength');
            $errors = $validationHelper->validatePassword($this, $password);

            if (empty($errors) || $password == '') {
                return parent::validate();
            }

            return $errors;
        } else {
            parent::validate();
        }

    }

    /**
     * Set plain and hashed password
     *
     * @param string $password
     * @return Mage_Customer_Model_Customer
     */
    public function setPassword($password)
    {
        $this->setData('password', $password);
        $this->setPasswordHash($this->hashPassword($password));
        $this->setPasswordConfirmation(null);

        Mage::dispatchEvent('customer_after_set_password', array(
            'model' => $this
        ));

        return $this;
    }
}
