<?php

$installer = $this;
/* @var $installer Dyna_Mobile_Model_Resource_Setup */

$installer->startSetup();

$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);

/** @var Mage_Eav_Model_Attribute $attribute */
$attributeModel = Mage::getModel('eav/entity_attribute');

$attributeId = $installer->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'start_date', 'attribute_id');
$attribute = $attributeModel->load($attributeId);
$attributeModel->setData('input_format', $dateFormatIso);
$attributeModel->setData('format', $dateFormatIso);
$attributeModel->setData('time', true);
$attributeModel->setData('frontend_input', 'datetime');
$attributeModel->save();

/** @var Mage_Eav_Model_Attribute $attribute */
$attributeModel = Mage::getModel('eav/entity_attribute');

$attributeId = $installer->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'end_date', 'attribute_id');
$attribute = $attributeModel->load($attributeId);
$attributeModel->setData('input_format', $dateFormatIso);
$attributeModel->setData('format', $dateFormatIso);
$attributeModel->setData('time', true);
$attributeModel->setData('frontend_input', 'datetime');
$attributeModel->save();

$installer->endSetup();