<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

// add attributes to corresponding attribute set
$attributesToSets = [
    'Mobile_Device' => [
        'product_family_id',
        'product_version_id',
        'lifecycle_status',
        'vat',
        'hierarchy_value',
        'contract_value',
    ],
    'Mobile_Footnote' => [
        'product_family_id',
        'product_version_id',
        'lifecycle_status',
        'vat',
        'hierarchy_value',
        'contract_value',
    ],
    'Mobile_Additional_Service' => [
        'sorting',
        'display_name_inventory',
        'display_name_configurator',
        'display_name_cart',
        'display_name_communication',
        'related_slave_sku',
        'ask_confirmation_first',
        'confirmation_text',
        'preselect',
        'use_service_value',
        'service_name',
        'service_xpath',
    ],
    'Mobile_Data_Tariff' => [
        'hierarchy_value',
        'sorting',
        'ident_needed',
        'display_name_inventory',
        'contract_value',
        'product_family_id',
        'product_version_id',
        'lifecycle_status',
        'dtc_restrict_ind',
        'ask_confirmation_first',
        'confirmation_text',
        'preselect',
        'use_service_value',
        'service_name',
        'service_xpath',
    ],
    'Mobile_Prepaid_Tariff' => [
        'hierarchy_value',
        'sorting',
        'ident_needed',
        'display_name_inventory',
        'contract_value',
        'product_family_id',
        'product_version_id',
        'lifecycle_status',
        'dtc_restrict_ind',
        'ask_confirmation_first',
        'confirmation_text',
        'preselect',
        'use_service_value',
        'service_name',
        'service_xpath',
    ],
    'Mobile_Voice_Data_Tariff' => [
        'hierarchy_value',
        'sorting',
        'ident_needed',
        'display_name_inventory',
        'contract_value',
        'product_family_id',
        'product_version_id',
        'lifecycle_status',
        'dtc_restrict_ind',
        'ask_confirmation_first',
        'confirmation_text',
        'preselect',
        'use_service_value',
        'service_name',
        'service_xpath',
    ]
];
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
foreach ($attributesToSets as $attributeSetCode => $attributes) {
    foreach ($attributes as $attributeCode) {
        $attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
        $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Mobile", $attributeCode);
    }
}
$installer->endSetup();
