<?php
/**
 * Update attribute name from general_descriptions to tooltip
 * Update the label to "Tooltip"
 */

$installer = $this;
/* @var $installer Dyna_Mobile_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$installer->updateAttribute(
    $entityTypeId,
    'general_descriptions',
    array('attribute_code' => 'tooltip',
        'frontend_label' => 'Tooltip'));

$installer->endSetup();
