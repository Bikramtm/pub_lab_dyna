<?php

/**
 * Cable product Hints attributes
 */

$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$attributeSetName = 'Mobile_Additional_Service';

/** Portal */
$attributes = [
    'extra_handle_indicator' => [
        'attribute_set' => $attributeSetName,
        'group' => 'General',
        'label' => 'Extra handle indicator',
        'input' => 'select',
        'type' => 'int',
        'backend' => 'eav/entity_attribute_backend_array',
        'option' => [
            'values' => Dyna_Cable_Model_Product_Attribute_Option::$hisOptions,
        ],
        'note' => 'Extra handle indicator',
    ]
];

$createAttributeSets = [];
foreach ($attributes as $code => $options) {
    $attributeSet = $options['attribute_set'];
    $group = $options['group'];
    unset($options['group'], $options['attribute_set']);

    $attributeId = $installer->getAttributeId($entityTypeId, $code);
    if (! $attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
    }

    $createAttributeSets[$attributeSet]['groups'][$group][] = $code;
}

$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($attributes, $createAttributeSets);

$installer->endSetup();
