<?php

class Dyna_Audit_Model_Logger_Log extends Omnius_Audit_Model_Logger_AbstractLogger
{
    public function log(Omnius_Audit_Model_Event $event)
    {

        $this->_getDumper($event->getDumper())
              ->dump($event);
    }

    protected function _getDumper($dumper)
    {
        if ( ! isset($this->_dumpers[$dumper])) {
            if ( ! ($_dumper = Mage::getModel(sprintf('dyna_audit/dumper_log', $dumper)))) {
                throw new Exception('Invalid dumper configuration.');
            }
            return $this->_dumpers[$dumper] = $_dumper;
        }

        return $this->_dumpers[$dumper];
    }

}