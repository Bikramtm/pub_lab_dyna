<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Audit_Model_Observer
 */
class Dyna_Audit_Model_Observer extends Omnius_Audit_Model_Observer
{
    /**
     * Get processor object
     *
     * @return Dyna_Audit_Model_Processor
     */
    protected function _getProcessor()
    {
        if ( ! $this->_processor) {
            return $this->_processor = Mage::getSingleton('dyna_audit/processor');
        }
        return $this->_processor;
    }
} 
