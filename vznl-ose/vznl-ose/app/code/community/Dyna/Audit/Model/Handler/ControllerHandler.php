<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Audit_Model_Handler_ControllerHandler
 */
class Dyna_Audit_Model_Handler_ControllerHandler extends Omnius_Audit_Model_Handler_ControllerHandler
{
    use Dyna_Audit_Model_Handler_TraitHandler;

    /**
     * @param Varien_Object $payload
     * @param string $actionType
     * @return mixed
     */
    public function handle(Varien_Object $payload, $actionType = null)
    {
        /** @var Mage_Core_Controller_Varien_Action $controller */
        $controller = $payload->getData('controller_action');
        $config = $this->getConfig();

        if ((int) $config->getValue('active')
            && ( ($config->isWhitelistMode() && in_array($controller->getFullActionName(), $config->getControllersList()))
                || ($config->isBlacklistMode() && !in_array($controller->getFullActionName(), $config->getControllersList()))
            )
        ) {
            /** @var Omnius_Audit_Model_Event $event */
            $event = Mage::getModel('audit/event');
            /** @var Dyna_Agent_Model_Agent $agent */
            $agent = $this->getAgent();
            /** @var Mage_Admin_Model_Session $admin */
            $admin = Mage::getSingleton('admin/session', ['name' => 'adminhtml'])->getUser();

            $event->setAdminId($admin ? $admin->getId() : 'null');
            $event->setAdminUsername($admin ? $admin->getUsername() : 'null');

            $event->setAgent($agent);
            $event->setAgentId($agent ? $agent->getId() : 'null');
            $event->setDealerId($agent ? $agent->getDealer()->getId() : 'null');
            $event->setDealerCode($agent ? $agent->getDealer()->getVfDealerCode() : 'null');
            $event->setAxiStoreCode($agent ? $agent->getDealer()->getAxiStoreCode() : 'null');

            $event->setAction($controller->getFullActionName());
            $event->setActionType($actionType);

            // check if we should log the execution time
            $boolLogTime = (in_array($controller->getFullActionName(), $config->getLogTimeActions()));

            if ($actionType == 'predispath') {
                $this->preDispatchActions($boolLogTime);
            }
            $params = $actionType == 'predispath' ? Mage::helper('audit')->censorData($controller->getRequest()->getParams()) : [];

            //for certain actions, directly add the response in the postdispatch as it is more useful
            if ($actionType == 'postdispath' && in_array($event->getAction(), $this->addResponse)) {
                $params['body'] = $controller->getResponse()->getBody();
            }

            $reqUniqueId = Mage::registry('req_unique_id');
            $params['req_unique_id'] = $reqUniqueId;

            if ($actionType == 'postdispath') {
                $params = $this->postDispatchActions($boolLogTime, $reqUniqueId, $params);
            }

            //for all requests, also log the unique request transaction id and correlation to match OIL requests
            if ($actionType == 'predispath') {
                $params['req_transaction_id'] = Mage::helper('dyna_core')::getRequestUniqueTransactionId();
                $params['CorrelationID'] = Mage::getSingleton("core/session")->getEncryptedSessionId();
            }
            $event->setParams($params ? json_encode($params) : 'null');
            $event->setIsController(true);
            $event->setDumper(lcfirst($config->getValue('dumper')));
            $event->setCorrelationId(Mage::getSingleton("core/session")->getEncryptedSessionId());

            /** @var Dyna_Audit_Model_Logger_Log $logger */
            $logger = Mage::getModel('dyna_audit/logger_log');
            $logger->log($event);
        }
    }
}
