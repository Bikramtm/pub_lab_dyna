<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Audit_Model_Processor
 */
class Dyna_Audit_Model_Processor extends Omnius_Audit_Model_Processor
{
    /** @var Dyna_Agent_Model_Agent */
    protected $_agent;

    /**
     * @return Omnius_Audit_Model_Handler_ControllerHandler
     */
    public function getControllerHandler()
    {
        if (!$this->_controllerHandler) {
            /** @var Omnius_Audit_Model_Handler_ControllerHandler $_controllerHandler */
            $_controllerHandler = Mage::getSingleton('dyna_audit/handler_controllerHandler');
            $_controllerHandler->setAgent($this->getAgent());
            $_controllerHandler->setConfig($this->getConfig());

            return $this->_controllerHandler = $_controllerHandler;
        }
        return $this->_controllerHandler;
    }

    /**
     * @return Dyna_Agent_Model_Agent
     */
    public function getAgent()
    {
        if (!$this->_agent) {
            return $this->_agent = Mage::getSingleton('customer/session', array('name' => 'frontend'))->getAgent(true);
        }
        return $this->_agent;
    }
}
