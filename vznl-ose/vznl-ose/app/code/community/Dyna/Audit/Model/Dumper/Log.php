<?php
class Dyna_Audit_Model_Dumper_Log extends Omnius_Audit_Model_Dumper_Log
{
    /** @var array */
    protected $_dumpers = array();

    public function dump(Omnius_Audit_Model_Event $event)
    {
        $section = (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml') ? 'backend' : 'frontend';

        // Do not log GET requests to the admin login page
        if ($section == 'backend' && $event->getAction() == 'adminhtml_index_index' && ! Mage::app()->getRequest()->isPost()) {
            return;
        }

        $event->setData('section', $section);
        /** @var Dyna_Audit_Model_Dumper_File $dumperFile */
        $dumperFile = Mage::getModel('Dyna_Audit_Model_Dumper_File');
        $data = trim($dumperFile->extractData($event), PHP_EOL);

        $filename = $this->createFilename(sprintf('audit_%s_%s', $section, $event->getIsController() ? 'controller' : 'entity'));
        $this->log($data, Zend_Log::ERR, $filename);
    }
}