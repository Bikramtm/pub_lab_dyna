<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once Mage::getModuleDir('controllers', 'Omnius_Configurator') . DS . 'InitController.php';

/**
 * Class Dyna_Configurator_InitController
 */
class Dyna_Configurator_InitController extends Omnius_Configurator_InitController
{
    protected $coreHelper = null;

    public function preDispatch()
    {
    	$value = Mage::getStoreConfig('agent/general_config/allowed_urls');
    	
    	if ($value && stripos($value,'getProducts') !== false) {
    		$this->setFlag('getProducts', self::FLAG_NO_START_SESSION, true);
    	} 
    	
    	if ($value && stripos($value,'getPrices') !== false) {
    		$this->setFlag('getPrices', self::FLAG_NO_START_SESSION, true);
    	}   	

        return parent::preDispatch();
    }

    /**
     * @IsGranted({"permissions":["CREATE_PACKAGE"]})
     */
    public function mobileAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->getActionData(__FUNCTION__);
    }

    /**
     * @IsGranted({"permissions":["CREATE_PACKAGE"]})
     */
    public function prepaidAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->getActionData(__FUNCTION__);
    }

    /**
     * @IsGranted({"permissions":["CREATE_PACKAGE"]})
     */
    public function hardwareAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->getActionData(__FUNCTION__);
    }

    /**
     * @IsGranted({"permissions":["CREATE_PACKAGE"]})
     */
    public function CABLE_INTERNET_PHONEAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->getActionData(__FUNCTION__);
    }

    /**
     * @IsGranted({"permissions":["CREATE_PACKAGE"]})
     */
    public function CABLE_INDEPENDENTAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->getActionData(__FUNCTION__);
    }

    /**
     * @IsGranted({"permissions":["CREATE_PACKAGE"]})
     */
    public function CABLE_TVAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->getActionData(__FUNCTION__);
    }

    /**
     * @IsGranted({"permissions":["CREATE_PACKAGE"]})
     */
    public function DSLAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->getActionData(__FUNCTION__);
    }

    /**
     * @IsGranted({"permissions":["CREATE_PACKAGE"]})
     */
    public function LTEAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->getActionData(__FUNCTION__);
    }

    /**
     * @IsGranted({"permissions":["CREATE_PACKAGE"]})
     */
    public function RedplusAction()
    {
        $this->getActionData(__FUNCTION__);
    }

    /**
     * Action that parses the shopping cart and renders the extended shopping cart.
     */
    public function expandCartAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $cartPackages = Mage::getSingleton('checkout/cart')->getQuote()->getCartPackages();
        $process_contexts = [];

        if($cartPackages){
            foreach($cartPackages as $cartPackage){
                $cartPackage->updateUseCaseIndication($cartPackage);
                $process_contexts[$cartPackage->getPackageId()] = $cartPackage->getSaleType();
            }
        }
        $html = Mage::getSingleton('core/layout')->createBlock('dyna_configurator/rightsidebar')
            ->setTemplate('customer/right_sidebar.phtml')->toHtml();
        $cartProducts = [];
        foreach (Mage::getModel('checkout/session')->getQuote()->getAllItems() as $item) {
            $packageId = $item->getPackageId();
            $cartProducts[$packageId]['products'][] = $item->getProductId();
            $cartProducts[$packageId]['type'] = $item->getPackageType();
            $cartProducts[$packageId]['process_context'] = $process_contexts[$packageId];
        }
        foreach ($cartProducts as $packageId => $package) {
            sort($package['products']);
            $cartProducts[$packageId] = [
                'products' => $package['products'],
                'type' => $package['type'],
                'process_context' => $package['process_context'],
            ];
        }
        $response = Mage::helper('omnius_core')->jsonResponse(['html' => $html, 'cartProducts' => md5(serialize($cartProducts))]);
        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json');
    }

    /**
     * Returns product information
     */
    public function getProductsAction()
    {
        try {
            $requestParams = array();
            $request = $this->getRequest();

            $requestParams['websiteId'] = $request->get('website_id');
            $requestParams['processContext'] = $request->get('process_context');
            $requestParams['packageCreationTypeId'] = $request->get('packageCreationTypeId');
            $requestParams['customerType'] = $request->get('customer_type');
            $requestParams['showPriceWithBtw'] = $request->get('show_price_with_btw');

            $response = Mage::getModel('dyna_catalog/product')->getCatalogProducts($requestParams);

            $date = new DateTime('+1 year');
            return $this->jsonResponse(
                $response,
                200, [
                'Pragma' => 'cache',
                'Cache-Control' => 'max-age=31536000, must-revalidate',
                'Expires' => $date->format('r'),
                'X-Cache-Override' => true,
            ]);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load products',
            ));
        }
    }

    /**
     * Returns product information
     * @todo check if this method is needed
     */
    public function getProductsModifiedAction()
    {
        try {
            $response = array();
            /** @var Dyna_Configurator_Helper_Data $helper */
            $helper = Mage::helper('dyna_configurator');
            /** @var Dyna_Catalog_Helper_Data $catalogHelper */
            $catalogHelper = Mage::helper('dyna_catalog');
            $request = $this->getRequest();
            $websiteId = $request->get('website_id');
            $processContext = $request->get('process_context');

            $generalFilters = $additionalFilters = $permanentFiltersAttributePairs = [];

            $packageCreationTypeId = $request->get('packageCreationTypeId');
            /** @var Dyna_Package_Model_PackageCreationTypes $packageCreationType */
            $packageCreationType = Mage::getModel('dyna_package/packageCreationTypes')->load($packageCreationTypeId);
            if ($packageCreationType) {
                /** @var Dyna_Package_Model_PackageType $packageType */
                $packageType = Mage::getModel('dyna_package/packageType')->load($packageCreationType->getPackageTypeId());
                $type = strtolower($packageType->getPackageCode());

                $generalFilters = $packageCreationType->getFilterAttributes();

                $tmpFilters = $packageCreationType->getPackageSubtypeFilterAttributes();
                if (!empty($tmpFilters)) {
                    $additionalFilters = Mage::helper('dyna_configurator')->getSubtypeFilterCondition($tmpFilters);
                }

                foreach ($packageType->getPackageSubTypes() as $packageSubType) {
                    if (!$packageSubType->getVisibleConfigurator()) {
                        continue;
                    }

                    $permanentFiltersAttributePairs[$packageSubType->getPackageCode(true)] = $helper->getAttributePairsPermanentFilters($packageSubType->getPackageCode(true), $generalFilters);
                }
            }

            foreach ($additionalFilters as $section => $filter) {

                if (isset($permanentFiltersAttributePairs[strtolower($section)])) {
                    $permanentFiltersAttributePairs[strtolower($section)] = array_merge($permanentFiltersAttributePairs[strtolower($section)], $helper->getAttributePairsPermanentFilters(strtolower($section), $filter));
                } else {
                    $permanentFiltersAttributePairs[strtolower($section)] = $helper->getAttributePairsPermanentFilters(strtolower($section), $filter);
                }
            }

            $permanentFiltersSearchText = $helper->getSearchTxtPermanentFilters($generalFilters);

            Mage::unregister('products_visibility_package_type');
            Mage::register('products_visibility_package_type', $type);

            /** @var Dyna_Configurator_Model_Catalog $configuratorTypeModel */
            $configuratorTypeModel = @Mage::getModel(sprintf('dyna_configurator/%s', $type));
            // fall back to catalog model if package type is not extended
            if (!$configuratorTypeModel) {
                $configuratorTypeModel = Mage::getModel('dyna_configurator/catalog');
            }
            // let configuration model know what package type it configures
            $configuratorTypeModel->setPackageType($type);
            $products = $configuratorTypeModel->getAllConfiguratorItems($websiteId, $permanentFiltersAttributePairs, $processContext);

            $productFilters = $products['filters'];
            $productFilters = isset($productFilters) && is_array($productFilters) && count($productFilters) ? $helper->toArray($productFilters) : [];
            unset($products['filters']);
            $allProducts = count($products) ? $helper->toArray($products) : [];

            // determine if a product should be visible or not
            foreach ($allProducts as $key => &$currentProducts) {
                if ($key != 'consumer_filters') {
                    foreach ($currentProducts as &$currentProduct) {
                        /** @var Dyna_Catalog_Model_Product $product */
                        $product = $products[$key]->getItemById($currentProduct['entity_id']);
                        $currentProduct['product_visibility'] = $catalogHelper->getVisibility(
                            Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CONFIGURATOR,
                            $currentProduct['sku'],
                            $product,
                            $product->getData('product_visibility')
                        );
                    }
                    unset($currentProduct);
                }
            }
            unset($currentProducts);

            $response[sprintf('prod_%s', $type)] = $allProducts;
            $response[sprintf('filter_%s', $type)] = $productFilters;

            $processedFamilies = $configuratorTypeModel->processFamilies($type, $allProducts);
            foreach ($processedFamilies['families']['items'] as &$family) {
                // If no family position set, moving it to the end of the list
                $family['family_position'] = isset($family['family_position']) ? (int)$family['family_position'] : 9999;
            }
            $response[sprintf('fam_%s', $type)] = $processedFamilies['families'];

            foreach ($permanentFiltersAttributePairs as $section => $filters) {
                $sectionFilters = array_map(
                    function ($filter) {
                        return $filter['string_filter'];
                    }, $filters);

                if($sectionFilters) {
                    $response[sprintf('permanent_filters_%s', $type)][mb_strtolower($section)] = [
                        'all_permanent_filters' => $sectionFilters,
                        'permanent_search_filters' => $permanentFiltersSearchText
                    ];
                }
            }

            if (isset($products['consumer_filters'])) {
                $consumerProductFilters = $products['consumer_filters'];
                $consumerProductFilters = isset($consumerProductFilters) && is_array($consumerProductFilters) && count($consumerProductFilters) ? $helper->toArray($consumerProductFilters) : new stdClass();
                unset($products['consumer_filters']);
                $response[sprintf('filter_consumer_%s', $type)] = $consumerProductFilters;
            }

            return $this->jsonResponse($response);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load products',
            ));
        }
    }

    /**
     * Get the updated products prices based on current selection
     */
    public function getPricesAction()
    {
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        $websiteId = $this->getRequest()->getParam('website_id');
        $isRetention = $this->getRequest()->getParam('type');
        $packageType = $this->getRequest()->getParam('packageType');
        $salesId = $this->getRequest()->getParam('sales_id');
        $productIds = array();
        $cacheKey = 'configurator_get_prices_action_' . $websiteId . '_' . $isRetention . '_' . $packageType . "_" . $salesId;
        if ($products = $this->getRequest()->getParam('products', false)) {
            $productIds = array_filter(explode(',', $products), function ($id) {
                return (int)$id;
            });
            sort($productIds);
            $cacheKey .= join('_', $productIds);
        }

        if ($cachedPrices = $cache->load($cacheKey)) {
            $prices = unserialize($cachedPrices);
        } else {
            $prices = Mage::helper('dyna_checkout')->setSalesId($salesId)->getUpdatedPrices($productIds, $websiteId, $isRetention, $packageType);
            $cache->save(serialize($prices), $cacheKey, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
        }

        $date = new DateTime('+1 year');
        return $this->jsonResponse(
            array(
                'prices' => $prices
            ), 200,
            [
                'Pragma' => 'cache',
                'Cache-Control' => 'max-age=31536000, must-revalidate',
                'Expires' => $date->format('r'),
                'X-Cache-Override' => true,
            ]
        );
    }

    /**Als dat je antwoord is
     * @param $section
     *
     * Get configuration for specific action
     */
    protected function getActionData($section)
    {
        $section = strtolower(str_replace('Action', '', $section));
        $config = array(
            'endpoints' => array(
                'products' => sprintf('configurator/%s/all', $section),
                'cart.addMulti' => 'configurator/cart/addMulti',
                'cart.delete' => 'configurator/cart/remove',
                'cart.list' => 'configurator/cart/list',
                'cart.empty' => 'configurator/cart/empty',
                'cart.getRules' => 'configurator/cart/getRules',
                'cart.getBundles' => 'configurator/cart/getEligibleBundles',
                'cart.getPrices' => 'configurator/init/getPrices',
                'cart.getdeviceRules' => 'configurator/cart/getDeviceRules',
                'cart.getsubscriptionRules' => 'configurator/cart/getSubscriptionRules',
                'bundles.create' => 'bundles/index/create',
                'bundles.getHint' => 'bundles/index/hint',
                'bundles.updateChoice' => 'bundles/index/updateBundleChoice',
                'bundles.fetchMandatoryFamilies' => 'bundles/index/fetchMandatoryFamilies',
                'redplus.updatePackageParent' => 'configurator/cart/updatePackageParent'
            ),
            'container' => '#config-wrapper',
            'sections' => array_map('strtolower', array_diff(
                Mage::getSingleton('dyna_configurator/catalog')->getOptionsForPackageType($section),
                Mage::getSingleton('dyna_configurator/catalog')->getOrderRestrictions(Mage::app()->getWebsite()->getCode())
            )),
            'type' => $section,
            'spinner' => 'skin/frontend/omnius/default/images/spinner.gif',
            'spinnerId' => '#spinner',
            'cache' => false,
            'include_btw' => Mage::getSingleton('customer/session')->showPriceWithBtw(),
            'hardwareOnlySwap' => Mage::getSingleton('customer/session')->getOrderEditMode()
        );

        return $this->buildConfigurator($section, $config);
    }

    /**
     * Initializes the configurator based on the package type and sale type.
     * @param $type
     * @param array $config
     * @param string $saleType
     */
    protected function buildConfigurator($type, array $config = array(), $saleType = Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION)
    {
        $packageId = (int)$this->getRequest()->getParam('packageId');
        $currentProducts = $this->getRequest()->getParam('current_products');
        $migrationOrMoveOffnet = $this->getRequest()->getParam('migrationOrMoveOffnet');
        $migrationOrMoveOffnetCustomerNo = $this->getRequest()->getParam('migrationOrMoveOffnetCustomerNo');
        $ctn = $this->getRequest()->getParam('ctn');
        $offerId = $this->getRequest()->getParam('offerIdCampaign');
        $hasParentPackage = $this->getRequest()->getParam('has_parent');
        $markBundleId = $this->getRequest()->getParam('bundleId');
        $relatedProductSku = $this->getRequest()->getParam('relatedProductSku');
        $filters = $this->getRequest()->getParam('filters');
        $packageCreationTypeId = $this->getRequest()->getParam('packageCreationTypeId');
        $executeClearCart = (int)$this->getRequest()->getParam('executeClearCart');
        $executeClearCart && Mage::helper('dyna_package')->clearServicePackages();
        $groupBundleId = null;
        $linkedAccountSelected = null;

        // search for permissions
        $customerSession            = Mage::getSingleton('dyna_customer/session');
        $contextHelper              = Mage::helper('dyna_catalog/processContext');
        $processContext             = $contextHelper->getProcessContextCode();
        $agent                      = $customerSession->getAgent();
        $agentHasAllowCab           = $agent->isGranted('ALLOW_CAB');
        $agentHasAllowDsl           = $agent->isGranted('ALLOW_DSL');
        $agentHasAllowMob           = $agent->isGranted('ALLOW_MOB');
        $agentHasDebitToCredit      = $agent->isGranted('DEBIT_TO_CREDIT');
        $agentHasSendSavedOffer     = $agent->isGranted('SEND_OFFER');

        // no permission error
        $permissionError = $this->jsonResponse(array(
            'error' => true,
            'message' => $this->__("You do not have the permission for this action")
        ));

        switch (strtoupper($type)) {
            case Dyna_Catalog_Model_Type::TYPE_FIXED_DSL:
            case Dyna_Catalog_Model_Type::TYPE_LTE:
                // disable dsl if general permissions are missing
                if (!$agentHasAllowDsl) {
                    return $permissionError;
                }

                // disable dsl if pre-post and permissions missing
                if ($agentHasDebitToCredit &&
                    !$agentHasSendSavedOffer &&
                    in_array($processContext, [Dyna_Catalog_Model_ProcessContext::MIGCOC])) {
                    return $permissionError;
                }
                break;

            case Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE:
            case Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT:
            case Dyna_Catalog_Model_Type::TYPE_CABLE_TV:
                // disable cable if general permissions are missing
                if (!$agentHasAllowCab) {
                    return $permissionError;
                }
                // disable cable if pre-post and permissions missing
                if ($agentHasDebitToCredit &&
                    !$agentHasSendSavedOffer &&
                    in_array($processContext, [Dyna_Catalog_Model_ProcessContext::MIGCOC])) {
                    return $permissionError;
                }
                break;

            case Dyna_Catalog_Model_Type::TYPE_MOBILE:
            case Dyna_Catalog_Model_Type::TYPE_PREPAID:
            case Dyna_Catalog_Model_Type::TYPE_REDPLUS:
                // disable mobile if general permissions are missing
                if (!$agentHasAllowMob) {
                    return $permissionError;
                }
                break;

            default:
                // nothing to do
        }

        $disabledSections = [];

        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('dyna_customer/session');

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        /** @var Dyna_Package_Model_Package $packageModel */
        $packageModel = null;
        $firstSubscriptionCustomerNumber = null;
        if ($packageId) {
            $packageModel = $quote->getCartPackage($packageId);
        } else {
            // try to get Customer Number from the first eligible subscription in installed base
            /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
            $bundlesHelper = Mage::helper('dyna_bundles');

            $eligibleBundles = $bundlesHelper->getFrontendEligibleBundles(true);
            $bundlesHelper->filterBundlesBySubscriptionStatus($eligibleBundles);

            foreach ($eligibleBundles as $eligibleBundle) {
                if ($eligibleBundle['redPlusBundle'] && !empty($eligibleBundle['targetedSubscriptions'])) {
                    foreach ($eligibleBundle['targetedSubscriptions'] as $targetedSubscription) {
                        $firstSubscriptionCustomerNumber = $targetedSubscription['customerNumber'];
                        break 2;
                    }
                }
            }

        }

        if (Mage::getSingleton('dyna_customer/session')->getCustomer()->isCustomerLoggedIn()) {
            // Get Customer Number from Cart package (if exists)
            if ($packageModel) {
                $customerNumber = $packageModel->getParentAccountNumber();
            } elseif ($firstSubscriptionCustomerNumber) { // from linked Account Number
                $customerNumber = $firstSubscriptionCustomerNumber;
            } else { // from Session
                $customerNumber = $customerSession->getCustomer()->getCustomerNumber();
            }

            $hasOrders = $this->checkOpenOrders($customerNumber, $packageModel, $type);
            if (!is_bool($hasOrders)) {
                $disabledSections[] = $hasOrders;
            }
        }

        if (!$this->checkGrantedPermissions($migrationOrMoveOffnet, $ctn)) {
            return $permissionError;
        }

        /** @var  $cartHelper  Dyna_Configurator_Helper_Cart */
        $cartHelper = Mage::helper('dyna_configurator/cart');
        $cartHelper->setNewPackagePermanentFilters($filters);
        $saleType = $this->getRequest()->getParam('saleType') ?: $saleType;
        $extraCTNPackages = array();
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Dyna_Bundles_Helper_Redplus $redPlusHelper */
        $redPlusHelper = Mage::helper('dyna_bundles/redplus');
        $bundlesHelper = Mage::helper('dyna_bundles');
        $redPlusEligiblePackages = $redPlusHelper->getRedPlusEligiblePackages();
        $relatedProductSku = $relatedProductSku ?: ($redPlusEligiblePackages->getSize() > 0 && $packageCreationTypeId == $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS) ? $redPlusEligiblePackages->getFirstItem()->getData('eligibleProductSku') : '');
        try {
            $linkedAccountSelected = null;

            if (!$packageId) {
                $ctns = explode(',', $ctn);
                $descriptions = Mage::getModel('ctn/ctn')->getAllCTNDetails($ctns);
                $getKey = function (array &$array, $key) {
                    return isset($array[$key]) ? $array[$key] : '';
                };

                $packageCTN = array_pop($ctns);
                // check if one-of-deal is active
                $isOneOfDeal = $cartHelper->isOneOfDealActive();
                $oneOfDealOptions = $isOneOfDeal && $packageCTN ? array('retainable' => false) : array();

                if ($packageCTN) {
                    $len = count($ctns);
                    for ($i = 0; $i < $len; ++$i) {
                        $extraCTNPackages[$ctns[$i]] = array(
                            'id' => (int)$cartHelper->initNewPackage(
                                $type,
                                $saleType,
                                $ctns[$i],
                                $getKey($descriptions, $ctns[$i]),
                                $oneOfDealOptions
                            ),
                            'description' => $getKey($descriptions, $ctns[$i]),
                        );
                    }

                    // only store description for last package
                    $extraCTNPackages[$packageCTN] = array(
                        'description' => $getKey($descriptions, $packageCTN),
                    );
                }
                $parentPackageId = null;

                if ($hasParentPackage) {
                    $parentPackageId = $quote->getActivePackage()->getEntityId();
                } elseif ($packageCreationTypeId == $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS) && $redPlusEligiblePackages->getSize() > 0) {
                    // try to get the first eligible package in cart
                    $parentPackageId = $redPlusEligiblePackages->getFirstItem()->getEntityId();
                    $groupBundleId = $redPlusEligiblePackages->getFirstItem()->getData('groupBundleId');
                } elseif ($packageCreationTypeId == $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS)) {
                    // try to get the first eligible subscription in installed base
                    /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
                    $bundlesHelper = Mage::helper('dyna_bundles');

                    $eligibleBundles = $bundlesHelper->getFrontendEligibleBundles(true);
                    $bundlesHelper->filterBundlesBySubscriptionStatus($eligibleBundles);
                    $parentCreationTypeId = $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_MOBILE);

                    foreach ($eligibleBundles as $eligibleBundle) {
                        if ($eligibleBundle['redPlusBundle'] && !empty($eligibleBundle['targetedSubscriptions'])) {
                            foreach ($eligibleBundle['targetedSubscriptions'] as $targetedSubscription) {
                                $parentPackageId = Mage::helper('dyna_configurator')
                                    ->getGhostPackageId(
                                        $eligibleBundle['bundleId'],
                                        $targetedSubscription['customerNumber'],
                                        $targetedSubscription['productId'],
                                        Dyna_Catalog_Model_Type::TYPE_MOBILE,
                                        $targetedSubscription['relatedProductSku'],
                                        $targetedSubscription['sharingGroupId'],
                                        $parentCreationTypeId
                                    );
                                $groupBundleId = $eligibleBundle['bundleId'];
                                $linkedAccountSelected = $targetedSubscription['customerNumber'];
                                break 2;
                            }
                        }
                    }
                }

                // Save bundleId and relatedProductSku on parent package in case of Red+
                if ($packageCreationTypeId == $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS) && $parentPackageId) {
                    $parentPackage = null;

                    foreach ($quote->getCartPackages() as $cartPackage) {
                        if ($cartPackage->getEntityId() == $parentPackageId) {
                            $parentPackage = $cartPackage;
                            break;
                        }
                    }

                    if ($parentPackage) {
                        if ($relatedProductSku) {
                            $parentPackage->setRedplusRelatedProduct($relatedProductSku);
                        }
                        if ($markBundleId) {
                            $bundlesHelper->addBundlePackageEntry($markBundleId, $parentPackage->getEntityId());
                        }
                        if ($groupBundleId) {
                            $bundlesHelper->addBundlePackageEntry($groupBundleId, $parentPackage->getEntityId());
                        }
                        $parentPackage->updateBundleTypes()->save();
                    } else {
                        Mage::throwException('Parent package with id ' . $parentPackageId . ' could not be loaded');
                    }

                }

                $packageId = (int)$cartHelper->initNewPackage(
                    $type,
                    $saleType,
                    $packageCTN,
                    $currentProducts,
                    $oneOfDealOptions,
                    $parentPackageId,
                    $relatedProductSku,
                    $markBundleId,
                    $packageCreationTypeId
                );

                //If migration or moveOffnet is in progress, set flag on customer model loaded to customer/session
                if ($migrationOrMoveOffnet) {
                    // the customer number for the migration/move offnet must be set on session for later use in the checkout: we consider it an unique identifier for the service product details
                    Mage::helper('dyna_customer')->setMigrationData($migrationOrMoveOffnet, $migrationOrMoveOffnetCustomerNo, $quote->getCartPackage($packageId)->getId());
                }
            }

            // Determine the agent's (COPS / Telesales) VOID(Red Sales Id)
            $redSalesId = Mage::helper('bundles')->getRedSalesId();
            $salesIdsTriple = Mage::helper('bundles')->getProvisDataByRedSalesId();
            $quote->saveActivePackageId($packageId, ['sales_id' => $redSalesId]);

            $cartItems = $cartHelper->getCartItems($type, $packageId);
            $initCart = $cartItems['initCart'];
            $installedBaseCart = $cartItems['installedBaseCart'];
            $defaultedItems = $cartHelper->getDefaultedProductsFromCart($quote, $packageId);
            $defaultedObligatedItems = $cartHelper->getDefaultedObligatedProductsFromCart($quote, $packageId);

            $config['packageId'] = (int)$packageId;
            $config['sale_type'] = $saleType;
            $config['identifier'] = (int)$quote->getId();
            $config['current_products'] = $currentProducts;
            Mage::register('freeze_models', true, true);
            /** @var Dyna_Package_Model_Package $packageModel */
            $packageModel = $quote->getCartPackage($packageId);
            $config['old_sim'] = $packageModel->getId() && $packageModel->getOldSim();

            /** @var Dyna_Catalog_Model_ProcessContext $processContextModel */
            $processContextId = Mage::getModel('dyna_catalog/processContext')->getProcessContextIdByCode($packageModel->getSaleType());
            foreach ($config['sections'] as $section) {
                if (Mage::helper('dyna_configurator')->isSectionDisabled($type, $section, $processContextId)) {
                    $disabledSections[] = $section;
                }
            }
            $config['disabledSections'] = $disabledSections;

            // add bundle entry if init configurator will create a bundle
            if ($markBundleId) {
                $bundlesHelper->addBundlePackageEntry($markBundleId, $packageModel->getEntityId());
            }
            if ($groupBundleId) {
                $bundlesHelper->addBundlePackageEntry($groupBundleId, $packageModel->getEntityId());
            }

            if (isset($linkedAccountSelected) && $linkedAccountSelected) {
                $packageModel->setParentAccountNumber($linkedAccountSelected);
            }
            $packageModel->updateBundleTypes()->save();

            /* If we have a campaign, every quote must have set the data campaign*/
            /** @var Dyna_Customer_Model_Session $session */
            $session = Mage::getSingleton('dyna_customer/session');
            $uctParams = $session->getUctParams();

            if ($uctParams && !empty($uctParams['transactionId']) && !empty($uctParams['campaignId'])) {
                $quote->setTransactionId($uctParams["transactionId"])
                    ->setCampaignCode($uctParams["campaignId"])
                    ->setSalesId($redSalesId) // this is needed because the model has salesId = NULL and it overrides the one set above in saveActivePackage
                    ->save();
                if ($offerId) {
                    $uctParams['offerId'] = $offerId;
                    $session->setUctParams($uctParams);
                    $packageModel->setOfferId($offerId);
                }
                $packageModel->save();
            }
            // get linked accounts block in case of mobile
            $linkedAccountSelection = "";
            /** @var Dyna_Customer_Model_Customer $customer */
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            /** @var Dyna_Customer_Helper_Customer $customerHelper */
            $customerHelper = Mage::helper('dyna_customer/customer');
            $customerHelper->handleLinkedAccountPerPackage($type, $packageCreationTypeId, $customer, $linkedAccountSelection, $linkedAccountSelected);
            $rightBlock = Mage::getSingleton('core/layout')->createBlock('core/template')
                ->setTemplate('checkout/cart/partials/package_block.phtml')
                ->setData(array(
                    'guiSection' => Dyna_Catalog_Model_Product::GUI_CART
                ))
                ->toHtml();
            /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
            $processContextHelper = Mage::helper("dyna_catalog/processContext");
            $responseArray = array(
                'error' => false,
                'initCart' => count($initCart) ? $initCart : new stdClass(),
                'installedBaseCart' => count($installedBaseCart) ? $installedBaseCart : new stdClass(),
                'defaultedItems' => count($defaultedItems) ? $defaultedItems : new stdClass(),
                'defaultedObligatedItems' => count($defaultedObligatedItems) ? $defaultedObligatedItems : new stdClass(),
                'config' => $config,
                'packageId' => (int)$packageId,
                'promoRules' => Mage::helper('pricerules')->findApplicablePromoRules(true),
                'processContextCode' => $processContextHelper->getProcessContextCode(),
                'rightBlock' => mb_convert_encoding($rightBlock, "UTF-8"),
                'cartIsRedPlusEligible' => Mage::helper('bundles')->cartIsRedPlusEligible(),
                'linkedAccountSelection' => !in_array($saleType, Dyna_Catalog_Model_ProcessContext::ILSProcessContexts()) ? $linkedAccountSelection : '',
                'salesIds' => $salesIdsTriple,
                'quoteHash' => Mage::helper('dyna_checkout/cart')->getQuoteProductsHash(),
                'salesId' => $quote->getSalesId(),
                'packageEntityId' => (int)$quote->getCartPackage()->getEntityId(),
                'hintMessages' => Mage::getSingleton('customer/session')->getHints() ?: array(),
            );

            if (($incompleteSections = Mage::helper('omnius_checkout')->checkPackageStatus($packageId)) && is_array($incompleteSections)) {
                $responseArray['incomplete_sections'] = $incompleteSections;
            }

            if (!empty($extraCTNPackages)) {
                $responseArray['ctnPackages'] = $extraCTNPackages;
            }

            // Get Kias for Red+
            $serviceCustomers = Mage::getSingleton('dyna_customer/session');
            $customerProducts = $serviceCustomers->getCustomerProducts();
            $kiasProducts  = (!empty($customerProducts[Dyna_Superorder_Helper_Client::BACKEND_KIAS])) ? $customerProducts[Dyna_Superorder_Helper_Client::BACKEND_KIAS] : [];
            // remove prepaid
            $marketCodeMMO = Dyna_Mobile_Model_Product_Attribute_Option::MARKET_CODE_MMO;
            if($kiasProducts) {
                foreach ($kiasProducts as $firstKey => $kiasProduct) {
                    foreach ($kiasProduct['contracts'] as $secondKey => $contract) {
                        if ($contract['subscriptions'][0]['contract_type'] == $marketCodeMMO) {
                            unset($kiasProduct['contracts'][$secondKey]);
                        }
                    }

                    if (count($kiasProduct['contracts']) == 0) {
                        unset($kiasProducts[$firstKey]);
                    }
                }
            }

            // For RED+ packages append the select parent package block
            if ($packageCreationTypeId == $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS) &&
                ($redPlusHelper->getNumberOfRedPlusEligiblePackagesInCart() > 0 || $packageModel->getParentId() || $kiasProducts)) {
                $quote->clearCachedPackages();

                $currentQuote = Mage::getSingleton('checkout/session')->getQuote();
                $activePackageParentId = $currentQuote->getActivePackage() ? $currentQuote->getActivePackage()->getParentId() : null;

                $dbConnection      = Mage::getSingleton('core/resource')->getConnection('core_read');
                $activeParentQuery = "SELECT `sharing_group_id` FROM `catalog_package` WHERE `entity_id` = :id LIMIT 1";
                $sharingGroupId    = $dbConnection->fetchOne($activeParentQuery, array(':id' => $activePackageParentId));

                $responseArray['redplusParentSelectionBlock'] = Mage::getSingleton('core/layout')
                    ->createBlock('dyna_bundles/redplus')
                    ->setTemplate('bundles/redplus.phtml')
                    ->setKiasProducts($kiasProducts)
                    ->setActivePackageParentId($activePackageParentId)
                    ->setSharingGroupId($sharingGroupId)
                    ->setAddedFromMyProducts($currentQuote->getActivePackage()->getAddedFromMyProducts())
                    ->toHtml();
            }

            $responseArray['activeBundleId'] = Mage::getSingleton('dyna_customer/session')->getActiveBundleId();
            $specialProducts = Mage::helper('dyna_configurator/services')->getSpecialPriceProducts();
            $responseArray['specialPriceProducts'] = $specialProducts['valid'] ?? null;
            $responseArray['specialPriceProductsInvalid'] = $specialProducts['invalid'] ?? null;

            if ($bundlesHelper->isBundleChoiceFlow()) {
                $responseArray['optionsPanel'] = $this->getLayout()->createBlock('dyna_bundles/options')->setTemplate('bundles/options.phtml')->toHtml();
            }

            // Check whether install base bundles have been broken and append an information message to the response
            $customer = $customerSession->getCustomer();
            if (($customer && $packageModel) && ($serviceLineId = $packageModel->getServiceLineId()) && ($customerId = $packageModel->getParentAccountNumber())) {
                if ($customer->isIBProductPartOfBundle($customerId, $serviceLineId) && $packageModel->hasDroppedBundleSubscription()) {
                    $responseArray['infoMessage'] = array(
                        'id' => sprintf("%d", $packageModel->getId()),
                        'message' => $this->__("The install base bundle was broken."),
                        'isModal' => true,
                    );
                }
            }

            return $this->jsonResponse($responseArray);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            return $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load configurator',
            ));
        }
    }

    /**
     * Get html block containing all modals
     */
    public function modalsAction()
    {
        $this->loadLayout();
        $this->getLayout()->removeOutputBlock('root')->addOutputBlock('content');
        $this->renderLayout();
    }

    /**
     * Get open orders from the database
     */
    public function getOpenOrdersAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        try {
            $response = [];

            $clusterType = $this->getRequest()->getParam('clusterType', 'all');
            $resultsPerPage = ($this->getRequest()->getParam('resultsPerPage')) ? $this->getRequest()->getParam('resultsPerPage') : 20;
            if (($resultsPerPage == 0) || ($resultsPerPage == '')) {
                $this->displayOpenOrdersError($clusterType, $resultsPerPage);
                return;
            }
            $currentPage = $this->getRequest()->getParam('currentPage', 1);

            $ordersCollection = Mage::helper('dyna_configurator')->getOpenOrders($clusterType, $resultsPerPage, $currentPage);
            if (sizeof($ordersCollection) == 0) {
                $this->displayOpenOrdersError($clusterType, $resultsPerPage);
                return;
            }
            $orders = Mage::helper('dyna_configurator')->getOrdersInfo($ordersCollection);

            $response['has_prev_page'] = !($currentPage == 1);

            if (sizeof($ordersCollection) > $resultsPerPage) {
                $response['has_next_page'] = true;
                array_pop($orders);
            }

            $response['next_page'] = $currentPage + 1;
            $response['prev_page'] = $currentPage - 1;
            $response['resultsPerPage'] = $resultsPerPage;
            $response['currentCluster'] = $clusterType;
            $response['orders'] = $orders;
        } catch (Exception $e) {
            $response = [
                'Success' => false,
                'message' => $e->getMessage()
            ];
        }

        $this->jsonResponse($response);
    }

    /**
     * Display error for invalid search
     */
    public function displayOpenOrdersError($clusterType, $resultsPerPage)
    {
        $errorResponse = array(
            'error' => true,
            'currentCluster' => $clusterType,
            'resultsPerPage' => $resultsPerPage,
            'message' => $this->__('No orders with error')
        );

        return $this->jsonResponse($errorResponse);

    }

    protected function checkGrantedPermissions($migrationOrMoveOffnet, $ctn)
    {
        $customerSession = Mage::getSingleton('customer/session');

        if($migrationOrMoveOffnet) {
            $moveOffNet = ($migrationOrMoveOffnet == Dyna_Catalog_Model_Type::MOVE_OFFNET && !$customerSession->getAgent()->isGranted(array('MOVE_OFFNET_CABLE_TO_DSL')));
            $migrationCable = ($migrationOrMoveOffnet == Dyna_Catalog_Model_Type::MIGRATION && !$customerSession->getAgent()->isGranted(array('MIGRATION_DSL_TO_CABLE')));

            if($moveOffNet || $migrationCable) {
                return false;
            }
        }

        $uctParams = $customerSession->getUctParams() ?: null;
        if ($uctParams && array_key_exists('callingNumber', $uctParams) && $uctParams['callingNumber'] == $ctn &&
            !$customerSession->getAgent()->isGranted(array('SELECT_CAMPAIGN_OFFER'))
        ) {
            return false;
        }
        return true;
    }

    /**
     * Set linked account data per session and cart packages
     */
    public function setLinkedAccountAction()
    {
        $linkedAccountNumber = $this->getRequest()->getParam('linked_account_number');
        $clearCart = $this->getRequest()->getParam('clear_cart', false);

        //clear cart for mobile stack on Pre-post flow
        if ($clearCart == 'true') {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            /** @var Dyna_Package_Helper_Data $packageHelper */
            $packageHelper = Mage::helper('dyna_package');
            $activePackage = $quote->getCartPackage();
            foreach ($quote->getCartPackages() as $package) {
                // This is an iteration over some cached instances, make sure these are still persistent
                $dbPackage = Mage::getModel('dyna_package/package')->load($package->getId());
                if ($dbPackage && $dbPackage->isMobile() && !$dbPackage->isPrepaid() && $activePackage->getId() != $dbPackage->getId() && !$dbPackage->getEditingDisabled()) {
                    $packageHelper->removePackage($dbPackage->getPackageId());
                }
            }
            Mage::getSingleton('checkout/cart')->save();
            $activePackage = Mage::getModel('dyna_package/package')->load($activePackage->getId());
            $activePackageId = (int) $activePackage->getPackageId();
            $quote ->setActivePackageId($activePackageId)->save();
            $result = [
                'rightBlock' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml(),
                'totals' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                'package_id' => $activePackageId
            ];
        }
        /** @var Dyna_Customer_Helper_Customer $customerHelper */
        $customerHelper = Mage::helper('dyna_customer/customer');
        $customerHelper->setPackageAndSessionLinkedAccount($linkedAccountNumber);

        /** @var Dyna_Bundles_Helper_Data $bundleHelper */
        $bundleHelper = Mage::helper('bundles');

        $result['cartIsRedPlusEligible'] = $bundleHelper->cartIsRedPlusEligible();
        $result['error'] = false;
        $this->jsonResponse($result);
    }

    protected function jsonResponse($data, $statusCode = 200, $headers=[])
    {
        parent::jsonResponse($data, $statusCode);
        if (!$this->getRequest()->isAjax()) {
            $body = $this->getResponse()->getBody();
            $this->getResponse()->setHeader('Content-Type', 'text/html; charset=utf-8');
            foreach($headers as $key => $value) {
                $this->getResponse()->setHeader($key, $value, true);
            }
            $this->getResponse()
                ->setBody($body);
        }
    }

    /**
     * @param string $customerNumber
     * @param Dyna_Package_Model_Package $packageModel
     * @param string $type
     * @return bool
     */
    public function checkOpenOrders($customerNumber, $packageModel, $type)
    {
        /** @var Dyna_Customer_Helper_Data $customerHelper */
        $customerHelper = Mage::helper('dyna_customer');
        switch (strtolower($type)) {
            case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
                // If Customer has Mobile OSF Open Orders no more actions allowed
                $hasOsfOrder = (bool)$customerHelper->hasOSFOpenOrders($customerNumber, Dyna_Catalog_Model_Type::getMobilePackages(), $packageModel ? $packageModel->getCtn() : null);
                if ($hasOsfOrder === true) {
                    return true;
                    // If Customer has Mobile Pending Orders with Future Activity Code status = allowed disable Tariff section
                } else {
                    $futureReasonCode = $customerHelper->retrieveFutureReasonCode(Dyna_Catalog_Model_Type::getMobilePackages());
                    $futureActivityCode = $customerHelper->hasNonOSFOrders($customerNumber, Dyna_Catalog_Model_Type::getMobilePackages(), $packageModel ? $packageModel->getCtn() : null);
                    $notAllowedActivityCodes = Mage::getSingleton('dyna_customer/activityCodes')->getNotAllowedActivityCodes();
                    if ($futureActivityCode !== false &&
                        ((strtoupper($futureActivityCode) === Dyna_Customer_Helper_Customer::OSF_ORDER_ACTIVITY_CAN) &&
                            (in_array(strtoupper($futureReasonCode),$customerHelper->cancelSubscriberReasonCodes())))
                        || (in_array($futureActivityCode, (!empty($notAllowedActivityCodes) ? $notAllowedActivityCodes : array())) &&
                        $packageModel && in_array($packageModel->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts()))
                    ) {
                        // disable Tariff section
                        return strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION);
                    }
                }
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_LTE):
                // Check if Customer has Fixed Open Orders
                if ($customerHelper->hasOpenOrders($customerNumber, Dyna_Catalog_Model_Type::getFixedPackages())) {
                    return true;
                }
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
                // Check if Customer has KIP Open Orders
                if ($customerHelper->hasOpenOrders($customerNumber, [Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE])) {
                    return true;
                }
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
                // Check if Customer has CLS Open Orders
                if ($customerHelper->hasOpenOrders($customerNumber, [Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT])) {
                    return true;
                }
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
                // Check if Customer has TV Open Orders
                if ($customerHelper->hasOpenOrders($customerNumber, [Dyna_Catalog_Model_Type::TYPE_CABLE_TV])) {
                    return true;
                }
                break;
        }
        return false;
    }
}
