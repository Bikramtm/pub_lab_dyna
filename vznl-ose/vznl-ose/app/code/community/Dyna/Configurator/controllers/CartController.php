<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once Mage::getModuleDir('controllers', 'Omnius_Configurator').DS.'CartController.php';

/**
 * Class Dyna_Configurator_CartController
 */
class Dyna_Configurator_CartController extends Omnius_Configurator_CartController
{
    /** @var Dyna_Configurator_Helper_ExtraLogCart $rulesHelper */
    protected $logRulesHelper;
    /** @var Dyna_Configurator_Helper_Cart $configuratorCartHelper */
    protected $configuratorCartHelper = false;

    public function preDispatch()
    {
        if (Mage::app()->getRequest()->getActionName() == 'getCompatibility') {
            define(CM_REDISSESSION_LOCKING_ENABLED, false);
        }

        $this->logRulesHelper = Mage::helper('dyna_configurator/extraLogCart');
        return parent::preDispatch();
    }

    /*
     * Check if products are compatible with each other
     */
    public function getCompatibilityAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $productIds = array();
        $type = $this->getRequest()->get('type');
        $offerCompatibility = false;
        if ($type) {
            if ($products = Mage::app()->getRequest()->getParam('products', false)) {
                $productIds = explode(',', $products);
            }

            try {
                $offerCompatibility = Mage::helper('dyna_configurator/cart')->getCampaignOfferCompatibility($type, $productIds);
            } catch (Exception $e) {
                return $this->jsonResponse([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        return $this->jsonResponse([
            'error' => false,
            'compatible' => $offerCompatibility
        ]);
    }

    /**
     * Returns eligible bundles in frontend
     *
     * @return string
     */
    public function getEligibleBundlesAction()
    {
        /** @var Dyna_Bundles_Helper_Data $bundleHelper */
        $bundleHelper = Mage::helper('dyna_bundles');
        $tariffs = array_filter($this->getRequest()->get('tariffs') ?: array());

        // Return eligible bundles for current state
        $this->jsonResponse(array(
            'activeBundles' => $bundleHelper->getActiveBundles(),
            'eligibleBundles' => $bundleHelper->getFrontendEligibleBundles(false, true, $tariffs),
            'bundleComponents' => $bundleHelper->getBundleComponents(),
            'bundleHints' => $bundleHelper->getBundleHints(),
        ));
    }

    /**
     * Get list of products that are visible in frontend based on current rules
     */
    public function getRulesAction()
    {
        $productIds = array();
        $ilsProductIds = array();
        /** If there are current products in cart, forward them to rules processor */
        if ($products = Mage::app()->getRequest()->getParam('products', false)) {
            $productIds = explode(',', $products);
            sort($productIds);
        }
        if ($ilsProducts = Mage::app()->getRequest()->getParam('ilsProducts', false)) {
            $ilsProductIds = explode(',', $ilsProducts);
            sort($ilsProductIds);
        }
        $type = $this->getRequest()->get('type');
        $packageId = $this->getRequest()->get('packageId');
        $websiteId = $this->getRequest()->get('websiteId');
        $customerType = $this->getRequest()->get('customer_type');
        $customerNumber = $this->getRequest()->get('customer_number');
        $addressId = $this->getRequest()->get('address_id');
        $hash = $this->getRequest()->get('hash');

        $packageCreationId = Mage::getSingleton('checkout/cart')
            ->getQuote()
            ->getCartPackage($packageId)
            ->getPackageCreationTypeId();

        Mage::register('getRules__customer_type', $customerType);

        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");

        try {
            $doCache = !Mage::helper('dyna_productmatchrule')->isDebugMode();
            $cablePackages = Dyna_Catalog_Model_Type::getCablePackages();
            // Cable specific service calls
            if (in_array(strtolower($type), $cablePackages)) {
                $doCache = false;
            }
            $cacheKey = "getRules_" . md5(serialize([$type, $packageId, $websiteId, $customerType, $customerNumber, $addressId, $productIds, $ilsProducts, $hash, $processContextHelper->getProcessContextCode(), $packageCreationId]));

            if ($doCache && ($cachedRules = $this->getCache()->load($cacheKey))) {
                $rules = unserialize($cachedRules);
                $rules['rules'] = array_values($rules['rules']);
                $rules['cache'] = 1;
            } else {
                // the getRules method call is under this line
                $rules = $this->getRules($productIds, $type, $websiteId, $ilsProductIds);
                $rules['context'] = $processContextHelper->getProcessContextCode();
                $rules['cache'] = 0;
                // with cache but not cached
                if ($doCache) {
                    $this->getCache()->save(serialize($rules), $cacheKey, [Dyna_Cache_Model_Cache::PRODUCT_TAG]);
                }
            }

            $rules['message_hint'] =  Mage::helper('dyna_checkout')->getMessageHint($rules['rules']);

            $this->jsonResponse($rules);
        } catch (Exception $e) {

            $this->jsonResponse([
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Removes the quote from session and creates another one.
     */
    public function emptyAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        Mage::helper('dyna_customer')->clearMigrationData();
        Mage::getSingleton('dyna_customer/session')->unsActiveBundleId();
        $this->unsetLinkedAccount();
        parent::emptyAction();
    }

    /**
     * Deletes a quote specified in a post call.
     */
    public function deleteSavedQuoteAction()
    {
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');

        if ($this->getRequest()->isPost()){
            $param = $this->getRequest()->get('quote_id');
            if($param){
                try{
                    $quote = Mage::getModel('sales/quote')->getCollection()
                        ->addFieldToFilter('entity_id', $param)
                        ->getFirstItem();

                    if ($quote->getCartStatus() == Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED &&
                        !$customerSession->getAgent()->isGranted(array('DELETE_SAVED_SHOPPING_CART'))) {
                        $this->jsonResponse(array(
                            'error'     => true,
                            'message'   => $this->__("You do not have the permission for this action")
                        ));
                        return;
                    }
                    if ($quote->getIsOffer() && !$customerSession->getAgent()->isGranted(array('DELETE_SAVED_OFFER'))) {
                        $this->jsonResponse(array(
                            'error'     => true,
                            'message'   => $this->__("You do not have the permission for this action")
                        ));
                        return;
                    }

                    if ($quote->getIsOffer()) {
                        Mage::helper('pricerules')->decrementAll($quote);
                    }

                    $projectionService = Mage::getModel('dyna_myvfapi/service_projection', [
                        'credisClient' => Mage::helper('dyna_myvfapi/credis')->buildCredisClient()
                    ]);
                    
                    /** @var Dyna_MyVFApi_Helper_Data $myVfApi */
                    $myVfApi = Mage::helper('dyna_myvfapi');
                    //we don't need the offer's data for the delete action
                    $quoteProjection = $myVfApi->buildProjectionFromQuoteWithoutExtractingOfferData($quote);
                    $projectionService->delete($quoteProjection);

                    $quote->setIsActive(false);
                    $quote->delete();
                    $this->jsonResponse(array(
                        'error' => false
                    ));
                } catch(Exception $e) {
                    $this->jsonResponse(array(
                        'error'     => true,
                        'message'   => $e->getMessage()
                    ));
                }
            }else {
                $this->jsonResponse(array(
                    'error'     => true,
                    'message'   => Mage::helper('omnius_checkout')->__('The quote id was not specified.')
                ));
            }
        }
    }

    /**
     * The action used for the removal of packages from the configurator.
     */
    public function removePackageAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $accepted = $this->getRequest()->getParam('accepted', '0');
        try {
            $packageId = $this->getRequest()->getParam('packageId');
            $expanded = $this->getRequest()->getParam('expanded');
            $btwToggle = $this->getRequest()->getParam('btwState');
            $initialPackageModelId = $this->_getQuote()->getCartPackage($this->getRequest()->getParam('packageId'))->getId();

            /** @var Dyna_Package_Helper_Data $packageHelper */
            $packageHelper = Mage::helper('dyna_package');
            $removePackageDetails = $packageHelper->removePackage($packageId,$accepted);
            // Determine the agent's (COPS / Telesales) VOID(Red Sales Id) for remaining packages
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = $this->_getCart()->getQuote();
            $redSalesId = Mage::helper('bundles')->getRedSalesId();
            $quote->setSalesId($redSalesId);

            $this->_getCart()->save();

            $rightBlock = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml();
            $mustRemoveBundleChoice= !Mage::helper('bundles')->isBundleChoiceFlow();

            /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
            $processContextHelper = Mage::helper("dyna_catalog/processContext");

            /** @var Dyna_Customer_Model_Session $session */
            $session = Mage::getSingleton('customer/session');
            $session->updateLinkedAccountNumber();

            $currentPackageModel = $quote->getCartPackage();
            $this->jsonResponse(array(
                'error'             => false,
                'totals'            => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->setBtwState($btwToggle)->toHtml(),
                'packagesIndex'     => $removePackageDetails['packagesIndex'],
                'complete'          => Mage::helper('dyna_checkout')->hasAllPackagesCompleted(),
                'expanded'          => $expanded ? Mage::getSingleton('core/layout')->createBlock('dyna_configurator/rightsidebar')->setTemplate('customer/right_sidebar.phtml')->toHtml() : '',
                'removeRetainables' => $removePackageDetails['removeRetainables'] ? array_values($removePackageDetails['packageIds']) : false,
                'quoteHash'         => Mage::helper('dyna_checkout/cart')->getQuoteProductsHash(),
                'salesId'           => $quote->getSalesId(),
                'rightBlock'        => $rightBlock,
                'packageType'        => $removePackageDetails['remainingCablePackage'],
                'mustRemoveBundleChoice' => $mustRemoveBundleChoice,
                'cartIsRedPlusEligible' => Mage::helper('bundles')->cartIsRedPlusEligible(),
                'processContextCode' => $processContextHelper->getProcessContextCode(),
                'clearConfigurator' => !$currentPackageModel || ($currentPackageModel->getId() != $initialPackageModelId),
            ));

        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__($e->getMessage())
            ));
        }
    }

    /**
     * Validate serial number for hardware products that client has in its installed base
     */
    public function validateHardwareSerialAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $serialNumber = trim($this->getRequest()->getParam("serialNumber"));
        $productId = trim($this->getRequest()->getParam("productId"));

        // Get customer model
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/session')->getCustomer();

        // Load product and check if it own smart card or own receiver
        /** @var Dyna_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->load($productId);

        /** @var Dyna_Validators_Helper_Data $validationHelper */
        $validationHelper = Mage::helper('dyna_validators');

        // Service calls for smartcard
        if ($product->isOwnSmartcard()) {
            if (!$validationHelper->validateSmartCardSerial($serialNumber)) {
                $response = $this->getErrorResponse($this->__("Invalid format for SmartCard serial number."));
                $this->jsonResponse($response);

                return;
            }

            // Get smartcard type call
            $requestData = ["SmartCardSerialNumber" => $serialNumber];
            /** @var Dyna_Configurator_Model_Client_SmartCardTypeClient $client */
            $smartCardTypeClient = Mage::getModel('dyna_configurator/client_smartCardTypeClient');
            $smartCardTypeData = $smartCardTypeClient->executeGetSmartCardType($requestData);

            if ($smartCardTypeData['ROOT']['DATA']['Type'] == 'UNKNOWN') {
                $response = $this->getErrorResponse($this->__("Type is unknown. SmartCard is not usable."));
                $this->jsonResponse($response);

                return;
            }

            // Set SmartCard on customer session
            /** @var Dyna_Catalog_Model_Smartcard $smartCard */
            $smartCard = Mage::getModel('dyna_catalog/smartcard');
            $smartCard->addData($smartCardTypeData['ROOT']['DATA']);
            $smartCard->setProductSku($product->getSku());
            $customer->setOwnSmartCard($smartCard);

            $response = Mage::helper('dyna_configurator/services')->checkSmartcardReceiverCompatibility();

            // Service calls for own receiver
        } elseif ($product->isOwnReceiver()) {
            if (!$validationHelper->validateReceiverSerial($serialNumber)) {
                $response = $this->getErrorResponse($this->__("Invalid format for Receiver serial number."));
                $this->jsonResponse($response);

                return;
            }

            // Get tv equipment service call
            /** @var Dyna_Configurator_Model_Client_TvEquipmentFeaturesClient $tvEquipmentClient */
            $tvEquipmentClient = Mage::getModel('dyna_configurator/client_tvEquipmentFeaturesClient');
            $requestData = ["EquipmentSerialNumber" => $serialNumber];
            $responseData = $tvEquipmentClient->executeGetTvEquipmentFeatures($requestData);

            /** @var Dyna_Catalog_Model_Receiver $receiver */
            $receiver = Mage::getModel('dyna_catalog/receiver');
            $receiver->addData($responseData['ROOT']['DATA']);
            $receiver->setSerialNumber($serialNumber);

            //If product is RCOWNHD and no HD attribute received true, or product is RCOWNHDB and no HDFree attribute received true, throw error to frontend (see OMNVFDE-2703)
            if (($product->getSku() === "RCOWNHD" && !$receiver->supportsHd()) || ($product->getSku() === "RCOWNHDB" && !$receiver->supportsHDFree())) {
                $response = $this->getErrorResponse($this->__("The receiver does not support HD or HDFree features."));
                return $this->jsonResponse($response);
            }

            $receiver->setProductSku($product->getSku());
            $customer->setOwnReceiver($receiver);

            //Again, if customer has already added it's own smartcard, check compatibility with it
            $response = Mage::helper('dyna_configurator/services')->checkSmartcardReceiverCompatibility();
        } else {
            $response = $this->getErrorResponse($this->__("Selected a product that is not a smartcard nor a receiver."));
        }

        $this->jsonResponse($response);
    }

    /**
     * Generate min/max/eql and cardinality warnings
     */
    public function getConfiguratorWarningsAction() {
        $cartProductsHash = $this->getRequest()->get('cartProductsHash');
        /** @var Dyna_Configurator_Helper_Data $configuratorHelper */
        $configuratorHelper = Mage::helper('dyna_configurator');
        $hasKipWhileNotAllowed = $configuratorHelper->hasKIPWhileNotAllowed() ? array('There should be only 1 kip at a specific address') : [];
        $ilsPackageHasBeenModified = Mage::getModel('package/package')->isILSPackageModified() || count($this->_getQuote()->getPackages()) == 0 ? [] : ['ILS package has not been modified'];
        $cardinalityWarnings = [];
        if (Mage::getStoreConfig('omnius_general/package_validation/enabled')) {
            $cardinalityWarnings = $this->getConfiguratorCartHelper()->getCardinalityWarnings() ?? [];
        }
        $minMaxEqlWarnings = $this->getConfiguratorCartHelper()->getMinMaxEqlWarnings($cartProductsHash) ?? [];
        $mandatoryWarnings = $this->getConfiguratorCartHelper()->getMandatoryWarnings() ?? [];
        $prolongationWarnings = $this->getConfiguratorCartHelper()->getProlongationWarnings() ?? [];

        $warnings = array_replace_recursive(
            $cardinalityWarnings
            ,$minMaxEqlWarnings
            ,$mandatoryWarnings
            ,$prolongationWarnings
            ,$hasKipWhileNotAllowed
            ,$ilsPackageHasBeenModified
        );

        foreach (Mage::getSingleton('checkout/cart')->getQuote()->getCartPackages() as $packageModel)
        {
            if (!empty($warnings[$packageModel->getPackageId()])) {
                // Update status
                $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
            } else {
                // Update status
                $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED);
            }
            $packageModel->save();
        }

        $response['warnings'] = $warnings;
        $response['complete'] = (empty($warnings) && $cartProductsHash != md5(serialize([]))) ? true : false;
        $this->jsonResponse($response);
    }

    /**
     * Method used for the duplication of packages in the configurator.
     */
    public function duplicatePackageAction()
    {
        $packageId = $this->getRequest()->getParam('packageId');
        $type = $this->getRequest()->getParam('type');
        if ($packageId) {
            try {
                $items = array();
                /** @var Dyna_Checkout_Model_Sales_Quote $quote */
                $quote = $this->_getQuote();

                /** @var Omnius_Package_Model_Package $oldPackageModel */
                $oldPackageModel = $quote->getCartPackage($packageId);
                if ($oldPackageModel->getSaleType() != Dyna_Catalog_Model_ProcessContext::ACQ) {
                    throw new Exception($this->__("Any package with process context different than ACQ should not be duplicated."));
                }

                $newPackageId = Omnius_Configurator_Model_CartObserver::getNewPackageId($quote);

                // gather all quote items for current duplicate package
                $productIds = [];
                /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
                foreach ($quote->getAllItems() as $item) {
                    if ($item->getPackageId() == $packageId && !$item->isPromo() && !$item->getProduct()->isServiceItem()) {
                        $productIds[] = $item->getProductId();
                        array_push($items, array($item->getProductId() => $item));
                    }
                }

                $quote->saveActivePackageId($newPackageId);

                // Create the new package for the quote
                /** @var Omnius_Package_Model_Package $packageModel */
                $newPackage = Mage::getModel('package/package')
                    ->setData($oldPackageModel->getData())
                    ->setCoupon(null)
                    ->setId(null)
                    ->setBundleId(null)
                    ->setPackageId($quote->getActivePackageId())
                    ->save();

                // todo: determine if this is needed anymore
                $serviceNotAllowed = array();

                // building an array of products rejected from duplicating due to incompatibility with current cart
                $skipped = [];
                $addedProductIds = [];
                foreach ($items as $value) {
                    $item = current($value);
                    if ($productIds && in_array(key($value), $serviceNotAllowed)) {
                        $skipped[key($value)] = $item;
                        continue;
                    }

                    /** @var Dyna_Checkout_Helper_Cart $checkoutCartHelper */
                    $checkoutCartHelper = Mage::helper('dyna_checkout/cart');

                    $newitem = $checkoutCartHelper->addProductToCart(
                        key($value),
                        $item->getData('is_defaulted'),
                        $item->getData('is_service_defaulted'),
                        $item->getData('is_obligated'),
                        $item->getData('sale_type') //this is process_context
                    );
                    $newitem->setRemoveRuleId(',');
                    $newitem->setAppliedRuleIds($item->getAppliedRuleIds());

                    $addedProductIds[] = key($value);
                }

                $warningMessage = array();
                foreach ($skipped as $item) {
                    $warningMessage[] = $item->getName();
                }
                $quote->setCouponsPerPackage(array($newPackage->getPackageId() => $oldPackageModel->getCoupon()));

                /** Initially used by FF modules */
                Mage::dispatchEvent('configurator_cart_duplicate_package', [
                    'quote' => $quote,
                    'old_package' => $oldPackageModel,
                    'new_package' => $newPackage
                ]);


                $allExclusiveFamilyCategoryIds = Mage::getModel('catalog/category')->getAllExclusiveFamilyCategoryIds();
                $exclusiveFamilyCategoryIds = [];
                /** @var Dyna_Package_Model_Package $newPackage `*/
                foreach ($newPackage->getItemsByProductId($addedProductIds) as $item) {
                    if ($categoriesExclusive = array_intersect($item->getProduct()->getCategoryIds(), $allExclusiveFamilyCategoryIds)) {
                        foreach ($categoriesExclusive as $categoryExclusive) {
                            $exclusiveFamilyCategoryIds[$item->getProductId()] = $categoryExclusive;
                        }
                    }
                }
                $checkoutCartHelper->parseServiceDefaultedProducts($addedProductIds, $newPackage, $addedProductIds, $allExclusiveFamilyCategoryIds, $exclusiveFamilyCategoryIds);

                $this->_getCart()->save();

                $this->jsonResponse(array(
                    'error'         => false,
                    'warning'         => count($warningMessage) ? $this->__("The following products were skipped from duplicating due to incompatibility with current cart: ") . implode(",", $warningMessage) : "",
                    'packageId'     => $this->_getQuote()->getActivePackageId(),
                    'type'          => $type,
                    'rightBlock'    => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->setCurrentPackageId($this->_getQuote()->getActivePackageId())->toHtml(),
                    'totals'        => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                ));
            } catch (Exception $e) {
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $e->getMessage(),
                ));
            }

        } else {
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'No package id provided',
            ));
        }
    }

    /**
     * Method used to update the parent_id of the current
     * active package on the quote.
     */
    public function updatePackageParentAction()
    {
        $packageId = $this->_getQuote()->getActivePackage()->getPackageId();
        $parentPackageId = $this->getRequest()->getParam('parentPackageId');
        $bundleId = $this->getRequest()->getParam('bundleId');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');

        if ($parentPackageId) {
            try {
                /** @var Dyna_Package_Model_Package $activeRedPlusPackage */
                $activeRedPlusPackage = $quote->getCartPackage($packageId);
                $previousParentPackageId = $activeRedPlusPackage->getParentId();
                /** @var Dyna_Package_Model_Package $newParentPackage */
                $newParentPackage = Mage::getModel('package/package')->load($parentPackageId);

                $bundlesHelper->addBundlePackageEntry($bundleId, $newParentPackage->getEntityId());
                $newParentPackage
                    ->updateBundleTypes()
                    ->save();

                $activeRedPlusPackage->setData('parent_id', $parentPackageId);
                $activeRedPlusPackage->save();

                // Delete the dummy package if it no longer has any children
                if ($previousParentPackageId) {
                    /** @var Dyna_Package_Model_Package $previousParent */
                    $previousParent = $quote->getCartPackageByEntityId($previousParentPackageId);
                    $childrenOfPreviousPackage = $quote->getCartPackagesWithParent($previousParentPackageId);

                    // The dummy has 0 children in cart, delete it
                    if ($previousParent->getEditingDisabled()) {
                        if (count($childrenOfPreviousPackage) == 0 && count($previousParent->getBundles()) <= 1) {
                            $previousParent->delete();
                            $bundlesHelper->removeBundlePackageEntry($previousParent->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS)->getId(), $previousParent->getId());
                        } else {
                            $previousParent
                                ->setRedplusRelatedProduct(null)
                                ->save();
                        }
                    } else {
                        if($previousParent->getSaleType() ===  Dyna_Catalog_Model_ProcessContext::ACQ){
                            $previousParent
                                ->setServiceLineId(null)
                                ->setParentAccountNumber(null)
                                ->save();
                        }

                        if (count($childrenOfPreviousPackage) == 0) {
                            $bundlesHelper->removeBundlePackageEntry($previousParent->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS)->getId(), $previousParent->getEntityId());
                        }
                    }
                }

                // Reindex package ids
                Mage::helper('dyna_configurator/cart')->reindexPackagesIds($quote, $this->_getQuote()->getActivePackageId());
                // We need to save cart to evaluate sales rules
                Mage::getSingleton('checkout/cart')->save();

                $rightBlock = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml();

                $this->jsonResponse(array(
                    'error' => false,
                    'rightBlock' => $rightBlock,
                    'packageId' => $this->_getQuote()->getActivePackageId()
                ));
            } catch (Exception $e) {
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $e->getMessage()
                ));
            }
        } else {
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'No parent package id provided',
            ));
        }
    }

    /**
     * Method used to create a Red+ group with installed base packages
     * If the sharingGroupId is null => the group does not exist. This is a tariff eligible for Red+ Group.
     */
    public function addRedPlusMemberAction()
    {
        try {
            $sharingGroupId = $this->getRequest()->getParam('sharingGroupId');
            $bundleId = $this->getRequest()->getParam('bundleId');
            $subscriptionNumber = $this->getRequest()->getParam('subscriptionNumber');
            $productId = $this->getRequest()->getParam('productId');

            /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
            $bundlesHelper = Mage::helper('dyna_bundles');

            /** @var Dyna_Configurator_Helper_Cart $cartHelper */
            $cartHelper = Mage::helper('dyna_configurator/cart');
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();

            // check if a Kias linked account already exists
            /** @var Dyna_Customer_Model_Session $customerSession */
            $customerSession = Mage::getSingleton('customer/session');
            if(!$customerSession->getKiasLinkedAccountNumber()){
                // if not, set it with the subscription number
                $customerSession->setKiasLinkedAccountNumber($subscriptionNumber);
            }

            $existingGhostPackage = $quote->getGhostPackageForIBSubscription($subscriptionNumber, $productId);

            $mobileCreationTypeId = $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_MOBILE);
            $redplusCreationTypeId = $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS);
            // get package creation type
            $typeModel = Mage::getModel('dyna_package/packageCreationTypes');
            $packageCreationType = $typeModel->getById($redplusCreationTypeId);
            //get permanent filters for the red+ member package
            $filters = $packageCreationType->getFilterAttributes();

            if ($existingGhostPackage) {
                $redPlusPackageId = $cartHelper->initNewPackage(
                    strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE),
                    Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    $redplusCreationTypeId
                );
                $redPlusPackageModel = $quote->getPackageById($redPlusPackageId);

                $redPlusPackageModel
                    ->setParentId($existingGhostPackage->getEntityId())
                    ->setQuoteId($quote->getId())
                    ->setAddedFromMyProducts(1)
                    ->setPermanentFilters($filters)
                    ->save();

                if (!$existingGhostPackage->getSharingGroupId()) {
                    $relatedProductSku = implode($customerSession->getCustomer()->getInstalledBaseProducts($subscriptionNumber, $productId, true));
                    $existingGhostPackageBundleTypes = $existingGhostPackage->getBundleTypes() . ',' . Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS;
                    $existingGhostPackage
                        ->setSharingGroupId($sharingGroupId)
                        ->setBundleTypes($existingGhostPackageBundleTypes)
                        ->setRedplusRelatedProduct($relatedProductSku);
                    $existingGhostPackage->save();
                }
                //if the owner group is already in cart because of an ILS flow(modify package)
                $bundlesHelper->addBundlePackageEntry($bundleId, $existingGhostPackage->getEntityId());
                //bundle entry for member package
                $bundlesHelper->addBundlePackageEntry($bundleId, $redPlusPackageModel->getEntityId());
            } else {
                $packageId = $cartHelper->initNewPackage(
                    strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE),
                    Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    $mobileCreationTypeId
                );
                $packageModel = $quote->getPackageById($packageId);

                $relatedProductSku = implode($customerSession->getCustomer()->getInstalledBaseProducts($subscriptionNumber, $productId, true));

                // Save install base package
                /** @var Dyna_Package_Model_Package $packageModel */
                $packageModel
                    ->setEditingDisabled(true)
                    ->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
                    ->setCreatedAt(now())
                    ->setInitialInstalledBaseProducts($relatedProductSku)
                    ->setInstalledBaseProducts($relatedProductSku)
                    ->setParentAccountNumber($subscriptionNumber)
                    ->setServiceLineId($productId)
                    ->setRedplusRelatedProduct($relatedProductSku)
                    ->setSharingGroupId($sharingGroupId)
                    ->setQuoteId($quote->getId())
                    ->save();


                // Create new Red+ package and set its parent to the above ghost package
                $redPlusPackageId = $cartHelper->initNewPackage(
                    strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE),
                    Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    $redplusCreationTypeId
                );
                $redPlusPackageModel = $quote->getPackageById($redPlusPackageId);

                $redPlusPackageModel
                    ->setParentId($packageModel->getEntityId())
                    ->setQuoteId($quote->getId())
                    ->setAddedFromMyProducts(1)
                    ->setPermanentFilters($filters)
                    ->save();

                $bundlesHelper->addBundlePackageEntry($bundleId, $packageModel->getEntityId());
                $bundlesHelper->addBundlePackageEntry($bundleId, $redPlusPackageModel->getEntityId());
                $packageModel
                    ->updateBundleTypes()
                    ->save();
            }

            /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
            $bundlesHelper = Mage::helper('bundles');
            $eligibleBundles = $bundlesHelper->getFrontendEligibleBundles();

            $this->jsonResponse(
                array(
                    'rightBlock' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml(),
                    'eligibleBundles' => $eligibleBundles,
                    'initNewPackage' => $redPlusPackageModel->getPackageId()
                )
            );
        } catch (Exception $e) {
            $this->jsonResponse(array(
                'error' => true,
                'message' => $e->getMessage()
            ));
        }

    }

    /**
     * TODO method that founds packages in cart that with PROCESS-CONTEXT from installed base and check if the subscription is member from an installed base sharing group @VFDED1W3S-2055
     */
    public function updateCartWithRedPlusMembers()
    {

    }

    /**
     * Method used to create a Red+ group with installed base packages from configurator
     */
    public function addRedPlusMemberFromConfiguratorAction()
    {
        try {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote              = Mage::getSingleton('checkout/cart')->getQuote();
            /** @var Dyna_Configurator_Helper_Cart $cartHelper */
            $cartHelper         = Mage::helper('dyna_configurator/cart');
            $sharingGroupId     = $this->getRequest()->getParam('sharingGroupId');
            $relatedProductSku  = $this->getRequest()->getParam('relatedProductSku');
            $subscriptionNumber = $this->getRequest()->getParam('customerId');
            $productId          = $this->getRequest()->getParam('productId');
            $bundleId           = $this->getRequest()->getParam('bundleId');
            $activeRedPlus      = $quote->getActivePackage();
            $parentPackageId    = $activeRedPlus->getParentId();

            /** @var Dyna_Customer_Model_Session $customerSession */
            $customerSession = Mage::getSingleton('customer/session');
            $customerSession->setKiasLinkedAccountNumber($subscriptionNumber);

            /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
            $bundlesHelper = Mage::helper('dyna_bundles');

            // check if a ghost package has been already created for the current subscription
            $existingGhostPackage = $quote->getGhostPackageForIBSubscription($subscriptionNumber, $productId);
            $mobileCreationTypeId = $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_MOBILE);

            if (!$existingGhostPackage) {
                // ghost package not existing, create it
                $packageId = $cartHelper->initNewPackage(
                    strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE),
                    Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    $mobileCreationTypeId
                );
                $packageModel = $quote->getPackageById($packageId);

                // Save install base package
                /** @var Dyna_Package_Model_Package $packageModel */
                $packageModel
                    ->setEditingDisabled(true)
                    ->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
                    ->setCreatedAt(now())
                    ->setQuoteId($quote->getId())
                    ->setSharingGroupId($sharingGroupId)
                    ->setRedplusRelatedProduct($relatedProductSku)
                    ->setInitialInstalledBaseProducts($relatedProductSku)
                    ->setInstalledBaseProducts($relatedProductSku)
                    ->setServiceLineId($productId)
                    ->setParentAccountNumber($subscriptionNumber)
                    ->save();

                $bundlesHelper->addBundlePackageEntry($bundleId, $packageModel->getEntityId());
                $packageModel->updateBundleTypes()->save();

                // assign the existing redplus one to the new dummy package
                $activeRedPlus
                    ->setParentId($packageModel->getEntityId())
                    ->setQuoteId($quote->getId())
                    ->setParentAccountNumber($subscriptionNumber)
                    ->updateBundleTypes()
                    ->save();

                // set red plus as active package again
                $quote->setActivePackageId($activeRedPlus->getPackageId())->save();
            } else {
                // ghost package exists, assign the existing redplus package to that ghost package
                $activeRedPlus
                    ->setParentId($existingGhostPackage->getEntityId())
                    ->setQuoteId($quote->getId())
                    ->updateBundleTypes()
                    ->save();

                // set red plus as active package again
                $quote->setActivePackageId($activeRedPlus->getPackageId())->save();

                // add bundle entry for existing ghost package
                $bundlesHelper->addBundlePackageEntry($bundleId, $existingGhostPackage->getEntityId());
                $existingGhostPackage->updateBundleTypes()->save();
            }

            // Delete the former dummy package if it no longer has any children
            if ($parentPackageId) {
                /** @var Dyna_Package_Model_Package $previousParent */
                $previousParent = Mage::getModel('package/package')->load($parentPackageId);
                $childrenOfPreviousDummy = $quote->getCartPackagesWithParent($parentPackageId);

                // The dummy has 0 children in cart, delete it
                if ($previousParent->getEditingDisabled()) {
                    if (count($childrenOfPreviousDummy) == 0 && count($previousParent->getBundles()) <= 1) {
                        $bundlesHelper
                            ->removeBundlePackageEntry(
                                $previousParent->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS)->getId(),
                                $previousParent->getId()
                            );
                        $previousParent->delete();
                    } elseif (count($childrenOfPreviousDummy) == 0) {
                        $bundlesHelper->removeBundlePackageEntry(
                            $previousParent->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS)->getId(),
                            $previousParent->getId()
                        );
                        $previousParent->updateBundleTypes()->save();
                    }
                } else {
                     if (count($childrenOfPreviousDummy) == 0) {
                        $bundlesHelper
                            ->removeBundlePackageEntry(
                                $previousParent->getBundleOfType(
                                    Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS)->getId(),
                                $previousParent->getId()
                            );
                        $previousParent
                            ->updateBundleTypes()
                            ->save();
                    }
                }
            }

            // Reindex package ids
            Mage::helper('dyna_configurator/cart')->reindexPackagesIds($quote, $activeRedPlus->getPackageId());
            $eligibleBundles = Mage::helper('bundles')->getFrontendEligibleBundles(true);

            $this->jsonResponse(array(
                    'rightBlock' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml(),
                    // This is needed to tell configurator instance which is the active package id
                    'packageId' => $quote->getActivePackageId(),
                    'eligibleBundles' => $eligibleBundles
                )
            );
        } catch (Exception $e) {
            $this->jsonResponse(array(
                'error' => true,
                'message' => $e->getMessage()
            ));
        }

    }

    /**
     * Method used for adding products to cart.
     */
    public function addMultiAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this;
        }

        if (Mage::getSingleton('customer/session')->getOrderEditMode() && ! Mage::getSingleton('checkout/session')->getIsEditMode()) {
            return $this->jsonResponse(array(
                'error' => true,
                'reload' => true,
                'message' => $this->__('Cannot add items to an already edited and saved quote'),
            ));
        }

        Mage::dispatchEvent('before_add_multi_request', $this->getRequest()->getPost());

        Mage::register('product_add_origin', 'addMulti');

        $initialPackageModel = $this->_getQuote()->getPackageById($this->getRequest()->getPost()['packageId']);

        /** @var Dyna_Checkout_Helper_Cart $checkoutCartHelper */
        $checkoutCartHelper = Mage::helper('dyna_checkout/cart');
        $result = $checkoutCartHelper->processAddMulti($this->getRequest()->getPost());

        $result['clearConfigurator'] = false;
        $currentPackageModel = $this->_getQuote()->getPackageById($this->_getQuote()->getActivePackageId());
        if (!$currentPackageModel->getEntityId() || $initialPackageModel->getEntityId() != $currentPackageModel->getEntityId()) {
            $result['clearConfigurator'] = true;
        }

        return $this->jsonResponse($result);
    }

    /**
     * In ILS scenario, check if the tariff was changed with a new one
     * @param $productIds
     * @return bool
     */
    protected function isIlsTariffChange($productIds)
    {
        foreach ($productIds as $productId) {
            $productModel = Mage::getModel('dyna_catalog/product');
            /** @var Dyna_Catalog_Model_Product $product */
            $product = $productModel->load($productId);
            if ($product->isSubscription()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $productIds
     * @param $type
     * @param $websiteId
     * @param array $ilsProductIds
     * @return array
     * @throws Mage_Core_Exception
     */
    protected function getRules($productIds, $type, $websiteId, $ilsProductIds = [])
    {
        /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
        $rulesHelper = Mage::helper('dyna_configurator/rules');
        Mage::unregister('rules_source_collection');

        if ($this->isIlsTariffChange($productIds) || count($ilsProductIds) == 0) {
            $rules = $rulesHelper->getPreconditionRules($productIds, $type, $websiteId);
        } elseif (count($ilsProductIds)) {
            Mage::register('rules_source_collection', Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_IB);
            $rules = $rulesHelper->getPreconditionRules($ilsProductIds, $type, $websiteId);
            Mage::unregister('rules_source_collection');
        }

        $cablePackages = Dyna_Catalog_Model_Type::getCablePackages();
        // Cable specific service calls
        if (in_array(strtolower($type), $cablePackages)) {
            /** @var Dyna_Configurator_Helper_Services $servicesHelper */
            $servicesHelper = Mage::helper('dyna_configurator/services');

            // InvalidProducts for Cable
            $invalidProducts = $servicesHelper->getInvalidProducts($type, $rules['rules']);
            if (count($invalidProducts)) {
                $rules['invalid'] = $invalidProducts;
            }
        }

        return $rules;
    }

    /**
     * Unset KIAS linked account number from session
     * @param bool $perPackage
     */
    protected function unsetLinkedAccount($perPackage = false)
    {
        $session = Mage::getSingleton('customer/session');
        if($perPackage){
            $quote =  $this->_getQuote();
            $quotePackages = Mage::getModel('package/package')->getPackages(null, $quote->getId());

            $kiasLinkedAccountNumber = $session->getKiasLinkedAccountNumber();
            $kdLinkedAccountNumber = $session->getKdLinkedAccountNumber();
            $kiasPackages = 0;
            $kdPackages = 0;

            foreach($quotePackages as $package){
                if($package->getType() == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)){
                    $kiasPackages++;
                } elseif(in_array($package->getType(),
                    array(
                        strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV),
                        strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE),
                        strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT)
                    )
                )){
                    $kdPackages++;
                }
            }

            if(($kiasPackages == 1) && ($kiasLinkedAccountNumber && ($kiasLinkedAccountNumber != ''))){
                $this->unsetLinkedAccountNumber($session);
            }
            if(($kdPackages == 1) && ($kdLinkedAccountNumber && ($kdLinkedAccountNumber != ''))){
                $this->unsetLinkedAccountNumber($session);
            }
        } else {
            $this->unsetLinkedAccountNumber($session);
        }
    }

    /**
     * Unset linked account from session when there are more than 1 linked customers available
     * @param $session
     */
    protected function unsetLinkedAccountNumber($session)
    {
        $serviceCustomers = Mage::getSingleton('dyna_customer/session');
        $customerProducts = $serviceCustomers->getCustomerProducts();
        $kiasProducts = isset($customerProducts[Dyna_Superorder_Helper_Client::BACKEND_KIAS])
            ? $customerProducts[Dyna_Superorder_Helper_Client::BACKEND_KIAS] : [];

        $kdProducts = isset($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD])
            ? $customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] : [];

        if (count($kiasProducts) > 1) {
            $session->unsKiasLinkedAccountNumber();
        }
        if (count($kdProducts) > 1) {
            $session->unsKdLinkedAccountNumber();
        }
    }

    /**
     * Return the cart helper
     * @return Dyna_Configurator_Helper_Cart|Mage_Core_Helper_Abstract
     */
    protected function getConfiguratorCartHelper()
    {
        if ($this->configuratorCartHelper === false) {
            $this->configuratorCartHelper = Mage::helper('dyna_configurator/cart');
        }

        return $this->configuratorCartHelper;
    }

    /**
     * Checks whether a product already exists in the current package of the shopping cart.
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    protected function exists(Mage_Catalog_Model_Product $product)
    {
        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        foreach ($this->_getQuote()->getItemsCollection() as $quoteItem) {
            if (!$quoteItem->isDeleted() && $quoteItem->getPackageId() == $this->_getQuote()->getActivePackageId() &&
                $quoteItem->getProductId() == $product->getId()) {
                return $quoteItem;
            }
        }
        return false;
    }


    /**
     * Remove a certain bundle from cart
     * @param Dyna_Package_Model_Package $package
     * @param $bundleId
     */
    protected function checkAndRemoveBundle(Dyna_Package_Model_Package $package)
    {
        // get gigakombi bundle for current package
        $bundle = $package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_KOMBI);
        if ($bundle) {
            $bundleId = $bundle->getId();
            /** @var Dyna_Package_Helper_Data $packageHelper */
            $packageHelper = Mage::helper('dyna_package');
            /** @var Dyna_Bundles_Helper_Data $bundleHelper */
            $bundleHelper = Mage::helper('dyna_bundles');
            foreach ($package->getPackagesInSameBundle($bundleId) as $package) {
                // Is a dummy package, let's remove it
                if ($package->getEditingDisabled()) {
                    $packageHelper->removePackage($package->getPackageId());
                    continue;
                }

                // Mark all bundle component items for deletion
                foreach ($package->getAllItems() as $item) {
                    if ($item->getBundleComponent()) {
                        $item->isDeleted(true);
                    }
                }
                $bundleHelper->removeBundlePackageEntry($bundleId, $package->getId());
                $package->updateBundleTypes()->save();
            }
        }
    }

    protected function checkAndRemoveSusoBundle(Dyna_Package_Model_Package $package)
    {
        if ($package->isPartOfSuso()) {
            $packagesInSameBundle = $package
                ->getPackagesInSameBundle(
                    $package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_SUSO)->getId(),
                    true
                );
            $bundlePackage = null;
            if (!$package->isPrepaid()) {
                foreach ($packagesInSameBundle as $packageInBundle) {
                    if ($packageInBundle->isPrepaid()) {
                        $bundlePackage = $packageInBundle;
                        break;
                    }
                }
            } else {
                foreach ($packagesInSameBundle as $packageInBundle) {
                    if (!$packageInBundle->isPrepaid()) {
                        $bundlePackage = $package;
                        $package = $packageInBundle;
                        break;
                    }
                }
            }

            if ($bundlePackage) {
                Mage::helper('dyna_package')->removePackage($bundlePackage->getPackageId());
            } else {
                Mage::throwException('Cannot find the prepaid package in the SurfSofort Bundle');
            }
        }
    }

    /**
     * Checks whether the first available RedPlus bundle
     * will be created with an install base subscription that
     * is already an owner of a group with a reserved member
     */
    public function checkReservedMemberStatusAction()
    {
        /** @var Dyna_Bundles_Helper_Redplus $redPlusHelper */
        $redPlusHelper = Mage::helper('dyna_bundles/redplus');
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');
        $showWarning = false;

        $customerSession = Mage::getSingleton('customer/session');
        $kiasAccountNumber = $customerSession->getKiasLinkedAccountNumber();

        try {
            if ($redPlusHelper->getNumberOfRedPlusEligiblePackagesInCart() == 0) {
                $eligibleBundles = $bundlesHelper->getFrontendEligibleBundles(true);

                foreach ($eligibleBundles as $bundle) {
                    if ($bundle['redPlusBundle']) {
                        foreach ($bundle['targetedSubscriptions'] as $subscription) {
                            if ((!$kiasAccountNumber || $subscription['customerNumber'] == $kiasAccountNumber)
                                && isset($subscription['sharingGroupReservedStatus'])
                                && $subscription['sharingGroupReservedStatus']
                            ) {
                                $showWarning = true;
                                break;
                            }
                        }

                        break;
                    }
                }
            }

            $this->jsonResponse(array(
                'showWarning' => $showWarning
            ));
        } catch (Exception $e) {
            $this->jsonResponse(array(
                'error' => true,
                'message' => $e->getMessage()
            ));
        }
    }

    /**
     * @return Dyna_Cache_Model_Cache|Mage_Core_Model_Abstract
     */
    protected function getCache()
    {
        return Mage::getSingleton('dyna_cache/cache');
    }

    /**
     * Helper method used to return a response as json
     * @param array $data
     * @param int $statusCode Response status code
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        parent::jsonResponse($data, $statusCode);
        if (!$this->getRequest()->isAjax()) {
            $body = $this->getResponse()->getBody();
            $this->getResponse()->setHeader('Content-Type', 'text/html; charset=utf-8');
            $this->getResponse()
                ->setBody($body);
        }
    }

    /**
     * Generic method that returns an error response message
     * @param $message
     * @return array
     */
    protected function getErrorResponse($message)
    {
        return [
            'error' => true,
            'message' => $message,
        ];
    }


    /**
     * Activating a cart quote into configurator:
     * case 1: quote is_offer -> quote is set active
     * case 2: quote is cloned and its status will be updated (removed from customer carts) after finishing order
     */
    public function activateCartAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            if (!($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest())) {
                throw new LogicException($this->__('Method not supported'));
            }
            $params = new Varien_Object($this->getRequest()->getParams());

            /** @var Dyna_Checkout_Helper_Cart $checkoutCartHelper */
            $checkoutCartHelper = Mage::helper('dyna_checkout/cart');

            // Validate requested quote
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            if ((!$quoteId = $params->getData('quote_id')) || !$quote = Mage::getModel('sales/quote')->load($quoteId)) {
                $checkoutCartHelper::throwJsonException($this->__("Trying to load invalid " . ($params->hasData('quote_id') ? "offer" : "cart")));
            }

            /** @var Dyna_Customer_Model_Session $customerSession */
            $customerSession = Mage::getSingleton('customer/session');

            if (!$customerSession->isLoggedIn()) {
                throw new LogicException($this->__('No customer currently logged in'));
            }

            // letting checkout cart helper to do all the job
            // referencing this as this helper needs to apply logic implemented in this class
            list($validationErrors, $newQuote) = $checkoutCartHelper->validateSavedQuote($quote, $this);

            if ($newQuote->isOffer() && empty($validationErrors)) {
                /** @var Dyna_Checkout_Model_Sales_Quote $newQuote */
                $newQuote
                        ->setNeedsRecollectTotals(false)
                        ->setTotalsCollected(true)
                        ->setCurrentStep('overview')
                        ->setIsActive(1);

                $uctParams = Mage::getSingleton('dyna_customer/session')->getUctParams();
                if ($uctParams && !empty($uctParams['transactionId']) && !empty($uctParams['campaignId'])) {
                    $newQuote->setTransactionId($uctParams["transactionId"])
                        ->setCampaignCode($uctParams["campaignId"]);
                } else {
                    $newQuote->setTransactionId(null)
                        ->setCampaignCode(null);
                }
                $newQuote->save();

                $copyMarketingData = false;
                $redirectUrl = '/checkout/cart' . '#voiceLog';
                $packageId = "";
                //marketing data should exist if quote has cable packages
                if ( $checkoutCartHelper->hasCable() ) {
                    $redSalesId = Mage::helper('bundles')->getRedSalesId();
                    //If the offer is loaded by the same agent who created it (having the same SalesID as on the offer),
                    // then they should be able to convert it to order without any other adjustments => marketing fields should be pre-filled
                    if ( $quote->getSalesId() == $redSalesId ) {
                        $copyMarketingData = true;
                    } else {
                        $packageId = $newQuote->getActivePackageId();
                        $redirectUrl = "";
                    }
                }

                $quote->cloneCheckoutFields($newQuote, $copyMarketingData);

                $this->_getCart()->setQuote($newQuote);
                Mage::getSingleton('checkout/session')->setQuoteId($newQuote->getId());
                // if initial products are altered by product expiration or by compatibility rules this offer will result in a new quote with valid products added
                // and current offer will be set to an invalid state (@see OMNVFDE-4616)
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(
                            array(
                                'error' => false,
                                'redirect' => $redirectUrl,
                                'packageId' => $packageId
                            )
                        )
                    );

                return;
            }

            // if reached this point, either quote is offer with validation errors, either it is not an offer
            if ($newQuote->getStoreId() == Mage::app()->getStore(true)->getId()) {
                // no need for pricerules in case offer failed validation as collect totals were already called
                $newQuote
                    ->setIsActive(1)
                    ->setCartStatus(null)
                    // make sure is not an offer
                    ->setIsOffer(null)
                    ->setNeedsRecollectTotals(false)
                    ->setTotalsCollected(true)
                    ->setStoreId(Mage::app()->getStore(true)->getId())
                    ->setProspectSavedQuote(null)
                    ->save();

                $this->_getCart()->setQuote($newQuote);
                Mage::getSingleton('checkout/session')->setQuoteId($newQuote->getId());
                Mage::helper('dyna_configurator/cart')->reindexPackagesIds($newQuote);
                $this->_getCart()->save();

                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(
                            array(
                                'error' => false,
                                'message' => $this->__('Quote successfully loaded'),
                                'packageId' => $newQuote->getActivePackageId()
                            )
                        )
                    );

                if (!empty($validationErrors)) {
                    $checkoutCartHelper::throwSessionException(current($validationErrors));
                }
            } else {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(
                            array('error' => true, 'message' => $this->__('Given quote belongs to another store'))
                        )
                    );
            }
        } catch (Dyna_Checkout_Model_Exception_Json $e) {
            // cannot perform serviceability check for saved offer (probably service down, quote remains as is)
            // cannot load offer due to pending orders, quote remains as is until pending orders are updated or offer expires
            // cannot load offer due to pending orders, quote remains as is until pending orders are updated or offer expires
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $e->getMessage())));
        } catch (Dyna_Checkout_Model_Exception_Session $e) {
            // prices no longer available due to campaign expired
            // cart content altered due to expired products
            // cart content altered by compatibility rules
            Mage::getSingleton('customer/session')->setFrontendMessage($e->getMessage());
        } catch
        (Exception $e) {
            Mage::logException($e);
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $e->getMessage())));
        }
    }

    /**
     * Call the GetNonStandardRates service and apply the retrieved prices
     */
    public function applyNonStandardRatesAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $addressId = Mage::getSingleton('dyna_address/storage')->getAddressId();
        $availableProducts = $this->getRequest()->getParam('availableProducts');
        $packageType = $this->getRequest()->getParam('packageType');

        try {
            /** @var Dyna_Configurator_Helper_Cart $cartHelper */
            $cartHelper = Mage::helper('dyna_configurator/cart');
            $rightBlock = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml();

            $response = array(
                'nonStandardRates' => $cartHelper->getNonStandardRates(strtolower($packageType), $addressId, $availableProducts),
                'rightBlock' => $rightBlock,
                'activePackageId' => $this->_getQuote()->getActivePackageId()
            );

            $this->jsonResponse($response);
        } catch (Exception $e) {
            Mage::logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__($e->getMessage())
            ));
        }
    }

    /**
     * The action used for creating single or multiple packages in the configurator.
     */
    public function createPackagesAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        try {
            /** @var Dyna_Configurator_Helper_Cart $cartHelper */
            $cartHelper = $this->getCartHelper();

            $packages = $this->getRequest()->getParam('packages');
            $saleType = $this->getRequest()->getParam('saleType');
            $ctn = $this->getRequest()->getParam('ctn');
            $offerId = $this->getRequest()->getParam('offerId');

            foreach ($packages as $package) {
                $newPackageId = $cartHelper->initNewPackage(
                    strtolower($package['type']),
                    $saleType,
                    $ctn,
                    $package['currentProducts'],
                    null,
                    null,
                    null,
                    null,
                    $package['packageCreationTypeId'],
                    $offerId
                );

                $currentProductsIds = explode(',', $package['currentProducts']);
                $this->_getCart()->getQuote()->setTotalsCollectedFlag(true);
                foreach ($this->_getCart()->getQuote()->getAllAddresses() as $address) {
                    $address->unsetData('cached_items_nonnominal');
                }
                Mage::helper('dyna_checkout/cart')->processAddMulti([
                    'type' => $package['type'],
                    'section' => 'tariff',
                    'packageId' => $newPackageId,
                    'products' => $currentProductsIds,
                    'clickedItemSelected' => true
                ]);
            }

            $this->_getCart()->getQuote()->setTotalsCollectedFlag(false);
            $this->_getCart()->save();

            $response = array(
                'error' => false,
                'packageId' => $newPackageId
            );

            Mage::helper('dyna_configurator/UCT')->exitUctFlow();
            $this->jsonResponse($response);
        } catch (Exception $e) {
            Mage::logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__($e->getMessage())
            ));
        }
    }
}
