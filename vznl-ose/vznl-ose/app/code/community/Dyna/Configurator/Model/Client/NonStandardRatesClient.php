<?php
class Dyna_Configurator_Model_Client_NonStandardRatesClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "non_standard_rate/wsdl_non_standard_rate";
    const ENDPOINT_CONFIG_KEY = "non_standard_rate/endpoint_non_standard_rate";
    const CONFIG_STUB_PATH = 'non_standard_rate/use_stubs';

    /**
     * VFDEGetConfigurationDataRequest
     * @param $params
     * @return mixed
     */
    public function executeGetNonStandardRates($params = array())
    {
        $filters = $root = [];
        foreach ($params['products'] as $data) {
            foreach ($data as $contractCode => $products) {
                $filters[] = [
                    'ContractCode' => $contractCode,
                    'ProductCodes' => implode(', ', $products),
                ];
            }
        }

        $callParams['AddressId'] = $params['addressId'];
        $callParams['Filters'] = $filters;

        $this->setOgwHeaderInfo($root, 'GetNonStandardRates');
        $root['ROOT']['DATA'] = $callParams;
        $values = $this->GetNonStandardRates($root);

        return $values['ROOT']['DATA'];
    }
}
