<?php

/**
 * Class Dyna_Configurator_Model_Client_ConfigurationDataClient
 */
class Dyna_Configurator_Model_Client_ConfigurationDataClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "get_configuration_data/wsdl_get_configuration_data";
    const ENDPOINT_CONFIG_KEY = "get_configuration_data/endpoint_get_configuration_data";
    const CONFIG_STUB_PATH = "get_configuration_data/use_stubs";

    /**
     * VFDEGetConfigurationDataRequest
     * @param $params
     * @return mixed
     */
    public function executeGetConfigurationData($params = array())
    {
        $root = array();
        $this->setOgwHeaderInfo($root, 'VFDEGetConfigurationData');
        $root['ROOT']['DATA'] = $params;
        $values = $this->VFDEGetConfigurationData($root);

        return $values['ROOT']['DATA'];
    }
}
