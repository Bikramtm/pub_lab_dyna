<?php

/**
 * Class Dyna_Configurator_Model_Client_IpEquipmentFeaturesClient
 */
class Dyna_Configurator_Model_Client_IpEquipmentFeaturesClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "ip_equipment_features/wsdl_ip_equipment_features";
    const ENDPOINT_CONFIG_KEY = "ip_equipment_features/endpoint_ip_equipment_features";
    const CONFIG_STUB_PATH = 'ip_equipment_features/use_stubs';

    /**
     * Get the features of IP equipment using the type information and the mac address of the equipment (cable modems) or its order job code.
     * Necessary for checking the compatibility between components (equipment, products) in an order.
     * This resource is not suitable for SmartCards and TV equipment.
     * @param $params
     * @return mixed
     */
    public function executeGetIpEquipmentFeatures($params = array())
    {
        if (empty($params['JobCode']) && (empty($params['Manufacturer']) || empty($params['ConverterType']) || empty($params['MacAddress']))) {
            Mage::logException(new Exception("No JobCode or (Manufacturer + ConverterType + MacAddress) received for GetIpEquipmentFeatures service call."));
        }
        $root = [];

        $this->setOgwHeaderInfo($root, 'GetIpEquipmentFeatures');

        $jobCode = !empty($params['JobCode']) ? $params['JobCode'] : "no-job-code";
        $root['ROOT']['DATA'] = $this->mapEquipmentParams($params);

        $values = $this->GetIpEquipmentFeatures($root);

        return array(
            'job_code' => $jobCode,
            'manufacturer' => !empty($values['Manufacturer']) ? $values['Manufacturer'] : null,
            'converter_type' => !empty($values['ConverterType']) ? $values['ConverterType'] : null,
            'mac_address' => !empty($values['MacAddress']) ? $values['MacAddress'] : null,
            'equipment_ownership' => !empty($values['EquipmentOwnership']) ? $values['EquipmentOwnership'] : null,
            'equipment_model_type' => !empty($values['EquipmentModelType']) ? $values['EquipmentModelType'] : null,
            'max_bandwidth' => !empty($values['MaxBandwidth']) ? $values['MaxBandwidth'] : null,
            'supports_wlan' => !empty($values['SupportsWlan']) ? $values['SupportsWlan'] : null,
            'supports_homespot' => !empty($values['SupportsHomespot']) ? $values['SupportsHomespot'] : null,
            'supports_voice' => !empty($values['SupportsVoice']) ? $values['SupportsVoice'] : null,
        );
    }

    /**
     * Either JobCode or (Manufacturer + ConverterType + MacAddress) can be present
     * @param $params
     * @return mixed
     */
    public function mapEquipmentParams($params)
    {
        $params['JobCode'] = $params['JobCode'] ?? null;
        $params['Manufacturer'] = $params['Manufacturer'] ?? null;
        $params['ConverterType'] = $params['ConverterType'] ?? null;
        $params['MacAddress'] = $params['MacAddress'] ?? null;

        return $params;
    }
}
