<?php

/**
 * Class used by the Compatibility rules for service expression conditions evaluation
 *
 * Class Dyna_Configurator_Model_Expression_Cart
 */
class Dyna_Configurator_Model_Expression_Cart
{
    /** @var Dyna_Configurator_Helper_Expression */
    protected $helper = null;
    /** @var null| Dyna_Configurator_Model_Expression_Package */
    public $package = null;
    /** @var null| Dyna_Configurator_Model_Expression_Package */
    public $parentPackage = null;

    /** @var Dyna_Cache_Model_Cache */
    protected $dynaCache = null;

    public function __construct()
    {
        $this->helper = Mage::helper('dyna_configurator/expression');

        // Instantiate the active package so that it can be accessed like: cart.package.methodName()
        $this->package = Mage::getModel('dyna_configurator/expression_package');
        $this->parentPackage = Mage::getModel('dyna_configurator/expression_package');

        // Set an empty package to prevent checking of package existence
        $this->package->setPackage(Mage::getModel('dyna_package/package'));
        $this->parentPackage->setPackage(Mage::getModel('dyna_package/package'));

        $activePackage = $this->helper->getActivePackage();
        if ($activePackage) {
            // Update the package instance with the active one
            $this->package->setPackage($activePackage);
            if ($activePackage->getParentId()) {
                // Update the parentPackage instance with the parent package of the active one
                $this->parentPackage->setPackage(Mage::getModel('dyna_package/package')->load($activePackage->getParentId()));
            }
        }
    }

    /**
     * Checks if the entire cart contains a product in a specific category
     * @param string $categoryName
     * @return bool
     */
    public function containsProductInCategory($categoryName)
    {
        return $this->helper->containsProductInCategory($categoryName);
    }

    /**
     * Checks if the entire cart contains a product in a specific category
     * @param string $categoryName
     * @return bool
     */
    public function containsProductInCategoryId($id)
    {
        return $this->helper->containsProductInCategoryId($id);
    }


    /**
     * Checks if the active package in the cart contains a product in a specific category
     * @param string $categoryName
     * @return bool
     */
    public function packageContainsProductInCategory($categoryName)
    {
        return $this->helper->packageContainsProductInCategory($categoryName);
    }

    /**
     * Checks if the active package in the cart contains a product in a specific category
     * @param string $categoryName
     * @return bool
     */
    public function packageContainsProductInCategoryId($id)
    {
        return $this->helper->packageContainsProductInCategoryId($id);
    }

    /**
     * Checks if the active package contains a specific product
     * @param string $sku 
     * @return bool
     */
    public function packageContainsProduct($sku)
    {
       return $this->helper->packageContainsProduct($sku);
    }

    /**
     * Checks if the active package contains an install base product
     * @param string $sku
     * @return bool
     */
    public function packageContainsInstalledBaseProduct($sku)
    {
        return $this->helper->packageContainsIBProduct($sku);
    }

    /**
     * Checks if the active package contained an install base product
     * @param string $sku
     * @return bool
     */
    public function packageContainedInstalledBaseProduct($sku)
    {
        return $this->helper->packageContainedIBProduct($sku);
    }

    /**
     * Check if cart contained at least one product in a specific category
     * @param $categoryPath
     * @return bool
     */
    public function packageContainedInstalledBaseCategory($categoryPath)
    {
        return $this->helper->packageContainedIBCategory($categoryPath);
    }

    /**
     * Check if cart contained at least one product in a specific category
     * @param $categoryPath
     * @return bool
     */
    public function packageContainedInstalledBaseCategoryId($id)
    {
        return $this->helper->packageContainedIBCategoryId($id);
    }

    /**
     * Check if cart contains at least one product in a specific category
     * @param $categoryPath
     * @return bool
     */
    public function packageContainsInstalledBaseCategory($categoryPath)
    {
        return $this->helper->packageContainsIBCategory($categoryPath);
    }

    /**
     * Check if cart contains at least one product in a specific category
     * @param $categoryPath
     * @return bool
     */
    public function packageContainsInstalledBaseCategoryId($id)
    {
        return $this->helper->packageContainsIBCategoryId($id);
    }

    /**
     * Checks if the cart contains a specific product
     * @param string $sku
     * @return bool
     */
    public function containsProduct($sku)
    {
        return $this->helper->containsProduct($sku);
    }

    /**
     * Use this method to set another helper than the real helper for evaluating expressions before actually adding something to cart
     * @param $helper
     */
    public function setBundlesHelper($helper)
    {
        $this->helper = $helper;

        return $this;
    }

    /**
     * Return the number of products in a certain category
     * @param $categoryName
     * @return int
     */
    public function numberOfProductsInCategory($categoryName)
    {
        return $this->helper->numberOfProductsInCategory($categoryName);
    }

    /**
     * Return the number of products in a certain category
     * @param $categoryName
     * @return int
     */
    public function numberOfProductsInCategoryId($id)
    {
        return $this->helper->numberOfProductsInCategoryId($id);
    }

    /**
     * Determine whether or not current active package contains related product
     * @return bool
     */
    public function packageContainsRelatedProduct()
    {
        return $this->helper->packageContainsRelatedProduct();
    }

    /**
     * Determine whether or not the active package contains a product in a certain category
     * @param $categoryName
     * @return bool
     */
    public function packageRelatedProductInCategory($categoryName)
    {
        return $this->helper->packageRelatedProductInCategory($categoryName);
    }


    /**
     * Determine whether or not the active package contains a product in a certain category
     * @param $categoryName
     * @return bool
     */
    public function packageRelatedProductInCategoryId($id)
    {
        return $this->helper->packageRelatedProductInCategoryId($id);
    }


    /**
     * Get the product the active package related product
     * @return Dyna_Checkout_Model_Sales_Quote_Item
     */
    public function getPackageRelatedProduct()
    {
        return $this->helper->getPackageRelatedProduct();
    }

    /**
     * Determine the number of packages in cart that have as parent a certain quote item package
     * @return bool
     */
    public function maxNumberOfProductsWithParent(array $item, $maxValue)
    {
        return $this->helper->maxNumberOfProductsWithParent($item, $maxValue);
    }

    /**
     * Determine whether or not active package in cart is of a certain type
     * @param $type
     * @return bool
     */
    public function packageType($type)
    {
        return $this->helper->packageType($type);
    }

    /**
     * Determine whether or not active package in cart is of a certain package creation type
     * @param $type
     * @return bool
     */
    public function packageCreationTypeId($type)
    {
        return $this->helper->packageCreationTypeId($type);
    }

    /**
     * Method that returns the number of packages with a certain install base customer number / product id combination
     * @see It sums the install base packages (members)
     * @param int $maxValue
     * @return int
     * @stores combination of customer number and product id in mage registry
     */
    public function maxNumberOfProductsWithIBParent($maxValue)
    {
        return $this->helper->maxNumberOfProductsWithIBParent($maxValue);
    }

    /**
     * Method that stores the required products in a register in order do determine the targeted sections in configurator
     * @param $productSku
     * @return true
     */
    public function requiresProduct($productSku)
    {
        $stack = Mage::registry(Dyna_Bundles_Helper_Data::EXPRESSION_REQUIRED_PRODUCTS);
        //  not found in registry, starting new one
        if ($stack === null) {
            $stack = array();
        } else {
            // found in registry, clearing it for no exception to be thrown
            Mage::unregister(Dyna_Bundles_Helper_Data::EXPRESSION_REQUIRED_PRODUCTS);
        }

        $stack[] = $productSku;

        Mage::register(Dyna_Bundles_Helper_Data::EXPRESSION_REQUIRED_PRODUCTS, $stack);

        // doesn't evaluate, just sets values
        return true;
    }

    /**
     * Determine whether or not current cart contains a certain package type
     * @param string $packageCode
     * @return bool
     */
    public function containsPackageCreationType($packageCode)
    {
        return $this->helper->containsPackageCreationType($packageCode);
    }

    /**
     * Determine whether or not current cart contains a certain package type id
     * @param string $packageCode
     * @return bool
     */
    public function containsPackageCreationTypeId($packageCode)
    {
        return $this->helper->containsPackageCreationTypeId($packageCode);
    }

    /**
     * @param array $productIds
     * @return $this
     */
    public function appendCurrentProducts(array $productIds)
    {
        $this->helper->injectActiveProducts($productIds);

        return $this;
    }

    /**
     * Update the instance of the active package in cart object
     * @param $quote
     * @return $this
     */
    public function updateCurrentPackages(Dyna_Checkout_Model_Sales_Quote $quote)
    {
        $this->helper->updateInstance($quote);

        $this->package = Mage::getModel('dyna_configurator/expression_package');
        $this->parentPackage = Mage::getModel('dyna_configurator/expression_package');

        $activePackage = $quote->getActivePackage();

        if ($activePackage) {
            // Update the package instance with the active one
            $this->package->setPackage($activePackage);
            if ($activePackage->getParentId()) {
                // Update the parentPackage instance with the parent package of the active one
                $this->parentPackage->setPackage(Mage::getModel('dyna_package/package')->load($activePackage->getParentId()));
            }
        }

        return $this;
    }

    /**
     * Determine whether or not cart contained a certain product (@see VFDED1W3S-869)
     * @param $productSku
     * @return bool
     */
    public function containedProduct($productSku)
    {
        return $this->helper->containedProduct($productSku);
    }

    /**
     * Determine whether or not cart contained a product in a certain category
     * @param $categoryPath
     * @return bool
     */
    public function containedProductInCategory($categoryPath)
    {
        return $this->helper->containedProductInCategory($categoryPath);
    }


    /**
     * Determine whether or not cart contained a product in a certain category
     * @param $categoryPath
     * @return bool
     */
    public function containedProductInCategoryId($id)
    {
        return $this->helper->containedProductInCategoryId($id);
    }

    /**
     * Notify agent with a certain message
     * @param $message
     * @return bool
     */
    public function showMessage($message)
    {
        return $this->helper->showMessage($message);
    }

    /**
     * @param Mage_SalesRule_Model_Rule $rule
     */
    public function setRule(Mage_SalesRule_Model_Rule $rule)
    {
        $this->helper->setRule($rule);
    }

    /**
     * Determine whether or not current package contained a product in a certain family
     * @param string $familyIdentifier
     * @return bool
     */
    public function packageContainedInstalledBaseProductInFamily($familyIdentifier)
    {
        return $this->helper->packageContainedInstalledBaseProductInFamily($familyIdentifier);
    }

    /**
     * Determine whether or not current package contains a product in a certain family
     * @param string $familyIdentifier
     * @return bool
     */
    public function packageContainsInstalledBaseProductInFamily($familyIdentifier)
    {
        return $this->helper->packageContainsInstalledBaseProductInFamily($familyIdentifier);
    }
}
