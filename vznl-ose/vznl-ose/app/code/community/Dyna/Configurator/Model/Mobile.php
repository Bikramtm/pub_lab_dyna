<?php

/**
 * Class Dyna_Configurator_Model_Mobile
 */
class Dyna_Configurator_Model_Mobile extends Dyna_Configurator_Model_Catalog
{
    /**
     * Checks what addon families are available based on the package type, current products in cart and website.
     * @param $type
     * @param array $allProducts
     * @param null $websiteId
     * @return mixed
     */
    public function processFamilies($type, &$allProducts = array(), $websiteId = null)
    {
        $websiteId = $websiteId ?: Mage::app()->getWebsite()->getId();
        $productsKey = md5(serialize($allProducts));
        $key = serialize(array(__METHOD__, $websiteId, $type, $productsKey));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $productIds = array();
            $categoryIds = array();
            $mandatoryCategories = array();

            foreach ($allProducts as $section) {
                $productIds = array_merge($productIds, array_column($section, 'entity_id'));
            }

            if ($productIds) {
                /** @var Magento_Db_Adapter_Pdo_Mysql $connection */
                $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
                $query = "select category_id from catalog_category_product where product_id in (" . implode(',', $productIds) . ")";
                $categoryIds = $connection->fetchAll($query, array(), Zend_Db::FETCH_COLUMN);
            }

            if ($categoryIds) {
                $mandatoryCategories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addFieldToFilter('entity_id', array('in' => $categoryIds))
                    ->addFieldToFilter('is_family', 1)
                    ->addFieldToFilter('is_active', array('eq' => '1'))
                    ->setOrder('family_sort_order', Varien_Data_Collection::SORT_ORDER_ASC);
            }
            $categories = new Varien_Data_Collection();

            if ($mandatoryCategories) {
                foreach ($mandatoryCategories as $cat) {
                    $categories->addItem($cat);
                }
            }

            $result['families'] = $categories->toArray();
            $result['products'] = $allProducts;

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }
}
