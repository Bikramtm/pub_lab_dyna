<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_Configurator_Model_Expression_ActiveInstallBase extends Varien_Object
{
    /** @var Dyna_Checkout_Model_Sales_Quote */
    protected $quote = null;
    /** @var Dyna_Package_Model_Package */
    protected $activePackage = null;
    /** @var Dyna_Customer_Model_Customer */
    protected $customer = null;
    /** @var array */
    protected $installBaseProduct = null;
    /** @var Dyna_Configurator_Helper_Expression */
    protected $helper = null;

    public function _construct()
    {
        // reference current quote
        $this->quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if ($customer->isCustomerLoggedIn() && ($package = $this->quote->getCartPackage()) && ($parentAccountNumber = $package->getParentAccountNumber()) && ($serviceLineId = $package->getServiceLineId())) {
            $this->activePackage = $package;
            $this->customer = $customer;
            $this->installBaseProduct = $this->customer->getInstalledBaseProduct($parentAccountNumber, $serviceLineId);
            $this->helper = Mage::helper('dyna_configurator/expression', "->");
        }
    }

    public function containsProductInCategory($categoryPath, $notExpression = false)
    {
        if ($this->customer && $this->activePackage) {
            return $this->helper->containsProductInCategory($categoryPath, $notExpression);
        }

        return false;
    }

    public function containsProductInCategoryId($id, $notExpression = false)
    {
        if ($this->customer && $this->activePackage) {
            return $this->helper->packageContainsProductInCategoryId($id, $notExpression);
        }

        return false;
    }

    public function containsProduct($productSku, $notExpression = false)
    {
        if ($this->customer && $this->activePackage) {
            return $this->helper->packageContainsProduct($productSku, $notExpression);
        }

        return false;
    }

    public function numberOfProductsInCategory($categoryPath)
    {
        if ($this->customer && $this->activePackage) {
            return $this->helper->packageNumberOfProductsInCategory($categoryPath);
        }

        return false;
    }

    /**
     * Method that determines for current customer  how many products has in his install base from a certain category
     * @param $categoryPath
     * @return int
     */
    public function numberOfProductsInCategoryId($id)
    {
        if ($this->customer && $this->activePackage) {
            return $this->helper->packageNumberOfProductsInCategoryId($id);
        }

        return false;
    }

    public function subscriptionContainsProduct($productSku)
    {
        if ($this->customer && $this->activePackage) {
            return $this->containsProduct($productSku);
        }

        return false;
    }
}
