<?php

/**
 * Class Dyna_Configurator_Model_Client_SmartCardCompatibilityClient
 */
class Dyna_Configurator_Model_Client_SmartCardCompatibilityClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "smart_card_compatibility/wsdl_smart_card_compatibility";
    const ENDPOINT_CONFIG_KEY = "smart_card_compatibility/endpoint_smart_card_compatibility";
    const CONFIG_STUB_PATH = 'smart_card_compatibility/use_stubs';

    /**
     * GetSmartCardCompatibility
     * @param $params
     * @return mixed
     */
    public function executeGetSmartCardCompatibility($params = array())
    {
        $root = array();
        $this->setOgwHeaderInfo($root, 'GetSmartCardCompatibility');
        $root['ROOT']['DATA'] = $params;

        return $this->GetSmartCardCompatibility($root);
    }
}
