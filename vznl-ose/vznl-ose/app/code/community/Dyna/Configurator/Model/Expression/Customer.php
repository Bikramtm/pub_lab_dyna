<?php

/**
 * Class used by the Compatibility rules for service expression conditions evaluation
 *
 * Class Dyna_Configurator_Model_Expression_Customer
 */
class Dyna_Configurator_Model_Expression_Customer extends Varien_Object
{
    public $isSoho = 0;
    /** @var Dyna_Customer_Model_Customer */
    protected $customer = null;
    protected $connectionTypes = array();
    protected $directoryEntryTypes = array();
    protected $customerSubscriptions = null;
    protected $customerComponents = null;

    public $credit = null;

    public function _construct()
    {
        $this->customer = Mage::getSingleton('customer/session')
            ->getCustomer();

        $this->credit = Mage::getModel('dyna_configurator/expression_creditCheck');

        $this->isSoho = (int)$this->customer->getIsSoho();
    }

    /**
     * Determine whether or not current customer has a certain connection type
     * @param $connectionType
     * @return bool
     */
    public function hasConnectionType($connectionType)
    {
        // If not already checked, go through customer's subscriptions
        if (!isset($this->connectionTypes[$connectionType])) {
            foreach ($this->getComponents() as $component) {
                if (isset($component["connectionType"])) {
                    // For this particular connection type setting true or false after iterating through all customer additional components data
                    $this->connectionTypes[$connectionType] = (strtolower($component["connectionType"]) === strtolower($connectionType));
                }
            }
        }

        if (!isset($this->connectionTypes[$connectionType])) {
            $this->connectionTypes[$connectionType] = false;
        }

        return $this->connectionTypes[$connectionType];
    }

    /**
     * Determine whether or not current customer has a certain directory entry type
     * @param $type
     * @return bool
     */
    public function hasDirectoryEntryType($type)
    {
        // If not already checked, go through customer's subscriptions components
        if (!isset($this->directoryEntryTypes[$type])) {
            foreach ($this->getComponents() as $component) {
                if (isset($component["directoryEntryType"])) {
                    // For this particular directory entry type setting true or false after iterating through all customer additional components data
                    $this->directoryEntryTypes[$type] = (strtolower($component["directoryEntryType"]) === strtolower($type));
                }
            }
        }

        if (!isset($this->directoryEntryTypes[$type])) {
            $this->directoryEntryTypes[$type] = false;
        }

        return $this->directoryEntryTypes[$type];
    }



    /**
     * Getting components for current customer
     * @return array|null
     */
    protected function getComponents()
    {
        if (is_null($this->customerComponents)) {
            $this->customerComponents = array();
            $customers = Mage::getSingleton('customer/session')->getServiceCustomers();
            foreach ($customers as $customerPerAccountType) {
                foreach ($customerPerAccountType as $customer) {
                    foreach ($this->getArrayNode($customer, "contracts") as $contract) {
                        foreach ($this->getArrayNode($contract, 'subscriptions') as $subscription) {
                            // Let parse additional components to fill in components on current instance
                            $this->parseAdditionalComponents($subscription);
                        }
                    }
                }
            }
        }

        return $this->customerComponents;
    }

    /**
     * Get all components for current customer into one big component list
     * @param $subscription
     * @return $this
     */
    protected function parseAdditionalComponents($subscription)
    {
        foreach ($this->getArrayNode($subscription, 'products') as $product) {
            foreach ($this->getArrayNode($product, "components") as $component) {
                foreach ($this->getArrayNode($component, "ComponentAdditionalData") as $additionalData) {
                    if (!empty($additionalData["Code"]) &&!empty($additionalData["Value"])) {
                        $this->customerComponents[] = array(
                            $additionalData["Code"] => $additionalData["Value"]
                        );
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Get a certain value from an array if key exists
     * @param $array
     * @param $node
     * @return array
     */
    protected function getArrayNode($array, $node)
    {
        return !empty($array[$node]) ? $array[$node] : [];
    }

    /**
     * Determine whether or not current customer is of type soho or not
     * @return bool
     */
    public function isSoho()
    {
        return (bool)$this->isSoho;
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return (bool) $this->customer->isCustomerLoggedIn();
    }

    /**
     * @param $inferiorLimit
     * @param $superiorLimit
     * @return bool
     */
    public function ageBetween($inferiorLimit, $superiorLimit)
    {
        if ((bool) $this->customer->isCustomerLoggedIn()) {
            if ($this->customer->getDob()) {
                $dobDate = new DateTime($this->customer->getDob());
                $curDate = new DateTime(date("Y-m-d"));
                $curAge = $curDate->diff($dobDate)->format('%y');
                return (bool) ($curAge >= $inferiorLimit && $curAge <= $superiorLimit);
            }
            return false;
        }

        return false;
    }

    /**
     * Returns current logged in customer age
     * @return bool|string
     */
    public function getAge()
    {
        if ((bool) $this->customer->isCustomerLoggedIn()) {
            if ($this->customer->getDob()) {
                $dobDate = new DateTime($this->customer->getDob());
                $curDate = new DateTime(date("Y-m-d"));
                $curAge = $curDate->diff($dobDate)->format('%y');
                return $curAge;
            }
            return false;
        }

        return false;
    }

    /**
     * Get any info from retrieve customer info response
     * @param $xpath
     * @return mixed
     */
    public function getByDotPath($xpath, $mode = 1)
    {
        if ($xpath) {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            if ($activePackage = $quote->getCartPackage()) {
                return $activePackage->getServiceValue("RetrieveCustomerInfo", $xpath, $mode);
            }
        }

        return null;
    }

    /**
     * Check if product with sku is in contract period
     * @param string $sku
     * @return bool
     */
    public function checkProductIsInContractMonth($sku)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        /** @var Dyna_Package_Model_Package $activePackage */
        $activePackage = $quote->getActivePackage();
        if ($activePackage) {
            foreach ($this->customer->getAllInstallBaseProducts() as $subscription) {
                if ($subscription['contract_id'] == $activePackage->getParentAccountNumber() && $subscription['product_id'] == $activePackage->getServiceLineId()) {
                    foreach ($subscription['products'] as $product) {
                        if ($product['product_sku'] == $sku) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check if tariff is in contract period
     * @return bool
     */
    public function checkTariffIsInContractMonth()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        /** @var Dyna_Package_Model_Package $activePackage */
        $activePackage = $quote->getActivePackage();
        foreach ($activePackage->getAllItems() as $item) {
            if (strtolower($item->getProduct()->getType()) == strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION)) {
                return $this->checkProductIsInContractMonth($item->getSku());
            }
        }

        return false;
    }


    /**
     * Return the contract month of IB tariff based on RetrieveCustomerInfo service call
     * @return integer
     */
    public function tariffIsInMonth()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        /** @var Dyna_Package_Model_Package $activePackage */
        $activePackage = $quote->getActivePackage();
        if ($activePackage) {
            foreach ($this->customer->getAllInstallBaseProducts() as $subscription) {
                if ($subscription['contract_id'] != $activePackage->getParentAccountNumber() || $subscription['product_id'] != $activePackage->getServiceLineId()) {
                    continue;
                }

                foreach ($subscription['products'] as $product) {
                    if ($product['product_package_subtype'] == strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION)) {
                        return (int)$subscription['actual_month_of_contract'];
                    }
                }
            }
        }

        return 0;
    }
}
