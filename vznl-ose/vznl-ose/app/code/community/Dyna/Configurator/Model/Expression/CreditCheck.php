<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_Configurator_Model_Expression_CreditCheck extends Varien_Object
{
    /**
     * Get any info from retrieve customer info response
     * @param $xpath
     * @return mixed
     */
    public function getByDotPath($xpath)
    {
        if ($xpath) {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            if ($activePackage = $quote->getCartPackage()) {
                return $activePackage->getServiceValue("RetrieveCustomerCreditProfile", $xpath);
            }
        }

        return null;
    }
}
