<?php

/**
 * Used in service source expression for evaluating customers installedBase
 * Class Dyna_Configurator_Model_Expression_InstallBase
 */

class Dyna_Configurator_Model_Expression_InstallBase extends Varien_Object
{
    CONST CATEGORY_PATH_SEPPARTOR = "/";
    // used for determining whether or not an expression targets install base or not
    CONST INSTALL_BASE_EXPRESSION_STRING = "installBase";

    protected $cache = array();
    protected $categoryCache = array();
    protected $customerLoggedIn = false;
    protected $installBaseProducts = array();
    protected $installBaseProductsForRedPlus = array();
    protected $relatedProducts = array();

    // used in bundle evaluation
    protected $inMigrationState = null;
    protected $migrationCustomerNumber = null;
    protected $migrationProductId = null;

    /** @var Dyna_Bundles_Helper_Data */
    protected $bundlesHelper = null;
    /** @var Dyna_Configurator_Helper_Expression */
    protected $expressionHelper = null;

    /** @var Dyna_Checkout_Model_Sales_Quote */
    protected $quote = null;

    /**
     * Constructor method
     */
    public function _construct()
    {
        /** @var Dyna_Customer_Model_Customer $currentCustomer */
        $currentCustomer = Mage::getSingleton('customer/session')
            ->getCustomer();

        // used for getting static constant (and not directly through class call for magento overriding purposes)
        $this->bundlesHelper = Mage::helper('dyna_bundles');

        // get reference to quote
        $this->quote = Mage::getSingleton('checkout/cart')->getQuote();

        // setting migration data on current instance
        $this->inMigrationState = Mage::getSingleton('checkout/cart')->getCheckoutSession()->getInMigrationOrMoveOffnetState();
        // setting customer install base product on current instance
        $migrationData = explode(",", Mage::getSingleton('checkout/cart')->getCheckoutSession()->getMigrationOrMoveOffnetCustomerNo());
        $this->migrationCustomerNumber = $migrationData[0] ?? null;
        $this->migrationProductId = $migrationData[1] ?? null;

        if ($currentCustomer->isCustomerLoggedIn()) {
            $this->customerLoggedIn = true;
            $allInstallBaseProducts = $currentCustomer->getAllInstallBaseProducts();
            foreach ($allInstallBaseProducts as $customerProduct) {
                if ($this->canBeBundled($customerProduct, $allInstallBaseProducts)) {
                    $this->installBaseProducts[] = $customerProduct;
                }
                $this->installBaseProductsForRedPlus[] = $customerProduct;
            }
        }

        $this->expressionHelper = Mage::helper('dyna_bundles/expression');
    }

    /**
     * @param Dyna_Bundles_Helper_Data $bundleHelper
     */
    public function setBundlesHelper(Dyna_Bundles_Helper_Data $bundleHelper)
    {
        $this->bundlesHelper = $bundleHelper;
    }

    public function removeLinkedAccountsFilter()
    {
        /** @var Dyna_Customer_Model_Customer $currentCustomer */
        $currentCustomer = Mage::getSingleton('customer/session')
            ->getCustomer();

        $this->installBaseProducts = [];
        $this->installBaseProductsForRedPlus = [];

        if ($currentCustomer->isCustomerLoggedIn()) {
            $this->customerLoggedIn = true;
            $allInstallBaseProducts = $currentCustomer->getAllInstallBaseProducts(false);
            foreach ($allInstallBaseProducts as $customerProduct) {
                if ($this->canBeBundled($customerProduct, $allInstallBaseProducts)) {
                    $this->installBaseProducts[] = $customerProduct;
                }
                $this->installBaseProductsForRedPlus[] = $customerProduct;
            }
        }
    }

    /**
     * Determine whether or not a certain product can be bundled
     * Ex: if install base is part of bundle and there is a migration/moveOffnet performing, the install base bundle gets broken and agent can create another bundle
     * using the install base product that is part of broken bundle
     * @param $installBaseProduct
     * @param $allInstallBaseProducts
     * @return bool
     */
    protected function canBeBundled($installBaseProduct, &$allInstallBaseProducts)
    {
        // check if there is already a bundle in cart with the current install product @see OVG-2287
        $productIsBundled = false;
        $cartPackage = null;
        if (strtolower($installBaseProduct['contract_category']) === strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) && ($kdParentAccountNumber = Mage::getSingleton('customer/session')->getKdLinkedAccountNumber())) {
            $cartPackage = $this->quote->getInstallBasePackage($kdParentAccountNumber, $installBaseProduct['product_id']);
        } else {
            $cartPackage = $this->quote->getInstallBasePackage($installBaseProduct['contract_id'], $installBaseProduct['product_id']);
        }

        if ($cartPackage && ($cartPackage->isPartOfGigaKombi() || $cartPackage->isPartOfIntrastack())) {
            $productIsBundled = true;
        }

        // first, check if this product is migrated/moved
        if ($productIsBundled || ($this->inMigrationState && ($installBaseProduct['contract_id'] == $this->migrationCustomerNumber && $installBaseProduct['product_id'] == $this->migrationProductId))) {
            return false;
        }

        // if it is not part of a bundle and there, it can go for evaluation
        if (!$installBaseProduct['part_of_bundle']) {
            return true;
        }

        // if product is in cart, and tariff is changed, than it can be eligible for bundling (@see VFDED1W3S-2668)
        if ($cartPackage && $cartPackage->ilsTariffChanged()) {
            return true;
        }

        // determine if this install base product is sibling of the migrated/moved one
        foreach ($allInstallBaseProducts as &$otherInstallBaseProduct) {
            // skip the identical product
            if ($otherInstallBaseProduct['contract_id'] == $installBaseProduct['contract_id'] && $otherInstallBaseProduct['product_id'] == $installBaseProduct['product_id']) {
                continue;
            }

            // if product is part of bundle but its sibling is in cart, than it can be bundled
            if ($otherInstallBaseProduct['part_of_bundle'] &&
                (($cartPackage = $this->quote->getInstallBasePackage($otherInstallBaseProduct['contract_id'], $otherInstallBaseProduct['product_id'], true)) && $cartPackage->ilsTariffChanged())
                || (Mage::getSingleton('checkout/cart')->getCheckoutSession()->getMigrationOrMoveOffnetCustomerNo())
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method that determines whether or not current customer has in his install base a product from a certain category
     * Returns install base subscription (customer number) number with it's associated product id, if found
     * @param $categoryPath string
     * @param $notExpression bool
     * @return bool
     */
    public function containsProductInCategory($categoryPath, $notExpression = false)
    {
        $installProduct = array();
        $contains = false;
        if (!isset($this->categoryCache[$categoryPath])) {
            $categoryModel = Mage::getModel('catalog/category');
            $this->categoryCache[$categoryPath] = $categoryModel->getCategoryByNamePath($categoryPath, self::CATEGORY_PATH_SEPPARTOR);
        }
        $category = $this->categoryCache[$categoryPath];
        $bundle = $this->bundlesHelper->getCurrentBundle();

        // if no customer logged in, no products in install base
        // same for category, if it doesn't exist condition will be evaluated to false
        // nothing to store in magento registry
        if (!$this->customerLoggedIn || !$category || !$categoryId = $category->getId()) {
            return $contains;
        }

        $kombiBundle = $bundle && ($bundle->isGigaKombi() || $bundle->isIntraStack());
        if ($kombiBundle && ($this->quote->getCartPackage()->isPartOfGigaKombi() || $this->quote->getCartPackage()->isPartOfIntrastack())) {
            return false;
        }

        $cartPackage = $this->quote->getActivePackage();
        if (empty($installProduct) && Mage::registry('install_base_expression_result')) {
            $installProduct = Mage::registry('install_base_expression_result') ?: array();
        }

        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");

        foreach ($this->installBaseProducts as $installBaseProduct) {
            // check if install base product is modified in cart and forward evaluation to the cart object
            if (($package = $this->quote->getInstallBasePackage($installBaseProduct['contract_id'], $installBaseProduct['product_id'], true))) {
                $processContextId = $processContextHelper->getProcessContextId($package);
                if (!in_array($processContextId, $bundle->getProcessContextIds())) {
                    continue;
                }
                // if package is in an omnius gigakombi or intrastack bundle, than false
                if ($bundle && $package->getBundleById($bundle->getId()) && $kombiBundle) {
                    return false;
                }
                if ($result = $this->expressionHelper->packageContainsProductInCategory($categoryPath, $notExpression, $package->getPackageId())) {
                    $installProduct = Mage::helper('dyna_core')->arrayRecursiveUnique(array_merge_recursive($installProduct, $result));
                    $contains = true;
                } elseif (!$package->hasTariff() && !$this->bundlesHelper->isSimulation()) {
                    return false;
                }
                continue;
            }

            // if current evaluation bundle is kombi (giga or intra stack) custom logic needs to be applied
            if ($cartPackage && $kombiBundle && $installBaseProduct && isset($installProduct[($this->bundlesHelper)::EXPRESSION_PACKAGES_RESULT])) {
                foreach ($installProduct[($this->bundlesHelper)::EXPRESSION_PACKAGES_RESULT] as $eligiblePackageId) {
                    if ($cartPackage->getPackageId() !== $eligiblePackageId) {
                        continue;
                    }
                    // cannot intrastack with the same package type
                    if ($bundle->isIntraStack() && (strtolower($cartPackage->getType()) == strtolower($installBaseProduct['package_type']))) {
                        continue 2;
                    }
                    // cannot gigakombi with the same stack
                    if ($bundle->isGigaKombi() && (strtolower($cartPackage->getPackageStack()) == strtolower($installBaseProduct['contract_category']))) {
                        continue 2;
                    }
                }
            }

            $productValid = false;
            // each product has an associated list of category ids to which it belongs
            foreach ($installBaseProduct['products'] as $product) {
                if (in_array($categoryId, $product['categories'])) {
                    // exit loop
                    $productValid = true;
                    $contains = true;
                    break;
                }
            }

            if ($productValid && !$notExpression) {
                // if just initialised, try loading it from registry
                $installProduct[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT][] = array(
                    ($this->bundlesHelper)::INSTALL_BASE_CUSTOMER_NUMBER => $installBaseProduct['contract_id'] ?? null,
                    ($this->bundlesHelper)::INSTALL_BASE_PRODUCT_ID => $installBaseProduct['product_id'] ?? null,
                );
            }
        }

        if ($contains && !$notExpression) {
            if (Mage::registry('install_base_expression_result') !== null) {
                Mage::unregister('install_base_expression_result');
            }

            if ($cartPackage && $kombiBundle && !empty($installProduct[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT])) {
                foreach ($installProduct[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT] as $key => $targetedProduct) {
                    foreach ($this->installBaseProducts as $baseProduct) {
                        if (($baseProduct['contract_id'] !== $targetedProduct['customerNumber']) || ($baseProduct['product_id'] !== $targetedProduct['productId'])) {
                            continue;
                        }
                        // cannot intrastack with the same package type
                        if ($bundle->isIntraStack() && (strtolower($cartPackage->getType()) == strtolower($baseProduct['package_type']))) {
                            unset ($installProduct[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT][$key]);
                        }
                        // cannot gigakombi with the same stack
                        if ($bundle->isGigaKombi() && (strtolower($cartPackage->getPackageStack()) == strtolower($baseProduct['contract_category']))) {
                            unset ($installProduct[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT][$key]);
                        }
                    }
                }
            }

            Mage::register('install_base_expression_result', $installProduct);
        }


        return $contains;
    }

    /**
     * Method that determines whether or not current customer has in his install base a product from a certain category
     * Returns install base subscription (customer number) number with it's associated product id, if found
     * @param $id string
     * @param $notExpression bool
     * @return bool
     */
    public function containsProductInCategoryId($id, $notExpression = false)
    {
        return $this->containsProductInCategory($this->getCategoryPathByUniqueId($id), $notExpression);
    }

    /**
     * Determine whether or not install base contains a certain product
     * @param string $productSku
     * @param $notExpression bool
     * @return bool
     */
    public function containsProduct($productSku, $notExpression = false)
    {
        if (Mage::registry('install_base_expression_result') !== null) {
            $installProduct = Mage::registry('install_base_expression_result');
        } else {
            $installProduct = array();
        }

        $contains = false;

        // if no customer logged in, no products in install base
        if (!$this->customerLoggedIn) {
            return false;
        }

        foreach ($this->installBaseProductsForRedPlus as $installBaseProduct) {
            $productValid = false;
            // check if install base product is modified in cart and forward evaluation to the cart object
            if ($this->quote->getInstallBasePackage($installBaseProduct['contract_id'], $installBaseProduct['product_id'], true)) {
                $productValid = Mage::helper('dyna_bundles/expression')->containsProduct($productSku);
            }
            foreach ($installBaseProduct['products'] as $product) {
                if ($product['product_sku'] === $productSku) {
                    // exit this loop
                    $contains = true;
                    $productValid = true;
                    break;
                }
            }

            if (($productValid && !$notExpression)) {
                // add this product to evaluation response
                // the bundles expression language helper will merge these responses for each bundle in order to determine the correct targeted install base product
                $installProduct[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT][] = array(
                    ($this->bundlesHelper)::INSTALL_BASE_CUSTOMER_NUMBER => $installBaseProduct['contract_id'] ?? null,
                    ($this->bundlesHelper)::INSTALL_BASE_PRODUCT_ID => $installBaseProduct['product_id'] ?? null,
                );
            }
        }

        if ($contains && !$notExpression) {
            if (Mage::registry('install_base_expression_result') !== null) {
                Mage::unregister('install_base_expression_result');
            }

            Mage::register('install_base_expression_result', $installProduct);
        }

        return $contains;
    }

    /**
     * Determine the number of installed base members for a certain subscription
     * @param $item
     * @param $contractId
     * @param $productId
     * @return int
     */
    public function numberOfProductsWithParent($item, $contractId, $productId)
    {
        switch (true) {
            // can pass quote item as argument (from cart object evaluator)
            case $item instanceof Dyna_Checkout_Model_Sales_Quote_Item :
                $productSku = $item->getSku();
                break;
            // cas pass non-empty string sku as argument from bellow contains related product method
            case is_string($item) && $item :
                $productSku = $item;
                break;
            // invalid sku received will result in counted down to zero
            default:
                $productSku = "";
        }

        // if install base doesn't contain this product return 0 (this should never happen because there should be a higher priority condition for this)
        if (!$this->customerLoggedIn || !$productSku || !$this->containsProduct($productSku)) {
            return 0;
        }

        $customerNumberProductIdCombinations = Mage::registry('install_base_expression_result');

        foreach ($customerNumberProductIdCombinations[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT] as $combination) {
            if ($combination['customerNumber'] !== $contractId || $combination['productId'] !== $productId) {
                continue;
            }
            foreach ($this->installBaseProductsForRedPlus as $baseProduct) {
                // skip this product if it doesn't match combination
                if ($baseProduct['product_id'] === $combination[($this->bundlesHelper)::INSTALL_BASE_PRODUCT_ID] && $baseProduct['contract_id'] === $combination[($this->bundlesHelper)::INSTALL_BASE_CUSTOMER_NUMBER]) {
                    if ($baseProduct['sharing_group_member_type'] === Dyna_Customer_Helper_Customer::SUBSCRIPTION_TYPE_OWNER) {
                        return (int)$baseProduct['sharing_group_number_of_members'];
                    } else {
                        return 0;
                    }
                }
            }
        }

        return 0;
    }

    /**
     * Determine whether or not install base contains related products
     * If found, will return a list of combinations between customer number and product id to determine the right install base contract
     * @return array
     */
    public function getRelatedProducts()
    {
        $cacheKey = md5(__METHOD__);
        if (isset($this->cache[$cacheKey])) {
            return $this->cache[$cacheKey];
        }

        $installProduct = array();

        // if no customer logged in, no products in install base
        if (!$this->customerLoggedIn) {
            return $installProduct;
        }

        foreach ($this->installBaseProductsForRedPlus as $installBaseProduct) {
            if ($installBaseProduct['sharing_group_member_type'] == Dyna_Customer_Helper_Customer::SUBSCRIPTION_TYPE_MEMBER) {
                continue;
            }

            if (!$this->hasTariffInSubscription($installBaseProduct['products'])) {
                continue;
            }

            if ($this->convertedToMember($installBaseProduct)) {
                continue;
            }

            $newTariff = null;
            $oldTariff = null;
            $productInCart = false;
            if ($package = $this->quote->getInstallBasePackage($installBaseProduct['contract_id'], $installBaseProduct['product_id'], true)) {
                // inlife scenario
                $productInCart = true;
                foreach ($package->getAllItems() as $item) {
                    if ($item->getProduct()->isSubscription() && !$item->getIsContract()) {
                        $newTariff = $item;
                    } else if ($item->getProduct()->isSubscription() && $item->getIsContract()) {
                        $oldTariff = $item;
                    }
                }
            }
            // exists if not added to cart, or if added to cart and tariff kept
            $oldTariffExists = !$productInCart || ($productInCart && $oldTariff);
            foreach ($installBaseProduct['products'] as $product) {
                // if this is not a tariff change, than members can be added if related product exists or group already created
                if ((!$newTariff && $oldTariffExists && ($this->isRelatedProduct($product['product_id']) || $this->isInstallBaseGroupOwner($installBaseProduct)))
                    || ($newTariff && $newTariff->getProduct()->isRedPlusOwner())) {
                    // add this product to evaluation response
                    // the bundles expression language helper will merge these responses for each bundle in order to determine the correct targeted install base product
                    $installProduct[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT][] = array(
                        ($this->bundlesHelper)::INSTALL_BASE_CUSTOMER_NUMBER => $installBaseProduct['contract_id'] ?? null,
                        ($this->bundlesHelper)::INSTALL_BASE_PRODUCT_ID => $installBaseProduct['product_id'] ?? null,
                        'product_sku' => $product['product_sku'],
                    );
                    break;
                }
            }
        }

        $this->cache[$cacheKey] = $installProduct;

        if (Mage::registry('install_base_expression_result') !== null) {
            Mage::unregister('install_base_expression_result');
        }

        Mage::register('install_base_expression_result', $installProduct);

        return $installProduct;
    }

    /**
     * Determine whether or not current customer has related products in it's install base
     * @return bool
     */
    public function containsRelatedProducts()
    {
        return (bool)$this->getRelatedProducts();
    }

    /**
     * Determine whether or not the maximum number of members in a install base group has been reached
     * Keeps result in registry for further evaluation by cart object for filtering those that
     * @param array $productsList
     * @param int $maximum
     * @return array
     */
    public function maxNumberOfProductsWithParent(array $productsList, $maximum)
    {
        $result = array();
        $maximum = (int)$maximum;

        // if no customer logged in, no products in install base
        if (!$this->customerLoggedIn) {
            return $result;
        }

        if (!empty($productsList[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT])) {
            foreach ($productsList[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT] as $combination) {
                $members = $this->numberOfProductsWithParent($combination['product_sku'], $combination['customerNumber'], $combination['productId']);
                if ($members < $maximum) {
                    $result[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT][] = array(
                        ($this->bundlesHelper)::INSTALL_BASE_CUSTOMER_NUMBER => $combination[($this->bundlesHelper)::INSTALL_BASE_CUSTOMER_NUMBER] ?? null,
                        ($this->bundlesHelper)::INSTALL_BASE_PRODUCT_ID => $combination[($this->bundlesHelper)::INSTALL_BASE_PRODUCT_ID] ?? null,
                        ($this->bundlesHelper)::INSTALL_BASE_NR_MEMBERS => $members,
                    );
                }
            }
        }

        if (Mage::registry('install_base_expression_result') !== null) {
            Mage::unregister('install_base_expression_result');
        }

        Mage::register('install_base_expression_result', $result);

        return $result;
    }

    /**
     * Determine whether or not a product with a certain id is a related type product
     * @param $productId
     * @return bool
     */
    protected function isRelatedProduct($productId)
    {
        if (!isset($this->relatedProducts[$productId])) {
            $this->relatedProducts[$productId] = Mage::getModel('catalog/product')->load($productId)->isRedPlusOwner();
        }

        return $this->relatedProducts[$productId];
    }

    /**
     * Determines whether the install base product is the owner of an existing sharing group
     *
     * @param $product
     * @return bool
     */
    protected function isInstallBaseGroupOwner($product)
    {
        return !empty($product['sharing_group_id']) && ($product['sharing_group_member_type'] === Dyna_Customer_Helper_Customer::SUBSCRIPTION_TYPE_OWNER);
    }

    /**
     * Checks whether an install base subscription has a mapped tariff
     *
     * @param $products
     * @return bool
     */
    protected function hasTariffInSubscription($products)
    {
        foreach ($products as $product) {
            if (strtolower($product['product_package_subtype']) == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method that determines for current customer  how many products has in his install base from a certain category
     * @param $categoryPath
     * @return int
     */
    public function numberOfProductsInCategory($categoryPath)
    {
        $counter = 0;

        if (!isset($this->categoryCache[$categoryPath])) {
            $categoryModel = Mage::getModel('catalog/category');
            $this->categoryCache[$categoryPath] = $categoryModel->getCategoryByNamePath($categoryPath, '->');
        }
        $category = $this->categoryCache[$categoryPath];

        // if no customer logged in, no products in install base
        // same for category, if it doesn't exist condition will be evaluated to false
        if (!$this->customerLoggedIn || !$category || !($categoryId = $category->getId())) {
            return $counter;
        }

        foreach ($this->installBaseProducts as $installBaseProduct) {
            // check if install base product is modified in cart and forward evaluation to the cart object
            if ($this->quote->getInstallBasePackage($installBaseProduct['contract_id'], $installBaseProduct['product_id'])) {
                return Mage::helper('dyna_bundles/expression')->updateInstance($this->quote)->numberOfProductsInCategory($categoryPath);
            }
            // each product has an associated list of category ids to which it belongs
            foreach ($installBaseProduct['products'] as $product) {
                if (in_array($categoryId, $product['categories'])) {
                    $counter++;
                }
            }
        }

        return $counter;
    }

    /**
     * Returns by category based on its unique id
     * @param $id
     * @return mixed
     */
    protected function getCategoryPathByUniqueId($id)
    {
        if (!isset($this->categoryCache[$id])) {
            /** @var Dyna_Catalog_Model_Category $categoryModel */
            $categoryModel = Mage::getSingleton('catalog/category');
            $this->categoryCache[$id] = $categoryModel->getCategoryByUniqueId($id);
        }

        return $this->categoryCache[$id]->getPathAsString(false, self::CATEGORY_PATH_SEPPARTOR);
    }

    /**
     * Method that determines for current customer  how many products has in his install base from a certain category
     * @param $id
     * @return int
     */
    public function numberOfProductsInCategoryId($id)
    {
        return $this->numberOfProductsInCategory($this->getCategoryPathByUniqueId($id));
    }

    /**
     * Determine whether or not a certain subscription from a contract contains a certain product (@see VFDED1W3S-1790)
     * @param string $productSku
     * @return bool
     */
    public function subscriptionContainsProduct($productSku)
    {
        /** @var Dyna_Customer_Model_Customer $currentCustomer */
        if ($currentCustomer = Mage::getSingleton('customer/session')->getCustomer()) {
            $activePackage = $this->quote->getCartPackage();
            if ($activePackage && $parentAccountNumber = $activePackage->getParentAccountNumber()) {
                $serviceLineId = $activePackage->getServiceLineId();
                if ($activePackage->getSaleType() == Dyna_Catalog_Model_ProcessContext::MIGCOC && !$activePackage->getConvertToMember() && $activePackage->isMobilePostpaid()) {
                    $parentAccountNumber = $currentCustomer->getParentAccountNumberByServicelineId($serviceLineId);
                }

                foreach ($currentCustomer->getInstalledBaseProducts($parentAccountNumber, $serviceLineId) as $product) {
                    if ($product == $productSku) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check if FamilyId exists in install base products
     * @param string $familyCode
     * @return bool
     */
    public function containsProductInProductFamily($familyCode)
    {

        $contains = false;

        $familyId = Mage::getSingleton('dyna_catalog/productFamily')->getProductFamilyIdByCode($familyCode);

        // Return if no customer loaded into session or no family id found
        if (!$this->customerLoggedIn || !$familyId) {
            return $contains;
        }

        foreach ($this->installBaseProducts as $installBaseProduct) {
            foreach ($installBaseProduct['products'] as $product) {
                if ($product['product_family'] && in_array($familyId, explode(",", $product['product_family']))) {
                    $contains = true;
                    break;
                }

            }
        }

        return $contains;
    }

    /**
     * Checks whether an install base product has been converted to member
     *
     * @param $installBaseProduct
     * @return bool
     */
    protected function convertedToMember($installBaseProduct)
    {
        foreach ($this->quote->getCartPackages() as $cartPackage) {
            if ($cartPackage->getParentAccountNumber() == $installBaseProduct['contract_id'] && $cartPackage->getServiceLineId() == $installBaseProduct['product_id']) {
                if ($cartPackage->getConvertedToMember()) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        return false;
    }
}
