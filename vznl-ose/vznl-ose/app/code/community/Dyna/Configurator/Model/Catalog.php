<?php

class Dyna_Configurator_Model_Catalog extends Omnius_Configurator_Model_Catalog
{
    protected $packageType = null;
    protected $customerType = null;
    protected $showPriceWithBtw = null;
    protected $processContext = null;

    // These are the only product attributes available in the Configurator Frontend
    protected $frontendAttributes = array(
        'sku',
        'thumbnail',
        'image',
        'entity_id',
        'name',
        'display_name_configurator',
        'stock_status_class',
        'stock_status_text',
        'stock_status',
        'rel_promotional_maf',
        'rel_promotional_maf_with_tax',
        'rel_regular_maf',
        'rel_regular_maf_with_tax',
        'regular_maf',
        'regular_maf_with_tax',
        'price',
        'price_with_tax',
        'sim_type',
        'maf',
        'group_type',
        'is_discontinued',
        'sim_only',
        'business_product',
        'include_btw',
        'contract_period',
        'available_stock_ind',
        'initial_period',
        'bandwidth',
        'minimum_contract_duration',
        'available_stock_ind',
        'additional_text',
        'has_maf',
        'has_price',
        'relation_components',
        'service_description',
        'product_visibility',
        'initial_selectable',
        Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR,
        Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR,
        Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ID,
        Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ID,
        'display_price',
        'display_price_repeated',
        'display_price_sliding',
        'type',
        'hierarchy_value',
        'lifecycle_status',
        'product_version',
        'product_family',
        'contract_value',
        'redplus_role',
        'product_type',
        'use_service_value',
        'service_xpath',
    );

    public function setPackageType(string $packageType)
    {
        $this->packageType = $packageType;

        return $this;
    }

    public function setCustomerType(string $customerType)
    {
        $this->customerType = $customerType;

        return $this;
    }

    public function setShowPriceWithBtw(string $showPriceWithBtw)
    {
        $this->showPriceWithBtw = $showPriceWithBtw;

        return $this;
    }

    /**
     * @param $packageType
     *
     * Get available configuration options based on package type
     * @return array
     */
    public static function getOptionsForPackageType($packageType)
    {
        $helper = Mage::helper('dyna_configurator');

        // In case it is a template package type requested by cart content, strip the dummy 'template-' substring
        $packageType = str_replace("template-", "", $packageType);
        return $helper->getPackageSubtypes($packageType);
    }

    public static function getOptionsForCartPackageType($packageType)
    {
        $helper = Mage::helper('dyna_configurator');

        // In case it is a template package type requested by cart content, strip the dummy 'template-' substring
        $packageType = str_replace("template-", "", $packageType);
        return $helper->getPackageSubtypes($packageType, false, true);
    }

    /**
     * @param $productType
     * @param null $websiteId
     * @param array $filters
     * @return Mage_Eav_Model_Entity_Collection_Abstract|mixed|Varien_Data_Collection
     *
     * Get list of products of type device, subscription, addon or accessory
     */
    public function getProductsOfType($productType, $websiteId = null, array $filters = array(), $packageType = null, $consumerType = null)
    {
        $type = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE;
        // Date key added when implemented expiration date for products
        // This cache will be refreshed every 24h
        // Use short keys for redis, because it affects read time
        $key = md5(serialize(array($packageType, $filters, $productType, $type, $websiteId, $consumerType, date('Y-m-d'), $this->processContext)));


        $productTypeAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'type');
        $productTypeValue = Mage::helper('dyna_catalog')->getAttributeAdminLabel($productTypeAttribute, null, Dyna_Cable_Model_Product_Attribute_Option::TYPE_OPTION_CONTAINER);

        $cableOptionProductTypeAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'type');
        $cableOptionProductTypeContainerValue = Mage::helper('dyna_catalog')->getAttributeAdminLabel($cableOptionProductTypeAttribute, null, Dyna_Cable_Model_Product_Attribute_Option::TYPE_OPTION_CONTAINER);

        // parse and set products data
        if (!($products = unserialize($this->getCache()->load($key)))) {
            $collection = $this->getProductCollection($productType, $type, $websiteId, $packageType, $consumerType, $filters)->load();
            $products = new Varien_Data_Collection();
            // Only return the following properties to avoid large json objects with properties that are not used
            $keys = $this->frontendAttributes;
            $mandatoryCategories = Mage::helper('omnius_configurator')->getMandatoryCategories();
            /** @var Dyna_Catalog_Model_Product $item */
            foreach ($collection->getItems() as $item) {
                // Skip Special Mobile discounts from promotions list
                if ($productType == Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION
                    && in_array($item->getSku(), Dyna_Catalog_Model_Product::getMobileDiscountsIds(false))
                ) {
                    continue;
                }
                $item = $this->getFrontendProduct($item, $keys, $mandatoryCategories, $productTypeValue);
                $products->addItem($item);
            }
            $this->getCache()->save(serialize($products), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        return $products;
    }

    /**
     * Gets the frontend product, removing data with keys that are not set inside $keys array.
     * @param Dyna_Catalog_Model_Product $item The item to use.
     * @param array $keys The keys to keep.
     * @param array $mandatoryCategories The mandatory categories.
     * @param string $productTypeValue The product type value to check.
     * @return Dyna_Catalog_Model_Product The adjusted product.
     */
    protected function getFrontendProduct($item, $keys, $mandatoryCategories, $productTypeValue)
    {
        /** @var Mage_Catalog_Model_Resource_Product $productResource */
        $productResource = Mage::getResourceModel("catalog/product");
        $attrHelper = Mage::helper('omnius_configurator/attribute');

        $data = $this->parseProductDataForType($item, $attrHelper, $keys);

        // Building family information
        $familyIds = $item->getCategoryIds(true);
        $mandatory = array_intersect($familyIds, $mandatoryCategories);
        // don't touch the following line, will break the entire configurator
        $data['families'] = array_combine(array_map("intval", array_values($mandatory)), array_values($mandatory));
        $data['category_ids'] = $productResource->getCategoryIdsWithAnchors($item);

        // check if the option cable type is "container"
        $data['isContainer'] = false;
        $data['display_price'] = 0.00;
        $data['display_price_repeated'] = 0.00;
        if (isset($data['type']) && $data['type'] == $productTypeValue) {
            $data['isContainer'] = true;
            $data['display_price'] = is_null($data['display_price']) ? 0.00 : (float)str_replace(",", ".", $data['display_price']);
            $data['display_price_repeated'] = is_null($data['display_price_repeated']) ? 0.00 : (float)str_replace(",", ".", $data['display_price_repeated']);
        }

        $item->setData($data);
        $item->setData("maf", $item->getMaf());
        $item->setData("bandwidth", $item->getBandwidth() / 1000);
        $item->setData("available_stock_ind", (int)$item->getAvailableStockInd());
        $item->setData("has_maf", $item->getMaf() && (bool)$item->getMaf() >= 0);
        $item->setData("has_price", (bool)$item->getPrice());
        $item->setData("requires_serial", $item->isOwnReceiver() || $item->isOwnSmartcard());
        $item->setData("is_own_receiver", $item->isOwnReceiver());
        $item->setData("is_own_smartcard", $item->isOwnSmartcard());
        $item->setData("name", $item->getDisplayNameConfigurator() ?: $item->getName());
        $item->setData("product_visibility", explode(",", $item->getProductVisibility()));
        try {
            $item->setData('thumbnail', (string)Mage::helper('catalog/image')->init($item, 'thumbnail')->resize(30));
        } catch (Exception $e) {
            Mage::log(sprintf('Assigned image for product %s not found on disk.', $item->getSku()), Zend_Log::NOTICE);
        }

        return $item;
    }

    /**
     * @param $packageSubType
     * @param string $type
     * @param $websiteId
     * @param $packageType
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection($packageSubType, $type, $websiteId, $packageType, $consumerType = null, $filters = [])
    {
        if ($packageType) {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $typeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageType);
        }

        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
        $subTypeValue = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, $packageSubType);

        //if website_id is provided in request param, overwrite it
        $websiteId = Mage::app()->getRequest()->getParam('website_id', $websiteId);
        /** @var Omnius_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->setStoreId(Mage::app()->getStore())
            ->addWebsiteFilter($websiteId)
            ->addTaxPercents()
            ->addPriceData(null, $websiteId)
            ->addAttributeToFilter('type_id', $type)
            ->addAttributeToFilter('is_deleted', array('neq' => 1))
            ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $subTypeValue);

        $cablePackages = Dyna_Catalog_Model_Type::getCablePackages();
        $cablePackages = array_map('strtolower', $cablePackages);
        if (!in_array(strtolower($packageType),$cablePackages)){
            $collection
                // Add attribute to sorting
                ->addAttributeToSort('sorting', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC)
                ->addAttributeToSort('name', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC)
                ->getSelect()
                // Resetting magento's way of doing it because it adds null sorting values at the top of the list
                ->reset(Zend_Db_Select::ORDER)
                ->order("ISNULL(sorting), sorting ASC, name ASC", Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC);
        }else{
            // @todo Waiting for feedback regarding the attribute by which we should sort cable products
            // If none of the above attributes are present sort by name
            $collection->addAttributeToSort('name', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC);
        }

        if (count($filters)) {
            $collection = $this->applyFilters($collection, $filters);
        }

        $allowedPackageTypes = [
            strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE),
            strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL),
            strtolower(Dyna_Catalog_Model_Type::TYPE_REDPLUS)
        ];

        if ($this->processContext == Dyna_Catalog_Model_ProcessContext::ACQ &&
            in_array(strtolower($packageType), $allowedPackageTypes)
        ) {
            $collection->addAttributeToFilter(
                array(
                    array('attribute' => 'expiration_date', 'gteq' => date('Y-m-d')),
                    array('attribute' => 'expiration_date', array('null' => true)),
                )
            );
        }

        // Don't get the price for mixmatch as we also need disabled products
        if (!Mage::registry('mix_match_with_disabled_products')) {
            $collection
                ->addTaxPercents()
                ->addPriceData(null, $websiteId);
        }

        if ($consumerType !== null) {
            $consValue = Mage::helper('dyna_configurator/attribute')->getSubscriptionIdentifier($consumerType);
            $collection->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_CONSUMER_TYPE, array('finset' => $consValue));
        }

        if ($packageType) {
            $collection->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, array("finset" => $typeValue));
        }

        // Skip Special Mobile discounts from promotions list
        if ($packageSubType == Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION) {
            $collection->addAttributeToFilter('sku', array("nin" => Dyna_Catalog_Model_Product::getMobileDiscountsIds(false)));
        }

        return $collection;
    }

    /**
     * Applies tax on the prices if the current customer is business
     *
     * @param Mage_Catalog_Model_Product $product
     */
    protected function preparePrice(Mage_Catalog_Model_Product $product)
    {
        $_taxHelper = Mage::helper('tax');
        $_store = $product->getStore();

        $allMafTypes = Dyna_Catalog_Model_Type::$hasMafPrices;

        if ($product->is($allMafTypes)) {
            $priceValues = array(
                'promotional_maf',
                'regular_maf',
            );
            foreach ($priceValues as $priceValue) {
                if ($product->hasData($priceValue)) {
                    $_convertedPrice = $_store->roundPrice($_store->convertPrice($product->getData($priceValue)));
                    $priceWithTax = $_taxHelper->getPrice($product, $_convertedPrice, true);
                    $price = $_taxHelper->getPrice($product, $_convertedPrice, false);
                    /**
                     * Save both prices (with and without tax)
                     */
                    $product->setData(sprintf('%s_with_tax', $priceValue), $priceWithTax);
                    $product->setData($priceValue, $price);
                }
            }
        }

        /**
         * Save both prices (with and without tax)
         */
        $_convertedFinalPrice = $_store->roundPrice($_store->convertPrice($product->getPrice()));
        $priceWithTax = $_taxHelper->getPrice($product, $_convertedFinalPrice, true);
        $price = $_taxHelper->getPrice($product, $_convertedFinalPrice, false);
        $product->setPriceWithTax($priceWithTax);
        $product->setPrice($price);
    }

    public function getMobileDiscounts($productType, $packageType, $websiteId = null)
    {
        $key = serialize(array($productType, $packageType, __FUNCTION__, Mage_Catalog_Model_Product_Type::TYPE_SIMPLE, $websiteId));
        if (!($products = unserialize($this->getCache()->load($key)))) {

            /** @var Omnius_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*')
                ->setStoreId(Mage::app()->getStore())
                ->addFieldToFilter('sku', array('in' => Dyna_Catalog_Model_Product::getMobileDiscountsIds(false)))
                ->addWebsiteFilter($websiteId)
                ->addAttributeToFilter('is_deleted', array('neq' => 1))
                ->load();

            $products = new Varien_Data_Collection();

            //only return the following properties to avoid large json objects with properties that are not used
            $keys = array('sku', 'entity_id', 'name');
            $keys = array_unique(array_merge($keys, $this->getFilterableAttributeCodes()));
            $attrHelper = Mage::helper('omnius_configurator/attribute');

            /** @var Dyna_Catalog_Model_Product $item */
            foreach ($collection->getItems() as $item) {
                $data = $this->parseProductDataForType($item, $attrHelper, $keys);
                $item->setData($data);
                $products->addItem($item);
            }

            $this->getCache()->save(serialize($products), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        return $products;

    }

    /**
     * @param $packageType
     * @return mixed|Varien_Data_Collection
     *
     * Get all items for a certain package type (mobile, prepaid, etc)
     * This relies on the "identifier_package_type" attribute meaning that only products that have this attribute set will be retrieved
     */
    public function getAllItems($packageType, $attributesList = array())
    {
        $dealerGroups = Mage::helper('agent')->getCurrentDealerGroups();
        sort($dealerGroups);
        $key = serialize(array(strtolower(__METHOD__), $packageType, implode(',', $dealerGroups), $attributesList, Mage::app()->getStore()->getWebsiteId()));
        if ($result = $this->getCache()->load($key)) {
            return unserialize($result);
        } else {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $type = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageType);
            /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId(Mage::app()->getStore())
                ->addWebsiteFilter();
            if ($attributesList) {
                foreach ($attributesList as $attribute) {
                    $collection->addAttributeToSelect($attribute);
                }
            } else {
                $collection
                    ->addAttributeToSelect('*')
                    ->addMinimalPrice()
                    ->addFinalPrice()
                    ->addTaxPercents();
            }
            $collection
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, $type)
                ->load();

            $result = new Varien_Data_Collection();
            foreach ($collection->getItems() as $item) {
                $result->addItem($item);
            }
            unset($collection);

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $tagName
     * @param array $filters TODO: Not implemented at this point, but it might be needed
     * @return mixed|Varien_Data_Collection
     */
    public function getProductsByTag($tagName, $filters = [])
    {
        $key = serialize([strtolower(__METHOD__), $tagName]);

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = new Varien_Data_Collection();

            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addWebsiteFilter()
                ->addAttributeToFilter('tags', ['finset' => [$tagName]]);

            if (isset($filters['packageType'])) {
                $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
                $packageType = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $filters['packageType']);
                $collection->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, $packageType);
            }

            $collection->load();

            foreach ($collection->getItems() as $item) {
                $result->addItem($item);
            }

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $collection;
        }
    }

    /**
     * @param $attr string
     * @param $value string
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductsByAttribute($attr, $value)
    {
        return Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter($attr, $value)
            ->load();
    }

    /**
     * @param $attributeName
     * @param $attributeId
     * @return array
     */
    public function getProductAttributeLabel($attributeName, $attributeId)
    {
        return $this->getAttributeLabelFromValue($attributeName, $attributeId);
    }

    /**
     * @param $item
     * @param $attrHelper
     * @param $keys
     * @return mixed
     */
    protected function parseProductDataForType($item, $attrHelper, $keys)
    {
        $data = $item->getData();
        foreach ($data as $key => &$value) {

            switch ($key)
            {
                case 'redplus_role':
                case 'product_type':
                    $data[$key] = $this->getAttributeLabelFromValue($key, $data[$key]);
                    break;
                case Omnius_Catalog_Model_Product::AVAILABLE_SIM_TYPES_ATTRIBUTE:
                    $sims = [];
                    // Array of possible simcards
                    $allSims = explode(',', $data[$key]);
                    foreach ($allSims as $sim) {
                        $sims[$attrHelper->getSimCardTypeById($sim)] = $attrHelper->getSimCardTypeById($sim);
                    }
                    $data[$key] = $sims;
                    break;
                case 'name':
                    $data[$key] = html_entity_decode($data[$key]);
                    break;
                case 'package_type':
                case 'package_subtype':
                    $keyId = $key."_id";
                    $data[$keyId] = $this->getAttributeLabelFromValue($key, $data[$key]);
                    break;
                default:
                    if (!in_array($key, $keys)) {
                        unset($data[$key]);
                    }
                    break;
            }
        }
        unset($value);
        return $data;
    }

    protected function getAttributeLabelFromValue($attribute_name, $attribute_value)
    {
        $label = [];
        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attribute_name);
        if ($attribute) {
            $label_json = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, $attribute_value);
            $label = json_decode($label_json, true);
        }
        return array_pop($label);
    }

    /**
     * Get all items that are visible in configurator for a specific package type and filters
     * @param null $websiteId
     * @param array $filters
     * @param null $consumerType
     * @return array|mixed
     * @throws Mage_Core_Exception
     */
    public function getAllConfiguratorItems($websiteId = null, array $filters = array(), $processContext = null)
    {
        $websiteId = $websiteId ?: Mage::app()->getWebsite()->getId();

        /**
         * @var Dyna_Configurator_Helper_Data $helper
         */
        $helper = Mage::helper('dyna_configurator');

        $key = serialize(array(__METHOD__, $websiteId, $filters, $this->packageType, $processContext));

        // store the current process context on the object
        $this->processContext = $processContext;

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getModel('dyna_package/packageType')->loadByCode($this->packageType);
            $result = array();
            $filterableAttributes = $this->getFilterableAttributeCodes();
            foreach ($packageTypeModel->getPackageSubTypes() as $packageSubType) {
                if (!$packageSubType->getVisibleConfigurator()) {
                    continue;
                }
                $result[$packageSubType->getPackageCode(true)] = $this->getProductsOfType($packageSubType->getPackageCode(), $websiteId,
                    $helper->getFiltersPerProductType($packageSubType->getPackageCode(true), $filters),
                    $this->packageType);

                $customFilters = $this->getAttrHelper()->getAvailableFilters(
                    Mage::getResourceModel('catalog/product_collection')
                        ->addAttributeToSelect($filterableAttributes)
                        ->addAttributeToFilter('entity_id', array('in' => $result[$packageSubType->getPackageCode(true)]->getColumnValues('entity_id')))
                );
                $result['filters'][$packageSubType->getPackageCode(true)] = $customFilters;
                $result['consumer_filters'][$packageSubType->getPackageCode(true)] = $customFilters;
            }

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @return mixed|Varien_Data_Collection
     *
     * Get all products available on the mobile package
     */
    public function getAllProducts(array $attributesList = array())
    {
        return $this->getAllItems($this->packageType, $attributesList);
    }
}
