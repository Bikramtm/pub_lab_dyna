<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Configurator_Model_Expression_Agent
 */
class Dyna_Configurator_Model_Expression_Agent extends Dyna_AgentDE_Model_Agent
{
    public function _construct()
    {
        parent::_construct();
        $this->salesid = $this->getProvisSalesInfo();
    }
}