<?php

class Dyna_Configurator_Model_Expression_Order
{
    /** @var Dyna_Configurator_Helper_Expression */
    protected $helper = null;

    /** @var Dyna_Cache_Model_Cache */
    protected $dynaCache = null;

    /** @var Dyna_Superorder_Model_Superorder */
    protected $superOrder = null;
    /** @var Dyna_Package_Model_Package */
    protected $currentPackage = null;

    public function __construct()
    {
        $this->helper = Mage::helper('dyna_configurator/expression');
    }

    /**
     * Checks if the entire order contains a product in a specific category
     * @param string $categoryName
     * @return bool
     */
    public function containsProductInCategory($categoryName)
    {
        return $this->helper->orderContainsProductInCategory($categoryName, $this->superOrder);
    }


    /**
     * Checks if the entire order contains a product in a specific category
     * @param string $categoryName
     * @return bool
     */
    public function containsProductInCategoryId($id)
    {
        return $this->helper->orderContainsProductInCategoryId($id, $this->superOrder);
    }
    /**
     * Checks if the order contains a specific product
     * @param string $sku
     * @return bool
     */
    public function containsProduct($sku)
    {
        return $this->helper->orderContainsProduct($sku, $this->superOrder);
    }

    /**
     * Set super order on current instance
     * @param Dyna_Superorder_Model_Superorder $superOrder
     * @return $this
     */
    public function setSuperOrder(Dyna_Superorder_Model_Superorder $superOrder)
    {
        $this->superOrder = $superOrder;

        return $this;
    }

    public function setPackage(Dyna_Package_Model_Package $package)
    {
        $this->currentPackage = $package;

        return $this;
    }

    /**
     * Determine whether or not a certain order contained a certain product
     * @param string $sku
     * @return bool
     */
    public function packageContainedInstalledBaseProduct($sku)
    {
        foreach ($this->currentPackage->getAllItems() as $item) {
            if ($item->getSku() == $sku && $item->getIsContract() && $item->getIsContractDrop()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether or not a certain order contained a product in a certain family
     * @param string $familyIdentifier
     * @return bool
     */
    public function packageContainedInstalledBaseProductInFamily($familyIdentifier)
    {
        $familyId = Dyna_Catalog_Model_ProductFamily::getProductFamilyIdByCode($familyIdentifier);

        /** @var Dyna_Checkout_Model_Sales_Order_Item $item */
        foreach ($this->currentPackage->getAllItems() as $item) {
            if ($item->getIsContract() && $item->getIsContractDrop() && in_array($familyId, explode(",", $item->getProduct()->getProductFamily()))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether or not a certain order contains a product in a certain family
     * @param string $familyIdentifier
     * @return bool
     */
    public function packageContainsInstalledBaseProductInFamily($familyIdentifier)
    {
        $familyId = Dyna_Catalog_Model_ProductFamily::getProductFamilyIdByCode($familyIdentifier);

        /** @var Dyna_Checkout_Model_Sales_Order_Item $item */
        foreach ($this->currentPackage->getAllItems() as $item) {
            if ($item->getIsContract() && in_array($familyId, explode(",", $item->getProduct()->getProductFamily()))) {
                return true;
            }
        }

        return false;
    }
}
