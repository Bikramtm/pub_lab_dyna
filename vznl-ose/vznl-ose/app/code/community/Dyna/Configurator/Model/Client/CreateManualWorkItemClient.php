<?php

/**
 * Class Dyna_Configurator_Model_Client_CreateManualWorkItemClient
 */
class Dyna_Configurator_Model_Client_CreateManualWorkItemClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "create_manual_work_item/wsdl_create_manual_work_item";
    const ENDPOINT_CONFIG_KEY = "create_manual_work_item/endpoint_create_manual_work_item";
    const CONFIG_STUB_PATH = 'create_manual_work_item/use_stubs';
    const DEFAULT_ENTITY_ID = '999';

    /**
     * VFDECreateManualWorkItem request
     * @param $params
     * @return mixed
     */
    public function executeCreManWorkItemRequest($params = array())
    {
        $this->setRequestHeaderInfo($params);
        $agent = Mage::getSingleton('customer/session')->getAgent(true);
        $dealer = $agent->getDealer();
        $dealerGroup = $dealer->getDealerGroups()->count() > 0 ? $dealer->getDealerGroups()->getFirstItem() : null;
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        $orderId = $quoteId = $offerId = null;
        $object = Mage::getModel('sales/quote')->load($params['orderCartId']);
        $entityType = 'Unknown';
        if ($object->getId()) {
            if ($object->getIsOffer()) {
                $offerId = $object->getId();
                $entityType = 'Offer';
            } else {
                $quoteId = $object->getId();
                $entityType = 'ShoppingCart';
            }
        } else {
            $object = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($params['orderCartId']);
            if ($object->getId()) {
                $orderId = $object->getOrderNumber();
                $entityType = 'Order';
            }
        }

        $params['WorkItem'] = array(
            'Action' => 'Analyse',
            'Manual' => 'true',
            'TargetSystem' => null,
            'Priority' => $params['priority'],
            'ErrorCategory' => 'Business',
            'Stage' => $params['stageId'],
            'EntityType' => $entityType,
            'EntityID' => $params['orderCartId'] ?: self::DEFAULT_ENTITY_ID,
            'Description' => $params['comments'], // Todo: determine if this field should contain other value when manually created
            'Comments' => $params['comments'],
            'StoreID' => null,
            'StoreName' => null,
            'DealerID' => $dealer->getId(),
            'DealerGroupID' => $dealerGroup ? $dealerGroup->getId() : null,
            'DealerGroupName' => $dealerGroup ? $dealerGroup->getName() : null,
            'AgentID' => $params['agentId'],
            'AgentFirstName' => $params['firstName'],
            'AgentLastName' => $params['lastName'],
            'AgentEmailAddress' => $params['agentEmail'],
            'Customer' => array(
                'PartyIdentification' => array(
                    'ID' => $params['globalId'],
                ),
                'PartyLegalEntity' => array(
                    'RegistrationName' => $params['companyName'],
                ),
                'PostalAddresses' => array(
                    'CityName' => $params['customerCity'],
                    'PostalZone' => $params['customerZIP']
                ),
                'Contact' => array(
                    'Telephone' => $params['customerTelephone'],
                    'ElectronicMail' => $params['customerEmail']
                ),
                'AccountCategory' => null,
                'PartyType' => $params['customerType'],
                'ParentAccountNumber' => null,
                'Role' => array(
                    'Person' => array(
                        'FirstName' => $params['customerFirstName'],
                        'FamilyName' => $params['customerLastName'],
                        'BirthDate' => $customer->getId() ? date('Y-m-d', strtotime($customer->getDob())) : null
                    ),
                )
            ),
            'Order' => ($orderId || $quoteId || $offerId) ? array(
                'ID' => $orderId ?: ($quoteId ?: $offerId),
                'UUID' => null,
                'OmniusShoppingCartID' => $quoteId && ($quoteId != '') ? $quoteId : null,
                'OmniusOfferID' => $offerId && ($offerId != '') ? $offerId : null
            ) : null
        );

        $values = $this->CreateManualWorkItem($params);

        return $values;
    }
}
