<?php

/**
 * Class Dyna_Configurator_Helper_Expression
 */
class Dyna_Configurator_Helper_Expression extends Mage_Core_Helper_Abstract
{
    const CATEGORIES_CACHING_KEY = "category_names_used_by_service_expression";
    protected $categoryPathSeparator = "->";

    /** @var Dyna_Checkout_Model_Sales_Quote $quote */
    protected $quote = null;
    /** @var Dyna_Package_Model_Package[] $packages */
    protected $packages = array();
    // hosts package types created in cart
    /** @var array $packageTypes */
    protected $packageTypes = array();
    // array('sku1', 'sku2', 'sku3' ...)
    protected $quoteProducts = array();
    // array('sku1', 'sku2', 'sku3' ...)
    protected $injectedProducts = array();
    // array('categoryName1', 'categoryName2', 'categoryName3')
    protected $quoteCategories = array();
    protected $quoteCategoriesCounter = array();
    protected $packageCategoriesCounter = array();
    // array('sku1' => ('categoryName1', 'categoryName2' ... ), 'sku2' => ..., 'sku3' => ...)
    protected $quoteProductsCategories = array();
    /** @var Dyna_Package_Model_Package */
    protected $activePackage = null;
    protected $activePackageId = null;
    // array('categoryName1', 'categoryName2', 'categoryName3')
    protected $activePackageCategories = array();
    // array('sku1', 'sku2', 'sku3' .. )
    protected $activePackageProducts = array();
    // array('sku1' => ('categoryName1', 'categoryName2' ... ), 'sku2' => ..., 'sku3' => ...)
    protected $activePackageProductsCategories = array();

    /** @var Dyna_Customer_Model_Customer $customer */
    protected $customer = null;
    protected $allCategories = array();

    /** @var Mage_SalesRule_Model_Rule */
    protected $rule = null;

    /** @var  Dyna_Cache_Model_Cache */
    protected $dynaCache;

    protected $cartEmpty = false;

    /** @var Dyna_Bundles_Model_BundleRule|null $currentBundle */
    protected $currentBundle = null;
    /**
     * @var array
     */
    protected $productCategoryCache = [];
    protected $productCache = [];

    protected $categoryIdToStringPathCache = array();
    protected $categoryIdToPathCache = array();

    /** @var Dyna_Package_Model_PackageCreationTypes */
    protected $packageCreationType = false;

    protected $quoteItems = [];
    public function __construct($separator = null)
    {
        if ($separator) {
            $this->categoryPathSeparator = $separator;
        }
        $this->quoteItems = Mage::getSingleton('checkout/cart')->getQuote()->getAllItems();
        $this
            ->setCategories()
            ->updateInstance(Mage::getSingleton('checkout/cart')->getQuote());
    }

    public function getCategoryPathSeparator()
    {
        return $this->categoryPathSeparator;
    }

    /**
     * Used to append to the cart the products that are added in the current addMulti iteration
     * A product can be added by clicking on in the configurator or added by default rules
     * @param $productIds
     */
    public function injectActiveProducts($productIds)
    {
        if ($productIds) {
            $items = Mage::getResourceModel('catalog/product')->getProductsSku($productIds);
            foreach ($items as $item) {
                $this->injectedProducts[$item['sku']] = $item['sku'];
            }

            $this->updateInjected();
        }
    }

    /**
     * Returns injected products as array
     * @return array
     */
    public function getInjectedProducts()
    {
        return array_intersect_key($this->productCache, $this->injectedProducts);
    }

    /**
     * Update quote instance for injected products
     * @return $this
     */
    public function updateInjected()
    {
        $newProductsSkus = array_diff($this->injectedProducts, array_keys($this->productCache));

        // if no new products found
        if (count($newProductsSkus) == 0) {
            return $this;
        }

        $newProducts = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('sku')// <- careful with this
            ->addAttributeToFilter(
                'sku', array('in' => $newProductsSkus)
            )
            ->load();

        foreach ($newProducts as $product) {
            $this->mapCategories($product, true);
            $this->productCache[$product->getSku()] = $this->loadProductForInjected($product);
        }

        return $this;
    }

    /**
     * Gather quote information (product and categories) and active package information used by expression rules
     * @return $this
     */
    public function updateInstance(Dyna_Checkout_Model_Sales_Quote $quote)
    {
        // Set quote reference on current instance
        $this->quote = $quote;
        $this->packages = array();
        $this->packageTypes = array();
        $this->quoteProducts = array();
        $this->injectedProducts = array();
        $this->quoteCategories = array();
        $this->quoteCategoriesCounter = array();
        $this->packageCategoriesCounter = array();
        $this->quoteProductsCategories = array();
        $this->activePackage = null;
        $this->activePackageId = null;
        $this->productCache = array();
        $this->activePackageCategories = array();
        $this->activePackageProducts = array();
        $this->activePackageProductsCategories = array();

        if ($this->quote->isEmpty()) {
            $this->cartEmpty = true;
        }

        // Set customer on current helper
        $this->customer = Mage::getSingleton('customer/session')->getCustomer();
        $this->quoteCategoriesCounter = array();

        // Set quote items products for current instance
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->quoteItems as $item) {
            $product = new Varien_Object();
            $product->setEntityId($item->getProductId());
            $product->setId($item->getProductId());
            $product->setSku($item->getSku());
            $this->mapCategories($product);
        }

        $newProductsSkus = array_diff($this->injectedProducts, array_keys($this->productCache));

        $newProducts = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('sku')// <- careful with this
            ->addAttributeToFilter(
                'sku', array('in' => $newProductsSkus)
            )
            ->load();

        foreach ($this->injectedProducts as $product) {
            if (isset($this->productCache[$product]) && $this->productCache[$product]) {
                $this->mapCategories($this->productCache[$product], true);
            }
        }
        foreach ($newProducts as $product) {
            $this->mapCategories($product, true);
            $this->productCache[$product->getSku()] = $product;
        }


        // Setting package items for active package
        $this->activePackage = $this->quote->getActivePackage();
        $this->activePackageId = $this->quote->getActivePackageId();
        $this->packages[$this->activePackageId] = array();
        $this->packageCategoriesCounter = array();

        // Might be null
        if ($this->activePackage) {
            foreach ($this->quote->getAllItems() as $item) {
                // add categories node for current package if it doesn't exist
                if (!isset($this->packages[$item->getPackageId()]['categories'])) {
                    // Build categories for current sku
                    $this->packages[$item->getPackageId()]['categories'] = array();
                }
                // Get all categories for current product
                //$cats = $item->getProduct()->getCategoryIds();
                $product = new Varien_Object();
                $product->setEntityId($item->getProductId());
                $product->setId($item->getProductId());
                $product->setSku($item->getSku());

                // Get all categories for current product
                $cats = Mage::getResourceModel("catalog/product")->getCategoryIdsWithAnchors($product);

                // Preserve product sku
                $sku = $item->getSku();
                // Add sku to quote list of products
                $this->packages[$item->getPackageId()][$sku] = $sku;
                $this->packageTypes[$item->getPackageId()] = strtoupper($item->getPackageType());
                foreach ($cats as $categoryId) {
                    // Should never be empty because foreign keys are set to product and category entities (even so imports with foreign key checks disabled might cause problems)
                    if (!empty($this->allCategories[$categoryId])) {
                        // Add category id and name to the list of categories for current sku
                        $this->quoteCategoriesCounter[$this->getCategoryPath($categoryId)] = ($this->quoteCategoriesCounter[$this->getCategoryPath($categoryId)] ?? 0) + 1;
                        $this->packageCategoriesCounter[$item->getPackageId()][$this->getCategoryPath($categoryId)] = ($this->packageCategoriesCounter[$item->getPackageId()][$this->getCategoryPath($categoryId)] ?? 0) + 1;
                        $this->packages[$item->getPackageId()]['categories'][$categoryId] = $this->getCategoryPath($categoryId);
                    }
                }

                if ($item->getPackageId() == $this->activePackage->getPackageId()) {
                    // Add sku to quote list of products
                    $this->activePackageProducts[$sku] = $sku;
                    // Build categories for current sku
                    $this->activePackageProductsCategories[$sku] = array();
                    foreach ($cats as $categoryId) {
                        // Should never be empty because foreign keys are set to product and category entities (even so imports with foreign key checks disabled might cause problems)
                        if (!empty($this->allCategories[$categoryId])) {
                            // Add category id and name to the list of categories for current sku
                            $this->activePackageProductsCategories[$sku][$categoryId] = $this->getCategoryPath($categoryId);
                        }
                    }
                }
            }
        }

        if (is_array($alternateItems = $this->quote->getAlternateItems(null, true, false))) {
            foreach ($alternateItems as $item) {
                $cats = Mage::getResourceModel("catalog/product")->getCategoryIdsWithAnchors($item->getProduct());
                foreach ($cats as $categoryId) {
                    $this->packages[$item->getPackageId()]['contained_categories'][$categoryId] = $this->getCategoryPath($categoryId);
                }
            }
        }

        // Build quote categories
        $this->quoteCategories = array_unique(array_reduce($this->quoteProductsCategories, "array_merge", array()));

        // Build active package categories
        $this->activePackageCategories = array_unique(array_reduce($this->activePackageProductsCategories, "array_merge", array()));

        return $this;
    }

    /**
     * @return Dyna_Package_Model_Package
     */
    public function getActivePackage()
    {
        return $this->activePackage;
    }

    /**
     * @param Dyna_Catalog_Model_Product|Omnius_Catalog_Model_Product $product
     * @param bool $isInjected
     */
    protected function mapCategories($product, $isInjected = false)
    {
        // Preserve product sku
        $sku = $product->getSku();

        if (!$isInjected && (isset($this->quoteProductsCategories[$sku]) || isset($this->activePackageProductsCategories[$sku]))) {
            return;
        }

        // Get all categories for current product
        $cats = Mage::getResourceModel("catalog/product")->getCategoryIdsWithAnchors($product);

        // Add sku to quote list of products
        $this->quoteProducts[$sku] = $sku;

        // Build categories for current sku
        $this->quoteProductsCategories[$sku] = array();
        foreach ($cats as $categoryId) {
            // Should never be empty because foreign keys are set to product and category entities (even so imports with foreign key checks disabled might cause problems)
            if (!empty($this->allCategories[$categoryId])) {
                // Add category id and name to the list of categories for current sku
                $this->quoteProductsCategories[$sku][$categoryId] = $this->getCategoryPath($categoryId);
            }
        }

        if ($isInjected) {
            // Add sku to quote list of products
            $this->activePackageProducts[$sku] = $sku;
            // Build categories for current sku
            $this->activePackageProductsCategories[$sku] = array();
            foreach ($cats as $categoryId) {
                // Should never be empty because foreign keys are set to product and category entities (even so imports with foreign key checks disabled might cause problems)
                if (!empty($this->allCategories[$categoryId])) {
                    // Add category id and name to the list of categories for current sku
                    $this->activePackageProductsCategories[$sku][$categoryId] = $this->getCategoryPath($categoryId);
                }
            }


            $injectedCategories = array_reduce($this->activePackageProductsCategories, "array_merge", array());
            // Build active package categories
            $this->activePackageCategories = array_unique(array_merge($this->activePackageCategories, $injectedCategories));
        }
    }

    /**
     * @return $this
     */
    public function setCategories()
    {
        if (!$this->allCategories = unserialize($this->getCache()->load($this->getCategoriesCachingKey()))) {
            $allCategories = Mage::helper('dyna_catalog')->getAllCategories();

            foreach ($allCategories as $categoryDefinition) {
                $this->allCategories[$categoryDefinition['entity_id']] = array(
                    'name' => trim($categoryDefinition['name']),
                    'path' => $categoryDefinition['path'],
                    'unique_id' => $categoryDefinition['unique_id'],
                    'category_id' => $categoryDefinition['category_id'],
                );
            }

            $this->getCache()->save(serialize($this->allCategories), $this->getCategoriesCachingKey(), array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        foreach ($this->allCategories as $categoryId => $categoryData) {
            $categoryData['category_id'] && $this->categoryIdToPathCache[$categoryData['category_id']] = $this->getCategoryPath($categoryId);
        }

        return $this;
    }

    /**
     * Get category path using category id
     * @param $categoryId
     * @return string
     */
    public function getCategoryPath($categoryId)
    {
        if (!isset($this->categoryIdToStringPathCache[$categoryId])) {
            $categoryPath = $this->allCategories[$categoryId]['path'];
            $categoryPathExploded = explode("/", $categoryPath);

            // Un-setting root and default category
            unset($categoryPathExploded[0]);
            unset($categoryPathExploded[1]);

            $categoryNames = array_map(function ($categoryId) {
                return $this->allCategories[$categoryId]['name'];
            }, $categoryPathExploded);

            $this->categoryIdToStringPathCache[$categoryId] = implode($this->categoryPathSeparator, $categoryNames);
        }


        return $this->categoryIdToStringPathCache[$categoryId];
    }

    /**
     * Get category id by category path
     * @param $categoryPath
     */
    public function getCategoryId($categoryPath)
    {
        foreach ($this->allCategories as $categoryId => $categoryDefinition) {
            if ($categoryDefinition['name'] == $categoryPath) {
                return $categoryId;
            }
        }

        return null;
    }


    /**
     * Get key used for caching category names
     * @return string
     */
    public function getCategoriesCachingKey()
    {
        return static::CATEGORIES_CACHING_KEY;
    }

    /*
    * Return dyna cache for caching parser expressions
    * @return Dyna_Cache_Model_Cache
    */
    public function getCache()
    {
        return $this->dynaCache ?: $this->dynaCache = Mage::getSingleton('dyna_cache/cache');
    }

    /**
     * Determines whether entire cart contains or not product in a certain category
     * @param $categoryName
     * @return array
     */
    public function containsProductInCategory($categoryName, $notExpression = false)
    {
        if ($this->cartEmpty) {
            return array();
        }

        $response = array();

        if (in_array($categoryName, $this->quoteCategories)) {
            // cart contains product in this category, let's get all packages that satisfy this condition
            foreach ($this->packages as $packageId => $packageData) {
                foreach ($this->_getCartPackages() as $package) {
                    if ((int)$package->getPackageId() != (int)$packageId) {
                        continue;
                    }

                    if ($this->currentBundle && !$this->hasProcessContext($package)) {
                        continue 2;
                    }

                    if ($this->currentBundle && $this->currentBundle->isSuso() && $package->isPartOfSuso()) {
                        continue 2;
                    }

                    if ($this->currentBundle && $this->currentBundle->isGigaKombi() && $package->isPartOfGigaKombi()) {
                        continue 2;
                    }
                }
                if (isset($this->packages[$packageId]['categories']) && in_array($categoryName, $this->packages[$packageId]['categories'])) {
                    if (empty($response)) {
                        if (Mage::registry('install_base_expression_result') !== null) {
                            $response = Mage::registry('install_base_expression_result');
                        } else {
                            $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = array();
                        }
                    }
                    if (!$notExpression && !in_array($packageId, $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT])) {
                        $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][] = $packageId;
                    } elseif ($notExpression && in_array($packageId, $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT])) {
                        unset($response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][array_search($packageId, $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT])]);
                    }
                }
            }
            // save response on register only if it is not empty
            if (!empty($response)) {
                if (Mage::registry('install_base_expression_result') !== null) {
                    Mage::unregister('install_base_expression_result');
                }

                Mage::register('install_base_expression_result', $response);
            }

            return $response;
        }

        return array();
    }

    /**
     * Determine whether or not current quote contains a product in a certain category identified by id
     * @param $categoryId
     * @return mixed
     */
    public function containsProductInCategoryId($categoryId, $notExpression = false)
    {
        if (isset($this->categoryIdToPathCache[$categoryId])) {
            return $this->containsProductInCategory($this->categoryIdToPathCache[$categoryId], $notExpression);
        } else {
            return false;
        }
    }

    /**
     * Determine whether or not package contained an install base product in a certain category
     * @param $categoryPath
     * @return bool
     */
    public function packageContainedIBCategory($categoryPath)
    {
        if ($this->cartEmpty) {
            return false;
        }
        foreach ($this->quote->getAlternateItems($this->activePackageId) as $item) {
            foreach ($item->getProduct()->getCategoryIds() as $categoryId) {
                if ($categoryPath == $this->getCategoryPath($categoryId)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Determine whether or not package contained an install base product in a certain category
     * @param $categoryPath
     * @return bool
     */
    public function packageContainedIBCategoryId($id)
    {
        return $this->packageContainedIBCategory($this->categoryIdToPathCache[$id]);
    }

    /**
     * Determine whether or not current package contains at least one install base product in a certain category
     * @param $categoryPath
     * @return bool
     */
    public function packageContainsIBCategory($categoryPath)
    {
        if ($this->cartEmpty || !$this->activePackage) {
            return false;
        }

        foreach ($this->activePackageProductsCategories as $sku => $categories) {
            if (in_array($categoryPath, $categories)) {
                foreach ($this->activePackage->getAllItems() as $item) {
                    if ($item->getSku() == $sku && $item->getIsContract()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Determine whether or not current package contains at least one install base product in a certain category
     * @param $categoryPath
     * @return array
     */
    public function packageContainsIBCategoryId($id)
    {
        return $this->containsProductInCategory($this->categoryIdToPathCache[$id]);
    }

    public function packageContainedProductInCategory($categoryName, $notExpression = false, $certainPackageId = false)
    {
        $certainPackageId = $certainPackageId ?: $this->quote->getActivePackageId();
        // contained_categories
        return isset($this->packages[$certainPackageId]['contained_categories']) && in_array($categoryName, $this->packages[$certainPackageId]['contained_categories']);
    }

    /**
     * Determines whether or not active package contains product in category
     * @param $categoryName
     * @return bool|array
     */
    public function packageContainsProductInCategory($categoryName, $notExpression = false, $certainPackageId = false)
    {
        if ($this->cartEmpty) {
            return false;
        }

        if ((!$certainPackageId && in_array($categoryName, $this->activePackageCategories)) ||
            ($certainPackageId && in_array($categoryName, (!empty($this->packages[$certainPackageId]['categories']) ? $this->packages[$certainPackageId]['categories'] : array())))) {
            if (Mage::registry('install_base_expression_result') !== null) {
                $response = Mage::registry('install_base_expression_result');
            } else {
                $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = array();
            }
            $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][] = $certainPackageId ?: $this->activePackageId;
            $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = array_unique($response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT]);

            if (Mage::registry('install_base_expression_result') !== null) {
                Mage::unregister('install_base_expression_result');
            }

            Mage::register('install_base_expression_result', $response);

            return $response;
        }

        return false;
    }

    /**
     * Determines whether or not active package contains product in category
     * @param $categoryName
     * @return bool|array
     */
    public function packageContainsProductInCategoryId($id, $notExpression = false)
    {
        return $this->packageContainsProductInCategory($this->categoryIdToPathCache[$id], $notExpression);
    }

    /**
     * Determine whether or not active package contains a certain product
     * @param $sku
     * @return bool
     */
    public function packageContainsProduct($sku, $notExpression = false)
    {
        if ($this->cartEmpty) {
            return false;
        }

        if ((!$notExpression && isset($this->activePackageProducts[$sku])) || ($notExpression && !isset($this->activePackageProducts[$sku]))) {
            if (Mage::registry('install_base_expression_result') !== null) {
                $response = Mage::registry('install_base_expression_result');
            } else {
                $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = array();
            }
            $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][] = $this->activePackageId;

            return $response;
        }

        return false;
    }

    /**
     * Determine whether or not cart contained an install base product
     * @param $sku
     */
    public function packageContainedIBProduct($sku)
    {
        if ($this->cartEmpty) {
            return false;
        }

        foreach ($this->quote->getAlternateItems($this->activePackageId) as $item) {
            if ($item->getSku() == $sku) {
                return true;
            }
        }

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->quote->getItemsCollection() as $item) {
            if ($item->isDeleted() && $item->getIsContract() && $item->getSku() === $sku) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether or not active package contains a certain product
     * @param $sku
     * @return bool
     */
    public function packageContainsIBProduct($sku)
    {
        if ($this->cartEmpty || !isset($this->activePackageProducts[$sku])) {
            return false;
        }

        // Return false if item is not in contract
        if ($this->quote->getCartPackage()) {
            foreach ($this->quote->getCartPackage()->getAllItems() as $item) {
                if ($item->getSku() !== $sku) {
                    continue;
                }
                return $item->getIsContract();
            }
        }

        return false;
    }

    /**
     * Determine whether or not current quote contains a certain product
     * @param $sku
     * @return bool
     */
    public function containsProduct($sku)
    {
        if ($this->cartEmpty) {
            return false;
        }

        if (isset($this->quoteProducts[$sku])) {
            if (Mage::registry('install_base_expression_result') !== null) {
                $response = Mage::registry('install_base_expression_result');
            } else {
                $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = array();
            }
            // cart contains product in this category, let's get all packages that satisfy this condition
            foreach ($this->packages as $packageId => $packageData) {
                if (isset($this->packages[$packageId][$sku])) {
                    $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][] = $packageId;
                }
            }

            if (Mage::registry('install_base_expression_result') !== null) {
                Mage::unregister('install_base_expression_result');
            }

            Mage::register('install_base_expression_result', $response);

            return $response;
        }

        return false;
    }

    /*
     * Determine whether or not a certain super order contains product
     * @return bool
     */
    public function orderContainsProduct(string $sku, Dyna_Superorder_Model_Superorder $order)
    {
        foreach ($order->getOrderedItems() as $packageItems) {
            /** @var Omnius_Checkout_Model_Sales_Order_Item $item */
            foreach ($packageItems as $item) {
                if ($item->getSku() == $sku) {
                    return true;
                }
            }
        }

        return false;
    }

    /*
     * Determine whether or not a certain super order contains product in a certain category
     * @return bool
     */
    public function orderContainsProductInCategory(string $categoryPath, Dyna_Superorder_Model_Superorder $order)
    {
        foreach ($order->getOrderedItems() as $packageItems) {
            /** @var Omnius_Checkout_Model_Sales_Order_Item $item */
            foreach ($packageItems as $item) {
                // Category ids are retrieved by product resource through raw query
                foreach ($item->getProduct()->getCategoryIds() as $categoryId) {
                    if ($this->getCategoryPath($categoryId) == $categoryPath) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /*
     * Determine whether or not a certain super order contains product in a certain category
     * @return bool
     */
    public function orderContainsProductInCategoryId(string $id, Dyna_Superorder_Model_Superorder $order)
    {
        return $this->orderContainsProductInCategory($this->categoryIdToPathCache[$id], $order);
    }

    /**
     * Count the number of products in a certain category for Source Expression evaluation
     * @param string $categoryPath
     * @return int
     */
    public function numberOfProductsInCategory($categoryPath)
    {
        if (!in_array($categoryPath, $this->quoteCategories)) {
            $categoryPath = str_replace(
                Dyna_Import_Model_Generator_CategoryDefinitionAbstract::TREE_DELIMITER,
                Dyna_Import_Model_Generator_CategoryDefinitionAbstract::PATH_DELIMITER,
                $categoryPath
            );
        }
        if (!$this->cartEmpty && in_array($categoryPath, $this->quoteCategories)) {
            return $this->quoteCategoriesCounter[$categoryPath] ?? 0;
        }

        return 0;
    }

    public function packageNumberOfProductsInCategory($categoryPath)
    {
        if (!$this->cartEmpty && in_array($categoryPath, $this->activePackageCategories)) {
            return $this->packageCategoriesCounter[$this->activePackageId][$categoryPath] ?? 0;
        }

        return 0;
    }

    public function packageNumberOfProductsInCategoryId($id)
    {
        return $this->numberOfProductsInCategory($this->categoryIdToPathCache[$id]);
    }

    /**
     * Count the number of products in a certain category for Source Expression evaluation
     * @param string $categoryPath
     * @return int
     */
    public function numberOfProductsInCategoryId($id)
    {
        return $this->numberOfProductsInCategory($this->categoryIdToPathCache[$id]);
    }

    /**
     * Determine whether or not current package contains a related product
     * @return bool
     */
    public function packageContainsRelatedProduct()
    {
        return (bool)$this->getPackageRelatedProduct();
    }

    /**
     * Get active package related product
     * @return Dyna_Checkout_Model_Sales_Quote_Item[]
     */
    public function getPackageRelatedProduct()
    {
        $response = array();
        foreach ($this->quote->getAllItems() as $item) {
            if ($item->getProduct()->isRedPlusOwner()) {
                $response[] = $item;
            }
        }

        return $response;
    }

    /**
     * Determine whether or not current related product is in a certain cateyory
     * @return bool
     */
    public function packageRelatedProductInCategory($categoryName)
    {
        $productSku = $this->getPackageRelatedProduct()->getSku();
        if (isset($this->quoteProductsCategories[$productSku]) && in_array($categoryName, $this->quoteProductsCategories[$productSku])) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether or not current related product is in a certain cateyory
     * @return bool
     */
    public function packageRelatedProductInCategoryId($id)
    {
        return $this->packageContainsProductInCategory($this->categoryIdToPathCache[$id]);
    }

    /**
     * Determine the number of packages in cart which have as parent the current active package that contains the related product
     * @param Dyna_Checkout_Model_Sales_Quote_Item $item
     * @return int
     */
    public function maxNumberOfProductsWithParent(array $items, $maxValue)
    {
        $eligible = false;
        $validPackages = array();
        foreach ($items as $item) {
            $workingOnPackage = $this->quote->getCartPackage($item->getPackageId());
            $counted = 0;
            if ($workingOnPackage && $this->currentBundle && $this->hasProcessContext($workingOnPackage)) {
                foreach ($this->_getCartPackages() as $package) {
                    if ($package->getParentId() == $workingOnPackage->getId() && $package->getSharingGroupId() == null) {
                        $counted++;
                    }
                }
                // check if it is modified from install base and count those members too
                if (($parentAccountNumber = $workingOnPackage->getParentAccountNumber()) && ($serviceLineId = $workingOnPackage->getServiceLineId())) {
                    $installBaseProduct = Mage::getSingleton('customer/session')->getCustomer()->getInstalledBaseProduct($parentAccountNumber, $serviceLineId);
                    if ($installBaseProduct['sharing_group_member_type'] === Dyna_Customer_Helper_Customer::SUBSCRIPTION_TYPE_OWNER) {
                        $counted += $installBaseProduct['sharing_group_number_of_members'] ?? 0;
                    }
                }
                if ($counted < $maxValue) {
                    $validPackages[] = $workingOnPackage->getPackageId();
                    $eligible = true;
                }
            }
        }

        $response = array();
        $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = $validPackages;
        Mage::helper('dyna_bundles/expressionLanguage')->resetResponse();
        if (Mage::registry('install_base_expression_result') !== null) {
            Mage::unregister('install_base_expression_result');
        }

        Mage::register('install_base_expression_result', $response);

        return $eligible;
    }

    /**
     * Method that returns the number of packages with a certain install base customer number / product id combination
     * @see It sums the install base packages (members)
     * @return int|false
     * @stores combination of customer number and product id in mage registry
     */
    public function maxNumberOfProductsWithIBParent($maxValue)
    {
        // get install base combinations that matched previous (mandatory) condition
        $installBaseList = Mage::registry('install_base_expression_result');
        $result = array();
        // make sure foundEligible is updated to true if at least one eligible product is found, otherwise, entire bundle will be found not eligible
        $foundEligible = false;

        // if install base list is empty, return false to fail bundle rule evaluation
        if (!$installBaseList || empty($installBaseList[Dyna_Bundles_Helper_Data::EXPRESSION_INSTALL_BASE_PRODUCT])) {
            return false;
        }

        // if no packages in cart, than all combinations stored in registry will be evaluated to true
        if ($cartPackages = $this->_getCartPackages()) {
            foreach ($installBaseList[Dyna_Bundles_Helper_Data::EXPRESSION_INSTALL_BASE_PRODUCT] as $item) {
                $foundBundleWithThisInstallBaseProduct = false;
                $customerNumber = $item[Dyna_Bundles_Helper_Data::INSTALL_BASE_CUSTOMER_NUMBER];
                $productId = $item[Dyna_Bundles_Helper_Data::INSTALL_BASE_PRODUCT_ID];
                $installBaseMembers = (int)$item[Dyna_Bundles_Helper_Data::INSTALL_BASE_NR_MEMBERS];
                // get bundle id from current combination of subscription number - product id
                foreach ($cartPackages as $package) {
                    if ($package->getParentId()) {
                        continue;
                    }
                    // if it is a dummy package, process context doesn't matter
                    if ($this->currentBundle && (!$package->getEditingDisabled() && !$this->hasProcessContext($package))) {
                        continue;
                    }
                    if ($package->getParentAccountNumber() == $customerNumber && $package->getServiceLineId() == $productId) {
                        $foundBundleWithThisInstallBaseProduct = true;
                        $bundleId = ($bundle = $package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS)) ? $bundle->getId() : null;
                        // the bundle exists, evaluate condition
                        if (!$bundleId || ($bundleId && ($installBaseMembers + $this->quote->getNumberOfChildPackagesForIBSubscription($bundleId, $customerNumber, $productId) < $maxValue))) {
                            $foundEligible = true;
                            if (!$package->getEditingDisabled()) {
                                $result[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = $result[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] ?? array();
                                $result[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][] = $package->getPackageId();
                            }
                            $result[Dyna_Bundles_Helper_Data::EXPRESSION_INSTALL_BASE_PRODUCT][] = array(
                                Dyna_Bundles_Helper_Data::INSTALL_BASE_CUSTOMER_NUMBER => $customerNumber,
                                Dyna_Bundles_Helper_Data::INSTALL_BASE_PRODUCT_ID => $productId,

                            );
                        }
                    }
                }

                // no bundle yet created with this install product, so add it to response
                if (!$foundBundleWithThisInstallBaseProduct) {
                    $foundEligible = true;
                    $result[Dyna_Bundles_Helper_Data::EXPRESSION_INSTALL_BASE_PRODUCT][] = array(
                        Dyna_Bundles_Helper_Data::INSTALL_BASE_CUSTOMER_NUMBER => $customerNumber,
                        Dyna_Bundles_Helper_Data::INSTALL_BASE_PRODUCT_ID => $productId,

                    );
                }
            }
        } else {
            $result = $installBaseList;
            $foundEligible = true;
        }

        // replacing previous install base products evaluated to true with the result
        if (Mage::registry('install_base_expression_result') !== null) {
            Mage::unregister('install_base_expression_result');
        }

        Mage::register('install_base_expression_result', $result);

        return $foundEligible;
    }

    /**
     * Checks whether or not active package has a certain package creation type code
     * @param $packageCode
     * @return array
     */
    public function packageCreationTypeId($packageCode)
    {
        /** @var Dyna_Package_Model_PackageCreationTypes $packageType */
        $packageType = $this->getPackageCreationTypeInstance()->getTypeByPackageCode($packageCode);
        if (!$packageType->getId()) {
            return array();
        }

        if (($activePackage = $this->quote->getCartPackage()) && $activePackage->getPackageCreationTypeId() == $packageType->getId()) {
            return $this->packageType($activePackage->getType());
        }

        return array();
    }

    /**
     * Determine whether or not active package in cart is of a certain type
     * @param $type
     * @return array
     */
    public function packageType($type)
    {
        $response = array();

        if (($activePackage = $this->quote->getCartPackage())
            && (strtolower($activePackage->getType()) == strtolower($type))
            && $this->canBeBundled($activePackage)) {
            if (Mage::registry('install_base_expression_result') !== null) {
                $response = Mage::registry('install_base_expression_result');
            } else {
                $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = array();
            }
            $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][] = $activePackage->getPackageId();
            $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_STACK][] = $activePackage->getPackageStack();

            if (Mage::registry('install_base_expression_result') !== null) {
                Mage::unregister('install_base_expression_result');
            }

            Mage::register('install_base_expression_result', $response);
        } else {
            if (Mage::registry('install_base_expression_result') !== null) {
                Mage::unregister('install_base_expression_result');
            }
        }

        return $response;
    }

    /**
     * Determine whether or not current cart contains a certain package creation type
     * @param string $packageCode
     * @return array
     */
    public function containsPackageCreationTypeId(string $packageCode)
    {
        /** @var Dyna_Package_Model_PackageCreationTypes $packageType */
        $packageType = $this->getPackageCreationTypeInstance()->getTypeByPackageCode($packageCode);
        if (!$packageType->getId()) {
            return array();
        }

        foreach ($this->_getCartPackages(true) as $package) {
            if ($package->getPackageCreationTypeId() == $packageType->getId()) {
                return $this->containsPackageCreationType($package->getType());
            }
        }

        return array();
    }

    /**
     * Return the package creation type instance
     * @return Dyna_Package_Model_PackageCreationTypes
     */
    protected function getPackageCreationTypeInstance()
    {
        if ($this->packageCreationType === false) {
            $this->packageCreationType = Mage::getModel('dyna_package/packageCreationTypes');
        }

        return $this->packageCreationType;
    }

    /**
     * Determine whether or not current cart contains a certain package type
     * @param string $packageCode
     * @return array
     */
    public function containsPackageCreationType(string $packageCode)
    {
        $response = array();
        $packageCode = strtoupper($packageCode);
        $cartPackages = $this->_getCartPackages();

        // Multiple Red+ bundles can be created in cart
        if ($cartPackages) {
            foreach ($cartPackages as $package) {
                if ($package->getEditingDisabled()) {
                    continue;
                }

                if (!$this->canBeBundled($package)) {
                    continue;
                }

                $bundle = $package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS);
                if ((!$bundle || ($bundle && $bundle->isRedPlus())) && (strtoupper($package->getType()) == $packageCode)) {
                    if (!isset($response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT])) {
                        if (Mage::registry('install_base_expression_result') !== null) {
                            $response = Mage::registry('install_base_expression_result');
                        } else {
                            $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = array();
                        }
                    }
                    $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][] = $package->getPackageId();
                    $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_STACK][] = $package->getPackageStack();
                }
            }
        }

        if ($response) {
            $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = array_unique($response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT]);
            if (Mage::registry('install_base_expression_result') !== null) {
                Mage::unregister('install_base_expression_result');
            }

            Mage::register('install_base_expression_result', $response);
        }

        return $response;
    }

    /**
     * Checks if the package can be bundled in a GigaKombi and Red+
     *
     * @param Dyna_Package_Model_Package $package
     * @return bool
     */
    protected function canBeBundled($package)
    {
        // check if this product is package is migrated from an install base product, and if so, check if product is bundled and tariff not changed
        if (($parentAccountNumber = $package->getParentAccountNumber()) && ($serviceLineId = $package->getServiceLineId())) {
            $installBaseProduct = Mage::getSingleton('customer/session')->getCustomer()->getInstalledBaseProduct($parentAccountNumber, $serviceLineId);
            if ($installBaseProduct['part_of_bundle'] && !$package->ilsTariffChanged() && !$this->isMigrationInProgress()) {
                return false;
            }
        }

        if ($this->currentBundle && !$this->hasProcessContext($package)) {
            return false;
        }

        if ($this->currentBundle && $package->isPartOfSuso() && $this->currentBundle->isSuso()) {
            return false;
        }

        if ($this->currentBundle && $package->isPartOfGigaKombi() && $this->currentBundle->isGigaKombi()) {
            return false;
        }

        if ($this->currentBundle && $package->isRedPlus()) {
            return false;
        }

        return true;
    }

    protected function isMigrationInProgress()
    {
        $migrationData = explode(",", Mage::getSingleton('checkout/cart')->getCheckoutSession()->getMigrationOrMoveOffnetCustomerNo());
        $migrationParentAccountNumber = $migrationData[0] ?? null;
        $migrationServiceLineId = $migrationData[1] ?? null;
        if ($migrationParentAccountNumber && $migrationServiceLineId) {
            return true;
        }

        return false;
    }


    /**
     * Checks whether the current evaluated bundle applies to the
     * current process context of the specified package
     *
     * @param Dyna_Package_Model_Package $package
     * @return bool
     */
    protected function hasProcessContext(Dyna_Package_Model_Package $package)
    {
        $packageProcessContext = $package->getSaleType();
        $bundleProcessContexts = $this->currentBundle->getProcessContexts();
        $hasProcessContext = false;

        foreach ($bundleProcessContexts as $context) {
            if (strtolower($context->getCode()) == strtolower($packageProcessContext)) {
                $hasProcessContext = true;
                break;
            }
        }

        return $hasProcessContext;
    }

    /**
     * @param Dyna_Bundles_Model_BundleRule $bundle
     * @return $this
     */
    public function setCurrentBundle(Dyna_Bundles_Model_BundleRule $bundle)
    {
        $this->currentBundle = $bundle;

        return $this;
    }

    /**
     * @return null
     */
    public function getCurrentBundle()
    {
        return $this->currentBundle;
    }

    /**
     * Clears the current bundle
     *
     * @return $this
     */
    public function clearCurrentBundle()
    {
        $this->currentBundle = null;

        return $this;
    }

    /**
     * Determine whether or not quote contained a certain product before save
     * @param $productSku
     * @return bool
     */
    public function containedProduct($productSku)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->quote->getItemsCollection() as $item) {
            if ($item->isDeleted() && $productSku == $item->getSku()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether or not cart contained a product in a certain category
     * @param $categoryPath
     */
    public function containedProductInCategory($categoryPath)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->quote->getItemsCollection() as $item) {
            if ($item->isDeleted()) {
                foreach ($item->getProduct()->getCategoryIds() as $categoryId) {
                    if ($this->getCategoryPath($categoryId) == $categoryPath) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Determine whether or not cart contained a product in a certain category
     * @param $categoryPath
     */
    public function containedProductInCategoryId($id)
    {
        return $this->containedProductInCategory($this->categoryIdToPathCache[$id]);
    }

    /**
     * Register a message on Magento
     * @param $message
     * @return bool
     */
    public function showMessage($message)
    {
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');

        // register these messages on customer session
        $customerSession->addHint(array(
            'message' => $message,
            // make sure it arrives int in response
            'ruleId' => $this->getRule() ? (int)$this->getRule()->getId() : false,
            'hintOnce' => $this->getRule() ? $this->getRule()->getShowHint() == Dyna_PriceRules_Model_Validator::HINT_ONCE : Dyna_PriceRules_Model_Validator::HINT_ONCE,
        ), $this->getRule() ? (int)$this->getRule()->getId() : false);

        return false;
    }

    /**
     * Getter for already set rule id on current instace
     * @return Mage_SalesRule_Model_Rule
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Setter for price rule id set on current instance
     * @param $ruleId
     */
    public function setRule(Mage_SalesRule_Model_Rule $rule)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Determine whether or not current package contained a product in a certain family
     * @param string $familyIdentifier
     * @return bool
     */
    public function packageContainedInstalledBaseProductInFamily(string $familyIdentifier)
    {
        $familyId = Dyna_Catalog_Model_ProductFamily::getProductFamilyIdByCode($familyIdentifier);

        if ($this->activePackage) {
            /** @var Dyna_Checkout_Model_Sales_Order_Item $item */
            foreach ($this->activePackage->getAllItems(true) as $item) {
                if ($item->getIsContract() && $item->getIsContractDrop() && in_array($familyId, explode(",", $item->getProduct()->getProductFamily()))) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Determine whether or not current package contains a product in a certain family
     * @param string $familyIdentifier
     * @return bool
     */
    public function packageContainsInstalledBaseProductInFamily(string $familyIdentifier)
    {
        $familyId = Dyna_Catalog_Model_ProductFamily::getProductFamilyIdByCode($familyIdentifier);

        if ($this->activePackage) {
            /** @var Dyna_Checkout_Model_Sales_Order_Item $item */
            foreach ($this->activePackage->getAllItems() as $item) {
                if ($item->getIsContract() && in_array($familyId, explode(",", $item->getProduct()->getProductFamily()))
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns removed installed base product ids of products belonging to a specific category
     * @param $categoryPath
     * @param $returnDeleted
     * @return int[]
     */
    public function getPackageIBItems($categoryPath = null, $returnDeleted = false, $returnAll = false)
    {
        $ids = array();

        if ($returnDeleted) {
            $items = $this->getRule()
                ? Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection()
                : Mage::getSingleton('checkout/session')->getQuote()->getAllItems(true, true);
        } else {
            $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
        }

        foreach ($items as $item) {
            if ($returnDeleted) {
                if (($item->isDeleted() && $item->getIsContract() && $item->getQuoteId())
                    || ($item->getAlternateQuoteId() && $item->getIsContract())
                ) {
                    if (isset($categoryPath)) {
                        foreach ($item->getProduct()->getCategoryIds() as $categoryId) {
                            if ($this->getCategoryPath($categoryId) == $categoryPath) {
                                $ids[] = $item->getProductId();
                            }
                        }
                    } else {
                        $ids[] = $item->getProductId();
                    }
                }
            } else {
                if ($item->getIsContract() && $item->getQuoteId()) {
                    if (isset($categoryPath)) {
                        foreach ($item->getProduct()->getCategoryIds() as $categoryId) {
                            if ($this->getCategoryPath($categoryId) == $categoryPath) {
                                $ids[] = $item->getProductId();
                            }
                        }
                    } else {
                        $ids[] = $item->getProductId();
                    }
                }
            }
        }

        return array_combine(array_unique($ids), array_unique($ids));
    }

    /**
     * Return product ids of option type items that are in minimum duration
     * @param bool $includeDeleted
     * @return int[]
     */
    public function getOptionsInMinimumDuration($includeDeleted = false)
    {
        $ids = array();

        if ($includeDeleted) {
            $items = $this->getRule()
                ? Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection()
                : Mage::getSingleton('checkout/session')->getQuote()->getAllItems(true, true);
        } else {
            $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
        }

        foreach ($items as $item) {
            if ($item->getInMinimumDuration() && $item->getProduct()->isOption()) {
                $ids[] = $item->getProductId();
            }
        }

        return array_combine(array_unique($ids), array_unique($ids));
    }

    /**
     * Cache quote packages to avoid multiple calls to db
     * @param $excludeDummy
     * @return bool|Dyna_Package_Model_Package[]
     */
    protected function _getCartPackages($excludeDummy = false)
    {
        return $this->quote->getCartPackages($excludeDummy);
    }

    /**
     * Load product info for the particular product
     * @param object $product
     * @return object
     */
    private function loadProductForInjected($product)
    {
        return Mage::getModel('catalog/product')->load($product->getId());
    }
}
