<?php

class Dyna_Configurator_Helper_Services extends Mage_Core_Helper_Abstract
{
    protected $cache = null;
    /**
     * Check the compatibility between own SmartCard and own/new Receiver
     * @return array
     */
    public function checkSmartcardReceiverCompatibility()
    {
        // Get customer model
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/session')->getCustomer();

        $response = ['error' => null];

        /** @var Dyna_Configurator_Model_Client_SmartCardCompatibilityClient $compatibilityClient */
        $compatibilityClient = Mage::getModel('dyna_configurator/client_smartCardCompatibilityClient');

        // Service call is performed only when own SmartCard
        if ($ownSmartCard = $customer->getOwnSmartCard()) {
            $serialNumber = $ownSmartCard->getSerialNumber();

            // If customer has already selected an own receiver
            if ($ownReceiver = $customer->getOwnReceiver()) {
                // Execute compatibility between own SmartCard and own Receiver
                $requestData = ["SmartCardSerialNumber" => $serialNumber, "JobCode" => null, "EquipmentSerialNumber" => $ownReceiver->getSerialNumber()];
                $responseData = $compatibilityClient->executeGetSmartCardCompatibility($requestData);

                if (!$responseData['ROOT']['DATA']['Compatibility'] || filter_var($responseData['ROOT']['DATA']['Compatibility'], FILTER_VALIDATE_BOOLEAN) === false) {
                    $response = $this->getErrorResponse($this->__("SmartCard is not compatible with your own receiver."));
                } else {
                    $response = [
                        'error' => false,
                        'data' => $responseData['ROOT']['DATA']
                    ];
                }

            } else {
                // Not own receiver case
                $receiverCode = null;

                $productTypeAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'type');
                $deviceValueId = Mage::helper('dyna_catalog')->getAttributeAdminLabel($productTypeAttribute, null, 'Device');

                /** @var Dyna_Checkout_Model_Sales_Quote $cart */
                $cart = Mage::getModel('checkout/cart')->getQuote();
                // Get all products in cart and check if they are receivers
                foreach ($cart->getAllItems() as $item) {
                    // Get first receiver in the package
                    if ($item->getPackageId() == $cart->getActivePackageId()
                        && $item->getProduct()->getSubType() == 'RC'
                        && $item->getProduct()->getData('type') == $deviceValueId // use getData() as getType() returns other values
                    ) {
                        $receiverCode = $item->getProduct()->getProductCode();
                        break;
                    }
                }

                if ($receiverCode) {
                    // Execute GetSmartCardCompatibility between own SmartCard and selected Receiver
                    $requestData = ["SmartCardSerialNumber" => $serialNumber, "JobCode" => $receiverCode, "EquipmentSerialNumber" => null];
                    $responseData = $compatibilityClient->executeGetSmartCardCompatibility($requestData);

                    if (!$responseData['ROOT']['DATA']['Compatibility'] || filter_var($responseData['ROOT']['DATA']['Compatibility'], FILTER_VALIDATE_BOOLEAN) === false) {
                        $response = $this->getErrorResponse($this->__("SmartCard is not compatible with the selected receiver."));
                    } else {
                        $response = [
                            'error' => false,
                            'data' => $responseData['ROOT']['DATA']
                        ];
                    }
                }
            }
        }

        return $response;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->cache) {
            $this->cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->cache;
    }

    /**
     * GetInvalidProducts service call for cable products
     * @param $packageType
     * @param $availableProducts
     * @return array
     */
    public function getInvalidProducts($packageType)
    {
        $cacheKey = 'invalid_products_for_' . $packageType;
        $invalidProducts = $this->getCache()->load($cacheKey);

        if ($invalidProducts) {
            return unserialize($invalidProducts);
        } else {
            $invalidProducts = [];
            $ospProducts = [];

            // Get all products with tag filterByOSP (only when creating the package = no products selected)
            $filterByOSP = Mage::getModel('dyna_configurator/catalog')->getProductsByTag('filterByOSP', ['packageType' => $packageType]);
            foreach ($filterByOSP as $ospProduct) {
                $ospProducts[] = $ospProduct->getSku();
            }

            if (count($ospProducts)) {
                /** @var Dyna_Configurator_Model_Client_InvalidProductsClient $client */
                $client = Mage::getModel('dyna_configurator/client_InvalidProductsClient');
                $addressId = Mage::getSingleton('dyna_address/storage')->getAddressId();

                $params = [
                    'AddressId' => $addressId,
                    'ProductCodes' => implode(',', $ospProducts),
                ];
                // Execute GetInvalidProducts service call
                $ospResponse = $client->executeGetInvalidProducts($params);

                if (! empty($ospResponse['ROOT']['DATA']['ProductCodes'])) {
                    $tmpInvalidProducts = explode(',', $ospResponse['ROOT']['DATA']['ProductCodes']);
                    /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
                    $rulesHelper = Mage::helper('dyna_configurator/rules');

                    foreach ($tmpInvalidProducts as $sku) {
                        $invalidProducts[$sku] =  $rulesHelper->getProductIdBySku($sku);
                    }
                }
            }

            $this->getCache()->save(serialize($invalidProducts), $cacheKey);

            return $invalidProducts;
        }

    }

    /**
     * GetNonStandardRates service call for cable products
     * @param $packageType
     * @param $availableProducts
     * @return mixed
     */
    public function getNonStandardRates($packageType, $availableProducts)
    {
        $result = [];
        $params = [];

        $cacheKey = sprintf('non_standard_rates_for_%s_%s', $packageType, md5(implode(',', $availableProducts)));

        if ($nonStandard = $this->getCache()->load($cacheKey)) {
            return unserialize($nonStandard);
        } else {

            /** @var Dyna_Configurator_Model_Catalog $productsModel */
            $productsModel =  Mage::getModel('dyna_configurator/catalog')->setPackageType($packageType);

            $products = $productsModel->getAllProducts(array('req_rate_prod_for_contracts', 'req_rate_min_bound', 'req_rate_max_bound'));
            /** @var Dyna_Catalog_Model_Product $product */
            foreach ($products as &$product) {
                if (!empty($reqRates = $product->getData('req_rate_prod_for_contracts')) && in_array($product->getId(), $availableProducts)) {
                    $sku = $product->getData('sku');
                    $params[$sku] = json_decode($reqRates);
                    $bounds[$sku] = [
                        'min' => $product->getData('req_rate_min_bound'),
                        'max' => $product->getData('req_rate_max_bound'),
                    ];
                }
                $product->clearInstance();
                unset($product);
            }
            unset($products);

            if (count($params)) {
                /** @var Dyna_Configurator_Model_Client_NonStandardRatesClient $client */
                $client = Mage::getModel('dyna_configurator/client_nonStandardRatesClient');
                $serviceResults = $client->executeGetNonStandardRates($params);

                if (!empty($serviceResults['NonStandardRates'])) {
                    /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
                    $rulesHelper = Mage::helper('dyna_configurator/rules');
                    /** @var Mage_Tax_Helper_Data $_taxHelper */
                    $_taxHelper = Mage::helper('tax');
                    foreach ($serviceResults['NonStandardRates'] as $rate) {
                        if ($rate['NonStandardRate'] >= $bounds[$sku]['min'] && $rate['NonStandardRate'] <= $bounds[$sku]['max']) {
                            $prodId = $rulesHelper->getProductIdBySku($rate['ProductCode']);
                            // Load product model
                            /** @var Dyna_Catalog_Model_Product $product */
                            $product = Mage::getModel('catalog/product')->load($prodId);

                            $result[$prodId] = [
                                'price' => $_taxHelper->getPrice($product, $rate['NonStandardRate'], false, null, null, null, null, false),
                                'price_with_tax' => $_taxHelper->getPrice($product, $rate['NonStandardRate'], true, null, null, null, null, false),
                            ];
                        }
                    }
                }
            }

            $this->getCache()->save(serialize($result), $cacheKey);

            return $result;
        }
    }

    /**
     * Generic method that returns an error response message
     * @param $message
     * @return array
     */
    protected function getErrorResponse($message)
    {
        return [
            'error' => true,
            'message' => $message,
        ];
    }

    public function getSpecialPriceProducts()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackage = $quote->getCartPackage();
        $websiteId = Mage::app()->getWebsite()->getId();
        $cacheKey = "SPECIAL_PRODUCTS_FOR_" . strtoupper($activePackage->getType()) . "_" . $websiteId;

        if (!$products = unserialize($this->getCache()->load($cacheKey))) {
            $packageType = $activePackage->getType();
            // get type attribute
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $typeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageType);
            // get service value attribute
            $useServiceAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'use_service_value');
            $useServiceValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($useServiceAttribute, null, "True");
            $type = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE;

            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('use_service_value')
                ->addAttributeToSelect('service_xpath')
                ->addAttributeToSelect('service_name')
                ->setStoreId(Mage::app()->getStore())
                ->addWebsiteFilter($websiteId)
                ->addTaxPercents()
                ->addPriceData(null, $websiteId)
                ->addAttributeToFilter('type_id', $type)
                ->addAttributeToFilter('is_deleted', array('neq' => 1))
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, $typeValue);

            $products = array();
            /** @var Dyna_Catalog_Model_Product $product */
            foreach ($collection as $product) {
                if ($product->getUseServiceValue() != $useServiceValue) {
                    continue;
                }

                $products[(int)$product->getId()] = (new Varien_Object())->addData(array(
                    'service_name' => $product->getServiceName(),
                    'xpath' => $product->getServiceXpath(),
                ));
            }

            $this->getCache()->save(serialize($products), $cacheKey, array($this->getCache()::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        $response = array();
        /** @var Varien_Object $product built above */
        foreach ($products as $id => $product) {
            if (($serviceValue = $activePackage->getServiceValue($product->getServiceName(), $product->getXpath())) && $serviceValue !== null) {
                $response['valid'][$id] = $serviceValue;
            } else {
                $response['invalid'][$id] = array(
                    'serviceName' => $product->getServiceName(),
                    'xpath' => $product->getXpath(),
                );
            }
        }

        return $response;
    }
}
