<?php

/**
 * Class Dyna_Checkout_Block_Cart_Steps_SaveOther
 */
class Dyna_Checkout_Block_Cart_Steps_SaveOther extends Omnius_Checkout_Block_Cart
{
    use Dyna_Checkout_Block_Cart_Steps_Common;

    // Phone entry type options
    const PHONE_ENTRY_TYPE_TELEPHONE = 'Telephone';
    const PHONE_ENTRY_TYPE_FAX = 'Fax';
    const PHONE_ENTRY_TYPE_TELEPHONE_FAX = 'Telephone and Fax';
    const PHONE_ENTRY_TYPE_NO_ENTRY = 'No entry';

    // phonebook entry choices
    const PHONEBOOK_CHOICE_STANDARD = 3;
    const PHONEBOOK_CHOICE_NO_ENTRIES = 1;
    const PHONEBOOK_CHOICE_EXTENDED = 2;

    // Single connection choices
    const NO_SINGLE_CONNECTION = 1;
    const SINGLE_CONNECTION_FULL_NUMBER = 2;
    const SINGLE_CONNECTION_SHORT_NUMBER = 3;

    // Data connection choices
    const SAVE_DATA_CONNECTION = 1;
    const DELETE_DATA_CONNECTION = 2;

    private $readyToViewProduct = null;

    public function getTelephoneInfoOptions()
    {
        return [
            'None',
            'Only phone',
            'Full entry'
        ];
    }

    public function getTelephoneProviders()
    {
        /** Return list of providers */
        return [
            'Telephone provider 1',
            'Telephone provider 2',
            'Telephone provider 3',
            'Telephone provider 4',
            'Telephone provider 5',
        ];
    }

    /**
     * Get notes from getConfigurationData service call
     * @return array
     */
    public function getNoteTypes()
    {
        $configurationData = Mage::getModel('dyna_configurator/configuration')
            ->getConfigurationData();

        $notesData = $configurationData->getComments();

        usort($notesData, function ($a, $b) {
            return $a['sort'] > $b['sort'];
        });

        return $notesData;
    }

    public function getInfoExchangeOptions()
    {
        return [
            'Everything',
            'Information on the phone number',
            'No Information',
        ];
    }

    public function getDataConnectionChoices()
    {
        return [
            self::SAVE_DATA_CONNECTION,
            self::DELETE_DATA_CONNECTION
        ];
    }


    public function getKeywordOptions()
    {
        return [
            '-',
            '[Defined entry]',
            '[Defined entry]',
            '[Defined entry]',
            '[Defined entry]',
        ];
    }

    /**
     * Gets phonebook entry type options
     * @return array
     */
    public function getPhoneEntryTypeOptions()
    {
        if ($this->hasFixedTypePackage()) {
            return $keywordOptions = [
                self::PHONE_ENTRY_TYPE_TELEPHONE,
                self::PHONE_ENTRY_TYPE_FAX,
                self::PHONE_ENTRY_TYPE_TELEPHONE_FAX,
                self::PHONE_ENTRY_TYPE_NO_ENTRY
            ];
        } else if ($this->hasCable()) {
            return $keywordOptions = [
                self::PHONE_ENTRY_TYPE_TELEPHONE,
                self::PHONE_ENTRY_TYPE_NO_ENTRY,
            ];
        }

        return [];
    }

    public function getPhoneEntryTypeOptionsForPackageType($type)
    {
        if(in_array(strtolower($type), Dyna_Catalog_Model_Type::getCablePackages()))
        {
            return $keywordOptions = [
                self::PHONE_ENTRY_TYPE_TELEPHONE,
                self::PHONE_ENTRY_TYPE_NO_ENTRY,
            ];
        } else if (in_array(strtolower($type), Dyna_Catalog_Model_Type::getDslPackages())) {
            return $keywordOptions = [
                self::PHONE_ENTRY_TYPE_TELEPHONE,
                self::PHONE_ENTRY_TYPE_NO_ENTRY
            ];
        } else if (in_array(strtolower($type), Dyna_Catalog_Model_Type::getLtePackages())) {
            return $keywordOptions = [
                self::PHONE_ENTRY_TYPE_TELEPHONE,
                self::PHONE_ENTRY_TYPE_FAX,
                self::PHONE_ENTRY_TYPE_TELEPHONE_FAX,
                self::PHONE_ENTRY_TYPE_NO_ENTRY
            ];
        }

        return [];
    }

    /**
     * Gets phonebook choices
     * @return array
     */
    public function getPhonebookChoices()
    {
        return [
            self::PHONEBOOK_CHOICE_NO_ENTRIES => $this->__('No phone book entries'),
            self::PHONEBOOK_CHOICE_STANDARD => $this->__('Standard (Name/Address)'),
            self::PHONEBOOK_CHOICE_EXTENDED => $this->__('Extended')
        ];
    }

    /**
     * Gets phonebook choices
     * @return array
     */
    public function getPhonebookItemsPerChoice()
    {
        return [
            self::PHONEBOOK_CHOICE_STANDARD => implode(',', Dyna_Catalog_Model_Type::getPhonebookStandard()),
            self::PHONEBOOK_CHOICE_NO_ENTRIES => Dyna_Catalog_Model_Type::getPhonebookNoEntries(),
            self::PHONEBOOK_CHOICE_EXTENDED => Dyna_Catalog_Model_Type::getPhonebookOneEntry()
        ];
    }

    /**
     * Gets phonebook choices
     * @return array
     */
    public function getSingleConnectionChoices()
    {
        return [
            self::NO_SINGLE_CONNECTION => $this->hasMobileTypePackage() ? $this->__('No connection') : $this->__('None'),
            self::SINGLE_CONNECTION_FULL_NUMBER => $this->hasMobileTypePackage() ? $this->__('Mini with full destination number (free of charge)') : $this->__('Full destination number'),
            self::SINGLE_CONNECTION_SHORT_NUMBER => $this->hasMobileTypePackage() ? $this->__('Mini with shortened destination number (free of charge)') : $this->__('Shortened destination number')
        ];
    }

    /**
     * Gets individual choices
     * @return array
     */
    public function getIndividualConnectionChoices()
    {
        return [
            self::NO_SINGLE_CONNECTION => $this->__('None'),
            self::SINGLE_CONNECTION_FULL_NUMBER => $this->__('Full destination number'),
            self::SINGLE_CONNECTION_SHORT_NUMBER => $this->__('Shortened destination number')
        ];
    }

    /**
     * Check if there is at least a package of type DSL in the cart
     *
     * @return bool
     */
    public function hasDsl()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            Dyna_Catalog_Model_Type::TYPE_FIXED_DSL
        );

        /** @var Dyna_Package_Model_Package $package */
//        foreach ($this->getPackages() as $package) {
//            if (strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL)) {
//                return true;
//            }
//        }
//
//        return false;
    }

    /**
     * Check if there is at least a package of type LTE in the cart
     *
     * @return bool
     */
    public function hasLTE()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            Dyna_Catalog_Model_Type::TYPE_LTE
        );

        /** @var Dyna_Package_Model_Package $package */
//        foreach ($this->getPackages() as $package) {
//            if (strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_LTE)) {
//                return true;
//            }
//        }
//
//        return false;
    }
}