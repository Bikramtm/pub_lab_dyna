<?php

class Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress extends Omnius_Checkout_Block_Cart
{
    use Dyna_Checkout_Block_Cart_Steps_Common;
    //OMNVFDE-1123
    const DELIVERY_SAME_DAY_18_20 = 1;
    const DELIVERY_SAME_DAY_19_21 = 2;
    const DELIVERY_SPECIFIED_ADDRESS = 'deliver';
    const DELIVERY_SPLIT_DELIVERY = 'split';
    const DELIVERY_SERVICE_ADDRESS = 'service';
    const DELIVERY_OTHER_ADDRESS = 'other_address';
    const DELIVERY_PICKUP_VODAFONE_SHOP = 'pickup';

    /**
     * Get selected package type as string
     * @return string
     */
    public function getPackageType()
    {
        return $this->getQuote()->getPackageType();
    }

    public function hasSubscriptionProductOnPrepaidPackage()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if ($package->hasSubscriptionProductOnPrepaidPackage()) {
                return true;
            }
        }

        return false;
    }

    public function hasHardwareProductOnPrepaidPackage()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if ($package->hasHardwareProductOnPrepaidPackage()) {
                return true;
            }
        }

        return false;
    }


    /**
     * Checks whether order contains cable / DSL packages
     * @return bool
     */
    public function hasCableTypePackage()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            array_merge(Dyna_Catalog_Model_Type::getCablePackages(), Dyna_Catalog_Model_Type::getFixedPackages()),
            true
        );

        /** @var Dyna_Package_Model_Package $package */
//        foreach ($this->getPackages() as $package) {
//            if (!$package->getEditingDisabled() && in_array(strtolower($package->getType()), array_merge(Dyna_Catalog_Model_Type::getCablePackages(), Dyna_Catalog_Model_Type::getFixedPackages()))) {
//                return true;
//            }
//        }
//
//        return false;
    }

    /**
     * Check if there is atleast a package of type fixed in the cart
     *
     * @return bool
     */
    public function hasDsl()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            Dyna_Catalog_Model_Type::getFixedPackages()
        );

        /** @var Dyna_Package_Model_Package $package */
//        foreach ($this->getPackages() as $package) {
//            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getFixedPackages())) {
//                return true;
//            }
//        }
//
//        return false;
    }

    /**
     * Check if there is atleast a package of type mobile (postpaid) in the cart
     *
     * @return bool
     */
    public function hasMobile()
    {
        return $this->hasMobileTypePackage(Dyna_Catalog_Model_Type::TYPE_MOBILE);
    }

    /**
     * Check if there is atleast a package of type prepaid in the cart
     *
     * @return bool
     */
    public function hasPrepaid()
    {
        return $this->hasMobileTypePackage(Dyna_Catalog_Model_Type::TYPE_PREPAID);
    }

    /**
     * @param bool $addDistrict
     * @return array
     */
    public function getDeliveryAddress($addDistrict = false)
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        return [
            ( $customer->getSalutationString() != "" ? $customer->getSalutationString() . " " : "" ) . $customer->getName(),
            $customer->getStreet() . " " . $customer->getHouseNo(),
            $customer->getPostalCode() . " " . $customer->getCity() . (($addDistrict) ? " " . $customer->getDistrict() : ""),
        ];
    }

    /**
     * @return array
     */
    public function getDeliveryAddressFields()
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        
        return [
            "postcode" => $customer->getPostalCode(),
            "street" => $customer->getStreet(),
            "houseno" => $customer->getHouseNo(),
            "city" => $customer->getCity(),
            "addition" => $customer->getDistrict()
            ];
    }

    /**
     * @param bool $asJson
     * @return object|string
     */
    public function getVodafoneShipToStoresAddresses($asJson = false)
    {
        /**
         * @var $helper Dyna_AgentDE_Helper_Data
         */
        $helper = Mage::helper('agentde');
        return $helper->getVodafoneShipToStoresAddresses($asJson);
    }

    public function hasSimOnly()
    {
        $simOnlyCategory = Mage::getModel('catalog/category')->getCategoryByNamePath( 'Mobile/ALL_SIMONLY_TO', '/' );

        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            $items = $package->getData('items');

            foreach ($items as $item) {
                $catalogProduct = Mage::getModel('catalog/product')->load($item->getProduct()->getId());
                $productCategories = $catalogProduct->getCategoryIds();
                if( $item->getProduct()->isSubscription() ) {
                    if ( ( $package->isPrepaid() && empty( $productCategories ) || in_array( $simOnlyCategory->getId(), $productCategories ) ) ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }


}
