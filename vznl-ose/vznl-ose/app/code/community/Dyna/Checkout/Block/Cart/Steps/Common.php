<?php

/**
 * trait Dyna_Checkout_Block_Cart_Steps_Common
 */
trait Dyna_Checkout_Block_Cart_Steps_Common
{
    /** @var array */
    protected $packages = null;
    /** @var  Dyna_Customer_Model_Customer */
    protected $customer;
    protected $session;

    /**
     * @return array
     */
    public function getCountries()
    {
        $config_countries = Mage::getConfig()->getNode('default/checkout/vodafoneoptions/vfcountrylist');

        $line = strtok($config_countries, "\r\n");
        $countries = [];
        while ($line !== false) {
            $countries[explode(':',$line)[0]] = explode(':',$line)[1];
            $line = strtok("\r\n");
        }

        return $countries;
    }

    /**
     * Check if there is at least a package of type cable, dsl , lte in the cart
     *
     * @return bool
     */
    public function hasCableOrFixedTypePackage()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            array_merge(
                Dyna_Catalog_Model_Type::getCablePackages(),
                Dyna_Catalog_Model_Type::getFixedPackages()
            ),
            true
        );

        /** @var Dyna_Package_Model_Package $package */
//        foreach ($this->getPackages() as $package) {
//            if (!$package->getEditingDisabled() && in_array(strtolower($package->getType()), array_merge(
//                Dyna_Catalog_Model_Type::getCablePackages(),
//                Dyna_Catalog_Model_Type::getFixedPackages()
//            ))) {
//                return true;
//            }
//        }
//
//        return false;
    }

    /**
     * Check if there is at least a package of type postpaid mobile
     *
     * @return bool
     */
    public function hasPostPaidMobileTypePackage()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            array_merge(
                Dyna_Catalog_Model_Type::getPostPaidMobilePackages()
            ),
            true
        );
    }

    /**
     * Retrieve the quote packages
     *
     * @return array
     */
    public function getPackages($includeInstalledBase = false)
    {
        if ($this->packages === null) {
            /** @var Dyna_Checkout_Model_Sales_Quote $quoteModel */
            $quoteModel = Mage::getSingleton('checkout/cart')->getQuote();

            /** @var Dyna_Package_Model_Package $packageModel */
            $packageModel = Mage::getModel('package/package');
            $tempPackages = $packageModel->getPackages(null, $quoteModel->getId());

            $quotePackages = $quoteModel->getPackages();
            $this->packages = [];
            foreach ($tempPackages as $package) {
                if ($package->getEditingDisabled() && !$includeInstalledBase) {
                    continue;
                }
                $package->setData('items', $quotePackages[$package->getPackageId()]['items']);
                $this->packages[] = $package;
            }
        }

        return $this->packages;
    }

    /**
     * Check if there is at least a package of type fixed in the cart
     *
     * @return bool
     */
    public function hasFixedTypePackage()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            Dyna_Catalog_Model_Type::getFixedPackages()
        );

        /** @var Dyna_Package_Model_Package $package */
//        foreach ($this->getPackages() as $package) {
//            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getFixedPackages())) {
//                return true;
//            }
//        }
//
//        return false;
    }

    /**
     * Retrieve session loaded customer
     * @return Dyna_Customer_Model_Customer
     */
    public function getSession()
    {
        if (!$this->session) {
            $this->session = Mage::getSingleton('dyna_customer/session');
        }

        return $this->session;
    }

    /**
     * Get address from storage either as a string ($isFullAddress = true) or by parts
     * @param bool $isFullAddress
     * @return array
     */
    public function getServiceAddressDetails($isFullAddress = false)
    {
        if ($isFullAddress) {
            return $this->getAddressStorage()
                ->getServiceAddress(true);
        } else {
            return $this->getAddressStorage()
                ->getServiceAddressParts();
        }
    }

    /**
     * Get the storage model for serviceability details address
     * @return Dyna_Address_Model_Storage
     */
    public function getAddressStorage()
    {
        return Mage::getModel("dyna_address/storage");
    }

    /**
     * @return bool
     */
    public function getHasStudentDiscount()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::getMobileStudentDiscount()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getHasDisabledDiscount()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::getMobileDisabledDiscount()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getHasYoungDiscount()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::getMobileYoungDiscount()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getHasYouBiageOption() {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::getYouBiageOption()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getHasComfortConnection()
    {
        $comfortAccessCategory = Mage::getResourceModel('catalog/category_collection')
            ->addFieldToFilter('name', Dyna_Catalog_Model_Type::COMFORT_CONNECTION_CATEGORY)
            ->getFirstItem();

        if ($comfortAccessId = $comfortAccessCategory->getId()) {
            foreach ($this->getPackages() as $package) {
                $items = $package['items'];
                foreach ($items as $item) {
                    /** @var Dyna_Catalog_Model_Product $product */
                    $productCategories = Mage::getModel('catalog/product')->load($item->getData()['product_id'])->getCategoryIds();

                    if (in_array($comfortAccessId, $productCategories)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getHasBasicConnection()
    {
        $basicAccessCategory = Mage::getResourceModel('catalog/category_collection')
            ->addFieldToFilter('name', Dyna_Catalog_Model_Type::BASIC_CONNECTION_CATEGORY)
            ->getFirstItem();

        if ($basicAccessId = $basicAccessCategory->getId()) {
            foreach ($this->getPackages() as $package) {
                $items = $package['items'];
                foreach ($items as $item) {
                    /** @var Dyna_Catalog_Model_Product $product */
                    $productCategories = Mage::getModel('catalog/product')->load($item->getData()['product_id'])->getCategoryIds();

                    if (in_array($basicAccessId, $productCategories)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getHasInstallationInstructions()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::INSTALL_INSTRUCTIONS_CABLE_SKU) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if there is at least a package of type dsl in the cart
     *
     * @return bool
     */
    public function hasDsl()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            Dyna_Catalog_Model_Type::getDslPackages()
        );
    }

    /**
     * Check if the cart exists of dsl packages only
     * @return bool
     */
    public function isDslOnly()
    {
        foreach ($this->getPackages() as $package) {
            if (strtolower($package->getType()) != strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check if there is at least a package of type lte in the cart
     *
     * @return bool
     */
    public function hasLte()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            Dyna_Catalog_Model_Type::getLtePackages()
        );

        /** @var Dyna_Package_Model_Package $package */
//        foreach ($this->getPackages() as $package) {
//            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getLtePackages())) {
//                return true;
//            }
//        }
//
//        return false;
    }

    /**
     * Check if there is atleast a package of type cable in the cart
     *
     * @return bool
     */
    public function hasCable()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            Dyna_Catalog_Model_Type::getCablePackages(),
            true
        );

        /** @var Dyna_Package_Model_Package $package */
//        foreach ($this->getPackages() as $package) {
//            if (!$package->getEditingDisabled() && in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getCablePackages())) {
//                return true;
//            }
//        }
//
//        return false;
    }

    /**
     * Check if there is atleast a package of type mobile (postpaid) in the cart
     *
     * @return bool
     */
    public function hasMobile()
    {
        return $this->hasMobileTypePackage(Dyna_Catalog_Model_Type::TYPE_MOBILE);
    }

    /**
    +     * Checks if the order only exists of mobile packages
    +     */
    public function isMobileOnly()
    {
        foreach ($this->getPackages() as $package){
            if(strtolower($package->getType()) != strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)){
                return false;
            }
        }

        return true;
    }

    /**
     * Check if there is atleast a package of type prepaid in the cart
     *
     * @return bool
     */
    public function hasPrepaid()
    {
        return $this->hasMobileTypePackage(Dyna_Catalog_Model_Type::TYPE_PREPAID);
    }


    /**
     * Checks whether order contains a certain mobile package type
     * @param null $type
     * @return bool
     */
    public function hasPackageOfType($type = null)
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            $type
        );
    }

    /**
     * Checks whether order contains a package type in ILS process context
     *
     * @return bool
     */
    public function getAllPackagesContext()
    {
        foreach($this->getPackages() as $package) {
            $result[] = strtoupper($package->getSaleType());
        }

        return $result;
    }

    /**
     * Checks whether order contains a package type in ILS process context
     *
     * @return bool
     */
    public function hasILSContext()
    {
        foreach($this->getPackages() as $package) {
            if(true === in_array(strtoupper($package->getSaleType()), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts())){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether order contains a package type in retention process context
     *
     * @return bool
     */
    public function hasRetentionContext()
    {
         foreach($this->getPackages() as $package) {
             if(true === in_array(strtoupper($package->getSaleType()), Dyna_Catalog_Model_ProcessContext::getProlongationProcessContexts())){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether order contains a package type in migration process context
     *
     * @return bool
     */
    public function hasMigrationContext()
    {
        foreach($this->getPackages() as $package) {
            if(true === in_array(strtoupper($package->getSaleType()), Dyna_Catalog_Model_ProcessContext::getMigrationProcessContexts())){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the order consists of prepaid packages only
     */
    public function isPrepaidOnly()
    {
        foreach ($this->getPackages() as $package){
            if(strtolower($package->getType()) != strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID)){
                return false;
            }
        }

        return true;
    }

    /**
     * Checks whether order contains mobile packages or a certain mobile package type
     * @param null $type
     * @return bool
     */
    public function hasMobileTypePackage($type = null)
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            ($type) ? $type : Dyna_Catalog_Model_Type::getMobilePackages()
        );

        /** @var Dyna_Package_Model_Package $package */
//        foreach ($this->getPackages() as $package) {
//            if ($type) {
//                if (strtolower($package->getType()) == strtolower($type)) {
//                    return true;
//                }
//            } else {
//                if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getMobilePackages())) {
//                    return true;
//                }
//            }
//        }
//
//
//        return false;
    }

    /**
     * @param string $step
     * @return array
     */
    public function getCustomerFields($step = "save_customer")
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quoteId */
        $quoteId = $this->getQuote()->getId();
        /** @var Dyna_Checkout_Helper_Fields $fieldsHelper */
        $fieldsHelper = Mage::helper("dyna_checkout/fields");

        return $fieldsHelper->getCheckoutFieldsAsArray($step, $quoteId);
    }

    /**
     * Return customer address, based on customer type and following checkout logic:
     * if customer isSoho and has fixed or cable package, return address from PostalAddress node of RetrieveCustomerInfo response (see OMNVFDE-1297)
     * return Varien_Object
     */
    public function getCustomerAddress()
    {
        $customer = $this->getCustomer();
        if ($customer->isCustomerLoggedIn()) {
            $customerAddress = $customer->getPostalAddress();
        } else {
            $customerAddress = $customer->getAddress() ?: new Varien_Object();
        }

        return $customerAddress;
    }

    /**
     * Retrieve session loaded customer
     * @return Dyna_Customer_Model_Customer
     */
    public function getCustomer()
    {
        return $this->customer ? $this->customer : Mage::getSingleton('dyna_customer/session')->getCustomer();
    }

    /**
     * Checks if the selected packages contain no phonebook entries product
     *
     * @return bool
     */
    public function getHasPhonebookNoEntries()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::getPhonebookNoEntries()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks if the selected packages contain one phonebook entry product
     *
     * @return bool
     */
    public function getHasPhonebookOneEntry()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::getPhonebookOneEntry()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks if the selected packages contain no individual connections product
     *
     * @return bool
     */
    public function getHasPhonebookNoIndividualConnection()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::getPhonebookNoIndividualConnection()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks if the selected packages contain no phonebook entries product as a relation
     *
     * @return bool
     */
    public function getHasPhonebookNoEntriesRelation()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                $relationsArray = array_intersect_key($item->getProduct()->getData(),
                    array_flip(preg_grep('/^relation_/', array_keys($item->getProduct()->getData()))));
                foreach ($relationsArray as $skuString) {
                    $skuArray = explode(',', $skuString);
                    if (in_array(Dyna_Catalog_Model_Type::getPhonebookNoEntries(), $skuArray)) {
                        return true;
                    }
                }
            }

        }

        return false;
    }

    /**
     * Checks if the selected packages contain one phonebook entry product as a relation
     *
     * @return bool
     */
    public function getHasPhonebookOneEntryRelation()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                $relationsArray = array_intersect_key($item->getProduct()->getData(),
                    array_flip(preg_grep('/^relation_/', array_keys($item->getProduct()->getData()))));
                foreach ($relationsArray as $skuString) {
                    $skuArray = explode(',', $skuString);
                    if (in_array(Dyna_Catalog_Model_Type::getPhonebookOneEntry(), $skuArray)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks if the selected packages contain no individual connections product as a relation
     * This check if available only for products with package type Cable Internet & Phone
     *
     * @return bool
     */
    public function getHasPhonebookNoIndividualConnectionRelation()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if (strtolower($item->getProduct()->getPackageType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE)) {
                    $relationsArray = array_intersect_key($item->getProduct()->getData(),
                        array_flip(preg_grep('/^relation_/', array_keys($item->getProduct()->getData()))));
                    foreach ($relationsArray as $skuString) {
                        $skuArray = explode(',', $skuString);
                        if (in_array(Dyna_Catalog_Model_Type::getPhonebookNoIndividualConnection(), $skuArray)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks if the selected packages contains delete connection data product as a relation
     *
     * @return bool
     */
    public function getHasPhonebookDeleteConnectionDataRelation()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                $relationsArray = array_intersect_key($item->getProduct()->getData(),
                    array_flip(preg_grep('/^relation_/', array_keys($item->getProduct()->getData()))));
                foreach ($relationsArray as $skuString) {
                    $skuArray = explode(',', $skuString);
                    if (in_array(Dyna_Catalog_Model_Type::getPhonebookDeleteConnectionData(), $skuArray)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks if the selected packages contains save connection data product as a relation
     *
     * @return bool
     */
    public function getHasPhonebookSaveConnectionDataSkus()
    {
        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $cart->getQuote();

        $saveConnectionOption = $relationsArray = array();

        foreach ($quote->getAllItems() as $item) {

            $productCollection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToFilter('entity_id', array('eq' => $item->getProductId()))
                ->addAttributeToSelect('relation_excludes')
                ->addAttributeToSelect('relation_excludes_selection')
                ->addAttributeToSelect('relation_upgrades')
                ->addAttributeToSelect('relation_options')
                ->addAttributeToSelect('relation_preselected_options')
                ->addAttributeToSelect('relation_inventory_preselected_options')
                ->addAttributeToSelect('relation_mandatory_options')
                ->addAttributeToSelect('relation_components')
                ->addAttributeToSelect('relation_devices');
            foreach ($productCollection as $product) {
                $relationsArray = explode(",", trim($product->getRelationPreselectedOptions()));
                $relationsArray = array_merge($relationsArray, explode(",", trim($product->getRelationAlternatives())));
                $relationsArray = array_merge($relationsArray, explode(",", trim($product->getRelationExcludes())));
                $relationsArray = array_merge($relationsArray, explode(",", trim($product->getRelationUpgrades())));
                $relationsArray = array_merge($relationsArray, explode(",", trim($product->getRelationOptions())));
                $relationsArray = array_merge($relationsArray, explode(",", trim($product->getRelationInventoryPreselectedOptions())));
                $relationsArray = array_merge($relationsArray, explode(",", trim($product->getRelationMandatoryOptions())));
                $relationsArray = array_merge($relationsArray, explode(",", trim($product->getRelationComponents())));
                $relationsArray = array_merge($relationsArray, explode(",", trim($product->getRelationDevices())));
                $relationsArray = array_merge($relationsArray, explode(",", trim($product->getRelationFees())));
            }

            if (!in_array(Dyna_Catalog_Model_Type::getPhonebookSaveConnectionData(), $saveConnectionOption) && in_array(Dyna_Catalog_Model_Type::getPhonebookSaveConnectionData(), $relationsArray)) {
                $saveConnectionOption[] = Dyna_Catalog_Model_Type::getPhonebookSaveConnectionData();
            }
            if (!in_array(Dyna_Catalog_Model_Type::getPhonebookDeleteConnectionData(), $saveConnectionOption) && in_array(Dyna_Catalog_Model_Type::getPhonebookDeleteConnectionData(), $relationsArray)) {
                $saveConnectionOption[] = Dyna_Catalog_Model_Type::getPhonebookDeleteConnectionData();
            }

        }

        return $saveConnectionOption;
    }

    /**
     * Checks if the selected packages contains save connection data product as a relation
     *
     * @return bool
     */
    public function getHasPhonebookSaveConnectionDataRelation()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                $relationsArray = array_intersect_key($item->getProduct()->getData(),
                    array_flip(preg_grep('/^relation_/', array_keys($item->getProduct()->getData()))));
                foreach ($relationsArray as $skuString) {
                    $skuArray = explode(',', $skuString);
                    if (in_array(Dyna_Catalog_Model_Type::getPhonebookSaveConnectionData(), $skuArray)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks if the selected packages contain second number porting item
     *
     * @return bool
     */
    public function getHasSecondNumberPortingItem()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::getPhonebookSecondNoPorting()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks if the selected packages contain second number porting with pickup item
     *
     * @return bool
     */
    public function getHasSecondNumberPortingWithPickupItem()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::getPhonebookPortPickupSecondNoPorting()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getHasServiceCategory( $package = null )
    {
        $serviceCategoryAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'service_category');
        $serviceCategoryValue = Mage::helper('dyna_catalog')->getAttributeAdminLabel($serviceCategoryAttribute, null, Dyna_Catalog_Model_Type::SERVICE_CATEGORY_VALUE_KAV);

        if(is_null( $package )){
            $packageList = $this->getPackages();
        } else {
            $packageList = array($package);
        }

        foreach ($packageList as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                $product = Mage::getModel('catalog/product')->load($item->getData('product_id'));
                $checkoutProductFlag = $product->getData('checkout_product');
                if ((int)$product->getData('service_category') == $serviceCategoryValue && (!isset($checkoutProductFlag) || !$checkoutProductFlag)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     * @param $orderPackages
     */
    public function isPartOfBundle($orderPackages)
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach($orderPackages as $package) {
            if ($package instanceof Varien_Object) {
                $package = $package->getData();
            }
            $bundlePackagesEntries = Mage::getModel('dyna_bundles/bundlePackages')
                ->getCollection()
                ->addFieldToFilter('package_id', $package['entity_id']);

            if ($bundlePackagesEntries->getSize() > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Flow: modify package
     * In case of tariff change from Internet to Internet & Phone
     * @param $currentProducts
     * @return bool
     */
    public function checkChangeToInternetAndPhone($currentProducts)
    {
        $hasKAV = $this->hasKAV($currentProducts);

        if ($this->getHasServiceCategory() && !$hasKAV) {
            return true;
        }

        return $hasKAV;
    }

    public function hasKAV($currentProducts)
    {
        $hasKAV = false;
        $currentItems = explode(',', $currentProducts);
        $serviceCatAttr = Mage::getSingleton("eav/config")
            ->getAttribute('catalog_product', 'service_category');
        $serviceCatValue = Mage::helper('dyna_catalog')
            ->getAttributeAdminLabel($serviceCatAttr, null, Dyna_Catalog_Model_Type::SERVICE_CATEGORY_VALUE_KAV);

        foreach ($currentItems as $productId) {
            $product = Mage::getModel('catalog/product')->load($productId);
            $checkoutProductFlag = $product->getData('checkout_product');

            if ((int)$product->getData('service_category') == $serviceCatValue &&
                (!isset($checkoutProductFlag) || !$checkoutProductFlag)) {
                $hasKAV = true;
                continue;
            }
        }

        return $hasKAV;
    }

    /**
     * @param $items
     * @return string
     */
    function checkFamilyCategory($items){
        $familyCategory = '';
        $voiceTariffCategories = ['FN148', 'FN152', 'FN151', 'FN127', 'FN128'];
        foreach ($items as $value) {
            $productInstance = Mage::getModel('catalog/product');
            $product = $productInstance->load($productInstance->getIdBySku($value['sku']));
            foreach($product->getCategoryIds() as $category) {
                $category = Mage::getModel('catalog/category')->load($category);
                if (true === in_array(strtoupper($category->getUniqueId()), $voiceTariffCategories)) {
                    $familyCategory = 'VoiceTariffOption';
                }
            }
        }

        return $familyCategory;

    }//end checkFamilyCategory()

    /**
     * @param $items
     * @return string
     */
    function checkBandwidthChange($items){
        $bandwidth = '';
        $bandwidthCategories = ['FN34'];
        foreach ($items as $value) {
            $productInstance = Mage::getModel('catalog/product');
            $product = $productInstance->load($productInstance->getIdBySku($value['sku']));
            foreach($product->getCategoryIds() as $category) {
                $category = Mage::getModel('catalog/category')->load($category);
                if (true === in_array(strtoupper($category->getUniqueId()), $bandwidthCategories)) {
                    if ($value['system_action'] !== Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING) {
                        $bandwidth = 'REMOVE';
                    } else {
                        $bandwidth = 'KEEP';
                    }
                }
            }
        }

        return $bandwidth;

    }//end checkBandwidthChange()


    /**
     * @param $items
     * @return string
     */
    function checkTechnologyChange($items){
        $technology = '';
        $technologyCategories = ['FN31','FN141','FN142','FN143','FN144'];
        foreach ($items as $value) {
            $productInstance = Mage::getModel('catalog/product');
            $product = $productInstance->load($productInstance->getIdBySku($value['sku']));
            foreach($product->getCategoryIds() as $category) {
                $category = Mage::getModel('catalog/category')->load($category);
                if (true === in_array(strtoupper($category->getUniqueId()), $technologyCategories)) {
                    if ($value['system_action'] !== Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING) {
                        $technology = 'REMOVE';
                    } else {
                        $technology = 'KEEP';
                    }
                }
            }
        }

        return $technology;

    }//end checkTechnologyChange()

    /**
     * Return target date
     * @return false|string
     */
    public function getTargetDate($params, $package)
    {
        $ruleId = 0;

        $result['targetDate']      = date('d.m.Y',strtotime("+40 day", strtotime(date('d-m-Y'))));
        $result['terminationDate'] = '';

        if (false === empty($package->getPackageAction())) {
            if ($package->getPackageAction() === Dyna_Package_Model_Package::PACKAGE_ACTION_CHANGE
                || $package->getPackageAction() === Dyna_Package_Model_Package::PACKAGE_ACTION_CHANGE_TARIFF) {
                $params['SKUActivityType'] = 'CHANGE';
            }

            if ($package->getPackageAction() === Dyna_Package_Model_Package::PACKAGE_ACTION_NEW
                || $package->getPackageAction() === Dyna_Package_Model_Package::PACKAGE_OGW_ORDERLINE_NEW) {
                $params['SKUActivityType'] = 'ADD';
            }

            if ($package->getPackageAction() === Dyna_Package_Model_Package::PACKAGE_ACTION_PROLONGATION) {
                $params['SKUActivityType'] = 'KEEP';
            }
        }

        $activityTypes = [
            'keep'   => 'KEEP',
            'add'    => 'ADD',
            'change' => 'CHANGE',
            'remove' => 'REMOVE',
        ];

        $familyCategories = [
            'tv'       => 'VodafoneTV',
            'voice'    => 'VoiceTariffOption',
            'tvtariff' => 'TVTariffOption',
        ];

        /* 1. tariff upgrade
        if [SKUActivityType = CHANGE]
            AND existing ContractValue < new ContractValue
            AND bandwidth [SKU ActivityType = KEEP]
            AND existing technology = KEEP in  MultiMapper
        */
        if (($params['SKUActivityType'] == $activityTypes['change'])
            && ($params['contractValue'] <= $params['newContractValue'])
            && ($params['bandwidth'] == $activityTypes['keep'])
            && ($params['technology'] == $activityTypes['keep'])
        ) {
            $result['targetDate'] = date('d.m.Y',strtotime("+1 day", strtotime(date('d-m-Y'))));
            $ruleId = 1;
        }

        /* 2. tariff upgrade
        if [SKUActivityType = CHANGE]
            AND existing ContractValue < new ContractValue
            AND existing bandwidth [SKU ActivityType = REMOVE]
            AND existing technology = KEEP in  MultiMapper
        */
        if (($params['SKUActivityType'] == $activityTypes['change'])
            && ($params['contractValue'] <= $params['newContractValue'])
            && ($params['bandwidth'] == $activityTypes['remove'])
            && ($params['technology'] == $activityTypes['keep'])
        ) {
            $result['targetDate'] = date('d.m.Y',strtotime("+1 day", strtotime(date('d-m-Y'))));
            $ruleId = 2;
        }

        /* 3. tariff upgrade
        if [SKUActivityType = CHANGE]
            AND existing ContractValue < new ContractValue
            AND EXISTING bandwidth [SKU ActivityType = REMOVE]
            AND existing technology = REMOVE in  MultiMapper
        */
        if (($params['SKUActivityType'] == $activityTypes['change'])
            && ($params['contractValue'] <= $params['newContractValue'])
            && ($params['bandwidth'] == $activityTypes['remove'])
            && ($params['technology'] == $activityTypes['remove'])
        ) {
            $result['targetDate'] = date('d.m.Y', strtotime("+14 day", strtotime(date('d-m-Y'))));
            $ruleId = 3;
        }

        /* 4. tariff downgrade
        if [SKUActivityType = CHANGE]
            AND existing ContractVale > new ContractValue
            AND existing bandwidth [SKU ActivityType = KEEP]
            AND existing technology = KEEP in  MultiMapper
        */
        if (($params['SKUActivityType'] == $activityTypes['change'])
            && ($params['contractValue'] > $params['newContractValue'])
            && ($params['bandwidth'] == $activityTypes['keep'])
            && ($params['technology'] == $activityTypes['keep'])
        ) {
            $result['targetDate'] = $params['NextBillCycleStartDate'];
            $ruleId = 4;
        }

        /* 5. tariff downgrade
        if [SKUActivityType = CHANGE]
            AND existing ContractVale > new ContractValue
            AND existing bandwidth [SKU ActivityType = REMOVE]
            AND existing technology = KEEP in  MultiMapper
        */
        if (($params['SKUActivityType'] == $activityTypes['change'])
            && ($params['contractValue'] > $params['newContractValue'])
            && ($params['bandwidth'] == $activityTypes['remove'])
            && ($params['technology'] == $activityTypes['keep'])
        ) {
            $result['targetDate'] = $params['NextBillCycleStartDate'];
            $ruleId = 5;
        }

        /* 6. tariff downgrade
        if [SKUActivityType = CHANGE]
            AND existing ContractVale > new ContractValue
            AND existing bandwidth [SKU ActivityType = REMOVE]
            AND existing technology = REMOVE in  MultiMapper
        */
        if (($params['SKUActivityType'] == $activityTypes['change'])
            && ($params['contractValue'] > $params['newContractValue'])
            && ($params['bandwidth'] == $activityTypes['remove'])
            && ($params['technology'] == $activityTypes['remove'])
        ) {
            if(strtotime($params['NextBillCycleStartDate']) < strtotime("+14 day", strtotime(date('Y-m-d')))) {
                $result['targetDate'] = date('d.m.Y', strtotime("+14 day", strtotime(date('d-m-Y'))));
            } else {
                $result['targetDate'] = $params['NextBillCycleStartDate'];
            }
            $ruleId = 6;
        }

        /* 7. tariff change from LTE to DSL
        if [SKUActivityType = CHANGE]
            AND existing TARIFF [attributefield = LTE]
            AND new TARIFF [attributefield = DSL]
        */
        if (($params['SKUActivityType'] == $activityTypes['change'])
            && ($params['tariff'] == Dyna_Catalog_Model_Type::TYPE_LTE)
            && ($params['newTariff'] == Dyna_Catalog_Model_Type::TYPE_FIXED_DSL)
        ) {
            $result['targetDate'] = date('d.m.Y',strtotime("+14 day", strtotime(date('d-m-Y'))));
            $ruleId = 7;
        }

        /* 8. order Vodafone TV
        if [FamilyCategory = VodafoneTV]
            AND existing technology = KEEP in MultiMapper
        */
        if (($params['familyCategory'] == $familyCategories['tv'])
            && ($params['technology'] == $activityTypes['keep'])
        ) {
            $result['targetDate'] = date('d.m.Y',strtotime("+5 day", strtotime(date('d-m-Y'))));
            $ruleId = 8;
        }

        /* 9. order Vodafone TV
        if [FamilyCategory = VodafoneTV]
            AND existing technology = REMOVE in MultiMapper
        */
        if (($params['familyCategory'] == $familyCategories['tv'])
            && ($params['technology'] == $activityTypes['remove'])
        ) {
            $result['targetDate'] = date('d.m.Y',strtotime("+14 day", strtotime(date('d-m-Y'))));
            $ruleId = 9;
        }

        /* 10. order new or upgrade voice tariff
        if [FamilyCategory = VoiceTariffOption]
            AND [SKUActivityType == ADD]
            OR [SKUActivityType = CHANGE]
        */

        if ($familyCategories['voice'] == $params['familyCategory']
            && ($params['SKUActivityType'] == $activityTypes['add'] || $params['SKUActivityType'] == $activityTypes['change'])
        ) {
            $result['targetDate'] = date('d.m.Y',strtotime("+1 day", strtotime(date('d-m-Y'))));
            $ruleId = 10;
        }

        /* 11. cancellation of voice tariff option
        if OptionInMinimumDuration = 'TRUE'
            AND SKUActivityType == REMOVE
        */
        if (($params['optionInMinimumDuration'] === 'TRUE')
            && ($params['SKUActivityType'] == $activityTypes['remove'])
            && ($familyCategories['voice'] == $params['familyCategory'])
        ) {
            $result['targetDate']      = date('d.m.Y',strtotime("+1 day", strtotime(date('d-m-Y'))));
            $result['terminationDate'] = date('d.m.Y', strtotime($params['ContractCancellationDate']));
            $ruleId = 11;
        }

        /* 12. cancellation of voice tariff option
        If OptionInMinimumDuration = ‘ELSE’
            AND SKUActivityType=REMOVE
        */
        if (($params['optionInMinimumDuration'] === 'ELSE')
            && ($params['SKUActivityType'] == $activityTypes['remove'])
            && ($familyCategories['voice'] == $params['familyCategory'])
        ) {
            $result['targetDate'] = $params['NextBillCycleStartDate'];
            $ruleId = 12;
        }

        /* 13. Downgrade voice tariff option after minimum contract duration
        If [SKUActivityType=CHANGE]
            AND OptionInMinimumDuration = ‘ELSE’
            AND existing voice tariff option ContractValue < new ContractValue
        */
        if (($params['SKUActivityType'] == $activityTypes['change'])
            && ($params['optionInMinimumDuration'] === 'ELSE')
            && ($params['contractValue'] < $params['newContractValue'])
            && ($familyCategories['voice'] == $params['familyCategory'])
        ) {
            $result['targetDate'] = $params['NextBillCycleStartDate'];
            $ruleId = 13;
        }

        /* 14. Order new or upgrade TV tariff option (=Pay TV option)
        If [FamilyCategory = TVTariffOption]
            AND [SKUActivityType=ADD] OR [SKUActivityType=CHANGE]
        */
        if (($params['familyCategory'] == $familyCategories['tvtariff'])
            && ($params['SKUActivityType'] == $activityTypes['add'] || $params['SKUActivityType'] == $activityTypes['change'])
        ) {
            $result['targetDate'] = date('d.m.Y',strtotime("+1 day", strtotime(date('d-m-Y'))));
            $ruleId = 14;
        }

        /* 15. Cancellation of TV tariff option (in min.duration)
        If OptionInMinimumDuration = ‘TRUE’ AND SKUActivityType=REMOVE
        */
        if (($params['optionInMinimumDuration'] === 'TRUE')
            && ($params['SKUActivityType'] == $activityTypes['remove'])
            && ($familyCategories['tvtariff'] == $params['familyCategory'])
        ) {
            $result['targetDate']      = date('d.m.Y',strtotime("+1 day", strtotime(date('d-m-Y'))));
            $result['terminationDate'] = date('d.m.Y', strtotime($params['ContractCancellationDate']));
            $ruleId = 15;
        }

        /* 16. Cancellation of TV tariff option (out of min. duration
        If OptionInMinimumDuration = ‘ELSE’ AND SKUActivityType=REMOVE
        */
        if (($params['optionInMinimumDuration'] === 'ELSE')
            && ($params['SKUActivityType'] == $activityTypes['remove'])
            && ($familyCategories['tvtariff'] == $params['familyCategory'])
        ) {
            $result['targetDate'] = $params['NextBillCycleStartDate'];
            $ruleId = 16;
        }

        /* 17. Downgrade TV tariff option after minimum contract duration
        If [SKUActivityType=CHANGE]
            AND OptionInMinimumDuration = ‘ELSE’
            AND existing TV tariff option ContractValue > new ContractValue
        */
        if (($params['SKUActivityType'] == $activityTypes['change'])
            && ($params['optionInMinimumDuration'] === 'ELSE')
            && ($params['contractValue'] > $params['newContractValue']
            && ($familyCategories['tvtariff'] == $params['familyCategory']))
        ) {
            $result['targetDate'] = $params['NextBillCycleStartDate'];
            $ruleId = 17;
        }

        return $result;
    }

    /**
     * Return if current quote contains packages from installed base
     *
     * @return array
     */
    public function containsSamePackageTypeInInstalledBase()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quoteModel */
        $quoteModel = Mage::getSingleton('checkout/cart')->getQuote();

        /** @var Dyna_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package');

        /** @var Dyna_Customer_Model_Customer $currentCustomer */
        $currentCustomer = Mage::getSingleton('customer/session')->getCustomer();

        // parse each cart package
        /** @var $package Dyna_Package_Model_Package */
        foreach ($packageModel->getPackages(null, $quoteModel->getId()) as $package) {
            // parse each installed base product
            foreach ($currentCustomer->getAllInstallBaseProducts() as $product) {
                if ($package->getPackageStack() == $product['contract_category']) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getHasCategory($category)
    {
        $searchCategory = Mage::getResourceModel('catalog/category_collection')
            ->addFieldToFilter('name', $category)
            ->getFirstItem();

        if ($searchCategoryId = $searchCategory->getId()) {
            foreach ($this->getPackages() as $package) {
                $items = $package['items'];
                foreach ($items as $item) {
                    /** @var Dyna_Catalog_Model_Product $product */
                    $productCategories = Mage::getModel('catalog/product')->load($item->getData()['product_id'])->getCategoryIds();

                    if (in_array($searchCategoryId, $productCategories)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function getHasFamily($familyName)
    {
        $productFamilyModel = Mage::getSingleton('dyna_catalog/productFamily');
        $familyId = $productFamilyModel->getProductFamilyIdByCode($familyName);

        if ($familyId) {
            foreach ($this->getPackages() as $package) {
                $items = $package['items'];
                foreach ($items as $item) {
                    /** @var Dyna_Catalog_Model_Product $product */
                    $productFamily = Mage::getModel('catalog/product')->load($item->getData()['product_id'])->getProductFamily();

                    if ($productFamily == $familyId) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function getInstalledBaseHasCategory($category)
    {
        $searchCategory = Mage::getResourceModel('catalog/category_collection')
            ->addFieldToFilter('name', $category)
            ->getFirstItem();

        /** @var Dyna_Customer_Model_Customer $currentCustomer */
        $currentCustomer = Mage::getSingleton('customer/session')->getCustomer();

        if ($searchCategoryId = $searchCategory->getId()) {
            foreach ($currentCustomer->getAllInstallBaseProducts() as $item) {
                foreach ($item['products'] as $installBaseProduct) {
                    /** @var Dyna_Catalog_Model_Product $product */
                    $productCategories = Mage::getModel('catalog/product')->load($installBaseProduct['product_id'])->getCategoryIds();

                    if (in_array($searchCategoryId, $productCategories)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
