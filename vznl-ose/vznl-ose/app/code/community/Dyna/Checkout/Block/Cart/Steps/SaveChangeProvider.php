<?php

/**
 * Class Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider
 * available only for cable, dsl and lte
 */
class Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider extends Omnius_Checkout_Block_Cart
{
    use Dyna_Checkout_Block_Cart_Steps_Common;

    //other provider address
    const OTHER_PROVIDER_ADDRESS = 1;

    // change provider values for the radio buttons
    const NO_CHANGE_PROVIDER = 0;
    const YES_CHANGE_PROVIDER = 1;
    const YES_CHANGE_PROVIDER_CONFIRMED = 2; // option only for cable

    // current provider options
    const CURRENT_AS_HOME_PHONE = 1;
    const CURRENT_AS_COMPANY_PHONE = 2;

    //owner of terminal options
    const YES_ONE_OF_THE_TERMINAL_OWNERS = 0;
    const YES_OWNER_OF_TERMINAL = 1;
    const NO_OWNER_OF_TERMINAL = 2;

    // termination date not filled reasons
    const TERMINATION_MODE_END_DATE = 'END_DATE';
    const TERMINATION_DATE_NOT_FILLED_NOW = 'HAND_IN_LATER';
    const TERMINATION_DATE_NOT_FILLED_NO_PERIOD = 'NO_TERM';
    const TERMINATION_DATE_NOT_FILLED_CONTRACT_END = 'ALREADY_CANCELED';

    // lte activation radio options: with new or old phone
    const ACTIVATE_WITH_NEW_PHONE = 0;
    const ACTIVATE_WITH_OLD_PHONE = 1;

    //dsl and cable desired date
    const NEW_CONNECTION_DATE_AS_SOON_AS_POSSIBLE = 0;
    const NEW_CONNECTION_DATE_SPECIFIC_DATE = 1;

    //dsl type of home
    const HOME_WITH_1_FAMILY = 0;
    const HOME_WITH_MULTIPLE_FAMILIES = 1;

    //dsl home entrance
    const HOME_ENTRANCE_VESTIBULE = 1;
    const HOME_ENTRANCE_ANNEX = 2;

    //dsl home floor
    const HOME_FLOOR_CELLAR = 1;
    const HOME_ENTRANCE_FLOOR = 2;

    //dsl home location
    const AP_LOCATION_LEFT = 1;
    const AP_LOCATION_MIDDLE = 2;
    const AP_LOCATION_RIGHT = 3;

    const LAST_POSSIBLE_DATE_NOTICE = 1;
    const TERMINATION_DATE_NOT_RELEVANT = 2;

    // Single connection choices
    const NO_SINGLE_CONNECTION = 1;
    const SINGLE_CONNECTION_FULL_NUMBER = 2;
    const SINGLE_CONNECTION_SHORT_NUMBER = 3;

    // phonebook entry choices
    const PHONEBOOK_CHOICE_STANDARD = 3;
    const PHONEBOOK_CHOICE_NO_ENTRIES = 1;
    const PHONEBOOK_CHOICE_EXTENDED = 2;

    private $numberPortingOneProduct = null;
    private $numberPortingMoreProduct = null;

    // Phone entry type options
    const PHONE_ENTRY_TYPE_TELEPHONE = 'Telephone';
    const PHONE_ENTRY_TYPE_FAX = 'Fax';
    const PHONE_ENTRY_TYPE_TELEPHONE_FAX = 'Telephone and Fax';
    const PHONE_ENTRY_TYPE_NO_ENTRY = 'No entry';

    // Data connection choices
    const SAVE_DATA_CONNECTION = 1;
    const DELETE_DATA_CONNECTION = 2;

    /**
     * @param Dyna_Package_Model_Package $packag
     * @return array
     */
    public function getProvidersOptions(Dyna_Package_Model_Package $package )
    {
        $providers = array();
        switch( TRUE ) {
            case $package->isCable():
                $providers = Mage::helper('dyna_checkout')->getOptionsForCable();
                break;
            case $package->isFixed():
                $providers = $this->getOptionsForDsl();
                break;
        }
        return $providers;
    }

    /**
     * @param bool $cableProvider
     * @return array
     */
    public function getOptionsForDsl($cableProvider = false)
    {
        $parsedProviders = [];
        $parsedCableProviders = [];

        $providers = Mage::getSingleton('dyna_operator/serviceProvider')
            ->getCollection()
            ->load();

        foreach ($providers as $provider) {
            if (!empty($provider->getName()) && !empty($provider->getCode())) {
                $parsedProviders[$provider->getCode()] = $provider->getName();
            }
            if ((int)$provider->getCableProvider()) {
                array_push($parsedCableProviders, $provider->getCode());
            }
        }

        if ($cableProvider) {
            return $parsedCableProviders;
        }

        return $parsedProviders;
    }

    public function isCableProvider($carrierCode)
    {
        /** @var Dyna_Configurator_Model_Configuration $configurationData */
        $providersData = Mage::getModel('dyna_configurator/configuration')
            ->getConfigurationData()
            ->getCarriers();

        foreach ($providersData as $provider) {
            if ($carrierCode == $provider['code']) {
                return $provider['cableprovider'];
            }
        }

        return false;
    }

    public function getNoOfPhoneTransfersOptions($packageType = null)
    {
        /** @var Dyna_Checkout_Helper_Data */
        $numberPorting[Dyna_Superorder_Helper_Client::PORTING_ONE_NUMBER] = Mage::helper('dyna_checkout')->getConfigurableCheckoutOptions('number_porting_one', $packageType);

        $isDSL = in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getDslPackages());

        // Don't show in case of DSL + BasicConnection
        if (!($this->getHasBasicConnection() == true && $isDSL == true)) {
            /** @var Dyna_Checkout_Helper_Data */
            $numberPorting[Dyna_Superorder_Helper_Client::PORTING_MULTIPLE_NUMBERS] = Mage::helper('dyna_checkout')->getConfigurableCheckoutOptions('number_porting_more', $packageType);
        }

        /** @var Dyna_Checkout_Helper_Data */
        $numberPorting[Dyna_Superorder_Helper_Client::PORTING_ALL_NUMBERS] = Mage::helper('dyna_checkout')->getConfigurableCheckoutOptions('number_porting_all', $packageType);

        // Pushing none option
        $numberPorting[Dyna_Superorder_Helper_Client::PORTING_NO_NUMBER] = array('display_label_checkout' => $this->__('None'));

        return $numberPorting;
    }

    public function getOwnerOfTerminalOptions()
    {
        return [
            self::YES_OWNER_OF_TERMINAL => $this->__("Yes, I'm connection owner"),
            self::YES_ONE_OF_THE_TERMINAL_OWNERS => $this->__("Yes, I'm one of the connection owners"),
            self::NO_OWNER_OF_TERMINAL => $this->__("No, I'm not the connection owner"),
        ];
    }

    public function getTerminationDateNotFilledReasons()
    {
        return [
            self::TERMINATION_DATE_NOT_FILLED_NOW => $this->__("We will fill it later"),
            self::TERMINATION_DATE_NOT_FILLED_NO_PERIOD => $this->__("No minimum contract period"),
            self::TERMINATION_DATE_NOT_FILLED_CONTRACT_END => $this->__("Agreement has terminated"),
        ];
    }

    public function getActivationWithPhoneOptionsForLte()
    {
        return [
            self::ACTIVATE_WITH_NEW_PHONE => $this->__("Activation immediately. You get new phones"),
            self::ACTIVATE_WITH_OLD_PHONE => $this->__("Enabling with old phone"),
        ];
    }

    public function getNewConnectionDesiredDatesOptionsForCableAndDsl()
    {
        return [
            self::NEW_CONNECTION_DATE_AS_SOON_AS_POSSIBLE => $this->__("As soon as possible"),
            self::NEW_CONNECTION_DATE_SPECIFIC_DATE => $this->__("On a certain day"),
        ];
    }

    public static function getDslResidenceTypesOptions()
    {
        return [
            self::HOME_WITH_1_FAMILY => Mage::helper("omnius_checkout")->__("House with one family"),
            self::HOME_WITH_MULTIPLE_FAMILIES => Mage::helper("omnius_checkout")->__("Multi-family house"),
        ];
    }

    public static function getDslHomeEntranceOptions()
    {
        return [
            self::HOME_ENTRANCE_VESTIBULE => Mage::helper("omnius_checkout")->__("Vestibule"),
            self::HOME_ENTRANCE_ANNEX => Mage::helper("omnius_checkout")->__("Annex"),
        ];
    }

    public static function getDslHomeFloorsOptions()
    {
        $levels = [];
        $configLevels = Mage::getStoreConfig('checkout/options/max_levels');

        $line = strtok($configLevels,"\r\n");
        while($line !== false){
            $levels[] = Mage::helper("omnius_checkout")->__($line);
            $line = strtok("\r\n");
        }

        return $levels;
    }

    public static function getDslApartmentLocationOptions()
    {
        return [
            self::AP_LOCATION_LEFT => Mage::helper("omnius_checkout")->__("Left"),
            self::AP_LOCATION_MIDDLE => Mage::helper("omnius_checkout")->__("Center"),
            self::AP_LOCATION_RIGHT => Mage::helper("omnius_checkout")->__("Right"),
        ];
    }

    private function getChangeProviderOptionsForCable()
    {
        $options = $this->getChangeProviderOptionsForFixed();
        $options[self::YES_CHANGE_PROVIDER_CONFIRMED] = $this->__("Yes, data is subsequently confirmed");
        return $options;
    }

    private function getChangeProviderOptionsForFixed()
    {
        return [
            self::NO_CHANGE_PROVIDER => $this->__('No'),
            self::YES_CHANGE_PROVIDER => $this->__('Yes')
        ];
    }

    private function hasBasisAnschluss()
    {
        $quote = $this->getQuote();

        foreach ($quote->getPackagesInfo() as $key => $package) {
            if (strtolower($package['type']) == strtolower(Dyna_Catalog_Model_Type::TYPE_LTE)) {
                $items = $quote->getPackageItems($key);
                foreach ($items as $item) {
                    //Check if name is 'Basis-Anschluss'
                    if (strtolower($item->getName()) == strtolower('Basis-Anschluss')) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Gets phone numbers array for offnet/migration scenario
     *
     * @param string $migrationOrMoveOffnetState
     * @return array
     */
    public function getMigrationOrMoveOffnetPhoneNumbers($migrationOrMoveOffnetState = Dyna_Catalog_Model_Type::MOVE_OFFNET)
    {
        $phoneNumbers = [];

        $accountCategory = Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD;
        if (Dyna_Catalog_Model_Type::MIGRATION == $migrationOrMoveOffnetState) {
            $accountCategory = Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN;
        }

        $migrationData = explode(",",
            Mage::getSingleton('checkout/cart')->getCheckoutSession()->getMigrationOrMoveOffnetCustomerNo());
        $migrationCustomerNumber = $migrationData[0] ?? null;

        if ($migrationCustomerNumber) {
            /** @var Dyna_Customer_Model_Session $customerStorage */
            $customerStorage = Mage::getSingleton('dyna_customer/session');
            $allCustomers = $customerStorage->getCustomerProducts();
            $customers = $allCustomers[$accountCategory] ?? [];
            $phoneNumbers = [];
            foreach ($customers as $customerNumber => $customer) {
                if (intval($migrationCustomerNumber) !== intval($customerNumber)) {
                    continue;
                }
                foreach ($customer['contracts'] as $contractDetails) {
                    $phoneNumbers = array_merge($phoneNumbers, $this->getSubscriptionPhonenumbers($contractDetails['subscriptions']));
                }
            }
        }

        return $phoneNumbers;
    }

    /**
     * @param array $subscriptions
     * @return array
     */
    protected function getSubscriptionPhonenumbers(array $subscriptions) : array
    {
        $phoneNumbers = [];
        foreach ($subscriptions as $subscriptionDetails) {
            if (isset($subscriptionDetails['all_phone_no'])) {
                foreach ($subscriptionDetails['all_phone_no'] as $phoneNo) {
                    $phoneNumbers[] = $phoneNo;
                }
            }
        }
        return $phoneNumbers;
    }

    /**
     * Gets service address array for offnet scenario
     *
     * @return bool
     */
    public function getMoveOffnetServiceAddressParts()
    {
        $customerNoToMoveOffnet = Mage::getSingleton('checkout/cart')->getCheckoutSession()->getMigrationOrMoveOffnetCustomerNo();

        if ($customerNoToMoveOffnet) {
            /** @var Dyna_Customer_Model_Session $session */
            $serviceCustomers = $this->getSession()->getServiceCustomers();
            $customers = $serviceCustomers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] ?? [];

            foreach ($customers as $customer) {
                $customerNoToMoveOffnetParts = explode(',', $customerNoToMoveOffnet);
                if ($customer['customer_number'] != $customerNoToMoveOffnetParts[0]) {
                    continue;
                }
                foreach ($customer['contracts'] as $contractDetails) {
                    foreach ($contractDetails['subscriptions'] as $subscriptionDetails) {
                        foreach ($subscriptionDetails['addresses'] as $addressDetails) {
                            if ($addressDetails['address_type'] == 'service') {
                                return $addressDetails;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Gets the area code of the migrated item
     *
     * @return bool
     */
    public function getMoveOffnetOldAreaCode()
    {
        $customerNoToMoveOffnetOrMigrate = Mage::getSingleton('checkout/cart')->getCheckoutSession()->getMigrationOrMoveOffnetCustomerNo();

        // $targetCtn - > $customerNo
        if ($customerNoToMoveOffnetOrMigrate) {
            /** @var Dyna_Customer_Model_Session $customerStorage */
            $customerStorage = Mage::getSingleton('dyna_customer/session');
            $allCustomers = $customerStorage->getCustomerProducts();
            $customers = $allCustomers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] ?? [];
            foreach ($customers as $customerNumber => $customer) {
                if ($customerNoToMoveOffnetOrMigrate != $customerNumber) {
                    continue;
                }
                foreach ($customer['contracts'] as $contractDetails) {
                    foreach ($contractDetails['subscriptions'] as $subscriptionDetails) {
                        if (isset($subscriptionDetails['all_phone_no'])) {
                            foreach ($subscriptionDetails['all_phone_no'] as $phoneNo) {
                                return $phoneNo['LocalAreaCode'];
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function getHasSameAreaCode()
    {
        $newAreaCode = $this->getCustomer()->getServiceAreaCode();
        $oldAreaCode = $this->getMoveOffnetOldAreaCode();

        return ($newAreaCode == $oldAreaCode);
    }

    /**
     * Get all area code before moving to new address
     * @return string
     */
    public function getOldAreaCode()
    {
        return $this->getMoveOffnetOldAreaCode();
    }

    /**
     * Gets individual choices
     * @return array
     */
    public function getIndividualConnectionChoices()
    {
        return [
            self::NO_SINGLE_CONNECTION => $this->__('None'),
            self::SINGLE_CONNECTION_FULL_NUMBER => $this->__('Full'),
            self::SINGLE_CONNECTION_SHORT_NUMBER => $this->__('Shortened')
        ];
    }

    /*
     * Determines whether or not the connection ready confirmation block is shown is sontinges section (see OMNVFDE-2076)
     * @return bool
     */
    public function confirmConnectionReady()
    {
        $storage = $this->getAddressStorage();
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();

        // If order has cable tv AND cable internet and phone with NE3 or NE4 both with self install
        if ($quote->hasPackage(Dyna_Catalog_Model_Type::TYPE_CABLE_TV) && $quote->hasPackage(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE)) {
            if ($storage->isConnectionSelfInstall() && in_array($storage->getConnectionObject(), [$storage::CONN_NE3, $storage::CONN_NE4])) {
                return true;
            }
        } // If order has cable tv without MNV connection object
        else if ($quote->hasPackage(Dyna_Catalog_Model_Type::TYPE_CABLE_TV)) {
            $configuratorHelper = Mage::helper('dyna_configurator');
            $customerHasCableInstalled = $configuratorHelper->customerHasServiceInstalled(Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_TV);
            // If customer has cable-tv already installed, return false here (see OVG-2144)
            if( !$customerHasCableInstalled ) {
                if ($storage->getConnectionObject() !== $storage::CONN_MNV) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Gets phonebook choices
     * @return array
     */
    public function getPhonebookChoices()
    {
        if($this->getCustomer()->getIsSoho()) {
            return [
                self::PHONEBOOK_CHOICE_NO_ENTRIES => $this->__('No phone book entries'),
                self::PHONEBOOK_CHOICE_EXTENDED => $this->__('Extended')
            ];
        } else {
            return [
                self::PHONEBOOK_CHOICE_NO_ENTRIES => $this->__('No phone book entries'),
                self::PHONEBOOK_CHOICE_STANDARD => $this->__('Standard (Name/Address)'),
                self::PHONEBOOK_CHOICE_EXTENDED => $this->__('Extended')
            ];
        }
    }

    /**
     * Gets phonebook choices
     * @return array
     */
    public function getPhonebookItemsPerChoice()
    {
        return [
            self::PHONEBOOK_CHOICE_STANDARD => implode(',', Dyna_Catalog_Model_Type::getPhonebookStandard()),
            self::PHONEBOOK_CHOICE_NO_ENTRIES => Dyna_Catalog_Model_Type::getPhonebookNoEntries(),
            self::PHONEBOOK_CHOICE_EXTENDED => Dyna_Catalog_Model_Type::getPhonebookOneEntry()
        ];
    }

    /**
     * @param null $packageType
     * @return array
     */
    public function getNumberOfRegistrationsOptions($packageType = null)
    {
        /** @var Dyna_Checkout_Helper_Data $phoneBookEntry1 */
        $phoneBookEntry1 = Mage::helper('dyna_checkout')->getConfigurableCheckoutOptions('phone_book_entry1', $packageType);

        if ($this->getHasComfortConnection()) {
            /** @var Dyna_Checkout_Helper_Data $phoneBookEntry2 */
            $phoneBookEntry2 = Mage::helper('dyna_checkout')->getConfigurableCheckoutOptions('phone_book_entry2', $packageType);

            /** @var Dyna_Checkout_Helper_Data $phoneBookEntry3 */
            $phoneBookEntry3 = Mage::helper('dyna_checkout')->getConfigurableCheckoutOptions('phone_book_entry3', $packageType);


            $phoneBookEntry = array($phoneBookEntry1,$phoneBookEntry2,$phoneBookEntry3);
        } else {
            $phoneBookEntry = array($phoneBookEntry1);
        }

        return $phoneBookEntry;
    }

    public function getPhoneEntryTypeOptionsForPackageType($type)
    {
        if(in_array(strtolower($type), Dyna_Catalog_Model_Type::getCablePackages()))
        {
            return $keywordOptions = [
                self::PHONE_ENTRY_TYPE_TELEPHONE,
                self::PHONE_ENTRY_TYPE_NO_ENTRY,
            ];
        } else if (in_array(strtolower($type), Dyna_Catalog_Model_Type::getDslPackages())) {
            return $keywordOptions = [
                self::PHONE_ENTRY_TYPE_TELEPHONE,
                self::PHONE_ENTRY_TYPE_FAX,
                self::PHONE_ENTRY_TYPE_TELEPHONE_FAX,
                self::PHONE_ENTRY_TYPE_NO_ENTRY
            ];
        } else if (in_array(strtolower($type), Dyna_Catalog_Model_Type::getLtePackages())) {
            return $keywordOptions = [
                self::PHONE_ENTRY_TYPE_TELEPHONE,
                self::PHONE_ENTRY_TYPE_FAX,
                self::PHONE_ENTRY_TYPE_TELEPHONE_FAX,
                self::PHONE_ENTRY_TYPE_NO_ENTRY
            ];
        }

        return [];
    }

    public function getKeywordOptions()
    {
        $result = Mage::helper('agentde')->getKeywordList();
        return $result ? $result :[
            '-',
            '[Defined entry]',
            '[Defined entry]',
            '[Defined entry]',
            '[Defined entry]',
        ];
    }

    public function getInfoExchangeOptions()
    {
        return [
            'Everything',
            'Information on the phone number',
            'No Information',
        ];
    }

    /**
    * Validation for the mobile porting
    * @param $items
    * @param $package_id
    * @return bool
    */
    public function isMobilePortingAllowed($items, $package_id="") {
        $status = true;
        $result = array();

        // Checking if agent has MOBILE_PORTING permission
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $agent = $customerSession->getAgent();

        if(!$agent->isGranted('MOBILE_PORTING')){
            return false;
        }

        // Checking if redplus owner tariff is selected
        if ($package_id) {
            foreach ($items as $item) {
                if ($item->getProduct()->isRedPlusOwner()) {
                    $status = false;
                    break;
                }
            }
        } else {
            foreach ($items as $item) {
                if ($item->getPackageType() == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {

                    // If redplus already exists in current package then we'll move to next package
                    if(isset($result[$item->getPackageId()]) && !$result[$item->getPackageId()])
                        continue;

                    $result[$item->getPackageId()] = true;

                    if ($item->getProduct()->isRedPlusOwner()) {
                        $result[$item->getPackageId()] = false;
                    }
                }
            }

            if (in_array(true, $result)) {
                $status = true;
            } else {
                $status = false;
            }
        }

        return $status;
    }

}
