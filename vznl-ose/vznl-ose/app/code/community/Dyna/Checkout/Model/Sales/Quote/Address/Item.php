<?php

class Dyna_Checkout_Model_Sales_Quote_Address_Item extends Omnius_Checkout_Model_Sales_Quote_Address_Item
{
    private $cache = null;
    /**
     * Import item to quote
     *
     * @param Mage_Sales_Model_Quote_Item $quoteItem
     * @return Mage_Sales_Model_Quote_Address_Item
     */
    public function importQuoteItem(Mage_Sales_Model_Quote_Item $quoteItem)
    {
        parent::importQuoteItem($quoteItem);

        // List of fields that will be saved from quote item to quote address item
        $fields = array(
            'Ctn',
            'AppliedRuleIds',
            'StoreId',
            'PriceInclTax',
            'Maf',
            'BaseMaf',
            'NetworkProvider',
            'NetworkOperator',
            'ContractNr',
            'ContractEndDate',
            'CurrentSimcardNumber',
            'CurrentNumber',
            'SimNumber',
            'TelNumber',
            'ConnectionType',
            'PackageId',
            'PackageType',
            'PackageStatus',
            'SaleType',
            'MafDiscountAmount',
            'BaseMafDiscountAmount',
            'MafRowTotalWithDiscount',
            'HiddenMafTaxAmount',
            'BaseHiddenMafTaxAmount',
            'AppliedPromoAmount',
            'BaseAppliedPromoAmount',
            'AppliedMafPromoAmount',
            'BaseAppliedMafPromoAmount',
            'CustomPrice',
            'OriginalCustomPrice',
            'NumberPortingType',
            'RowTotal',
            'RowTotalInclTax',
            'ConnectionCost',
            'IsPromo',
            'IsBundlePromo',
            'IsPromotion',
            'TargetId',
            'RegisterReturn',
            'OldSim',
            'IsDefaulted',
            'CustomMaf',
            'OriginalCustomMaf',
            'IsContract',
            'IsContractDrop',
            'SystemAction',
            'ProductVisibility',
            'HasNonStandardRate',
        );

        foreach ($fields as $field) {
            $val = call_user_func(array($quoteItem, 'get' . $field));
            call_user_func(array($this, 'set' . $field), $val);
        }
        $this->setCustomStatus(Omnius_Checkout_Model_Sales_Quote::VALIDATION);

        return $this;
    }

    /**
     * Specify custom item price (used in case whe we have apply not product price to item)
     *
     * @param   float $value
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setCustomMaf($value)
    {
        $this->setCalculationMaf($value);
        $this->setBaseCalculationMaf(null);
        return $this->setData('custom_maf', $value);
    }

    /**
     * @return mixed
     */
    public function isPromotion()
    {
        if ($this->getProduct()->isPromotion()) {
            $this->setIsPromotion(true);
        }

        return $this->getIsPromotion();
    }

    /**
     * Returns true if the promo product was added by a bundle rule
     * @return mixed
     */
    public function isBundlePromo()
    {
        return $this->getData('is_bundle_promo');
    }

    /**
     * Returns true if the item was added from installed base
     * @return mixed
     */
    public function isContract()
    {
        return $this->getData('is_contract');
    }

    /**
     * @return float
     */
    public function getItemFinalPriceInclTax()
    {
        return $this->getRowTotalInclTax() - $this->getDiscountAmount();
    }
    
    /**
     * Retrieve product model object associated with item
     *
     * @return Omnius_Catalog_Model_Product
     */
    public function getProduct()
    {
        $product = $this->_getData('product');
        if ($product === null && $this->getProductId()) {
            $key = serialize(array(__METHOD__, $this->getProductId(), $this->getQuote()->getStoreId()));
            if ($product = unserialize($this->getCache()->load($key))) {
                $this->setProduct($product);
            } else {
                /** @var Omnius_Catalog_Model_Product $product */
                $product = Mage::getModel('catalog/product')
                    ->setStoreId($this->getQuote()->getStoreId())
                    ->load($this->getProductId());
                $this->setProduct($product);
                $this->getCache()->save(serialize($product), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
            }
        }

        return $product;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    public function getCache()
    {
        if (!$this->cache) {
            $this->cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->cache;
    }
}
