<?php
/**
 * Class Dyna_Checkout_Model_Client_GetPickupLocationClient
 */
class Dyna_Checkout_Model_Client_GetPickupLocationClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "pickup_locations/wsdl";
    const ENDPOINT_CONFIG_KEY = "pickup_locations/usage_url";
    const CONFIG_STUB_PATH = 'pickup_locations/use_stubs';

    /** Mage::getModel("dyna_checkout/client_GetPickupLocationClient"); */


    /**
     * @param array $params
     * @return mixed
     */
    public function executeGetPickupLocation($parameters = [])
    {
        /**
         * $params = [
         *
         * ]
         */

        //TODO: Replace dummy values
        $params['ShippingCondition'] = array(
            'ServiceProviderParty' => array(
                'Party' => array(
                    'PartyName' => 1,
                    'AccountCategory' => 2,
                    'IdentificationMethod' => 3,
                    'CashOnDelivery' => 4
                )
            ),
            'Address' => array(
                'PostalZone' => $parameters['postcode'],
                'LocationCoordinate' => array(
                    'LatitudeDirectionCode' => $parameters['latitude'],
                    'LongitudeDirectionCode' => $parameters['longitude']
                ),
                'Radius' => Mage::getStoreConfig('omnius_service/google_maps_api/radius') ?? 10
            )
        );

        $this->setRequestHeaderInfo($params);

        $result = $this->RetrievePickupLocations($params);

        if(!$result || $result['Success'] == false){
            return null;
        } else {
            return $result['Account'];
        }
    }
}