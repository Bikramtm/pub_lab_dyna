<?php

class Dyna_Checkout_Model_Sales_Order extends Omnius_Checkout_Model_Sales_Order
{
    const DEFAULT_COUNTRY = 'DE';

    /**
     * Get packages from an order
     * @param $orderId
     * @return array|mixed
     */
    public function getOrderPackages($orderId)
    {
        if (($packages = Mage::registry('orderPackages')) === null) {
            $packages = $disabledPackages = $disabledPackagesIds = [];
            // Set packages type and sale type
            if ($packagesInfo = $this->getOrderPackagesInfo($orderId)) {
                foreach ($packagesInfo as $package) {
                    $packageId = $package['package_id'];
                    $packages[$packageId] = $package;
                    $packages[$packageId]['items'] = array();

                    if ($package['editing_disabled']) {
                        $disabledPackages[$packageId] = $package;
                        array_push($disabledPackagesIds, $packageId);
                    }
                }
            }

            /** @var Dyna_Catalog_Model_Product $productModel */
            $productModel = Mage::getModel('catalog/product');

            // Special handling for Installed base packages that are used in bundles
            foreach ($disabledPackages as $packageId => $disabledPackage) {
                $skus = explode(',', $disabledPackage['installed_base_products']);
                $oldSkus = explode(',', $disabledPackage['initial_installed_base_products']);
                $diff = array_diff($skus, $oldSkus);
                $items = [];
                foreach ($skus as $sku) {
                    $productSku = trim($sku);
                    $productId = $productModel->getIdBySku($productSku);
                    if ($productId) {
                        /** @var Mage_Sales_Model_Quote_Item $quoteItemModel */
                        $quoteItemModel = Mage::getModel('sales/quote_item');
                        /** @var Omnius_Catalog_Model_Product $product */
                        $product = Mage::getModel('catalog/product')
                            ->setStoreId($this->getStoreId())
                            ->load($productId);

                        $items[] = $quoteItemModel->setProduct($product);
                        // Simulate discount if product is promotion
                        $price = $product->getPrice() * ($product->isPromotion() ? -1 : 1);
                        $maf = $product->getMaf() * ($product->isPromotion() ? -1 : 1);
                        $quoteItemModel->setRowTotalInclTax($price);
                        $quoteItemModel->setMafRowTotalInclTax($maf);
                        if (!in_array($sku, $diff)) {
                            $quoteItemModel->setOldInstallBase(true);
                        }
                    }
                }
                $packages[$packageId]['items'] = $items;
            }
            /** @var Mage_Sales_Model_Order $orderItems */
            // @todo handle for multiple orders per superorder
            $orders = Mage::getModel('sales/order')
                ->getCollection()
                ->addFieldToFilter('superorder_id', $orderId);

            if (count($orders) > 1) {
                $orderIds = [];
                foreach ($orders as $order) {
                    $orderIds[] = $order->getEntityId();
                }

                $orderItems = Mage::getModel('sales/order_item')
                    ->getCollection()
                    ->addFieldToFilter('order_id', array('in' => $orderIds));
            } else {
                $order = $orders->getFirstItem();
                $orderItems = Mage::getModel('sales/order')->load($order->getEntityId())->getAllItems();
            }

            // Get items for packages which are not from installed base
            foreach ($packages as $packageId => $package) {
                if (!in_array($packageId, $disabledPackagesIds)) {
                    $items = [];
                    foreach ($orderItems as $item) {
                        if ($item->getPackageId() == $packageId) {
                            $items[] = $item;
                        }
                    }
                    $packages[$packageId]['items'] = $items;
                }
            }

            if ($this->getId()) {
                Mage::unregister('orderPackages');
                Mage::register('orderPackages', $packages);
            }
        }

        return $packages;
    }

    /**
     * Return packages info
     * @return array
     */
    public function getOrderPackagesInfo($orderId)
    {
        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $orderId)
            ->setOrder('package_id', 'ASC');
        return $packages;
    }

    public function processPackageChecksum()
    {
        // heavy NL logic, not needed
    }
}
