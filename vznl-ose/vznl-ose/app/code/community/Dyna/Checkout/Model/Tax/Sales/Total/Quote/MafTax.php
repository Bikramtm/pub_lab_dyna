<?php

/**
 * Class Dyna_Checkout_Model_Tax_Sales_Total_Quote_MafTax
 */
class Dyna_Checkout_Model_Tax_Sales_Total_Quote_MafTax extends Omnius_Checkout_Model_Tax_Sales_Total_Quote_Tax
{
    /**
     * Process hidden taxes for items and shippings (in accordance with hidden tax type)
     *
     * @return void
     */
    protected function _processHiddenTaxes()
    {
        $this->_getAddress()->setMafTotalAmount('hidden_maf_tax', 0);
        $this->_getAddress()->setBaseMafTotalAmount('hidden_maf_tax', 0);
        foreach ($this->_hiddenTaxes as $taxInfoItem) {
            if (isset($taxInfoItem['item'])) {
                // Item hidden taxes
                $item = $taxInfoItem['item'];
                /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
                if ($item->isPromotion()) {
                    // todo: determine if this is needed for minus prices
                    continue;
                }
                $hiddenTax = $taxInfoItem['value'];
                $baseHiddenTax = $taxInfoItem['base_value'];
                $qty = $taxInfoItem['qty'];

                $hiddenTax = $this->_calculator->round($hiddenTax);
                $baseHiddenTax = $this->_calculator->round($baseHiddenTax);
                $item->setHiddenMafTaxAmount($qty * $hiddenTax);
                $item->setBaseHiddenMafTaxAmount($qty * $baseHiddenTax);
                $this->_getAddress()->addMafTotalAmount('hidden_maf_tax', $item->getHiddenMafTaxAmount());
                $this->_getAddress()->addBaseMafTotalAmount('hidden_maf_tax', $item->getBaseHiddenMafTaxAmount());
            }
        }
    }

    /**
     * Calculate unit tax anount based on unit price
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @param   float $rate
     * @param   array $taxGroups
     * @param   string $taxId
     * @param   boolean $recalculateRowTotalInclTax
     * @return  Omnius_Checkout_Model_Tax_Sales_Total_Quote_Tax
     */
    protected function _calcUnitTaxAmount($item, $rate, &$taxGroups = null, $taxId = null, $recalculateRowTotalInclTax = false)
    {
        $qty = $item->getTotalQty();
        $inclTax = $item->getIsPriceInclTax();
        $price = $item->getMafTaxableAmount();
        $basePrice = $item->getBaseMafTaxableAmount();
        $rateKey = ($taxId == null) ? (string) $rate : $taxId;

        $hiddenTax = null;
        $baseHiddenTax = null;
        $unitTaxBeforeDiscount = null;
        $baseUnitTaxBeforeDiscount = null;

        $discountAmount = $item->getMafDiscountAmount() / $qty;
        $baseDiscountAmount = $item->getBaseMafDiscountAmount() / $qty;

        $unitTaxBeforeDiscount = $this->_calculator->calcTaxAmount($price, $rate, $inclTax, false);
        $unitTaxDiscount = $this->_calculator->calcTaxAmount($discountAmount, $rate, $inclTax, false);
        $unitTax = $this->_calculator->round($unitTaxBeforeDiscount - $unitTaxDiscount);

        $baseUnitTaxBeforeDiscount = $this->_calculator->calcTaxAmount($basePrice, $rate, $inclTax, false);
        $baseUnitTaxDiscount = $this->_calculator->calcTaxAmount($baseDiscountAmount, $rate, $inclTax, false);
        $baseUnitTax = $this->_calculator->round($baseUnitTaxBeforeDiscount - $baseUnitTaxDiscount, 0);

        $unitTax = $this->_calculator->round($unitTax);
        $baseUnitTax = $this->_calculator->round($baseUnitTax);

        $unitTaxBeforeDiscount = $this->_calculator->round($unitTaxBeforeDiscount);
        $baseUnitTaxBeforeDiscount = $this->_calculator->round($baseUnitTaxBeforeDiscount);

        if ($inclTax && $discountAmount > 0) {
            $hiddenTax = $unitTaxBeforeDiscount - $unitTax;
            $baseHiddenTax = $baseUnitTaxBeforeDiscount - $baseUnitTax;
            $this->_hiddenTaxes[] = array(
                'rate_key' => $rateKey,
                'qty' => $qty,
                'item' => $item,
                'value' => $hiddenTax,
                'base_value' => $baseHiddenTax,
                'incl_tax' => $inclTax,
            );
        } elseif ($discountAmount > $price) {
            // case with 100% discount on price incl. tax
            $this->_hiddenTaxes[] = array(
                'rate_key' => $rateKey,
                'qty' => $qty,
                'item' => $item,
                'value' => $hiddenTax,
                'base_value' => $baseHiddenTax,
                'incl_tax' => $inclTax,
            );
        }

        $rowTax = $this->_store->roundPrice($qty * $unitTax);
        $baseRowTax = $this->_store->roundPrice($qty * $baseUnitTax);
        $item->setMafTaxAmount($item->getMafTaxAmount() + $rowTax);
        $item->setBaseMafTaxAmount($item->getBaseMafTaxAmount() + $baseRowTax);
        if (is_array($taxGroups)) {
            $taxGroups[$rateKey]['maf_tax'] = $rowTax;
            $taxGroups[$rateKey]['base_maf_tax'] = $baseRowTax;
        }

        $rowTotalInclTax = $item->getMafRowTotalInclTax();
        if (!isset($rowTotalInclTax) || $recalculateRowTotalInclTax) {
            if ($this->_config->priceIncludesTax($this->_store)) {
                $item->setMafRowTotalInclTax($price * $qty);
                $item->setBaseMafRowTotalInclTax($basePrice * $qty);
            } else {
                $item->setMafRowTotalInclTax($item->getMafRowTotalInclTax() + $unitTaxBeforeDiscount * $qty);
                $item->setBaseMafRowTotalInclTax($item->getBaseMafRowTotalInclTax() + $baseUnitTaxBeforeDiscount * $qty);
            }
        }

        return $this;
    }

    /**
     * Calculate item tax amount based on row total
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @param   float $rate
     * @param   array $taxGroups
     * @param   string $taxId
     * @param   boolean $recalculateRowTotalInclTax
     * @return  Mage_Tax_Model_Sales_Total_Quote
     */
    protected function _calcRowTaxAmount(
        $item,
        $rate,
        &$taxGroups = null,
        $taxId = null,
        $recalculateRowTotalInclTax = false
    ) {
        $inclTax = $item->getIsPriceInclTax();
        $subtotal = $item->getMafTaxableAmount();
        $baseSubtotal = $item->getBaseMafTaxableAmount();
        $rateKey = ($taxId == null) ? (string) $rate : $taxId;

        $hiddenTax = null;
        $baseHiddenTax = null;
        $rowTaxBeforeDiscount = null;
        $baseRowTaxBeforeDiscount = null;

        $discountAmount = $item->getMafDiscountAmount();
        $baseDiscountAmount = $item->getBaseMafDiscountAmount();

        $rowTax = $this->_calculator->calcTaxAmount(
            $subtotal - $discountAmount,
            $rate,
            $inclTax
        );
        $baseRowTax = $this->_calculator->calcTaxAmount(
            $baseSubtotal - $baseDiscountAmount,
            $rate,
            $inclTax
        );

        $rowTax = $this->_calculator->round($rowTax);
        $baseRowTax = $this->_calculator->round($baseRowTax);

        //Calculate the Row Tax before discount
        $rowTaxBeforeDiscount = $this->_calculator->calcTaxAmount(
            $subtotal,
            $rate,
            $inclTax,
            false
        );
        $baseRowTaxBeforeDiscount = $this->_calculator->calcTaxAmount(
            $baseSubtotal,
            $rate,
            $inclTax,
            false
        );

        $rowTaxBeforeDiscount = $this->_calculator->round($rowTaxBeforeDiscount);
        $baseRowTaxBeforeDiscount = $this->_calculator->round($baseRowTaxBeforeDiscount);

        if ($inclTax && $discountAmount > 0) {
            $hiddenTax = $rowTaxBeforeDiscount - $rowTax;
            $baseHiddenTax = $baseRowTaxBeforeDiscount - $baseRowTax;
            $this->_hiddenTaxes[] = array(
                'rate_key' => $rateKey,
                'qty' => 1,
                'item' => $item,
                'value' => $hiddenTax,
                'base_value' => $baseHiddenTax,
                'incl_tax' => $inclTax,
            );
        } elseif ($discountAmount > $subtotal) {
            // case with 100% discount on price incl. tax
            $hiddenTax = 0;
            $baseHiddenTax = 0;
            $this->_hiddenTaxes[] = array(
                'rate_key' => $rateKey,
                'qty' => 1,
                'item' => $item,
                'value' => $hiddenTax,
                'base_value' => $baseHiddenTax,
                'incl_tax' => $inclTax,
            );
        }
        $item->setMafTaxAmount($item->getMafTaxAmount() + $rowTax);
        $item->setBaseMafTaxAmount($item->getBaseMafTaxAmount() + $baseRowTax);
        if (is_array($taxGroups)) {
            $taxGroups[$rateKey]['maf_tax'] = $rowTax;
            $taxGroups[$rateKey]['base_maf_tax'] = $baseRowTax;
        }

        $rowTotalInclTax = $item->getMafRowTotalInclTax();
        if (!isset($rowTotalInclTax) || $recalculateRowTotalInclTax) {
            if ($this->_config->priceIncludesTax($this->_store)) {
                $item->setMafRowTotalInclTax($subtotal);
                $item->setBaseMafRowTotalInclTax($baseSubtotal);
            } else {
                $item->setMafRowTotalInclTax(
                    $item->getMafRowTotalInclTax() + $rowTaxBeforeDiscount);
                $item->setBaseMafRowTotalInclTax($item->getBaseMafRowTotalInclTax() +
                    $baseRowTaxBeforeDiscount);
            }
        }

        return $this;
    }

    /**
     * Aggregate row totals per tax rate in array
     *
     * @param   Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param   float $rate
     * @param   array $taxGroups
     * @param   int|null $taxId
     * @param   bool $recalculateRowTotalInclTax
     * @return  Omnius_Checkout_Model_Tax_Sales_Total_Quote_Tax
     */
    protected function _aggregateTaxPerRate(
        $item,
        $rate,
        &$taxGroups,
        $taxId = null,
        $recalculateRowTotalInclTax = false
    ) {
        $inclTax = $item->getIsPriceInclTax();
        $rateKey = ($taxId == null) ? (string) $rate : $taxId;
        $subtotal = $item->getMafTaxableAmount();
        $baseSubtotal = $item->getBaseMafTaxableAmount();

        if (!isset($taxGroups[$rateKey]['maf_totals'])) {
            $taxGroups[$rateKey]['maf_totals'] = array();
            $taxGroups[$rateKey]['base_maf_totals'] = array();
        }


        $discount = $item->getMafDiscountAmount();
        $baseDiscount = $item->getBaseMafDiscountAmount();

        $taxSubtotal = $subtotal - $discount;
        $baseTaxSubtotal = $baseSubtotal - $baseDiscount;

        $rowTax = $this->_calculator->calcTaxAmount($taxSubtotal, $rate, $inclTax, false);
        $baseRowTax = $this->_calculator->calcTaxAmount($baseTaxSubtotal, $rate, $inclTax, false);

        $rowTax = $this->_deltaRound($rowTax, $rateKey, $inclTax);
        $baseRowTax = $this->_deltaRound($baseRowTax, $rateKey, $inclTax, 'base');

        $item->setMafTaxAmount($item->getMafTaxAmount() + $rowTax);
        $item->setBaseMafTaxAmount($item->getBaseMafTaxAmount() + $baseRowTax);

        //Calculate the Row taxes before discount
        $rowTaxBeforeDiscount = $this->_calculator->calcTaxAmount(
            $subtotal,
            $rate,
            $inclTax,
            false
        );
        $baseRowTaxBeforeDiscount = $this->_calculator->calcTaxAmount(
            $baseSubtotal,
            $rate,
            $inclTax,
            false
        );

        $taxBeforeDiscountRounded = $this->_deltaRound($rowTaxBeforeDiscount, $rateKey, $inclTax, 'tax_before_discount');
        $baseTaxBeforeDiscountRounded = $this->_deltaRound($baseRowTaxBeforeDiscount, $rateKey, $inclTax, 'tax_before_discount_base');

        if ($inclTax && $discount > 0) {
            $roundedHiddenTax = $taxBeforeDiscountRounded - $rowTax;
            $baseRoundedHiddenTax = $baseTaxBeforeDiscountRounded - $baseRowTax;
            $this->_hiddenTaxes[] = array(
                'rate_key' => $rateKey,
                'qty' => 1,
                'item' => $item,
                'value' => $roundedHiddenTax,
                'base_value' => $baseRoundedHiddenTax,
                'incl_tax' => $inclTax,
            );
        }

        $rowTotalInclTax = $item->getRowTotalInclTax();
        if (!isset($rowTotalInclTax) || $recalculateRowTotalInclTax) {
            if ($this->_config->priceIncludesTax($this->_store)) {
                $item->setMafRowTotalInclTax($subtotal);
                $item->setBaseMafRowTotalInclTax($baseSubtotal);
            } else {
                $item->setMafRowTotalInclTax(
                    $item->getMafRowTotalInclTax() + $taxBeforeDiscountRounded);
                $item->setBaseMafRowTotalInclTax(
                    $item->getBaseMafRowTotalInclTax()
                    + $baseTaxBeforeDiscountRounded);
            }
        }

        $taxGroups[$rateKey]['maf_totals'][] = $taxSubtotal;
        $taxGroups[$rateKey]['base_maf_totals'][] = $baseTaxSubtotal;
        $taxGroups[$rateKey]['maf_tax'][] = $rowTax;
        $taxGroups[$rateKey]['base_maf_tax'][] = $baseRowTax;

        return $this;
    }
}
