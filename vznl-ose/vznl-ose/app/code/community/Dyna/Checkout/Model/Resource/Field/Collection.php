<?php

class Dyna_Checkout_Model_Resource_Field_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Dyna_Checkout_Model_Resource_Field_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('dyna_checkout/field');
    }

    /**
     * Returns all fields saved for current step
     * @param $currentStep
     * @param $quoteId
     * @return $this
     */
    public function getAllFieldsForStep($currentStep, $quoteId)
    {
        $this->addFieldToFilter("quote_id", ["eq" => $quoteId])
        ->addFieldToFilter("checkout_step", ["eq" => $currentStep]);

        return $this;
    }

    /**
     * Returns all fields saved for current step with that belong to the
     * specified package
     * @param $currentStep
     * @param $packageId
     * @return $this
     */
    public function getAllFieldsForStepFromPackage($currentStep, $packageId)
    {
        $this->addFieldToFilter("package_id", ["eq" => $packageId])
            ->addFieldToFilter("checkout_step", ["eq" => $currentStep]);

        return $this;
    }

    /**
     * Delete all fields for current step
     * @param $currentStep
     * @param $quoteId
     * @param $data
     */
    public function deleteAllFieldsForStop($currentStep, $quoteId, $data)
    {
        $currentFields =  Mage::getModel("dyna_checkout/field")
            ->getCollection()
            ->addFieldToFilter('checkout_step', $currentStep)
            ->addFieldToFilter('quote_id', $quoteId)
            ->addFieldToFilter('field_name', array('nlike' => '%bank_code%')) //@todo refactor this properly
            ->getItems();

        foreach ($currentFields as $currentField) {
            $fieldData = $currentField->getData();
            if (!array_key_exists($fieldData['field_name'], $data)) {
                $currentField->delete();
            } else {
                if ($fieldData['field_value'] != $data[$fieldData['field_name']]) {
                    $currentField->delete();
                }
            }
        }
    }

    /**
     * Mass update the current collection with the same data
     *
     * @param $data
     *
     * @return int
     */
    public function massUpdate(array $data)
    {
        $ids = $this->getAllIds();
        if(count($ids) > 0) {
            $this->getConnection()->update(
                $this->getResource()->getMainTable(),
                $data,
                'entity_id IN (' . implode(',', $ids) . ')'
            );
        }
    }
}
