<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Checkout_Model_Resource_Uct extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_Checkout_Model_Resource_Uct constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_checkout/uct', null);
    }

    /**
     * Override to correctly indicate id field
     * @return string
     */
    public function getIdFieldName()
    {
        return 'entity_id';
    }
}
