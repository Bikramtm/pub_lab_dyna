<?php

class Dyna_Checkout_Model_Client_ShippingFeeCalculateClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "shipping_fee/wsdl";
    const ENDPOINT_CONFIG_KEY = "shipping_fee/usage_url";
    const CONFIG_STUB_PATH = 'shipping_fee/use_stubs';
    const CART_ID_PREFIX = 'Q';

    /**
     * @param array $params
     * @return array
     */
    public function executeShippingFeeCalculate($params = []) : array
    {
        $params = $this->mapRequestFields($params);
        $this->setRequestHeaderInfo($params);

        $result = $this->ShippingFeeCalculate($params);
        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getShippingFee($params = [])
    {
        $result = $this->executeShippingFeeCalculate($params);

        return $result['ShippingFee']['PaidAmount'];
    }

    /**
     * @param $params
     * @return array
     */
    private function mapRequestFields(array $params) : array
    {
        // OMNVFDE-1406
        // When the CustomeNumber (BAN or MSISDN) is not know
        //  the proposition is to use the ShoppingCartID which will also be accepted by EAI for the 'Customer'>'PartyIdentification'>'ID' (boID) field.
        $shoppingCartId = Mage::getSingleton('checkout/session')->getQuoteId();

        /** @var  $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $deliveryMethod = [
            'address' => 'STD',
            'store' => 'STS',
            'pickup' => 'STS',
            'split' => 'STD'
        ];
        /**
         * OMNVFDE-1406
         * Tariff is optional, can be left empty
         * ShippingFeeTerms/ID is optional for now. Prefilled in OIL with "hardware". This should be sorted out by BA.
         */
        $parametersMapping['ShippingFeeTerms'] = [
            'ID' => 'hardware',
            'Tariff' => '',
            'DeliveryMethod' => $deliveryMethod[$params['deliveryType']],
            'Material' => '301156', // todo remove hardcode; hardcoded based on OMNVFDE-1406
            'Installment' => 'N'
        ];

        $parametersMapping['ShippingFeeTerms']['Customer']['PartyIdentification']['ID'] = $customer && $customer->getCustomerNumber() ? $customer->getCustomerNumber() : self::CART_ID_PREFIX . $shoppingCartId;

        return $parametersMapping;
    }

    /**
     * @return string Hardcoded to SAP for SAP services
     */
    public function getPartyName()
    {
        return 'SAP';
    }
}
