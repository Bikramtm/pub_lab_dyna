<?php

/**
 * Class Dyna_Catalog_Model_Resource_MobilePortingFreeDays
 */
class Dyna_Checkout_Model_Resource_MobilePortingFreeDays extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init("dyna_checkout/mobilePortingFreeDays", "entity_id");
    }
}
