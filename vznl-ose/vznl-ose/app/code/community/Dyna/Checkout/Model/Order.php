<?php
// require once the controller
require_once Mage::getModuleDir('controllers', 'Dyna_Checkout') . DS . 'IndexController.php';

class Dyna_Checkout_Model_Order
{
    const PENDING_ORDER_STATUS_PARTIAL   = 'PARTIAL COMPLETED';
    const PENDING_ORDER_STATUS_PENDING   = 'PENDING';
    const PENDING_ORDER_STATUS_IN_PROGRESS = 'ORDER_POSITION_IN_PROGRESS';
    const PENDING_ORDER_STATUS_COMPLETED = 'COMPLETED';
    const PENDING_ORDER_STATUS_CANCELLED = 'CANCELLED';

    use Dyna_Job_Model_LoggerTrait;
    /** @var Dyna_Checkout_Model_Sales_Quote */
    protected $quote;
    /** @var  int */
    protected $superOrderId;

    /**
     * If a superorder was already reserved, load that
     * @param $quoteId
     * @param Dyna_Agent_Model_Agent $agent
     * @param $superOrderId
     * @return bool
     * @throws \InvalidArgumentException
     * @internal param Dyna_Checkout_Model_Sales_Quote $quote
     * @internal param $quoteId
     */
    public function createOrder($data)
    {
        Mage::unregister('freeze_models');
        $checkout = unserialize(base64_decode($data['checkout']));
        if(!is_array($checkout)) {
            $checkout = [];
        }
        Mage::getSingleton('checkout/session')->setData($checkout);
        $customer = unserialize(base64_decode($data['customer']));
        if(!$customer) {
            $customer = [];
        }
        Mage::getSingleton('customer/session')->setData($customer);
        $address=  unserialize(base64_decode($data['address']));
        if(!$address) {
            $address = [];
        }
        Mage::getSingleton('dyna_address/storage')->setData($address);

        $quoteId = $data['quote_id'];
        $superOrderId = $data['superorder_id'];

        $this->updateCheckoutSession($quoteId);
        $this->updateCustomerSession(Mage::getModel('agent/agent')->load($data['agent_id']));

        /**
         * Recheck the sales id. If the triple sales is invalid, the order cannot be processed
         */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $salesIdsTriple = Mage::helper('bundles')->getProvisDataByRedSalesId($quote->getRedSalesId());

        if (isset($salesIdsTriple['error'])) {
            throw new InvalidArgumentException('The provided red sales ID is no longer valid. The order cannot be completed');
        }

        $controller = new Dyna_Checkout_IndexController(
            Mage::app()->getRequest(),
            Mage::app()->getResponse()
        );

        $_SERVER['REQUEST_METHOD'] = 'POST';

        $request = $controller->getRequest();
        $request->setPost('superOrderId', $superOrderId);
        $response = $controller->getResponse();


        $controller->createOrder($superOrderId);
        $createOrderResponseJSON = $response->getBody();
        $createOrderResponse = Mage::helper('core')->jsonDecode($createOrderResponseJSON, true);



        if ($createOrderResponse['error']) {
            throw new Exception($createOrderResponse['message']);
        }

        $quote = Mage::getSingleton('checkout/session')->getQuote();
        if ($request->getPost('current_step')) {
            $quote->setCurrentStep($request->getPost('current_step'));
        }
        $quote->save();

        // Mark the quote as not a cart
        if ($quote->getQuoteParentId()) {
            $parentQuote = Mage::getModel('sales/quote')->load($quote->getQuoteParentId());
            if ($parentQuote->getId()) {
                $parentQuote->setCartStatus(null)
                    ->save();
            }
        }


        $customerHelper = Mage::helper('dyna_customer');
        $customerHelper->unloadCustomer();


        $this->clearSession();

        return true;
    }

    protected function updateCheckoutSession($quoteId)
    {
        $checkoutSession = Mage::getSingleton('checkout/session');
        $checkoutSession->setLoadInactive(true)->setQuoteId($quoteId);
    }

    protected function updateCustomerSession($agent)
    {
        $customerSession = Mage::getSingleton('customer/session');
        $customerSession->setAgent($agent);
    }

    protected function clearSession()
    {
        Mage::getSingleton('checkout/session')->clear();
        Mage::getSingleton('checkout/session')->unsetAll();
        Mage::getSingleton('core/session')->clear();
        Mage::getSingleton('customer/session')->clear();
        Mage::getSingleton('dyna_address/storage')->clear();
    }

}
