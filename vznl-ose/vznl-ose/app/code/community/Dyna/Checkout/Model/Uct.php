<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Checkout_Model_Uct extends Mage_Core_Model_Abstract
{
    /**
     * Dyna_Checkout_Model_Uct constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_checkout/uct');
    }
}