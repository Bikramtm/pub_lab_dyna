<?php

class Dyna_Checkout_Model_PackageNotes extends Mage_Core_Model_Abstract {

    protected function _construct()
    {
        $this->_init('dyna_checkout/packageNotes');
    }

}