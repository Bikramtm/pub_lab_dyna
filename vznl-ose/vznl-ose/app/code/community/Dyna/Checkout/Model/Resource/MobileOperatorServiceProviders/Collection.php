<?php


/**
 * Class Dyna_Checkout_Model_Resource_MobileOperatorServiceProviders_Collection
 */
class Dyna_Checkout_Model_Resource_MobileOperatorServiceProviders_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Dyna_Checkout_Model_Resource_MobileOperatorServiceProviders_Collection constructor
     */
    protected function _construct()
    {
        $this->_init('dyna_checkout/mobileOperatorServiceProviders');
    }
}