<?php

/**
 * Class Dyna_Checkout_Model_Sales_Quote_Address_Total_Grand
 */
class Dyna_Checkout_Model_Sales_Quote_Address_Total_Grand extends Omnius_Checkout_Model_Sales_Quote_Address_Total_Grand
{
    /**
     * Collect grand total address amount
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Grand
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {

        $grandTotal = $address->getMafGrandTotal();
        $baseGrandTotal = $address->getBaseMafGrandTotal();

        $store = $address->getQuote()->getStore();
        $totals = array_sum($address->getAllMafTotalAmounts());
        $totals = $store->roundPrice($totals);
        $baseTotals = array_sum($address->getAllBaseMafTotalAmounts());
        $baseTotals = $store->roundPrice($baseTotals);

        // Price handler
        $address->setInitialMixmatchTotal(0);
        $address->setInitialMixmatchSubtotal(0);
        $address->setInitialMixmatchTax(0);

        $address->getQuote()->setInitialMixmatchTotal(0);
        $address->getQuote()->setInitialMixmatchSubtotal(0);
        $address->getQuote()->setInitialMixmatchTax(0);

        $mixmatchSubtotal = 0;
        $mixmatchTax = 0;
        $mixmatchTotal = 0;

        // MAF handler
        $address->setInitialMixmatchMafTotal(0);
        $address->setInitialMixmatchMafSubtotal(0);
        $address->setInitialMixmatchMafTax(0);

        $address->getQuote()->setInitialMixmatchMafTotal(0);
        $address->getQuote()->setInitialMixmatchMafSubtotal(0);
        $address->getQuote()->setInitialMixmatchMafTax(0);

        $mixmatchMafSubtotal = 0;
        $mixmatchMafTax = 0;
        $mixmatchMafTotal = 0;

        foreach ($this->_getAddressItems($address) as $item) {
            if ($item->getMixmatchSubtotal() !== null) {
                $mixmatchSubtotal += $item->getMixmatchSubtotal() - ($item->getDiscountAmount() - $item->getHiddenTaxAmount());
                $mixmatchTax += $item->getMixmatchTax() - $item->getHiddenTaxAmount();
                $mixmatchTotal += $item->getMixmatchSubtotal() + $item->getMixmatchTax() - $item->getDiscountAmount();
            } else {
                $mixmatchSubtotal += $item->getItemFinalPriceExclTax();
                $mixmatchTax += $item->getTaxAmount();
                $mixmatchTotal += $item->getItemFinalPriceInclTax();
            }

            if ($item->getMixmatchMafSubtotal() !== null) {
                $mixmatchMafSubtotal += $item->getMixmatchMafSubtotal() - ($item->getMafDiscountAmount() - $item->getHiddenMafTaxAmount());
                $mixmatchMafTax += $item->getMixmatchMafTax() - $item->getHiddenMafTaxAmount();
                $mixmatchMafTotal += $item->getMixmatchMafSubtotal() + $item->getMixmatchMafTax() - $item->getMafDiscountAmount();
            } else {
                $mixmatchMafSubtotal += $item->getItemFinalMafExclTax();
                $mixmatchMafTax += $item->getMafTaxAmount();
                $mixmatchMafTotal += $item->getItemFinalMafInclTax();
            }
        }

        // Added to handle minus maf and prevent totals with minus
        $mixmatchMafTotal = max($mixmatchMafTotal, 0);
        $mixmatchMafSubtotal = max($mixmatchMafSubtotal, 0);
        $mixmatchMafTax = max($mixmatchMafTax, 0);

        $address->setInitialMixmatchTotal($mixmatchTotal);
        $address->setInitialMixmatchSubtotal($mixmatchSubtotal);
        $address->setInitialMixmatchTax($mixmatchTax);

        $address->getQuote()->setInitialMixmatchTotal($mixmatchTotal);
        $address->getQuote()->setInitialMixmatchSubtotal($mixmatchSubtotal);
        $address->getQuote()->setInitialMixmatchTax($mixmatchTax);

        $address->setInitialMixmatchMafTotal($mixmatchMafTotal);
        $address->setInitialMixmatchMafSubtotal($mixmatchMafSubtotal);
        $address->setInitialMixmatchMafTax($mixmatchMafTax);

        $address->getQuote()->setInitialMixmatchMafTotal($mixmatchMafTotal);
        $address->getQuote()->setInitialMixmatchMafSubtotal($mixmatchMafSubtotal);
        $address->getQuote()->setInitialMixmatchMafTax($mixmatchMafTax);

        $address->setMafGrandTotal($grandTotal + $totals);
        $address->setBaseMafGrandTotal($baseGrandTotal + $baseTotals);

        return $this;
    }
}
