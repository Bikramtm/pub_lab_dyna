<?php

/**
 * Class Dyna_Checkout_Model_Observer
 */
class Dyna_Checkout_Model_Observer extends Mage_Core_Model_Abstract
{
    /**
     * Method that notifies quote to clear instance packages and bundle helper to clear its active bundles list - if needed
     * @param $event
     */
    public function updatePackagesDependencies($event)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Dyna_Package_Model_Package $package */
        $package = $event->getAlteredPackage();

        if ($package->getQuoteId() == $quote->getId()) {
            // clear quote instance cached packages
            $quote->clearCachedPackages();
            Mage::unregister('quote_packages_' . $quote->getId() . '_' . md5($quote->getUpdatedAt()));
            // clear bundles helper cached active bundles
            if (!empty($package->getBundles())) {
                Mage::helper('dyna_bundles')->clearCachedActiveBundles();
            }
        }
    }

    /**
     * Set item visibility after product has been added to cart
     * @param $event
     */
    public function updateItemVisibility($event)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItem */
        $quoteItem = $event->getQuoteItem();
        /** @var Dyna_Catalog_Model_Product $product */
        $product = $event->getProduct();
        $quoteItem->setProductVisibility($product->getProductVisibility());
    }

    /**
     * Event triggered in the Dyna_Checkout_Model_Sales_Quote::cloneQuote method before adding a package to the new quote
     * @param $event
     */
    public function updatePackageAfterClone($event)
    {
        /** @var Dyna_Package_Model_Package $installedBasePackage */
        $installedBasePackage = ($event->getPackage()->getEditingDisabled()) ? $event->getPackage() : null;

        if ($installedBasePackage) {
            $bundleTypes = explode(",", $installedBasePackage->getBundleTypes());
            $hasRedplus = false;
            foreach ($bundleTypes as $bundleType) {
                if ($bundleType == Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS) {
                    $hasRedplus = true;
                    break;
                }
            }

            if (!$hasRedplus) {
                // Do not clone installed base packages
                $event->getPackage()->setSkipSave(true);
            }
        } else {
            // Remove bundle ID from the new quote
            $event->getPackage()
                ->setBundleId(null)
                ->setBundleName(null);
        }
    }

    /**
     * Event triggered in the Dyna_Checkout_Model_Sales_Quote::cloneQuote method for each quote item that was added to the new quote
     * @param $event
     */
    public function updateQuoteItemAfterClone($event)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quoteItem = $event->getQuoteItem();
        $quoteId = $quoteItem->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        $package = $quote->getCartPackage($quoteItem->getPackageId());

        $beforeBundleProducts = $package->getBeforeBundleProducts() ? Mage::helper('core')->jsonDecode($package->getBeforeBundleProducts()) : [];

        if (!empty($beforeBundleProducts) && !in_array($quoteItem->getSku(), $beforeBundleProducts)) {
            // Remove all products that were added by the bundle actions
            $event->getQuoteItem()->isDeleted(true);
        } else {
            // Remove the bundle component marker from the quote item
            $event->getQuoteItem()->setBundleComponent(0);
        }
    }
}
