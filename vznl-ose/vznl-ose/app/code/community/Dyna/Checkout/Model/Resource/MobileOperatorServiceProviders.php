<?php

/**
 * Class Dyna_Checkout_Model_Resource_MobileOperatorServiceProviders
 */
class Dyna_Checkout_Model_Resource_MobileOperatorServiceProviders extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init("dyna_checkout/mobileOperatorServiceProviders", "entity_id");
    }
}