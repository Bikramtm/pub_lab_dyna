<?php

/**
 * Class Dyna_Checkout_Model_Sales_Quote_Item
 * @method $this setBundleComponent(string $bundleId)
 * @method int getBundleComponent()
 * @method bool getIsDefaulted()
 * @method int getIsContract()
 * @method int getIsContractDrop()
 * @method $this setIsContract(int $is)
 * @method bool getIsServiceDefaulted()
 * @method string getSystemAction()
 * @method $this setInMinimumDuration(int $value)
 * @method int getInMinimumDuration()
 * @method Dyna_Catalog_Model_Product getProduct()
 * @method int getAlternateQuoteId()
 * @method $this setAlternateQuoteId(int $quoteId)
 * @method $this setContractEndDate()
 * @method $this getContractEndDate()
 * @method $this setContractPossibleCancellationDate()
 * @method $this getContractPossibleCancellationDate()
 */
class Dyna_Checkout_Model_Sales_Quote_Item extends Omnius_Checkout_Model_Sales_Quote_Item
{
    const ACQUISITION = Dyna_Catalog_Model_ProcessContext::ACQ;
    const RETENTION = Dyna_Catalog_Model_ProcessContext::RETBLU;
    const INLIFE = Dyna_Catalog_Model_ProcessContext::ILSBLU;

    const SYSTEM_ACTION_EXISTING = "existing";
    const SYSTEM_ACTION_ADD = "add";
    const SYSTEM_ACTION_REMOVE = "remove";
    const SYSTEM_ACTION_NOT_APPLICABLE = "N/A";
    /**
     * @return mixed
     */
    public function isPromotion()
    {
        if ($this->getProduct()->isPromotion()) {
            $this->setIsPromotion(true);
        }

        return $this->getIsPromotion();
    }

    /**
     * Prevent persisting deleted items
     * @return Mage_Core_Model_Abstract
     */
    public function save()
    {
        return $this->isDeleted() ? $this->delete() : parent::save();
    }

    /**
     * Determine whether or not current quote item has a mix match price
     * @return bool
     */
    public function hasMixmatchPrice()
    {
        return (bool)($this->getMixmatchSubtotal() !== null || $this->getMixmatchMafSubtotal() !== null);
    }

    /**
     * @return mixed
     */
    public function getMixmatchMafPriceWithTax()
    {
        return $this->getMixmatchMafSubtotal() + $this->getMixmatchMafTax();
    }

    /**
     * Cannot apply a discount to a discount product (which has negative price) so the use of hidden tax amount is not needed
     * @param float $value
     * @return self|Mage_Sales_Model_Quote_Item
     */
    public function setHiddenTaxAmount(float $value)
    {
        if ($this->isPromotion()) {
            return $this;
        }

        return parent::setHiddenTaxAmount($value);
    }

    /**
     * Get the package instance to which this item belongs
     * @return Dyna_Package_Model_Package|false
     */
    public function getPackage()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        foreach ($this->getQuote()->getCartPackages() as $package) {
            if ($package->getPackageId() == $this->getPackageId()) {
                return $package;
            }
        }

        return false;
    }

    /**
     * Method that returns the quote item that contains the product targeted by a promo item
     * @return $this|null
     */
    public function getTargetItem()
    {
        if ($this->isPromo() || $this->isBundlePromo()) {
            foreach ($this->getQuote()->getAllItems() as $item) {
                if ($this->getPackageId() === $item->getPackageId()
                    && $this->getTargetId() === $item->getProductId()
                ) {
                    return $item;
                }
            }
        }

        return null;
    }

    /**
     * Returns true if the promo product was added by a bundle rule
     * @return mixed
     */
    public function isBundlePromo()
    {
        return $this->getData('is_bundle_promo');
    }

    /**
     * Returns true if the item was added from installed base
     * @return mixed
     */
    public function isContract()
    {
        return $this->getData('is_contract');
    }

    /**
     * Get calculation price used for quote calculation in base currency.
     *
     * @return float
     */
    public function getBaseCalculationMaf()
    {
        if (!$this->hasBaseCalculationMaf()) {
            $price = $this->getCalculationMaf() ?: ($this->getCustomMaf() ?: $this->getMaf());
            $this->setBaseCalculationMaf($price);
        }

        return $this->_getData('base_calculation_maf');
    }

    /**
     * @param string $systemAction
     * @return $this
     */
    public function setSystemAction(string $systemAction)
    {
        if ($this->isContract()) {
            if ($this->getSystemAction() == self::SYSTEM_ACTION_EXISTING) {
                if ($systemAction == self::SYSTEM_ACTION_NOT_APPLICABLE && $this->getAlternateQuoteId() == null) {
                    $this->setData('system_action', self::SYSTEM_ACTION_NOT_APPLICABLE);
                }
            } else if ($this->getSystemAction() == self::SYSTEM_ACTION_NOT_APPLICABLE) {
                if ($systemAction == self::SYSTEM_ACTION_ADD || $systemAction == self::SYSTEM_ACTION_REMOVE) {
                    $this->setData('system_action', self::SYSTEM_ACTION_EXISTING);
                }
            } else {
                $this->setData('system_action', $systemAction);
            }
        } else {
            $this->setData('system_action', $systemAction);
        }
        return $this;
    }

    public function getPackageCreationTypeId() {
        if ($this->getPackage()) {
            return $this->getPackage()->getPackageCreationTypeId();
        } else {
            return $this->getData('package_creation_type_id');
        }
    }
}
