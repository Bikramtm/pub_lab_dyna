<?php
/**
 * Class Dyna_Checkout_Model_Client_GetAvailableShippingOptionsClient
 */
class Dyna_Checkout_Model_Client_GetAvailableShippingOptionsClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "shipping_options/wsdl";
    const ENDPOINT_CONFIG_KEY = "shipping_options/usage_url";
    const CONFIG_STUB_PATH = 'shipping_options/use_stubs';

    /** Mage::getModel("dyna_checkout/client_GetAvailableShippingOptionsClient"); */


    /**
     * @param array $params
     * @return mixed
     */
    public function executeGetAvailableShippingOptions($parameters = [])
    {
        /**
         * $params = [
         *
         * ]
         */

        $params['ShippingCondition'] = array(
            'ServiceProviderParty' => array(
                'Party' => array(
                    'PartyName' => 1,
                    'AccountCategory' => 2,
                    'IdentificationMethod' => 3,
                    'CashOnDelivery' => 4
                )
            ),
            'Address' => array(
                'PostalZone' => $parameters['postcode'],
                'LocationCoordinate' => array(
                    'LatitudeDirectionCode' => 55,
                    'LongitudeDirectionCode' => 56
                ),
                'Radius' => Mage::getStoreConfig('omnius_service/google_maps_api/radius') ?? 10
            )
        );

        $this->setRequestHeaderInfo($params);

        $result = $this->RetrieveAvailableShippingOptions($params);

        if(!$result || $result['Success'] == false){
            return null;
        } else {
            return $result['Account'];
        }
    }
}