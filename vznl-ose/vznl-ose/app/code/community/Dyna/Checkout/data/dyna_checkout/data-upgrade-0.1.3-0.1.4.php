<?php
$websiteIds = Mage::getModel('core/website')->getCollection()
    ->addFieldToFilter('website_id', array('neq'=>0))
    ->getAllIds();

$product = Mage::getModel('catalog/product');
$product->setStoreId(0);
$product->setWebsiteIds($websiteIds);
$product->setTypeId('virtual');
$product->addData(array(
    'name' => 'Mobile Shipping',
    'attribute_set_id' => $product->getDefaultAttributeSetId(), //use the default attribute set or an other id if needed.
    'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED, //set product as enabled
    'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH, //set visibility in catalog and search
    'meta_title' => 'Mobile Shipping',
    'weight' => 1,
    'sku' => 'shippingfee',
    'tax_class_id' => 2,
    'description' => 'Mobile Shipping',
    'short_description' => 'Mobile Shipping',
    'stock_data' => array( //set stock data
        'manage_stock' => 1,
        'qty' => 9999999999,
        'use_config_manage_stock' => 1,
        'use_config_min_sale_qty' => 1,
        'use_config_max_sale_qty' => 1,
        'use_config_enable_qty_increments' => 1,
        'in_stock' => 1
    ),

));
$product->save();