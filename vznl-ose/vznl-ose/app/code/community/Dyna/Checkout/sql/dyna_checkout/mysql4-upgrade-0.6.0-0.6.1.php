<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->modifyColumn('sales_flat_quote_checkout_fields', 'field_name', 'VARCHAR(255) default NULL');
$installer->endSetup();