<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'is_bundle_promo', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment' => 'Is promotion product added by bundle rules',
        'default' => 0,
        'after'   => 'is_promo'
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'is_bundle_promo', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment' => 'Is promotion product added by bundle rules',
        'default' => 0,
        'after'   => 'is_promo'
    ));

// add quote address item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'is_bundle_promo', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment' => 'Is promotion product added by bundle rules',
        'default' => 0,
        'after'   => 'is_promo'
    ));

$installer->endSetup();