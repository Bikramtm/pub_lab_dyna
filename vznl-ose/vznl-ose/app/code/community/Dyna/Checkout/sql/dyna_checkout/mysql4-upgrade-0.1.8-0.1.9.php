<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn('dealer_group', 'description', 'text');

$installer->endSetup();
