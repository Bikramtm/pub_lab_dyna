<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE and ORDER item attributes */
$quoteOrderItemAttr = array(
    'has_non_standard_rate' => array(
        'comment' => 'Attribute to know if the item has a NonStandardRate',
        'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'required' => false
    ),
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order_item', $attributeCode, $attributeProp);
}
/* end QUOTE and ORDER item attributes */

$salesInstaller->endSetup();
