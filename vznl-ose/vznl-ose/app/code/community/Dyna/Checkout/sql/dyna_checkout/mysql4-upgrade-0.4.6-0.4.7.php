<?php

$cssContent = <<< End
    <style>
        @page {
            margin: 0px;
        }

        body {
            color: #000000;
            font-size: 14px;
            line-height: 22px;
            font-family: "Arial Rounded MT", Arial, Helvetica, sans-serif;
            margin: 30px 50px 20px 50px;
        }

        h1, h2 {
            color: #E60000;
            margin: 0;
        }

        h3, h4 {
            margin: 0;
        }

        h1 {
            color: #E60000;
            font-size: 28px;
            line-height: 32px;
            font-family: "Arial Rounded MT", Arial, Helvetica, sans-serif;
            font-weight: bold;
        }

        h2 {
            font-size: 24px;
            line-height: 28px;
            font-weight: normal;
        }

        .ghost-table {
            border: none;
        }

        .ghost-table td {
            background: none
        }

        .greetings {
            padding-top: 30px;
        }

        .call-back dl {
            margin: 1em 0;
        }

        .call-back dt {
            float: left;
            width: 60px;
        }

        .ending {
            line-height: 2;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
            margin-top: 10px;
            margin-bottom: 40px;
            padding: 0;
        }

        th, td {
            padding: 15px 10px;
            text-align: left;
        }

        td {
            padding: 8px 10px;
            background-color: #eeeeee;
        }

        .borderless {
            border-style: none;
        }

        .no-bg {
            background-color: transparent;
        }

        .center {
            border-left-style: solid;
            border-right-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-top {
            border-top-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-bottom {
            border-bottom-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-left {
            border-left-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-right {
            border-right-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder {
            border-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .additional {
            font-size: smaller;
        }

        .narrow {
            width: 80px;
        }

        .wider {
            width: 360px;
        }

        .mwst {
            font-weight: normal;
            font-size: smaller;
        }

        .table-spacing {
            border-style: none;
            height: 20px;
        }

        .empty-row {
            border-bottom-style: solid;
            border-bottom-width: thin;
        }

        .title h1 {
            padding: 50px 0 45px 85px;
        }

        .personal-data {
            padding-bottom: 30px;
        }

        .presonal-data {
            font-size: 16px;
            line-height: 20px;
        }

        .personal-data td {
            padding: 10px 20px 10px 0;
        }

        .offer-table {
            margin-top: 2em;
        }

        .offer-table h2 {
            padding-top: 20px;
        }

        .price {
            text-align: right;
            font-weight: bold;
            color: #262626;
            font-size: 16px;
            line-height: 22px;
        }

        img.top-logo {
            position: absolute;
            top: -30px;
            left: 0px;
        }

        .adresses {
            padding: 10px 20px 10px 0px;
            vertical-align: top;
            display: inline-block;
        }

        h3.delivery-address {
            margin-bottom: 1em;
        }

        .delivery-img {
            height: 2.5em;
        }

        .discount {
            color: #427D00;
        }

        h3 {
            color: #333333;
            font-size: 20px;
            line-height: 24px;
        }

        h4 {
            color: #262626;
            font-size: 16px;
            line-height: 22px;
        }

        h5 {
            color: #262626;
            font-size: 14px;
            line-height: 24px;
            margin: 0;
        }

        td.item-name {
            color: #333333;
            font-size: 14px;
            line-height: 20px;
        }

        .price-bold {
            font-size: 20px;
            line-height: 24px;
        }

        .black h4 {
            color: #000000;
        }
        .ghost-table td {
            padding: 0;
            vertical-align: text-top;
        }

        .ghost-table th {
            padding: 0;
        }
        td.monthly, th.monthly {
            width: 18.57%;
        }
        td.once, th.once {
            width: 17.14%;
        }
        tr.empty-row td {
            height: 30px;
            padding: 0;
        }
        .sum-table {
            margin-bottom: 24px;
        }
        .order-table {
            margin-bottom: 40px;
            margin-top: 0;
        }
        .hidden{
            display: none;
            visibility: hidden;
        }
        .order-no {
            border: none;
            margin-bottom: 1em;
        }
        .goes-by {
            margin-top: 2em;
        }
        .personal-data-table {
            margin-bottom: 0;
        }
    </style>
End;

$contentGigaKombiDSL = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p>Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{else}}
        <strong>Rufen Sie uns dazu einfach an</strong>,<br>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{/if}}
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    <p>Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen. Daher ist dieses Angebot nicht vertraglich bindend.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentGigaKombiKabel = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p>Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{else}}
        <strong>Rufen Sie uns dazu einfach an</strong>,<br>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{/if}}
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    <p>Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen. Daher ist dieses Angebot nicht vertraglich bindend.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentGigaKombiMobile = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p>Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{else}}
        <strong>Rufen Sie uns dazu einfach an</strong>,<br>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{/if}}
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    <p>Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen. Daher ist dieses Angebot nicht vertraglich bindend.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentSummaryDSL = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p>Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{else}}
        <strong>Rufen Sie uns dazu einfach an</strong>,<br>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{/if}}
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    <p>Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen. Daher ist dieses Angebot nicht vertraglich bindend.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentSummaryKabel = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p>Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{else}}
        <strong>Rufen Sie uns dazu einfach an</strong>,<br>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{/if}}
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    <p>Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen. Daher ist dieses Angebot nicht vertraglich bindend.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentSummaryMobile = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p>Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{else}}
        <strong>Rufen Sie uns dazu einfach an</strong>,<br>
        jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
        <strong>unter {{var hotlineNumber}}</strong>
        {{/if}}
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentArray = [
    [   "title" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_DSL_TITLE,
        "key" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_DSL_KEY,
        "content" => $contentGigaKombiDSL
    ],
    [
        "title" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_KABEL_TITLE,
        "key" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_KABEL_KEY,
        "content" => $contentGigaKombiKabel
    ],
    [
        "title" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_MOBILE_TITLE,
        "key" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_MOBILE_KEY,
        "content" => $contentGigaKombiMobile
    ],
    [
        "title" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_DSL_TITLE,
        "key" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_DSL_KEY,
        "content" => $contentSummaryDSL
    ],
    [
        "title" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_KABEL_TITLE,
        "key" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_KABEL_KEY,
        "content" => $contentSummaryKabel
    ],
    [
        "title" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_MOBILE_TITLE,
        "key" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_MOBILE_KEY,
        "content" => $contentSummaryMobile
    ]
];

$store = Mage::getModel('core/store')
    ->getCollection()
    ->addFieldToFilter('code', ['eq'=>'admin'])
    ->getFirstItem();

/** @var Mage_Cms_Model_Block $blockModel */
$blockModel = Mage::getModel('cms/block');

foreach($contentArray as $content) {
    $blockModel = Mage::getModel('cms/block');
    $blockId = $blockModel
        ->load('offer_block_'.$content['key'])
        ->getId();

    if (!$blockId) {
        $blockModel->setTitle($content['title'])
            ->setIdentifier('offer_block_'.$content['key'])
            ->setStores(array($store->getId()))
            ->setIsActive(1)
            ->setContent($cssContent . $content['content'])
            ->save();
    } else {
        $loadedBlock = $blockModel->load('offer_block_' . $content['key']);
        $loadedBlock->setContent($cssContent . $content['content'])
            ->save();
    }
}