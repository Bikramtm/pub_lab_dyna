<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->modifyColumn($this->getTable('sales/quote'), 'sales_id', 'VARCHAR(500) default NULL');
$installer->endSetup();