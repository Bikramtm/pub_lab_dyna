<?php
/**
 * Installer that creates the checkout fields resource table
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;

$installer->startSetup();

$uctTable = new Varien_Db_Ddl_Table();
$uctTable->setName('sales_flat_quote_uct');

$uctTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
        "unsigned" => true,
        "primary" => true,
        "auto_increment" => true,
        'nullable' => false
    ], "Primary key for fields table")
    ->addColumn('transaction_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, [
    ], "The transaction id coming from UCT")
    ->addColumn('caller_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
    ], "Caller Id coming from UCT")
    ->addColumn("campaign_id", Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
    ], "Campaign ID coming from UCT")
    ->addColumn("created_at", Varien_Db_Ddl_Table::TYPE_DATETIME, null, [
    ], "The date and time when this entry was saved");

if(!$this->getConnection()->isTableExists($uctTable->getName())) {
    $this->getConnection()->createTable($uctTable);
}

$installer->endSetup();
