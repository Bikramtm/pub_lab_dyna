<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$this->getConnection()->addColumn($this->getTable('package/package'), 'bundle_id', 'varchar(255)');
$this->getConnection()->addColumn($this->getTable('package/package'), 'bundle_name', 'varchar(255)');

$newColumns = array(
    'action_id',
    'campaign_code',
    'marketing_code'
);

foreach($newColumns as $newColumn){
    $this->getConnection()->addColumn($this->getTable('sales/quote'), $newColumn, 'varchar(255)');
    $this->getConnection()->addColumn($this->getTable('sales/order'), $newColumn, 'varchar(255)');
}

$installer->endSetup();
