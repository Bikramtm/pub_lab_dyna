<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$this->getConnection()->addColumn($this->getTable('sales/quote'), 'offer_msisdn', 'varchar(25)');
$this->getConnection()->addColumn($this->getTable('sales/order'), 'offer_msisdn', 'varchar(25)');

$installer->endSetup();
