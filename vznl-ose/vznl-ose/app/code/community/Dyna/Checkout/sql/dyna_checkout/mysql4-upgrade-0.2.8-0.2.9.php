<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->changeColumn($this->getTable('sales/quote_item'), 'quote_id', 'quote_id',
        ["type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
            "length" => 10,
            "unsigned" => true,
            "nullable" => true,
            "comment" => "Quote Id"
        ]
    );
$installer->getConnection()->addColumn($this->getTable('sales/quote_item'), 'alternate_quote_id',
        ["type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
            "after" => "quote_id",
            "length" => 10,
            "unsigned" => true,
            "comment" => "Alternate Quote Id (ILS)"
        ]
    );
$installer->getConnection()->addForeignKey(
        $this->getFkName($this->getTable('sales/quote_item'), 'alternate_quote_id', $this->getTable('sales/quote'), 'entity_id'),
        $this->getTable('sales/quote_item'), 'alternate_quote_id', $this->getTable('sales/quote'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    );


$installer->endSetup();