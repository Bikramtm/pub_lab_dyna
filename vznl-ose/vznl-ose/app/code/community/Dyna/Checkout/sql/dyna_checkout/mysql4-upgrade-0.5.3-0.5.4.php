<?php

$offerAndCallSummaryCssContent = <<< End
    <style>
        @page {
            margin: 0px;
        }

        body {
            color: #000000;
            font-size: 14px;
            line-height: 22px;
            font-family: "Arial Rounded MT", Arial, Helvetica, sans-serif;
            margin: 30px 50px 20px 50px;
        }

        h1, h2 {
            color: #E60000;
            margin: 0;
        }

        h3, h4 {
            margin: 0;
        }

        h1 {
            color: #E60000;
            font-size: 28px;
            line-height: 32px;
            font-family: "Arial Rounded MT", Arial, Helvetica, sans-serif;
            font-weight: bold;
        }

        h2 {
            font-size: 24px;
            line-height: 28px;
            font-weight: normal;
        }

        .ghost-table {
            border: none;
        }

        .ghost-table td {
            background: none
        }

        .greetings {
            padding-top: 30px;
        }

        .call-back dl {
            margin: 1em 0;
        }

        .call-back dt {
            float: left;
            width: 60px;
        }

        .ending {
            line-height: 2;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
            margin-top: 10px;
            margin-bottom: 40px;
            padding: 0;
        }

        th, td {
            padding: 15px 10px;
            text-align: left;
        }

        td {
            padding: 8px 10px;
            background-color: #eeeeee;
        }

        .borderless {
            border-style: none;
        }

        .no-bg {
            background-color: transparent;
        }

        .center {
            border-left-style: solid;
            border-right-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-top {
            border-top-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-bottom {
            border-bottom-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-left {
            border-left-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-right {
            border-right-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder {
            border-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .additional {
            font-size: smaller;
        }

        .narrow {
            width: 80px;
        }

        .wider {
            width: 360px;
        }

        .mwst {
            font-weight: normal;
            font-size: smaller;
        }

        .table-spacing {
            border-style: none;
            height: 20px;
        }

        .empty-row {
            border-bottom-style: solid;
            border-bottom-width: thin;
        }

        .title h1 {
            padding: 50px 0 45px 85px;
        }

        .personal-data {
            padding-bottom: 30px;
        }

        .presonal-data {
            font-size: 16px;
            line-height: 20px;
        }

        .personal-data td {
            padding: 10px 20px 10px 0;
        }

        .offer-table {
            margin-top: 2em;
        }

        .offer-table h2 {
            padding-top: 20px;
        }

        .price {
            text-align: right;
            font-weight: bold;
            color: #262626;
            font-size: 16px;
            line-height: 22px;
        }

        img.top-logo {
            position: absolute;
            top: -30px;
            left: 0px;
        }

        .adresses {
            padding: 10px 20px 10px 0px;
            vertical-align: top;
            display: inline-block;
        }

        h3.delivery-address {
            margin-bottom: 1em;
        }

        .delivery-img {
            height: 2.5em;
        }

        .discount {
            color: #427D00;
        }

        h3 {
            color: #333333;
            font-size: 20px;
            line-height: 24px;
        }

        h4 {
            color: #262626;
            font-size: 16px;
            line-height: 22px;
        }

        h5 {
            color: #262626;
            font-size: 14px;
            line-height: 24px;
            margin: 0;
        }

        td.item-name {
            color: #333333;
            font-size: 14px;
            line-height: 20px;
        }

        .price-bold {
            font-size: 20px;
            line-height: 24px;
        }

        .black h4 {
            color: #000000;
        }
        .ghost-table td {
            padding: 0;
            vertical-align: text-top;
        }

        .ghost-table th {
            padding: 0;
        }
        td.monthly, th.monthly {
            width: 18.57%;
        }
        td.once, th.once {
            width: 17.14%;
        }
        tr.empty-row td {
            height: 30px;
            padding: 0;
        }
        .sum-table {
            margin-bottom: 24px;
        }
        .order-table {
            margin-bottom: 40px;
            margin-top: 0;
        }
        .hidden{
            display: none;
            visibility: hidden;
        }
        .order-no {
            border: none;
            margin-bottom: 1em;
        }
        .goes-by {
            margin-top: 2em;
        }
        .personal-data-table {
            margin-bottom: 0;
        }
    </style>
End;

$contentOfferGigaKombiDSL = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p class="header">Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        {{else}}
        <p class="bold-header">Rufen Sie uns dazu einfach an,</p>
        {{/if}}
        <p class="center-align">
            jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
            <strong>unter {{var hotlineNumber}}</strong>
        </p>
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    <p>Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen. Daher ist dieses Angebot nicht vertraglich bindend.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentOfferGigaKombiKabel = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p class="header">Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        {{else}}
        <p class="bold-header">Rufen Sie uns dazu einfach an,</p>
        {{/if}}
        <p class="center-align">
            jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
            <strong>unter {{var hotlineNumber}}</strong>
        </p>
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    <p>Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen. Daher ist dieses Angebot nicht vertraglich bindend.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentOfferGigaKombiMobile = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p class="header">Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        {{else}}
        <p class="bold-header">Rufen Sie uns dazu einfach an,</p>
        {{/if}}
        <p class="center-align">
            jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
            <strong>unter {{var hotlineNumber}}</strong>
        </p>
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    <p>Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen. Daher ist dieses Angebot nicht vertraglich bindend.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentOfferSummaryDSL = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p class="header">Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        {{else}}
        <p class="bold-header">Rufen Sie uns dazu einfach an,</p>
        {{/if}}
        <p class="center-align">
            jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
            <strong>unter {{var hotlineNumber}}</strong>
        </p>
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    <p>Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen. Daher ist dieses Angebot nicht vertraglich bindend.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentOfferSummaryKabel = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p class="header">Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        {{else}}
        <p class="bold-header">Rufen Sie uns dazu einfach an,</p>
        {{/if}}
        <p class="center-align">
            jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
            <strong>unter {{var hotlineNumber}}</strong>
        </p>
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    <p>Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen. Daher ist dieses Angebot nicht vertraglich bindend.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentOfferSummaryMobile = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" /><h1>Unser Angebot für Sie</h1>
</div>
<div>
    <div class="greetings">
        <p><b>{{var greetings}},</b></p>
        <p>danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.</p>
    </div>
    <div class="call-back">
        {{if callbackDateTime}}
        <strong>
            <strong>Wir rufen Sie zurück – wie vereinbart:</strong>
            <dl>
                <dt><b>Datum:</b></dt>
                <dd>{{var callbackDate}}</dd>
                <dt><b>Uhrzeit:</b></dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p class="header">Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns an. Sie erreichen uns</p>
        {{else}}
        <p class="bold-header">Rufen Sie uns dazu einfach an,</p>
        {{/if}}
        <p class="center-align">
            jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
            <strong>unter {{var hotlineNumber}}</strong>
        </p>
    </div>
</div>
<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Angebotsnummer</strong></td>
                        <td><span>: {{var offerId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Angebotsdatum</strong></td>
                        <td><span>: {{var offerDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var offer_table}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
</div>
<div class="ending">
    <h2>Passt alles für Sie?</h2>
    <div>
        <p>
            Dann freuen wir uns, wenn Sie unser Angebot annehmen. Vielen Dank für Ihr Vertrauen!
        </p>
    </div>
    <div>
        <p>
            Freundliche Grüße,<br>
            Ihr Vodafone-Team
        </p>
    </div>
</div>
End;

$contentCallDsl = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    <span><b>{{var greetings}},</b></span><br>
    <span>vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige im Überblick:</span>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Sie haben noch Fragen zur Bestellung? Sie erreichen uns täglich rund um die Uhr: <b>vodafone.de/kontakt</b></p>
    <p>Sie haben noch Fragen zu unseren Services und Geräten? Besuchen Sie unsere Hilfe-Seiten: <b>vodafone.de/hilfe</b></p>
</div>
<div>
    {{var wishDate}}
</div>
<div class="goes-by">
    {{var nextSteps}}
</div>
<div class="footer">
    {{var endingNote}}
</div>
<p>Vielen Dank für Ihr Vertrauen!</p>
<p>Freundliche Grüße,<br>Ihr Vodafone-Team</p>
End;

$contentCallCable = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    <span><b>{{var greetings}},</b></span><br>
    <span>vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige im Überblick:</span>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Sie haben noch Fragen zur Bestellung? Sie erreichen uns täglich rund um die Uhr: <b>vodafone.de/kontakt</b></p>
    <p>Sie haben noch Fragen zu unseren Services und Geräten? Besuchen Sie unsere Hilfe-Seiten: <b>vodafone.de/hilfe</b></p>
</div>
<div>
    {{var wishDate}}
</div>
<div class="goes-by">
    {{var nextSteps}}
</div>
<div class="footer">
    {{var endingNote}}
</div>
<p>Vielen Dank für Ihr Vertrauen!</p>
<p>Freundliche Grüße,<br>Ihr Vodafone-Team</p>
End;

$contentCallCableMigration = $contentCallCable;

$contentCallMobile = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    <span><b>{{var greetings}},</b></span><br>
    <span>vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige im Überblick:</span>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Sie haben noch Fragen zur Bestellung? Sie erreichen uns täglich rund um die Uhr: <b>vodafone.de/kontakt</b></p>
    <p>Sie haben noch Fragen zu unseren Services und Geräten? Besuchen Sie unsere Hilfe-Seiten: <b>vodafone.de/hilfe</b></p>
</div>
<div class="goes-by">
    {{var nextSteps}}
</div>
<div class="footer">
    {{var endingNote}}
</div>
<p>Vielen Dank für Ihr Vertrauen!</p>
<p>Freundliche Grüße,<br>Ihr Vodafone-Team</p>
End;

$contentCallGigakombiCable = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    <span><b>{{var greetings}},</b></span><br>
    <span>vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige zu Ihrem Kabel-Vertrag mit GigaKombi im Überblick:</span>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Sie haben noch Fragen zur Bestellung? Sie erreichen uns täglich rund um die Uhr: <b>vodafone.de/kontakt</b></p>
    <p>Sie haben noch Fragen zu unseren Services und Geräten? Besuchen Sie unsere Hilfe-Seiten: <b>vodafone.de/hilfe</b></p>
</div>
<div>
    {{var wishDate}}
</div>
<div class="goes-by">
    {{var nextSteps}}
</div>
<div class="footer">
    {{var endingNote}}
</div>
<p>Vielen Dank für Ihr Vertrauen!</p>
<p>Freundliche Grüße,<br>Ihr Vodafone-Team</p>
End;

$contentCallGigakombiCableMigration = $contentCallGigakombiCable;

$contentCallGigakombiMobile = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    <span><b>{{var greetings}},</b></span><br>
    <span>vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige zu Ihrem Mobilfunkvertrag mit GigaKombi im Überblick:</span>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Sie haben noch Fragen zur Bestellung? Sie erreichen uns täglich rund um die Uhr: <b>vodafone.de/kontakt</b></p>
    <p>Sie haben noch Fragen zu unseren Services und Geräten? Besuchen Sie unsere Hilfe-Seiten: <b>vodafone.de/hilfe</b></p>
</div>
<div class="goes-by">
    {{var nextSteps}}
</div>
<div class="footer">
    {{var endingNote}}
</div>
<p>Vielen Dank für Ihr Vertrauen!</p>
<p>Freundliche Grüße,<br>Ihr Vodafone-Team</p>
End;

$store = Mage::getModel('core/store')
    ->getCollection()
    ->addFieldToFilter('code', ['eq'=>'admin'])
    ->getFirstItem();

$offerSummaryContent = [
    [   "title" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_DSL_TITLE,
        "identifier" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_DSL_KEY,
        "content" => $contentOfferGigaKombiDSL
    ],
    [
        "title" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_KABEL_TITLE,
        "identifier" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_KABEL_KEY,
        "content" => $contentOfferGigaKombiKabel
    ],
    [
        "title" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_MOBILE_TITLE,
        "identifier" => Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_MOBILE_KEY,
        "content" => $contentOfferGigaKombiMobile
    ],
    [
        "title" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_DSL_TITLE,
        "identifier" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_DSL_KEY,
        "content" => $contentOfferSummaryDSL
    ],
    [
        "title" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_KABEL_TITLE,
        "identifier" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_KABEL_KEY,
        "content" => $contentOfferSummaryKabel
    ],
    [
        "title" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_MOBILE_TITLE,
        "identifier" => Dyna_Checkout_Block_Offer::OFFER_SUMMARY_MOBILE_KEY,
        "content" => $contentOfferSummaryMobile
    ]
];

$store = Mage::getModel('core/store')
    ->getCollection()
    ->addFieldToFilter('code', ['eq'=>'admin'])
    ->getFirstItem();

/** @var Mage_Cms_Model_Block $blockModel */
$blockModel = Mage::getModel('cms/block');

foreach($offerSummaryContent as $content) {
    $blockId = $blockModel
        ->load('offer_block_'.$content['identifier'])
        ->getId();

    if (!$blockId) {
        $blockModel->setTitle($content['title'])
            ->setIdentifier('offer_block_'.$content['identifier'])
            ->setStores(array($store->getId()))
            ->setIsActive(1)
            ->setContent($offerAndCallSummaryCssContent . $content['content'])
            ->save();
    } else {
        $loadedBlock = $blockModel->load('offer_block_' . $content['identifier']);
        $loadedBlock->setContent($offerAndCallSummaryCssContent . $content['content'])
            ->save();
    }
}

$callSummaryContent = [
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_DSL,
        'content' => $offerAndCallSummaryCssContent . $contentCallDsl,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE,
        'content' => $offerAndCallSummaryCssContent . $contentCallCable,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_MIGRATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE_MIGRATION,
        'content' => $offerAndCallSummaryCssContent . $contentCallCableMigration,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_MOBILE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_MOBILE,
        'content' => $offerAndCallSummaryCssContent . $contentCallMobile,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_CABLE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_CABLE,
        'content' => $offerAndCallSummaryCssContent . $contentCallGigakombiCable,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_CABLE_MIGRATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_CABLE_MIGRATION,
        'content' => $offerAndCallSummaryCssContent . $contentCallGigakombiCableMigration,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_MOBILE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_MOBILE,
        'content' => $offerAndCallSummaryCssContent . $contentCallGigakombiMobile,
    ]
];

foreach($callSummaryContent as $content) {
    $blockId = $blockModel
        ->load($content['identifier'])
        ->getId();

    if (!$blockId) {
        $blockModel->setTitle($content['title'])
            ->setIdentifier($content['identifier'])
            ->setStores(array($store->getId()))
            ->setIsActive(1)
            ->setContent($content['content'])
            ->save();
    } else {
        $loadedBlock = $blockModel->load($content['identifier']);
        $loadedBlock->setContent($content['content'])
            ->save();
    }
}