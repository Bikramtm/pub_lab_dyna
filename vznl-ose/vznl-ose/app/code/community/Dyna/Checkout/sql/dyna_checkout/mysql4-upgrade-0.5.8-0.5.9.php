<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();

$alterSql =
<<<SQL
ALTER TABLE `sales_flat_quote`
  CHANGE `created_at` `created_at` TIMESTAMP DEFAULT NOW()  NOT NULL   COMMENT 'Created At',
  ADD  INDEX `PRIMARY_WEBSITEID_ACTIVE` (`entity_id`, `store_id`, `is_active`);
SQL;

$this->getConnection()->query($alterSql);

$this->endSetup();