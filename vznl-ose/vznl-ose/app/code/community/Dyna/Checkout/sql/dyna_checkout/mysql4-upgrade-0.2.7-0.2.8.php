<?php

/**
 * Add new table catalog_package_notes
 *
 */
$installer = $this;

$installer->startSetup();

$tableName = $installer->getTable('catalog_package_notes');

if (! $installer->getConnection()->isTableExists($tableName)) {
    // Defining table structure via object
    $table = $installer->getConnection()
        ->newTable($tableName)
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
            "unsigned" => true,
            "primary" => true,
            "auto_increment" => true,
            'nullable' => false
        ], "Primary key for fields table")
        ->addColumn('package_id',Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
            "unsigned" => true,
            "nullable" => false,
        ], "Package id")
        ->addColumn('quote_id',Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
            "unsigned" => true,
            "nullable" => false,
        ], "Quote id")
        ->addColumn('created_date',Varien_Db_Ddl_Table::TYPE_DATETIME, null, [
            "nullable" => false,
        ], "Created date")
        ->addColumn('created_agent_id',Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
            "unsigned" => true,
            "nullable" => false,
        ], "Created agent id")
        ->addColumn('last_update_date',Varien_Db_Ddl_Table::TYPE_DATETIME, null, [
            "nullable" => false,
        ], "Created date")
        ->addColumn('last_update_agent_id',Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
            "nullable" => false,
        ], "Last update agent id")
        ->addColumn('note',Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, [
            "nullable" => false,
        ], "Note")
    ;

    // Executing table creation object
    $installer->getConnection()->createTable($table);
}


$installer->endSetup();

