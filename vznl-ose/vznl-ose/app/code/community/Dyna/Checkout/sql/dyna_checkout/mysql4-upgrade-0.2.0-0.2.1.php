<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn('sap_orders', 'is_valid', 'int(1)');

$installer->endSetup();
