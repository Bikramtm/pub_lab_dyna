<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$tableName = $this->getTable('catalog_package_notes');

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();

if ($connection->tableColumnExists($tableName, 'created_agent_id')) {
    $connection->changeColumn($tableName, 'created_agent_id', 'created_agent', 'varchar(255) null');
}

if ($connection->tableColumnExists($tableName, 'last_update_agent_id')) {
    $connection->changeColumn($tableName, 'last_update_agent_id', 'last_update_agent', 'varchar(255) null');
}

$installer->endSetup();