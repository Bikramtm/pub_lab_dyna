<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$this->getConnection()->addColumn($this->getTable("sales_flat_quote"), "blue_sales_id", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'length' => 50, 'comment' => ' ', 'after' => 'sales_id'
));

$this->getConnection()->addColumn($this->getTable("sales_flat_quote"), "yellow_sales_id", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'length' => 50, 'comment' => ' ', 'after' => 'blue_sales_id'
));

$installer->endSetup();
