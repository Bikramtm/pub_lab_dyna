<?php
/**
 * Installer that adds product visibility on both quote item and order item resources
 * Skipping 0.3.0 version as wave 1 databases have this version in core_resource without having the proper installer
 * Copyright (c) 2017. Dynacommerce B.V.
 */


/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'product_visibility', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Set product visibility on quote item',
        'default' => NULL,
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'product_visibility', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Set product visibility on order item',
        'default' => NULL,
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'product_visibility', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Set product visibility on address item',
        'default' => NULL,
    ));

$installer->endSetup();
