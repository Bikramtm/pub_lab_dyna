<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$installer->setConfigData('checkout/cart/youbiage_birthday_check_sku', 'KYOUABIAGE');
$installer->endSetup();