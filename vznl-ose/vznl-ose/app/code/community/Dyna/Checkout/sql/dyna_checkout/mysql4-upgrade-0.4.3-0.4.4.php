<?php

$contentCss = <<< End
    <style>
        .hidden {
            display: none;
        }
    </style>
End;

$contentCableNextSteps = <<< End
<table class="{{var cableStackVisibility}}">
    <tr>
        <td class="no-bg brder-left brder-top brder-bottom image-cell">
            <img class="delivery-img" src="{{var cableIconPath}}" />
        </td>
        <td class="black no-bg brder-right brder-top brder-bottom">
            <h4 class="cableHeadlineVisibility}}">Kabel</h4>
            {{var cableNextStepsContent}}
        </td>
    </tr>
</table>
End;

$contentDslNextSteps = <<< End
<table class="{{var dslStackVisibility}}">
    <tr>
        <td class="no-bg brder-left brder-top brder-bottom image-cell">
            <img class="delivery-img" src="{{var dslIconPath}}" />
        </td>
        <td class="black no-bg brder-right brder-top brder-bottom">
            <h4 class="{{var dslHeadlineVisibility}}">DSL/LTE</h4>
            {{var dslNextStepsContent}}
        </td>
    </tr>
</table>
End;

$contentPostpaidNextSteps = <<< End
<table class="{{var postpaidStackVisibility}}">
    <tr>
        <td class="no-bg brder-left brder-top brder-bottom image-cell">
            <img class="delivery-img" src="{{var postpaidIconPath}}" />
        </td>
        <td class="black no-bg brder-right brder-top brder-bottom">
            <h4 class="{{var postpaidHeadlineVisibility}}">Mobilfunk</h4>
            {{var postpaidNextStepsContent}}
        </td>
    </tr>
</table>
End;

$contentPrepaidNextSteps = <<< End
<table class="{{var prepaidStackVisibility}}">
    <tr>
        <td class="no-bg brder-left brder-top brder-bottom image-cell">
            <img class="delivery-img" src="{{var prepaidIconPath}}" />
        </td>
        <td class="black no-bg brder-right brder-top brder-bottom">
            <h4 class="{{var prepaidHeadlineVisibility}}">Prepaid</h4>
            {{var prepaidNextSteps}}
        </td>
    </tr>
</table>
End;

$contentGigaKombiNextSteps = <<<End
<table class="{{var gigakombiVisibility}}">
    <tr>
        <td class="no-bg brder-left brder-top brder-bottom image-cell">
            <img class="delivery-img" src="{{var gigakombiIconPath}}" />
        </td>
        <td class="black no-bg brder-right brder-top brder-bottom">
            <h4 class="{{var gigakombiHeadlineVisibility}}">GigaKombi</h4>
            {{var gigakombiNextStepsContent}}
        </td>
    </tr>
</table>
End;

$contentEndNotes = <<<End
<div class="footer">
    <p>
        <strong>Bitte beachten Sie: </strong><br>
         {{var endNotesContent}}
    </p>
</div>
End;

// ############################
// Static block details array
// ############################

$staticBlockDetailsArray = [
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS,
        'content' => $contentCss.$contentPostpaidNextSteps,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_PREPAID_NEXT_STEPS,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_PREPAID_NEXT_STEPS,
        'content' => $contentCss.$contentPrepaidNextSteps,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS,
        'content' => $contentCss.$contentCableNextSteps,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_DSL_NEXT_STEPS,
        'content' => $contentCss.$contentDslNextSteps,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_NEXT_STEPS,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_NEXT_STEPS,
        'content' => $contentCss.$contentGigaKombiNextSteps,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_END_NOTES,
        'content' => $contentCss.$contentEndNotes,
    ]
];


// ############################
// Logic starts here
// ############################

$store = Mage::getModel('core/store')
    ->getCollection()
    ->addFieldToFilter('code', ['eq'=>'admin'])
    ->getFirstItem();

foreach ($staticBlockDetailsArray as $key => $details) {
    /** @var Mage_Cms_Model_Block $blockModel */
    $blockModel = Mage::getModel('cms/block');
    $blockId = $blockModel
        ->load($details['identifier'])
        ->getId();

    if (!$blockId) {
        $blockModel->setTitle($details['title'])
            ->setIdentifier($details['identifier'])
            ->setStores(array($store->getId()))
            ->setIsActive(1)
            ->setContent($details['content'])
            ->save();
    }
}