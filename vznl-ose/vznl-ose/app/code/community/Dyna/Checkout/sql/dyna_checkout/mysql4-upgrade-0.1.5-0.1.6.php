<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Dyna
 * @package     Dyna_Checkout
 */

$installer = $this;

$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable("dyna_checkout/fields"), 'package_number', Varien_Db_Ddl_Table::TYPE_TINYINT, null,
    array(
        'nullable' => false
    ), 'Package number/sequence');

$installer->endSetup();
