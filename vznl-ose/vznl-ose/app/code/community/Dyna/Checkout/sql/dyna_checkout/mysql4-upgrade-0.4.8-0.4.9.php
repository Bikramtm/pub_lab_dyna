<?php

$cssContent = <<< End
    <style>
        @page {
            margin-top: 50px;
            margin-bottom: 50px;
        }

        tbody:before, tbody:after { 
            display: none; 
        }

        .title, .customer-info, .end-info, .top-logo {
            color: #000000;
            font-size: 14px;
            line-height: 22px;
            font-family: "Arial Rounded MT", Arial, Helvetica, sans-serif;
            margin: 30px 50px 20px 50px;
        }
        
        .end-info {
            /*clear: both;*/
        }

        h1, h2 {
            color: #E60000;
            margin: 0;
        }

        h3, h4 {
            margin: 0;
        }

        h1 {
            color: #E60000;
            font-size: 28px;
            line-height: 32px;
            font-family: "Arial Rounded MT", Arial, Helvetica, sans-serif;
            font-weight: bold;
        }

        h2 {
            font-size: 24px;
            line-height: 28px;
            font-weight: normal;
        }

        .greetings {
            padding: 30px 0 30px 0;
        }

        .call-back dl {
            margin: 1em 0;
        }

        .call-back dt {
            float: left;
            width: 60px;
        }

        .ending {
            line-height: 2;
        }

        .title h1 {
            padding: 50px 0 45px 85px;
        }

        table {
            width: 90%;
            border-collapse: collapse;
            border-style: solid;
            border-width: thin;
            margin-top: 10px;
            margin-bottom: 40px;
            margin-left: 30px;
            padding: 0;
        }

        th, td {
            padding: 15px 10px;
            text-align: left;
        }

        td {
            padding: 8px 10px;
        }

        .offer-table {
            margin-top: 2em;
            display: table;
        }

        img.top-logo {
            position: absolute;
            top: -30px;
            left: 0px;
        }

        h3 {
            color: #333333;
            font-size: 20px;
            line-height: 24px;
        }

        h4 {
            color: #262626;
            font-size: 16px;
            line-height: 22px;
        }

        h5 {
            color: #262626;
            font-size: 14px;
            line-height: 24px;
            margin: 0;
        }
        
        .hidden {
            display: none;
            visibility: hidden;
        }
        
        div.separator {
            clear: both;
            height: 0px;
            overflow: hidden;
        }
    </style>
End;

$contentSummary = <<< End
<div class="title">
    <img class="top-logo" src="{{var imagePath}}" />
    <h1>Unser Angebot für Sie</h1>
</div>
<div class="customer-info">
    <br>
    <div class="greetings">
        <strong>{{var greetings}}</strong>,<br>
        danke für Ihr Interesse. Hier ist unser Angebot für Sie – so, wie wir es besprochen haben.<br> 
        <p>Nennen Sie uns Ihre Angebotsnummer {{var shoppingCartId}} bitte bei Rückfragen.</p>
    </div>
    <div class="call-back">
        {{if callbackDatetime}}
        <strong>
            Wir rufen Sie zurück - wie vereinbart: 
            <dl>
                <dt>Datum:</dt>
                <dd>{{var callbackDate}}</dd>
                <dt>Uhrzeit:</dt>
                <dd>{{var callbackTime}}</dd>
            </dl>
        </strong>
        <p>Erreichen wir Sie nicht, probieren wir es natürlich nochmal. Oder Sie rufen uns nochmal an. Sie erreichen uns</p>
        <p>
            jeden Tag in der Zeit von 8:00 bis 20:00 Uhr <br>
            <strong>unter {{var hotlineNumber}}</strong>
        </p>
        {{else}}
        <p>Rufen Sie uns dazu einfach an,</p>
        <p>
            jeden Tag in der Zeit von 8:00 bis 20:00 Uhr<br>
            <strong>unter {{var hotlineNumber}}</strong>
        </p>
        {{/if}}
    </div>
</div>
<div class="offer-table">
    {{var offer_table}}
</div>
<div class="end-info">
    <div>
        {{var lostProducts}}
    </div>
    <div>
        <p class="{{var technicalAvailabilityVisibility}}">Die technische Verfügbarkeit können wir erst bei der Bereitstellung sicherstellen.</p>
        <p>Das dargestellte Angebot ist unverbindlich und kann nicht garantiert werden.</p>
    </div>
    <div class="ending">
        <h2>Passt alles für Sie?</h2>
        <div>
            <p>Dann freuen wir uns, von Ihnen zu hören.</p>
            <p>Vielen Dank für Ihr Vertrauen!</p>
        </div>
        <div>
            <p>
                Freundliche Grüße<br>
                Ihr Vodafone-Team
            </p>
        </div>
    </div>
</div>
End;

$contentArray = [
    [
        "title" => Dyna_Checkout_Block_Info::INFO_SUMMARY_TITLE,
        "key" => Dyna_Checkout_Block_Info::INFO_SUMMARY_KEY,
        "content" => $contentSummary
    ]
];

$store = Mage::getModel('core/store')
    ->getCollection()
    ->addFieldToFilter('code', ['eq'=>'admin'])
    ->getFirstItem();

/** @var Mage_Cms_Model_Block $blockModel */
foreach($contentArray as $content) {
    $blockModel = Mage::getModel('cms/block');
    $blockId = $blockModel
        ->load('offer_block_'.$content['key'])
        ->getId();

    if (!$blockId) {
        $blockModel->setTitle($content['title'])
            ->setIdentifier('offer_block_'.$content['key'])
            ->setStores(array($store->getId()))
            ->setIsActive(1)
            ->setContent($cssContent.$content['content'])
            ->save();
    } else {
        $loadedBlock = $blockModel->load('offer_block_' . $content['key']);
        $loadedBlock->setContent($cssContent . $content['content'])
            ->save();
    }
}