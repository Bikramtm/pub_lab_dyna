<?php

//  ########################
//  So geht es weiter: Kabel
//  ########################

$cableActivationContent = <<<End
<p>Sie bekommen rechtzeitig vor Ihrem Anschaltetermin eine Auftragsbestätigung. Darin finden Sie Ihre Vertragsnummer. Nachdem wir Ihre Bestellung 
bearbeitet haben, bekommen Sie alle  Unterlagen von uns per Post. Das dauert in der Regel 1 bis 2 Werktage.</p>

<p>Ihre Geräte bekommen Sie rechtzeitig zur Anschaltung. Haben Sie sich für SurfSofort entschieden? Dann ist Ihr mobiler WLAN-Hotspot schon bald auf 
dem Weg zu Ihnen.  Stecken Sie einfach die SIM-Karte ein, registrieren Sie sich und surfen Sie los.</p>
End;

$cableProviderChangeContent = <<<End
<p>Infos zu Ihrem Anbieterwechsel bekommen Sie in Kürze separat.</p>
End;

$cableCancellationContent = <<<End
<p>Mit Ihrem Umzug bekommen Sie für die Kündigung Ihres Kabel-Anschlusses eine separate Bestätigung von uns.</p>
End;

$cableChangeContent = <<<End
<p>Ihre Auftragsbestätigung mit allen Details bekommen Sie in Kürze separat von uns.</p>
End;

$cableSelfInstallContent = <<<End
<p>Geräte selbst anschließen? Geht natürlich auch: Schauen Sie einfach in die Anleitung – und folgen Sie der Schritt-für-Schritt-Beschreibung. Viel Spaß im Internet!</p>
End;

//  ########################
//  So geht es weiter: DSL/LTE
//  ########################

$dslCancellationContent = <<<End
<p>Für die Kündigung Ihres DSL- oder LTE-Anschlusses bekommen Sie eine separate Bestätigung von uns.</p>
End;

$dslActivationContent = <<<End
<p>Sie bekommen bald eine Auftragsbestätigung per E-Mail. Darin finden Sie Ihre Auftragsnummer und eventuelle Infos zu Ihrem Anbieterwechsel.</p>
<p>Ihre Geräte bekommen Sie rechtzeitig zur Anschaltung. Haben Sie sich für SurfSofort entschieden? Dann ist Ihr mobiler WLAN-Hotspot schon 
bald auf dem Weg zu Ihnen.  Stecken Sie einfach die SIM-Karte ein, registrieren Sie sich und surfen Sie los.</p>
End;

$dslChangeContent = <<<End
<p>Ihre Auftragsbestätigung mit allen Details bekommen Sie in Kürze separat von uns.</p>
End;

//  ###########################
//  So geht es weiter: Postpaid
//  ###########################

$postpaidActivationContent = <<<End
<p>Für Ihren neuen Mobilfunkvertrag bekommen Sie bald Ihre SIM-Karte und Ihr Smartphone oder Tablet, wenn Sie ein Gerät bestellt haben.</p>
<p>Identifizieren Sie sich noch mit Ihrem Personalausweis. Erst dann können Sie Ihren Tarif nutzen. Ihre Bestellung liefert Ihnen unser 
Versandpartner. Seien Sie bitte vor Ort und halten Sie Ihren Ausweis bereit. Mit ihm identifizieren Sie sich dann, um den Vertrag zu unterschreiben. Wenn Sie 
sich mit Ihrem Reisepass ausweisen möchten, legen Sie bitte zusätzlich eine Meldebescheinigung vor.</p>
<p>Möchten Sie Ihre Rufnummer von Ihrem aktuellen Anbieter mitnehmen? Alle Infos dazu finden Sie hier: [https://www.vodafone.de/privat/hilfe-support/rufnummernmitnahme.html]</p>
End;

$postpaidProlongationContent = <<<End
<p>Zu Ihrer Vertragsverlängerung bekommen Sie bald Ihr Smartphone oder Tablet, wenn Sie ein Gerät bestellt haben.</p>
End;

$postpaidChangeContent = <<<End
<p>Ihre Auftragsbestätigung mit allen Details bekommen Sie in Kürze separat von uns.</p>
End;

$postpaidDebitToCreditContent = <<<End
<p>Für Ihren neuen Mobilfunkvertrag bekommen Sie bald Ihre SIM-Karte und Ihr Smartphone oder Tablet, wenn Sie ein Gerät bestellt haben.</p>
<p>Identifizieren Sie sich noch mit Ihrem Personalausweis. Erst dann können Sie Ihren Tarif nutzen. Ihre Bestellung liefert Ihnen 
unser Versandpartner. Seien Sie bitte vor Ort und halten Sie Ihren Ausweis bereit. Mit ihm identifizieren Sie sich dann, um den Vertrag 
zu unterschreiben. Wenn Sie sich mit Ihrem Reisepass ausweisen möchten, legen Sie bitte zusätzlich eine Meldebescheinigung vor.</p>
End;

$postpaidShipToStoreContent = <<<End
<p>Wenn Sie Ihr neues Gerät in Ihrem Vodafone-Shop abholen können, schicken wir Ihnen eine Info. Haben Sie noch keinen 
Vertrag bei uns? Dann bekommen Sie dort auch Ihre SIM-Karte. Bringen Sie dazu bitte Ihren Personalausweis mit. Wir  
brauchen ihn für Ihre Registrierung. Sie sind schon unser Kunde, brauchen wir das nicht mehr zu machen.</p>
End;

//  ###########################
//  So geht es weiter: Prepaid
//  ###########################

$prepaidActivationContent = <<<End
<p>Sie bekommen bald Ihre SIM-Karte und Ihr Smartphone oder Tablet, wenn Sie ein Gerät bestellt haben. Identifizieren Sie sich noch mit Ihrem Personalausweis. Erst 
dann können Sie Ihren Tarif nutzen. Alle Infos zum Video-Ident-Verfahren gibt's in dem Brief, den Sie mit Ihrer SIM-Karte bekommen.</p>
End;

$prepaidShipToStoreContent = <<<End
<p>Wenn Sie Ihr neues Gerät in Ihrem Vodafone-Shop abholen können, schicken wir Ihnen eine Info. Haben Sie noch keinen Vertrag bei uns? Dann bekommen 
Sie dort auch Ihre SIM-Karte. Bringen Sie dazu bitte Ihren Personalausweis mit. Wir  brauchen ihn für Ihre Registrierung. Sie sind schon unser Kunde, 
brauchen wir das nicht mehr zu machen.</p>
End;

//  ###########################
//  So geht es weiter: GigaKombi
//  ###########################

$gigakombiActivationContent = <<<End
<p>Sie bekommen Ihre GigaKombi-Vorteile, wenn Ihre Verträge aktiv sind.</p>
End;


//  ###########################
//  Bitte beachten Sie
//  ###########################

$endNoteNotBindingContractContent = <<<End
<p>Diese Zusammenfassung dient ausschließlich der Bestätigung des Einganges Ihrer Bestellung und stellt noch keine Annahme Ihres Angebotes auf Abschluss eines Vertrages dar.</p>
End;

$endNotePostpaidSpecificContent = <<<End
<p>Ihr Vertrag über Mobilfunkdienstleistungen kommt mit Aktivierung/Freischaltung der Mobilfunkdienstleistung bzw. soweit Sie nur Hardware oder Zubehör bestellt haben, 
mit dem Versand des Artikels an Sie zustande.</p>
End;

$endNoteDedicatedContractContent = <<<End
<p>Ihre Vertragsdokumente erhalten Sie ggf. zu unterschiedlichen Zeitpunkten.</p>
End;

// ############################
// Static block details array
// ############################

$staticBlockDetailsArray = [
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_ACTIVATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS_ACTIVATION,
        'content' => $postpaidActivationContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_CHANGE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS_CHANGE,
        'content' => $postpaidChangeContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_DEBIT,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS_DEBIT,
        'content' => $postpaidDebitToCreditContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_PROLONGATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS_PROLONGATION,
        'content' => $postpaidProlongationContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_SHIP,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS_SHIP,
        'content' => $postpaidShipToStoreContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_PREPAID_NEXT_STEPS_ACTIVATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_PREPAID_NEXT_STEPS_ACTIVATION,
        'content' => $prepaidActivationContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_PREPAID_NEXT_STEPS_SHIP,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_PREPAID_NEXT_STEPS_SHIP,
        'content' => $prepaidShipToStoreContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_ACTIVATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS_ACTIVATION,
        'content' => $cableActivationContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_CANCELLATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS_CANCELLATION,
        'content' => $cableCancellationContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_CHANGE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS_CHANGE,
        'content' => $cableChangeContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_PROVIDER_CHANGE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS_PROVIDER_CHANGE,
        'content' => $cableProviderChangeContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_SELF,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS_SELF,
        'content' => $cableSelfInstallContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS_ACTIVATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_DSL_NEXT_STEPS_ACTIVATION,
        'content' => $dslActivationContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS_CANCELLATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_DSL_NEXT_STEPS_CANCELLATION,
        'content' => $dslCancellationContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS_CHANGE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_DSL_NEXT_STEPS_CHANGE,
        'content' => $dslChangeContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_NEXT_STEPS_ACTIVATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_NEXT_STEPS_ACTIVATION,
        'content' => $gigakombiActivationContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES_DEDICATED,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_END_NOTES_DEDICATED,
        'content' => $endNoteDedicatedContractContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES_NOT_BINDING,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_END_NOTES_NOT_BINDING,
        'content' => $endNoteNotBindingContractContent,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES_POSTPAID,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_END_NOTES_POSTPAID,
        'content' => $endNotePostpaidSpecificContent,
    ]
];


// ############################
// Logic starts here
// ############################

$store = Mage::getModel('core/store')
    ->getCollection()
    ->addFieldToFilter('code', ['eq'=>'admin'])
    ->getFirstItem();

foreach ($staticBlockDetailsArray as $key => $details) {
    /** @var Mage_Cms_Model_Block $blockModel */
    $blockModel = Mage::getModel('cms/block');
    $blockId = $blockModel
        ->load($details['identifier'])
        ->getId();

    if (!$blockId) {
        $blockModel->setTitle($details['title'])
            ->setIdentifier($details['identifier'])
            ->setStores(array($store->getId()))
            ->setIsActive(1)
            ->setContent($details['content'])
            ->save();
    }
}