<?php
/**
 * Installer that creates the checkout fields resource table
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;

$installer->startSetup();

$checkoutFieldsTable = new Varien_Db_Ddl_Table();
$checkoutFieldsTable->setName($this->getTable("dyna_checkout/fields"));

$checkoutFieldsTable
    ->addColumn('field_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
        "unsigned" => true,
        "primary" => true,
        'nullable' => false,
        "auto_increment" => true,
    ], "Primary key for fields table")
    ->addColumn('quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, [
        "unsigned" => true,
    ], "The quote to which the checkout fields are mapped")
    ->addColumn('checkout_step', Varien_Db_Ddl_Table::TYPE_TEXT, 45, [
    ], "The step to which this fields is mapped")
    ->addColumn("field_name", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], "Field name that is shown in frontend and sent back through request")
    ->addColumn("field_value", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
    ], "The inputted value for current field")
    ->addColumn("date", Varien_Db_Ddl_Table::TYPE_DATETIME, null, [
    ], "The date and time when this entry was saved")
    ->addForeignKey(
        $this->getFkName($checkoutFieldsTable->getName(), 'quote_id', 'sales/quote', 'entity_id'),
        'quote_id', $this->getTable('sales/quote'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
;

if(!$this->getConnection()->isTableExists($checkoutFieldsTable->getName())){
    $this->getConnection()->createTable($checkoutFieldsTable);
}


$installer->endSetup();
