<?php

/**
 * Layout helper for checkout steps
 * Class Dyna_Checkout_Helper_Fields
 */

class Dyna_Checkout_Helper_Layout extends Mage_Core_Helper_Data
{

    protected $isPrivateMobilePostpaid = FALSE;
    protected $isPrivateMobilePrepaid = FALSE;
    protected $isPrivateCable = FALSE;
    protected $isPrivateFixed = FALSE;
    protected $isCompanyMobilePostpaid = FALSE;
    protected $isCompanyCable = FALSE;
    protected $isCompanyFixed = FALSE;
    protected $isPrivate = FALSE;
    protected $isCompany = FALSE;
    protected $packages = NULL;
    protected $packageSurvey = array();
    protected $surveyDone = FALSE;
    protected $cartHelper = NULL;

    /**
     * Setup for the helper
     *
     * @return void
     */
    public function setupFormLayout()
    {
        if( is_null( $this->cartHelper ) ) {
            $this->cartHelper = Mage::helper('dyna_checkout/cart');
        }
        $this->surveyPackagesTypes();
        foreach($this->getPackages() as $package) {
            $this->surveyPackageType( $package );
        }
    }

    /**
     * Retrieve the quote packages
     *
     * @return array
     */
    public function getPackages($includeInstalledBase = false)
    {
        if ($this->packages === null) {
            /** @var Dyna_Checkout_Model_Sales_Quote $quoteModel */
            $quoteModel = Mage::getSingleton('checkout/cart')->getQuote();

            /** @var Dyna_Package_Model_Package $packageModel */
            $packageModel = Mage::getModel('package/package');
            $tempPackages = $packageModel->getPackages(null, $quoteModel->getId());

            $this->packages = [];
            foreach ($tempPackages as $package) {
                $package->setData('items', $package->getItems());
                if (!$package->getInstalledBaseProducts() || $includeInstalledBase) {
                    $this->packages[] = $package;
                }
            }
        }

        return $this->packages;
    }


    /**
     * Surveys current packages in cart
     *
     * @return void
     */
    protected function surveyPackagesTypes()
    {
        if( $this->surveyDone ) return;
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        // Defaults
        $this->alwaysShow = TRUE;
        $cartPackages = $this->getPackages();
        // Global
        $this->isMobile             = $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_MOBILE ) ||
                                        $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_PREPAID ) ||
                                        $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_REDPLUS );
        $this->isMobilePostpaid     = $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_MOBILE );
        $this->isMobilePrepaid      = $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_PREPAID );
        $this->isCable              = $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_CABLE_TV ) ||
                                        $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE ) ||
                                        $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT );
        $this->isFixed              = $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_FIXED_DSL ) ||
                                        $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_LTE );
        $this->isDSL                = $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_FIXED_DSL );
        $this->isLTE                = $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_LTE );
        $this->isRedplus            = $this->cartHelper->hasPackageOfType( $cartPackages, Dyna_Catalog_Model_Type::TYPE_REDPLUS );
        $this->isMobileCable        = $this->isMobile && $this->isCable;

        // Customer type
        if( !$customer->getIsSoho() ) {
            $this->isPrivate = TRUE;
            $this->isPrivateMobilePostpaid = $this->isMobilePostpaid;
            $this->isPrivateMobilePrepaid = $this->isMobilePrepaid;
            $this->isPrivateCable = $this->isCable;
            $this->isPrivateFixed = $this->isFixed;
        } else {
            $this->isCompany = TRUE;
            $this->isCompanyMobilePostpaid = $this->isMobilePostpaid;
            $this->isCompanyCable = $this->isCable;
            $this->isCompanyFixed = $this->isFixed;
            $this->isCompanyDSL   = $this->isDSL;
        }
        $this->surveyDone = TRUE;
    }

    /**
     * Surveys specific package
     *
     * @return void
     */
    protected function surveyPackageType( $package ) {
        $packageId = $package->getId();

        if( isset( $this->packageSurvey[$packageId] ))  return;

        $customer = Mage::getSingleton('customer/session')->getCustomer();

        $this->packageSurvey[$packageId] = new Varien_Object();
        $this->packageSurvey[$packageId]->alawaysShow = TRUE;
        $this->packageSurvey[$packageId]->isMobile = $package->isMobile();
        $this->packageSurvey[$packageId]->isMobilePostpaid = $package->isMobilePostpaid();
        $this->packageSurvey[$packageId]->isMobilePrepaid = $package->isPrepaid();
        $this->packageSurvey[$packageId]->isCable = $package->isCable();
        $this->packageSurvey[$packageId]->isFixed = $package->isFixed();
        $this->packageSurvey[$packageId]->isDSL = $package->isDSL();
        $this->packageSurvey[$packageId]->isLTE = $package->isLTE();
        $this->packageSurvey[$packageId]->isRedplus = $package->isPartOfRedPlus();
        if( !$customer->getIsSoho() ) {
            $this->packageSurvey[$packageId]->isPrivate = TRUE;
            $this->packageSurvey[$packageId]->isPrivateMobilePostpaid = $this->packageSurvey[$packageId]->isMobilePostpaid;
            $this->packageSurvey[$packageId]->isPrivateMobilePrepaid = $this->packageSurvey[$packageId]->isMobilePrepaid;
            $this->packageSurvey[$packageId]->isPrivateCable = $this->packageSurvey[$packageId]->isCable;
            $this->packageSurvey[$packageId]->isPrivateFixed = $this->packageSurvey[$packageId]->isFixed;
        } else {
            $this->packageSurvey[$packageId]->isCompany = TRUE;
            $this->packageSurvey[$packageId]->isCompanyMobilePostpaid = $this->packageSurvey[$packageId]->isMobilePostpaid;
            $this->packageSurvey[$packageId]->isCompanyCable = $this->packageSurvey[$packageId]->isCable;
            $this->packageSurvey[$packageId]->isCompanyFixed = $this->packageSurvey[$packageId]->isFixed;
        }
    }

    protected $vodafoneRuling = array(    'section'     => array( 'service_address'                               => 'isCable OR isFixed',
                                                                'service_address_district'                      => 'isCable',
                                                                'identitication_id_and_nationality'             => 'isMobile',
                                                                'other_identity'                                => 'isMobile',
                                                                'company_commercial_entry'                      => 'isCompanyDSL',
                                                                'company_address'                               => 'isCompanyFixed',
                                                                'company_information_headline'                  => 'isCompanyCable OR isCompanyFixed',
                                                                'company_details'                               => 'isCompanyCable OR isCompanyFixed',
                                                                'company_details_registration'                  => 'isCompanyFixed',
                                                                'company_details_cable'                         => 'isCompanyCable',
                                                                'company_details_dsl'                           => 'isCompanyFixed',
                                                                'contact_phone_list'                            => 'isMobile OR isPrivate OR isCable OR isCompanyCable OR isFixed',
                                                                'contact_phone_list_add'                        => 'isCable OR isCompanyCable OR isFixed',
                                                                'contact_person'                                => 'isMobile',
                                                                'contact_phone_required_two'                    => 'isMobile OR isPrivate OR isCable OR isCompanyCable OR isFixed',
                                                                'order_status'                                  => '', // 'isPrivateMobilePostpaid OR isPrivateMobilePrepaid OR isCompanyMobilePostpaid',
                                                                'billing_phone'                                 => 'isMobile',
                                                                'billing_fixed'                                 => 'isFixed OR isCable',
                                                                'billing_po_box'                                => 'isMobile',
                                                                'billing_po_box_disabled'                       => 'isCable OR isFixed',
                                                                'billing_send_options'                          => 'isMobilePostpaid OR isFixed OR isCable',
                                                                'billing_send_email'                            => 'isMobilePostpaid OR isFixed OR isCable',
                                                                'billing_send_post'                             => 'isCompanyMobilePostpaid OR isFixed OR isCable',
                                                                'billing_nationality_other_address'             => '',
                                                                'billing_nationality_pobox'                     => 'isMobilePostpaid',
                                                                '_js_save_billing_data'                         => 'isMobile OR isFixed OR isCable',
                                                                '_js_commercial_register'                       => 'isMobile OR isCable',
                                                                'change_provider'                               => 'isFixed OR isCable OR isMobile',
                                                                'change_provider_notice_supplier'               => 'isCable OR isFixed',
                                                                'new_connection'                                => 'isFixed OR isCable OR isMobile',
                                                                'new_connection_for_lte'                        => 'isLTE',
                                                                'new_connection_for_dsl'                        => 'isDSL',
                                                                'new_connection_dates_setup'                    => 'isDSL OR isCable',
                                                                'delivery_to_shop'                              => 'isMobile',
                                                                'delivery_same_day'                             => 'isMobile',
                                                                'delivery_set_date'                             => 'isCable',
                                                                'payments_monthly'                              => 'isMobilePostpaid OR isFixed OR isCable OR isRedplus',
                                                                'payments_alternative_account_holder'           => 'isMobilePostpaid OR isFixed OR isCable OR isRedplus',
                                                                'payments_monthly_invoice'                      => 'isCompanyCable',
                                                                'payments_monthly_banktransfer'                 => 'isCompany',
                                                                'payments_onetime'                              => 'isMobile',
                                                                'payments_onetime_cod'                          => 'isMobilePrepaid',
                                                                'payments_onetime_invoice'                      => 'isMobile',
                                                                'payments_onetime_bank_transfer'                => 'isMobile',
                                                                'other_discounts_and_phonebook'                 => 'isMobile',
                                                                'other_marketing_data'                          => 'isCable'
                                                                ),

                                        'field'       => array( 'customer[prefix]'                              => 'alwaysShow',
                                                                'customer[dob]'                                 => 'isMobile OR isPrivateCable OR isPrivateFixed OR isCompanyFixed',
                                                                'customer[address][company_additional_name]'    => 'isCompanyFixed',
                                                                'customer[address][addition]'                   => 'isPrivate OR isCompanyMobilePostpaid OR isCompanyFixed',
                                                                'customer[account_password]'                    => 'isMobilePostpaid OR isFixed',
                                                                'customer_soho[address][addition]'              => 'isFixed OR isCable',
                                                                'provider[dsl][other_owner][gender]'            => 'isPrivateFixed',
                                                                'provider[lte][other_owner][gender]'            => 'isPrivateFixed',
                                                                'payment[sepa_mandate][type]'                   => 'isCable',
                                                                ),

                                        'value'       => array( 'customer[gender]->company'                     => 'isCompanyCable',
                                                                'customer[gender]->selected'                    => 'isPrivateMobilePostpaid OR isPrivateMobilePrepaid',
                                                                'customer[contact_person][gender]->company'     => 'isCompanyCable',
                                                                'customer[account_password]->mobile'            => 'isMobile',
                                                                'customer[account_password]->dsl'               => 'isFixed',
                                                                'billing[other_address][gender]->company'       => 'isCompany',
                                                                'billing[email_invoice]->checked'               => 'isPrivateFixed OR isPrivateMobilePostpaid OR isPrivateMobilePrepaid',
                                                                'billing[email_invoice]->readonly'              => 'isPrivateMobilePostpaid OR isPrivateMobilePrepaid',
                                                                'billing[email_address]->required'              => 'isFixed OR isMobilePostpaid OR isMobilePrepaid',
                                                                'billing[post_invoice]->value'                  => 'isMobile OR isFixed'
                                                                ),

                                        'data-binding' => array( 'individual_package'                           => 'isMobile',
                                                                 'fixednet'                                     => 'isFixed'
                                                                ),
                                    );

    // set for ILS scenario
    protected $visibleRuling = [
        'ACQ' => [
            'PRIVATE' => [
                'CREYES' => [

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => '',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'alwaysShow',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => 'alwaysShow',
                    'customer[nationality]'            => 'alwaysShow',

                    'customer[c1-3]'                   => 'alwaysShow',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => '',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => '',
                    'customer_soho[address][city]'     => '',
                    'customer_soho[address][street]'   => '',
                    'customer_soho[address][houseno]'  => '',
                    'customer_soho[address][addition]' => '',

                    'customer[c1-5]'                             => 'alwaysShow',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobilePostpaid OR isFixed', // customer password

                    'customer[confirmation][email]'    => '',
                    'customer[confirmation][sms]'      => '',
                ],
            ],
            'SOHO' => [
                'CREYES' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'isMobile OR isCable OR isFixed',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'isMobile OR isCable',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => 'alwaysShow',
                    'customer[nationality]'            => 'alwaysShow',

                    'customer[c1-3]'                   => 'alwaysShow',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => 'isMobile OR isCable OR isFixed',
                    'customer[c1-4a]'                  => 'isMobile OR isCable OR isFixed',
                    'customer[c1-4b]'                  => 'isCable',
                    'customer_soho[address][postcode]' => 'alwaysShow',
                    'customer_soho[address][city]'     => 'alwaysShow',
                    'customer_soho[address][street]'   => 'alwaysShow',
                    'customer_soho[address][houseno]'  => 'alwaysShow',
                    'customer_soho[address][addition]' => 'alwaysShow',

                    'customer[c1-5]'                             => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile or isFixed', // customer password

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'isMobile OR isCable OR isFixed',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'isMobile OR isFixed',
                    'customer[c1-2c]'                  => 'isCable',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => 'alwaysShow',
                    'customer[nationality]'            => 'alwaysShow',

                    'customer[c1-3]'                   => 'isMobile OR isFixed',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => 'isCable OR isFixed',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => 'isMobile OR isCable OR isFixed',
                    'customer_soho[address][postcode]' => 'alwaysShow',
                    'customer_soho[address][city]'     => 'alwaysShow',
                    'customer_soho[address][street]'   => 'alwaysShow',
                    'customer_soho[address][houseno]'  => 'alwaysShow',
                    'customer_soho[address][addition]' => 'alwaysShow',

                    'customer[c1-5]'                             => 'isMobile or isCable or isFixed or isMobileCable',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile or isFixed', // customer password
                ],
            ],
            'delivery[method][service]'        => 'isDSL OR isCable',
            'delivery[method][deliver]'        => 'isCable OR isMobile OR isFixed',
            'delivery[method][other_address]'  => 'isCable OR isMobile OR isFixed',
            'delivery[method][pickup]'         => 'isMobile',
            'delivery[same_day]'               => 'isMobile',
            'delivery[date]'                   => 'isCable',

            'provider[subuser_dob]'            => 'isMobile',
            'provider[change_provider]'        => 'isFixed',
            'provider[save_connection_data]'   => 'isCable',
            'other[phonebook_choice]'          => 'isMobile OR isDSL',
            'other[single_connection]'         => 'isMobilePostpaid OR isDSL OR isCable',
        ],
        'ILSBLU' => [
            'PRIVATE' => [
                'CREYES' => [

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => '',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'alwaysShow',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'isMobile OR isCable OR isFixed',
                    'customer[prefix]'                 => 'isMobile OR isCable OR isFixed',
                    'customer[firstname]'              => 'isMobile OR isCable OR isFixed',
                    'customer[lastname]'               => 'isMobile OR isCable OR isFixed',
                    'customer[dob]'                    => 'isMobile OR isCable',
                    'customer[id_number]'              => 'isMobile',
                    'customer[nationality]'            => 'isMobile',

                    'customer[c1-3]'                   => 'alwaysShow',
                    'customer[address][postcode]'      => 'isMobile OR isCable OR isFixed',
                    'customer[address][city]'          => 'isMobile OR isCable OR isFixed',
                    'customer[address][street]'        => 'isMobile OR isCable OR isFixed',
                    'customer[address][houseno]'       => 'isMobile OR isCable OR isFixed',
                    'customer[address][addition]'      => 'isMobile OR isCable OR isFixed',

                    'customer[c1-4]'                   => '',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => '',
                    'customer_soho[address][city]'     => '',
                    'customer_soho[address][street]'   => '',
                    'customer_soho[address][houseno]'  => '',
                    'customer_soho[address][addition]' => '',

                    'customer[c1-5]'                             => 'alwaysShow',
                    'customer[contact_details][email]'           => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list]'      => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => '',

                    'customer[c1-6]'                   => 'isFixed', // customer password
                ],
            ],
            'SOHO' => [
                'CREYES' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'isMobile OR isCable',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'isMobile OR isCable',
                    'customer[id_number]'              => 'isMobile',
                    'customer[nationality]'            => 'isMobile',

                    'customer[c1-3]'                   => 'isMobile',
                    'customer[address][postcode]'      => 'isMobile',
                    'customer[address][city]'          => 'isMobile',
                    'customer[address][street]'        => 'isMobile',
                    'customer[address][houseno]'       => 'isMobile',
                    'customer[address][addition]'      => 'isMobile',

                    'customer[c1-4]'                   => 'isCable OR isFixed',
                    'customer[c1-4a]'                  => 'isFixed',
                    'customer[c1-4b]'                  => 'isCable',
                    'customer_soho[address][postcode]' => 'isCable OR isFixed',
                    'customer_soho[address][city]'     => 'isCable OR isFixed',
                    'customer_soho[address][street]'   => 'isCable OR isFixed',
                    'customer_soho[address][houseno]'  => 'isCable OR isFixed',
                    'customer_soho[address][addition]' => 'isCable OR isFixed',

                    'customer[c1-5]'                             => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][email]'           => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list]'      => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile OR isFixed', // customer password

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'isMobile OR isFixed',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'isMobile OR isFixed',
                    'customer[id_number]'              => 'isMobile',
                    'customer[nationality]'            => 'isMobile',

                    'customer[c1-3]'                   => 'isMobile OR isFixed',
                    'customer[address][postcode]'      => 'isMobile OR isFixed',
                    'customer[address][city]'          => 'isMobile OR isFixed',
                    'customer[address][street]'        => 'isMobile OR isFixed',
                    'customer[address][houseno]'       => 'isMobile OR isFixed',
                    'customer[address][addition]'      => 'isMobile OR isFixed',

                    'customer[c1-4]'                   => 'isCable OR isFixed',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => 'isCable OR isFixed',
                    'customer_soho[address][postcode]' => 'isCable OR isFixed',
                    'customer_soho[address][city]'     => 'isCable OR isFixed',
                    'customer_soho[address][street]'   => 'isCable OR isFixed',
                    'customer_soho[address][houseno]'  => 'isCable OR isFixed',
                    'customer_soho[address][addition]' => 'isCable OR isFixed',

                    'customer[c1-5]'                             => 'isMobileCable OR isFixed OR isCable',
                    'customer[contact_details][email]'           => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list]'      => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile OR isFixed', // customer password
                ],
            ],
            'delivery[method][service]'        => 'isCable OR isFixed',
            'delivery[method][deliver]'        => 'isCable OR isMobile OR isFixed',
            'delivery[method][other_address]'  => 'isCable OR isMobile OR isFixed',
            'delivery[method][pickup]'         => 'isMobile OR isFixed',
            'delivery[same_day]'               => 'isMobile',
            'delivery[date]'                   => 'isCable',

            'other[single_connection]'         => 'isMobilePostpaid OR isCable',

            'provider[cable_internet_phone][change_provider]' => 'isCable',
            'provider[cable_tv][change_provider]'             => 'isCable',
            'provider[cable_independent][change_provider]'    => 'isCable',
            'provider[dsl][change_provider]'   => '',
            'provider[lte][change_provider]'   => 'isLTE',
            'provider[connection_active]'      => 'isCable',
            'provider[save_connection_data]'   => 'isCable',
            'provider[subuser_dob]'            => 'isMobile',
        ],
        'ILSNOBLU' => [
            'PRIVATE' => [
                'CREYES' => [

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => '',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'alwaysShow',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'isMobile OR isCable OR isFixed',
                    'customer[prefix]'                 => 'isMobile OR isCable OR isFixed',
                    'customer[firstname]'              => 'isMobile OR isCable OR isFixed',
                    'customer[lastname]'               => 'isMobile OR isCable OR isFixed',
                    'customer[dob]'                    => 'isMobile OR isCable',
                    'customer[id_number]'              => 'isMobile',
                    'customer[nationality]'            => 'isMobile OR isCable',

                    'customer[c1-3]'                   => 'alwaysShow',
                    'customer[address][postcode]'      => 'isMobile OR isCable OR isFixed',
                    'customer[address][city]'          => 'isMobile OR isCable OR isFixed',
                    'customer[address][street]'        => 'isMobile OR isCable OR isFixed',
                    'customer[address][houseno]'       => 'isMobile OR isCable OR isFixed',
                    'customer[address][addition]'      => 'isMobile OR isCable OR isFixed',

                    'customer[c1-4]'                   => '',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => '',
                    'customer_soho[address][city]'     => '',
                    'customer_soho[address][street]'   => '',
                    'customer_soho[address][houseno]'  => '',
                    'customer_soho[address][addition]' => '',

                    'customer[c1-5]'                             => 'alwaysShow',
                    'customer[contact_details][email]'           => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list]'      => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => '',

                    'customer[c1-6]'                   => 'isFixed', // customer password
                ],
            ],
            'SOHO' => [
                'CREYES' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'isMobile OR isCable',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'isMobile OR isCable',
                    'customer[id_number]'              => 'isMobile',
                    'customer[nationality]'            => 'isMobile',

                    'customer[c1-3]'                   => 'isMobile',
                    'customer[address][postcode]'      => 'isMobile',
                    'customer[address][city]'          => 'isMobile',
                    'customer[address][street]'        => 'isMobile',
                    'customer[address][houseno]'       => 'isMobile',
                    'customer[address][addition]'      => 'isMobile',

                    'customer[c1-4]'                   => 'isCable OR isFixed',
                    'customer[c1-4a]'                  => 'isFixed',
                    'customer[c1-4b]'                  => 'isCable',
                    'customer_soho[address][postcode]' => 'isCable OR isFixed',
                    'customer_soho[address][city]'     => 'isCable OR isFixed',
                    'customer_soho[address][street]'   => 'isCable OR isFixed',
                    'customer_soho[address][houseno]'  => 'isCable OR isFixed',
                    'customer_soho[address][addition]' => 'isCable OR isFixed',

                    'customer[c1-5]'                             => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][email]'           => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list]'      => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => '', // customer password
                ],
                'CRENO' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'isMobile OR isFixed',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'isMobile OR isFixed',
                    'customer[id_number]'              => 'isMobile',
                    'customer[nationality]'            => 'isMobile',

                    'customer[c1-3]'                   => 'isMobile OR isFixed',
                    'customer[address][postcode]'      => 'isMobile OR isFixed',
                    'customer[address][city]'          => 'isMobile OR isFixed',
                    'customer[address][street]'        => 'isMobile OR isFixed',
                    'customer[address][houseno]'       => 'isMobile OR isFixed',
                    'customer[address][addition]'      => 'isMobile OR isFixed',

                    'customer[c1-4]'                   => 'isCable OR isFixed',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => 'isCable OR isFixed',
                    'customer_soho[address][postcode]' => 'isCable OR isFixed',
                    'customer_soho[address][city]'     => 'isCable OR isFixed',
                    'customer_soho[address][street]'   => 'isCable OR isFixed',
                    'customer_soho[address][houseno]'  => 'isCable OR isFixed',
                    'customer_soho[address][addition]' => 'isCable OR isFixed',

                    'customer[c1-5]'                             => 'isMobileCable OR isFixed',
                    'customer[contact_details][email]'           => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list]'      => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile OR isFixed', // customer password
                ],
            ],
            'delivery[method][service]'        => 'isCable OR isFixed',
            'delivery[method][deliver]'        => 'isCable OR isMobile OR isFixed',
            'delivery[method][other_address]'  => 'isCable OR isMobile OR isFixed',
            'delivery[method][pickup]'         => 'isMobile OR isFixed',

            'provider[cable_internet_phone][change_provider]' => 'isCable',
            'provider[cable_tv][change_provider]' => 'isCable',
            'provider[cable_independent][change_provider]' => 'isCable',
            'provider[dsl][change_provider]'   => '',
            'provider[lte][change_provider]'   => 'isLTE',
            'provider[connection_active]'      => 'isCable',
            'provider[save_connection_data]'   => 'isCable',
            'provider[subuser_dob]'            => 'isMobile',
            'other[single_connection]'         => 'isDSL',
        ],
        'MIGCOC' => [
            'PRIVATE' => [
                'CREYES' => [

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => '',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'alwaysShow',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => 'alwaysShow',
                    'customer[nationality]'            => 'alwaysShow',

                    'customer[c1-3]'                   => 'alwaysShow',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => '',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => '',
                    'customer_soho[address][city]'     => '',
                    'customer_soho[address][street]'   => '',
                    'customer_soho[address][houseno]'  => '',
                    'customer_soho[address][addition]' => '',

                    'customer[c1-5]'                             => 'alwaysShow',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => '',

                    'customer[c1-6]'                   => 'isMobile OR isFixed', // customer password

                    'customer[confirmation][email]'    => '',
                    'customer[confirmation][sms]'      => '',
                ],
            ],
            'SOHO' => [
                'CREYES' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'isMobile OR isCable',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'isMobile OR isCable',
                    'customer[id_number]'              => 'isMobile',
                    'customer[nationality]'            => 'isMobile',

                    'customer[c1-3]'                   => 'isMobile',
                    'customer[address][postcode]'      => 'isMobile',
                    'customer[address][city]'          => 'isMobile',
                    'customer[address][street]'        => 'isMobile',
                    'customer[address][houseno]'       => 'isMobile',
                    'customer[address][addition]'      => 'isMobile',

                    'customer[c1-4]'                   => 'isCable OR isFixed',
                    'customer[c1-4a]'                  => 'isFixed',
                    'customer[c1-4b]'                  => 'isCable',
                    'customer_soho[address][postcode]' => 'isCable OR isFixed',
                    'customer_soho[address][city]'     => 'isCable OR isFixed',
                    'customer_soho[address][street]'   => 'isCable OR isFixed',
                    'customer_soho[address][houseno]'  => 'isCable OR isFixed',
                    'customer_soho[address][addition]' => 'isCable OR isFixed',

                    'customer[c1-5]'                             => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][email]'           => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list]'      => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => '', // customer password
                ],
                'CRENO' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'isMobile OR isFixed',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'isMobile OR isFixed',
                    'customer[id_number]'              => 'isMobile',
                    'customer[nationality]'            => 'isMobile',

                    'customer[c1-3]'                   => 'isMobile OR isFixed',
                    'customer[address][postcode]'      => 'isMobile OR isFixed',
                    'customer[address][city]'          => 'isMobile OR isFixed',
                    'customer[address][street]'        => 'isMobile OR isFixed',
                    'customer[address][houseno]'       => 'isMobile OR isFixed',
                    'customer[address][addition]'      => 'isMobile OR isFixed',

                    'customer[c1-4]'                   => 'isCable OR isFixed',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => 'isCable OR isFixed',
                    'customer_soho[address][postcode]' => 'isCable OR isFixed',
                    'customer_soho[address][city]'     => 'isCable OR isFixed',
                    'customer_soho[address][street]'   => 'isCable OR isFixed',
                    'customer_soho[address][houseno]'  => 'isCable OR isFixed',
                    'customer_soho[address][addition]' => 'isCable OR isFixed',

                    'customer[c1-5]'                             => 'isMobile or isCable or isMobileCable or isFixed',
                    'customer[contact_details][email]'           => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list]'      => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile OR isFixed', // customer password
                ],
            ],
            'customer[confirmation][email]'    => '',
            'customer[confirmation][sms]'      => '',

            'voicelog[terms3]'                 => 'isMobile',


            'provider[change_provider]'        => 'isDSL',
            'provider[dsl][change_provider]'   => 'isDSL',
            'provider[current_provider_type]'  => 'isCable OR isDSL',
            'other[phonebook_choice]'          => 'isMobile OR isDSL',
            'other[single_connection]'         => 'isMobilePostpaid OR isDSL',
            'provider[subuser_dob]'            => 'isMobile',

            'delivery[method][service]'        => 'isCable OR isFixed',
            'delivery[method][deliver]'        => 'isMobile OR isCable OR isFixed',
            'delivery[method][other_address]'  => 'isMobile OR isCable OR isFixed',
            'delivery[method][pickup]'         => 'isMobile',
            'delivery[date]'                   => 'isCable',
            'delivery[same_day]'               => 'isMobile',
        ],
        'MIGNOCOC' => [
            'PRIVATE' => [
                'CREYES' => [

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => '',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => 'alwaysShow',
                    'customer[c1-2b]'                  => 'alwaysShow',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => 'alwaysShow',
                    'customer[nationality]'            => 'alwaysShow',

                    'customer[c1-3]'                   => 'alwaysShow',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => '',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => '',
                    'customer_soho[address][city]'     => '',
                    'customer_soho[address][street]'   => '',
                    'customer_soho[address][houseno]'  => '',
                    'customer_soho[address][addition]' => '',

                    'customer[c1-5]'                             => 'alwaysShow',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile OR isFixed', // customer password

                    'customer[confirmation][email]'    => '',
                    'customer[confirmation][sms]'      => '',
                ],
            ],
            'SOHO' => [
                'CREYES' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'isMobile OR isCable',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'isMobile OR isCable',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'isMobile OR isCable',
                    'customer[id_number]'              => 'isMobile',
                    'customer[nationality]'            => 'isMobile',

                    'customer[c1-3]'                   => 'isMobile',
                    'customer[address][postcode]'      => 'isMobile',
                    'customer[address][city]'          => 'isMobile',
                    'customer[address][street]'        => 'isMobile',
                    'customer[address][houseno]'       => 'isMobile',
                    'customer[address][addition]'      => 'isMobile',

                    'customer[c1-4]'                   => 'isCable OR isFixed',
                    'customer[c1-4a]'                  => 'isFixed',
                    'customer[c1-4b]'                  => 'isCable',
                    'customer_soho[address][postcode]' => 'isCable OR isFixed',
                    'customer_soho[address][city]'     => 'isCable OR isFixed',
                    'customer_soho[address][street]'   => 'isCable OR isFixed',
                    'customer_soho[address][houseno]'  => 'isCable OR isFixed',
                    'customer_soho[address][addition]' => 'isCable OR isFixed',

                    'customer[c1-5]'                             => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][email]'           => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list]'      => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => '', // customer password
                ],
                'CRENO' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => 'isMobile',
                    'customer[c1-2b]'                  => 'isMobile OR isFixed',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'isMobile OR isFixed',
                    'customer[id_number]'              => 'isMobile',
                    'customer[nationality]'            => 'isMobile',

                    'customer[c1-3]'                   => 'isMobile OR isFixed',
                    'customer[address][postcode]'      => 'isMobile OR isFixed',
                    'customer[address][city]'          => 'isMobile OR isFixed',
                    'customer[address][street]'        => 'isMobile OR isFixed',
                    'customer[address][houseno]'       => 'isMobile OR isFixed',
                    'customer[address][addition]'      => 'isMobile OR isFixed',

                    'customer[c1-4]'                   => 'isCable OR isFixed',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => 'isCable OR isFixed',
                    'customer_soho[address][postcode]' => 'isCable OR isFixed',
                    'customer_soho[address][city]'     => 'isCable OR isFixed',
                    'customer_soho[address][street]'   => 'isCable OR isFixed',
                    'customer_soho[address][houseno]'  => 'isCable OR isFixed',
                    'customer_soho[address][addition]' => 'isCable OR isFixed',

                    'customer[c1-5]'                             => 'isMobile or isCable or isMobileCable',
                    'customer[contact_details][email]'           => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list]'      => 'isMobile OR isCable OR isFixed',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile OR isFixed', // customer password
                ],
            ],
            'provider[subuser_dob]'            => 'isMobile',
            'customer[confirmation][email]'    => '',
            'customer[confirmation][sms]'      => '',

            'delivery[same_day]'               => 'isMobile',
        ],
        'RETBLU' => [
            'PRIVATE' => [
                'CREYES' => [

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => '',

                    'customer[c1-2]'                   => 'isMobile',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'isMobile',
                    'customer[c1-2c]'                  => 'isMobile',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'isMobile',
                    'customer[prefix]'                 => 'isMobile',
                    'customer[firstname]'              => 'isMobile',
                    'customer[lastname]'               => 'isMobile',
                    'customer[dob]'                    => 'isMobile',
                    'customer[id_number]'              => '',
                    'customer[nationality]'            => '',

                    'customer[c1-3]'                   => 'isMobile',
                    'customer[address][postcode]'      => 'isMobile',
                    'customer[address][city]'          => 'isMobile',
                    'customer[address][street]'        => 'isMobile',
                    'customer[address][houseno]'       => 'isMobile',
                    'customer[address][addition]'      => 'isMobile',

                    'customer[c1-4]'                   => '',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => '',
                    'customer_soho[address][city]'     => '',
                    'customer_soho[address][street]'   => '',
                    'customer_soho[address][houseno]'  => '',
                    'customer_soho[address][addition]' => '',

                    'customer[c1-5]'                             => 'isMobile',
                    'customer[contact_details][email]'           => 'isMobile',
                    'customer[contact_details][phone_list]'      => 'isMobile',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => '',

                    'customer[c1-6]'                   => 'isFixed', // customer password

                    'customer[confirmation][email]'    => '',
                    'customer[confirmation][sms]'      => '',
                ],
            ],
            'SOHO' => [
                'CREYES' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'isMobile',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'isMobile',
                    'customer[c1-2c]'                  => 'isMobile',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'isMobile',
                    'customer[prefix]'                 => 'isMobile',
                    'customer[firstname]'              => 'isMobile',
                    'customer[lastname]'               => 'isMobile',
                    'customer[dob]'                    => 'isMobile',
                    'customer[id_number]'              => '',
                    'customer[nationality]'            => '',

                    'customer[c1-3]'                   => '',
                    'customer[address][postcode]'      => 'isMobile',
                    'customer[address][city]'          => 'isMobile',
                    'customer[address][street]'        => 'isMobile',
                    'customer[address][houseno]'       => 'isMobile',
                    'customer[address][addition]'      => 'isMobile',

                    'customer[c1-4]'                   => 'isMobile',
                    'customer[c1-4a]'                  => 'isMobile',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => 'isMobile',
                    'customer_soho[address][city]'     => 'isMobile',
                    'customer_soho[address][street]'   => 'isMobile',
                    'customer_soho[address][houseno]'  => 'isMobile',
                    'customer_soho[address][addition]' => 'isMobile',

                    'customer[c1-5]'                             => 'isMobile',
                    'customer[contact_details][email]'           => 'isMobile',
                    'customer[contact_details][phone_list]'      => 'isMobile',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile', // customer password

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'isMobile',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'isMobile',
                    'customer[c1-2c]'                  => 'isMobile',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'isMobile',
                    'customer[prefix]'                 => 'isMobile',
                    'customer[firstname]'              => 'isMobile',
                    'customer[lastname]'               => 'isMobile',
                    'customer[dob]'                    => 'isMobile',
                    'customer[id_number]'              => '',
                    'customer[nationality]'            => '',

                    'customer[c1-3]'                   => 'isMobile',
                    'customer[address][postcode]'      => 'isMobile',
                    'customer[address][city]'          => 'isMobile',
                    'customer[address][street]'        => 'isMobile',
                    'customer[address][houseno]'       => 'isMobile',
                    'customer[address][addition]'      => 'isMobile',

                    'customer[c1-4]'                   => 'isMobile',
                    'customer[c1-4a]'                  => 'isMobile',
                    'customer[c1-4b]'                  => 'isMobile',
                    'customer_soho[address][postcode]' => 'isMobile',
                    'customer_soho[address][city]'     => 'isMobile',
                    'customer_soho[address][street]'   => 'isMobile',
                    'customer_soho[address][houseno]'  => 'isMobile',
                    'customer_soho[address][addition]' => 'isMobile',

                    'customer[c1-5]'                             => 'isMobileCable',
                    'customer[contact_details][email]'           => 'isMobile',
                    'customer[contact_details][phone_list]'      => 'isMobile',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile', // customer password
                ],
            ],

            'other[single_connection]'         => 'isMobilePostpaid',
            'provider[subuser_dob]'            => 'isMobile',

            'delivery[method][deliver]'        => 'isMobile',
            'delivery[method][other_address]'  => 'isMobile',
            'delivery[method][pickup]'         => 'isMobile',
            'delivery[same_day]'               => 'isMobile',
        ],
        'RETNOBLU' => [
            'PRIVATE' => [
                'CREYES' => [

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => '',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'alwaysShow',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => '',
                    'customer[nationality]'            => '',

                    'customer[c1-3]'                   => 'alwaysShow',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => '',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => '',
                    'customer_soho[address][city]'     => '',
                    'customer_soho[address][street]'   => '',
                    'customer_soho[address][houseno]'  => '',
                    'customer_soho[address][addition]' => '',

                    'customer[c1-5]'                             => 'alwaysShow',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => '',

                    'customer[c1-6]'                   => 'isFixed', // customer password

                    'customer[confirmation][email]'    => '',
                    'customer[confirmation][sms]'      => '',
                ],
            ],
            'SOHO' => [
                'CREYES' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'isMobile OR isCable',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'isMobile OR isCable',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => '',
                    'customer[nationality]'            => '',

                    'customer[c1-3]'                   => '',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => 'isMobile OR isCable OR isFixed',
                    'customer[c1-4a]'                  => 'isMobile OR isCable OR isFixed',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => 'isMobile OR isCable OR isFixed',
                    'customer_soho[address][city]'     => 'isMobile OR isCable OR isFixed',
                    'customer_soho[address][street]'   => 'isMobile OR isCable OR isFixed',
                    'customer_soho[address][houseno]'  => 'isMobile OR isCable OR isFixed',
                    'customer_soho[address][addition]' => 'isMobile OR isCable OR isFixed',

                    'customer[c1-5]'                             => 'isMobile OR isCable',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => '', // customer password

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'isMobile',
                    'customer[c1-2c]'                  => 'isCable',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => '',
                    'customer[nationality]'            => '',

                    'customer[c1-3]'                   => 'isMobile OR isCable',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => 'isMobile OR isCable OR isFixed',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => 'isMobile OR isCable OR isFixed',
                    'customer_soho[address][postcode]' => 'isMobile OR isCable OR isFixed',
                    'customer_soho[address][city]'     => 'isMobile OR isCable OR isFixed',
                    'customer_soho[address][street]'   => 'isMobile OR isCable OR isFixed',
                    'customer_soho[address][houseno]'  => 'isMobile OR isCable OR isFixed',
                    'customer_soho[address][addition]' => 'isMobile OR isCable OR isFixed',

                    'customer[c1-5]'                             => 'isMobileCable',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile OR isCable', // customer password
                ],
            ],
            'other[single_connection]'         => 'isMobilePostpaid',
            'provider[subuser_dob]'            => 'isMobile',

            'delivery[method][deliver]'        => 'isMobile',
            'delivery[method][other_address]'  => 'isMobile',
            'delivery[method][pickup]'         => 'isMobile',
            'delivery[same_day]'               => 'isMobile',
        ],
        'RETTER' => [
            'PRIVATE' => [
                'CREYES' => [

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => '',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'alwaysShow',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => '',
                    'customer[nationality]'            => '',

                    'customer[c1-3]'                   => 'alwaysShow',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => '',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => '',
                    'customer_soho[address][city]'     => '',
                    'customer_soho[address][street]'   => '',
                    'customer_soho[address][houseno]'  => '',
                    'customer_soho[address][addition]' => '',

                    'customer[c1-5]'                             => 'alwaysShow',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => '',

                    'customer[c1-6]'                   => 'isFixed', // customer password

                    'customer[confirmation][email]'    => '',
                    'customer[confirmation][sms]'      => '',
                ],
            ],
            'SOHO' => [
                'CREYES' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'isMobile OR isCable',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'isMobile OR isCable',
                    'customer[c1-2c]'                  => 'alwaysShow',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => '',
                    'customer[nationality]'            => '',

                    'customer[c1-3]'                   => '',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => 'isMobile OR isCable',
                    'customer[c1-4a]'                  => 'isMobile OR isCable',
                    'customer[c1-4b]'                  => '',
                    'customer_soho[address][postcode]' => 'alwaysShow',
                    'customer_soho[address][city]'     => 'alwaysShow',
                    'customer_soho[address][street]'   => 'alwaysShow',
                    'customer_soho[address][houseno]'  => 'alwaysShow',
                    'customer_soho[address][addition]' => 'alwaysShow',

                    'customer[c1-5]'                             => 'isMobile OR isCable',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => '', // customer password

                ],
                'CRENO' => [
                    'customer[c1-1]'                   => 'isFixed',

                    'customer[c1-2]'                   => 'alwaysShow',
                    'customer[c1-2a]'                  => '',
                    'customer[c1-2b]'                  => 'isMobile',
                    'customer[c1-2c]'                  => 'isCable',
                    'customer[type]'                   => 'alwaysShow',
                    'customer[gender]'                 => 'alwaysShow',
                    'customer[prefix]'                 => 'alwaysShow',
                    'customer[firstname]'              => 'alwaysShow',
                    'customer[lastname]'               => 'alwaysShow',
                    'customer[dob]'                    => 'alwaysShow',
                    'customer[id_number]'              => '',
                    'customer[nationality]'            => '',

                    'customer[c1-3]'                   => 'isMobile OR isCable',
                    'customer[address][postcode]'      => 'alwaysShow',
                    'customer[address][city]'          => 'alwaysShow',
                    'customer[address][street]'        => 'alwaysShow',
                    'customer[address][houseno]'       => 'alwaysShow',
                    'customer[address][addition]'      => 'alwaysShow',

                    'customer[c1-4]'                   => 'isMobile OR isCable',
                    'customer[c1-4a]'                  => '',
                    'customer[c1-4b]'                  => 'isMobile OR isCable',
                    'customer_soho[address][postcode]' => 'alwaysShow',
                    'customer_soho[address][city]'     => 'alwaysShow',
                    'customer_soho[address][street]'   => 'alwaysShow',
                    'customer_soho[address][houseno]'  => 'alwaysShow',
                    'customer_soho[address][addition]' => 'alwaysShow',

                    'customer[c1-5]'                             => 'isMobileCable',
                    'customer[contact_details][email]'           => 'alwaysShow',
                    'customer[contact_details][phone_list]'      => 'alwaysShow',
                    'customer[contact_details][phone_list][add]' => 'isCable or isFixed',
                    'customer[contact_person]'                   => 'isMobile',

                    'customer[c1-6]'                   => 'isMobile OR isFixed', // customer password
                ],
            ],
            'other[single_connection]'         => 'isMobilePostpaid',
            'provider[subuser_dob]'            => 'isMobile',

            'delivery[method][deliver]'        => 'isMobile',
            'delivery[method][other_address]'  => 'isMobile',
            'delivery[method][pickup]'         => 'isMobile',
            'delivery[same_day]'               => 'isMobile',
        ],
        'MOVE' => [
            'provider[change_provider]'        => 'isDSL',
            'provider[current_provider_type]'  => 'isDSL',
            'other[phonebook_choice]'          => 'isDSL'

        ],
    ];

    // set for ILS scenario
    protected $disabledRuling = [
        'ACQ' => [
            'customer[type]'                   => 'isMobile or isCable or isFixed',
            'customer[gender]'                 => 'isMobile or isCable or isFixed',
            'customer[prefix]'                 => 'isMobile or isCable or isFixed',
            'customer[firstname]'              => 'isMobile or isCable or isFixed',
            'customer[lastname]'               => 'isMobile or isCable or isFixed',
            'customer[dob]'                    => 'isMobile or isCable or isFixed',
            'customer[id_number]'              => 'isMobile',
            'customer[nationality]'            => 'isMobile',

            'customer[address][postcode]'      => 'isMobile or isCable or isFixed',
            'customer[address][city]'          => 'isMobile or isCable or isFixed',
            'customer[address][street]'        => 'isMobile or isCable or isFixed',
            'customer[address][houseno]'       => 'isMobile or isCable or isFixed',
            'customer[address][addition]'      => 'isMobile or isCable or isFixed',

            'customer[address][company_name]'                => 'isMobile OR isCable OR isFixed',
            'customer[address][company_additional_name]'     => 'isMobile OR isCable OR isFixed',
            'customer[address][commercial_register_type]'    => 'isMobile OR isCable OR isFixed',
            'customer[address][company_registration_number]' => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][postcode]'               => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][city]'                   => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][street]'                 => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][houseno]'                => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][addition]'               => 'isMobile OR isCable OR isFixed',

            'customer[contact_details][email]'               => 'isMobile OR isCable OR isFixed',
            'customer[contact_details][phone_list]'          => 'isMobile OR isCable OR isFixed',
            'customer[contact_details][phone_list][add]' => '',

            'customer[account_password]'       => 'isMobile OR isFixed',
        ],
        'ILSBLU' => [
            'customer[type]'                   => 'isMobile or isCable or isFixed',
            'customer[gender]'                 => 'isMobile or isCable or isFixed',
            'customer[prefix]'                 => 'isMobile or isCable or isFixed',
            'customer[firstname]'              => 'isMobile or isCable or isFixed',
            'customer[lastname]'               => 'isMobile or isCable or isFixed',
            'customer[dob]'                    => 'isMobile or isCable or isFixed',
            'customer[id_number]'              => 'isMobile',
            'customer[nationality]'            => 'isMobile',

            'customer[address][postcode]'      => 'isMobile or isCable or isFixed',
            'customer[address][city]'          => 'isMobile or isCable or isFixed',
            'customer[address][street]'        => 'isMobile or isCable or isFixed',
            'customer[address][houseno]'       => 'isMobile or isCable or isFixed',
            'customer[address][addition]'      => 'isMobile or isCable or isFixed',

            'customer[address][company_name]'                => 'isMobile OR isCable OR isFixed',
            'customer[address][company_additional_name]'     => 'isMobile OR isCable OR isFixed',
            'customer[address][commercial_register_type]'    => 'isMobile OR isCable OR isFixed',
            'customer[address][company_registration_number]' => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][postcode]'               => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][city]'                   => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][street]'                 => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][houseno]'                => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][addition]'               => 'isMobile OR isCable OR isFixed',

            'customer[contact_details][email]'               => 'isMobile OR isCable OR isFixed',
            'customer[contact_details][phone_list]'          => 'isMobile OR isCable OR isFixed',
            'customer[contact_details][phone_list][add]' => '',

            'customer[account_password]'       => 'isMobile OR isFixed',

            'other[single_connection]'         => 'isMobilePostpaid',
        ],
        'ILSNOBLU' => [
            'customer[type]'                   => 'isMobile or isCable or isFixed',
            'customer[gender]'                 => 'isMobile or isCable or isFixed',
            'customer[prefix]'                 => 'isMobile or isCable or isFixed',
            'customer[firstname]'              => 'isMobile or isCable or isFixed',
            'customer[lastname]'               => 'isMobile or isCable or isFixed',
            'customer[dob]'                    => 'isMobile or isCable or isFixed',
            'customer[id_number]'              => 'isMobile',
            'customer[nationality]'            => 'isMobile',

            'customer[address][postcode]'      => 'isMobile or isCable or isFixed',
            'customer[address][city]'          => 'isMobile or isCable or isFixed',
            'customer[address][street]'        => 'isMobile or isCable or isFixed',
            'customer[address][houseno]'       => 'isMobile or isCable or isFixed',
            'customer[address][addition]'      => 'isMobile or isCable or isFixed',

            'customer[address][company_name]'                => 'isMobile OR isCable OR isFixed',
            'customer[address][company_additional_name]'     => 'isMobile OR isCable OR isFixed',
            'customer[address][commercial_register_type]'    => 'isMobile OR isCable OR isFixed',
            'customer[address][company_registration_number]' => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][postcode]'               => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][city]'                   => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][street]'                 => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][houseno]'                => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][addition]'               => 'isMobile OR isCable OR isFixed',

            'customer[contact_details][email]'               => 'isMobile OR isCable OR isFixed',
            'customer[contact_details][phone_list]'          => 'isMobile OR isCable OR isFixed',
            'customer[contact_details][phone_list][add]' => '',

            'customer[account_password]'       => 'isMobile OR isFixed',

            'other[single_connection]'         => 'isDSL',
        ],
        'MIGCOC' => [
            'customer[type]'                   => 'isMobile or isCable or isFixed',
            'customer[gender]'                 => 'isMobile or isCable or isFixed',
            'customer[prefix]'                 => 'isMobile or isCable or isFixed',
            'customer[firstname]'              => 'isMobile or isCable or isFixed',
            'customer[lastname]'               => 'isMobile or isCable or isFixed',
            'customer[dob]'                    => 'isMobile or isCable or isFixed',
            'customer[id_number]'              => 'isMobile',
            'customer[nationality]'            => 'isMobile',

            'customer[address][postcode]'      => 'isCable or isFixed',
            'customer[address][city]'          => 'isCable or isFixed',
            'customer[address][street]'        => 'isCable or isFixed',
            'customer[address][houseno]'       => 'isCable or isFixed',
            'customer[address][addition]'      => 'isCable or isFixed',

            'customer[address][company_name]'                => 'isMobile OR isCable OR isFixed',
            'customer[address][company_additional_name]'     => 'isMobile OR isCable OR isFixed',
            'customer[address][commercial_register_type]'    => 'isMobile OR isCable OR isFixed',
            'customer[address][company_registration_number]' => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][postcode]'               => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][city]'                   => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][street]'                 => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][houseno]'                => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][addition]'               => 'isMobile OR isCable OR isFixed',

            'customer[contact_details][email]'               => 'isMobile OR isCable OR isFixed',
            'customer[contact_details][phone_list]'          => 'isMobile OR isCable OR isFixed',
            'customer[contact_details][phone_list][add]' => '',

            'customer[account_password]'       => 'isMobile OR isFixed',

            'provider[change_provider]'        => 'isCable',
        ],
        'MIGNOCOC' => [
            'customer[type]'                   => 'isMobile or isCable or isFixed',
            'customer[gender]'                 => 'isMobile or isCable or isFixed',
            'customer[prefix]'                 => 'isMobile or isCable or isFixed',
            'customer[firstname]'              => 'isMobile or isCable or isFixed',
            'customer[lastname]'               => 'isMobile or isCable or isFixed',
            'customer[dob]'                    => 'isMobile or isCable or isFixed',
            'customer[id_number]'              => 'isMobile',
            'customer[nationality]'            => 'isMobile',

            'customer[address][postcode]'      => 'isCable or isFixed',
            'customer[address][city]'          => 'isCable or isFixed',
            'customer[address][street]'        => 'isCable or isFixed',
            'customer[address][houseno]'       => 'isCable or isFixed',
            'customer[address][addition]'      => 'isCable or isFixed',

            'customer[address][company_name]'                => 'isMobile OR isCable OR isFixed',
            'customer[address][company_additional_name]'     => 'isMobile OR isCable OR isFixed',
            'customer[address][commercial_register_type]'    => 'isMobile OR isCable OR isFixed',
            'customer[address][company_registration_number]' => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][postcode]'               => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][city]'                   => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][street]'                 => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][houseno]'                => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][addition]'               => 'isMobile OR isCable OR isFixed',

            'customer[contact_details][email]'               => 'isMobile OR isCable OR isFixed',
            'customer[contact_details][phone_list]'          => 'isMobile OR isCable OR isFixed',
            'customer[contact_details][phone_list][add]' => '',

            'customer[account_password]'       => 'isMobile OR isFixed',

            'provider[change_provider]'        => 'isCable',
        ],
        'RETBLU' => [
            'customer[type]'                   => 'isMobile',
            'customer[gender]'                 => 'isMobile',
            'customer[prefix]'                 => 'isMobile',
            'customer[firstname]'              => 'isMobile',
            'customer[lastname]'               => 'isMobile',
            'customer[dob]'                    => 'isMobile',
            'customer[id_number]'              => '',
            'customer[nationality]'            => '',
            'customer[address][postcode]'      => 'isMobile',
            'customer[address][city]'          => 'isMobile',
            'customer[address][street]'        => 'isMobile',
            'customer[address][houseno]'       => 'isMobile',
            'customer[address][addition]'      => 'isMobile',

            'customer[contact_details][email]'      => 'isMobile',
            'customer[contact_details][phone_list]' => 'isMobile',
            'customer[contact_details][phone_list][add]' => '',

            'customer[account_password]'       => '',
            'customer[confirmation][email]'    => '',
            'customer[confirmation][sms]'      => '',

            'other[single_connection]'         => 'isMobilePostpaid',
        ],
        'RETNOBLU' => [
            'customer[type]'                   => 'isMobile',
            'customer[gender]'                 => 'isMobile',
            'customer[prefix]'                 => 'isMobile',
            'customer[firstname]'              => 'isMobile',
            'customer[lastname]'               => 'isMobile',
            'customer[dob]'                    => 'isMobile',
            'customer[id_number]'              => '',
            'customer[nationality]'            => '',
            'customer[address][postcode]'      => 'isMobile',
            'customer[address][city]'          => 'isMobile',
            'customer[address][street]'        => 'isMobile',
            'customer[address][houseno]'       => 'isMobile',
            'customer[address][addition]'      => 'isMobile',

            'customer_soho[address][postcode]' => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][city]'     => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][street]'   => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][houseno]'  => 'isMobile OR isCable OR isFixed',
            'customer_soho[address][addition]' => 'isMobile OR isCable OR isFixed',

            'customer[contact_details][email]'      => 'isMobile',
            'customer[contact_details][phone_list]' => 'isMobile',
            'customer[contact_details][phone_list][add]' => '',

            'customer[account_password]'       => '',
            'customer[confirmation][email]'    => '',
            'customer[confirmation][sms]'      => '',

            'other[single_connection]'         => 'isMobilePostpaid',
        ],
        'RETTER' => [
            'customer[type]'                   => 'isMobile',
            'customer[gender]'                 => 'isMobile',
            'customer[prefix]'                 => 'isMobile',
            'customer[firstname]'              => 'isMobile',
            'customer[lastname]'               => 'isMobile',
            'customer[dob]'                    => 'isMobile',
            'customer[id_number]'              => '',
            'customer[nationality]'            => '',
            'customer[address][postcode]'      => 'isMobile',
            'customer[address][city]'          => 'isMobile',
            'customer[address][street]'        => 'isMobile',
            'customer[address][houseno]'       => 'isMobile',
            'customer[address][addition]'      => 'isMobile',

            'customer[contact_details][email]'      => 'isMobile',
            'customer[contact_details][phone_list]' => 'isMobile',
            'customer[contact_details][phone_list][add]' => '',

            'customer[account_password]'       => '',
            'customer[confirmation][email]'    => '',
            'customer[confirmation][sms]'      => '',

            'other[single_connection]'         => 'isMobilePostpaid',
        ],
        'MOVE' => [
            'provider[change_provider]'        => 'isDSL',
        ],
    ];

    /**
     * Get rules list for an item
     *
     * @return array
     */
    protected function getRuling( $type, $name )
    {
        if( isset( $this->vodafoneRuling[$type][$name] ) ) {
            return explode( ' ', $this->vodafoneRuling[$type][$name] );
        }
        return array();
    }

    /**
     * Get checkout rules list for an item
     *
     * @return array
     */
    protected function getVisibleRuling($scenarios, $field = '', $customerType = NULL, $cre = NULL)
    {
        foreach ($scenarios as $scenario) {
            if (true === empty($customerType) && true == empty($cre)) {
                if (false === empty($this->visibleRuling[$scenario][$field])) {
                    return explode(' ', $this->visibleRuling[$scenario][$field]);
                }
            } else {
                if (false === empty($this->visibleRuling[$scenario][$customerType][$cre][$field])) {
                    return explode(' ', $this->visibleRuling[$scenario][$customerType][$cre][$field]);
                }
            }
        }

        return [];

    }//end getVisibleRuling()

    /**
     * Get checkout rules list for an item
     *
     * @return array
     */
    protected function getDisabledRuling($scenarios, $field = '', $package = '')
    {
        foreach ($scenarios as $scenario) {
            if (false === empty($this->disabledRuling[$scenario][$field])) {
                return explode(' ', $this->disabledRuling[$scenario][$field]);
            }
        }

        return [];

    }//end getDisabledRuling()

    /**
     * Check if specified rules is met
     *
     * @return bool
     */
    protected function ruleIsMet( $rule, $package = NULL )
    {
        if( $rule == 'OR' || $rule == 'AND' ) return FALSE;
        $ruleType = TRUE;
        if( mb_substr( $rule, 0, 1 ) == "!" ) {
            $ruleType = FALSE;
            $rule = mb_substr( $rule, 1 );
        }
        if( !is_null( $package )) {
            $packageId = $package->getId();
            if( $this->packageSurvey[$packageId]->{$rule} == $ruleType ) {
                return TRUE;
            }
        } else {
            if( isset( $this->{$rule} ) && $this->{$rule} == $ruleType ) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Check if specified rules is met
     *
     * @return bool
     */
    protected function checkoutRuleIsMet( $rule, $package = NULL )
    {
        if( $rule == 'OR' || $rule == 'AND' ) return FALSE;
        $ruleType = TRUE;
        if( mb_substr( $rule, 0, 1 ) == "!" ) {
            $ruleType = FALSE;
            $rule = mb_substr( $rule, 1 );
        }
        if( !is_null( $package )) {
            $packageId = $package->getId();
            if( $this->packageSurvey[$packageId]->{$rule} == $ruleType ) {
                return TRUE;
            }
        } else {
            if( isset( $this->{$rule} ) && $this->{$rule} == $ruleType ) {
                return TRUE;
            }
        }
        return FALSE;

    }//end checkoutRuleIsMet()

    /**
     * Check if specified rules is met
     *
     * @return bool
     */
    protected function visibleRuleIsMet( $rule, $package = NULL )
    {
        if( $rule == 'OR' || $rule == 'AND' ) return FALSE;
        $ruleType = TRUE;
        if( mb_substr( $rule, 0, 1 ) == "!" ) {
            $ruleType = FALSE;
            $rule = mb_substr( $rule, 1 );
        }
        if( !is_null( $package )) {
            $packageId = $package->getId();
            if( $this->packageSurvey[$packageId]->{$rule} == $ruleType ) {
                return TRUE;
            }
        } else {
            if( isset( $this->{$rule} ) && $this->{$rule} == $ruleType ) {
                return TRUE;
            }
        }
        return FALSE;

    }//end visibleRuleIsMet()

    /**
     * Check if section is visible in the checkout
     *
     * @return bool
     */
    public function isAvailableSection( $sectionName, $package = NULL )
    {
        if( !is_null( $package ) ) {
            $this->surveyPackageType( $package );
        }
        foreach( $this->getRuling( 'section', $sectionName ) as $rule ) {
            if( $this->ruleIsMet( $rule, $package ) ) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Check if fields is visible in the checkout
     *
     * @return bool
     */
    public function isAvailableField( $fieldName, $package = NULL )
    {
        if( !is_null( $package ) ) {
            $this->surveyPackageType( $package );
        }
        foreach( $this->getRuling( 'field', $fieldName ) as $rule ) {
            if( $this->ruleIsMet( $rule, $package ) ) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Check if field value is visible in the checkout
     *
     * @return bool
     */
    public function isVisibleFieldValue( $fieldName, $valueName )
    {
        foreach( $this->getRuling( 'value', $fieldName . '->' . $valueName ) as $rule ) {
            if( $this->ruleIsMet( $rule ) ) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Check if fields is visible in the checkout - ILS scenarios and new
     *
     * @return bool
     */
    public function isFieldAvailable($fieldName, $package = NULL, $customerType = NULL, $cre = NULL)
    {
        $this->setupFormLayout();
        $result = FALSE;
        $scenarios = [];
        if($package) {
            $scenarios[] = strtoupper($package->getSaleType());
        } else {
            $scenarios = $this->getAllPackagesContext();
        }

        if (true == isset($customerType)) {
            foreach($this->getPackages() as $package) {
                $scenarios = [strtoupper($package->getSaleType())];
                if (true == isset($cre)) {
                    $ct = ($customerType === 1) ? 'SOHO' : 'PRIVATE';
                    $cr = ($cre === 1) ? 'CREYES' : 'CRENO';
                    foreach ($this->getVisibleRuling($scenarios, $fieldName, $ct, $cr) as $rule) {
                        if ($this->visibleRuleIsMet($rule) && ($rule == 'alwaysShow' || true === in_array($rule, $this->getPackageRule($package)))) {
                            $result = TRUE;
                        }
                    }
                }
            }
        } else {
            foreach ($this->getVisibleRuling($scenarios, $fieldName) as $rule) {
                if ($this->visibleRuleIsMet($rule, $package)) {
                    $result = TRUE;
                }
            }
        }

        return $result;
    }

    /**
     * Check if fields is visible in the checkout for a specific package(s)
     *
     * @param string $fieldName
     * @param array $packages
     * @param string $customerType
     * @param bool $cre
     * @return bool
     */
    public function isFieldAvailableForSpecificPackages($fieldName, $packages, $customerType = null, $cre = null)
    {
        foreach($packages as $package) {
            $scenarios = [strtoupper($package->getSaleType())];
            if(isset($customerType)) {
                if(isset($cre)) {
                    $customerType = ($customerType === 1) ? 'SOHO' : 'PRIVATE';
                    $cre = ($cre === 1) ? 'CREYES' : 'CRENO';
                    foreach ($this->getVisibleRuling($scenarios, $fieldName, $customerType, $cre) as $rule) {
                        if ($this->visibleRuleIsMet($rule, $package) && ($rule == 'alwaysShow'
                                || in_array($rule, $this->getPackageRule($package)))) {
                            return true;
                        }
                    }
                }
            } else {
                foreach ($this->getVisibleRuling($scenarios, $fieldName) as $rule) {
                    if ($this->visibleRuleIsMet($rule, $package)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    /**
     * Get package types in rule code
     *
     * @param $package
     * @return array
     */
    public function getPackageRule($package)
    {
        $result = [];

        if (true === in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getMobilePackages())) {
            $result[] = 'isMobile';
        }

        if (true === in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getPostPaidMobilePackages())) {
            $result[] = 'isMobilePostpaid';
        }

        if (true === in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getPrePaidMobilePackages())) {
            $result[] = 'isMobilePrepaid';
        }

        if (true === in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getCablePackages())) {
            $result[] = 'isCable';
        }

        if (true === in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getFixedPackages())) {
            $result[] = 'isFixed';
        }

        if (true === in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getDslPackages())) {
            $result[] = 'isDSL';
        }

        if (true === in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getLtePackages())) {
            $result[] = 'isLTE';
        }

        return $result;

    }//end getPackageRule()


    /**
     * Check if field should be disabled in checkout
     *
     * @param $fieldName
     * @param $customerLoggedIn
     * @param $isSohoCustomer
     * @param $isProspect
     *
     * @return boolean
     */
    public function isFieldDisabled($fieldName, $customerLoggedIn = true, $isProspect = false, $package=null)
    {

        if (!is_null($package)) {
            $this->surveyPackageType($package);
            $scenarios = array($package->getSaleType());
        }else{
            $scenarios = $this->getAllPackagesContext();
        }

        if (false === empty($scenarios)) {
            foreach ($this->getDisabledRuling($scenarios, $fieldName) as $rule) {
                if (true === $this->checkoutRuleIsMet($rule, $package) && false === empty($customerLoggedIn) && false === $isProspect) {
                    return true;
                }
            }
        } else {
            if (false === empty($customerLoggedIn) && false === $isProspect) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get all packages context
     *
     * @return array
     */
    public function getAllPackagesContext()
    {
        foreach($this->getPackages() as $package) {
            $result[] = strtoupper($package->getSaleType());
        }

        return $result;
    }

    /**
     * Check if current agent is COPS
     *
     * @return bool
     */
    public function currentAgentIsCOPS()
    {
        /** @var Dyna_Customer_Model_Session $session */
        $session = Mage::getSingleton('dyna_customer/session');

        // If UCT link was used, agent is not COPS
        $uctParams = $session->getUctParams();
        if ($uctParams && !empty($uctParams['transactionId']) && !empty($uctParams['campaignId'])) {
            return false;
        }

        return true;
    }

    /**
     * Check if current agent is COPS and has valid SalesID set
     *
     * @return bool
     */
    public function currentAgentHasSalesId()
    {
        /* If we have a campaign, every quote must have set the data campaign*/
        /** @var Dyna_Customer_Model_Session $session */
        $session = Mage::getSingleton('dyna_customer/session');
        /** @var Dyna_Agent_Model_Dealer $agent */
        $agent = $session->getAgent();
        /** @array Dyna_AgentDE_Model_Commision $agentCommision */
        $copsAgentSalesIds = Mage::getModel('agentde/commision')
            ->getCollection()
            ->addFieldToFilter('agent_id', array('eq' => $agent->getAgentId()));
        // if there is a COPS agent, mapping of sales_id is made from agent_commision
        if (count($copsAgentSalesIds) > 0) {
            foreach ($copsAgentSalesIds as $salesOption) {
                if ((int)$salesOption->getSalesId() > 0) {
                    return true;
                }
            }
            return false;
        }
        // other types of agents are always allowed
        return true;
    }

    /**
     * Check if package has data binding
     *
     * @return bool
     */
    public function hasBinding( $packageType, $package )
    {
        foreach( $this->getRuling( 'data-binding', $packageType ) as $rule ) {
            if( $this->ruleIsMet( $rule, $package ) ) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Check if delivery options can be split
     *
     * @return bool
     */
    public function showSplitDelivery() {
        return $this->countContractlessItems($this->getPackages()) > 1;
    }

    /**
     * Check if the delivery icon for billing address delivery will be available or not
     *
    +     * @param array $packages
     * @return bool
     */
    public function hasSameDayDeliveryIcon($packages)
    {
        foreach($packages as $package) {
            if(!$package->isMobile()) {
                return false;
            }
        }
        return true;
     }

    /**
     * Check if payments can be split
     *
     * @return bool
     */
    public function showSplitPayments() {
        // Show only if package number is greater than one
        $packagesCount = 0;
        foreach( $this->getPackages() as $package ) {
            if( !$package->isPrepaid() ) {
                // Prepaids do not have monthly payments, so we exclude them from counting
                $packagesCount++;
            }
        }
        return $packagesCount > 1;
    }

    /**
     * Check if billing can be split
     *
     * @return bool
     */
    public function showSplitBilling()
    {
        return count($this->getPackages()) > 1;
    }

    /**
     * Check if customer is known in any of the stack found in cart
     *
     * @return bool
     */
    public function customerIsKnownInAnyStackFromCart()
    {
        foreach ($this->getPackages() as $package) {
            if ($this->customerIsKnownInStack($package) && !$package->isPrepaid()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if customer is known in stack
     *
     * @return bool
     */
    public function customerIsKnownInStack( $package ) {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $packageType = strtolower( $package->getType() );
        $stackType = Dyna_Superorder_Helper_Client :: getPackageType( $packageType );
        return $customer->belongsToAccountCategory( $stackType );
    }

    /**
     * Ensures we have array list as variable - SOAP sometimes returns simple array
     *
     * @return array
     */
    public function ensureElementIsArrayList( $elements ) {
        if( is_array( $elements ) && isset( $elements[0] ) && is_array( $elements[0] ) ) {
            return $elements;
        }
        return [$elements];
    }

    /**
     * Check if customer IBAN is known
     *
     * @return bool
     */
    public function customerIBANIsKnown( $checkInStack = 'ANY' ) {
        $linkedCustomers = Mage::getSingleton('customer/session')->getServiceCustomers() ?? array();
        foreach( $linkedCustomers as $legacySystem => $customers ) {
            if( $checkInStack != 'ANY' && $legacySystem != $checkInStack ) continue;
            foreach( $customers as $customer ) {
                if( isset( $customer['billing_account'] ) ) {
                    if( !empty( $customer['billing_account']->getIban() ) ) {
                        return TRUE;
                    }
                }
            }
        }
        return FALSE;
    }

    /**
     * Returns customer bank account number
     *
     * @return string
     */
    public function getCustomerBankAccountNumber( $checkInStack = 'ANY' ) {
        $linkedCustomers = Mage::getSingleton('customer/session')->getServiceCustomers();
        foreach( $linkedCustomers as $legacySystem => $customers ) {
            if( $checkInStack != 'ANY' && $legacySystem != $checkInStack ) continue;
            foreach( $customers as $customer ) {
                if( isset( $customer['billing_account'] ) ) {
                    if( !empty( $customer['billing_account']->getIban() ) ) {
                        return $customer['billing_account']->getIban();
                    }
                }
            }
        }
        return '';
    }

    /**
     * Returns customer bank account numbers (plural)
     *
     * @return array
     */
    public function getCustomerBankAccountNumbers()
    {
        $knownBankAccounts = array();
        $linkedCustomers = Mage::getSingleton('customer/session')->getServiceCustomers() ?? array();
        $availableStacks  = [];

        foreach( $this->getPackages() as $package ) {
            $availableStacks[] = $package->getPackageStack();
        }
        $availableStacks = array_unique($availableStacks);

        foreach( $linkedCustomers as $legacySystem => $customers ) {
            foreach( $customers as $customer ) {
                if( isset( $customer['billing_account'] ) ) {
                    if(!empty( $customer['billing_account']->getIban()) && in_array($legacySystem, $availableStacks)) {
                        $knownBankAccounts[$customer['billing_account']->getIban()] = $legacySystem;
                    }
                }
            }
        }
        return $knownBankAccounts;
    }

    /**
     * Check if customer address is known
     *
     * @return bool
     */
    public function customerAddressIsKnown( $checkInStack = 'ANY' ) {
        $linkedCustomers = Mage::getSingleton('customer/session')->getServiceCustomers() ?? array();
        foreach( $linkedCustomers as $legacySystem => $customers ) {
            if( $checkInStack != 'ANY' && $legacySystem != $checkInStack ) continue;
            foreach( $customers as $customer ) {
                if (isset($customer['contracts'])) {
                    foreach( $customer['contracts'] as $contract ) {
                        foreach( $contract['subscriptions'] as $subscription ) {
                            foreach( $subscription['addresses'] as $address ) {
                                if( $address['address_type'] == 'billing' ) {
                                    return TRUE;
                                }
                            }
                        }
                    }
                }
            }
        }
        return FALSE;
    }

    /**
     * Return customer address
     *
     * @return string
     */
    public function getCustomerKnownAddressString( $checkInStack = 'ANY' ) {
        if( $this->customerAddressIsKnown() ) {
            $linkedCustomers = Mage::getSingleton('customer/session')->getServiceCustomers();
            foreach( $linkedCustomers as $legacySystem => $customers ) {
                if( $checkInStack != 'ANY' && $legacySystem != $checkInStack ) continue;
                foreach( $customers as $customer ) {
                    foreach( $customer['contracts'] as $contract ) {
                        foreach( $contract['subscriptions'] as $subscription ) {
                            foreach( $subscription['addresses'] as $address ) {
                                if( $address['address_type'] == 'billing' ) {
                                    return trim( $address['street'] . ' ' . $address['house_number'] . ' ' . $address['house_addition'] ) . ', ' . $address['postcode'] . ' ' . $address['city'];
                                }
                            }
                        }
                    }
                }
            }
        }
        return '';
    }


    /**
     * Return part of customer address
     *
     * @return string
     */
    public function getCustomerKnownAddressPart( $part, $checkInStack = 'ANY' ) {
        if( $this->customerAddressIsKnown() ) {
            $linkedCustomers = Mage::getSingleton('customer/session')->getServiceCustomers();
            foreach( $linkedCustomers as $legacySystem => $customers ) {
                if( $checkInStack != 'ANY' && $legacySystem != $checkInStack ) continue;
                foreach( $customers as $customer ) {
                    foreach( $customer['contracts'] as $contract ) {
                        foreach( $contract['subscriptions'] as $subscription ) {
                            foreach( $subscription['addresses'] as $address ) {
                                if( $address['address_type'] == 'billing' ) {
                                    if( isset( $address[$part] ) ) {
                                        return trim( $address[$part] );
                                    }
                               }
                            }
                        }
                    }
                }
            }
        }
        return '';
    }


    /**
     * Return customer address
     *
     * @return array
     */
    public function getCustomerKnownAddressArray()
    {
        if ($this->customerAddressIsKnown()) {
            $linkedCustomers = Mage::getSingleton('customer/session')->getServiceCustomers();
            foreach ($linkedCustomers as $customers) {
                foreach ($customers as $customer) {
                    if (isset($customer['contracts'])) {
                        foreach ($customer['contracts'] as $contract) {
                            foreach ($contract['subscriptions'] as $subscription) {
                                foreach ($subscription['addresses'] as $address) {
                                    if ($address['address_type'] == 'billing') {
                                        return $address;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return array();
    }

    /**
     * Check if customer is migrating from Mobile Prepaid to Mobile Postpaid
     *
     * @return bool
     */
    public function customerIsMigratingFromMobilePrepaidToMobilePostpaid() {
        // check if customer is only known in mobile PREPAID (!) stack
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if( !$customer->belongsToAccountCategory( Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_PREPAID ) ) {
            return FALSE;
        }
        // make sure customer is not know to other stacks
        if( $customer->belongsToAccountCategory( Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_POSTPAID  ) ) {
            return FALSE;
        }
        if( $customer->belongsToAccountCategory( Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_FIXED  ) ) {
            return FALSE;
        }
        if( $customer->belongsToAccountCategory( Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_CABLE  ) ) {
            return FALSE;
        }
        // check if there is only mobile postpaid in basket
        foreach( $this->getPackages() as $package ) {
            if( !$package->isMobilePostpaid() ) {
                return FALSE;
            }
        }
        return TRUE;
    }

    public function hasSubsequentDataConfirmationAllowed($package) {
        return !$package->isDSL();
    }

    /**
     * Check is mobile
     *
     * @return bool
     */
    public function getIsMobile()
    {
        return $this->isMobile;
    }

    /**
     * Check is mobile prepaid
     *
     * @return bool
     */
    public function getIsMobilePrepaid()
    {
        return $this->isMobilePrepaid;
    }

    /**
     * Check is mobile postpaid
     *
     * @return bool
     */
    public function getIsPrivateMobilePostpaid()
    {
        return $this->isPrivateMobilePostpaid;
    }

    /**
     * Check is mobile prepaid
     *
     * @return bool
     */
    public function getIsPrivateMobilePrepaid()
    {
        return $this->isPrivateMobilePrepaid;
    }

    /**
     * Check is cable
     *
     * @return bool
     */
    public function getIsCable()
    {
        return $this->isCable;
    }

    /**
     * Check is fixedNet
     *
     * @return bool
     */
    public function getIsFixed()
    {
        return $this->isFixed;
    }

    /**
     * Check is DSL
     *
     * @return bool
     */
    public function getIsDSL()
    {
        return $this->isDSL;
    }

    /**
     * Get name of lost products and build an HTML output
     * (ILS scenarios only)
     * @return string
     */
    public function getLostProducts($packages = array())
    {
        $lostProducts = array();
        $htmlOutput = '';

        $stacks = array(
            'Mobilfunk' => array('mobile','prepaid'),
            'DSL/LTE' => array('dsl','lte'),
            'Kabel' => array('cable_internet_phone','cable_tv','cable_independent')
        );

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        $packages = !empty($packages) ? $packages : $quote->getPackages();
        // Get alternate items / lost products (just on ILS flow)
        foreach ($packages as $package){
            $alternateItems = $quote->getAlternateItems($package['package_id']);
            foreach ($alternateItems as $alternateItem) {
                $lostProducts[$alternateItem->getPackageType()][$alternateItem->getItemId()] = '<p>&middot; ' . $alternateItem->getName() . '</p>';
            }
        }
        // build html output
        if(!empty($lostProducts)){
            $htmlOutput = '<h3>Folgende Services nutzen Sie dann nicht mehr:</h3><br/>';
            $currentStackName = '';
            foreach($lostProducts as $stack => $item) {
                foreach($item as $itemName) {
                    foreach($stacks as $stackName => $stackTypes) {
                        if(in_array($stack,$stackTypes) && ($currentStackName != $stackName)){
                            // add just one heading per stack
                            $htmlOutput .= '<h4>' . $stackName . ':</h4>';
                            $currentStackName = $stackName;
                        }
                    }
                    $htmlOutput .= $itemName;
                }
            }
            $htmlOutput .= '<br/><hr class="footer-separator"/><br/>';
        }

        return $htmlOutput;
    }

    /**
     * Get unique base customer identifier for call/offer/info summary
     * @param $package
     * @return string
     */
    public function getUniqueBaseIdentifier($package)
    {
        if (in_array($package->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts())) {
            if ($package->isCable()|| $package->isFixed()) {
                $packageAddress = $package->getServiceAddressData();
                $uniqueBaseCustomerIdentifier = $this->__('Service address') . ': ' . $packageAddress['street'] . ', ' .
                    $packageAddress['houseno'] . ', ' . $packageAddress['postcode'] . ', ' . $packageAddress['city'];
            } else {
                $uniqueBaseCustomerIdentifier = $this->__('Phone number') . ': ' . $package->getCtn();
            }
            return $uniqueBaseCustomerIdentifier;
        }
        return false;
    }

    /**
     * Determine if the flow is move offnet only (per package or per order
     * @param array|null $package
     * @return bool
     */
    public function isMoveOffnetOnly( $package = null ) {
        $packagesToCheck = array();
        if( is_null( $package ) ) {
            $packagesToCheck = $this->getPackages();
        } else {
            $packagesToCheck[] = $package;
        }
        $globalMoveOffnetState = Mage::getSingleton('checkout/cart')->getCheckoutSession()->getInMigrationOrMoveOffnetState();
        foreach( $packagesToCheck as $package ) {
            if( !$package->isDsl() || $package->getSaleType() != 'MIGCOC' || $globalMoveOffnetState != Dyna_Catalog_Model_Type::MOVE_OFFNET ) {
                return FALSE;
            }
        }
        return TRUE;
    }


    /**
     * Determine if the use service address - delivery option should be available or not
     * @param array $packages
     * @return bool
     */
    public function hasServiceAddressVisible($packages)
    {
        foreach($packages as $package) {
            if(!$this->isFieldAvailableForSpecificPackages('delivery[method][service]', array($package))) {
                return false;
            }
        }
        foreach($packages as $package) {
            if($this->hasContractlessItem($package)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine if the use billing address - delivery option should be available or not
     * @param array $packages
     * @return bool
     */
    public function hasBillingAddressVisible($packages)
    {
        foreach($packages as $package) {
            if (!$this->isFieldAvailableForSpecificPackages('delivery[method][deliver]', array($package))) {
                return false;
            }
        }

        foreach($packages as $package) {
            $packageContext = strtoupper($package->getSaleType());
            if(in_array($packageContext, array('ILSBLU', 'RETNOBLU', 'RETBLU', 'RETTER', 'ACQ'))) {

                if($this->hasContractlessItem($package) && $package->isMobile()) {
                    return true;
                }

                if($package->isDsl() && $packageContext === 'ACQ') {
                    return true;
                }
                if(($package->isCable() || $package->isDsl()) && $packageContext === 'ACQ') {
                    return true;
                }
            }

            if($packageContext === 'MIGCOC' && $package->isMobile()) {
                return true;
            }

            if(in_array($packageContext, array('ILSBLU', 'ILSNOBLU', 'MIGCOC'))
                && ($package->isCable() || $package->isDsl())
                && $this->hasContractlessItem($package)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine if the use different address - delivery option should be available or not
     * @param array $packages
     * @return bool
     */
    public function hasDifferentAddressVisible($packages)
    {
        foreach($packages as $package) {
            if (!$this->isFieldAvailableForSpecificPackages('delivery[method][other_address]', array($package))) {
                return false;
            }
        }

        foreach($packages as $package) {
            $packageContext = strtoupper($package->getSaleType());

            if(in_array($packageContext, array('ILSBLU', 'RETNOBLU', 'RETBLU', 'RETTER', 'ACQ'))) {

                if($this->hasContractlessItem($package) && $package->isMobile()) {
                    return true;

                }
                if(($package->isCable() || $package->isDsl()) && $packageContext === 'ACQ') {
                    return true;
                }
            }

            if($packageContext === 'MIGCOC' && $package->isMobile()) {
                return true;
            }

            if(in_array($packageContext, array('ILSBLU', 'ILSNOBLU', 'MIGCOC'))
                && ($package->isCable() || $package->isDsl())
                && $this->hasContractlessItem($package)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine if the deliver to vf shop - delivery option should be available or not
     * @param array $packages
     * @param bool $convergedOptions
     * @return bool
     */
    public function hasDeliverToVFShopVisible($packages, $convergedOptions = false)
    {
        if($convergedOptions && $this->showSplitDelivery()) {
            return false;
        }
        foreach($packages as $package) {
            if (!$this->isFieldAvailableForSpecificPackages('delivery[method][deliver]', array($package))) {
                return false;
            }
        }

        foreach($packages as $package) {
            $packageContext = strtoupper($package->getSaleType());

            if(in_array($packageContext, array('ILSBLU', 'RETNOBLU', 'RETBLU', 'RETTER', 'ACQ'))
                && $this->hasContractlessItem($package)
                && $package->isMobile()) {
                return true;
            }

            if($packageContext === 'MIGCOC' && $package->isMobile()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine if the same day delivery options should be displayed or not
     * @param array $packages
     * @return bool
     */
    public function hasDeliverySameDayVisible($packages)
    {
        foreach($packages as $package) {
            if (!$this->isFieldAvailableForSpecificPackages('delivery[same_day]', array($package))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Splits the date string on spaces and translates each word
     *
     * @param $date
     * @return string
     */
    public function translateDate($date)
    {
        $translation = '';

        foreach (explode(" ", $date) as $part) {
            $translation .= $this->__($part) . ' ';
        }

        return $translation;
    }

    /**
     * Determine if the delivery on a certain date will be available
     * @param $packages
     * @return bool
     */
    public function hasDeliveryOnACertainDayVisible($packages)
    {
        foreach($packages as $package) {
            if (!$this->isFieldAvailableForSpecificPackages('delivery[date]', array($package))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Determine if the package contains an item which was added from installed base
     * @param $package
     * @return bool
     */
    public function hasContractlessItem($package)
    {
        foreach(Dyna_Catalog_Model_Type::$devices as $hardwareSubtype) {
            foreach($package->getItems($hardwareSubtype) as $deviceItem) {
                if (!$deviceItem->isContract()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param array $packages
     * @return int $count
     */
    protected function countContractlessItems($packages)
    {
        $count = 0;
        foreach($packages as $package) {
            foreach(Dyna_Catalog_Model_Type::$devices as $hardwareSubtype) {
                $found = false;
                foreach($package->getItems($hardwareSubtype) as $deviceItem) {
                    if (!$deviceItem->isContract()) {
                        $found = true;
                        break;
                    }
                }
                if($found) {
                    $count++;
                    break;
                }
            }
        }
        return $count;
    }

    /**
     * Determine if the delivery section will be visible at all
     * @return bool
     */
    public function isDeliveryOptionsSectionAvailable()
    {
        $packages = $this->getPackages();
        return $this->hasBillingAddressVisible($packages) || $this->hasDifferentAddressVisible($packages) || $this->hasDeliverToVFShopVisible($packages);
    }

    /**
     * Determine if the package contains ILS sale type
     *
     * @param $packages
     * @return bool
     */
    public function hasIls($packages)
    {
        foreach ($packages as $package) {
            if (strtolower($package->getSaleType()) == 'ils') {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine if the package contains ILS sale type
     *
     * @param object $product
     * @param string $defaultText
     * @param string $addToProductNameText
     * @return string
     */
    public function getHeaderNameWithConfigurableProducts( $product, $defaultText = '', $addToProductNameText = '' ) {
        if( isset( $product['display_label_checkout'] ) ){
            echo $this->__($product['display_label_checkout']) . ' ' . ($product['price'] != '0' ? Mage::helper('dyna_checkout')->showNewFormattedPriced($product['price']) : '') . $addToProductNameText;
        }
        echo $this->__($defaultText);
    }

    /**
     * Return sale type of the order
     *
     * @return string
     */
    public function getOrderSaleType() {
        return $this->getPackages()[0]->getSaleType();
    }

    /**
     * Return package type of the order
     *
     * @return string
     */
    public function getOrderPackageType() {
        return $this->getPackages()[0]->getPackageStack();
    }

    /**
     * Determine if order has multiple stacks
     *
     * @return bool
     */
    public function isConvergedOrder() {
            $packagesTypes = array();
            foreach( $this->getPackages() as $package ) {
                $packageType = strtolower( $package->getType() );
                $stackType = Dyna_Superorder_Helper_Client :: getPackageType( $packageType );
                $packagesTypes[ $stackType ] = TRUE;
            }
            return count( $packagesTypes ) > 1;
    }

    /**
     * Determine Template for remarks
     *
     * @return array
     */
    public function getNoteTypesArray()
    {
        $configurationData = Mage::getModel('dyna_configurator/configuration')
            ->getConfigurationData();

        $notesData = $configurationData->getComments();

        usort($notesData, function ($a, $b) {
            return $a['sort'] > $b['sort'];
        });

        return $notesData;
    }

    /**
     * Determine pakagetypes and retirn the pakage type
     *
     * @return array
     */
    public function getPackageTypeArray()
    {
        $packagesTypes = array();
        foreach ($this->getPackages() as $package) {
            $packageType = strtolower($package->getType());
            $packagesTypes[$packageType] = true;
        }
        return $packagesTypes;
    }

    /**
     * Determine if per-order payment options can be visible
     *
     * @param object $package
     * @return bool
     */
    public function allowPerOrderPaymentOptions( $checkPerPackageFunction ) {
        if( $this->showSplitPayments() ) {
            // If split payments available, check if every package in order has this option available
            foreach( $this->getPackages() as $package ) {
                if( !$this->{$checkPerPackageFunction}( $package ) ) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    /**
     * Determine if per-order billing options can be visible
     *
     * @param object $package
     * @return bool
     */
    public function allowPerOrderBillingOptions( $checkPerPackageFunction ) {
        if( $this->showSplitBilling() ) {
            // If split payments available, check if every package in order has this option available
            foreach( $this->getPackages() as $package ) {
                if( !$this->{$checkPerPackageFunction}( $package ) ) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    /**
     * Determine if the monthly reuse iban should be shown
     *
     * @param object $package
     * @return bool
     */
    public function showMonthlyPaymentsReuseIBAN( $package = NULL, $stackType = NULL ) {
        if( is_null( $stackType ) ) {
            if( is_null( $package ) ) {
                if( !$this->allowPerOrderPaymentOptions( 'showMonthlyPaymentsReuseIBAN' ) ) {
                    return FALSE;
                }
                $stackType = 'ANY';
            } else {
                $stackType = $package->getPackageStack();
            }
        }
        if( !$this->customerIBANIsKnown( $stackType ) ) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Determine if the monthly transfer should be shown
     *
     * @param object $package
     * @return bool
     */
    public function showMonthlyPaymentsByBankTransfer( $package = NULL ) {
        if( is_null( $package ) ) {
            if( !$this->allowPerOrderPaymentOptions( 'showMonthlyPaymentsByBankTransfer' ) ) {
                return FALSE;
            }
            $saleType = $this->getOrderSaleType();
            $stackType = $this->getOrderPackageType();
        } else {
            $saleType = $package->getSaleType();
            $stackType = $package->getPackageStack();
        }
        switch( $saleType ) {
            case "ACQ":
            case "ILSBLU":
            case "ILSNOBLU":
            case "RETNOBLU":
            case "MIGCOC":
                if( $this->customerIBANIsKnown() ) {
                    return FALSE;
                }
                break;
        }
        return TRUE;
    }

    /**
     * Determine if the monthly transfer for another account should be shown
     *
     * @param object $package
     * @return bool
     */
    public function showMonthlyPaymentsByDifferentBankTransfer( $package = NULL ) {
        if( $this->showMonthlyPaymentsByBankTransfer() ) {
            return FALSE;
        }
        if( is_null( $package ) ) {
            if( !$this->allowPerOrderPaymentOptions( 'showMonthlyPaymentsByDifferentBankTransfer' ) ) {
                return FALSE;
            }
            $saleType = $this->getOrderSaleType();
            $stackType = $this->getOrderPackageType();
            $package = $this->getPackages()[0];
        } else {
            $saleType = $package->getSaleType();
            $stackType = $package->getPackageStack();
        }
        switch( $saleType ) {
            case "ACQ":
            case "ILSBLU":
            case "ILSNOBLU":
            case "RETNOBLU":
                if( $this->customerIBANIsKnown( $stackType ) ) {
                    return FALSE;
                }
            break;
            case "MIGCOC":
                if( $this->customerIBANIsKnown( $stackType ) ) {
                    if( !$this->getIsMobile() ) {
                        return FALSE;
                    }
                    if( !$package->isPrePost() || $package->getParentAccountNumber() != Dyna_Customer_Helper_Customer::PRE_POST_BAN ) {
                        return FALSE;
                    }
                }
                break;
        }
        return TRUE;
    }

    /**
     * Determine if the monthly invoice option should be shown
     *
     * @param object $package
     * @return bool
     */
    public function showMonthlyPaymentsByInvoice( $package = NULL ) {
        if( is_null( $package ) ) {
            if( !$this->allowPerOrderPaymentOptions( 'showMonthlyPaymentsByInvoice' ) ) {
                return FALSE;
            }
            $saleType = $this->getOrderSaleType();
            $stackType = $this->getOrderPackageType();
        } else {
            $saleType = $package->getSaleType();
            $stackType = $package->getPackageStack();
        }
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if( !$customer->getIsSoho() ) {
            return FALSE;
        }
        switch( $saleType ) {
            case "ILSBLU":
            case "RETNOBLU":
            case "RETBLU":
            case "RETTER":
                if( !$this->getIsMobile() ) {
                    return FALSE;
                }
                break;
            case "MIGCOC":
                if( $this->customerIBANIsKnown( $stackType ) ) {
                    if( !$this->getIsMobile() ) {
                        return FALSE;
                    }
                    if( !$package->isPrePost() || $package->getParentAccountNumber() != Dyna_Customer_Helper_Customer::PRE_POST_BAN ) {
                        return FALSE;
                    }
                }
                break;
        }
        return TRUE;
    }

    /**
     * Determine if onetime payments COD should be shown
     *
     * @return bool
     */
    public function showOnetimePaymentsByCOD( $hasSimOnly, $totalOTCOnPrepaidPackage )
    {
        if (!$this->isAvailableSection('payments_onetime_cod')) {
            return false;
        }
        if (!($hasSimOnly
                || (!$hasSimOnly && $totalOTCOnPrepaidPackage > Dyna_Checkout_Block_Cart_Steps_SavePayments::MAXIMUM_PRICE_FOR_DIRECT_DEBIT))
            && $this->getIsMobilePrepaid()
        ) {
            return false;
        }

        $saleType = $this->getOrderSaleType();
        switch ($saleType) {
            case "ILSNOBLU":
            case "ILSBLU":
            case "RETNOBLU":
            case "RETBLU":
            case "RETTER":
                return false;
            default:
                return true;
        }
    }

    /**
     * Determine if onetime payments by bank transfer should be shown
     *
     * @return bool
     */
    public function showOnetimePaymentsByBankTransfer( $hasSimOnly, $totalOTCOnPrepaidPackage ) {
        if( !$this->isAvailableSection( 'payments_onetime_bank_transfer' ) ) {
            return FALSE;
        }
        if( !( !$hasSimOnly && $totalOTCOnPrepaidPackage <= Dyna_Checkout_Block_Cart_Steps_SavePayments::MAXIMUM_PRICE_FOR_DIRECT_DEBIT ) ) {
            if( $this->getIsMobilePrepaid() ) {
                return FALSE;
            }
        }
        $saleType = $this->getOrderSaleType();
        switch( $saleType ) {
            case "ACQ":
            case "ILSBLU":
            case "RETNOBLU":
            case "RETBLU":
            case "RETTER":
            case "MIGCOC":
                if( !$this->getIsMobile() ) {
                    return FALSE;
                }
                break;
        }
        return TRUE;
    }

    /**
     * Determine if onetime payments by invoice should be shown
     *
     * @return bool
     */
    public function showOnetimePaymentsByInvoice( $hasSimOnly, $totalOTCOnPrepaidPackage ) {
        if( !$this->isAvailableSection( 'payments_onetime_invoice' ) ) {
            return FALSE;
        }
        if( !( !$hasSimOnly && $totalOTCOnPrepaidPackage <= Dyna_Checkout_Block_Cart_Steps_SavePayments::MAXIMUM_PRICE_FOR_DIRECT_DEBIT ) ) {
            if( $this->getIsMobilePrepaid() ) {
                return FALSE;
            }
        }
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if( !$customer->getIsSoho() ) {
            return FALSE;
        }
        $saleType = $this->getOrderSaleType();
        switch( $saleType ) {
            case "ACQ":
            case "ILSBLU":
            case "RETNOBLU":
            case "RETBLU":
            case "RETTER":
            case "MIGCOC":
                if( !$this->getIsMobile() ) {
                    return FALSE;
                }
                break;
        }
        return TRUE;
    }

    /**
     * Determine if billing 'known address' option should be available
     *
     * @return bool
     */
    public function showBillingKnownAddress( $package = NULL ) {
        if( is_null( $package ) ) {
            if( !$this->allowPerOrderBillingOptions( 'showBillingKnownAddress' ) ) {
                return FALSE;
            }
            $saleType = $this->getOrderSaleType();
            $stackType = $this->getOrderPackageType();
        } else {
            $saleType = $package->getSaleType();
            $stackType = $package->getPackageStack();
        }
        if( !$this->customerAddressIsKnown( $stackType ) ) {
            return FALSE;
        }
        switch( $saleType ) {
            case "ILSNOBLU":
                if( !$this->getIsDSL() ) {
                    return FALSE;
                }
                break;
            case "ILSBLU":
                if( !$this->getIsMobile() && !$this->getIsCable() ) {
                    return FALSE;
                }
                break;
            case "RETNOBLU":
            case "RETBLU":
            case "RETTER":
                if( !$this->getIsMobile() ) {
                    return FALSE;
                }
                break;
        }
        return TRUE;
    }

    /**
     * Determine if billing 'customer address' option should be available
     *
     * @return bool
     */
    public function showBillingCustomerAddress( $package = NULL ) {
        if( is_null( $package ) ) {
            if( !$this->allowPerOrderBillingOptions( 'showBillingCustomerAddress' ) ) {
                return FALSE;
            }
            $saleType = $this->getOrderSaleType();
            $stackType = $this->getOrderPackageType();
        } else {
            $saleType = $package->getSaleType();
            $stackType = $package->getPackageStack();
        }
        if( $this->showBillingKnownAddress( $package ) ) {
            if( in_array( $saleType, array( 'ILSBLU', 'ILSNOBLU', 'RETNOBLU', 'RETBLU', 'RETTER', 'MIGCOC' ) ) ) {
                return FALSE;
            }
        }
        if( !$this->showBillingKnownAddress( $package ) ) {
            return TRUE;
        }
        switch( $saleType ) {
            case "ILSNOBLU":
                if( $this->getIsDSL() ) {
                    return FALSE;
                }
                break;
            case "ILSBLU":
                if( $this->getIsMobile() ) {
                    return FALSE;
                }
                if(  $this->getIsCable() ) {
                    return FALSE;
                }
                break;
            case "RETNOBLU":
            case "RETBLU":
            case "RETTER":
                if( $this->getIsMobile() ) {
                    return FALSE;
                }
                break;
        }
        return TRUE;
    }

    /**
     * Determine if billing 'use other address' option should be available
     *
     * @return bool
     */
    public function showBillingOtherAddress( $package = NULL ) {
        if( is_null( $package ) ) {
            if( !$this->allowPerOrderBillingOptions( 'showBillingOtherAddress' ) ) {
                return FALSE;
            }
            $saleType = $this->getOrderSaleType();
            $stackType = $this->getOrderPackageType();
        } else {
            $saleType = $package->getSaleType();
            $stackType = $package->getPackageStack();
        }
        if( $this->showBillingKnownAddress( $package ) ) {
            if( in_array( $saleType, array( 'ILSBLU', 'ILSNOBLU', 'RETNOBLU', 'RETBLU', 'RETTER', 'MIGCOC' ) ) ) {
                return FALSE;
            }
        }
        if( !$this->showBillingKnownAddress( $package ) ) {
            return TRUE;
        }
        switch( $saleType ) {
            case "ILSNOBLU":
                if( $this->getIsDSL() ) {
                    return FALSE;
                }
                break;
            case "ILSBLU":
                if( $this->getIsCable() ) {
                    return FALSE;
                }
                if( $this->getIsMobile() ) {
                    return FALSE;
                }
                break;
            case "RETNOBLU":
            case "RETBLU":
            case "RETTER":
                if( !$this->getIsMobile() ) {
                    return FALSE;
                }
                break;
        }
        return TRUE;
    }

    /**
     * Determine if billing 'po box' option should be available
     *
     * @return bool
     */
    public function showBillingPOBox( $package = NULL ) {
        if( !$this->isAvailableSection( 'billing_po_box', $package ) ) {
            return FALSE;
        }
        if( $this->isAvailableSection( 'billing_po_box_disabled', $package ) ) {
            return FALSE;
        }
        if( is_null( $package ) ) {
            if( !$this->allowPerOrderBillingOptions( 'showBillingPOBox' ) ) {
                return FALSE;
            }
            $saleType = $this->getOrderSaleType();
            $stackType = $this->getOrderPackageType();
        } else {
            $saleType = $package->getSaleType();
            $stackType = $package->getPackageStack();
        }
        if( $this->showBillingKnownAddress( $package ) ) {
            if( in_array( $saleType, array( 'ILSBLU', 'ILSNOBLU', 'RETNOBLU', 'RETBLU', 'RETTER', 'MIGCOC' ) ) ) {
                return FALSE;
            }
        }
        if( !$this->showBillingKnownAddress( $package ) ) {
            return TRUE;
        }
        switch( $saleType ) {
            case "ILSBLU":
            case "RETNOBLU":
            case "RETBLU":
            case "RETTER":
                if( $this->getIsMobile() ) {
                    return FALSE;
                }
                break;
        }
        return TRUE;
    }

    /**
     * Determine if billing type select should be enabled
     *
     * @return bool
     */
    public function isEnabledBillingType() {
        $saleType = $this->getOrderSaleType();
        switch( $saleType ) {
            case "ILSNOBLU":
                if( $this->getIsDSL() ) {
                    return FALSE;
                }
                break;
            case "ILSBLU":
                if( $this->getIsMobile() ) {
                    return FALSE;
                }
                if( $this->getIsCable() ) {
                    return FALSE;
                }
                break;
            case "RETNOBLU":
            case "RETBLU":
            case "RETTER":
                if( $this->getIsMobile() ) {
                    return FALSE;
                }
                break;
        }
        return TRUE;
    }


    /**
     * Determine the default delivery field
     *
     * @param string $option
     * @param array $packages
     * @return bool
     */
    public function isDefaultDeliveryOption($option, $packages)
    {
        if($this->hasServiceAddressVisible($packages)) {
            return $option == 'service';
        }
        if($this->hasBillingAddressVisible($packages)) {
            return $option == 'deliver';
        }

        $convergedOptions = (count($packages) != 1);
        if($this->hasDeliverToVFShopVisible($packages, $convergedOptions)) {
            return $option == 'pickup';
        }
        if($this->hasDifferentAddressVisible($packages)) {
            return $option == 'other_address';
        }
        return false;
    }

    /**
     * @return string
     */
    public function getSameDayDeliveryDateString()
    {
        return date('l')." ".strtolower(date('F'))." ".date('d')." ".date('Y');
    }

    public function isSaveChangeProviderAvailable()
    {
        $packages = $this->getPackages();
        $package = null;
        if (count($packages) == 1) {
            $package = $packages[0];
            $saleType = $package->getSaleType();

            if ($package->isDsl()) {
                if ($saleType == Dyna_Catalog_Model_ProcessContext::ILSBLU) {
                    return false;
                }
            } elseif ($package->isMobile()) {
                $hasDisabledDiscount = false;
                $hasStudentDiscount = false;
                $packageHasYoung = $package->hasYoungItems();

                $items = $package['items'];
                foreach ($items as $item) {
                    if (strpos($item->getData()['sku'], Dyna_Catalog_Model_Type::getMobileStudentDiscount()) !== false) {
                        $hasStudentDiscount = true;
                    } if (strpos($item->getData()['sku'], Dyna_Catalog_Model_Type::getMobileDisabledDiscount()) !== false) {
                        $hasDisabledDiscount = true;
                    }
                }



                if (in_array($saleType, [Dyna_Catalog_Model_ProcessContext::ILSBLU, Dyna_Catalog_Model_ProcessContext::RETBLU, Dyna_Catalog_Model_ProcessContext::RETNOBLU, Dyna_Catalog_Model_ProcessContext::RETTER])) {
                    if (($hasStudentDiscount || $hasDisabledDiscount || ($packageHasYoung && $this->isFieldAvailable('provider[subuser_dob]', $package))) && $this->isAvailableSection('other_discounts_and_phonebook', $package)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param bool $hasFixedTypePackage
     * @param bool $hasCableOrFixedTypePackage
     * @return bool
     */
    public function isCustomerPrivateAddressAvailable($hasFixedTypePackage, $hasCableOrFixedTypePackage)
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $isSohoCustomer = $customer->getIsSoho();
        return($this->isFieldAvailable('customer[c1-3]', null, intval($isSohoCustomer), intval($customer->getCommercialRegister()))
            && (
            (
                $isSohoCustomer && (($customer->getCommercialRegister() && !$hasCableOrFixedTypePackage)
                    || (!$customer->getCommercialRegister() && ($hasFixedTypePackage || !$hasCableOrFixedTypePackage))
                )
            )
            || (!$isSohoCustomer)
            )
        );
    }

    /**
     * @param bool $hasCableOrFixedTypePackage
     * @return bool
     */
    public function isCustomerSohoAddressAvailable($hasCableOrFixedTypePackage)
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $isSohoCustomer = $customer->getIsSoho();

        return $this->isFieldAvailable('customer[c1-4]', null, intval($isSohoCustomer), intval($customer->getCommercialRegister())) && ($hasCableOrFixedTypePackage);
    }

    /**
     * @param Dyna_Package_Model_Package $package
     * @return bool
     */
    public function isPackageAvailableInNewCunnection($package)
    {
        return !$package->isFixed() || ($package->isFixed() && $package->getSaleType() !== "ILSBLU"
                || !(empty(Mage::helper("dyna_checkout/fields")->getFieldValue("other[single_connection][dsl]"))
                    && empty(Mage::getSingleton('customer/session')->getCustomer()->getBillingAccount()->getBillingType())));
    }

    /**
     * @return bool
     */
    public function customerPasswordIsKnownForAllPresentStacks() {
        $systemsToCheck = array();
        $packagesInCart = $this->getPackages();
        foreach( $packagesInCart as $package ) {
            switch( TRUE ) {
                case $package->isMobile():
                    $systemsToCheck[] = Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS;
                    break;
                case $package->isFixed():
                    $systemsToCheck[] = Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN;
                    break;
            }
        }
        $systemsToCheck = array_unique( $systemsToCheck );
        $systemsToCheck = array_flip( $systemsToCheck );
        if( count( $systemsToCheck ) > 0 ) {
            $linkedCustomers = Mage::getSingleton('customer/session')->getServiceCustomers() ?? array();
            foreach( $linkedCustomers as $legacySystem => $customers ) {
                if( !isset( $systemsToCheck[$legacySystem] ) ) continue;
                foreach( $customers as $customer ) {
                    if( !empty( $customer['customer_password'] ) ) {
                        unset( $systemsToCheck[$legacySystem] );
                    }
                }
            }
        }
        return count( $systemsToCheck ) == 0;
    }



}
