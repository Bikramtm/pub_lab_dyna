<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_Checkout_Helper_Delivery extends Mage_Core_Helper_Abstract
{
    CONST DELIVERY_UNKNOWN = 'unknown-delivery';

    // Setting addresses on current instance after iterating through all package deliveries
    // Each other_address selected, will be saved on the other_address key and later compared against another package address
    protected $addresses = array();

    /**
     * Update mobile shipping fee based on delivery agent has chosen in checkout
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @return boolean
     */
    public function calculateShippingFee(Dyna_Checkout_Model_Sales_Quote $quote)
    {
        // Do nothing if quote has no mobile packages or no shipping data is set on quote
        if (!$quote->hasMobilePackage() || !$shippingData = $quote->getShippingData()) {
            return false;
        }

        $shippingData = json_decode($shippingData, true);
        /** @var $mapperAddonModel Dyna_MultiMapper_Model_Addon */
        $mapperAddonModel = Mage::getModel("dyna_multimapper/addon");
        /** @var Dyna_Checkout_Model_Client_ShippingFeeCalculateClient $client */
        $client = Mage::getModel("dyna_checkout/client_shippingFeeCalculateClient");
        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        $doCartSave = false;

        // Remove shipping product from the packages as we'll add it based on the delivery selected
        $this->removeShippingFeeProduct();

        foreach ($quote->getCartPackages(true) as $package) {
            if (!$package->isMobile()) {
                continue;
            }

            // Get delivery data for this package
            $deliveryData = $this->getDeliveryData($shippingData, $package->getPackageId());

            $chargeDeliveryFee = $this->chargeDeliveryFee($quote, $shippingData, $deliveryData['address']);

            $item = null;

            if($chargeDeliveryFee != $package->getPackageId()) {

                Mage::unregister(Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU);
                Mage::register(Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU, 0);

                $product = Mage::getModel('catalog/product')->getShippingFeeProduct();

                // Add product will trigger the add product after event which will set the appropriate fee price
                $item = $cart->addProduct($product->getId());
                $item->setPackageId($package->getPackageId());
                $doCartSave = true;
                continue;
            }

            $packageSkus = array_keys($package->getAllProductIs());
            $hasSAPAddons = $mapperAddonModel->quoteSkuMappersHaveSAPAddons($packageSkus, $package->getSaleType());

            if ($hasSAPAddons > 0 || $package->hasSimOnly()) {
                // Get shipping fee price return by services
                $price = $client->getShippingFee(array('deliveryType' => $deliveryData['address'] == 'store' ? 'pickup' : 'address'));

                Mage::unregister(Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU);
                Mage::register(Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU, $price);

                // If item already present in cart, check if it's price differs from the one returned by service call
                if (!$item || ($item && is_numeric($price) && (float)$item->getPriceInclTax() !== round((float)$price, 2))) {
                    $product = Mage::getModel('catalog/product')->getShippingFeeProduct();
                    // Remove shipping item if exits for it to be added again
                    !$item ?: $cart->removeItem($item->getId());
                    // Add product will trigger the add product after event which will set the appropriate fee price
                    $item = $cart->addProduct($product->getId());
                    $item->setPackageId($package->getPackageId());
                    $item->setPackageType($package->getType());
                    $doCartSave = true;
                }
            }
        }

        return $doCartSave;
    }

    /**
     * Extract deliver
     * @param $quoteShippingData
     * @param $packageId
     * @return string
     */
    public function getDeliveryData($quoteShippingData, $packageId)
    {
        $deliveryAddress = array();
        // Check if shipping data has the pakket node which states that this is split delivery
        if (isset($quoteShippingData['pakket'])) {
            // split delivery
            foreach ($quoteShippingData['pakket'] as $addressPackageId => $address) {
                if ($packageId == $addressPackageId) {
                    $deliveryAddress = $address['address'];
                }
            }
        } else {
            // all in one place delivery
            $deliveryAddress = $quoteShippingData['deliver']['address'] ?? array();
        }

        return $deliveryAddress;
    }

    /**
     * Returns cart package id on which shipping fee will be added
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @param $shippingData
     * @param $address_type
     * @return array
     */
    protected function chargeDeliveryFee(Dyna_Checkout_Model_Sales_Quote $quote, $shippingData, $address_type) {
        $chargePackageId = "";
        foreach ($quote->getCartPackages(true) as $package) {
            if($package->isMobile()) {
                $deliveryData = $this->getDeliveryData($shippingData, $package->getPackageId());
                if ($deliveryData['address'] == 'store' && $address_type == 'store') {
                    $chargePackageId = $package->getPackageId();
                }else if($deliveryData['address'] != 'store' && $address_type != 'store') {
                    $chargePackageId = $package->getPackageId();
                }
            }
        }
        return $chargePackageId;
    }

    /**
     * Remove shipping fee product from cart
     */
    public function removeShippingFeeProduct() {
        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $cart->getQuote();
        $doCartSave = false;

        foreach ($quote->getAllItems() as $item) {
            if(strtoupper($item->getSku()) == strtoupper(Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU)) {
                $item->isDeleted(true);
                $item->save();
                $doCartSave = true;
            }
        }
        if ($doCartSave) {
            $cart->save();
        }
    }
}
