<?php

/**
 * Class Dyna_Checkout_Helper_Data
 */
class Dyna_Checkout_Helper_Data extends Omnius_Checkout_Helper_Data
{
    const UCT_PROGRESS_LOG_FILE = 'uct_progress.log';
    const TAG_IS_PHONE_ONLY = "isPhoneOnly";
    const TAG_IS_INTERNET_ONLY = "isInternetOnly";

    public $salesId = false;

    private $billingOnlineProduct = null;
    private $billingPaperProduct = null;

    /**
     * Set sales id on current instance (will not change doing requests as changing it will hit another action)
     * @param $value
     * @return $this
     */
    public function setSalesId($value)
    {
        $this->salesId = $value;

        return $this;
    }

    /**
     * @param null $productsIds
     * @param null $websiteId
     * @param null $salesType
     * @return array
     */
    public function getUpdatedPrices($productsIds = null, $websiteId = null, $salesType = null, $packageType = null)
    {
        /** @var Mage_Tax_Helper_Data $_taxHelper */
        $_taxHelper = Mage::helper('tax');
        $_store = Mage::app()->getStore();

        //if website is provided, get the store of the website
        if ($websiteId) {
            $_store = Mage::app()->getWebsite($websiteId)->getDefaultStore();
        }

        $types = [];
        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
        $allMixMatchTypes = array_merge(Dyna_Catalog_Model_Type::$subscriptions, Dyna_Catalog_Model_Type::$devices, Dyna_Catalog_Model_Type::$options);

        foreach ($allMixMatchTypes as $mixMatchType) {
            $types[] = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, $mixMatchType);
        }

        // Filtering by package type and not subtype, because mixmatches can be made between subscription and device/addons
        /** @var Dyna_Catalog_Model_Resource_Product_Collection $productsCollection */
        $cacheKey = $websiteId ? "ALL_MIXMATCH_PRODUCTS_" . $websiteId : "ALL_MIXMATCH_PRODUCTS";
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        if (!$collectionSerialised = $cache->load($cacheKey)) {
            $productsCollection = Mage::getResourceModel('catalog/product_collection');
            $productsCollection
                ->addAttributeToFilter('entity_id', ['in' => Mage::getResourceModel('catalog/product_collection')->getPackageTypeProductIds($packageType)])
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, ['in' => [$types]])
                ->addWebsiteFilter($websiteId)
                ->addAttributeToSelect('price')
                ->addAttributeToSelect('maf');

            if (in_array($packageType, Dyna_Catalog_Model_Type::getCablePackages())) {
                $productsCollection->addAttributeToSelect('display_price');
            }
            $productsCollection->addAttributeToSelect('tax_class_id')
                ->addTaxPercents();

            $products = new Varien_Data_Collection();
            foreach ($productsCollection as $product) {
                $products->addItem($product);
            }
            $cache->save(serialize($products), $cacheKey, array($cache::PRODUCT_TAG), $cache->getTtl());
        } else {
            $products = unserialize($collectionSerialised);
        }

        /**
         * We must extract the subscription from the package
         * with the provided package ID
         */
        $subscription = null;
        $deviceRCSku = null;
        $device = [];
        $option = [];

        // check selected products
        $selectedProducts = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('tax_class_id')
            ->addAttributeToFilter('entity_id', ['in' => $productsIds])
            ->load();
        foreach ($selectedProducts as $product) {
            if (!$subscription &&
                Mage::helper('dyna_catalog')->is([
                    Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION,
                ], $product)
            ) {
                $subscription = $product;
            } elseif (!$device &&
                Mage::helper('dyna_catalog')->is([
                    Dyna_Catalog_Model_Type::SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE,
                ], $product)
            ) {
                $device[] = $product;
            } elseif (!$option &&
                Mage::helper('dyna_catalog')->is([
                    Dyna_Catalog_Model_Type::SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::LTE_SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::DSL_SUBTYPE_ADDON,
                ], $product)
            ) {
                $option[] = $product;
            }


            if ($subscription && ($device || $option)) {
                break;
            }
        }

        $mixMatchesDevices = $this->getMixMatchPrices(
            $subscription ? $subscription->getSku() : null,
            $device,
            $deviceRCSku,
            $websiteId,
            $salesType
        );

        $mixMatchesOptions = $this->getMixMatchPrices(
            $subscription ? $subscription->getSku() : null,
            $option,
            null,
            $websiteId,
            $salesType
        );

        $mixMatches = array_merge($mixMatchesDevices, $mixMatchesOptions);
        $prices = [];
        /** @var Dyna_Catalog_Model_Product $product */
        foreach ($products->getItems() as $product) {
            // check if product is device
            $isDevice = Mage::helper('dyna_catalog')->is([
                Dyna_Catalog_Model_Type::SUBTYPE_DEVICE,
                Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE,
            ], $product);

            $isOption = Mage::helper('dyna_catalog')->is([
                Dyna_Catalog_Model_Type::SUBTYPE_ADDON,
                Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON,
                Dyna_Catalog_Model_Type::LTE_SUBTYPE_ADDON,
                Dyna_Catalog_Model_Type::DSL_SUBTYPE_ADDON,
            ], $product);
            if (in_array($packageType, Dyna_Catalog_Model_Type::getCablePackages()) && empty((float)$product->getPrice())) {
                $productPrice = $product->getDisplayPrice();
            } else {
                $productPrice = $product->getPrice();
            }
            $priceStart = $_store->roundPrice($_store->convertPrice($productPrice));
            $price = $_taxHelper->getPrice($product, $priceStart, false);
            $priceWithTax = $_taxHelper->getPrice($product, $priceStart, true);

            $mafStart = $_store->roundPrice($_store->convertPrice($product->getData('maf')));
            $maf = $_taxHelper->getPrice($product, $mafStart, false);
            $mafWithTax = $_taxHelper->getPrice($product, $mafStart, true);


            if (isset($mixMatches[$product->getSku()])) {
                if (isset($mixMatches[$product->getSku()]['price'])) {
                    $priceStart = $_store->roundPrice($_store->convertPrice($mixMatches[$product->getSku()]['price']));
                    $priceWithTax = $_taxHelper->getPrice($product, $priceStart, true);
                    $price = $_taxHelper->getPrice($product, $priceStart, false);
                } else {
                    $price = null;
                    $priceWithTax = null;
                }

                if (isset($mixMatches[$product->getSku()]['maf'])) {
                    $mafStart = $_store->roundPrice($_store->convertPrice($mixMatches[$product->getSku()]['maf']));
                    $maf = $_taxHelper->getPrice($product, $mafStart, false);
                    $mafWithTax = $_taxHelper->getPrice($product, $mafStart, true);
                } else {
                    $maf = null;
                    $mafWithTax = null;
                }
            } elseif ($isDevice) {
                //if change, also change in the _afterSave event of the sales order as same formula is applied
                if ($subscription && $product->hasPurchasePrice()) {
                    $priceStart = $_store->roundPrice($_store->convertPrice($this->getPricing()->calculatePrice($product->getPurchasePrice(), $product->getTaxRate())));
                    $price = $_taxHelper->getPrice($product, $priceStart, false);
                    $priceWithTax = $_taxHelper->getPrice($product, $priceStart, true);

                    $mafStart = $_store->roundPrice($_store->convertPrice($this->getPricing()->calculatePrice($product->getData('maf'), $product->getTaxRate())));
                    $maf = $_taxHelper->getPrice($product, $mafStart, false);
                    $mafWithTax = $_taxHelper->getPrice($product, $mafStart, true);
                }
            }


            $prices[$product->getId()]['rel_id'] = null;
            if (($isDevice && $subscription) || ($isOption && $subscription)) {
                $prices[$product->getId()]['rel_id'] = (int)$subscription->getId();
            } elseif (!$isDevice && $device) {
                //@todo if multiple devices are selected what do we do with the other ids not sent via rel_id
                $prices[$product->getId()]['rel_id'] = (int)$device[0]->getId();
            } elseif (!$isOption && $option) {
                //@todo if multiple options are selected what do we do with the other ids not sent via rel_id
                $prices[$product->getId()]['rel_id'] = (int)$option[0]->getId();
            }

            $prices[$product->getId()]['price'] = $price;
            $prices[$product->getId()]['price_with_tax'] = $priceWithTax;
            $prices[$product->getId()]['maf'] = $maf;
            $prices[$product->getId()]['maf_with_tax'] = $mafWithTax;
        }

        return $prices;
    }

    /**
     * @return Dyna_MixMatch_Model_Pricing
     */
    protected function getPricing()
    {
        return Mage::getSingleton('dyna_mixmatch/pricing');
    }

    /**
     * @param null $subscriptionSku
     * @param null $deviceSku
     * @param null $deviceRC
     * @param null $websiteId
     * @param null $salesType
     * @return array|mixed
     */
    public function getMixMatchPrices(
        $sourceSku = null,
        $targets = [],
        $deviceRC = null,
        $websiteId = null,
        $salesType = null
    )
    {
        $key = 'getMixMatchPrices_' . md5(serialize([__METHOD__, $sourceSku, $targets, $deviceRC, $websiteId, $salesType, $this->salesId]));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = [];
            /** @var Dyna_MixMatch_Model_Pricing $mixMatchPricing */
            $mixMatchPricing = Mage::getModel('dyna_mixmatch/pricing');
            // if sales id is not set on current session, let it be
            if ($this->salesId !== false) {
                $mixMatchPricing->setSalesId($this->salesId);
            }

            if ($sourceSku) {
                $sourceMixMatches = $mixMatchPricing->getMixMatchCollection(null, $sourceSku, $websiteId);
                if ($sourceMixMatches) {
                    foreach ($sourceMixMatches->getItems() as $mixMatch) {
                        if (!isset($result[$mixMatch->getData('target_sku')])) {
                            $result[$mixMatch->getData('target_sku')] = [
                                'price' => $mixMatch->getData('price'),
                                'maf' => $mixMatch->getData('maf')
                            ];
                        }
                    }
                }
            }

            foreach ($targets as $target) {
                $targetSku = $target->getSku();
                if ($targetSku) {
                    $targetMixMatches = $mixMatchPricing->getMixMatchCollection($targetSku, null, $websiteId);
                    if ($targetMixMatches) {
                        foreach ($targetMixMatches->getItems() as $mixMatch) {
                            if ($sourceSku) {
                                $result[$mixMatch->getData('target_sku')] = [
                                    'price' => $mixMatch->getData('price'),
                                    'maf' => $mixMatch->getData('maf')
                                ];
                            }

                            if (Mage::helper('dyna_catalog')->is([
                                Dyna_Catalog_Model_Type::SUBTYPE_DEVICE,
                                Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE,
                            ], $target)) {
                                if (!isset($result[$mixMatch->getData('source_sku')])) {
                                    $result[$mixMatch->getData('source_sku')] = [
                                        'price' => $mixMatch->getData('price'),
                                        'maf' => $mixMatch->getData('maf')
                                    ];
                                }
                            }
                        }
                    }
                }
            }

            $this->getCache()->save(serialize($result), $key, [Dyna_Cache_Model_Cache::PRODUCT_TAG], $this->getCache()->getTtl());
            return $result;
        }
    }

    /**
     * Get list of prefixes as defined in the backend
     * @return array
     */
    public function getPrefixOptions()
    {
        $config_prefix = Mage::getConfig()->getNode('default/checkout/vodafoneoptions/prefix_list');
        $line = strtok($config_prefix, "\r\n");
        $prefixes = [];
        while ($line !== false) {
            $prefixes[] = $line;
            $line = strtok("\r\n");
        }

        return $prefixes;
    }

    public function showNewFormattedPriced($price = 0, $currencyInFront = true)
    {
        $priceWithoutSymbol = Mage::getModel('directory/currency')->format($price, ['display' => Zend_Currency::NO_SYMBOL], false);
        $currencySymbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $priceWithSymbolInFront = (($currencyInFront) ? ($currencySymbol . ' ') : ('')) . $priceWithoutSymbol . (($currencyInFront) ? ('') : (' ' . $currencySymbol));

        return $priceWithSymbolInFront;
    }

    public function showPriceWithoutCurrencySymbol($price)
    {
        $priceWithoutSymbol = Mage::getModel('directory/currency')->format($price, ['display' => Zend_Currency::NO_SYMBOL], false);

        return $priceWithoutSymbol;
    }

    /**
     * Ensure all packages are completed
     *
     * @return bool
     */
    public function hasAllPackagesCompleted()
    {
        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
            ->addFieldToFilter('current_status', Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
            // Skip editing disabled packages (bundles use dummy packages with editing disabled flag set tot true)
            ->addFieldToFilter('editing_disabled', array('null' => true))
            ->getSize();
        $allPackages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
            // Skip editing disabled packages (bundles use dummy packages with editing disabled flag set tot true)
            ->addFieldToFilter('editing_disabled', array('null' => true))
            ->getSize();
        return $allPackages > 0 && $packages == $allPackages;
    }

    /**
     * @param $checkoutData A collection or array of checkout data
     * @param $searchedField The field for which we try to get the value
     * @return the value or empty
     */
    public function getCheckoutFieldData($checkoutData, $searchedField)
    {
        if (!is_null($checkoutData)) {
            $checkoutData = is_array($checkoutData) ? $checkoutData : $checkoutData->getData();
            foreach ($checkoutData as $fieldData) {
                if (isset($fieldData['field_name']) && $fieldData['field_name'] == $searchedField) {
                    return $fieldData['field_value'];
                }
            }
            return null;
        }
    }

    /**
     * @param $request Mage_Core_Controller_Request_Http
     * @throws Exception
     */
    public function processNotes($request)
    {
        $packageNotes = $request->get('package_note');
        /** @var Omnius_Package_Model_Mysql4_Package $packageCollection */
        $packageCollection = Mage::getModel('package/package')->getPackages(null, Mage::getSingleton('checkout/session')->getQuote()->getId());

        /** @var Omnius_Package_Model_Package $package */
        foreach ($packageCollection as $package) {
            $packageId = $package->getPackageId();

            if (isset($packageNotes[$packageId]) && !empty($packageNotes[$packageId])) {
                if (!Mage::helper('dyna_validators')->validatePackageNotesLength($packageNotes[$packageId])) {
                    $packageNotes[$packageId] = substr($packageNotes[$packageId], 0, Dyna_Validators_Helper_Data::MAX_LENGTH_FOR_PACKAGE_NOTES);
                }
                $package->setNotes($packageNotes[$packageId]);
            } else {
                $package->setNotes(null);
            }
            $package->save();
        }
    }

    /**
     * Returns the number of days after the creation of a saved cart or offer in which that cart or offer
     * is still valid and can be edited
     *
     * @return int
     */
    public function getSavedCartValidityPeriod()
    {
        return (int)Mage::getConfig()->getNode('default/checkout/vodafoneoptions/saved_carts_validity_period');
    }

    /**
     * Returns the number of days after the expiration of a cart or offer in which that cart or offer is
     * in readonly mode (can only be viewed).
     *
     * @return int
     */
    public function getSavedCartReadonlyPeriod()
    {
        return (int)Mage::getConfig()->getNode('default/checkout/vodafoneoptions/saved_carts_readonly_period');
    }

    /**
     * Offer validity period
     *
     * @return int
     */
    public function getSavedOfferValidityPeriod()
    {
        return (int)Mage::getConfig()->getNode('default/checkout/vodafoneoptions/saved_offers_validity_period');
    }

    /**
     * Offer readonly period
     *
     * @return int
     */
    public function getSavedOfferReadonlyPeriod()
    {
        return (int)Mage::getConfig()->getNode('default/checkout/vodafoneoptions/saved_offers_readonly_period');
    }

    /**
     * @param string $createdAt
     * @return bool
     */
    public function isCartExpired(string $createdAt)
    {
        $now = new \DateTime();
        $now->setTimestamp(Mage::getModel('core/date')->timestamp());

        $creationDate = new \DateTime();
        $creationDate->setTimestamp(Mage::getModel('core/date')->timestamp($createdAt));

        $daysSinceCreation = (int)$creationDate->diff($now)->format("%r%a");

        return $this->getSavedCartValidityPeriod() < $daysSinceCreation;
    }

    /**
     * @param string $createdAt
     * @return bool
     */
    public function isCartReadonly(string $createdAt)
    {
        $now = new \DateTime();
        $now->setTimestamp(Mage::getModel('core/date')->timestamp());

        $creationDate = new \DateTime();
        $creationDate->setTimestamp(Mage::getModel('core/date')->timestamp($createdAt));

        $daysSinceCreation = (int)$creationDate->diff($now)->format("%r%a");

        return $this->getSavedCartReadonlyPeriod() < $daysSinceCreation;
    }

    /**
     * @param string $createdAt
     * @return bool
     */
    public function isOfferExpired(string $createdAt)
    {
        $now = new \DateTime();
        $now->setTimestamp(Mage::getModel('core/date')->timestamp());

        $creationDate = new \DateTime();
        $creationDate->setTimestamp(Mage::getModel('core/date')->timestamp($createdAt));

        $daysSinceCreation = (int)$creationDate->diff($now)->format("%r%a");

        return $this->getSavedOfferValidityPeriod() < $daysSinceCreation;
    }

    /**
     * @param string $createdAt
     * @return bool
     */
    public function isOfferReadonly(string $createdAt)
    {
        $now = new \DateTime();
        $now->setTimestamp(Mage::getModel('core/date')->timestamp());

        $creationDate = new \DateTime();
        $creationDate->setTimestamp(Mage::getModel('core/date')->timestamp($createdAt));

        $daysSinceCreation = (int)$creationDate->diff($now)->format("%r%a");

        return $this->getSavedOfferReadonlyPeriod() < $daysSinceCreation;
    }

    /**
     * Send the progress back to UCT when a shopping cart is saved or finalized as order
     * @param null $superOrder
     * @param null $quote
     */
    public function sendUCTReportProgress($superOrder = null, $quote = null)
    {
        //if not enabled from backend, do nothing
        if (!Mage::getStoreConfig('omnius_service/uct/enabled')) {
            return;
        }
        try {
            //at least 1 of the parameters should always be sent
            if (!$superOrder && !$quote) {
                return;
            }
            $scenario = Mage::getSingleton('checkout/session')->getInMigrationOrMoveOffnetState();
            $scenario = empty($scenario) ? 'none' : $scenario;
            $serviceCategory =
            $category_condition =
            $activeBundleRuleGuiName = 'none';
            $productTypeId = '';
            $packageTypes = array();
            $packages = $this->getPackagesBasedOnSuperOrQuoteId($quote,$superOrder);
            $lifeCycle = 'Unknown';
            $offerId = $shoppingCartId = $omniusOrderId = null;
            $ids = $this->getOfferShoppingCartOrOrderId(
                $offerId,
                $shoppingCartId,
                $omniusOrderId,
                $lifeCycle,
                $quote,
                $superOrder
            );

            $offerId = $ids['offerId'];
            $shoppingCartId = $ids['shoppingCartId'];
            $omniusOrderId = $ids['omniusOrderId'];
            $lifeCycle = $ids['lifeCycle'];

            $packageGroups = [];
            foreach ($packages as $package) {
                /* get activity type */

                /** @var $submitOrderPackage Dyna_Superorder_Model_Client_SubmitOrderClient_Package */
                $submitOrderPackage = Mage::getModel('dyna_superorder/client_submitOrderClient_package');

                $activityType = $package->getPackageAction();
                $useCaseInd = $package->getUseCaseIndication() ? $package->getUseCaseIndication() : $submitOrderPackage->getUseCaseIndication($package);

                foreach ($package->getItems() as $item) {
                    $product = $item->getProduct();
                    if (!$product) {
                        continue;
                    }

                    $packageId = $item->getPackageId();
                    $packageType = $item->getPackageType();
                    $packageSubtype = $product->getPackageSubtypeModel()->getPackageCode();

                    /* get familyCategory */
                    $categoryIds = $product->getCategoryIds(true);
                    $familyCategory = [];
                    foreach ($categoryIds as $category) {
                        $familyCategory [] = Mage::getModel('catalog/category')
                            ->load($category)
                            ->getName();
                    }

                    /* add each item to package group array */
                    if (count($familyCategory)) {
                        foreach ($familyCategory as $item) {
                            $packageGroups [] = [
                                "PackageId" => $packageId,
                                "ActivityType" => $activityType,
                                "PackageType" => strtoupper($packageType),
                                "PackageSubtype" => strtoupper($packageSubtype),
                                "FamilyCategory" => $item,
                                "UseCaseInd" => $useCaseInd,
                            ];
                        }

                    } else {
                        $packageGroups [] = [
                            "PackageId" => $packageId,
                            "ActivityType" => $activityType,
                            "PackageType" => strtoupper($packageType),
                            "PackageSubtype" => strtoupper($packageSubtype),
                            "FamilyCategory" => '',
                            "UseCaseInd" => $useCaseInd,
                        ];
                    }

                }

                $packageTypes[] = $packageType;
                if ($packageType == 'CABLE_TV' || $packageType == 'DSL') {
                    /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
                    foreach ($package->getItems() as $item) {
                        /** @var Dyna_Catalog_Model_Product $product */
                        $product = $item->getProduct();

                        if ($packageType == 'CABLE_TV' && $product->hasServiceCategoryKAA()) {
                            $serviceCategory = 'KAA';
                        } elseif ($packageType == 'CABLE_TV' && $product->hasServiceCategoryKAD()) {
                            $serviceCategory = 'KAD';
                        }
                        if ($packageType == 'DSL') {
                            $productCategories = Mage::getResourceModel("catalog/product")->getCategoryIdsWithAnchors($product);
                            $tvCategoryName = "FN_Package Configuration/Business Configuration/Products/TV";
                            /** @var Dyna_Catalog_Model_Category $tvCategory */
                            $tvCategory = Mage::getModel('catalog/category');
                            $tvCategory = $tvCategory->getCategoryByNamePath($tvCategoryName, '/');
                            if ($tvCategory) {
                                foreach ($productCategories as $cat) {
                                    if ($cat == $tvCategory->getId()) {
                                        $category_condition = 'TV';
                                    }
                                }
                            }
                        }
                    }
                }

            }

            /** @var Dyna_Bundles_Model_BundleRule[] $packageBundles */
            $packageBundles = $packages->getFirstItem()->getBundles();

            foreach ($packageBundles as $bundle) {
                $activeBundleRuleGuiName = $bundle->getBundleGuiName();
                break;
                // @todo handle for multiple bundles
            }

            $productTypeIds = [
                'GigaKombi' => [
                    'Mobile' => '002',
                    'Prepaid' => '003',
                    'DSL' => [
                        'moveOffnet' => '007',
                        'TV' => '010',
                        'none' => '005'
                    ],
                    'CABLE_INTERNET_PHONE' => '012',
                    'CABLE_TV' => [
                        'KAA' => '015',
                        'KAD' => '018'
                    ],
                    'migration' => '019',
                    'KAA' => '013',
                    'KAD' => '016'
                ],
                'none' => [
                    'Mobile' => '001',
                    'Prepaid' => '003',
                    'DSL' => [
                        'moveOffnet' => '006',
                        'TV' => '009',
                        'none' => '004'
                    ],
                    'LTE' => '008',
                    'CABLE_INTERNET_PHONE' => '011',
                    'CABLE_TV' => [
                        'KAA' => '014',
                        'KAD' => '017'
                    ],
                    'migration' => '019',
                    'KAA' => '013',
                    'KAD' => '016'
                ]
            ];

            $productTypeId = $this->getProductTypeIdForUtc(
                $productTypeId,
                $packages,
                $packageTypes,
                $productTypeIds,
                $activeBundleRuleGuiName,
                $category_condition,
                $serviceCategory,
                $scenario
            );

            $url = Mage::getStoreConfig('omnius_service/uct/uct_webhook_url') . (isset($superOrder) ? '/webhooks/osf/complete' : '/webhooks/osf/savecart');

            $orderId = isset($superOrder) ? $superOrder->getData('ogw_id') : $quote->getId();

            /* new structure */
            $UCTRequestMessage = array(
                "transactionID" => $quote->getTransactionId(),
                "ShoppingCartId" => $shoppingCartId,
                "OfferId" => $offerId,
                "OmniusOrderId" => $omniusOrderId,
                "LifeCyle" => $lifeCycle,
                "PackageGroup" => $packageGroups
            );

            $options = array(
                CURLOPT_VERBOSE => 1,
                CURLOPT_HEADER => 1,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => http_build_query($UCTRequestMessage),
                CURLOPT_RETURNTRANSFER => true,
                CURLINFO_HEADER_OUT => TRUE,
            );

            $curl = new Varien_Http_Adapter_Curl();
            $curl->setOptions($options);
            Mage::log("Sending POST request to: '$url' with body: \n" . json_encode($UCTRequestMessage), true, Zend_Log::DEBUG, self::UCT_PROGRESS_LOG_FILE, true);

            $curl->write(Zend_Http_Client::POST, $url, '1.0',array(),$UCTRequestMessage);
            $response = $curl->read();

            $responseCode = $curl->getInfo(CURLINFO_HTTP_CODE);
            $header_size = $curl->getInfo(CURLINFO_HEADER_SIZE);
            $responseHeader = substr($response, 0, $header_size);
            $responseBody = substr($response, $header_size);
            $log = [
                'transactionID' => $quote->getTransactionId(),
                "orderReference" => $orderId,
                'productTypeId' => $productTypeId,
                'status_code' => $responseCode,
                'header' => $responseHeader,
                'body' => $responseBody
            ];

            $info = $curl->getInfo(CURLINFO_HEADER_OUT);

            Mage::log("CURL request headers: \n" . json_encode($info['request_header']), Zend_Log::DEBUG, self::UCT_PROGRESS_LOG_FILE, true);
            Mage::log("CURL request body: \n" . json_encode($info['request_body']), Zend_Log::DEBUG, self::UCT_PROGRESS_LOG_FILE, true);

            Mage::log("Response for transactionId '$quote->getTransactionId()' \n" . json_encode($log), Zend_Log::DEBUG, self::UCT_PROGRESS_LOG_FILE, true);

            Mage::log("CURL response headers: \n" . json_encode($info['response_header']), Zend_Log::DEBUG, self::UCT_PROGRESS_LOG_FILE, true);
            Mage::log("CURL response body: \n" . json_encode($info['response_body']), Zend_Log::DEBUG, self::UCT_PROGRESS_LOG_FILE, true);

            $curl->close();
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * @param array $array
     * @param $key
     * @param array $new
     * @return array
     */
    private function array_insert_after(array $array, $key, array $new)
    {
        $keys = array_keys($array);
        $index = array_search($key, $keys);
        $pos = false === $index ? count($array) : $index + 1;
        return array_merge(array_slice($array, 0, $pos), $new, array_slice($array, $pos));
    }

    /**
     * @param $address
     * @return array
     * @throws Exception
     */
    public function _getAddressByType($address)
    {
        switch ($address['address']) {
            case 'pickup' :
            case 'store' :
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                $addressData = Mage::getModel('agentde/vodafoneShip2Stores')->getCollection()
                    ->addFieldToFilter('entity_id', $address['store_id']);
                $addressData->fetchItem();

                if ($addressData) {
                    $addressData = $addressData->getFirstItem();
                    $result = array(
                        'street' => trim($addressData['street']),
                        'houseNo' => trim($addressData['house_no']),
                        'postcode' => trim($addressData['postcode']),
                        'city' => trim($addressData['city']),
                        'shopName1' => trim($addressData['shop_name1']),
                        'shopName2' => trim($addressData['shop_name2']),
                        'shopName3' => trim($addressData['shop_name3']),
                    );
                    $result['store_id'] = $address['store_id'];
                }
                break;
            case 'direct' :
                $storeId = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                $addressData = Mage::getModel('agent/dealer')->getCollection()
                    ->addFieldToFilter('dealer_id', $storeId)
                    ->fetchItem();
                if ($addressData) {
                    $addressData = $addressData->getData();
                    $address['address'] = 'store';
                    $result = array(
                        'street' => array(trim($addressData['street']), trim($addressData['house_nr']), ''),
                        'postcode' => trim($addressData['postcode']),
                        'city' => trim($addressData['city']),
                    );
                    $result['store_id'] = $storeId;
                }
                break;
            case 'billing_address' :
            case 'deliver' :
                $this->validateIncompleteData($address,'billingAddress');

                $result = array(
                    'street' => array($address['billingAddress']['street'], $address['billingAddress']['houseno'], $address['billingAddress']['addition']),
                    'postcode' => $address['billingAddress']['postcode'],
                    'city' => $address['billingAddress']['city'],
                );
                break;
            case 'service' :
                $result = Mage::getModel("dyna_address/Storage")->getServiceAddressArray();
                if (!$result) {
                    throw new Exception($this->__('No service data available'));
                }
                break;
            case 'foreign_address' :
                $this->validateIncompleteData($address,'foreignAddress');
                $temp = $address['foreignAddress'];
                $result = $temp;
                // Build street data from input data
                $result['street'] = array($temp['street']);

                $result['extra_street'] = array();
                if (isset($temp['extra']) && is_array($temp['extra'])) {
                    foreach ($temp['extra'] as $extraLine) {
                        if (!empty($extraLine)) {
                            $result['extra_street'][] = $extraLine;
                        }
                    }
                }
                break;
            case 'other_address' :
                if (isset($address['other_address'])) {
                    $address['otherAddress'] = $address['other_address'];
                }
                $this->validateIncompleteData($address,'otherAddress');
                $result = array(
                    'street' => array($address['otherAddress']['street'], $address['otherAddress']['houseno'], $address['otherAddress']['addition']),
                    'postcode' => $address['otherAddress']['postcode'],
                    'city' => $address['otherAddress']['city'],
                );
                break;
            default:
                if (isset($address['other_address'])) {
                    $address['otherAddress'] = $address['other_address'];
                }
                $this->validateIncompleteData($address,'otherAddress');
        }
        $result['address'] = $address['address'];
        return $result;
    }

    /**
     *
     * @param $productsDb
     * @return array
     */
    public function getPriceSlidingPerMonthForAllProduct($productsDb)
    {
        $sortedProductsDb = $this->sortPriceSlidingProductsOrder($productsDb);
        $skus = array_keys($sortedProductsDb);

        $maxPeriod = 0;
        $maxMonth = 0;

        $filledProducts = [];
        foreach ($sortedProductsDb as $sku => $values) {
            //get array of prices
            $priceSliding = isset($values['price_sliding']) ? explode(":", $values['price_sliding']) : [];
            //get array of month intervals
            $priceRepeated = isset($values['price_repeated']) ? explode(":", $values['price_repeated']) : [];
            $lastMonth = 1;
            $row = [];
            foreach ($priceSliding as $key => $month) {
                //if month is not set, set the max
                $month = $month == '' ? $maxPeriod : $month;
                $period = [$lastMonth, $month];
                //check if the months are consecutive
                if ($month != $maxPeriod && $lastMonth + 1 < $month) {
                    //interal
                    $interval = range($lastMonth, $month);
                    // get the missing months
                    $toAdd = array_diff($interval, $period);
                    //concat
                    $period = array_merge($period, $toAdd);
                    //sort it just in case
                    sort($period);
                }
                //make float values
                $cost = (float)str_replace(',', '.', $priceRepeated[$key]);
                //set cost per month
                $formatted = array_fill_keys($period, $cost != null ? $cost : 0);
                //add to row
                $row += $formatted;
            }
            //fill the products array
            $filledProducts[$sku] = $row;
            if (count($row)) {
                if (max(array_keys($row)) > $maxMonth) {
                    $maxMonth = max(array_keys($row));
                }
            }
        }

        $finished = false;
        $i = 0;
        $results = [];
        unset($row);

        while (!$finished) {
            $i++;
            $row = [
                'month' => [
                    'from' => $i,
                    'to' => $i
                ],
                'total' => 0
            ];

            $match = [];
            foreach ($skus as $sku) {
                $month = !isset($filledProducts[$sku][$i]) ? $maxPeriod : $i;
                //current row
                $currentPrice = isset($filledProducts[$sku][$month]) ? $filledProducts[$sku][$month] : 0;
                //prev row
                end($results);         // move the internal pointer to the end of the array
                $key = key($results);
                $prevPrice = isset($results[$key][$sku]) ? $results[$key][$sku] : 0;
                //if the current price match the prev price
                $match[$sku] = $currentPrice == $prevPrice ? true : false;
                //total per row
                $row['total'] += (float)$currentPrice;
                //set row price
                $row[$sku] = $currentPrice;
            }

            //if this row has all values as the prev then change only the months interval
            if (count(array_unique($match)) === 1 && current($match) && $i != 1) {
                end($results);         // move the internal pointer to the end of the array
                $key = key($results);  // fetches the key of the element pointed to by the internal pointer
                //set the last month of the interval
                $results[$key]['month']['to'] = $i;
            } else {
                //add new row because it doesn't match the prev one
                $results[$i] = $row;
            }

            unset($row);
            //check if the next month is the last one
            if (($i + 1 >= $maxPeriod && $maxPeriod > 0) || ($i > $maxMonth)) {
                //get the last element
                end($results);
                $key = key($results);
                //set the last month
                $results[$key]['month']['to'] = ($maxPeriod > 0) ? $maxPeriod : '... ';
                $finished = true;
            }
        }

        return $results;
    }

    /**
     * The price sliding display should be done in the following order:
     * - first the subscription
     * - after the promotions
     * - after all other products
     * - at the end, before the totals, the promos
     *
     * @param array $productsDb
     * @return array
     */
    private function sortPriceSlidingProductsOrder($productsDb)
    {
        $foundSubscription = $foundPromotion = $foundPromos = $otherProducts = [];
        foreach ($productsDb as $sku => $product) {
            if (isset($product['isSubscription']) && $product['isSubscription']) {
                $foundSubscription[$sku] = $product;
            } else if (isset($product['isPromotion']) && $product['isPromotion']) {
                $foundPromotion[$sku] = $product;
            } else if (isset($product['isPromo']) && $product['isPromo']) {
                $foundPromos[$sku] = $product;
            } else {
                $otherProducts[$sku] = $product;
            }
        }

        $sortedProductsDb = [];
        $this->addProductToSortedSlidingProducts($sortedProductsDb, $foundSubscription);
        $this->addProductToSortedSlidingProducts($sortedProductsDb, $foundPromos);
        $this->addProductToSortedSlidingProducts($sortedProductsDb, $otherProducts);
        $this->addProductToSortedSlidingProducts($sortedProductsDb, $foundPromotion);

        return $sortedProductsDb;
    }

    private function addProductToSortedSlidingProducts(&$sortedProductsDb, $products)
    {
        if ($products) {
            foreach ($products as $sku => $product) {
                $sortedProductsDb[$sku] = $product;
            }
        }
    }

    /**
     * format
     * @param $items
     * @return array
     */
    public function getFormattedItemsForPriceSliding($items)
    {
        $priceSlicing = [];
        foreach ($items as $item) {
            // skip contract items
            if ($item['is_alternate']) {
                continue;
            }

            if (isset($item['has_non_standard_rate']) && $item['has_non_standard_rate']) {
                continue;
            }

            if ($item['product_visibility']) {
                // find if there is a price slicing to be display at the bottom of this packages
                if (isset($item['price_sliding']) && isset($item['price_repeated'])) {
                    $priceSlicing[$item['sku']] = [
                        'price_sliding' => isset($item['price_sliding']) ? $item['price_sliding'] : null,
                        'price_repeated' => $item['price_repeated'],
                        'period' => isset($item['period']) ? $item['period'] : 0,
                        'isSubscription' => $item['isSubscription']
                    ];
                }
                if ((isset($item['isPromotion']) && $item['isPromotion'])) {
                    $priceSlicing[$item['sku']] = [
                        'price_sliding' => isset($item['price_sliding']) ? $item['price_sliding'] : null,
                        'price_repeated' => !empty($item['price_repeated']) ? $item['price_repeated'] : $item['maf_row_total_incl_tax'],
                        'period' => isset($item['period']) ? $item['period'] : 0,
                        'isPromotion' => $item['isPromotion']
                    ];
                }
                if (isset($item['promos']) && $item['promos']) {
                    foreach ($item['promos'] as $promo) {
                        $priceSlicing[$promo['sku']] = [
                            'price_sliding' => isset($item['price_sliding']) ? $item['price_sliding'] : null,
                            'price_repeated' => isset($promo['maf_discount']) ? $promo['maf_discount'] : 0,
                            'period' => isset($item['period']) ? $item['period'] : 0,
                            'isPromo' => isset($promo['isPromo']) ? $promo['isPromo'] : false
                        ];
                    }
                }
            }
        }

        return $priceSlicing;
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @param int $packageId
     * @param $target
     * @return array
     * @internal param Dyna_Checkout_Model_Sales_Quote_Item $forItem
     */
    public function updateMixMatches($quote, $packageId, $target)
    {
        $targetMixMatch = array(
            'subtotal' => null,
            'tax' => null,
            'maf_subtotal' => null,
            'maf_tax' => null,
        );
        /** @var Dyna_Checkout_Helper_Tax $taxHelper */
        $taxHelper = Mage::helper('tax');
        foreach ($quote->getAllItems() as $item) {
            if ($item->getPackageId() != $packageId || !$item->getProduct()->is(Dyna_Catalog_Model_Type::$subscriptions)) {
                continue;
            }

            /** @var Dyna_MixMatch_Model_Pricing $mm */
            $mm = Mage::getModel('dyna_mixmatch/pricing');
            $mixMatch = $mm->getMixMatchCollection($target->getSku(), $item->getSku());
            if ($mixMatch && count($mixMatch) > 0) {
                // Calculating item mixmatch price if not null
                if ($mixMatch->getFirstItem()->getPrice() !== null && !$target->isContract()) { // VFDED1W3S-565: Ignore OC prices for ILS products
                    // Just to be sure that price is positive for mixmatch
                    $baseMixMatch = abs($mixMatch->getFirstItem()->getPrice());
                    $subtotal = $taxHelper->getPrice($target->getProduct(), $baseMixMatch, false);
                    $tax = $baseMixMatch - $subtotal;
                    if ($target->getProduct()->isPromotion()) {
                        $subtotal = -1 * $subtotal;
                        $tax = -1 * $tax;
                    }
                }

                // Calculating item mixmatch maf if not null
                if ($mixMatch->getFirstItem()->getMaf() !== null) {
                    // Just to be sure that price is positive
                    $baseMafMixMatch = abs($mixMatch->getFirstItem()->getMaf());
                    $mafSubtotal = $taxHelper->getPrice($target->getProduct(), $baseMafMixMatch, false);
                    $mafTax = $baseMafMixMatch - $mafSubtotal;
                    if ($target->getProduct()->isPromotion()) {
                        // Making subtotal and tax negative so that they will decrease price
                        // Discount mixmatch maf
                        $mafSubtotal = -1 * $mafSubtotal;
                        $mafTax = -1 * $mafTax;
                    }
                }

                $targetMixMatch = array(
                    'subtotal' => isset($subtotal) ? $subtotal : null,
                    'tax' => isset($tax) ? $tax : null,
                    'maf_subtotal' => isset($mafSubtotal) ? $mafSubtotal : null,
                    'maf_tax' => isset($mafTax) ? $mafTax : null,
                );
            }
        }

        return $targetMixMatch;
    }

    public function getOptionsForCable()
    {
        /** @var Dyna_Configurator_Model_Configuration $configurationData */
        $configurationData = Mage::getModel('dyna_configurator/configuration')
            ->getConfigurationData();
        $providersData = $configurationData->getCarriers();

        $parsedProviders = [];
        foreach ($providersData as $provider) {
            $parsedProviders[$provider['code']] = $provider['presentationtext'];
        }

        return $parsedProviders;
    }

    public function upgradeFromInternetToInternetAndPhone()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quotes */
        $quote = $this->_getQuote();
        $oldTags = array();
        $newTags = array();

        foreach ($quote->getAlternateItems() as $alternateItem) {
            $tempTags = explode(",", $alternateItem->getProduct()->getTags());
            $oldTags = array_merge($oldTags, $tempTags);
            $oldTags = array_unique($oldTags);
        }

        foreach ($quote->getItemsCollection() as $item) {
            $tempTags = explode(",", $item->getProduct()->getTags());
            $newTags = array_merge($newTags, $tempTags);
            $newTags = array_unique($newTags);
        }

        $oldIsInternetOnly = in_array(self::TAG_IS_INTERNET_ONLY, $oldTags);
        $oldIsPhoneOnly = in_array(self::TAG_IS_PHONE_ONLY, $oldTags);
        $newIsInternetOnly = in_array(self::TAG_IS_INTERNET_ONLY, $newTags);
        $newIsPhoneOnly = in_array(self::TAG_IS_PHONE_ONLY, $newTags);

        if ($oldIsInternetOnly && !$oldIsPhoneOnly && $newIsPhoneOnly && $newIsInternetOnly) {
            return true;
        }

        return false;
    }

    /**
     * Check if some attribute/s is/are missing in case of an UCT flow for a cable offer
     * @return bool
     */
    public function checkCableOfferAttributes()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $currentQuote */
        $currentQuote = $this->_getQuote();

        $packages = $currentQuote->getCartPackages();
        foreach ($packages as $package) {
            if ($package->isCable()) {
                /** @var Dyna_Customer_Model_Session $session */
                $session = Mage::getSingleton('dyna_customer/session');
                /** @var Dyna_Bundles_Helper_Data $bundleHelper */
                $bundleHelper = Mage::helper('dyna_bundles');
                $uctParams = $session->getUctParams();
                if ($uctParams && !empty($uctParams['transactionId']) && !empty($uctParams['campaignId'])) {
                    $campaignData = $bundleHelper->getCampaignData($uctParams['campaignId'], $uctParams['callingNumber'])['campaignData'];
                    $campaignCode = $campaignData['campaign_code'];
                    $advertisingCode = $campaignData['advertising_code'];
                    $marketingCode = $campaignData['marketing_code'];

                    return ($campaignCode && $advertisingCode && $marketingCode);
                }
            }
        }

        return true;
    }

    /**
     * Check existence of Sales Ids Triple
     * @return array
     */
    public function checkSalesIdTriple()
    {
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('bundles');

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        // if sales ids triple is not available, save and confirm order buttons will be disabled
        return $bundlesHelper->getProvisDataByRedSalesId($quote->getRedSalesId());
    }

    /**
     * Updates the product_id of the order items to match the current
     * product ids in the catalog.
     *
     * @param $superorderNumber
     */
    public function updateOrderItemProducts($superorderNumber)
    {
        /** @var Dyna_Superorder_Model_Superorder $superorder */
        $superorder = Mage::getModel('superorder/superorder')->getCollection()
            ->addFieldToFilter('order_number', $superorderNumber)
            ->getFirstItem();

        /** @var Dyna_Checkout_Model_Sales_Order[] $orders */
        $orders = $superorder->getOrders();

        foreach ($orders as $order) {
            $orderItems = $order->getAllItems();

            foreach ($orderItems as $orderItem) {
                /**
                 * Load the product using the SKU of the order item. If a product id has changed
                 * update the order item to reference the new id.
                 */
                /** @var Dyna_Catalog_Model_Product $product */
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $orderItem->getSku());

                if ($product && ($productId = $product->getId()) && $productId != $orderItem->getProductId()) {
                    $orderItem->setProductId($productId);
                    $orderItem->save();
                }
            }
        }
    }

    /**
     * It will be used to get the option_type product details which will display with the option type in frontend
     * @param $optionType
     * @return array
     */
    public function getConfigurableCheckoutOptions($optionType, $packageType = null)
    {
        /** @var Dyna_Catalog_Helper_Data $configurableProducts */
        $configurableProducts = Mage::helper('dyna_catalog')->getConfigurableCheckoutProducts($optionType, $packageType);

        $checkoutOptionType = Mage::getModel('dyna_catalog/checkoutOptionTypeReferencetable')->load($optionType, 'option_type')->getDisplayLabelCheckout();

        //If option_type not found in DB, we'll use fallback
        if (!$checkoutOptionType)
            $checkoutOptionType = $this->getTranslatedDefaultOptionType($optionType);

        $results = array();
        $total_price = 0;
        $results['display_label_checkout'] = $this->truncateTextWithEllipsis($checkoutOptionType, 0, 60, '...');

        // We'll restrict maximum 6 configurable checkout products display in info indicator
        $i = 0;
        foreach ($configurableProducts->getItems() as $item) {

            if ($item->getMaf()) {
                $total_price += $item->getMaf();
                $price = Mage::helper('dyna_checkout')->showNewFormattedPriced($item->getMaf(), true) . ' p.m';
            } elseif ($item->getPrice()) {
                $total_price += $item->getPrice();
                $price = Mage::helper('dyna_checkout')->showNewFormattedPriced($item->getPrice(), true);
            }
            $results['details'][] = array(
                'display_name' => $this->truncateTextWithEllipsis($item->getDisplayNameCommunication(), 0, 25, '...'),
                'price' => $price,
                'sku' => $item->getSku()
            );

            $i++;
        }
        if (isset($results['details'])) {
            $results['total_price'] = Mage::helper('dyna_checkout')->showNewFormattedPriced($total_price);
        }
        return $results;
    }

    /**
     * Design Configurable checkout options to display in front end
     * @param $details
     * @return string
     */
    public function configurableCheckoutOptionsDesign($details)
    {

        $title = "<table>";
        foreach ($details as $detail) {
            $title .= "<tr>";
            $title .= "<td>" . $detail['display_name'] . "</td><td>" . $detail['price'] . "</td>";
            $title .= "</tr>";
        }
        $title .= "</table>";
        return htmlentities($title);
    }

    /**
     * It will add configurable checkout products into shopping cart based on the option type and package type
     * @param $optionType
     * @param $packageType
     *
     * @todo check this for performance
     */
    public function addConfigurableCheckoutProducts($optionType, $packageType = null)
    {

        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $cart->getQuote();

        static $count = 0;
        $packageTypes = array();

        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $serviceCustomersAccountCategories = array_keys($customerSession->getServiceCustomers() ?? array());

        $kiasCategory = false;
        if (in_array(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS, $serviceCustomersAccountCategories)) {
            $kiasCategory = true;
        }

        $configurableProducts = Mage::helper('dyna_catalog')->getConfigurableCheckoutProducts($optionType, $packageType);

        foreach ($quote->getPackages() as $id => $package) {
            foreach ($configurableProducts as $item) {
                if ($packageType) {
                    if ($packageType == $package['type']) {
                        $quote->saveActivePackageId($id);
                        $item->setPackageId($id);
                        $addedItem = $cart->addProduct($item->getId());
                        if (!is_string($addedItem)) {
                            $addedItem->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
                            $addedItem->save();
                            if (!$addedItem->getId()) {
                                $cart->getQuote()->addItem($addedItem);
                            }
                        }
                    }
                } else {
                    if ($item->getPackageType() == $package['type']) {
                        // If Customer Account category is KIAS and processContext is ACQ then we'll not consider paper bill.
                        if ($kiasCategory && $package['sale_type'] == 'ACQ') {
                            $count = 1;
                        }

                        // Getting first KIAS cart package id, we'll consider prices for all the billing paper/online for first KIAS package
                        if (!in_array($package['type'], $packageTypes) && in_array(strtolower($package['type']), Dyna_Catalog_Model_Type::getMobilePackages())) {
                            if (!isset($cartPackageId))
                                $cartPackageId = $id;
                        }

                        // If mobile package is added multiple times then count will be increased and if count is more than 0 then we'll remove the amount of paper bill for other mobile package
                        if (($count > 0 && in_array($package['type'], $packageTypes) && $cartPackageId != $id) || ($count > 0 && $kiasCategory)) {
                            $this->modifyCartProductPrice($item);
                        }

                        // Add all the package type
                        if (!in_array($package['type'], $packageTypes))
                            $packageTypes[] = $package['type'];

                        // Package type is mobile and package already exists then increase the counter to remove the price for next mobile package
                        if (in_array($package['type'], $packageTypes) && in_array(strtolower($package['type']), Dyna_Catalog_Model_Type::getMobilePackages())) {
                            $count++;
                        }

                        $quote->saveActivePackageId($id);
                        $item->setPackageId($id);
                        $addedItem = $cart->addProduct($item->getId());
                        if (!is_string($addedItem)) {
                            $addedItem->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
                            $addedItem->save();
                            if (!$addedItem->getId()) {
                                $cart->getQuote()->addItem($addedItem);
                            }
                        }
                    }
                }
            }
        }
        $cart->save();
    }

    /**
     * It will remove configurable checkout products from shopping cart based on the option type and package type
     * @param $optionTypes
     * @param $packageType
     *
     * @todo check this for performance
     */
    public function removeConfigurableCheckoutProducts($optionTypes, $packageType = null)
    {
        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $cart->getQuote();

        foreach ($optionTypes as $optionType) {
            $configurableProducts = Mage::helper('dyna_catalog')->getConfigurableCheckoutProducts($optionType, $packageType);
            foreach ($configurableProducts as $configurableProduct) {
                foreach ($quote->getAllItems() as $item) {
                    if ($configurableProduct->getId() == $item->getProductId()) {
                        $item->isDeleted(true);
                        $item->save();
                    }
                }
            }
        }
        $quote->save();
    }

    /**
     * Trigger cable artifacts when technician selection changes
     * @param $requestedTechnician
     * @param null $packageType
     *
     * @todo check this for performance
     */
    public function triggerCableArtifacts($requestedTechnician, $packageType = null)
    {
        // Save technician needed to customer session with each stack cable/KIP
        $technicianArray[$packageType] = $requestedTechnician;

        $currentTechnician = Mage::getSingleton('customer/session')->getCustomer()->getTechnicianNeeded($packageType);

        // if the technician need has change => trigger the cablePostConditionRules
        if ($requestedTechnician != $currentTechnician) {
            // Preserve requested technician check
            Mage::getSingleton('customer/session')->getCustomer()->setTechnicianNeeded($technicianArray);
        }
    }

    /**
     * Validate Configurable Checkout Products
     *
     * @todo check this for performance
     */
    public function validateConfigurableCheckoutProducts()
    {

        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $cart->getQuote();

        $packageTypes = array();
        foreach ($quote->getPackages() as $package) {
            if (!in_array($package['type'], $packageTypes))
                $packageTypes[] = $package['type'];
        }

        // If the Cable/KIP no longer available in cart then we'll call cable artifacts as it was already called and coming again from configurator
        $currentTechnician = $technicianArray = Mage::getSingleton('customer/session')->getCustomer()->getTechnicianNeeded();
        if (!empty($currentTechnician)) {
            foreach ($currentTechnician as $key => $value) {
                if (!in_array($key, $packageTypes)) {
                    $this->triggerCableArtifacts($value, $key);
                    unset($technicianArray[$key]);
                    Mage::getSingleton('customer/session')->getCustomer()->setTechnicianNeeded($technicianArray);
                }
            }
        }
    }

    /**
     * It will remove the item price and subtract the amount from total as well, if you want to set price for the particular SKUs to 0 then send the item to this function
     * @param $item
     */
    public function modifyCartProductPrice($item)
    {
        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $cart->getQuote();

        //Deducting Paperbill amount from subtotal, tax & total as we will not be taking any charge for multiple same package(mobile)
        $quote->setInitialMixmatchMafSubtotal($quote->getInitialMixmatchMafSubtotal() - $item->getMafRowTotal());
        $quote->setInitialMixmatchMafTax($quote->getInitialMixmatchMafTax() - $item->getMafTaxAmount());
        $quote->setInitialMixmatchMafTotal($quote->getInitialMixmatchMafTotal() - $item->getMafInclTax());

        $item->setMaf(0);
        $item->setMafRowTotal(0);
        $item->setMafInclTax(0);
        $item->setMafRowTotalInclTax(0);
        $item->setMafTaxAmount(0);

        $quote->setInitialMixmatchSubtotal($quote->getInitialMixmatchSubtotal() - $item->getRowTotal());
        $quote->setInitialMixmatchTax($quote->getInitialMixmatchTax() - $item->getTaxAmount());
        $quote->setInitialMixmatchTotal($quote->getInitialMixmatchTotal() - $item->getPriceInclTax());

        $item->setPrice(0);
        $item->setRowTotal(0);
        $item->setPriceInclTax(0);
        $item->setRowTotalInclTax(0);
        $item->setTaxAmount(0);

        $cart->save();
    }

    /**
     * Truncate the string with Ellipsis
     * @param $text
     * @param $start
     * @param $length
     * @param string $ellipsis
     * @return string
     */
    public function truncateTextWithEllipsis($text, $start, $length, $ellipsis = '')
    {
        return (strlen($text) > $length) ? substr($text, $start, $length) . $ellipsis : $text;
    }

    /**
     * Checks whether the current agent has the allowed permissions to view
     * the specified order
     *
     * @param $superorderNumber
     * @return bool
     */
    public function checkAgentOrderPermissions($superorderNumber)
    {
        /** @var Dyna_Superorder_Model_Superorder $superorder */
        $superorder = Mage::getModel('superorder/superorder')
            ->getCollection()
            ->addFieldToFilter('order_number', $superorderNumber)
            ->getFirstItem();

        $hasPermission = false;
        if ($superorder->getId()) {
            /** @var Dyna_Customer_Model_Session $session */
            $session = Mage::getSingleton('customer/session');
            $agent = $session->getSuperAgent() ?: $session->getAgent(true);

            if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_ALL)) {
                $hasPermission = true;
            } else if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_TELESALES)) {
                $orderWebsiteId = $superorder->getCreatedWebsiteId();
                $websiteId = Mage::app()->getWebsite(Dyna_AgentDE_Model_Website::WEBSITE_TELESALES_CODE)->getId();

                if ($orderWebsiteId == $websiteId) {
                    $hasPermission = true;
                }
            } else if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_DEALER_GROUP)) {
                $orderDealerId = $superorder->getCreatedDealerId();
                $dealer = $agent->getDealer();
                $dealerGroups = $dealer->getDealerGroups();

                /** @var Dyna_AgentDE_Model_Dealer $dealerModel */
                $dealerModel = Mage::getModel('agent/dealer');
                $allDealers = [];
                foreach ($dealerGroups as $group) {
                    $allDealers = array_merge($allDealers, $dealerModel->getDealersInGroup($group->getGroupId()));
                }

                if (in_array($orderDealerId, $allDealers)) {
                    $hasPermission = true;
                }
            } else if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_DEALER)) {
                $orderDealerId = $superorder->getCreatedDealerId();
                $dealerId = $agent->getDealer()->getDealerId();

                if ($orderDealerId == $dealerId) {
                    $hasPermission = true;
                }
            }
        }

        return $hasPermission;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To validate incomplete data
     * @param $address
     * @param $param
     * @return void
     * @throws Exception
     */
    protected function validateIncompleteData($address,$param)
    {
        if (!isset($address[$param])) {
            throw new Exception($this->__('Incomplete data'));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To validate service address data
     * @param $result
     * @return void
     * @throws Exception
     */
    private function validateServiceAddressData($result)
    {
        if (!$result) {
            throw new Exception($this->__('No service data available'));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To retrieve packages based on super order or quote id
     * @param $quote
     * @param $superOrder
     * @return array
     */
    private function getPackagesBasedOnSuperOrQuoteId($quote, $superOrder)
    {
        $packages = Mage::getModel('package/package')
            ->getPackages(null, $quote->getId());

        if (!count($packages)) {
            $packages = Mage::getModel('package/package')
                ->getPackages($superOrder->getId());
        }
        return $packages;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To retrieve offer id , shopping cart id or order id
     * @param $offerId
     * @param $shoppingCartId
     * @param $omniusOrderId
     * @param $lifeCycle
     * @param $quote
     * @param $superOrder
     * @return array
     */
    private function getOfferShoppingCartOrOrderId($offerId, $shoppingCartId, $omniusOrderId, $lifeCycle, $quote, $superOrder)
    {
        $return = array(
            'offerId' => $offerId,
            'shoppingCartId' => $shoppingCartId,
            'omniusOrderId' => $omniusOrderId,
            'lifeCycle' => $lifeCycle,
        );
        if ($quote->getId()) {
            $return['lifeCycle'] = 'ShoppingCart';
            $return['shoppingCartId'] = $quote->getId();

            if ($quote->getIsOffer()) {
                $return['offerId'] = $quote->getId();
                $return['lifeCycle'] = 'Offer';
            }

        }

        if ($superOrder) {
            $return['lifeCycle'] = 'Order';
            $return['omniusOrderId'] = $superOrder->getOrderNumber();
        }

        return $return;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To retrieve product type id for campaign
     * @param $productTypeId
     * @param $packages
     * @param $packageTypes
     * @param $productTypeIds
     * @param $activeBundleRuleGuiName
     * @param $category_condition
     * @param $serviceCategory
     * @param $scenario
     * @return integer
     */
    private function getProductTypeIdForUtc(
        $productTypeId,
        $packages,
        $packageTypes,
        $productTypeIds,
        $activeBundleRuleGuiName,
        $category_condition,
        $serviceCategory,
        $scenario
    )
    {
        if ($packages->count()) {
            if (in_array(strtoupper(Dyna_Catalog_Model_Type::TYPE_MOBILE), $packageTypes)) {
                $productTypeId = $productTypeIds[$activeBundleRuleGuiName]['Mobile'];
            } elseif (in_array(strtoupper(Dyna_Catalog_Model_Type::TYPE_PREPAID),
                    $packageTypes) && $activeBundleRuleGuiName == 'none') {
                $productTypeId = $productTypeIds[$activeBundleRuleGuiName]['Prepaid'];
            } elseif (in_array(strtoupper(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL), $packageTypes)) {
                if ($category_condition != 'none') {
                    $productTypeId = $productTypeIds[$activeBundleRuleGuiName]['DSL'][$category_condition];
                } else {
                    $productTypeId = $productTypeIds[$activeBundleRuleGuiName]['DSL'][$scenario];
                }
            } elseif (in_array(strtoupper(Dyna_Catalog_Model_Type::TYPE_LTE),
                    $packageTypes) && $activeBundleRuleGuiName == 'none') {
                $productTypeId = $productTypeIds[$activeBundleRuleGuiName]['LTE'];
            } elseif (in_array(strtoupper(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE),
                    $packageTypes) && in_array(strtoupper(Dyna_Catalog_Model_Type::TYPE_CABLE_TV),
                    $packageTypes) && $serviceCategory != 'none') {
                $productTypeId = $productTypeIds[$activeBundleRuleGuiName]['CABLE_TV'][$serviceCategory];
            } elseif (empty($productTypeId) && in_array(strtoupper(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE),
                    $packageTypes)) {
                $productTypeId = $productTypeIds[$activeBundleRuleGuiName]['CABLE_INTERNET_PHONE'];
            } elseif ($scenario != 'none') {
                $productTypeId = $productTypeIds[$activeBundleRuleGuiName][$scenario];
            } elseif (in_array(strtoupper(Dyna_Catalog_Model_Type::TYPE_CABLE_TV),
                    $packageTypes) && $serviceCategory != 'none') {
                $productTypeId = $productTypeIds[$activeBundleRuleGuiName][$serviceCategory];
            }
        }

        return $productTypeId;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To retrieve product groups  for campaign
     * @param $familyCategory
     * @param $packageId
     * @param $activityType
     * @param $packageType
     * @param $packageSubtype
     * @param $useCaseInd
     * @return array
     */
    private function getPackageGroupsForUtc($familyCategory, $packageId, $activityType, $packageType, $packageSubtype, $useCaseInd)
    {
        $packageGroups = [];
        if (count($familyCategory)) {
            foreach ($familyCategory as $item) {
                $packageGroups [] = [
                    "PackageId" => $packageId,
                    "ActivityType" => $activityType,
                    "PackageType" => strtoupper($packageType),
                    "PackageSubtype" => strtoupper($packageSubtype),
                    "FamilyCategory" => $item,
                    "UseCaseInd" => $useCaseInd,
                ];
            }

        } else {
            $packageGroups [] = [
                "PackageId" => $packageId,
                "ActivityType" => $activityType,
                "PackageType" => strtoupper($packageType),
                "PackageSubtype" => strtoupper($packageSubtype),
                "FamilyCategory" => '',
                "UseCaseInd" => $useCaseInd,
            ];
        }

        return $packageGroups;
    }

    public function getMobileOperatorServiceProviders()
    {
        $mobileServiceProvider = Mage::getModel('dyna_checkout/mobileOperatorServiceProviders')->getCollection()->getData();

        $serviceProviderCount = count($mobileServiceProvider);
        $result = null;

        for ($iCount = 0; $iCount < $serviceProviderCount; $iCount++) {
            $code = $mobileServiceProvider[$iCount]['code'];
            $description = $mobileServiceProvider[$iCount]['description'];
            $result[$code] = $description;
        }
        return $result;
    }

    /**
     * Get mobile porting free days
     * @return mixed
     */
    public function getMobilePortingFreeDays()
    {

        $results = array();

        $mobileFreeDays = Mage::getModel('dyna_checkout/mobilePortingFreeDays')->getCollection()->getData();

        foreach ($mobileFreeDays as $freeDay) {
            $results[] = array($freeDay['free_day'], $freeDay['reason']);
        }

        return $results;
    }

    /**
     * Checks whether there is a translation for defaulted option type name
     *
     * @param $optionType
     * @return string
     */
    private function getTranslatedDefaultOptionType($optionType)
    {
        switch ($optionType) {
            case 'billing_online':
                $optionType = $this->__("Paper Bill");
                break;
            case 'billing_paper':
                $optionType = $this->__("Paper Bill");
                break;
        }
        return $optionType;
    }

    /**
     * Checks whether there is a Message Hint is there or not 
     *
     * @return array
     */
    public function getMessageHint($rules)
    {
        $message_hint = array();
        foreach ($rules as $rule_id) {
            $rulemodel = Mage::getModel('salesrule/rule');
            $salesrule = $rulemodel->load($rule_id);
            $rule_simple_action = $salesrule->getData('simple_action');
            if ($rule_simple_action == 'sales_hint') {
                $message_hint['message_hint'] = array("id" => $rule_id, "message" => $salesrule->getData('sales_hint_message'), "hint_title" => $salesrule->getData('sales_hint_title'));
            }
        }

        return $message_hint;
    }

}
