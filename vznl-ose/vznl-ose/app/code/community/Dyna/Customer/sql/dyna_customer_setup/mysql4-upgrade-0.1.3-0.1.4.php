<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$insertData = [
    ["CCN" => 0],
    ["CIM" => 0],
    ["MCN" => 0],
    ["SPC" => 0],
    ["TRC" => 0],
    ["CAN" => 1]
];

$siteModel   = Mage::getResourceModel('core/website_collection')->addFieldToFilter('name', 'telesales');
$websiteId   = $siteModel->getFirstItem()->getId();
$customerModel = Mage::getModel('dyna_customer/activityCodes');

if ($installer->tableExists('dyna_customer_activity_codes')) {
    $installer->getConnection()->truncateTable('dyna_customer_activity_codes');
    foreach ($insertData as $data) {
        foreach ($data as $actCode => $allowed) {
            $customerModel->setActivityCode($actCode);
            $customerModel->setAllowed($allowed);
            $customerModel->save();
            $customerModel->unsetData();
        }
    }
}

$installer->endSetup();
