<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table
 */
if (!$installer->tableExists('dyna_customer_activity_codes')) {
    // Creating non-supported table of combinations between account type/subtype
    $table = $installer->getConnection()
        ->newTable("dyna_customer_activity_codes")
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('activity_code', Varien_Db_Ddl_Table::TYPE_TEXT, 11, array(
        ), 'Activity code')
        ->addColumn('allowed', Varien_Db_Ddl_Table::TYPE_BOOLEAN,  array(
        ), 'Allowed/Not allowed')
        ->setComment('Activity codes');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
