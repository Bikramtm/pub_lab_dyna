<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$insertData = [
    ["A" => "AS"],
    ["A" => "CN"],
    ["A" => "JO"],
    ["A" => "KY"],
    ["A" => "MS"],
    ["A" => "PL"],
    ["A" => "RO"],
    ["A" => "SC"],
    ["A" => "SL"],
    ["A" => "SP"],
    ["B" => "AR"],
    ["B" => "AS"],
    ["B" => "AT"],
    ["B" => "CA"],
    ["B" => "CN"],
    ["B" => "EH"],
    ["B" => "FH"],
    ["B" => "GD"],
    ["B" => "JO"],
    ["B" => "MS"],
    ["B" => "NM"],
    ["B" => "PL"],
    ["B" => "RO"],
    ["B" => "SC"],
    ["B" => "SL"],
    ["B" => "SP"],
    ["E" => "RG"],
    ["I" => "AK"],
    ["I" => "AS"],
    ["I" => "AT"],
    ["I" => "CA"],
    ["I" => "FH"],
    ["I" => "FT"],
    ["I" => "ME"],
    ["I" => "MM"],
    ["I" => "MO"],
    ["I" => "NE"],
    ["I" => "NM"],
    ["I" => "NR"],
    ["I" => "PB"],
    ["I" => "PL"],
    ["I" => "SC"],
    ["I" => "SP"],
    ["I" => "UI"],
    ["S" => "AS"],
    ["S" => "CN"],
    ["S" => "FH"],
    ["S" => "JO"],
    ["S" => "PL"],
    ["S" => "SC"],
    ["S" => "SP"]
];

/**
 * Create table
 */
if (!$installer->tableExists('dyna_customer_type_subtype_combinations')) {
    // Creating non-supported table of combinations between account type/subtype
    $table = $installer->getConnection()
        ->newTable("dyna_customer_type_subtype_combinations")
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('acc_type', Varien_Db_Ddl_Table::TYPE_TEXT, 11, array(
        ), 'Account type')
        ->addColumn('acc_sub_type', Varien_Db_Ddl_Table::TYPE_TEXT, 11, array(
        ), 'Account subtype')
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, 5, array(
            'unsigned' => true,
            'nullable'  => false,
        ), 'Channel')
        ->setComment('Non-supported combinations of account type/subtype');

    $installer->getConnection()->createTable($table);

    $siteModel = Mage::getResourceModel('core/website_collection')->addFieldToFilter('name', 'telesales');
    $websiteId = $siteModel->getFirstItem()->getId();
    $model     = Mage::getModel('dyna_customer/typeSubtypeCombinations');

    foreach ($insertData as $data) {
        foreach ($data as $accType => $accSubType) {
            $model->setAccType($accType);
            $model->setAccSubType($accSubType);
            $model->setWebsiteId($websiteId);
            $model->save();
            $model->unsetData();
        }
    }
}

if ($installer->tableExists('dyna_mobile_non_supported_combinations')) {
    $installer->getConnection()->dropTable('dyna_mobile_non_supported_combinations');
}

$installer->endSetup();
