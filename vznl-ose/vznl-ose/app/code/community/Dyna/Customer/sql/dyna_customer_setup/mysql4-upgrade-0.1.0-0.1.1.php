<?php

/**
 * Installer that several customer attributes and 3 additional tables for mapping services retrieved customer data to omnius customer
 * Tables will include foreign keys to customer entity
 * @var $this Mage_Core_Model_Resource_Setup
 */

$this->startSetup();

$customerAttributes = array(
    "company_id" => array(
        'type'       => 'varchar',
        'label'      => 'The company registration number',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "company_legal_form" => array(
        'type'       => 'varchar',
        'label'      => 'Company legal form',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "company_contact_firstname" => array(
        'type'       => 'varchar',
        'label'      => 'Company contact name',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "company_contact_lastname" => array(
        'type'       => 'varchar',
        'label'      => 'Company contact name',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "company_contact_email" => array(
        'type'       => 'varchar',
        'label'      => 'Company contact email',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "company_contact_add_name" => array(
        'type'       => 'varchar',
        'label'      => 'Company contact additional name',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "phone_number" => array(
        'type'       => 'varchar',
        'label'      => 'Customer phone number',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "phone_local_area_code" => array(
        'type'       => 'varchar',
        'label'      => 'Customer area phone code',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "customer_password" => array(
        'type'       => 'varchar',
        'label'      => 'Customer password',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "fax_number" => array(
        'type'       => 'varchar',
        'label'      => 'Customer fax number',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "party_type" => array(
        'type'       => 'varchar',
        'label'      => 'Customer type (Private, SOHO etc)',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "profession" => array(
        'type'       => 'varchar',
        'label'      => 'Customer profession',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "global_id" => array(
        'type'       => 'varchar',
        'label'      => 'Customer global ID',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "life_cycle_status" => array(
        'type'       => 'varchar',
        'label'      => 'Customer life cycle status',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "customer_status" => array(
        'type'       => 'varchar',
        'label'      => 'Customer status',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "dunning_status" => array(
        'type'       => 'varchar',
        'label'      => 'Dunning status',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "account_category" => array(
        'type'       => 'varchar',
        'label'      => 'Account Category (KIAS, FN, KD or ALL)',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "bank_account" => array(
        'type'       => 'varchar',
        'label'      => 'Customer bank account',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "bank_name" => array(
        'type'       => 'varchar',
        'label'      => 'Customer bank name',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
);

$addressAttributes = array(
    "address_id" => array(
        'type'       => 'varchar',
        'label'      => 'Address ID retrieved from services',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 0,
        'is_system'  => 0,
    ),
    "address_type" => array(
        'type'       => 'varchar',
        'label'      => 'Address type',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 0,
        'is_system'  => 0,
    ),
    "post_box" => array(
        'type'       => 'varchar',
        'label'      => 'Post box',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "house_number" => array(
        'type'       => 'varchar',
        'label'      => 'House number',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "house_addition" => array(
        'type'       => 'varchar',
        'label'      => 'House addition',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "district" => array(
        'type'       => 'varchar',
        'label'      => 'District',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "region" => array(
        'type'       => 'varchar',
        'label'      => 'Region',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "country" => array(
        'type'       => 'varchar',
        'label'      => 'Country',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
    "state" => array(
        'type'       => 'varchar',
        'label'      => 'State',
        'input'      => 'text',
        'required'   => 0,
        'is_visible' => 1,
        'is_system'  => 0,
    ),
);

// Add customer attributes
foreach ($customerAttributes as $attributeCode => $attributeData) {
    $this->addAttribute("customer", $attributeCode, $attributeData);
    //After adding attribute, set form visibility
    if ($attributeData['is_visible']) {
        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute('customer', $attributeCode);
        $attribute->setData('used_in_forms', array(
            'customer_account_create',
            'customer_account_edit',
            'checkout_register',
            'adminhtml_customer',
            'adminhtml_checkout'
        ));
        $attribute->save();
    }
}

// Add address attributes
foreach ($addressAttributes as $attributeCode => $attributeData) {
    $this->addAttribute("customer_address", $attributeCode, $attributeData);
    //After adding attribute, set form visibility
    if ($attributeData['is_visible']) {
        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute('customer', $attributeCode);
        $attribute->setData('used_in_forms', array(
            'adminhtml_customer_address',
            'customer_address_edit',
            'customer_register_address'
        ));
        $attribute->save();
    }
}

$this->endSetup();
