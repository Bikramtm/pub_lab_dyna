<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

if ($installer->tableExists('dyna_customer_activity_codes')) {
    $installer->getConnection()
        ->addIndex(
            'dyna_customer_activity_codes',
            'UNIQUE_ACTIVITY_CODE',
            'activity_code',
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        );
}

$installer->endSetup();
