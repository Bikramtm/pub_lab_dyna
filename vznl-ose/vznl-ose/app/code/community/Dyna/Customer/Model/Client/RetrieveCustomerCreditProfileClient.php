<?php

/**
 * Class Dyna_Customer_Model_Client_RetrieveCustomerCreditProfileClient
 */
class Dyna_Customer_Model_Client_RetrieveCustomerCreditProfileClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "customer_credit_profile/wsdl_customer_credit_profile";
    const ENDPOINT_CONFIG_KEY = "customer_credit_profile/endpoint_customer_credit_profile";
    const CONFIG_STUB_PATH = 'customer_credit_profile/use_stubs';
    const OVER_A_MONTH_AVAILABILITY = '30+';

    protected $xmlStubs = true;

    /**
     * @param $params
     * @return mixed
     */
    public function executeRetrieveCustomerCreditProfile($params)
    {
        $searchParams = $this->mapRetrieveCustomerCreditProfile($params);
        $this->setRequestHeaderInfo($searchParams);

        $response = $this->RetrieveCustomerCreditProfile($searchParams);

        $results = $this->format($response);

        $options = [];
        foreach ($results['CustomerCreditProfile'] as $item) {
            $legacyId = $item['PartyIdentification']['ID'] ?? null;

            if ($legacyId && isset($item['Contract'])) {
                foreach ($item['Contract'] as $contract) {
                    $ctn = $contract['Subscription']['Ctn']['PhoneNumber'] ?? null;

                    if (!isset($contract['Subscription']['ProlongationOption'][0])) {
                        $contract['Subscription']['ProlongationOption'] = [$contract['Subscription']['ProlongationOption']];
                    }

                    foreach ($contract['Subscription']['ProlongationOption'] as $prolongationOption) {
                        $eligibleStatus = $this->prolongationEligibility($prolongationOption['Elegibility']['Status']);
                        $type = $this->prolongationType($prolongationOption['Type']);
                        $name = $prolongationOption['Name'];
                        $amount = $prolongationOption['Elegibility']['ChargeGross'] ? '€ ' . $prolongationOption['Elegibility']['ChargeGross'] : '';
                        $nextSubsidyDateTmp = !empty($prolongationOption['NextSubsidyDate']) ? strtotime($prolongationOption['NextSubsidyDate']) : null;
                        $currentDateTmp = strtotime(now(true));
                        $daysDiff = '';
                        $prolongationIndicator = 'red';
                        if ($nextSubsidyDateTmp) {
                            /**
                             * "Green if NextSubsiday <= current date
                             * Red if NextSubsidyDate > current date"
                             * (mapping document)
                             */
                            if ($nextSubsidyDateTmp <= $currentDateTmp) {
                                $prolongationIndicator = 'green';
                            }

                            /**
                             * If NextSubidyDate > current date && NextSubsidyDate - current date > 30 then display remaning number of days
                             * If NextSubidyDate > current date && NextSubsidyDate - current date < 30 then display 30+
                             * If NextSubsidyDate <= current date display nothing
                             */
                            if ($nextSubsidyDateTmp > $currentDateTmp) {
                                $nextSubsidyDateObj = new DateTime(date('Y-m-d', $nextSubsidyDateTmp));
                                $currentDateObj = new DateTime(date('Y-m-d', $currentDateTmp));
                                $daysBetweenSubsidyDateAndCurrentDate = $nextSubsidyDateObj->diff($currentDateObj)->format("%a");
                                if ($daysBetweenSubsidyDateAndCurrentDate > 30) {
                                    $daysDiff = $this::OVER_A_MONTH_AVAILABILITY;
                                } else {
                                    $daysDiff = $daysBetweenSubsidyDateAndCurrentDate;
                                }
                            }
                        }

                        if ($ctn && $eligibleStatus && $type) {
                            if ((in_array(key($eligibleStatus), ['N', 'T', 'E']) && key($type) == 'STD') ||
                                (in_array(key($eligibleStatus), ['N', 'A']) && key($type) == 'ART') ||
                                (in_array(key($eligibleStatus), ['N', 'I']) && in_array(key($type), ['INL', 'INO']))
                            ) {
                                $options[$legacyId . '-' . $ctn][] = [
                                    'ctn' => $ctn,
                                    'days' => $daysDiff,
                                    'endDate' => !empty($prolongationOption['NextSubsidyDate']) ? date('d.m.Y',
                                        strtotime($prolongationOption['NextSubsidyDate'])) : null,
                                    'name' => $name,
                                    'status' => [
                                        'key' => key($eligibleStatus),
                                        'value' => $eligibleStatus[key($eligibleStatus)],
                                    ],
                                    'type' => [
                                        'key' => key($type),
                                        'value' => $type[key($type)]
                                    ],
                                    'amount' => $amount,
                                    'prolongationIndicator' => $prolongationIndicator,
                                ];
                            }
                        }
                    }
                }
            }
        }
        $results['options'] = $options;

        return $results;
    }

    /**
     * @param $params
     * @return mixed
     */
    protected function mapRetrieveCustomerCreditProfile($params)
    {
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('bundles');
        $redVoid = $bundlesHelper->getRedSalesId(Dyna_Bundles_Model_CampaignDealercode::DEFAULT_CLUSTER_CATEGORY);

        $parametersMapping['DealerCode']['ID'] = $redVoid ?? '';
        $data = [];

        foreach ($params['accounts'] as $acc) {
            $customerId = trim($acc['contract']);
            $ctn = $acc['ctn'];

            $data[$customerId][] = $ctn;
        }

        foreach ($data as $customerId => $ctns) {
            $mapData[$customerId] = [
                'PartyIdentification' => ['ID' => $customerId],
            ];

            foreach ($ctns as $ctn) {
                $localAreaCode = trim($ctn['localAreaCode']);
                $phoneNumber = trim($ctn['phoneNumber']);
                $mapData[$customerId]['Contract'][] = [
                    'Subscription' => [
                        'Ctn' => [
                            'LocalAreaCode' => $localAreaCode,
                            'PhoneNumber' => $localAreaCode != "" ? $localAreaCode . $phoneNumber : $phoneNumber
                        ]
                    ]
                ];
            }
        }

        $parametersMapping['CustomerAccount'] = array_values($mapData);
        // prolongation is available only for mobile postpaid -> send always the market code mmo
        $parametersMapping['MarketCode'] = Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_POSTPAID;

        return $parametersMapping;
    }

    protected function format($results)
    {
        if (!isset($results['CustomerCreditProfile'][0])) {
            $results['CustomerCreditProfile'] = [$results['CustomerCreditProfile']];
        }

        foreach ($results['CustomerCreditProfile'] as &$item) {
            if (!isset($item['Contract'][0])) {
                $item['Contract'] = [$item['Contract']];
            }
        }

        return $results;
    }

    /**
     * @param $value
     * @return mixed|null
     */
    protected function prolongationEligibility($value)
    {
        $values = [
            "E" => "Eligible",
            "C" => "Eligible with Charge",
            "N" => "Not Eligible",
            // (not to be displayed)
            "P" => "Perhaps Eligible",
            "T" => "Eligible with Tariff change",
            "S" => "Eligible with SOC Prolongation",
            // (Only retail, not to be displayed for Telesales)
            "X" => "Eligible for NextPhone",
            "I" => "Eligible for NEY-Inlife",
            "A" => "Eligible for Added Runtime",
        ];

        return isset($values[$value]) ? [$value => $values[$value]] : null;
    }

    /**
     * @param $type
     * @return mixed|null
     */
    protected function prolongationType($type)
    {
        $types = [
            "STD" => "Standard Prolongation",
            // (Only retail, not to be displayed for Telesales)
            "NPH" => "Next Phone",
            // (only SOHO)
            "INL" => "Inlife with Recurring Charge",
            "INO" => "Inlife with One Time Charge",
            "ART" => "Added Runtime",
        ];

        return isset($types[$type]) ? [$type => $types[$type]] : null;
    }
}
