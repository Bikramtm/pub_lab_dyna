<?php

/**
 * Class Dyna_Customer_Model_Client_CreateOrAddLinkClient
 */
class Dyna_Customer_Model_Client_CreateOrAddLinkClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "create_add_link/wsdl_create_add_link";
    const ENDPOINT_CONFIG_KEY = "create_add_link/endpoint_create_add_link";
    const CONFIG_STUB_PATH = 'create_add_link/use_stubs';

    /**
     * @param $params
     * @return mixed
     */
    public function executeCreateOrAddLink($params)
    {
        $requestParams = $this->mapRetrieveCustomerCreditProfile($params);
        $this->setRequestHeaderInfo($requestParams);
        
        return $this->WSCreateOrAddLink($requestParams);
    }

    /**
     * @param $params
     * @return mixed
     */
    private function mapRetrieveCustomerCreditProfile($params)
    {
        $parametersMapping['TargetLink']['PartyIdentification']['ID'] = $params['targetId'];

        foreach ($params['customers'] as $targetId) {
            $node["PartyIdentification"]["ID"] = $targetId;
            $parametersMapping["Customer"][] = $node;
        }

        return $parametersMapping;
    }
}
