<?php

/**
 *
 * Class Dyna_Customer_Model_ExpressionLanguage_ProductCategory
 */
class Dyna_Customer_Model_Import_FutureTransactions extends Dyna_Import_Model_Adapter_Xml
{
    /**
     * Dyna_Customer_Model_Import_FutureTransactions constructor.
     * @param $args
     */
    public function __construct($args)
    {
        parent::__construct($args);
    }

    public function import()
    {
        // retrieve data
        $data = [];
        $rows = $this->getXmlData();

        // parse data
        foreach ($rows['future'] as $row) {
            $this->_totalFileRows++;

            if (trim($row['code']) != '') {
                $data[] = $row['code'];
            } else {
                $this->_skippedFileRows++;
            }
        }

        // save to db
        Mage::getConfig()
            ->saveConfig(
                'omnius_general/customer_general_settings/future_transactions',
                implode($data, "\n"),
                'default',
                0
            );
    }
}