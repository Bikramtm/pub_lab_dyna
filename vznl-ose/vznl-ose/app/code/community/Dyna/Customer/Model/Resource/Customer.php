<?php

class Dyna_Customer_Model_Resource_Customer extends Omnius_Customer_Model_Customer_Resource_Customer
{
    /**
     * Add customer_number to the default fields
     * @return array
     */
    protected function _getDefaultAttributes()
    {
        return array_unique(array_merge(array('customer_number'), parent::_getDefaultAttributes()), SORT_REGULAR);
    }
}
