<?php

/**
 * Class Dyna_Customer_Model_TypeSubtypeCombinations_Collection
 */
class Dyna_Customer_Model_Resource_TypeSubtypeCombinations_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Dyna_Customer_Model_TypeSubtypeCombinations_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('dyna_customer/typeSubtypeCombinations');
    }
}
