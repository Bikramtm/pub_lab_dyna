<?php

class Dyna_Customer_Model_ActivityCodesImporterXML extends Dyna_Import_Model_Adapter_Xml
{
    protected $lineNo = 0;

    /**
     * Dyna_MultiMapper_Model_ImporterXML constructor.
     * @param $args
     */
    public function __construct($args)
    {
        $this->logFile = "import_activity_codes.log";
        parent::__construct($args);
    }

    public function import()
    {
        $this->removePreviousData(Mage::getModel('dyna_customer/activityCodes'));
        // retrieve data
        $activityCodes = $this->getXmlData(true, false);

        // parse components
        foreach ($activityCodes->ActivityCode as $activityCode) {
            // set current line
            $this->lineNo++;

            if (trim((string) $activityCode->Code) == '') {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] Skipping element no. '.$this->lineNo.' because Code is missing.');
                continue;
            }

            if (trim((string) $activityCode->Allowed) == '') {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] Skipping element no. '.$this->lineNo.' because Allowed is missing.');
                continue;
            }

            if (!in_array(trim((string) $activityCode->Allowed), ['0', '1'])) {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] Skipping element no. '.$this->lineNo.' because of invalid value for Allowed.');
                continue;
            }

            $currentCode = Mage::getModel('dyna_customer/activityCodes');
            $currentCode->load(trim((string) $activityCode->Code), 'activity_code');

            if ($currentCode->getId()) {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] Skipping element no. '.$this->lineNo.' because Code already exists.');
                continue;
            }

            $model = Mage::getModel('dyna_customer/activityCodes');
            $model->setActivityCode(trim((string) $activityCode->Code));
            $model->setAllowed(trim((int) $activityCode->Allowed));
            $model->save();
            $model->unsetData();
        }

        $this->_totalFileRows = $this->lineNo;
    }
}
