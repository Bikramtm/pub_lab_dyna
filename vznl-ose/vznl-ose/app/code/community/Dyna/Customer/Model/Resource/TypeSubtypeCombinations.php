<?php

/**
 * Class Dyna_Customer_Model_Resource_TypeSubtypeCombinations
 */
class Dyna_Customer_Model_Resource_TypeSubtypeCombinations extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_customer/typeSubtypeCombinations', 'entity_id');
    }
}
