<?php

/**
 * Class Dyna_Customer_Model_Client_SearchCustomerFromLegacyClient
 */
class Dyna_Customer_Model_Client_SearchCustomerFromLegacyClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "customer_search_legacy/wsdl";
    const ENDPOINT_CONFIG_KEY = "customer_search_legacy/usage_url";
    const CONFIG_STUB_PATH = 'customer_search_legacy/use_stubs';

    /**
     * @param $params
     * @return mixed
     */
    public function executeSearchCustomerFromLegacy($params)
    {
        $params = $this->mapCustomerSearchParametersToXMLNodes($params);
        $this->setRequestHeaderInfo($params);

        return $this->WSSearchCustomerFromLegacy($params);
    }

    /**
     * Map the form search parameters for customer to an array with the structure needed for the XML that will be send to the service
     *
     * @access private
     * @param array $searchParams
     * @return array
     */
    private function mapCustomerSearchParametersToXMLNodes($searchParams)
    {
        $parametersMapping = [];

        if(!isset($searchParams['birthday'])){
            $searchParams['birthday'] = null;
        }

        $parametersMapping = $this->getParametersMapping($parametersMapping,$searchParams);

        //Check if we have DSL as legacy search and map the barcodeField (if filled in as customerNumber)
        $legacyField = ($parametersMapping['SearchedContact'] != null && $parametersMapping['SearchedContact']['AccountCategory'] != null) ?
            $parametersMapping['SearchedContact']['AccountCategory'] :
            null;

        if ($legacyField != null && $legacyField == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
            if (!is_numeric(mb_substr($parametersMapping['SearchedContact']['PartyIdentification']['ID'], 0, 3))) {
                $parametersMapping['SearchedOrder']['ID'] = $parametersMapping['SearchedContact']['PartyIdentification']['ID'];
                $parametersMapping['SearchedContact']['PartyIdentification'] = null;
            }
        }

        //Remove the CountryCode when no telephoneNumber is given
        if (empty($parametersMapping['SearchedContact']['ContactPhoneNumber']['PhoneNumber'])) {
            $parametersMapping['SearchedContact']['ContactPhoneNumber']['CountryCode'] = null;
        }

        return $parametersMapping;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To get $parametersMapping variables
     * @param $parametersMapping
     * @param $searchParams
     * @return array
     */
    private function getParametersMapping($parametersMapping,$searchParams)
    {
        foreach ($searchParams as $key => $value) {
            switch ($key) {
                case 'company_name':
                    $parametersMapping['SearchedAccount']['PartyName']['Name'] = $value;
                    break;
                case 'customer':
                    if (strtolower($searchParams['legacy_type']) == 'dsl') {
                        $firstChars = mb_substr($value, 0, 3);
                        $value = (is_numeric($firstChars)) ? str_pad($value, 12, "0", STR_PAD_LEFT) : $value;
                    }
                    $parametersMapping['SearchedContact']['PartyIdentification']['ID'] = $value;
                    break;
                case 'first_name':
                    $parametersMapping['SearchedContact']['Role']['Person']['FirstName'] = $value;
                    break;
                case 'last_name':
                    $parametersMapping['SearchedContact']['Role']['Person']['FamilyName'] = $value;
                    break;
                case 'birthday':
                    $parametersMapping['SearchedContact']['Role']['Person']['BirthDate'] = !empty($value) ? date('Y-m-d', strtotime($value)) : null;
                    break;
                case 'telephone_number':
                    $parametersMapping['SearchedContact']['ContactPhoneNumber']['PhoneNumber'] = $value;
                    break;
                case 'email':
                    $parametersMapping['SearchedContact']['Role']['Person']['Contact']['ElectronicMail'] = $value;
                    break;
                case 'street':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['StreetName'] = $value;
                    break;
                case 'no':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['BuildingNumber'] = $value;
                    break;
                case 'addNo':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['HouseNumberAddition'] = $value;
                    break;
                case 'city':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['CityName'] = $value;
                    break;
                case 'zipcode':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['PostalZone'] = $value;
                    break;
                default:
                    /* breaking the switch case into two to avoid cyclomatic complexity error of threshold value 20. */
                    $parametersMapping = $this->getParametersMappingOthers($parametersMapping,$key,$value);
                    break;
            }
        }

        return $parametersMapping;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To get $parametersMapping variables with more cases for switch
     * @param $parametersMapping
     * @param $key
     * @param $value
     * @return array
     */
    private function getParametersMappingOthers($parametersMapping,$key,$value)
    {
        switch ($key) {
            case 'legacy_type':
                $legacy = 'KS';
                if (strtolower($value) === 'mobile') {
                    $legacy = 'KS';
                } elseif (strtolower($value) === 'dsl') {
                    $legacy = Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN;
                } elseif (strtolower($value) === 'cable') {
                    $legacy = Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD;
                }

                $parametersMapping['SearchedContact']['AccountCategory'] = $legacy;
                break;
            case 'sim_imei':
                // sim imei field should be mapped to the same field as device id. (OMNVFDE-3518)
                $parametersMapping['SearchedEquipment']['ManufacturersItemIdentification']['ID'] = $value;
                break;
            case 'device_id':
                // update as req from OMNVFDE-2807
                $parametersMapping['SearchedContact']['PartyIdentification']['ID'] = $value;
                break;
            case 'subscriber_number':
                $parametersMapping['SearchedContact']['Contract']['Subscription']['ID'] = $value;
                break;
            case 'local_number':
                $parametersMapping['SearchedContact']['ContactPhoneNumber']['LocalAreaCode'] = $value;
                break;
        }

        return $parametersMapping;
    }
}
