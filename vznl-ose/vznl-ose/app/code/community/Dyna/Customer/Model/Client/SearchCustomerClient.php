<?php

/**
 * Class Dyna_Customer_Model_Client_SearchCustomerClient
 */
class Dyna_Customer_Model_Client_SearchCustomerClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "customer_search/wsdl";
    const ENDPOINT_CONFIG_KEY = "customer_search/usage_url";
    const CONFIG_STUB_PATH = 'customer_search/use_stubs';

    protected $xmlStubs = false;

    /**
     * @param $params
     * @return mixed
     */
    public function executeSearchCustomer($params)
    {
        $searchParams = $this->mapCustomerSearchParametersToXMLNodes($params);
        $this->setRequestHeaderInfo($searchParams);
        
        return $this->SearchCustomer($searchParams);
    }

    /**
     * Map the form search parameters for customer to an array with the structure needed for the XML that will be send to the service
     *
     * @access private
     * @param array $searchParams
     * @return array
     */
    private function mapCustomerSearchParametersToXMLNodes($searchParams)
    {
        $parametersMapping = [];
        // Set birthday and HouseNumberAddition default to null.
        $parametersMapping['SearchedContact']['Role']['Person']['BirthDate'] = null;
        $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['HouseNumberAddition'] = null;

        foreach ($searchParams as $key => $value) {
            switch ($key) {
                case 'company_name':
                    $parametersMapping['SearchedAccount']['PartyName']['Name'] = $value;
                    break;
                case 'customer':
                    $parametersMapping['SearchedContact']['Role']['Person']['ID'] = $value;
                    break;
                case 'first_name':
                    $parametersMapping['SearchedContact']['Role']['Person']['FirstName'] = $value;
                    break;
                case 'last_name':
                    $parametersMapping['SearchedContact']['Role']['Person']['FamilyName'] = $value;
                    break;
                case 'birthday':
                    $parametersMapping['SearchedContact']['Role']['Person']['BirthDate'] = !empty($value) ? date('Y-m-d', strtotime($value)) : null;
                    break;
                case 'telephone_number':
                    $parametersMapping['SearchedContact']['Role']['Person']['Contact']['Telephone'] = $value;
                    break;
                case 'email':
                    $parametersMapping['SearchedContact']['Role']['Person']['Contact']['ElectronicMail'] = $value;
                    break;
                case 'street':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['StreetName'] = $value;
                    break;
                case 'no':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['BuildingNumber'] = $value;
                    break;
                case 'addNo':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['HouseNumberAddition'] = !empty($value) ? $value : null;
                    break;
                case 'city':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['CityName'] = $value;
                    break;
                case 'zipcode':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['PostalZone'] = $value;
                    break;
                case 'device_id':
                    $parametersMapping['SearchedEquipment']['ManufacturersItemIdentification']['ID'] = $value;
                    break;
                default:
                    break;
            }
        }

        return $parametersMapping;
    }
}
