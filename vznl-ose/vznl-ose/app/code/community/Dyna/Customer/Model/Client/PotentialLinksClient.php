<?php

/**
 * Class Dyna_Customer_Model_Client_PotentialLinksClient
 */
class Dyna_Customer_Model_Client_PotentialLinksClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "potential_links/wsdl_potential_links";
    const ENDPOINT_CONFIG_KEY = "potential_links/endpoint_potential_links";
    const CONFIG_STUB_PATH = 'potential_links/use_stubs';

    /**
     * @param $params
     * @return mixed
     */
    public function executeGetPotentialLink($params)
    {
        $searchParams = $this->mapCustomerSearchPotentialLinks($params);
        $this->setRequestHeaderInfo($searchParams);
        $values = $this->WSSearchPotentialLinks($searchParams);

        return $this->parseResponseData($values);
    }

    /**
     * @param $response
     * @return mixed
     */
    public function parseResponseData($response)
    {

        // Make sure that we always return an array of accounts
        if (!empty($response['Account']['PartyName'])) {
            $response['Account'] = [$response['Account']];
        }

        if (!empty($response['Account'])) {
            foreach($response['Account'] as $key => $account) {
                $response['Account'][$key]['Role']['Person']['BirthDate'] = !empty($account['Role']['Person']['BirthDate']) ? date('d.m.Y', strtotime($account['Role']['Person']['BirthDate'])) : '';
                $response['Account'][$key]['isTemporary'] =
                    false !== strpos($account['ParentAccountNumber']['ID'], Dyna_Customer_Helper_Customer::TEMPORARY_CUSTOMER_PREFIX)
                        ? 1 : 0;
            }
        }

        return $response;
    }

    /**
     * @param $searchParams
     * @return mixed
     */
    private function mapCustomerSearchPotentialLinks($searchParams)
    {
        $parametersMapping['Customer']['ID'] = $searchParams['contactId'];

        return $parametersMapping;
    }
}
