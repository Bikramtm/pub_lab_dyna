<?php

class Dyna_Customer_Block_Adminhtml_Activitycodes_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("activitycodesGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("dyna_customer/activityCodes")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        if (!$this->_isExport) {
            $this->addColumn("entity_id", array(
                "header" => Mage::helper("customer")->__("ID"),
                "align" => "right",
                "width" => "50px",
                "type" => "number",
                "index" => "entity_id",
            ));
        }
        $this->addColumn("activity_code", array(
            "header" => Mage::helper("customer")->__("Activity Code"),
            "index" => "activity_code",
        ));
        $this->addColumn("allowed", array(
            "header" => Mage::helper("customer")->__("Allowed"),
            "index" => "allowed",
            'type' => 'options',
            'options' => array('False','True')
        ));

        return parent::_prepareColumns();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }


}
