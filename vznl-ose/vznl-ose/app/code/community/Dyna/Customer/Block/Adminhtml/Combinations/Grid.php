<?php

class Dyna_Customer_Block_Adminhtml_Combinations_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("combinationsGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("dyna_customer/typeSubtypeCombinations")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        if (!$this->_isExport) {
            $this->addColumn("entity_id", array(
                "header" => Mage::helper("dyna_customer/data")->__("ID"),
                "align" => "right",
                "width" => "50px",
                "type" => "number",
                "index" => "entity_id",
            ));
        }
        $this->addColumn("acc_type", array(
            "header" => Mage::helper("dyna_customer/data")->__("Account Type"),
            "index" => "acc_type",
        ));
        $this->addColumn("acc_sub_type", array(
            "header" => Mage::helper("dyna_customer/data")->__("Account Subtype"),
            "index" => "acc_sub_type",
        ));
        $this->addColumn('website_id', array(
            'header' =>  Mage::helper("dyna_customer/data")->__('Website'),
            'index' => 'website_id',
            'type' => 'options',
            'options' => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(false),
        ));

        return parent::_prepareColumns();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }


}
