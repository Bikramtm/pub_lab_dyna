<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Customer_Block_Adminhtml_Activitycodes_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId("account_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("customer")->__("Item Information"));
    }

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {

        $this->addTab("form_section", array(
            "label" => Mage::helper("customer")->__("Item Information"),
            "title" => Mage::helper("customer")->__("Item Information"),
            "content"=> $this->getLayout()->createBlock('dyna_customer/adminhtml_activitycodes_edit_tab_form')->toHtml()
        ));
        return parent::_beforeToHtml();
    }


}
