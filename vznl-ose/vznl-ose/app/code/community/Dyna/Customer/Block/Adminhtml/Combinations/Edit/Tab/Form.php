<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
class Dyna_Customer_Block_Adminhtml_Combinations_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Class constructor
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setDestElementId('combinations_form');
    }


    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("combinations_form", array("legend" => Mage::helper("dyna_customer/data")->__("Item information")));

        $fieldset->addField("acc_type", "text", array(
            "label" => Mage::helper("dyna_customer/data")->__("Account Type"),
            "name" => "acc_type",
        ));

        $fieldset->addField("acc_sub_type", "text", array(
            "label" => Mage::helper("dyna_customer/data")->__("Account Subtype"),
            "name" => "acc_sub_type",
        ));

        $fieldset->addField("website_id", 'select', array(
            'label' => Mage::helper('agent')->__('Website'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(),
            'name' => 'website_id',
            "class" => "required-entry",
            "required" => true,
        ));



        if (Mage::getSingleton("adminhtml/session")->getAccountData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getAccountData());
            Mage::getSingleton("adminhtml/session")->setAccountData(null);
        } elseif (Mage::registry("dyna_customer_typeSubtypeCombinations")) {
            $form->setValues(Mage::registry("dyna_customer_typeSubtypeCombinations")->getData());
        }
        return parent::_prepareForm();
    }
}
