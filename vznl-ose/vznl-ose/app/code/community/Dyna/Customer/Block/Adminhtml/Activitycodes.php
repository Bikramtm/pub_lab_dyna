<?php

class Dyna_Customer_Block_Adminhtml_Activitycodes extends Mage_Adminhtml_Block_Widget_Grid_Container{

    public function __construct()
    {
        $this->_controller = "adminhtml_activitycodes";
        $this->_blockGroup = "dyna_customer";
        $this->_headerText = Mage::helper("customer")->__("Activity Code");
        $this->_addButtonLabel = Mage::helper("customer")->__("Add New Item");
        parent::__construct();

//        $this->_addButton('import', array(
//            'label'     => Mage::helper('customer')->__('Upload activiy code'),
//            'onclick'   => 'javascript:new Popup(\'' . $this->getImportUrl() . '\', {title:\'' . Mage::helper('customer')->__('Upload new Customer type/subtype combination file') . '\', width: 600, height:220})',
//            'class'     => 'go',
//        ));
    }

    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }


}