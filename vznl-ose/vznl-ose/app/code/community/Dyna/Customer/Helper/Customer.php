<?php

/**
 * Class Dyna_Customer_Helper_Customer Helper
 */
class Dyna_Customer_Helper_Customer extends Mage_Core_Helper_Abstract
{
    const CUSTOMER_STATUS_SUSPENDED = 'S';
    const CUSTOMER_STATUS_CANCELLED = 'N';
    const CUSTOMER_STATUS_CLOSED = 'C';
    const CUSTOMER_STATUS_OPEN = 'O';

    const MOBILE_PRODUCT_STATUS_ACTIVE = 'Active';
    const MOBILE_PRODUCT_STATUS_SUSPENDED = 'Suspended';
    const MOBILE_PRODUCT_STATUS_RESERVED = 'Reserved';
    const MOBILE_PRODUCT_STATUS_CANCELLED = 'Cancelled';

    const CABLE_PRODUCT_STATUS_ACTIVE = 'Active';
    const CABLE_PRODUCT_STATUS_INTERESTED = 'Interested';
    const CABLE_PRODUCT_STATUS_NEW = 'New';
    const CABLE_PRODUCT_STATUS_BARRED = 'Barred';
    const CABLE_PRODUCT_STATUS_INACTIVE = 'Inactive';

    const FIXED_PRODUCT_STATUS_ACTIVE = 'Active';
    const FIXED_PRODUCT_STATUS_SUSPENDED = 'Suspended';

    const MOBILE_PROLONGATION_TYPE_NEW_PHONE_EVERY_YEAR = "New phone every year";
    const MOBILE_PROLONGATION_TYPE_CONTRACT_EXTENSION_WITHOUT_PAYMENT = "Extension of the contract without payment";
    const MOBILE_PROLONGATION_TYPE_STARNDARD_CONTRACT_EXTENSION = "Standard contract extension";

    const MOBILE_CONTRACT_TYPE_PREPAID = "MMO";
    const MOBILE_CONTRACT_TYPE_POSTPAID = "MMC";

    const CONTRACT_PRODUCT_STATUS_CANCELLED = "N";
    const CONTRACT_PRODUCT_STATUS_CLOSED = "C";
    const CONTRACT_PRODUCT_STATUS_SUSPENDED = "S";

    // Status active
    const MOBILE_CONTRACT_PRODUCT_STATUS_A = "A";
    // Status suspended
    const MOBILE_CONTRACT_PRODUCT_STATUS_S = "S";
    // Status reserved
    const MOBILE_CONTRACT_PRODUCT_STATUS_R = "R";
    // Status cancelled
    const MOBILE_CONTRACT_PRODUCT_STATUS_C = "C";

    const ACCOUNT_MOBILE = "Mobile";
    const ACCOUNT_CABLE = "Cable";
    const ACCOUNT_FIXED = "Fixed";

    const CUSTOMER_LINK_STATUS_DRAFT = "Draft";
    const CUSTOMER_LINK_STATUS_ACTIVE = "Active";

    const SUBSCRIPTION_NEW_PHONE_EVERY_YEAR = "NPEY";
    const SUBSCRIPTION_EXTENSION_OF_THE_CONTRACT_WITHOUT_PAYMENT = "ECWP";
    const SUBSCRIPTION_STANDARD_CONTRACT_EXTENSION = "SCE";

    const OSF_ORDER_ACTIVITY_CCN = "CCN";
    const OSF_ORDER_ACTIVITY_CIM = "CIM";
    const OSF_ORDER_ACTIVITY_MCN = "MCN";
    const OSF_ORDER_ACTIVITY_SPC = "SPC";
    const OSF_ORDER_ACTIVITY_TRC = "TRC";
    const OSF_ORDER_ACTIVITY_CAN = "CAN"; // only for this activity is the prolongation allowed on mobile

    // If the number of days until the next possible prolongation date is <= 30 days then the number of days will be displayed. Else >30 will be displayed.
    const OVER_30_DAYS = ">30";

    const TEMPORARY_CUSTOMER_PREFIX = 'TMP_';

    const PRE_POST_BAN = "PRE_POST_BAN";

    public static function getOrderActivityCodesForNoRenewalContracts()
    {
        return Mage::getSingleton('dyna_customer/activityCodes')->getNotAllowedActivityCodes();
    }

    public static function getCustomerInactiveStatusList()
    {
        return [
            self::CUSTOMER_STATUS_CANCELLED,
            self::CUSTOMER_STATUS_SUSPENDED
        ];
    }

    public static function getContractProductInactiveStatusList()
    {
        return [
            self::CONTRACT_PRODUCT_STATUS_CANCELLED,
            self::CONTRACT_PRODUCT_STATUS_SUSPENDED
        ];
    }

    const SUBSCRIPTION_TYPE_OWNER = 'O';
    const SUBSCRIPTION_TYPE_MEMBER = 'M';

    /**
     * Handle linked accounts
     * @param $type
     * @param $packageCreationTypeId
     * @param Dyna_Customer_Model_Customer $customer
     * @param $linkedAccountSelection
     * @return bool
     */
    public function handleLinkedAccountPerPackage($type, $packageCreationTypeId, $customer, &$linkedAccountSelection, $linkedAccountSelected)
    {
        /** @var Dyna_Configurator_Helper_Cart $cartHelper */
        $cartHelper = Mage::helper('dyna_configurator/cart');

        $stacks = array(
            strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE),
            strtolower(Dyna_Catalog_Model_Type::TYPE_REDPLUS),
            strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE),
            strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT),
            strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV)
        );

        $kiasPackageTypes = array(
            strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE),
            strtolower(Dyna_Catalog_Model_Type::TYPE_REDPLUS),
        );

        if (in_array(strtolower($type), $stacks) && $customer->getCustomerNumber()) {
            $customerSession = Mage::getSingleton('dyna_customer/session');
            $customerProducts = $customerSession->getCustomerProducts();
            $kiasLinkedAccountNumber = Mage::getSingleton('customer/session')->getKiasLinkedAccountNumber();
            $kdLinkedAccountNumber = Mage::getSingleton('customer/session')->getKdLinkedAccountNumber();

            // if no kias linked account on session, getting the first one
            // do this only if the package type is KIAS
            if (in_array(strtolower($type), $kiasPackageTypes)
                && !$kiasLinkedAccountNumber
                && $customer->isCustomerLoggedIn()
                && $customer->belongsToAccountCategory(Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_POSTPAID)
            ) {
                foreach ($customerProducts as $customerStack => $stackData) {
                    if ($customerStack !== Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS) {
                        continue;
                    }

                    // got first account number, breaking iteration
                    $kiasLinkedAccountNumber = ($linkedAccountSelected) ? $linkedAccountSelected : key($stackData);
                    Mage::getSingleton('customer/session')->setKiasLinkedAccountNumber($kiasLinkedAccountNumber);
                    break;
                }
            }

            if ($packageCreationTypeId != $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS) && strtolower($type) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) && $customer->belongsToAccountCategory(Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_POSTPAID)) {
                $this->handleKIASLinkedAccount($customerProducts, $kiasLinkedAccountNumber, $linkedAccountSelection);
            } elseif ($packageCreationTypeId != $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS) && strtolower($type) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) && $customer->belongsToAccountCategory(Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_PREPAID)) {
                /** @var Dyna_Checkout_Model_Sales_Quote $quote */
                $quote = Mage::getSingleton('checkout/cart')->getQuote();
                $activePackage = $quote->getCartPackage();
                if ($activePackage->getSaleType() == Dyna_Catalog_Model_ProcessContext::MIGCOC) {
                    $activePackage->setParentAccountNumber(Dyna_Customer_Helper_Customer::PRE_POST_BAN)->save();
                    Mage::getSingleton('customer/session')->setKiasLinkedAccountNumber(Dyna_Customer_Helper_Customer::PRE_POST_BAN);
                }
            } elseif ($packageCreationTypeId == $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS) && $customer->belongsToAccountCategory(Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_POSTPAID)) {
                if (!$kiasLinkedAccountNumber) {
                    $quote = Mage::getSingleton('checkout/cart')->getQuote();
                    $activePackage = $quote->getPackageById($quote->getActivePackageId());

                    if ($activePackage->getParentId()) {
                        $parentPackage = Mage::getSingleton('dyna_package/package')->load($activePackage->getParentId());
                        if ($parentPackage->getParentAccountData()) {
                            $kiasLinkedAccountNumber = $parentPackage->getParentAccountNumber();
                            Mage::getSingleton('customer/session')->setKiasLinkedAccountNumber($kiasLinkedAccountNumber);
                        }
                    }
                }

                $this->addLinkedAccount($kiasLinkedAccountNumber,array(strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE), strtolower(Dyna_Catalog_Model_Type::TYPE_REDPLUS)));
            } elseif (in_array(strtolower($type), Dyna_Catalog_Model_Type::getCablePackages()) && $customer->belongsToAccountCategory(Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_CABLE)) {
                // build additional customer data
                $customerData = array();
                foreach ($customerSession['service_customers'][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] as $service_customer) {
                    $customerInfo = array(
                        'firstName' => $service_customer['firstname'],
                        'lastName' => $service_customer['lastname'],
                        'installBase' => $customer->getAllInstallBaseProducts(),
                        'packageCreationTypeId' => $packageCreationTypeId
                    );

                    $customerData[$service_customer['customer_number']] = $customerInfo;
                }

                $this->handleKDLinkedAccount($type, $customerProducts, $kdLinkedAccountNumber, $linkedAccountSelection, $customerData);
            }
        }
    }

    /**
     * Set linked account number on quote packages and on session as well
     * @param $linkedAccountNumber
     */
    public function setPackageAndSessionLinkedAccount($linkedAccountNumber)
    {
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackage = $quote->getPackageById($quote->getActivePackageId());

        if ($activePackage->getType() == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
            $customerSession->setKiasLinkedAccountNumber($linkedAccountNumber);
            $this->addLinkedAccount($linkedAccountNumber, array(strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)));
        } elseif (in_array($activePackage->getType(), Dyna_Catalog_Model_Type::getCablePackages())) {
            $customerSession->setKdLinkedAccountNumber($linkedAccountNumber);
            $this->addLinkedAccount($linkedAccountNumber, Dyna_Catalog_Model_Type::getCablePackages());
        }
    }

    public function getLinkedAccountNo()
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackage = $quote->getPackageById($quote->getActivePackageId());
        $parentId = $activePackage->getData('parent_id');

        if ($parentId > 0) {
            $parentPackage = Mage::getModel("package/package")->load($parentId);
            if ($parentPackage->getParentAccountNumber()) {
                return $parentPackage->getParentAccountNumber();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * TASI mapping defined in VFDED1W3S-1135
     * @param string $customerNumber
     * @return array
     */
    public function getTASIOrTN($customerNumber)
    {
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('dyna_customer/session');

        $tasi = null;
        $componentCtn = null;
        $localAreaCode = null;
        $phoneNumber = null;

        if (isset($customerSession->getCustomerProducts()[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN][$customerNumber])) {
            $customerData = $customerSession->getCustomerProducts()[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN][$customerNumber];
            foreach ($customerData['contracts'] as $contract) {
                foreach ($contract['subscriptions'] as $subscription) {
                    if (!empty($subscription['technical_service_id'])) {
                        $tasi = $subscription['technical_service_id'];
                        break(2);
                    } elseif (!empty($subscription['ctn_phoneNumber'])) {
                        $componentCtn = $subscription['ctn_localAreaCode'] . $subscription['ctn_phoneNumber'];
                        $localAreaCode = $subscription['ctn_localAreaCode'];
                        $phoneNumber = $subscription['ctn_phoneNumber'];
                    }
                }
            }
        }

        return ["tasi" => $tasi, "component_ctn" => $componentCtn, "local_area_code" => $localAreaCode, 'phone_number' => $phoneNumber];
    }

    /**
     * Add linked account per package
     * @param $linkedAccountNumber
     */
    protected function addLinkedAccount($linkedAccountNumber, $stack)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        foreach ($quote->getCartPackages() as $package) {
            if (in_array($package->getType(), $stack)) {
                $package->setParentAccountNumber($linkedAccountNumber);
                $package->save();
            }
        }
    }

    /**
     * Handle KIAS linked account
     * @param $customerProducts
     * @param $linkedAccountNumber
     * @param $linkedAccountSelection
     */
    protected function handleKIASLinkedAccount($customerProducts, $linkedAccountNumber, &$linkedAccountSelection)
    {
        $kiasLinkedAccounts = isset($customerProducts[Dyna_Superorder_Helper_Client::BACKEND_KIAS]) ? $customerProducts[Dyna_Superorder_Helper_Client::BACKEND_KIAS] : [];
        $kiasLinkedAccountsPrepaid = [];
        // As specified in VFDED1W3S-1482, display in linked accounts panel only postpaid subscriptions
        foreach($kiasLinkedAccounts as $customerId => $customerContracts) {
            foreach($customerContracts['contracts'] as $contractKey => $contract) {
                foreach($contract['subscriptions'] as $subscriptionKey => $subscription) {
                    if($subscription['package_type'] == strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID)) {
                        $kiasLinkedAccountsPrepaid[$customerId]['contracts'][$contractKey]['subscriptions'][$subscriptionKey] = $kiasLinkedAccounts[$customerId]['contracts'][$contractKey]['subscriptions'][$subscriptionKey];
                        unset($kiasLinkedAccounts[$customerId]['contracts'][$contractKey]['subscriptions'][$subscriptionKey]);
                    }
                }
                if(empty($kiasLinkedAccounts[$customerId]['contracts'][$contractKey]['subscriptions'])) {
                    unset($kiasLinkedAccounts[$customerId]['contracts'][$contractKey]);
                }
            }
            if(empty($kiasLinkedAccounts[$customerId]['contracts'])) {
                unset($kiasLinkedAccounts[$customerId]);
            }
        }

        // if there are more than 1 linked customers display a select list with them
        if (count($kiasLinkedAccounts) > 1) {
            $this->displayLinkedAccounts($kiasLinkedAccounts, $linkedAccountNumber, $linkedAccountSelection, array(strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)));
        } elseif (count($kiasLinkedAccounts) == 1) {
            $this->displayLinkedAccounts($kiasLinkedAccounts, $linkedAccountNumber, $linkedAccountSelection, array(strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)));
            //case when active package is in Pre-Post flow
            if (count($kiasLinkedAccountsPrepaid) > 1) {
                $this->displayLinkedAccounts($kiasLinkedAccounts, $linkedAccountNumber, $linkedAccountSelection, array(strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)));
            } else {
                // or set linked account customer number per package if there is just one linked account
                $this->addLinkedAccount($linkedAccountNumber, array(strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)));
            }
        } elseif (count($kiasLinkedAccounts) == 0 && count($kiasLinkedAccountsPrepaid) > 1) {
            // or set linked account customer number per package if there is just one linked account
            $this->addLinkedAccount($linkedAccountNumber, array(strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)));
        }
    }

    /**
     * Handle KD linked account
     * @param $customerProducts
     * @param $linkedAccountNumber
     * @param $linkedAccountSelection
     * @param $customerData
     */
    protected function handleKDLinkedAccount($type, $customerProducts, $linkedAccountNumber, &$linkedAccountSelection, $customerData = null)
    {
        $kdStack = array(
            strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV),
            strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE),
            strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT)
        );
        $linkedAccounts = $customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD];
        $linkedAccountsInfo = $this->getKDProducts($customerProducts);
        $this->handleKDlinking($linkedAccountsInfo, $linkedAccounts, $type);

        if (isset($customerData[$linkedAccountNumber])) {
            $linkedAccounts[$linkedAccountNumber]['customerData'] = $customerData[$linkedAccountNumber];
        }

        if (count($linkedAccounts) > 1) {
            $this->displayLinkedAccounts($linkedAccounts, $linkedAccountNumber, $linkedAccountSelection, $kdStack, $customerData);
        } elseif (count($linkedAccounts) == 1) {
            // or set linked account customer number per package if there is just one linked account
            $doLinkAccount = true;
            if (!$linkedAccountNumber) {
                foreach ($linkedAccounts as $kdCustomerId => $kdDetails) {
                    Mage::getSingleton('customer/session')->setKdLinkedAccountNumber($kdCustomerId);
                    break;
                }

                $linkedAccountNumber = Mage::getSingleton('customer/session')->getKdLinkedAccountNumber();
            }

            if ($linkedAccounts[$linkedAccountNumber]['customerData']['packageCreationTypeId'] == Mage::helper('dyna_configurator/cart')->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_CABLE_INTERNET_PHONE)) {
                foreach ($linkedAccounts[$linkedAccountNumber]['customerData']['installBase'] as $productDetails) {
                    if ($productDetails['package_type'] == Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE) {
                        $doLinkAccount = false;
                        break;
                    }
                }
            }

            if ($doLinkAccount) {
                $this->addLinkedAccount($linkedAccountNumber, $kdStack);
            }
        }
    }

    /**
     * Display linked accounts
     * @param $linkedAccounts
     * @param $linkedAccountNumber
     * @param $linkedAccountSelection
     * @param $stack
     * @param $customerData
     */
    protected function displayLinkedAccounts($linkedAccounts, $linkedAccountNumber, &$linkedAccountSelection, $stack, $customerData = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        $customerSession = Mage::getSingleton('customer/session');

        // if linked account number has already been set on session do not display the customer list anymore
        if (!$linkedAccountNumber) {
            foreach ($quote->getCartPackages() as $package) {
                if (!$linkedAccountNumber = $package->getParentAccountNumber()) {
                    continue;
                }

                // Before pre-selecting, try loading the linked account from package (@see OVG-2375)
                if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getCablePackages())) {
                    $customerSession->setKdLinkedAccountNumber($linkedAccountNumber);
                }

                if (strtolower($package->getType() === strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE))) {
                    $customerSession->setKiasLinkedAccountNumber($linkedAccountNumber);
                }
            }
            // if still no linked account found, proceed to loading the last activated
            if (!$linkedAccountNumber) {
                $this->preselectLatestBANByActivationDate($linkedAccounts);
            }
        } else {
            $this->addLinkedAccount($linkedAccountNumber, $stack);
        }

        $disabledState = $clearCart =  false;
        // searching for previous package of type mobile postpaid or cable which sets the linked accounts
        foreach ($quote->getCartPackages() as $package) {
            // if cable, searching for one of the cable packages
            if (in_array(strtolower($quote->getCartPackage()->getType()), [strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV), strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE)])) {
                if ($package->isCable() && !$package->getEditingDisabled() && $package->getPackageId() < $quote->getCartPackage()->getPackageId()) {
                    $disabledState = true;
                    break;
                }
                // if mobile postpaid, searching only for mobile postpaid
            } elseif (strtolower($quote->getCartPackage()->getType()) === strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
                if (strtolower($package->getType()) === strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)
                    && $quote->getCartPackage()->isPrePost() && $quote->getCartPackage()->getId() != $package->getId()
                    && !$package->getEditingDisabled()
                    && !$clearCart
                ) {
                    $clearCart = true;
                }

                if (strtolower($package->getType()) === strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)
                    && $package->getPackageId() < $quote->getCartPackage()->getPackageId()
                    || $package->isPartOfRedPlus()) {
                    $disabledState = true;
                    break;
                }
            }
        }

        $linkedAccountSelection = Mage::getSingleton('core/layout')
            ->createBlock('checkout/cart_totals')
            ->setTemplate('configurator/linked-account.phtml')
            ->setLinkedAccounts($linkedAccounts)
            ->setCustomerData($customerData)
            ->setDisabledState($disabledState)
            ->setClearCart($clearCart)
            ->toHtml();
    }

    /**
     * Get products from linked KD accounts
     * @param $customerProducts
     * @return array
     */
    protected function getKDProducts($customerProducts)
    {
        $kdLinkedAccounts = $customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD];
        $linkedAccounts = array();
        $counter = 0;
        foreach ($kdLinkedAccounts as $customerId => $account) {
            $linkedAccounts[$counter]['customerId'] = $customerId;
            $categories = '';
            foreach ($account['contracts'][0]['subscriptions'][0]['products'] as $productCategory => $product) {
                $productCategory = $productCategory == Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_CLS ?
                    'KUD' : $productCategory;
                $categories .= $productCategory . ',';
            }
            $categories = rtrim($categories, ',');
            $linkedAccounts[$counter]['products'] = $categories;
            $counter++;
        }

        return $linkedAccounts;
    }

    /**
     * Get KD linking conditions (OVG-1344)
     * @return array
     */
    protected function getKDlinkingConditions()
    {
        return array(
            // outcome - 0 (don't display linked accounts), 1 (display them), stack (display the account with that stack)
            // KUD = CLS, without stack = !
            array('linkedAccounts' => 1, 'with' => 'KIP', 'orders' => 'KIP', 'outcome' => 0),
            array('linkedAccounts' => 1, 'with' => 'KUD', 'orders' => 'KUD', 'outcome' => 0),
            array('linkedAccounts' => 1, 'with' => 'TV', 'orders' => 'TV', 'outcome' => 1),
            array('linkedAccounts' => 2, 'with' => '!,!', 'orders' => 'KIP', 'outcome' => 1),
            array('linkedAccounts' => 2, 'with' => 'KIP,!', 'orders' => 'KIP', 'outcome' => '!'),
            array('linkedAccounts' => 2, 'with' => 'KIP,KIP', 'orders' => 'KIP', 'outcome' => 0),
            array('linkedAccounts' => 2, 'with' => '!,!', 'orders' => 'TV', 'outcome' => 1),
            array('linkedAccounts' => 2, 'with' => 'TV,!', 'orders' => 'TV', 'outcome' => 1),
            array('linkedAccounts' => 2, 'with' => 'TV,TV', 'orders' => 'TV', 'outcome' => 1),
            array('linkedAccounts' => 2, 'with' => 'KUD,KUD', 'orders' => 'KUD', 'outcome' => 0),
        );
    }

    /**
     * Set KD linking conditions of current session linked accounts
     * @param $linkedAccountsInfo
     * @param $type
     * @return array
     */
    protected function setKDlinkingConditions($linkedAccountsInfo, $type)
    {
        $stacks = array(
            strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE) => 'KIP',
            strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT) => 'KUD',
            strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV) => 'TV'
        );
        $linkedAccountsNumber = count($linkedAccountsInfo) > 2 ? 2 : count($linkedAccountsInfo);
        $orderedPackage = $stacks[strtolower($type)];
        $containedPackages = '';
        $found = 0;

        if ($linkedAccountsNumber == 1) {
            $containedPackages = $orderedPackage;
            $found = 1;
            $idOfCustomers = (current($linkedAccountsInfo))['customerId'] ?? "";
        } elseif ($linkedAccountsNumber > 1) {
            $idOfCustomers = '';
            foreach ($linkedAccountsInfo as $account) {
                if (substr_count($account['products'], $orderedPackage) == 1) {
                    $found++;
                } elseif (substr_count($account['products'], $orderedPackage) == 0) {
                    $idOfCustomers .= $account['customerId'] . ',';
                }
            }
            $idOfCustomers = $idOfCustomers != '' ? rtrim($idOfCustomers, ',') : '';

            if ($found == 1) {
                $containedPackages = $stacks[strtolower($type)] . ',!';
            } elseif ($found > 1) {
                $containedPackages = $stacks[strtolower($type)] . ',' . $stacks[strtolower($type)];
            } else {
                $containedPackages = '!,!';
            }
        }

        return array(
            'linkedAccounts' => $linkedAccountsNumber,
            'with' => $containedPackages,
            'orders' => $orderedPackage,
            'customerId' => $found >= 1 ? $idOfCustomers : ''
        );
    }

    /**
     * Handle KD linking
     * @param $linkedAccountsInfo
     * @param $linkedAccounts
     * @param $type
     */
    protected function handleKDlinking($linkedAccountsInfo, &$linkedAccounts, $type)
    {
        $conditions = $this->getKDlinkingConditions();
        $KDcondition = $this->setKDlinkingConditions($linkedAccountsInfo, $type);

        foreach ($conditions as $condition) {
            if (($condition['linkedAccounts'] == $KDcondition['linkedAccounts'])
                && ($condition['with'] == $KDcondition['with'])
                && ($condition['orders'] == $KDcondition['orders'])
            ) {
                $linkedAccounts = $this->handleKDlinkingOutcome($condition['outcome'], $linkedAccounts, $KDcondition['customerId']);
            }
        }
    }

    /**
     * Handle linking outcome for KD accounts
     * @param $outcome
     * @param $linkedAccounts
     * @param null $accountId
     * @return bool
     */
    protected function handleKDlinkingOutcome($outcome, $linkedAccounts, $accountId = null)
    {
        $accounts = $linkedAccounts;
        if ((string)$outcome === '!') {
            $accountsToDisplay = explode(',', $accountId);
            $accounts = array();
            foreach ($accountsToDisplay as $account) {
                if(!empty($linkedAccounts[$account])) {
                    $accounts[$account] = $linkedAccounts[$account];
                }
            }
        } elseif ((string)$outcome === '0') {
            $accounts = array();
        }

        return $accounts;
    }

    /**
     * Preselect lastest BAN by activation date
     * @param $linkedAccounts
     */
    protected function preselectLatestBANByActivationDate($linkedAccounts)
    {
        // get BAN by finding the latest activation date in the eligible linked accounts
        $lastActivationDate = '';
        $lastActivationDateBan = '';
        foreach ($linkedAccounts as $BAN => $accountDetails) {
            foreach ($accountDetails['contracts'] as $contract) {
                $activationDate = $contract['subscriptions'][0]['contract_activation_date'];
                $lastActivationDate = $lastActivationDate == '' ? $activationDate : $lastActivationDate;
                $lastActivationDateBan = $lastActivationDateBan == '' ? $BAN : $lastActivationDateBan;
                if (strtotime($activationDate) > strtotime($lastActivationDate)) {
                    $lastActivationDate = $activationDate;
                    $lastActivationDateBan = $BAN;
                }
            }
        }
        // set it on session and current quote packages of the same stack
        $this->setPackageAndSessionLinkedAccount($lastActivationDateBan);
    }

    /**
     * From all the merged subscriptions retrieved from service RetrieveCustomerInfo (mode1 for DSL/Cable and mode2 for Mobile)
     * return the first subscription, first product nature code:
     * lc:RetrieveCustomerInfoResponse/lc:Customer/lean:Contract/lean:Subscription/lean:Product/cac:CommodityClassification/NatureCode
     * OMNVFDE-2458
     * @return string
     */
    public function getCustomerDataAccountCategory()
    {
        $customerSession = Mage::getSingleton('customer/session');
        $customer = $customerSession->getCustomer();
        $soapCustomers = $customerSession->getCustomerProducts();
        $customerAccountCategory = $customer->getData('account_category');

        if (isset($soapCustomers[$customerAccountCategory])) {
            foreach ($soapCustomers[$customerAccountCategory] as $soapCustomer) {
                foreach ($soapCustomer['contracts'] as $contract) {
                    if ($contract['customer_number'] != $customer->getCustomerNumber()) {
                        continue;
                    }
                    foreach ($contract['subscriptions'] as $subscription) {
                        if ($customerAccountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                            if(isset($subscription['commodity_classification_nature_code'])){
                                return $subscription['commodity_classification_nature_code'];
                            }
                        }
                        elseif ($customerAccountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {
                            foreach ($subscription['products'] as $products) {
                                foreach ($products as $product) {
                                    if (isset($product['commodity_classification_nature_code'])) {
                                        return $product['commodity_classification_nature_code'];
                                    }
                                }
                            }
                        }
                        else {
                            foreach ($subscription['products'] as $product) {
                                if (in_array(
                                    strtolower(Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_POSTPAID) ,
                                    array(
                                        strtolower($subscription['contract_type']) ,
                                        strtolower($product['contract_type'])
                                    )
                                )) {
                                    return Dyna_Catalog_Model_Type::TYPE_MOBILE;
                                }
                                elseif (in_array(
                                    strtolower(Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_PREPAID) ,
                                    array(
                                        strtolower($subscription['contract_type']) ,
                                        strtolower($product['contract_type'])
                                    )
                                )) {
                                    return Dyna_Catalog_Model_Type::TYPE_PREPAID;
                                }

                                if (isset($product['commodity_classification_nature_code'])) {
                                    return $product['commodity_classification_nature_code'];
                                }
                            }
                        }
                    }
                }
            }
        }

        return '';
    }
}
