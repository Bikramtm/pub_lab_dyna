<?php

/**
 * Map customer data from RetrieveCustomerInfo service call to magento customer model / session model
 * Class Dyna_Customer_Helper_Services
 */

class Dyna_Customer_Helper_Services extends Mage_Core_Helper_Abstract
{
    CONST CODE_CABLE = "Cable";
    CONST CODE_DSL = "Dsl";
    CONST CODE_MOBILE = "Mobile";

    CONST DUNNING_IN = "IN_DUNNING";
    CONST DUNNING_NO = "NO_DUNNING";

    CONST ACCOUNT_CATEGORY_KIAS = "KIAS";
    CONST ACCOUNT_CATEGORY_KS = "KS";
    CONST ACCOUNT_CATEGORY_FN = "FN";
    CONST ACCOUNT_CATEGORY_KD = "KD";
    CONST ACCOUNT_CATEGORY_ALL = "ALL";
    CONST ACCOUNT_CATEGORY_MOBILE = "Mobile";
    CONST ACCOUNT_CATEGORY_DSL = "DSL";
    CONST ACCOUNT_CATEGORY_FIXED = "Fixed";
    CONST ACCOUNT_CATEGORY_CABLE = "Cable";

    CONST PARTY_TYPE_PRIVATE = "PRIVATE";
    CONST PARTY_TYPE_SOHO = "SOHO";

    CONST ACCOUNT_CLASS_PRIVATE = "P";
    CONST ACCOUNT_CLASS_SOHO = "S";

    CONST ADDRESS_TYPE_LEGAL = "L";
    CONST ADDRESS_TYPE_BILLING = "B";
    CONST ADDRESS_TYPE_SERVICE = "S";
    CONST ADDRESS_TYPE_SHIPPING = "SH";

    CONST PHONE_PRIVATE = "PRIVATE";
    CONST PHONE_WORK = "WORK";

    CONST CUSTOMER_SALUTATION_MR = "Mr.";
    CONST CUSTOMER_SALUTATION_MRS = "Mrs.";

    CONST ADDRESS_TYPE_SERVICE_LONG = "service";
    CONST ADDRESS_TYPE_LEGAL_LONG = "legal";

    CONST KIP_LEGACY_ID_TRIPLE_PLAY = 'BR5';

    protected $flatArray = null;
    protected $servicesCache = array();

    /**
     * @param $package
     * @return null|string
     */
    public static function getStackPerPackage($package, $returnLegacySystemId = false)
    {
        if (is_array($package)) {
            $package = array_shift($package);
        }

        switch (strtolower($package)) {
            case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_REDPLUS):
                return self::ACCOUNT_CATEGORY_KIAS;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_LTE):
                return $returnLegacySystemId ? self::ACCOUNT_CATEGORY_FN : self::ACCOUNT_CATEGORY_FIXED;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
                return self::ACCOUNT_CATEGORY_KD;
            default:
                return null;
        }
    }

    public static function getMobileStack() {
        return array(
            self::ACCOUNT_CATEGORY_KIAS, self::ACCOUNT_CATEGORY_MOBILE, self::ACCOUNT_CATEGORY_KS
        );
    }
    public static function getFixedStack() {
        return array(
            self::ACCOUNT_CATEGORY_FN, self::ACCOUNT_CATEGORY_DSL
        );
    }
    public static function getCableStack() {
        return array(
            self::ACCOUNT_CATEGORY_KD, self::ACCOUNT_CATEGORY_CABLE
        );
    }
    public static function getAddressMapping()
    {
        return array(
            static::ADDRESS_TYPE_LEGAL => Dyna_Customer_Model_Customer::DEFAULT_ADDRESS,
            static::ADDRESS_TYPE_BILLING => Mage_Customer_Model_Address_Abstract::TYPE_BILLING,
            static::ADDRESS_TYPE_SERVICE => Dyna_Customer_Model_Customer::SERVICE_ADDRESS,
            static::ADDRESS_TYPE_SHIPPING => Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING,
        );
    }

    public static function excludedCableAccountsLifeCycleStatuses()
    {
        return [
            "EN",
            "NE"
        ];
    }

    public static function excludedMobileAccountsLifeCycleStatuses()
    {
        return [
            "T"
        ];
    }

    /**
    /*
     * Return the value of a certain service response node by searching through service responses stored in customer session
     * @param string $serviceName
     * @param string $xpath
     * @param string|null $requestHash
     * @param bool $asArray
     *
     * @return string|array
     */
    public function getServiceValueByXpath(string $serviceName, string $xpath, string $requestHash = null, $asArray = false)
    {
        if (!isset($this->servicesCache[$serviceName][$requestHash])) {
            $defaultResponse = $asArray ? array() : null;

            /** @var Dyna_Customer_Model_Session $customerSession */
            $customerSession = Mage::getSingleton('customer/session');
            $responses = $customerSession->getServicesResponse($serviceName);
            // Search session for the appropriate response
            if ($requestHash) {
                $response = $responses[$requestHash] ?? null;
            } else {
                $response = $responses ? current($responses) : null;
            }
            // If no response, return an empty one
            if (!$response) {
                return $defaultResponse;
            }

            // Get rid of namespace references from nodes
            $response= preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$3", $response);
            // Parse xml an fetch xpath data
            $this->servicesCache[$serviceName][$requestHash] = simplexml_load_string($response, null, LIBXML_NSCLEAN);
        }
        // Get node data by xpath
        // Suppress warnings if xpatch is invalid
        $node = @$this->servicesCache[$serviceName][$requestHash]->xpath($xpath);

        return $asArray ? (array)($node[0] ?? array()) : (string)($node[0] ?? "");
    }

    /**
     * Get array with all stack types
     * @return array
     */
    public static function getAllStacks()
    {
        return array_merge(self::getMobileStack(), self::getFixedStack(), self::getCableStack());
    }
}
