<?php

class Dyna_Customer_Helper_Data extends Mage_Core_Helper_Abstract
{
    const OSF_ORDER_ACTIVITY_REASON_OB = "OB";
    const OSF_ORDER_ACTIVITY_REASON_OU = "OU";
    const OSF_ORDER_ACTIVITY_REASON_ON = "ON";
    /**********
     * METHOD NOT USED ANYMORE
     */

    /**
     * Return customer set on session
     * @return Dyna_Customer_Model_Customer
     */
    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')
            ->getCustomer();
    }

    protected function getServiceCustomerNumbersForEachStack($customerNumber = null)
    {
        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');

        // If no customer number provided, use all customer for that stack
        if (is_null($customerNumber)) {
            $serviceCustomerNumbers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = $customerStorage->getServiceCustomerNumbersPerStack(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS);
            $serviceCustomerNumbers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = $customerStorage->getServiceCustomerNumbersPerStack(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD);
            $serviceCustomerNumbers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FIXED] = $customerStorage->getServiceCustomerNumbersPerStack(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN);
            // use only current customer number provided
        } else {
            $serviceCustomerNumbers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS][] = $customerNumber;
            $serviceCustomerNumbers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD][] = $customerNumber;
            $serviceCustomerNumbers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FIXED][] = $customerNumber;
        }

        return $serviceCustomerNumbers;
    }

    public function cancelSubscriberReasonCodes(){
        return array(
            $this::OSF_ORDER_ACTIVITY_REASON_OB,$this::OSF_ORDER_ACTIVITY_REASON_OU,$this::OSF_ORDER_ACTIVITY_REASON_ON
        );
    }

    /**
     * @param $customerNumber
     * @param $packageTypes
     * @param null $ctn
     * @param null $serviceLineId
     * @return bool
     */
    public function hasOpenOrders($customerNumber, $packageTypes, $ctn = null, $serviceLineId = null)
    {
        if ($this->hasOSFOpenOrders($customerNumber, $packageTypes, $ctn, $serviceLineId)) {
            return true;
        } else {
            return $this->hasNonOSFOrders($customerNumber, $packageTypes, $ctn);
        }
    }

    public function hasNonOSFOrders($customerNumber = null, $packageTypes, $ctn = null)
    {
        if (!$packageTypes) {
            return false;
        }
        if (!is_array($packageTypes)) {
            $packageTypes = [$packageTypes];
        }
        $packageTypes = array_map('strtolower', $packageTypes);

        /** @var Dyna_Customer_Helper_Panel $customerPanelHelper */
        $customerPanelHelper = Mage::helper('dyna_customer/panel');
        $lineOfBusinesses = [];
        foreach ($packageTypes as $packageType) {
            $lineOfBusinesses[] = strtolower($customerPanelHelper->getLineOfBusinessForPackageType($packageType));
        }
        $lineOfBusinesses = array_unique($lineOfBusinesses);

        $serviceCustomerNumbers = $this->getServiceCustomerNumbersForEachStack($customerNumber);

        // Check non OSF Orders
        $nonOsfOrders = $customerPanelHelper->getServiceCustomerOrders();
        foreach ($lineOfBusinesses as $lineOfBusiness) {
            if (isset($nonOsfOrders[$lineOfBusiness]) && count($nonOsfOrders[$lineOfBusiness])) {
                foreach ($nonOsfOrders[$lineOfBusiness] as $nonOsfOrder) {
                    // For FN stack does NOT matter customer number, it just blocks everything
                    switch ($lineOfBusiness) {
                        // Cable
                        case strtolower(Dyna_Customer_Model_Client_RetrieveCustomerInfo::NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE):
                            if (in_array($nonOsfOrder['legacy_customer_id'], $serviceCustomerNumbers[Dyna_Customer_Helper_Services::getStackPerPackage($packageTypes)])) {
                                if ($nonOsfOrder['kip_category'] == 'true' && in_array(strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE), $packageTypes)) {
                                    return true;
                                }
                                if ($nonOsfOrder['tv_category'] == 'true' && in_array(strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV), $packageTypes)) {
                                    return true;
                                }
                                if ($nonOsfOrder['cls_category'] == 'true' && in_array(strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT), $packageTypes)) {
                                    return true;
                                }
                            }
                            break;
                        // Mobile
                        case strtolower(Dyna_Customer_Model_Client_RetrieveCustomerInfo::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE):
                            if (in_array($nonOsfOrder['legacy_customer_id'], $serviceCustomerNumbers[Dyna_Customer_Helper_Services::getStackPerPackage($packageTypes)])) {
                                if (is_null($ctn) ||
                                    ($ctn && (strpos($ctn, $nonOsfOrder['subscriber_number']) !== false))
                                ) {
                                    /** @var Dyna_Customer_Model_ActivityCodes $activityCode */
                                    $activityCode = Mage::getModel('dyna_customer/activityCodes')->load($nonOsfOrder['future_activity_code'], 'activity_code');
                                    if ($activityCode->getEntityId()) {
                                        return $activityCode->getActivityCode();
                                    }
                                }
                            }
                            break;
                        // DSL
                        case strtolower(Dyna_Customer_Model_Client_RetrieveCustomerInfo::NON_OSF_ORDER_LINE_OF_BUSINESS_DSL):
                            return true;
                    }
                }
            }
        }

        return false;
    }


    public function retrieveFutureReasonCode($packageTypes)
    {
        $futureReasonCode = '';
        $customerPanelHelper = Mage::helper('dyna_customer/panel');
        $nonOsfOrders = $customerPanelHelper->getServiceCustomerOrders();
        foreach ($packageTypes as $packageType) {
            $lineOfBusinesses[] = strtolower($customerPanelHelper->getLineOfBusinessForPackageType($packageType));
        }
        $lineOfBusinesses = array_unique($lineOfBusinesses);
        foreach ($lineOfBusinesses as $lineOfBusiness) {
            if (isset($nonOsfOrders[$lineOfBusiness]) && count($nonOsfOrders[$lineOfBusiness])) {
                foreach ($nonOsfOrders[$lineOfBusiness] as $nonOsfOrder) {
                    // For FN stack does NOT matter customer number, it just blocks everything
                    if ($lineOfBusiness == strtolower(Dyna_Customer_Model_Client_RetrieveCustomerInfo::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE)) {
                        $futureReasonCode = $nonOsfOrder['future_reason_code'];
                    }
                }
            }
        }
        return $futureReasonCode;
    }

    /**
     * @param null $customerNumber
     * @param $packageTypes
     * @param null $ctn
     * @param null $serviceLineId
     * @return bool
     */
    public function hasOSFOpenOrders($customerNumber = null, $packageTypes, $ctn = null, $serviceLineId = null)
    {
        if (!$packageTypes) {
            return false;
        }

        return $this->getOSFOpenOrders($customerNumber, $packageTypes, $ctn, $serviceLineId)->getSize() > 0;
    }

    /**
     * Clear the session, but leave the quote active
     */
    public function clearCustomerInfo()
    {
        Mage::getSingleton('checkout/session')->clear();
        Mage::getSingleton('checkout/session')->unsetAll();
        // Unload UCT data
        $this->getCustomerSession()->unsUctParams();
        // Unset RetrieveCustomerInfo data
        $this->getCustomerSession()->unsServiceCustomers();

        // Unset Campaign Offer Product Compatibility checks
        $this->getCustomerSession()->unsOfferCompatibility();

        /** Clear serviceability data from session when unloading customer */
        Mage::getSingleton('dyna_address/storage')->clearAddress();

        // Clear session migration / move offnet
        $this->clearMigrationData();

        $this->getCustomerSession()->unsetCustomer();
        $this->getCustomerSession()->unsOrderEdit();
        $this->getCustomerSession()->unsOrderEditMode();
        $this->getCustomerSession()->unsNewEmail();
        $this->getCustomerSession()->unsWelcomeMessage();
        $this->getCustomerSession()->unsProspect();
        $this->getCheckoutSession()->unsIsEditMode();
        $this->getCheckoutSession()->setCustomerInfoSync(false);
        $this->getCheckoutSession()->clear();

        /** Clear active bundle */
        Mage::getSingleton('dyna_customer/session')->unsActiveBundleId();
    }


    public function unloadCustomer()
    {
        /** @var Omnius_Checkout_Model_Sales_Quote $theQuote */
        $theQuote = Mage::getSingleton('checkout/cart')->getQuote();
        $theQuote->setIsActive(false)->save();

        // Unset RetrieveCustomerInfo data
        $this->getCustomerSession()->unsServiceCustomers();
        $this->getDynaCustomerSession()->unsCustomerOrders();

        // Unset Campaign Offer Product Compatibility checks
        $this->getCustomerSession()->unsOfferCompatibility();
        $this->getCustomerSession()->unsKiasLinkedAccountNumber();
        $this->getCustomerSession()->unsKdLinkedAccountNumber();
        $this->getCustomerSession()->unsSubscriberCtn();
        $this->getCustomerSession()->clearCustomerOrders();

        $this->getCustomerSession()->setData('household_members_count', 0);

        /** Clear serviceability data from session when unloading customer */
        Mage::getSingleton('dyna_address/storage')->clearAddress();

        // Clear session migration / move offnet
        $this->clearMigrationData();

        $this->getCustomerSession()->unsetCustomer();
        $this->getCustomerSession()->unsOrderEdit();
        $this->getCustomerSession()->unsOrderEditMode();
        $this->getCustomerSession()->unsNewEmail();
        $this->getCustomerSession()->unsWelcomeMessage();
        $this->getCustomerSession()->unsProspect();
        $this->getCheckoutSession()->unsIsEditMode();
        $this->getCheckoutSession()->setCustomerInfoSync(false);
        $this->getCheckoutSession()->clear();

        // Clear active bundle
        $this->getCustomerSession()->unsActiveBundleId();

        // Clear customer products
        $this->getCustomerSession()->clearCustomerProducts();
        $this->getCustomerSession()->setData('all_installed_base_products', null);
    }

    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }


    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getDynaCustomerSession()
    {
        return Mage::getSingleton('dyna_customer/session');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * @param Dyna_Agent_Model_Agent|Mage_Core_Model_Abstract $savedOfferAgent
     * @return bool
     */
    public function hasPermissionLoadOffer($savedOfferAgent)
    {
        $currentAgent = Mage::getSingleton('customer/session')->getAgent();
        if (($savedOfferAgent->getDealerId() == $currentAgent->getDealerId() &&
                Mage::getSingleton('customer/session')->getAgent()->isGranted(array('CHANGE_SAVED_OFFER_DEALER'))) ||
            Mage::getSingleton('customer/session')->getAgent()->isGranted(array('CHANGE_SAVED_OFFER'))
        ) {
            return true;
        }

        return false;
    }

    /**
     * Set migration data on current session
     * @param $migrationType
     * @param $customerNumber
     * @param $entityId
     * @return $this
     */
    public function setMigrationData($migrationType, $customerNumber, $entityId)
    {
        $cartSession = Mage::getSingleton('checkout/session');
        $cartSession->setInMigrationOrMoveOffnetState($migrationType)
            ->setMigrationOrMoveOffnetCustomerNo($customerNumber)
            ->setMigrationPackage($entityId);

        return $this;
    }

    /**
     * Clear all references to migration from cart session
     * @return $this
     */
    public function clearMigrationData()
    {
        $this->setMigrationData(null, null, null);
        return $this;
    }

    /**
     * @param null $customerNumber
     * @param $packageTypes
     * @param null $ctn
     * @param null $serviceLineId
     * @return Omnius_Superorder_Model_Mysql4_Superorder_Collection
     */
    public function getOSFOpenOrders($customerNumber = null, $packageTypes, $ctn = null, $serviceLineId = null)
    {
        if (!is_array($packageTypes)) {
            $packageTypes = [$packageTypes];
        }
        $packageTypes = array_map('strtolower', $packageTypes);

        $serviceCustomerNumbers = $this->getServiceCustomerNumbersForEachStack($customerNumber);

        $custNumbers = [];

        foreach ( array_unique($packageTypes) as $packageType ) {
            $custNumbers = array_merge($custNumbers, $serviceCustomerNumbers[Dyna_Customer_Helper_Services::getStackPerPackage($packageType)]);
        }

        $sessionCustomerNumber = Mage::getSingleton('customer/session')->getCustomer()->getCustomerNumber();

        // Check db Orders
        /** @var Omnius_Superorder_Model_Mysql4_Superorder_Collection $superOrders */
        $superOrders = Mage::getModel('superorder/superorder')->getCollection()
            ->addFieldToSelect('entity_id')
            ->addFieldToFilter('main_table.order_status', array('neq' => Dyna_Superorder_Model_Superorder::SO_COMPLETED_SUCCESS))
            ->addFieldToFilter('main_table.order_status', array('neq' => Dyna_Superorder_Model_Superorder::SO_COMPLETED_FAILED))
            ->addFieldToFilter('main_table.customer_number', array('eq' => $sessionCustomerNumber));

        $superOrders
            ->getSelect()
            ->group('main_table.entity_id' )
            ->join(array("package" => "catalog_package"),"main_table.entity_id = package.order_id", array())
            ->where('package.type IN(?)', $packageTypes);

        if (empty($custNumbers)) {
            $custNumbers = Mage::getSingleton('customer/session')->getCustomer()->getCustomerNumber();
        }

        if ($custNumbers) {
            if (!is_array($custNumbers)) {
                $custNumbers = (array) $custNumbers;
            }
            $superOrders
                ->getSelect()
                ->where('package.parent_account_number IN (?) OR package.parent_account_number IS NULL' , array_map('strval', $custNumbers));
        }

        if (!is_null($ctn)) {
            $superOrders
                ->getSelect()
                ->where('package.ctn = ?', $ctn);
        }

        if (!is_null($serviceLineId)) {
            $superOrders
                ->getSelect()
                ->where('package.service_line_id = ?', $serviceLineId);
        }

        return $superOrders;
    }

    /**
     * Placeholder method to implement identities mapping
     *
     * @param string $type
     * @param bool $flip
     * @return string
     */
    public function mapCustomerIDType(string $type, bool $flip = false): string
    {
        return $type;
    }
}
