<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
$installer = $this;
$installer->startSetup();
$installer->getConnection()->changeColumn('login_details', 'ip', 'ip', 'VARCHAR(50)');
$installer->endSetup();
