<?php

/**
 * Class Dyna_Job_Helper_Data
 */
class Dyna_Job_Helper_Data
{
    /** @var  Dyna_Job_Helper_PHPBinaryFinder */
    protected $phpFinder;

    public function __construct()
    {
        $this->phpFinder = Mage::helper('dyna_job/pHPBinaryFinder');
    }

    /**
     * @return Dyna_Job_Model_Publisher|false|Mage_Core_Model_Abstract
     */
    public function buildPublisher(): Dyna_Job_Model_Publisher
    {

        $args = [
            'config' => Mage::getModel('dyna_job/config_adminConfigBuilder')->build($this->phpFinder)
        ];

        $publisher = Mage::getModel('dyna_job/publisher', $args);

        return $publisher;
    }

    /**
     * @return Dyna_Job_Model_Consumer
     */
    public function buildConsumer()
    {
        $consumer = Mage::getModel('dyna_job/standaloneConsumer', [
            'dispatcher' => Mage::getModel('dyna_job/dispatcher'),
            'config' => Mage::getModel('dyna_job/config_adminConfigBuilder')->build($this->phpFinder),
            'messageTranslator' => Mage::getModel('dyna_job/messageTranslator'),
        ]);

        return $consumer;
    }

    public function buildStandaloneRunner(): Dyna_Job_Model_StandaloneJobRunner
    {
        $consumer = Mage::getModel('dyna_job/standaloneJobRunner', [
            'dispatcher' => Mage::getModel('dyna_job/dispatcher'),
            'config' => Mage::getModel('dyna_job/config_adminConfigBuilder')->build($this->phpFinder),
            'messageTranslator' => Mage::getModel('dyna_job/messageTranslator'),
        ]);

        return $consumer;
    }

}