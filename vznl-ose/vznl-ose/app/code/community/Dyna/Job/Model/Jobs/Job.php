<?php

/**
 * Class Dyna_Job_Model_Job
 */
abstract class Dyna_Job_Model_Jobs_Job implements JsonSerializable
{
    const JOB_TYPE = 'none';
    /**
     * @var array
     */
    protected $data;

    protected $retries = 0;

    public function getType()
    {
        return static::JOB_TYPE;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData(array $data = [])
    {
        $this->data = $data;
    }

    public function getRetries()
    {
        return $this->retries;
    }

    public function incrementRetries()
    {
        $this->retries++;
    }

    public function setRetries($retries)
    {
        $this->retries = $retries;
    }

    public function jsonSerialize()
    {
        return [
            'type' => $this->getType(),
            'data' => $this->getData(),
            'retries' => $this->getRetries(),
        ];
    }

    public function getUniqueID()
    {
        return md5(json_encode([
            'type' => $this->getType(),
            'data' => $this->getData(),
        ]));
    }
}