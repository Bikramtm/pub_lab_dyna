<?php

class Dyna_Job_Model_Config_AdminConfigBuilder
{
    /**
     * @param Dyna_Job_Helper_PHPBinaryFinder $phpFinder
     * @return Dyna_Job_Model_Config_Config
     */
    public function build(Dyna_Job_Helper_PHPBinaryFinder $phpFinder): Dyna_Job_Model_Config_Config
    {
        $data = [
            'hostname' => Mage::getStoreConfig('omnius_service/job_queue/amqp_host') ?? '',
            'port' => Mage::getStoreConfig('omnius_service/job_queue/amqp_port') ?? '',
            'pass' => Mage::getStoreConfig('omnius_service/job_queue/amqp_pass') ?? '',
            'user' => Mage::getStoreConfig('omnius_service/job_queue/amqp_user') ?? '',
            'vhost' => Mage::getStoreConfig('omnius_service/job_queue/amqp_vhost') ?? '',
            'queue' => Mage::getStoreConfig('omnius_service/job_queue/amqp_queue') ?? '',
            'exchange' => Mage::getStoreConfig('omnius_service/job_queue/amqp_exchange') ?? '',
            'maxRetries' => Mage::getStoreConfig('omnius_service/job_queue/amqp_retries') ?? '',
            'secondsBetweenRetries' => Mage::getStoreConfig('omnius_service/job_queue/amqp_seconds_between_retries') ?? '',
            'clearJobsOlderThan' => 86400,
            'delayQueue' => Mage::getStoreConfig('omnius_service/job_queue/amqp_retry_queue') ?? '',
            'phpPath' => Mage::getStoreConfig('omnius_service/job_queue/php_path') ?: $phpFinder->find(),
            'amqpSSL' => Mage::getStoreConfig('omnius_service/job_queue/amqp_ssl') ?? false,
            'amqpVerifyPeer' => Mage::getStoreConfig('omnius_service/job_queue/amqpp_verify_peer') ?? false,
            'amqpAllowSelfSigned' => Mage::getStoreConfig('omnius_service/job_queue/amqp_allow_self_signed') ?? false,
        ];

        return new Dyna_Job_Model_Config_Config($data);
    }
}