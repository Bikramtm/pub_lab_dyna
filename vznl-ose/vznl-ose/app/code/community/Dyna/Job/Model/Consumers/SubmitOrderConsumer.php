<?php

class Dyna_Job_Model_Consumers_SubmitOrderConsumer implements Dyna_Job_Model_Consumers_ConsumerInterface
{
    /**
     * @param Dyna_Job_Model_Jobs_Job $job
     * @return bool
     */
    public function canProcess(Dyna_Job_Model_Jobs_Job $job): bool
    {
        return $job->getType() == 'submitOrder';
    }

    public function process(Dyna_Job_Model_Jobs_Job $job): bool
    {
        $order = Mage::getModel('dyna_checkout/order');
        try {
            $order->createOrder($job->getData());
        } catch (Exception $e) {
            //cleanup
            Mage::getSingleton('checkout/session')->clear();
            Mage::getSingleton('checkout/session')->unsetAll();
            Mage::getSingleton('core/session')->clear();
            Mage::getSingleton('customer/session')->clear();
            Mage::getSingleton('dyna_address/storage')->clear();
            Mage::helper('dyna_customer')->unloadCustomer();
            throw $e;
        }

        return true;
    }
}