<?php

class Dyna_Job_Model_Config_XmlConfigBuilder extends Varien_Simplexml_Config
{
    public function __construct()
    {
        parent::__construct($this->_getConfigFile());
    }

    /**
     * @return string
     */
    protected function _getConfigFile()
    {
        return Mage::getBaseDir('etc') . DS . 'job_config.xml';
    }

    public function build(): Dyna_Job_Model_Config_Config
    {
        $configRoot = $this->getConfigRoot();
        if ($configRoot->isEmpty()) {
            throw new Exception(sprintf('Config could not be loaded. job_config.xml not found at %s or invalid', $this->_getConfigFile()));
        }

        $data = [
            'hostname' => $configRoot->getData('hostname'),
            'port' => $configRoot->getData('port'),
            'pass' => $configRoot->getData('password'),
            'user' => $configRoot->getData('user'),
            'vhost' => $configRoot->getData('vhost'),
            'queue' => $configRoot->getData('queue'),
            'exchange' => $configRoot->getData('exchange'),
            'maxRetries' => $configRoot->getData('maxRetries'),
            'secondsBetweenRetries' => $configRoot->getData('secondsBetweenRetries'),
            'clearJobsOlderThan' => 86400,
            'delayQueue' => $configRoot->getData('retryQueue'),
            'phpPath' => '',
        ];

        $config = new Dyna_Job_Model_Config_Config($data);

        return $config;
    }

    public function getConfigRoot()
    {
        if ($this->_config) {
            return $this->_config;
        }

        $xml = $this->getNode();

        if ($xml) {
            $this->_config = new Varien_Object($xml->asArray());
        } else {
            $this->_config = new Varien_Object();
        }

        return $this->_config;
    }
}