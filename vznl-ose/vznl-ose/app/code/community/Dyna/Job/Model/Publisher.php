<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Dyna_Job_Model_Publisher
{
    use Dyna_Job_Model_LoggerTrait;

    /** @var  Dyna_Job_Model_Config_Config */
    protected $config;

    public function __construct($arguments)
    {
        if (isset($arguments['config']) && $arguments['config'] instanceof Dyna_Job_Model_Config_Config) {
            $this->config = $arguments['config'];
        } else {
            throw new InvalidArgumentException('Dyna_Job_Model_Publisher requires an instance of Dyna_Job_Model_Config');
        }
    }

    public function publish(Dyna_Job_Model_Jobs_Job $job)
    {
        try {
            if ($this->config->isAmqpSSL()) {
                $streamContext = [
                    'verify_peer' => (bool)$this->config->isAmqpVerifyPeer(),
                    'allow_self_signed' => (bool)$this->config->isAmqpAllowSelfSigned(),
                    'verify_peer_name' => false,
                ];
                $connection = new \PhpAmqpLib\Connection\AMQPSSLConnection($this->config->getHostname(),
                    $this->config->getPort(), $this->config->getUser(), $this->config->getPass(),
                    $this->config->getVhost(), $streamContext);
            } else {
                $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($this->config->getHostname(),
                    $this->config->getPort(), $this->config->getUser(), $this->config->getPass(),
                    $this->config->getVhost());
            }
            $channel = $connection->channel();
            $channel->exchange_declare($this->config->getExchange(), 'direct', false, true, false);
            $channel->queue_declare($this->config->getQueue(), false, true, false, false);
            $channel->queue_bind($this->config->getQueue(), $this->config->getExchange());

            $dbRepository = Mage::getModel('dyna_job/jobRepository_database');
            $jobIdentifier = $dbRepository->store($job);

            $message = new AMQPMessage($jobIdentifier,
                array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
            $channel->basic_publish($message, $this->config->getExchange(), $this->config->getQueue());
            $this->log(sprintf('Published job with id %s', $job->getUniqueID()), Zend_Log::DEBUG);
            $channel->close();
            $connection->close();
            return true;
        } catch (Exception $e) {
            $this->log(sprintf('Could not publish job %s : Exception %s', json_encode($job), $e->getMessage()),
                Zend_Log::EMERG);
            throw new Exception(Mage::helper('checkout')->__('Could not publish job'));
        }

        return false;
    }
}