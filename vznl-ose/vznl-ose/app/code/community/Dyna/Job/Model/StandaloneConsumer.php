<?php

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

/**
 * Retrieves queue messages and invokes a php script to handle each message.
 * Class Dyna_Job_Model_StandaloneConsumer
 */
class Dyna_Job_Model_StandaloneConsumer
{
    use Dyna_Job_Model_LoggerTrait;

    /** @var  Dyna_Job_Model_Dispatcher */
    protected $dispatcher;
    /** @var  Dyna_Job_Model_Config_Config */
    protected $config;
    /**
     * @var \AMQPChannel
     */
    protected $channel;
    /**
     * @var \AMQPChannel
     */
    protected $retryChannel;
    /**
     * @var Dyna_Job_Model_MessageTranslator
     * Converts JSON encoded jobs to Dyna_Job_Model_Jobs_Job
     */
    protected $translator;

    /**
     * Dyna_Job_Model_Consumer constructor.
     * @param $arguments
     */
    public function __construct($arguments)
    {
        if (isset($arguments['dispatcher']) && $arguments['dispatcher'] instanceof Dyna_Job_Model_Dispatcher) {
            $this->dispatcher = $arguments['dispatcher'];
        } else {
            throw new InvalidArgumentException('Dyna_Job_Model_Consumer requires a Dyna_Job_Model_Dispatcher argument');
        }

        if (isset($arguments['config']) && $arguments['config'] instanceof Dyna_Job_Model_Config_Config) {
            $this->config = $arguments['config'];
        } else {
            throw new InvalidArgumentException('Dyna_Job_Model_Consumer requires a Dyna_Job_Model_Config_Config argument');
        }

        if (isset($arguments['messageTranslator']) && $arguments['messageTranslator'] instanceof Dyna_Job_Model_MessageTranslator) {
            $this->translator = $arguments['messageTranslator'];
        } else {
            throw new InvalidArgumentException('Dyna_Job_Model_Consumer requires a Dyna_Job_Model_MessageTranslator argument');
        }

    }

    /**
     * Path to the standalone job runner script
     * @return string
     */
    protected function getRunnerPath()
    {
        return 'job_processor_runner.php';
    }


    public function consume()
    {
        try {
            $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($this->config->getHostname(), $this->config->getPort(), $this->config->getUser(), $this->config->getPass(), $this->config->getVhost());
            $this->channel = $connection->channel();
            $this->retryChannel = $connection->channel();

            $this->channel->exchange_declare($this->config->getExchange(), 'direct', false, true, false);
            $this->channel->queue_declare($this->config->getQueue(), false, true, false, false);
            $this->channel->queue_bind($this->config->getQueue(), $this->config->getExchange(), $this->config->getQueue());
            $this->channel->basic_qos(null, 2, null);
            $this->channel->basic_consume($this->config->getQueue(), '', false, false, false, false, array($this, 'processMessage'));


            $this->retryChannel->exchange_declare($this->config->getExchange(), 'direct', false, true, false);
            $this->retryChannel->queue_declare($this->config->getDelayQueue(), false, true, false, false, false
                , new AMQPTable(array(
                    "x-dead-letter-exchange" => $this->config->getExchange(),
                    "x-dead-letter-routing-key" => $this->config->getQueue(),
                    "x-message-ttl" => $this->config->getSecondsBetweenRetries() * 1000,
                ))
            );
            $this->retryChannel->queue_bind($this->config->getDelayQueue(), $this->config->getExchange(), $this->config->getDelayQueue());

            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }

            $this->channel->close();
            $connection->close();
        } catch (Exception $e) {
            $this->log(sprintf('Consumer died: %s', $e->getMessage()), Zend_Log::ALERT);
        }

    }

    /**
     *
     * @param $msg
     */
    public function processMessage($msg)
    {
        $output = [];
        $messageContent = $msg->body;
        exec(sprintf("%s %s --job %s 2>&1", $this->config->getPhpPath(), $this->getRunnerPath(), base64_encode($messageContent)), $output, $returnCode);
        $fullOutput = implode(PHP_EOL, $output);
        $shouldAck = true;
        switch ($returnCode) {
            case Dyna_Job_Model_StandaloneJobRunner::INVALID_JOB:
                $this->log($fullOutput, Zend_Log::ALERT);
                break;
            case 0:
                $this->log($fullOutput, Zend_Log::DEBUG);
                break;
            default:
                // determine if job should be republished or not
                $job = $this->translator->setMessage($messageContent)->translateToJob();
                try {
                    $job->incrementRetries();

                    if ($job->getRetries() <= $this->config->getMaxRetries() || $this->config->getMaxRetries() == -1) {
                        $this->log(sprintf('Job %s could not be consumed  with output %s and error code %s, republishing to retry queue', $job->getUniqueID(), $fullOutput, $returnCode) , Zend_Log::ERR);
                        $this->republishToRetryQueue($job);
                        $this->log(sprintf('Job %s was republished to retry queue', $job->getUniqueID()), Zend_Log::ERR);
                    } else {
                        $this->log(sprintf('Job %s could not be consumed with output %s and error code %s', $job->getUniqueID(), $fullOutput, $returnCode),  Zend_Log::ERR);
                        $this->log(sprintf('Job %s reached maximum retries, will not be republished.', json_encode($job)), Zend_Log::EMERG);
                    }
                } catch (Exception $e) {
                    $this->log(sprintf('Job %s could not be republished with error %s, not acking .', json_encode($job), $e->getMessage()), Zend_Log::EMERG);
                    $msg->delivery_info['channel']->basic_nack($msg->delivery_info['delivery_tag']);
                    $shouldAck = false;
                }


                break;
        }

        if ($shouldAck) {
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        }
    }


    /**
     * @param Dyna_Job_Model_Jobs_Job $job
     * @internal param $msg
     */
    protected function republishToRetryQueue(Dyna_Job_Model_Jobs_Job $job)
    {
        $messageBody = json_encode($job);
        $message = new AMQPMessage($messageBody, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
        $this->retryChannel->basic_publish($message, $this->config->getExchange(), $this->config->getDelayQueue());
    }
}