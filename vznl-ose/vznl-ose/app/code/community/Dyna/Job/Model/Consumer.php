<?php

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class Dyna_Job_Model_Consumer
{
    use Dyna_Job_Model_LoggerTrait;

    /** @var  Dyna_Job_Model_Dispatcher */
    protected $dispatcher;
    /** @var  Dyna_Job_Model_Config_Config */
    protected $config;
    /**
     * @var \AMQPChannel
     */
    protected $channel;

    /**
     * Dyna_Job_Model_Consumer constructor.
     * @param $arguments
     */
    public function __construct($arguments)
    {
        if (isset($arguments['dispatcher']) && $arguments['dispatcher'] instanceof Dyna_Job_Model_Dispatcher) {
            $this->dispatcher = $arguments['dispatcher'];
        } else {
            throw new InvalidArgumentException('Dyna_Job_Model_Consumer requires a Dyna_Job_Model_Dispatcher argument');
        }

        if (isset($arguments['config']) && $arguments['config'] instanceof Dyna_Job_Model_Config_Config) {
            $this->config = $arguments['config'];
        } else {
            throw new InvalidArgumentException('Dyna_Job_Model_Consumer requires a Dyna_Job_Model_Config_Config argument');
        }
    }

    public function consume()
    {
        try {
            $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($this->config->getHostname(), $this->config->getPort(), $this->config->getUser(), $this->config->getPass(), $this->config->getVhost());
            $this->channel = $connection->channel();
            $this->channel->exchange_declare($this->config->getExchange(), 'direct', false, true, false);
            $this->channel->queue_declare($this->config->getQueue(), false, true, false, false);
            $this->channel->queue_bind($this->config->getQueue(), $this->config->getExchange(), $this->config->getQueue());
            $this->channel->basic_consume($this->config->getQueue(), '', false, false, false, false, array($this, 'processMessage'));

            $this->channel->queue_declare($this->config->getDelayQueue(), false, true, false, false, false
                , new AMQPTable(array(
                    "x-dead-letter-exchange" => $this->config->getExchange(),
                    "x-dead-letter-routing-key" => $this->config->getQueue(),
                    "x-message-ttl" => $this->config->getSecondsBetweenRetries() * 1000,
                ))
            );
            $this->channel->queue_bind($this->config->getDelayQueue(), $this->config->getExchange(), $this->config->getDelayQueue());

            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }

            $this->channel->close();
            $connection->close();
        } catch (Exception $e) {
            $this->log(sprintf('Consumer died: %s', $e->getMessage()), Zend_Log::ALERT);
        }

    }

    /**
     *
     * @param $msg
     */
    public function processMessage($msg)
    {

        try {
            $job = $this->convertMsgToJob($msg);
        } catch (Exception $e) {
            $this->log(sprintf('Message is not a valid job, acking and skipping it: %s', $msg->body), Zend_Log::ALERT);
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            return;
        }

        $jobId = $job->getUniqueID();

        try {
            $jobWasProcessed = $this->dispatcher->dispatch($job);
        } catch (Exception $e) {
            $this->log(sprintf('Failed to consume job %s with error %s', $jobId, $e->getMessage()), Zend_Log::WARN);
            $jobWasProcessed = false;
        }

        $republishedMessage = true;
        try {
            if (!$jobWasProcessed) {
                $job->incrementRetries();
                if ($job->getRetries() <= $this->config->getMaxRetries() || $this->config->getMaxRetries() == 0) {
                    $this->log(sprintf('Job %s could not be consumed, republishing to retry queue', $jobId), Zend_Log::WARN);
                    $this->republishToRetryQueue($job);
                    $this->log(sprintf('Job %s was republished to retry queue', $jobId), Zend_Log::WARN);
                } else {
                    $this->log(sprintf('Job %s could not be consumed', $jobId), Zend_Log::WARN);
                    $this->log(sprintf('Job %s reached maximum retries, will not be republished.', json_encode($job)), Zend_Log::EMERG);
                }

            } else {
                $this->log(sprintf('Job %s consumed.', $jobId), Zend_Log::DEBUG);
            }
        } catch (Exception $e) {
            $republishedMessage = false;
            $this->log(sprintf('Job %s could not be republished with error %s, not acking .', $jobId, $e->getMessage()), Zend_Log::EMERG);
        }
        if ($jobWasProcessed || $republishedMessage) {
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        }
    }

    /**
     * @param $msg
     * @return Dyna_Job_Model_Jobs_Job
     */
    protected function convertMsgToJob($msg): Dyna_Job_Model_Jobs_Job
    {
        $msgData = json_decode($msg->body, true);

        switch ($msgData['type']) {
            case Dyna_Job_Model_Jobs_SubmitOrder::JOB_TYPE:
                $job = Mage::getModel('dyna_job/jobs_submitOrder');
                break;
            default:
                throw new InvalidArgumentException('Message could not be converted to a job');
        }

        $job->setData($msgData['data']);
        $job->setRetries($msgData['retries']);

        return $job;
    }

    /**
     * @param Dyna_Job_Model_Jobs_Job $job
     * @internal param $msg
     */
    protected function republishToRetryQueue(Dyna_Job_Model_Jobs_Job $job)
    {
        $messageBody = json_encode($job);
        $message = new AMQPMessage($messageBody, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
        $this->channel->basic_publish($message, $this->config->getExchange(), $this->config->getDelayQueue());
    }
}