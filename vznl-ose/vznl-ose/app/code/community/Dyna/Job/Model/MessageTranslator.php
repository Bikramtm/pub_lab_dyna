<?php

class Dyna_Job_Model_MessageTranslator
{
    /** @var  string JSON Encoded Job Message */
    protected $message;

    public function __construct($args)
    {
        if ($args['message'] && is_string($args['message'])) {
            $this->message = $args['message'];
        }
    }

    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }

    public function translateToJob(): Dyna_Job_Model_Jobs_Job
    {
        if (!$this->message) {
            throw new InvalidArgumentException('No message provided');
        }
        $msgData = json_decode($this->message, true);

        switch ($msgData['type']) {
            case Dyna_Job_Model_Jobs_SubmitOrder::JOB_TYPE:
                $job = Mage::getModel('dyna_job/jobs_submitOrder');
                break;
            default:
                throw new InvalidArgumentException('Message could not be converted to a job');
        }

        $job->setData($msgData['data']);
        $job->setRetries($msgData['retries']);

        return $job;
    }
}