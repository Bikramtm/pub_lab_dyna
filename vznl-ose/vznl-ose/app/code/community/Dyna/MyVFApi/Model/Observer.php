<?php

class Dyna_MyVFApi_Model_Observer
{
    protected $credisClient;

    /**
     * Dyna_MyVFApi_Model_Observer constructor.
     * @param $arguments
     */
    public function __construct($arguments)
    {
        if (isset($arguments['credisClient'])) {
            $this->credisClient = $arguments['credisClient'];
        } else {
            $this->credisClient = Mage::helper('dyna_myvfapi/credis')->buildCredisClient();
        }

    }

    /**
     * Event: dyna_myvfapi_projection_created
     * @param Varien_Event_Observer $observer
     */
    public function onProjectionCreatedUpdateCustomerData(Varien_Event_Observer $observer)
    {
        /** @var Dyna_MyVFApi_Model_Projection $projection */
        $projection = $observer->getProjection();

        $ban = $projection->getBan();
        $ctns = $projection->getCtn();

        $campaign = $projection->getCampaign();
        if ($campaign) {
            $campaignRedisKey = Dyna_MyVFApi_Model_Service_Projection::TAG_CAMPAIGN . $campaign;
            $this->addToCollection($campaignRedisKey, $projection->getId());
        }

        $banRedisKey = Dyna_MyVFApi_Model_Service_Projection::TAG_BAN . $ban;

        $this->addToCollection($banRedisKey, $projection->getId());

        $ctns = (array)$ctns;

        foreach ($ctns as $ctn) {
            $ctnRedisKey = Dyna_MyVFApi_Model_Service_Projection::TAG_CUSTOMER_NO . $ctn;
            $this->addToCollection($ctnRedisKey, $projection->getId());
            $ctnGSMRedisKey = Dyna_MyVFApi_Model_Service_Projection::TAG_CUSTOMER_NO . 'GSM' . substr($ctn, 2);
            $this->addToCollection($ctnGSMRedisKey, $projection->getId());
        }
    }

    /**
     * Event: dyna_myvfapi_projection_deleted
     * @param $observer
     */
    public function onProjectionDeletedUpdateCustomerData(Varien_Event_Observer $observer)
    {
        /** @var Dyna_MyVFApi_Model_Projection $projection */
        $projection = $observer->getProjection();

        $ban = $projection->getBan();
        $ctns = $projection->getCtn();

        $banRedisKey = Dyna_MyVFApi_Model_Service_Projection::TAG_BAN . $ban;
        $this->deleteFromCollection($banRedisKey, $projection->getId());

        if($projection->getCampaign()) {
            $campaignRedisKey = Dyna_MyVFApi_Model_Service_Projection::TAG_CAMPAIGN . $projection->getCampaign();
            $this->deleteFromCollection($campaignRedisKey, $projection->getId());
        }

        $ctns = (array)$ctns;
        foreach ($ctns as $ctn) {
            $ctnRedisKey = Dyna_MyVFApi_Model_Service_Projection::TAG_CUSTOMER_NO . $ctn;
            $this->deleteFromCollection($ctnRedisKey, $projection->getId());
            $ctnGSMRedisKey = Dyna_MyVFApi_Model_Service_Projection::TAG_CUSTOMER_NO . 'GSM' . substr($ctn, 2);
            $this->deleteFromCollection($ctnGSMRedisKey, $projection->getId());
        }
    }

    public function onCampaignSavedUpdateValidity(Varien_Event_Observer $observer)
    {
        $model = $observer->getObject();
        if ($model instanceof Dyna_Bundles_Model_Campaign) {
            $campaign = $model;
            $offerKeys = $this->getCollection(Dyna_MyVFApi_Model_Service_Projection::TAG_CAMPAIGN . $campaign->getCampaignId());
            foreach ($offerKeys as $offerId) {
                $offerRedisKey = Dyna_MyVFApi_Model_Service_Projection::TAG_OFFERS . $offerId;

                $offer = unserialize($this->credisClient->get($offerRedisKey), []);
                $offer['campaignValidityDate'] = $campaign->getValidTo();
                $this->credisClient->set($offerRedisKey, serialize($offer));
            }
        }
    }

    /** @TODO: Extract
     * @param string $collection
     * @param $value
     * @return array|mixed
     */
    protected function addToCollection(string $collection, $value)
    {
        $values = unserialize($this->credisClient->get($collection), []) ?: [];
        if (!in_array($value, $values, true)) {
            $values[] = $value;
            $this->credisClient->set($collection, serialize($values));
        }

        return $values;
    }

    protected function deleteFromCollection($collection, $value)
    {
        $values = unserialize($this->credisClient->get($collection), []);
        if ($values) {
            $key = array_search($value, $values, true);
            if ($key !== FALSE) {
                unset($values[$key]);
                $this->credisClient->set($collection, serialize($values));
            }
        }

        return $values;
    }

    protected function getCollection(string $collection)
    {
        $values = unserialize($this->credisClient->get($collection), []) ?: [];

        return $values;
    }


}