<?php

/**
 * Class Dyna_Order_Helper_Data
 */
class Dyna_Order_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * It has to be implemented per project
     *
     * @param string $orderId
     * @return string
     */
    public function submitOrder(string $orderId)
    {
        return $orderId;
    }
}
