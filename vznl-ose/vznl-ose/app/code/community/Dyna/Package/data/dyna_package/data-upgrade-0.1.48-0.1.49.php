<?php
/**
 * set the default values of all package subtypes to be visible in the entire application
 */
$this->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();
$sql = "update `catalog_package_subtypes` SET `package_subtype_visibility` = 'inventory,configurator,shopping_cart,extended_shopping_cart,order_overview,voice_log,offer_pdf,call_summary'";


$connection->query($sql);

$this->endSetup();

