<?php

require_once Mage::getModuleDir('controllers', 'Omnius_Package') . DS . '/Adminhtml/PackageController.php';

/**
 * Class Dyna_Package_Adminhtml_PackageController
 */
class Dyna_Package_Adminhtml_PackageController extends Omnius_Package_Adminhtml_PackageController
{
    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            $model = Mage::getModel("package/packageSubtype");
            if (isset($postData['package_subtype_visibility'])) {
                $trimmedPackageSubtypeVisibilityValues = [];
                foreach ($postData['package_subtype_visibility'] as $visibilityValue) {
                    $trimmedPackageSubtypeVisibilityValues[] = trim($visibilityValue);
                }
                $postData['package_subtype_visibility'] = implode(',', $trimmedPackageSubtypeVisibilityValues);
            } else {
                $postData['package_subtype_visibility'] = null;
            }

            if ($this->getRequest()->getParam("id")) {
                $existingOption = $model->load($this->getRequest()->getParam("id"));
                $existingOption->addData($postData);

                $existingOption->save();
                $contextCardinalities = $this->getRequest()->getParam('context_cardinality');

                foreach ($contextCardinalities as $contextId => $cardinality) {
                    /** @var Dyna_Package_Model_PackageSubtypeCardinality $cardinalityModel */
                    $cardinalityModel = Mage::getModel('dyna_package/packageSubtypeCardinality')
                        ->getCollection()
                        ->addFieldToFilter('package_subtype_id', $this->getRequest()->getParam('id'))
                        ->addFieldToFilter('process_context_id', $contextId)
                        ->getFirstItem();

                    if ($cardinalityModel->getId()) {
                        $cardinalityModel->setData('cardinality', $cardinality);

                        $cardinalityModel->save();
                    } else {
                        /** @var Dyna_Package_Model_PackageSubtypeCardinality $newSubtypeCardinality */
                        $newSubtypeCardinality = Mage::getModel('dyna_package/packageSubtypeCardinality');

                        $newSubtypeCardinality
                            ->setData('package_subtype_id', $this->getRequest()->getParam('id'))
                            ->setData('process_context_id', $contextId)
                            ->setData('cardinality', $cardinality);

                        $newSubtypeCardinality->save();
                    }
                }
            } else {
                $model->setData($postData);
                $model->save();

                /** @var Dyna_Package_Model_PackageSubtype $packageSubtype */
                $packageSubtype = Mage::getModel('package/packageSubtype')
                    ->getCollection()
                    ->setOrder('entity_id', Varien_Data_Collection::SORT_ORDER_DESC)
                    ->getFirstItem();

                $contextCardinalities = $this->getRequest()->getParam('context_cardinality');

                foreach ($contextCardinalities as $contextId => $cardinality) {
                    if ($cardinality) {
                        $cardinalityModel = Mage::getModel('dyna_package/packageSubtypeCardinality');

                        $cardinalityModel
                            ->setData('package_subtype_id', $packageSubtype->getId())
                            ->setData('process_context_id', $contextId)
                            ->setData('cardinality', $cardinality);

                        $cardinalityModel->save();
                    }
                }
            }
        }

        $this->_redirect("*/*/");
    }
}
