<?php

class Dyna_Package_Block_Adminhtml_PackageCreationGroups_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_blockGroup = "dyna_package";
        $this->_controller = "adminhtml_packageCreationGroups";
        $this->_headerText = Mage::helper("dyna_package")->__("Add Package Creation Group");
        $this->_mode = "edit_tab";
        $this->_updateButton("save", "label", Mage::helper("dyna_package")->__("Save group"));

        parent::__construct();
    }
}
