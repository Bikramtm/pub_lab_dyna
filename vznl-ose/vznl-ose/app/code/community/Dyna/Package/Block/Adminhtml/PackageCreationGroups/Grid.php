<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Package_Block_Adminhtml_PackageCreationGroups_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("packageCreationGroupsGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("dyna_package/packageCreationGroups")->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return self|Mage_Adminhtml_Block_Widget_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $helper = Mage::helper('dyna_package');
        $this->addColumn("entity_id", array(
            "header" => $helper->__("ID"),
            "type" => "number",
            "index" => "entity_id",
        ));

        $this->addColumn("name", array(
            "header" => $helper->__("Name"),
            "index" => "name",
        ));

        $this->addColumn("gui_name", array(
            "header" => $helper->__("GUI Name"),
            "index" => "gui_name",
        ));

        $this->addColumn("sorting", array(
            "header" => $helper->__("Sorting"),
            "index" => "sorting",
        ));

        return parent::_prepareColumns();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);

        return $this;
    }
}
