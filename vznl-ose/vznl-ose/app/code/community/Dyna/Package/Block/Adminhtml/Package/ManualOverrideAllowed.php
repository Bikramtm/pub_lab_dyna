<?php

class Dyna_Package_Block_Adminhtml_Package_ManualOverrideAllowed extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        return $row->getManualOverrideAllowed() ? "True" : "False";
    }
}