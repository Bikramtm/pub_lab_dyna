<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_Package_Block_Adminhtml_PackageType_Grid extends Omnius_Package_Block_Adminhtml_PackageType_Grid
{
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn("lifecycle_status", array(
            "header" => Mage::helper("omnius_package")->__("Lifecycle status"),
            "index" => "lifecycle_status",
            "searchable" => false,
            "renderer" => "dyna_catalog/adminhtml_lifecycleRenderer",
            'type' => 'options',
            'options' => Dyna_Catalog_Model_Lifecycle::getLifecycleTypes(),
        ));
        $this->removeColumn('is_visible');

        return $this;
    }
}
