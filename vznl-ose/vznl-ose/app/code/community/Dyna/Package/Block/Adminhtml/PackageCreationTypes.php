<?php

class Dyna_Package_Block_Adminhtml_PackageCreationTypes extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_packageCreationTypes";
        $this->_blockGroup = "dyna_package";
        $this->_headerText = Mage::helper("dyna_package")->__("Package Creation Types");
        $this->_addButtonLabel = Mage::helper("dyna_package")->__("Add New Package Creation Type");
        parent::__construct();
    }
}
