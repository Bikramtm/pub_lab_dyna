<?php

class Dyna_Package_Block_Adminhtml_Package_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        // Building package types
        $packageTypes = [];
        $packageTypesCollection = Mage::getModel('dyna_package/packageType')
        ->getCollection();

        foreach ($packageTypesCollection as $packageType) {
            $packageTypes[$packageType->getId()] = $packageType->getPackageCode();
        }


        $form = new Varien_Data_Form(array(
                "id" => "edit_form",
                "action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldSet = $form->addFieldset("general",
            array(
                "legend" => "Package configuration"
            )
        );

        $fieldSet->addField("package_code", "text",
            array(
                "name" => "package_code",
                "label" => "Package code",
                "input" => "text",
                "required" => true,
                'note' => "Mapped to <b>" . Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR . "</b> product attribute",
            )
        );

        $fieldSet->addField("type_package_id", "select",
            array(
                "name" => "type_package_id",
                "label" => "Package type",
                "input" => "text",
                "required" => true,
                "values" => $packageTypes,
                'note' => "The package type to which this item is related",
            )
        );

        $fieldSet->addField("front_end_name", "text",
            array(
                "name" => "front_end_name",
                "label" => "Frontend name",
                "input" => "text",
                "required" => true,
                'note' => "This is the name displayed in configurator and cart",
            )
        );

        $fieldSet->addField("front_end_position", "select",
            array(
                "name" => "front_end_position",
                "label" => "Frontend position",
                "input" => "text",
                "required" => true,
                "values" => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9],
                'note' => "Ordered in fronted by selected value",
            )
        );

        $fieldSet->addField("ignored_by_compatibility_rules", "select",
            array(
                "name" => "ignored_by_compatibility_rules",
                "label" => "Ignored by compatibility rules",
                "note" => "If is set to 'Yes', then the products of this subtype will be ignored by compatibility rules and showed in the configurator.",
                "input" => "text",
                "values" => [0 => "No", 1 => "Yes"],
            )
        );

        $fieldSet->addField('package_subtype_visibility', 'multiselect', array(
            'label' => Mage::helper('agent')->__('Package subtype visibility'),
            'values' => Dyna_Package_Block_Adminhtml_Package_Grid::getPackageSubtypeVisibilityOptions(),
            'name' => 'package_subtype_visibility',
            "required" => false,
        ));

        $fieldSet->addField("package_subtype_lifecycle_status", "select",
            array(
                "name" => "package_subtype_lifecycle_status",
                "label" => "Lifecycle status",
                "input" => "select",
                "values" => Dyna_Catalog_Model_Lifecycle::getLifecycleTypes(),
            )
        );

        $fieldSet->addField("cardinality", "text",
            array(
                "name" => "cardinality",
                "label" => "Default package cardinality",
                "input" => "text",
                "required" => true,
                'note' => "0...0 = none, 0...1 = none or just one, 1...1 = only one, 1...n = at least one, 0...n = doesn't matter",
            )
        );

        $processContexts = Mage::getModel('dyna_catalog/processContext')
            ->getCollection();

        /** @var Dyna_Package_Model_PackageSubtypeCardinality $packageSubtypeCardinality */
        $packageSubtypeCardinality = Mage::getModel('dyna_package/packageSubtypeCardinality');

        foreach ($processContexts as $processContext) {
            $fieldSet->addField(sprintf("context_cardinality_%s", strtolower($processContext->getCode())), "text",
                array(
                    'name' => sprintf("context_cardinality[%d]", strtolower($processContext->getId())),
                    'label' => sprintf("[ %s ] %s cardinality", $processContext->getCode(), $processContext->getName()),
                    'input' => 'text',
                    'note' => "0...0 = none, 0...1 = none or just one, 1...1 = only one, 1...n = at least one, 0...n = doesn't matter",
                    'value' => $packageSubtypeCardinality->loadBySubtypeAndContext($this->getRequest()->getParam('id'), $processContext->getId())->getCardinality()
                )
            );
        }

        $fieldSet->addField("manual_override_allowed", "select",
            array(
                "name" => "manual_override_allowed",
                "label" => "Manual Override Allowed",
                "note" => "If is set to 'False', it is NOT allowed to manually add a product that was automatically removed in that order and it is NOT allowed to manually remove a product that was automatically added in that order",
                "input" => "text",
                "values" => [0 => "False", 1 => "True"],
            )
        );

        /**
         * @var Dyna_Package_Helper_Data $helper
         */
        $helper = Mage::helper('dyna_package');
        $helper->mapPackageSubtypeVisibility();

        $configurationModel = Mage::registry("package_model_configuration");
        if ($configurationModel) {
            $form->addValues($configurationModel->getData());
        }

        return parent::_prepareForm();
    }
}
