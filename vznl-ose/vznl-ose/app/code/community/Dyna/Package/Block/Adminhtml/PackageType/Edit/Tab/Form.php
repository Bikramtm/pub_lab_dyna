<?php

class Dyna_Package_Block_Adminhtml_PackageType_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                "id" => "edit_form",
                "action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldSet = $form->addFieldset("general",
            array(
                "legend" => "Package type configuration"
            )
        );

        $fieldSet->addField("package_code", "text",
            array(
                "name" => "package_code",
                "label" => "Package code",
                "input" => "text",
                "required" => true,
                'note' => "Mapped to <b>" . Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR . "</b> product attribute",
            )
        );

        $fieldSet->addField("front_end_name", "text",
            array(
                "name" => "front_end_name",
                "label" => "Frontend name",
                "input" => "text",
                "required" => true,
                'note' => "This is the name displayed in cart packages",
            )
        );

        $fieldSet->addField("lifecycle_status", "select",
            array(
                "name" => "lifecycle_status",
                "label" => "Lifecycle status",
                "input" => "select",
                "values" => Dyna_Catalog_Model_Lifecycle::getLifecycleTypes(),
            )
        );

        $packageTypeModel = Mage::registry("package_model_type");
        if ($packageTypeModel) {
            $form->addValues($packageTypeModel->getData());
        }

        return parent::_prepareForm();
    }
}
