<?php

class Dyna_Package_Helper_Data extends Omnius_Package_Helper_Data
{
    protected $cache;
    protected $cachedPackageVisibilities = array();
    protected $cachedEligibleBundles = null;

    /**
     * Checks whether or not the current quote contains a serviceability needed package
     * At this time, all packages that are not mobile, require serviceability check
     * @return bool
     */
    public function hasServiceAbilityRequiredPackages()
    {
        $hasAtLeastOne = false;

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        /** @var Dyna_Package_Model_Package $package */
        $cartPackages = $quote->getCartPackages();
        if($cartPackages) {
            foreach ($cartPackages as $package) {
                if (!in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getMobilePackages()) && !$package->getEditingDisabled()) {
                    // Found one
                    $hasAtLeastOne = true;
                    break;
                }
            }
        }

        return $hasAtLeastOne;
    }

    /**
     * Get all available package types(code => gui name)
     * @return mixed
     */
    public function getPackageTypes()
    {
        // cache key
        $key = serialize(array(__CLASS__, __METHOD__));

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $packageTypesCollection = Mage::getModel('dyna_package/packageType')->getCollection();
            foreach ($packageTypesCollection as $packageType) {
                $result[$packageType->getPackageCode()] = $packageType->getFrontEndName();
            }

            $this->getCache()->save(
                serialize($result),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );

            return $result;
        }
    }

    /**
     * Get all available package types(code => gui name)
     * @param $packageType
     * @param $subTypeCode
     * @param bool $returnName
     * @param array $filterVisibility
     * @return mixed
     */
    public function getPackageSubtype($packageType, $subTypeCode, $returnName = false, $filterVisibility = array("configurator"))
    {
        // support packageType that comes like template-mobile
        // this should resolve the problem with the names
        switch ($packageType) {
            case "template-mobile":
                $packageType = Dyna_Catalog_Model_Type::TYPE_MOBILE;
                break;
            case "template-prepaid":
                $packageType = Dyna_Catalog_Model_Type::TYPE_PREPAID;
                break;
            case "template-cable_tv":
                $packageType = Dyna_Catalog_Model_Type::TYPE_CABLE_TV;
                break;
            case "template-cable_internet_phone":
                $packageType = Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE;
                break;
            case "template-dsl":
                $packageType = Dyna_Catalog_Model_Type::TYPE_FIXED_DSL;
                break;
            case "template-lte":
                $packageType = Dyna_Catalog_Model_Type::TYPE_LTE;
                break;
            case "template-cable_independent":
                $packageType = Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT;
                break;
            default:
        }

        $filterVisibility = !is_array($filterVisibility) ? array("configurator") : $filterVisibility;
        asort($filterVisibility);
        // cache key
        $key = sprintf('package_%s_subtype_%s_filters_%s', $packageType, $subTypeCode, md5(implode(',',$filterVisibility)));

        if ($result = unserialize($this->getCache()->load($key))) {
            return ($returnName) ? (($result && $result['front_end_name'] != '') ? $result['front_end_name'] : $subTypeCode) : $result;
        } else {
            $packageModel      = Mage::getModel("dyna_package/packageType")->load($packageType, 'package_code');
            $subtypeCollection = Mage::getModel('dyna_package/packageSubtype')
                ->getCollection()
                ->addFieldToFilter('type_package_id', $packageModel->getEntityId())
                ->addFieldToFilter('package_code', $subTypeCode);

            if(!empty($filterVisibility)){
                $subtypeCollection->addFieldToFilter('package_subtype_visibility',
                    array(
                        array('finset'=> $filterVisibility),
                    )
                );
            }

            $result = $subtypeCollection->getFirstItem()->getData();
            $this->getCache()->save(
                serialize($result),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );
            return ($returnName) ? (($result && $result['front_end_name'] != '') ? $result['front_end_name'] : $subTypeCode) : $result;
        }
    }

     /*
     * Map package subtype visibility in the admin section
     */
    public function mapPackageSubtypeVisibility()
    {
        /** This empty will not work beneath php 5.5 */
        /**
         * @var Dyna_Package_Model_PackageSubtype $packageSubType
         */
        if (!empty($packageSubType = Mage::registry('package_model_configuration'))) {
            $packageSubtypeVisibility = $packageSubType->getPackageSubtypeVisibilityForEditForm();

            /** fetch already set data on registry */
            $data = Mage::registry("package_model_configuration");

            /** if the multiselect id changes in Dyna_Package_Block_Adminhtml_Package_Edit_Tab_Form it needs to be changed here too */
            $data['package_subtype_visibility'] = $packageSubtypeVisibility;

            /** unregister and register data */
            Mage::unregister('package_model_configuration');

            Mage::register('package_model_configuration', $data);
        }
    }

    /**
     * Find if a package subtype is visible in a section
     *
     * @param string $packageTypeCode
     * @param string $packageSubtypeCode
     * @param string $section
     * @return bool
     */
    public function getPackageSubtypeVisibilityForSection($packageTypeCode, $packageSubtypeCode, $section)
    {
        if (!$section) {
            return false;
        }

        $cacheKey = $packageTypeCode . "|" . $packageSubtypeCode . "|" . $section;
        if (!isset($this->cachedPackageVisibilities[$cacheKey])) {
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getModel('dyna_package/packageType');
            $packageTypeId = $packageTypeModel->getPackageTypeIdByCode($packageTypeCode);
            /** @var Dyna_Package_Model_PackageSubtype $packageSubtypeModel */
            $packageSubtypeModel = Mage::getModel('dyna_package/packageSubtype');
            $packageSubtypeVisibility = $packageSubtypeModel->getPackageSubtypeVisibilityByTypeAndSubtype($packageTypeId, $packageSubtypeCode);
            $packageSubtypeVisibilityArray = explode(',', $packageSubtypeVisibility);

            $this->cachedPackageVisibilities[$cacheKey] = $packageSubtypeVisibilityArray && in_array($section, $packageSubtypeVisibilityArray);
        }

        return $this->cachedPackageVisibilities[$cacheKey];
    }

    /**
     * Get cache instance
     * @return Dyna_Cache_Model_Cache
     */

    protected function getCache()
    {
        if (!$this->cache) {
            $this->cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->cache;
    }

    public function getPackageState($package)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');

        if ($this->cachedEligibleBundles === null) {
            $this->cachedEligibleBundles = $bundlesHelper->getFrontendEligibleBundles(true);
        }

        $response = [
            'title' => null, // can be sharing-group | bundle | intra-stack-bundle | installed-base-bundle |disabled
            /*
             * sharing-group
             * intra-stack-bundle
             *
             */
            'state' => [
                'icon'  => null,
                'badge' => null // empty | empty darkcyan | darkcyan  (colors: darkcyan | darkorchid | darkslategray | hint | attachment)
            ]
        ];

        $packageModel = isset($package['package_id']) ? $quote->getCartPackage($package['package_id']) : null;
        /** @var $packageCreationTypeModel Dyna_Package_Model_PackageCreationTypes */
        $packageCreationTypeModel = Mage::getSingleton('dyna_package/packageCreationTypes');

        if (isset($packageModel)) {

            $processContextHelper = Mage::helper("dyna_catalog/processContext");
            $processContextName = $processContextHelper->getNameByCode($packageModel->getSaleType());
            $response['tooltipInfo'] = array(
                'title' => isset($processContextName) ? $processContextName : '',
            );

            if (in_array($packageModel->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts())) {
                $response['state']['badge'] = 'ils-modify';
                $response['state']['icon'] = null;
                $response['tooltipInfo']['title'] = $this->__('Modify package');
            }
            if (in_array($packageModel->getSaleType(), Dyna_Catalog_Model_ProcessContext::getProlongationProcessContexts())) {
                $response['state']['badge'] = 'ils-renew';
                $response['state']['icon'] = null;
                $response['tooltipInfo']['title'] = $this->__('Renew package: ');
                if ($packageModel->getSaleType() == Dyna_Catalog_Model_ProcessContext::RETTER){
                    $response['tooltipInfo']['title'] .= $this->__('Early Bird');
                }elseif($packageModel->getSaleType() == Dyna_Catalog_Model_ProcessContext::RETNOBLU){
                    $response['tooltipInfo']['title'] .= $this->__('Standard Prolongation');
                }else{
                    $response['tooltipInfo']['title'] .= $this->__('Added Runtime');
                }

            }

            if ($packageModel->getSaleType() ==  Dyna_Catalog_Model_ProcessContext::MIGCOC){
                $response['state']['badge'] = 'ils-change';
                $response['state']['icon'] = null;
                $packageCreationTypeFrontName = str_replace('_',' ',$packageCreationTypeModel->load($packageModel->getPackageCreationTypeId())->getPackageTypeCode());
                $response['tooltipInfo']['title'] = $this->__('Change service: ');
                $migrationSourcePackageCreationType = Mage::getSingleton('customer/session')->getSourcePackageCreationType();
                $migrationSourcePackageCreationTypeFrontName = $packageCreationTypeModel->getNameByPackageTypeCode($migrationSourcePackageCreationType);
                if ($packageCreationTypeFrontName == 'DSL'){
                    $response['state']['badge'] .= ' to-dsl';
                    $response['tooltipInfo']['title'] .= $this->__($migrationSourcePackageCreationTypeFrontName) .' to '.$packageCreationTypeFrontName;
                }elseif (strpos($packageCreationTypeFrontName,'Cable') !== false){
                    $response['state']['badge'] .= ' to-cable';
                    $response['tooltipInfo']['title'] .=$this->__($migrationSourcePackageCreationTypeFrontName) .' to '.$packageCreationTypeFrontName;
                }else{
                    $response['tooltipInfo']['title'] .=$this->__('PRE to POST');
                }
            }

            if ($packageModel->getSaleType() ==  Dyna_Catalog_Model_ProcessContext::ACQ){
                $response['state']['badge'] = 'acq';
                $response['state']['icon'] = null;
                $processContextHelper = Mage::helper("dyna_catalog/processContext");
                $processContextName = $processContextHelper->getNameByCode($packageModel->getSaleType());
                $response['tooltipInfo'] = array(
                        'title' => isset($processContextName) ? $processContextName : '',
                );
            }

            if ($packageModel->isPartOfGigaKombi() || $packageModel->isPartOfLowEntry()) {
                if(!empty($package['installed_base_products'])){
                    $response['title'] = 'bundle';
                    $response['title'] .= ' disabled';
                } else {
                    $response['title'] = 'bundle default';
                }
            }
            if ($packageModel->isPartOfRedPlus()) {
                $response = $this->getRedPlusPackageDisplay($package);
            } elseif ($packageModel->isPartOfSuso()) {
                if (strtolower($package['type']) == strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID)) {
                    $response['state']['badge'] = 'badge empty attachment';
                    $response['state']['icon'] = 'vfde-icon vfde-attach';
                } else {
                    $response['state']['badge'] = 'badge attachment';
                    $response['state']['icon'] = 'vfde-icon vfde-attach';
                }
            }
        }

        foreach ($this->cachedEligibleBundles as $bundle) {
            if ($bundle['lowEntryBundle']) {
                foreach ($bundle['highlightPackages'] as $highlightPackage) {
                    if (isset($package['package_id']) && $package['package_id'] == $highlightPackage['packageId']) {
                        if (empty($response['state']['partOfGroup'])) {
                            $response['state']['badge'] = 'hint';
                            $response['state']['icon'] = 'vfde-lightbulb';
                        }

                        /** @var Dyna_Bundles_Model_BundleRule $tooltipBundle */
                        $tooltipBundle = Mage::getModel('dyna_bundles/bundleRule')->load($bundle['bundleId']);
                        $cartHint = $tooltipBundle->getHintWithLocation(Dyna_Bundles_Model_BundleRule::LOCATION_CART);
                        $response['tooltipInfo'] = array(
                            'title' => isset($cartHint) ? $cartHint->getTitle() : '',
                            'message' => isset($cartHint) ? $cartHint->getText() : ''
                        );
                    }
                }
            }
        }

        return $response;
    }


    public function getBundleColor($packageId)
    {
        $badgeColors = ['darkcyan', 'darkorchid', 'darkslategray'];
        $bundlesHelper = Mage::helper('dyna_bundles');
        $bundlesInCart = $bundlesHelper->getActiveBundles();
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packageModel = isset($packageId) ? $quote->getCartPackage($packageId) : null;
        $keyOfBundle = null;
        if (isset($packageModel) && $packageModel->getBundles()) {
            foreach ($bundlesInCart as $activeBundle) {
                foreach ($packageModel->getBundles() as $packageBundle) {
                    if ($activeBundle['bundleId'] == $packageBundle->getId()) {
                        $keyOfBundle = array_search($activeBundle, $bundlesInCart);
                    }
                }
            }
        }
        return isset($badgeColors[$keyOfBundle]) ? $badgeColors[$keyOfBundle] : '';
    }
    /**
     * Method used to build the display of the packages that
     * are part of Red+ group
     *
     * @param $package
     * @return array
     */
    protected function getRedPlusPackageDisplay($package)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packageModel = isset($package['package_id']) ? $quote->getCartPackage($package['package_id']) : null;
        $parentPackage = $quote->getCartPackageByEntityId($packageModel->getParentId());
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");

        $badgeColors = ['darkorchid', 'darkcyan', 'darkslategray'];

        $response = [];
        if (strtolower($packageModel->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
            $response['state']['badge'] = 'darkorchid';
            $response['state']['icon'] = 'vfde-person small';
            if ($packageModel->getEditingDisabled()) {
                $response['title'] = 'sharing-group disabled_full';
            }
        } elseif ($packageModel->isRedPlus()) {
            $response['state']['badge'] = 'empty darkorchid';
            $response['state']['icon'] = 'vfde-person small';
        }



        $groupPackages = [];
        $groupCounter = 0;

        foreach ($quote->getCartPackages() as $cartPackage) {
            if ($cartPackage->isPartOfRedPlus() && !$cartPackage->getParentId()) {
                $cartPackage->setGroupCounter($groupCounter);
                $groupPackages[] = $cartPackage;
                foreach ($quote->getCartPackages() as $childPackage) {
                    if ($childPackage->getParentId() == $cartPackage->getId()) {
                        $childPackage->setGroupCounter($groupCounter);
                        $groupPackages[] = $childPackage;
                    }
                }
                $groupCounter++;
            }
        }

        foreach ($groupPackages as $groupPackage) {
            if ($packageModel->getId() == $groupPackage->getId()) {
                $processContextName = $processContextHelper->getNameByCode($packageModel->getSaleType());
                $response['tooltipInfo'] = array('title' => isset($processContextName) ? $processContextName : '');
                $response['state']['badge'] = $badgeColors[$groupPackage->getGroupCounter() % count($badgeColors)];
                $response['state']['secondBadge'] = '';

                if ($packageModel->isRedPlus()) {
                    $response['state']['badge'] .= ' empty';
                }
                if (in_array($packageModel->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts())) {
                    $response['state']['secondBadge'] .= ' ils-change';
                    $response['state']['partOfGroup'] = true;
                }
                if (in_array($packageModel->getSaleType(), Dyna_Catalog_Model_ProcessContext::getProlongationProcessContexts())) {
                    $response['state']['secondBadge'] .= ' ils-renew';
                    $response['state']['partOfGroup'] = true;
                }
                if ($packageModel->getSaleType() ==  Dyna_Catalog_Model_ProcessContext::ACQ){
                    $response['state']['secondBadge'] .= ' acq';
                    $response['state']['partOfGroup'] = true;
                }
                if (in_array($packageModel->getSaleType(), Dyna_Catalog_Model_ProcessContext::getMigrationProcessContexts())) {
                    $response['tooltipInfo'] = ['title' => $this->__("Change service: PRE to POST")];
                    $response['state']['secondBadge'] .= ' ils-change';
                    $response['state']['partOfGroup'] = true;
                }
            }
        }

        if ($packageModel->isPartOfGigaKombi()) {
            $response['title'] = 'bundle';
            $response['state']['badge'] = 'darkcyan';
        }
        if ($parentPackage && $parentPackage->isPartOfGigaKombi()) {
            $response['state']['badge'] = 'empty darkcyan';
        }

        return $response;
    }

    /**
     * Get title of package type and subtype
     * @param Dyna_Package_Model_Package $package
     * @param $packageType
     * @param string $guiSection
     * @return array
     */
    public function getTypeAndSubtypeTitle($package, $packageType, $guiSection = '')
    {
        $packageItems = is_object($package) ? $package->getData('items') : $package['items'];

        if (count($package['items']) != 0) {
            $model = Mage::getModel('dyna_package/package');
            !is_object($package) ? $model->setData('type', $package['type']) : '';

            $packageItemsBySections = is_object($package) ?
                $package->getPackagesProductsForSidebarSection($packageItems, null, false, $guiSection)
                : $model->getPackagesProductsForSidebarSection($packageItems, null, false, $guiSection);
            $packageSubtypeTitle = '';

            foreach ($packageItemsBySections as $section => $productsInSection) {
                if ($section == strtolower($packageType) && count($productsInSection)) {
                    foreach ($packageItemsBySections[$section]['items'] as $product) {
                        if (!isset($product['is_contract_drop'])) {
                            $packageSubtypeTitle = $product['name'];
                            break;
                        } else {
                            continue;
                        }
                    }
                }
            }
            $packageTitle = Mage::getSingleton('dyna_package/packageCreationTypes')->getById($package['package_creation_type_id'])->getGuiName();
        }

        return array(
            'packageSubtypeTitle' => $packageSubtypeTitle ?? null,
            'packageTitle' => $packageTitle ?? null
        );
    }


    /**
     * Gets the number of packages that are children of the specified package
     * @param $package
     * @return int
     */
    public function getNumberOfRedPlusGroupMembers($package)
    {
        if (isset($package['entity_id'])) {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $packages = $quote->getCartPackagesWithParent($package['entity_id']);

            return count($packages);
        }

        return 0;
    }

    /**
     * Returns a new array of packages, grouped as parent -> children
     * @param array $packages
     * @return array
     */
    public function groupRedPlusPackages(array $packages)
    {
        $sortedPackages = array();
        foreach ($packages as $key => $package) {
            if (isset($package['entity_id'])) {
                if (!isset($package['parent_id'])) {
                    // Add the parent
                    $sortedPackages[$key] = $package;

                    // Find the children of the current package in the iteration and add them in the final array
                    foreach ($packages as $childKey => $childPackage) {
                        if (isset($childPackage['parent_id']) && $childPackage['parent_id'] == $package['entity_id']) {
                            $sortedPackages[$childKey] = $childPackage;
                        }
                    }
                }
            } else {
                // Leave the template-* packages as they are
                $sortedPackages[$key] = $package;
            }
        }

        return $sortedPackages;
    }

    public function groupInstalledBaseBundlePackages(array $packages)
    {
        $sortedPackages = array();
        foreach ($packages as $key => $package) {
            // Add package
            $sortedPackages[$key] = $package;
            if (isset($package['entity_id']) &&  isset($package['bundle_id'])) {
                // Find the children of the current package in the iteration and add them in the final array
                foreach ($packages as $childKey => $childPackage) {
                    if (isset($childPackage['bundle_id']) && $childPackage['bundle_id'] == $package['bundle_id']) {
                        $sortedPackages[$childKey] = $childPackage;
                    }
                }
            }
        }

        return $sortedPackages;
    }

    /**
     * Returns the type of modal to be displayed when the user attempts
     * to delete a package
     * @param $package
     * @return string
     */
    public function getDeleteModalType($package)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packageModel = isset($package['package_id']) ? $quote->getCartPackage($package['package_id']) : null;

        $result = [
            'modalType' => 'default',
            'bundleId' => null,
            'packageTitle' => $packageModel ? $packageModel->getTitle() : null,
            'dummyPackageTitle' => null,
            'modalMessage' => null,
        ];

        if ($packageModel && $packageModel->isPartOfGigaKombi()) {
            $bundledPackages = $quote->getCartPackagesInBundle($packageModel->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_KOMBI)->getId());

            foreach ($bundledPackages as $bundledPackage) {
                if ($bundledPackage->getId() !== $packageModel->getId()) {
                    $result['dummyPackageTitle'] = $bundledPackage->getTitle();
                    break;
                }
            }
            $currentPackageType = Mage::getModel('dyna_package/packageType')->loadByCode(strtolower($packageModel->getType()));
            $bundledPackageType = Mage::getModel('dyna_package/packageType')->loadByCode(strtolower($bundledPackage->getType()));

            $result['modalType'] = 'alert-gigakombi-delete-modal';
            $result['bundleId'] = $packageModel->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_KOMBI)->getId();
            $result['modalMessage'] = $this->__(sprintf("Deleting package %s will also discard %s. Do you want to continue?", $currentPackageType->getFrontEndName(), $bundledPackageType->getFrontEndName()));

        }elseif ($packageModel && $packageModel->isPartOfSuso()) {
            $currentPackageType = Mage::getModel('dyna_package/packageType')->loadByCode(strtolower($packageModel->getType()));

            $result['modalType'] = 'break-suso-after-deselecting-modal';
            $result['bundleId'] = $packageModel->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_SUSO)->getId();
            $result['modalMessage'] = $this->__(sprintf("Deleting package %s will break the bundle. Do you want to continue?", $currentPackageType->getFrontEndName()));
        }elseif (isset($package['parent_id'])) {
            $packages = $quote->getCartPackagesWithParent($package['parent_id']);

            if (count($packages) == 1) {
                $result['modalType'] = 'alert-redplus-group-delete';
            }
        } elseif (isset($package['entity_id']) && $package['type'] == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
            $childPackages = $quote->getCartPackagesWithParent($package['entity_id']);

            if (count($childPackages) > 0) {
                $result['modalType'] = 'alert-redplus-group-delete';
            }
        }

        return $result;
    }

    /**
     * Get status as CSS class for OSF orders
     * @param $status
     * @return string
     */
    public function getStatusAsCSSClass($status)
    {
        // get css class for order/package status
        $trimmedStatus = preg_replace('/\s+/', '', $status);

        switch($trimmedStatus) {
            case str_replace(' ','','Erfolgreich'):
                $orderStatusClass = 'completed-success';
                break;
            case str_replace(' ','','Fehlgeschlagen'):
                $orderStatusClass = 'completed-failed';
                break;
            case str_replace(' ','','Zum Teil erfolgreich'):
                $orderStatusClass = 'completed-partial';
                break;
            case str_replace(' ','','In Bearbeitung'):
                $orderStatusClass = 'pending';
                break;
            default:
                $orderStatusClass = '';
        }

        return $orderStatusClass;
    }

    /**
     * Remove all packages from current quote
     * @return $this
     */
    public function clearServicePackages()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        foreach ($quote->getCartPackages() as $package) {
            // This is an iteration over some cached instances, make sure these are still persistent
            if ($package->isCable() || $package->isFixed() && $package = Mage::getModel('dyna_package/package')->load($package->getId())) {
                $this->removePackage($package->getPackageId(), 1, $package->getId());
            }
        }
        // collect totals for new prices
        Mage::getSingleton('checkout/cart')->save();

        return $this;
    }

    /**
     * Remove package from configurator
     * @param $packageId
     * @param null $accepted
     * @param null $entityId
     * @return array
     * @throws Exception
     */
    public function removePackage($packageId, $accepted = null, $entityId = null)
    {
        // check if one of deal
        $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();
        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $cart->getQuote();
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');

        /** @var Dyna_Package_Model_Package $packageModel */
        if ($entityId) {
            foreach ($quote->getCartPackages() as $packageModel) {
                if ($packageModel->getEntityId() == $entityId) {
                    break;
                }
            }
        } else {
            $packageModel = $quote->getCartPackage($packageId);
        }

        if(!isset($packageModel) || !$packageModel->getId()) {
            throw new Exception(sprintf($this->__('Could not find package %d for quote: %d'), $packageId, $quote->getId()));
        }

        /** Store deleted packages ids */
        $packageIds = array();


        /** @var Dyna_Bundles_Model_BundleRule $bundle */
        if ($bundle = $packageModel->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_INTRA)) {
            $this->removeIntrastackBundle($packageModel, $packageIds);
            Mage::getSingleton('customer/session')->unsSubscriberCtn();
        } elseif ($bundle = $packageModel->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_KOMBI)) {
            $this->removeGigaKombiBundle($packageModel, $packageIds);
            Mage::getSingleton('customer/session')->unsSubscriberCtn();
        } elseif ($bundle = $packageModel->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_SUSO)) {
            $this->removeSusoBundle($packageModel, $packageIds);
        } elseif ($bundle = $packageModel->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS)) {
            /** @var Dyna_Package_Model_Package[] $childrenPackageModels */
            $childrenPackageModels = $quote->getCartPackagesWithParent($packageModel->getEntityId());

            if (count($childrenPackageModels) == 0) { // is child
                /** @var Dyna_Package_Model_Package $parentPackageModel */
                $parentPackageModel = $quote->getCartPackageByEntityId($packageModel->getParentId());
                if ($parentPackageModel->isPartOfGigaKombi()) {
                    $this->removeGigaKombiBundle($parentPackageModel, $packageIds);
                    $this->removeSusoBundle($parentPackageModel, $packageIds);

                    $packageIds[] = $parentPackageModel->getPackageId();
                    $parentPackageModel->delete();
                } elseif ($parentPackageModel->getEditingDisabled() ) { // is install base
                    if (count($parentPackageModel->getChildPackages()) == 1) { // only one child
                        $packageIds[] = $parentPackageModel->getPackageId();
                        $parentPackageModel->delete();
                    }
                } else { // is NOT install base
                    if (count($parentPackageModel->getChildPackages()) == 1) { // only one child
                        // Keep parent, but remove RedPlus bundle (no more members)
                        $bundlesHelper->removeBundlePackageEntry($bundle->getId(), $parentPackageModel->getEntityId());
                        $parentPackageModel->updateBundleTypes()->save();
                    }
                }
            }  else { // is parent
                foreach ($childrenPackageModels as $childPackage) {
                    if ($childPackage->isRedPlus()) {
                        $packageIds[] = $childPackage->getPackageId();
                        $childPackage->delete();
                    }
                }
            }
        }

        $removeRetainables = false;
        $couponsToDecrement = array();
        if ($isOneOfDeal && $packageModel->getRetainable()) {
            if ($accepted != '1') {
                $this->jsonResponse(array(
                    'error'              => false,
                    'showOneOfDealPopup' => true
                ));
                return;
            }

            // Get all the packages
            $packagesModels = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id', $quote->getId());

            foreach($packagesModels as $packagesModel) {
                if($packagesModel->getOneOfDeal()) {
                    $packageIds[] = $packagesModel->getPackageId();
                    $couponsToDecrement[] = $packageModel->getCoupon();
                    $packagesModel->delete();

                }
            }

            $removeRetainables = true;
        }

        $couponsToDecrement[] = $packageModel->getCoupon();
        // Before deleting, check if it is part of migration / move offnet flow
        if ($packageModel->getId() == Mage::getSingleton('checkout/session')->getMigrationPackage()) {
            Mage::helper('dyna_customer')->clearMigrationData();
        }

        $deletedPackageType = $packageModel->getType();


        // Remove the package from the packages table
        $packageIds[] = $packageModel->getPackageId();
        $packageModel->delete();

        $customerId = $quote->getCustomerId();
        foreach($couponsToDecrement as $coupon){
            Mage::helper('pricerules')->decrementCoupon($coupon, $customerId);
        }

        $packageIds = array_unique($packageIds);
        if (in_array((int) $quote->getActivePackageId(), $packageIds)) {
            $quote->setActivePackageId(false);
        }

        $packagesIndex = Mage::helper('dyna_configurator/cart')->reindexPackagesIds($quote);

        /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
        $rulesHelper = Mage::helper('dyna_configurator/rules');
        $remainingCablePackage = "";
        if ($packagesIndex && $rulesHelper->checkGateWay($deletedPackageType)) {
            if (!$quote->getCartPackages()) {
                $rulesHelper->cacheSave(null);
            }
            /** @var Dyna_Package_Model_Package $package */
            foreach ($quote->getCartPackages() as $packageId => $package) {
                if ($rulesHelper->checkGateWay($package->getType())) {
                    $remainingCablePackage = $package->getType();
                    /*****************************
                     * SPECIAL HANDLING FOR CABLE
                     ******************************/
                    // Setting this package as active on quote and collect_totals_after event will execute cable post condition rules for it
                    $quote
                        ->setActivePackageId($packageId)
                        ->setTotalsCollectedFlag(false)
                        ->setSkipCablePostConditions(false);
                    // Make sure passes collect totals after (restriction exists in observer that requires addMulti origin for cable post condition rules to be processed)
                    Mage::register('product_add_origin', 'addMulti');
                    // Saving cart will trigger the post condition rules
                    // No need to save cart here, controller or who ever calls this remove package method should handle cart save
                    break;
                }
            }
        }

        return array(
            'packagesIndex'         => $packagesIndex,
            'removeRetainables'     => $removeRetainables,
            'packageIds'            => $packageIds,
            'remainingCablePackage' => $remainingCablePackage
        );
    }

    /**
     * @param Dyna_Package_Model_Package $currentPackage
     * @param array $packageIds
     */
    public function removeGigaKombiBundle(Dyna_Package_Model_Package $currentPackage, &$packageIds) {
        // Check for migration in progress targeting the current package to remove @see OVG-1921
        $migrationType = Mage::getSingleton('checkout/cart')->getCheckoutSession()->getInMigrationOrMoveOffnetState();
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $sessionMigrationData = explode(",", Mage::getSingleton('checkout/cart')->getCheckoutSession()->getMigrationOrMoveOffnetCustomerNo());
        $subscriptionNumber = $sessionMigrationData[0] ?? null;
        $productId = $sessionMigrationData[1] ?? null;

        //installed base siblings
        if ($migrationType && $customer->isIBProductPartOfBundle($subscriptionNumber, $productId)) {
            $installBaseBundleSibling = $customer->getIBBundleSibling($subscriptionNumber, $productId);
            // search for a bundle in cart with this install base sibling and break it
            if ($installBaseBundleSibling && ($this->getBundleIdWithInstallBase($installBaseBundleSibling['contract_id'], $installBaseBundleSibling['product_id']))) {
                Mage::getSingleton('checkout/cart')->getCheckoutSession()->setInMigrationOrMoveOffnetState(null);
                Mage::getSingleton('checkout/cart')->getCheckoutSession()->setMigrationOrMoveOffnetCustomerNo(null);
            }
        }

        $bundle = $currentPackage->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_KOMBI);
        /** @var Dyna_Package_Model_Package $package */
        foreach ($currentPackage->getPackagesInSameBundle($bundle->getId(), true) as $package) {
            $this->removeRedPlusBundle($package, $packageIds);
            $this->removeSusoBundle($package, $packageIds);

            $packageIds[] = $package->getPackageId();
            $package->delete();
        }

        Mage::getSingleton('customer/session')->setBundleRuleParserResponse(null);

        $this->removeRedPlusBundle($currentPackage, $packageIds);
        $this->removeSusoBundle($currentPackage, $packageIds);
    }

    /**
     * @param Dyna_Package_Model_Package $currentPackage
     * @param array $packageIds
     */
    public function removeIntrastackBundle(Dyna_Package_Model_Package $currentPackage, &$packageIds) {
        // Check for migration in progress targeting the current package to remove @see OVG-1921
        $migrationType = Mage::getSingleton('checkout/cart')->getCheckoutSession()->getInMigrationOrMoveOffnetState();
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $sessionMigrationData = explode(",", Mage::getSingleton('checkout/cart')->getCheckoutSession()->getMigrationOrMoveOffnetCustomerNo());
        $subscriptionNumber = $sessionMigrationData[0] ?? null;
        $productId = $sessionMigrationData[1] ?? null;

        //installed base siblings
        if ($migrationType && $customer->isIBProductPartOfBundle($subscriptionNumber, $productId)) {
            $installBaseBundleSibling = $customer->getIBBundleSibling($subscriptionNumber, $productId);
            // search for a bundle in cart with this install base sibling and break it
            if ($installBaseBundleSibling && ($this->getBundleIdWithInstallBase($installBaseBundleSibling['contract_id'], $installBaseBundleSibling['product_id']))) {
                Mage::getSingleton('checkout/cart')->getCheckoutSession()->setInMigrationOrMoveOffnetState(null);
                Mage::getSingleton('checkout/cart')->getCheckoutSession()->setMigrationOrMoveOffnetCustomerNo(null);
            }
        }

        $bundle = $currentPackage->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_INTRA);
        /** @var Dyna_Package_Model_Package $package */
        foreach ($currentPackage->getPackagesInSameBundle($bundle->getId(), true) as $package) {
            $this->removeRedPlusBundle($package, $packageIds);
            $this->removeSusoBundle($package, $packageIds);

            $packageIds[] = $package->getPackageId();
            $package->delete();
        }

        Mage::getSingleton('customer/session')->setBundleRuleParserResponse(null);

        $this->removeRedPlusBundle($currentPackage, $packageIds);
        $this->removeSusoBundle($currentPackage, $packageIds);
    }

    /**
     * @param Dyna_Package_Model_Package $currentPackage
     * @param array $packageIds
     */
    public function removeRedPlusBundle(Dyna_Package_Model_Package $currentPackage, &$packageIds) {
        // If package has children remove them
        foreach ($currentPackage->getChildPackages() as $childPackage) {
            $packageIds[] = $childPackage->getPackageId();
            $childPackage->delete();
        }
    }

    /**
     * @param Dyna_Package_Model_Package $currentPackage
     * @param array $packageIds
     */
    public function removeSusoBundle(Dyna_Package_Model_Package $currentPackage, &$packageIds) {
        if ($currentPackage->isPrepaid()) {
            $this->breakSusoBundle($currentPackage);
        } elseif ($bundle = $currentPackage->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_SUSO)) {
            $bundledPackages = $currentPackage->getPackagesInSameBundle($bundle->getId(), true);

            foreach ($bundledPackages as $package) {
                $packageIds[] = $package->getPackageId();
                $package->delete();
            }
        }
    }

    /**
     * Breaks the SurfSofort bundle by removing the bundle ID from the associated
     * DSL package & removing its bundle components
     *
     * @param Dyna_Package_Model_Package $package
     */
    public function breakSusoBundle(Dyna_Package_Model_Package $package)
    {
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');

        if ($package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_SUSO)) {
            $bundleId = $package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_SUSO)->getId();
            $bundledPackages = $package->getPackagesInSameBundle($bundleId, true);
            $bundledPackage = null;

            foreach ($bundledPackages as $package) {
                if (!$package->isPrepaid()) {
                    $bundledPackage = $package;
                    break;
                }
            }

            if ($bundledPackage) {
                $bundlesHelper->removeBundlePackageEntry($bundleId, $bundledPackage->getEntityId());
                $bundledPackage->updateBundleTypes()->save();

                foreach ($bundledPackage->getAllItems() as $item) {
                    if ($item->getBundleComponent() == $bundleId) {
                        if (!$item->getProduct()->isSubscription()) {
                            $item->isDeleted(true);
                            continue;
                        }

                        $item->setBundleComponent(0);
                    }
                }
            } else {
                Mage::throwException("DSL Package was not found in SurfSofort bundle");
            }
        }
    }

    /**
     * Return the bundle id if found in current cart that has an install base product component
     * @return int
     */
    public function getBundleIdWithInstallBase($subscriptionNumber, $productId)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        foreach ($quote->getCartPackages() as $package) {
            if (!empty($package->getBundles()) && $package->getParentAccountNumber() === $subscriptionNumber && $package->getServiceLineId() === $productId) {
                return $package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_KOMBI);
            }
        }

        return 0;
    }

    /**
     * Get first tarif name for a certain package
     * @param $packageItemsBySubtype
     * @return string
     */
    public function getPackageTarifName($packageItemsBySubtype)
    {
        $tariffName = '';
        foreach($packageItemsBySubtype as $sectionName => $sectionDetails){
            if($sectionName == 'tariff'){
                $tariffName = $sectionDetails['items'][0]['name'] ?? '';
            }
        }
        return $tariffName;
    }

    /**
     * Returns the packages with the specified parent id from a certain order
     * @param $packageId
     * @param $orderId
     * @return array
     */
    public function getOrderPackagesWithParent($packageId, $orderId)
    {
        $cartPackages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $orderId);
        $packagesWithParent = array();

        foreach ($cartPackages as $package) {
            if ($package->getParentId() == $packageId) {
                $packagesWithParent[] = $package;
            }
        }

        return $packagesWithParent;
    }

    /**
     * Method that calculates the total price and maf for a package inside an order
     *
     * @param $orderId
     * @param $packageId
     * @param bool $isOffer
     * @return array
     */
    public function calculateTotalsForPackage($orderId, $packageId, $isOffer = false)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')
            ->load($orderId);
        $items = $order->getAllItems();

        $totalPrice = $totalMaf = $taxPrice = $taxMaf = 0;

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($items as $item) {
            $correctId = $item->getPackageId() == $packageId || $packageId == null;
            $isOfferOrPromo = !($item->isPromo() || $item->isBundlePromo() || $isOffer);
            if ($correctId && $isOfferOrPromo) {
                $totalPrice += ($item->getData('row_total_incl_tax') - $item->getData('base_discount_amount'));
                $totalMaf += ($item->getData('maf_row_total_incl_tax') - $item->getData('maf_discount_amount'));
                $taxAmmount = $item->getTaxAmount() ? $item->getTaxAmount() : 0;
                $taxPrice += $taxAmmount;

                $mafTaxAmmount = $item->getMafTaxAmount() ? $item->getMafTaxAmount() : 0;
                $taxMaf += $mafTaxAmmount;
            }
        }

        return array(
            'total_maf' => $totalMaf,
            'total' => $totalPrice,
            'tax_maf' => $taxMaf,
            'tax_price' => $taxPrice
        );
    }

    /**
     * Check notes existence on package
     * @param null $package
     * @return bool
     */
    public function checkNotes($package)
    {
        // package notes
        $packageNote = Mage::getModel('dyna_checkout/packageNotes')
            ->getCollection()
            ->addFieldToFilter('package_id',$package->getEntityId());
        
        return $packageNote->count() > 0;
    }

    /**
     * Get package from curent quote by entity_id
     * @param $quote
     * @param $packageEntityId
     * @return mixed
     */
    public function getPackageByEntityId($packageEntityId)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        foreach ($quote->getCartPackages() as $package) {
            if($package->getEntityId() == $packageEntityId){
                return $package;
            }
        }

        return false;
    }

    /**
     * Get all available package types(code => gui name)
     * @return mixed
     */
    public function getPackageCreationGroups()
    {
        // cache key
        $key = serialize(array(__CLASS__, __METHOD__));

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $packageCreationGroupsCollection = Mage::getModel('dyna_package/packageCreationGroups')->getCollection();
            foreach ($packageCreationGroupsCollection as $packageGroup) {
                $result[$packageGroup->getId()] = $packageGroup->getGuiName();
            }

            $this->getCache()->save(
                serialize($result),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );

            return $result;
        }
    }

    /**
     * Get all available package types(entity_id => gui name)
     * @return mixed
     */
    public function getPackageTypesId()
    {
        // cache key
        $key = serialize(array(__CLASS__, __METHOD__));

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $packageTypesCollection = Mage::getModel('dyna_package/packageType')->getCollection();
            foreach ($packageTypesCollection as $packageType) {
                $result[$packageType->getId()] = $packageType->getFrontEndName();
            }

            $this->getCache()->save(
                serialize($result),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );

            return $result;
        }
    }

    /**
     * @param Dyna_Package_Model_Package $a
     * @param Dyna_Package_Model_Package $b
     * @return int
     */
    public static function sortPackagesById($a,$b)
    {
        if ($a->getPackageId() == $b->getPackageId()) {
            return 0 ;
        }
        return ($a->getPackageId() < $b->getPackageId()) ? -1 : 1;
    }
}
