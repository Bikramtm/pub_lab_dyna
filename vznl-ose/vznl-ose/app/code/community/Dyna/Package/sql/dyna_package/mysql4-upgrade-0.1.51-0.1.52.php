<?php
/**
 * Installer that adds a new column in the catalog_package table.
 * Column: sharing_group_id
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageResourceTable = $this->getTable('package/package');

if (!$this->getConnection()->tableColumnExists($this->getTable('package/package'), 'sharing_group_id')) {
    $this->getConnection()->addColumn($packageResourceTable, 'sharing_group_id', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'comment' => 'Column used for identifying the sharing group of the package'
    ));
}

$this->endSetup();
