<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$this->getConnection()->addColumn($this->getTable('package/package'), "activity_type", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable' => true,
    'comment' => 'Keeping the activity type',
));

$this->endSetup();
