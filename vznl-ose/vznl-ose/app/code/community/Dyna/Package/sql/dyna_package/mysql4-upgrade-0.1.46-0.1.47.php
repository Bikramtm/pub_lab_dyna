<?php
/**
 * Installer that adds two columns for catalog package resource:
 * install_base_products - used for saving product skus that customer has in its install base
 * editing_disabled - flag that tells front end that package cannot be edited
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageResourceTable = $this->getTable('package/package');

$this->getConnection()->addColumn($packageResourceTable, 'campaign_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'comment' => 'Column used for setting Campaign ID of package products'
));

$this->endSetup();
