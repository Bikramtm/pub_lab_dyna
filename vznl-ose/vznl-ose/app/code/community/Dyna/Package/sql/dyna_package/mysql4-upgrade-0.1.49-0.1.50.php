<?php
/**
 * Installer that adds a new column in the catalog_package table.
 * Column: parent_id
 * Usage: determine the parent-child relationship between packages (Used in Red+ scenarios)

 * =================== MERGED INSTALLERS

 * Installer that adds 1 column for table dyna_package_subtype
 * - package_subtype_visibility
 * Removes 2 columns for table dyna_package_subtype"
 * visible_configurator
 * visible_cart
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageResourceTable = $this->getTable('package/package');

if (!$this->getConnection()->tableColumnExists($this->getTable('package/package'), 'parent_id')) {
    $this->getConnection()->addColumn($packageResourceTable, 'parent_id', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'comment' => 'Column used for setting the parent id of the package'
    ));
}

$packageSubtypeTable = $this->getTable('package/subtype');

$this->getConnection()->addColumn($packageSubtypeTable, 'package_subtype_visibility', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'comment' => 'Package subtype frontend visibility'
));

$fields = [
    'visible_cart',
    'visible_configurator'
];
foreach($fields as $field) {
    $this->getConnection()->dropColumn($packageSubtypeTable, $field);
}

$this->endSetup();
?>
