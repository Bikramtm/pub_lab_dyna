<?php
/**
 * Installer that adds 1 column for table catalog_package
 * - before_bundle_products
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$catalogPackageTable = $this->getTable('package/package');
$columnName = 'before_bundle_products';

$this->getConnection()->addColumn($catalogPackageTable, $columnName, array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 256,
    'comment' => "Column holds the ID's of products which are in quote before creating a bundle.",
    'after' => 'installed_base_products'
));

$this->endSetup();