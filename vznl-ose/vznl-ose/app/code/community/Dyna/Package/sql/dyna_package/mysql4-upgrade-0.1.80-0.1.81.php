<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$this->getConnection()->addColumn(
    $this->getTable('package_creation_types'),
    'package_subtype_filter_attributes',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => true,
        'after'     => 'filter_attributes', // insert after this column
        'comment' => "Attribute filters that should be preselected for package subtype"
    ]
);

$this->endSetup();
