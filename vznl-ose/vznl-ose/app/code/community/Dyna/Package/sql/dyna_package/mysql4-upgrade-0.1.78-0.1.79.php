<?php

/** Copyright (c) 2017. Dynacommerce B.V. */
/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();
$packageTable = $this->getTable('package/package');
$columnName = 'use_case_indication';

if (!$this->getConnection()->tableColumnExists($packageTable, $columnName)) {
    $this->getConnection()->addColumn($packageTable, $columnName, array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => '20',
        'after' => 'service_address_data',
        'comment' => 'Column holds use case indication value'
    ));
}
$this->endSetup();