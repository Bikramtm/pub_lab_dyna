<?php
/**
 * Installer that adds two new columns in the catalog_package table.
 * Columns: install_base_subscription_number, install_base_product_id
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();

$packageResourceTable = $this->getTable('package/package');

if (!$this->getConnection()->tableColumnExists($this->getTable('package/package'), 'install_base_subscription_number')) {
    $this->getConnection()->addColumn($packageResourceTable, 'install_base_subscription_number', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'comment' => 'Column used for bundling, to store the subscription number for the current package'
    ));
}

if (!$this->getConnection()->tableColumnExists($this->getTable('package/package'), 'install_base_product_id')) {
    $this->getConnection()->addColumn($packageResourceTable, 'install_base_product_id', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'comment' => 'Column used for bundling, to store the product id for the current package'
    ));
}

$this->endSetup();
