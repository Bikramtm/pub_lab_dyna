<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageTable = $this->getTable('package/package');
$columnName = 'converted_to_member';

if (!$this->getConnection()->tableColumnExists($packageTable, $columnName)) {
    $this->getConnection()->addColumn($packageTable, $columnName, array(
        'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'length' => 2,
        'comment' => 'Column used for identifying if the package resulted after convert to member operation'
    ));
}

$this->endSetup();
