<?php
/** Copyright (c) 2017. Dynacommerce B.V. */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageTable = $this->getTable('package/package');
$newColumns = [
    'migration_service_line_id',
    'migration_parent_account'
];
foreach ($newColumns as $newColumn) {
    if (!$this->getConnection()->tableColumnExists($packageTable, $newColumn)) {
        $this->getConnection()->addColumn($packageTable, $newColumn, array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 20,
            'after' => 'parent_account_number',
            'comment' => "Column used for the identification of an IB subscription that is in migration flow"
        ));
    }
}

$this->endSetup();