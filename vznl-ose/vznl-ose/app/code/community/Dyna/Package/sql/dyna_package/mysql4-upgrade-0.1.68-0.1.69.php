<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 *
 * Added column to save bundle types for a package
 * Values will be comma separated
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;
$installer->startSetup();

$packageTable = $this->getTable('package/package');
$columnName = 'bundle_types';

$installer->getConnection()->addColumn($packageTable, $columnName, array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 100,
    'comment' => 'Holds bundle types list'
));

$installer->endSetup();