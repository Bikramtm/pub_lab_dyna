<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;
$installer->startSetup();

try {
    $write = Mage::getSingleton('core/resource')->getConnection('core_write');

    $write->exec("
USE dbextract;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

DROP TABLE IF EXISTS `lastrun`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lastrun` (
  `date_time` DATETIME NOT NULL,
  `attempts`  INT      NOT NULL DEFAULT 0
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `lastrun` WRITE;
/*!40000 ALTER TABLE `lastrun`
  DISABLE KEYS */;
INSERT INTO `lastrun` VALUES ('2017-02-09 11:50:10', 0);
/*!40000 ALTER TABLE `lastrun`
  ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `total_details`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `total_details` (
  `transaction_id`      VARCHAR(45)   DEFAULT NULL,
  `package_id`          VARCHAR(45)   DEFAULT NULL,
  `package_type`        VARCHAR(45)   DEFAULT NULL,
  `package_sub_type`    VARCHAR(45)   DEFAULT NULL,
  `product_id`          VARCHAR(45)   DEFAULT NULL,
  `product_type`        VARCHAR(45)   DEFAULT NULL,
  `product_price`       VARCHAR(45)   DEFAULT NULL,
  `product_name`        VARCHAR(300)  DEFAULT NULL,
  `product_description` VARCHAR(2500) DEFAULT NULL,
  `product_options`     VARCHAR(45)   DEFAULT NULL,
  `bundle_id`           VARCHAR(45)   DEFAULT NULL,
  `bundle_name`         VARCHAR(45)   DEFAULT NULL,
  `action_id`           VARCHAR(45)   DEFAULT NULL,
  `campaign_code`       VARCHAR(45)   DEFAULT NULL,
  `marketing_code`      VARCHAR(45)   DEFAULT NULL,
  KEY `transaction_id_index` (`transaction_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `total_details` WRITE;
/*!40000 ALTER TABLE `total_details`
  DISABLE KEYS */;
/*!40000 ALTER TABLE `total_details`
  ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `total_header`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `total_header` (
  `transaction_id`              VARCHAR(45)  DEFAULT NULL,
  `agent_id`                    VARCHAR(45)  DEFAULT NULL,
  `agent_first_name`            VARCHAR(45)  DEFAULT NULL,
  `agent_last_name`             VARCHAR(45)  DEFAULT NULL,
  `dealer_id`                   VARCHAR(45)  DEFAULT NULL,
  `dealer_street`               VARCHAR(45)  DEFAULT NULL,
  `dealer_postcode`             VARCHAR(45)  DEFAULT NULL,
  `dealer_city`                 VARCHAR(45)  DEFAULT NULL,
  `dealer_group_id`             VARCHAR(45)  DEFAULT NULL,
  `dealer_group_name`           VARCHAR(45)  DEFAULT NULL,
  `customer_id`                 VARCHAR(45)  DEFAULT NULL,
  `customer_firstname`          VARCHAR(45)  DEFAULT NULL,
  `customer_lastname`           VARCHAR(45)  DEFAULT NULL,
  `customer_email`              VARCHAR(300) DEFAULT NULL,
  `customer_type`               VARCHAR(45)  DEFAULT NULL,
  `company_name`                VARCHAR(45)  DEFAULT NULL,
  `customer_dob`                VARCHAR(45)  DEFAULT NULL,
  `customer_telephone`          VARCHAR(45)  DEFAULT NULL,
  `customer_city`               VARCHAR(500) DEFAULT NULL,
  `customer_postcode`           VARCHAR(45)  DEFAULT NULL,
  `customer_street`             VARCHAR(300) DEFAULT NULL,
  `porting_status`              VARCHAR(45)  DEFAULT NULL,
  `shopping_cart_id`            VARCHAR(45)  DEFAULT NULL,
  `offer_id`                    VARCHAR(45)  DEFAULT NULL,
  `order_number`                VARCHAR(45)  DEFAULT NULL,
  `ogw_id`                      VARCHAR(45)  DEFAULT NULL,
  `order_status`                VARCHAR(45)  DEFAULT NULL,
  `shopping_cart_creation_date` VARCHAR(45)  DEFAULT NULL,
  `offer_creation_date`         VARCHAR(45)  DEFAULT NULL,
  `order_creation_date`         VARCHAR(45)  DEFAULT NULL,
  `planned_date`                VARCHAR(45)  DEFAULT NULL,
  `updated_at`                  VARCHAR(45)  DEFAULT NULL,
  KEY `transaction_id_index` (`transaction_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `total_header` WRITE;
/*!40000 ALTER TABLE `total_header`
  DISABLE KEYS */;
/*!40000 ALTER TABLE `total_header`
  ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;
");

    $write->exec("USE " . Mage::getConfig()->getResourceConnectionConfig("default_setup")->dbname . ";");
} catch (Exception $e){
    Mage::getSingleton('core/logger')->logException('Couldn\'t initialize dbextract schema: ' . $e->getMessage());
}
$installer->endSetup();