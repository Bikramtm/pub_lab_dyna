<?php
/** Copyright (c) 2017. Dynacommerce B.V. */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageTable = $this->getTable('package/package');
$columnName = 'deselected_defaulted_items';

if (!$this->getConnection()->tableColumnExists($packageTable, $columnName)) {
    $this->getConnection()->addColumn($packageTable, $columnName, array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'after' => 'refused_defaulted_items',
        'comment' => 'Column holds service defaulted items deselected by agent'
    ));
}

$this->endSetup();
