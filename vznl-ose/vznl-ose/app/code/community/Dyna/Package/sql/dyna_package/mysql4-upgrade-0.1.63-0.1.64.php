<?php

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageResourceTable = $this->getTable('package/package');

if (!$this->getConnection()->tableColumnExists($packageResourceTable, 'permanent_filters')) {
    $this->getConnection()->addColumn($packageResourceTable, 'permanent_filters', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'The permanent filters that are used for this created package'
    ));
}

$this->endSetup();
