<?php
/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();

if ($this->getConnection()->tableColumnExists($this->getTable("package/type"), 'is_visible')) {
   $this->getConnection()->dropColumn($this->getTable("package/type"), 'is_visible');
}
if ($this->getConnection()->tableColumnExists($this->getTable("package/subtype"), 'is_visible')) {
    $this->getConnection()->dropColumn($this->getTable("package/subtype"), 'is_visible');
}

$this->endSetup();
