<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('package/package'), 'customer_number', 'varchar(255)');

$installer->endSetup();
