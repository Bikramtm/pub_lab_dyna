<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;
$installer->startSetup();

try {
    $write = Mage::getSingleton('core/resource')->getConnection('core_write');

//Remove all old dbextract tables
    $write->exec("
SET FOREIGN_KEY_CHECKS = 0; 
SET @tables = NULL;
SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables
  FROM information_schema.tables 
  WHERE table_schema = 'dbextract';

SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;
");

//Create new tables
    $write->exec("
USE dbextract;

CREATE TABLE `bundle_info_extract` (
  `TransactionID` tinytext,
  `BundleID` tinytext,
  `BundleName` tinytext,
  `BundleType` tinytext,
  `OGWOrchestrationType` tinytext,
  `LegacyBundleID` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bundle_package_extract` (
  `TransactionID` tinytext,
  `PackageID` tinytext,
  `BundleID` tinytext,
  `BundleMembership` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `details_extract` (
  `TransactionID` tinytext,
  `PackageID` tinytext,
  `PackageType` tinytext,
  `PackageSubType` tinytext,
  `ProductID` tinytext,
  `ProductType` tinytext,
  `ProductPrice` tinytext,
  `ProductName` tinytext,
  `ProductDescription` tinytext,
  `Options` tinytext,
  `BundleID` tinytext,
  `BundleName` tinytext,
  `ActionID` tinytext,
  `SKU` tinytext,
  `KiasCode` tinytext,
  `SAPCode` tinytext,
  `CableProductID` tinytext,
  `ActivityType` tinytext,
  `NewCustomerInd` tinytext,
  `ProductActivityType` tinytext,
  `UseCaseInd` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `header_extract` (
  `TransactionID` tinytext,
  `StoreID` tinytext,
  `StoreName` tinytext,
  `StoreAddressCity` tinytext,
  `StoreAddressZip` tinytext,
  `StoreAddressStreet` tinytext,
  `AgentID` tinytext,
  `AgentFirstName` tinytext,
  `AgentLastName` tinytext,
  `DealerID` tinytext,
  `DealerAddressStreet` tinytext,
  `DealerAddressZIP` tinytext,
  `DealerAddressCity` tinytext,
  `DealerGroupID` tinytext,
  `DealerGroupName` tinytext,
  `CustomerID` tinytext,
  `CustomerFirstName` tinytext,
  `CustomerLastName` tinytext,
  `CustomerEmail` tinytext,
  `CustomerType` tinytext,
  `CustomerCompanyName` tinytext,
  `CustomerDOB` tinytext,
  `CustomerCTN` tinytext,
  `CustomerCallerID` tinytext,
  `CustomerAddressCity` tinytext,
  `CustomerAddressZIP` tinytext,
  `CustomerAddressStreet` tinytext,
  `NumberPortingStatus` tinytext,
  `ShoppingCartID` tinytext,
  `OfferID` tinytext,
  `OmniusOrderID` tinytext,
  `OGWOrderID` tinytext,
  `TransactionStatus` tinytext,
  `ShoppingCartCreationDate` tinytext,
  `OfferCreateDate` tinytext,
  `OrderCreateDate` tinytext,
  `CustomerInstallationDate` tinytext,
  `CorrelationID` tinytext,
  `UCTTransactionID` tinytext,
  `DealerCode` tinytext,
  `DealerCodeKias` tinytext,
  `DealerCodeFN` tinytext,
  `DealerCodeCable` tinytext,
  `CampaignCode` tinytext,
  `MarketingCode` tinytext,
  `AdvertisingCode` tinytext,
  `AgentBusinessFunctionID` tinytext,
  `AgentBusinessFunctionDescription` tinytext,
  `AgentUserID` tinytext,
  `OrderSubmissionChannel` tinytext,
  `OrderOriginatingChannel` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `lastrun` (
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

    $write->exec("USE " . Mage::getConfig()->getResourceConnectionConfig("default_setup")->dbname . ";");
} catch (Exception $e){
    Mage::getSingleton('core/logger')->logException(new Exception('Couldn\'t initialize dbextract schema: ' . $e->getMessage()));
}
$installer->endSetup();