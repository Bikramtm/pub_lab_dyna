
<?php
/**
 * Add table package_creation_types with columns
 * - entity_id
 * - name
 * - gui_name
 * - sorting
 * - filter_attributes
 * - package_type_code FK to catalog_package_types.package_code
 * - package_creation_group_id FK to package_creation_groups.entity_id
 */


/** @var Mage_Sales_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

// create new table for process context cardinality
if (!$installer->getConnection()->isTableExists($installer->getTable('package_creation_types'))) {
    $packageCreationTypesTable = new Varien_Db_Ddl_Table();
    $packageCreationTypesTable->setName($installer->getTable('package_creation_types'));

    $packageCreationTypesTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            'unsigned' => true,
            'primary' => true,
            'auto_increment' => true,
            'nullable' => false,
        ), 'Package creation types unique identifier')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        ], "The package creation types name to refer to the button")
        ->addColumn('gui_name', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        ], "The package creation types the actual name on the button in the GUI")
        ->addColumn('sorting', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            'unsigned' => true,
            'nullable' => false,
        ), 'numeric value similar as product sorting to determine the order of the buttons')
        ->addColumn('filter_attributes', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        ], "The name(s) of the filter attribute(s) that should be preselected")
        ->addColumn('package_type_code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, [
        ], "The package type code of the package that will be opened when clicking the button ")
        ->addColumn('package_creation_group_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            'unsigned' => true,
            'nullable' => false,
        ), 'The package creation group id that this type belongs to')
        ->addForeignKey($installer->getFkName($packageCreationTypesTable->getName(), 'package_type_code', 'catalog_package_types', 'package_code'),
            'package_type_code', 'catalog_package_types', 'package_code',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->addForeignKey($installer->getFkName($packageCreationTypesTable->getName(), 'package_creation_group_id', 'package_creation_groups', 'entity_id'),
            'package_creation_group_id', 'package_creation_groups', 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

    $installer->getConnection()->createTable($packageCreationTypesTable);
}

$installer->endSetup();