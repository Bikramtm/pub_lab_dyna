<?php
/**
 * Installer that adds a new column in the catalog_package table.
 * Column: redplus_related_product
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageResourceTable = $this->getTable('package/package');

if (!$this->getConnection()->tableColumnExists($this->getTable('package/package'), 'redplus_related_product')) {
    $this->getConnection()->addColumn($packageResourceTable, 'redplus_related_product', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'comment' => 'Column used for setting related products of the package'
    ));
}

$this->endSetup();
