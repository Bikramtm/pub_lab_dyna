<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 *
 * Moved to a greater version due to merge conflicts
 * Installer that adds 1 column for table dyna_package_subtype
 * - package_subtype_visibility
 * Removes 2 columns for table dyna_package_subtype"
 * visible_configurator
 * visible_cart
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageTable = $this->getTable('package/package');
$columnName = 'refused_defaulted_items';

if (!$this->getConnection()->tableColumnExists($packageTable, $columnName)) {
    $this->getConnection()->addColumn($packageTable, $columnName, array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'comment' => 'Column holds defaulted item skus deselected by agent'
    ));
}

$this->endSetup();
