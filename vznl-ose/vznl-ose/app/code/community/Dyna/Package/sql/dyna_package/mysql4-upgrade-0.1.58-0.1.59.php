<?php
/**
 * Installer that changes 1 column  name for catalog package resource:
 * ils_initial_products - renamed to ils_initial_socs
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageResourceTable = $this->getTable('package/package');

if ($this->getConnection()->tableColumnExists($packageResourceTable, 'ils_initial_products') &&
    !$this->getConnection()->tableColumnExists($packageResourceTable, 'ils_initial_socs')) {
    $this->getConnection()->changeColumn($packageResourceTable, "ils_initial_products", "ils_initial_socs", array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Column used for setting package initial component socs for an in life sale flow (ils)'
    ));
} else if (!$this->getConnection()->tableColumnExists($packageResourceTable, 'ils_initial_socs')) {
    $this->getConnection()->addColumn($packageResourceTable, 'ils_initial_socs', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Column used for setting customer all initial component products skus for an in life sale flow (ils)'
    ));
}

$this->endSetup();
