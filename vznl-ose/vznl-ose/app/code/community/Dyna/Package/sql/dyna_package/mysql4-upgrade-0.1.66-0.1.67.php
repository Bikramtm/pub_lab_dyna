<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 *
 * Added column prolongation_info_eligibility to save the prolongation info eligibility at package level (status retrieved from CreditProfile call)
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;
$installer->startSetup();

$packageTable = $this->getTable('package/package');
$columnName = 'prolongation_info_eligibility';

if (!$installer->getConnection()->tableColumnExists($packageTable, $columnName)) {
    $installer->getConnection()->addColumn($packageTable, $columnName, array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 30,
        'comment' => 'Text field to hold the prolongation info eligibility N,E,P,C,T,S,I,X,A)'
    ));
}

$installer->endSetup();
