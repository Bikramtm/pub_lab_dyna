<?php

/**
 * Class Dyna_Package_Model_PackageType
 */
class Dyna_Package_Model_PackageType extends Omnius_Package_Model_PackageType
{
    CONST STR_ID = "package_code";
    /** @var $cache Dyna_Cache_Model_Cache */
    protected $cache = null;
    protected static $packageTypes = array();
    protected static $packageTypeIds = array();

    /**
     * @return Dyna_Cache_Model_Cache|null
     */
    protected function getCache()
    {
        if ($this->cache === null) {
            $this->cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->cache;
    }

    /**
     * Get this instance loaded by package code
     * @param $packageCode
     * @return mixed
     */
    public function loadByCode($packageCode)
    {
        $key = sprintf('package_type_code_%s', $packageCode);

        if (empty(static::$packageTypes[$packageCode])) {
            if ($result = $this->getCache()->load($key)) {
                static::$packageTypes[$packageCode] = unserialize($result);
            } else {
                $result = $this->getCollection()
                    ->addFieldToFilter(self::STR_ID, ["eq" => $packageCode])
                    ->getFirstItem();

                $this->getCache()->save(serialize($result), $key, [Dyna_Cache_Model_Cache::CACHE_TAG], $this->getCache()->getTtl());

                static::$packageTypes[$packageCode] = $result;
            }
        }

        return static::$packageTypes[$packageCode] ?? null;
    }

    /**
     * @param $packageCode
     * @return mixed|null
     */
    public function getPackageTypeIdByCode($packageCode) {
        /** @var Dyna_Package_Model_PackageType $packageType */
        $packageCodes = explode(',', $packageCode);
        $packageCodes = array_map('trim', $packageCodes);
        $packageCode = array_shift($packageCodes);

        if (empty(static::$packageTypeIds[$packageCode])) {
            $packageType = $this->getCollection()
                ->addFieldToFilter(self::STR_ID, $packageCode)
                ->getFirstItem();

            static::$packageTypeIds[$packageCode] = (int)$packageType->getId() ?: null;
        }

        return static::$packageTypeIds[$packageCode];
    }

    /**
     * Get current package code for this instance
     * @param bool $lowerCased
     * @return mixed|string
     */
    public function getPackageCode(bool $lowerCased = false)
    {
        return $lowerCased ? strtolower($this->getData(self::STR_ID)) : $this->getData(self::STR_ID);
    }

    /**
     * Get package subtype instance by package code
     * @param string $packageCode
     * @return Dyna_Package_Model_PackageSubtype
     */
    public function getPackageSubType(string $packageCode)
    {
        $packageCode = strtoupper($packageCode);

        return Mage::getModel('dyna_package/packageSubtype')
            ->getCollection()
            ->addFieldToFilter('type_package_id', array('eq' => $this->getId()))
            ->addFieldToFilter(self::STR_ID, array('eq' => $packageCode))
            ->getFirstItem();
    }

    /**
     * Get all package subtypes instance for current package type
     * @return Dyna_Package_Model_PackageSubtype[]|Dyna_Package_Model_Resource_PackageSubtype_Collection
     * @internal param string $packageCode
     */
    public function getPackageSubTypes()
    {
        return Mage::getModel('dyna_package/packageSubtype')
            ->getCollection()
            ->addFieldToFilter('type_package_id', array('eq' => $this->getId()));
    }
}
