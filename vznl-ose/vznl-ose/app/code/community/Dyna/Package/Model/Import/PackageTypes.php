<?php

/**
 *
 * Class Dyna_Customer_Model_ExpressionLanguage_ProductCategory
 */
class Dyna_Package_Model_Import_PackageTypes extends Dyna_Import_Model_Adapter_Xml
{
    /**
     * Dyna_Package_Model_Import_PackageTypes constructor.
     * @param $args
     */
    public function __construct($args)
    {
        // set log file name
        $this->logFile = "import_package_types.log";

        parent::__construct($args);
    }

    public function import()
    {
        //@todo this needs to support package_subtype_cardinality as the csv import does
        // retrieve data
        $rows = $this->getXmlData(true, false);

        // parse data
        foreach ($rows->PackageType as $packageType) {
            $this->_totalFileRows++;
            $group = trim((string) $packageType->Group);
            $name = trim((string) $packageType->Name);

            if ($group != '') {
                // search for package and update/create
                $package = Mage::getModel('dyna_package/packageType')
                    ->getCollection()
                    ->addFieldToFilter('package_code', ['eq' => $group])
                    ->getFirstItem();

                if (!$package || !$package->getId()) {
                    $package = Mage::getModel('dyna_package/packageType');
                    $package
                        ->setPackageCode($group)
                        ->setFrontEndName($name)
                        ->setStack($this->stack)
                        ->save();
                } else {
                    if ($package->getFrontEndName() != $name) {
                        $package->setFrontEndName($name)
                            ->save();
                    }
                }

                // update/insert subtypes
                foreach ($packageType->SubTypes as $subType) {
                    $this->importSubtype($package, $subType);
                }
            } else {
                $this->_logError('[ERROR] Group not present. Skipping row no ' . $this->_totalFileRows);
                $this->_skippedFileRows++;
                continue;
            }
        }
    }

    protected function importSubtype($packageType, $packageSubtype)
    {
        foreach ($packageSubtype->children() as $subTypeNode) {
            $identifier = (string)$subTypeNode->Identifier;
            $name = (string)$subTypeNode->Name;
            $position = (string)$subTypeNode->Position;
            $defaultCadinality = (string)$subTypeNode->DefaultCadinality;

            $subtype = Mage::getModel('dyna_package/packageSubtype')
                ->getCollection()
                ->addFieldToFilter('type_package_id', $packageType->getId())
                ->addFieldToFilter('package_code', ['eq' => $identifier])
                ->getFirstItem();

            if (!$subtype || !$subtype->getId()) {
                $subtype = Mage::getModel('dyna_package/packageSubtype');
                $subtype
                    ->setTypePackageId($packageType->getId())
                    ->setPackageCode($identifier)
                    ->setFrontEndName($name)
                    ->setStack($this->stack)
                    ->setFrontEndPosition($position)
                    ->setCardinality($defaultCadinality)
                    ->setIgnoredByCompatibilityRules('0')
                    ->save();
            }
        }
    }
}
