<?php

/**
 * Class Dyna_Package_Model_Mysql4_PackageSubtypeCardinality
 */
class Dyna_Package_Model_Mysql4_PackageSubtypeCardinality extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_package/packageSubtypeCardinality', 'entity_id');
    }
}
