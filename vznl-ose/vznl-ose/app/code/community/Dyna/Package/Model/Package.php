<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Package_Model_Package
 * @method string getType() Returns the package type (e.g. mobile, cable_tv, cable_internet_phone, prepaid etc)
 * @method bool getEditingDisabled()
 * @method Dyna_Package_Model_Package setQuote(Dyna_Checkout_Model_Sales_Quote $quote)
 * @method $this setBundleId(int $bundleId)
 * @method int|null getParentId()
 * @method string getPermanentFilters()
 * @method string getSaleType()
 * @method $this setServiceLineId(string $lineId)
 * @method string getPackageCreationTypeId()
 * @method string getParentAccountNumber()
 * @method $this setParentAccountNumber(string $accountNumber)
 * @method $this setServicesSnapshot(string $lineId)
 * @method array getServicesSnapshot()
 * @method string getCtn()
 * @var Dyna_Checkout_Model_Sales_Quote $quote
 */
class Dyna_Package_Model_Package extends Omnius_Package_Model_Package
{
    const PACKAGE_ACTION_NEW = 'NEW';
    const PACKAGE_ACTION_CHANGE = 'CHANGEPRODUCT';
    const PACKAGE_OGW_ORDERLINE_NEW = 'NEW';
    const PACKAGE_ACTION_CHANGE_TARIFF = 'CHANGETARIFF';
    const PACKAGE_ACTION_PROLONGATION = 'PROLONGATION';
    const PACKAGE_ACTION_DEBIT_TO_CREDIT = 'DEBITTOCREDIT';
    const PACKAGE_ACTION_ADD_TO_REDPLUS = 'ADDTOREDPLUS';
    /**
     * @var $catalogHelper Dyna_Catalog_Helper_Data
     */
    protected $catalogHelper;
    protected $sectionItems = array();
    protected $packageSubtypesOrdered = [];
    
    protected $bundle = false;
    /** @var array Dyna_Bundles_Model_BundleRule */
    protected $bundles = false;
    protected $creationType = false;

    /**
     * @return array
     *
     * Get default product order
     */
    public static function getDefaultProductOrder($simcard = false, $cartPackageType = Dyna_Catalog_Model_Type::TYPE_MOBILE)
    {
        /** @var Dyna_Configurator_Helper_Data $helper */
        $helper = Mage::helper('dyna_configurator');

        // In case it is a template package type requested by cart content, strip the dummy 'template-' substring
        $packageType = str_replace("template-", "", $cartPackageType);
        return $helper->getPackageSubtypes($packageType, false, true);
    }

    /**
     * Get title based on package type
     *
     * @return string
     */
    public function getTitle()
    {
        $packageType = strtolower($this->getType());

        switch ($packageType) {
            case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
                $title = Mage::helper('dyna_checkout')->__("Mobile");
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
                $title = Mage::helper('dyna_checkout')->__("Mobile Prepaid");
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
                $title = Mage::helper('dyna_checkout')->__("Cable");
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
                $title = Mage::helper('dyna_checkout')->__("DSL");
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_LTE):
                $title = Mage::helper('dyna_checkout')->__("LTE");
                break;
            default:
                $title = Mage::helper('dyna_checkout')->__("Package");
                break;
        }

        return $title;
    }

    public function getPackageSubtypesByDefaultOrder()
    {
        $this->packageSubtypesOrdered = self::getDefaultProductOrder(false, $this->getType());

        foreach ($this->packageSubtypesOrdered as $subtype) {
            // for Footnote products
            if ($subtype != Dyna_Catalog_Model_Type::SUBTYPE_FOOTNOTE) {
                $this->packageSubtypesOrdered[] = Dyna_Catalog_Model_Type::SUBTYPE_FOOTNOTE;
            }
            //for Mobile Shipping product
            if (in_array($this->getType(),
                array( strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE),
                strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID)))) {
                $this->packageSubtypesOrdered[] = Dyna_Catalog_Model_Type::SUBTYPE_FEE;
            }
            // OMNVFDE-2994: Fees appear in extended cart section for dummy packages
            if (!$this->getData('editing_disabled')) {
                $this->packageSubtypesOrdered[] = Dyna_Catalog_Model_Type::TYPE_OTHER;
            }

            // for Configurable checkout products
            if ($subtype != Dyna_Catalog_Model_Type::SUBTYPE_CONFIGURABLE_CHECKOUT_OPTIONS) {
                $this->packageSubtypesOrdered[] = Dyna_Catalog_Model_Type::SUBTYPE_CONFIGURABLE_CHECKOUT_OPTIONS;
            }
        }

        return $this->packageSubtypesOrdered;
    }

    /**
     * Get packages for extended shopping cart
     *
     * @param $items
     * @param string|null $productsVisibilitySectionToCheck
     * @param bool $pdfSection
     * @param string $guiSection
     * @return array
     */
    public function getPackagesProductsForSidebarSection($items, $productsVisibilitySectionToCheck = null, $pdfSection = false, $guiSection = '')
    {
        /** @var Dyna_Catalog_Helper_Data $catalogHelper */
        $this->catalogHelper = Mage::helper('dyna_catalog');
        $this->getPackageSubtypesByDefaultOrder();
        if ($productsVisibilitySectionToCheck == Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_EXTENDED_SHOPPING_CART
            && $this->getPackageId()
        ) {
            $alternateItems = $this->getAlternateItems();

            if (count($alternateItems)) {
                // Inject ILS changed items in extended cart
                $items = array_merge($alternateItems, $items);
            }
        }

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($items as $item) {
            if ($item instanceof Mage_Sales_Model_Order_Item && $item->getProductActivityType() === Dyna_Superorder_Helper_Client::COMPONENT_ACTION_REMOVE) {
                continue;
            }

            if ($item->isPromo() || $item->isBundlePromo()) {
                continue;
            }
            $promos = [];
            /** @var Dyna_Catalog_Model_Product $product */
            $product = $item->getProduct();
            $itemData = $item->getData();

            /** @var Dyna_Checkout_Model_Sales_Quote_Item $_item */
            foreach ($items as $_item) {
                if (($_item->isPromo() || $_item->isBundlePromo()) && ($_item->getTargetId() == $product->getId() || $_item->getTargetId() == $product->getOldId())) {
                    if ($pdfSection) {
                        $promos[] = $this->getParsedItemDataForPdf($_item, $_item->getProduct(), $productsVisibilitySectionToCheck);
                    } else {
                        $promos[] = $this->getParsedItemData($_item, $_item->getProduct(), $productsVisibilitySectionToCheck, $guiSection);
                    }
                }
            }

            // get the parsed item data that should be displayed
            if ($pdfSection) {
                $parsedItemData = $this->getParsedItemDataForPdf($item, $product, $productsVisibilitySectionToCheck);
            } else {
                $parsedItemData = $this->getParsedItemData($item, $product, $productsVisibilitySectionToCheck, $guiSection);
            }

            if (count($promos)) {
                $parsedItemData['promos'] = $promos;
            }
            // group the items based on the type of the product
            if ($product->isDevice()) {
                $this->getParsedAdditionalItemData($parsedItemData, $itemData);
            } else if ($product->isSubscription() || $product->isPromotion() || $product->isMobileSpecialDiscount() || $product->isDiscount()) {
                $this->getParsedPriceSlidingAdditionalItemData($parsedItemData, $product);
            } else if ($product->isOption()) {
                $this->getParsedAdditionalItemData($parsedItemData, $itemData);
                $this->getParsedPriceSlidingAdditionalItemData($parsedItemData, $product);
            }

            foreach ($product->getType() as $type) {
                $type = strtolower($type);
                $this->sectionItems[$type]['items'][$item->getSku()] = $parsedItemData;
            }
        }

        $packageItemsBySections = $this->getPackageItemsBySections();

        /**
         * if the products visibility is provided => it needs to be checked => we should also know if an entire section has at least product
         * that should be visible;
         * if no product is found visible in a section =>
         * we should not displayed the header/titles for that section
         */
        if ($productsVisibilitySectionToCheck) {
            $this->setSectionVisibilityByType($packageItemsBySections);
        }

        return $packageItemsBySections;
    }

    /**
     * Get packages for pdfs (offer and call summary)
     *
     * @param $items
     * @param null $productsVisibilitySectionToCheck
     * @return array
     */
    public function getPackagesProductsForPdfs($items, $productsVisibilitySectionToCheck = null)
    {
        $result = $this->getPackagesProductsForSidebarSection($items, $productsVisibilitySectionToCheck, true);
        if (!empty($result[strtolower(Dyna_Catalog_Model_Type::TYPE_OTHER)]['items'])) {
            foreach ($result[strtolower(Dyna_Catalog_Model_Type::TYPE_OTHER)]['items'] as $key => $itemData) {
                if (!((float)$itemData['maf_row_total_incl_tax'] > 0) && !((float)$itemData['price_incl_tax'] > 0)) {
                    unset($result[strtolower(Dyna_Catalog_Model_Type::TYPE_OTHER)]['items'][$key]);
                }
            }
        }

        return $result;
    }

    public function hasSubscriptionProductOnPrepaidPackage()
    {
        if ($this->isPrepaid()) {
            $items = $this->getData('items');
            foreach ($items as $item) {
                $product = $item->getData('product');
                $subType = current($product->getType());
                if ($subType == Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION) {
                    return true;
                }
            }
        }
        return false;
    }

    public function hasHardwareProductOnPrepaidPackage()
    {
        if ($this->isPrepaid()) {
            $items = $this->getData('items');
            foreach ($items as $item) {
                $product = $item->getData('product');
                $subType = current($product->getType());
                if ($subType == Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the current package status (OPEN or COMPLETED), determined by the status of package items
     * Logic is moved to Configuration Model, preventing model from beying to heavy
     * @return Dyna_Package_Model_Configuration
     */
    public function getPackageConfiguration()
    {
        /** @var Dyna_Package_Model_Configuration */
        return Mage::getModel('package/configuration')
            ->setPackage($this);
    }

    public function getPackageSection()
    {
        $type = $this->getType();

        if (in_array($type, Dyna_Catalog_Model_Type::getFixedPackages())) {
            return "fixed";
        }

        if (in_array($type, Dyna_Catalog_Model_Type::getMobilePackages())) {
            return "mobile";
        }

        if (in_array($type, Dyna_Catalog_Model_Type::getCablePackages())) {
            return "cable";
        }

        Mage::throwException("Got an invalid package type: " . $type);
        return "";
    }

    /**
     *
     * @param null $ofType String or array of strings representing the type(s) to retrieve
     * @param null $notOfType String or array of strings representing type(s) to skip
     * @return Dyna_Checkout_Model_Sales_Quote_Item[]|Dyna_Checkout_Model_Sales_Order_Item[]
     */
    public function getItems($ofType = null, $notOffType = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item[] $allItems */
        if ($this->getQuoteId()) {
            $allItems = $this->getQuote()->getPackageItems($this->getPackageId());
        } else {
            $allSuperorderItems = Mage::getModel('superorder/superorder')->load($this->getOrderId())->getOrderedItems(true);
            $allItems = isset($allSuperorderItems[$this->getPackageId()]) ? $allSuperorderItems[$this->getPackageId()] : array();
        }

        if ($ofType || $notOffType) {
            if ($ofType) {
                $ofType = is_array($ofType) ? $ofType : array($ofType);
                $ofType = array_map('strtolower', $ofType);
            }
            if ($notOffType) {
                $notOffType = is_array($notOffType) ? $notOffType : array($notOffType);
                $notOffType = array_map('strtolower', $notOffType);
            }
            // Get attribute option value
            $attributeCode = Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR;

            /** @var Dyna_Checkout_Model_Sales_Quote_Item[] $filteredItems */
            $filteredItems = [];
            foreach ($allItems as $item) {
                if (($ofType && in_array(strtolower($item->getProduct()->getAttributeText($attributeCode)), $ofType)) ||
                    ($notOffType && !in_array(strtolower($item->getProduct()->getAttributeText($attributeCode)), $notOffType))
                ) {
                    $filteredItems[] = $item;
                }
            }

            return $filteredItems;
        } else {
            return $allItems;
        }
    }

    /**
     * Determines whether or not current package is of type mobile
     * @return bool
     */
    public function isMobile()
    {
        return in_array(strtolower($this->getType()), Dyna_Catalog_Model_Type::getMobilePackages());
    }

    public function isPrepaid()
    {
        return strtolower($this->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID);
    }

    public function isMobilePostpaid()
    {
        return strtolower($this->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE);
    }

    /**
     * Determines whether or not current package is of type cable
     * @return bool
     */
    public function isCable()
    {
        return in_array(strtolower($this->getType()), Dyna_Catalog_Model_Type::getCablePackages());
    }

    /**
     * Determines whether or not current package is of type fixed / dsl
     * @return bool
     */
    public function isFixed()
    {
        return in_array(strtolower($this->getType()), Dyna_Catalog_Model_Type::getFixedPackages());
    }

    public function isDSL()
    {
        return strtolower($this->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL);
    }

    public function isLTE()
    {
        return strtolower($this->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_LTE);
    }

    /**
     * Determine the type of action to be used for submitOrder
     * @return string
     */
    public function getPackageAction()
    {
        $processContext = $this->getSaleType();
        $packageAction = self::PACKAGE_ACTION_NEW;
        $packageType = strtolower($this->getType());
        switch ($processContext) {
            case Dyna_Catalog_Model_ProcessContext::ACQ:
                if ($this->isPartOfRedPlus() && $packageType == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) && $this->getEditingDisabled()) {
                    $packageAction = self::PACKAGE_ACTION_ADD_TO_REDPLUS;
                }elseif($this->getBundles(true) && !empty($this->getInstalledBaseProducts())) {
                    $packageAction = self::PACKAGE_ACTION_CHANGE;
                }else{
                    $packageAction = self::PACKAGE_ACTION_NEW;
                }
                break;
            case Dyna_Catalog_Model_ProcessContext::REO:
            case Dyna_Catalog_Model_ProcessContext::RETNOBLU:
            case Dyna_Catalog_Model_ProcessContext::RETBLU:
            case Dyna_Catalog_Model_ProcessContext::RETTER:
                $packageAction = self::PACKAGE_ACTION_PROLONGATION;
                break;
            case Dyna_Catalog_Model_ProcessContext::ILSBLU:
                if (!$this->isCable()) {
                    if ($this->isPartOfRedPlus() && !$this->getParentId()) {
                        $packageAction = self::PACKAGE_ACTION_ADD_TO_REDPLUS;
                    } else {
                        if ($alternateItems = $this->getAlternateItems()) {
                            foreach ($alternateItems as $item) {
                                if ($item->getProduct()->isSubscription()) {
                                    $packageAction = self::PACKAGE_ACTION_CHANGE_TARIFF;
                                    break;
                                }
                            }
                            if ($packageAction != self::PACKAGE_ACTION_CHANGE_TARIFF) {
                                $packageAction = self::PACKAGE_ACTION_CHANGE;
                            }
                        } else {
                            $packageAction = self::PACKAGE_ACTION_CHANGE_TARIFF;
                        }
                    }
                } else {
                    $packageAction = self::PACKAGE_ACTION_CHANGE;
                }
                break;
            case Dyna_Catalog_Model_ProcessContext::ILSNOBLU:
                $packageAction = self::PACKAGE_ACTION_CHANGE;
                break;
            case Dyna_Catalog_Model_ProcessContext::MIGCOC:
            case Dyna_Catalog_Model_ProcessContext::MIGNOCOC:
                if (in_array($packageType, [strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID), strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)])) {
                    $packageAction = self::PACKAGE_ACTION_DEBIT_TO_CREDIT;
                }
                break;
        }
        $this->setActivityType($packageAction)->save();
        return $packageAction;
    }


    /**
     * Get all packages of a certain "type" that belong to a superOrder with a specific "ogwId"
     *
     * @param string $ogwId
     * @param string $packageType
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getPackagesWithOgwId($ogwId = null, $packageType = null)
    {
        $collection = Mage::getResourceModel('package/package_collection')->addFieldToFilter('main_table.type', ['eq' => $packageType]);

        $collection->getSelect()
            ->join(
                array('superorder' => 'superorder'),
                'main_table.order_id = superorder.entity_id',
                ['superorder.entity_id AS super_entity_id']
            )
            ->where('superorder.ogw_id = ?', $ogwId);

        return $collection;
    }

    /**
     * Get all packages with a specific customer_number
     *
     * @param string $customerNo
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getPackagesWithCustomerNo($customerNo = null)
    {
        return $this->getCollection()->addFieldToFilter('main_table.customer_number', ['eq' => $customerNo]);
    }

    /**
     * Identify the ogw order line id node for the submit order
     * @param $customer
     * @return string
     */
    public function getOGWOrderLineIDNode()
    {
        $result = [];
        $result['PhysicalAttribute'] = null;
        if ($this->isMobile()) {
            foreach ($this->getItems() as $item) {
                /** @var Dyna_Checkout_Model_Sales_Order_Item $item */
                if (null !== $item->getProduct()->getData('ident_needed')) {
                    $identificationNeededValue = $item->getProduct()->getData('ident_needed') ? 'Y' : 'N';
                    $result['PhysicalAttribute'] = array(
                        'AttributeID' => $identificationNeededValue,
                    );
                    break;
                }
            }
        }

        // Either service line ID or NEW for new orders (@see VFDED1W3S-1501)
        $result['ID'] = $this->getServiceLineId() ?: self::PACKAGE_OGW_ORDERLINE_NEW;

        return $result;
    }


    /**
     * @return array
     */
    protected function getPackageItemsBySections()
    {
        if (!$this->packageSubtypesOrdered) {
            $this->packageSubtypesOrdered = $this->getPackageSubtypesByDefaultOrder();
        }

        $packageItemsBySections = [];
        foreach ($this->packageSubtypesOrdered as $subType) {
            $subType = strtolower($subType);
            $itemCollection = $this->sectionItems[$subType] ?? array();
            $itemCollection['show_prices'] = !in_array($subType, Dyna_Catalog_Model_Type::$footnotes);

            if (isset($itemCollection['items']) && count($itemCollection['items'])) {
                $packageItemsBySections[$subType] = $itemCollection;
                $packageType = Mage::getModel('dyna_package/packageType')->loadByCode($this->getType());
                /**
                 * @var Dyna_Package_Model_Packagetype $packageType
                 */
                $sectionName = ($packageType->getId() && $packageType->getPackageSubType($subType)) ?
                    $packageType->getPackageSubType($subType)->getFrontEndName() : Mage::helper('dyna_checkout')->__(ucfirst($subType));
                $sectionName = ($sectionName) ?: Mage::helper('dyna_checkout')->__(ucfirst($subType));
                $packageItemsBySections[$subType]['section_name'] = $sectionName;
            }
        }

        return $packageItemsBySections;
    }

    /**
     * See if a section (a section in the generated pdfs is the merge of multiple package subtypes) should be displayed:
     *- if must have at least 1 product that belongs to one of the merged packages subtypes section has
     * visibility set to true (package subtype visibility and product visibility)
     *
     * @param array $packageItemsBySections
     */
    protected function setMergedSectionVisibilityByType(&$packageItemsBySections)
    {
        $sectionHardwareDeviceTariffVisibility = false;
        foreach ($packageItemsBySections as $section => $sectionItems) {
            if (in_array($section, Dyna_Catalog_Model_Type::$subscriptions) ||
                in_array($section, Dyna_Catalog_Model_Type::$devices)
            ) {
                if ($sectionItems['section_visibility'] && !$sectionHardwareDeviceTariffVisibility) {
                    $sectionHardwareDeviceTariffVisibility = true;
                }
            }
        }
        foreach ($packageItemsBySections as $section => &$sectionItems) {
            if (in_array($section, Dyna_Catalog_Model_Type::$subscriptions) ||
                in_array($section, Dyna_Catalog_Model_Type::$devices)
            ) {
                $sectionItems['section_visibility'] = $sectionHardwareDeviceTariffVisibility;
            }
        }
    }

    /**
     * See if a section (strictly based on a package subtype) should be displayed:
     *- if must have at least 1 product from that package subtype section has
     * visibility set to true (package subtype visibility and product visibility)
     *
     * @param array $packageItemsBySections
     */
    protected function setSectionVisibilityByType(&$packageItemsBySections)
    {
        /**
         * if the products visibility is provided => it needs to be checked => we should also know if an entire section has at least product
         * that should be visible;
         * if no product is found visible in a section => we should not displayed the header/titles for that section
         */
        foreach ($packageItemsBySections as &$sectionItems) {
            $sectionIsVisible = false;
            if (isset($sectionItems['items'])) {
                foreach ($sectionItems['items'] as $product) {
                    if ($product['product_visibility'] && !$sectionIsVisible) {
                        $sectionIsVisible = true;
                        break;
                    }
                }
            }
            $sectionItems['section_visibility'] = $sectionIsVisible;
        }
    }

    /**
     * Return the fields from the item needed to be displayed in the extended cart
     *
     * @param Dyna_Checkout_Model_Sales_Quote_Item $item
     * @param  Dyna_Catalog_Model_Product $product
     * @param $productsVisibilitySectionToCheck
     * @param string  $guiSection
     * @return array
     */
    protected function getParsedItemData($item, $product, $productsVisibilitySectionToCheck = null, $guiSection = '')
    {
        $itemData = $item->getData();
        $parsedItemData['name'] = $this->getProductNameForSectionDisplay($product, $guiSection, $productsVisibilitySectionToCheck);
        $parsedItemData['sku'] = $itemData['sku'];
        $parsedItemData['show_prices'] = true;
        $parsedItemData['is_alternate'] = isset($itemData['is_alternate']) ? $itemData['is_alternate'] : false;
        $parsedItemData['system_action'] = isset($itemData['system_action']) ? $itemData['system_action'] : null;
        $parsedItemData['is_contract_drop'] = isset($itemData['is_contract_drop']) ? $itemData['is_contract_drop'] : null;

        if ((isset($itemData['is_promo']) && $itemData['is_promo']) || (isset($itemData['is_bundle_promo']) && $itemData['is_bundle_promo'])) {
            $parsedItemData['maf_discount'] = $itemData['applied_maf_promo_amount'];
            $parsedItemData['price_discount'] = $itemData['applied_promo_amount'];
        }
        if ($hasMaf = $product->hasMafPrice()) {
            $parsedItemData['maf_row_total_incl_tax'] = $itemData['maf_row_total_incl_tax'] ?? 0;
        }
        if ($hasOTC = $product->hasOTCPrice()) {
            $parsedItemData['row_total_incl_tax'] = $itemData['row_total_incl_tax'] ?? 0;
        }
        // special product with no package subtype (eg. Mobile Shipping)
        if (!$hasMaf && !$hasOTC) {
            $parsedItemData['maf_row_total_incl_tax'] = $itemData['maf_row_total_incl_tax'] ?? 0;
            $parsedItemData['row_total_incl_tax'] = $itemData['row_total_incl_tax'] ?? 0;
        }

        if ($item->hasMixmatchPrice()) {
            $parsedItemData['maf_row_total_incl_tax'] = $item->getMixmatchMafPriceWithTax();
        }

        if ($productsVisibilitySectionToCheck) {
            $parsedItemData['product_visibility'] = $this->catalogHelper->getVisibility(
                $productsVisibilitySectionToCheck,
                $parsedItemData['sku'],
                $product
            );
        }

        $parsedItemData['isPromotion'] = $product->isPromotion();
        $parsedItemData['isSubscription'] = $product->isSubscription();

        return $parsedItemData;
    }

    /**
     * @param Dyna_Catalog_Model_Product $product
     * @param bool $pdfName
     * @param string $guiSection
     * @return string
     */
    protected function getProductNameForSectionDisplay(Dyna_Catalog_Model_Product $product, $guiSection = '', $productsVisibilitySectionToCheck = false)
    {
        $packageType = $product->getType();
        $packageSubtypeCode = array_shift($packageType);
        if (in_array(strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE), explode(",", $product->getPackageType())) &&
            strtolower($packageSubtypeCode) == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_FOOTNOTE) && $product->getFootnoteText()
        ) {
            return $product->getFootnoteText();
        }

        if(!$guiSection && $productsVisibilitySectionToCheck) {

            $visibility = null;

            switch($productsVisibilitySectionToCheck) {
                case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY:
                    $visibility = $product->getDisplayNameInventory() ?: $product->getName();
                    break;
                case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CONFIGURATOR:
                    $visibility = $product->getDisplayNameConfigurator() ?: $product->getName();
                    break;
                case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_SHOPPING_CART:
                    $visibility = $product->getDisplayNameCart() ?: $product->getName();
                    break;
                case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_EXTENDED_SHOPPING_CART:
                case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_ORDER_OVERVIEW:
                case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_VOICELOG:
                case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_OFFER_PDF:
                case Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CALL_SUMMARY:
                    $visibility = $product->getDisplayNameCommunication() ?: $product->getName();
                    break;
                default:
                    $visibility = $product->getName();
            }

            return $visibility;
        }

        $section = null;

        switch($guiSection) {
            case Dyna_Catalog_Model_Product::GUI_CONFIGURATOR:
                $section = $product->getDisplayNameConfigurator() ?: $product->getName();
                break;
            case Dyna_Catalog_Model_Product::GUI_COMMUNICATION:
                $section = $product->getDisplayNameCommunication() ?: $product->getName();
                break;
            case Dyna_Catalog_Model_Product::GUI_INVENTORY:
                $section = $product->getDisplayNameInventory() ?: $product->getName();
                break;
            case Dyna_Catalog_Model_Product::GUI_CART:
                $section = $product->getDisplayNameCart() ?: $product->getName();
                break;
            default:
                $section = $product->getName();

        }

        return $section;

    }

    protected function getParsedItemDataForPdf($item, $product, $productsVisibilitySectionToCheck)
    {
        $parsedItemData = $this->getParsedItemData($item, $product, $productsVisibilitySectionToCheck);
        $parsedItemData['product'] = $product;
        return $parsedItemData;
    }

    /**
     * For subscription we have also a contract period (the period is stored either in the attribute initial_period for mobile
     * of in the attribute contract_period for cable)
     * For cable subscriptions we have also a price sliding per month that should be displayed
     *
     * @param $product
     * @param array $parsedItemData
     */
    protected function getParsedPriceSlidingAdditionalItemData(&$parsedItemData, $product)
    {
        $parsedItemData['period'] = '';
        // the period is stored for cable in the attribute "contract_period"
        if (isset($product['contract_period']) && $product['contract_period']) {
            $parsedItemData['period'] = $product['contract_period'];
        } else if (isset($product['initial_period']) && $product['initial_period']) {
            $parsedItemData['period'] = $product['initial_period'];
        } else if (isset($product['minimum_contract_duration']) && $product['minimum_contract_duration']) {
            $parsedItemData['period'] = $product['minimum_contract_duration'];
        }
        // for cable packages for subscription products we need to display the price sliding per months
        if (in_array($this->getType(), Dyna_Catalog_Model_Type::getCablePackages())) {
            $parsedItemData['price_sliding'] = isset($product['price_sliding']) ? $product['price_sliding'] : '';
            $parsedItemData['price_repeated'] = isset($product['price_repeated']) ? $product['price_repeated'] : '';
        }
    }

    /**
     * For devices we should display also the original price
     *
     * @param array $parsedItemData
     * @param array $item
     */
    protected function getParsedAdditionalItemData(&$parsedItemData, $item)
    {
        /** @var Dyna_Catalog_Model_Product $product */
        $product = $item['product'];

        if (!empty($item['original_custom_price'])) {
            // the devices have an initial price before the match rules are applied
            $parsedItemData['original_custom_price'] = $product ? $product->getData('price') : $item['original_custom_price'];
        }

        if (!empty($item['mixmatch_maf_subtotal']) && $product) {
            $parsedItemData['original_custom_maf'] = $product->getData('maf');
        }
    }

    /**
     * Overwrite get delivery order as bundle packages will not have items
     */
    public function getDeliveryOrder()
    {
        $superorder = Mage::getModel('superorder/superorder')->load($this->getOrderId());
        return $superorder->getOrders(true)->getFirstItem();
    }

    /**
     * Determines if the current package is cross sell by comparing the customer's account category with the package type
     */
    public function isCrossSell()
    {
        $customerType = Mage::getSingleton('customer/session')->getCustomer()->getAccountCategory();

        if (in_array($customerType, array(
                Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD,
                Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_CABLE
            )) && !$this->isCable()
        ) {
            return true;
        }
        if (in_array($customerType, array(
                Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_DSL,
                Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN
            )) && !$this->isFixed()
        ) {
            return true;
        }
        if (in_array($customerType, array(
                Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS,
                Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE
            )) && !$this->isMobile()
        ) {
            return true;
        }

        return false;
    }

    /**
     * Return service address data as array
     * @return mixed
     */
    public function getServiceAddressData()
    {
        return json_decode($this->getData("service_address_data"), true);
    }

    /**
     * Before deleting package, send notification that packages have been changed
     * @return Mage_Core_Model_Abstract
     */
    public function delete()
    {
        //VFDED1W3S-1108 (merge with items that have preselect set to 0)
        $items = $this->getAllItems(true, 'both');
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($items as $item) {
            $item->isDeleted(true);
            if ($item->getAlternateQuoteId()) {
                $item->save();
            } else if ($item->getIsContract()) {
                $item->setAlternateQuoteId($item->getQuoteId());
            }
        }
        Mage::dispatchEvent("packages_list_altered", array('altered_package' => $this));
        $this->getQuote()->clearCachedAlternateItems($this->getPackageId());

        return parent::delete();
    }

    /**
     * After saving, notifying everybody that packages have been hanged
     * @return $this
     */
    public function save()
    {
        parent::save();

        Mage::dispatchEvent("packages_list_altered", array('altered_package' => $this));

        return $this;
    }

    /**
     * Determine whether or not current package is part of a
     * SurfSofort bundle
     *
     * @return bool
     */
    public function isPartOfSuso()
    {
        foreach ($this->getBundles() as $bundle) {
            if ($bundle->isSuso()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether or not current package is part of a
     * RedPlus group
     *
     * @return bool
     */
    public function isPartOfRedPlus()
    {
        foreach ($this->getBundles() as $bundle) {
            if ($bundle->isRedPlus()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether or not current package is part of a
     * LowEntry bundle
     *
     * @return bool
     */
    public function isPartOfLowEntry()
    {
        foreach ($this->getBundles() as $bundle) {
            if ($bundle->isLowEntry()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether or not current package is part of a
     * GigaKombi bundle
     *
     * @return bool
     */
    public function isPartOfGigaKombi()
    {
        foreach ($this->getBundles() as $bundle) {
            if ($bundle->isGigaKombi()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return null|string
     *
     * Determine whether or not current package is part of a
     * GigaKombi bundle
     *
     * @return bool
     */
    public function isPartOfIntrastack()
    {
        foreach ($this->getBundles() as $bundle) {
            if ($bundle->isIntraStack()) {
                return true;
            }
        }

        return false;
    }

    public function getGigaKombiAccountNumber()
    {
        if (!$this->isPartOfGigaKombi()) {
            return null;
        }

        $bundleId = $this->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_KOMBI)->getId();

        foreach ($this->getPackagesInSameBundle($bundleId, true) as $package) {
            if ($accountNumber = $package->getParentAccountNumber()) {
                return $accountNumber;
            }
        }

        return null;
    }

    /**
     * Returns all bundles of this package
     * @return Dyna_Bundles_Model_BundleRule[]
     */
    public function getBundles($excludeRedplus = false)
    {
        if ($this->bundles === false) {
            $this->bundles = array();

            $bundlePackageRelations = Mage::getModel('dyna_bundles/bundlePackages')
                ->getCollection()
                ->addFieldToFilter('package_id', $this->getEntityId());

            /** @var Dyna_Bundles_Model_BundlePackages $relation */
            foreach ($bundlePackageRelations as $relation) {
                /** @var Dyna_Bundles_Model_BundleRule $bundle */
                $bundle = Mage::getModel('dyna_bundles/bundleRule')->load($relation->getBundleRuleId());

                if ($excludeRedplus && $bundle->isRedPlus()) {
                    continue;
                }
                $this->bundles[] = $bundle;
            }

            return $this->bundles;
        }

        return $this->bundles;
    }

    /**
     * Determine whether or not current package is part of bundle
     * @param $bundleId
     * @return Dyna_Bundles_Model_BundleRule|null
     */
    public function getBundleById($bundleId)
    {
        foreach ($this->getBundles(true) as $bundle) {
            if ($bundle->getId() == $bundleId) {
                return $bundle;
            }
        }

        return null;
    }

    /**
     * Returns a bundle that the current package is part of, with the
     * specified type
     *
     * @param string $type
     * @return Dyna_Bundles_Model_BundleRule|null
     */
    public function getBundleOfType(string $type)
    {
        $bundles = $this->getBundles();

        foreach ($bundles as $bundle) {
            if ($bundle->getType() === $type) {
                return $bundle;
            }
        }

        return null;
    }

    /**
     * Overriding omnius package getQuote method to return cart session quote instead of loading it from db
     * @return Mage_Core_Model_Abstract|Dyna_Checkout_Model_Sales_Quote
     */
    public function getQuote()
    {
        if (!$this->quote) {
            $sessionQuote = Mage::getSingleton('checkout/cart')->getQuote();
            if ($this->getQuoteId() == $sessionQuote->getId()) {
                $this->quote = $sessionQuote;
            } else {
                $this->quote = parent::getQuote();
            }
        }

        return $this->quote;
    }

    /**
     * Return related product id (of type owner)
     * @return int|null
     */
    public function getRelatedProductId()
    {
        $relatedProductId = null;
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItem */
        foreach ($this->getQuote()->getAllItems() as $quoteItem) {
            if ($quoteItem->getPackageId() !== $this->getPackageId()) {
                continue;
            }

            if ($quoteItem->getProduct()->isRedPlusOwner()) {
                $relatedProductId = $quoteItem->getProductId();
                break;
            }
        }

        return (int)$relatedProductId;
    }

    /**
     * Method that returns the product ids for quote items that are bundle components
     * @return array
     */
    public function getBundleComponents()
    {
        $productIds = array();

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItem */
        foreach ($this->getQuote()->getAllItems() as $quoteItem) {
            if ($quoteItem->getPackageId() !== $this->getPackageId()) {
                continue;
            }

            if ($quoteItem->getBundleComponent()) {
                $productIds[] = (int)$quoteItem->getProductId();
            }
        }

        return $productIds;
    }


    /**
     * Get all items in current package
     * @return Dyna_Checkout_Model_Sales_Quote_Item[]
     */
    public function getAllItems($includingAlternate = false, $preselect = true, $clearCachedItems = false, $includingDeleted = false)
    {
        $items = array();
        // if quote package, load from quote
        if ($this->getQuoteId()) {
            if ($includingAlternate && $clearCachedItems) {
                $this->getQuote()->clearAlternateItems();
            }

            foreach ($this->getQuote()->getAllItems(false, $includingDeleted) as $quoteItem) {
                if ($quoteItem->getPackageId() != $this->getPackageId()) {
                    continue;
                }

                $items[] = $quoteItem;
            }
            if ($includingAlternate) {
                $items = array_merge($items, $this->getQuote()->getAlternateItems($this->getPackageId(), false, $preselect));
            }
        // if order package, load from order
        } elseif ($this->getOrderId()) {
            $items = $this->getPackageItems();
        }

        return $items;
    }

    /**
     * Return quote item by product id
     * @param $productId
     * @return Dyna_Checkout_Model_Sales_Quote_Item[]|Dyna_Checkout_Model_Sales_Quote_Item|null
     */
    public function getItemsByProductId($productIds, $skipContracts = false)
    {
        $quoteItems = array();
        $ids = is_array($productIds) ? $productIds : array($productIds);
        foreach ($this->getAllItems() as $item) {
            if (in_array($item->getProductId(), $ids)) {
                if ($skipContracts && $item->isContract()) {
                    continue;
                }
                $quoteItems[] = $item;
            }
        }

        return is_array($productIds) ? $quoteItems : current($quoteItems);
    }

    /**
     * Return an array of productSku => productId
     * @return int[]
     */
    public function getAllProductIs()
    {
        $ids = array();
        foreach ($this->getAllItems() as $item) {
            $ids[$item->getSku()] = $item->getProductId();
        }

        return $ids;
    }

    /**
     * Returns the packages that are part of the same bundle with the
     * current package.
     *
     * @param int $bundleId
     * @param bool $excludeCurrentPackage
     * @return Dyna_Package_Model_Package[]
     */
    public function getPackagesInSameBundle($bundleId, $excludeCurrentPackage = false)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();
        $cartPackages = $quote->getCartPackages();
        $bundledPackages = array();

        $packageBundleRelations = Mage::getModel('dyna_bundles/bundlePackages')
            ->getCollection()
            ->addFieldToFilter('bundle_rule_id', $bundleId);

        foreach ($cartPackages as $cartPackage) {
            foreach ($packageBundleRelations as $relation) {
                if ($cartPackage->getEntityId() == $relation->getPackageId()) {
                    if ($excludeCurrentPackage) {
                        if ($this->getPackageId() != $cartPackage->getPackageId()) {
                            $bundledPackages[] = $cartPackage;
                        }
                    } else {
                        $bundledPackages[] = $cartPackage;
                    }
                }
            }
        }

        return $bundledPackages;
    }

    /**
     * Method that clears bundles property set on current package instance
     * @return $this
     */
    public function clearCachedBundles()
    {
        $this->bundles = false;

        return $this;
    }

    /**
     * Syncs current package bundle_types field with bundle_packages table
     * @return $this
     */
    public function updateBundleTypes()
    {
        $this->clearCachedBundles();

        $packageBundles = $this->getBundles();
        $packageBundleTypes = [];
        foreach ($packageBundles as $bundle)
        {
            $packageBundleTypes[] = $bundle->getBundleType();
        }
        $this->setBundleTypes(implode(',', array_unique($packageBundleTypes)));
        $this->save();

        return $this;
    }

    /**
     * Check if current package has children or not.
     *
     * @return bool
     */
    public function hasChildren()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();
        $cartPackages = $quote->getCartPackages();

        foreach ($cartPackages as $cartPackage) {
            if ($this->getEntityId() == $cartPackage->getParentId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns current package children
     * @return Dyna_Package_Model_Package[]
     */
    public function getChildPackages()
    {
        $children = array();

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();
        $cartPackages = $quote->getCartPackages();
        foreach ($cartPackages as $cartPackage) {
            if ($this->getEntityId() == $cartPackage->getParentId()) {
                $children[] = $cartPackage;
            }
        }

        return $children;
    }

    /**
     * ILS Contract items that were removed from quote
     * @param boolean $preselect
     * @return mixed
     */
    public function getAlternateItems($preselect = true)
    {
        $quoteId = null;

        if ($this->getQuoteId()) {
            $quoteId = $this->getQuoteId();

            /** @var Mage_Sales_Model_Quote_Item $collection */
            $select = Mage::getModel('sales/quote_item')->getCollection()
                ->getSelect($this->getQuote())
                ->reset(Zend_Db_Select::WHERE)
                ->where('quote_id IS NULL')
                ->where('alternate_quote_id = ?', $quoteId)
                ->where('package_id = ?', $this->getPackageId())
                ->where('preselect_product = ?', $preselect)
                ->order('package_id', 'asc');

            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');

            $alternateItems = [];
            $arrayItems = $conn->fetchAll($select);

            if ($arrayItems) {
                foreach ($arrayItems as $arrayItem) {
                    $arrayItem['is_alternate'] = true;
                    $alternateItems[] = Mage::getModel('sales/quote_item')->addData($arrayItem);
                }
            }
        } else {
            $allSuperorderItems = Mage::getModel('superorder/superorder')->load($this->getOrderId())->getOrderedItems(true);
            $alternateItems = isset($allSuperorderItems[$this->getPackageId()]) ? $allSuperorderItems[$this->getPackageId()] : array();

            foreach ($alternateItems as $key => $droppedItem) {
                if (!$droppedItem->getIsContractDrop()) {
                    unset($alternateItems[$key]);
                }
            }
        }

        return $alternateItems;
    }

    /**
     * Determines if the current package has items in it which require an additional birthdate input field.
     * Mostly used for YOUNG subscriptions
     */
    public function hasYoungItems() : bool
    {
        $youngMobileCategories = array_map("strtoupper", explode(',', Dyna_Catalog_Model_Type::getMobileYoungCategory()));
        foreach($this->getAllItems() as $item) {
            $productCategoryNames = array_map("strtoupper", $item->getProduct()->getCategoryNames());
            if (array_intersect($productCategoryNames, $youngMobileCategories)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determines if the current package has items in it which require an additional birthdate input field.
     * Mostly used for YOUNG subscriptions
     */
    public function hasYoubiageItems() : bool
    {
        foreach($this->getItems() as $item) {
            /** @var Mage_Catalog_Model_Product $catalogProduct */
            $catalogProduct = Mage::getModel('catalog/product')->load($item->getProduct()->getId());

            $productCategories = $catalogProduct->getCategoryIds();

            /** @var Mage_Catalog_Model_Category $firstCategory */
            foreach($productCategories as $category) {
                if(in_array(Mage::getModel('catalog/category')
                    ->load($category)
                    ->getName(),
                    explode(',', Dyna_Catalog_Model_Type::getYouBiageCategory()))
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasYouBiageOption() : bool {
        foreach ($this->getItems() as $item) {
            if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::getYouBiageOption()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determines if the current package is in edit mode
     */
    public function isInEditMode() : bool
    {
        $items = array_merge($this->getItems(), $this->getAlternateItems());
        foreach ($items as $item) {
            if( $item->getData( 'system_action' ) == Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if package contains any product of type handset (item with extra indicator HIS)
     * @return bool
     */
    public function hasHandset()
    {
        foreach($this->getItems() as $item){
            if($item->getProduct()->getExtraIndicator('HIS')){
                return true;
            }
        }

        return false;
    }
    /**
     * Returns the creation type of the current package
     *
     * @return Dyna_Package_Model_PackageCreationTypes
     */
    public function getCreationType() : Dyna_Package_Model_PackageCreationTypes
    {
        if ($this->creationType === false) {
            $this->creationType = Mage::getModel('dyna_package/packageCreationTypes')
                ->getCollection()
                ->addFieldToFilter('entity_id', $this->getPackageCreationTypeId())
                ->getFirstItem();
        }

        return $this->creationType;
    }

    /**
     * Determines whether the current package is Red+
     *
     * @return bool
     */
    public function isRedPlus() : bool
    {
        return strtolower($this->getCreationType()->getPackageTypeCode()) == strtolower(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS);
    }

    public function getServiceLineId()
    {
        return $this->getData('service_line_id') !== null ? $this->getData('service_line_id') : '';
    }


    /**
     * Return all quote items skus that are assigned to this package
     * @return array
     */
    public function getQuoteItemsPackageSkus()
    {
        $skus = array();
        foreach ($this->getQuote()->getAllItems() as $item) {
            if ($item->getPackageId() != $this->getPackageId()) {
                continue;
            }
            $skus[] = $item->getSku();
        }

        return $skus;
    }

    /**
     * Build checksum on current package out of products skus
     * @return $this
     */
    public function buildChecksum()
    {
        $this->setChecksum(implode(",", $this->getQuoteItemsPackageSkus()))->save();

        return $this;
    }

    /**
     * Check if previous package skus are the same as current package skus
     * @return bool
     */
    public function isValidChecksum()
    {
        $packageSkus = explode(",", $this->getChecksum());
        $quoteSkus = $this->getQuoteItemsPackageSkus();

        return (bool)(array_diff($packageSkus, $quoteSkus) === array_diff($quoteSkus, $packageSkus));
    }

    /**
     * Get services snapshot from customer session and save it on current package
     * @return $this
     */
    public function takeServicesSnapshot()
    {
        $snapShot = array();
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $serviceResponses = $customerSession->getServicesResponse() ?: array();
        foreach ($serviceResponses as $serviceName => $responses) {
            if ($serviceName == "RetrieveCustomerInfo") {
                $counter = 0;
                foreach (array_reverse($responses, true) as $hash => $data) {
                    $snapShot[$serviceName][] = $hash;
                    $counter++;
                    if ($counter == 2) {
                        break;
                    }
                }
                $snapShot[$serviceName] = array_reverse($snapShot[$serviceName]);
            } else {
                // get last response for each service and set
                end($responses);
                $snapShot[$serviceName] = [key($responses)];
            }
        }

        $this->setServicesSnapshot(json_encode($snapShot));

        return $this;
    }

    /*
     * Return a service response value using xpath
     * @param $serviceName the name of the service from which to get the value
     * @param $serviceXpath the xpath to the value
     * @param $mode special case for retrieve customer info service call which is split in two
     * @return null|float
     */
    public function getServiceValue(string $serviceName, string $serviceXpath, $mode = 1)
    {
        $serviceHashes = json_decode($this->getServicesSnapshot() ?: "{}", true);
        $serviceHash = $serviceHashes[$serviceName] ?? array();
        if ($serviceName == "RetrieveCustomerInfo") {
            $countedMode = 1;
            foreach ($serviceHashes[$serviceName] as $serviceHash) {
                if ($countedMode == $mode) {
                    break;
                }
                $countedMode++;
            }
        } else {
            // get last hash from package snapshot
            $serviceHash = current($serviceHash);
        }

        if ($serviceHash) {
            /** @var Dyna_Customer_Helper_Services $serviceHelper */
            $serviceHelper = Mage::helper('dyna_customer/services');
            return $serviceHelper->getServiceValueByXpath($serviceName, $serviceXpath, $serviceHash) ?: null;
        }

        return null;
    }

    public function hasHardwareProducts()
    {
        $hardwareSubtypes = Dyna_Catalog_Model_Type::$devices;

        $items = $this->getItems();
        foreach ($items as $item) {
            $product = $item->getProduct();
            $subType = current($product->getType());
            if (in_array(strtolower($subType), array_map('strtolower', $hardwareSubtypes))) {
                return true;
            }
        }

        return false;
    }

    public function getPackageStack()
    {
        if (in_array(strtolower($this->getType()), Dyna_Catalog_Model_Type::getMobilePackages())) {
            return Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS;
        }
        elseif (in_array(strtolower($this->getType()), Dyna_Catalog_Model_Type::getCablePackages())) {
            return Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD;
        }
        elseif (in_array(strtolower($this->getType()), Dyna_Catalog_Model_Type::getFixedPackages())) {
            return Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN;
        }
        else {
            return $this->getType();
        }
    }

    /**
     * Checks whether the current package contains an install base subscription
     *
     * @return bool
     */
    public function hasSubscriptionFromContract()
    {
        $quoteItems = $this->getAllItems();

        foreach ($quoteItems as $item) {
            if ($item->isContract() && $item->getProduct()->is(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the package has a dropped tariff that was part
     * of an installed base bundle
     *
     * @return bool
     */
    public function hasDroppedBundleSubscription()
    {
        $quoteItems = $this->getAllItems(true);

        foreach ($quoteItems as $item) {
            if (
                $item->getProduct()->is(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)
                && $item->getAlternateQuoteId()
                && $item->getBundleComponent()
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get items from package except those which are contract_drop (alternate quote items)
     *
     * @return array
     */
    public function getPackageItemsWithoutAlternate()
    {
        $packageItems = array();
        $orders = Mage::getModel('sales/order')->getNonEditedOrderItems($this->getOrderId());

        foreach ($orders as $order) {
            foreach ($order->getAllItems() as $item) {
                if (($item->getPackageId() == $this->getPackageId()) && !($item->getIsContractDrop())) {
                    $packageItems[] = $item;
                }
            }
        }

        return $packageItems;
    }

    /**
     * Generated the Use case Indication for a package
     * and saves it to the use_case_indication field in the catalog_package table
     */
    public function updateUseCaseIndication()
    {
        $packageType = strtolower($this->getType());
        $processContext = $this->getSaleType();
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $account_category = $customer->getAccountCategory();

        $useCase = '';

        if(!$account_category){
            switch ($packageType) {
                case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
                    $useCase = 'NACQMOB';
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
                    $useCase = 'NACQCAB';
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
                    $useCase = 'NACQDSL';
                    break;
            }
        } else {
            switch ($packageType) {
                case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
                    switch ($processContext) {
                        case Dyna_Catalog_Model_ProcessContext::ACQ:
                            switch ($account_category) {
                                case 'KIAS':
                                    $useCase = 'XMOBMOB';
                                    break;
                                case 'FN':
                                    $useCase = 'XMOBDSL';
                                    break;
                                case 'KD':
                                    $useCase = 'XDSLMOBCAB';
                                    break;
                                case 'ALL':
                                    $useCase = 'XMOBMOB';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::ILSBLU:
                            switch ($account_category) {
                                case 'KIAS':
                                    $useCase = 'ILSMOBCHG';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::RETNOBLU:
                        case Dyna_Catalog_Model_ProcessContext::RETBLU:
                        case Dyna_Catalog_Model_ProcessContext::RETTER:
                            switch ($account_category) {
                                case 'KIAS':
                                    $useCase = 'ILSMOBVVL';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::MIGCOC:
                            switch ($account_category) {
                                case 'KIAS':
                                    $useCase = 'ILSMOBD2C';
                                    break;
                            }
                            break;
                    }
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
                    switch ($processContext) {
                        case Dyna_Catalog_Model_ProcessContext::ACQ:
                            switch ($account_category) {
                                case 'KD':
                                    $useCase = 'XCABCAB';
                                    break;
                                case 'KIAS':
                                    $useCase = 'XCABMOB';
                                    break;
                                case 'FN':
                                    $useCase = 'XCABDSL';
                                    break;
                                case 'ALL':
                                    $useCase = 'XCABCAB';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::ILSBLU:
                            switch ($account_category) {
                                case 'KD':
                                    $useCase = 'ILSCAB';
                                    break;
                                case 'FN':
                                    $useCase = 'ILSCAB';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::MIGCOC:
                            switch ($account_category) {
                                case 'FN':
                                    $useCase = 'MIGDSLCAB';
                                    break;
                            }
                            break;
                    }
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
                    switch ($processContext) {
                        case Dyna_Catalog_Model_ProcessContext::ACQ:
                            switch ($account_category) {
                                case 'KD':
                                    $useCase = 'XDSLCAB';
                                    break;
                                case 'KIAS':
                                    $useCase = 'XDSLMOB';
                                    break;
                                case 'FN':
                                    $useCase = 'XDSLDSL';
                                    break;
                                case 'ALL':
                                    $useCase = 'XDSLDSL';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::ILSNOBLU:
                            switch ($account_category) {
                                case 'KD':
                                    $useCase = 'ILSDSL';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::MOVE:
                            switch ($account_category) {
                                case 'KD':
                                    $useCase = 'MOVCABDSL';
                                    break;
                            }
                            break;
                    }
                    break;
            }
        }
        $this->setUseCaseIndication($useCase)->save();
    }

    /**
     * Check if a package has been modified in an ILS flow
     * @return bool
     */
    public function isILSPackageModified()
    {
        /** @var Dyna_Configurator_Helper_Data $helper */
        $helper = Mage::helper('dyna_configurator');
        $packagesAddedInCart = $helper->getPackages();
        $isModified = false;

        foreach ($packagesAddedInCart as $package) {
            if ($package->getSaleType() == Dyna_Catalog_Model_ProcessContext::ILSBLU) {
                // check if package has removed products
                $alternateItems = $package->getAlternateItems();
                if (count($alternateItems) == 0) {

                    // check if new products have been added
                    foreach ($package->getAllItems() as $item) {
                        if (!$item->getIsContract()){
                            $isModified = true;
                        }
                    }

                } else {
                    $isModified = true;
                }
            } else {
                // if saleType is not ILS
                $isModified = true;
            }
        }

        return $isModified;
    }

    /**
     * Determine whether or not ils tariff is kept in cart
     * @return bool
     */
    public function ilsTariffChanged()
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getProduct()->isSubscription() && $item->getIsContract()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether or not ils tariff is kept in cart
     * @return bool
     */
    public function hasTariff()
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getProduct()->isSubscription()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if current package is in pre-post flow
     * @return bool
     */
    public function isPrePost()
    {
        return $this->getType() == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) && !$this->getConvertToMember() && $this->getSaleType() == Dyna_Catalog_Model_ProcessContext::MIGCOC;
    }

    /**
     * Checks
     * @return bool
     */
    public function hasNewAccountID()
    {
        return $this->isPrePost() && $this->getParentAccountNumber() == Dyna_Customer_Helper_Customer::PRE_POST_BAN;
    }

    /**
     * Returns the process context id
     * @return mixed
     */
    public function getProcessContextId()
    {
        return Mage::getModel('dyna_catalog/processContext')->getProcessContextIdByCode($this->getSaleType());
    }

    public function getSubtypeFilter($section)
    {
        static $filters = array();
        $section = strtolower($section);

        if (isset($filters[$section])) {
            return $filters[$section];
        }

        /** @var Dyna_Package_Model_PackageCreationTypes $packageType */
        $packageType = Mage::getModel('dyna_package/packageCreationTypes')->getById($this->getPackageCreationTypeId());

        if ($packageType && $subtypeFilters = $packageType->getPackageSubtypeFilterAttributes()) {
            $subtypeFilters = Mage::helper('dyna_configurator')->getSubtypeFilterCondition($subtypeFilters);
            return $filters[$section] = isset($subtypeFilters[$section]) ? $subtypeFilters[$section] : array();
        }

        return $filters[$section];
    }

    /*
     * Verifies if package contains SimOnly product
     * @return boolean
     */
    public function hasSimOnly()
    {
        $simOnlyCategory = Mage::getModel('catalog/category')->getCategoryByNamePath( 'Mobile/ALL_SIMONLY_TO', '/' );

        foreach ($this->getAllItems() as $item) {
            $catalogProduct = Mage::getModel('catalog/product')->load($item->getProduct()->getId());
            $productCategories = $catalogProduct->getCategoryIds();

            if (in_array($simOnlyCategory->getId(), $productCategories )) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns an array with dummy quote items based on the skus present in installed_base_products field from catalog_package
     * @return array Dyna_Checkout_Model_Sales_Quote_Item
     */
    public function getInstalledBaseItems()
    {
        $dummyItems = [];

        $dummySkus = explode(',', $this->getInstalledBaseProducts());

        if (count($dummySkus)) {
            $dummyProducts = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToFilter('sku', array('in' => $dummySkus));

            $quote = $this->getQuote();
            /** @var Vfde_Catalog_Model_Product $dummyProduct */
            foreach ($dummyProducts as $dummyProduct) {
                $dummyItems[] = Mage::getModel('sales/quote_item')
                    ->setStoreId($quote->getStoreId())
                    ->setQuote($quote)
                    ->setSku($dummyProduct->getSku())
                    ->setProduct($dummyProduct)
                    ->setProductId($dummyProduct->getId());
            }
        }

        return $dummyItems;
    }

    /**
     * Check if the package is in a ILS flow
     *
     * @return bool
     */
    public function isIlsSaleType()
    {
        return in_array($this->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts());
    }
}
