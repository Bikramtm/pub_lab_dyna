<?php

/**
 * Class Dyna_Package_Model_Mysql4_PackageCreationGroups_Collection
 */
class Dyna_Package_Model_Mysql4_PackageCreationGroups_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_package/packageCreationGroups');
    }
}
