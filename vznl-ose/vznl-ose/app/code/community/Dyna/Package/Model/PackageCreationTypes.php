<?php

/**
 * Class Dyna_Package_Model_PackageCreationTypes
 * @method string getPackageTypeId()
 * @method string getPackageTypeCode
 * @method string getPackageSubtypeFilterAttributes()
 */
class Dyna_Package_Model_PackageCreationTypes extends Mage_Core_Model_Abstract
{
    protected static $packageTypeCache = array();
    protected static $packageTypeIds = array();
    protected static $packageCreationGroupsCache = array();

    public function _construct()
    {
        $this->_init('dyna_package/packageCreationTypes');
    }

    public function getPackageCreationTypesByGroup($packageGroupId)
    {
        if (!isset(static::$packageCreationGroupsCache[$packageGroupId])) {
            /** @var Dyna_Package_Model_Mysql4_PackageCreationTypes_Collection $collection */
            $collection = $this->getCollection()
                ->addFieldToFilter('package_creation_group_id', $packageGroupId);
            static::$packageCreationGroupsCache[$packageGroupId] = $collection->getSelect()->order('main_table.sorting', 'ASC')->query()->fetchAll();
        }

        return static::$packageCreationGroupsCache[$packageGroupId];
    }

    public function getById($packageCreationTypeId)
    {
        if (!isset(static::$packageTypeIds[$packageCreationTypeId])) {
            static::$packageTypeIds[$packageCreationTypeId] = $this->getCollection()
                ->addFieldToFilter('entity_id', array('eq' => $packageCreationTypeId))
                ->getFirstItem();
        }

        return static::$packageTypeIds[$packageCreationTypeId];
    }

    /**
     * @param $packageCode
     * @return Dyna_Package_Model_PackageCreationTypes
     */
    public function getTypeByPackageCode($packageCode) : Dyna_Package_Model_PackageCreationTypes
    {
        if (isset(static::$packageTypeCache[$packageCode])) {
            return static::$packageTypeCache[$packageCode];
        }

        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        $cacheKey = "PACKAGE_TYPE_BY_CODE_" . $packageCode;
        if (($result = $cache->load($cacheKey)) && $result = unserialize($result)) {
            static::$packageTypeCache[$packageCode] = $result;
        } else {
            static::$packageTypeCache[$packageCode] = $this->getCollection()
                ->addFieldToFilter('package_type_code', array('eq' => $packageCode))
                ->getFirstItem();
            $cache->save(serialize(static::$packageTypeCache[$packageCode]), $cacheKey, array($cache::PRODUCT_TAG));
        }

        return static::$packageTypeCache[$packageCode];
    }

    /**
     * @return Dyna_Package_Model_PackageType|Mage_Core_Model_Abstract
     */
    public function getReferencedPackageType() : Dyna_Package_Model_PackageType
    {
        return Mage::getModel('dyna_package/packageType')->load($this->getPackageTypeId());
    }

    /**
     * @return array
     */
    public function toOptionArray() {
        $options = array();

        /** @var self $packageCreationType */
        foreach ($this->getCollection() as $packageCreationType) {
            $options[] = array(
                'label' => $packageCreationType->getPackageTypeCode(),
                'value' => $packageCreationType->getId()
            );
        }

        return $options;
    }

    public function getNameByPackageTypeCode($packageTypeCode){
            $frontName = $this->getCollection()
                ->addFieldToFilter('package_type_code', array('eq' => $packageTypeCode))
                ->getFirstItem();

        return str_replace('_',' ',$frontName->getPackageTypeCode());
    }

    /**
     * Return filter attributes by process context code
     * @param string $processContextCode
     * @return null|string
     */
    public function getFilterAttributes($processContextCode = null)
    {
        $result = null;

        if ($processContextCode) {
            /** @var Dyna_Catalog_Model_ProcessContext $processContextModel */
            $processContextModel = Mage::getModel('dyna_catalog/processContext');
            $processContextId = $processContextModel->getProcessContextIdByCode($processContextCode);
            if ($processContextId) {
                /** @var Dyna_Package_Model_PackageCreationTypesProcessContext $packageCreationTypesProcessContext */
                $packageCreationTypesProcessContextModel = Mage::getModel('dyna_package/packageCreationTypesProcessContext');
                $packageCreationTypesProcessContext = $packageCreationTypesProcessContextModel->getByPackageCreationIdAndProcessContextId($this->getId(),
                    $processContextId);

                if ($packageCreationTypesProcessContext) {
                    $result = $packageCreationTypesProcessContext->getFilterAttributes();
                }
            }
        }

        return !empty($result) ? $result : $this->getDefaultFilterAttributes();
    }

    /**
     * Return default filter attributes for package creation type
     * @return mixed
     */
    public function getDefaultFilterAttributes()
    {
        return $this->getData('filter_attributes');
    }
}
