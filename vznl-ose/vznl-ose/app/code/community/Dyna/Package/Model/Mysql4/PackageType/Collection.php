<?php

/**
 * Class Dyna_Package_Model_Mysql4_Configuration_Collection
 */
class Dyna_Package_Model_Mysql4_PackageType_Collection extends Omnius_Package_Model_Mysql4_PackageType_Collection
{
    /**
     * Method that returns either this collection filtered by visible, either an array of codes => package names
     * @param bool $toArray
     * @return $this|array
     */
    public function getPackageTypesToArray($toArray = false)
    {
        if (!$toArray) {
            return $this;
        } else {
            $packageTypes = array();
            foreach ($this as $packageType) {
                $packageTypes[$packageType->getPackageCode(true)] = $packageType->getFrontEndName();
            }
            return $packageTypes;
        }
    }
}
