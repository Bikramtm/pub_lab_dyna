<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Package_Model_Configuration extends Omnius_Package_Model_Configuration
{
    /**
     * Check whether or not a package can be updated to completed state after validating against it's configuration
     */
    public function isPackageCompleted()
    {
        // Check if package validation is enabled in config
        if (!Mage::getStoreConfig('omnius_general/package_validation/enabled')) {
            return true;
        }

        // Get package subtype collection
        $packageSubtypes = $this->getConfiguration();

        if (!count($packageSubtypes)) {
            Mage::log("Cannot find any package subtypes for package of type: " . $this->package->getType(), Zend_Log::INFO, "packages-validation.log");
            return false;
        }

        // Check if packages has all the package subtypes valid
        /** @var Omnius_Package_Model_PackageSubtype $packageSubtype */
        foreach ($packageSubtypes as $packageSubtype) {
            // Define package_subtype product attribute
            $subtypeIdentifier = $packageSubtype->getPackageCode();

            // Load all package items
            $packageItems = $this->package->getItems(strtolower($subtypeIdentifier));

            // Set count limit to which a product of certain type can reach
            $packageSubtypeCardinality = $packageSubtype->getCardinality();

            /** @var Dyna_Catalog_Helper_ProcessContext $processContext */
            $processContext = Mage::helper('dyna_catalog/processContext');
            $processContextId = $processContext->getProcessContextId();

            if ($processContextId > 0) {
                $subtypeCardinality = Mage::getModel('dyna_package/packageSubtypeCardinality')
                    ->loadBySubtypeAndContext($packageSubtype->getId(), $processContextId)
                    ->getData();

                if (isset($subtypeCardinality['cardinality']) && $subtypeCardinality['cardinality'] != '') {
                    $packageSubtypeCardinality = $subtypeCardinality['cardinality'];
                }
            }

            $minItems = $this->getMinProducts($packageSubtypeCardinality);
            $maxItems = $this->getMaxProducts($packageSubtypeCardinality);

            if (count($packageItems) < $minItems) {
                if (Mage::getStoreConfig('omnius_general/package_validation/debugging')) {
                    Mage::log("Package " . $this->package->getId() . " does not have the minimum: " . $subtypeIdentifier, Zend_Log::INFO, "packages-validation.log");
                }
                // Package doesn't meet the minimum items requirement
                return false;
            }

            // If max is not infinite
            if ($maxItems && $maxItems != -1 && count($packageItems) > $maxItems) {
                if (Mage::getStoreConfig('omnius_general/package_validation/debugging')) {
                    Mage::log("Package " . $this->package->getId() . " exceeds the maximum: " . $subtypeIdentifier, Zend_Log::INFO, "packages-validation.log");
                }
                // To much items, package cannot be completed
                return false;
            }
        }

        return true;
    }
}
