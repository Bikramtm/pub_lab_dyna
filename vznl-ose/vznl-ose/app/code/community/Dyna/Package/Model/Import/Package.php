<?php

/**
 * Class Dyna_Package_Model_Import_Package
 */
class Dyna_Package_Model_Import_Package extends Omnius_Package_Model_Import_Package
{
    public $_header = [];

    /** @var Dyna_Import_Helper_Data $_helper */
    public $_helper;

    protected $_debug = false;

    /**
     * Dyna_Package_Model_Import_Package constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper->setImportLogFile($this->_logFileName . '.log');
    }

    /**
     * Import cable product
     *
     * @param array $data
     * @return void
     */
    public function importPackage($data)
    {
        $data = array_map("trim", $data);
        try {
            $data = array_combine($this->_header, $data);
            $data['stack'] = $this->stack;

            // Let's make use of a Varien_Object
            /**
             * @method getPackageSubtypeName()
             */
            $importData = new Varien_Object();
            $importData->setData($data);
            
            $packageTypeCode = $importData->getPackageTypeCode();
            if (empty($this->packageTypes[$packageTypeCode])) {
                // Let's try loading it
                /** @var Dyna_Package_Model_PackageType $packageTypeModel */
                $packageTypeModel = Mage::getModel('dyna_package/packageType')
                    ->getCollection()
                    ->addFieldToFilter('package_code', ['eq' => $packageTypeCode])
                    ->getFirstItem();
                if (!$packageTypeModel || !$packageTypeModel->getId()) {
                    $packageTypeModel = Mage::getModel('dyna_package/packageType');
                }
                // If exists, update its data
                $packageTypeModel
                    ->setFrontEndName($importData->getPackageTypeName())
                    ->setPackageCode($packageTypeCode)
                    ->setLifecycleStatus(in_array($importData->getLifecycleStatus(), Dyna_Catalog_Model_Lifecycle::getLifecycleTypes()) ? $importData->getLifecycleStatus() : null )
                    ->setStack($importData->getStack())
                    ->save();

                $this->packageTypes[$packageTypeCode] = $packageTypeModel;
            } else {
                $packageTypeModel = $this->packageTypes[$packageTypeCode];
            }

            // Try loading package subtype by code
            /** @var Dyna_Package_Model_PackageSubtype $packageSubTypeModel */
            $packageSubTypeModel = Mage::getModel('dyna_package/packageSubtype')
                ->getCollection()
                ->addFieldToFilter('package_code', ['eq' => $importData->getPackageSubtypeCode()])
                ->addFieldToFilter('type_package_id', ['eq' => $packageTypeModel->getId()])
                ->getFirstItem();
            if (!$packageTypeModel || !$packageTypeModel->getId()) {
                $packageSubTypeModel = Mage::getModel('package/configuration');
            }

            // Set data
            $packageSubTypeModel
                ->setTypePackageId($packageTypeModel->getId())
                ->setPackageCode($importData->getPackageSubtypeCode())
                ->setFrontEndName($importData->getPackageSubtypeName())
                ->setCardinality($importData->getCardinality())
                ->setIgnoredByCompatibilityRules(strtolower($importData->getIgnoredByCompatibilityRules()) == 'yes' ? 1 : 0);

            $cardinality = mb_convert_encoding($packageSubTypeModel->getCardinality(), "UTF-8");
            // Dot sign in example files has an invalid character converted to question sign
            // Doesn't matter how many points are in cardinality, they will be trimmed
            $cardinality = str_replace("?", "...", $cardinality);
            $packageSubTypeModel->setCardinality($cardinality);

            // Set visibility
            $packageSubTypeModel->setPackageSubtypeVisibility($this->parseProductVisibility($importData->getPackageSubtypeVisibility()));
            // Set position
            $packageSubTypeModel->setFrontEndPosition(
            // If no position defined, set the max (least) one
                (int)$importData->getPackageSubtypePosition() ?: 10
            );

            $packageSubTypeModel->setManualOverrideAllowed($importData->getManualOverrideAllowed() != null && trim($importData->getManualOverrideAllowed()) == 'yes' ? 1 : 0);
            $packageSubTypeModel->setPackageSubtypeLifecycleStatus(in_array($importData->getPackageSubtypeLifecycleStatus(), Dyna_Catalog_Model_Lifecycle::getLifecycleTypes()) ? $importData->getPackageSubtypeLifecycleStatus() : null );
            $packageSubTypeModel->setPackageSubtypePosition($packageSubTypeModel->getPackageSubtypeGuiSorting());
            $packageSubTypeModel->setStack($importData->getStack());
            $packageSubTypeModel->save();

            // save the cardinality of the subtype per process context
            $subtypeId = $packageSubTypeModel->getId();

            $cardinalityContexts = $this->parseContextCardinality($importData);

            foreach ($cardinalityContexts as $context) {
                $subtypeContextCardinality = Mage::getModel('dyna_package/packageSubtypeCardinality');
                $subtypeContextCardinality
                    ->setData('package_subtype_id', $subtypeId)
                    ->setData('process_context_id', $context['context_id'])
                    ->setData('cardinality', $context['cardinality']);

                $subtypeContextCardinality->save();
            }


            $this->_totalFileRows++;
            $this->_log('Finished importing row: ' . $packageTypeModel->getFrontEndName() . " >> " . $packageSubTypeModel->getFrontEndName(), true);
        } catch (Exception $ex) {
            $this->_skippedFileRows++;
            $this->_logEx($ex);
            fwrite(STDERR, 'Skipped file because of '.$ex->getMessage());
        }
    }

    /**
     * Set header on current import instance
     * @param $header array
     * @return $this
     */
    public function setHeader($header)
    {
        $builtHeader = [];
        //get header mapping
        $mapping = $this->getImportHelper()->getMapping('importPackage');

        foreach ($header as $columnHead) {
            $builtHeader[] = array_key_exists($columnHead, $mapping) ? $mapping[$columnHead] : $columnHead;
        }

        $this->_header = $builtHeader;

        return $this;
    }

    /**
     * @return Mage_Core_Helper_Abstract|null
     */
    public function getImportHelper()
    {
        /** @var Dyna_Import_Helper_Data _helper */
        return $this->_helper == null ? Mage::helper("dyna_import/data") : $this->_helper;
    }

    /**
     * Get the mapped header data
     * @return array
     */
    public function getHeader()
    {
        return $this->_header;
    }

    /**
     * Method to parse the value of the package subtype visibility
     * @param string $values
     * @return string
     */
    private function parseProductVisibility($values)
    {
        $markedOptions = [];
        $markedOptionIds = [];
        $allVisibilityOptionsArray = array();

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY,
                Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);

        if ($attribute->usesSource()) {
            $allVisibilityOptionsArray = array_map(function ($value) {
                return strtolower(str_replace(' ', '_', trim($value)));
            }, array_column($attribute->getSource()->getAllOptions(false), 'label'));
        }

        if ('*' == $values) {
            $markedOptionIds = $allVisibilityOptionsArray;
        } else {
            if (trim($values)) {
                $values = explode(',', $values);
                foreach ($values as $value) {
                    $markedOptions[] = strtolower(str_replace(' ', '_', trim($value)));
                }
                foreach ($markedOptions as $option) {
                    if (in_array($option, $allVisibilityOptionsArray)) {
                        $markedOptionIds[] = $option;
                    } else {
                        $this->_logError('[Warning] package_subtype_visibility value "' . $option . '" cannot be mapped for row no: ' . $this->_totalFileRows);
                        continue;
                    }
                }
            }
        }

        return (!$markedOptionIds) ? null : implode(',', $markedOptionIds);
    }

    /**
     * Method to parse the package subtype cardinality for
     * each existing process context
     *
     * @param Varien_Object $values
     * @return array
     */
    private function parseContextCardinality($values)
    {
        /** @var Dyna_Catalog_Model_ProcessContext $contextModel */
        $contextModel = Mage::getModel('dyna_catalog/processContext');
        $subtypeCardinality = array();

        $allProcessContexts = $contextModel->getAllProcessContextCodeIdPairs();
        foreach ($allProcessContexts as $context => $id) {
            $subtypeCardinality[] = [
                'context_id' => $id,
                'cardinality' => $values->getData(strtolower($context) . '_cardinality') ?? null
            ];
        }

        return $subtypeCardinality;
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }
    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }

}
