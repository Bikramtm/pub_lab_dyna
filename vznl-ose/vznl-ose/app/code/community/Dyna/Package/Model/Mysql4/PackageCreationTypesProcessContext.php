<?php

/**
 * Class Dyna_Package_Model_Mysql4_PackageCreationTypesProcessContext
 */
class Dyna_Package_Model_Mysql4_PackageCreationTypesProcessContext extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_package/packageCreationTypesProcessContext', 'entity_id');
    }
}