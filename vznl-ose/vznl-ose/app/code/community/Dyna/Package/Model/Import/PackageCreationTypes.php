<?php

/**
 * Class Dyna_Package_Model_Import_PackageCreationTypes
 */
class Dyna_Package_Model_Import_PackageCreationTypes extends Omnius_Import_Model_Import_ImportAbstract {

    /**
     * @var string
     */
    protected $_csvDelimiter = ';';

    /**
     * @var array
     */
    public $_header = [];

    /**
     * @var string
     */
    public $_logFileName = "package_creation_types_import";

    /**
     * Dyna_Package_Model_Import_PackageCreationTypes constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper->setImportLogFile($this->_logFileName . '.log');
    }

    /**
     * Set header on current import instance
     * @param $header array
     * @return $this
     */
    public function setHeader($header)
    {
        $builtHeader = [];
        //get header mapping
        $mapping = $this->_helper->getMapping('packageCreationTypes');

        foreach ($header as $columnHead) {
            $builtHeader[] = array_key_exists($columnHead, $mapping) ? $mapping[$columnHead] : $columnHead;
        }

        $this->_header = $builtHeader;
        return $this;
    }

    /**
     * Import the creation type and creation group
     *
     * @param $data
     */
    public function importCreationType($data)
    {
        $data = array_combine($this->_header, $data);
        $data['stack'] = $this->stack;
        $this->_totalFileRows++;

        if (!$this->validateRequiredData($data)) {
            $this->_skippedFileRows++;
            return;
        }

        $packageTypeId = Mage::getModel('dyna_package/packageType')
            ->getCollection()
            ->addFieldToFilter('package_code', $data['package_type_id'])
            ->getFirstItem()
            ->getId();
        if ($packageTypeId) {
            $creationGroupId = $this->getCreationGroupId($data);

            // Build the creation type
            $packageCreationType = Mage::getModel('dyna_package/packageCreationTypes');

            $packageCreationType
                ->setData('name', $data['package_creation_name'])
                ->setData('gui_name', $data['package_creation_name'])
                ->setData('sorting', $data['sorting'])
                ->setData('filter_attributes', $data['filter_attributes'])
                ->setData('package_subtype_filter_attributes', $data['package_subtype_filter_attributes'])
                ->setData('package_type_code', $data['package_creation_type_id'])
                ->setData('package_type_id', $packageTypeId)
                ->setData('package_creation_group_id', $creationGroupId)
                ->setData('stack', $data['stack'])
                ->setData('requires_serviceability', $data['requires_serviceability']);

            /** @var Dyna_Catalog_Model_ProcessContext $processContext */
            $processContextModel = Mage::getModel('dyna_catalog/processContext');
            try {
                // save package creation
                $packageCreationType->save();

                foreach ($processContextModel->getCollection() as $processContext) {
                    $key = 'filter_attributes_' . strtolower($processContext->getCode());

                    if (array_key_exists($key, $data) && !empty($data[$key])) {
                        $packageCreationTypeProcessContext = Mage::getModel('dyna_package/packageCreationTypesProcessContext');
                        $packageCreationTypeProcessContext
                            ->setData('package_creation_type_id', $packageCreationType->getId())
                            ->setData('process_context_id', $processContext->getId())
                            ->setData('filter_attributes', $data[$key]);

                        // save package creation process context
                        $packageCreationTypeProcessContext->save();
                    }
                }
            } catch (Exception $e) {
                $this->_skippedFileRows++;
                echo $e->getMessage();
                $this->_logError($e);
            }
        } else {
            $this->_logError(sprintf("The referenced package type %s was not found in catalog_package_types", $data['package_type_id']));
            $this->_skippedFileRows++;
        }

    }

    /**
     * Checks whether a package creation group already exists for the specified name.
     * If the does not exist the method will create a new one and return its id
     *
     * @param $data
     * @return mixed
     */
    protected function getCreationGroupId($data)
    {
        // first check if the group exists for the specified type
        /** @var Dyna_Package_Model_PackageCreationGroups $existingPackageCreationGroup */
        $existingPackageCreationGroup = Mage::getModel('dyna_package/packageCreationGroups')
            ->getCollection()
            ->addFieldToFilter('gui_name', $data['package_creation_grouping'])
            ->getFirstItem();

        if (!$existingPackageCreationGroup->getId()) {
            // group does not exist, create a new one
            $packageCreationGroup = Mage::getModel('dyna_package/packageCreationGroups');

            $packageCreationGroup
                ->setData('gui_name', $data['package_creation_grouping'])
                ->setData('stack', $data['stack'])
                ->setData('sorting', $data['package_creation_grouping_sorting']);

            // save the group and retrieve its id
            $packageCreationGroup->save();

            $creationGroupId = $packageCreationGroup->getId();
        } else {
            $stacks = explode(',', $existingPackageCreationGroup->getStack());
            $stacks = array_combine($stacks, $stacks);
            $stacks[$data['stack']] = $data['stack'];
            if (count($stacks) > 1) {
                $existingPackageCreationGroup->setStack(implode(',', $stacks));
                $existingPackageCreationGroup->save();
            }

            $creationGroupId = $existingPackageCreationGroup->getId();
        }

        return $creationGroupId;
    }

    /**
     * Checks whether the current imported row contains all required data
     *
     * @param $data
     * @return bool
     */
    protected function validateRequiredData($data)
    {
        $requiredFields = [
            'package_creation_type_id',
            'package_type_id',
            'package_creation_name',
        ];

        foreach ($data as $column => $value) {
            if (in_array($column, $requiredFields) && empty($data[$column])) {
                $this->_logError(sprintf("Skipping row due to missing data in column %s", $column));

                return false;
            }
        }

        return true;
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    /**
     * @param $msg
     */
    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }
}
