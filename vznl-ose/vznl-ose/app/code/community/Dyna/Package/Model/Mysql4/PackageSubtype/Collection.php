<?php

/**
 * Class Dyna_Package_Model_Mysql4_PackageSubtype_Collection
 */
class Dyna_Package_Model_Mysql4_PackageSubtype_Collection extends Omnius_Package_Model_Mysql4_PackageSubtype_Collection
{
    /**
     * Join packageSubtype collection with packageTypes in order to have the the associated package code for every packageSubtype row
     * @return $this
     */
    public function joinCollectionPackageTypes()
    {
        $this->join(
            array('package_type' => 'package/type'),
            'main_table.type_package_id=package_type.entity_id',
            array('package_code AS package_type_code')
        );
        return $this;
    }
}
