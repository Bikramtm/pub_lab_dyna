<?php

/**
 * Class Dyna_Package_Model_PackageSubtypeCardinality
 */
class Dyna_Package_Model_PackageSubtypeCardinality extends Mage_Core_Model_Abstract
{
    CONST PACKAGE_SUBTYPE_CARDINALITY_ONE = '1...1';

    public function _construct()
    {
        $this->_init('dyna_package/packageSubtypeCardinality');
    }

    public function loadBySubtypeAndContext($packageSubtypeId, $processContextId)
    {
        $result = $this->getCollection()
            ->addFieldToFilter('package_subtype_id', $packageSubtypeId)
            ->addFieldToFilter('process_context_id', $processContextId)
            ->getFirstItem();

        return $result;
    }

}
