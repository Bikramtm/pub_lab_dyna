<?php

class Dyna_Import_Helper_ImportSupportedFiles
{
    protected $supportedFiles = [
        'FN_PackageTypes.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'packageTypes',
        ],
        'FN_PackageCreation.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'packageCreationTypes',
        ],
        'FN_Hardware.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedProduct',
        ],
        'FN_SalesPackage.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedProduct',
        ],
        'FN_Options.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedProduct',
        ],
        'FN_Promotions.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedProduct',
        ],
        'FN_Accessory.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedProduct',
        ],
        'FN_CategoryTree.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'categoriesTree',
        ],
        'FN_Categories.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'categoriesTreeProducts',
        ],
//       'FN_Comp_Rules.csv'
        'FN_Comp_Rules.xml' => [
            'stack' => 'Fixed',
            'import_type' => 'productMatchRules',
        ],
        'FN_SalesRules.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedSalesRules',
        ],
//       'FN_Multimapper_Hardware.csv'
//       'FN_Multimapper_SalesPackage.csv'
//       'FN_Multimapper_Options.csv'
//       'FN_Multimapper_Promotion.csv'
//       'FN_Multimapper_Accessory.csv'
        'FN_Mapper.xml' => [
            'stack' => 'Fixed',
            'import_type' => 'multimapper',
        ],
        'FN_ProductFamily.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'productFamily',
        ],
        'FN_ProductVersion.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'productVersion',
        ],
        'FN_Version.xml' => [
            'stack' => 'Fixed',
            'import_type' => 'logfile',
            'optional' => true,
        ],
        'Mobile_Package_Types.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'packageTypes',
        ],
        'Mobile_PackageCreation.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'packageCreationTypes',
        ],
        'Mobile_Tariff.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileProduct',
        ],
        'Mobile_Hardware.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileProduct',
        ],
        'Mobile_Footnote.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileProduct',
        ],
        'Mobile_Service.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileProduct',
        ],
        'Mobile_Categories.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'categories',
        ],
        'Mobile_Device_Categories.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'categories',
        ],
        'Mobile_Comp_Rules.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'productMatchRules',
        ],
        'Mobile_Comp_Rules_Article_Constraints.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'productMatchRules',
        ],
        'Mobile_Comp_Rules_Footnote.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'productMatchRules',
        ],
        'Mobile_Mixmatch_Complete.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mixMatch',
        ],
        'Mobile_Sales_Rules_PostPaid.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileSalesRules',
        ],
        'Mobile_Sales_Rules_Campaigns.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileSalesRules',
        ],
        'Mobile_Multimapper_Kias.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'multimapper',
        ],
        'Mobile_Multimapper_SAP.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'multimapper',
        ],
        'Mobile_ProductFamily.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'productFamily',
        ],
        'Mobile_ProductVersion.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'productVersion',
        ],
        'Mobile_Version.xml' => [
            'stack' => 'Mobile',
            'import_type' => 'logfile',
            'optional' => true,
        ],
        'Cable_Package_Types.csv' => [
            'stack' => 'Cable',
            'import_type' => 'packageTypes',
        ],
        'Cable_PackageCreation.csv' => [
            'stack' => 'Cable',
            'import_type' => 'packageCreationTypes',
        ],
        'Cable_Products.json' => [
            'stack' => 'Cable',
            'import_type' => 'cableProduct',
        ],
        'Cable_Categories.csv' => [
            'stack' => 'Cable',
            'import_type' => 'categories',
        ],
        'Cable_Compatibility.csv' => [
            'stack' => 'Cable',
            'import_type' => 'productMatchRules',
        ],
        'Cable_Sales_Rules.csv' => [
            'stack' => 'Cable',
            'import_type' => 'salesRules',
        ],
        'Cable_Multimapper.csv' => [
            'stack' => 'Cable',
            'import_type' => 'multimapper',
        ],
        'Cable_Version.xml' => [
            'stack' => 'Cable',
            'import_type' => 'logfile',
            'optional' => true,
        ],
        'Cable_Rules_Plugin.phar' => [
            'stack' => 'Cable_artifact',
            'import_type' => 'cableArtifact',
        ],
        'Campaign_Offers.xml' => [
            'stack' => 'Campaign',
            'import_type' => 'campaignOffer',
        ],
        'Campaigns.xml' => [
            'stack' => 'Campaign',
            'import_type' => 'campaign',
        ],
        'Campaign_Version.xml' => [
            'stack' => 'Campaign',
            'import_type' => 'logfile',
            'optional' => true,
        ],
        'Bundles.xml' => [
            'stack' => 'Bundle',
            'import_type' => 'bundles',
        ],
        'Bundle_Categories.csv' => [
            'stack' => 'Bundle',
            'import_type' => 'categories',
        ],
        'Bundle_Version.xml' => [
            'stack' => 'Bundle',
            'import_type' => 'logfile',
            'optional' => true,
        ],
        'Reference_OSF_ServiceProvider.csv' => [
            'stack' => 'Reference',
            'import_type' => 'serviceProviders',
        ],
        'Reference_Ship2Store_List.csv' => [
            'stack' => 'Reference',
            'import_type' => 'vodafoneShip2Stores',
        ],
        'Reference_activityCodes.xml' => [
            'stack' => 'Reference',
            'import_type' => 'activityCodes',
        ],
        'Reference_typeSubtypeCombinations.xml' => [
            'stack' => 'Reference',
            'import_type' => 'typeSubtypeCombinations',
        ],
        'Reference_Stichwort_list.csv' => [
            'stack' => 'Reference',
            'import_type' => 'phoneBookKeywords',
        ],
        'Reference_telefonbuch-branchenliste.csv' => [
            'stack' => 'Reference',
            'import_type' => 'phoneBookIndustries',
        ],
        'Reference_option_type.csv' => [
            'stack' => 'Reference',
            'import_type' => 'optionTypes',
        ],
        'Reference_CommunicationVariables.csv' => [
            'stack' => 'Reference',
            'import_type' => 'communicationVariables',
            'optional' => true,
        ],
//        'Reference_DealerInfos.xml' => [
//            'stack' => 'Reference',
//            'import_type' => 'dealerInfos',
//            'optional' => true,
//        ],
        'SalesId_Dealercode_Mapping.xml' => [
            'stack' => 'Salesid',
            'import_type' => 'salesIdDealercode',
        ],
        'SalesId_Version.xml' => [
            'stack' => 'Salesid',
            'import_type' => 'logfile',
            'optional' => true,
        ],
        'provis_to_omnius_<yyyymmdd>_<hh24miss>.csv' => [
            'stack' => 'Provis',
            'import_type' => 'provisSalesId',
            'optional' => true,
        ],
    ];

    protected $patternFileNames = [
        'provis_to_omnius_<yyyymmdd>_<hh24miss>.csv' => '/^provis_to_omnius_\d\d\d\d(0[0-9]|1[0-2])(0[0-9]|1[0-9]|2[0-9]|3[0-1])_(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])([0-5][0-9])\.csv$/'
    ];

    public function isValid($file, $returnFileName = false)
    {
        $isValid = array_key_exists($file, $this->supportedFiles);
        if (!$isValid) {
            $isValid = $this->validatePatternFileName($file) !== false;
            $file = $this->validatePatternFileName($file);
        }

        return $returnFileName ? $file : $isValid;
    }

    /**
     * Validates provis file name with the following format: provis_to_omnius_<yyyymmdd>_<hh24miss>.csv
     *
     * @param $file
     * @return bool
     */
    public function validatePatternFileName($file)
    {
        foreach ($this->patternFileNames as $fileNamePattern => $regex) {
            if (preg_match($regex, $file) === 1) {
                return $fileNamePattern;
            }
        }

        return false;
    }

    public function getSupportedFiles(): array
    {
        return $this->supportedFiles;
    }

    public function getSupportedFilesFormatted(): string
    {
        return PHP_EOL.implode(PHP_EOL, array_keys($this->supportedFiles)).PHP_EOL;
    }

}
