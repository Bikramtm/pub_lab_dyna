<?php


/**
 * Class Dyna_Import_Helper_Data
 */
class Dyna_Import_Helper_Data extends Omnius_Import_Helper_Data
{
    /** @var bool $_debug */
    protected $pathToImportFile;
    protected $_debug = false;
    protected $_currentProductSku;
    // ToDo: Set import dir when directory structure for PLM is established
    const IMPORT_LOG_DIR = '';
    const LOG_FILE_EXECUTION_SUFFIX = 'execution';

    // Import file names
    const CABLE_PRODUCT_IMPORT_LOG_FILE_JSON = 'cable_product_import_json';
    const CABLE_PRODUCT_IMPORT_LOG_FILE = 'cable_product_import';
    const CABLE_ARTIFACT_IMPORT_LOG_FILE = 'cable_artifact_import';
    const MOBILE_PRODUCT_IMPORT_LOG_FILE = 'mobile_product_import';
    const FIXED_PRODUCT_IMPORT_LOG_FILE = 'fixed_product_import';
    const MIXMATCH_IMPORT_LOG_FILE = 'mixmatch_import';
    const MULTIMAPPER_IMPORT_LOG_FILE = 'multimapper_import';
    const CATEGORIES_IMPORT_LOG_FILE = 'categories_import';
    const DEALER_COMPAIGNS_IMPORT_LOG_FILE = 'dealer_campaigns_import';
    const SALES_RULES_IMPORT_LOG_FILE = 'sales_rules_import';
    const MATCHRULES_IMPORT_LOG_FILE = 'matchrules_import';
    const PACKAGE_TYPE_IMPORT_LOG_FILE = 'package_types_import';
    const BUNDLE_CAMPAIGN_IMPORT_LOG_FILE = 'bundle_campaign_import';
    const SALESID_DEALERCODE_IMPORT_LOG_FILE = 'salesid_dealercode_import';
    const BUNDLE_COMPAIGN_OFFER_IMPORT_LOG_FILE = 'bundle_campaignoffer_import';
    const BUNDLE_RULE_IMPORT_LOG_FILE = 'bundle_bundle_rule_import';
    const SHIP_TO_STORES_IMPORT_LOG_FILE = 'vodafone_ship2stores_import';
    const SERVICE_PROVIDERS_IMPORT_LOG_FILE = 'service_providers_import';
    const PROVIS_SALES_ID_LOG_FILE = 'provis_sales_ids_import';
    const CATALOG_LOG_FILE_LOG_FILE = 'import_version';
    const PRODUCT_FAMILY_LOG_FILE = 'product_family';
    const PRODUCT_VERSION_LOG_FILE = 'product_version';
    const PACKAGE_CREATION_TYPES_LOG_FILE = 'package_creation_types_import';
    const CATEGORIES_TREE_LOG_FILE = 'categories_tree';
    const CATEGORIES_TREE_PRODUCTS_LOG_FILE = 'categories_tree';
    const XML_FUTURE_TRANSACTIONS_IMPORT_LOG_FILE = 'futureTransactions';
    const XML_PACKAGE_TYPE_IMPORT_LOG_FILE = 'import_package_types';
    const XML_CATEGORIES_IMPORT_LOG_FILE = 'import_xml';
    const XML_MULTIMAPPER_IMPORT_LOG_FILE = 'import_multimapper_rules';
    const XML_PRODUCT_MATCHRULES_IMPORT_LOG_FILE = 'import_match_rules';
    const XML_PRODUCTS_IMPORT_LOG_FILE = 'import_xml';
    const XML_ACTIVITY_CODES_LOG_FILE = 'import_activity_codes';
    const XML_TYPE_SUBTYPE_COMBINATIONS_LOG_FILE = 'import_type_subtype_combinations';
    const OPTION_TYPES_LOG_FILE = 'option_types_import';
    const COMMUNICATION_VARIABLE_LOG_FILE = 'communication_variable';
    const PHONE_BOOK_KEYWORDS_LOG_FILE = 'phone_book_keywords_import';
    const PHONE_BOOK_INDUSTRIES_LOG_FILE = 'phone_book_industries_import';

    // Import categories
    const CABLE_PRODUCT_IMPORT_CATEGORY = 'cableProduct';
    const CABLE_ARTIFACT_IMPORT_CATEGORY = 'cableArtifact';
    const MOBILE_PRODUCT_IMPORT_CATEGORY = 'mobileProduct';
    const FIXED_PRODUCT_IMPORT_CATEGORY = 'fixedProduct';
    const MIXMATCH_IMPORT_CATEGORY = 'mixmatch';
    const MULTIMAPPER_IMPORT_CATEGORY = 'multimapper';
    const CATEGORIES_IMPORT_CATEGORY = 'categories';
    const PRODUCT_MATCH_RULES_IMPORT_CATEGORY = 'productMatchRules';
    const SALES_RULES_IMPORT_CATEGORY = 'salesRules';
    const MOBILE_SALES_RULES_IMPORT_CATEGORY = 'mobileSalesRules';
    const FIXED_SALES_RULES_IMPORT_CATEGORY = 'fixedSalesRules';
    const PACKAGE_TYPES_IMPORT_CATEGORY = 'packageTypes';
    const PACKAGE_CREATION_TYPES_IMPORT_CATEGORY = 'packageCreationTypes';
    const CAMPAIGN_IMPORT_CATEGORY = 'campaign';
    const CAMPAIGN_OFFER_IMPORT_CATEGORY = 'campaignOffer';
    const SALESID_DEALERCODE_IMPORT_CATEGORY = 'salesIdDealercode';
    const DEALER_CAMPAIGNS_IMPORT_CATEGORY = 'dealerCampaigns';
    const BUNDLES_IMPORT_CATEGORY = 'bundles';
    const SHIP_TO_STORES_IMPORT_CATEGORY = 'vodafoneShip2Stores';
    const SERVICE_PROVIDERS_IMPORT_CATEGORY = 'serviceProviders';
    const AGENT_COMMISIONS_IMPORT_CATEGORY = 'agentCommisions';
    const CHECKOUT_PRODUCTS_IMPORT_CATEGORY = 'checkoutProducts';
    const PROVIS_SALES_ID_IMPORT_CATEGORY = 'provisSalesId';

    const OPTION_TYPES_CATEGORY = 'optionTypes';
    const COMMUNICATION_VARIABLE_CATEGORY = 'communicationVariables';
    const CATALOG_LOG_FILE_CATEGORY = 'logfile';
    const PRODUCT_FAMILY_CATEGORY = 'productFamily';
    const PRODUCT_VERSION_CATEGORY = 'productVersion';
    const CATEGORIES_TREE_CATEGORY = 'categoriesTree';
    const CATEGORIES_TREE_PRODUCTS_CATEGORY = 'categoriesTreeProducts';
    const XML_FUTURE_TRANSACTIONS_IMPORT_CATEGORY = 'futureTransactions';
    const XML_PACKAGE_TYPES_IMPORT_CATEGORY = 'packageTypes';
    const XML_CATEGORIES_IMPORT_CATEGORY = 'categories';
    const XML_MULTIMAPPER_IMPORT_CATEGORY = 'multimapper';
    const XML_PRODUCT_MATCH_RULES_IMPORT_CATEGORY = 'productMatchRules';
    const XML_PRODUCTS_IMPORT_CATEGORY = 'products';
    const XML_TYPE_SUBTYPE_COMBINATIONS_CATEGORY = 'typeSubtypeCombinations';
    const XML_ACTIVITY_CODES_CATEGORY = 'activityCodes';
    const PHONE_BOOK_KEYWORDS_CATEGORY = 'phoneBookKeywords';
    const PHONE_BOOK_INDUSTRIES_CATEGORY = 'phoneBookIndustries';
    const XML_SALES_RULES_IMPORT_CATEGORY = 'salesRules';
    const MNPFREEDAYS_IMPORT_CATEGORY = 'mnpFreeDays';
    const MSPROVIDERSLIST_IMPORT_CATEGORY = 'mobileProvidersList';


    /** @var array Used for logging purposes */
    protected $_logColumns = [];
    /** @var array Array of messages */
    protected $messages = [
        /** Success message; The imported file has the right column headers */
        'success'   => 'Headers file passed the check test.',
        /** Fail message; The imported file has missing required column headers */
        'required'  => '[ERROR] The required [%s] headers are missing from the imported file.',
        /** Fail message; The imported file has column headers that might not be used, even if this message appears, the required columns headers passed the test and the import will go on */
        'extra'     => '[ERROR] The [%s] headers are not matching no required columns neither the optional ones.',
        /** Info message; The imported file has missing optional headers, but the import will go on */
        'optional'  => '[INFO] The optional [%s] headers are missing from the file import.',
        /** Fail message; The specified type doesn't have a designed template */
        'no_type'   => '[ERROR] No template was found for the %s type.'
    ];

    public function getXMLImportCategories() {
        return [
            self::XML_FUTURE_TRANSACTIONS_IMPORT_CATEGORY,
            self::XML_PACKAGE_TYPES_IMPORT_CATEGORY,
            self::XML_CATEGORIES_IMPORT_CATEGORY,
            self::XML_MULTIMAPPER_IMPORT_CATEGORY,
            self::XML_PRODUCT_MATCH_RULES_IMPORT_CATEGORY,
            self::XML_PRODUCTS_IMPORT_CATEGORY,
            self::XML_TYPE_SUBTYPE_COMBINATIONS_CATEGORY,
            self::XML_ACTIVITY_CODES_CATEGORY,
            self::XML_SALES_RULES_IMPORT_CATEGORY,
        ];
    }

    public function getImportCategories() {
        return [
            self::CABLE_PRODUCT_IMPORT_CATEGORY,
            self::CABLE_ARTIFACT_IMPORT_CATEGORY,
            self::MOBILE_PRODUCT_IMPORT_CATEGORY,
            self::FIXED_PRODUCT_IMPORT_CATEGORY,
            self::MIXMATCH_IMPORT_CATEGORY,
            self::MULTIMAPPER_IMPORT_CATEGORY,
            self::CATEGORIES_IMPORT_CATEGORY,
            self::PRODUCT_MATCH_RULES_IMPORT_CATEGORY,
            self::SALES_RULES_IMPORT_CATEGORY,
            self::MOBILE_SALES_RULES_IMPORT_CATEGORY,
            self::FIXED_SALES_RULES_IMPORT_CATEGORY,
            self::PACKAGE_TYPES_IMPORT_CATEGORY,
            self::CAMPAIGN_IMPORT_CATEGORY,
            self::CAMPAIGN_OFFER_IMPORT_CATEGORY,
            self::DEALER_CAMPAIGNS_IMPORT_CATEGORY,
            self::BUNDLES_IMPORT_CATEGORY,
            self::SHIP_TO_STORES_IMPORT_CATEGORY,
            self::SERVICE_PROVIDERS_IMPORT_CATEGORY,
            self::AGENT_COMMISIONS_IMPORT_CATEGORY,
            self::CHECKOUT_PRODUCTS_IMPORT_CATEGORY,
            self::PROVIS_SALES_ID_IMPORT_CATEGORY,
            self::CATALOG_LOG_FILE_CATEGORY,
            self::PACKAGE_CREATION_TYPES_IMPORT_CATEGORY,
            self::OPTION_TYPES_CATEGORY,
            self::COMMUNICATION_VARIABLE_CATEGORY,
            self::PRODUCT_FAMILY_CATEGORY,
            self::PRODUCT_VERSION_CATEGORY,
            self::CATEGORIES_TREE_CATEGORY,
            self::CATEGORIES_TREE_PRODUCTS_CATEGORY,
            self::PHONE_BOOK_KEYWORDS_CATEGORY,
            self::PHONE_BOOK_INDUSTRIES_CATEGORY,
            self::SALESID_DEALERCODE_IMPORT_CATEGORY,
            self::MNPFREEDAYS_IMPORT_CATEGORY,
            self::MSPROVIDERSLIST_IMPORT_CATEGORY,
        ];
    }

    public function getCategoriesToFiles() {
        return [
            self::CABLE_PRODUCT_IMPORT_CATEGORY => [self::CABLE_PRODUCT_IMPORT_LOG_FILE, self::CABLE_PRODUCT_IMPORT_LOG_FILE_JSON],
            self::CABLE_ARTIFACT_IMPORT_CATEGORY => self::CABLE_ARTIFACT_IMPORT_LOG_FILE,
            self::MOBILE_PRODUCT_IMPORT_CATEGORY => self::MOBILE_PRODUCT_IMPORT_LOG_FILE,
            self::FIXED_PRODUCT_IMPORT_CATEGORY => self::FIXED_PRODUCT_IMPORT_LOG_FILE,
            self::MIXMATCH_IMPORT_CATEGORY => self::MIXMATCH_IMPORT_LOG_FILE,
            self::MULTIMAPPER_IMPORT_CATEGORY => self::MULTIMAPPER_IMPORT_LOG_FILE,
            self::CATEGORIES_IMPORT_CATEGORY => self::CATEGORIES_IMPORT_LOG_FILE,
            self::PRODUCT_MATCH_RULES_IMPORT_CATEGORY => self::MATCHRULES_IMPORT_LOG_FILE,
            self::SALES_RULES_IMPORT_CATEGORY => self::SALES_RULES_IMPORT_LOG_FILE,
            self::MOBILE_SALES_RULES_IMPORT_CATEGORY => self::SALES_RULES_IMPORT_LOG_FILE,
            self::FIXED_SALES_RULES_IMPORT_CATEGORY => self::SALES_RULES_IMPORT_LOG_FILE,
            self::PACKAGE_TYPES_IMPORT_CATEGORY => self::PACKAGE_TYPE_IMPORT_LOG_FILE,
            self::CAMPAIGN_IMPORT_CATEGORY => self::BUNDLE_CAMPAIGN_IMPORT_LOG_FILE,
            self::CAMPAIGN_OFFER_IMPORT_CATEGORY => self::BUNDLE_COMPAIGN_OFFER_IMPORT_LOG_FILE,
            self::SALESID_DEALERCODE_IMPORT_CATEGORY => self::SALESID_DEALERCODE_IMPORT_LOG_FILE,
            self::DEALER_CAMPAIGNS_IMPORT_CATEGORY => self::DEALER_COMPAIGNS_IMPORT_LOG_FILE,
            self::BUNDLES_IMPORT_CATEGORY => self::BUNDLE_RULE_IMPORT_LOG_FILE,
            self::SHIP_TO_STORES_IMPORT_CATEGORY => self::SHIP_TO_STORES_IMPORT_LOG_FILE,
            self::SERVICE_PROVIDERS_IMPORT_CATEGORY => self::SERVICE_PROVIDERS_IMPORT_LOG_FILE,
            self::CATALOG_LOG_FILE_CATEGORY => self::CATALOG_LOG_FILE_LOG_FILE,
            self::PACKAGE_CREATION_TYPES_IMPORT_CATEGORY => self::PACKAGE_CREATION_TYPES_LOG_FILE,
            self::PRODUCT_FAMILY_CATEGORY => self::PRODUCT_FAMILY_LOG_FILE,
            self::PRODUCT_VERSION_CATEGORY => self::PRODUCT_VERSION_LOG_FILE,
            self::CATEGORIES_TREE_CATEGORY => self::CATEGORIES_TREE_LOG_FILE,
            self::CATEGORIES_TREE_PRODUCTS_CATEGORY => self::CATEGORIES_TREE_PRODUCTS_LOG_FILE,
            self::XML_FUTURE_TRANSACTIONS_IMPORT_CATEGORY => self::XML_FUTURE_TRANSACTIONS_IMPORT_LOG_FILE,
            self::XML_PACKAGE_TYPES_IMPORT_CATEGORY => self::XML_PACKAGE_TYPE_IMPORT_LOG_FILE,
            self::XML_CATEGORIES_IMPORT_CATEGORY => self::XML_CATEGORIES_IMPORT_LOG_FILE,
            self::XML_MULTIMAPPER_IMPORT_CATEGORY => self::XML_MULTIMAPPER_IMPORT_LOG_FILE,
            self::XML_PRODUCT_MATCH_RULES_IMPORT_CATEGORY => self::XML_PRODUCT_MATCHRULES_IMPORT_LOG_FILE,
            self::XML_PRODUCTS_IMPORT_CATEGORY => self::XML_PRODUCTS_IMPORT_LOG_FILE,
            self::XML_TYPE_SUBTYPE_COMBINATIONS_CATEGORY => self::XML_TYPE_SUBTYPE_COMBINATIONS_LOG_FILE,
            self::XML_ACTIVITY_CODES_CATEGORY => self::XML_ACTIVITY_CODES_LOG_FILE,
            self::OPTION_TYPES_CATEGORY => self::OPTION_TYPES_LOG_FILE,
            self::COMMUNICATION_VARIABLE_CATEGORY => self::COMMUNICATION_VARIABLE_LOG_FILE,
            self::MNPFREEDAYS_IMPORT_CATEGORY => self::CATALOG_MNPFREEDAYS_IMPORT_LOG_FILE,
            self::MSPROVIDERSLIST_IMPORT_CATEGORY => self::CATALOG_MNPFREEDAYS_IMPORT_LOG_FILE,
            self::PHONE_BOOK_KEYWORDS_CATEGORY => self::PHONE_BOOK_KEYWORDS_LOG_FILE,
            self::PHONE_BOOK_INDUSTRIES_CATEGORY => self::PHONE_BOOK_INDUSTRIES_LOG_FILE,
        ];
    }

    /**
     * This method checks if the imported file column header match the template
     * @param $fileHeaders
     * @param $type
     * @return bool
     */
    public function checkHeader($fileHeaders, $type)
    {
        $fileHeaders = array_map('trim', $fileHeaders);
        //get the header columns for the type specified
        $mappingHeaderData = $this->getHeader($type);
        //type doesn't have a designed template in config
        if(empty($mappingHeaderData)){
            $this->_logColumns['no_type'] = [$type];
            return false;
        }
        //make array of columns header from template
        $mappingHeaderColumns = !isset($mappingHeaderData['optional']) ? $mappingHeaderData['required'] : array_merge($mappingHeaderData['required'],$mappingHeaderData['optional']);
        //check if they are equal from the begging (best scenario)
        if($fileHeaders == $mappingHeaderColumns){
            return true;
        }
        //parse required columns
        $requiredColumns = array_diff($mappingHeaderData['required'], $fileHeaders);
        if(!empty($requiredColumns)){
            $this->_logColumns['required'] = $requiredColumns;
            return false;
        }else{
            //remain columns after checking the required ones
            $remainColumns = array_diff($fileHeaders, $mappingHeaderData['required']);
            if(isset($mappingHeaderData['optional'])){
                //set missing optional columns if they are
                $optional = array_diff($mappingHeaderData['optional'], $remainColumns);
                //set extra founded columns in file if they are
                $extra = array_diff($remainColumns,$mappingHeaderData['optional']);

            }elseif(!empty($remainColumns)){
                $extra = $remainColumns;
            }

            if(!empty($optional)){
                $this->_logColumns['optional'] = $optional;
            }
            if(!empty($extra)){
                $this->_logColumns['extra'] = $extra;
            }
        }

        return true;
    }

    /**
     * Get header for type
     * @param $type
     * @return mixed
     */
    protected function getHeader($type)
    {
        $headers = [
            //products (mobile, fixed, cable)
            'product_template' => [
                'required' => [
                    'sku',
                    'name',
                    'package_type_id',
                    'package_subtype_id',
                    'vat',
                    'status',
                    'product_visibility',
                    'attribute_set',
                ],
                'optional' => [
                    'price',
                    'maf',
                    'sorting',
                    'net_material_price',
                    'mwst_ind',
                    'lock_ind',
                    'article_no',
                    'available_stock_ind',
                    'sub_base_gross_price',
                    'sub_discount_net',
                    'sub_discount_gross',
                    'hwi_indicator',
                    'hwi_amount_gross',
                    'hwi_onetime_payment_net',
                    'hwi_onetime_payment_gross',
                    'pricelist',
                    'effective_date',
                    'expiration_date',
                    'prepaid',
                    'initial_selectable',
                    'ident_needed',
                    'checkout_product',
                    'redplus_role',
                    'product_family_id',
                    'hierarchy_value',
                    'contract_value',
                    'product_version_id',
                    'service_description',
                    'related_slave_sku',
                    'option_type'
                ],
                'mapping' => [
                    'package_type_id' => 'package_type',
                    'package_subtype_id' => 'package_subtype',
                    'product_family_id' => 'product_family',
                    'product_version_id' => 'product_version',
                ]
            ],
            'mixMatch' => [
                'required'=>[
                    'source_sku',
                    'target_sku',
                    'source_category',
                    'target_category',
                    'effective_date',
                    'subscriber_segment',
                    'red_sales_id',
                    'price',
                    'maf',
                    'channel',
                ],
                'optional' => [
                    'expiration_date',
                    'priority',
                    'stream',
                    'package_type_id'
                ],
                'mapping' => [
                    'package_type_id' => 'package_type',
                ]
            ],
            'categories' => [
                'required' => [
                    'sku',
                    'is_family',
                    'is_anchor',
                    'mutually_exclusive',
                    'category_tree',
                ],
                'optional' =>[
                    'family_category_sorting',
                    'description',
                    'hide_not_selected_products'
                ],
                'mapping' => [
                    'family_category_sorting' => 'family_position'
                ]
            ],
            'categories_tree' => [
                'required' => [
                    'external_category_id',
                    'category_name',
                    'external_parent_category_id'
                ],
                'optional' =>[
                    'is_family',
                    'is_anchor',
                    'mutually_exclusive',
                    'family_category_sorting',
                    'hide_not_selected_products',
                    'family_display_name'
                ]
            ],
            'categories_products' => [
                'required' => [
                    'sku',
                    'external_category_id'
                ],
                'optional' =>[
                    'position'
                ]
            ],
            'salesRules' => [
                'required' => [
                    'rule_title',
                    'rule_description',
                    'priority',
                    'is_promo',
                    'is_active',
                    'action',
                    'action_scope',
                    'action_scope_category',
                    'based_on_product',
                    'based_on_category',
                    'channel',
                    'based_on_process_context',
                ],
                'optional' =>[
                    'effective_date',
                    'expiration_date',
                    'based_on_red_sales_id',
                    'maf_discount',
                    'maf_discount_percent',
                    'discount_period_amount',
                    'discount_period',
                    'price_discount',
                    'price_discount_percent',
                    'usage_discount',
                    'usage_discount_percent',
                    'service_source_expression',
                    'show_hint_once',
                    'message_text',
                    'message_title',
                    'rule_type',
                ],
                'mapping' => [
                    'based_on_process_context' => 'process_context'
                ]
            ],
            'multimapper' => [
                'required' => [
                    'sku',
                ],
                'optional' =>[
                    'service_expression',
                    'priority',
                    'stop_execution',
                    'technicalid1',
                    'naturecode1',
                    'technicalid2',
                    'naturecode2',
                    'technicalid3',
                    'naturecode3',
                    'backend',
                    'default_backend',
                    'component_type',
                    'direction',
                    'process_context', //@todo when VFDED1W3S-2478 is done this must be moved to required
                    'xpath_outgoing',
                    'xpath_incomming',
                    /** For fn can be more optional fields */
                ],
                'mapping' => [
                    "technicalid1" => "technical_id",
                    "naturecode1" => "nature_code",
                ]
            ],
            'importPackage' => [
                'required' => [
                    'package_type_id',
                    'package_subtype_id',
                    'package_type_name',
                    'package_subtype_name',
                    'package_subtype_gui_sorting',
                    'default_cardinality',
                ],
                'optional'=>[
                    'package_subtype_visibility',
                    'ignored_by_compatibility_rules',
                    'acq_cardinality',
                    'reo_cardinality',
                    'retblu_cardinality',
                    'retnoblu_cardinality',
                    'retter_cardinality',
                    'ilsblu_cardinality',
                    'ilsnoblu_cardinality',
                    'migcoc_cardinality',
                    'mignococ_cardinality',
                    'move_cardinality',
                    'tou_cardinality',
                    'sorting',
                    'lifecycle_status',
                    'manual_override_allowed',
                    'package_subtype_lifecycle_status',
                    'cardinality'
                ],
                'mapping' => [
                    "package_type_id" => "package_type_code",
                    "package_type_name" => "package_type_name",
                    "package_type_gui_name" => "package_type_name",
                    "package_type_gui-name" => "package_type_name",
                    "package_subtype" => "package_subtype_code",
                    "package_subtype_id" => "package_subtype_code",
                    "package_subtype_name" => "package_subtype_name",
                    "package_subtype_gui_name" => "package_subtype_name",
                    "package_subtype_sorting" => "package_subtype_position",
                    "package_subtype_gui_sorting" => "package_subtype_position",
                    "sorting" => "package_subtype_position",
                    "default_cardinality" => "cardinality",
                    "package_subtype_visibility" => "package_subtype_visibility"
                ]
            ],
            'packageCreationTypes' => [
                'required' => [
                    'package_creation_type_id',
                    'package_type_id',
                    'filter_attributes',
                    'package_creation_name',
                ],
                'optional' => [
                    'sorting',
                    'package_creation_grouping',
                    'package_creation_grouping_sorting',
                    'package_subtype_filter_attributes',
                    'filter_attributes_ACQ',
                    'filter_attributes_REO',
                    'filter_attributes_RETBLU',
                    'filter_attributes_RETNOBLU',
                    'filter_attributes_RETTER',
                    'filter_attributes_ILSBLU',
                    'filter_attributes_ILSNOBLU',
                    'filter_attributes_MIGCOC',
                    'filter_attributes_MIGCOCOC',
                    'filter_attributes_MOVE',
                    'filter_attributes_TO',
                ]
            ],
            'productMatchRules' => [
                'required' => [
                    'rule_title',
                    'source_type',
                    'target_type',
                    'source',
                    'source_collection',
                    'service_source_expression',
                    'service_target_expression',
                    'target',
                    'operation',
                    'priority',
                    'channel',
                    'rule_origin',
                    'process_context',
                    'operation_type',
                    'effective_date',
                    'package_type_id'
                ],
                'optional' => [
                    'expiration_date',
                    'rule_description',
                    'override_initial_selectable'
                ],
                'mapping' => [
                    'package_type_id'=>'package_type',
                    'source' => 'left_id',
                    'operation' => 'operation',
                    'target' => 'right_id',
                    'operation_type' => 'operation_type',
                    'priority' => 'priority',
                    'channel' => 'website_id',
                    'rule_title' => 'rule_title',
                    'rule_description' => 'rule_description',
                    'rule_origin' => 'rule_origin',
                    'process_context' => 'process_context',
                    'service_source_expression' => 'service_source_expression',
                    'service_target_expression' => 'service_target_expression',
                    'source_collection' => 'source_collection',
                    'source_type' => 'source_type',
                    'target_type' => 'target_type',
                    'effective_date' => 'effective_date',
                    'expiration_date' => 'expiration_date',
                    'override_initial_selectable' => 'override_initial_selectable'
                ]
            ],
            'dealerCampaign' =>[
                'required' => [
                    'void',
                    'allowed/notallowedfor',
                    'campaign_id'
                ],
                'mapping' => [
                    'void' => 'vf_dealer_code',
                    'allowed/notallowedfor' => 'allowed',
                    'campaign_id' => 'name'
                ]
            ],
            'serviceProviders' => [
                'required' => [
                    "telcompany",
                    "osf gui name",
                    "cableprovider",
                ],
                'optional' => [
                    'haftungsfreistellung_vorh'
                ],
                'mapping' => [
                    'telcompany' => 'code',
                    'osf gui name' => 'name',
                    'cableprovider' => 'cable_provider',
                    'haftungsfreistellung_vorh' => 'fixed_line',
                ]
            ],
            //(not used yet)
            'pre_conf_packages_template' => [
                'package_id',
                'package_type_id',
                'package_name',
                'sku',
                'website',
                'optional_campaign_code'
            ],
            'agent' => [
                'required' => [
                    'username',
                    'password',
                    'is_active',
                    'role_id',
                    'dealer_code',
                    'store_id',
                ],
                'optional' => [
                    'first_name',
                    'last_name',
                    'email',
                    'phone',
                    'employee_number',
                ],
            ],
            'checkoutProducts' => [
                'required' => [
                    'sku',
                    'attribute_set',
                    'package_type_id',
                    'package_subtype_id',
                    'name',
                    'option_type',
                    'display_label_checkout',
                    'option_ranking',
                    'price',
                    'product_visibility'
                ]
            ],
            'productFamily' =>[
                'required'=>[
                    'product_family_id',
                ],
                'optional'=>[
                    'lifecycle_status',
                    'product_family_name',
                    'product_version_id',
                    'package_type_id',
                    'package_subtype_id'
                ]
            ],
            'productVersion' =>[
                'required'=>[
                    'product_version_id',
                ],
                'optional'=>[
                    'lifecycle_status',
                    'product_family_id',
                    'product_version_name',
                    'package_type_id',
                ]
            ],
            'optionTypes' =>[
                'required'=>[
                    'option_type'
                ],
                'optional'=>[
                    'display_label_checkout'
                ]
             ],
             'communicationVariables' =>[
                'required'=>[
                    'communication_variable'
                ],
                'optional'=>[
                    'message'
                ]
             ],
            'provisSalesId' => [
                'required' => [
                    'red_sales_id',
                    'status',
                    'level',
                    'sales_channel',
                    'sales_subchannel',
                    'classification',
                    'blue_sales_id',
                    'yellow_sales_id',
                ],
                'optional' => [
                    'red_sales_id_level-1',
                    'red_sales_id_level-2',
                    'blue_top_vo',
                ]
            ],
        ];

        return $headers[$type];
    }

    /**
     * @param string $date
     * @return false|string
     */
    public function formatDate(string $date)
    {
        if (!$date) {
            return '';
        }
        $time = strtotime($date);
        if ($time === false) {
            return '';
        }
        return date('Y-m-d 00:00:00', $time);
    }

    /**
     * @param string $price
     * @return string
     */
    public function formatPrice(string $price)
    {
        $price = str_replace('.', '', $price);
        $price = str_replace(',', '.', $price);
        $price = filter_var($price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        return number_format($price, 4, '.', '');
    }

    /**
     * Convert consecutive uppercase letters to lowercase
     * e.g MWSTIndicator => mwst_indicator
     * e.g. AbcAAc => abc_a_ac
     * e.g. asda_SSConcs => asda_ss_concs
     *
     * @param $string
     * @return string
     */
    public function replaceUpper($string)
    {
        $regex = '/(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])/x';

        return strtolower(implode("_", preg_split($regex, $string)));
    }

    /**
     * Get the message check
     * @return array
     */
    public function getMessages()
    {
        $response = [];
        if(empty($this->_logColumns)){
            $response['success'] = $this->messages['success'];
        }else{
            foreach ($this->_logColumns as $key => $columns){
                $keyResponse = sprintf($this->messages[$key], implode(', ',$columns));
                $response[$key] = $keyResponse;
            }
        }
        return $response;
    }

    /**
     * Mapping data for type
     * @param $type
     * @param array $subTypes (defaulted 'mapping', but in order to not duplicate data, you can use any combination from ('required','optional','mapping')
     * @return array
     */
    public function getMapping($type, $subTypes = array('mapping'))
    {
        $tempMappingData =[];
        if($mappingData = $this->getHeader($type)){
            foreach ($subTypes as $value){
                if(isset($mappingData[$value])){
                    $tempMappingData = array_merge($tempMappingData, $mappingData[$value]);
                }
            }
        }
        return $tempMappingData;
    }

    /**
     * Gets required columns for specified type
     *
     * @param $type
     * @return array
     */
    public function getRequiredColumns($type)
    {
        $headers = $this->getHeader($type);
        return $headers['required'];
    }

    /**
     * Mapping data for type
     * @param $type
     * @param array $subTypes (defaulted 'mapping', but in order to not duplicate data, you can use any combination from ('required','optional','mapping')
     * @return array
     */
    public function getLogContent($executionId, $category) {
        $content = '';
        $categoriesToFiles = $this->getCategoriesToFiles();

        if (!is_array($categoriesToFiles[$category])) {
            $categoriesToFiles[$category] = [$categoriesToFiles[$category]];
        }

        foreach ($categoriesToFiles[$category] as $filename) {
            $logFilename = $filename . '_' . self::LOG_FILE_EXECUTION_SUFFIX . '_' . $executionId . '.log';
            $logFile = Mage::getBaseDir('log') . '/' . $logFilename;
            $oldLogDir = Mage::getBaseDir('log') . '/old/';

            if (file_exists($logFile)) {
                $content .= file_get_contents($logFile);
            } else if (is_dir($oldLogDir)) {
                chdir($oldLogDir);
                if (!empty($foundFiles = glob('*/'.$logFilename))) {
                    $content .= file_get_contents($foundFiles[0]);
                }
            }
        }

        if ($content !== '') {
            return $content;
        }

        return false;
    }

    /**
     * Checks whether or not product data has both package type and package subtype valid
     * @return bool
     */
    public function hasValidPackageDefined($productData, $cable = false)
    {
        if ($cable) {
            $productData['package_type'] = $productData['packageTypeId'];
            $productData['package_subtype'] = $productData['packageSubtypeId'];
            $productData['sku'] = $productData['productId'];
        }
        $valid = true;
        $this->_currentProductSku = $productData['sku'];

        $packageTypes = explode(',', $productData['package_type']);
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');
        //get package types from db
        $validPackageTypes = $packageHelper->getPackageTypes();

        if(!empty($validPackageTypes)){
            $validPackageTypes = array_keys($validPackageTypes);
        }

        $invalidPackageType = null;
        $invalidPackageSubType = null;
        foreach ($packageTypes as $packageType) {
            $packageType = trim($packageType);
            if (!in_array($packageType, $validPackageTypes)) {
                $invalidPackageType = $packageType;
                break;
            }
            if(empty($packageHelper->getPackageSubtype($packageType, $productData['package_subtype'], false, []))){
                $invalidPackageSubType = $productData['package_subtype'];
                break;
            }

        }

        if (empty($productData['package_type']) || $invalidPackageType) {
            $productData['package_type'] = !empty($productData['package_type']) ? $productData['package_type'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['sku'] . "\e[0m) Skipped entire product because of invalid package \e[1;33mtype\e[0m value: \e[1;31m" . $invalidPackageType, "\033[0m (case sensitive)\n";
            $this->_logError("[ERROR] (SKU " . $productData['sku'] . ") Skipped entire product because of invalid package type value: " . $invalidPackageType . " (case insensitive)");
            $valid = false;
        }

        if (empty($productData['package_subtype']) || $invalidPackageSubType) {
            $productData['package_subtype'] = !empty($productData['package_subtype']) ? $productData['package_subtype'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['sku'] . "\e[0m) Skipped entire product because of invalid package \e[1;33msubType\e[0m value: \e[1;31m" . $productData['package_subtype'], "\033[0m (case sensitive)\n";
            $this->_logError("[ERROR] (SKU " . $productData['sku'] . ") Skipped entire product because of invalid package subType value: " . $productData['package_subtype'] . " (case sensitive)");
            $valid = false;
        }

        return $valid;
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }

    /**
     * Log Error function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        $this->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }

    /**
     * Set debug mode
     *
     * @param bool $debug
     */
    public function setDebug($debug)
    {
        $this->_debug = $debug;
    }

    /**
     * Get debug mode
     *
     * @return bool
     */
    public function getDebug()
    {
        return $this->_debug;
    }

    /**
     * Log simple messages
     *
     * @param string $msg
     */
    public function logMsg($msg, $toSlack = true, $level = null)
    {
        $attachments = [];
        if ($toSlack) {
            $attachments[] = [
                "fields" => ['title' => 'File', 'value' => $this->pathToImportFile, 'short' => false],
            ];
            $this->postToSlack($msg, $attachments);
        }
        Mage::log($msg, $level, $this->getImportLogFile());
    }

    public function setPathToImportFile($path) {
        $this->pathToImportFile = $path;
    }

    /**
     * Post relevant information to Slack Channel during import
     * @param string $msg
     * @param array $attachments ['text' => extraMsg, 'color' => hexCode]
     * @param string $channel
     * @param string $hook
     * @return boolean false if disabled from backend or result from Slack curl call
     */
    public function postToSlack($msg, $attachments = [], $channel = null, $hook = null)
    {
        $postToSlack = Mage::getStoreConfig('omnius_general/log_settings/post_to_slack');
        $storeConfig = [
            'channel' => Mage::getStoreConfig('omnius_general/log_settings/slack_channel'),
            'hook' => Mage::getStoreConfig('omnius_general/log_settings/slack_endpoint'),
            'emoji' => Mage::getStoreConfig('omnius_general/log_settings/slack_emoji'),
            'username' => Mage::getStoreConfig('omnius_general/log_settings/slack_username'),
            'color' => Mage::getStoreConfig('omnius_general/log_settings/slack_color'),
        ];
        if ($postToSlack && strstr($msg, '/vagrant/') === false) {
            $payload = [
                'channel' => "#" . ($channel ?: $storeConfig['channel']),
                'icon_emoji' => $storeConfig['emoji'],
                'text' => $msg,
                'username' => $storeConfig['username'],
            ];
            if (count($attachments)) {
                foreach ($attachments as &$attachment) {
                    if (!isset($attachment['key'])) {
                        continue;
                    }
                    switch ($attachment['key']) {
                        case LIBXML_ERR_WARNING:
                        case 'extra':
                            $attachment['color'] = '#ffaa00';
                            break;
                        case 'optional':
                            $attachment['color'] = '#ffcc00';
                            break;
                        case LIBXML_ERR_ERROR:
                        case LIBXML_ERR_FATAL:
                        case 'required':
                        case 'no_type':
                            $attachment['color'] = '#ff0000';
                            break;
                        default:
                            $attachment['color'] = $storeConfig['color'] ?: '#ff0000';
                            break;
                    }
                    unset($attachment['key']);
                }
                unset($attachment);
                if (count($attachments)) {
                    $payload['attachments'] = $attachments;
                }
            }
            $encoded = json_encode($payload, JSON_UNESCAPED_UNICODE);
            if ($encoded === false) {
                throw new RuntimeException(sprintf('JSON encoding error %s: %s', json_last_error(), json_last_error_msg()));
            }

            $slack = curl_init($hook ?: $storeConfig['hook']);
            curl_setopt($slack, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($slack, CURLOPT_POSTFIELDS, ["payload" => $encoded]);
            curl_setopt($slack, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($slack, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($slack);
            curl_close($slack);

            return $result;
        } else {
            return false;
        }
    }

    /**
     * Log exception messages
     *
     * @param Exception $ex
     */
    public function log($ex, $level = null)
    {
        Mage::log($ex->getMessage(), $level, $this->getImportLogFile());
    }
}
