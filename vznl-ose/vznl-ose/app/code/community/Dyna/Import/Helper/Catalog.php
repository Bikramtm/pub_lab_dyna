<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)

 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class Dyna_Import_Helper_Catalog extends ISAAC_Import_Helper_Catalog
{
    protected $alwaysImportAttributeCodes = [
        'expiration_date'
    ];

    /**
     * ISAAC_Import_Helper_Catalog constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Mage_Catalog_Model_Abstract $model
     * @param array $attributeValues
     * @param int $storeId
     * @return array
     * @throws Mage_Core_Exception
     */
    public function getAttributeValuesToUpdate(
        Mage_Catalog_Model_Abstract $model, array $attributeValues, $storeId
    ) {
        /** @var Mage_Catalog_Model_Resource_Abstract $modelResource */
        $modelResource = $model->getResource();

        $modelValuesToUpdate = [];
        $isAdminImport = $storeId == Mage_Core_Model_App::ADMIN_STORE_ID;

        foreach ($attributeValues as $attributeCode => $importAttributeValue) {
            $attribute = $this->eavConfig->getAttribute($modelResource->getEntityType(), $attributeCode);
            if (!$attribute || !$attribute->getId()) {
                $this->importHelperData->logMessage('warning: attribute ' . $attributeCode . ' does not exist');
                continue;
            }
            $attributeValue = null;
            switch ($attribute->getData('frontend_input')) {
                case 'text':
                case 'textarea':
                case 'date':
                case 'image':
                    $attributeValue = (string) $importAttributeValue;
                    if ($attributeValue === '') {
                        $attributeValue = null;
                    }
                    break;
                case 'boolean':
                    $attributeValue = $importAttributeValue ? '1' : '0';
                    break;
                case 'weight':
                    $attributeValue = $this->formatDecimal($importAttributeValue);
                    break;
                case 'price':
                    if ($isAdminImport) {
                        //prices can only be set for admin stores
                        if (empty($importAttributeValue) && !$attribute->getData('is_required')) {
                            $attributeValue = null;
                        } else {
                            $attributeValue = $this->formatDecimal($importAttributeValue);
                        }
                    } else {
                        $attributeValue = $model->getData($attributeCode);
                    }
                    break;
                case 'select':
                    if ($attribute->getData('source_model')
                        && !in_array(
                            $attribute->getData('source_model'),
                            array('eav/entity_attribute_source_table', 'tax/class_source_product')
                        )
                    ) {
                        $attributeValue = (string) $importAttributeValue;
                        if ($attributeValue === '') {
                            $attributeValue = null;
                        }
                    } elseif ($isAdminImport) {
                        //attribute options can only be created for admin stores
                        $attributeValue = $this->importAttributeOptionHelper->getDropdownAttributeOptionId(
                            $attribute->getEntityType(),
                            $attributeCode,
                            $importAttributeValue,
                            $storeId
                        );
                    } else {
                        $attributeValue = $model->getData($attributeCode);
                        $this->importAttributeOptionHelper->updateOptionStoreValues(
                            $attribute->getEntityType(),
                            $attributeCode,
                            $attributeValue,
                            $importAttributeValue,
                            $storeId
                        );
                    }

                    break;
                case 'multiselect':
                    if ($attribute->getData('source_model') &&
                        in_array(
                            $attribute->getData('source_model'),
                            array('catalog/product_attribute_source_countryofmanufacture')
                        )
                    ) {
                        $attributeValue = $importAttributeValue;
                        if ($attributeValue === '') {
                            $attributeValue = null;
                        }
                    } else {
                        $importAttributeValue = (array) $importAttributeValue;
                        $attributeIds = [];
                        foreach ($importAttributeValue as $importAttributeValueIndex => $optionLabel) {
                            if ($isAdminImport) {
                                //attribute options can only be created for admin stores
                                $attributeIds[] = $this->importAttributeOptionHelper->getDropdownAttributeOptionId(
                                    $attribute->getEntityType(),
                                    $attributeCode,
                                    $optionLabel,
                                    $storeId
                                );
                            } else {
                                $attributeIds = explode(',', $model->getData($attributeCode));

                                if (isset($attributeIds[$importAttributeValueIndex])) {
                                    $this->importAttributeOptionHelper->updateOptionStoreValues(
                                        $attribute->getEntityType(),
                                        $attributeCode,
                                        $attributeIds[$importAttributeValueIndex],
                                        $optionLabel,
                                        $storeId
                                    );
                                }
                            }
                        }

                        if (count($attributeIds)) {
                            $attributeValue = implode(',', $attributeIds);
                        } else {
                            $attributeValue = null;
                        }
                    }
                    break;
                default:
                    $this->importHelperData->logMessage('warning: cannot update ' . $attribute->getData('frontend_input') . ' attribute ' . $attributeCode . ' with value ' . $attributeValue . ' (this feature is not yet supported)');
            }
            //update $updatedAttributeValues index with the new values, and only when necessary
            if (in_array($attributeCode, $this->alwaysImportAttributeCodes) || $attributeValue !== $model->getData($attributeCode)) {
                $modelValuesToUpdate[$attributeCode] = $attributeValue;
            }
        }

        return $modelValuesToUpdate;
    }
}
