<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class Dyna_Import_Helper_File extends Mage_Core_Helper_Abstract
{
    protected $importType = 'product';

    /** @var ISAAC_Import_Helper_Data */
    protected $importHelper;

    public function __construct()
    {
        $this->importHelper = Mage::helper('isaac_import');
    }

    /**
     * @return string
     */
    public function getBaseDirName()
    {
        return Mage::getBaseDir('var') . DS . 'import';
    }

    /**
     * @param string $fileName
     * @return string
     */
    public function getFilePath($fileName)
    {
        return $this->getBaseDirName() . DS .  $fileName;
    }
}
