<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer  = $this;
/* @var $connection Varien_Db_Adapter_Pdo_Mysql */
$connection = $installer->getConnection();

$installer->startSetup();

$flat_entities = [
    'dyna_bundles/bundle_rules',
    'dyna_bundles/bundle_actions',
    'dyna_bundles/bundle_conditions',
    'dyna_bundles/bundle_hints',
    'dyna_bundles/bundle_processcontext',
    'dyna_bundles/campaign_offer',
    'dyna_bundles/campaign',
    'dyna_bundles/campaign_offer_relation',
    'dyna_bundles/campaign_dealercode',
];
$column = 'stack';

foreach ($flat_entities as $entity) {
    $table = $installer->getTable($entity);
    if ($connection->tableColumnExists($table, $column)) {
        $connection->dropColumn($table, $column);
    }
}

$installer->endSetup();
