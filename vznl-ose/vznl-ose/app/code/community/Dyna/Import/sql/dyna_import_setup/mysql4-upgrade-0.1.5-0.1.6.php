<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer  = $this;
/* @var $connection Varien_Db_Adapter_Pdo_Mysql */
$connection = $installer->getConnection();

$installer->startSetup();

$flat_entities = [
    'dyna_package/packageCreationGroups',
];
$column = 'stack';

foreach ($flat_entities as $entity) {
    $table = $installer->getTable($entity);
    if ($connection->tableColumnExists($table, $column)) {
        $connection->changeColumn($table, $column, $column, [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 20,
            'comment' => 'Stack'
        ], true);
    }
}

$installer->endSetup();
