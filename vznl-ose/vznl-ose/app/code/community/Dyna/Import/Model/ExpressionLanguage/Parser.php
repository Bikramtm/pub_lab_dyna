<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_Import_Model_ExpressionLanguage_Parser extends Mage_Core_Model_Abstract
{
    /**
     * Exception handler for invalid source condition import file
     * @param $message
     */
    public function throwException($message)
    {
        return Mage::getModel('dyna_import/expressionLanguage_exception', array(
            'message' => $message
        ));
    }

    public function test()
    {
$string =
<<<XML
<Conditions>
    <Aggregator>any</Aggregator>
    <ConditionValue>true</ConditionValue>
    <SourceCondition>
        <Type>availability.HasTechnology</Type>
        <Params>
            <Param name="DowngradeBandwith">null</Param>
            <Param name="Priority">1</Param>
            <Param name="Technology">IP-Bitstream</Param>
            <Param name="AvailabiliyIndicator">GRUEN</Param>
        </Params>
        <Operator>is</Operator>
        <ConditionValue>true</ConditionValue>
    </SourceCondition>
    <SourceCondition>
        <Type>availability.HasTechnology</Type>
        <Params>
            <Param name="DowngradeBandwith">null</Param>
            <Param name="Priority">1</Param>
            <Param name="Technology">IP-Bitstream VDSL</Param>
            <Param name="AvailabiliyIndicator">GRUEN</Param>
        </Params>
        <Operator>is</Operator>
        <ConditionValue>true</ConditionValue>
    </SourceCondition>
    <SourceCondition>
        <SourceConditions>
            <Aggregator>all</Aggregator>
            <ConditionValue>true</ConditionValue>
            <SourceCondition>
                <Type>catalog.getOptionsWithHierarchy</Type>
                <Params>
                    <Param name="ProductSku">SKU</Param>
                    <Param name="Operator">&lt;=</Param>
                </Params>
                <Operator>is one of</Operator>
                <ConditionValue>SKU1, SKU2</ConditionValue>
            </SourceCondition>
            <SourceCondition>
                <Type>catalog.getOptionsWithHierarchy</Type>
                <Params>
                    <Param name="ProductSku">SKU</Param>
                    <Param name="Operator">&lt;=</Param>
                </Params>
                <Operator>contains</Operator>
                <ConditionValue>SKU1,SKU2</ConditionValue>
            </SourceCondition>
            <SourceCondition>
                <Type>availability.HasTechnology</Type>
                <Params>
                    <Param name="DowngradeBandwith">null</Param>
                    <Param name="Priority">1</Param>
                    <Param name="Technology">L2-BSA VDSL</Param>
                    <Param name="AvailabiliyIndicator">GRUEN</Param>
                </Params>
                <Operator>is</Operator>
                <ConditionValue>true</ConditionValue>
            </SourceCondition>
        </SourceConditions>
    </SourceCondition>
</Conditions>
XML;
        $xmlContent = simplexml_load_string($string);
        /** @var Dyna_Import_Model_ExpressionLanguage_Source_Conditions $condition */
        $condition = Mage::getSingleton('dyna_import/expressionLanguage_source_conditions');
        $condition->parseConditions($xmlContent);
        return $condition->asString();
    }
}
