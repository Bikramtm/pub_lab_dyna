<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class Dyna_Import_Model_Generator_Mobile_CategoryAbstract implements ISAAC_Import_Generator
{
    /** @var bool */
    protected $canExecute;

    /** @var Dyna_Import_Model_Generator_Cable_Definition_Category */
    protected $definitionModel;

    /** @var Dyna_Import_Helper_File */
    protected $fileHelper;

    /** @var ISAAC_Import_Helper_Data */
    protected $importHelper;

    /** @var string */
    protected $importFile = 'FN_Categories.csv';

    public function __construct()
    {
        // make sure all categories have a category_code set
        $this->checkCategoryCodes();

        $this->fileHelper = Mage::helper('dyna_import/file');
        $this->importHelper = Mage::helper('isaac_import');
        $this->importFile = $this->fileHelper->getFilePath($this->importFile);

        $this->canExecute = $this->canExecute();

        /** @var Dyna_Import_Model_Generator_Cable_Definition_Category */
        $this->definitionModel = Mage::getModel('dyna_import/generator_cable_definition_category');
    }

    /**
     * @return bool
     */
    public function canExecute()
    {
        if (!file_exists($this->importFile)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        if (!$this->canExecute) {
            return new ArrayIterator();
        }

        $csvDictReader = new ISAAC_Import_CSV_Dict_Reader($this->importFile);
        $csvDictReader->setDelimiter(';');
        $csvDictReader->setSkipEmptyLines(true);

        $productsFilterIterator = new CallbackFilterIterator($csvDictReader, function ($data) {
            return $this->definitionModel->validate($data);
        });

        $categoryIterator = new ISAAC_Import_Map_Iterator($productsFilterIterator, function ($data) {
            return $this->definitionModel->transform($data);
        });

        return $categoryIterator;
    }

    public function cleanup()
    {
    }

    /**
     * @param Dyna_Catalog_Model_Category $category
     * @param array $tree
     * @return string
     */
    private function buildCategoryCode(Dyna_Catalog_Model_Category $category, $tree = [])
    {
        $tree[] = $category->getName();

        /** @var Dyna_Catalog_Model_Category $parentCategory */
        $parentCategory = $category->getParentCategory();

        if($parentCategory->getId()) {
            return $this->buildCategoryCode($parentCategory, $tree);
        }

        return implode(Dyna_Import_Model_Generator_Cable_Definition_Category::TREE_DELIMITER, array_reverse($tree));
    }

    private function checkCategoryCodes()
    {
        /** @var Mage_Catalog_Model_Resource_Category_Collection $categoryCollection */
        $categoryCollection = Mage::getResourceModel('catalog/category_collection');
        $categoryCollection->addAttributeToSelect('name');
        $categoryCollection->addAttributeToFilter('category_code', ['null' => true], 'left');

        /** @var Dyna_Catalog_Model_Category $category */
        foreach ($categoryCollection as $category) {
            $category->setData('category_code', $this->buildCategoryCode($category));
            $category->setData('assign_products_automatically', true);
            $category->save();
        }
    }
}