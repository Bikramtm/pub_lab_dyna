<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class Dyna_Import_Model_Generator_DefinitionAbstract
{
    /** @var array */
    protected $data = [];

    /** @var bool */
    protected $debug = false;

    /** @var Dyna_Import_Helper_Data */
    protected $importHelper;

    /** @var string */
    protected $importLogFile = 'isaac_import.log';

    public function __construct()
    {
        $this->importHelper = Mage::helper('dyna_import');
        $this->importHelper->setImportLogFile($this->importLogFile);
    }

    /**
     * @param string $value
     * @return bool|null
     */
    protected function setBooleanData(string $value)
    {
        switch (strtolower($value)) {
            case 'ja':
            case 'y':
            case 'yes':
            case '1':
            case 'ok':
                $value = true;
                break;
            case 'nee':
            case 'n':
            case 'no':
            case '0':
            case 'x':
                $value = false;
                break;
            default:
                $value = null;
                break;
        }

        return $value;
    }

    /**
     * @param $message
     * @param bool $verbose
     */
    protected function logMessage($message, $verbose = false)
    {
        if (!$this->debug || ($verbose && $this->debug)) {
            $this->importHelper->logMsg($message);
        }
    }

    /**
     * @param array|string $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
}