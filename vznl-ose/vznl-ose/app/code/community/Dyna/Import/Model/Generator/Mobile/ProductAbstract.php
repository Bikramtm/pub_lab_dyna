<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class Dyna_Import_Model_Generator_Mobile_ProductAbstract
{
    /** @var bool */
    protected $canExecute;

    /** @var Dyna_Import_Helper_File */
    protected $fileHelper;

    /** @var ISAAC_Import_Helper_Data */
    protected $importHelper;

    /** @var string */
    protected $importFile;

    /** @var Dyna_Import_Model_Generator_Mobile_Definition_ProductAbstract */
    protected $definitionModel;

    /**
     * Dyna_Import_Model_Generator_Product constructor.
     */
    public function __construct()
    {
        $this->fileHelper = Mage::helper('dyna_import/file');
        $this->importHelper = Mage::helper('isaac_import');
        $this->importFile = $this->fileHelper->getFilePath($this->importFile);

        $this->canExecute = $this->canExecute();
    }

    /**
     * @return bool
     */
    public function canExecute()
    {
        if (!file_exists($this->importFile)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        if (!$this->canExecute) {
            return new ArrayIterator();
        }

        $csvDictReader = new ISAAC_Import_CSV_Dict_Reader($this->importFile);
        $csvDictReader->setDelimiter(';');
        $csvDictReader->setSkipEmptyLines(true);

        $productFilterIterator = new CallbackFilterIterator($csvDictReader, function ($data) {
            return $this->definitionModel->validate($data);
        });

        return new ISAAC_Import_Map_Iterator($productFilterIterator, function ($data) {
            return $this->definitionModel->transform($data);
        });
    }

    public function cleanup()
    {
    }
}