<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Omnius_Import_Model_Adapter_Csv
 */
class Dyna_Import_Model_Adapter_Xml
{
    protected $stack;
    protected $type;
    protected $path;
    protected $file;
    protected $fullImport;
    protected $types  = null;
    protected $status = null;
    protected $schema = null;
    protected $_hasError;
    /** @var Dyna_Import_Helper_Data $_importHelper */
    public $_importHelper;
    /** @var Omnius_Import_Helper_Data $_helper */
    public $_helper;
    protected $_executionId;
    protected $importId = null;
    protected $_importLogCategory;
    protected $isUTF8Converted;
    protected $validExtensions;
    protected $_totalFileRows = 0;
    protected $_skippedFileRows = 0;
    protected $logFile = 'import_xml.log';
    protected $debug;
    const ERROR_CODE_SUCCESS = 0;
    const ERROR_CODE_TECHNICAL = 1;
    const ERROR_CODE_DATA = 2;
    const ERROR_CODE_USER = 3;
    protected $SUPPORTED_ERROR_CODES = [
        'ERROR_CODE_SUCCESS' => self::ERROR_CODE_SUCCESS,
        'ERROR_CODE_TECHNICAL' => self::ERROR_CODE_TECHNICAL,
        'ERROR_CODE_DATA' => self::ERROR_CODE_DATA,
        'ERROR_CODE_USER' => self::ERROR_CODE_USER,
    ];

    /**
     * Dyna_Import_Model_Adapter_Xml constructor.
     * @param $args
     * @throws Exception
     */
    public function __construct($args)
    {
        $this->_importHelper = Mage::helper('dyna_import');

        $this->type = $args['type'] ?: false;
        $this->file = $args['file'] ?: false;
        $this->fullImport = $args['fullImport'] == true;
        $this->path = $this->getImportDir() . DS . $this->file;
        $this->types = $this->_importHelper->getXMLImportCategories();
        $this->schema = ($this->type) ? ($this->getImportValidationDir() . DS . $this->type . '.xsd') : false;
        $this->status = $args['status'] ?: false;
        $this->_hasError = false;
        $this->executionId = $args['executionId'] ?: false;
        $this->validExtensions = ['json', 'csv', 'xml'];
        $this->isUTF8Converted = false;
        $this->_importLogCategory = $args['logCategory'] ?: $this->type;
        $this->_helper = Mage::helper("omnius_import/data");
        $this->_helper->setImportLogFile($this->logFile);
        $this->_importHelper->setImportLogFile($this->logFile);

        // verify is an import type was specified
        if (!$this->type) {
            $this->_hasError = true;
            throw new Exception('Import type not specified.');
        }

        // verify if an import file was specified
        if (!$this->file || !file_exists($this->path)) {
            $this->_hasError = true;
            throw new Exception('Import file not specified or import file is missing.');
        }

        // check current status
        $this->checkStatus();

        // execution id
        $this->getExecutionId();

        // validate type
        $this->validateType();

        // check and convert to UTF8
        $this->checkEncoding();
    }

    /**
     * Start the counter before starting the import
     */
    public function beforeImport()
    {
        Dyna_Import_Model_Adapter_Stopwatch::start($this->type);
        $this->_log('Importing '. $this->type .' file: ' . $this->path);
    }

    /**
     * Stops tbe counter after the import is done
     */
    public function afterImport()
    {
        $this->_log(sprintf('Import contains in total %d rows.', $this->_totalFileRows));
        $this->_log(sprintf('Successfully imported %d rows.', $this->_totalFileRows - $this->_skippedFileRows));
        $this->_log(sprintf('Rows with error %d', $this->_skippedFileRows));
        $this->_log(sprintf('Finished importing %s file: %s', $this->type, $this->path));

        if ($this->isUTF8Converted && !$this->_hasError) {
            $this->writeLine('++++++++++++++++');
            $this->writeLine('File "' . $this->path . '" was imported with auto convert to UTF8.');
            $this->writeLine('++++++++++++++++');
        }
        $this->writeLine(sprintf('Time taken for import %s', Dyna_Import_Model_Adapter_Stopwatch::elapsed($this->type)));
        $this->logImportedFile();
    }

    /**
     * To be called from import script
     */
    public function run()
    {
        $this->beforeImport();
        $this->import();
        $this->afterImport();
    }

    /**
     * Get data as object from XML
     *
     * @param bool $validateXml
     * @param bool $asArray
     * @return bool|mixed
     * @throws Exception
     */
    public function getXmlData($validateXml = false, $asArray = true)
    {

        if ($validateXml) {
            if (!$this->schema) {
                $this->_hasError = true;
                throw new Exception('No schema file provided.');
            } else {
                if (!file_exists($this->schema)) {
                    $this->_hasError = true;
                    throw new Exception('Validation schema file not found, should be in ' . $this->schema);
                }

                libxml_use_internal_errors(true);
                $document = new DOMDocument();
                $contents = file_get_contents($this->path);

                if (!mb_check_encoding($contents, 'UTF-8')) {
                    // Make sure that content is always set to UTF-8
                    $contents = iconv(mb_detect_encoding($contents, mb_detect_order(), true), "UTF-8", $contents);
                }

                $document->loadXML($contents);

                if (!$document->schemaValidate($this->schema)) {
                    $this->_hasError = true;
                    $xmlErrors = libxml_get_errors();

                    $attachments = [];
                    foreach ($xmlErrors as $xmlError) {
                        $msg = $this->libxmlDisplayError($xmlError);
                        $this->_logError($msg);
                        $attachments[] = ['key' => $xmlError->level, 'text' => $msg];
                    }
                    $msg = 'While processing file ' . $this->path . ' we encountered the following:';
                    $this->_importHelper->postToSlack($msg, $attachments);
                    $this->_skippedFileRows = 1;
                    $this->logImportedFile();

                    libxml_clear_errors();
                    throw new Exception('The provided XML is not valid according to schema.');
                } else {
                    $this->logImportedFile();
                }
            }
        }

        return ($asArray) ? json_decode(json_encode(simplexml_load_file($this->path)), true) : simplexml_load_file($this->path);
    }

    /**
     * Validates import type
     * @throws Exception
     */
    protected function validateType()
    {
        if (!$this->type || !in_array($this->type, $this->types)) {
            $this->_hasError = true;
            throw new Exception('Invalid type, please specify [' . implode('/', $this->types) . ']');
        }
    }

    /**
     * Return path for import dir (if not exist it will create it)
     * @return string
     */
    protected function getImportDir()
    {
        if (!is_dir(Mage::getBaseDir('var') . DS . 'import')) {
            mkdir(Mage::getBaseDir('var') . DS .'import', 0777, true);
        }

        return Mage::getBaseDir('var') . DS . 'import';
    }

    /**
     * Return path for xsd dir (if not exist it will create it)
     * @return string
     */
    protected function getImportValidationDir()
    {
        if (!is_dir(Mage::getBaseDir('var') . DS . 'xsd')) {
            mkdir(Mage::getBaseDir('var') . DS .'xsd', 0777, true);
        }

        return Mage::getBaseDir('var') . DS . 'xsd';
    }

    /**
     * Check running status for sandbox imports
     */
    protected function checkStatus()
    {
        if ($this->status) {
            $messages = Mage::helper("dyna_sandbox")->checkRunningImports();

            foreach ($messages as $message) {
                $this->writeLine($message);
            }

            //if count > 1, we have errors
            if (count($messages) > 1) {
                fwrite(STDERR, var_dump($messages));
                exit(1);
            }
        }
    }

    /**
     * Write message on screen
     * @param $message
     */
    protected function writeLine($message)
    {
        echo $message . PHP_EOL;
    }

    /**
     * Write message in logs
     * @param $message
     */
    protected function logLine($message)
    {
        Mage::log($message, null, $this->logFile);
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if ($verbose && !$this->debug) {
            return;
        }
        $this->_importHelper->setPathToImportFile($this->path);
        $this->_importHelper->logMsg($msg, false);
    }

    public function _logError($msg)
    {
        $this->_importHelper->setPathToImportFile($this->path);
        $this->_importHelper->logMsg($msg);
    }

    /**
     * Checks executionId argument, sets property and returns property if valid
     *
     * @return int|string
     * @internal param $import
     */
    protected function getExecutionId()
    {
        if (!isset($this->_executionId) && $this->executionId && is_numeric($this->executionId)) {
            $this->_executionId = $this->executionId;
        }

        return $this->_executionId;
    }

    /**
     * Check the import file encoding and converts it to UTF8 if needed
     */
    protected function checkEncoding()
    {
        $contents = file_get_contents($this->path);
        $bom      = pack("CCC", 0xef, 0xbb, 0xbf);
        $withBom = (0 === strncmp($contents, $bom, 3)) ? true : false;

        if (in_array(pathinfo($this->path, PATHINFO_EXTENSION), $this->validExtensions) &&
            (!mb_check_encoding($contents, 'UTF-8') || $withBom)) {
            $this->writeLine('********************************************');
            $this->writeLine('[WARNING] Import file is not encoded in UTF8.');


            if ($withBom) {
                $contents = substr($contents, 3);
                $this->writeLine('[WARNING] File also contained BOM which was removed');
            }

            $this->writeLine('********************************************');
            $this->writeLine('File "' . $this->path. '" will be converted to UTF8.');

            $contents = iconv(mb_detect_encoding($contents, mb_detect_order(), true), "UTF-8", $contents);

            file_put_contents($this->path . '_UTF8', $contents);
            $this->path = $this->path . '_UTF8';

            $this->isUTF8Converted = true;
            $this->writeLine('File converted to UTF8.');
        }
    }

    /**
     * Saves the import result to DB
     */
    protected function logImportedFile()
    {
        $pathFile = explode('/', $this->path);
        $fileName = array_pop($pathFile);
        /** @var Dyna_Import_Model_Resource_Summary_Collection $collection */
        $collection = Mage::getModel("import/summary")->getCollection();
        if (isset($this->_executionId)) {
            $collection = $collection->addFieldToFilter('execution_id', $this->_executionId);
        }
        if (!is_null($this->importId)) {
            $collection = $collection->addFieldToFilter('import_id', $this->importId)
                ->addFieldToFilter('filename', $fileName);
            $collection->getSelect()->order('import_id DESC');
            /** @var Dyna_Import_Model_Summary $model */
            $model = $collection->getFirstItem();
        } else {
            /** @var Dyna_Import_Model_Summary $model */
            $model = Mage::getModel("import/summary");
        }

        if ($model->getId()) {
            if ($this->_hasError) {
                $model->setHasErrors(1);
            } else {
                if ($this->_skippedFileRows > 0 || $this->_totalFileRows == 0) {
                    $model->setHasErrors(1);
                } else {
                    $model->setHasErrors(0);
                }
                $model->setHeadersValidated(($this->_totalFileRows > 0 && ($this->_skippedFileRows > 0 ? 0 : 1)))
                    ->setTotalRows($this->_totalFileRows)
                    ->setImportedRows($this->_totalFileRows - $this->_skippedFileRows)
                    ->setSkippedRows($this->_skippedFileRows)
                    ->setFinishedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()));

                if (isset($this->_executionId)) {
                    $model->setExecutionId($this->_executionId);
                }

                if (is_link($this->path)) {
                    $pathFile = explode('/', readlink($this->path));
                    $fileName = array_pop($pathFile);
                }
                if ($this->fullImport) {
                    $sourcePath = implode('/', array_merge($pathFile, [$fileName]));
                    if ($this->_totalFileRows > 0 && ($this->_skippedFileRows > 0 ? 0 : 1)) {
                        // processed
                        $targetPath = implode('/', array_merge($pathFile, ['Processed_' . $fileName]));
                        rename($sourcePath, $targetPath);
                    } else {
                        // failed
                        $targetPath = implode('/', array_merge($pathFile, ['Failed_' . $fileName]));
                        rename($sourcePath, $targetPath);
                    }
                }
            }
            $model->save();
            $this->importId = null;
            if (!$model->getHasErrors() && $model->getHeadersValidated()) {
                $this->outputExitCode(self::ERROR_CODE_SUCCESS);
            } else {
                $this->outputExitCode(self::ERROR_CODE_DATA);
            }
        } else {
            $model->setFilename($fileName)
                ->setCategory($this->_importLogCategory)
                ->setCreatedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()))
                ->setHasErrors($this->_hasError)
                ->setHeadersValidated(0)
                ->setTotalRows(0)
                ->setImportedRows(0)
                ->setSkippedRows(0);

            if (isset($this->_executionId)) {
                $model->setExecutionId($this->_executionId);
            }
            $model->save();
            $this->importId = $model->getId();
        }
    }

    protected function camelToSnakeCase(&$array)
    {
        $array['stack'] = $this->stack;
        foreach (array_keys($array) as $key) {
            $value = &$array[$key];
            unset($array[$key]);

            # This is what you actually want to do with your keys:
            #  - remove exclamation marks at the front
            #  - camelCase to snake_case
            $transformedKey = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', ltrim($key, '!')));

            # Work recursively
            if (is_array($value)) {
                $this->camelToSnakeCase($value);
            }

            # Store with new key
            $array[$transformedKey] = $value;

            # Do not forget to unset references!
            unset($value);
        }
    }

    /**
     * Determine if an array has numeric keys or not
     * @param $array
     * @return bool
     */
    protected function hasNumericKeys($array)
    {
        return is_numeric(current(array_keys($array))) ? true : false;
    }

    /**
     * Returns an array with xml rules
     * @param $xmlRows
     * @param $entity
     * @return array
     */
    protected function parseXmlRules($xmlRows, $entity): array
    {
        $xmlRules = array();

        // create an array with xml rules
        if (!isset($xmlRows->$entity[0])) {
            $xmlRules[] = $xmlRows->$entity;
        } else {
            foreach ($xmlRows->$entity as $item) {
                $xmlRules[] = $item;
            }
        }

        return $xmlRules;
    }

    protected function libxmlDisplayError($xmlError)
    {
        switch ($xmlError->level) {
            case LIBXML_ERR_WARNING:
                $return = "[Warning] $xmlError->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return = "[Error] $xmlError->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return = "[Fatal] $xmlError->code: ";
                break;
        }

        $return .= trim($xmlError->message);

        if ($xmlError->file) {
            $return .=    " in $xmlError->file";
        }

        $return .= " on line $xmlError->line";

        return $return;
    }

    public function setStack($stack) {
        return $this->stack = $stack;
    }

    /**
     * Truncate previous data
     * Set the table index to 1
     */
    protected function removePreviousData($model) {
        $resource = $model->getResource();
        $connection = $resource->getReadConnection();
        /* @see Varien_Db_Adapter_Pdo_Mysql */
        $connection->truncateTable($resource->getMainTable());
        $connection->changeTableAutoIncrement($resource->getMainTable(), 1);
    }

    protected function outputExitCode($code) {
        if (in_array($code, $this->SUPPORTED_ERROR_CODES)) {
            echo sprintf('ERROR CODE `%d`', $code) . "\n";
            exit($code);
        } else {
            echo 'Unsupported ERROR CODE ' . $code . '. Please use one of ' . implode(',', $this->SUPPORTED_ERROR_CODES) . "\n";
        }
    }
}
