<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class Dyna_Import_Model_Generator_Cable_Definition_Product extends Dyna_Import_Model_Generator_ProductDefinitionAbstract
{
    /** @var string */
    protected $attributeSet = 'KD_Cable_Products';

    /** @var array */
    protected $attributeSourceCache = [];

    /** @var array */
    protected $groupingNodes = array(
        'relations',
        'status',
        'properties',
        'display',
    );

    /** @var array */
    protected $mappedNodesToAttributes = array(
        'productId' => 'sku',
        'excludes' => 'relation_excludes',
        'upgrades' => 'relation_upgrades',
        'options' => 'relation_options',
        'preselectOptions' => 'relation_preselected_options',
        'inventoryPreselectedOptions' => 'relation_inv_pres_options',
        'mandatoryOptions' => 'relation_mandatory_options',
        'components' => 'relation_components',
        'devices' => 'relation_devices',
        'fees' => 'relation_fees',
        'minBound' => 'req_rate_min_bound',
        'maxBound' => 'req_rate_max_bound',
    );

    /** @var array */
    protected $productAttributes = [];

    /** @var array */
    protected $reservedDataValues = [
        '_attribute_set',
        '_entity_type',
        '_stock_item',
        '_websites',
    ];

    /** @var int */
    protected $status = 1;

    /** @var array */
    protected $transformNodes = array(
        'tariffChange',
        'price',
        'products',
        'serviceCategory',
        'selectable',
        'contractCode',
        'packageType',
        'packageSubtype',
        'premiumClass',
        'productSegment',
        'visibility',
        'productVisibility',
        'tags',
        'requiredBandwidth',
        'contractPeriod',
        'feePriority',
    );

    public function __construct()
    {
        parent::__construct();

        /* @var $eavConfig Mage_Eav_Model_Config */
        $eavConfig = Mage::getModel('eav/config');
        $this->productAttributes = $eavConfig->getEntityAttributeCodes(Mage_Catalog_Model_Product::ENTITY,
            Mage::getModel('catalog/product'));
    }

    /**
     * @param string $code
     * @return bool
     */
    protected function attributeUsesSource(string $code)
    {
        if (!isset($this->attributeSourceCache[$code])) {
            /** @var Mage_Eav_Model_Entity_Attribute $entityAttributeModel */
            $entityAttributeModel = Mage::getModel('eav/entity_attribute');
            $attributeModel = $entityAttributeModel->loadByCode(Mage_Catalog_Model_Product::ENTITY, $code);
            if($attributeModel) {
                $this->attributeSourceCache[$code] = $attributeModel->usesSource();
            } else {
                $this->attributeSourceCache[$code] = false;
            }
        }

        return $this->attributeSourceCache[$code];
    }

    /**
     * @param $data
     * @return bool
     */
    protected function hasNumericKeys($data)
    {
        $numeric = true;

        foreach ($data as $key => $value) {
            if (!is_numeric($key)) {
                return false;
            }
        }

        return $numeric;
    }

    /**
     * @param array $values
     */
    protected function parseProductVisibility(array $values)
    {
        $this->data['product_visibility'] = array_keys($values);
    }

    protected function setAttributeSet()
    {
        $this->data['_attribute_set'] = $this->attributeSet;
    }

    /**
     * @param string $value
     */
    protected function setContractCode(string $value)
    {
        $attribute = 'contract_code';
        $value = strtoupper($value);

        if ($this->validateValue($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$durationOptions)) {
            $this->data[$attribute] = $value;
        }
    }

    /**
     * @param $value
     */
    protected function setContractPeriod($value)
    {
        $value = (string)$value;

        $this->data['contract_period'] = $value;
    }

    /**
     * @param array|string $data
     * @return bool|string
     */
    public function setData($data)
    {
        if (!is_array($data)) {
            return $data;
        }

        if ($this->hasNumericKeys($data)) {
            return implode(',', $data);
        }

        foreach ($data as $key => $value) {
            $newValue = false;

            if (in_array($key, $this->transformNodes)) {
                $this->transformNode($key, $value);
            } else {
                $newValue = $this->setData($value);
            }

            if ($newValue) {
                $assigned = false;

                if (in_array($key, $this->groupingNodes)) {
                    $this->logMessage('(SKU ' . $this->data['sku'] . ') Received value <' . $newValue . '> for a grouping node <' . $key . '>. Trying to map this key to an attribute. This grouping key should be changed.');
                }

                if (array_key_exists($key, $this->mappedNodesToAttributes)) {
                    $this->data[$this->mappedNodesToAttributes[$key]] = $newValue;
                    $assigned = true;
                }

                if (!$assigned && in_array($key, $this->productAttributes)) {
                    $this->data[$key] = $value;
                    $assigned = true;
                }

                if (!$assigned && in_array($this->importHelper->replaceUpper($key), $this->productAttributes)) {
                    $this->data[$this->importHelper->replaceUpper($key)] = $newValue;
                    $assigned = true;
                }

                if (!$assigned) {
                    $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipped adding value <' . $newValue . '> because of unknown attribute <' . $key . '>.');
                }
            }
        }

        return false;
    }

    protected function setFeePriority($value)
    {
        $value = (string)$value;

        $this->data['fee_priority'] = $value;
    }

    /**
     * @param string $value
     */
    protected function setPackageType(string $value)
    {
        $attribute = 'package_type';
        $value = trim($value);

        if ($this->validateValue($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::packageTypeOptions(false))) {
            $this->data[$attribute] = [$value];
        }
    }

    /**
     * @param string $value
     */
    protected function setPackageSubtype(string $value)
    {
        $attribute = 'package_subtype';
        $value = trim($value);

        if ($this->validateValue($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::packageSubtypeOptions())) {
            $this->data[$attribute] = $value;
        }
    }

    /**
     * @param string $value
     */
    protected function setPremiumClass(string $value)
    {
        $attribute = 'premium_class';
        $value = ucfirst(strtolower(trim($value)));

        if ($this->validateValue($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$hintsPremiumClassOptions)) {
            $this->data[$attribute] = $value;
        }
    }

    /**
     * @param array $value
     */
    protected function setPrice(array $value)
    {
        $priceSliding = '';
        $priceRepeated = '';
        $priceOnce = 0;
        $vatClass = '';

        foreach ($value as $price) {
            if (!empty($price['billingFrequency'])) {
                $this->setPriceBillingFrequency($price['billingFrequency']);

                $billingFrequency = strtolower(trim($price['billingFrequency']));
                if ($billingFrequency == 'monthly') {
                    $priceRepeated .= !empty($priceRepeated) ? ':' . $price['amount'] : $price['amount'];
                    $period = !empty($price['periodStart']) && !empty($price['periodEnd']) ? $price['periodEnd'] - $price['periodStart'] + 1 : '';
                    $priceSliding .= !empty($priceSliding) ? ':' . $period : $period;
                } elseif ($billingFrequency == 'once') {
                    $priceOnce = $price['amount'];
                } else {
                    $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipping price value: invalid billing frequency <' . $price['billingFrequency'] . '>');
                }
            } else {
                $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipping price value: no billing frequency set.');
            }

            if (!empty($price['includedVat'])) {
                $vatValue = (int)str_replace('%', '', $price['includedVat']);
                $vatClass = $this->getVatTaxClass($vatValue);
            }
        }

        $this->data['price_sliding'] = $priceSliding;
        $this->data['price_repeated'] = $priceRepeated;

        if ($priceOnce) {
            $this->data['price'] = $this->importHelper->formatPrice($priceOnce);
        } else {
            $this->data['price'] = 0;
            $this->logMessage('(SKU ' . $this->data['sku'] . ') No price_once received, setting price 0.');
        }

        $this->data['tax_class_id'] = $vatClass;
    }

    /**
     * @param string $value
     */
    protected function setPriceBillingFrequency(string $value)
    {
        $attribute = 'price_billing_frequency';

        if (!$this->attributeUsesSource($attribute)) {
            $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipping attribute <' . $attribute . '> because it is not a select or multiselect type.');
        }

        $this->data[$attribute] = $value;
    }

    protected function setProductCodes()
    {
        $parts = explode(':', $this->data['sku']);

        $this->data['product_code'] = $parts[0];

        if (isset($parts[1])) {
            $this->data['promotion_code'] = $parts[1];
        }
    }

    /**
     * @param string $value
     */
    protected function setProductSegment(string $value)
    {
        $attribute = 'product_segment';
        $value = ucfirst(strtolower(trim($value)));

        if ($this->validateValue($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$productSegmentOptions)) {
            $this->data[$attribute] = $value;
        }
    }

    protected function setProductVisibility()
    {
        $productVisibilityOptions = [];
        $productVisibilityAttributeOptions = $this->getAttributeOptionsByCode(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);
        foreach($productVisibilityAttributeOptions as $productVisibilityAttributeOption) {
            $productVisibilityOptions[$productVisibilityAttributeOption] = strtolower(
                str_replace(' ', '', $productVisibilityAttributeOption)
            );
        }

        $productVisibilitySelection = [];
        foreach ($this->data['product_visibility'] as $productVisibility) {
            if (in_array(strtolower($productVisibility), $productVisibilityOptions)) {
                $productVisibilitySelection[] = array_search(strtolower($productVisibility), $productVisibilityOptions);
            }
        }

        $this->data['product_visibility'] = $productVisibilitySelection;
    }

    protected function setRequiredProducts($value)
    {
        /** @var Omnius_Checkout_Helper_Core $coreHelper */
        $coreHelper = Mage::helper('core');
        $value = $coreHelper->jsonEncode($value);

        $this->data['req_rate_prod_for_contracts'] = $value;
    }

    /**
     * @param $value
     */
    protected function setRequiredBandwidth($value)
    {
        $value = (string)$value;

        $this->data['required_bandwidth'] = $value;
    }

    /**
     * @param bool $value
     */
    protected function setSelectable($value)
    {
        $attribute = 'selectable';
        $portalStateInitialOptions = Dyna_Cable_Model_Product_Attribute_Option::$portalStateInitialOptions;

        if (!is_bool($value)) {
            $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipping attribute <selectable> because it contains the value <' . $value . '> other than true or false.');
        }

        if (!$this->attributeUsesSource($attribute)) {
            $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipping attribute <' . $attribute . '> because it is not a select or multiselect type.');
        }

        if ($value) {
            $this->data[$attribute] = $portalStateInitialOptions[0];
        } else {
            $this->data[$attribute] = $portalStateInitialOptions[1];
        }
    }

    /**
     * @param string $value
     */
    protected function setServiceCategory(string $value)
    {
        $attribute = 'service_category';
        $value = strtoupper($value);

        if ($this->validateValue($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$serviceCategoryOptions)) {
            $this->data[$attribute] = $value;
        }
    }

    protected function setSku()
    {
        if(!$this->data['sku'] && $this->data['productId']) {
            $this->data['sku'] = trim($this->data['productId']);
        }
    }

    protected function setStatus()
    {
        $this->data['status'] = $this->status;
    }

    /**
     * @param $value
     */
    protected function setTags($value)
    {
        $this->data['tags'] = implode(',', $value);
    }

    protected function setType()
    {
//        $this->data['type'] = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE; // don't use this, bug in _setDefaults (Dyna_Cable_Model_Import_CableProductJsonFile)
    }

    /**
     * @param array $value
     */
    protected function setTariffChangeAction(array $value)
    {
        $action = strtoupper(trim($value['action']));

        if (($action == 'REPLACE' && !empty($value['replaceBy'])) || $value['action'] == 'KEEP') {
            $this->data['tariff_change_action'] = $action;
        } else {
            $this->logMessage('(SKU ' . $this->data['sku'] . ') do not know what to do with <tariffChange>, dumping array to logs:');
            $this->logMessage($value);
        }
    }

    /**
     * @param array $data
     * @return ISAAC_Import_Model_Import_Value_Catalog_Product
     */
    public function transform(array $data)
    {
        $this->setData($data);

        // set data
        $this->setAttributeSet();
        $this->setWebsites();
        $this->setStatus();
        $this->setSku();
        $this->setCheckoutProduct();

        // set defaults
        $this->setType();
        $this->setUrlKey();
        $this->setProductCodes();
        $this->setProductVisibility();
        $this->setProductVisibilityStrategy();
        $this->setVisibility();
        $this->setIsDeleted();
        $this->setStock();

        return $this->createCatalogProduct();
    }

    /**
     * @param $attribute
     * @param $value
     */
    protected function transformNode($attribute, $value)
    {
        switch ($attribute) {
            case 'contractCode':
                $this->setContractCode($value);
                break;
            case 'contractPeriod':
                $this->setContractPeriod($value);
                break;
            case 'feePriority':
                $this->setFeePriority($value);
                break;
            case 'packageType':
                $this->setPackageType($value);
                break;
            case 'packageSubtype':
                $this->setPackageSubtype($value);
                break;
            case 'premiumClass':
                $this->setPremiumClass($value);
                break;
            case 'price':
                $this->setPrice($value);
                break;
            case 'productSegment':
                $this->setProductSegment($value);
                break;
            case 'products':
                $this->setRequiredProducts($value);
                break;
            case 'requiredBandwidth':
                $this->setRequiredBandwidth($value);
                break;
            case 'selectable':
                $this->setSelectable($value);
                break;
            case 'serviceCategory':
                $this->setServiceCategory($value);
                break;
            case 'tags':
                $this->setTags($value);
                break;
            case 'tariffChange':
                $this->setTariffChangeAction($value);
                break;

            // @todo: this won't work, see changes in https://bitbucket.org/dynalean/dynacommerce-omnius-vodafone-germany/commits/25a1ff4
            case 'visibility':
            case 'product_visibility':
            case 'productVisibility':
                $this->parseProductVisibility($value);
                break;

            default:
                $this->logMessage('(SKU ' . $this->data['sku'] . ') special case defined for node <' . $attribute . '> but not treated in parseSpecialCase method.');
                break;
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    public function validate(array $data)
    {
        if (empty($data['packageType']) || !in_array(strtolower($data['packageType']),
                Dyna_Cable_Model_Product_Attribute_Option::packageTypeOptions())
        ) {
            $this->logMessage('(SKU ' . $data['sku'] . ') Skipped entire product because of invalid package type value: ' . $data['packageType'] . ' (case insensitive)');
            return false;
        }

        if (empty($data['packageSubtype']) || !in_array($data['packageSubtype'],
                Dyna_Cable_Model_Product_Attribute_Option::packageSubtypeOptions())
        ) {
            $this->logMessage('(SKU ' . $data['sku'] . ') Skipped entire product because of invalid package subtype value: ' . $data['packageSubtype'] . ' (case insensitive)');
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param array $allowedOptions
     * @return bool
     */
    protected function validateValue(string $attribute, string $value, array $allowedOptions)
    {
        if (!in_array($value, $allowedOptions)) {
            $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipping attribute <' . $attribute . '> because value <' . $value . '> it is not in the defined list of values.');
            return false;
        }

        if (!$this->attributeUsesSource($attribute)) {
            $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipping attribute <' . $attribute . '> because it is not a select or multiselect type.');
            return false;
        }

        return true;
    }
}