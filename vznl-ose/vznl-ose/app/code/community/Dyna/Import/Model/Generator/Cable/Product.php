<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class Dyna_Import_Model_Generator_Cable_Product implements ISAAC_Import_Generator
{
    /** @var bool */
    protected $canExecute;

    /** @var Dyna_Import_Model_Generator_Cable_Definition_Product */
    protected $definitionModel;

    /** @var Dyna_Import_Helper_File */
    protected $fileHelper;

    /** @var string */
    protected $importFile = 'Cable_Products.json';

    /** @var ISAAC_Import_Helper_Data */
    protected $importHelper;

    /**
     * Dyna_Import_Model_Generator_Product constructor.
     */
    public function __construct()
    {
        $this->fileHelper = Mage::helper('dyna_import/file');
        $this->importHelper = Mage::helper('isaac_import');
        $this->importFile = $this->fileHelper->getFilePath($this->importFile);

        $this->canExecute = $this->canExecute();

        /** @var Dyna_Import_Model_Generator_Cable_Definition_Product */
        $this->definitionModel = Mage::getModel('dyna_import/generator_cable_definition_product');
    }

    /**
     * @return bool
     */
    public function canExecute()
    {
        if (!file_exists($this->importFile)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        if (!$this->canExecute) {
            return new ArrayIterator();
        }

        $fileContents = file_get_contents($this->importFile);
        $jsonDictReader = new RecursiveArrayIterator(
            new RecursiveArrayIterator(json_decode($fileContents, true)),
            RecursiveIteratorIterator::SELF_FIRST
        );

        $productFilterIterator = new CallbackFilterIterator($jsonDictReader, function ($data) {
            return $this->definitionModel->validate($data);
        });

        return new ISAAC_Import_Map_Iterator($productFilterIterator, function ($data) {
            return $this->definitionModel->transform($data);
        });
    }

    public function cleanup()
    {
    }
}