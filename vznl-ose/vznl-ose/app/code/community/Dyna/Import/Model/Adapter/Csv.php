<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Import_Model_Adapter_Csv
 */
class Dyna_Import_Model_Adapter_Csv extends Omnius_Import_Model_Adapter_Csv
{
    /**
     * Field delimiter.
     *
     * @var string
     */
    protected $_delimiter = ';';
}
