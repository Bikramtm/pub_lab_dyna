<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class Dyna_Import_Model_Generator_Cable_CategoryProducts extends Dyna_Import_Model_Generator_CategoryProductsAbstract
{
    /** @var string */
    protected $importFile = 'Cable_Categories.csv';
}