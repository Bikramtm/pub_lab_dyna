<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class Dyna_Import_Model_Generator_Mobile_CategoryFootnote extends Dyna_Import_Model_Generator_Mobile_CategoryAbstract
{
    /** @var string */
    protected $importFile = 'Mobile_categories_footnotes.csv';

    public function __construct()
    {
        parent::__construct();

        /** @var Dyna_Import_Model_Generator_Mobile_Definition_Category */
        $this->definitionModel = Mage::getModel('dyna_import/generator_mobile_definition_category');
    }
}