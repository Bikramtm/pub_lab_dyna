<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class Dyna_Import_Model_Generator_ProductDefinitionAbstract extends Dyna_Import_Model_Generator_DefinitionAbstract
{
    /** @var string */
    const ENTITY_IDENTIFIER = 'sku';

    /** @var array $attributes */
    protected $attributes = [];

    /** @var array $attributeOptions */
    protected $attributeOptions = [];

    /** @var array $_attributeTypes */
    protected $attributeTypes = [];

    /** @var array */
    protected $bbParseFields = [];

    /** @var array */
    protected $dateTimeFields = [];

    /** @var string */
    protected $entity_type = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE;

    /** @var int */
    protected $is_deleted = 0;

    /** @var Dyna_Catalog_Model_Product */
    protected $productModel;

    /** @var string */
    protected $productVisibilityStrategy = 'all';

    /** @var array */
    protected $renameFields = [];

    /** @var array */
    protected $reservedAttributeCodes = [];

    /** @var array */
    protected $reservedDataValues = [];

    /** @var array */
    protected $vatTaxClasses = [];

    /** @var int */
    protected $visibility = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;

    /** @var array */
    protected $websites = [
        'telesales'
    ];

    /**
     * @param string $code
     * @param string $value
     * @return null|int
     */
    protected function addAttributeOption(string $code, string $value)
    {
        if (empty($value)) {
            return null;
        }

        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_write');
        $optionTable = $resource->getTableName('eav/attribute_option');
        $optionValueTable = $resource->getTableName('eav/attribute_option_value');

        $connection->insert($optionTable, [
            'attribute_id' => $this->getAttributeIdByCode($code),
            'sort_order' => 0
        ]);
        $optionId = $connection->lastInsertId($optionTable);

        $connection->insert($optionValueTable, [
            'option_id' => $optionId,
            'store_id' => 0,
            'value' => $value
        ]);

        $this->attributeOptions[$code][strtolower(trim($value))] = $value;

        return $optionId;
    }

    protected function bbParseFields()
    {
        if ($this->bbParseFields) {
            /** @var Dyna_Mobile_Model_Import_BBParser $bbParser */
            $bbParser = Mage::getModel('dyna_mobile/import_BBParser');
            foreach ($this->bbParseFields as $bbParseField) {
                if (!empty($this->data[$bbParseField])) {
                    $bbParser->setText($this->data[$bbParseField]);
                    $this->data[$bbParseField] = $bbParser->parseText();
                }
            }
        }
    }

    /**
     * @return ISAAC_Import_Model_Import_Value_Catalog_Product
     */
    public function createCatalogProduct()
    {
        // load data into import model
        $importValue = new ISAAC_Import_Model_Import_Value_Catalog_Product($this->data[self::ENTITY_IDENTIFIER]);
        $importValue->setLoadingFlags([
            ISAAC_Import_Model_Import_Value_Catalog_Product::UPDATE,
            ISAAC_Import_Model_Import_Value_Catalog_Product::CREATE,
        ]);

        $importValue->addDataValues($this->data);

        return $importValue;
    }

    /**
     * @param string $code
     * @return mixed
     */
    protected function getAttributeByCode(string $code)
    {
        if (!isset($this->attributes[$code])) {
            $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $code);
            $this->attributes[$code] = $attribute;
        }

        return $this->attributes[$code];
    }

    /**
     * @param string $code
     * @return int
     */
    protected function getAttributeIdByCode(string $code)
    {
        return $this->getAttributeByCode($code)->getId();
    }

    /**
     * @param string $code
     * @param string $value
     * @return string
     */
    protected function getAttributeOptionByCode(string $code, string $value)
    {
        $attributeOptions = $this->getAttributeOptionsByCode($code);
        $lookupValue = strtolower(trim($value));

        if(array_key_exists($lookupValue, $attributeOptions)) {
            $value = $attributeOptions[$lookupValue];
        } else {
            $this->addAttributeOption($code, $value);
        }

        return $value;
    }

    /**
     * @param string $code
     * @return array
     */
    protected function getAttributeOptionsByCode(string $code)
    {
        if(!isset($this->attributeOptions[$code])) {
            $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $code);
            $options = $attribute->getSource()->getAllOptions(false);
            $attributeOptions = [];
            foreach ($options as $option) {
                $attributeOptions[strtolower(trim($option['label']))] = $option['label'];
            }

            $this->attributeOptions[$code] = $attributeOptions;
        }

        return $this->attributeOptions[$code];
    }

    /**
     * @return Dyna_Catalog_Model_Product|int
     */
    protected function getProductModel()
    {
        if(!isset($this->productModel)) {
            /** @var Dyna_Catalog_Model_Product $productModel */
            $productModel = Mage::getModel('catalog/product');
            $this->productModel = $productModel->getIdBySku($this->data['sku']);
        }

        return $this->productModel;
    }

    /**
     * @param $vatValue
     * @return string
     */
    protected function getVatTaxClass($vatValue)
    {
        if (!isset($this->vatTaxClasses[$vatValue])) {
            /** @var Dyna_Catalog_Helper_Data $catalogHelper */
            $catalogHelper = Mage::helper('dyna_catalog');
            $this->vatTaxClasses[$vatValue] = $catalogHelper->getVatTaxClass($vatValue);
        }

        return $this->vatTaxClasses[$vatValue];
    }

    protected function renameFields()
    {
        foreach ($this->renameFields as $new => $old) {
            if (empty($this->data[$new]) && !empty($this->data[$old])) {
                $this->data[$new] = $this->data[$old];
                unset($this->data[$old]);
            }
        }
    }

    protected function setAttributeTypeValues()
    {
        foreach($this->data as $code => $value)
        {
            $value = $this->setAttributeTypeValueByCode($code, $value);
            if(is_null($value)) {
                unset($this->data[$code]);
            } else {
                $this->data[$code] = $value;
            }
        }
    }

    /**
     * @param string $code
     * @param string|array $value
     * @return bool|string
     */
    protected function setAttributeTypeValueByCode(string $code, $value)
    {
        if(in_array($code, $this->reservedDataValues)) {
            return $value;
        }

        if (!array_key_exists($code, $this->attributeTypes)) {
            /** @var Mage_Eav_Model_Entity_Attribute $eavEntityAttributeModel */
            $eavEntityAttributeModel = Mage::getModel('eav/entity_attribute');
            $attribute = $eavEntityAttributeModel->loadByCode('catalog_product', $code);
            if (!$attribute || !$attribute->getId()) {
                $this->logMessage('Unknown attribute, attribute_code specified: ' . $code);
                return null;
            }
            $this->attributeTypes[$code] = $attribute->getFrontendInput();
        }

        switch ($this->attributeTypes[$code]) {
            case 'text':
            case 'textarea':
            case 'date':
            case 'datetime':
            case 'price':
                $value = $this->setTextData($value);
                break;
            case 'boolean':
                $value = $this->setBooleanData($value);
                break;
            case 'multiselect':
                $value = $this->setMultiSelectData($code, $value);
                break;
            case 'select':
                $value = $this->setSelectData($code, $value);
                break;
            default:
                return false;
        }

        return $value;
    }

    protected function setDateTimeFields()
    {
        foreach ($this->dateTimeFields as $dateTimeField) {
            if (!empty($this->data[$dateTimeField])) {
                $this->data[$dateTimeField] = $this->importHelper->formatDate($this->data[$dateTimeField]);
            }
        }
    }

    /**
     * @param string $code
     * @param string $value
     * @return string
     */
    protected function setSelectData(string $code, string $value)
    {
        return $this->getAttributeOptionByCode($code, $value);
    }

    protected function setCheckoutProduct()
    {
        if(empty($this->data['checkout_product'])) {
            $this->data['checkout_product'] = false;
        }
    }

    protected function setIsDeleted()
    {
        $this->data['is_deleted'] = $this->is_deleted;
    }

    /**
     * @param string $code
     * @param string $data
     * @return array|null
     */
    protected function setMultiSelectData(string $code, string $data)
    {
        $values = explode(',', $data);
        if (empty($values)) {
            return null;
        }

        $attributeOptions = [];
        foreach ($values as $value) {
            $attributeOptions[] = $this->getAttributeOptionByCode($code, $value);
        }

        return $attributeOptions;
    }

    protected function setProductVisibility()
    {
        if ($this->data['product_visibility'] == '*') {
            /** @var Mage_Eav_Model_Config $eavConfig */
            $eavConfig = Mage::getSingleton('eav/config');
            $attribute = $eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY,
                Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);
            if ($attribute->usesSource()) {
                $options = $attribute->getSource()->getAllOptions(false);
                $this->data['product_visibility'] = implode(',', array_column($options, 'label'));
            }
        }
    }

    protected function setProductVisibilityStrategy()
    {
//        $this->data['product_visibility_strategy'] = $this->productVisibilityStrategy; // attribute doesn't exist
    }

    protected function setStock()
    {
        $this->data['_stock_item'] = [
            'use_config_manage_stock' => true,
            'use_config_min_sale_qty' => true,
            'use_config_max_sale_qty' => true,
            'use_config_enable_qty_increments' => true,
            'is_in_stock' => false,
            'qty' => 0,
            'manage_stock' => 0,
        ];
    }

    /**
     * @param string $value
     * @return string
     */
    protected function setTextData(string $value)
    {
        return trim($value);
    }

    protected function setType()
    {
        $this->data['_entity_type'] = $this->entity_type;
    }

    protected function setUrlKey()
    {
        /** @var Mage_Catalog_Model_Product_Url $productUrlModel */
        $productUrlModel = Mage::getModel('catalog/product_url');
        $this->data['url_key'] = $productUrlModel->formatUrlKey($this->data['name']);
    }

    protected function setVisibility()
    {
        $this->data['visibility'] = $this->visibility;
    }

    protected function setWebsites()
    {
        $this->data['_websites'] = $this->websites;
    }

    protected function unsetReservedAttributeCodes()
    {
        if($this->reservedAttributeCodes) {
            foreach($this->reservedAttributeCodes as $attributeCode) {
                unset($this->data[$attributeCode]);
            }
        }
    }
}