<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class Dyna_Import_Model_Generator_Fixed_ProductSalespackage extends Dyna_Import_Model_Generator_Fixed_ProductAbstract implements ISAAC_Import_Generator
{
    /** @var string */
    protected $importFile = 'FN_SalesPackage.csv';

    public function __construct()
    {
        parent::__construct();

        /** @var Dyna_Import_Model_Generator_Fixed_Definition_ProductSalespackage */
        $this->definitionModel = Mage::getModel('dyna_import/generator_fixed_definition_productSalespackage');
    }
}