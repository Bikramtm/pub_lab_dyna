<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once Mage::getModuleDir('controllers', 'Mage_Index') . DS . 'Adminhtml' . DS . 'ProcessController.php';

class Dyna_Adminhtml_Adminhtml_ProcessController extends Mage_Index_Adminhtml_ProcessController
{

    public function listAction()
    {
        $this->_getSession()->addNotice(
            Mage::helper('index')->__('Please note that the grid refreshes at a set interval to update the statuses of the indexers.')
        );
        parent::listAction();
    }

    /**
     * Reindex all data what process is responsible
     */
    public function reindexProcessAction()
    {
        /** @var $process Mage_Index_Model_Process */
        $process = $this->_initProcess();
        if ($process) {
            try {
                $this->runProcessIndexer($process);
                $this->_getSession()->addSuccess(
                    Mage::helper('index')->__('%s index was started.', $process->getIndexer()->getName())
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addException($e, Mage::helper('index')->__('There was a problem with reindexing process.'));
            }
        } else {
            $this->_getSession()->addError(
                Mage::helper('index')->__('Cannot initialize the indexer process.')
            );
        }

        $this->_redirect('*/*/list');
    }

    /**
     * Mass rebuild selected processes index
     *
     */
    public function massReindexAction()
    {
        /* @var $indexer Mage_Index_Model_Indexer */
        $indexer = Mage::getSingleton('index/indexer');
        $processIds = $this->getRequest()->getParam('process');
        try {
            if (empty($processIds) || !is_array($processIds)) {
                throw new Mage_Core_Exception(Mage::helper('index')->__('Please select Indexes'));
            }
            $counter = 0;
            foreach ($processIds as $processId) {
                /* @var $process Mage_Index_Model_Process */
                $process = $indexer->getProcessById($processId);
                if ($process && $process->getIndexer()->isVisible()) {
                    try {
                        $this->runProcessIndexer($process);
                        $counter++;
                    } catch (Exception $e) {
                        $this->_getSession()->addError($e->getMessage());
                    }
                }
            }
            $this->_getSession()->addSuccess(Mage::helper('index')->__('Total of %d index(es) have been started.', $counter));
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException($e, Mage::helper('index')->__('Cannot initialize the indexer process.'));
        }

        $this->_redirect('*/*/list');
    }

    /**
     * @param $process Mage_Index_Model_Process
     * @throws Mage_Core_Exception
     */
    protected function runProcessIndexer($process)
    {
        if ($process->isLocked()) {
            Mage::throwException(Mage::helper('index')->__('%s Index process is working now. Please try run this process later.',
                $process->getIndexer()->getName()));
        }
        $command = sprintf(
            'nohup nice php -d memory_limit=256M %s --reindex %s> /dev/null 2>&1 &',
            'shell/indexer.php',
            $process->getIndexerCode()
        );

        @exec($command);
    }

    public function checkIndexerAction()
    {
        return $this->getResponse()->setBody(
            $this->getLayout()->createBlock('index/adminhtml_process')->toHtml()
        );
    }
}
