<?php


class Dyna_PriceRules_Model_Condition_RedSalesId extends Omnius_PriceRules_Model_Condition_Abstract
{

    /**
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setupTextCondition('Red sales id', 'red_sales_id');
        return $this;
    }

    /**
     * Returns the customer session.
     * @return mixed
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Validates the condition.
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        $redSalesId = $quote->getRedSalesId();
        return $this->validateAttribute($redSalesId);
    }

    /**
     * @return string
     */
    public function getInputType()
    {
        return 'string';
    }

}