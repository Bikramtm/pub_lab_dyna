<?php

/**
 * Class Dyna_PriceRules_Model_Condition_ProcessContext
 */
class Dyna_PriceRules_Model_Condition_ProcessContext extends Omnius_PriceRules_Model_Condition_Abstract
{
    public function loadAttributeOptions()
    {
        $this->setupSelectCondition('Process context', 'process_context');
        return $this;
    }

    public function getValueSelectOptions()
    {
        /** @var Dyna_PriceRules_Model_Eav_Entity_Attribute_Source_ProcessContext $optionsModel */
        $optionsModel = Mage::getModel('dyna_pricerules/eav_entity_attribute_source_processContext');
        $options = $optionsModel->getAllOptions();

        return $this->setupOptions($options);
    }

    public function validate(Varien_Object $object)
    {
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $contextId = $processContextHelper->getProcessContextId();
        if ($contextId !== null) {
            return $this->validateAttribute($contextId);
        }
        return false;
    }

    /**
     * Returns types of condition operators.
     * @return array
     */
    public function getOperatorSelectOptions()
    {
        $options = array(
            '()' => Mage::helper('rule')->__('is one of'),
            '!()' => Mage::helper('rule')->__('is not one of')
        );

        $operatorOptions = array();
        foreach ($options as $value => $label) {
            $operatorOptions[] = array(
                'value' => $value,
                'label' => $label,
            );
        }

        return $operatorOptions;
    }

    /**
     * @return string
     */
    public function getInputType()
    {
        return 'multiselect';
    }

    /**
     * @return string
     */
    public function getValueElementType()
    {
        return 'multiselect';
    }
}
