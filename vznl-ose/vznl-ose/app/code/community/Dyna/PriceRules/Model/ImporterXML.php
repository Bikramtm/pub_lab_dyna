<?php

class Dyna_PriceRules_Model_ImporterXML extends Dyna_Import_Model_Adapter_Xml
{
    protected $data = [];
    protected $line;
    protected $lineNo = 0;
    protected $_websites;
    protected $conditionIndex = 0;
    protected $categoryPathAttributes = [];
    protected $_packageTypes;

    const ALL_WEBSITES = "*";
    const ALL_PACKAGE_TYPES = "*";
    const ATTR_PRODUCT = "Product";
    const ATTR_CATEGORY = "Category";
    const PARAM_PRODUCT_SKU = 'Sku';
    const PARAM_CATEGORY = "Category";
    const PARAM_CATEGORY_ID = "categoryID";
    const PARAM_CATEGORY_PATH = "categoryPath";
    const OPERATOR_CONDITIONS = array(
        "is" => "==",
        "is not" => "!=", "is" => "==",
        "is not" => "!=",
        "contains" => "{}",
        "does not contain" => "!{}",
        "is one of" => "()",
        "is not one of" => "!()"
    );
    const ACTION_TYPES = array(
        'process_add_promo' => 'ampromo_items',
        'process_remove_promo' => 'ampromo_remove_items',
        'process_replace_promo' => 'ampromo_replace_items',
        'process_add_product' => 'process_add_product',
        'process_remove_product' => 'process_remove_product',
        'process_replace_product' => 'process_replace_product',
        'sales_hint' => 'sales_hint',
    );

    /**
     * Dyna_PriceRules_Model_ImporterXML constructor.
     * @param $args
     */
    public function __construct($args)
    {
        $this->logFile = 'import_sales_rules.log';
        $this->setDefaultWebsites();
        $this->setPackageTypes();
        $this->categoryPathAttributes = [
            strtolower(self::PARAM_CATEGORY_PATH),
            strtolower(self::PARAM_CATEGORY)
        ];
        parent::__construct($args);
    }

    public function import()
    {
        // retrieve data
        $xmlRows = $this->getXmlData(true, false);

        // parse each xml rule
        foreach ($xmlRows->SalesRule as $xmlRule) {
            $this->data = [];
            $this->line = $xmlRule;
            $this->conditionIndex = 1;
            $this->lineNo++;

            // get rule type
            $sourceType = strtolower(trim((string)$this->line->SourceType));

            // get rule data
            $name = trim((string)$this->line->Name);

            // Skip row is service source expression is invalid
            try {
                // if it's not a Product/Category rule
                if ($sourceType == strtolower(Dyna_ProductMatchRule_Helper_Data::SOURCE_TYPE_SERVICE) || !$sourceType) {
                    /** @var Dyna_Import_Model_ExpressionLanguage_Source_Conditions $condition */
                    $condition = Mage::getModel('dyna_import/expressionLanguage_source_conditions');
                    $sourceConditions = $condition->parseConditions($this->line->SourceConditions, 'Source');
                } else {
                    $sourceConditions = false;
                }
            } catch (Exception $exception) {
                $sourceConditions = false;
                $this->_skippedFileRows++;
                $this->_logError("[ERROR] Skipped row because: Invalid source condition for ".$name.".");
                continue;
            }

            // search if current rule already exists
            $existing = Mage::getModel('salesrule/rule')->load($name, 'name');
            $this->_log(($existing->getId() ? 'Updating' : 'Saving') . 'rule: ' . $name);

            // try to save the rule
            try {
                // get needed data from XML
                $action = $this->parseAction();
                $isActive = (empty($this->line->IsActive) || strtolower((string)$this->line->IsActive) == 'true' ? 1 : 0);
                $sortOrder = isset($this->line->Priority) ? trim((string)$this->line->Priority) : null;

                $showHint = (trim(strtolower((string)$this->line->ShowHintOnce)) === "true") ?
                    Dyna_PriceRules_Model_Validator::HINT_ONCE :
                    ((trim(strtolower((string)$this->line->ShowHintOnce)) === "false") ?
                        Dyna_PriceRules_Model_Validator::HINT_ALWAYS :
                        Dyna_PriceRules_Model_Validator::HINT_NONE);

                /** process basic data like name, start date, end date etc */
                $this->data = array(
                    'name' => $name,
                    'origin' => trim((string)$this->line->Origin),
                    'identifier' => trim((string)$this->line->Identifier),
                    'rule_id' => $existing->getId() ?: null,
                    'to_date' => $this->formatDate((string)$this->line->ExpirationDate),
                    'show_hint' => $showHint,
                    'from_date' => $this->formatDate((string)$this->line->EffectiveDate),
                    'is_active' => $isActive,
                    'sort_order' => $sortOrder,
                    'website_ids' => $this->setWebsitesIds($this->line->Channels),
                    'simple_action' => $action['action'],
                    'promo_sku' => $action['sku'],
                    'promo_sku_replace' => $action['replace_sku'],
                    'sales_hint_message' => $action['hint_message'],
                    'service_source_expression' => (($sourceConditions) ? $sourceConditions->asString() : '')
                );

                /** fixed sales rules login */
                if (!empty($this->line->UsageDiscount)) {
                    $this->data['usage_discount'] = str_replace(["-", ","], ["", "."], (string)$this->line->UsageDiscount);
                }

                if (!empty($this->line->UsageDiscountPercent)) {
                    $this->data['usage_discount_percent'] = str_replace(["-", ","], ["", "."], (string)$this->line->UsageDiscountPercent);
                }

                // Is promo rule
                if (!empty($this->line->IsPromo)) {
                    $this->data['is_promo'] = $this->setIsPromo((string)$this->line->IsPromo);
                }

                if (!empty($this->line->DiscountPeriod)) {
                    $this->data['discount_period'] = trim((string)$this->line->DiscountPeriod);
                }

                if (!empty($this->line->DiscountPeriodAmount)) {
                    $this->data['discount_period_amount'] = (int)$this->line->DiscountPeriodAmount;
                }

                if (!empty($this->line->Description)) {
                    $this->data['description'] = (string)$this->line->Description;
                }

                if (isset($this->line->ProcessContexts)) {
                    $this->data['rule']['conditions'] = $this->setProcessContext();
                }

                if ((string)$this->line->RedSalesId != "") {
                    $this->data['rule']['conditions'] = $this->setRedSalesId();
                }

                $this->_data['rule']['conditions'] = $this->setPackageType($xmlRule);

                if ($sourceType == strtolower(Dyna_ProductMatchRule_Helper_Data::SOURCE_TYPE_PRODUCT_SELECTION)) {
                    if ($this->conditionIndex == 1) {
                        $this->data['rule']['conditions']['1'] = array(
                            'type' => 'salesrule/rule_condition_combine',
                            'aggregator' => 'all',
                            'value' => 1,
                            'new_child' => null
                        );
                    }

                    foreach ($this->line->SourceConditions as $thisSourceCondition) {
                        $this->data['rule']['conditions']['1--' . $this->conditionIndex] = array(
                            'type' => 'salesrule/rule_condition_product_found',
                            'attribute' => '',
                            'operator' => '',
                            'value' => ((string)$thisSourceCondition->ConditionValue === 'true'),
                            'aggregator' => (string)$thisSourceCondition->Aggregator,
                        );

                        $productConditionIndex = 1;
                        foreach ($thisSourceCondition->SourceCondition as $sourceCondition) {
                            $attribute = '';
                            $paramValue = [];
                            $conditionType = trim(strtolower((string)$sourceCondition->Type));

                            if ($conditionType == strtolower(Dyna_ProductMatchRule_Helper_Data::OPERATION_TYPE_PRODUCT)) {
                                $attribute = 'sku';
                            } else if ($conditionType == strtolower(Dyna_ProductMatchRule_Helper_Data::OPERATION_TYPE_CATEGORY)) {
                                $attribute = 'category_ids';
                            } else {
                                continue;
                            }

                            // parse each condition params, and create an array with correct values
                            foreach ($sourceCondition->Params->Param as $param) {
                                // determine param value
                                $attributeName = trim(strtolower((string)$param->attributes()->name));

                                /** @var $categoryModel Dyna_Catalog_Model_Category */
                                $categoryModel = Mage::getModel('dyna_catalog/category');

                                // if param is an SKU, just trim it
                                if ($attributeName == strtolower(self::PARAM_PRODUCT_SKU)) {
                                    $paramValue[] = trim((string)$param);
                                    // it it's a category path
                                } elseif (in_array($attributeName, $this->categoryPathAttributes)) {
                                    $category = $categoryModel
                                        ->getCategoryByNamePath(str_replace('->', '/', (string)$param), '/');

                                    if ($category) {
                                        $paramValue[] = $category->getId();
                                    }
                                    // or maybe it's a category unique ID
                                } elseif ($attributeName == strtolower(self::PARAM_CATEGORY_ID)) {
                                    $category = $categoryModel
                                        ->getCollection()
                                        ->addFieldToFilter('unique_id', ['eq' => trim((string)$param)])
                                        ->getFirstItem();

                                    if ($category) {
                                        $paramValue[] = $category->getId();
                                    }
                                }
                            }

                            // add the condition
                            $this->data['rule']['conditions']['1--' . $this->conditionIndex . '--' . $productConditionIndex] = array(
                                'type' => 'salesrule/rule_condition_product',
                                'attribute' => $attribute,
                                'operator' => $this->getOperator($sourceCondition->Operator),
                                'value' => implode(', ', $paramValue),
                            );
                            $productConditionIndex++;
                        }
                        $this->conditionIndex++;
                    }
                }

                // Skip row if channel not found
                if (!$this->data['website_ids']) {
                    $this->_skippedFileRows++;
                    $this->_logError("[ERROR] " . $name . " does not have specified websites");
                    continue;
                }

                $this->data['rule']['actions'] = $this->getActionScopeConditions();

                // save the rule
                $this->saveRule();
                $this->_log('[SUCCESS] Rule ' . $name . ' saved.');
            } catch (Exception $exception) {
                $this->_skippedFileRows++;
                $this->_logError("[ERROR] Skipped row because: " . $exception->getMessage());
                continue;
            }
        }
        $this->_totalFileRows = $this->lineNo;
    }

    /**
     * Returns a parsed string for sales rules action
     * @return array
     */
    protected function parseAction()
    {
        $tmpSkus = [];
        $tmpReplaceSku = [];
        $tmpHintMessage = [];

        if (isset($this->line->Action->ActionParameters->Sku)) {
            foreach ($this->line->Action->ActionParameters->Sku as $sku) {
                $tmpSkus[] = (string)$sku;
            }
        }

        if (isset($this->line->Action->ActionParameters->ReplaceSku)) {
            foreach ($this->line->Action->ActionParameters->ReplaceSku as $replaceSku) {
                $tmpReplaceSku[] = (string)$replaceSku;
            }
        }

        if (isset($this->line->Action->ActionParameters->HintMessage)) {
            foreach ($this->line->Action->ActionParameters->HintMessage as $hintMessage) {
                $tmpHintMessage[] = (string)$hintMessage;
            }
        }

        $action = (string)$this->line->Action->ActionType;

        return array(
            'sku' => implode(',', $tmpSkus),
            'replace_sku' => implode(',', $tmpReplaceSku),
            'hint_message' => implode(',', $tmpHintMessage),
            'action' => self::ACTION_TYPES[$action],
        );
    }

    /**
     * Returns an array with action scope conditions
     * @return array
     */
    public function getActionScopeConditions()
    {
        $data = array(
            '1' => array(
                'type' => 'salesrule/rule_condition_product_combine',
                'aggregator' => (string)$this->line->ActionScopeConditions->Aggregator,
                'value' => ((string)$this->line->ActionScopeConditions->ConditionValue === 'true'),
                'new_child' => null
            )
        );

        $conditionIndex = 1;
        // parse each condition
        foreach ($this->line->ActionScopeConditions->ActionScopeCondition as $actionScopeCondition) {
            // determine right values; we only treat product sku and category ids
            $foundSku = $foundCategories = [];
            $actionType = strtolower(trim((string)$actionScopeCondition->Type));
            $isCategory = ($actionType == strtolower(self::ATTR_CATEGORY)) ? true : false;
            $conditionAttribute = $isCategory ? 'category_ids' : 'sku';

            /** @var $categoryModel Dyna_Catalog_Model_Category */
            $categoryModel = Mage::getModel('dyna_catalog/category');

            foreach ($actionScopeCondition->Params->Param as $param) {
                $attributeName = trim(strtolower((string)$param->attributes()->name));
                // Product SKU
                if ($attributeName == strtolower(self::PARAM_PRODUCT_SKU)) {
                    $foundSku[] = (string)$param;
                    // Category path
                } elseif (in_array($attributeName, $this->categoryPathAttributes)) {
                    $category = $categoryModel
                        ->getCategoryByNamePath(str_replace('->', '/', (string)$param), '/');

                    if ($category) {
                        $foundCategories[] = $category->getId();
                    }
                    // Category unique ID
                } elseif ($attributeName == strtolower(self::PARAM_CATEGORY_ID)) {
                    $category = $categoryModel
                        ->getCollection()
                        ->addFieldToFilter('unique_id', ['eq' => trim((string)$param)])
                        ->getFirstItem();

                    if ($category) {
                        $foundCategories[] = $category->getId();
                    }
                }
            }

            // determine condition value
            $value = implode(', ', $isCategory ? $foundCategories : $foundSku);

            $data['1--' . $conditionIndex] = array(
                'type' => 'salesrule/rule_condition_product',
                'attribute' => $conditionAttribute,
                'operator' => $this->getOperator($actionScopeCondition->Operator),
                'value' => $value
            );

            $conditionIndex++;
        }

        return $data;
    }

    /**
     * Returns an array with sales rule conditions
     * @return array
     */
    public function setProcessContext()
    {
        $finalProcessContext = [];
        $allProcessContexts = Mage::getModel('dyna_catalog/processContext')
            ->getAllProcessContextCodeIdPairs();

        foreach ($this->line->ProcessContexts->ProcessContext as $currentProcessContext)
        {
            $processContext = trim((string)$currentProcessContext);
            if ($processContext == "*") {
                $finalProcessContext = $allProcessContexts;
            } else {
                foreach (explode(",", $processContext) as $foundProcessContext) {
                    if (array_key_exists(trim($foundProcessContext), $allProcessContexts)) {
                        $finalProcessContext[] = $allProcessContexts[trim($foundProcessContext)];
                    }
                }
            }
        }

        $data = array(
            '1' => array(
                'type' => 'salesrule/rule_condition_combine',
                'aggregator' => 'all',
                'value' => 1,
                'new_child' => null
            ),
            "1--{$this->conditionIndex}" => array(
                'type' => 'dyna_pricerules/condition_processContext',
                'attribute' => 'process_context',
                'operator' => '()',
                'value' => $finalProcessContext,

            )
        );
        $this->conditionIndex++;

        return $data;
    }

    /**
     * Returns an array with sales rule conditions
     * @return array
     */
    public function setPackageType($xmlRule)
    {
        if (empty($xmlRule->PackageTypeId) || trim($xmlRule->PackageTypeId) == self::ALL_PACKAGE_TYPES) {
            $parsedPackageTypes = array_values($this->_packageTypes);
        } else {
            $packageTypes = explode(',', $xmlRule->PackageTypeId);
            $packageTypes = array_map('trim', $packageTypes);
            $parsedPackageTypes = [];

            foreach ($packageTypes as $packageType) {
                if (isset($this->_packageTypes[strtoupper($packageType)])) {
                    $parsedPackageTypes[] = $this->_packageTypes[strtoupper($packageType)];
                }
            }
        }

        $this->_data['package_type'] = implode(",",$parsedPackageTypes);

        if ($this->conditionIndex > 1) {
            $data = $this->_data['rule']['conditions'];
        } else {
            $data = array(
                '1' => array(
                    'type' => 'salesrule/rule_condition_combine',
                    'aggregator' => 'all',
                    'value' => 1,
                    'new_child' => null
                ));
        }

        $data["1--{$this->conditionIndex}"] = array(
            'type' => 'dyna_pricerules/condition_packageType',
            'attribute' => 'package_type',
            'operator' => '()',
            'value' => $parsedPackageTypes,
        );
        $this->conditionIndex++;

        return $data;
    }

    /**
     * Returns an array with sales rule conditions
     * @return array
     */
    public function setRedSalesId()
    {
        if ($this->conditionIndex > 1) {
            $data = $this->data['rule']['conditions'];
        } else {
            $data = array(
                '1' => array(
                    'type' => 'salesrule/rule_condition_combine',
                    'aggregator' => 'all',
                    'value' => 1,
                    'new_child' => null
                ));
        }

        $data["1--{$this->conditionIndex}"] = array(
            'type' => 'dyna_pricerules/condition_redSalesId',
            'attribute' => 'red_sales_id',
            'operator' => '()',
            'value' => trim((string)$this->line->RedSalesId),
        );

        $this->conditionIndex++;

        return $data;
    }

    /**
     * @param $date
     * @return bool|null|string
     */
    protected function formatDate($date)
    {
        /**
         * if date is set strip the character "-" from date
         * this is needed since the csv can contain instead of an empty cell the character "-"
         */
        if (trim($date) == "-" || !$date) {
            return null;
        }
        return date("Y-m-d", strtotime($date));
    }

    /**
     * Try to set the websites for the rule;
     * Go through each website id or website code provided
     * (the list provided in the csv can be of the form "id1,id2" or "code1,code2"
     * and see if the code/id provided is in the list of websites codes/ids that are possible.
     * If one id/code is not found in the possible list => skip the row, error
     *
     * @param $websites
     * @return array
     */
    protected function setWebsitesIds($websites)
    {
        $websitesToAdd = [];
        foreach ($websites->Channel as $website)
        {
            $websitesData = explode(',', strtolower(trim((string)$website)));
            foreach ($websitesData as $key => $possibleWebsite) {
                // if one of the possible websites provided in the csv is "*"
                // the rule will be available for all websites; skip the rest of the code
                if ($possibleWebsite == self::ALL_WEBSITES) {
                    return array_keys($this->_websites);
                }

                // the website provided is the code and we should store the id
                if ($validWebsite = array_search($possibleWebsite, $this->_websites)) {
                    $websitesToAdd[] = $validWebsite;
                } elseif (array_key_exists($possibleWebsite, $this->_websites)) {
                    // the website provided is id and we should store it
                    $websitesToAdd[] = $possibleWebsite;
                } else {
                    // not match can be found between the given website and a code or an id
                    $this->_logError('[ERROR] The website ' . print_r($websitesData, true) . ' is not present/valid. Skipping row');
                    continue;
                }
            }
        }

        return $websitesToAdd;
    }

    /**
     * @param $isPromo
     * @return int
     */
    protected function setIsPromo($isPromo)
    {
        switch (strtolower($isPromo)) {
            case 'ja':
            case 'y':
            case 'yes':
            case '1':
            case 'ok':
            case 'true':
                return 1;
            case 'nee':
            case 'n':
            case 'no':
            case '0':
            case 'x':
            case 'false':
                return 0;
            default:
                return 0;
        }
    }

    /**
     * Save sales rule
     * @return $this
     */
    public function saveRule()
    {
        /** @var Mage_SalesRule_Model_Rule $model */
        $model = Mage::getModel('salesrule/rule');
        $data = $this->data;

        $validateResult = $model->validateData(new Varien_Object($data));

        if ($validateResult) {
            if (isset($data['simple_action']) && $data['simple_action'] == 'by_percent' && isset($data['discount_amount'])) {
                $data['discount_amount'] = min(100, $data['discount_amount']);
            }

            if (isset($data['rule']['conditions'])) {
                $data['conditions'] = $data['rule']['conditions'];
            }

            if (isset($data['rule']['actions'])) {
                $data['actions'] = $data['rule']['actions'];
            }
            $data['stack'] = $this->stack;

            unset($data['rule']);
            $model->loadPost($data);
            $model->save();

            return $model;
        }

        return $this;
    }

    /**
     * Get all DB websites and assign them to local variable
     * @return $this
     */
    protected function setDefaultWebsites()
    {
        $websites = Mage::app()->getWebsites();
        foreach ($websites as $website) {
            $this->_websites[$website->getId()] = $website->getCode();
        }

        return $this;
    }

    protected function setPackageTypes()
    {
        $packageTypes = Mage::getModel('dyna_package/packageType')
            ->getCollection();

        /** @var Dyna_Package_Model_PackageType $packageType */
        foreach ($packageTypes as $packageType) {
            $this->_packageTypes[strtoupper($packageType->getPackageCode())] = $packageType->getId();
        }

        return $this;
    }

    /**
     * Returns the correct operator based on XML input
     * @param $operator
     * @return bool|mixed
     */
    protected function getOperator($operator)
    {
        if (array_key_exists(strtolower(trim((string)$operator)), self::OPERATOR_CONDITIONS)) {
            return self::OPERATOR_CONDITIONS[strtolower(trim((string)$operator))];
        }

        return null;
    }
}
