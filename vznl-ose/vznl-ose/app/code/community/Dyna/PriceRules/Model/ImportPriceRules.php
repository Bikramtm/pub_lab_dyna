<?php

/**
 * Class Dyna_PriceRules_Model_ImportPriceRules
 */
class Dyna_PriceRules_Model_ImportPriceRules extends Omnius_Import_Model_Import_ImportAbstract
{
    protected $_data;
    protected $_headerColumns;
    protected $_csvDelimiter = ';';
    protected $_header;
    protected $_websites;
    protected $_importType = '';
    protected $_rawData;
    protected $_processRule = null;
    protected $_currentCondition = 1;
    protected $_currentAction = 1;
    protected $_error;
    protected $_logFileName = "sales_rules_import";
    protected $_logFileExtension = "log";

    const ALL_WEBSITES = "*";
    const MOBILE_TYPE = 'mobile';
    const FIXED_TYPE = 'fixed';
    const SKU_DEMILIMITER_FOR_IS_ONE_OF = ",";
    const CONDITION_ACTION_AND = " and ";
    const CONDITION_ACTION_OR = " or ";
    const CONDITION_ACTION_ONE_OF = "()";
    const CONDITION_ACTION_IS = "==";

    /** @var Dyna_Import_Helper_Data $_helper */
    public $_helper;
    public $_skipRow = false;


    /**
     * Dyna_PriceRules_Model_ImportPriceRules constructor.
     */
    //to edit
    public function __construct()
    {
        parent::__construct();
        $this->_helper = Mage::helper('dyna_import');
        $this->setDefaultWebsites();
    }

    /**
     * Reset rule information
     */
    public function clearData()
    {
        $this->_data = array(
            'rule_id' => null,
            'product_ids' => '',
            'name' => '',
            'rule_description' => '',
            'is_active' => '',
            'website_ids' => '',
            'customer_group_ids' => array(0, 1),
            'coupon_type' => '1',
            'coupon_code' => '',
            'uses_per_coupon' => '',
            'uses_per_customer' => '',
            'from_date' => '',
            'to_date' => '',
            'sort_order' => '',
            'is_rss' => '',
            'conditions' => array(),
            'actions' => array(),
            'simple_action' => '',
            'discount_amount' => null,
            'discount_qty' => '',
            'discount_step' => '',
            'apply_to_shipping' => '',
            'simple_free_shipping' => '',
            'stop_rules_processing' => '',
            'store_labels' => array(),
            'show_hint' => Dyna_PriceRules_Model_Validator::HINT_NONE,
        );
        $this->_skipRow = false;
    }

    /**
     * @return $this
     */
    public function saveRule()
    {
        /** @var Mage_SalesRule_Model_Rule $model */
        $model = Mage::getModel('salesrule/rule');
        $data = $this->_data;

        $validateResult = $model->validateData(new Varien_Object($data));

        if ($validateResult) {
            if (isset($data['simple_action']) && $data['simple_action'] == 'by_percent'
                && isset($data['discount_amount'])
            ) {
                $data['discount_amount'] = min(100, $data['discount_amount']);
            }

            if (isset($data['rule']['conditions'])) {
                $data['conditions'] = $data['rule']['conditions'];
            }

            if (isset($data['rule']['actions'])) {
                $data['actions'] = $data['rule']['actions'];
            }
            $data['stack'] = $this->stack;

            unset($data['rule']);

            $model->loadPost($data);

            $model->save();
        }

        return $this;
    }

    /**
     * @param $rawData
     * @return bool
     */
    public function process($rawData)
    {
        $this->_totalFileRows++;
        $rawData = array_combine($this->_header, $rawData);
        if (!array_filter($rawData)) {
            $this->_skippedFileRows++;
            return false;
        }

        $ruleName = trim($rawData['rule_title']);
        $existing = Mage::getModel('salesrule/rule')->load($ruleName, 'name');

        if ($existing->getId()) {
            $this->_log("Updating rule: " . $rawData['rule_title'], true);
        } else {
            $this->_log("Saving rule: " . $rawData['rule_title'], true);
        }

        try {
            /** process basic data like name, start date, end date etc */
            $this->clearData();

            $showHint = Dyna_PriceRules_Model_Validator::HINT_NONE;
            if (isset($rawData['show_hint_once'])) {
                if (in_array(trim(strtolower($rawData['show_hint_once'])), ["yes", "true"])) {
                    $showHint = Dyna_PriceRules_Model_Validator::HINT_ONCE;
                } else if (in_array(trim(strtolower($rawData['show_hint_once'])), ["no", "false"])) {
                    $showHint = Dyna_PriceRules_Model_Validator::HINT_ALWAYS;
                }
            }

            $serviceSourceExpression = trim($rawData['service_source_expression'] ?? '');
            if ($serviceSourceExpression != "") {
                $dynaImportHelper = Mage::getModel('productmatchrule/importer');
                if (!$dynaImportHelper->validateServiceSourceExpression($serviceSourceExpression)) {
                    $this->_skippedFileRows++;
                    $this->_logError("[ERROR] Skipped row because: Invalid expression for ".$rawData['rule_title'].".");
                    return false;
                }
            }

            $this->_data['stack'] = $this->stack;
            $this->_data['rule_id'] = $existing->getId() ?: null;
            $this->_data['name'] = trim($rawData['rule_title']);
            $rawData['service_source_expression'] = $rawData['service_source_expression'] ?? '';
            $this->_data['service_source_expression'] = trim($rawData['service_source_expression']);
            $this->_data['show_hint'] = $showHint;
            $this->_data['sort_order'] = isset($rawData['priority']) ? trim($rawData['priority']) : null;
            $this->_data['from_date'] = $this->formatDate($rawData['effective_date']);
            $this->_data['to_date'] = $this->formatDate($rawData['expiration_date']);
            $this->_data['is_active'] = empty($rawData['is_active']) || strtolower($rawData['is_active']) == 'active' ? 1 : 0;
            $this->_data['website_ids'] = $this->setWebsitesIds($rawData['channel']);
            /** fixed sales rules login */
            if (!empty($rawData['usage_discount'])) {
                $this->_data['usage_discount'] = str_replace(["-", ","], ["", "."], $rawData['usage_discount']);
            }
            if (!empty($rawData['usage_discount_percent'])) {
                $this->_data['usage_discount_percent'] = str_replace(["-", ","], ["", "."], $rawData['usage_discount_percent']);
            }
            /** end fixed sales rules logic */
            if (!empty($rawData['is_promo'])) {
                $this->_data['is_promo'] = $this->setIsPromo($rawData['is_promo']);
            }
            if (!$this->_data['website_ids']) {
                $this->_logError("[ERROR] " . $rawData['rule_title'] . " does not have specified websites");
                $this->_skippedFileRows++;
                return false;
            }
            if (!empty($rawData['discount_period'])) {
                $this->_data['discount_period'] = trim($rawData['discount_period']);
            }
            if (!empty($rawData['discount_period_amount'])) {
                $this->_data['discount_period_amount'] = (int)$rawData['discount_period_amount'];
            }
            if (!empty($rawData['rule_description'])) {
                $this->_data['description'] = $rawData['rule_description'];
            }
            if (!empty($rawData['rule_type'])) {
                $this->_data['rule_type'] = $rawData['rule_type'];
            }

            /** leave rules processing to other method */
            $processedRule = $this->processRulesData($rawData);
            if (!$processedRule || $this->_skipRow) {
                $this->_logError("[ERROR] Skipping rule: " . $rawData['rule_title']);
                $this->_skippedFileRows++;
            } else {
                $processedRule->saveRule();
            }
        } catch (Exception $e) {
            $this->_skippedFileRows++;
            $this->_logError("[ERROR] Skipped row because: " . $e->getMessage());
            fwrite(STDERR, "[ERROR] Skipped row because: " . $e->getMessage());
        }
    }

    /**
     * @param $rawData
     * @return $this
     */
    public function processRulesData($rawData)
    {
        $this->setRawData($rawData);
        //$this->setHelper($this->_helper);
        $conditionsAndActions = $this->processRule()->getProcessedRules();
        /** fixed sales rules login */
        if (!$conditionsAndActions) {
            return false;
        }
        $this->_data = array_merge($this->_data, $conditionsAndActions);

        return $this;
    }

    /**
     * @param $date
     * @return bool|null|string
     */
    protected function formatDate($date)
    {
        /**
         * if date is set strip the character "-" from date
         * this is needed since the csv can contain instead of an empty cell the character "-"
         */
        if (trim($date) == "-" || !$date) {
            return null;
        }
        return date("Y-m-d", strtotime($date));
    }

    /**
     * @param $array
     * @param $dateFields
     * @return mixed
     */
    protected function _filterDates($array, $dateFields)
    {
        if (empty($dateFields)) {
            return $array;
        }
        $filterInput = new Zend_Filter_LocalizedToNormalized(array(
            'date_format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
        ));
        $filterInternal = new Zend_Filter_NormalizedToLocalized(array(
            'date_format' => Varien_Date::DATE_INTERNAL_FORMAT
        ));

        foreach ($dateFields as $dateField) {
            if (array_key_exists($dateField, $array) && !empty($dateField)) {
                $array[$dateField] = $filterInput->filter($array[$dateField]);
                $array[$dateField] = $filterInternal->filter($array[$dateField]);
            }
        }
        return $array;
    }

    /**
     * @param $header
     * @return $this
     */
    public function setHeader($header)
    {
        $this->_header = $header;

        return $this;
    }

    /**
     * Try to set the websites for the rule;
     * Go through each website id or website code provided
     * (the list provided in the csv can be of the form "id1,id2" or "code1,code2"
     * and see if the code/id provided is in the list of websites codes/ids that are possible.
     * If one id/code is not found in the possible list => skip the row, error
     *
     * @param $websites
     * @return array
     */
    protected function setWebsitesIds($websites)
    {
        $websitesData = explode(',', strtolower(trim($websites)));
        $websitesToAdd = [];
        foreach ($websitesData as $key => $possibleWebsite) {
            // if one of the possible websites provided in the csv is "*"
            // the rule will be available for all websites; skip the rest of the code
            if ($possibleWebsite == self::ALL_WEBSITES) {
                return array_keys($this->_websites);
            }
            // the website provided is the code and we should store the id
            if ($validWebsite = array_search($possibleWebsite, $this->_websites)) {
                $websitesToAdd[$key] = $validWebsite;
            } elseif (array_key_exists($possibleWebsite, $this->_websites)) {
                // the website provided is id and we should store it
                $websitesToAdd[$key] = $possibleWebsite;
            } else {
                // not match can be found between the given website and a code or an id
                $this->_logError('[ERROR]The website ' . print_r($websitesData, true) . ' is not present/valid. skipping row');
                return false;
            }
        }

        return $websitesToAdd;
    }

    /**
     * Get all DB websites and assign them to local variable
     * @return $this
     */
    protected function setDefaultWebsites()
    {
        $websites = Mage::app()->getWebsites();
        foreach ($websites as $website) {
            $this->_websites[$website->getId()] = $website->getCode();
        }

        return $this;
    }

    /**
     * @param $isPromo
     * @return int
     */
    protected function setIsPromo($isPromo)
    {
        switch (strtolower($isPromo)) {
            case 'ja':
            case 'y':
            case 'yes':
            case '1':
            case 'ok':
            case 'true':
                return 1;
            case 'nee':
            case 'n':
            case 'no':
            case '0':
            case 'x':
            case 'false':
                return 0;
            default:
                return 0;
        }
    }

    /**
     * @param $type
     * @return $this
     */
    public function setImportType($type)
    {
        $this->_importType = $type;
        $this->setImportLogFile();

        return $this;
    }

    /**
     * @return string
     */
    public function getImportType()
    {
        return $this->_importType;
    }

    /**
     * Moved from the constructor, because we need the type to be set
     * This function will set te log file name
     * @return $this
     */
    protected function setImportLogFile()
    {
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processProcessContext()
    {
        if (!empty($this->_rawData['based_on_process_context'])) {
            $processContext = trim($this->_rawData['based_on_process_context']);
            if (!empty($processContext)) {
                /**
                 * @var $processContextModel Dyna_Catalog_Model_ProcessContext
                 */
                $processContextModel = Mage::getModel('dyna_catalog/processContext');
                $toSave = [];

                if ($processContext == "*") {
                    // Assign to all process contexts
                    $toSave = array_keys($processContextModel->getAllProcessContextIdCodePairs());
                } else {
                    $allValues = explode(',', $processContext);

                    foreach ($allValues as $processContextCode) {
                        $toSave[] = $processContextModel->getProcessContextIdByCode(trim($processContextCode));
                    }
                }

                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'dyna_pricerules/condition_processContext',
                    'attribute' => 'process_context',
                    'operator' => self::CONDITION_ACTION_ONE_OF,
                    'value' => $toSave,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processLifeCycle()
    {
        if (!empty($this->_rawData['based_on_a_lifecycle'])) {
            $lifeCycle = trim($this->_rawData['based_on_a_lifecycle']);
            if (!empty($lifeCycle)) {
                if (strtoupper($lifeCycle)=='ACQUISITION') {
                    $lifeCycle = 'ACQ';
                }
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'pricerules/condition_lifecycle',
                    'attribute' => 'lifecycle',
                    'operator' => self::CONDITION_ACTION_IS,
                    'value' => strtoupper($lifeCycle),
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setRawData($data)
    {
        $this->_processRule = null;
        $this->_rawData = $data;

        return $this;
    }

    /**
     * @param $helper
     * @return $this
     */
    public function setHelper($helper)
    {
        $this->_helper = $helper;

        return $this;
    }


    /**
     * @return $this
     */
    public function processRule()
    {
        $this->setDefaultAction();
        $this->setDefaultCondition();

        $this->_processSkusConditionsRules();
        $this->_processCategoryConditionsRules();
        $this->_processDealerCampaigns();
        $this->_processProcessContext();
        $this->_processLifeCycleDetail();
        $this->_processCustomerSegment();
        $this->_processDealer();
        $this->_processAction();
        $this->_processActionScopeProduct();
        $this->_processActionScopeCategory();

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processSkusConditionsRules()
    {
        /** Adding product combination condition */
        if (!empty(trim($this->_rawData['based_on_product']))) {
            $basedOnAProduct = trim($this->_rawData['based_on_product']);

            if (stripos($basedOnAProduct, self::CONDITION_ACTION_AND) !== false ||
                stripos($basedOnAProduct, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd($basedOnAProduct, true, true);
            } else {
                $this->setSimpleConditionProductRule($basedOnAProduct, self::CONDITION_ACTION_IS);
            }
        }

        return $this;
    }

    /**
     * Set Sku/Category Ids conditions that are based on multiple skus/ids
     * The skus/ids separated by AND will be in conditions written on the same level in the tree;
     * The skus/ids separated by Or will be written in the same condition as "is one of A, B, C"
     * Example in the csv we have "I1208_YI003_v017K_LTE AND I1208_YI003_V018P_LTE"
     *
     * @param string $condition
     * @param bool $productCondition // determine if is a product condition or a category condition
     * @param bool $conditionsSection // determine if is a condition from the action section or from the condition section
     */
    protected function _setRuleWithOrAnd($condition, $productCondition = true, $conditionsSection = true)
    {
        // find if the sku/ids are like A and B or C and D or F or E and H
        // replace parentheses if found. For more complex conditions use processParentheses method
        $condition = str_replace(['(', ')'], ['', ''], $condition);
        $itemsExplodedFromAnd = $this->explodeByAnd($condition);
        $itemsExplodedFromOr = $this->explodeByOr($condition);

        if ($productCondition) {
            $product = Mage::getModel('catalog/product');
            // if the sku/ids are with "and"
            if ($itemsExplodedFromAnd) {
                /**
                 * for each exploded group delimited by "and" find if it contains a group delimited by "or"
                 * for the initial group A and B or C and D or F or E and H => the following groups wil be exploded by and:
                 * A
                 * B or C
                 * D or F or E
                 * H
                 * if in the $itemExplodedFromAnd item we have an A or B or C, we check if the every SKU exists.
                 * else we check if $itemExplodedFromAnd sku exists. (OMNVFDE-2882)
                 */
                foreach ($itemsExplodedFromAnd as $itemExplodedFromAnd) {
                    $explodedByOr = $this->explodeByOr($itemExplodedFromAnd);
                    if ($explodedByOr) {
                        foreach ($explodedByOr as $item) {
                            $item = str_replace(" ", "", $item);
                            if (!$productId = $product->getIdBySku($item)) {
                                throw new Exception("Can't find product for SKU: " . $item);
                            }
                        }
                    } else {
                        $itemExplodedFromAnd = str_replace(" ", "", $itemExplodedFromAnd);
                        if (!$productId = $product->getIdBySku($itemExplodedFromAnd)) {
                            throw new Exception("Can't find product for SKU: " . $itemExplodedFromAnd);
                        }
                    }
                    $itemsToAddWithCondition = $this->findItemsToAddFromAPossibleOrGroup($itemExplodedFromAnd);
                    $this->addProductOrCategoryActionOrCondition($productCondition, $conditionsSection, $itemsToAddWithCondition);
                }
            } else {
                /**
                 * the SKUs can be in the form A or B or C or D
                 * check if every SKU exists
                 */
                foreach ($itemsExplodedFromOr as $item) {
                    $item = str_replace(" ", "", $item);
                    if (!$productId = $product->getIdBySku($item)) {
                        throw new Exception("Can't find product for SKU: " . $item);
                    }
                }
                $itemsToAddWithCondition = $this->findItemsToAddFromAPossibleOrGroup($condition);
                $this->addProductOrCategoryActionOrCondition($productCondition, $conditionsSection, $itemsToAddWithCondition);
            }
        } else {
            if ($itemsExplodedFromAnd) {
                foreach ($itemsExplodedFromAnd as $itemExplodedFromAnd) {
                    $itemsToAddWithCondition = $this->findItemsToAddFromAPossibleOrGroup($itemExplodedFromAnd);
                    $this->addProductOrCategoryActionOrCondition($productCondition, $conditionsSection, $itemsToAddWithCondition);
                }
            } else {
                $itemsToAddWithCondition = $this->findItemsToAddFromAPossibleOrGroup($condition);
                $this->addProductOrCategoryActionOrCondition($productCondition, $conditionsSection, $itemsToAddWithCondition);
            }

        }

    }


    /**
     * @param $productCondition
     * @param $conditionsSection
     * @param $itemsToAddWithCondition
     */
    protected function addProductOrCategoryActionOrCondition($productCondition, $conditionsSection, $itemsToAddWithCondition)
    {
        if ($productCondition) {
            if ($conditionsSection) {
                $this->setSimpleConditionProductRule($itemsToAddWithCondition['itemsToAdd'], $itemsToAddWithCondition['condition']);
            } else {
                $this->setSimpleActionProductRule($itemsToAddWithCondition['itemsToAdd'], $itemsToAddWithCondition['condition']);
            }
        } else {
            if ($conditionsSection) {
                $this->setSimpleConditionCategoryRule($itemsToAddWithCondition['itemsToAdd'], $itemsToAddWithCondition['condition']);
            } else {
                $this->setSimpleActionCategoryRule($itemsToAddWithCondition['itemsToAdd'], $itemsToAddWithCondition['condition']);
            }
        }
    }

    /**
     * Explode a string that may contain skus/ids separated by AND;
     * If is not a string with AND => return NULL;
     *
     * @param string $stringToExplode
     * @return array|null
     */
    protected function explodeByAnd($stringToExplode)
    {

        $explodedArrayWithAnd = null;
        if (stripos($stringToExplode, self::CONDITION_ACTION_AND) !== false) {
            $explodedArrayWithAnd = preg_split("/" . self::CONDITION_ACTION_AND . "/i", trim($stringToExplode));
        }
        return $explodedArrayWithAnd;
    }

    protected function explodeByOr($stringToExplode)
    {
        $explodedArrayWithOr = null;
        if (stripos($stringToExplode, self::CONDITION_ACTION_OR) !== false) {
            $explodedArrayWithOr = preg_split("/" . self::CONDITION_ACTION_OR . "/i", trim($stringToExplode));
        }
        return $explodedArrayWithOr;
    }

    /**
     * Get all the conditions that are grouped in parentheses
     * e.g. For (SKU-X AND SKU-Y) OR SKU-Z   ===> will return
     *      [ 0 => [0 => (SKU-X AND SKU-Y), 1 => SKU-X AND SKU-Y] ]
     * @param $string
     * @return null
     * @todo Use this method if complex logic is needed (multiple parentheses)
     */
    protected function processParentheses($string)
    {
        $found = preg_match_all("/\((.*?)\)/", $string, $matches, PREG_SET_ORDER);

        if ($found) {
            return $matches;
        }

        return null;
    }

    /**
     * Find individual sku or category ID to add in a condition from a string that can contain a single id/sku OR
     * from a string that can contain multiple ids/skus separated by or
     * @param string $itemsGroup
     * @return array
     */
    protected function findItemsToAddFromAPossibleOrGroup($itemsGroup)
    {
        /**
         * go through each group delimited previously by "and" to find if it contains items are delimited by "or";
         * if it contains replace the "or" with "," in order to add them in the condition as "one of A,B,C)
         * for the previous example the groups B or C will become "B,C" and the group "D or F or E" will become "D, F, E"
         */
        if (stripos($itemsGroup, self::CONDITION_ACTION_OR) !== false) {
            $itemsToAdd = str_ireplace(self::CONDITION_ACTION_OR, self::SKU_DEMILIMITER_FOR_IS_ONE_OF, $itemsGroup);
            $condition = self::CONDITION_ACTION_ONE_OF;
        } /**
         * if it not contains items delimited by "or" it means that is a single sku/id and we will trim the extra spaces;
         * for the previous example the group formed only from A will be added; and the group formed only from "H" will be added;
         */
        else {
            $itemsToAdd = trim($itemsGroup);
            $condition = self::CONDITION_ACTION_IS;
        }

        return ['itemsToAdd' => $itemsToAdd, 'condition' => $condition];
    }

    /**
     * @param $skus
     * @param string $condition
     * @return $this
     */
    protected function setSimpleConditionProductRule($skus, $condition = self::CONDITION_ACTION_ONE_OF)
    {
        $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
            'type' => 'salesrule/rule_condition_product_found',
            'value' => 1,
            'aggregator' => 'all',
            'new_child' => '',
        );

        $this->_processRule['conditions']["1--" . $this->_currentCondition . "--1"] = array(
            'type' => 'salesrule/rule_condition_product',
            'attribute' => 'sku',
            'operator' => $condition,
            'value' => $skus,
        );

        $this->_currentCondition++;

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processCategoryConditionsRules()
    {
        /** Adding product combination condition */
        if (!empty(trim($this->_rawData['based_on_category']))) {
            $useUniqueId = ($this->getImportType() == 'fixedSalesRules') ? true : false;
            $basedOnCategory = $this->_replaceCategoryPaths(trim($this->_rawData['based_on_category']), $useUniqueId);
            if(!$basedOnCategory){
                $this->_skipRow = true;
                $this->_logError("[ERROR] Category ". $this->_rawData['based_on_category']." was not found");
                return false;
            }
            if (stripos($basedOnCategory, self::CONDITION_ACTION_AND) !== false ||
                stripos($basedOnCategory, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd(trim($basedOnCategory), false, true);
            } else {
                $this->setSimpleConditionCategoryRule($basedOnCategory);
            }
        }

        return $this;
    }

    /**
     * @param $category
     * @param string $condition
     * @return $this
     */
    protected function setSimpleConditionCategoryRule($category, $condition = self::CONDITION_ACTION_IS)
    {
        $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
            'type' => 'salesrule/rule_condition_product_found',
            'value' => 1,
            'aggregator' => 'all',
            'new_child' => '',
        );

        $this->_processRule['conditions']["1--" . $this->_currentCondition . "--1"] = array(
            'type' => 'salesrule/rule_condition_product',
            'attribute' => 'category_ids',
            'operator' => $condition,
            'value' => $category,
        );

        $this->_currentCondition++;

        return $this;
    }

    /**
     * Replace category paths in with their respective ids
     *
     * @param string $categoryString
     *
     * @param bool $useUniqueId
     * @return mixed|string
     * @throws Exception
     */
    protected function _replaceCategoryPaths($categoryString, $useUniqueId = false)
    {
        if (stripos($categoryString, self::CONDITION_ACTION_AND) !== false ||
            stripos($categoryString, self::CONDITION_ACTION_OR) !== false
        ) {
            // replace parentheses if found. For more complex conditions use processParentheses method
            $categoryString = str_replace(['(', ')'], ['', ''], $categoryString);
            //leave %s with white spaces because he can find : 'or' in comfort. or/and must be words.
            $categoryPaths = preg_split(sprintf('/(%s|%s)/i', self::CONDITION_ACTION_AND, self::CONDITION_ACTION_OR), $categoryString);
        } else {
            $categoryPaths = [$categoryString];
        }
        $pathIds = [];
        /** @var $categoryModel Dyna_Catalog_Model_Category */
        $categoryModel = Mage::getModel('catalog/category');
        foreach ($categoryPaths as $path) {
            $path = trim($path);
            if (!array_key_exists($path, $pathIds)) {
                if ($useUniqueId) {
                    $category = $categoryModel->getCategoryByUniqueId($path);
                } else {
                    $category = $categoryModel->getCategoryByNamePath(str_replace('->', '/', $path), '/');
                }

                if ($category) {
                    $pathIds[$path] = $category->getId();
                } else {
                    throw new \Exception('Invalid category ' . (($useUniqueId) ? 'ID' : 'path') . ' ' . $path);
                }
            }
        }
        foreach ($pathIds as $path => $id) {
            $categoryString = str_replace($path, $id, $categoryString);
        }
        return $categoryString;
    }

    /**
     * @return $this
     */
    protected function _processDealerCampaigns()
    {
        if (!empty($this->_rawData['based_on_sales_force_id'])) {
            $this->_rawData['based_on_sales_force_id'] = trim($this->_rawData['based_on_sales_force_id']);
            $allowedCampaign = str_replace("VOID is allowed for campaign ", "", $this->_rawData['based_on_sales_force_id']);
            if (!empty($allowedCampaign)) {
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'dyna_pricerules/condition_dealerCampaign',
                    'attribute' => 'dealer_campaign',
                    'operator' => self::CONDITION_ACTION_ONE_OF,
                    'value' => $allowedCampaign,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processLifeCycleDetail()
    {
        if (!empty($this->_rawData['based_on_a_lifecycle_detail'])) {
            $lifeCycleDetail = trim($this->_rawData['based_on_a_lifecycle_detail']);
            if (!empty($lifeCycleDetail)) {
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'dyna_pricerules/condition_lifecycledetail',
                    'attribute' => 'lifecycle_detail',
                    'operator' => self::CONDITION_ACTION_IS,
                    'value' => $lifeCycleDetail,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processDealer()
    {
        if (!empty($this->_rawData['based_on_red_sales_id'])) {
            $dealerCode = trim($this->_rawData['based_on_red_sales_id']);
            if (!empty($dealerCode)) {
                $operator = self::CONDITION_ACTION_IS; // is
                $tmpCode = array_map('trim', explode(',', $dealerCode));
                if (count($tmpCode) > 1) {
                    $operator = self::CONDITION_ACTION_ONE_OF; // is one of
                    $dealerCode = implode(',', $tmpCode);
                }
                unset($tmpCode);
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'pricerules/condition_dealer',
                    'attribute' => 'dealer',
                    'operator' => $operator,
                    'value' => $dealerCode,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processAction()
    {
        $action = isset($this->_rawData['action']) ? trim(str_replace(' ', '', strtolower($this->_rawData['action']))) : null;

        if ($action && stripos($action, "productwheresku") !== false) {
            $this->__addProduct();
        } elseif ($action && $action == "autoremoveproducts") {
            $this->_addRemoveProducts();
        } elseif (isset($this->_rawData['rule_type']) && $this->_rawData['rule_type'] != 'message_hint') {
            /** Error */
            $this->_helper->log(new Exception(sprintf("Unknown action '%s' type for: %s", $this->_rawData['action'], $this->_rawData['rule_name'])));
            $this->_error = 1;
        }

        if (isset($this->_rawData['rule_type']) && $this->_rawData['rule_type'] == 'message_hint') {
            $this->__addSalesHint();
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processActionScopeProduct()
    {
        /** Adding product combination condition */
        if (!empty(trim($this->_rawData['action_scope']))) {
            $actionBasedOnAProduct = trim($this->_rawData['action_scope']);
            if (stripos($actionBasedOnAProduct, self::CONDITION_ACTION_AND) !== false ||
                stripos($actionBasedOnAProduct, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd(trim($this->_rawData['action_scope']), true, false);
            } else {
                $this->setSimpleActionProductRule($actionBasedOnAProduct);
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processActionScopeCategory()
    {
        /** Adding product combination condition */
        if (!empty(trim($this->_rawData['action_scope_category']))) {
            $useUniqueId = ($this->getImportType() == 'fixedSalesRules') ? true : false;
            $actionScopeCategory = $this->_replaceCategoryPaths(trim($this->_rawData['action_scope_category']), $useUniqueId);
            if(!$actionScopeCategory){
                $this->_skipRow = true;
                $this->_logError("[ERROR] Category ". $this->_rawData['action_scope_category']." was not found");
                return false;
            }

            if (stripos($actionScopeCategory, self::CONDITION_ACTION_AND) !== false ||
                stripos($actionScopeCategory, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd(trim($actionScopeCategory), false, false);
            } else {
                $this->setSimpleActionCategoryRule($actionScopeCategory);
            }
        }

        return $this;
    }

    /**
     * @param $product
     * @param string $condition
     * @return $this
     */
    protected function setSimpleActionProductRule($product, $condition = self::CONDITION_ACTION_IS)
    {
        $this->_processRule['actions']["1--" . $this->_currentAction] = array(
            'type' => 'salesrule/rule_condition_product',
            'attribute' => 'sku',
            'operator' => $condition,
            'value' => $product,
        );

        $this->_currentAction++;

        return $this;
    }

    /**
     * @param $category
     * @param string $condition
     * @return $this
     */
    protected function setSimpleActionCategoryRule($category, $condition = self::CONDITION_ACTION_IS)
    {
        $this->_processRule['actions']["1--" . $this->_currentAction] = array(
            'type' => 'salesrule/rule_condition_product',
            'attribute' => 'category_ids',
            'operator' => $condition,
            'value' => $category,
        );

        $this->_currentAction++;

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processCustomerSegment()
    {
        if (isset($this->_rawData['customer_segment']) && !empty(trim($this->_rawData['customer_segment']))) {
            $customerSegment = trim($this->_rawData['customer_segment']);
            $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                'type' => 'pricerules/condition_customersegment',
                'attribute' => 'customer_segment',
                'operator' => self::CONDITION_ACTION_IS,
                'value' => $customerSegment,
            );

            $this->_currentCondition++;
        }

        return $this;
    }

    protected function __addProduct()
    {
        $sku = trim(explode('=', $this->_rawData['action'], 2)[1]);
        $action = str_replace(' ', '', $this->_rawData['action']);

        // Todo: handle replace and remove actions
        if (stripos($action, "addproductwheresku") !== false) {
            $this->_processRule['simple_action'] = "process_add_product";
        } elseif (stripos($action, "addpromoproductwheresku") !== false) {
            $this->_processRule['simple_action'] = "ampromo_items";
        }elseif (stripos($action, "removeproductwheresku") !== false) {
            $this->_processRule['simple_action'] = "process_remove_product";
        }

        $this->_processRule['promo_sku'] = $sku;
    }

    protected function _addRemoveProducts()
    {
        $this->_processRule['simple_action'] = 'process_remove_product';
    }

    protected function __addSalesHint()
    {
        $this->_processRule['simple_action'] = 'sales_hint';
        $this->_processRule['sales_hint_title'] = $this->_rawData['message_title'];
        $this->_processRule['sales_hint_message'] = $this->_rawData['message_text'];
    }

    /**
     * @return bool|null
     */
    public function getProcessedRules()
    {
        if (!$this->_error) {
            return $this->_processRule;
        } else {
            return false;
        }
    }

    /**
     * @return $this
     */
    protected function setDefaultAction()
    {
        if (empty($this->_processRule['actions'])) {
            $this->_processRule['actions'][1] = array(
                'type' => 'salesrule/rule_condition_product_combine',
                'aggregator' => 'all',
                'value' => 1,
                'new_child' => '',
            );
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function setDefaultCondition()
    {
        if (empty($this->_processRule['conditions'])) {
            $this->_processRule['conditions'][1] = array(
                'type' => 'salesrule/rule_condition_combine',
                'aggregator' => 'all',
                'value' => 1,
                'new_child' => '',
            );
        }

        return $this;
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    public function _logError($msg) {
        $this->_helper->logMsg($msg);
    }
}
