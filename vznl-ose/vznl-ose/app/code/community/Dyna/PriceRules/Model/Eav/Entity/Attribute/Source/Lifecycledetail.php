<?php

/**
 * Class Dyna_PriceRules_Model_Eav_Entity_Attribute_Source_Lifecycledetail
 */
class Dyna_PriceRules_Model_Eav_Entity_Attribute_Source_Lifecycledetail extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array(
                array(
                    "label" => Mage::helper("eav")->__("NewCustomerActivation"),
                    "value" => Dyna_PriceRules_Model_Condition_Lifecycledetail::NEW_CUSTOMER_ACTIVATION
                ),
                array(
                    "label" => Mage::helper("eav")->__("DebitToCreditSwap"),
                    "value" => Dyna_PriceRules_Model_Condition_Lifecycledetail::DEBIT_TO_CREDIT_SWAP
                ),
                array(
                    "label" => Mage::helper("eav")->__("Importing"),
                    "value" => Dyna_PriceRules_Model_Condition_Lifecycledetail::IMPORTING
                ),
                array(
                    "label" => Mage::helper("eav")->__("ChangeTariffOption"),
                    "value" => Dyna_PriceRules_Model_Condition_Lifecycledetail::CHANGE_TARIFF_OPTION
                ),
                array(
                    "label" => Mage::helper("eav")->__("ContractProlongation"),
                    "value" => Dyna_PriceRules_Model_Condition_Lifecycledetail::CONTRACT_PROLONGATION
                )
            );
        }

        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option["value"]] = $option["label"];
        }
        return $_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value)
    {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option["value"] == $value) {
                return $option["label"];
            }
        }
        return false;
    }
}
