<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_PriceRules_Model_Observer
 */
class Dyna_PriceRules_Model_Observer extends Omnius_PriceRules_Model_Observer
{
    /**
     * @param $observer
     * @return mixed
     */
    public function addConditionToSalesRule($observer)
    {
        $additional = $observer->getAdditional();
        $conditions = (array) $additional->getConditions();

        $conditions = array_merge_recursive($conditions, array(
            array('label' => Mage::helper('pricerules')->__('Channel'), 'value' => 'pricerules/condition_channel'),
            array('label' => Mage::helper('pricerules')->__('Dealer'), 'value' => 'pricerules/condition_dealer'),
            array('label' => Mage::helper('pricerules')->__('Dealer Group'), 'value' => 'pricerules/condition_dealergroup'),
            array('label' => Mage::helper('pricerules')->__('Value'), 'value' => 'pricerules/condition_value'),
            array('label' => Mage::helper('pricerules')->__('Campaign'), 'value' => 'pricerules/condition_campaign'),
            array('label' => Mage::helper('pricerules')->__('Customer segment'), 'value' => 'pricerules/condition_customersegment'),
            array('label' => Mage::helper('pricerules')->__('Paidcode'), 'value' => 'pricerules/condition_paidcode'),
            array('label' => Mage::helper('pricerules')->__('Lifecycle'), 'value' => 'pricerules/condition_lifecycle'),
            array('label' => Mage::helper('pricerules')->__('Lifecycle detail'), 'value' => 'dyna_pricerules/condition_lifecycledetail'),
            array('label' => Mage::helper('pricerules')->__('Brand'), 'value' => 'dyna_pricerules/condition_brand'),
            array('label' => Mage::helper('pricerules')->__('Dealer allowed campaigns'), 'value' => 'dyna_pricerules/condition_dealerCampaign'),
            array('label' => Mage::helper('pricerules')->__('Process context'), 'value' => 'dyna_pricerules/condition_processContext'),
            array('label' => Mage::helper('pricerules')->__('Red Sales Id'), 'value' => 'dyna_pricerules/condition_redSalesId'),
            array('label' => Mage::helper('pricerules')->__('Package type'), 'value' => 'dyna_pricerules/condition_packageType'),
        ));

        $additional->setConditions($conditions);
        $observer->setAdditional($additional);

        return $observer;
    }

    /**
     * Process sales rule form creation
     * @param   Varien_Event_Observer $observer
     * @return  Dyna_PriceRules_Model_Observer
     */
    public function handleFormCreation($observer)
    {
        $actionsSelect = $observer->getForm()->getElement('simple_action');
        if ($actionsSelect) {
            $vals = $actionsSelect->getValues();

            $vals[] = array(
                'value' => 'process_add_product',
                'label' => Mage::helper('ampromo')->__('Auto add products'),
            );

            $vals[] = array(
                'value' => 'process_remove_product',
                'label' => Mage::helper('ampromo')->__('Auto remove products'),

            );

            $vals[] = array(
                'value' => 'process_replace_product',
                'label' => Mage::helper('ampromo')->__('Auto replace products'),

            );
            /** Amasty fields  */
            $vals[] = array(
                'value' => 'ampromo_items',
                'label' => Mage::helper('ampromo')->__('Auto add promo items with products'),

            );

            $vals[] = array(
                'value' => 'ampromo_remove_items',
                'label' => Mage::helper('ampromo')->__('Auto remove promo items'),

            );

            $vals[] = array(
                'value' => 'ampromo_replace_items',
                'label' => Mage::helper('ampromo')->__('Auto replace promo items'),

            );

            $vals[] = array(
                'value' => 'sales_hint',
                'label' => Mage::helper('ampromo')->__('Sales hint'),

            );
            /** Amasty fields*/

            $actionsSelect->setValues($vals);
            $actionsSelect->setOnchange('promo_check()');

            $fldSet = $observer->getForm()->getElement('action_fieldset');

            $fldSet->addField(
                'promo_sku_replace',
                'text',
                array(
                    'name' => 'promo_sku_replace',
                    'label' => Mage::helper('ampromo')->__('Remove Products'),
                    'note' => Mage::helper('ampromo')->__('Comma separated list of the SKUs'),
                ),
                'promo_sku'
            );

            $fldSet->addField(
                'promo_sku',
                'text',
                array(
                    'name' => 'promo_sku',
                    'label' => Mage::helper('ampromo')->__('Products'),
                    'note' => Mage::helper('ampromo')->__('Comma separated list of the SKUs'),
                ),
                'simple_action'
            );

            $fldSet->addField(
                'sales_hint_message',
                'textarea',
                array(
                    'name' => 'sales_hint_message',
                    'label' => Mage::helper('ampromo')->__('Message'),
                    'note' => Mage::helper('ampromo')->__('Sales hint message'),
                ),
                'simple_action'
            );

        }

        return $this;
    }

    public function updatePromoMaf($observer)
    {

        /** @var Mage_Sales_Model_Quote_Item $item */
        $item = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quote = $item->getQuote();

        if ($product->getIsPromo() && $targetId = $product->getTargetId()) {

            $targetItem = Mage::getModel('dyna_checkout/sales_quote_item')->load($targetId, 'product_id');
            if ($targetItem->getId()) {

                $finalPrice = $quote->getStore()->convertPrice($item->getProduct()->getMafDiscount());

                if ($item->getProduct()->getMafDiscountPercent() > 0) {
                    $targetPrice = $targetItem->getMafInclTax();
                    if ($targetPrice > 0) {
                        $finalPrice += $item->getProduct()->getMafDiscountPercent() * $targetPrice / 100;
                    }
                }

                $item
                    ->setMaf($finalPrice)
                    ->save();
                $item->getProduct()
                    ->setFinalMaf($finalPrice);
            }
        }
    }

    /**
     * Add new products to quote
     *
     * @param Mage_Sales_Model_Resource_Quote_Item_Collection $items
     * @param $skus
     * @param $parentItem
     */
    protected function add($items, $skus, $parentItem)
    {
        //existing sku in given package
        $existing = array();
        //parse item and build existing array
        foreach ($items as &$i) {
            $existing[$i->getPackageId()][$i->getSku()][] = $i->getTargetId();
        }
        unset($i);

        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        //parse skus given for add
        foreach ($skus as $sku) {
            //check if this sku exists already in this package
            if (isset($existing[$parentItem->getPackageId()][$sku]) || is_array($existing[$parentItem->getPackageId()][$sku])) {
                continue;
            }
            //Get product id by sku
            $id = Mage::getModel('catalog/product')->getIdBySku($sku);
            $item = $cart->addProduct($id);
            if (!is_string($item)) {
                $item->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
                $item->save();
                if (!$item->getId()) {
                    $cart->getQuote()->addItem($item);
                }
            }
        }
        //save quote after remove items

    }

    /**
     * Add new products to active package from quote
     *
     * @param $observer
     * @return bool
     */
    public function addProduct($observer)
    {
        //get rule
        $rule = $observer->getRule();
        //parse sku input, transform into array and trim
        $skuAdd = array_map('trim', explode(',', $rule->getPromoSku()));
        //check if we have data
        if (!$skuAdd && !empty($skuAdd)) {
            return false;
        }
        //get quote
        $quote = $observer->getQuote();
        //get item that triggered this rule
        $item = $observer->getItem();
        $appliedRules = array_filter(explode(",", $item->getAppliedRuleIds()));
        $appliedRules[] = $rule->getId();
        $item->setAppliedRuleIds(implode(",", $appliedRules));
        //get all visible items from quote
        $allItems = $quote->getAllVisibleItems();
        //add products from quote
        $this->add($allItems, $skuAdd, $item);

        return true;
    }

    /**
     * @param $observer
     * @return bool
     */
    public function addPromo($observer)
    {
        $rule = $observer->getRule();

        $promoSku = $rule->getPromoSku();
        if (!$promoSku) {
            return false;
        }

        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $item = $observer->getItem();
        $address = $observer->getAddress();
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $address->getQuote();

        $allItems = $quote->getAllItems();

        $existing = array();
        foreach ($allItems as $i) {
            $existing[$i->getPackageId()][$i->getSku()][] = $i->getTargetId();
        }
        unset($i);

        $promoSku = explode(',', $promoSku);

        foreach ($promoSku as $sku) {
            $sku = trim($sku);
            if (!$sku) {
                continue;
            }

            $alreadyExisting = false;
            if (isset($existing[$item->getPackageId()][$sku]) && is_array($existing[$item->getPackageId()][$sku])) {
                $alreadyExisting = true;
            }

            if ($alreadyExisting && in_array($item->getProductId(), $existing[$item->getPackageId()][$sku])) {
                continue;
            }

            $product = $this->_loadProduct($sku);
            if (!$product || Mage_Catalog_Model_Product_Status::STATUS_ENABLED != $product->getStatus()) {
                continue;
            }

            if (Mage_Catalog_Model_Product_Status::STATUS_ENABLED != $product->getStatus()) {
                continue;
            }

            /** @var Dyna_Checkout_Model_Sales_Quote_Item $promoItem */
            if (!$alreadyExisting) {
                $alternateQuoteItem = $quote->getItemBySku($sku, $item->getPackageId(), true);

                if ($alternateQuoteItem && $alternateQuoteItem->isContract() && $alternateQuoteItem->isPromo()) {
                    $promoItem = $alternateQuoteItem;
                    $promoItem->isDeleted(false);
                } else {
                    $promoItem = $quote->addProduct($product, 1);
                    $promoItem->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
                }

                if (!is_string($promoItem)) {
                    // set active package id to the promo product
                    $promoItem->setTargetId($item->getProductId());
                    $promoItem->setIsPromo(true);
                    $promoItem->setPackageId($item->getPackageId());
                    $promoItem->setPackageType($item->getPackageType());
                    $promoItem->setEditOrderId($item->getEditOrderId());
                    $promoItem->setCustomPrice(0);
                    $promoItem->setOriginalCustomPrice(0);
                }
            } else {
                $promoItem = $quote->getItemBySku($sku, $item->getPackageId());
                // set active package id to the promo product
                $promoItem->setTargetId($item->getProductId());
                $promoItem->setIsPromo(true);
            }
        }

        $address->unsetData('cached_items_nonnominal');
        $address->unsetData('cached_items_all');
        $address->unsetData('cached_items_nominal');

        return true;
    }

    /**
     * @param $observer
     * @return bool
     */
    public function replacePromo($observer)
    {
        $rule = $observer->getRule();
        $promoSku = $rule->getPromoSku();
        $promoSkuReplace = $rule->getPromoSkuReplace();

        if (!$promoSku || !$promoSkuReplace) {
            return false;
        }

        $promoSku = trim(array_shift(explode(',', $promoSku)));
        $promoSkuReplace = trim(array_shift(explode(',', $promoSkuReplace)));

        if (!$promoSku || !$promoSkuReplace) {
            return false;
        }

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $observer->getQuote();
        $item = $observer->getItem();
        $allItems = $quote->getAllVisibleItems();

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $i */
        foreach ($allItems as &$i) {
            if ($i->getSku() == $promoSku) {
                try {
                    $quote->removeItemBySku($i->getSku(), $i->getPackageId());
                } catch (Exception $e) {
                    Mage::log($e->getMessage(), Zend_Log::ALERT, "exception.log");
                }
                $newProduct = $this->_loadProduct($promoSkuReplace);
                /** @var Dyna_Checkout_Model_Sales_Quote_Item $promoItem */
                $promoItem = $quote->addProduct($newProduct, 1);

                if (!is_string($promoItem)) {
                    $promoItem->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
                    $promoItem->setTargetId($item->getProductId());
                    $promoItem->setIsPromo(true);
                    $promoItem->setPackageId($item->getPackageId());
                    $promoItem->setPackageType($item->getPackageType());
                    $promoItem->setEditOrderId($item->getEditOrderId());
                    $promoItem->setCustomPrice(0);
                    $promoItem->setOriginalCustomPrice(0);

                    try {
                        $promoItem->save();
                        $quote->addItem($promoItem);
                        //Mage::dispatchEvent('sales_quote_address_mixmatch_here', ['quote_item' => $promoItem,"callback_method"=>'observer']);
                    } catch (Exception $e) {
                        Mage::log($e->getMessage(), Zend_Log::ALERT, "exception.log");
                    }
                }
            }
        }
        unset($i);
        return true;
    }

    /**
     * Remove products to active package from quote
     *
     * @param $observer
     * @return bool
     */
    public function removeProduct($observer)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        $item = $observer->getItem();

        // get the sales rule
        /** @var Omnius_Checkout_Model_SalesRule_Rule $rule */
        $rule = $observer->getRule();

        // check if there is a list of skus that specifically need to be removed from cart or whether current event item met the action's condition
        $skuRemove = $rule->getPromoSku() ? array_map('trim', explode(',', $rule->getPromoSku())) : array();
        if (empty($skuRemove)) {
            $skuRemove[] = $item->getSku();
        }
        $skuRemove = array_filter(array_unique($skuRemove));
        // check if there is something to remove from cart
        if (empty($skuRemove)) {
            return false;
        }
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $item->getQuote();
        // get all visible items from quote
        $allItems = $quote->getAllVisibleItems();

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItem */
        foreach ($allItems as $quoteItem) {
            // skip if not marked for removal
            if (!in_array($quoteItem->getSku(), $skuRemove)) {
                continue;
            }

            try {
                $quote->removeItemBySku($quoteItem->getSku(), $quoteItem->getPackageId());
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, "exception.log");
            }
        }

        return true;
    }

    /**
     * Display sales hint message
     *
     * @param $observer
     * @return bool
     */
    public function salesHint($observer)
    {
        $rule = $observer->getRule();

        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');

        // register these messages on customer session
        $customerSession->addHint(array(
            'message' => $rule->getSalesHintMessage(),
            // make sure it arrives int in response
            'ruleId' => (int)$rule->getId(),
            'hintOnce' => $rule->getShowHint() == Dyna_PriceRules_Model_Validator::HINT_ONCE,
        ), $rule->getId());

        return true;
    }
}
