<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
$salesruleTable = $this->getTable('salesrule/rule');

if (!$this->getConnection()->tableColumnExists($salesruleTable, 'communication_variable')) {
    $this->getConnection()
        ->addColumn(
            $salesruleTable,
            'communication_variable',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Communication Variable',
                'nullable' => true,
                'default' => null
            )
        );
}

// Creating a new table communication_variable_reference if not exists
if($this->getConnection()->isTableExists('communication_variable_reference') != true) {
    $table = $this->getConnection()
        ->newTable('communication_variable_reference')
        ->addColumn(
            'reference_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            array(
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true,
                'auto_increment' => true,
            ),
            'Entity Id'
        )
        ->addColumn('communication_variable', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Communication Variable')
        ->addColumn('communication_variable_reference', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Display Label Checkout');
    $this->getConnection()->createTable($table);
}

$this->endSetup();
