<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
$salesTable = $this->getTable('salesrule/rule');

if (!$this->getConnection()->tableColumnExists($salesTable, 'identifier')) {
    $this->getConnection()
        ->addColumn(
            $salesTable,
            'identifier',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Identifier',
                'nullable' => true,
                'default' => null
            )
        );
}

if (!$this->getConnection()->tableColumnExists($salesTable, 'origin')) {
    $this->getConnection()
        ->addColumn(
            $salesTable,
            'origin',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Origin',
                'nullable' => true,
                'default' => null
            )
        );
}

$this->endSetup();
