<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

$this->getConnection()->addColumn($this->getTable('sales_rule_product_match'), 'dealerCampaign', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'default' => null,
    'nullable' => true,
    'required' => false,
    'comment' => "Dealer allowed campaigns",
]);
$this->getConnection()->addColumn($this->getTable('sales_rule_product_match'), 'lifecycledetail', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'default' => null,
    'nullable' => true,
    'required' => false,
    'comment' => "Lifecycle detail",
]);

$installer->endSetup();