<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
$tableName = $this->getTable('sales_rule_product_match');
// Add new process_context column
$this->getConnection()->addColumn($tableName, 'process_context', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'default' => null,
    'nullable' => true,
    'required' => false,
    'comment' => "Process context",
]);

if ($installer->getConnection()->tableColumnExists($tableName, 'dealerCampaign')) {
    // Update dealerCapaign column name to dealer_campaign (to follow conventions)
    $this->getConnection()->changeColumn($tableName, 'dealerCampaign', 'dealer_campaign', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'default' => null,
        'nullable' => true,
        'required' => false,
        'comment' => "Dealer allowed campaigns",
    ]);
}

if ($installer->getConnection()->tableColumnExists($tableName, 'lifecycledetail')) {
    // Update lifecycledetail column name to lifecycle_detail (to follow conventions)
    $this->getConnection()->changeColumn($tableName, 'lifecycledetail', 'lifecycle_detail', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'default' => null,
        'nullable' => true,
        'required' => false,
        'comment' => "Lifecycle detail",
    ]);
}

if ($installer->getConnection()->tableColumnExists($tableName, 'dealer_group')) {
    // Update dealer_group column definition from INT to VARCHAR as it can contain multiple dealers
    $this->getConnection()->changeColumn($tableName, 'dealer_group', 'dealer_group', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'nullable' => true,
        'required' => false,
        'comment' => "Dealer group(s)",
    ]);
}

$installer->endSetup();