<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$this->getConnection()->addColumn($this->getTable('salesrule/rule'), 'install_base_target', array(
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'nullable'  => false,
    'default'   => '0',
    'comment'   => 'Flags rule for install base targeting',
));

$this->endSetup();
