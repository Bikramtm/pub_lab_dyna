<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Catalog price rules reqrite
 *
 */
class Dyna_PriceRules_Block_Adminhtml_Promo_Quote extends Omnius_PriceRules_Block_Adminhtml_Promo_Quote
{
    /**
     * Dyna_PriceRules_Block_Adminhtml_Promo_Quote constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_removeButton('import');
    }
}
