<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Cache_CacheController
 */
require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . '/CacheController.php';

class Dyna_Cache_CacheController extends Mage_Adminhtml_CacheController
{
    /**
     * Flush cache storage
     */
    public function flushAllAction()
    {
        Mage::dispatchEvent('adminhtml_cache_flush_all');
        Mage::app()->getCacheInstance()->getFrontend()->clean(
            Zend_Cache::CLEANING_MODE_NOT_MATCHING_TAG,
            array(
                Dyna_Cache_Model_Cache::CACHE_TAG,
                Dyna_Cache_Model_Cache::AGENT_TAG,
                Dyna_Cache_Model_Cache::PRODUCT_TAG
            )
        );
        $this->_getSession()->addSuccess(
            Mage::helper('adminhtml')->__("The cache storage has been flushed.")
        );
        $this->_redirect('*/*');
    }

    /**
     * Flush application cache
     */
    public function flushApplicationCacheAction()
    {
        Mage::dispatchEvent('adminhtml_cache_flush_application_cache');
        Mage::app()->getCacheInstance()->getFrontend()->clean(
            Zend_Cache::CLEANING_MODE_MATCHING_TAG,
            array(Dyna_Cache_Model_Cache::CACHE_TAG)
        );
        $this->_getSession()->addSuccess(
            Mage::helper('adminhtml')->__("The application cache storage has been flushed.")
        );
        $this->_redirect('*/*');
    }

    /**
     * Flush product cache
     */
    public function flushProductCacheAction()
    {
        Mage::dispatchEvent('adminhtml_cache_flush_application_cache');
        Mage::app()->getCacheInstance()->getFrontend()->clean(
            Zend_Cache::CLEANING_MODE_MATCHING_TAG,
            array(Dyna_Cache_Model_Cache::PRODUCT_TAG)
        );
        $this->_getSession()->addSuccess(
            Mage::helper('adminhtml')->__("The product cache storage has been flushed.")
        );
        $this->_redirect('*/*');
    }

    /**
     * Flush application cache
     */
    public function flushVarnishCacheAction()
    {
        Mage::dispatchEvent('adminhtml_cache_flush_varnish_cache');
        try {
            Mage::helper('dyna_cache')->clearVarnish();

            $this->_getSession()->addSuccess(
                Mage::helper('adminhtml')->__("The varnish cache storage has been flushed.")
            );
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        $this->_redirect('*/*');
    }
}
