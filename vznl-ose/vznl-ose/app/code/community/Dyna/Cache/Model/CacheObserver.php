<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Cache_Model_CacheObserver
 */
class Dyna_Cache_Model_CacheObserver
{
    /**
     * Regenerates frontend cache prefix to invalidate
     * existing items from the frontend javascript cache
     */
    public function regenerateFrontendCacheKeyPrefix()
    {
        Mage::getSingleton('dyna_cache/cache')->getFrontendCachePrefix(true);
    }
}
