<?php
// Create FN_Tariff_Options attribute set

/* @var $installer Dyna_Fixed_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributesList = [
    'product_type' => [
        'label' => 'Product Type',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'print_name' => [
        'label' => 'Print name (documents)',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'minimum_contract_duration' => [
        'label' => 'Minimum Contract Duration',
        'input' => 'text',
        'type' => 'int',
    ],
    'minimum_contract_duration_unit' => [
        'label' => 'Minimum Contract Duration Unit',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'maf' => [
        'label' => 'Maf',
        'input' => 'price',
        'type' => 'decimal',
    ],
];


foreach ($attributesList as $attributeCode => $attributeData) {
    $attributeData['user_defined'] = 1;
    $installer->addAttribute($entityTypeId, $attributeCode, $attributeData);
}

$createAttributeSets = [
    'FN_Tariff_Options' => [
        'groups' => [
            'Fixed' => [
                'product_type',
                'print_name',
                'minimum_contract_duration',
                'minimum_contract_duration_unit',
                'maf',
            ]
        ]
    ]
];

$sortOrder = 10;
$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($createAttributeSets);

$installer->endSetup();
