<?php
// Create sorting attribute

/* @var $installer Dyna_Fixed_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributesList = [
    'sorting' => [
        'label' => 'Sorting',
        'input' => 'text',
        'type' => 'int',
    ],
];


foreach ($attributesList as $attributeCode => $attributeData) {
    $attributeData['user_defined'] = 1;
    $installer->addAttribute($entityTypeId, $attributeCode, $attributeData);
}

$createAttributeSets = [
    'FN_SalesPackage' => [
        'groups' => [
            'Fixed' => [
                'sorting',
            ]
        ]
    ],
    'FN_Options' => [
        'groups' => [
            'Fixed' => [
                'sorting',
            ]
        ]
    ]
];

$sortOrder = 100;
$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($createAttributeSets);

$installer->endSetup();
