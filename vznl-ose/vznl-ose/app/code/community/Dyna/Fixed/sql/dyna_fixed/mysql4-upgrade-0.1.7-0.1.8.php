<?php
/**
 * Installer for adding new attributes (details in OMNVFDE-107)
 */

/* @var $this Dyna_Mobile_Model_Resource_Setup */

$installer = $this;

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

/** Add missing attributes */

$existingAttributes = [
    'internal_name',
    'display_name',
    'print_name',
    'special_legal_text',
    'special_legal_text_nr_porting',
    'special_legal_text_for_redone',
    'product_hint',
    'not_compatible_hint'
];


// ===================================================================================== //

$attributeGroupNames['General'] = [];


/** Remove FN general attributes from wrong Mobile attribute sets */
$removeFromAttributeSet = [
    1 => 'Prepaid_Package',
    2 => 'Mobile_Voice_Data_Tariff',
    3 => 'Mobile_Data_Tariff',
    4 => 'Mobile_Prepaid_Tariff',
];

try {
    foreach ($removeFromAttributeSet as $setName) {
        $attributeSetId = $installer->getAttributeSetId($entityTypeId, $setName);
        $attributeSet = Mage::getModel('eav/entity_attribute_set')->load($attributeSetId);

        foreach ($existingAttributes as $code) {
            // Assign attribute to attribute group to be used in next block
            // that adds FN general attributes to all FN attribute sets
            $attributeGroupNames['General'][$code] = '1,2,3,4';

            $attributeId = $installer->getAttribute($entityTypeId, $code, 'attribute_id');
            $attribute = Mage::getModel('eav/entity_attribute')->load($attributeId);
            $attribute->setAttributeSetId($attributeSet->getId())->loadEntityAttributeIdBySet();

            if ($attribute->getEntityAttributeId()) {
                Mage::getModel('catalog/product_attribute_set_api')->attributeRemove($attributeId, $attributeSetId);
            }
        }
    }
} catch (Exception $e) {
    // silent
}


// ===================================================================================== //

/** Add FN general attributes to all FN attribute sets */
$attributeSetNames = [
    1 => 'FN_Accessory',
    2 => 'FN_Promotion',
    3 => 'FN_Service',
    4 => 'FN_Unknown',
];

$createAttributeSets = [];
$sortOrder = 20;

foreach ($attributeGroupNames as $group => $attributes) {
    foreach ($attributes as $code => $sets) {
        $setIds = explode(',', $sets);
        foreach ($setIds as $setId) {
            $attributeSet = $attributeSetNames[$setId];
            $createAttributeSets[$attributeSet]['groups'][$group][] = $code;
        }
    }
}

$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($attributeSetNames, $attributeGroupNames, $createAttributeSets);

unset($existingAttributes);


// ===================================================================================== //

/** New attributes: Option Group and Option Category */
$newAttributes = [
    'option_group' => [
        'label' => 'Option Group',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'option_category' => [
        'label' => 'Option Category',
        'input' => 'text',
        'type' => 'varchar'
    ],
];

$attrSetId = $installer->getAttributeSetId($entityTypeId, 'FN_Options');
foreach ($newAttributes as $code => $options) {
    $attributeId = $installer->getAttribute($entityTypeId, $code, 'attribute_id');
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);

        $installer->addAttributeToSet($entityTypeId, $attrSetId, 'Fixed', $code, $sortOrder);
    }
    $sortOrder++;
}

unset($newAttributes, $attrSetId);


// ===================================================================================== //

/** Update attribute portfolio to be used in promo rules */
if ($attributeId = $installer->getAttributeId($entityTypeId, 'portfolio')) {
    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $attribute->setData('is_used_for_promo_rules', 1)->save();
}


$installer->endSetup();
