<?php
/**
 * Installer that adds the contract_value attribute to product entity and assigns this attribute to all attribute sets (see @VFDED1W3S-913)
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Sales_Model_Entity_Setup */
$this->startSetup();

// remove old attribute
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$this->removeAttribute($entityTypeId, 'product_type');

$this->addAttribute(Mage_Catalog_Model_Product::ENTITY, "product_type", array(
    'type' => 'text',
    'input' => 'multiselect',
    'label' => 'Product Type',
    'backend' => 'eav/entity_attribute_backend_array',
    'visible' => true,
    'option' => ['values' => ["DSL" => "DSL", "LTE" => "LTE"]],
    'user_defined' => 1,
    'required' => false,
    'visible_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

// FN_SalesPackage, FN_Options, FN_Accessory, FN_Hardware, FN_Tariff_Options
$createAttributeSets = [
    'FN_Tariff_Options' => [
        'groups' => [
            'Fixed' => [
                'product_type'
            ]
        ]
    ],
    'FN_SalesPackage' => [
        'groups' => [
            'Fixed' => [
                'product_type'
            ]
        ]
    ],
    'FN_Options' => [
        'groups' => [
            'Fixed' => [
                'product_type'
            ]
        ]
    ],
    'FN_Accessory' => [
        'groups' => [
            'Fixed' => [
                'product_type'
            ]
        ]
    ],
    'FN_Hardware' => [
        'groups' => [
            'Fixed' => [
                'product_type'
            ]
        ]
    ],
];

$sortOrder = 10;
$this->assignAttributesToSet($createAttributeSets, $sortOrder);

$this->endSetup();
