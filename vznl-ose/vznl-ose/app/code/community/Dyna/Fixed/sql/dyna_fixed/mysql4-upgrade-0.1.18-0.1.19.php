<?php

/* @var $this Dyna_Fixed_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeSetCode = 'FN_Accessory';
$attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");


// Assign visibility attributes to FN_Accessory/General group
$assignAttributes = [
    "visibility",
];

foreach ($assignAttributes as $attributeCode) {
    if ($attributeSetId) {
        $installer->addAttributeToSet($entityTypeId, $attributeSetId, "General", $attributeCode);
    }
}

$installer->endSetup();
