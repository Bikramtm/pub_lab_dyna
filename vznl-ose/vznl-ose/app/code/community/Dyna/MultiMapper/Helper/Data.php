<?php

/**
 * Class Dyna_MultiMapper_Helper_Data
 */
class Dyna_MultiMapper_Helper_Data extends Omnius_MultiMapper_Helper_Data
{
    /**
     * @param $priceplanSku
     * @param $promoSku
     * @return false|Mage_Core_Model_Abstract
     */
    public function getMappingInstanceBySku($priceplanSku, $promoSku)
    {
        return $this->_getMappingInstanceByField($priceplanSku, $promoSku, 'commercial_sku', 'promo_sku');
    }

    /**
     * Get all multi mapper rules that validate the service expression
     *
     * @param $inputParams
     * @return array $active
     */
    public function getMappingInstanceByService($inputParams)
    {
        /**
         * @var $dynaCoreHelper Dyna_Core_Helper_Data
         */
        $dynaCoreHelper = Mage::helper('dyna_core');

        $collection = Mage::getModel('multimapper/mapper')->getCollection()
            ->addFieldToFilter('service_expression', ['notnull' => true])
            ->load();

        $active = [];
        foreach ($collection as $instance) {
            // Evaluate each rule
            $passed = $dynaCoreHelper->evaluateExpressionLanguage(
                $instance->getServiceExpression(),
                $inputParams
            );

            if ($passed) {
                $active[] = $instance;
            }
        }

        return $active;
    }

    /**
     * Reverse MultiMapper for soc
     *
     * @param $socs
     * @return array of SOCs (article numbers)
     */
    public function getSkuBySoc($socs)
    {
        // array to store in the SKU's
        $skus = array();

        if (is_array($socs)) {
            foreach ($socs as $soc) {
                $skus = array_merge($skus, $this->getSkuBySoc($soc));
            }
        } else {
            $skus = Mage::getModel('multimapper/mapper')->getSkusForSoc($socs);
        }

        return count($skus) ? array_shift($skus) : null;
    }

    /**
     * Reverse MultiMapper skus for socs
     *
     * @param $socs
     * @return array of SOCs (article numbers)
     */
    public function getSkusBySocs($socs)
    {
        // array to store in the SKU's
        $skus = array();

        if (is_array($socs)) {
            foreach ($socs as $soc) {
                $skus = array_merge($skus, $this->getSkusBySocs($soc));
            }
        } else {
            $skus = Mage::getModel('multimapper/mapper')->getSkusForSoc($socs);
        }

        return $skus;
    }

    /**
     * Reverse MultiMapper for FN data
     *
     * @param $code
     * @param $value
     * @param $additional
     * @return array of mapper ids
     */
    public function getMapperIdByFnData($code, $value, $additional)
    {
        /**
         * @var $addonsModel Dyna_MultiMapper_Model_Addon
         */
        $addonsModel = Mage::getModel("dyna_multimapper/addon");
        $addons = $addonsModel->getMapperIdsByFNData($code, $value, $additional);
        $mapperIds = [];
        foreach($addons as $mapper){
            $mapperIds[] = $mapper->getMapperId();
        }

        return $mapperIds;
    }
}
