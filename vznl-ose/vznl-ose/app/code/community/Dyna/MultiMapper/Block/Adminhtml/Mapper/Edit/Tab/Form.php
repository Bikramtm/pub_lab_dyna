<?php

/**
 * Class Dyna_MultiMapper_Block_Adminhtml_Mapper_Edit_Tab_Form
 */
class Dyna_MultiMapper_Block_Adminhtml_Mapper_Edit_Tab_Form extends Omnius_MultiMapper_Block_Adminhtml_Mapper_Edit_Tab_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** OMNVFDE-405: Add more than 10 addons */
        /** New button added at the base of the form which adds new inputs for a new addon */

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("multimapper_form", array("legend" => Mage::helper("multimapper")->__("Item information")));
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");


        $fieldset->addField("sku", "text", array(
            "label" => Mage::helper("multimapper")->__("SKU"),
            "class" => "required-entry",
            "required" => true,
            "name" => "sku",
        ));

        $fieldset->addField("service_expression", "textarea", array(
            "label" => Mage::helper("multimapper")->__("Service Expression"),
            "name" => "service_expression",
        ));

        $fieldset->addField("priority", "text", array(
            "label" => Mage::helper("multimapper")->__("Priority"),
            "name" => "priority",
        ));

        $fieldset->addField("stop_execution", "select", array(
            "label" => Mage::helper("multimapper")->__("Stop execution"),
            "name" => "stop_execution",
            "values" => ["1" => "Yes", "0" => "No"],
        ));

        $fieldset->addField("component_type", "text", array(
            "label" => Mage::helper("multimapper")->__("Component type"),
            "name" => "component_type",
            "after_element_html" => "<br/><i class=\"nm\"><small>Used by FN</small></i>"
        ));

        $fieldset->addField("comment", "textarea", array(
            "label" => Mage::helper("multimapper")->__("Comment"),
            "name" => "comment",
        ));

        $fieldset->addField("xpath_incomming", "textarea", array(
            "label" => Mage::helper("multimapper")->__("XPath Incoming"),
            "name" => "xpath_incomming",
        ));

        $fieldset->addField("xpath_outgoing", "textarea", array(
            "label" => Mage::helper("multimapper")->__("XPath Outgoing"),
            "name" => "xpath_outgoing",
        ));

        $fieldset->addField("direction", "select", array(
            "label" => Mage::helper("multimapper")->__("Direction"),
            "name" => "direction",
            "values" => ["in" => "In", "out" => "Out", "both" => "Both"],
        ));

        /**
         * @var $addonModel Dyna_MultiMapper_Model_Addon
         */
        $addonModel = Mage::getModel("dyna_multimapper/addon");

        if ($registry = Mage::registry("mapper_data")) {
            $registryData = $registry->getData();
            if (isset($registryData['entity_id']) && $registryData['entity_id']) {
                $mapperAddons = $addonModel->getAddonByMapperId($registryData['entity_id']);
            }

        }

        if (isset($mapperAddons) && count($mapperAddons)) {
            $defaultAddonsCount = count($mapperAddons) + 1;
        } else {
            $mapperAddons = [ $addonModel ];
            $defaultAddonsCount = 2;
        }

        $i = 1;
        /** @var $addon Dyna_Multimapper_Model_Addon */
        foreach ($mapperAddons as $addon) {
            $fieldset->addField("addon" . $i . "_id", "hidden", array(
                "name" => "addon" . $i . "_id",
                "value" => $addon->getId()
            ));
            $fieldset->addField("addon" . $i . "_name", "text", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " name"),
                "name" => "addon" . $i . "_name",
                "value" => $addon->getAddonName(),
            ));

            $fieldset->addField("addon" . $i . "_value", "text", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " value"),
                "name" => "addon" . $i . "_value",
                "value" => $addon->getAddonValue(),
            ));

            $fieldset->addField("addon" . $i . "_additional_value", "text", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " additional value"),
                "name" => "addon" . $i . "_additional_value",
                "value" => $addon->getAddonAdditionalValue(),
            ));
            $fieldset->addField("addon" . $i . "_promo_id", "text", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " promo"),
                "name" => "addon" . $i . "_promo_id",
                "value" => $addon->getPromoId()
            ));

            $fieldset->addField("addon" . $i . "_technical_id", "text", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " technical id"),
                "name" => "addon" . $i . "_technical_id",
                "value" => $addon->getTechnicalId()
            ));

            $fieldset->addField("addon" . $i . "_nature_code", "text", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " nature code"),
                "name" => "addon" . $i . "_nature_code",
                "value" => $addon->getNatureCode(),
            ));

            $fieldset->addField("addon" . $i . "_backend", "text", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " backend"),
                "name" => "addon" . $i . "_backend",
                "value" => $addon->getBackend(),
                "after_element_html" => "<p>&nbsp;</p>",
            ));

            $fieldset->addField('addon_' . $i . '_process_context', 'multiselect', array(
                'label' => Mage::helper("multimapper")->__('Addon ' . $i . ' Process context'),
                'values' => $processContextHelper->getProcessContextsByNamesForForm(),
                'name' => 'addon' . $i . '_process_context',
                'value' => $addon->getProcessContextIds()
            ));

            $i++;
        }

        $addAddonButton = $fieldset->addField('add_addon', 'button', array(
                'name' => 'add_addon',
                'label' => $this->helper('dyna_multimapper')->__('Add another addon'),
                'value' => $this->helper('dyna_multimapper')->__('Add addon'),
                'class' => 'form-button',
                'onclick' => "multiMapperAppend(this)",
            )
        );

        /** The add new addon script with the following increment number */
        $addAddonButton->setAfterElementHtml('<script>
//< ![CDATA
var counter = ' . $defaultAddonsCount . ';
multiMapperAppend = function (element)
{
    var prependedElement = element.parentNode.parentNode;
    var newChildPromo = document.createElement("tr");
    var newChildTechnical = document.createElement("tr");
    var newChildNature = document.createElement("tr");
    var newChildName = document.createElement("tr");
    var newChildValue = document.createElement("tr");
    var newChildAdditionalValue = document.createElement("tr");
    var newChildBackend = document.createElement("tr");

    newChildName.innerHTML = "<td class=\'label\'><label for=\'addon" + counter + "_name\'>Addon " + counter + " name</label></td>"
                         + "<td class=\'value\'><input type=\'text\' id=\'addon" + counter + "_name\' name=\'addon" + counter + "_name\' rows=\'2\' cols=\'15\' class=\'input-text\' />";    
    newChildValue.innerHTML = "<td class=\'label\'><label for=\'addon" + counter + "_value\'>Addon " + counter + " value</label></td>"
                         + "<td class=\'value\'><input type=\'text\' id=\'addon" + counter + "_value\' name=\'addon" + counter + "_value\' rows=\'2\' cols=\'15\' class=\'input-text\' /></td>";
    newChildAdditionalValue.innerHTML = "<td class=\'label\'><label for=\'addon" + counter + "_additional_value\'>Addon " + counter + " additional value</label></td>"
                         + "<td class=\'value\'><input type=\'text\' id=\'addon" + counter + "_additional_value\' name=\'addon" + counter + "_additional_value\' rows=\'2\' cols=\'15\' class=\'input-text\' /></td>";
    newChildPromo.innerHTML = "<td class=\'label\'><label for=\'addon" + counter + "_promo\'>Addon " + counter + " promo</label></td>"
                         + "<td class=\'value\'><input type=\'text\' id=\'addon" + counter + "_promo\' name=\'addon" + counter + "_promo_id\' rows=\'2\' cols=\'15\' class=\'input-text\' /></td>";
    newChildTechnical.innerHTML = "<td class=\'label\'><label for=\'addon" + counter + "_technical\'>Addon " + counter + " technical id</label></td>"
                         + "<td class=\'value\'><input type=\'text\' id=\'addon" + counter + "_technical\' name=\'addon" + counter + "_technical_id\' rows=\'2\' cols=\'15\' class=\'input-text\' /></td>";
    newChildNature.innerHTML = "<td class=\'label\'><label for=\'addon" + counter + "_nature\'>Addon " + counter + " nature code</label></td>"
                         + "<td class=\'value\'><input type=\'text\' id=\'addon" + counter + "_nature\' name=\'addon" + counter + "_nature_code\' rows=\'2\' cols=\'15\' class=\'input-text\' /></td>";
    newChildBackend.innerHTML = "<td class=\'label\'><label for=\'addon" + counter + "_backend\'>Addon " + counter + " backend</label></td>"
                         + "<td class=\'value\'><input type=\'text\' id=\'addon" + counter + "_backend\' name=\'addon" + counter + "_backend\' rows=\'2\' cols=\'15\' class=\'input-text\' /><p>&nbsp;</p></td>";
    
    prependedElement.parentNode.insertBefore(newChildName, prependedElement);
    prependedElement.parentNode.insertBefore(newChildValue, prependedElement);
    prependedElement.parentNode.insertBefore(newChildAdditionalValue, prependedElement);
    prependedElement.parentNode.insertBefore(newChildPromo, prependedElement);
    prependedElement.parentNode.insertBefore(newChildTechnical, prependedElement);
    prependedElement.parentNode.insertBefore(newChildNature, prependedElement);
    prependedElement.parentNode.insertBefore(newChildBackend, prependedElement);

    counter++;
}
//]]>
</script>');

        if ($mapperData = Mage::getSingleton("adminhtml/session")->getMapperData()) {
            $form->addValues($mapperData);
            Mage::getSingleton("adminhtml/session")->setMapperData(null);
            unset($mapperData);
        } elseif (Mage::registry("mapper_data")) {
            $form->addValues(Mage::registry("mapper_data")->getData());
        }

        return Mage_Adminhtml_Block_Widget_Form::_prepareForm();
    }
}
