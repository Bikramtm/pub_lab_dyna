<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$this->getConnection()->dropColumn($this->getTable('multimapper/mapper'), 'xpath_outgoing');
$this->getConnection()->dropColumn($this->getTable('multimapper/mapper'), 'xpath_incomming');

$this->getConnection()->addColumn('dyna_multi_mapper_addons', 'xpath_outgoing', 'varchar(255)');
$this->getConnection()->addColumn('dyna_multi_mapper_addons', 'xpath_incomming', 'varchar(255)');

$this->endSetup();
$installer->endSetup();
