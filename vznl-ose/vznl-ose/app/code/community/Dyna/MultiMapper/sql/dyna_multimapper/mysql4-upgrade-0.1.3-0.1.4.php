<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
$installer = $this;
$installer->startSetup();
//addons table
$addonsTable = 'dyna_multi_mapper_addons';
//Alter collumn addon_value change size fron 45 to 255s
$installer->getConnection()->changeColumn( $addonsTable, 'addon_value', 'addon_value', 'VARCHAR(255)');
//END Setup connection
$installer->endSetup();
