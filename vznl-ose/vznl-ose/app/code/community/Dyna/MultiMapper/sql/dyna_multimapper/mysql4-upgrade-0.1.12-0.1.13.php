<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$mapperTable = 'dyna_multi_mapper_index_process_context';

$installer->getConnection()
    ->addColumn($mapperTable, 'mapper_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => TRUE,
        'comment'   => 'Map to dyna_multi_mapper'
    ));

$installer->endSetup();
