<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 * OMNVFDE-2088 Add component_type field for FN in Multimapper
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
// multi mapper table
$mapperTable = 'dyna_multi_mapper';
//add new property 'component_type'
$installer->getConnection()->addColumn($mapperTable, 'component_type', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => TRUE,
    'comment'   => 'Component type field used by FN'
));
//END Setup connection
$installer->endSetup();
