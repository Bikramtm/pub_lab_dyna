<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

// Add indexes for Mapper Addons table

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$mapperTable = $installer->getTable('multimapper/mapper');
$addonsTable = $installer->getTable('dyna_multimapper/addon');

$mapperColumns = [
    'ogw_component_name' => [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'comment'   => 'OGW Component Name'
    ],
    'source_component_id' => [
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'unsigned'  => true,
        'comment'   => 'Source Component ID'
    ],
    'system_name' => [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'comment'   => 'System Name'
    ],
    'element_id' => [
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'unsigned'  => true,
        'comment'   => 'Element ID'
    ],
    'ogw_component_id_reference' => [
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'unsigned'  => true,
        'comment'   => 'OGW Component ID Reference'
    ]
];

$addonsColumns = [
    'service_expression' => [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment'   => 'Service Expression'
    ],
    'basket_action' => [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'comment'   => 'Basket Action'
    ],
    'component_element_id' => [
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'unsigned'  => true,
        'comment'   => 'Component Element ID'
    ],
    'attribute_action' => [
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'comment'   => 'Attribute Action'
    ],
    'direction' => [
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 4,
        'default'   => 'both',
        'nullable'  => false,
        'comment'   => 'Direction used to differentiate Reverse Multimappers'
    ]
];


foreach ($mapperColumns as $column => $data) {
    if ($connection->tableColumnExists($mapperTable, $column) === false) {
        $connection->addColumn($mapperTable, $column, $data);
    } else {
        continue;
    }
}

foreach ($addonsColumns as $column => $data) {
    if ($connection->tableColumnExists($addonsTable, $column) === false) {
        $connection->addColumn($addonsTable, $column, $data);
    } else {
        continue;
    }
}

$installer->endSetup();
