<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$mapperTable = 'dyna_multi_mapper_index_process_context';

//remove useless column, renamed in 0.1.11-0.1.12 installer but readded in 0.1.12-0.1.13
$installer->getConnection()
    ->dropColumn($mapperTable, 'mapper_id');

$installer->endSetup();
