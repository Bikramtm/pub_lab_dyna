<?php

/**
 * Class Dyna_MultiMapper_Model_Resource_Addon_Collection
 */
class Dyna_MultiMapper_Model_Resource_Addon_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("dyna_multimapper/addon");
    }
}
