<?php

/**
 * Class Dyna_MultiMapper_Model_Resource_Addon
 */
class Dyna_MultiMapper_Model_Resource_Addon extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init("dyna_multimapper/addon", "entity_id");
    }
}
