<?php

/**
 * Class Dyna_Magemonolog_Model_Logwriter
 */
class Dyna_Magemonolog_Model_Logwriter
  extends Aleron75_Magemonolog_Model_Logwriter
{
    /**
     * Dyna_Magemonolog_Model_Logwriter constructor.
     *
     * @param string $logFile
     */
    public function __construct($logFile)
    {
        parent::__construct($logFile);
        $this->_logger->setHandlers([]);
        $handlers = Mage::getStoreConfig('magemonolog/handlers');

        if (!is_null($handlers) && is_array($handlers)) {
            foreach ($handlers as $handlerModel => $handlerValues) {
                $isActive = Mage::getStoreConfigFlag('magemonolog/handlers/' . $handlerModel . '/active');
                if (!$isActive || !$this->isApproved($logFile, $handlerModel) || ($handlerModel == 'redisHandler') && Mage::registry('sandbox_import_no_redis_logging')) {
                    continue;
                }

                $args = array();
                if (array_key_exists('params', $handlerValues)) {
                    $args = $handlerValues['params'];
                }
                if (empty($args['stream'])) {
                    $args['stream'] = empty($this->_logFile) ? $args['defaultStream'] : $this->_logFile;
                }

                $handlerWrapper = Mage::getModel('aleron75_magemonolog/handlerWrapper_' . $handlerModel, $args);

                if (array_key_exists('formatter', $handlerValues)
                  && array_key_exists('class', $handlerValues['formatter'])
                ) {
                    $class = new ReflectionClass('\\Monolog\Formatter\\' . $handlerValues['formatter']['class']);
                    foreach($handlerValues['formatter']['args'] as $argName => &$arg) {
                        if ($arg === '0' || $arg === '1') {
                            $arg = (int) $arg;
                        }
                        if ($argName === 'format') {
                            $arg = str_replace(['\\r', '\\n'], ["\r", "\n"], $arg);
                        }
                    }
                    $formatter = $class->newInstanceArgs($handlerValues['formatter']['args']);

                    if (array_key_exists('formatter', $handlerValues)
                      && array_key_exists('lockedFormat', $handlerValues['formatter'])
                    ) {
                        $formatter->hasLockedFormat = (bool) $handlerValues['formatter']['lockedFormat'];
                    }

                    $handlerWrapper->setFormatter($formatter);
                }

                $this->_logger->pushHandler($handlerWrapper->getHandler());

                if (array_key_exists('processors', $handlerValues)) {
                    foreach ($handlerValues['processors'] as $processor) {
                        $processorInstance = new $processor['class']($processor['defaultArgs']);
                        $this->_logger->pushProcessor($processorInstance);
                    }
                }
            }
        }
    }

    /**
     * @param \Zend_Log_Formatter_Interface $formatter
     *
     * @return $this
     */
    public function setFormatter(Zend_Log_Formatter_Interface $formatter)
    {
        try {
            $this->_formatter = $formatter;
            $formatterReflection = new ReflectionClass($this->_formatter);
            $formatProperty = $formatterReflection->getProperty('_format');
            $formatProperty->setAccessible(true);
            $format = $formatProperty->getValue($formatter);
            foreach ($this->_logger->getHandlers() as $handler) {
                $formatter = $handler->getFormatter();
                if (property_exists($formatter, 'hasLockedFormat') && $formatter->hasLockedFormat) {
                    return $this;
                }
                $formatterReflection = new ReflectionClass($formatter);
                $formatProperty = $formatterReflection->getProperty('format');
                $formatProperty->setAccessible(true);
                $formatProperty->setValue($formatter, $this->getMonologFormat($format));
            }
        } catch (\Exception $e) {
        }
        return $this;
    }

    /**
     * Replace Zend logging format with compatible Monolog placeholders
     *
     * @param $zendFormat
     *
     * @return string
     */
    private function getMonologFormat($zendFormat)
    {
        return str_replace(
          array('%timestamp%', '%priorityName%', '%priority%'),
          array('%datetime%', '%level_name%', '%level%'),
          $zendFormat
        );
    }

    /**
     * Write a message using Monolog.
     *
     * @param  array $event  event data
     * @return void
     */
    protected function _write($event)
    {
        $logFilePathElements = explode('/', $this->_logFile);
        $logFileName = $logFilePathElements[count($logFilePathElements)-1];
        switch ($logFileName) {
            case 'system.log':
                $level = $this->_logger::NOTICE;
                break;
            case 'exception.log':
                $level = $this->_logger::ALERT;
                break;
            default:
                $level = $this->_levelMap[$event['priority']];
                break;
        }
        $message = $event['message'];
        $this->_logger->addRecord($level, $message);
    }
}
