<?php

/*
 * Fixing portal_state_initial attribute values
 */

$installer = $this;
/* @var $installer Dyna_Mobile_Model_Resource_Setup */

$installer->startSetup();

$attributeValues = Dyna_Cable_Model_Product_Attribute_Option::$portalStateInitialOptions;

/** @var Mage_Eav_Model_Attribute $attribute */
$attributeModel = Mage::getModel('eav/entity_attribute');
/** @var Mage_Eav_Model_Entity_Attribute_Source_Table $attributeOptionModel */
$attributeOptionModel = Mage::getModel('eav/entity_attribute_source_table');

$attributeId = $installer->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'portal_state_initial', 'attribute_id');

/** @var Mage_Eav_Model_Attribute $attribute */
$attribute = $attributeModel->load($attributeId);
$attributeOptionModel->setAttribute($attribute);
$options = $attributeOptionModel->getAllOptions(false);

$newOption = array();

foreach ($options as $option) {
    if (!in_array($option['label'], $attributeValues)) {
        $newOption['delete'][$option['value']] = true;
        $newOption['value'][$option['value']] = true;
        $installer->addAttributeOption($newOption);
    }
}

$installer->addAttributeOption($newOption);

$newOption = [
    'attribute_id' => $attributeId,
    'values' => [],
];

foreach ($attributeValues as $value) {
    if (!in_array($value, array_column($options, 'label'))) {
        $newOption['values'][] = $value;
    }
}

$installer->addAttributeOption($newOption);

$installer->endSetup();