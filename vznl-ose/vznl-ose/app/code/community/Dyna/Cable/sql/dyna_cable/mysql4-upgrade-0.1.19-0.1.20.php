<?php
// update the tier_code attribute type: instead of price use text
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Update attributes
$attributeCode = 'selectable';
$newOptions = [
    'label' => 'Selectable',
    'input' => 'boolean',
    'type' => 'int',
    'source' => 'eav/entity_attribute_source_boolean',
    'user_defined' => 1,
    'default' => 1,
    'note' => ''
];

$installer->addAttribute($entityTypeId, $attributeCode, $newOptions);

$installer->endSetup();
