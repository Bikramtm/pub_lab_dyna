<?php
    $installer = $this;
    /* @var $installer Dyna_Cable_Model_Resource_Setup */

    $entityTypeId = Mage_Catalog_Model_Product::ENTITY;

    $attributeId = $installer->getAttribute($entityTypeId, 'product_segment', 'attribute_id');

    if ($attributeId) {
        $installer->updateAttribute($entityTypeId, $attributeId, array('required' => false));
    }


$installer->endSetup();
