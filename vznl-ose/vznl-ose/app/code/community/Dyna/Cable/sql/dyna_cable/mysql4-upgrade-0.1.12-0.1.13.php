<?php
// remove service_type from KD_Cable_Products
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeCode = 'service_type';
$attributeSetCode = 'KD_Cable_Products';
$attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
$attId = $installer->getAttribute($entityTypeId, $attributeCode, 'attribute_id');

if ($attId && $attributeSetId) {
    try {
        Mage::getModel('catalog/product_attribute_set_api')->attributeRemove($attId, $attributeSetId);
    } catch (Exception $e) {
        // skip if error
    }
}

$installer->endSetup();
