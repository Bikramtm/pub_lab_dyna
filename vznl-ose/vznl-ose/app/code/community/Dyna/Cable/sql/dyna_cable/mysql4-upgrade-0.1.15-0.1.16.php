<?php
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeId = $installer->getAttribute($entityTypeId, 'required_bandwidth', 'attribute_id');

if ($attributeId) {
    $installer->updateAttribute($entityTypeId, $attributeId, array('is_filterable' => true));
}


$installer->endSetup();
