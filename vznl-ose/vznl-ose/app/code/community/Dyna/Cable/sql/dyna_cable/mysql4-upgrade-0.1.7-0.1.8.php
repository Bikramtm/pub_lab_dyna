<?php
/**
 * Installer that adds new attributes according to Cable proposal.docx (see OMNVFDE-133)
 */

$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$cableSet = 'KD_Cable_Products';

$attrSetId = $installer->getAttributeSetId($entityTypeId, $cableSet);

$generalAttributes = [
    'filter_category' => [
        'label' => 'Filter Category',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'relation_excludes' => [
        'label' => 'Relation Excludes',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'relation_excludes_selection' => [
        'label' => 'Relation Excludes Selection',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'relation_upgrades' => [
        'label' => 'Relation Upgrades',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'relation_options' => [
        'label' => 'Relation Options',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'relation_preselected_options' => [
        'label' => 'Relation Preselected Options',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'relation_inv_pres_options' => [
        'label' => 'Relation Inventory Preselected Options',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'relation_mandatory_options' => [
        'label' => 'Relation Mandatory Options',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'relation_components' => [
        'label' => 'Relation Components',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'relation_devices' => [
        'label' => 'Relation Devices',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'product_family' => [
        'label' => 'Product Family',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'portal_hide_tw_summary' => [
        'label' => 'Portal Hide TW Summary',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
    ],
    'marketable' => [
        'label' => 'Marketable',
        'input' => 'text',
        'type' => 'int',
        'note' => 'Number indicates that the product is active and number is the position between the group (for tv-device the number has 5 digits with codingInformation)',
    ],
    'droppable' => [
        'label' => 'Droppable',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
        'note' => '1=product (if it is in inventory) can be dropped from customer',
    ],
    'selectable' => [
        'label' => 'Selectable',
        'input' => 'select',
        'type' => 'int',
        'backend' => 'eav/entity_attribute_backend_array',
        'option' => [
            'values' => Dyna_Cable_Model_Product_Attribute_Option::$portalStateInitialOptions,
        ],
        'note' => 'set initial status of a checkbox Values: -: selectable, not selected, x: not selectable, not selected, X:not selectable, selected, +: selectable, selected',
    ],
    'channels_overview' => [
        'label' => 'Channels Overview',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
    ],
    'hide_summary' => [
        'label' => 'Hide Summary',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
        'note' => 'suppress the product in the order summary page',
    ],
    'hide_inventory' => [
        'label' => 'Hide inventory',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
        'note' => 'suppress the product in inventory (in CSC existing customer display)',
    ],
    'label' => [
        'label' => 'Display label',
        'input' => 'text',
        'type' => 'text',
        'note' => 'The product description in the different portal',
    ],
    'additional_text' => [
        'label' => 'Additional text',
        'input' => 'text',
        'type' => 'text',
        'note' => 'the additional text for a product (renamed from portal_display_add_line)',
    ],
    'premium_class' => [
        'label' => 'Premium Class',
        'input' => 'select',
        'type' => 'int',
        'backend' => 'eav/entity_attribute_backend_array',
        'option' => [
            'values' => Dyna_Cable_Model_Product_Attribute_Option::$hintsPremiumClassOptions,
        ],
        'note' => 'Describes the Assignment from product to the premiumClass (Standard or Premium)',
    ],
    'required_bandwidth' => [
        'label' => 'Required Bandwidth',
        'input' => 'text',
        'type' => 'int',
        'note' => 'max Bandwidth of the product',
    ],
    'contract_period' => [
        'label' => 'Contract Period',
        'input' => 'text',
        'type' => 'int',
        'note' => 'Specifies the time span for the contract : p.e. 24 (months)',
    ],
    'fee_priority' => [
        'label' => 'Fee Priority',
        'input' => 'text',
        'type' => 'int',
        'note' => 'indicates which priority a product has (min. value = max priority)',
    ],
];

$sortOrder = 100;
foreach ($generalAttributes as $code => $options) {
    $attributeId = $installer->getAttributeId($entityTypeId, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
        $installer->addAttributeToSet($entityTypeId, $attrSetId, 'Cable', $code, $sortOrder);
        $sortOrder++;
    }
}

/** Update <duration> attribute: rename it as contract_code */
$installer->updateAttribute($entityTypeId, 'duration', array(
    'attribute_code' => 'contract_code',
    'frontend_label' => 'Contract Code',
));

/** Update <price_billing_frequency> attribute values */
$attribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, 'price_billing_frequency');
$newOptionValues = array(
    'attribute_id' => $attribute->getId(),
);
$newAttributeValues = Dyna_Cable_Model_Product_Attribute_Option::$priceBillingFrequencyOptions;

$newOptionValues['values'][3] = $newAttributeValues[3];

$installer->addAttributeOption($newOptionValues);

$installer->endSetup();
