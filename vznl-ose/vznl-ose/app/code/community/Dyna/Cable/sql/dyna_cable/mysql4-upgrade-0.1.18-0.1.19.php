<?php

/**
 * OMNVFDE-3373
 * Added attributes:
 * display_price,
 * display_price_repeated
 *
 */

$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Create KD_Cable_Products attribute set
$attributeSetName = 'KD_Cable_Products';

// Create attributes
$attributes = [
    'display_price_repeated' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Price repeated display',
        'input' => 'text',
        'type' => 'text',
        'note' => 'Same behaviour as Price repeated but it is only used for display in the Configurator',
    ],
    'display_price' => [
        'attribute_set' => $attributeSetName,
        'group' => 'General',
        'label' => 'Display price',
        'input' => 'price',
        'type' => 'decimal',
        'note' => 'Displayed in the Configurator instead of Price',
    ],
    'display_price_sliding' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Display price sliding',
        'input' => 'text',
        'type' => 'text',
        'note' => 'Price scale used for display in configurator',
    ],
];

$createAttributeSets = [];
// Create the attributes
foreach ($attributes as $code => $options) {
    $attributeSet = $options['attribute_set'];
    $group = $options['group'];
    unset($options['group'], $options['attribute_set']);

    $attributeId = $installer->getAttributeId($entityTypeId, $code);
   // if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
    //}

    $createAttributeSets[$attributeSet]['groups'][$group][] = $code;
}
$sortOrder = 10;
$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($attributes, $createAttributeSets);

$installer->endSetup();
