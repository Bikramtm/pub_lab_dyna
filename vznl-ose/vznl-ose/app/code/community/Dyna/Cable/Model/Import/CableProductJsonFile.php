<?php

/**
 * Class Dyna_Cable_Model_Import_CableProductJsonFile
 *
 * Parsing recursively through all individual product data
 * If json nodes are numeric, their values will be imploded and assigned to parent node
 * If json nodes are not numeric and not found in the attributes of a product and not mapped to certain attributes,
 * installer will try to parse them from camel case to snake case
 * and if still not found in the attributes list, these nodes will be logged
 */
class Dyna_Cable_Model_Import_CableProductJsonFile extends Omnius_Import_Model_Import_Product_Abstract
{
    /** @var  Dyna_Import_Helper_Data $_helper */
    public $_helper;
    protected $logFileName = "cable_product_import_json";
    protected $newProducts = 0;
    protected $updatedProducts = 0;
    protected $productAttributes;
    protected $attributeSets;
    protected $attributeSet = "KD_Cable_Products";
    protected $_debug = false;
    /** @var $currentProduct Mage_Catalog_Model_Product  */
    protected $currentProduct;
    protected $attributeSetId;
    protected $websites = "telesales";
    protected $websitesIds = array();
    protected $cachedAttributes = array();
    protected $productVisibilityOptions = null;
    protected $specialNodesUsedForGrouping = array(
        'relations',
        'status',
        'properties',
        'display',
        'displayLabel',
    );
    protected $specialCaseNodes = array(
        'display',
        'tariffChange',
        'price',
        'products',
        'serviceCategory',
        'type',
        'selectable',
        'contractCode',
        'packageTypeId',
        'packageSubtypeId',
        'premiumClass',
        'productSegment',
        'productVisibility',
        'product_visibility',
        'displayLabel',
        'attributeSet',
        'optionTypes'
    );
    protected $mappedNodesToAttributes = array(
        'productId' => 'sku',
        'excludes' => 'relation_excludes',
        'upgrades' => 'relation_upgrades',
        'alternatives' => 'relation_alternatives',
        'options' => 'relation_options',
        'preselectOptions' => 'relation_preselected_options',
        'inventoryPreselectedOptions' => 'relation_inv_pres_options',
        'mandatoryOptions' => 'relation_mandatory_options',
        'components' => 'relation_components',
        'devices' => 'relation_devices',
        'fees' => 'relation_fees',
        'minBound' => 'req_rate_min_bound',
        'maxBound' => 'req_rate_max_bound',
        'initialSelectable' => 'initial_selectable',
        'optionRanking' => 'option_ranking'
    );

    protected $displayLabelMapping = array(
        'configurator' => 'display_name_configurator',
        'cart' => 'display_name_cart',
        'communication' => 'display_name_communication',
        'inventory' => 'display_name_inventory'
    );

    /**
     * Dyna_Cable_Model_Import_CableProductJsonFile constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper->setImportLogFile($this->logFileName . '.log');

        /** Load all attributes defined for product entity */
        $product = Mage::getModel('catalog/product');

        /* @var $eavConfig Mage_Eav_Model_Config */
        $eavConfig = Mage::getModel('eav/config');
        $this->productAttributes = $eavConfig->getEntityAttributeCodes(Mage_Catalog_Model_Product::ENTITY, $product);

        /** Set attribute set id for current instance */
        $this->attributeSetId = Mage::getModel("eav/entity_attribute_set")->getCollection()
            ->addFieldToFilter("attribute_set_name", $this->attributeSet)
            ->getFirstItem()
            ->getId();

        /** Set allowed websites */
        $allowedWebsites = explode(",", $this->websites);
        $websites = Mage::getModel('core/website')->getCollection()
                    ->addFieldToFilter('code', array('in' => $allowedWebsites));
        foreach ($websites as $website) {
            $this->websitesIds[] = $website->getId();
        }

        /** @var Mage_Eav_Model_Entity_Attribute_Set[] $attributeSets */
        $attributeSets = Mage::getModel("eav/entity_attribute_set")->getCollection()
                                ->addFieldToSelect('attribute_set_id')
                                ->addFieldToSelect('attribute_set_name');
        foreach ($attributeSets as $attr) {
            $this->attributeSets[$attr->getAttributeSetName()] = $attr->getId();
        }
    }

    /**
     * Main import method called from shell/import.php for --type cableProduct --file *.json
     * @param array $allProducts
     * @return mixed|void
     */
    public function importProduct($allProducts)
    {
        //@todo implement json_schema to validate whole file at once and prevent execution; postToSlack() when schema validation fails
        foreach ($allProducts as $product) {
            if ($this->_helper->hasValidPackageDefined($product, true)) {
                if (empty($product['attributeSet'])) {
                    $product['attributeSet'] = $this->attributeSet;
                }
                if (!in_array($product['attributeSet'], array_keys($this->attributeSets))) {
                    echo "(SKU \e[1;32m" . $product['productId'] . "\e[0m) Skipped entire product because of invalid \e[1;33mattributeSet\e[0m value: \e[1;31m" . $product['attributeSet'], "\033[0m\n";
                    $this->_logError("(SKU " . $product['productId'] . ") Skipped entire product because of invalid attributeSet value: " . $product['attributeSet']);

                    $this->_skippedFileRows++;
                } else {
                    /** Setting default product data */
                    $product['stack'] = $this->stack;
                    $this->saveProductData($product);
                }
            }else{
                $this->_skippedFileRows++;
            }
        }
        //used for logging purposes
        $this->_totalFileRows = (int)$this->updatedProducts + (int)$this->newProducts + (int)$this->_skippedFileRows;
        $this->_log("Finished: Updated " . (int)$this->updatedProducts . " products");
        $this->_log("Finished: Added " . (int)$this->newProducts . " new products");
    }

    /**
     * Checks whether or not product data has both package type and package subtype valid
     * @return bool
     */
    public function hasValidPackageDefined($productData)
    {
        $valid = true;
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');
        //get package types from db
        $types = $packageHelper->getPackageTypes();

        if(!empty($types)){
            $types = array_keys($types);
        }

        if (empty($productData['packageType']) || !in_array($productData['packageType'], $types)) {
            $productData['packageType'] = !empty($productData['packageType']) ? $productData['packageType'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['productId'] . "\e[0m) Skipped entire product because of invalid package \e[1;33mtype\e[0m value: \e[1;31m" . $productData['packageType'], "\033[0m (case sensitive)\n";
            $this->_logError("(SKU " . $productData['productId'] . ") Skipped entire product because of invalid package type value: " . $productData['packageType'] . " (case insensitive)");
            $valid = false;
        }
       
        if (empty($productData['packageSubtype']) || empty($packageHelper->getPackageSubtype($productData['packageType'], $productData['packageSubtype'], false, []))) {
            $productData['packageSubtype'] = !empty($productData['packageSubtype']) ? $productData['packageSubtype'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['productId'] . "\e[0m) Skipped entire product because of invalid package \e[1;33msubType\e[0m value: \e[1;31m" . $productData['packageSubtype'], "\033[0m  (case sensitive)\n";
            $this->_logError("(SKU " . $productData['productId'] . ") Skipped entire product because of invalid package subType value: " . $productData['packageSubtype'] . " (case sensitive)");
            $valid = false;
        }

        return $valid;
    }


    public function saveProductData($productData)
    {
        /** Try to load product by SKU (productId - unique identifier) */
        $this->currentProduct = Mage::getModel('catalog/product');
        $productId = $this->currentProduct->getIdBySku($productData['productId']);
        $new = false;
        if ($productId) {
            $this->currentProduct->load($productId);
            $this->_log('Updating existing product "' . $productData['productId'] . '" with ID ' . $this->currentProduct->getId(), true);
            $this->updatedProducts++;
        } else {
            $new = true;
            $this->currentProduct->setData('sku', $productData['productId']);
            $this->_log('Saving new product <' . $this->currentProduct->getSku() . '>', true);
            $this->newProducts++;
        }

        /** Setting default product data */
        $this->currentProduct->setAttributeSetId($this->attributeSetId);
        $this->currentProduct->setWebsiteIds($this->websitesIds);
        $this->currentProduct->setStatus(1);

        $this->scanData($productData);
        $this->_setDefaults($this->currentProduct, $productData);
        if ($new) {
            $this->_createStockItem($this->currentProduct);
        }

        /** Check if product has a tax_class defined */
        if (!$this->currentProduct->getTaxClassId() && !$this->currentProduct->getTaxClass()) {
            $this->setTaxClassIdByName("Taxable Goods");
        }
        $this->setCheckoutProduct($productData);
        $this->currentProduct->save();

        unset($this->currentProduct);
    }

    /**
     * @param $data
     * @return bool|string
     */
    protected function scanData($data)
    {
        /** Has nodes beneath */
        if (is_array($data)) {
            /** Has numeric nodes, that means values need to be imploded and assigned to parent node */
            if ($this->hasNumericKeys($data)) {
                return implode(",", $data);
            }

            /** Try to scan deeper through nodes */
            foreach ($data as $key => $value) {
                $newValue = false;

                /** Check if this key is in the special case list */
                if (in_array($key, $this->specialCaseNodes)) {
                    $this->parseSpecialCase($key, $value);
                } else {
                    /** Not a special case, scan deeper for json nodes */
                    $newValue = $this->scanData($value);
                }

                /** If value exists, we need to assign it to current key */
                if ($newValue) {
                    if ($newValue && in_array($key, $this->specialNodesUsedForGrouping)) {
                        $this->_log("(SKU " . $this->currentProduct->getSku() . ") Received value <" . $newValue . "> for a grouping node <" . $key . ">. Trying to map this key to an attribute. This grouping key should be changed.", true);
                    }

                    $assigned = false;
                    /** Check if this key is mapped to an attribute */
                    if (array_key_exists($key, $this->mappedNodesToAttributes)) {
                        $this->currentProduct->setData($this->mappedNodesToAttributes[$key], $newValue);
                        $assigned = true;
                    }

                    /** Check if attribute exists for this key */
                    if (!$assigned && in_array($key, $this->productAttributes)) {
                        $this->currentProduct->setData($key, $newValue);
                        $assigned = true;
                    }

                    /** Check if conversion from camel case to snake case results in an existing attribute */
                    if (!$assigned && in_array($this->_replaceUpper($key), $this->productAttributes)) {
                        $this->currentProduct->setData($this->_replaceUpper($key), $newValue);
                        $assigned = true;
                    }

                    /** If none of the above and node is not in used for grouping list, log notice that attribute does not exists */
                    if (!$assigned) {
                        $this->_logError("(SKU " . $this->currentProduct->getSku() . ") Skipped adding value <" . $newValue . "> because of unknown attribute <" . $key . ">.");
                    }
                }
            }
        } else {
            return $data;
        }

        /** default return: no value */
        return false;
    }

    /**
     * Check if current array contains numeric keys, if yes it will further be imploded and resulting value will be assigned to parent node
     *
     * @param $array
     * @return bool
     */
    protected function hasNumericKeys($array)
    {
        $numeric = true;

        foreach ($array as $key => $value) {
            if (!is_numeric($key)) {
                return false;
            }
        }

        return $numeric;
    }

    /**
     * @param $key
     * @param $value
     */
    protected function parseSpecialCase($key, $value)
    {
        switch ($key) {
            case "display":
                $this->parseDisplay($value);
                break;
            case "tariffChange":
                $this->parseTariffChange($value);
                break;
            case "price":
                $this->parsePrice($value);
                break;
            case "products":
                $this->parseRequiredProducts($value);
                break;
            case "serviceCategory":
                $this->parseServiceCategory($value);
                break;
            case "type":
                $this->parseType($value);
                break;
            case "selectable":
                $this->parseSelectable($value);
                break;
            case "contractCode":
                $this->parseContractCode($value);
                break;
            case "packageTypeId":
                $this->parsePackageType($value);
                break;
            case "premiumClass":
                $this->parsePremiumClass($value);
                break;
            case "productSegment":
                $value = ucfirst(strtolower(trim($value)));
                $attribute = "product_segment";
                $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$productSegmentOptions);
                break;
            case "packageSubtypeId":
                $value = trim($value);
                $attribute = "package_subtype";
                $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::packageSubtypeOptions());
                break;
            case "productVisibility":
            case "product_visibility":
                $this->parseProductVisibility($value);
                break;
            case "displayLabel" :
                $this->parseDisplayLabel($value);
                break;
            case "attributeSet" :
                $this->parseAttributeSet($value);
                break;
            case "optionTypes" :
                $this->parseCableOptionTypes($value);
                break;
            default :
                $this->_logError("(SKU " . $this->currentProduct->getSku() . ") special case defined for node <" . $key . "> but not treated in parseSpecialCase method.");
                break;
        }
    }

    /**
     * Parsing tariffChange values
     * @param $value array
     */
    protected function parseTariffChange($value)
    {
        $value['action'] = strtoupper(trim($value['action']));
        $attribute = "tariff_change_action";

        if ($value['action'] == "REPLACE" && !empty($value['replaceBy'])) {
            if (empty($this->cachedAttributes[$attribute][$value['action']])) {
                $attr = $this->currentProduct->getResource()->getAttribute($attribute);
                if ($attr->usesSource()) {
                    $this->cachedAttributes[$attribute][$value['action']] = $attr->getSource()->getOptionId($value['action']);
                } else {
                    $this->_logError("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it is not a select or multiselect type.");
                    return;
                }
            }
            $this->currentProduct->setData($attribute, $this->cachedAttributes[$attribute][$value['action']]);
            $this->currentProduct->setTariffChangeReplaceBy($value['replaceBy']);
        } elseif ($value['action'] == "KEEP") {
            if (empty($this->cachedAttributes[$attribute][$value['action']])) {
                $attr = $this->currentProduct->getResource()->getAttribute($attribute);
                if ($attr->usesSource()) {
                    $this->cachedAttributes[$attribute][$value['action']] = $attr->getSource()->getOptionId($value['action']);
                } else {
                    $this->_logError("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it is not a select or multiselect type.");
                    return;
                }
            }
            $this->currentProduct->setData($attribute, $this->cachedAttributes[$attribute][$value['action']]);
        } else {
            $this->_logError("(SKU " . $this->currentProduct->getSku() . ") do not know what to do with <tariffChange>, dumping array to logs:");
            $this->_logError($value);
        }
    }

    /**
     * Parsing price combination values
     * @param $value array
     * @param $isDisplay bool
     */
    protected function parsePrice($value, $isDisplay = false)
    {
        $priceSliding = "";
        $priceRepeated = "";
        $priceOnce = "";
        $vatClass = '';

        // Handle one price node
        if (!isset($value[0])) {
            $value = [$value];
        }

        if ($isDisplay) {
            $txt = 'display price';
        } else {
            $txt = 'price';
        }
        foreach ($value as $price) {
            if (!empty($price['billingFrequency'])) {
                $this->parseBillingFrequency($price['billingFrequency']);

                if (strtolower(trim($price['billingFrequency'])) == 'monthly') {
                    $priceRepeated .= $priceRepeated !== "" ? ":" . (string)$price['amount'] : (string)$price['amount'];
                    $period = !empty($price['periodStart']) && !empty($price['periodEnd']) ? $price['periodEnd'] - $price['periodStart'] + 1 : "";
                    $priceSliding .= !empty($priceSliding) ? ":" . $period : $period;
                } elseif (strtolower(trim($price['billingFrequency'])) == 'once') {
                    $priceOnce = $price['amount'];
                } else {
                    $this->_logError("(SKU " . $this->currentProduct->getSku() . ") Skipping $txt value: invalid billing frequency <" . $price['billingFrequency'] . ">");
                }

            } else {
                $this->_logError("(SKU " . $this->currentProduct->getSku() . ") Skipping $txt value: no billing frequency set.");
            }
            // check vat value and tax
            if (!empty($price['includedVat'])) {
                $vatValue = (int)str_replace("%", "", $price['includedVat']);
                $vatClass = Mage::helper('dyna_catalog')->getVatTaxClass($vatValue);
            }
        }

        if ($isDisplay) {
            $this->currentProduct->setData('display_price_sliding', $priceSliding);
            $this->currentProduct->setData('display_price_repeated', $priceRepeated);
            /** OMNVFDE-345: Map priceOnce to price and log if priceOnce is not defined */
            if ($priceOnce) {
                $this->currentProduct->setData('display_price', $priceOnce);
            } else {
                $this->currentProduct->setData('display_price', 0);
                $this->_log("(SKU " . $this->currentProduct->getSku() . ") No display_price once received, setting price 0.");
            }
        } else {
            $this->currentProduct->setData('price_sliding', $priceSliding);
            $this->currentProduct->setData('price_repeated', $priceRepeated);
            if ($priceOnce) {
                $this->currentProduct->setData('price', $priceOnce);
            } else {
                $this->currentProduct->setData('price', 0);
                $this->_log("(SKU " . $this->currentProduct->getSku() . ") No price_once received, setting price 0.");
            }
        }


        $this->setTaxClassIdByName($vatClass);
    }

    /**
     * @param $value
     */
    protected function parseBillingFrequency($value)
    {
        $attribute = "price_billing_frequency";

        if (empty($this->cachedAttributes[$attribute][$value])) {
            $attr = $this->currentProduct->getResource()->getAttribute($attribute);
            if ($attr->usesSource()) {
                $this->cachedAttributes[$attribute][$value] = $attr->getSource()->getOptionId($value);
            } else {
                $this->_logError("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it is not a select or multiselect type.");
                return;
            }
        }
        $this->currentProduct->setData($attribute, $this->cachedAttributes[$attribute][$value]);

    }

    /**
     * Set product class id based on the provided tax_class_id in the CSV file
     *
     * @param string $className
     * @return bool
     */
    protected function setTaxClassIdByName($className)
    {
        $productTaxClass = Mage::getModel('tax/class')
            ->getCollection()
            ->addFieldToFilter('class_name', $className)
            ->load()
            ->getFirstItem();
        if (!$productTaxClass || !$productTaxClass->getId()) {
            return false;
        }
        $this->currentProduct->setTaxClassId($productTaxClass->getId());

        return true;
    }

    /**
     * Special case for productsForContracts attribute (omnius: req_rate_prod_for_contracts)
     *
     * @param $value array
     */
    protected function parseRequiredProducts($value)
    {
        $newValue = Mage::helper('core')->jsonEncode($value);
        $this->currentProduct->setData('req_rate_prod_for_contracts', $newValue);
    }

    /**
     * @param $value
     */
    protected function parseServiceCategory($value)
    {
        $attribute = "service_category";
        $value = strtoupper($value);

        $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$serviceCategoryOptions);
    }

    /**
     * @param $attribute
     * @param $value
     * @param $allowedValues
     */
    protected function parseSelectTypeAttribute($attribute, $value, $allowedValues)
    {
        if (in_array($value, $allowedValues)) {
            if (empty($this->cachedAttributes[$attribute][$value])) {
                $attr = $this->currentProduct->getResource()->getAttribute($attribute);
                if ($attr->usesSource()) {
                    $this->cachedAttributes[$attribute][$value] = $attr->getSource()->getOptionId($value);
                } else {
                    $this->_logError("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it is not a select or multiselect type.");
                    return;
                }
            }
            $this->currentProduct->setData($attribute, $this->cachedAttributes[$attribute][$value]);
        } else {
            $this->_logError("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because value <" . $value . "> it is not in the defined list of values.");
        }
    }

    /**
     * @param $value
     */
    protected function parseType($value)
    {
        $attribute = "type";
        $value = ucfirst(strtolower($value));

        $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$typeOptions);
    }

    /**
     * @param $value
     */
    protected function parseSelectable($value)
    {
        $attribute = "selectable";

        if (is_bool($value)) {
            $this->currentProduct->setData($attribute, (int)$value);
        } else {
            $this->_logError("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it contains the value <" . $value . "> other than true or false.");
        }
    }

    /**
     * @param $value
     */
    protected function parseContractCode($value)
    {
        $attribute = "contract_code";
        $value = strtoupper($value);

        $this->parseSelectTypeAttribute(
            $attribute,
            $value,
            Dyna_Cable_Model_Product_Attribute_Option::$durationOptions
        );
    }

    /**
     * @param $value
     */
    protected function parsePackageType($value)
    {
        $value = trim($value);
        $attribute = "package_type";

        $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::packageTypeOptions(false));
    }

    /**
     * @param $value
     */
    protected function parsePremiumClass($value)
    {
        $value = ucfirst(strtolower(trim($value)));
        $attribute = "premium_class";

        $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$hintsPremiumClassOptions);
    }

    /**
     * @param $values
     */
    protected function parseDisplay($values) {
        if (isset($values['price'])) {
            $this->parsePrice($values['price'], true);
        }
        if (isset($values['productVisibility'])) {
            $this->parseProductVisibility($values['productVisibility']);
        }
        if (isset($values['displayLabel'])) {
            $this->parseDisplayLabel($values['displayLabel']);
        }
        if (isset($values['contractPeriod'])) {
            $this->currentProduct->setData('contract_period', $values['contractPeriod']);
        }
        if (isset($values['additionalText'])) {
            $this->currentProduct->setData('additional_text', $values['additionalText']);
        }
    }

    /**
     * Method to parse the value of the product visibility attributes
     * @param $values
     */
    protected function parseProductVisibility($values){
        $markedOptions = array();
        $markedOptionIds = array();
        foreach($values as $optionName => $value){
            if($value === true){
                $markedOptions[] = strtolower(str_replace(' ', '', $optionName));
            }
        }
        if(!$this->_productVisibilityOptions){
            $attribute = Mage::getSingleton('eav/config')
                ->getAttribute(Mage_Catalog_Model_Product::ENTITY, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);

            if ($attribute->usesSource()) {
                $options = $attribute->getSource()->getAllOptions(false);
            }
        }

        foreach($options as $option){
            if(in_array(strtolower(str_replace(' ', '', $option['label'])), $markedOptions)){
                $markedOptionIds[] = $option['value'];
            }
        }
        if($markedOptionIds){
            $this->currentProduct->setData(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR, $markedOptionIds);
        }
    }

    /**
     * Method for handling updating display label for cable products
     */
    public function parseDisplayLabel($value)
    {
        foreach ($value as $key => $attributeValue) {
            if (isset($this->displayLabelMapping[$key])) {
                $key = $this->displayLabelMapping[$key];
            }
            $this->currentProduct->setData($key, $attributeValue);
        }
    }

    public function parseAttributeSet($name)
    {
        $this->currentProduct->setAttributeSetId($this->attributeSets[$name]);
    }

    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        // This will format itself trough observer
        $product->setUrlKey($data['name']);

        // Add product_code and promotion_code from productId
        $parts = explode(":", $data['productId']);
        $product->setProductCode($parts[0]);

        if (isset($parts[1])) {
            $product->setPromotionCode($parts[1]);
        }

        $product->setProductVisibilityStrategy('all');
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setIsDeleted('0');
    }

    /**
     * @param $productData
     * OMNVFDE-2082
     * The checkout_product attribute will be used to display the products in the cart
     * For cable this attribute is true if the package subtype = CheckoutOption
     */
    protected function setCheckoutProduct($productData)
    {
        if(isset($productData['packageSubtype']) && $productData['packageSubtype'] &&
        $productData['packageSubtype'] == Dyna_Catalog_Model_Type::SUBTYPE_CHECKOUTOPTION) {
            $this->currentProduct->setCheckoutProduct(1);
        }
        else {
            $this->currentProduct->setCheckoutProduct(0);
        }
    }

    public function setImportHelper($helper) {
        $this->_helper = $helper;
    }

    /**
     * Log Error function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    /**
     * Parse option_type attributes for cable
     * @param $values
     * @return string
     * @internal param $value
     */
    protected function parseCableOptionTypes($values){
        $markedOptionIds = [];
        $options = [];

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_OPTION_TYPE_ATTR);
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        if(!empty($values)) {
            foreach ($options as $option) {
                if (in_array($option['label'], $values)) {
                    $markedOptionIds[] = $option['value'];
                }
            }
        }

        if($markedOptionIds){
            $this->currentProduct->setData(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_OPTION_TYPE_ATTR,  $markedOptionIds);
        }

    }
}
