<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$dropSQL = <<<SQL
DROP TABLE IF EXISTS dealer_group_link;
DROP TABLE IF EXISTS dealer_group;
SQL;
$installer->run($dropSQL);

/** Agent Group table */
$dealerGroupTable = new Varien_Db_Ddl_Table();
$dealerGroupTable->setName('dealer_group');
$dealerGroupTable->addColumn(
    'group_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10,
    array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
);
$dealerGroupTable->addColumn(
    'name',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    100
);

$dealerGroupTable->setOption('type', 'InnoDB');
$dealerGroupTable->setOption('charset', 'utf8');
$this->getConnection()->createTable($dealerGroupTable);

$linkTable = new Varien_Db_Ddl_Table();
$linkTable
    ->setName('dealer_group_link')
    ->addColumn(
        'link_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    )
    ->addColumn(
        'dealer_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    )
    ->addColumn(
        'group_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    )
    ->addForeignKey(
        'fk_dealer_id',
        'dealer_id',
        Mage::getSingleton('core/resource')->getTableName('agent/dealer'),
        'dealer_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        'fk_group_id',
        'group_id',
        Mage::getSingleton('core/resource')->getTableName('agent/dealergroup'),
        'group_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setOption('type', 'InnoDB')
    ->setOption('charset', 'utf8');
$this->getConnection()->createTable($linkTable);

$installer->endSetup();
