<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Add agent specific fields to quote and order tables
 */

$installer = $this;

$installer->startSetup();

$installer->addAttribute(
    'order',
    'dealer_id',
    array(
        'type' => 'int'
    )
);
$installer->addAttribute(
    'order',
    'agent_id',
    array(
        'type' => 'int'
    )
);
$installer->addAttribute(
    'order',
    'super_agent_id',
    array(
        'type' => 'int'
    )
);


$installer->addAttribute(
    'quote',
    'dealer_id',
    array(
        'type' => 'int'
    )
);
$installer->addAttribute(
    'quote',
    'agent_id',
    array(
        'type' => 'varchar'
    )
);
$installer->addAttribute(
    'quote',
    'super_agent_id',
    array(
        'type' => 'int'
    )
);

$installer->endSetup();
	 