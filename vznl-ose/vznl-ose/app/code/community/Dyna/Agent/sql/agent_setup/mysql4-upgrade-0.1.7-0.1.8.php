<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();
$table = $this->getTable('channel_override');

if (! $connection->tableColumnExists($table, 'created_admin_id')) {
    $connection->addColumn($table, 'created_admin_id',
        'INT(10) UNSIGNED DEFAULT NULL'
    );
    $connection->addForeignKey('FK_CHANNEL_OVERRIDE_CREATED_ADMIN_ID', $table, 'created_admin_id', 'admin_user', 'user_id',  Varien_Db_Adapter_Interface::FK_ACTION_SET_NULL, Varien_Db_Adapter_Interface::FK_ACTION_CASCADE);
}

if (! $connection->tableColumnExists($table, 'updated_admin_id')) {
    $connection->addColumn($table, 'updated_admin_id',
        'INT(10) UNSIGNED DEFAULT NULL'
    );
    $connection->addForeignKey('FK_CHANNEL_OVERRIDE_UPDATED_ADMIN_ID', $table, 'updated_admin_id', 'admin_user', 'user_id',  Varien_Db_Adapter_Interface::FK_ACTION_SET_NULL, Varien_Db_Adapter_Interface::FK_ACTION_CASCADE);
}

$installer->endSetup();