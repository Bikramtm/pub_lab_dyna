<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

$installer->startSetup();

$sql = <<<SQL
DROP TABLE IF EXISTS `agent_group`;
SQL;

if ($installer->tableExists('catalog_product_agent_visibility')) {
    foreach ($connection->getForeignKeys('catalog_product_agent_visibility') as $foreignKeyId => $definition) {
        if ($definition['REF_TABLE_NAME'] == 'agent_group') {
            $connection->dropForeignKey('catalog_product_agent_visibility', $foreignKeyId);
        }
    }

    $connection
        ->addForeignKey(
            $this->getFkName('catalog_product_agent_visibility', 'group_id', 'agent/dealergroup', 'group_id'),
            'catalog_product_agent_visibility',
            'group_id',
            'dealer_group',
            'group_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_CASCADE
        );

    $connection->query($sql);
}

$installer->endSetup();
