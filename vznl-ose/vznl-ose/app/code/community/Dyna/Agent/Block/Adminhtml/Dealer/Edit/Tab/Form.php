<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Dealer_Edit_Tab_Form
 */
class Dyna_Agent_Block_Adminhtml_Dealer_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Dealer Information")));


        $fieldset->addField("street", "text", array(
            "label" => Mage::helper("agent")->__("Street"),
            "class" => "required-entry",
            "required" => true,
            "name" => "street",
        ));

        $fieldset->addField("postcode", "text", array(
            "label" => Mage::helper("agent")->__("Postal Code"),
            "class" => "required-entry",
            "required" => true,
            "name" => "postcode",
        ));

        $fieldset->addField("house_nr", "text", array(
            "label" => Mage::helper("agent")->__("House Nr."),
            "class" => "required-entry",
            "required" => true,
            "name" => "house_nr",
        ));

        $fieldset->addField("city", "text", array(
            "label" => Mage::helper("agent")->__("City"),
            "class" => "required-entry",
            "required" => true,
            "name" => "city",
        ));

        $fieldset->addField("telephone", "text", array(
            "label" => Mage::helper("agent")->__("Telephone"),
            "class" => "required-entry",
            "required" => true,
            "name" => "telephone",
        ));
        
        $fieldset->addField('store_id', 'select', array(
            'label' => Mage::helper('agent')->__('Store'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
            'name' => 'store_id',
            "class" => "required-entry",
            "required" => true,
        ));
        
        $fieldset->addField("axi_store_code", "text", array(
            "label" => Mage::helper("agent")->__("AXI store code"),
            "name" => "axi_store_code",
        ));
        
        $fieldset->addField("vf_dealer_code", "text", array(
            "label" => Mage::helper("agent")->__("VF dealer code"),
            "name" => "vf_dealer_code",
        ));

        $fieldset->addField('group_id', 'multiselect', array(
            'label' => Mage::helper('agent')->__('Dealer Group ID'),
            'values' => Dyna_Agent_Block_Adminhtml_Dealer_Grid::getDealerGroups(),
            'name' => 'group_id',
            "class" => "required-entry",
            "required" => true,
        ));

        $fieldset->addField("naw_data_dealer_code", "text", array(
            "label" => Mage::helper("agent")->__("NAW DataDealer Code"),
            "name" => "naw_data_dealer_code",
        ));

        $fieldset->addField("axi_code", "text", array(
            "label" => Mage::helper("agent")->__("AXI Code"),
            "name" => "axi_code",
        ));

        $fieldset->addField("paid_code", "text", array(
            "label" => Mage::helper("agent")->__("Paid Code"),
            "name" => "paid_code",
        ));

        if (Mage::getSingleton("adminhtml/session")->getDealerData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getDealerData());
            Mage::getSingleton("adminhtml/session")->setDealerData(null);
        } elseif (Mage::registry("dealer_data")) {
            $form->setValues(Mage::registry("dealer_data")->getData());
        }
        return parent::_prepareForm();
    }
}
