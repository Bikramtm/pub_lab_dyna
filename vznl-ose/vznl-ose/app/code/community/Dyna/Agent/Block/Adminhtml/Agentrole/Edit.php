<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Agentrole_Edit
 */
class Dyna_Agent_Block_Adminhtml_Agentrole_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = "role_id";
        $this->_blockGroup = "agent";
        $this->_controller = "adminhtml_agentrole";
        $this->_updateButton("save", "label", Mage::helper("agent")->__("Save Agent Role"));
        $this->_updateButton("delete", "label", Mage::helper("agent")->__("Delete Agent Role"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("agent")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    public function getHeaderText()
    {
        if (Mage::registry("agentrole_data") && Mage::registry("agentrole_data")->getId()) {
            return Mage::helper("agent")->__("Edit Agent Role '%s'",
                $this->htmlEscape(Mage::registry("agentrole_data")->getRoleId()));
        } else {
            return Mage::helper("agent")->__("Add Agent Role");
        }
    }
}