<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Dealergroup_Grid
 */
class Dyna_Agent_Block_Adminhtml_Dealergroup_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("dealergroupGrid");
        $this->setDefaultSort("group_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("agent/dealergroup")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn("group_id", array(
            "header" => Mage::helper("agent")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "group_id",
        ));

        $this->addColumn("name", array(
            "header" => Mage::helper("agent")->__("Group Name"),
            "index" => "name",
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('group_id');
        $this->getMassactionBlock()->setFormFieldName('group_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_dealergroup', array(
            'label' => Mage::helper('agent')->__('Remove Dealer Group'),
            'url' => $this->getUrl('*/adminhtml_dealergroup/massRemove'),
            'confirm' => Mage::helper('agent')->__('Are you sure?')
        ));
        return $this;
    }
}
