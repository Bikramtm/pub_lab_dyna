<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Dealergroup_Edit_Tab_Form
 */
class Dyna_Agent_Block_Adminhtml_Dealergroup_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Dealer Group Information")));


        $fieldset->addField("name", "text", array(
            "label" => Mage::helper("agent")->__("Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "name",
        ));

        if (Mage::getSingleton("adminhtml/session")->getDealergroupData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getDealergroupData());
            Mage::getSingleton("adminhtml/session")->setDealergroupData(null);
        } elseif (Mage::registry("dealergroup_data")) {
            $form->setValues(Mage::registry("dealergroup_data")->getData());
        }
        return parent::_prepareForm();
    }
}
