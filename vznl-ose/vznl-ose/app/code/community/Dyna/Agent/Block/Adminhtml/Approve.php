<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Approve
 */
class Dyna_Agent_Block_Adminhtml_Approve extends Mage_Core_Block_Template
{
    protected $_template = 'dyna_agent/approve_import.phtml';

    public function getReferrer()
    {
        return Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer()  : Mage::getUrl('admin');
    }
}