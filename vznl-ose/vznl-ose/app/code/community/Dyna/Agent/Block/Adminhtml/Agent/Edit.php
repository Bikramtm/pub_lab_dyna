<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Agent_Edit
 */
class Dyna_Agent_Block_Adminhtml_Agent_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = "agent_id";
        $this->_blockGroup = "agent";
        $this->_controller = "adminhtml_agent";
        $this->_updateButton("save", "label", Mage::helper("agent")->__("Save Agent"));
        $this->_updateButton("delete", "label", Mage::helper("agent")->__("Delete Agent"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("agent")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_addButton("unlock", array(
            "label" => Mage::helper("agent")->__("Unlock Agent"),
            'onclick' => 'deleteConfirm(\'' . Mage::helper('adminhtml')->__('Are you sure you want to do this?')
                . '\', \'' . $this->getUnlockUrl() . '\')',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    public function getHeaderText()
    {
        if (Mage::registry("agent_data") && Mage::registry("agent_data")->getId()) {
            return sprintf(Mage::helper("agent")->__("Edit Agent '%s' (%s)"),
                $this->htmlEscape(Mage::registry("agent_data")->getId()),
                Mage::registry("agent_data")->getLocked() ? Mage::helper("agent")->__('Locked') : Mage::helper("agent")->__('Unlocked'));
        } else {
            return Mage::helper("agent")->__("Add Agent");
        }
    }

    public function getUnlockUrl()
    {
        return $this->getUrl('*/*/unlock', array($this->_objectId => Mage::registry("agent_data")->getId()));
    }
}