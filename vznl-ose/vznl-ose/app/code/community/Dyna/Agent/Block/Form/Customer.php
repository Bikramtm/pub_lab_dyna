<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Customer dealer form block
 */
class Dyna_Agent_Block_Form_Customer extends Mage_Core_Block_Template
{
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('customer')->__('Customer'));
        return parent::_prepareLayout();
    }

    /**
     * Retrieve form posting url
     *
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->helper('agent')->getCustomerPostUrl();
    }

    public function getCustomers()
    {
        /** @var Mage_Customer_Model_Resource_Customer_Collection $collection */
        return Mage::getModel('customer/customer')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->load();
    }
}
