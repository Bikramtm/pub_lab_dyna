<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Permission_Edit
 */
class Dyna_Agent_Block_Adminhtml_Permission_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = "entity_id";
        $this->_blockGroup = "agent";
        $this->_controller = "adminhtml_permission";
        $this->_updateButton("save", "label", Mage::helper("agent")->__("Save Permission"));
        $this->_updateButton("delete", "label", Mage::helper("agent")->__("Delete Permission"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("agent")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    public function getHeaderText()
    {
        if (Mage::registry("permission_data") && Mage::registry("permission_data")->getId()) {
            return Mage::helper("agent")->__("Edit Permission '%s'",
                $this->htmlEscape(Mage::registry("permission_data")->getId()));
        } else {
            return Mage::helper("agent")->__("Add Permission");
        }
    }
}