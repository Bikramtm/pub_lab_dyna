<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Dyna_Agent_Block_Adminhtml_Permission
 */
class Dyna_Agent_Block_Adminhtml_Permission extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_permission";
        $this->_blockGroup = "agent";
        $this->_headerText = Mage::helper("agent")->__("Permissions Manager");
        $this->_addButtonLabel = Mage::helper("agent")->__("Add New Permission");

        parent::__construct();
    }
}