<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Dealer_Import_Form
 */
class Dyna_Agent_Block_Adminhtml_Dealer_Import_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                "id" => "upload_form",
                "action" => $this->getUrl("*/*/importDealers"),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );

        $fieldset = $form->addFieldset("dealer_form", array("legend" => Mage::helper("agent")->__("Import File Info")));

        $fieldset->addField('file', 'file', array(
            'label' => Mage::helper('agent')->__('File'),
            'name' => 'file',
            "class" => "required-entry",
            "required" => true,
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}