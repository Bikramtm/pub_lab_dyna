<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Dyna_Agent_Block_Adminhtml_Agentrole
 */
class Dyna_Agent_Block_Adminhtml_Agentrole extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_agentrole";
        $this->_blockGroup = "agent";
        $this->_headerText = Mage::helper("agent")->__("Agent Roles Manager");
        $this->_addButtonLabel = Mage::helper("agent")->__("Add New Agent Role");

        parent::__construct();
    }
}