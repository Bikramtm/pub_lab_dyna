<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Agent_Import_Form
 */
class Dyna_Agent_Block_Adminhtml_Agent_Import_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                "id" => "upload_form",
                "action" => $this->getUrl("*/*/importAgents"),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Import File Info")));

        $fieldset->addField('file', 'file', array(
            'label' => Mage::helper('agent')->__('File'),
            'name' => 'file',
            "class" => "required-entry",
            "required" => true,
        ));

        return parent::_prepareForm();
    }
}