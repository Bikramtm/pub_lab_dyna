<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Dealer_Edit
 */
class Dyna_Agent_Block_Adminhtml_Dealer_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = "dealer_id";
        $this->_blockGroup = "agent";
        $this->_controller = "adminhtml_dealer";
        $this->_updateButton("save", "label", Mage::helper("agent")->__("Save Dealer"));
        $this->_updateButton("delete", "label", Mage::helper("agent")->__("Delete Dealer"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("agent")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    public function getHeaderText()
    {
        if (Mage::registry("dealer_data") && Mage::registry("dealer_data")->getId()) {
            return Mage::helper("agent")->__("Edit Dealer '%s'", $this->htmlEscape(Mage::registry("dealer_data")->getId()));
        } else {
            return Mage::helper("agent")->__("Add Dealer");
        }
    }
}