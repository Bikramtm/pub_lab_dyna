<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Dealer_Grid
 */
class Dyna_Agent_Block_Adminhtml_Dealer_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("dealerGrid");
        $this->setDefaultSort("dealer_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("agent/dealer")->getCollection()
            ->addFieldToFilter(
                array('is_deleted', 'is_deleted'),
                array(
                    array('null' => true),
                    array('neq' => 1)
                )
            );

        $columnId = $this->getParam($this->getVarNameSort(), $this->_defaultSort);
        if ('group_id' == $columnId) {
            $collection->getSelect()
                ->joinLeft(array('r' => 'dealer_group_link'), '`r`.`dealer_id` = `main_table`.`dealer_id` ', array('group' => 'group_id'))
                ->columns('GROUP_CONCAT( `r`.`group_id` SEPARATOR \',\') AS group_id')
                ->group('main_table.dealer_id');
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @param Dyna_Agent_Model_Mysql4_Dealer_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     */
    public function filterGroupIds($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        /** @var Varien_Db_Adapter_Interface $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $preparedValue = array_map(function($val) {
            return sprintf('\'%s\'', trim($val, '\''));
        }, is_array($value) ? $value : array($value));
        $sql = 'SELECT `dealer_group_link`.`dealer_id` FROM `dealer_group_link` WHERE (group_id IN (' . implode(', ', $preparedValue) . '))';
        $dealerIds = $connection->fetchCol($sql);

        $collection->addFieldToFilter('dealer_id', array('in' => $dealerIds));
    }

    protected function _prepareColumns()
    {
        $this->addColumn("dealer_id", array(
            "header" => Mage::helper("agent")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "dealer_id",
        ));

        $this->addColumn("street", array(
            "header" => Mage::helper("agent")->__("Street"),
            "index" => "street",
        ));
        $this->addColumn("postcode", array(
            "header" => Mage::helper("agent")->__("Postal Code"),
            "index" => "postcode",
        ));
        $this->addColumn("house_nr", array(
            "header" => Mage::helper("agent")->__("House Nr."),
            "index" => "house_nr",
        ));
        $this->addColumn("city", array(
            "header" => Mage::helper("agent")->__("City"),
            "index" => "city",
        ));
        $this->addColumn("telephone", array(
            "header" => Mage::helper("agent")->__("Telephone"),
            "index" => "telephone",
        ));
        
        $this->addColumn('store_id', array(
            'header' => Mage::helper('agent')->__('Store'),
            'index' => 'store_id',
            'type' => 'options',
            'options' => Mage::getSingleton('adminhtml/system_store')->getStoreOptionHash(false),
        ));

        $this->addColumn("hotline_number", array(
            "header" => Mage::helper("agent")->__("Hotline number"),
            "index" => "hotline_number",
        ));

        $this->addColumn("axi_store_code", array(
            "header" => Mage::helper("agent")->__("AXI store code"),
            "index" => "axi_store_code",
        ));

        $this->addColumn("vf_dealer_code", array(
            "header" => Mage::helper("agent")->__("VF dealer code"),
            "index" => "vf_dealer_code",
        ));

        $this->addColumn("group_id", array(
            "header" => Mage::helper("agent")->__("Group ID"),
            "index" => "group_id",
            'frame_callback' => array($this, 'getGroupIds'),
            'filter_condition_callback' => array($this, 'filterGroupIds')
        ));

        $this->addColumn("paid_code", array(
            "header" => Mage::helper("agent")->__("Paid code"),
            "index" => "paid_code",
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('dealer_id');
        $this->getMassactionBlock()->setFormFieldName('dealer_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_dealer', array(
            'label' => Mage::helper('agent')->__('Remove Dealer'),
            'url' => $this->getUrl('*/adminhtml_dealer/massRemove'),
            'confirm' => Mage::helper('agent')->__('Are you sure?')
        ));
        return $this;
    }

    public function getGroupIds($a, $item)
    {
        return implode(';', $item->load($item->getId())->getData('group_id'));
    }

    public static function getDealerGroups()
    {
        $collection = Mage::getModel("agent/dealergroup")->getCollection()->load();
        $data_array = array();
        foreach ($collection as $group) {
            $data_array[] = array('value' => $group->getId(), 'label' => $group->getName());
        }
        return $data_array;
    }

    /**
     * Retrieve Headers row array for Export
     *
     * @return array
     */
    protected function _getExportHeaders()
    {
        $row = array();
        foreach ($this->_columns as $column) { /** @var Mage_Adminhtml_Block_Widget_Grid_Column $column */
            if (!$column->getIsSystem()) {
                $row[] = $column->getIndex();
            }
        }
        return $row;
    }

    /**
     * Write item data to csv export file
     *
     * @param Varien_Object $item
     * @param Varien_Io_File $adapter
     */
    protected function _exportCsvItem(Varien_Object $item, Varien_Io_File $adapter)
    {
        $item->load($item->getId());
        $row = array();
        foreach ($this->_columns as $column) {
            if (!$column->getIsSystem()) {
                $value = $item->getData($column->getIndex());
                if (is_array($value)) {
                    $value = implode(';', $value);
                }
                $row[] = $value;
            }
        }
        $adapter->streamWriteCsv($row);
    }
}
