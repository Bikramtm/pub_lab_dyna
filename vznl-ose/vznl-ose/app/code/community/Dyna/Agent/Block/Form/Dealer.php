<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Customer dealer form block
 */

class Dyna_Agent_Block_Form_Dealer extends Mage_Core_Block_Template
{
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('customer')->__('Agent Login'));
        return parent::_prepareLayout();
    }
    /**
     * Retrieve form posting url
     *
     * @return string
     */
    public function getPostActionUrl()
    {
        return Mage::helper('agent')->getDealerPostUrl();
    }

    /**
     * Retrieve the dealers list
     *
     * @param bool $asJson
     * @return string
     */
    public function getDealers($asJson = false)
    {
        return Mage::helper('agent')->getDealers($asJson);
    }

    /**
     * @return string|null
     */
    public function getDealer()
    {
        $agent = $this->_getSession()->getAgent();
        if (is_object($agent) && is_object($agent->getDealer())) {
            return $agent->getDealer();
        }
        return null;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _getSession()
    {
        /** @var Mage_Customer_Model_Session $session */
        return Mage::getModel('customer/session');
    }
}
