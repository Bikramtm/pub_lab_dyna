<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Mapper_Agent
 */
class Dyna_Agent_Model_Mapper_Agent extends Dyna_Agent_Model_Mapper_AbstractMapper
{
    /** @var Dyna_Agent_Model_Mysql4_Agent_Collection */
    protected $_collection;

    /** @var Dyna_Agent_Model_Mysql4_Agentrole_Collection */
    protected $_roleCollection;

    /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection */
    protected $_dealerCollection;

    /** @var Dyna_Agent_Model_Mysql4_Dealergroup_Collection */
    protected $_groupCollection;

    /**
     * @param array $data
     * @return Dyna_Agent_Model_Agent
     */
    public function map(array $data)
    {
        if (isset($data['agent_id'])){
            unset($data['agent_id']);
        }

        /** @var Dyna_Agent_Model_Agent $agent */
        if (isset($data['username']) && $data['username'] && ($existingAgent = $this->_getCollection()->getItemByColumnValue('username', $data['username']))) {
            $agent = $existingAgent->load($existingAgent->getId());
            $agent->setIsNew(false);
        } else {
            $agent = Mage::getModel('agent/agent');
            if ( ! isset($data['password']) || (isset($data['password']) && ! strlen(trim($data['password'])))) {
                throw new RuntimeException(__('Please make sure all new agents have a password defined in the the import file.'));
            }
            $agent->setIsNew(true);
        }

        foreach ($data as $property => $value) {
            try {
                $agent->setData($property, $this->mapProperty($property, $value, $agent));
            } catch (UnexpectedValueException $e) {
                //no-op as this ex will be thrown for old agents
            }
        }

        if ( ! $agent->getIsNew()) {
            $agent->unsetData('password');
        }

        return $agent;
    }

    /**
     * @param $roleId
     * @return mixed
     */
    protected function mapRoleId($roleId)
    {
        if ($role = $this->_getRoleCollection()->getItemByColumnValue('role', $roleId)) {
            return $role->getRoleId();
        } elseif ($roleRoleId = $this->_getRoleCollection()->getItemByColumnValue('role_id', $roleId)) {
            return $roleRoleId->getRoleId();
        }
        return $roleId;
    }

    /**
     * @param $dealerId
     * @return mixed
     */
    protected function mapDealerId($dealerId)
    {
        if ($dealer = $this->_getDealerCollection()->getItemByColumnValue('vf_dealer_code', $dealerId)) {
            return $dealer->getId();
        } elseif ($dealerDealerId = $this->_getDealerCollection()->getItemByColumnValue('dealer_id', $dealerId)) {
            return $dealerDealerId->getId();
        }
        return $dealerId;
    }

    /**
     * @param $dealerCode
     * @return mixed
     */
    protected function mapDealerCode($dealerCode)
    {
        if ($dealer = $this->_getDealerCollection()->getItemByColumnValue('vf_dealer_code', $dealerCode)) {
            return $dealer->getId();
        } elseif ($dealerDealerId = $this->_getDealerCollection()->getItemByColumnValue('dealer_id', $dealerCode)) {
            return $dealerDealerId->getId();
        }

        return $dealerCode;
    }

    /**
     * @param $groupId
     * @return mixed
     */
    protected function mapGroupId($groupId)
    {
        if ($group = $this->_getGroupCollection()->getItemByColumnValue('vf_dealer_code', $groupId)) {
            return $group->getId();
        } elseif ($groupDealer = $this->_getGroupCollection()->getItemByColumnValue('dealer_id', $groupId)) {
            return $groupDealer->getId();
        }
        return $groupId;
    }

    /**
     * @param $storeId
     * @return mixed
     */
    protected function mapStoreId($storeId)
    {
        $storeId = trim($storeId);
        if (is_numeric($storeId)) {
            return $storeId;
        }

        foreach (Mage::app()->getStores() as $store) { /** @var Mage_Core_Model_Store $store */
            if ($storeId === $store->getName() || $storeId === $store->getFrontendName()) {
                return $store->getId();
            }
        }
        return $storeId;
    }

    /**
     * @param $isActive
     * @return int|string
     */
    protected function mapIsActive($isActive)
    {
        if (is_numeric($isActive)) {
            return (int) $isActive;
        }
        $isActive = strtolower(trim(__($isActive)));
        $yes = strtolower(__('Yes'));
        $no = strtolower(__('No'));
        if ($yes === $isActive || 'ja' === $isActive) {
            return 1;
        } elseif ($no === $isActive || 'nee' === $isActive) {
            return 0;
        }
        return $isActive;
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Agent_Collection
     */
    protected function _getCollection()
    {
        if ( ! $this->_collection) {
            return $this->_collection = Mage::getResourceModel('agent/agent_collection');
        }
        return $this->_collection;
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Agentrole_Collection
     */
    protected function _getRoleCollection()
    {
        if ( ! $this->_roleCollection) {
            return $this->_roleCollection = Mage::getResourceModel('agent/agentrole_collection');
        }
        return $this->_roleCollection;
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Dealer_Collection
     */
    protected function _getDealerCollection()
    {
        if ( ! $this->_dealerCollection) {
            return $this->_dealerCollection = Mage::getResourceModel('agent/dealer_collection');
        }
        return $this->_dealerCollection;
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Dealergroup_Collection
     */
    protected function _getGroupCollection()
    {
        if ( ! $this->_groupCollection) {
            return $this->_groupCollection = Mage::getResourceModel('agent/dealergroup_collection');
        }
        return $this->_groupCollection;
    }
} 