<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Dealergroup
 */
class Dyna_Agent_Model_Dealergroup extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
       $this->_init("agent/dealergroup");
    }

    /**
     * @param int $id
     * @param null $field
     * @return $this|Mage_Core_Model_Abstract
     */
    public function load($id, $field=null)
    {
        $key = 'dealergroup_' . md5(serialize(array($id, $field)));
        if ($product = Mage::objects()->load($key)) {
            $this->setData($product->getData());
            return $this;
        }

        // One dealer should have one group
        if (is_array($id)) {
            $id = current($id);
        }

        parent::load($id, $field);
        Mage::objects()->save($this, $key);
        return $this;
    }
}