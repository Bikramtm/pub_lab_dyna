<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_GenericImporter
 */
class Dyna_Agent_Model_GenericImporter
{
    /** @var array */
    protected $_supported = array(
        'csv',
        //'xlsx',
    );

    /**
     * @return array
     */
    public function getSupportedFormats()
    {
        return $this->_supported;
    }

    /**
     * @param $path
     * @param Dyna_Agent_Model_Mapper_AbstractMapper $mapper
     * @throws Exception
     */
    public function import($path, Dyna_Agent_Model_Mapper_AbstractMapper $mapper)
    {
        switch($format = $this->_detectType($this->_removeBom($path))) {
            case 'csv':
                $this->importCsv($path, $mapper);
                break;
            case 'xlsx':
                $this->importExcel($path, $mapper);
                break;
            case 'xml':
                $this->importXml($path, $mapper);
                break;
            default:
                throw new RuntimeException(sprintf('Format "%s" is not currently supported', $format));
        }
    }

    /**
     * @param $path
     * @param Dyna_Agent_Model_Mapper_AbstractMapper $mapper
     */
    protected function importCsv($path, Dyna_Agent_Model_Mapper_AbstractMapper $mapper)
    {
        $session = Mage::getSingleton('core/session');
        $csv = new Mage_ImportExport_Model_Import_Adapter_Csv($path);
        $session->setData('import_headers', $csv->getColNames());
        $objs = array();
        while( ! is_null($csv->key())) {
            $row = $csv->current();
            $obj = $mapper->map($row);
            $objs[] = $obj;
            $csv->next();
        }
        $session->setData('import_rows', $objs);
    }

    /**
     * @param $path
     * @param Dyna_Agent_Model_Mapper_AbstractMapper $mapper
     * @throws Exception
     */
    protected function importExcel()
    {
        throw new Exception('Not supported');
    }

    /**
     * @param $path
     * @param Dyna_Agent_Model_Mapper_AbstractMapper $mapper
     * @throws Exception
     */
    protected function importXml()
    {
        throw new Exception('Not supported');
    }

    /**
     * @param $path
     * @return string
     * @TODO improve mime type detection
     */
    protected function _detectType($path)
    {
        $parts = explode('.', $path);
        return trim(strtolower(end($parts)));
    }

    /**
     * @param $path
     * @param bool $returnPath
     * @return string
     */
    protected function _removeBom($path, $returnPath = true)
    {
        if ( ! ($path = realpath($path))) {
            throw new InvalidArgumentException('Invalid path given');
        }

        if ( ! is_file($path)) {
            throw new InvalidArgumentException('Given path is not a file');
        }

        $contents = file_get_contents($path);
        if (ord($contents[0]) == 0xEF && ord($contents[1]) == 0xBB && ord($contents[2]) == 0xBF) {
            $contents = substr($contents, 3);
            file_put_contents($path, $contents);
        }

        if ( ! $returnPath) {
            return $contents;
        }

        unset($contents);
        return $path;
    }
} 