<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Mysql4_Dealer_Collection
 */
class Dyna_Agent_Model_Mysql4_Dealer_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("agent/dealer");
    }

    /**
     * @param array $groupIds
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getDealersByGroups($groupIds)
    {
        if ($groupIds === null) {
            return null;
        }

        if (!is_array($groupIds)) {
            $groupIds = [$groupIds];
        }

        if (count($groupIds)) {
            return $this->addFieldToFilter(
                'dealer_id',
                array('in' => $this->getResource()->getReadConnection()
                    ->query('SELECT DISTINCT dealer_id FROM dealer_group_link WHERE group_id IN (' . implode(',', $groupIds) . ')')->fetchAll(PDO::FETCH_COLUMN))
            );
        } else {
            return null;
        }
    }
}
