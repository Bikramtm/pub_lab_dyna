<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Mysql4_Agent
 */
class Dyna_Agent_Model_Mysql4_Agent extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("agent/agent", "agent_id");
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Resource_Db_Abstract|void
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        if ($object->hasDataChanges() && Mage::registry('agent_password_changed')) {
            Mage::getModel('agent/agentpassword')
                ->setPassword($object->getData('password'))
                ->setAgentId($object->getId())
                ->setId(null)
                ->setHashMethod($object->getData('hash_method'))
                ->save();

            Mage::unregister('agent_password_changed');
        }

        parent::_afterSave($object);
    }
}
