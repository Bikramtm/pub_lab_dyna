<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Observer
 */
class Dyna_Agent_Model_Observer extends Mage_Core_Model_Abstract
{
    /**
     * Listener for the preDispatch method of the Mage_Core_Controller_Front_Action
     * If the customers are logged in but no dealer is found on their session, redirect
     * them to the customer/account/dealer
     *
     * @param $observer
     */
    public function controllerActionPreDispatch($observer)
    {
        // Check for public allowed URLs
        $allowedUrls = Mage::helper('agent')->getConfigAllowedUrls();
        if ($allowedUrls && is_array($allowedUrls)) {
            foreach ($allowedUrls as $allowedUrl) {
                if (trim($allowedUrl) == $observer->getEvent()->getControllerAction()->getFullActionName()) {
                    return;
                }
            }
        }

        $pageAlias = $observer->getControllerAction()->getRequest()->getAliases();
        $action = isset($pageAlias["rewrite_request_path"]) ? $pageAlias["rewrite_request_path"] : '';

        //only redirect to login if store is set to show agent login
        if(!Mage::app()->getStore()->getShowAgent()) {
            return;
        }

        /** @var Mage_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');

        /** @var Mage_Core_Controller_Front_Action $controller */
        $controller = $observer->getEvent()->getControllerAction();
        $request = $observer->getControllerAction()->getRequest();
        Mage::helper('agent')->checkOrderEditMode($request,$controller);
        $isIpSecurityEnabled = Mage::helper('core')->isModuleEnabled('IpSecurity');
        if ($isIpSecurityEnabled) {
            $ipSecurityPage = Mage::getStoreConfig('ipsecurity/ipsecurityfront/redirect_page');
        }

        if ( ! $session->getData('agent')
            && 'agent_account_login' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_loginPost' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_forgotCredentials' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_password' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_temporaryPassword' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_passwordReset' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_dealer' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_dealerPost' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'adyen_process_ins' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && (!$action || ($isIpSecurityEnabled && $action !== $ipSecurityPage))
        ) {
            // Check if the agent is not logged in
            if($observer->getControllerAction()->getRequest()->isXmlHttpRequest()) {
                $controller->getResponse()->setHeader('Content-Type', 'application/json');
                $controller->getResponse()->setBody(json_encode(array('error' => true, 'session' => 'expired')));
                $controller->getResponse()->setHttpResponseCode(200)->sendResponse();
                exit;
            } else {
                if ('core_index_noCookies' == $observer->getEvent()->getControllerAction()->getFullActionName()) {
                    Mage::getSingleton('core/session')->addNotice(Mage::helper('dyna_customer')->__('Your cookies expired. Please try again.'));
                }

                $controller->getResponse()->setRedirect(Mage::getUrl('agent/account/login'))->sendHeaders();
                exit;
            }
        }
        if (Mage::app()->getStore()->getShowStore()
            && $session->getData('agent')
            && !$session->getData('agent')->getData('dealer')
            && 'agent_account_dealer' !== $observer->getEvent()->getControllerAction()->getFullActionName()
        ) {
            $controller->getResponse()->setRedirect(Mage::getUrl('agent/account/dealer'))->sendHeaders();
            return;
        }
    }

    /**
     * @param $observer
     *
     * Add the agent show login and agent show store fields in the edit form
     */
    public function prepareStoreViewForm($observer)
    {
        $event = $observer->getEvent();
        $block = $event->getBlock();
        $form = $block->getForm();
        $storeModel = Mage::registry('store_data');

        $fieldset = $form->addFieldset('website_fieldset_agent', array(
            'legend' => Mage::helper('core')->__('Agent store display')
        ));

        $fieldset->addField('store_show_login', 'select', array(
            'name'      => 'store[show_agent]',
            'label'     => Mage::helper('core')->__('Show agent login'),
            'value'     => $storeModel->getShowAgent(),
            'options'   => array(
                0 => Mage::helper('adminhtml')->__('Disabled'),
                1 => Mage::helper('adminhtml')->__('Enabled')),
            'required'  => true,
            'disabled'  => false,
        ));

        $fieldset->addField('store_show_store', 'select', array(
            'name'      => 'store[show_store]',
            'label'     => Mage::helper('core')->__('Show store selection'),
            'value'     => $storeModel->getShowStore(),
            'options'   => array(
                0 => Mage::helper('adminhtml')->__('Disabled'),
                1 => Mage::helper('adminhtml')->__('Enabled')),
            'required'  => true,
            'disabled'  => false,
        ));
    }
}
