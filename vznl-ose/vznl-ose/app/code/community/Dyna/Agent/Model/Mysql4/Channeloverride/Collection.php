<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Mysql4_Channeloverride_Collection
 */
class Dyna_Agent_Model_Mysql4_Channeloverride_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("agent/channeloverride");
    }
}
