<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Mysql4_Dealer
 */
class Dyna_Agent_Model_Mysql4_Dealer extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("agent/dealer", "dealer_id");
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Resource_Db_Abstract|void
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $groups = $object->getData('group_id');
        if ($object->hasDataChanges()) {
            $data = array();
            $groupsIds = array();
            foreach ($groups as $group) {
                array_push($data, array('group_id' => $group, 'dealer_id' => $object->getId()));
                array_push($groupsIds, $group);
            }
            $connection = $this->_getConnection('core_write');
            $connection->beginTransaction();
            try {
                /**
                 * Find already existing connections
                 */
                $adapter = $this->_getWriteAdapter();
                /**
                 * Delete all previous keys, and add the new ones
                 */
                $adapter->delete(
                    Mage::getSingleton('core/resource')->getTableName('dealer_group_link'),
                    sprintf('dealer_id = %s', $adapter->quote($object->getId()))
                );

                foreach ($data as $row) {
                    $connection->insert(Mage::getSingleton('core/resource')->getTableName('dealer_group_link'), $row);
                }
                $connection->commit();
            } catch (Exception $e) {
                $connection->rollBack();
                Mage::getSingleton('core/logger')->logException($e);
            }
        }

        parent::_afterSave($object);
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Eav_Model_Entity_Abstract|void
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        parent::_afterLoad($this->loadGroups($object));
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Abstract
     */
    protected function loadGroups(Mage_Core_Model_Abstract $object)
    {
        $adapter = $this->_getWriteAdapter();
        $bind = array('dealer_id' => $object->getId());
        $select = $adapter->select()
            ->from('dealer_group_link', array('group_id'))
            ->where('dealer_id = :dealer_id');
        $result = $adapter->fetchCol($select, $bind);

        $object->setData('group_id', $result);

        return $object;
    }
}