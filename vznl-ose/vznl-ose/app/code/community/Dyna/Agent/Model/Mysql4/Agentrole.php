<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Mysql4_Agentrole
 */
class Dyna_Agent_Model_Mysql4_Agentrole extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("agent/agentrole", "entity_id");
    }
}
