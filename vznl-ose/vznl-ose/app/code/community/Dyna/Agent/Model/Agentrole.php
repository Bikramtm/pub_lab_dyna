<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Agentrole
 */
class Dyna_Agent_Model_Agentrole extends Mage_Core_Model_Abstract
{
    const SUPERAGENT_ROLE = 2;
    const BUSINESS_SPECIALIST = 3;

    protected function _construct()
    {
        $this->_init("agent/agentrole");
    }

    /**
     * @param int $id
     * @param null $field
     * @return $this|Mage_Core_Model_Abstract
     */
    public function load($id, $field = null)
    {
        $key = 'agent_role_' . md5(serialize(array($id, $field)));
        if ($product = Mage::objects()->load($key)) {
            $this->setData($product->getData());
            return $this;
        }

        parent::load($id, $field);
        $this->getPermissionIds();
        $this->getPermissions();
        Mage::objects()->save($this, $key);
        return $this;
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Permission_Collection
     */
    public function getPermissions()
    {
        if (!$this->hasData('permissions')) {
            if (!$this->hasData('permission_ids')) {
                $this->getPermissionIds();
            }
            $this->setData('permissions',
                Mage::getResourceModel('agent/permission_collection')->addFieldToFilter('entity_id',
                    array('in' => $this->getData('permission_ids')))->toArray());
        }
        return $this->getData('permissions');
    }

    /**
     * @param $permission
     * @return bool
     */
    public function isGranted($permission)
    {
        $permissions = $this->getPermissions();
        if (isset($permissions['items']) && is_array($permissions['items'])) {
            foreach ($permissions['items'] as $item) {
                if (isset($item['name']) && $item['name'] == $permission) {
                    return true;
                }
            }
        }

        return false;
    }

    protected function getPermissionIds()
    {
        if (!$this->hasData('permission_ids')) {
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            $sql = "SELECT `permission_id` FROM `role_permission_link` WHERE role_id=:id";
            $this->setData('permission_ids', $conn->fetchCol($sql, array(':id' => $this->getRoleId())));
        }
        return $this->getData('permission_ids');
    }

    /**
     * Retrieve max role_id from agent_role table
     * @return int
     */
    public function getHighestId()
    {
        $maxID =  Mage::getModel('agent/agentrole')->getCollection()
            ->addFieldToSelect('role_id')
            ->setOrder('role_id', 'DESC')
            ->getFirstItem();

        return $maxID->getRoleId() ? $maxID->getRoleId() + 1 : 1;
    }
}
