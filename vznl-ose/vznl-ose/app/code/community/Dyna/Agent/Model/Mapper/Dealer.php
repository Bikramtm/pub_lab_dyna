<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Mapper_Dealer
 */
class Dyna_Agent_Model_Mapper_Dealer extends Dyna_Agent_Model_Mapper_AbstractMapper
{
    /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection */
    protected $_collection;

    /**
     * @param array $data
     * @return Dyna_Agent_Model_Dealer
     */
    public function map(array $data)
    {
        if(isset($data['dealer_id'])) {
            unset($data['dealer_id']);
        }

        /** @var Dyna_Agent_Model_Dealer $dealer */
        if (isset($data['vf_dealer_code']) && $data['vf_dealer_code'] && ($existingDealer = $this->_getCollection()->getItemByColumnValue('vf_dealer_code', $data['vf_dealer_code']))) {
            $dealer = $existingDealer->load($existingDealer->getId());
        } else {
            $dealer = Mage::getModel('agent/dealer');
        }

        foreach ($data as $property => $value) {
            try {
                $dealer->setData($property, $this->mapProperty($property, $value, $dealer));
            } catch (UnexpectedValueException $e) {
                //no-op as this ex will be thrown for old agents
            }
        }

        return $dealer;
    }

    /**
     * @param $groupIds
     * @return array
     */
    protected function mapGroupId($groupIds)
    {
        return array_map('intval', array_filter(array_map('trim', explode(';', $groupIds))));
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Dealer_Collection
     */
    protected function _getCollection()
    {
        if ( ! $this->_collection) {
            return $this->_collection = Mage::getResourceModel('agent/dealer_collection');
        }
        return $this->_collection;
    }
} 