<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Adminhtml_DealergroupController
 */
class Dyna_Agent_Adminhtml_DealergroupController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("agent/dealergroup")->_addBreadcrumb(Mage::helper("adminhtml")->__("Dealer Group Manager"), Mage::helper("adminhtml")->__("Dealer Group Manager"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Manager Dealer Group"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Dealer Group"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agent/dealergroup")->load($id);
        if ($model->getId()) {
            Mage::register("dealergroup_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("agent/dealergroup");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Dealer Group Manager"), Mage::helper("adminhtml")->__("Dealer Group Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Dealer Group Description"), Mage::helper("adminhtml")->__("Dealer Group Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("agent/adminhtml_dealergroup_edit"))->_addLeft($this->getLayout()->createBlock("agent/adminhtml_dealergroup_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("agent")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {

        $this->_title($this->__("Agent"));
        $this->_title($this->__("Dealer Group"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agent/dealergroup")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("dealergroup_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("agent/dealergroup");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Dealer Group Manager"), Mage::helper("adminhtml")->__("Dealer Group Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Dealer Group Description"), Mage::helper("adminhtml")->__("Dealer Group Description"));

        $this->_addContent($this->getLayout()->createBlock("agent/adminhtml_dealergroup_edit"))->_addLeft($this->getLayout()->createBlock("agent/adminhtml_dealergroup_edit_tabs"));

        $this->renderLayout();

    }

    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data) {
            try {
                $model = Mage::getModel("agent/dealergroup")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Dealer group was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setDealergroupData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setDealergroupData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }


    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $id = $this->getRequest()->getParam("id");
                $collection = Mage::getModel('agent/dealer')->getCollection();
                $collection->getSelect()
                    ->join( array('dealer_group_link' => 'dealer_group_link'), 'dealer_group_link.dealer_id = main_table.dealer_id', array('group_id'));
                $collection->addFieldToFilter('group_id', $id);
                if ($collection->count()) {
                    Mage::getSingleton("adminhtml/session")->addError('You can not delete this group as it is assigned to an existing dealer.');
                    return $this->_redirect("*/*/");
                }

                $model = Mage::getModel("agent/dealergroup");
                $model->setId($id)->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }


    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('group_ids', array());
            foreach ($ids as $id) {
                $collection = Mage::getModel('agent/dealer')->getCollection();
                $collection->getSelect()
                    ->join( array('dealer_group_link' => 'dealer_group_link'), 'dealer_group_link.dealer_id = main_table.dealer_id', array('group_id'));
                $collection->addFieldToFilter('group_id', $id);
                if ($collection->count()) {
                    Mage::getSingleton("adminhtml/session")->addError('You can not delete group as it is assigned to an existing dealer.');
                    return $this->_redirect("*/*/");
                }

                $model = Mage::getModel("agent/dealergroup");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'dealergroup.csv';
        $grid = $this->getLayout()->createBlock('agent/adminhtml_dealergroup_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'dealergroup.xml';
        $grid = $this->getLayout()->createBlock('agent/adminhtml_dealergroup_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * @param string $fileName
     * @param array|string $content
     * @param string $contentType
     * @param null $contentLength
     * @return $this|Mage_Core_Controller_Varien_Action
     * @throws Exception
     * @throws Mage_Core_Exception
     * @throws Zend_Controller_Response_Exception
     */
    protected function _prepareDownloadResponse($fileName, $content, $contentType = 'application/octet-stream',
                                                $contentLength = null
    ) {
        $session = Mage::getSingleton('admin/session');
        if ($session->isFirstPageAfterLogin()) {
            $this->_redirect($session->getUser()->getStartupPageUrl());
            return $this;
        }

        $isFile = false;
        $file   = null;
        if (is_array($content)) {
            if (!isset($content['type']) || !isset($content['value'])) {
                return $this;
            }
            if ($content['type'] == 'filename') {
                $isFile         = true;
                $file           = $content['value'];
                $contentLength  = filesize($file);
            }
        }

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) : $contentLength)
            ->setHeader('Content-Disposition', 'attachment; filename="'.$fileName.'"')
            ->setHeader('Last-Modified', date('r'));

        if (!is_null($content)) {
            if ($isFile) {
                $this->getResponse()->clearBody();
                $this->getResponse()->sendHeaders();

                $ioAdapter = new Varien_Io_File();
                if (!$ioAdapter->fileExists($file)) {
                    Mage::throwException(Mage::helper('core')->__('File not found'));
                }
                $ioAdapter->open(array('path' => $ioAdapter->dirname($file)));
                $ioAdapter->streamOpen($file, 'r');
                while ($buffer = $ioAdapter->streamRead()) {
                    print $buffer;
                }
                $ioAdapter->streamClose();
                if (!empty($content['rm'])) {
                    $ioAdapter->rm($file);
                }

                exit(0);
            } else {
                $this->getResponse()->setBody($content);
            }
        }
        return $this;
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }
}
