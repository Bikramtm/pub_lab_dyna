<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Adminhtml_PermissionController
 */
class Dyna_Agent_Adminhtml_PermissionController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("agent/permission")->_addBreadcrumb(Mage::helper("adminhtml")->__("Permission Manager"), Mage::helper("adminhtml")->__("Permission Manager"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Manager Permission"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Permission"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agent/permission")->load($id);
        if ($model->getId()) {
            Mage::register("permission_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("agent/permission");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Permission Manager"), Mage::helper("adminhtml")->__("Permission Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Permission Description"), Mage::helper("adminhtml")->__("Permission Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("agent/adminhtml_permission_edit"))->_addLeft($this->getLayout()->createBlock("agent/adminhtml_permission_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("agent")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {

        $this->_title($this->__("Agent"));
        $this->_title($this->__("Permission"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agent/permission")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("permission_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("agent/permission");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Permission Manager"), Mage::helper("adminhtml")->__("Permission Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Permission Description"), Mage::helper("adminhtml")->__("Permission Description"));


        $this->_addContent($this->getLayout()->createBlock("agent/adminhtml_permission_edit"))->_addLeft($this->getLayout()->createBlock("agent/adminhtml_permission_edit_tabs"));

        $this->renderLayout();

    }

    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data) {
            try {
                $model = Mage::getModel("agent/permission")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Permission was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setPermissionData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setPermissionData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }


    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $id = $this->getRequest()->getParam("id");
                $model = Mage::getModel("agent/permission");
                $model->setId($id)->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }


    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('entity_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("agent/permission");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'permission.csv';
        $grid = $this->getLayout()->createBlock('agent/adminhtml_permission_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'permission.xml';
        $grid = $this->getLayout()->createBlock('agent/adminhtml_permission_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * @param string $fileName
     * @param array|string $content
     * @param string $contentType
     * @param null $contentLength
     * @return $this|Mage_Core_Controller_Varien_Action
     * @throws Exception
     * @throws Mage_Core_Exception
     * @throws Zend_Controller_Response_Exception
     */
    protected function _prepareDownloadResponse($fileName, $content, $contentType = 'application/octet-stream',
                                                $contentLength = null
    ) {
        $session = Mage::getSingleton('admin/session');
        if ($session->isFirstPageAfterLogin()) {
            $this->_redirect($session->getUser()->getStartupPageUrl());
            return $this;
        }

        $isFile = false;
        $file   = null;
        if (is_array($content)) {
            if (!isset($content['type']) || !isset($content['value'])) {
                return $this;
            }
            if ($content['type'] == 'filename') {
                $isFile         = true;
                $file           = $content['value'];
                $contentLength  = filesize($file);
            }
        }

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) : $contentLength)
            ->setHeader('Content-Disposition', 'attachment; filename="'.$fileName.'"')
            ->setHeader('Last-Modified', date('r'));

        if (!is_null($content)) {
            if ($isFile) {
                $this->getResponse()->clearBody();
                $this->getResponse()->sendHeaders();

                $ioAdapter = new Varien_Io_File();
                if (!$ioAdapter->fileExists($file)) {
                    Mage::throwException(Mage::helper('core')->__('File not found'));
                }
                $ioAdapter->open(array('path' => $ioAdapter->dirname($file)));
                $ioAdapter->streamOpen($file, 'r');
                while ($buffer = $ioAdapter->streamRead()) {
                    print $buffer;
                }
                $ioAdapter->streamClose();
                if (!empty($content['rm'])) {
                    $ioAdapter->rm($file);
                }

                exit(0);
            } else {
                $this->getResponse()->setBody($content);
            }
        }
        return $this;
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }
}
