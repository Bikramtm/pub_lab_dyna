<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Adminhtml_AgentroleController
 */
class Dyna_Agent_Adminhtml_AgentroleController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("agent/agentrole")->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Role Manager"), Mage::helper("adminhtml")->__("Dealer Role Manager"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Manager Agent Role"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Agent Role"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agent/agentrole")->load($id);
        if ($model->getId()) {
            Mage::register("agentrole_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("agent/agentrole");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Role Manager"), Mage::helper("adminhtml")->__("Agent Role Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Role Description"), Mage::helper("adminhtml")->__("Agent Role Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("agent/adminhtml_agentrole_edit"))->_addLeft($this->getLayout()->createBlock("agent/adminhtml_agentrole_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("agent")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {

        $this->_title($this->__("Agent"));
        $this->_title($this->__("Agent Role"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        /** @var Dyna_Agent_Model_Agentrole $model */
        $model = Mage::getModel("agent/agentrole")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }else{
            $model->setData(['role_id' => $model->getHighestId()]);
        }

        Mage::register("agentrole_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("agent/agentrole");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Role Manager"), Mage::helper("adminhtml")->__("Agent Role Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Role Description"), Mage::helper("adminhtml")->__("Agent Role Description"));


        $this->_addContent($this->getLayout()->createBlock("agent/adminhtml_agentrole_edit"))->_addLeft($this->getLayout()->createBlock("agent/adminhtml_agentrole_edit_tabs"));

        $this->renderLayout();

    }

    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data) {
            try {
                $model = Mage::getModel("agent/agentrole")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                $permissionIds = isset($post_data['permission_ids']) ? $post_data['permission_ids'] : array();
                $permissionIds = array_filter(array_map('intval', $permissionIds));
                $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
                if ( ! count($permissionIds)) {
                    $sql = sprintf("DELETE FROM `role_permission_link` WHERE role_id='%s'", $model->getRoleId());
                } else {
                    $sql = sprintf("DELETE FROM `role_permission_link` WHERE `permission_id` NOT IN (%s) AND role_id='%s'", join(',', $permissionIds), $model->getRoleId());
                }
                $conn->query($sql);
                foreach ($permissionIds as $id) {
                    try {
                        $conn->insert('role_permission_link', array('role_id' => $model->getRoleId(), 'permission_id' => $id));
                    } catch (Exception $e) {
                        //record already exists
                    }
                }

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Agent role was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setDealerRoleData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Zend_Db_Statement_Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError('Saving agent role failed because there is already an role with Role ID #' . $this->getRequest()->getParam("role_id"));
                if ($this->getRequest()->getParam("id")) {
                    $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                }else{
                    $this->_redirect("*/*/");
                }
                return;
            }catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setDealerRoleData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }


    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $id = $this->getRequest()->getParam("id");
                $role = Mage::getModel('agent/agentrole')->load($id);
                $collection = Mage::getModel('agent/agent')
                    ->getCollection();
                $collection->addFieldToFilter('role_id', $role['role_id']);
                if ($collection->count()) {
                    Mage::getSingleton("adminhtml/session")->addError('You can not delete this role as it is assigned to an existing agent.');
                    return $this->_redirect("*/*/");
                }

                $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
                $conn->query("DELETE FROM `role_permission_link` WHERE role_id=".$role->getRoleId());
                $model = Mage::getModel("agent/agentrole");
                $model->setId($id)->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }


    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('role_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("agent/agentrole");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'agentrole.csv';
        $grid = $this->getLayout()->createBlock('agent/adminhtml_agentrole_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'agentrole.xml';
        $grid = $this->getLayout()->createBlock('agent/adminhtml_agentrole_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * @param string $fileName
     * @param array|string $content
     * @param string $contentType
     * @param null $contentLength
     * @return $this|Mage_Core_Controller_Varien_Action
     * @throws Exception
     * @throws Mage_Core_Exception
     * @throws Zend_Controller_Response_Exception
     */
    protected function _prepareDownloadResponse($fileName, $content, $contentType = 'application/octet-stream',
                                                $contentLength = null
    ) {
        $session = Mage::getSingleton('admin/session');
        if ($session->isFirstPageAfterLogin()) {
            $this->_redirect($session->getUser()->getStartupPageUrl());
            return $this;
        }

        $isFile = false;
        $file   = null;
        if (is_array($content)) {
            if (!isset($content['type']) || !isset($content['value'])) {
                return $this;
            }
            if ($content['type'] == 'filename') {
                $isFile         = true;
                $file           = $content['value'];
                $contentLength  = filesize($file);
            }
        }

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) : $contentLength)
            ->setHeader('Content-Disposition', 'attachment; filename="'.$fileName.'"')
            ->setHeader('Last-Modified', date('r'));

        if (!is_null($content)) {
            if ($isFile) {
                $this->getResponse()->clearBody();
                $this->getResponse()->sendHeaders();

                $ioAdapter = new Varien_Io_File();
                if (!$ioAdapter->fileExists($file)) {
                    Mage::throwException(Mage::helper('core')->__('File not found'));
                }
                $ioAdapter->open(array('path' => $ioAdapter->dirname($file)));
                $ioAdapter->streamOpen($file, 'r');
                while ($buffer = $ioAdapter->streamRead()) {
                    print $buffer;
                }
                $ioAdapter->streamClose();
                if (!empty($content['rm'])) {
                    $ioAdapter->rm($file);
                }

                exit(0);
            } else {
                $this->getResponse()->setBody($content);
            }
        }
        return $this;
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }
}
