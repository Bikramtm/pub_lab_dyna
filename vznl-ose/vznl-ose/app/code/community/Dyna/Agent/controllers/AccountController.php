<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class AccountController
 */
class Dyna_Agent_AccountController extends Mage_Core_Controller_Front_Action
{
    /**
     * Retrieve customer session model object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Check if the call was Ajas
     *
     * @return bool
     */
    protected function isAjax()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            return true;
        }

        return false;
    }

    /**
     * Checks that the selected dealer address
     * also exists in the dealers collection
     *
     * @param $address
     * @return bool
     */
    protected function validateAddress($address)
    {
        foreach (Mage::getModel('agent/dealer')->getCollection()->getItems() as $dealer) {
            if ($address === $dealer->getAddress()) {
                return $dealer;
            }
        }
        return false;
    }

    /**
     * Assures proper conditions
     */
    protected function _preCheck()
    {
        if ( ! $this->_getSession()->getAgent()
            || ($this->_getSession()->getAgent()
                && ! $this->_getSession()->getAgent()->getDealer())) {
            $this->_redirectUrl(Mage::getUrl('agent/account/login'));
            return;
        }
    }

    /**
     * Restrict new customer creation
     */
    protected function _restrictNew()
    {
        $this->_getSession()->addError('Registration is disabled. Only administrators can add new agents.');
        $this->_redirect('*/*/');
    }

    /**
     * Get Helper
     *
     * @param string $path
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper($path)
    {
        return Mage::helper($path);
    }

    /**
     * Get Url method
     *
     * @param string $url
     * @param array $params
     * @return string
     */
    protected function _getUrl($url, $params = array())
    {
        return Mage::getUrl($url, $params);
    }

    /**
     * Define target URL and redirect customer after logging in
     */
    protected function _loginPostRedirect()
    {
        // if require agent but not set
        if(!$this->_getSession()->getAgent() && Mage::app()->getStore()->getShowAgent()) {
            $this->_redirectUrl(Mage::getUrl('agent/account/agent'));
            return;
        }
        // if require dealer but not set
        if($this->_getSession()->getAgent() && Mage::app()->getStore()->getShowStore()) {
            $this->_redirectUrl(Mage::getUrl('agent/account/dealer'));
            return;
        }

        // else either agent/dealer is set or not required so we're good
        $this->_redirectUrl(Mage::getUrl());
        return;
    }

    /**
     * Action predispatch
     *
     * Check agent authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        $action = $this->getRequest()->getActionName();
        $openActions = array(
            'create',
            'login',
            'logout',
            'dealer',
            'dealerPost',
            'password',
            'forgotCredentials',
            'passwordReset',
            'temporaryPassword'
        );
        $pattern = '/^(' . implode('|', $openActions) . ')/i';
        if ( ! preg_match($pattern, $action)) {
            if ( ! $this->_getSession()->getAgent()) {
                $this->setFlag('', 'no-dispatch', true);
            }
        } else {
            $this->_getSession()->setNoReferer(true);
        }
    }

    /**
     * Action postdispatch
     *
     * Remove No-referer flag from customer session after each action
     */
    public function postDispatch()
    {
        parent::postDispatch();
        $this->_getSession()->unsNoReferer(false);
    }

    /**
     * Customer login form page
     */
    public function loginAction()
    {
        if ($this->_getSession()->getAgent()) {
            $this->_redirectUrl(Mage::getUrl());
            return;
        }
        $this->getResponse()->setHeader('Login-Required', 'true');
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    /**
     * Login post action
     */
    public function loginPostAction()
    {
        if ($this->_getSession()->getAgent()) {
            $this->_redirectUrl(Mage::getUrl());
            return;
        }
        $session = $this->_getSession();

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                $agent = Mage::getModel('agent/agent')->getCollection()->addFieldToFilter('username', $login['username'])->getFirstItem();
                try {
                    $agent->authenticate($login['username'], $login['password']);
                } catch (Mage_Core_Exception $e) {
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $agent->incrementLoginAttempts();
                            break;
                        case Dyna_Agent_Model_Agent::LOCKED_AGENT:
                            $this->_getSession()->setAgent(null);
                            $this->_getSession()->setSuperAgent(null);
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $message = $e->getMessage();
                            break;
                        case Dyna_Agent_Model_Agent::TEMPORARY_PASSWORD:
                            $this->_getSession()->setAgent(null);
                            $this->_getSession()->setSuperAgent(null);
                            if (Mage::getSingleton('customer/session')->getTemporaryAgent()) {
                                return $this->_redirect('*/*/temporaryPassword');
                            }
                            break;
                        default:
                            $message = $e->getMessage();
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $agent->incrementLoginAttempts();
                    }
                    $session->addError($message);
                    $session->setUsername($login['username']);
                    return $this->_redirect('*/*/login');
                } catch (Exception $e) {
                    // Mage::getSingleton('core/logger')->logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } else {
                $session->addError($this->__('Login and password are required.'));
            }
        }

        Mage::getSingleton('customer/session')->unsTemporaryAgent();
        $agent = $session->getAgent();
        if ($agent && ! $agent->getEverlastingPass()) {
            $passwordDeadline = new DateTime($agent->getPasswordChanged());
            $passwordDeadline->add(new DateInterval(sprintf('P%sD', abs((int) Mage::getStoreConfig(Dyna_Agent_Model_Agent::PASSWORD_EXPIRY_DURATION)))));
            if ($passwordDeadline->getTimestamp() <= Mage::getModel('core/date')->timestamp(time())) {
                Mage::getSingleton('customer/session')->setTemporaryAgent($agent);
                $this->_getSession()->setAgent(null);
                $this->_getSession()->setSuperAgent(null);
                return $this->_redirect('*/*/temporaryPassword');
            }
        }
        if ($session->getAgent()
            && ! empty($dealerForm['address'])
            && ($dealer = $this->validateAddress($dealerForm['address']))
            && $dealer->getStoreId() == Mage::app()->getStore()->getId()
        ) {
            $session->getAgent()->setDealer($dealer);
        }

        $this->_getSession()->renewSession();
        Mage::dispatchEvent("dyna_agent_login");

        $this->_loginPostRedirect();
    }

    /**
     * Dealer selection action
     */
    public function dealerAction()
    {
        if ( ! $this->_getSession()->getAgent()) {
            $this->_redirectUrl(Mage::getUrl('agent/account/login'));
            return;
        }

        $this->getResponse()->setHeader('Login-Required', 'true');
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    /**
     * Dealer post action
     */
    public function dealerPostAction()
    {
        if ( ! ($this->_getSession()->getAgent() && $this->_getSession()->getAgent()->getId())) {
            $this->_getSession()->addError($this->__('Session expired'));
            return $this->_redirectUrl(Mage::getUrl('agent/account/login'));
        }

        $session = $this->_getSession();

        if ($this->getRequest()->isPost()) {
            $dealerForm = $this->getRequest()->getPost('dealer');
            if ( ! empty($dealerForm['address']) && ($dealer = $this->validateAddress($dealerForm['address']))) {
                if($dealer->getStoreId() == Mage::app()->getStore()->getId()) {
                    $agent = $session->getAgent()->load($session->getAgent()->getId());
                    $agent->setDealerId($dealer->getId());
                    $agent->setDealer($dealer->load($dealer->getId()));
                    $session->setAgent($agent);
                    return $this->_redirectUrl(Mage::getUrl());
                } else {
                    $session->addError($this->__('Dealer not allowed on this channel.'));
                }
            } else {
                $session->addError($this->__('Please provide a dealer address.'));
            }
        }
        return $this->_redirect('*/*/dealer');
    }

    /**
     * Agent logout action
     */
    public function logoutAction()
    {
        // clear cart
        Mage::getSingleton('checkout/session')->clear();

        $this->_getSession()->setAgent(null);
        $this->_getSession()->setSuperAgent(null);
        $this->_getSession()->unsOrderEdit();
        $this->_getSession()->unsOrderEditMode();
        $this->_getSession()->logout();
        $this->_getSession()->renewSession();

        if($this->isAjax()) {
            $this->getResponse()->setHeader('Content-Type', 'application/json');
            $this->getResponse()->setBody(json_encode(array('success' => true)));
            $this->getResponse()->setHttpResponseCode(200);
        }
        else {
            $this->_redirectUrl(Mage::getUrl('agent/account/login'));
        }

        Mage::dispatchEvent("dyna_agent_logout");
    }

    /**
     * Restrict create action
     */
    public function createAction()
    {
        $this->_restrictNew();
    }

    /**
     * Restrict create action
     */
    public function createPostAction()
    {
        $this->_restrictNew();
    }

    /**
     * Login as Another Agent Action
     */
    public function loginAsAgentAction()
    {
        $session = $this->_getSession();
        $superAgent = $session->getSuperAgent();
        $agent = $session->getAgent();
        if(!($superAgent || $agent->getIsSuperAgent())) {
            $session->addError($this->__('Super agent rights are required.'));
            return ;
        } elseif ($this->isAjax()) {
            $login = $this->getRequest()->getPost();

            try {
                $actualQuote = Mage::getSingleton('checkout/cart')->getQuote();
                $agent->authenticate($login['username'], null, true);
                $username = $this->_getSession()->getAgent()->getUsername();
                $dealerId = $this->_getSession()->getAgent()->getDealer() ? $this->_getSession()->getAgent()->getDealer()->getVfDealerCode() : '';
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode(array("error" => false, "username" => $username, "dealer" => $dealerId)));
                // clear cart and create new quote
                Mage::helper('dyna_checkout')->createNewQuote($actualQuote);
            } catch (Mage_Core_Exception $e) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $e->getMessage())));
                $this->getResponse()->setHttpResponseCode(200);
            } catch (Exception $e) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setHttpResponseCode(200);
                $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $this->__('Server Error'))));
                // Mage::getSingleton('core/logger')->logException($e); // PA DSS violation: this exception log can disclose customer password
            }
            return ;
        } elseif ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username'])) {
                try {
                    $actualQuote = Mage::getSingleton('checkout/cart')->getQuote();
                    $agent->authenticate($login['username'], null, true);
                    // clear cart and create new quote
                    Mage::helper('dyna_checkout')->createNewQuote($actualQuote);
                } catch (Mage_Core_Exception $e) {
                    $session->addError($e->getMessage());
                } catch (Exception $e) {
                    $session->addError($this->__('Server Error'));
                    // Mage::getSingleton('core/logger')->logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } else {
                $session->addError($this->__('Login is required.'));
            }
            return ;
        }
        $this->_loginPostRedirect();
    }

    /**
     * Super Agent logged in as another agent logout action
     */
    public function logoutAsAgentAction()
    {
        $superAgent = $this->_getSession()->getSuperAgent();
        if (!$superAgent) {
            $this->getResponse()->setHttpResponseCode(200);
            $this->getResponse()->setHeader('Content-Type', 'application/json');
            $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $this->__('You are not logged in as another agent'))));
        } else {
            $this->_getSession()->setAgent($superAgent);
            $this->_getSession()->setSuperAgent(null);
            // clear cart and create new quote
            Mage::helper('dyna_checkout')->createNewQuote();

            $this->getResponse()->setHttpResponseCode(200);
        }
        return;
    }

    /**
     * Retrieve a list of all agents from the database
     */
    public function getAgentsListAction()
    {
        $superAgent = $this->_getSession()->getSuperAgent();
        $agent = $this->_getSession()->getAgent();
        $agents = array();
        if($superAgent || $agent->getIsSuperAgent()) {
            $currentStore = Mage::app()->getStore();
            $agentId = $superAgent
                ? $superAgent->getId()
                : $agent->getId();

            /** @var Dyna_Agent_Model_Mysql4_Agent_Collection $collection */
            $collection = Mage::getModel('agent/agent')
                ->getCollection();

            $collection
                ->addFieldToFilter('is_active', 1)
                ->addFieldToFilter('store_id', $currentStore->getId())
                ->addFieldToFilter('agent_id', array('neq' => $agentId))
            ;
            switch (Mage::helper('agent')->getWebsiteChannel()) {
                // Telesales
                case Dyna_Agent_Model_System_Config_Channel::WEBSITE_CHANNEL_TELESALES_CODE:
                    $collection->addFieldToFilter('role_id', array('nin' => array(Dyna_Agent_Model_Agentrole::SUPERAGENT_ROLE, Dyna_Agent_Model_Agentrole::BUSINESS_SPECIALIST)));
                    break;
                // Retail
                case Dyna_Agent_Model_System_Config_Channel::WEBSITE_CHANNEL_RETAIL_CODE:
                    $collection->addFieldToFilter('role_id', array('eq' => Dyna_Agent_Model_Agentrole::BUSINESS_SPECIALIST));
                    break;
                default:
            }

            $collection->load();
            $dealerCodes = Mage::getModel('agent/dealer')->getCollection()->load();
            $allAgents = $collection->getItems();
            /** @var Dyna_Agent_Model_Agent $agent */
            foreach ($allAgents as $agent) {
                $agents[] = array(
                    'value' => $agent->getUsername(),
                    'data' => $agent->getDealerId() ? $dealerCodes->getItemById($agent->getDealerId())->getVfDealerCode() : null
                );
            }
        }

        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode(array("error" => false, "agents" => $agents)));
        $this->getResponse()->setHttpResponseCode(200);

        return;
    }

    /**
     * @IsGranted({"permissions":["CHANGE_PASSWORD"]})
     */
    public function changeAgentPasswordAction()
    {
        $session = $this->_getSession();
        $superAgent = $session->getSuperAgent();
        /** @var Dyna_Agent_Model_Agent $agent */
        $agent = $superAgent ? $superAgent : $session->getAgent();
        $agent = Mage::getModel('agent/agent')->load($agent->getId());
        if( ! $agent) {
            $session->addError($this->__('You must be logged in as an agent'));
        } elseif ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();

            if(!$agent->isGranted('CHANGE_PASSWORD')) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $this->__('You don\'t have permission to reset password'))));
                $this->getResponse()->setHttpResponseCode(200);
                return;
            }

            if (count(array_filter($data)) !== 3
                || ! isset($data['password'])
                || ! isset($data['password-again'])
                || ! isset($data['curr-password'])
            ) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $this->__('Please fill all fields'))));
                $this->getResponse()->setHttpResponseCode(200);
                return;
            }

            if ($data['password'] !== $data['password-again']) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $this->__('The new password does not match'))));
                $this->getResponse()->setHttpResponseCode(200);
                return;
            }

            try{
                Mage::helper('agent')->validatePassword($agent, $data['password-again']);
            }catch(Exception $e) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $e->getMessage())));
                $this->getResponse()->setHttpResponseCode(200);
                return;
            }

            if ( ! $agent->validatePassword($data['curr-password'], $agent)) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $this->__('Your current password does not match the provided one'))));
                $this->getResponse()->setHttpResponseCode(200);
                return;
            }

            $agent->setPassword($data['password'])
                ->save();

            $this->getResponse()->setHeader('Content-Type', 'application/json');
            $this->getResponse()->setBody(json_encode(array("error" => false, "message" => $this->__('The new password has been saved'))));
            $this->getResponse()->setHttpResponseCode(200);
            return;
        }
    }

    /**
     * Reset the password for an agent
     * @IsGranted({"permissions":["CHANGE_PASSWORD"]})
     */
    public function passwordAction()
    {
        if ($this->_getSession()->getAgent()) {
            $this->_redirectUrl(Mage::getUrl());
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    /**
     * Reset the password for an agent
     * @IsGranted({"permissions":["CHANGE_PASSWORD"]})
     */
    public function temporarypasswordAction()
    {
        if(!Mage::getSingleton('customer/session')->getTemporaryAgent()) {
            $this->_redirectUrl(Mage::getUrl());
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    /**
     * Reset action post for the agent password
     */
    public function passwordResetAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            if (isset($data['token']) && strlen($data['token']) > 0) {
                $agent = Mage::helper('agent')->validatePasswordResetToken($data['token']);
                $isTemporary = false;
            } else {
                $agent = Mage::getSingleton('customer/session')->getTemporaryAgent();
                $isTemporary = true;
            }

            try {
                if( !$agent || $agent->getId() == null) {
                    throw new Exception(sprintf($this->__('No agent to change the password for was found. Please try to login again by going to the following link: %s'), $this->_getUrl('/agent/account/login')));
                }

                if(!isset($data['password']) || !isset($data['password_confirmation']) || ($data['password'] != $data['password_confirmation'])) {
                    throw new Exception($this->__('The password is different from the password confirmation. Please try again.'));
                }

                Mage::helper('agent')->validatePassword($agent, $data['password']);
                // Password is automatically encrypted through agent model -> setPassword method
                $newPassword = $data['password'];
                if ($isTemporary) {
                    $agent->setTemporaryPassword(0);
                    Mage::getSingleton('customer/session')->unsTemporaryAgent();
                }
                $agent->setPassword($newPassword);
                $agent->setPasswordResetToken(null);
                $agent->setPasswordResetTokenExpiryDate(null);
                $agent->save();

                $this->_getSession()->addSuccess($this->__('Password changed successfully.'));
                $this->_redirect('*/*/login');
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirectReferer('agent/account/' . ($isTemporary ? 'temporaryPassword' : 'password'));
                return;
            }
        }
    }

    /**
     * Send the password or username forgot email
     */
    public function forgotCredentialsAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('forgot');
            if (!empty($data['username'])) {
                try {
                    $agent = Mage::getModel('agent/agent')->load($data['username'], 'username');
                    $agent->forgotPassword();

                    $result['error'] = false;
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                } catch (Exception $e) {
                    $result['error'] = true;
                    $result['message'] = $e->getMessage();
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }
            } else {
                $result['error'] = true;
                $result['message'] = $this->__('Missing input data');
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        } else {
            return $this->_redirect('*/*/login');
        }
    }
}
