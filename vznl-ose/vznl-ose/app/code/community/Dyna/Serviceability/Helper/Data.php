<?php

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;

/**
 * Class Dyna_Serviceability_Helper_Data
 */
class Dyna_Serviceability_Helper_Data extends Mage_Core_Helper_Abstract
{

    const CACHE_KEY = 'serviceability_data_quote_id_';

    /**
     * @var GuzzleHttp Client
     */
    public $client;

    /**
     * Dyna_Serviceability_Helper_Data constructor.
     * @param Client $serviceabilityClient
     */
    public function __construct(Client $serviceabilityClient)
    {
        $this->client = $serviceabilityClient;
    }

    /**
     * @param string $requestType
     * @param string $postalCode
     * @param string|null $houseNumber
     * @param string|null $houseNumberAddition
     * @param string|null $addressId
     * @param string|null $street
     * @param string|null $city
     * @param string|null $country
     * @param string|null $locationId
     * @return mixed|null|\Psr\Http\Message\ResponseInterface
     */
    public function getServiceability(
        string $requestType = 'GET',
        string $postalCode,
        string $houseNumber = null,
        string $houseNumberAddition = null,
        string $addressId = null,
        string $street = null,
        string $city = null,
        string $country = null,
        string $locationId = null
    ) {
        $mappedAddress = [
            'id' => $addressId,
            'street' => $street,
            'housenumber' => $houseNumber,
            'suffix' => $houseNumberAddition,
            'postalcode' => $postalCode,
            'city' => $city,
            'country' => $country,
            'locationId' => $locationId,
        ];

        $serviceabilityEndpoint = Mage::getStoreConfig('omnius_service/service_ability/endpoint');
        $url = $serviceabilityEndpoint . "?" . http_build_query($mappedAddress);
        $result = null;
        try {
            $result = $this->client->request($requestType, $url);
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            Mage::logException($responseBodyAsString);
        }

        return $result;
    }

    /**
     * @param string $postalCode
     * @param string $houseNumber
     * @param string|null $addressId
     * @param string|null $street
     * @param string|null $houseNumberAddition
     * @param string|null $city
     * @param string|null $country
     * @param string|null $locationId
     * @param int $quoteId
     * @return mixed
     */
    public function updateServiceability(
        string $postalCode,
        string $houseNumber = null,
        string $houseNumberAddition = null,
        string $addressId = null,
        string $street = null,
        string $city = null,
        string $country = null,
        string $locationId = null,
        int $quoteId
    ) {
        $cache = Mage::getSingleton('dyna_cache/cache');
        $cacheKey = self::CACHE_KEY . $quoteId;

        $mappedAddress = [
            'id' => $addressId,
            'street' => $street,
            'housenumber' => $houseNumber,
            'suffix' => $houseNumberAddition,
            'postalcode' => $postalCode,
            'city' => $city,
            'country' => $country,
            'locationId' => $locationId,
        ];

        if ($cacheData = $cache->load($cacheKey)) {
            $serviceabilityData = json_decode(unserialize($cacheData), true);
        } else {

            $curlResponse = $this->getServiceability('GET',
                $postalCode,
                $houseNumber,
                $houseNumberAddition,
                $addressId,
                $street,
                $city,
                $country,
                $locationId);
            if ($curlResponse) {
                $serviceabilityData = json_decode($curlResponse->getBody(), true);
                $cache->save(serialize($curlResponse->getBody()), $cacheKey, array(), $cache->getTtl());
            }
        }

        $serviceabilityClient = Mage::getModel("dyna_address/client_CheckServiceAbilityClient");
        $response = $serviceabilityClient->executeCheckServiceAbility((array)$serviceabilityData, $mappedAddress);

        return $response;
    }
}