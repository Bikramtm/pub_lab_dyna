<?php

$installer = $this;

/**
 * Create table 'catalog_product_search'
 */
if ( ! $installer->getConnection()->isTableExists($installer->getTable('stock/stock'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('stock/stock'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
        ), 'Product Identifier')
        ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
        ), 'Store Identifier')
        ->addColumn('item_count', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'unsigned' => true
        ), 'Item Stock Count')
        ->addColumn('other_store_list', Varien_Db_Ddl_Table::TYPE_TEXT, 65535, array(
            'nullable'  => true,
        ), 'Other Store List')
        ->addIndex($installer->getIdxName('stock/stock', array('product_id', 'store_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('product_id', 'store_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('Catalog Product Stock')
        ->setOption('charset', 'utf8');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();