<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `dyna_store_stock` (
  `entity_id` INT NOT NULL,
  `product_id` INT NOT NULL,
  `store_id` INT NOT NULL,
  `qty` INT NOT NULL,
  `last_modified` INT NOT NULL,
  PRIMARY KEY (`entity_id`));

ALTER TABLE `dyna_store_stock` 
CHANGE COLUMN `entity_id` `entity_id` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `dyna_store_stock` 
ADD UNIQUE INDEX `idx_productid_storeid` (`product_id` ASC, `store_id` ASC);


SQLTEXT;

$installer->run($sql);

$installer->endSetup();
	 
