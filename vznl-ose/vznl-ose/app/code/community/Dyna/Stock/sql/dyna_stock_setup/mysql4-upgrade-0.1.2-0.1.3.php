<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT

ALTER TABLE `dyna_store_stock` 
ADD COLUMN `backorder_qty` INT(11) NOT NULL DEFAULT 0 AFTER `qty`;

SQLTEXT;

$installer->run($sql);

$installer->endSetup();
	 
