<?php

/**
 * Class Cron
 */
class Dyna_Stock_Model_Cron extends Varien_Object
{
    const DISTANCE_RANGE = 500; //TODO this may need to be dynamic?

    /** @var Dyna_Service_Model_Client_AxiClient */
    protected static $client;

    /** @var Dyna_Configurator_Model_Cache */
    protected static $_cache;

    /**
     * Retrieves fresh stock information for each
     * of the existing products and saves the info
     * in the stock table
     *
     * If the response fails or returns no data, we leave
     * the existing stock information (if exists) intact,
     * otherwise we delete it and save the new one in its place,
     * all actions being made through a transaction
     */
    public static function updateStock(array $productIds = null)
    {
        /** @var Dyna_Service_Model_DotAccessor $accessor */
        $accessor = Mage::getModel('dyna_service/dotAccessor');

        /**
         * This way we make only one SQL call instead of one per product
         * @var Dyna_Stock_Model_Mysql4_Stock_Collection $stockCollection */
        $stockCollection = Mage::getResourceModel('stock/stock_collection');

        /**
         * Use a transaction
         * @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getModel('core/resource_transaction');

        $productCollection = Mage::getResourceModel('catalog/product_collection');

        if (is_array($productIds)) {
            $productCollection->addIdFilter($productIds);
        }

        $productSkus = array_unique(array_filter($productCollection->getColumnValues('sku')));

        $dealers = Mage::getModel('agent/dealer')->getCollection();

        foreach ($dealers as $dealer) {
            if ( ! $dealer->getAxiStoreCode()) {
                continue;
            }

            try {
                $resultIncludingWarehouse = static::getAxiClient()->getStock($productSkus, $dealer->getAxiStoreCode(), self::DISTANCE_RANGE, true);
                $resultExcludingWarehouse = static::getAxiClient()->getStock($productSkus, $dealer->getAxiStoreCode(), self::DISTANCE_RANGE, false);

                if ( ! $accessor->getValue($resultExcludingWarehouse, 'request_status')) {
                    foreach ($resultExcludingWarehouse as $productSku => $data) {
                        /**
                         * If it already has a stock for it, just update it
                         */
                        $includingWarehouse = $accessor->getValue($resultIncludingWarehouse, $productSku);
                        $warehouseStock = 0;
                        if ($includingWarehouse && $warehouseStock = $accessor->getValue($includingWarehouse, 'stock')) {
                            $warehouseStock = (int) $warehouseStock - (int) $accessor->getValue($data, 'stock');
                        }

                        $stock = $stockCollection->getItemByColumnValue('product_id', $productSku);

                        if ($stock && $stock->getStoreId() == $dealer->getAxiStoreCode()) {
                            $stock
                                ->setProductId($productSku)
                                ->setStoreId($dealer->getAxiStoreCode())
                                ->setItemCount($accessor->getValue($data, 'stock'))
                                ->setWarehouseCount($warehouseStock)
                                ->setOtherStoreList($accessor->getValue($data, 'other_store_stock_list.item'));

                        } else { //else create the stock
                            $stock = Mage::getModel('stock/stock')
                                ->setProductId($productSku)
                                ->setStoreId($dealer->getAxiStoreCode())
                                ->setItemCount($accessor->getValue($data, 'stock'))
                                ->setWarehouseCount($warehouseStock)
                                ->setOtherStoreList($accessor->getValue($data, 'other_store_stock_list.item'));
                        }

                        if (Mage::helper('dyna_service')->useStubs()) {
                            /**
                             * HACK for stub mode
                             * TODO remove this when GetStock call is implemented on AXI part
                             */
                            $stock->setWarehouseCount(50);
                        }

                        $transaction->addObject($stock);
                    }

                } else {
                    Mage::log(sprintf(
                        'Stock for item %s return status "%s" with description "%s".',
                        $accessor->getValue($resultExcludingWarehouse, 'request_status.status'),
                        $accessor->getValue($resultExcludingWarehouse, 'request_status.description')
                    ), Zend_Log::EMERG);
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                //TODO should this be silent?
            }
        }

        /**
         * Commit the changes
         */
        try {
            $transaction->save();
            /**
             * Clear cache after updating the table
             */
            self::getCache()->clean(array('catalog_product_stock'), Zend_Cache::CLEANING_MODE_MATCHING_TAG);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            //TODO should this be silent?
        }
    }

    /**
     * @return Dyna_Service_Model_Client_AxiClient
     */
    protected static function getAxiClient()
    {
        if ( ! static::$client) {
            static::$client = Mage::helper('dyna_service')->getClient('axi');
        }
        return static::$client;
    }

    /**
     * @return Dyna_Configurator_Model_Cache
     */
    protected static function getCache()
    {
        if ( ! self::$_cache) {
            self::$_cache = Mage::getSingleton('dyna_configurator/cache');
        }
        return self::$_cache;
    }
}