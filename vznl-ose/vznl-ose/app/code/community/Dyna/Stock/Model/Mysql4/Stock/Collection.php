<?php

/**
 * Class Dyna_Stock_Model_Mysql4_Stock_Collection
 */
class Dyna_Stock_Model_Mysql4_Stock_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('stock/stock');
    }

    public function delete()
    {
        foreach ($this->getItems() as $item) {
            $item->delete();
            unset($this->_items[$item->getId()]);
        }
        return $this;
    }
}
