<?php
	
class Dyna_Stock_Block_Adminhtml_Storestock_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "entity_id";
				$this->_blockGroup = "stock";
				$this->_controller = "adminhtml_storestock";
				$this->_updateButton("save", "label", Mage::helper("stock")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("stock")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("stock")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("storestock_data") && Mage::registry("storestock_data")->getId() ){

				    return Mage::helper("stock")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("storestock_data")->getId()));

				} 
				else{

				     return Mage::helper("stock")->__("Add Item");

				}
		}
}