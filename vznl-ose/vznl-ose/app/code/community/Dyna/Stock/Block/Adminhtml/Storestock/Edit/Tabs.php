<?php
class Dyna_Stock_Block_Adminhtml_Storestock_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("storestock_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("stock")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("stock")->__("Item Information"),
				"title" => Mage::helper("stock")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("stock/adminhtml_storestock_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
