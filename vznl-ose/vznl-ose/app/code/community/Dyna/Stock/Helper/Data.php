<?php

/**
 * Class Dyna_Stock_Helper_Data
 */
class Dyna_Stock_Helper_Data extends Mage_Core_Helper_Data
{
    public function getPackageStockStatus($packageId)
    {
        return Dyna_Stock_Model_Stock::STATUS_IN_STOCK; //temporary fix until getStock call finalised

        $packageItems = $this->_getQuote()->getPackageItems($packageId);
        $status = Dyna_Stock_Model_Stock::STATUS_IN_STOCK;
        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($packageItems as $item) {
            if (!Mage::getModel('stock/stock')->getItemCount($item->getProduct())) {
                $status = Dyna_Stock_Model_Stock::STATUS_OUT_OF_STOCK;
                break;
            }
        }
        return $status;
    }

    public function getStockInStore($productId, $storeId)
    {
        /*$stockColl = Mage::getResourceModel('stock/storestock_collection')
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToSelect('qty');

        $stock = Mage::getSingleton('core/resource')
            ->getConnection('core_read')
            ->fetchCol($stockColl->getSelect()->reset('COLUMNS'));
        */

        /** @var Varien_Db_Adapter_Interface $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $sth = $connection->prepare("SELECT `main_table`.qty FROM `dyna_store_stock` AS `main_table` WHERE (product_id = ?) AND (store_id = ?)");
        $sth->execute(array((int) $productId, (int) $storeId));

        if ($stock = $sth->fetch())
            return $stock['qty'];
        else
            return 0;
    }

    /**
     * Get the stock amount of all stores except the store with the given store id.
     * @param $productId The id of the product.
     * @param $axiStoreCode The store code.
     * @param $storeId The store id.
     * @return integer The amount of stock for all other stores.
     */
    public function getStockOtherStores($productId, $axiStoreCode, $storeId)
    {
        $warehouseStoreId = Mage::getStoreConfig('vodafone_service/axi/vodafone_warehouse');

        /** @var Dyna_Stock_Model_Mysql4_Storestock_Collection $storeStockCollection */
        $storeStockCollection = Mage::getModel('stock/storestock')->getCollection();
        $storeStockCollection->addFieldToFilter('product_id', $productId);
        $storeStockCollection->addFieldToFilter('main_table.store_id', ['nin' => [$axiStoreCode, $warehouseStoreId]]);
        $storeStockCollection->getSelect()->group('store_id');
        $storeStockCollection->join(['d' => 'agent/dealer'], 'main_table.store_id=d.axi_store_code', ['dealer_store_id' => 'd.store_id']);
        $storeStockCollection->addFieldToFilter('d.store_id', $storeId);

        $stockCollection = $storeStockCollection->getColumnValues('qty');
        return array_sum($stockCollection);
    }

    public function getStockForProducts(array $productIds, $storeId)
    {
        die($storeId);
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $col = Mage::getResourceModel('stock/storestock_collection')
            ->addFieldToFilter('product_id', array('in' => $productIds))
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToSelect(array('product_id', 'qty', 'backorder_qty'));
        $data = array();
        foreach ($adapter->fetchAll($col->getSelect()->reset('COLUMNS')) as $row) {
            $data[$row['product_id']]['qty'] = $row['qty'];
            $data[$row['product_id']]['backorder_qty'] = isset($row['backorder_qty']) ? $row['backorder_qty'] : 0;
        }

        return $data;
    }

    /**
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    protected function _getQuote()
    {
        return Mage::getSingleton('checkout/session')->getQuote();
    }
}
