<?php

class Dyna_Service_Helper_Request extends Omnius_Service_Helper_Request
{
    /**
     * @param $namespace
     * @param $method
     * @param array $params
     * @return mixed
     */
    public function create($namespace, $method, array $params = array())
    {
        $schema = $this->_loadSchema($namespace, $method);

        $this->_injectParams($schema, $params);

        $request = trim(str_replace('<?xml version="1.0"?>', '', html_entity_decode($schema->asPrettyXML(), ENT_NOQUOTES, 'UTF-8')));

        return $this->convertEncoding($request);
    }

    /**
     * Returns an array of paths
     * @param $moduleName
     * @param $folderPrefix
     * @param $folderSuffix
     * @param $results
     */
    public function getDependencyPaths($moduleName, $folderPrefix, $folderSuffix, &$results)
    {
        $modules = Mage::getConfig()->getNode('modules');
        $currentModule = $modules->{$moduleName};

        $currentPath = Mage::getModuleDir($folderPrefix, $moduleName);
        if ($folderSuffix) {
            $currentPath = $currentPath . DS . $folderSuffix;
        }

        $results[] = $currentPath;

        if ($currentModule->depends) {
            foreach ($currentModule->depends as $key => $depends) {
                if ($key == "depends" && !empty((array) $depends)) {
                    foreach ($depends as $dependencyKey => $dependencyValue) {
                        $this->getDependencyPaths($dependencyKey, $folderPrefix, $folderSuffix, $results);
                    }
                }
            }
        }
    }

    public function getDependencyNamespaces($namespace, &$results)
    {
        $modules = Mage::getConfig()->getNode('modules');
        $currentModule = $modules->{$namespace};

        $results[] = $namespace;

        if ($currentModule->depends) {
            foreach ($currentModule->depends as $key => $depends) {
                if ($key == "depends" && !empty((array) $depends)) {
                    foreach ($depends as $dependencyKey => $dependencyValue) {
                        $this->getDependencyNamespaces($dependencyKey, $results);
                    }
                }
            }
        }
    }
}