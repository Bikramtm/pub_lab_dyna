<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');
$query = 'UPDATE core_config_data SET core_config_data.value="OSF" WHERE core_config_data.path="omnius_service/service_settings_header/application_id" AND core_config_data.scope_id="0"';
$writeConnection->query($query);
