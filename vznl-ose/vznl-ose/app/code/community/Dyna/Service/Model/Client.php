<?php

/**
 * Class Dyna_Service_Model_Client
 */
class Dyna_Service_Model_Client extends Omnius_Service_Model_Client_Client
{
    const CONFIG_PATH = 'omnius_service/';
    const WSDL_CONFIG_KEY = "wsdl";
    const ENDPOINT_CONFIG_KEY = "usage_url";
    const CONFIG_HEADER_PATH = 'omnius_service/service_settings_header/';
    const ERROR_DESCRIPTION_SUCCESS = "SUCCESS";
    const CONFIG_PREVENT_EMPTY_NODES = 'omnius_service/general/prevent_empty_nodes';
    const NO_ERROR_CODE = "ERR-0000";
    const BASIC_AUTH_USERNAME = 'basic_auth_username';
    const BASIC_AUTH_PASSWORD = 'basic_auth_password';

    /** @var Omnius_Service_Helper_Data */
    protected $helper = null;

    protected $cache;

    protected $xmlStubs = false;

    /**
     * Dyna_Service_Model_Client constructor.
     * @param null $wsdl
     * @param array $options
     */
    public function __construct($wsdl = null, $options = array())
    {
        try {
            $this->loadConfig();
            $this->setup();

            $defaultOptions = $this->getDefaultOptions();

            $options = array_merge(isset($options['options']) ? $options['options'] : array(), $defaultOptions);

            if (!empty($wsdl)) {
                $this->_wsdl = $wsdl;
            }

            $this->_context = stream_context_create(array(
                "ssl" => array(
                    "verify_peer" => (bool)$this->getHelper()->getGeneralConfig('stream_context_verify_peer'),
                    "allow_self_signed" => (bool)$this->getHelper()->getGeneralConfig('stream_context_allow_self_signed'),
                )
            ));
            $this->_options = array_merge($options, array('stream_context' => $this->_context));
            $this->_soapDebug = 'services' . DS . 'soap_debug_%s.log';

        } catch (Exception $e) {
            Mage::throwException('SOAP ERROR: Could not connect to WSDL. ' . $e->getMessage());
        }
    }

    /**
     * Load config value for WSDL and Schema
     */
    public function loadConfig()
    {
        if (empty(static::WSDL_CONFIG_KEY)) {
            Mage::throwException(
                sprintf("Invalid WSDL_CONFIG_KEY in class %s", get_called_class())
            );
        }
        $this->_wsdl = $this->getConfig(static::WSDL_CONFIG_KEY);
        if (empty($this->_wsdl) && !$this->getUseStubs()) {
            Mage::throwException(
                sprintf("Empty WSDL in class %s\n Please check endpoint and WSDL configuration fields in OSE backOffice", get_called_class())
            );
        }

        /** @var array $validPaths */
        $validPaths = array();

        /** @var Dyna_Service_Helper_Request $serviceHelper */
        $serviceHelper = Mage::helper('dyna_service/request');
        $serviceHelper->getDependencyPaths($this->getNamespace(), 'etc', 'schema', $validPaths);

        /** @var Omnius_Service_Helper_Request $requestBuilder */
        $requestBuilder = $this->_getRequestBuilder();

        foreach ($validPaths as $validPath) {
            if (realpath($validPath)) {
                $requestBuilder->setSchemaDir($validPath);
                break;
            }
        }
    }

    /**
     * Check stub mode state
     * @return bool
     */
    public function getUseStubs()
    {
        // Use the general settings stub mode
        if ($this->getHelper()->useStubs() || !is_null(Mage::getSingleton('customer/session')->getData(Dyna_AgentDE_Model_Observer::CUSTOM_STUBS_NAMESPACE_PARAM))) {
            return true;
        }
        // Use each service stub mode if general settings = No
        if (static::CONFIG_STUB_PATH) {
            return $this->getConfig(static::CONFIG_STUB_PATH);
        }

        return parent::getUseStubs();
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        if (!$this->_namespace) {
            preg_match('/(.*)_Model_Client/', get_called_class(), $parts);
            if (isset($parts[1])) {
                $this->_namespace = $parts[1];
            } else {
                Mage::throwException(
                    sprintf('Client class (%s) does not follow naming convention', get_called_class())
                );
            }
        }

        return $this->_namespace;
    }

    public function setup()
    {
        // No login at this moment
    }

    /**
     * @return array
     */
    public function getDefaultOptions()
    {
        $options = [
            'soap_version' => SOAP_1_1,
            'trace' => true,
            'connection_timeout' => $this->getHelper()->getGeneralConfig('timeout'),
            'keep_alive' => false,
            'cache_wsdl' => constant(Mage::getStoreConfig('omnius_service/general/wsdl_cache_service'))
        ];

        if ($this->getConfig(static::ENDPOINT_CONFIG_KEY)) {
            $options['location'] = $this->getConfig(static::ENDPOINT_CONFIG_KEY);
        }

        $currentUsername = $this->getConfig(static::BASIC_AUTH_USERNAME);
        $currentPassword = $this->getConfig(static::BASIC_AUTH_PASSWORD);

        if (!$currentPassword || !$currentUsername) {

            $wsdlPath = explode('/', static::WSDL_CONFIG_KEY);
            $wsdlPath = $wsdlPath[0];

            $currentUsername = $this->getConfig($wsdlPath . '/' . static::BASIC_AUTH_USERNAME);
            $currentPassword = $this->getConfig($wsdlPath . '/' . static::BASIC_AUTH_PASSWORD);

        }
        if (!$currentPassword || !$currentUsername) {
            //load default user & pass
            $currentUsername = Mage::getStoreConfig(self::CONFIG_PATH . 'general/' . self::BASIC_AUTH_USERNAME);
            $currentPassword = Mage::getStoreConfig(self::CONFIG_PATH . 'general/' . self::BASIC_AUTH_PASSWORD);
        }
        if ($currentUsername && $currentPassword) {
            $options['login'] = $currentUsername;
            $options['password'] = $currentPassword;
        }

        return $options;
    }

    /**
     * Prevent helper from being cached in the registry
     * @return Mage_Core_Helper_Abstract
     */
    protected function getHelper()
    {
        if (!$this->helper) {
            $helperClass = Mage::getConfig()->getHelperClassName('omnius_service');
            $this->helper = new $helperClass;
        }

        return $this->helper;
    }

    /**
     * @return Omnius_Service_Helper_Request
     */
    protected function _getRequestBuilder()
    {
        if (!$this->_requestBuilder) {
            $helperClass = Mage::getConfig()->getHelperClassName('omnius_service/request');
            $this->_requestBuilder = new $helperClass;
        }

        return $this->_requestBuilder;
    }

    /**
     * @param $response
     * @throws Exception
     */
    public function validateResponseStatus($response)
    {
        if (!isset($response['Status'])) {
            throw new Exception("Invalid response structure, missing status.");
        }
        if ($response['Status']['StatusReasonCode'] !== self::NO_ERROR_CODE) {
            throw new Exception(htmlentities($response['Status']['StatusReason']));
        }
    }

    /**
     * Overwrite default handling of calls to check if service is valid by checking the success node value
     * @param string $name
     * @param string $arguments
     * @return array|mixed
     * @throws Exception
     * @throws Zend_Soap_Client_Exception
     */
    public function __call($name, $arguments)
    {
        $response = parent::__call($name, $arguments);

        $serviceError = $this->hasServiceErrors($response);
        $ogwError = false;

        // OMNVFDE-2599 the success for direct OGW responses is stored in ['ROOT']['Status']['ErrorDescription'] node
        if (!isset($response['ROOT'])
            || !isset($response['ROOT']['Status'])
            || !isset($response['ROOT']['Status']['ErrorDescription'])
            || (strtolower($response['ROOT']['Status']['ErrorDescription']) != strtolower(self::ERROR_DESCRIPTION_SUCCESS))
        ) {
            $ogwError = true;
        }

        //if no success node present or false, build the error message
        if ($serviceError && $ogwError) {
            $error = $name . " " . Mage::helper('dyna_checkout')->__('service was not successful');
            if (isset($response['Status']['StatusReason']) && $response['Status']['StatusReasonCode'] !== self::NO_ERROR_CODE) {
                if (is_object($response['Status']['StatusReasonCode']) || is_array($response['Status']['StatusReasonCode'])) {
                    $error .= " : " . "a WebServiceUALException occurred";
                } else {
                    $error .= " : " . $response['Status']['StatusReason'];
                }
            }

            if (isset($response['ROOT']) && isset($response['ROOT']['Status']['ErrorDescription'])) {
                $error .= " : " . $response['ROOT']['Status']['ErrorDescription'];
            }

            $exception = new Exception(htmlentities($error));
            $exception->statusReasonCode = $response['Status']['StatusReasonCode'];

            throw $exception;
        }

        return $response;
    }


    /**
     * @param array $response
     * @return bool
     */
    protected function hasServiceErrors($response)
    {
        // If Success node differs from true (and for OIL StatusReasonCode differs from ERR-000) we have an error @see OMNVFDE-2851
        return !isset($response['Success'])
            || $response['Success'] != 'true'
            || (isset($response['Status']['StatusReasonCode']) && $response['Status']['StatusReasonCode'] !== self::NO_ERROR_CODE);
    }

    public function buildCacheKey(string $method, array $arguments): string
    {
        return md5($method . serialize($arguments));
    }

    /**
     * @param $searchParams
     */
    public function setOgwHeaderInfo(&$searchParams, $type = null)
    {
        $agent = Mage::getSingleton('customer/session')->getAgent();
        $searchParams['ROOT']['Event'] = array(
            'Type' => $type,
            'ID' => uniqid(),
            'CallerID' => 'Omnius',
            'CorrelationID' => Mage::getSingleton("core/session")->getEncryptedSessionId(),
            'SourceID' => null,
            'OriginalAppl' => null,
            'OriginalPubID' => null,
            'MessageCreationTimestamp' => date("Y-m-d") . 'T' . date("H:i:s"),
            'FrontendUserId' => $agent ? $agent->getId() : null,
            'DealerCode' => $agent ? $agent->getDealerCode() : null,
            'SalesChannel' => Mage::helper('agent')->getWebsiteChannel() ?: null,
            'DealerOrganisation' => null,
            'ShopID' => null,
        );
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->cache) {
            $this->cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->cache;
    }

    /**
     * @param $searchParams
     */
    protected function setRequestHeaderInfo(&$searchParams)
    {
        $searchParams['HeaderInfo']['TransactionId'] = Dyna_Core_Helper_Data::getRequestUniqueTransactionId();
        $searchParams['HeaderInfo']['UserId'] = Mage::helper('agent')->getAgentId();
        $searchParams['HeaderInfo']['ApplicationId'] = $this->getApplicationId();
        $searchParams['HeaderInfo']['PartyName'] = $this->getPartyName();
        $searchParams['HeaderInfo']['CorrelationId'] = Mage::getSingleton("core/session")->getEncryptedSessionId();
    }

    /**
     * Get application id from the configurable input in Admin
     * @return bool
     */
    public function getApplicationId()
    {
        return $this->getServiceSettingsHeaderConfig('application_id');
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getServiceSettingsHeaderConfig($config)
    {
        return Mage::getStoreConfig(static::CONFIG_HEADER_PATH . $config);
    }

    /**
     * Get party_name from the configurable input in Admin
     * @return bool
     */
    public function getPartyName()
    {
        return $this->getServiceSettingsHeaderConfig('party_name');
    }

    /**
     * Override proxy call to remove Omnius specific logic
     * @param $name
     * @param $arguments
     * @param bool $isJob
     * @throws Exception
     * @throws SoapFault
     * @throws Zend_Soap_Client_Exception
     * @return array|mixed
     */
    protected function proxyCall($name, $arguments, $isJob = false)
    {
        $requestHash = md5(serialize($arguments[0]));
        if ($this->getUseStubs()) {
            $requestData = $isJob ? $arguments : $this->buildRequest($name, $arguments[0]);
            /** @var Dyna_Service_Model_MultipleStubClient $stubClient */
            $stubClient = $this->getStubClient();
            $stubClient
                ->setNamespace($this->getNamespace())
                ->setRequestHash($requestHash)
                ->setXmlStubs($this->hasXmlStubs());
            $responseData = $stubClient->call($name, ($isJob ? array($arguments) : $arguments));
            $this->logStubTransfer($requestData, $responseData);

            return $responseData;
        }

        $oldTimeout = ini_get('default_socket_timeout');
        $asyncTimeout = (int)$this->getHelper()->getGeneralConfig('async_timeout');
        $newTimeout = ($isJob && $asyncTimeout)
            ? $asyncTimeout
            : ($this->getHelper()->getGeneralConfig('timeout')
                ? (int)$this->getHelper()->getGeneralConfig('timeout')
                : 60);
        ini_set('default_socket_timeout', $newTimeout);

        //load WSDL
        $this->_init();

        $logDone = false;
        try {
            if ($isJob) {
                $callArgs = array(new SoapVar($arguments, XSD_ANYXML));
            } else {
                $callArgs = $this->_prepareArguments($name, $arguments[0]);
            }

            $callResult = SoapClient::__soapCall($name, $callArgs);
            $exceptionalResult = null;
            $faultyCalls = [
                'SearchCustomer' => 'SearchCustomerResponse',
                'ObjectSearch' => 'ObjectSearchResponse',
                'GetHouseHoldMembers' => 'GetHouseHoldMembersResponse',
                'WSSearchPotentialLinks' => 'SearchPotentialLinksResponse',
                'WSSearchCustomerFromLegacy' => 'SearchCustomerFromLegacyResponse',
                'RetrieveCustomerInfo' => 'RetrieveCustomerInfoResponse',
                'CheckServiceAbility' => 'CheckServiceAbilityResponse',
                'RetrieveCustomerCreditProfile' => 'RetrieveCustomerCreditProfileResponse',
                'SendDocument' => 'SendDocumentResponse'
            ];

            $stringResponse = $this->__getLastResponse();
            // set service response on session for later usage in rules by xpath
            Mage::getSingleton('customer/session')->addServiceResponse($name, $stringResponse, $requestHash);

            if (in_array($name, array_keys($faultyCalls))) {
                $exceptionalResult = $this->getXmlError($stringResponse);
                $responseTag = $faultyCalls[$name];
                $exceptionalResult = $exceptionalResult->Body->$responseTag;
            }

            $response = $this->getNormalizer()
                ->normalizeKeys(json_decode(json_encode($exceptionalResult ?: $callResult), true));
        } catch (Exception $e) {
            ini_set('default_socket_timeout', $oldTimeout);
            // log request and response
            $this->logTransfer();
            $e->logSoapException = $this->logSoapException;

            throw $e;
        }

        ini_set('default_socket_timeout', $oldTimeout);
        if (!$logDone) {
            //log request and response if not already logged
            $this->logTransfer();
        }

        return $response;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    public function getStubClient()
    {
        return Mage::getModel('dyna_service/multipleStubClient');
    }

    /**
     * @return Omnius_Service_Model_Normalizer
     */
    protected function getNormalizer()
    {
        if (!$this->_normalizer) {
            $this->_normalizer = Mage::getSingleton('dyna_service/normalizer');
        }

        return $this->_normalizer;
    }

    /**
     * Overwrite log transfer to log duration of calls
     */
    protected function logTransfer()
    {
        $requestBody = $this->__getLastRequest();
        $requestBody = str_replace(["\n", "\r"], '', $requestBody);
        $requestHeaders = $this->__getLastRequestHeaders();

        $soapName = explode('SOAPAction: ', $requestHeaders);
        $soapName = explode('Content-Length:', $soapName[1]);

        $reqData = explode('X-START-DATE: ', $requestHeaders);
        $reqData = str_replace(' UTC', '', str_replace(' GMT', '', end($reqData)));
        $startTime = strtotime($reqData);

        $responseBody = $this->__getLastResponse();
        $responseBody = str_replace(["\n", "\r"], '', $responseBody);
        $responseHeaders = $this->__getLastResponseHeaders();
        $responseHeadersArr = explode(' ', $responseHeaders);
        $httpResponseCode = $responseHeadersArr[1];
        if ($httpResponseCode == 200) {
            $logLevel = Zend_Log::ERR;
        } else {
            $logLevel = Zend_Log::CRIT;
        }

        $resData = explode('Date: ', $responseHeaders);
        $resData = explode("\nConnection", end($resData));
        $resData = str_replace(' UTC', '', str_replace(' GMT', '', current($resData)));
        $stopTime = strtotime($resData);
        $delta = $stopTime - $startTime;
        if (is_numeric($delta)) {
            $responseHeaders .= "Duration: " . $delta;
            if ($delta >= 2) {
                Mage::log($delta . ' seconds duration for ' . $soapName[0], $logLevel);
            }
        }

        $this->_lastResponse = $this->__getLastResponse();
        $identifier = $this->callName . '.' . sha1(rand(0, 1000) . time());
        $this->getLogger()->setLogLevel(Zend_Log::WARN);
        $this->getLogger()->logTransfer(
            sprintf("Request Headers:\n%s\n\n", $requestHeaders),
            'services' . DS . $this->getNamespace(),
            $identifier,
            'log'
        );
        $this->getLogger()->setLogLevel(Zend_Log::INFO);
        $this->getLogger()->logTransfer(
            sprintf("Request Body:\n%s\n\n", $requestBody),
            'services' . DS . $this->getNamespace(),
            $identifier,
            'log'
        );
        $this->getLogger()->setLogLevel(Zend_Log::WARN);
        $this->getLogger()->logTransfer(
            sprintf("\nResponse Headers:\n%s\n\n", $responseHeaders),
            'services' . DS . $this->getNamespace(),
            $identifier,
            'log'
        );
        $this->getLogger()->setLogLevel(Zend_Log::INFO);
        $this->getLogger()->logTransfer(
            sprintf("Response Body:\n%s\n\n", $responseBody),
            'services' . DS . $this->getNamespace(),
            $identifier,
            'log'
        );
        $this->getLogger()->setLogLevel($logLevel);
    }

    /**
     * Method to check if the stubs are in XML format.
     * By default, they are in JSON
     * @return bool
     */
    protected function hasXmlStubs()
    {
        return $this->xmlStubs;
    }
}
