<?php
/**
 * MAP "WSDL Cache Options"
 */
class Dyna_Service_Model_WsdlCacheOptions
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'WSDL_CACHE_NONE',
                'label' => 'WSDL_CACHE_NONE'
            ),
            array(
                'value' => 'WSDL_CACHE_DISK',
                'label' => 'WSDL_CACHE_DISK'
            ),
            array(
                'value' => 'WSDL_CACHE_MEMORY',
                'label' => 'WSDL_CACHE_MEMORY'
            ),
            array(
                'value' => 'WSDL_CACHE_BOTH',
                'label' => 'WSDL_CACHE_BOTH'
            )
        );
    }
}
