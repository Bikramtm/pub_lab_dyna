<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class Queue
 */
class Dyna_Service_Model_Queue
{
    /**
     * @var string
     */
    protected $host;
    /**
     * @var string
     */
    protected $port;
    /**
     * @var string
     */
    protected $user;
    /**
     * @var string
     */
    protected $pass;
    /**
     * @var string
     */
    protected $vhost;
    /**
     * @var string
     */
    protected $exchange;
    /**
     * @var string AMQP endpoint
     */
    protected $queue;
    /**
     * @var string Routing key
     */
    protected $routing;

    /**
     * Dyna_Service_Model_Queue constructor.
     */
    public function __construct()
    {
        $this->host = $this->getConfig('workitem_host');
        $this->port = $this->getConfig('workitem_port');
        $this->user = $this->getConfig('workitem_user');
        $this->pass = $this->getConfig('workitem_password');
        $this->vhost = $this->getConfig('workitem_vhost');
        $this->exchange = $this->getConfig('workitem_exchange');
        $this->routing = $this->getConfig('workitem_routingkey');
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    protected function getConfig($key)
    {
        return Mage::getStoreConfig('omnius_service/workitemqueueoptions/' . $key);
    }

    /**
     * @param string $request
     */
    public function pushWorkItem($request)
    {
        $connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->pass, $this->vhost);
        $channel = $connection->channel();
        $messageBody = $request;
        $message = new AMQPMessage($messageBody, array('content_type' => 'text/xml', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_NON_PERSISTENT));
        $channel->basic_publish($message, $this->exchange, $this->routing);
        $channel->close();
        $connection->close();
    }
}
