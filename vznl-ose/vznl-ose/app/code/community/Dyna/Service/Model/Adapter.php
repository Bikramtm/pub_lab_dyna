<?php
/**
 * MAP "WSDL Cache Options"
 */
class Dyna_Service_Model_Adapter
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'Default',
                'label' => 'Default'
            ),
            array(
                'value' => 'OpenID Connect',
                'label' => 'OpenID Connect'
            )
        );
    }
}
