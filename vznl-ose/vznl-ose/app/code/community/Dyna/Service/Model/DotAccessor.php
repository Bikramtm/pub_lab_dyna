<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Service_Model_DotAccessor
 */
class Dyna_Service_Model_DotAccessor
{
    const SEPARATOR = '.';

    /**
     * @param $data
     * @param $string
     * @param null $default
     * @return mixed|null
     */
    public function getValue(&$data, $string, $default = null)
    {
        $result = $this->iterateNodeGet($data, $string);

        return ((null === $result) || (is_array($result) && empty($result))) ? $default : $result;
    }

    /**
     * @param $data
     * @param $string
     * @return mixed
     * @throws \LogicException
     */
    private function iterateNodeGet(&$data, $string)
    {
        $paths = explode(self::SEPARATOR, $string);
        $node =& $data;
        foreach ($paths as $path) {
            if (@array_key_exists($path, $node)) {
                $node =& $node[$path];
            } else {
                return null;
            }
        }

        return $node;
    }

    /**
     * @param $data
     * @param $string
     * @param $val
     */
    public function setValue(&$data, $string, $val)
    {
        $node =& $this->iterateNodeSet($data, $string);
        $node = $val;
    }

    /**
     * @param $data
     * @param $string
     * @return null
     */
    private function &iterateNodeSet(&$data, $string)
    {
        $paths = explode(self::SEPARATOR, $string);
        $pathCount = count($paths);
        $currentIteration = 0;
        $node =& $data;
        foreach ($paths as $path) {
            if (@array_key_exists($path, $node)) {
                $node =& $node[$path];
                $currentIteration++;
            } elseif ($currentIteration < $pathCount) {
                $node[$path] = array();
                $node =& $node[$path];
                $currentIteration++;
            } else {
                return null;
            }
        }

        return $node;
    }
}
