<?php

class Dyna_Service_Model_MultipleStubClient extends Omnius_Service_Model_Client_StubClient
{
    const DEFAULT_STUB_FILE = "result.json";
    const DEFAULT_STUB_XML_FILE = "result.xml";
    const DEFAULT_STUB_DIR = "default";
    protected $xmlStubs = false;
    protected $_rawStubs = array();
    protected $_requestHash = null;

    /** @var Dyna_Service_Model_Normalizer $_normalizer */
    protected $_normalizer = null;

    public function __construct()
    {
        $this->xmlStubs = false;
    }

    public function setRequestHash(string $hash)
    {
        $this->_requestHash = $hash;

        return $this;
    }

    public function setXmlStubs($bool)
    {
        $this->xmlStubs = $bool;

        return $this;
    }
    /**
     * @param $method
     * @param $arguments
     * @return mixed
     */
    protected function getStubbedResponse($method, $arguments)
    {
        $customStub = !empty($arguments[1]) ? $arguments[1] : [];
        $arguments = array_shift($arguments);

        /** @var Omnius_Service_Model_DotAccessor $accessor */
        $accessor = Mage::getSingleton('omnius_service/dotAccessor');
        if (!$this->_gathered) {
            $this->gatherStubs();
        }

        if ('ExecuteRequestWithLogin' == $method && isset($arguments['functionalityName'])) {
            $method = $arguments['functionalityName'];
        }

        if ($stubbedResponse = $accessor->getValue($this->_stubs, $method)) {
            $rawXmlResponse = $accessor->getValue($this->_rawStubs, $method);
            if ($customStub) {

                foreach ($stubbedResponse as $key => $value) {
                    if (strpos($key, $customStub) === false) {
                        continue;
                    }

                    if ($this->xmlStubs) {

                        $response = $this->getNormalizer()
                            ->normalizeKeys(
                                $this->getNormalizer()
                                    ->objectToArray($value)
                            );
                    } else {
                        $response = $stubbedResponse[$key];
                    }

                    if ($rawXmlResponse) {
                        foreach ($rawXmlResponse as $stubbedNamespace => $rawString) {
                            if (strpos($stubbedNamespace, $customStub) !== false) {
                                $this->setSessionResponse($method, $rawString, $this->_requestHash);
                                break;
                            }
                        }
                    }

                    return $response;
                }
            }

            if (isset($stubbedResponse[self::DEFAULT_STUB_FILE])) {
                if (isset($rawXmlResponse[self::DEFAULT_STUB_FILE])) {
                    $this->setSessionResponse($method, $rawXmlResponse[self::DEFAULT_STUB_FILE], $this->_requestHash);
                }
                return $stubbedResponse[self::DEFAULT_STUB_FILE];
            }

            if ($this->xmlStubs) {
                $response = $this->getNormalizer()
                    ->normalizeKeys(
                        $this->getNormalizer()
                            ->objectToArray(current($stubbedResponse))
                    );
            } else {
                $response = array_values($stubbedResponse)[0];
            }

            $rawXmlResponse = !empty($rawXmlResponse) ? $rawXmlResponse : array();
            if (current($rawXmlResponse)) {
                $this->setSessionResponse($method, current($rawXmlResponse), $this->_requestHash);
            }

            return $response;
        }

        Mage::throwException(sprintf(
            'No stub found for the "%s" request. Please add one in %s directory or set the application to not use stubs.',
            $method,
            Mage::getModuleDir('etc', $this->getNamespace()) . DS . 'stubs' . DS . $method
        ));
    }

    /**
     * @param string $requestName
     * @param string|null $stubResponse If null, won't be set on session
     */
    protected function setSessionResponse(string $requestName, string $stubResponse, $requestHash)
    {
        if ($stubResponse) {
            Mage::getSingleton('customer/session')->addServiceResponse($requestName, $stubResponse, $requestHash);
        }

        return $this;
    }

    /**
     *
     */
    protected function gatherStubs()
    {
        $namespace = $this->getNamespace();

        $path = $this->getStubsDirPath('default', $namespace);

        $defaultStubsIterator = new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS);
        $defaultStubsIterator = new RecursiveIteratorIterator($defaultStubsIterator, RecursiveIteratorIterator::SELF_FIRST);

        /** @var SplFileInfo $file */
        foreach ($defaultStubsIterator as $file) {
            $this->verifyAndSaveStub($file, $path);
        }

        // If a custom stubs group exists, replace the default stubs with the available custom stubs. See OMNVFDE-3276
        if (!empty($this->getCustomStubsGroupName()) && false !== $this->getStubsDirPath('custom', $namespace)) {
            $path = $this->getStubsDirPath('custom', $namespace);
            $customStubsIterator = new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS);
            $customStubsIterator = new RecursiveIteratorIterator($customStubsIterator, RecursiveIteratorIterator::SELF_FIRST);

            foreach ($customStubsIterator as $file) {
                $this->verifyAndSaveStub($file, $path);
            }
        }

        $this->_gathered = true;
    }

    /**
     * Gets stubs directory path
     *
     * @param string $type
     * @param bool $namespace
     * @return bool|string
     * @throws Exception
     */
    public function getStubsDirPath($type = 'default', $namespace = false)
    {
        /** @var array $validPaths */
        $validNamespaces = array();

        /** @var Dyna_Service_Helper_Request $serviceHelper */
        $serviceHelper = Mage::helper('dyna_service/request');
        $serviceHelper->getDependencyNamespaces($namespace, $validNamespaces);

        foreach ($validNamespaces as $validNamespace) {
            $namespace = $validNamespace !== false ? DS . implode(DS, explode('_', $validNamespace)) : '';
            $stubGroup = ('custom' === $type) ? $this->getCustomStubsGroupName() : self::DEFAULT_STUB_DIR;
            $stubsDir = !empty($stubGroup) ? Mage::getBaseDir('var') . DS . 'stubs' . DS . $stubGroup : Mage::getBaseDir('var') . DS . 'stubs';
            $fullDir = $stubsDir . $namespace;

            if (is_dir($fullDir)) {
                return $fullDir;
            }
        }

        if (!is_dir($stubsDir)) {
            //mkdir($stubsDir, '0777', true);
            throw new Exception(sprintf('Directory "%s" does not exist.', $stubsDir));
        }

        return false;
    }


    /**
     * Check custom stub group
     * @return string
     */
    public function getCustomStubsGroupName()
    {
        /** @var Mage_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');
        return $session->getData(Dyna_AgentDE_Model_Observer::CUSTOM_STUBS_NAMESPACE_PARAM);
    }

    /**
     * Verifies if $file is not a directory and saves content as stub
     *
     * @param $file
     * @param $path
     */
    public function verifyAndSaveStub($file, $path)
    {
        if (preg_match('/^\_/', $file->getFilename())) {
            //skip files starting with underscore
            return;
        }

        $subPath = trim(str_replace($path, '', $file->getPathName()), DS);
        $parts = explode(DS, $subPath);
        $method = $parts[0];
        $methodPath = $path . DS . $method;
        if (is_dir($methodPath) && $file->isFile()) {
            $ext = pathinfo($file->getRealPath(), PATHINFO_EXTENSION);

            switch ($ext) {
                case "json":
                    if (!$this->xmlStubs) {
                        // default stubs are in json format
                        $stubContent = file_get_contents($file->getRealPath());
                        $stub = @json_decode($stubContent, true);
                        if (JSON_ERROR_NONE === json_last_error()) {
                            $this->_stubs[$method][$file->getFilename()] = $stub;
                        }
                    }
                    break;
                case "xml":
                    if ($this->xmlStubs) {
                        $stubContent = file_get_contents($file->getRealPath());
                        $this->_stubs[$method][$file->getFilename()] = $this->parseXmlResponse($stubContent);
                        $this->_rawStubs[$method][$file->getFilename()] = $stubContent;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @return Omnius_Service_Model_Normalizer
     */
    protected function getNormalizer()
    {
        if (!$this->_normalizer) {
            $this->_normalizer = Mage::getSingleton('dyna_service/normalizer');
        }

        return $this->_normalizer;
    }

    /**
     * @param $str
     * @return SimpleXMLElement
     * @throws Exception
     */
    protected function parseXmlResponse($str)
    {
        try {
            $xml = new SimpleXMLElement($str);
            foreach ($xml->getNamespaces(true) as $ns => $uri) {
                $str = str_replace(sprintf('%s:', $ns), '', $str);
            }
            $xml = new SimpleXMLElement($str);

            return json_decode(json_encode($xml));
        } catch (Exception $e) {
            $e->logSoapException = $this->logSoapException;

            throw $e;
        }
    }
}
