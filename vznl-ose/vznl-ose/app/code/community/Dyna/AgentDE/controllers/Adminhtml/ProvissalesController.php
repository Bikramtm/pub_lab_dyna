<?php
/**
 * Class Dyna_AgentDE_Adminhtml_ProvisSalesController
 */
class Dyna_AgentDE_Adminhtml_ProvissalesController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Provis sales"));

        $this->_initAction();
        $this->renderLayout();
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu("agent/provissales")
            ->_addBreadcrumb(Mage::helper("adminhtml")
                ->__("Provis sales"), Mage::helper("adminhtml")->__("Provis sales"));
        return $this;
    }

    public function editAction()
    {
        $this->_title($this->__("AgentDE"));
        $this->_title($this->__("Provis sale"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agentde/provisSales")->load($id);
        if ($model->getId()) {
            Mage::register("provissales_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("agent/provissales");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Provis Sale Manager"), Mage::helper("adminhtml")->__("Provis Sale Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Provis Sale Description"), Mage::helper("adminhtml")->__("Provis Sale Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("agentde/adminhtml_provissales_edit"))->_addLeft($this->getLayout()->createBlock("agentde/adminhtml_provissales_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("agentde")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data) {
            try {
                $model = Mage::getModel("agentde/provisSales")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Provis sale was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setProvissalesData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setProvissalesData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $id = $this->getRequest()->getParam("id");
                $model = Mage::getModel("agentde/provisSales");
                $model->setId($id)->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('entity_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("agentde/provisSales");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    //Patch for SUPEE-6285

    protected function _isAllowed()
    {
        return true;
    }
}
