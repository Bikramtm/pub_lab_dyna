<?php
/**
 * Class Dyna_AgentDE_Block_Adminhtml_Phonebookindustries_Edit
 */
class Dyna_AgentDE_Block_Adminhtml_Phonebookindustries_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = "id";
        $this->_blockGroup = "agentde";
        $this->_controller = "adminhtml_phonebookindustries";
        $this->_updateButton("save", "label", Mage::helper("agent")->__("Save Phone Book Industry"));
        $this->_updateButton("delete", "label", Mage::helper("agent")->__("Delete Phone Book Industry"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("agentde")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    public function getHeaderText()
    {
        if (Mage::registry("phonebookindustries_data") && Mage::registry("phonebookindustries_data")->getId()) {
            return Mage::helper("agentde")->__("Edit Phone Book Industry '%s'",
                $this->htmlEscape(Mage::registry("phonebookindustries_data")->getId()));
        } else {
            return Mage::helper("agentde")->__("Add Phone Book Industry");
        }
    }
}