<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Vodafoneship2stores_Edit_Tab_Form
 */
class Dyna_AgentDE_Block_Adminhtml_Vodafoneship2stores_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Vodafone ship to stores Information")));


        $fields = [
            array(
                "name" => "shop_name1",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Shop Name1"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "shop_name1",
                )
            ),
            array(
                "name" => "shop_name2",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Shop Name2"),
//                    "class" => "required-entry",
                    "required" => false,
                    "name" => "shop_name2",
                )
            ),
            array(
                "name" => "street",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Street"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "street",
                )
            ),
            array(
                "name" => "house_no",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("House number"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "house_no",
                )
            ),
            array(
                "name" => "postcode",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Postal code"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "postcode",
                )
            ),
            array(
                "name" => "city",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("City"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "city",
                )
            ),
        ];

        foreach ($fields as $field) {
            $fieldset->addField($field["name"], $field["type"], $field["parameters"]);
        }

        if (Mage::getSingleton("adminhtml/session")->getVodafoneship2storesData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getDealergroupData());
            Mage::getSingleton("adminhtml/session")->setVodafoneship2storesData(null);
        } elseif (Mage::registry("vodafoneship2stores_data")) {
            $form->setValues(Mage::registry("vodafoneship2stores_data")->getData());
        }

        return parent::_prepareForm();
    }
}
