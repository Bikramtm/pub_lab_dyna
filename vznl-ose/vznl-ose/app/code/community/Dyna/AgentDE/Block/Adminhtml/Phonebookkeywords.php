<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Phonebookkeywords
 */
class Dyna_AgentDE_Block_Adminhtml_Phonebookkeywords extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_phonebookkeywords";
        $this->_blockGroup = "agentde";
        $this->_headerText = Mage::helper("agentde")->__("Phone Book Keywords");

        parent::__construct();
        $this->_addButton('import', array(
            'label'     => Mage::helper('agent')->__('Import Phone Book Keywords'),
            'onclick'   => 'javascript:new Popup(\'' . $this->getImportUrl() . '\', {title:\'' . Mage::helper('agent')->__('Import Phone Book Keywords') . '\', width: 600, height:220})',
            'class'     => 'go',
        ));
    }

    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }
}