<?php
/**
 * Class Dyna_AgentDE_Block_Adminhtml_Provissales_Edit
 */
class Dyna_AgentDE_Block_Adminhtml_Provissales_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = "id";
        $this->_blockGroup = "agentde";
        $this->_controller = "adminhtml_provissales";
        $this->_updateButton("save", "label", Mage::helper("agent")->__("Save Provis Sale"));
        $this->_updateButton("delete", "label", Mage::helper("agent")->__("Delete Provis Sale"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("agentde")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    public function getHeaderText()
    {
        if (Mage::registry("provissales_data") && Mage::registry("provissales_data")->getId()) {
            return Mage::helper("agentde")->__("Edit Provis sale '%s'",
                $this->htmlEscape(Mage::registry("provissales_data")->getId()));
        } else {
            return Mage::helper("agentde")->__("Add Provis Sale");
        }
    }
}