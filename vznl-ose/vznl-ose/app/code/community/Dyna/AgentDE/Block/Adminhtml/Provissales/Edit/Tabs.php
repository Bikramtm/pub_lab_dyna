<?php
/**
 * Class Dyna_AgentDE_Block_Adminhtml_Provissales_Edit_Tabs
 */
class Dyna_AgentDE_Block_Adminhtml_Provissales_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();

        $this->setId("provissales_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("agentde")->__("Provis Sales Information"));
    }

    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("agentde")->__("Provis Sales Information"),
            "title" => Mage::helper("agentde")->__("Provis Sales Information"),
            "content" => $this->getLayout()->createBlock("agentde/adminhtml_provissales_edit_tab_form")->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
