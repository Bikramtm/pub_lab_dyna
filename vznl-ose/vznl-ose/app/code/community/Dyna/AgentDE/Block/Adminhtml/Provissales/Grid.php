<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Provissales_Grid
 */
class Dyna_AgentDE_Block_Adminhtml_Provissales_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("provissalesGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("agentde/provisSales")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $columns = [
            "red_sales_id" => array(
                "header" => Mage::helper("agentde")->__("Red sales id"),
                "index" => "red_sales_id",
            ),
            "status" => array(
                "header" => Mage::helper("agentde")->__("Status"),
                "index" => "status",
            ),
            "level" => array(
                "header" => Mage::helper("agentde")->__("Level"),
                "index" => "level",
            ),
            "sales_channel" => array(
                "header" => Mage::helper("agentde")->__("Sales channel"),
                "index" => "sales_channel",
            ),
            "sales_subchannel" => array(
                "header" => Mage::helper("agentde")->__("Sales subchannel"),
                "index" => "sales_subchannel",
            ),
            "classification" => array(
                "header" => Mage::helper("agentde")->__("Classification"),
                "index" => "classification",
            ),
            "red_sales_id_level-0" => array(
                "header" => Mage::helper("agentde")->__("Red sales id level 0"),
                "index" => "red_sales_id_level-0",
            ),
            "red_sales_id_level-1" => array(
                "header" => Mage::helper("agentde")->__("Red sales id level 1"),
                "index" => "red_sales_id_level-1",
            ),
            "red_sales_id_level-2" => array(
                "header" => Mage::helper("agentde")->__("Red sales id level 2"),
                "index" => "red_sales_id_level-2",
            ),
            "blue_sales_id" => array(
                "header" => Mage::helper("agentde")->__("Blue sales id"),
                "index" => "blue_sales_id",
            ),
            "yellow_sales_id" => array(
                "header" => Mage::helper("agentde")->__("Yellow sales id"),
                "index" => "yellow_sales_id",
            ),
            "blue_top_vo" => array(
                "header" => Mage::helper("agentde")->__("Blue top vo"),
                "index" => "blue_top_vo",
            )
        ];


        foreach ($columns as $columnKey => $columnValue) {
            $this->addColumn($columnKey, $columnValue);
        }

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_provissale', array(
            'label' => Mage::helper('agentde')->__('Remove Provis Sale(s)'),
            'url' => $this->getUrl('*/adminhtml_provissales/massRemove'),
            'confirm' => Mage::helper('agent')->__('Are you sure?')
        ));
        return $this;
    }
}