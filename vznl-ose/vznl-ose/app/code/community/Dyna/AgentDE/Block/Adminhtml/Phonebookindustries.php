<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Phonebookindustries
 */
class Dyna_AgentDE_Block_Adminhtml_Phonebookindustries extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_phonebookindustries";
        $this->_blockGroup = "agentde";
        $this->_headerText = Mage::helper("agentde")->__("Phone Book Industries");

        parent::__construct();
        $this->_addButton('import', array(
            'label'     => Mage::helper('agent')->__('Import Phone Book Industries'),
            'onclick'   => 'javascript:new Popup(\'' . $this->getImportUrl() . '\', {title:\'' . Mage::helper('agent')->__('Import Phone Book Industries') . '\', width: 600, height:220})',
            'class'     => 'go',
        ));
    }

    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }
}