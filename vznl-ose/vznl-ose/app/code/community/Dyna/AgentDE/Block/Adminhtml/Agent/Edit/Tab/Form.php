<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Agent_Edit_Tab_Form
 * rewrites Dyna_Agent_Block_Adminhtml_Agent_Edit_Tab_Form to include exclude dealer_code
 */
class Dyna_AgentDE_Block_Adminhtml_Agent_Edit_Tab_Form extends Dyna_Agent_Block_Adminhtml_Agent_Edit_Tab_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Agent Information")));
        $data = Mage::registry("agent_data")->getData();

        $yesno = array(
            '0' => Mage::helper('catalog')->__('No'),
            '1' => Mage::helper('catalog')->__('Yes')
        );

        $fieldset->addField("usertype", "text", array(
            "label" => Mage::helper("agent")->__("User Type"),
            "class" => "required-entry",
            "required" => true,
            "name" => "usertype",
        ));

        $fieldset->addField("username", "text", array(
            "label" => Mage::helper("agent")->__("Username"),
            "class" => "required-entry",
            "required" => true,
            "name" => "username",
        ));

        if (isset($data['agent_id']) && isset($data['password'])) {
            $fieldset->addField('password', 'hidden', array(
                'label'     => Mage::helper('agent')->__('Password'),
                'after_element_html' => '<tr><td class="label"><label for="password">'. Mage::helper('agent')->__('Password') .'</label></td>
                <td class="value">******<input type="password" style="display: none" /></td></tr>',
            ));

            $fieldset->addField("change_password", "obscure", array(
                "label" => Mage::helper("agent")->__("Change password"),
                "required" => false,
                "name" => "change_password",
            ));
        } else {
            $fieldset->addField("password", "obscure", array(
                "label" => Mage::helper("agent")->__("Password"),
                "class" => "required-entry",
                "required" => true,
                "name" => "password",
            ));
        }

        $fieldset->addField("first_name", "text", array(
            "label" => Mage::helper("agent")->__("First Name"),
            "class" => "validate-emailSender",
            "required" => true,
            "name" => "first_name",
        ));

        $fieldset->addField("last_name", "text", array(
            "label" => Mage::helper("agent")->__("Last Name"),
            "class" => "validate-emailSender",
            "required" => true,
            "name" => "last_name",
        ));

        $fieldset->addField("red_sales_id", "text", array(
            "label" => Mage::helper("agent")->__("RED Sales ID"),
            "required" => false,
            "name" => "red_sales_id",
        ));

        $fieldset->addField("ngumid", "text", array(
            "label" => Mage::helper("agent")->__("Ngum ID"),
            "required" => true,
            "name" => "ngumid",
        ));

        $fieldset->addField("phone", "text", array(
            "label" => Mage::helper("agent")->__("Phone"),
            "required" => true,
            "name" => "phone",
        ));

        $fieldset->addField("email", "text", array(
            "label" => Mage::helper("agent")->__("Email"),
            "class" => "validate-email",
            "required" => true,
            "name" => "email",
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('agent')->__('Is Active'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'is_active',
            'onclick' => "",
            'onchange' => "",
            'selected'  => '1',
            'values'    => $yesno
        ));

        $fieldset->addField('everlasting_pass', 'select', array(
            'label'     => Mage::helper('agent')->__('Has everlasting password'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'everlasting_pass',
            'onclick' => "",
            'onchange' => "",
            'selected'  => '0',
            'values'    => $yesno
        ));

        $fieldset->addField('role_id', 'select', array(
            'label' => Mage::helper('agent')->__('Role ID'),
            'values' => Dyna_Agent_Block_Adminhtml_Agent_Grid::getValueArray6(),
            'name' => 'role_id',
            "class" => "required-entry",
            "required" => true,
        ));

        $fieldset->addField('dealer_id', 'select', array(
            'label' => Mage::helper('agent')->__('Default Dealer ID'),
            'values' => Dyna_Agent_Block_Adminhtml_Agent_Grid::getValueArray8(),
            'name' => 'dealer_id',
            "required" => true,
        ));

        $fieldset->addField('store_id', 'select', array(
            'label' => Mage::helper('agent')->__('Store'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
            'name' => 'store_id',
            "class" => "required-entry",
            "required" => true,
        ));

        $fieldset->addField("employee_number", "text", array(
            "label" => Mage::helper("agent")->__("Employee number"),
            "class" => "required-entry",
            "required" => true,
            "name" => "employee_number",
        ));

        $fieldset->addField("partnermanager", "text", array(
            "label" => Mage::helper("agent")->__("Partner Manager"),
            "required" => false,
            "name" => "partnermanager",
        ));

        $fieldset->addField("reportingto", "text", array(
            "label" => Mage::helper("agent")->__("Reporting to"),
            "required" => false,
            "name" => "reportingto",
        ));

        $fieldset->addField("orgunitcode", "text", array(
            "label" => Mage::helper("agent")->__("Org Unit code"),
            "required" => false,
            "name" => "orgunitcode",
        ));

        $fieldset->addField("orgunitlong", "text", array(
            "label" => Mage::helper("agent")->__("Org Unit Long"),
            "required" => false,
            "name" => "orgunitlong",
        ));

        $fieldset->addField("location", "text", array(
            "label" => Mage::helper("agent")->__("Location"),
            "required" => false,
            "name" => "location",
        ));

        $fieldset->addField("workplace", "text", array(
            "label" => Mage::helper("agent")->__("Workplace"),
            "required" => false,
            "name" => "workplace",
        ));

        $fieldset->addField('reset_attempts', 'checkbox', array(
            'label'     =>Mage::helper('agent')->__('Reset login attempts'),
            'name'      => 'reset_attempts',
            'value'     => '1',
            'after_element_html' => '<small>Current attempts: '. (isset($data['login_attempts']) && !empty($data['login_attempts']) ? $data['login_attempts'] : 0). '</small>'
        ));

        if (Mage::getSingleton("adminhtml/session")->getAgentData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getAgentData());
            Mage::getSingleton("adminhtml/session")->setAgentData(null);
        } elseif (Mage::registry("agent_data")) {
            $form->setValues(Mage::registry("agent_data")->getData());
        }

        return Mage_Adminhtml_Block_Widget_Form::_prepareForm();
    }
}
