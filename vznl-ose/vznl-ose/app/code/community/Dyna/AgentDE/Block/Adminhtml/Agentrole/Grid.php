<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Dealer_Grid
 */
class Dyna_AgentDE_Block_Adminhtml_Agentrole_Grid extends Dyna_Agent_Block_Adminhtml_Agentrole_Grid
{
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('role_id');
        $this->getMassactionBlock()->setFormFieldName('role_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_agentrole', array(
            'label' => Mage::helper('agent')->__('Remove Agent Role'),
            'url' => $this->getUrl('*/adminhtml_agentrole/massRemove'),
            'confirm' => Mage::helper('agent')->__('Are you sure you want to remove this agent role?')
        ));
        return $this;
    }
}
