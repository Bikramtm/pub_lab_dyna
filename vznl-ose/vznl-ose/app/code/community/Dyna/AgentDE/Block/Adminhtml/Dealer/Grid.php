<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Dealer_Grid
 */
class Dyna_AgentDE_Block_Adminhtml_Dealer_Grid extends Dyna_Agent_Block_Adminhtml_Dealer_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("dealerGrid");
        $this->setDefaultSort("dealer_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareColumns()
    {
        $this->addColumn("dealer_id", array(
            "header" => Mage::helper("agent")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "dealer_id",
        ));
        $this->addColumn("vf_dealer_code", array(
            "header" => Mage::helper("agent")->__("VF Dealer Code"),
            "index" => "vf_dealer_code",
        ));
        $this->addColumn("street", array(
            "header" => Mage::helper("agent")->__("Street"),
            "index" => "street",
        ));
        $this->addColumn("postcode", array(
            "header" => Mage::helper("agent")->__("Postal Code"),
            "index" => "postcode",
        ));
        $this->addColumn("house_nr", array(
            "header" => Mage::helper("agent")->__("House Nr."),
            "index" => "house_nr",
        ));
        $this->addColumn("city", array(
            "header" => Mage::helper("agent")->__("City"),
            "index" => "city",
        ));
        $this->addColumn("telephone", array(
            "header" => Mage::helper("agent")->__("Telephone"),
            "index" => "telephone",
        ));

        $this->addColumn("hotline_number", array(
            "header" => Mage::helper("agent")->__("Hotline number"),
            "index" => "hotline_number",
        ));

        $this->addColumn("axi_store_code", array(
            "header" => Mage::helper("agent")->__("AXI store code"),
            "index" => "axi_store_code",
        ));

        $this->addColumn("group_id", array(
            "header" => Mage::helper("agent")->__("Group ID"),
            "index" => "group_id",
            'frame_callback' => array($this, 'getGroupIds'),
            'filter_condition_callback' => array($this, 'filterGroupIds')
        ));

        $this->addColumn("paid_code", array(
            "header" => Mage::helper("agent")->__("Paid code"),
            "index" => "paid_code",
        ));

	$this->addColumn('store_id', array(
            'header' => Mage::helper('agent')->__('Store'),
            'index' => 'store_id',
            'type' => 'options',
            'options' => Mage::getSingleton('adminhtml/system_store')->getStoreOptionHash(false),
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return $this;
    }
}
