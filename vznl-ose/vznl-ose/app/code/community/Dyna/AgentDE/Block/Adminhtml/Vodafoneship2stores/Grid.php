<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Vodafoneship2stores_Grid
 */
class Dyna_AgentDE_Block_Adminhtml_Vodafoneship2stores_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("vodafoneship2storesGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("agentde/vodafoneShip2Stores")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $columns = [
            "shop_name1" => array(
                "header" => Mage::helper("agentde")->__("Shop Name1"),
                "index" => "shop_name1",
            ),
            "shop_name2" => array(
                "header" => Mage::helper("agentde")->__("Shop Name2"),
                "index" => "shop_name2",
            ),
            "shop_name3" => array(
                "header" => Mage::helper("agentde")->__("Shop Name3"),
                "index" => "shop_name3",
            ),
            "street" => array(
                "header" => Mage::helper("agent")->__("Street"),
                "index" => "street",
            ),
            "house_no" => array(
                "header" => Mage::helper("agentde")->__("House Number"),
                "index" => "house_no",
            ),
            "postcode" => array(
                "header" => Mage::helper("agentde")->__("Postal Code"),
                "index" => "postcode",
            ),
            "city" => array(
                "header" => Mage::helper("agentde")->__("City"),
                "index" => "city",
            )
        ];


        foreach ($columns as $columnKey => $columnValue) {
            $this->addColumn($columnKey, $columnValue);
        }

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_vodafoneship2store2', array(
            'label' => Mage::helper('agentde')->__('Remove Vodafone Ship To Store(s)'),
            'url' => $this->getUrl('*/adminhtml_vodafoneship2stores/massRemove'),
            'confirm' => Mage::helper('agent')->__('Are you sure?')
        ));
        return $this;
    }
}