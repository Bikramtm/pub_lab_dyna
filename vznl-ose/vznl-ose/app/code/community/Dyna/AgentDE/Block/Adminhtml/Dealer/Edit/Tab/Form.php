<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Dealer_Edit_Tab_Form
 * rewrites Dyna_Agent_Block_Adminhtml_Dealer_Edit_Tab_Form to include dealer campaigns
 */
class Dyna_AgentDE_Block_Adminhtml_Dealer_Edit_Tab_Form extends Dyna_Agent_Block_Adminhtml_Dealer_Edit_Tab_Form
{
    /**
     * Data regarding allowed campaigns is set on registry in agentde helper through mapCampaignData() method
     * and saved in Dyna_AgentDE_Model_Dealer which extends Dyna_Agent_Dealer model
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Dealer Information")));


        $fields = [
            array(
                "name" => "vf_dealer_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("VF Dealer Code"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "vf_dealer_code",
                )
            ),
            array(
                "name" => "street",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Street"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "street",
                )
            ),
            array(
                "name" => "postcode",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Postal Code"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "postcode",
                )
            ),
            array(
                "name" => "house_nr",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("House Nr."),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "house_nr",
                )
            ),
            array(
                "name" => "city",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("City"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "city",
                )
            ),
            array(
                "name" => "telephone",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Telephone"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "telephone",
                )
            ),
            array(
                "name" => "store_id",
                "type" => "select",
                "parameters" => array(
                    'label' => Mage::helper('agent')->__('Store'),
                    'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
                    'name' => 'store_id',
                    "class" => "required-entry",
                    "required" => true,
                )
            ),
            array(
                "name" => "hotline_number",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Hotline number"),
                    "name" => "hotline_number",
                    "class" => "validation_hotline_number",
                )
            ),
            array(
                "name" => "axi_store_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("AXI store code"),
                    "name" => "axi_store_code",
                )
            ),
            array(
                "name" => "group_id",
                "type" => "multiselect",
                "parameters" => array(
                    'label' => Mage::helper('agent')->__('Dealer Group ID'),
                    'values' => Dyna_Agent_Block_Adminhtml_Dealer_Grid::getDealerGroups(),
                    'name' => 'group_id',
                    "class" => "required-entry",
                    "required" => true,
                )
            ),
            array(
                "name" => "campaign_id",
                "type" => "multiselect",
                "parameters" => array(
                    'label' => Mage::helper('agentde')->__('Allowed campaigns'),
                    'values' => Mage::helper('agentde')->getAvailableCampaigns(),
                    'name' => 'campaign_id',
                    "class" => "",
                    "required" => false,
                )
            ),
            array(
                "name" => "naw_data_dealer_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("NAW DataDealer Code"),
                    "name" => "naw_data_dealer_code",
                )
            ),
            array(
                "name" => "axi_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("AXI Code"),
                    "name" => "axi_code",
                )
            ),
            array(
                "name" => "paid_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Paid Code"),
                    "name" => "paid_code",
                )
            ),
            array(
                "name" => "bounce_info_email_address",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Bounce Information Email Address"),
                    "name" => "bounce_info_email_address",
                    "class" => "validate-mails validate-email validate-emailSender",
                )
            ),
        ];

        foreach ($fields as $field) {
            $fieldset->addField($field["name"], $field["type"], $field["parameters"]);
        }

        $form->getElement('hotline_number')->setAfterElementHtml("<script>
            Validation.add('validation_hotline_number', 'Number not match to pattern 000/000000000 or +00/000000000', function(val) {
                return val.length == 0 ? true : /(^(\+[0-9]{2}|[0-9]{3})\/([0-9]*)$)/g.test(val);
            });
        </script>");

        Mage::helper('agentde')->mapCampaignData();

        if (Mage::getSingleton("adminhtml/session")->getDealerData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getDealerData());
            Mage::getSingleton("adminhtml/session")->setDealerData(null);
        } elseif (Mage::registry("dealer_data")) {
            $form->setValues(Mage::registry("dealer_data")->getData());
        }

        return Mage_Adminhtml_Block_Widget_Form::_prepareForm();
    }
}
