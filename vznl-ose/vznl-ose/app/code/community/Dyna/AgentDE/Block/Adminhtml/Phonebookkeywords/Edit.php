<?php
/**
 * Class Dyna_AgentDE_Block_Adminhtml_Phonebookkeywords_Edit
 */
class Dyna_AgentDE_Block_Adminhtml_Phonebookkeywords_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = "id";
        $this->_blockGroup = "agentde";
        $this->_controller = "adminhtml_phonebookkeywords";
        $this->_updateButton("save", "label", Mage::helper("agent")->__("Save Phone Book Keyword"));
        $this->_updateButton("delete", "label", Mage::helper("agent")->__("Delete Phone Book Keyword"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("agentde")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    public function getHeaderText()
    {
        if (Mage::registry("phonebookkeywords_data") && Mage::registry("phonebookkeywords_data")->getId()) {
            return Mage::helper("agentde")->__("Edit Phone Book Keyword '%s'",
                $this->htmlEscape(Mage::registry("phonebookkeywords_data")->getId()));
        } else {
            return Mage::helper("agentde")->__("Add Phone Book Keyword");
        }
    }
}