<?php

$installer = $this;

// add permission to SEND OFFER
$permissions = array(
    'SEND_OFFER' => 'Agent is able to send the offer to the customer',
);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissions as $code => $description) {
    $conn->insert('role_permission', array('name' => $code, 'description' => $description));
}

$installer->endSetup();