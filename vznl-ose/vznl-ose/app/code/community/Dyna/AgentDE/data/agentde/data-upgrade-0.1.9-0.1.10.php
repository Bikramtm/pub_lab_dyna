<?php

$installer = $this;

// add permission to SEND OFFER
$permissions = array(
    'CREATE_WORKITEM' => 'Agent is able to create a workitem',
);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissions as $code => $description) {
    $conn->insert('role_permission', array('name' => $code, 'description' => $description));
}

$installer->endSetup();