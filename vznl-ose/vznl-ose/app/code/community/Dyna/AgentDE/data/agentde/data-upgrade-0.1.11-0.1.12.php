<?php

$installer = $this;

// add permission to OPEN ORDERS
$permissions = array(
    'SEARCH_ORDER_OF_ALL' => 'Agent is able to view all orders in the database',
    'SEARCH_ORDER_OF_GROUP' => 'Agent is able to view all orders in the database that have been created by the dealers of the same group',
    'SEARCH_ORDER_OF_DEALER' => 'Agent is able to view all orders in the database that have been created by the current dealer',
    'SEARCH_ORDER_OF_TELESALES' => 'Agent is able to view all orders in the database that have been created in the current store'
);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissions as $code => $description) {
    // Check if permissions already exist
    $sql = "SELECT `name` FROM `role_permission` WHERE `name` LIKE '$code'";
    $result = $conn->fetchAll($sql);

    if (sizeof($result) == 0) {
        $conn->insert('role_permission', array('name' => $code, 'description' => $description));
    }
}

$installer->endSetup();