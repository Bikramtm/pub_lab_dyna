<?php
/**
 * VFDED1W3S-3861
 */
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
//tables
$permissionsTable = 'role_permission';
$permissionsLinkTable = 'role_permission_link';
$agentsTable = 'agent';
$agentRolesTable = 'agent_role';


$permissions = $conn->fetchAll("SELECT * FROM `" . $permissionsTable . "` WHERE  `name` LIKE 'CREATE_WORKITEM'");
if(count($permissions) > 1){
    $remainPermission = $permissions[0];
    unset( $permissions[0]);
    $alreadyUsed = $conn->fetchAll("SELECT `role_id` FROM `role_permission_link` WHERE `permission_id` = ".$remainPermission['entity_id'],  [], Zend_Db::FETCH_COLUMN);

    foreach ($permissions as $permission){
        $SQL = " DELETE FROM `role_permission_link` WHERE `permission_id` = ".$permission['entity_id']. (count($alreadyUsed) > 0 ? " AND role_id IN (".implode(',', $alreadyUsed).")" : "")  .";
                 UPDATE `" . $permissionsLinkTable . "` SET `permission_id` = " . $remainPermission['entity_id'] ." WHERE `permission_id` = " . $permission['entity_id'].";  
                 DELETE FROM `role_permission` WHERE `entity_id` = ". $permission['entity_id'];
        $conn->exec($SQL);
    }
}

$newPositionsRoles = [
    1 => "Default Role",
    4 => "360-View Agent",
    6 => "Telesales Agent",
    7 => "Super Agent",
    8 => "Back-Office Agent",
    9 => "COPS Agent",
    10 => "Admin User Agent"
];

//cache data
$agentsRoles = $conn->fetchAll("SELECT `agent_id`, `role_id` FROM `" . $agentsTable . "` ");
$permissionsLikedToRoles = $conn->fetchAll("SELECT * FROM `" . $permissionsLinkTable . "` ");
$rolesDb = $conn->fetchAll("SELECT * FROM `" . $agentRolesTable . "` ");
$filteredRolesDb = [];
foreach ($rolesDb as $row) {
    $filteredRolesDb[$row['role_id']] = $row['role'];
}
unset($rolesDb, $row);
//check if entity_id already exists on agent_role table
if (!$conn->tableColumnExists($agentRolesTable, 'entity_id')) {
    $sql = "SET FOREIGN_KEY_CHECKS = 0;
        ALTER TABLE `" . $agentsTable . "`
           DROP INDEX `fk_agent_role`,
           DROP FOREIGN KEY `fk_agent_role`; 
        ALTER TABLE `" . $agentRolesTable . "`
          DROP PRIMARY KEY,
          CHANGE `role_id` `role_id` INT(11) UNSIGNED,
          ADD COLUMN `entity_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;
        UPDATE `" . $agentRolesTable . "` SET `role_id` = NULL;
        DELETE FROM `" . $permissionsLinkTable . "`;
       ";
    $conn->exec($sql);
}

$roleRelations = [];
$rolesDb = $conn->fetchAll("SELECT * FROM `" . $agentRolesTable . "`");
foreach ($rolesDb as $row) {
    if (in_array($row['role'], $newPositionsRoles)) {
        $newId = array_search($row['role'], $newPositionsRoles);
        $oldId = array_search($row['role'], $filteredRolesDb);
        $roleRelations[$newId] = $oldId;
        $rolePermissions = $permissionsLikedToRoles;
        $conn->exec("UPDATE `" . $agentRolesTable . "` SET `role_id` = " . $newId . " WHERE role LIKE '" . $row['role'] . "'");
        foreach ($permissionsLikedToRoles as $linked) {
            if ($linked['role_id'] == $oldId) {
                $conn->insert($permissionsLinkTable, ['role_id' => $newId, "permission_id" => $linked['permission_id']]);
            }
        }
    } else {
        //delete roles that were created manually from backend
        $conn->exec("DELETE FROM `" . $agentRolesTable . "` WHERE role LIKE '" . $row['role'] . "'");
    }
}

foreach ($agentsRoles as $agentsRole) {
    if (in_array($agentsRole['role_id'], $roleRelations)) {
        $roleID = array_search($agentsRole['role_id'], $roleRelations);
    } else {
        $roleID = 1; //Default Role
    }

    $conn->update($agentsTable, ['role_id' => $roleID, 'additional_role_ids' => $roleID], ["agent_id =?" => $agentsRole['agent_id']]);
}

$conn->exec("ALTER TABLE `" . $agentRolesTable . "` 
                CHANGE `role_id` `role_id` INT(11) UNSIGNED UNIQUE NOT NULL;
             CREATE INDEX `fk_agent_role` ON `" . $agentsTable . "` (`role_id`);
             ALTER TABLE `" . $agentsTable . "` ADD FOREIGN KEY (`role_id`) REFERENCES `" . $rolesDb . "`(`role_id`);
             SET FOREIGN_KEY_CHECKS = 1;");
