<?php

$installer = $this;

// add permission to OPEN ORDERS
$permissions = array(
    'VOICELOG_REQUIRED' => array(
        'description' => 'The permission controls for which agent the voicelog shall be a required part of the checkout section. Agents without this permission do not have to consider the voicelog questions when submitting an order.',
        'roles' => array(
            'Telesales Agent'
        )
    ),
    'CHANGE_TO_MOB_BASE' => array(
        'description' => 'The permission controls for which agent it shall be allowed to trigger MOB Change scenarios (i.e. Prolongation, Tariff change and add/remove options). If the permission is not granted, the "VVL" & "Change" Buttons in 360 will be disabled.',
        'roles' => array(
            'Telesales Agent',
            'Super Agent',
            'Backoffice Agent',
            'Retail Agent',
            '360 only'
        )
    ),
    'CHANGE_TO_CAB_BASE' => array(
        'description' => 'The permission controls, for which user it is allowed to trigger CAB Change scenarios (i.e. Tariff change and add/remove options). If the permission is not granted, the "Change" Buttons in 360 are disabled.',
        'roles' => array(
            'Telesales Agent',
            'Super Agent',
            'Backoffice Agent',
            'Retail Agent',
            '360 only'
        )
    ),
    'CHANGE_TO_DSL_BASE' => array(
        'description' => 'The permission controls, for which agent it is allowed to trigger DSL Change scenarios (i.e. Tariff change and add/remove options). If the permission is not granted, the "Change" Buttons in 360 are disabled.',
        'roles' => array(
            'Telesales Agent',
            'Super Agent',
            'Backoffice Agent',
            'Retail Agent',
            '360 only'
        )
    ),
    'UNKNOWN_PRODUCTS' => array(
        'description' => 'The permission controls, for which agent it is allowed to see those products of the customers’ installed base in 360 that are unknown in the product catalog.',
        'roles' => array(
            'Retail Agent'
        )
    ),
    'NPH_PROLONGATION' => array(
        'description' => 'The permission controls, for which agent it is allowed to trigger a contract prolongation (vvl-type =NPH).',
        'roles' => array(
            'Retail Agent'
        )
    ),
    'DEBIT_TO_CREDIT' => array(
        'description' => 'The permission controls, for which agent it is allowed to trigger a DTC (debit to credit change). If the permission is not granted, the "DTC" buttons in 360 shall be disabled.',
        'roles' => array(
            'Telesales Agent',
            'Super Agent',
            'Backoffice Agent',
            'Retail Agent',
            '360 only'
        )
    ),
    'ALLOW_MOB' => array(
        'description' => 'The permission controls, for which agent it is allowed to trigger MOB-stack related sales scenarios in general. If the permission is not granted, the sales Agent shall not be able
		-	to retreive any MOB customer data in 360
		-	to order any new MOB package (pre-/ postpaid)
		-	to process any MOB Change package',
        'roles' => array(
            'Telesales Agent',
            'Super Agent',
            'Backoffice Agent',
            'Retail Agent',
            '360 only'
        )
    ),
    'ALLOW_CAB' => array(
        'description' => 'The permission controls, for which agent it is allowed to trigger CAB-stack related sales scenarios in general. If the permission is not granted, the sales Agent shall not be able
		-	to retreive any CAB(KIP, KUD, TV) customer data in 360
		-	to order any new CAB(KIP, KUD, TV)package (pre-/ postpaid)
		-	to process any CAB(KIP, KUD, TV) Change package',
        'roles' => array(
            'Telesales Agent',
            'Super Agent',
            'Backoffice Agent',
            'Retail Agent',
            '360 only'
        )
    ),
    'ALLOW_DSL' => array(
        'description' => 'The permission controls, for which agent it is allowed to trigger DSL/LTE-stack related sales scenarios in general. If the permission is not granted, the sales Agent shall not be able
		-	to retreive any DSL/LTE customer data in 360
		-	to order any new DSL/LTE package (pre-/ postpaid)
		-	to process any DSL/LTE Change package',
        'roles' => array(
            'Telesales Agent',
            'Super Agent',
            'Backoffice Agent',
            'Retail Agent',
            '360 only'
        )
    ),
    'SEND_SAVED_OFFER' => array(
        'description' => 'The permission controls, for which agent it is allowed to send saved offers. If the permission is not granted the Agent shall not be able to click the "send" button for a saved offer.',
        'roles' => array()
    ),
    'LOCKING_ACCOUNTS' => array(
        'description' => 'The permission controls for which agent it shall be allowd to lock or unlock accounts. If the permission is not granted, the “Locking Accounts” functionality in the User Management menu will be disabled.',
        'roles' => array(
            'User Admin'
        )
    )
);

$permissionIds = array();
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissions as $code => $details) {
    // Check if permissions already exist
    $sql = "SELECT `name` FROM `role_permission` WHERE `name` LIKE '$code'";
    $result = $conn->fetchAll($sql);

    if (sizeof($result) == 0) {
        $conn->insert('role_permission', array('name' => $code, 'description' => $details['description']));
        $permissionId = $conn->lastInsertId();

        foreach ($details['roles'] as $role) {
            $sql = "SELECT `role_id` FROM `agent_role` WHERE `role` = '$role'";
            $roleIds = $conn->fetchAll($sql, array(), Zend_Db::FETCH_COLUMN);

            foreach ($roleIds as $roleId) {
                $sql = "SELECT `link_id` FROM `role_permission_link` WHERE `role_id`=" . (int)$roleId . " AND `permission_id`=" . (int)$permissionId;
                $result = $conn->fetchAll($sql);

                if (sizeof($result) == 0) {
                    $conn->insert('role_permission_link', array('role_id' => $roleId, 'permission_id' => $permissionId));
                }
            }
        }
    }
}

$installer->endSetup();
