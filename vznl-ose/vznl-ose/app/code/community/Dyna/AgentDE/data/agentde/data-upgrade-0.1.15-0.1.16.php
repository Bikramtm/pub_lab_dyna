<?php

/**
 * Installer that adds default values for allowed urls (to be skipped by controller pre-dispatch event observer in agent module)
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

$configPath = "agent/general_config/allowed_urls";

if (Mage::getStoreConfig($configPath)) {
    // Get default value listed in config.xml file from Dyna_AgentDE module
    $value = Mage::getConfig()
        ->loadModulesConfiguration('config.xml')
        ->getNode("default/" . $configPath)
        ->asArray();
    Mage::getConfig()->saveConfig($configPath, $value, 'default', 0);
}

$this->endSetup();