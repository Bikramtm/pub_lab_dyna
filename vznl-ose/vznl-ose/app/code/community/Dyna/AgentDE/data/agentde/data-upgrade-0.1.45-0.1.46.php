<?php
/**
* VFDED1W3S-3861
*/
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
$newRole = "Admin User Agent";
$permissions = [
    "CHANGE_PASSWORD",
    "LOCKING_ACCOUNTS"
];

// Check if role exits already exist
$roleIdResult = $conn->fetchAll("SELECT `role_id` FROM `agent_role` WHERE `role` LIKE '".$newRole."' LIMIT 1", [], Zend_Db::FETCH_COLUMN);
$roleId = 0;
if((sizeof($roleIdResult) == 0)){
    $conn->insert('agent_role', ['role' => $newRole]);
    $roleId = $conn->lastInsertId();
}else{
    $roleId = current($roleIdResult);
}

//link permissions with agent role
foreach ($permissions as $code) {
    // Check if permissions already exist
    $result = $conn->fetchAll("SELECT `entity_id` FROM `role_permission` WHERE `name` LIKE '$code'", [], Zend_Db::FETCH_COLUMN);
    //if permission doesn't exist, create it
    if (sizeof($result) == 0) {
        $conn->insert('role_permission', ['name' => $code]);
        $permissionId = $conn->lastInsertId();
    } else {
        $permissionId = current($result);
    }
    //check if link exists between role and permission
    $links = $conn->fetchAll("SELECT `link_id` FROM `role_permission_link` WHERE `permission_id` = ".$permissionId." AND `role_id` = ".$roleId, [], Zend_Db::FETCH_COLUMN);
    if(sizeof($links) == 0){
        $conn->insert('role_permission_link', ['role_id' => $roleId, 'permission_id' => $permissionId]);
    }
}

//update LINKED_ACCOUNT_RESULTS permission
if (sizeof($conn->fetchAll("SELECT `entity_id` FROM `role_permission` WHERE `name` LIKE 'LINKED_ACCOUNT_RESULTS'", [], Zend_Db::FETCH_COLUMN)) > 0) {
    $conn->exec("DELETE FROM `role_permission` WHERE `name` LIKE 'LINKED_ACCOUNTS_RESULTS'; 
                 UPDATE `role_permission` SET `name` = 'LINKED_ACCOUNTS_RESULTS' WHERE `name` = 'LINKED_ACCOUNT_RESULTS'");
}