<?php

$installer = $this;

// add permission to OPEN ORDERS
$permissions = array(
    'CHANGE_OPTION' => 'Agent is able to change installed base product option (for a logged in customer)',
    'CHANGE_TARIFF' => 'Agent is able to change installed base product tariff (for a logged in customer)',
    'MOVE_OFFNET' => 'Agent is able to move offnet a customer installed base cable product to dsl (for a logged in customer)',
    'CHANGE_SAVED_OFFER' => 'Agent is able to change a saved offer (for a logged in customer)',
    'LINKED_ACCOUNTS_RESULTS' => 'Agent is able to find other link accounts (for a logged in customer) ',
    'PROLONGATION_CHECK' => 'Agent is able to make a prolongation check for a installed base mobile postpaid product (for a logged in customer)',
    'CHANGE_SAVED_SHOPPINGCART' => 'Agent is able to change a saved cart (for a logged in customer)',
    'SELECT_CAMPAIGN_OFFER' => 'Agent is able to select a campaign offer (for a logged in customer)',
    'CREATE_PACKAGE' => 'Agent is able to create a package',
);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissions as $code => $description) {
    // Check if permissions already exist
    $sql = "SELECT `name` FROM `role_permission` WHERE `name` LIKE '$code'";
    $result = $conn->fetchAll($sql);

    if (sizeof($result) == 0) {
        $conn->insert('role_permission', array('name' => $code, 'description' => $description));
    }
}

$installer->endSetup();