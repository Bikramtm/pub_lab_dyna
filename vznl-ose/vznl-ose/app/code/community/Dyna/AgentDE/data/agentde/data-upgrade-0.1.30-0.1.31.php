<?php

$installer = $this;
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
$sql = "SELECT `entity_id` FROM `role_permission` WHERE `name` LIKE 'SEND_SAVED_OFFER' LIMIT 1";
$res = $conn->fetchRow($sql);

if (isset($res['entity_id']) && $res['entity_id'] > 0) {
    $conn->query("DELETE FROM `role_permission_link` WHERE `permission_id` = {$res['entity_id']}");
    $conn->query("DELETE FROM `role_permission` WHERE `entity_id` = {$res['entity_id']}");
}

$installer->endSetup();
