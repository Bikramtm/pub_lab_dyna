<?php

$installer = $this;

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');

// Roles list (role id - role name)
$rolesSetup = array(
    6 => 'Telesales Agent',
    7 => 'Super Agent',
    8 => 'Backoffice Agent',
    4 => '360 only',
);

// Set up roles
foreach( $rolesSetup as $roleId => $roleName ) {
    // Checking if role already exists
    $sql = "SELECT `role` FROM `agent_role` WHERE `role_id`=" . (int) $roleId;
    $result = $conn->fetchAll( $sql );
    if( sizeof( $result ) == 0 ) {
        // Add role, if does not exist
        $conn->insert( 'agent_role', array( 'role_id' => $roleId, 'role' => $roleName ));
    } else {
        $localRoleName = mb_strtolower( trim( $result[0]['role'] ) );
        $properRoleName = mb_strtolower( trim( $roleName ) );
        if( $localRoleName != $properRoleName ) {
            // Update role name, if not properly set
            $conn->update( 'agent_role', array( 'role' => $roleName ), array( 'role_id = ?' => $roleId ));
        }
    }
}

// Permissions list for the roles (permission - roles id list)
$permissionsForRoleSetup = array(
    'CHANGE_PASSWORD'               => array( 6, 7, 8, 4 ),
    'SEARCH_CUSTOMER'               => array( 6, 7, 8, 4 ),
    'CREATE_PACKAGE'                => array( 6, 7, 8 ),
    'SEARCH_ORDER_OF_DEALER'        => array( 6 ),
    'SEARCH_ORDER_OF_GROUP'         => array( 7, 8 ),
    'SEARCH_ORDER_OF_ALL'           => array( 7, 8 ),
    'SEARCH_ORDER_OF_TELESALES'     => array(),
    'CHANGE_OPTION'                 => array( 6, 7, 8 ),
    'CHANGE_SAVED_OFFER'            => array(),
    'CHANGE_SAVED_OFFER_DEALER'     => array( 6, 7, 8 ),
    'CHANGE_TARIFF'                 => array( 6, 7, 8 ),
    'DELETE_SAVED_OFFER'            => array( 7, 8 ),
    'DELETE_SAVED_SHOPPING_CART'    => array( 7, 8 ),
    'LINKED_ACCOUNTS_RESULTS'       => array( 6, 7, 8, 4 ),
    'MOBILE_PORTING'                => array(),
    'MOVE_OFFNET_CABLE_TO_DSL'      => array( 6, 7, 8 ),
    'MIGRATION_DSL_TO_CABLE'        => array( 6, 7, 8 ),
    'ONLINE_REPORT'                 => array(),
    'PROLONGATION_CHECK'            => array( 6, 7, 8, 4 ),
    'SELECT_CAMPAIGN_OFFER'         => array(),
    'SEND_OFFER'                    => array( 6, 7, 8 ),
);


// Set up permissions for the roles
foreach( $permissionsForRoleSetup as $permissionName => $rolesIds ) {
    // Look for permission, if not found, we omit setting it up
    $sql = "SELECT * FROM `role_permission` WHERE `name`='" . $permissionName . "'";
    $result = $conn->fetchAll( $sql );
    if( sizeof( $result ) != 0 ) {
        $permissionId = (int) $result[0]['entity_id'];
        // Verify if roles are set
        foreach( $rolesIds as $roleId ) {
            $sql = "SELECT `link_id` FROM `role_permission_link` WHERE `role_id`=" . (int) $roleId . " AND `permission_id`=" . (int) $permissionId;
            $result = $conn->fetchAll( $sql );
            if( sizeof( $result ) == 0 ) {
                $conn->insert( 'role_permission_link', array( 'role_id' => $roleId, 'permission_id' => $permissionId ));
            }
        }
    }
}

$installer->endSetup();