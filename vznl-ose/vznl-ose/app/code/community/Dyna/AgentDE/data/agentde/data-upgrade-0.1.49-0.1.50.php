<?php
/**
 * VFDED1W3S-3861
 */
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');

$conn->exec("
    SET FOREIGN_KEY_CHECKS = 0;
    ALTER TABLE `agent` DROP FOREIGN KEY `agent_ibfk_1`;
    ALTER TABLE `agent` ADD FOREIGN KEY (`role_id`) REFERENCES `agent_role`(`role_id`);
    SET FOREIGN_KEY_CHECKS = 1;
");
