<?php
/**
 * VFDED1W3S-3861
 */
$newRoles = [
    1 => "Telesales Agent",
    2 => "COPS Agent",
    3 => "Super Agent",
    4 => "Back-Office Agent",
    5 => "360-View Agent",
    6 => "Default Role",
];
$newPermissions = [
    "CHANGE_PASSWORD"               => [1, 2, 3, 4, 5],
    "SEARCH_CUSTOMER"               => [1, 2, 3, 4, 5],
    "CREATE_PACKAGE"                => [1, 2, 3, 4],
    "CREATE_WORKITEM"               => [1, 2, 3, 4, 5],
    "SEARCH_ORDER_OF_DEALER"        => [2],
    "SEARCH_ORDER_OF_GROUP"         => [1, 3, 4],
    "SEARCH_ORDER_OF_ALL"           => [3, 4],
    "SEARCH_ORDER_OF_TELESALES"     => [],
    "CHANGE_OPTION"                 => [1, 2, 3, 4],
    "CHANGE_SAVED_OFFER"            => [],
    "CHANGE_SAVED_OFFER_DEALER"     => [1, 2, 3, 4],
    "CHANGE_TARIFF"                 => [1, 2, 3, 4],
    "DELETE_SAVED_OFFER"            => [3, 4],
    "DELETE_SAVED_SHOPPING_CART"    => [3, 4],
    "LINKED_ACCOUNT_RESULTS"        => [1, 2, 3, 4, 5],
    "MOVE_OFFNET_CABLE_TO_DSL"      => [1, 2, 3, 4],
    "MIGRATION_DSL_TO_CABLE"        => [1, 2, 3, 4],
    "ONLINE_REPORT"                 => [],
    "PROLONGATION_CHECK"            => [1, 2, 3, 4, 5],
    "SELECT_CAMPAIGN_OFFER"         => [1, 3, 4],
    "SEND_OFFER"                    => [1, 2, 3, 4],
    "SELECT_MARKETING_DATA"         => [2, 4],
    "VOICELOG_REQUIRED"             => [1],
    "CHANGE_TO_MOB_BASE"            => [1, 2, 3, 4, 5],
    "CHANGE_TO_CAB_BASE"            => [1, 2, 3, 4, 5],
    "CHANGE_TO_DSL_BASE"            => [1, 2, 3, 4, 5],
    "DEBIT_TO_CREDIT"               => [1, 2, 3, 4, 5],
    "ALLOW_MOB"                     => [1, 2, 3, 4, 5],
    "ALLOW_CAB"                     => [1, 2, 3, 4, 5],
    "ALLOW_DSL"                     => [1, 2, 3, 4, 5],
    "NPH_PROLONGATION"              => [],
    "TRIGGER_ILS"                   => [1, 2, 3, 4, 5],
    "PREPAID_TO_POSTPAID"           => [1, 2, 3, 4, 5],
    "PREPAID_TO_RED+"               => [1, 2, 3, 4, 5],
    "RENEW_PACKAGE"                 => [1, 2, 3, 4, 5],
    "POSTPAID_TO_RED+"              => [1, 2, 3, 4, 5],
    "LOCKING_ACCOUNTS"              => [],
    "UNKNOWN_PRODUCTS"              => []
];

$permissionIds = [];
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
$roleIds = [];

//delete all user roles and role permission links
$conn->exec(
    "DELETE FROM `role_permission_link`;
     SET FOREIGN_KEY_CHECKS = 0;
     DELETE FROM `agent_role`;"
);
//first create the new agent roles
foreach ($newRoles as $role) {
    $conn->insert('agent_role', ['role' => $role]);
    $roleIds[$conn->lastInsertId()] = $role;
}

$conn->exec("UPDATE `agent` SET `role_id` = " . end(array_keys($roleIds)) ."; SET foreign_key_checks=1;");

//link permissions with agent roles
foreach ($newPermissions as $code => $roles) {
    // Check if permissions already exist
    $sql = "SELECT `entity_id` FROM `role_permission` WHERE `name` LIKE '$code'";
    $result = $conn->fetchAll($sql, [], Zend_Db::FETCH_COLUMN);

    //if permission doesn't exist, create it
    if (sizeof($result) == 0) {
        $conn->insert('role_permission', ['name' => $code]);
        $permissionId = $conn->lastInsertId();
    } else {
        $permissionId = current($result);
    }

    //only create the permission but don't assign to any role
    if(empty($roles)){
        continue;
    }

    //assign permissions to roles
    foreach ($roles as $roleNr) {
        $roleId = array_search ($newRoles[$roleNr], $roleIds);
        if(in_array($newRoles[$roleNr], $roleIds)){
            $roleId = array_search ($newRoles[$roleNr], $roleIds);
            $conn->insert('role_permission_link', ['role_id' => $roleId, 'permission_id' => $permissionId]);
        }
    }
}