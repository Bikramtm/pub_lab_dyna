<?php
/**
 * VFDED1W3S-4349
 *
PREPAID_TO_POSTPAID > not needed since there is already DEBIT_TO_CREDIT
PREPAID_TO_RED+ > not needed since there is already debit_to_credit (D2C should also consider convert to red+ from debit)
RENEW_PACKAGE > not needed since there is already CHANGE_TO_MOB_BASE
POSTPAID_TO_RED+ > not needed since there is already CHANGE_TO_MOB_BASE
TRIGGER_ILS > is not needed since there is already CHANGE_TO_MOB_BASE, CHANGE_TO_DSL_BASE and CHANGE_TO_CAB_BASE
 */
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
//tables
$permissionsTable = 'role_permission';
$permissionsLinkTable = 'role_permission_link';
$permissionsToDelete = "'PREPAID_TO_POSTPAID', 'PREPAID_TO_RED+', 'RENEW_PACKAGE', 'POSTPAID_TO_RED+', 'TRIGGER_ILS'";

$sql = "SELECT * FROM `" . $permissionsTable . "` WHERE `name` IN (".$permissionsToDelete.")";
$permissions = $conn->fetchAll($sql);
if (count($permissions) > 0) {
    foreach ($permissions as $permission) {
        $conn->exec(" DELETE FROM `".$permissionsLinkTable."` WHERE `permission_id` = ".$permission['entity_id'] .";
                      DELETE FROM `".$permissionsTable."` WHERE `entity_id` = ". $permission['entity_id']);
    }
}