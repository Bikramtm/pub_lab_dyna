<?php

$installer = $this;

// add permission to DELETE_SHOPPING_CART
$permissions = array(
    'DELETE_SHOPPING_CART' => 'Agent is able to delete a customer saved shopping cart',
);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissions as $code => $description) {
    // Check if permissions already exist
    $sql = "SELECT `name` FROM `role_permission` WHERE `name` LIKE '$code'";
    $result = $conn->fetchAll($sql);

    if (sizeof($result) == 0) {
        $conn->insert('role_permission', array('name' => $code, 'description' => $description));
    }
}

$installer->endSetup();