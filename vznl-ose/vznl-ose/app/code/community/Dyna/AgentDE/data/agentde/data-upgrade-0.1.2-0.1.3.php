<?php
/**
 * Cms Page  with 'home' identifier page should have by default 3 columns
 */
/** @var $homePage Mage_Cms_Model_Page */
$homePage = Mage::getModel('cms/page')->load('home', 'identifier');
$homePage->setRootTemplate('three_columns');
$homePage->save();

