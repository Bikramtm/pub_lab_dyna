<?php

/**
 * Class Dyna_AgentDE_Model_ProvisSales
 */
class Dyna_AgentDE_Model_ProvisSales extends Mage_Core_Model_Abstract {

    protected function _construct()
    {
        parent::_construct();
        $this->_init('agentde/provisSales');
    }
}