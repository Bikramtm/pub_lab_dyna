<?php

/**
 * Class Dyna_AgentDE_Model_Mysql4_VodafoneShip2Stores_Collection
 */
class Dyna_AgentDE_Model_Mysql4_VodafoneShip2Stores_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Dyna_AgentDE_Model_Mysql4_VodafoneShip2Stores_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('agentde/vodafoneShip2Stores');
    }
}
