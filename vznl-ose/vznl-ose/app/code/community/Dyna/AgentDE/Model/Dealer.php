<?php

/**
 * Class Dyna_AgentDE_Model_Dealer
 * Extends and overrides Dyna_Agent_Model_Dealer _beforeSave method to map the dealer to it's allowed campaigns
 */
class Dyna_AgentDE_Model_Dealer extends Dyna_Agent_Model_Dealer
{
    const ORDER_PERMISSION_ALL = "SEARCH_ORDER_OF_ALL";
    const ORDER_PERMISSION_TELESALES = "SEARCH_ORDER_OF_TELESALES";
    const ORDER_PERMISSION_DEALER_GROUP = "SEARCH_ORDER_OF_GROUP";
    const ORDER_PERMISSION_DEALER = "SEARCH_ORDER_OF_DEALER";

    /**
     * @return $this
     */
    public function _beforeSave()
    {
        if (!$dealerId = $this->getId()) {
            return $this;
        }

        /** Previously existing campaigns */
        $dealerHadCampaigns = Mage::getModel('agentde/DealerCampaigns')
            ->getCollection()
            ->getCampaignsForDealer($dealerId, true);

        if (empty($dealerHadCampaigns)) {
            $dealerHadCampaigns = [];
        }

        /** New campaigns */
        $dealerHasCampaigns = $this->getData('campaign_id');
        if (empty($dealerHasCampaigns)) {
            $dealerHasCampaigns = [];
        }

        $connection = Mage::getSingleton('core/resource')->getConnection('read');

        /** Delete the no longer available asigned campaigns */
        $toBeDeleted = array_diff($dealerHadCampaigns, $dealerHasCampaigns);
        if (!empty($toBeDeleted)) {
            $query = "delete from dealer_allowed_campaigns where dealer_id='" . $this->getId() . "' and campaign_id in (" . implode(',', $toBeDeleted) . ")";
            $connection->query($query);
        }

        /** Add the new assigned campaigns */
        $toBeAdded = array_diff($dealerHasCampaigns, $dealerHadCampaigns);
        foreach ($toBeAdded as $new) {
            $query = "insert into dealer_allowed_campaigns(dealer_id, campaign_id, allowed) values('" . $this->getId() . "', '" . $new . "', '1')";
            $connection->query($query);
        }

        return $this;
    }

    /**
     * Returns all campaigns to which a dealer is assigned
     * @return mixed
     */
    public function getDealerCampaigns($asArray = false)
    {
        return Mage::getModel('agentde/DealerCampaigns')
            ->getCollection()
            ->getCampaignsForDealer($this->getId(), $asArray);
    }

    /**
     * Returns all dealers that are part of the group
     * @param $dealerGroupId
     * @return array
     */
    public function getDealersInGroup($dealerGroupId)
    {
        /** @var Varien_Db_Adapter_Interface $adapter */
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $adapter->select()
            ->from('dealer_group_link')
            ->columns('dealer_id')
            ->where('group_id = ?', $dealerGroupId);
        $rows = $adapter->fetchAll($sql);

        $dealers = [];
        foreach ($rows as $singleDealer) {
            $dealers[] = $singleDealer['dealer_id'];
        }

        return $dealers;
    }
}
