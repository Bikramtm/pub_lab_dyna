<?php

/**
 * Class Dyna_AgentDE_Model_Mysql4_Commision
 */
class Dyna_AgentDE_Model_Mysql4_Commision extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_AgentDE_Model_Mysql4_Commision constructor.
     */
    public function _construct()
    {
        $this->_init('agentde/commision', 'entity_id');
    }
}
