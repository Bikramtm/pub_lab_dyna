<?php

/**
 * Class Dyna_AgentDE_Model_Mysql4_VodafoneShip2Stores
 */
class Dyna_AgentDE_Model_Mysql4_PhoneBookIndustries extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_AgentDE_Model_Mysql4_PhoneBookIndustries constructor.
     */
    public function _construct()
    {
        $this->_init('agentde/phoneBookIndustries', 'entity_id');
    }
}