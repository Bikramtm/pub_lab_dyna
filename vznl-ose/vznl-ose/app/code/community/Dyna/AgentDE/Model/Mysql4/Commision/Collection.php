<?php

/**
 * Class Dyna_AgentDE_Model_Mysql4_Commision_Collection
 */
class Dyna_AgentDE_Model_Mysql4_Commision_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Dyna_AgentDE_Model_Mysql4_Commision_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('agentde/commision');
    }
}
