<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_AgentDE_Model_Mapper_PhoneBookKeywords
 */
class Dyna_AgentDE_Model_Mapper_PhoneBookKeywords extends Dyna_Agent_Model_Mapper_AbstractMapper
{
    /** @var Dyna_AgentDE_Model_Mysql4_PhoneBookKeywords_Collection */
    protected $_collection;

    /**
     * @param array $data
     * @return Dyna_AgentDE_Model_Agent
     */
    public function map(array $data)
    {
        /** @var Dyna_Agent_Model_Agent $agent */
        if (isset($data['name']) && $data['name'] && ($existingPhoneBookKeyword = $this->_getCollection()->getItemByColumnValue('name', $data['name']))) {
            $phoneBookKeyword = $existingPhoneBookKeyword->load($existingPhoneBookKeyword->getId());
            $phoneBookKeyword->setIsNew(false);
        } else {
            $phoneBookKeyword = Mage::getModel('agentde/phoneBookKeywords');
            $phoneBookKeyword->setIsNew(true);
        }

        foreach ($data as $property => $value) {
            try {
                $phoneBookKeyword->setData($property, $this->mapProperty($property, $value, $phoneBookKeyword));
            } catch (UnexpectedValueException $e) {
                //no-op as this ex will be thrown for old agents
            }
        }

        return $phoneBookKeyword;
    }

    /**
     * @return Dyna_AgentDE_Model_Mysql4_PhoneBookKeywords_Collection
     */
    protected function _getCollection()
    {
        if ( ! $this->_collection) {
            return $this->_collection = Mage::getResourceModel('agentde/phoneBookKeywords_collection');
        }
        return $this->_collection;
    }
}