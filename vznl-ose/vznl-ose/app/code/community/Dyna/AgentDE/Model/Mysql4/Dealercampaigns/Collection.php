<?php

/**
 * Class Dyna_AgentDE_Model_Mysql4_DealerCampaigns_Collection
 */
class Dyna_AgentDE_Model_Mysql4_DealerCampaigns_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Dyna_AgentDE_Model_Mysql4_DealerCampaigns_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('agentde/dealerCampaigns');
    }

    /**
     * Return a collection of campaigns asigned to a user
     *
     * @param $dealerId int
     * @param bool $asArray
     * @return $this|mixed
     */
    public function getCampaignsForDealer($dealerId, $asArray = false)
    {
        $this->join(array('ac' => 'dealerallowedcampaigns'), 'main_table.campaign_id = ac.campaign_id');
        $this->addFieldToFilter('dealer_id', array('eq' => $dealerId));
        $this->addFieldToFilter('allowed', array('eq' => 1));

        if ($asArray) {
            $campaigns = array();
            foreach ($this as $campaign) {
                $campaigns[$campaign->getName()] = $campaign->getId();
            }

            return $campaigns;
        }

        return $this;
    }
}
