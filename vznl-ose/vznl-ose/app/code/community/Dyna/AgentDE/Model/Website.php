<?php

/**
 * Class Dyna_AgentDE_Model_Website
 */
class Dyna_AgentDE_Model_Website extends Mage_Core_Model_Abstract
{
    const WEBSITE_TELESALES_CODE = 'telesales';
    const WEBSITE_INDIRECT_CODE = 'indirect';
    const WEBSITE_BC_WEBSHOP_CODE = 'belcompany_webshop';
    const WEBSITE_TRADERS_CODE = 'vodafone_traders';

    const SHOW_AGENT = 1;

    /** @var array */
    protected static $_codes = array();

    /**
     * Returns an array containing all website codes
     *
     * @return array
     */
    public static function getWebsiteCodes()
    {
        if (! self::$_codes) {
            foreach (Mage::app()->getWebsites() as $_websiteId => $val) {
                array_push(self::$_codes, Mage::app()->getWebsite($_websiteId)->getCode());
            }
        }

        return self::$_codes;
    }
}
