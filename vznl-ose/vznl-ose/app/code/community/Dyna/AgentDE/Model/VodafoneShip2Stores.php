<?php

/**
 * Class Dyna_AgentDE_Model_VodafoneShip2Stores
 */
class Dyna_AgentDE_Model_VodafoneShip2Stores extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('agentde/vodafoneShip2Stores');
    }

    /**
     * @param int $id
     * @param null $field
     * @return $this|Mage_Core_Model_Abstract
     */
    public function load($id, $field=null)
    {
        $key = 'vodafoneship2stores_' . md5(serialize(array($id, $field)));
        if ($vfStore = Mage::objects()->load($key)) {
            $this->setData($vfStore->getData());
            return $this;
        }

        parent::load($id, $field);
        Mage::objects()->save($this, $key);
        return $this;
    }

    public function validateData($dataObject)
    {
        $response = [
            'success' => true,
            'missingFields' => []
        ];
        if(!$dataObject->getStoreIdDataSource()) {
            $response['success'] = false;
            $response['missingFields'][] = 'StoreIdDataSource';
        }
        if(!$dataObject->getShopName1()) {
            $response['success'] = false;
            $response['missingFields'][] = 'ShopName1';
        }
        if(!$dataObject->getStreet()) {
            $response['success'] = false;
            $response['missingFields'][] = 'Street';
        }
        if(!$dataObject->getPostcode()) {
            $response['success'] = false;
            $response['missingFields'][] = 'PostalCode';
        }
        if(!$dataObject->getCity()) {
            $response['success'] = false;
            $response['missingFields'][] = 'City';
        }
        if(!$dataObject->getGeoX()) {
            $response['success'] = false;
            $response['missingFields'][] = 'GeoX';
        }
        if(!$dataObject->getGeoY()) {
            $response['success'] = false;
            $response['missingFields'][] = 'GeoY';
        }
        return $response;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->getStreet() . ' ' . $this->getHouseNo(). ", " . $this->getPostcode() . ' ' . $this->getCity();
    }

    /**
     * @return string
     */
    public function getAddressHtml()
    {
        return $this->getStreet() . ' ' . $this->getHouseNo(). "<br/> " . $this->getPostcode() . ' ' . $this->getCity();
    }

    /**
     * @return array
     */
    public function getAddressJson()
    {
        return array(
            'shopName' => $this->getShopName1(),
            'street' => $this->getStreet(),
            'houseno' => $this->getHouseNo(),
            'postcode' => $this->getPostcode(),
            'city' => $this->getCity(),
        );
    }
}