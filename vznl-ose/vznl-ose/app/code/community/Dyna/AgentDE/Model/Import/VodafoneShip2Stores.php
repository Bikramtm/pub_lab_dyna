<?php

/**
 * Class Dyna_AgentDE_Model_Import_VodafoneShip2Stores
 */
class Dyna_AgentDE_Model_Import_VodafoneShip2Stores extends Omnius_Import_Model_Import_ImportAbstract
{
    const NUMBER_OF_COLUMNS = 12;
    protected $_data;
    protected $_headerColumns;
    protected $_csvDelimiter = '|';
    protected $_header;
    protected $_rowNumber = 1;
    protected $_logFileName = "vodafone_ship2stores_import";
    protected $_logFileExtension = "log";

    /** @var Dyna_Import_Helper_Data $_helper */
    public $_helper;

    
    /**
     * Dyna_AgentDE_Model_Import_VodafoneShip2Stores constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
    }

    /**
     * @param $header
     * @return bool
     */
    public function setHeader($header)
    {
        if ($this->validateHeader($header)) {
            $this->_header = $header;
            return true;
        }
        return false;
    }

    private function validateHeader($header)
    {
        if (count($header) != self::NUMBER_OF_COLUMNS) {
            $this->_logError("[ERROR] File rejected: header is invalid. The header should be " . implode(",", self::importFileHeader()));
            return false;
        }
        $validHeader = self::importFileHeader();
        foreach ($header as $columnName) {
            if (false === $keyFound = array_search($columnName, self::importFileHeader())) {
                $this->_logError("[ERROR] File rejected: header is invalid. The column is not valid: " . $columnName);
                $this->_logError("[ERROR] File rejected: header is invalid. The header should be: " . implode(",", self::importFileHeader()));
                return false;
            } else {
                unset($validHeader[$keyFound]);
            }
        }
        if (count($validHeader)) {
            $this->_logError("[ERROR] File rejected: header is invalid. These columns were not present in the import: " . implode(",", self::importFileHeader()));
        }
        return true;
    }

    public static function importFileHeader()
    {
        return [
            "store_id_data_source",
            "shop_name1",
            "shop_name2",
            "shop_name3",
            "street",
            "house_number",
            "postal_code",
            "city",
            "phone_number",
            "email",
            "geo_x",
            "geo_y"
        ];
    }

    public function process($rawData)
    {
        $this->_rowNumber++;
        $rawData = array_combine($this->_header, $rawData);
        if (!array_filter($rawData)) {
            return false;
        }

        try {
            $this->clearData();
            $this->_data['store_id_data_source'] = $rawData['store_id_data_source'];
            $this->_data['shop_name1'] = $rawData['shop_name1'];
            $this->_data['shop_name2'] = $rawData['shop_name2'];
            $this->_data['shop_name3'] = $rawData['shop_name3'];
            $this->_data['street'] = $rawData['street'];
            $this->_data['house_no'] = $rawData['house_number'];
            $this->_data['postcode'] = $rawData['postal_code'];
            $this->_data['city'] = $rawData['city'];
            $this->_data['telephone'] = $rawData['phone_number'];
            $this->_data['email'] = $rawData['email'];
            $this->_data['geo_x'] = $rawData['geo_x'];
            $this->_data['geo_y'] = $rawData['geo_y'];

            /** leave rules processing to other method */
            $this->saveVodafoneShip2Store();
            $this->_totalFileRows++;
        } catch (Exception $e) {
            $this->_skippedFileRows++;
            $this->_logError("[ERROR] Skipped row because :" . $e->getMessage());
            fwrite(STDERR, $e->getMessage());
        }
    }

    /**
     * Reset vodafone store information
     */
    public function clearData()
    {
        $this->_data = array(
            'store_id_data_source' => '',
            'shop_name1' => '',
            'shop_name2' => '',
            'shop_name3' => '',
            'street' => '',
            'house_no' => '',
            'postcode' => '',
            'city' => '',
            'telephone' => '',
            'email' => '',
            'geo_x' => '',
            'geo_y' => '',
        );
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function saveVodafoneShip2Store()
    {
        /** @var Dyna_AgentDE_Model_VodafoneShip2Stores $model */
        $model = Mage::getModel('agentde/vodafoneShip2Stores');

        $validateResult = $model->validateData(new Varien_Object($this->_data));

        if (!$validateResult['success']) {
            $this->_logError("[ERROR] Skipped row. Missing mandatory fields :" . implode(",", $validateResult['missingFields']) . " at row " . $this->_rowNumber);
        } else {
            $model->addData($this->_data);
            $model->save();
            $this->_log("Successfully imported CSV line " . $this->_rowNumber . " : " . implode('|', $this->_data));
        }

        return $this;
    }
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }
}
