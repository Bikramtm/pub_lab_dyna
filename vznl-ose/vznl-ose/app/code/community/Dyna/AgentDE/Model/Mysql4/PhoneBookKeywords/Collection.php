<?php

/**
 * Class Dyna_AgentDE_Model_Mysql4_PhoneBookKeywords_Collection
 */
class Dyna_AgentDE_Model_Mysql4_PhoneBookKeywords_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Dyna_AgentDE_Model_Mysql4_PhoneBookKeywords_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('agentde/phoneBookKeywords');
    }
}
