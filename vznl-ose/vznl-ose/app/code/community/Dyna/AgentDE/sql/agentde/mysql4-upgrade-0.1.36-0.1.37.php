<?php
/**
 * Change salesIds type to varchar in order to avoid possible errors on Provis import
 * varchar is also the recommended type as seen in VFDED1W3S-542 (Provis mapping.xlsx)
 */
/** @var Mage_Eav_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

$table = $this->getTable('provis_sales_id');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

if ($connection->isTableExists("provis_sales_id")) {
    $connection->changeColumn($table, 'red_sales_id', 'red_sales_id', 'VARCHAR(50)');
    $connection->changeColumn($table, 'blue_sales_id', 'blue_sales_id', 'VARCHAR(50)');
    $connection->changeColumn($table, 'yellow_sales_id', 'yellow_sales_id', 'VARCHAR(50)');
    // update level too, to int, as specified in the source from above
    $connection->changeColumn($table, 'level', 'level', 'INT(10)');
}
// update red_sales_id as well for agent
$table = $this->getTable('agent');

if ($connection->isTableExists("agent")) {
    $connection->changeColumn($table, 'red_sales_id', 'red_sales_id', 'VARCHAR(50)');
}

$installer->endSetup();