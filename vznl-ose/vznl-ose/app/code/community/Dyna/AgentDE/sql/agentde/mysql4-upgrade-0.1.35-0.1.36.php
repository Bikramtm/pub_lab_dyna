<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();
$this->getConnection()
    ->addColumn($this->getTable('agent'),
        'last_login_date',
        [
            'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
            'default' => date('Y-m-d H:m:s'),
            'nullable' => false,
            'after' => 'login_attempts',
            'comment' => 'Last login date',
        ]
    );
$this->endSetup();