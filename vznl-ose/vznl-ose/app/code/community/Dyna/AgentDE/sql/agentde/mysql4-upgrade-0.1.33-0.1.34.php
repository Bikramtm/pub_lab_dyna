<?php
$installer = $this;

$permissions = array(
    'VIEW_CATALOG_VERSION' => 'Agent is able to create a workitem',
);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissions as $code => $description) {
    $conn->insert('role_permission', array('name' => $code, 'description' => $description));
}

$installer->endSetup();