<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();

// Create new table
$agentSalesIdsTable = new Varien_Db_Ddl_Table();
$agentSalesIdsTable->setName('agent_sales_ids');

// Drop table if exists
$dropSQL1 = "DROP TABLE IF EXISTS `agent_commision`";
$dropSQL2 = "DROP TABLE IF EXISTS " . $agentSalesIdsTable->getName();
$this->run($dropSQL1);
$this->run($dropSQL2);

$agentSalesIdsTable->addColumn(
    'agent_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER
);

$agentSalesIdsTable->addColumn(
    'sales_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER
);

// Add agent id as foreign key
$agentSalesIdsTable->addForeignKey(
    'fk_agent_id',
    'agent_id',
    'agent',
    'agent_id'
);

$connection->createTable($agentSalesIdsTable);

// Add column type as ENUM
$enumValues = "'blue/DSL','red/mobile','yellow/VPKNID'";
$addEnumSql = sprintf("ALTER TABLE `%s` ADD type enum(%s) NOT NULL", $agentSalesIdsTable->getName(), $enumValues);
$installer->run($addEnumSql);

$installer->endSetup();