<?php

$installer = $this;

// add permission to SELECT_MARKETING_DATA
$permissions = array(
    'SELECT_MARKETING_DATA' => 'Agent is able to see Marketing Data',
);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissions as $code => $description) {
    $sql = "SELECT * FROM `role_permission` WHERE `name`='" . $code . "'";
    $result = $conn->fetchAll( $sql );
    if (sizeof($result) == 0) {
        $conn->insert('role_permission', array('name' => $code, 'description' => $description));
    }
}

$installer->endSetup();
