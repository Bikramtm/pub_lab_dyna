<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();

$agentCommisionTableName = 'agent_commision';
// Drop old table
$dropSQL = "DROP TABLE IF EXISTS " . $agentCommisionTableName;
$this->run($dropSQL);

// Rename table
if ($connection->isTableExists("agent_sales_ids")) {
    $connection->renameTable('agent_sales_ids', $agentCommisionTableName);

    $connection->addColumn(
        $agentCommisionTableName,
        'entity_id',
        'INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY'
    );

    $connection->changeColumn(
        $agentCommisionTableName,
        'type',
        'type',
        "ENUM('Blue','Red','VPKN','Blue + Red') NOT NULL"
    );
}

$installer->endSetup();
