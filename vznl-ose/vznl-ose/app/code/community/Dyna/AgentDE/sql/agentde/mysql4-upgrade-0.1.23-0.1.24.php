<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();

$agentCommissionTableName = 'agent_commision';

// if table doesn't exist, the create
if (!$installer->getConnection()->isTableExists($agentCommissionTableName)) {
    $agentCommissionTable = new Varien_Db_Ddl_Table();
    $agentCommissionTable->setName($agentCommissionTableName);

    $agentCommissionTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, [
            "unsigned" => true,
            "primary" => true,
            "auto_increment" => true,
            "nullable" => false,
        ], "Primary key")
        ->addColumn(
            'agent_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER
        )
        ->addColumn(
            'sales_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER
        )
        ->addForeignKey(
            'fk_agent_id',
            'agent_id',
            'agent',
            'agent_id'
        )
    ;

    $connection->createTable($agentCommissionTable);

    // Add column type as ENUM
    $enumValues = "'blue/DSL','red/mobile','yellow/VPKNID'";
    $addEnumSql = sprintf("ALTER TABLE `%s` ADD type enum(%s) NOT NULL", $agentCommissionTable->getName(), $enumValues);
    $installer->run($addEnumSql);
}

// Rename table
$connection->addColumn(
    $agentCommissionTableName,
    'import_line',
    'INT(11)'
);

$installer->endSetup();
