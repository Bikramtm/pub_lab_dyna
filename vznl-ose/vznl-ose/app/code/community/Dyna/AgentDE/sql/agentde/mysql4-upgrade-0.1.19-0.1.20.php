<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();
$table = $this->getTable('dealer');

// Add dealer code kias
if (!$connection->tableColumnExists($table, 'vf_dealer_code_kias')) {
    $connection->addColumn(
        $table,
        'vf_dealer_code_kias',
        'VARCHAR(20) DEFAULT NULL'
    );
}

// Add dealer code fn
if (!$connection->tableColumnExists($table, 'vf_dealer_code_fn')) {
    $connection->addColumn(
        $table,
        'vf_dealer_code_fn',
        'VARCHAR(20) DEFAULT NULL'
    );
}

// Add dealer code cable
if (!$connection->tableColumnExists($table, 'vf_dealer_code_cable')) {
    $connection->addColumn(
        $table,
        'vf_dealer_code_cable',
        'VARCHAR(20) DEFAULT NULL'
    );
}

$installer->endSetup();
