<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

if (!$this->getConnection()->tableColumnExists($this->getTable("dealer"),"hotline_number")) {
    $this->getConnection()->addColumn($this->getTable("dealer"), "hotline_number", [
        "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
        "length" => 50,
        "after" => "is_deleted",
        "comment" => "Hotline number",
    ]);
}

$this->endSetup();
