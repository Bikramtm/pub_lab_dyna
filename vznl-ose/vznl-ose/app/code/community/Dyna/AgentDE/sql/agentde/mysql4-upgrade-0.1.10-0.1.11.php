<?php

/**
 *  Installer for adding table
 *  vodafone_ship2stores: the table that contains the data for the vodafone stores used for mobile postpaid shipping
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

/** Definition of vodafone_ship2stores table */

$vodafoneShip2storesTable = new Varien_Db_Ddl_Table();
$vodafoneShip2storesTable->setName('vodafone_ship2stores');

$dropSQL = "DROP TABLE IF EXISTS " . $vodafoneShip2storesTable->getName();
$this->run($dropSQL);

$vodafoneShip2storesTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
        "unsigned" => true,
        "primary" => true,
        "auto_increment" => true,
        "nullable" => false,
    ], "Primary key for vodafone_ship2stores table");
$vodafoneShip2storesTable->addColumn(
    'store_id_data_source',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    30,
    [
        "nullable" => false,
    ]
);
$vodafoneShip2storesTable->addColumn(
    'shop_name1',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    255,
    [
        "nullable" => false,
    ]
);
$vodafoneShip2storesTable->addColumn(
    'shop_name2',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    255
);
$vodafoneShip2storesTable->addColumn(
    'shop_name3',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    255
);
$vodafoneShip2storesTable->addColumn(
    'street',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    255,
    [
        "nullable" => false,
    ]
);
$vodafoneShip2storesTable->addColumn(
    'house_no',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    10,
    [
        "nullable" => false,
    ]
);
$vodafoneShip2storesTable->addColumn(
    'postcode',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    10,
    [
        "nullable" => false,
    ]
);
$vodafoneShip2storesTable->addColumn(
    'city',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    255,
    [
        "nullable" => false,
    ]
);
$vodafoneShip2storesTable->addColumn(
    'telephone',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    50,
    [
        "nullable" => false,
    ]
);
$vodafoneShip2storesTable->addColumn(
    'email',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    255,
    [
        "nullable" => false,
    ]
);
$vodafoneShip2storesTable->addColumn(
    'geo_x',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    255,
    [
        "nullable" => false,
    ]
);
$vodafoneShip2storesTable->addColumn(
    'geo_y',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    255,
    [
        "nullable" => false,
    ]
);

$vodafoneShip2storesTable->setOption('type', 'InnoDB');
$vodafoneShip2storesTable->setOption('charset', 'utf8');
$this->getConnection()->createTable($vodafoneShip2storesTable);