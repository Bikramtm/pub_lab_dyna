<?php

$installer = $this;

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');

// add permission for MOBILE PORTING
$permissions = array(
    'MOBILE_PORTING' => array(
        'description' => 'Agent is able to do mobile porting',
        'roles' => array(
            'COPS Agent',
            'Super Agent',
            'Back-Office Agent'
        )
    )
);

$permissionIds = array();

foreach ($permissions as $code => $details) {
    // Check if permissions already exist
    $sql = "SELECT `name` FROM `role_permission` WHERE `name` LIKE '$code'";
    $result = $conn->fetchAll($sql);

    if (sizeof($result) == 0) {
        $conn->insert('role_permission', array('name' => $code, 'description' => $details['description']));
        $permissionId = $conn->lastInsertId();

        foreach ($details['roles'] as $role) {
            $sql = "SELECT `role_id` FROM `agent_role` WHERE `role` = '$role'";
            $roleIds = $conn->fetchAll($sql, array(), Zend_Db::FETCH_COLUMN);

            foreach ($roleIds as $roleId) {
                $sql = "SELECT `link_id` FROM `role_permission_link` WHERE `role_id`=" . (int)$roleId . " AND `permission_id`=" . (int)$permissionId;
                $result = $conn->fetchAll($sql);

                if (sizeof($result) == 0) {
                    $conn->insert('role_permission_link', array('role_id' => $roleId, 'permission_id' => $permissionId));
                }
            }
        }

    }
}

$installer->endSetup();