<?php
/**
 * Change columns "house_no", "email", "telephone" from table vodafone_ship2stores to be nullable
 */
/** @var Mage_Eav_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

$table = $this->getTable('vodafone_ship2stores');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$connection->changeColumn($table, 'house_no', 'house_no', 'VARCHAR(10) NULL');
$connection->changeColumn($table, 'email', 'email', 'VARCHAR(255) NULL');
$connection->changeColumn($table, 'telephone', 'telephone', 'VARCHAR(50) NULL');

$installer->endSetup();