<?php
/** @var Mage_Eav_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

$table = $this->getTable('role_permission');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

if ($connection->isTableExists("role_permission")) {
    $connection->insert('role_permission', array('name' => 'TRIGGER_ILS'));
}

$installer->endSetup();