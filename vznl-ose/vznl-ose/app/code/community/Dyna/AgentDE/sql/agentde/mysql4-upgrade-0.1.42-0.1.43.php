<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

$connection->query("delete from core_config_data where path='contacts/contacts/enabled'");
$connection->query("insert into core_config_data(scope, scope_id, path, value) values ('default', 0, 'contacts/contacts/enabled', 0)");

$installer->endSetup();