<?php

/**
 *  Installer for adding two tables:
 *  dealer_campaigns: the table that holds all the campaigns available for dealers
 *  dealer_allowed_campaigns: the table that sets the rules by which a dealer is allowed or disallowed to participate to a certain campaign
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

/** Definition of dealer_campaign table */

$dealerCampaignsTable = new Varien_Db_Ddl_Table();
$dealerCampaignsTable->setName('dealer_campaigns');

$dropSQL = "DROP TABLE IF EXISTS " . $dealerCampaignsTable->getName();
$this->run($dropSQL);

$dealerCampaignsTable->addColumn(
    'campaign_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10,
    array('unsigned' => true, 'primary' => true, 'auto_increment' => true, 'nullable'=>false)
);
$dealerCampaignsTable->addColumn(
    'name',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    30
);


$dealerCampaignsTable->setOption('type', 'InnoDB');
$dealerCampaignsTable->setOption('charset', 'utf8');
$this->getConnection()->createTable($dealerCampaignsTable);

/** Definition of dealer_allowed_campaigns */

$dealerAllowedCampaignsTable = new Varien_Db_Ddl_Table();
$dealerAllowedCampaignsTable->setName('dealer_allowed_campaigns');

$dropSQL = "DROP TABLE IF EXISTS " . $dealerAllowedCampaignsTable->getName();
$this->run($dropSQL);

$dealerAllowedCampaignsTable->addColumn(
    'entity_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10,
    array('unsigned' => true, 'primary' => true, 'auto_increment' => true, 'nullable'=>false)
);
$dealerAllowedCampaignsTable->addColumn(
    'dealer_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10
);
$dealerAllowedCampaignsTable->addColumn(
    'campaign_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10
);
$dealerAllowedCampaignsTable->addColumn(
    'allowed',
    Varien_Db_Ddl_Table::TYPE_BOOLEAN
);
$dealerAllowedCampaignsTable->setOption('type', 'InnoDB');
$dealerAllowedCampaignsTable->setOption('charset', 'utf8');

/** Lets set foreign keys to dealers and campaigns */

$dealerAllowedCampaignsTable->addForeignKey(
    $this->getFkName($dealerAllowedCampaignsTable->getName(), 'dealer_id', 'agent/dealer', 'dealer_id'),
    'dealer_id', $this->getTable('agent/dealer'), 'dealer_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
);

$dealerAllowedCampaignsTable->addForeignKey(
    $this->getFkName($dealerAllowedCampaignsTable->getName(), 'campaign_id', $dealerCampaignsTable->getName(), 'campaign_id'),
    'campaign_id', $dealerCampaignsTable->getName(), 'campaign_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
);

$this->getConnection()->createTable($dealerAllowedCampaignsTable);

$this->endSetup();
