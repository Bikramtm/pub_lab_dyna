<?php
/**
 * Change column "status" from boolean, in order to support strings
 */
/** @var Mage_Eav_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

$table = $this->getTable('provis_sales_id');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

if ($connection->isTableExists("provis_sales_id")) {
    $connection->changeColumn($table, 'status', 'status', 'VARCHAR(50)');
}

$installer->endSetup();