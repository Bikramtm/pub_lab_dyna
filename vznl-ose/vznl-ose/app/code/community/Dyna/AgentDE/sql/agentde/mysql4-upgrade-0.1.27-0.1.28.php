<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();
$table = $this->getTable('dealer');

// Add dealer Bounce Email.
if (!$connection->tableColumnExists($table, 'bounce_info_email_address')) {
    $connection->addColumn(
        $table,
        'bounce_info_email_address',
        'VARCHAR(255) DEFAULT NULL'
    );
}

$installer->endSetup();
