<?php

/**
 * Add new table provis_sales_id (Provis import)
 *
 */
$installer = $this;

$installer->startSetup();

$tableName = $installer->getTable('provis_sales_id');

if (! $installer->getConnection()->isTableExists($tableName)) {
    // Defining table structure via object
    $table = $installer->getConnection()
        ->newTable($tableName)
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
            "unsigned" => true,
            "primary" => true,
            "auto_increment" => true,
            'nullable' => false
        ], "Primary key")
        ->addColumn('red_sales_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
            "unsigned" => true,
            'nullable' => false
        ], "Red part of the Sales ID Triple")
        ->addColumn('status',Varien_Db_Ddl_Table::TYPE_INTEGER, [
            "unsigned" => true,
            "nullable" => false,
        ], "Active or inactive Sales ID")
        ->addColumn('level',Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
            "unsigned" => true,
            "nullable" => false,
        ], "Level within the sales force organization hierarchy")
        ->addColumn('sales_channel',Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
            "unsigned" => true,
            "nullable" => false,
        ], "Sales Force organization categorization")
        ->addColumn('sales_subchannel',Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
            "unsigned" => true,
            "nullable" => false,
        ], "Sales Subchannel")
        ->addColumn('classification',Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
            "unsigned" => true,
            "nullable" => false,
        ], "Classification")
        ->addColumn('red_sales_id_level-0',Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
            "unsigned" => true,
            "nullable" => false,
        ], "topmost level of sales force organization hierarchy")
        ->addColumn('red_sales_id_level-1',Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
            "unsigned" => true,
            "nullable" => false,
        ], "Intermediate levels of sales force organization hierarchy")
        ->addColumn('red_sales_id_level-2',Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
            "unsigned" => true,
            "nullable" => false,
        ], "Intermediate levels of sales force organization hierarchy")
        ->addColumn('blue_sales_id',Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
            "unsigned" => true,
            "nullable" => false,
        ], "Blue part of the Sales ID Triple")
        ->addColumn('yellow_sales_id',Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
            "unsigned" => true,
            "nullable" => false,
        ], "Yellow part of the Sales ID Triple")
        ->addColumn('blue_top_vo',Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
            "unsigned" => true,
            "nullable" => false,
        ], "Additional grouping criteria based on DSL sales force organization")
    ;

    // Executing table creation object
    $installer->getConnection()->createTable($table);
}


$installer->endSetup();

