<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();
$this->getConnection()->addColumn($this->getTable("agent"), "usertype", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "after" => "agent_id",
    "comment" => "User Type",
]);

$this->getConnection()->addColumn($this->getTable("agent"), "ngumid", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "after" => "password",
    "comment" => "User NGUM ID",
]);

$this->getConnection()->addColumn($this->getTable("agent"), "externalmail", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "email",
    "comment" => "Extenal Email",
]);

$this->getConnection()->addColumn($this->getTable("agent"), "partnermanager", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "digest_auth_hash",
    "comment" => "Partner Manager",
]);

$this->getConnection()->addColumn($this->getTable("agent"), "reportingto", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "partnermanager",
    "comment" => "Reporting to",
]);

$this->getConnection()->addColumn($this->getTable("agent"), "orgunitcode", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "reportingto",
    "comment" => "Organisation unit code",
]);

$this->getConnection()->addColumn($this->getTable("agent"), "orgunitlong", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "orgunitcode",
    "comment" => "Organisation unit description",
]);

$this->getConnection()->addColumn($this->getTable("agent"), "location", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "orgunitlong",
    "comment" => "Location",
]);

$this->getConnection()->addColumn($this->getTable("agent"), "workplace", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "location",
    "comment" => "Workplace",
]);

$this->endSetup();
