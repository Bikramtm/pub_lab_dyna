<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Adminhtml_ImportInformationController
 */
class Dyna_Sandbox_Adminhtml_ImportInformationController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Initialize a certain action
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('sandbox/import')->_addBreadcrumb(Mage::helper('adminhtml')->__('Import Overview'), Mage::helper('adminhtml')->__('Import Overview'));
        return $this;
    }

    /**
     * Render the layout on default index action
     */
    public function indexAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Import Overview'));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Trigger edit mode for a record
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('dyna_sandbox/importInformation')->load($id);
        if ($model->getId()) {
            $this->_getSession()->addNotice(
                Mage::helper('index')->__('Please note that this page refreshes at a set interval to update the duration of the import execution.')
            );

            Mage::register('import_information_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('sandbox/import');
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Import Overview'), Mage::helper('adminhtml')->__('Import Overview'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Import Details'), Mage::helper('adminhtml')->__('Import Details'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addLeft($this->getLayout()->createBlock('dyna_sandbox/adminhtml_importInformation_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sandbox')->__('Item does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Displays log contents for a specific import
     */
    public function showLogsAction()
    {
        $executionId = $this->getRequest()->getParam("executionId");
        $category = $this->getRequest()->getParam("category");

        /** @var Dyna_Import_Helper_Data $importHelper */
        $importHelper = Mage::helper('dyna_import');
        $logContent = $importHelper->getLogContent($executionId, $category);

        $this->loadLayout();
        /** @var Dyna_Sandbox_Block_Adminhtml_ImportInformation_Log $block */
        $block = $this->getLayout()->createBlock('dyna_sandbox/adminhtml_importInformation_log')->setTemplate('sandbox/showLog.phtml');
        $block->addData([
            'executionId' => $executionId,
            'category' => $category,
            'logContent' => $logContent,
            'categoriesToFiles' => $importHelper->getCategoriesToFiles()
        ]);

        echo $block->toHtml();
    }

    /**
     * Initialize the addition of a new record
     */
    public function newAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Release'));
        $this->_title($this->__('New Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sandbox/release')->load($id);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('release_data', $model);

        $this->loadLayout();
        $this->_setActiveMenu('sandbox/release');

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Release Manager'), Mage::helper('adminhtml')->__('Release Manager'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Release Description'), Mage::helper('adminhtml')->__('Release Description'));

        $this->_addContent($this->getLayout()->createBlock('sandbox/adminhtml_release_edit'))->_addLeft($this->getLayout()->createBlock('sandbox/adminhtml_release_edit_tabs'));

        $this->renderLayout();

    }

    /**
     * Persist the data in the database
     */
    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();
        if ($post_data) {
            if (isset($post_data['deadline'])) {
                $a = new DateTime(Mage::getSingleton('core/date')->gmtDate());
                $b = new DateTime(Mage::getSingleton('core/date')->date());
                $diff = $a->diff($b);
                $deadlineStr = $post_data['deadline'];
                $deadline = new DateTime($deadlineStr);
                if ($diff->invert) {
                    $deadline->add($diff);
                } else {
                    $deadline->sub($diff);
                }
                $post_data['deadline'] = $deadline->format('Y-m-d H:i:s');
                if(isset($post_data['is_heavy'])){
                    $post_data['is_heavy'] = 1;
                }else {
                    $post_data['is_heavy'] = 0;
                }

                if(isset($post_data['export_media'])){
                    $post_data['export_media'] = 1;
                }else {
                    $post_data['export_media'] = 0;
                }
            }

            try {
                $model = Mage::getModel('sandbox/release')
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam('id'))
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Release was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setReleaseData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setReleaseData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

        }
        $this->_redirect('*/*/');
    }

    /**
     * Deletes a certain provided record
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('sandbox/release');
                $model->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Trigger the mass remove to delete multiple selected records
     */
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel('dyna_sandbox/importInformation');
                $model->setId($id)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item(s) was successfully removed'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Trigger the mass process method for multiple selected records
     */
    public function massProcessAction()
    {
        Mage::register('bypass_replication', true, true); //disable replication while publishing
        try {
            $ids = array_filter(array_map('intval', $this->getRequest()->getPost('ids', array())));
            /** @var Omnius_Sandbox_Model_Mysql4_Release_Collection $collection */
            $collection = Mage::getResourceModel('sandbox/release_collection')
                ->addFieldToFilter('id', array('in' => $ids))
                ->addFieldToFilter('tries', array('lt' => Mage::helper('sandbox')->getTriesLimit()))
                ->load();
            /** @var Omnius_Sandbox_Model_Publisher $publisher */
            $publisher = Mage::getSingleton('sandbox/publisher');
            foreach ($collection->getItems() as $release) { /** @var Omnius_Sandbox_Model_Release $release */
                switch($release->getStatus()) {
                    case Omnius_Sandbox_Model_Release::STATUS_SUCCESS:
                    case Omnius_Sandbox_Model_Release::STATUS_RUNNING:
                        //do nothing
                        break;
                    case Omnius_Sandbox_Model_Release::STATUS_PENDING:
                    case Omnius_Sandbox_Model_Release::STATUS_ERROR:
                        $publisher->publish($release)->save();
                        break;
                    default:
                        break;
                }
            }

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Releases were successfully processed'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'import_overview.csv';
        $grid = $this->getLayout()->createBlock('dyna_sandbox/adminhtml_importInformation_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'import_overview.xml';
        $grid = $this->getLayout()->createBlock('dyna_sandbox/adminhtml_importInformation_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function checkImportExecutionAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('dyna_sandbox/importInformation')->load($id);
        if ($model->getId()) {
            Mage::register('import_information_data', $model);
            return $this->getResponse()->setBody(
                $this->getLayout()->createBlock('dyna_sandbox/adminhtml_importInformation_edit_tab_form')->toHtml()
            );
        }
    }

    public function killImportExecutionAction()
    {
        $id = $this->getRequest()->getParam('id');
        $messages = Mage::helper("dyna_sandbox")->checkRunningImports();
        if ($this->getRequest()->isPost() && count($messages) != 1) {
            /** @var Dyna_Sandbox_Model_ImportInformation $model */
            $model = Mage::getModel('dyna_sandbox/importInformation')->load($id);

            if($model->getId()){
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Killed import with Execution Id ' . $id));
                /** @var Dyna_Sandbox_Helper_Data $importHelper */
                $importHelper = Mage::helper('dyna_sandbox');
                $importHelper->killImport($model);
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('You cannot kill this'));
        }

        //redirect to index
        $this->_redirect('*/*');
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
