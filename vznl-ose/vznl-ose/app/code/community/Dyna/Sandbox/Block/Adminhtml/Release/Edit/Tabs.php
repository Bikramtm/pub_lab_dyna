<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tabs
 */
class Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tabs extends Omnius_Sandbox_Block_Adminhtml_Release_Edit_Tabs
{
    /**
     * Override constructor to customize release grid
     * Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTitle(Mage::helper('sandbox')->__('Release Information'));
    }

    /**
     * Add a custom tab on the release form
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section_tab', array(
            'label' => Mage::helper('sandbox')->__('Release Information'),
            'title' => Mage::helper('sandbox')->__('Release Information'),
            'content' => $this->getLayout()->createBlock('sandbox/adminhtml_release_edit_tab_form')->toHtml(),
        ));
        return Mage_Adminhtml_Block_Widget_Tabs::_beforeToHtml();
    }
}
