<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_Export_Edit_Tab_Form
 */
class Dyna_Sandbox_Block_Adminhtml_Export_Edit_Tab_Form extends Omnius_Sandbox_Block_Adminhtml_Export_Edit_Tab_Form
{
    const PRODUCTS_EXPORT_TYPE = 1;

    protected $_types = array(
        1 => 'Products'
    );

    /**
     * Returns the export types
     * @return array
     */
    protected function getExportTypeOptions()
    {
        return array(
            "" => Mage::helper('sandbox')->__('Please select...'),
            Dyna_Sandbox_Model_Export::PRODUCTS_EXPORT_TYPE => Dyna_Sandbox_Model_Export::$_types[self::PRODUCTS_EXPORT_TYPE],
        );
    }

    /**
     * Get the fields for the export form
     * @return array
     */
    protected function getFields()
    {
        return array(
            'General' => array(
                'deadline' => array(
                    'type' => 'date',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Execution Deadline'),
                        'name' => 'deadline',
                        'time' => true,
                        'format' => 'yyyy-MM-dd HH:mm:ss',
                        'image' => $this->getSkinUrl('images/grid-cal.gif'),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'type' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Export Type'),
                        'name' => 'type',
                        'class' => 'required-entry',
                        'required' => true,
                        'values' => $this->getExportTypeOptions()
                    ),
                ),
                'export_filename' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Export filename (including extension)'),
                        'name' => 'export_filename',
                        'class' => 'required-entry',
                        'required' => true,
                        'after_element_html' => '<span>Ex.: products.xml<br>(Timestamp will be appended before .xml extension)</span>'
                    ),
                ),
            ),
            'Destination' => array(
                'replica' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Target Replica'),
                        'name' => 'replica',
                        'values' => $this->getReplicaOptions(),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
            ),
        );
    }
}
