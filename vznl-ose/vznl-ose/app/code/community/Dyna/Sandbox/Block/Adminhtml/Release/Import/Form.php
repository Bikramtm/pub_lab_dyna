<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_Release_Import_Form
 */
class Dyna_Sandbox_Block_Adminhtml_Release_Import_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl("*/*/import"),
            'method'    => 'post'
        ));
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset("release_import_form", array("legend" => "Import"));

        $fieldset->addField('type[]', 'multiselect', array(
            'label'     => "Type import",
            'name'      => 'type[]',
            'values' => array(
                array('value'=>'mobile',    'label'=>'Mobile'),
                array('value'=>'cable',     'label'=>'Cable'),
                array('value'=>'cable_artifact',     'label'=>'Cable Artifact'),
                array('value'=>'fixed',       'label'=>'Fixed'),
                array('value'=>'bundle',   'label'=>'Bundles'),
                array('value'=>'campaign', 'label'=>'Campaigns'),
                array('value'=>'reference',   'label'=>'Reference Data'),
                array('value'=>'salesid',   'label'=>'SalesId'),
            ),
            'onclick'  => "",
            'onchange' => "",
            'disabled' => false,
            'tabindex' => 1,
            'required' => true
        ));

        return parent::_prepareForm();
    }
}
