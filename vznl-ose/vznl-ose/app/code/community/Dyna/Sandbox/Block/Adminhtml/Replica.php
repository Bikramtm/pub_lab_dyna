<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_Replica
 */
class Dyna_Sandbox_Block_Adminhtml_Replica extends Omnius_Sandbox_Block_Adminhtml_Replica
{
    /**
     * Override the constructor to customzie the grid
     * Dyna_Sandbox_Block_Adminhtml_Replica constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_replica';
        $this->_blockGroup = 'sandbox';
        $this->_headerText = Mage::helper('sandbox')->__('Replica Manager');
        $this->_addButtonLabel = Mage::helper('sandbox')->__('Add New Replica');

        Mage_Adminhtml_Block_Widget_Grid_Container::__construct();
    }
}
