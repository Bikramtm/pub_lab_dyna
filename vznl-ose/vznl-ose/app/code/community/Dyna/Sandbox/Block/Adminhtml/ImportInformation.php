<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_ImportInformation
 */
class Dyna_Sandbox_Block_Adminhtml_ImportInformation extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_importInformation';
        $this->_blockGroup = 'dyna_sandbox';
        $this->_headerText = Mage::helper('sandbox')->__('Import Overview');

        parent::__construct();
        $this->_removeButton('add');
    }
}
