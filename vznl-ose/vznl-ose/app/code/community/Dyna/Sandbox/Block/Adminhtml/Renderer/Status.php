<?php

/**
 * Class Dyna_Sandbox_Block_Adminhtml_Renderer_Status
 */
class Dyna_Sandbox_Block_Adminhtml_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    protected $_status = [
        Dyna_Sandbox_Model_ImportInformation::STATUS_SUCCESS => 'Success',
        Dyna_Sandbox_Model_ImportInformation::STATUS_RUNNING => 'Running',
        Dyna_Sandbox_Model_ImportInformation::STATUS_PENDING => 'Pending',
        Dyna_Sandbox_Model_ImportInformation::STATUS_ERROR => 'Failed',
        Dyna_Sandbox_Model_ImportInformation::STATUS_KILLED => 'Killed by user'
    ];

    public function render(Varien_Object $row)
    {
        $status = $row->getStatus();
        if (isset($this->_status[$status])) {
            return $this->_status[$status];
        }
        return $status;
    }
}