<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form
 */
class Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form extends Omnius_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form
{
    CONST SCHEDULE_TYPE_ONE_TIME = 1;
    CONST SCHEDULE_TYPE_PERIODIC = 2;

    CONST PERIODIC_DAILY = 1;
    CONST PERIODIC_WEEKLY = 2;
    CONST PERIODIC_MONTHLY = 3;

    private $checkboxes = array('is_heavy', 'export_media', 'import_check');

    private $_displayOneTimeExecution = 1;
    private $_displayPeriodicExecution = 0;

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $this->_displayPeriodicExecution = Dyna_Sandbox_Model_Release::getDisplayPeriodicReleaseScheduler();

        if (Mage::getSingleton('adminhtml/session')->getReleaseData()) {
            $data = Mage::getSingleton('adminhtml/session')->getReleaseData();
            Mage::getSingleton('adminhtml/session')->setReleaseData(null);
        } elseif (Mage::registry('release_data')) {
            $data = Mage::registry('release_data')->getData();
        }

        foreach ($this->getFields() as $fieldSetName => $setFields) {
            $fieldSet = $form->addFieldset(sprintf('sandbox_%s_form', strtolower($fieldSetName)), array('legend' => Mage::helper('sandbox')->__($fieldSetName)));
            foreach ($setFields as $name => $fieldDefinition) {
                if(isset($data) && isset($data[$name]) && in_array($name, $this->checkboxes)){
                    $fieldDefinition['config']['checked'] = $data[$name];
                }
                $fieldSet->addField($name, $fieldDefinition['type'], $fieldDefinition['config']);
            }
        }

        $form->setValues($data);

        return $this;
    }


    /**
     * @return array
     */
    protected function getFields()
    {
        $fields = array(
            'Replica' => array(
                'replica' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Target Replica'),
                        'name' => 'replica',
                        'values' => $this->getReplicaOptions(),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'version' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Release Version'),
                        'name' => 'version',
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'deadline' => array(
                    'type' => 'date',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Execution Deadline'),
                        'name' => 'deadline',
                        'time' => true,
                        'format' => 'yyyy-MM-dd HH:mm:ss',
                        'image' => $this->getSkinUrl('images/grid-cal.gif'),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'schedule_type' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Execution date'),
                        'name' => 'schedule_type',
                        'values' => $this->getExecutionDateOptions(),
                    ),
                ),
                'one_time_date' => array(
                    'type' => 'date',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__(''),
                        'name' => 'one_time_date',
                        'time' => true,
                        'format' => 'yyyy-MM-dd HH:mm:ss',
                        'image' => $this->getSkinUrl('images/grid-cal.gif'),
                        'class' => 'required-entry',
                    ),
                ),
                'period' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__(''),
                        'name' => 'period',
                        'values' => $this->getPeriodicOptions(),
                        'class' => 'required-entry',
                    ),
                ),
                'week_day' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Day of the week'),
                        'name' => 'week_day',
                        'values' => $this->getWeekDays(),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'month_day' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Day of month'),
                        'name' => 'month_day',
                        'values' => $this->getMonthDays(),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'time' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Time'),
                        'name' => 'time',
                        'class' => 'required-entry',
                        'title' => 'hh:mm',
                        'required' => true,
                    ),
                ),
                'import_check' => array(
                    'type' => 'checkbox',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Import check'),
                        'name' => 'import_check',
                        'checked' => 0,
                        'after_element_html' => Mage::helper('sandbox')->__(
                            '<br/><span>Only publish if last import ready and successful.</span>'
                        )
                    ),
                ),
            ),
            'Media' => array(
                'export_media' => array(
                    'type' => 'checkbox',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Handle media also?'),
                        'name' => 'export_media',
                        'checked' => 1,
                    ),
                ),
            ),
            'Processing' => array(
                'is_heavy' => array(
                    'type' => 'checkbox',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Complete?'),
                        'name' => 'is_heavy',
                        'checked' => 1,
                        'after_element_html' => Mage::helper('sandbox')->__(
                            '<br/><span>If checked, the heavy indexers and cache building process will be executed also.</span>'
                        )
                    ),
                ),
            )
        );

        return $fields;
    }

    /**
     * Returns scheduler execution date options
     * @return array
     */
    public function getExecutionDateOptions()
    {
        $options = array(
            null => Mage::helper('sandbox')->__('Please select...'),
        );

        if ($this->_displayOneTimeExecution) {
            $options = $options + $this->getExecutionDateOneTimeOptions();
        }
        if ($this->_displayPeriodicExecution ) {
            $options = $options + $this->getExecutionDatePeriodicOptions();
        }
        
        return $options;
    }

    /**
     * Returns scheduler execution date one time options
     * @return array
     */
    public function getExecutionDateOneTimeOptions()
    {
        return array(
            self::SCHEDULE_TYPE_ONE_TIME => Mage::helper('sandbox')->__('One time'),
        );
    }

    /**
     * Returns scheduler execution date periodic options
     * @return array
     */
    public function getExecutionDatePeriodicOptions()
    {
        return array(
            self::SCHEDULE_TYPE_PERIODIC => Mage::helper('sandbox')->__('Periodic'),
        );
    }

    /**
     * Returns the options for all replicas
     * @return array
     */
    public function getPeriodicOptions()
    {
        $options = array(
            null => Mage::helper('sandbox')->__('Please select...'),
            self::PERIODIC_DAILY => Mage::helper('sandbox')->__('Daily'),
            self::PERIODIC_WEEKLY => Mage::helper('sandbox')->__('Weekly'),
            self::PERIODIC_MONTHLY => Mage::helper('sandbox')->__('Monthly'),
        );

        return $options;
    }

    /**
     * Returns the options for all replicas
     * @return array
     */
    public function getWeekDays()
    {
        $options = array(
            null => Mage::helper('sandbox')->__('Please select...'),
            1 => Mage::helper('sandbox')->__('Monday'),
            2 => Mage::helper('sandbox')->__('Tuesday'),
            3 => Mage::helper('sandbox')->__('Wednesday'),
            4 => Mage::helper('sandbox')->__('Thursday'),
            5 => Mage::helper('sandbox')->__('Friday'),
            6 => Mage::helper('sandbox')->__('Sathurday'),
            7 => Mage::helper('sandbox')->__('Sunday'),
        );

        return $options;
    }

    /**
     * Returns the options for all replicas
     * @return array
     */
    protected function getMonthDays()
    {
        $options = [Mage::helper('sandbox')->__('Please select...')];

        for ($i=1; $i<=31; $i++) {
            $options[] = $i;
        }

        return $options;
    }
}
