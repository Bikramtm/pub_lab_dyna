<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_Release
 */
class Dyna_Sandbox_Block_Adminhtml_Release extends Omnius_Sandbox_Block_Adminhtml_Release
{
    protected $_importButtonLabel;
    /**
     * Override the constructor to customize the grid
     * Dyna_Sandbox_Block_Adminhtml_Release constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_release';
        $this->_blockGroup = 'sandbox';
        $this->_headerText = Mage::helper('sandbox')->__('Release Manager');
        $this->_addButtonLabel = Mage::helper('sandbox')->__('Add New Release');
        $this->_importButtonLabel = Mage::helper('sandbox')->__('Import');

        $buttonClass = 'go';
        if(count(Mage::helper("dyna_sandbox")->checkRunningImports()) > 1){
            $buttonClass .= ' disabled';
        }

        $this->_addButton('import', array(
            'label'     => Mage::helper('multimapper')->__('Import'),
            'onclick'   => 'javascript:new Popup(\'' . $this->getImportUrl() . '\', {title:\'' . Mage::helper('multimapper')->__('Import') . '\', width: 650, height:420})',
            'class'     => $buttonClass,
        ));
        Mage_Adminhtml_Block_Widget_Grid_Container::__construct();
    }

    /**
     * @return string
     */
    public function getImportUrl()
    {
        return $this->getUrl('*/*/importView');
    }

}
