<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Exporter
 */
class Dyna_Sandbox_Model_Exporter extends Omnius_Sandbox_Model_Exporter
{
    /** @var Varien_Db_Adapter_Interface */
    protected $_conn;

    /** @var Omnius_Sandbox_Model_Sftp */
    protected $_sftpClient;

    /** @var Omnius_Sandbox_Model_CatalogExporter */
    protected $_catalogExporter;

    /**
     * @param Omnius_Sandbox_Model_Export $export
     * @return Omnius_Sandbox_Model_Export
     */
    public function export(Omnius_Sandbox_Model_Export $export)
    {
        if ($this->_lockExport($export->getId())) {
            $export
                ->setStatus(Omnius_Sandbox_Model_Export::STATUS_RUNNING) //in order to not load the release again
                ->addMessage('Export locked')
                ->save();

            try {
                /** @var Dyna_Sandbox_Model_ExportStrategy_ExportStrategy $strategy */
                if ( ! ($strategy = Mage::getSingleton(sprintf('dyna_sandbox/exportStrategy_%sXmlCatalog', $export->getExportType())))) {
                    throw new Exception('Invalid strategy set on export.');
                }

                $export->addMessage(sprintf('Starting %s export', $export->getExportType()));

                $export
                    ->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                    ->save();

                $export->addMessage(sprintf('Cd to location %s', $export->getExportLocation()));

                if (Dyna_Sandbox_Model_Export::PRODUCTS_TYPE == $export->getExportType()) {
                    $remoteFilePath = $this->_addTimestamp($export->getExportFilename());
                    $command = sprintf(
                        'nohup nice php %s --exportProductsXML --exportId ' . $export->getId() . ' --sftp ' . base64_encode(serialize($export->getSftpArgs())) . ' --path ' . $export->getExportLocation() . DS . ' --filename ' . $remoteFilePath . ' > /dev/null 2>&1 &',
                        'shell/sandbox_ops.php'
                    );

                    @exec($command, $output, $exitCode);
                    $export->addMessage('Writing to export file');
                    $export->save();
                    return $export;

                } else if (Omnius_Sandbox_Model_Export::DYNA_TYPE == $export->getExportType()) {
                    /**
                     * The export for dyna catalog must generate two files, one for belcompany
                     * and one for the retail
                     */
                    $storeIds = array_filter(array_map(function(Mage_Core_Model_Store $store){
                        if (in_array($store->getCode(), array('default', 'belcompany'))) {
                            return $store->getId();
                        }
                        return false;
                    }, Mage::app()->getStores()));

                    $appEmulation = Mage::getSingleton('core/app_emulation');
                    foreach ($storeIds as $storeId) {
                        $exportXml = null;
                        $remoteFilePath = $this->_addTimestamp($export->getExportFilename());
                        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
                        $collection = Mage::getModel('catalog/product')
                            ->getCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('is_deleted', array('neq' => 1))
                            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
                            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                            ->addStoreFilter();

                        $exportXml = $this->getExporter()->export($collection, $strategy, $exportXml);
                        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

                        $export->addMessage(sprintf('Copying export file to %s', $export->getExportPath()));
                        $succeeded = $this->getSftp()->write(
                            $remoteFilePath,
                            str_replace(
                                '<?xml version="1.0"?>',
                                '<?xml version="1.0" encoding="UTF-8"?>',
                                $exportXml->asPrettyXML()
                            )
                        );
                        if ( ! $succeeded) {
                            $this->getSftp()->close();
                            throw new Exception(sprintf('Could not copy export file ("%s").', $remoteFilePath));
                        } else {
                            $export->addMessage('Upload succeeded');
                        }
                    }

                    $this->getSftp()->close();
                    $export->addMessage('Closed SFTP connection');
                    return $export
                        ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                        ->setStatus(Omnius_Sandbox_Model_Export::STATUS_SUCCESS)
                        ->addMessage(sprintf('Export successfully made at %s', $export->getExportPath()), Omnius_Sandbox_Model_Export::MESSAGE_SUCCESS);
                } else {
                    $remoteFilePath = $this->_addTimestamp($export->getExportFilename());

                    $appEmulation = Mage::getSingleton('core/app_emulation');
                    $exportXml = null;
                    foreach ($this->_getStoreIds($export) as $storeId) {
                        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

                        $collection = Mage::getModel('catalog/product')
                            ->getCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('is_deleted', array('neq' => 1))
                            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
                            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                            ->addStoreFilter();

                        $exportXml = $this->getExporter()->export($collection, $strategy, $exportXml);

                        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
                    }

                    $export->addMessage(sprintf('Copying export file to %s', $export->getExportPath()));
                    $succeeded = $this->getSftp()->write(
                        $remoteFilePath,
                        str_replace(
                            '<?xml version="1.0"?>',
                            '<?xml version="1.0" encoding="UTF-8"?>',
                            $exportXml->asPrettyXML()
                        )
                    );

                    if ( ! $succeeded) {
                        $this->getSftp()->close();
                        throw new Exception(sprintf('Could not copy export file ("%s").', $remoteFilePath));
                    } else {
                        $export->addMessage('Upload succeeded');
                    }

                    $this->getSftp()->close();
                    $export->addMessage('Closed SFTP connection');
                    return $export
                        ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                        ->setStatus(Omnius_Sandbox_Model_Export::STATUS_SUCCESS)
                        ->addMessage(sprintf('Export successfully made at %s', $export->getExportPath()), Omnius_Sandbox_Model_Export::MESSAGE_SUCCESS);
                }
            } catch (Exception $e) {
                return $export
                    ->addMessage($e->getMessage(), Omnius_Sandbox_Model_Export::MESSAGE_ERROR)
                    ->setStatus(Omnius_Sandbox_Model_Release::STATUS_ERROR)
                    ->incrementTries()
                    ->save();
            }
        }
        return $export->addMessage(sprintf(
            'Could not acquire lock for export (ID: %s). Already running or successfully finished.',
            $export->getId()
        ), Omnius_Sandbox_Model_Export::MESSAGE_ERROR);
    }
}
