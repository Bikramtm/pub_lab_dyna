<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Publisher
 */


class Dyna_Sandbox_Model_Publisher extends Omnius_Sandbox_Model_Publisher
{
    protected $replicaPHPPath;

    public function publish(Omnius_Sandbox_Model_Release $release)
    {
        /**
         * Check if release has importCheck and the last import has status success (if at least one import was done)
         */
        if($release->getImportCheck())
        {
            /** @var Dyna_Sandbox_Model_ImportInformation $model */
            $model = Mage::getModel('dyna_sandbox/importInformation');
            $lastImport = $model->getCollection()->getLastItem();
            if($lastImport->getId() && $lastImport->getStatus() !== $model::STATUS_SUCCESS){
                return $release->addMessage('Exists an import with status '.$lastImport->getStatus(), $release::MESSAGE_ERROR);
            }
        }

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        if ($this->_lockRelease($release->getId())) {
            $release
                ->setStatus(Omnius_Sandbox_Model_Release::STATUS_RUNNING) //in order to not load the release again
                ->addMessage('Release locked')
                ->save();
            /** @var Omnius_Sandbox_Model_Replica $replica */
            $replica = Mage::getModel('sandbox/replica')->load($release->getReplica());
            if ( ! $replica || ($replica && !$replica->getId())) {
                return $release->addMessage(sprintf('Release references a non-existing replica (ID %s)', $replica->getId()), $release::MESSAGE_ERROR);
            }

            $release
                ->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                ->save();
            try {
                /**
                 * Run indexers before dumping tables
                 * @var Dyna_Warmer_Model_Warmer_Indexing $indexersWarmer
                 */
                if ($indexersWarmer = Mage::getModel('warmer/warmer_indexing')) {
                    $release->addMessage('Starting indexers')->save();
                    Mage::dispatchEvent('warmer_cache_warmCache', ['model' => $indexersWarmer, 'verbose' => false, 'includeHeavy' => $release->getIsHeavy(), 'release' => $release]);
                    $release->addMessage('Indexers finished')->save();
                }

                /**
                 * Migrate tables to the replica
                 */
                $release
                    ->addMessage('Database tables migration started')
                    ->save();
                if ($replica->isRemote()) {
                    $release = $this->externalPublish($release, $replica);
                } else {
                    $publishCommand = $this->_getDbHelper()
                        ->createDumpCommand($this->_getDatabaseConfig(), $release->getTables(), $replica->getMysqlConfig());
                    $release
                        ->addMessage(sprintf('Executed command "%s"', $this->censorCommand($publishCommand)), $release::MESSAGE_INFO);

                    $this->executeAdditionalCommandsBeforeDBDump($release, $replica);

                    $this->_getShell()->exec($publishCommand);

                    $this->executeAdditionalCommandsAfterDBDump($release, $replica);

                    // ToDo: handle missing script - shell/warm_cache.php

                    //if ('success' != ($output = trim($this->_getShell()->exec($this->getCacheWarmerCommand($replica, $release->getIsHeavy()))))) {
                    //    throw new Exception(sprintf('Cache warmer command did not succeed. Output: %s', $output));
                    //}

                    $release
                        ->addMessage('Database tables migration ended')
                        ->save();
                }

                $release = $this->updateCableArtifactPhar($release, $replica, $replica->isRemote());

                /**
                 * Migrate media files to the replica
                 */
                if ($release->getExportMedia()) {
                    if ($replica->isRemote()) {
                        $release
                            ->addMessage('Media files migration started')
                            ->save();
                        $release = $this->externalMediaSync($release, $replica);
                        $release->addMessage('Media files migration ended');
                    } else {
                        $from = DS . trim(Mage::getBaseDir('media'), DS) . DS . 'catalog';
                        $to = DS . trim($replica->getEnvironmentPath(), DS) . DS . join(DS, array('media', 'catalog'));
                        $release
                            ->addMessage('Media catalog files migration started')
                            ->save();
                        $this->_getFileMapper()->mapMedia($from, $to, null, false);
                        $release->addMessage('Media catalog files migration ended');

                        $from = DS . trim(Mage::getBaseDir('media'), DS) . DS . 'bundles';
                        $to = DS . trim($replica->getEnvironmentPath(), DS) . DS . join(DS, array('media', 'bundles'));
                        $release
                            ->addMessage('Media bundles files migration started')
                            ->save();
                        $this->_getFileMapper()->mapMedia($from, $to, null, false);
                        $release->addMessage('Media bundles files migration ended');
                    }
                } else {
                    $release
                        ->addMessage('Media files migration disabled for this release. Skipping.')
                        ->save();
                }

                /**
                 * Update release version
                 * ToDo: if APP_VERSION.php is necessary in this case, write permission must be verified before command execution
                 */
                /*  $versionUpdateCmd = sprintf(
                    "echo '<?php return \"%s\";' > %s",
                    $release->getVersion(),
                    join(DS, array(rtrim($replica->getEnvironmentPath(), DS), 'media', Dyna_Cache_Helper_Data::VERSION_FILE))
                );
                if ($replica->isRemote()) {
                    $this->_getSftp()->getConnection()->exec($versionUpdateCmd);
                } else {
                    $this->_getShell()->exec($versionUpdateCmd);
                }   */

                /**
                 * Clear Magento cache if is not an external publish
                 * ToDo: handle missing script - shell/cache.php
                 */
                /*$clearCacheCmd = sprintf(
                    "%s %s",
                    $replica->getPhpPath(),
                    join(DS, array(rtrim($replica->getEnvironmentPath(), DS), 'shell', 'cache.php'))
                );
                $release->addMessage('Clearing Magento cache', Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);

                if ($replica->isRemote()) {
                    $output = $this->_getSftp()->getConnection()->exec($clearCacheCmd);
                } else {
                    $output = $this->_getShell()->exec($clearCacheCmd);
                }
                if (strlen($output)) {
                    $release->addMessage(sprintf('Could not clear Magento cache. Output: %s', $output), Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);
                } else {
                    $release->addMessage('Successfully cleared Magento cache', Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);
                }*/

                /**
                 * Clear Varnish
                 */
                if ($servers = $replica->getVarnishServers()) {
                    $cmd = join('; ', array_map(function($server) { return sprintf('(curl -X BAN %s > /dev/null 2>&1 &)', $server); }, $servers)) . ';';
                    $release->addMessage(sprintf('Clearing Varnish servers (%s) with command "%s"', join(',', $servers), $cmd), Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);

                    if ($replica->isRemote()) {
                        $output = $this->_getSftp()->getConnection()->exec($cmd);
                    } else {
                        $output = $this->_getShell()->exec($cmd);
                    }
                    if (strlen($output)) {
                        $release->addMessage(sprintf('Could not clear Varnish cache. Output: %s', $output), Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);
                    }
                }
                $release->save();

                // release update should only be executed on the master environment
                if ($replica->isRemote()) {
                    $this->replicaPHPPath = $replica->getPhpPath();
                    Mage::log('Starting update release at: '.date("Y-m-d H:i:s"), null, 'external_release.log', true);
                    $output = $this->_getShell()->exec($this->_getReleaseUpdateCmd($release->getId(), $replica->getId()));
                    Mage::log(var_export($output, true), null, 'external_release.log', true);
                }

                /**
                 * Assure the connection is close
                 */
                if ($replica->isRemote() && $this->_getSftp()->getConnection()) {
                    $this->_getSftp()->close();
                    $release
                        ->addMessage('SFTP connection closed')
                        ->save();
                }

                //remove releases will be finished by a shell script (see "/shell/update_release_status.php")
                if ( ! $replica->isRemote()) {
                    /**
                     * Return release after updating finish timestamp and status
                     */
                    return $release
                        ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                        ->setStatus(Omnius_Sandbox_Model_Release::STATUS_SUCCESS)
                        ->addMessage('Release successfully published', $release::MESSAGE_SUCCESS);
                }

                return $release;

            } catch (Exception $e) {
                return $release
                    ->addMessage($e->getMessage(), $release::MESSAGE_ERROR)
                    ->setStatus(Omnius_Sandbox_Model_Release::STATUS_ERROR)
                    ->incrementTries()
                    ->save();
            }
        }
        return $release->addMessage(
            sprintf(
                'Could not acquire lock for release (ID: %s). Already running or successfully finished.',
                $release->getId()
            ), $release::MESSAGE_ERROR
        );
    }

    public function updateCableArtifactPhar($release, $replica, $remote = null)
    {
        $remoteBasePath = rtrim($replica->getEnvironmentPath(), DS);
        $release
            ->addMessage('Cable artifact migration started')
            ->save();
        if ($remote) {

            $sftpClient = $this->_getSftp();
            $sftpClient->open($replica->getSshConfig());
            $release
                ->addMessage('SFTP connection made')
                ->save();
            if ( ! $this->_getSftp()->cd($remoteBasePath)) {
                throw new Exception(sprintf('Remote environment path (%s) not found', $remoteBasePath));
            }
            $release
                ->addMessage(sprintf('Started moving artifact PHAR to %s', $remoteBasePath))
                ->save();
            if(!$sftpClient->cd('lib')) {
                throw new Exception('Could not find the lib directory');
            }
            $from = Mage::helper('dyna_configurator')->getArtifactPath();
            $to = Dyna_Configurator_Helper_Rules_Cable::ARTIFACT_FILE;
            $successful = $this->_getSftp()->write($to, $from, NET_SFTP_LOCAL_FILE);
        } else {
            $from = Mage::helper('dyna_configurator')->getArtifactPath();
            $to = DS . trim($replica->getEnvironmentPath(), DS) . DS . Dyna_Configurator_Helper_Rules_Cable::ARTIFACT_PATH;

            $successful = copy($from, $to);

        }
        if($successful) {
            $release->addMessage('Cable artifact migration ended');
        } else {
            $release->addMessage(sprintf('Could not copy Cable artifact from %s to %s', $from, $to), Omnius_Sandbox_Model_Release::MESSAGE_ERROR);
        }


        return $release;
    }

    /**
     * Ex: /usr/bin/php /var/www/PROJECT_ROOT/shell/update_release_status.php --release_id 123 --replica_id 124
     *
     * @param $releaseId int
     * @param $replicaId int
     * @return string
     */
    protected function _getReleaseUpdateCmd($releaseId, $replicaId)
    {
        return sprintf(
            '%s %supdate_release_status.php --release_id %s --replica_id "%s"',
            $this->replicaPHPPath,
            rtrim(Mage::getBaseDir(), DS) . DS . 'shell' . DS,
            $releaseId,
            $replicaId
        );
    }
}
