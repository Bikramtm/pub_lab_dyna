<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Model_Export
 */
class Dyna_Sandbox_Model_Export extends Omnius_Sandbox_Model_Export
{
    const PRODUCTS_EXPORT_TYPE = 1;
    const PRODUCTS_TYPE = 'Products';

    public static $_types = array(
        1 => 'Products'
    );

    /**
     * @return string
     */
    public function getExportType()
    {
        if ( ! isset(self::$_types[(int) $this->getType()])) {
            throw new RuntimeException('Export type "%s" is invalid', $this->getType());
        }
        return self::$_types[(int) $this->getType()];
    }
}
