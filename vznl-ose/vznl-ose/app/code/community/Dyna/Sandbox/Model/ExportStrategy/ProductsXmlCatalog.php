<?php

/**
 * Class Dyna_Sandbox_Model_ExportStrategy_ProductsXmlCatalog
 */
class Dyna_Sandbox_Model_ExportStrategy_ProductsXmlCatalog extends Omnius_Sandbox_Model_ExportStrategy_ExportStrategy
{

    /** @var Omnius_Core_Model_SimpleDOM $_data */
    protected $_data;

    /**
     * Init the XML document
     */
    public function start()
    {
    }

    /**
     * @return Omnius_Core_Model_SimpleDOM|void
     */
    public function end()
    {
        return $this->_data;
    }

    /**
     * @return $this
     */
    public function resetData()
    {
        $this->_data = null;
        return $this;
    }

    /**
     * Executed for every item in the collection
     *
     * @param Dyna_Catalog_Model_Product $product
     * @return mixed
     */
    public function processItem($item)
    {
//        $this->_data = $data;
        return null;
    }
}