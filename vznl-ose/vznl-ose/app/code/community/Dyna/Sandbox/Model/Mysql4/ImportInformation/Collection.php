<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Model_Mysql4_ImportInformation_Collection
 */
class Dyna_Sandbox_Model_Mysql4_ImportInformation_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Constructor override
     */
    public function _construct()
    {
        $this->_init('dyna_sandbox/importInformation');
    }
}
