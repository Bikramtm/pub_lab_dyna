<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
// Get all existing indexes to avoid duplicates
$indexList = $this->getConnection()->getIndexList('sales_rule_product_match');

if (!isset($indexList['fk_salesrule_process_contexts'])) {
    $alterSql =
        <<<SQL
        ALTER TABLE sales_rule_product_match CHANGE `process_context` `process_context` SET('1','2','3','4','5','6','7','8','9','10','11') CHARSET utf8 COLLATE utf8_general_ci NULL COMMENT 'Process context', 
        ADD  INDEX `fk_salesrule_process_contexts` (`process_context`); 
SQL;
    $this->getConnection()->query($alterSql);
}

$installer->endSetup();
