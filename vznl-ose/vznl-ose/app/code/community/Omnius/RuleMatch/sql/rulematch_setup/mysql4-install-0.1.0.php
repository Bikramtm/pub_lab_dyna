<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
/**
 * Create table 'sales_rule_product_match'
 */
if (!$installer->getConnection()->isTableExists($installer->getTable('rulematch/rulematch'))) {
    /** @var Varien_Db_Ddl_Table $table */
    $table = $installer->getConnection()
        ->newTable($installer->getTable('rulematch/rulematch'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'ID')
        ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
        ))
        ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('value', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('addon_type', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('attribute_set_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('network', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('camera', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('category_ids', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('identifier_commitment_months', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('memory', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('product_visibility_strategy', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ))
        ->addColumn('identifier_subscr_type', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true,
        ))
        ->addColumn('brand', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ))
        ->addColumn('channel', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ))
        ->addColumn('dealer', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ))
        ->addColumn('customer_segment', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('value', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('campaign', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('lifecycle', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ))
        ->addColumn('base_subtotal', Varien_Db_Ddl_Table::TYPE_DECIMAL, null, array(
            'scale' => 4,
            'precision' => 12,
            'default' => null,
            'nullable' => true,
        ))
        ->addColumn('total_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('weight', Varien_Db_Ddl_Table::TYPE_DECIMAL, null, array(
            'scale' => 4,
            'precision' => 12,
            'default' => null,
            'nullable' => true,
        ))
        ->addColumn('payment_method', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ))
        ->addColumn('shipping_method', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ))
        ->addColumn('postcode', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ))
        ->addColumn('region', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ))
        ->addColumn('region_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('country_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('quote_item_price', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ))
        ->addColumn('quote_item_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => true,
        ))
        ->addColumn('quote_item_row_total', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ));

    $table->addForeignKey('fk_salesrule_rule_id', 'rule_id', Mage::getSingleton('core/resource')->getTableName('salesrule/rule'), 'rule_id', $table::ACTION_CASCADE, $table::ACTION_CASCADE);

    $table->setComment('Sales Rules Product Match')
        ->setOption('charset', 'utf8');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
