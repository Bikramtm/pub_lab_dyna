<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_RuleMatch_Model_Mysql4_Rulematch
 */
class Omnius_RuleMatch_Model_Mysql4_Rulematch extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('rulematch/rulematch', 'entity_id');
    }
}