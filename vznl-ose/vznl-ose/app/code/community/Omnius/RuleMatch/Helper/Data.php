<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_RuleMatch_Helper_Data
 */
class Omnius_RuleMatch_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_quoteItemAttributes = array(
        'sku'
    );

    protected $_packageAttributes = array(
        'lifecycle'
    );

    protected $_productAttributes = array(
        'product_visibility_strategy',
        'identifier_commitment_months',
        'identifier_subscr_type',
        'attribute_set_id'
    );

    protected $_addressAttributes = array(
    );

    protected $_customerAttributes = array(
        'customer_segment',
        'value',
        'campaign',
    );

    /**
     * @var Omnius_Package_Model_Package
     */
    private $_activePackage = null;

    protected $_dealerAttributes = array(
        'dealer',
    );

    protected $_mappings = array(
        'quote_item' => array(
            'quote_item_price' => 'row_total',
            'quote_item_qty' => 'qty',
            'quote_item_row_total' => 'getBaseRowTotal',
        ),
        'package' => array(
            'lifecycle'  => 'sale_type'
        ),
        'product' => array(
            'channel' => 'prodspecs_hawaii_channel',
            'memory' => 'prodspecs_opslagcapaciteit',
            'camera' => 'prodspecs_camera',
            'network' => 'identifier_subscr_brand',
            'category_ids' => 'getAvailableInCategories'
        ),
        'address' => array(

        ),
        'customer' => array(
            'customer_segment' => 'value_segment',
            'value' => 'customer_value',
            'campaign' => 'getCampaigns',
        ),
        'dealer' => array(
            'dealer' => 'vf_dealer_code',
            'dealer_group' => 'group_id',
        ),
    );

    /** @var Varien_Db_Adapter_Interface */
    protected $_adapter;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /** @var  Omnius_Checkout_Model_Sales_Quote */
    private $_quote;

    /** @var array List of attributes that can have "is one of" conditions */
    protected $isOneOffAttributes = [
        'dealer',
        'dealer_group',
    ];

    /**
     * @param $activePackage
     * @return $this
     */
    public function setActivePackage($activePackage)
    {
        $this->_activePackage = $activePackage;

        return $this;
    }

    /**
     * @return Dyna_Package_Model_Package
     */
    public function getActivePackage(){
        return $this->_activePackage;
    }

    /**
     * Retrieves available sales rules from the index table based on the current cart attributes.
     * @param   string $coupon
     * @param   bool $asCollection
     * @return  array
     */
    public function getApplicableRules($asCollection = false, $coupon = '')
    {
        /** @var Omnius_RuleMatch_Model_Mysql4_Rulematch_Collection $collection */
        $collection = Mage::getResourceModel('rulematch/rulematch_collection');

        $attrValues = $this->_getAttributeValues();
        ksort($attrValues);
        $cacheKey = sprintf('sales_rules_%s_%s', md5(serialize($attrValues)), $coupon);
        if ($result = $this->getCache()->load($cacheKey)) {
            return unserialize($result);
        } else {
            foreach ($attrValues as $attrCode => $values) {
                $filters = array();
                $needsNull = true;
                if (in_array($attrCode, $this->isOneOffAttributes)) {
                    $collection->addFieldToFilter($attrCode, array(array('finset' => $values), array('null' => true)));
                } else {
                    foreach ($values as $value) {
                        $needsNull = ($value === null) ? false : true;
                        if ($value instanceof Varien_Data_Collection) {
                            array_push($filters, array('in' => $value->getAllIds()));
                        } elseif ($value instanceof Varien_Object) {
                            array_push($filters, array('eq' => $value->getId()));
                        } elseif (is_array($value)) {
                            array_push($filters, array('in' => $value));
                        } else {
                            array_push($filters, (!$needsNull) ? array('null' => true) : array('eq' => $value));
                        }
                    }
                    if ($needsNull) {
                        array_push($filters, array('null' => true));
                    }

                    $collection->addFieldToFilter(array_fill(0, count($filters), $attrCode), $filters);
                }
            }

            $select = $collection->getSelect()
                ->reset(Varien_Db_Select::COLUMNS)
                ->columns(array('rule_id'))
                ->distinct(true)
                ->assemble();

            try {
                unset($collection);
                $applicableRuleIds = $this->_getAdapter()->fetchCol($select);
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                Mage::log('SQL FAILED: ' . $select . "\n", null, 'rule_match_results.log', true);
                $rules = Mage::getResourceModel('salesrule/rule_collection');
                return $asCollection
                    ? $rules
                        ->setValidationFilter(Mage::app()->getWebsite()->getId(), Mage::getSingleton('customer/session')->getCustomerGroupId(), $coupon)
                        ->load()
                    : $rules
                        ->setValidationFilter(Mage::app()->getWebsite()->getId(), Mage::getSingleton('customer/session')->getCustomerGroupId(), $coupon)
                        ->load()
                        ->getColumnValues('rule_id');
            }

            $result = $asCollection
                ? Mage::getResourceModel('salesrule/rule_collection')
                    ->setValidationFilter(Mage::app()->getWebsite()->getId(), Mage::getSingleton('customer/session')->getCustomerGroupId(), $coupon)
                    ->addFieldToFilter('rule_id', array('in' => $applicableRuleIds))
                : $applicableRuleIds;

            if ($result instanceof Varien_Data_Collection) {
                //Serialization of 'Mage_Core_Model_Config_Element' is not allowed
                $tmp = new Varien_Data_Collection();
                foreach ($result->getItems() as $item) {
                    $tmp->addItem($item);
                }
                $result = $tmp;
            }

            $this->getCache()->save(serialize($result), $cacheKey, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * Parses attributes for the indexer conditions.
     * @return array
     */
    protected function _getAttributeValues()
    {
        $attrValues = array();
        foreach ($this->_getQuote()->getAllItems() as $quoteItem) {
            foreach ($this->_quoteItemAttributes as $attrCode) {
                if ($quoteItem->getPackageId() == $this->getActivePackage()->getPackageId()) {
                    $attrValues[$attrCode][] = $this->getValue($quoteItem, $attrCode, 'quote_item');
                }
            }
        }

        foreach ($this->_packageAttributes as $attrCode) {
            $attrValues[$attrCode][] = $this->getValue($this->getActivePackage(), $attrCode, 'package');
        }

        $address = $this->_getAddress();
        foreach ($this->_addressAttributes as $attrCode) {
            $attrValues[$attrCode][] = $this->getValue($address, $attrCode, 'address');
        }

        $customer = $this->_getCustomer();
        foreach ($this->_customerAttributes as $attrCode) {
            $attrValues[$attrCode][] = $this->getValue($customer, $attrCode, 'customer');
        }

        $dealer = $this->_getDealer();
        foreach ($this->_dealerAttributes as $attrCode) {
            $attrValues[$attrCode][] = $this->getValue($dealer, $attrCode, 'dealer');
        }

        foreach ($attrValues as $attrCode => $values) {
            $attrValues[$attrCode] = array_unique($values, SORT_REGULAR);
        }

        return $attrValues;
    }

    /**
     * Extracts attribute vales frum the current cart. 
     * @param Varien_Object $object
     * @param $attrCode string
     * @param $namespace string
     * @return mixed
     */
    protected function getValue(Varien_Object $object, $attrCode, $namespace)
    {
        if (isset($this->_mappings[$namespace][$attrCode])) {
            $proxyProp = $this->_mappings[$namespace][$attrCode];
            if (method_exists($object, $proxyProp)) {
                return call_user_func(array($object, $proxyProp));
            } else {
                return $object->getData($proxyProp);
            }
        } else {
            return $object->getData($attrCode);
        }
    }

    /**
     * @return Mage_Sales_Model_Quote_Address
     */
    protected function _getAddress()
    {
        if ($this->_getQuote()->isVirtual()) {
            return $this->_getQuote()->getBillingAddress();
        } else {
            return $this->_getQuote()->getShippingAddress();
        }
    }

    /**
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    protected function _getQuote()
    {
        if($this->_quote == null){
            if (Mage::registry('collect_totals_quote') == null){
                $this->_quote = $this->_getCart()->getQuote(true);
            }else {
                $this->_quote = Mage::registry('collect_totals_quote');
            }
        }

        return $this->_quote;
    }

    /**
     * @return Mage_Customer_Model_Customer
     */
    protected function _getCustomer()
    {
        return $this->_getQuote()->getCustomer();
    }

    /**
     * @return Dyna_Agent_Model_Dealer
     */
    protected function _getDealer()
    {
        $session = Mage::getSingleton('customer/session');
        $agent = $session->getAgent();
        if($agent) {
            return $agent->getDealer();
        }
        return new Varien_Object(); //fallback
    }

    /**
     * @return Varien_Db_Adapter_Interface
     */
    protected function _getAdapter()
    {
        if ( ! $this->_adapter) {
            $this->_adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        }
        return $this->_adapter;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * @return $this
     */
    public function reset(){
        $this->_quote = null;
        return $this;
    }
}
