<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Model_Mysql4_Field
 */
class Omnius_Field_Model_Mysql4_Field extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Omnius_Field_Model_Mysql4_Field constructor
     */
    protected function _construct()
    {
        $this->_init('field/field', 'entity_id');
    }
}