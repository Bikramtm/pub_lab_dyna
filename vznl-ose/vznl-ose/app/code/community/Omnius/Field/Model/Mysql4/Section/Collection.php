<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Model_Mysql4_Section_Collection
 */
class Omnius_Field_Model_Mysql4_Section_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Omnius_Field_Model_Mysql4_Section_Collection constructor
     */
    public function _construct()
    {
        $this->_init('field/section');
    }

    /**
     * Delete item from collection
     * @return $this
     */
    public function delete()
    {
        foreach ($this->getItems() as $item) {
            $item->delete();
            unset($this->_items[$item->getId()]);
        }
        return $this;
    }
}
