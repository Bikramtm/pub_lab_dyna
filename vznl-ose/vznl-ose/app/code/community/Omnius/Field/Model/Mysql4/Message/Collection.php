<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Model_Mysql4_Message_Collection
 */
class Omnius_Field_Model_Mysql4_Message_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Omnius_Field_Model_Mysql4_Message_Collection constructor
     */
    public function _construct()
    {
        $this->_init('field/message');
    }
}
