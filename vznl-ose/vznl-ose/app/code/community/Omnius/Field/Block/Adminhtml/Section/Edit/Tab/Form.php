<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Section_Edit_Tab_Form
 */
class Omnius_Field_Block_Adminhtml_Section_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Define add/edit form fields in backend for fields sections
     * @return mixed
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("section_form", array("legend" => Mage::helper("field")->__("Section information")));

        $fieldset->addField("name", "text", array(
            "label" => Mage::helper("field")->__("Section Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "name",
        ));

        if (Mage::getSingleton("adminhtml/session")->getSectionData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getSectionData());
            Mage::getSingleton("adminhtml/session")->setSectionData(null);
        } elseif (Mage::registry("section_data")) {
            $form->setValues(Mage::registry("section_data")->getData());
        }

        return parent::_prepareForm();
    }
}
