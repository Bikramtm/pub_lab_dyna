<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Message_Edit
 */
class Omnius_Field_Block_Adminhtml_Message_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Omnius_Field_Block_Adminhtml_Message_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = "entity_id";
        $this->_blockGroup = "field";
        $this->_controller = "adminhtml_message";
        $this->_updateButton("save", "label", Mage::helper("field")->__("Save Item"));
        $this->_updateButton("delete", "label", Mage::helper("field")->__("Delete Item"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("field")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    /**
     * Set message add/edit header text according to current action
     * @return mixed
     */
    public function getHeaderText()
    {
        if (Mage::registry("message_data") && Mage::registry("message_data")->getId()) {
            return Mage::helper("field")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("message_data")->getId()));
        } else {
            return Mage::helper("field")->__("Add Item");
        }
    }
}