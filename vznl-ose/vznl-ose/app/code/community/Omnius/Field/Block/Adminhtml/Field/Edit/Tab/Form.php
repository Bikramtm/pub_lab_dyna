<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Field_Edit_Tab_Form
 */
class Omnius_Field_Block_Adminhtml_Field_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Define add/edit form fields in backend
     * @return mixed
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("field_form", array("legend" => Mage::helper("field")->__("Field information")));

        $fieldset->addField("name", "text", array(
            "label" => Mage::helper("field")->__("Field Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "name",
        ));

        $fieldset->addField("default_value", "text", array(
            "label" => Mage::helper("field")->__("Field Default Value"),
            "class" => "required-entry",
            "required" => true,
            "name" => "default_value",
        ));

        $fieldset->addField('type', 'select', array(
            'label' => Mage::helper('field')->__('Field Type'),
            'values' => Omnius_Field_Block_Adminhtml_Field_Grid::getOptionFieldTypes(),
            'name' => 'type',
            "class" => "required-entry",
            "required" => true,
        ));

        $fieldset->addField("comment", "textarea", array(
            "label" => Mage::helper("field")->__("Field Comment"),
            /*"class" => "",*/
            "required" => false,
            "name" => "comment",
        ));
      
        $fieldset->addField('section_id', 'select', array(
            'label' => Mage::helper('field')->__('Section'),
            'values' => Omnius_Field_Block_Adminhtml_Field_Grid::getOptionSections(),
            'name' => 'section_id',
            "class" => "required-entry",
            "required" => true,
        ));

        $fieldset->addField('store_id', 'select', array(
            'label' => Mage::helper('field')->__('Store'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
            'name' => 'store_id',
            "class" => "required-entry",
            "required" => true,
        ));
        
        if (Mage::getSingleton("adminhtml/session")->getFieldData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getFieldData());
            Mage::getSingleton("adminhtml/session")->setFieldData(null);
        } elseif (Mage::registry("field_data")) {
            $form->setValues(Mage::registry("field_data")->getData());
        }
        
        return parent::_prepareForm();
    }
}
