<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Field_Edit_Tabs
 */
class Omnius_Field_Block_Adminhtml_Field_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Omnius_Field_Block_Adminhtml_Field_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId("field_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("field")->__("Field Information"));
    }

    /**
     * Define left sidebar tab text
     * @return mixed
     */
    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("field")->__("Field Information"),
            "title" => Mage::helper("field")->__("Field Information"),
            "content" => $this->getLayout()->createBlock("field/adminhtml_field_edit_tab_form")->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}
