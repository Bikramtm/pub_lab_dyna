<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Adminhtml_MessageController
 */
class Omnius_Field_Adminhtml_MessageController extends Mage_Adminhtml_Controller_Action
{
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("field/message")->_addBreadcrumb(Mage::helper("adminhtml")->__("Service Error Messages"), Mage::helper("adminhtml")->__("Service Error Messages"));
        return $this;
    }

    /**
     * Admin fields messages grid display action
     */
    public function indexAction()
    {
        $this->_title($this->__("Service Error Messages"));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     *  Edit field message admin action
     */
    public function editAction()
    {
        $this->_title($this->__("Edit Error Messages"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("field/message")->load($id);

        if ($model->getId()) {
            Mage::register("message_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("field/message");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Service Error Messages"), Mage::helper("adminhtml")->__("Service Error Messages"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("field/adminhtml_message_edit"))->_addLeft($this->getLayout()->createBlock("field/adminhtml_message_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("field")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    /**
     * Add new field message admin action
     */
    public function newAction()
    {
        $this->_title($this->__("New Message"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("field/message")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("message_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("field/message");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Service Error Messages"), Mage::helper("adminhtml")->__("Service Error Messages"));


        $this->_addContent($this->getLayout()->createBlock("field/adminhtml_message_edit"))->_addLeft($this->getLayout()->createBlock("field/adminhtml_message_edit_tabs"));

        $this->renderLayout();
    }

    /**
     * Save field message admin action
     */
    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $currentDefaultValue = Mage::getModel("field/message")->load($this->getRequest()->getParam("id"));

                $model = Mage::getModel("field/message")
                    ->addData($postData)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Message was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setAgentData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));

                    return;
                }
            } catch (Zend_Db_Statement_Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError('A message with the same name already exists');
                $this->getRedirectUrl($currentDefaultValue);

                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->getRedirectUrl($currentDefaultValue);

                return;
            }
        }
        $this->_redirect("*/*/");
    }

    /**
     * Delete field message admin action
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("field/message");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Message was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    /**
     * Remove multiple fields.
     * Called from the grid mass action.
     */
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('entity_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("field/message");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export field messages grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'messages.csv';
        $grid = $this->getLayout()->createBlock('field/adminhtml_message_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export field messages grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'messages.xml';
        $grid = $this->getLayout()->createBlock('field/adminhtml_message_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * @param $currentDefaultValue
     */
    protected function getRedirectUrl($currentDefaultValue)
    {
        Mage::getSingleton("adminhtml/session")->setMessageData($this->getRequest()->getPost());
        if ($currentDefaultValue->getId()) {
            $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
        } else {
            $this->_redirect("*/*/new");
        }
    }
}
