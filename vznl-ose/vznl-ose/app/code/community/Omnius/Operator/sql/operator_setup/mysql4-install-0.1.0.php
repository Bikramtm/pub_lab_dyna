<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
if (!$installer->getConnection()->isTableExists($installer->getTable('operator/operator'))) {
    /** Operator table */
    $operator = new Varien_Db_Ddl_Table();
    $operator->setName('operator');
    $operator->addColumn(
        'operator_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    );

    $operator->addColumn(
        'operator',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        100
    );

    $operator->addColumn(
        'service_provider',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        100
    );

    $operator->addColumn(
        'sim_range',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    );

    $operator->setOption('type', 'InnoDB');
    $operator->setOption('charset', 'utf8');
    $this->getConnection()->createTable($operator);
    /** End Operator Table */
}
$installer->endSetup();
