<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Model_ServiceProvider extends  Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('operator/serviceProvider');
    }
}
