<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Default Operators Administration Controller
 */

class Omnius_Operator_Adminhtml_OperatorController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('system/operator')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Operator'), Mage::helper('adminhtml')->__('Operator'));
        return $this;
    }

    /**
     * Display the admin grid (table) with all the welcome messages
     */
    public function indexAction() {
        $this->_initAction()->renderLayout();
    }

    /**
     * Gather data and generate edit form
     *
     */
    public function editAction() {
        $id     = $this->getRequest()->getParam('operator_id');
        $model  = Mage::getModel('operator/operator')->load($id);

        if ($model->getOperatorId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('operator_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('system/operator');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Operators manager'), Mage::helper('adminhtml')->__('Operators manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('operator/adminhtml_operator_edit'))
                ->_addLeft($this->getLayout()->createBlock('operator/adminhtml_operator_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('operator')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Admin add a new Announcement, forwards to edit action
     */
    public function newAction() {
        $this->_forward('edit');
    }

    /**
     * Perform the validation and saving of the announcements code
     *
     */
    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {

            $model = Mage::getModel('operator/operator');
            $model->setData($data)
                ->setOperatorId($this->getRequest()->getParam('operator_id'));

            //building networkOperatorId
            $operatorId = $this->getRequest()->getParam('network_operator_id');

            //building serviceProviderId
            $providerId = $this->getRequest()->getParam('service_provider_id');

            try {
                $model->setNetworkOperatorId($operatorId);
                $model->setServiceProviderId($providerId);
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('operator')->__('The operator combination was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('operator_id' => $model->getOperatorId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                $this->throwErrror($e->getMessage(), $data);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('operator')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     */
    private function throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', array('operator_id' => $this->getRequest()->getParam('operator_id')));
    }

    /**
     * Method that deleted a operator code
     *
     */
    public function deleteAction() {
        if( $this->getRequest()->getParam('operator_id') > 0 ) {
            try {
                $model = Mage::getModel('operator/operator');

                $model->setOperatorId($this->getRequest()->getParam('operator_id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('operator_id' => $this->getRequest()->getParam('operator_id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Method to allow bulk deletion of announcements codes
     *
     */
    public function massDeleteAction() {
        $announcementsIds = $this->getRequest()->getParam('operator');
        if(!is_array($announcementsIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($announcementsIds as $announcementsId) {
                    $announcements = Mage::getModel('operator/operator')->load($announcementsId);
                    $announcements->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($announcementsIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
