<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Adminhtml_FixedController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('system/operator')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Service providers'), Mage::helper('adminhtml')->__('Service providers'));
        return $this;
    }

    /**
     * Display the admin grid (table) with all the welcome messages
     */
    public function indexAction() {
        $this->_initAction()->renderLayout();
    }

    /**
     * Gather data and generate edit form
     *
     */
    public function editAction() {
        $id     = $this->getRequest()->getParam('entity_id');
        $model  = Mage::getModel('operator/serviceProvider')->load($id);

        if ($model->getEntityId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('operator_service_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('system/operator');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Fixed-line service provider'), Mage::helper('adminhtml')->__('Fixed-line service provider'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Update provider'), Mage::helper('adminhtml')->__('Update provider'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('operator/adminhtml_fixed_edit'))
                ->_addLeft($this->getLayout()->createBlock('operator/adminhtml_fixed_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('operator')->__('Fixed-line service provider does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Admin add a new Announcement, forwards to edit action
     */
    public function newAction() {
        $this->_forward('edit');
    }

    /**
     * Perform the validation and saving of the announcements code
     *
     */
    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('operator/serviceProvider');
            $model->setData($data)
                ->setEntityId($this->getRequest()->getParam('entity_id'));
            $model->setFixedLine(1);
            try {
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('operator')->__('The service provider was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('entity_id' => $model->getEntityId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                $this->throwErrror($e->getMessage(), $data);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('operator')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     */
    private function throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', array('entity_id' => $this->getRequest()->getParam('entity_id')));
    }

    /**
     * Method that deleted a operator code
     *
     */
    public function deleteAction() {
        if( $this->getRequest()->getParam('entity_id') > 0 ) {
            try {
                $model = Mage::getModel('operator/serviceProvider');

                $model->setEntityId($this->getRequest()->getParam('entity_id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('entity_id' => $this->getRequest()->getParam('entity_id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Method to allow bulk deletion of announcements codes
     *
     */
    public function massDeleteAction() {
        $announcementsIds = $this->getRequest()->getParam('operator');
        if(!is_array($announcementsIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($announcementsIds as $announcementsId) {
                    $announcements = Mage::getModel('operator/serviceProvider')->load($announcementsId);
                    $announcements->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($announcementsIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
