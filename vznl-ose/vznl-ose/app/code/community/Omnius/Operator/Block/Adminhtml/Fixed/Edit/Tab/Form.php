<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Block_Adminhtml_Fixed_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $data = Mage::registry('operator_service_data');
        $fieldset = $form->addFieldset('service_form', array('legend'=>Mage::helper('operator')->__('Fixed-line service provider')));

        $fieldset->addField('operator', 'text', array(
            'label'     => Mage::helper('operator')->__('Fixed-line service provider code'),
            'name'      => 'code',
            'value'    => $data->getCode(),
        ));

        $fieldset->addField('service_provider', 'text', array(
            'label'     => Mage::helper('operator')->__('Fixed-line service provider name'),
            'name'      => 'name',
            'value'    => $data->getName(),
        ));

        return parent::_prepareForm();
    }

}
