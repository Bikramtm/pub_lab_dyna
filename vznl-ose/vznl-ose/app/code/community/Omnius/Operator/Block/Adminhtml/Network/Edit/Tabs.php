<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Block_Adminhtml_Network_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('network_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('operator')->__('Item Information'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('operator')->__('Item Information'),
            'title'     => Mage::helper('operator')->__('Item Information'),
            'content'   => $this->getLayout()->createBlock('operator/adminhtml_network_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
