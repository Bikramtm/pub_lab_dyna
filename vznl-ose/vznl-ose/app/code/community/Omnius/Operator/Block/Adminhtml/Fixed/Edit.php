<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Block_Adminhtml_Fixed_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'operator';
        $this->_controller = 'adminhtml_fixed';

        $this->_updateButton('save', 'label', Mage::helper('operator')->__('Save fixed-line provider'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
        $objId = $this->getRequest()->getParam($this->_objectId);
        if (! empty($objId)) {
            $this->_addButton('delete', array(
                'label'     => Mage::helper('adminhtml')->__('Delete'),
                'class'     => 'delete',
                'onclick'   => 'deleteConfirm(\''. Mage::helper('adminhtml')->__('Are you sure you want to do this?')
                    .'\', \'' . $this->getDeleteUrl() . '\')',
            ));
        }

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('messages_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'messages_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'messages_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if( Mage::registry('operator_service_data') && Mage::registry('operator_service_data')->getEntityId() ) {
            return Mage::helper('operator')->__("Edit fixed-line service provider %s", $this->htmlEscape(Mage::registry('operator_service_data')->getName()));
        } else {
            return Mage::helper('operator')->__('Add fixed-line service provider');
        }
    }
}
