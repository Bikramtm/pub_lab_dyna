<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Block_Adminhtml_Fixed extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_fixed';
        $this->_blockGroup = 'operator';
        $this->_headerText = Mage::helper('operator')->__('Fixed line providers');
        $this->_addButtonLabel = Mage::helper('operator')->__('Add fixed line provider');
        parent::__construct();
    }
}
