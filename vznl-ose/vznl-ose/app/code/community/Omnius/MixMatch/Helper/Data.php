<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class MixMatch Helper
 */
class Omnius_MixMatch_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Return the filename of last imported document by website id
     *
     * @param $websiteId
     * @return mixed
     */
    public function getLastImportedFileName($websiteId)
    {
        return 'var' . DS . 'export';
    }
}
