<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * MixMatch data Import default front controller
 */
class Omnius_MixMatch_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Import data action
     *
     */
    public function importAction()
    {
        $a = Mage::getModel('omnius_mixmatch/price');
        echo $a->getCollection()->getSize();
    }
}
