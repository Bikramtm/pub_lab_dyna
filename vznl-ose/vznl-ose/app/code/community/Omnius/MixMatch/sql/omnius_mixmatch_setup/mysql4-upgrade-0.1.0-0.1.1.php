<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$table = $this->getTable('catalog_mixmatch');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$fields = array(
    'website_id',
    'device_sku',
    'subscription_sku',
);

$isValid = true;
foreach ($fields as $field) {
    if (!$connection->tableColumnExists($table, $field)) {
        $isValid = false;
    }
}

if ($isValid) {
    $connection->addIndex($table, Mage::getSingleton('core/resource')->getIdxName($table, $fields), $fields);
}


$installer->endSetup();
