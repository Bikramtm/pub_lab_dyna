<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Omnius
 * @package     Omniu_MixMatch
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table
 */
if (!$installer->tableExists('catalog_mixmatch')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('omnius_mixmatch/mixmatch'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('device_sku', Varien_Db_Ddl_Table::TYPE_TEXT, 64, array(
        ), 'Device SKU')
        ->addColumn('subscription_sku', Varien_Db_Ddl_Table::TYPE_TEXT, 64, array(
        ), 'Subscription SKU')
        ->addColumn('price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
            'nullable'  => true,
        ), 'Purchase Price')
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, 5, array(
            'unsigned' => true,
            'nullable'  => false,
        ), 'Store')
        ->addIndex($installer->getIdxName('omnius_mixmatch/mixmatch', array('device_sku', 'subscription_sku', 'website_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('device_sku', 'subscription_sku', 'website_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('MixMatch Prices Table');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
