<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->run('DELETE FROM `catalog_mixmatch` WHERE `website_id` NOT IN (SELECT `website_id` FROM `core_website`)');

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();

$row = $connection->fetchRow("SELECT * FROM information_schema.TABLE_CONSTRAINTS 
WHERE information_schema.TABLE_CONSTRAINTS.CONSTRAINT_TYPE = 'FOREIGN KEY' 
AND information_schema.TABLE_CONSTRAINTS.CONSTRAINT_NAME = 'FK_MIXMATCH_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID'
AND information_schema.TABLE_CONSTRAINTS.TABLE_SCHEMA = DATABASE()
AND information_schema.TABLE_CONSTRAINTS.TABLE_NAME = 'catalog_mixmatch'");

if (empty($row)) {
    $installer->run('
      ALTER TABLE `catalog_mixmatch` ADD CONSTRAINT FK_MIXMATCH_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID
      FOREIGN KEY (`website_id`) REFERENCES `core_website`(`website_id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;');
}

$installer->endSetup();
