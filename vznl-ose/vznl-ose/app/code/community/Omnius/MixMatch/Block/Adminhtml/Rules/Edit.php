<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_MixMatch_Block_Adminhtml_Rules_Edit
 */
class Omnius_MixMatch_Block_Adminhtml_Rules_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = "entity_id";
        $this->_blockGroup = "omnius_mixmatch";
        $this->_controller = "adminhtml_rules";
        $this->_updateButton("save", "label", Mage::helper("omnius_mixmatch")->__("Save Item"));
        $this->_updateButton("delete", "label", Mage::helper("omnius_mixmatch")->__("Delete Item"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("omnius_mixmatch")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
                            function saveAndContinueEdit(){
                                editForm.submit($('edit_form').action+'back/edit/');
                            }
                        ";
    }

    /**
     * Get header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry("omnius_mixmatch_data") && Mage::registry("omnius_mixmatch_data")->getId()) {
            return Mage::helper("omnius_mixmatch")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("omnius_mixmatch_data")->getId()));
        } else {
            return Mage::helper("omnius_mixmatch")->__("Add Item");
        }
    }
}