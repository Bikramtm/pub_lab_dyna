<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_MixMatch_Block_Adminhtml_Rules_Edit_Tab_Form
 */
class Omnius_MixMatch_Block_Adminhtml_Rules_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("omnius_mixmatch_form", array("legend" => Mage::helper("omnius_mixmatch")->__("Item information")));

        $fieldset->addField("device_sku", "text", array(
            "label" => Mage::helper("omnius_mixmatch")->__("Device SKU"),
            "class" => "required-entry",
            "required" => true,
            "name" => "device_sku",
        ));

        $fieldset->addField("subscription_sku", "text", array(
            "label" => Mage::helper("omnius_mixmatch")->__("Subscription SKU"),
            "class" => "required-entry",
            "required" => true,
            "name" => "subscription_sku",
        ));
        $fieldset->addField("device_subscription_sku", "text", array(
            "label" => Mage::helper("omnius_mixmatch")->__("Device Subscription SKU"),
            "class" => "",
            "required" => false,
            "name" => "device_subscription_sku",
        ));
        $fieldset->addField("price", "text", array(
            "label" => Mage::helper("omnius_mixmatch")->__("Price"),
            "required" => false,
            "name" => "price",
        ));

        $fieldset->addField('website_id', 'multiselect', array(
            'label' => Mage::helper('omnius_mixmatch')->__('Website'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(),
            'name' => 'website_id',
            "class" => "required-entry",
            "required" => true,
        ));
        
        if (Mage::getSingleton("adminhtml/session")->getOmniusMixMatchData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getOmniusMixMatchData());
            Mage::getSingleton("adminhtml/session")->setOmniusMixMatchData(null);
        } elseif (Mage::registry("omnius_mixmatch_data")) {
            $form->setValues(Mage::registry("omnius_mixmatch_data")->getData());
        }
        
        return parent::_prepareForm();
    }
}
