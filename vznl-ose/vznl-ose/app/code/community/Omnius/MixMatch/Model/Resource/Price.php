<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * MixMatch price entity resource model
 *
 * @category    Mage
 * @package     Mage_Catalog
 */
class Omnius_MixMatch_Model_Resource_Price extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Model Initialization
     *
     */
    protected function _construct()
    {
        $this->_init('omnius_mixmatch/mixmatch', 'entity_id');
    }
}
