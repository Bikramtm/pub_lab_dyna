<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Helper_Data
 */
class Omnius_Audit_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @var array
     */
    protected $_censoredFields = array(
        'pass',
        'card',
        'bank'
    );

    /**
     * @var array
     */
    protected $_partialCensoredFields = array(
        'iban',
    );

    /**
     * Get context diff
     *
     * @param Varien_Object $context
     * @return array
     */
    public function getDiff(Varien_Object $context)
    {
        if (Mage::app()->getStore()->isAdmin() && $context->getOrigData()) {
            /** @var Omnius_Audit_Helper_DiffInterface $differ */
            $differ = $this->figureHandler($context);

            return $differ->diff($context->getOrigData(), $context->getData());
        }

        return array();
    }

    /**
     * Figure handler
     *
     * @param   Varien_Object $context
     * @return  Mage_Core_Helper_Abstract
     */
    public function figureHandler(Varien_Object $context)
    {
        if ($this->classExists($context->getResourceName())) {
            $composeClass = sprintf('audit/differ%s', ucfirst($this->getEntityName($context->getResourceName())));
            $differ = Mage::helper($composeClass);
        } else {
            $differ = Mage::helper('audit/differGeneral');
        }

        return $differ;
    }

    /**
     * Checks whether the handler class file exists or not. 
     * @param $entityName
     * @return bool
     */
    protected function classExists($entityName)
    {
        $exploded = explode('_', $this->getEntityName($entityName));
        $imploded = implode(DS, array_map('ucfirst', $exploded));
        $filePath = Mage::getModuleDir('', 'Omnius_Audit') . DS . 'Helper' . DS . 'Differ' . $imploded . '.php';

        return file_exists($filePath);
    }

    /**
     * @param $string string
     * @return string string
     */
    protected function getEntityName($string)
    {
        return substr($string, strpos($string, '/') + 1);
    }

    /**
     * Censor data
     *
     * @param $data
     * @return mixed
     */
    public function censorData($data)
    {
        array_walk_recursive($data, array($this, 'censorValue'));

        return $data;
    }

    /**
     * Censor value
     *
     * @param $value
     * @param $key
     */
    public function censorValue(&$value, $key)
    {
        if (!is_scalar($value)) {
            return;
        }
        $fullReg = sprintf('/%s/i', join('|', $this->_censoredFields));
        $partialReg = sprintf('/%s/i', join('|', $this->_partialCensoredFields));
        if (preg_match($partialReg, $key)) {
            $pattern = "/([a-zA-Z0-9]{4})([a-zA-Z0-9]*)([a-zA-Z0-9]{4})/";
            $replacement = '$1' . str_repeat('*',(strlen($value) - 8)) . '$3';
            $value = preg_replace($pattern, $replacement, $value);
        } elseif (preg_match($fullReg, $key)) {
            $value = '*******';
        }
    }

    /**
     * @param $data
     * @return array
     */
    public function toArray($data)
    {
        $items = [];
        if (is_null($data) || is_scalar($data)) {
            $items = $data;
        } elseif (is_array($data) || ($data instanceof Varien_Data_Collection)) {
            foreach ($data as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif ($data instanceof Varien_Object) {
            foreach ($data->getData() as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif ($data instanceof SimpleXMLElement) {
            foreach (json_decode(json_encode($data), true) as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif (is_object($data)) {
            foreach (get_object_vars($data) as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        }

        return $items;
    }
}