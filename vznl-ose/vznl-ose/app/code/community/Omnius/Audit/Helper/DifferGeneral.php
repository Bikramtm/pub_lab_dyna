<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Audit_Helper_DifferGeneral extends Omnius_Audit_Helper_Data implements Omnius_Audit_Helper_DiffInterface
{

    /**
     * Calculates diff between two objects
     *
     * @param $origData     Varien_Object
     * @param $currentData  Varien_Object
     * @return array
     */
    public function diff($origData, $currentData)
    {
        $origData = $this->toArray($origData);
        $currentData = $this->toArray($currentData);
        $updated = array();
        $reg = sprintf('/%s/i', join('|', $this->_censoredFields));
        if ($origData && $currentData) {
            $new = array_diff_key($currentData, $origData);
            $same = array_intersect_key($origData, $currentData);
            foreach ($same as $key => &$value) {
                if (preg_match($reg, $key)) {
                    $updated['has_changes'] = true;
                    $updated[$key] = array(
                        'old' => '*******',
                        'new' => '*******',
                    );
                } else {
                    if (is_string($origData[$key]) && is_array($currentData[$key])) {
                        $origData[$key] = explode(',', $origData[$key]);
                    }

                    if ($origData[$key] != $currentData[$key]) {
                        $updated['has_changes'] = true;
                        $updated[$key] = array(
                            'old' => $origData[$key],
                            'new' => $currentData[$key],
                        );
                    }
                }
                unset($value);
            }

            $result = array_merge($updated, $new);

            unset($updated, $new, $same, $origData, $currentData, $reg);

            return $result;
        }

        return array();
    }
}