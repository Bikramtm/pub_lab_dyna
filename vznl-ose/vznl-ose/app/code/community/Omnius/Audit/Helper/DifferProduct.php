<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Helper_DifferProduct
 */
class Omnius_Audit_Helper_DifferProduct extends Omnius_Audit_Helper_Data implements Omnius_Audit_Helper_DiffInterface
{
    /**
     * Calculates diff between two objects
     *
     * @param $origData     Varien_Object
     * @param $currentData  Varien_Object
     * @return array
     */
    public function diff($origData, $currentData)
    {
        $origData = $this->toArray($origData);
        $currentData = $this->toArray($currentData);
        $updated = [];
        $reg = sprintf('/%s/i', join('|', $this->_censoredFields));
        if ($origData && $currentData) {
            $new = array_diff_key($currentData, $origData);
            $same = array_intersect_key($origData, $currentData);
            foreach ($same as $key => &$value) {
                if ($key == 'prijs_thuiskopieheffing_bedrag') {
                    continue;
                }

                if (is_string($origData[$key]) && is_array($currentData[$key])) {
                    $origData[$key] = explode(',', $origData[$key]);
                }

                $changedImage = ($key == 'media_gallery') && (json_encode($origData[$key]['images']) != $currentData[$key]['images']) && ($origData[$key] != $currentData[$key]);
                $changedOther = ($key != 'media_gallery') && ($origData[$key] != $currentData[$key]);
                if ($changedImage || $changedOther) {
                    $updated['has_changes'] = true;
                    $updated[$key] = array(
                        'old' => $origData[$key],
                        'new' => $currentData[$key],
                    );
                }
                unset($value);
            }

            $result = $updated;

            unset($updated, $new, $same, $origData, $currentData, $reg);

            return $result;
        }

        return [];
    }
}