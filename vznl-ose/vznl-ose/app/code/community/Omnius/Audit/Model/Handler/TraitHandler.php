<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

trait Omnius_Audit_Model_Handler_TraitHandler
{
    /** @var Omnius_Audit_Model_Config */
    protected $_config;

    /** @var Omnius_Audit_Model_Logger_AbstractLogger */
    protected $_controllerLogger;

    /** @var Omnius_Audit_Model_Logger_AbstractLogger */
    protected $_entityLogger;

    /**
     * @param Omnius_Audit_Model_Config $config
     * @return $this
     */
    public function setConfig(Omnius_Audit_Model_Config $config)
    {
        $this->_config = $config;

        return $this;
    }

    /**
     * @return Omnius_Audit_Model_Config
     */
    public function getConfig()
    {
        return $this->_config;
    }

    /**
     * @return Omnius_Audit_Model_Logger_ControllerLogger
     */
    public function getControllerLogger()
    {
        if (!$this->_controllerLogger) {
            return $this->_controllerLogger = Mage::getSingleton('audit/logger_controllerLogger');
        }

        return $this->_controllerLogger;
    }

    /**
     * @return Omnius_Audit_Model_Logger_EntityLogger
     */
    public function getEntityLogger()
    {
        if (!$this->_entityLogger) {
            return $this->_entityLogger = Mage::getSingleton('audit/logger_entityLogger');
        }

        return $this->_entityLogger;
    }
}
