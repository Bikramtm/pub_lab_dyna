<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Observer
 */
class Omnius_Audit_Model_Observer
{
    /** @var Omnius_Audit_Model_Processor */
    protected $_processor;

    /**
     * Handle controller pre action
     *
     * @param Varien_Object $payload
     */
    public function handleControllerPreAction(Varien_Object $payload)
    {
        try {
            $this->_getProcessor()->processController($payload, 'predispath');
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
        }
    }

    /**
     * Handle controller post action
     *
     * @param Varien_Object $payload
     */
    public function handleControllerPostAction(Varien_Object $payload)
    {
        try {
            $this->_getProcessor()->processController($payload, 'postdispath');
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
        }
    }

    /**
     * Handle entity save event
     *
     * @param Varien_Object $payload
     */
    public function handleEntitySaveEvent(Varien_Object $payload)
    {
        $this->handleEntityEvent($payload, 'save');
    }

    /**
     * Handle entity delete event
     *
     * @param Varien_Object $payload
     */
    public function handleEntityDeleteEvent(Varien_Object $payload)
    {
        $this->handleEntityEvent($payload, 'delete');
    }

    /**
     * Handle entity event
     *
     * @param Varien_Object $payload
     * @param string $action
     */
    protected function handleEntityEvent(Varien_Object $payload, $action = 'save')
    {
        try {
            $this->_getProcessor()->processEntity($payload, $action);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
        }
    }

    /**
     * Get processor object
     *
     * @return Omnius_Audit_Model_Processor
     */
    protected function _getProcessor()
    {
        if ( ! $this->_processor) {
            return $this->_processor = Mage::getSingleton('audit/processor');
        }
        return $this->_processor;
    }
} 