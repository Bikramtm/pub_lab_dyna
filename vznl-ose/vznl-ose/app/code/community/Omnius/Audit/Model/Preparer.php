<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Preparer
 */
class Omnius_Audit_Model_Preparer
{
    protected $_supports = array(
        'Mage_Sales_Model_Order',
        'Mage_Sales_Model_Order_Payment',
        'Mage_Customer_Model_Customer',
    );

    /**
     * Prepare entity
     *
     * @param $entity
     * @return Varien_Object
     */
    public function prepare($entity)
    {
        if (is_object($entity)) {
            if ($entity instanceof Mage_Sales_Model_Order) {
                $entity = $this->_prepareOrder($entity);
            } elseif ($entity instanceof Mage_Sales_Model_Order_Payment) {
                $entity = $this->_preparePayment($entity);
            } elseif ($entity instanceof Mage_Customer_Model_Customer) {
                $entity = $this->_prepareCustomer($entity);
            }
        }

        return $entity;
    }

    /**
     * Check if entity is an instance of a supported class
     *
     * @param $entity
     * @return bool
     */
    public function supports($entity)
    {
        if (is_object($entity)) {
            foreach ($this->_supports as $className) {
                if ($entity instanceof $className) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Prepare order
     *
     * @param $order
     * @return mixed
     */
    protected function _prepareOrder(Mage_Sales_Model_Order $order)
    {
        $order->setData('__getItemsCollection', $order->getItemsCollection());
        $order->setData('__billing_address_id_obj', $order->getBillingAddress());
        $order->setData('__shipping_address_id_obj', $order->getShippingAddress());

        $order->setData('__payment_obj', $order->getPayment());

        //$order->setData('__superorder_obj', Mage::getModel('superorder/superorder')->load($order->getData('superorder_id'))); infinite recursion

        return $order;
    }

    /**
     * Prepare payment
     *
     * @param Mage_Sales_Model_Order_Payment $payment
     * @return Mage_Sales_Model_Order_Payment
     */
    protected function _preparePayment(Mage_Sales_Model_Order_Payment $payment)
    {
        $payment->unsetData('additional_information'); //remove infinite recursion causing out of memory issues
        return $payment;
    }

    /**
     * Prepare customer
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Customer_Model_Customer
     */
    protected function _prepareCustomer(Mage_Customer_Model_Customer $customer)
    {
        $customer->setData('__billing_address', $customer->getDefaultBillingAddress());
        $customer->setData('__shipping_address', $customer->getDefaultShippingAddress());
        return $customer;
    }
}