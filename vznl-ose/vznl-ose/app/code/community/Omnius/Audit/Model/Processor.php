<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Processor
 */
class Omnius_Audit_Model_Processor
{
    /** @var Omnius_Audit_Model_Config */
    protected $_config;

    /** @var Omnius_Audit_Model_Handler_ControllerHandler */
    protected $_controllerHandler;

    /** @var Omnius_Audit_Model_Handler_EntityHandler */
    protected $_entityHandler;
    
    /** @var bool */
    protected $_loaded = false;

    /**
     * Process controller
     *
     * @param Varien_Object $payload
     * @param null $actionType
     */
    public function processController(Varien_Object $payload, $actionType = null)
    {
        $this->getControllerHandler()->handle($payload, $actionType);
    }

    /**
     * @param Varien_Object $payload
     */
    public function processEntity(Varien_Object $payload, $action)
    {
        $this->getEntityHandler()->handle($payload, $action);
    }

    /**
     * @return Omnius_Audit_Model_Handler_ControllerHandler
     */
    public function getControllerHandler()
    {
        if ( ! $this->_controllerHandler) {
            /** @var Omnius_Audit_Model_Handler_ControllerHandler $_controllerHandler */
            $_controllerHandler = Mage::getSingleton('audit/handler_controllerHandler');
            $_controllerHandler->setConfig($this->getConfig());

            return $this->_controllerHandler = $_controllerHandler;
        }
        return $this->_controllerHandler;
    }

    /**
     * @return Omnius_Audit_Model_Handler_EntityHandler
     */
    public function getEntityHandler()
    {
        if ( ! $this->_entityHandler) {
            /** @var Omnius_Audit_Model_Handler_EntityHandler $_entityHandler */
            $_entityHandler = Mage::getSingleton('audit/handler_entityHandler');
            $_entityHandler->setConfig($this->getConfig());
            return $this->_entityHandler = $_entityHandler;
        }
        return $this->_entityHandler;
    }

    /**
     * Returns the audit config
     *
     * @return Omnius_Audit_Model_Config
     */
    public function getConfig()
    {
        /** @var Omnius_Audit_Model_Config $config */
        if (!$this->_loaded) {
            if (Mage::app()->useCache('config')) {
                $cacheConfig = Mage::app()->loadCache(Omnius_Audit_Model_Config::CONFIG_CACHE_KEY);
            } else {
                $cacheConfig = false;
            }

            if ($cacheConfig) {
                $config = Mage::getSingleton('audit/config');
                $config->loadString($cacheConfig);
                $this->_config = $config;
            } else {
                $config = Mage::getSingleton('audit/config');
                $config->loadString(Mage::getConfig()->loadModulesConfiguration('audit.xml')->getXmlString());
                if (Mage::app()->useCache('config')) {
                    Mage::app()->saveCache($config->getXmlString(), Omnius_Audit_Model_Config::CONFIG_CACHE_KEY, array(Mage_Core_Model_Config::CACHE_TAG));
                }
                $this->_config = $config;
            }
            $this->_loaded = true;
        }
        return $this->_config;
    }
}