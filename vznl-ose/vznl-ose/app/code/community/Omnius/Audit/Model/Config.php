<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Config
 */
class Omnius_Audit_Model_Config extends Varien_Simplexml_Config
{
    const CONFIG_CACHE_KEY = 'audit_config';
    const CONFIG_MODE_WHITELIST = 'whitelist';
    const CONFIG_MODE_BLACKLIST = 'blacklist';

    /** @var array */
    protected $_events = array();

    /**
     * @param string $path
     * @return string
     */
    public function getValue($path)
    {
        return (string) $this->getNode($path);
    }

    /**
     * Get denied controllers
     *
     * @return array
     */
    public function getControllersList()
    {
        $res = array();
        $controllers = $this->getXpath('events/controllers_list/child::node()');
        foreach ($controllers as $controller) {
            $res[] = (string) $controller->getName();
        }

        return $res;
    }

    /**
     * Get allowed entities
     *
     * @return array
     */
    public function getAllowedEntities()
    {
        $res = array();
        $entities = $this->getXpath('events/entities/child::node()');
        foreach ($entities as $entity) {
            $res[] = (string) $entity->getName();
        }

        return $res;
    }

    /**
     * Get log time actions
     *
     * @return array
     */
    public function getLogTimeActions()
    {
        $res = [];
        $controllers = $this->getXpath('events/log_time/child::node()');
        foreach ($controllers as $controller) {
            $res[] = (string) $controller->getName();
        }

        return $res;
    }

    /**
     * Is whitelist mode
     *
     * @return array
     */
    public function isWhitelistMode()
    {
        $auditMode = $this->getValue('mode');
        return $auditMode == self::CONFIG_MODE_WHITELIST;
    }

    /**
     * Is blacklist mode
     *
     * @return array
     */
    public function isBlacklistMode()
    {
        $auditMode = $this->getValue('mode');
        return $auditMode == self::CONFIG_MODE_BLACKLIST;
    }
}