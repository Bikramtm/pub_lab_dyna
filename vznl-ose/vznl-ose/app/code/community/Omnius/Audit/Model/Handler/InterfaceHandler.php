<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

interface Omnius_Audit_Model_Handler_InterfaceHandler
{
    /**
     * @param Varien_Object $payload
     * @param string $action
     * @return mixed
     */
    public function handle(Varien_Object $payload, $action = 'save');
}
