<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Dumper_AbstractDumper
 */
abstract class Omnius_Audit_Model_Dumper_AbstractDumper
{
    /**
     * @param Omnius_Audit_Model_Event $event
     * @return mixed
     */
    abstract public function dump(Omnius_Audit_Model_Event $event);
} 