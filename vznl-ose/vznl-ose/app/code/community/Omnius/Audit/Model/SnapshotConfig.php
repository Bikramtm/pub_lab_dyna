<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_ShapshotConfig
 */
class Omnius_Audit_Model_SnapshotConfig extends Omnius_Audit_Model_Config
{
    const CONFIG_CACHE_KEY = 'snapshot_config';
}
