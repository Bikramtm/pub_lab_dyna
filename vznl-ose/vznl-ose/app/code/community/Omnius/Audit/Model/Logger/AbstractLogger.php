<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Logger_AbstractLogger
 */
abstract class Omnius_Audit_Model_Logger_AbstractLogger
{
    /** @var array */
    protected $_dumpers = array();

    /**
     * @param Omnius_Audit_Model_Event $event
     */
    public function log(Omnius_Audit_Model_Event $event)
    {
        $this->_getDumper($event->getDumper())
            ->dump($event);
    }

    /**
     * @param string $dumper
     * @return Omnius_Audit_Model_Dumper_AbstractDumper
     * @throws Exception
     */
    protected function _getDumper($dumper)
    {
        if ( ! isset($this->_dumpers[$dumper])) {
            if ( ! ($_dumper = Mage::getModel(sprintf('audit/dumper_%s', $dumper)))) {
                throw new Exception('Invalid dumper configuration.');
            }
            return $this->_dumpers[$dumper] = $_dumper;
        }
        return $this->_dumpers[$dumper];
    }
} 