<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Dumper_File
 */
class Omnius_Audit_Model_Dumper_Log extends Omnius_Audit_Model_Dumper_File
{
    /**
     * @param Omnius_Audit_Model_Event $event
     * @return mixed|void
     * @throws Exception
     */
    public function dump(Omnius_Audit_Model_Event $event)
    {
        $section = (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml') ? 'backend' : 'frontend';

        // Do not log GET requests to the admin login page
        if ($section == 'backend' && $event->getAction() == 'adminhtml_index_index' && ! Mage::app()->getRequest()->isPost()) {
            return;
        }

        $event->setData('section', $section);
        $data = trim($this->extractData($event), PHP_EOL);

        $filename = $this->createFilename(sprintf('audit_%s_%s', $section, $event->getIsController() ? 'controller' : 'entity'));
        $this->log($data, Zend_log::DEBUG, $filename);
    }

    /**
     * @param $message
     * @param null $level
     * @param string $file
     * @param bool $forceLog
     */
    public function log($message, $level = null, $file = '', $forceLog = false)
    {
        try {
            $logActive = Mage::getStoreConfig('dev/log/active');
            if (empty($file)) {
                $file = Mage::getStoreConfig('dev/log/file');
            }
        }
        catch (Exception $e) {
            $logActive = true;
        }

        if (!$logActive && !$forceLog) {
            return;
        }

        static $loggers = array();

        $level  = is_null($level) ? Zend_Log::DEBUG : $level;
        $file = empty($file) ? 'system.log' : $file;

        try {
            if (!isset($loggers[$file])) {
                $logDir  = Mage::getBaseDir('var') . DS . 'log';
                $logFile = $file;

                if (!is_dir($logDir)) {
                    mkdir($logDir);
                    chmod($logDir, 0777);
                }

                if (!file_exists($logFile)) {
                    file_put_contents($logFile, '');
                    chmod($logFile, 0777);
                }

                $format = '%message%' . PHP_EOL;
                $formatter = new Zend_Log_Formatter_Simple($format);
                $writerModel = (string)Mage::getConfig()->getNode('global/log/core/writer_model');
                if (!$writerModel) {
                    $writer = new Zend_Log_Writer_Stream($logFile);
                }
                else {
                    $writer = new $writerModel($logFile);
                }
                $writer->setFormatter($formatter);
                $loggers[$file] = new Zend_Log($writer);
            }

            if (is_array($message) || is_object($message)) {
                $message = print_r($message, true);
            }

            $loggers[$file]->log($message, $level);
        }
        catch (Exception $e) {
            // Do nothing
        }
    }

}
