<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Ctn_Model_Mysql4_Ctn_Collection
 */
class Omnius_Ctn_Model_Mysql4_Ctn_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Override constructor
     */
    public function _construct()
    {
        $this->_init('ctn/ctn');
    }
}
