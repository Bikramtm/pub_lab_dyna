<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

if (!$installer->tableExists('customer_ctn')) {
    /** Ctn Role table */
    $customerCtnTable = new Varien_Db_Ddl_Table();

    $customerCtnTable->setName('customer_ctn');

    $customerCtnTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    );

    $customerCtnTable->addColumn(
        'customer_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $customerCtnTable->addColumn(
        'ctn',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        50
    );

    $customerCtnTable->addForeignKey(
        'fk_customer_id',
        'customer_id',
        Mage::getSingleton('core/resource')->getTableName('customer/entity'),
        'entity_id',
        $customerCtnTable::ACTION_SET_NULL,
        $customerCtnTable::ACTION_CASCADE
    );

    $customerCtnTable->setOption('type', 'InnoDB');
    $customerCtnTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($customerCtnTable);
}

$installer->endSetup();
