<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$tableName = Mage::getSingleton('core/resource')->getTableName('ctn/ctn');

$installer->getConnection()->addColumn($tableName, 'email', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'comment' => 'Email',
    'after' => 'additional',
]);

$installer->endSetup();
