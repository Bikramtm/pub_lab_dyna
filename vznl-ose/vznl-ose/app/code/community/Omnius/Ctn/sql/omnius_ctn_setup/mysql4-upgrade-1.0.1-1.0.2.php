<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

/** @var Magento_Db_Adapter_Pdo_Mysql $c */
$connection = Mage::getSingleton('core/resource')->getConnection('core');
$tableName = Mage::getSingleton('core/resource')->getTableName('ctn/ctn');

$connection->addColumn($tableName, 'start_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'comment' => 'CTN Valid Starting From',
));

$connection->addColumn($tableName, 'end_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'comment' => 'CTN Valid Ending On',
));

$installer->endSetup();