<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Validators_Helper_Data
 */
class Omnius_Validators_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Magento backend config path for the field validation rules
     */
    CONST FIELD_RULES_CONFIG_PATH = 'form_validations/form_fields/';
    CONST FIXED_RULES_CONFIG_PATH = 'form_validations/inlife_vom/';
    CONST MIN_PORTING_DAYS = 5;
    CONST MIN_PORTING_DAYS_PREPAID = 2;
    CONST MIN_PORTING_DAYS_RETAIL = 1;
    CONST MAX_PORTING_DAYS_BUSINESS = 120;
    CONST MAX_PORTING_DAYS_CONSUMER = 60;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Validates the sim number against a provided regex to make sure it has a valid format
     * @param string $sim
     * @return bool
     */
    public function validateSim($sim)
    {
        return $this->testRegex($this->getRule('sim_format'), $sim);
    }

    /**
     * Checks whether the value is only numeric
     * @param string $value
     * @return bool
     */
    public function validateNumber($value)
    {
        return $this->testRegex('/^[0-9]+$/', $value);
    }

    /**
     * Validate a date is entered in the format dd-mm-yyyy
     *
     * @param $date
     * @return bool
     */
    public function validateIsDate($date)
    {
        if (empty($date)) {
            return false;
        }

        $dateData = explode('-', $date);
        if (count($dateData) !== 3) {
            return false;
        }

        return preg_match('/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/', $date) && checkdate($dateData[1], $dateData[0], $dateData[2]);
    }

    /**
     * Checks if the porting date is bigger than the maximum accepted porting date.
     * @param $date
     * @return bool
     */
    public function validateMaxPortingDaysC($date)
    {
        return $this->validateMaxPortingDays($date, self::MAX_PORTING_DAYS_CONSUMER);
    }

    /**
     * Checks if the porting date is bigger than the maximum accepted porting date.
     * @param $date
     * @return bool
     */
    public function validateMaxPortingDays($date, $type)
    {
        $date = strtotime($date);
        $maxDate = strtotime('+' . $type . ' days', time());

        return $date <= $maxDate;
    }

    /**
     * Checks if the porting date is bigger than the maximum accepted porting date.
     * @param $date
     * @return bool
     */
    public function validateMaxPortingDaysB($date)
    {
        return $this->validateMaxPortingDays($date, self::MAX_PORTING_DAYS_BUSINESS);
    }

    /**
     * Checks if the date is a working day
     * @param $date
     * @return bool
     */
    public function validateWorkingDay($date)
    {
        $date = strtotime($date);
        $nowDay = strftime("%u", $date);
        if ($nowDay == 6 || $nowDay == 7) {
            return false;
        }

        return true;
    }

    /**
     * Validate the street array has data
     */
    public function validateStreet($data)
    {
        return !(empty($data[0]) || empty($data[1]));
    }

    /**
     * Checks if the porting date is smaller than 5 days.
     * @param $date
     * @param int $days
     * @return bool
     */
    public function validateMinPortingDays($date, $days = self::MIN_PORTING_DAYS)
    {
        $date = strtotime($date);
        $i = $j = 1;
        $holidayList = array_map('trim', explode("\n", $this->getRule('holiday_days'))); // get holiday days from admin
        while ($i <= $days) {
            $tmp = strftime("%u", strtotime("+$j day"));
            $minDate = strftime("%Y-%m-%d", strtotime("+$j day"));
            if ($tmp != "6" && $tmp != "7" && !in_array($minDate, $holidayList)) {
                $i = $i + 1;
                $j = $j + 1;
            } else {
                $j = $j + 1;
            }
        }

        $minDate = strtotime($minDate);

        return $minDate <= $date;
    }

    /**
     * For prepiad, perform same validation but set the min future day to 1
     * @param $date
     * @return mixed
     *
     * For prepiad, perform same validation but set the min future day to 1
     */
    public function validateMinPortingDaysPrepaid($date)
    {
        return $this->validateMinPortingDays($date, self::MIN_PORTING_DAYS_PREPAID);
    }

    /**
     * Checks if a date is a holiday date or a working day.
     */
    public function validateHolidayDate($date)
    {
        $date = strtotime($date);
        $nowDate = strftime("%Y-%m-%d", $date);
        $holidayList = explode("\n", $this->getRule('holiday_days')); // get holiday days from admin

        if (in_array($nowDate, $holidayList)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns a list of holidays for the current year
     * @return array
     */
    public function getHolidayDates()
    {
        $input = explode("\n", $this->getRule('holiday_days'));

        return array_map('trim', $input);
    }

    /**
     * Validates the identification document based on its type (passport, id, etc)
     * @param $idType
     * @param $idNumber
     * @param $countryCode
     * @return bool
     */
    public function validateIdType($idType, $idNumber, $countryCode)
    {
        switch ($idType) {
            case 'P':
                if (strtoupper($countryCode) == 'NL') {
                    // If country is NL match 9 digits, 1st is Character, no O, 2-9 any char + number
                    $return = $this->validateLegitimationPNL($idNumber);
                } else {
                    // If country is not NL, it must be in EU
                    $return = true;
                }
                break;
            case 'R':
                // NL Only and 10 digits
                if (strtoupper($countryCode) == 'NL') {
                    $return = $this->validateLegitimationR($idNumber);
                } else {
                    $return = false;
                }
                break;
            case 'N':
                // NL Only and 9 digits
                if (strtoupper($countryCode) == 'NL') {
                    $return = $this->validateLegitimationN($idNumber);
                } else {
                    $return = false;
                }
                break;
            case '0':
                $return = true;
                break;
            case '1':
            case '2':
            case '3':
            case '4':
                // Strip NLD and 8 digits
                $return = $this->validateLegitimationType1To4($idNumber);
                break;
            default:
                $return = false;
        }

        return $return;
    }

    /**
     * Validates an IMEI number against a provided regex to make sure it has a valid format
     * @param string $imei
     * @param bool $use_checksum
     * @return bool
     */
    public function validateIMEI($imei, $use_checksum = true)
    {
        if ($this->testRegex($this->getRule('imei_format'), $imei)) {
            if (!$use_checksum) {
                return true;
            }
            $sum = 0;
            for ($i = 0; $i < 14; $i++) {
                $tmp = $imei[$i] * (($i % 2) + 1);
                $sum += ($tmp % 10) + intval($tmp / 10);
            }

            return (((10 - ($sum % 10)) % 10) == $imei[14]);
        }

        return false;
    }

    /**
     * Validate email syntax against a provided regex to make sure it has a valid format
     * @param string $email
     * @return bool
     */
    public function validateEmailSyntax($email)
    {
        return $this->testRegex($this->getRule('email_address'), $email);
    }

    /**
     * Validates a numeric input
     * @param $input
     * @return bool
     */
    public function validateIsNumber($input)
    {
        return is_numeric($input);
    }

    /**
     * Make sure the specific country exists in the database
     * @param $countryId
     * @return bool
     */
    public function validateCountryCode($countryId)
    {
        $country = Mage::getModel("directory/country")->load($countryId);

        return (bool) $country->getId();
    }

    /**
     * Needed to validate fields that need to be in the future compared to 'now'
     * Returns true if date is in the future
     *
     * @param $date
     * @return bool
     */
    public function validateFutureDate($date)
    {
        // Current day should also be available
        return (bool) (Mage::getModel('core/date')->timestamp(time()) <= (strtotime($date)));
    }

    /**
     * Needed to validate fields that need to be in the past compared to 'now'
     * Returns true if date is in the past
     *
     * @param $date
     * @return bool
     */
    public function validatePastDate($date)
    {
        return (bool) (Mage::getModel('core/date')->timestamp(time()) > strtotime($date));
    }

    /**
     * Validates email against an option dns checker
     * @param string $email
     * @return bool
     */
    public function validateEmailDns($email)
    {
        $rule = $this->getRule('email_checkdnsrr');

        if (!$rule) {
            return true;
        }

        return Mage::helper('omnius_validators/emailCheck')->validateEmailExists($email);
    }

    /**
     * Validates a date to determine if it goes beyond a specific max age
     * @param string $date
     * @return bool
     */
    public function validateMaxAge($date)
    {
        $rule = $this->getRule('max_age');

        $valid = true;
        $year = (int) date('Y');
        $dob_year = (int) date('Y', strtotime($date));
        $cond1 = $year - $rule > $dob_year;
        $cond2 = ($year - $rule == $dob_year) && (strtotime(date('d-m-', strtotime($date)) . $year) < strtotime(date('d-m-Y')));
        if ($cond1 || $cond2) {
            $valid = false;
        }

        return $valid;
    }

    /**
     * Validates a date to determine if it goes beyond a specific min age
     * @param string $date
     * @return bool
     */
    public function validateMinAge($date)
    {
        $rule = $this->getRule('min_age');

        $valid = true;
        $year = (int) date('Y');
        $dob_year = (int) date('Y', strtotime($date));
        $cond1 = $year - $rule < $dob_year;
        $cond2 = ($year - $rule == $dob_year) && (strtotime(date('d-m-', strtotime($date)) . $year) > strtotime(date('d-m-Y')));
        if ($cond1 || $cond2) {
            $valid = false;
        }

        return $valid;
    }

    /**
     * Validate a KvK number to make sure it has the right format
     * @param string $coc
     * @return bool
     */
    public function validateCoc($coc)
    {
        if (preg_match(Mage::helper('omnius_validators')->getRule('kvk_number'), $coc)) {
            return true;
        }

        return false;
    }

    /**
     * Validates the name against possibly defined patterns in the backend
     * @param string $name
     * @return bool
     */
    public function validateName($name)
    {
        $restrictedChars = $this->escape($this->getRule('name_fields'));
        $regex = sprintf('/[%s]/', $restrictedChars);

        return false === $this->testRegex($regex, $name);
    }

    /**
     * Validates the name to make sure at least 1 character was provided
     * @param $name
     * @return bool
     */
    public function validateNameLength($name)
    {
        return mb_strlen(trim($name)) > 1;
    }

    /**
     * Validates the name against possibly defined patterns in the backend
     * @return string
     */
    public function getNameRegex()
    {
        $restrictedChars = $this->escape($this->getRule('name_fields'));

        return sprintf('/[%s]/', $restrictedChars);
    }

    /**
     * Validates a NL i18 standard IBAN
     * @param $iban
     * @return bool
     */
    public function validate18IBAN($iban)
    {
        if (preg_match('/^NL[0-9]{2}[0-9A-Z]{14}$/i', $iban)) {
            $iban = substr($iban, 4 - strlen($iban)) . substr($iban, 0, 4);
            $number = '';
            foreach (str_split(strtoupper($iban)) as $char) {
                if (!is_numeric($char)) {
                    $char = ord(strtoupper($char)) - 55; //convert A (65) to 10, B (66) to 11 ...
                }
                $number .= $char;
            }
            $temp = $number;
            while (strlen($temp)) {
                $length = strlen($temp) >= 9 && !isset($result)
                    ? 9
                    : (strlen($temp) >= 7
                        ? 7
                        : strlen($temp));
                $number = (!isset($result)
                        ? ''
                        : (intval($result) < 10
                            ? (0 . $result)
                            : $result))
                    . substr($temp, 0, $length);
                $temp = substr($temp, $length - strlen($temp));
                $result = $number % 97;
                if (strlen($temp) < 7 && $length < 7) {
                    $temp = '';
                }
            }

            return $result === 1;
        }

        return false;
    }

    /**
     * Checks if BIC or IBAN is valid.
     * @param $isValid
     * @return bool|string
     */
    public function validateBicIban($isValid)
    {
        $temp = $isValid;
        // Do the 11 test
        if (preg_match('/^(\d{9}|\d{10})$/', $isValid)) {
            if (preg_match('/^(\d{9})$/', $isValid)) {
                $isValid = '0' . $isValid;
            }
            $splitted = str_split($isValid);
            $sum = 0;
            $multiplier = 10;
            foreach ($splitted as $s) {
                $sum += $multiplier * (int) $s;
                $multiplier--;
            }

        } elseif (preg_match($this->getRule('if_iban_nl'), $isValid)) {
            $return = $temp;
        } else {
            $return = false;
        }

        return $return;
    }

    /**
     * Validates the VAT against possibly defined regex in the backend
     * @param string $vat
     * @return bool
     */
    public function validateVat($vat)
    {
        return $this->testRegex($this->getRule('vat_fields'), $vat);
    }

    /**
     * Validates the documents field against possibly defined regex in the backend
     * @param string $paper
     * @return bool
     */
    public function validatePapers($paper)
    {
        return $this->testRegex($this->getRule('id_papers'), $paper);
    }

    /**
     * Checks the code or the name of the provider
     *
     * @param string $provider
     * @return bool
     */
    public function validateNetworkProvider($provider)
    {
        return false !== (bool) $this->getProvider($provider);
    }

    /**
     * Checks the code or the code of the operator
     *
     * @param string $operator
     * @return bool
     */
    public function validateNetworkOperator($operator)
    {
        $allOperators = Mage::helper('operator')->getOperatorOptions();

        return false !== (bool) array_key_exists($operator, $allOperators);
    }

    /**
     * Validates the sim number against the possible sim range based also on the provided network
     * @param $network
     * @param $sim
     * @return bool
     */
    public function validateNetworkSim($network, $sim)
    {
        if (!$provider = $this->getProvider($network)) {
            return false;
        }
        $checker = $this->getSimRangeChecker($provider['sim_range']);

        return $checker($sim);
    }

    /**
     * Validates the sim number against the possible sim range based also on the provided network and operator
     * @param $sim
     * @param $operator
     * @param $provider
     * @return mixed
     */
    public function validateSimByProviderAndOperator($sim, $operator, $provider)
    {
        $simrange = Mage::getModel('operator/operator')->getRangeByCodes($operator, $provider);
        $checker = $this->getSimRangeChecker($simrange);

        return $checker($sim);
    }

    /**
     * Depending on the given sim range, it will generate
     * a callback that will check the validity of a given sim number
     *
     * @param $simRange
     * @return callable
     */
    protected function getSimRangeChecker($simRange)
    {
        if (!trim($simRange)) {
            return function () {
                return true;
            };
        }

        $regex = $this->getRegexForRange($simRange);
        if (is_array($regex)) {
            return function ($sim) use ($regex) {
                return $regex[0] <= $sim && $sim <= $regex[1];
            };
        } else {
            return function ($sim) use ($regex) {
                return true == preg_match($regex, $sim);
            };
        }
    }

    /**
     * Returns a list with the sim regex for each operator
     * @return array
     */
    public function getDatabaseSimRegex()
    {
        $regexes = array();
        foreach (Mage::getModel('operator/operator')->getCollection() as $provider) {
            $regexes[$provider->getOperatorId()] = $this->getRegexForRange($provider->getSimRange());
        }

        return $regexes;
    }


    /**
     * Generates the regex for the given sim range
     * If sim range is a numeric range, an array will
     * be returned with the two limits
     *
     * @param $simRange
     * @return callable|string
     */
    public function getRegexForRange($simRange)
    {
        $simRange = preg_replace('/(\s|[^a-zA-Z0-9\/\+\,])+/', '', $simRange); //cleanup
        if (false !== strpos($simRange, '/') && false === strpos(strtolower($simRange), 't/m')) {
            preg_match('/^([a-zA-Z0-9\+]+)\/?([a-zA-Z0-9\+]+)/', $simRange, $matches);
            array_shift($matches);
        } elseif (false !== strpos($simRange, ',')) {
            $parts = array_map('trim', explode(',', $simRange));
            $matches = array_filter(array_map(function ($el) {
                preg_match('/^[a-zA-Z0-9\+]+/', $el, $match);

                return $match[0];
            }, $parts));
        } elseif (false !== strpos(strtolower($simRange), 't/m')) {
            $parts = explode('t/m', $simRange);
            $matches = array_filter(array_map(function ($el) {
                preg_match('/^[a-zA-Z0-9\+]+/', $el, $match);
                if (false !== strpos(strtolower($match[0]), 'x')) {
                    return str_replace('x', '9', $match[0]);
                }

                return $match[0];
            }, $parts));
            sort($matches);

            return $matches;
        } else {
            preg_match('/^[a-zA-Z0-9\+]+/', $simRange, $matches);
        }

        $rules = array();

        foreach ($matches as $rule) {
            if (false !== strpos($rule, '+')) {
                preg_match('/^([a-zA-Z0-9\+])+/', $rule, $subMatch);
                list ($beginsWith, $trailNumber) = explode('+', $subMatch[0]);
                $regRule = sprintf('%s[0-9]{%s}', $beginsWith, $trailNumber);
                array_push($rules, $regRule);
            } elseif (false !== strpos(strtolower($rule), 'x')) {
                $beginsWith = str_replace('x', '', $rule);
                $trailNumber = strlen($rule) - strlen($beginsWith);
                $regRule = sprintf('%s[0-9]{%s}', $beginsWith, $trailNumber);
                array_push($rules, $regRule);
            } else {
                preg_match('/\d+/', $rule, $match);
                if (count($match)) {
                    $regRule = sprintf('[0-9]{%s}', $match[0]);
                }
                array_push($rules, $regRule);
            }
        }

        return count($rules) > 1
            ? sprintf('/^(%s)$/', join('|', $rules))
            : count($rules) == 1
                ? sprintf('/^(%s)$/', join('|', $rules))
                : '/^.*$/'; //match anything of no rules exist
    }

    /**
     * Accepts either to provider code or name
     *
     * @param $providerId
     * @return null|array
     */
    protected function getProvider($providerId)
    {
        $provider = array_values(array_filter(array_map(function ($provider) use ($providerId) {
            return in_array($providerId, array($provider['service_provider'])) ? $provider : false;
        }, array_values($this->getNetworkProviders()))));

        return count($provider) ? $provider[0] : false;
    }

    /**
     * Gets a list of network providers from the backend
     * @return array
     */
    protected function getNetworkProviders()
    {
        $newData = array();
        $data = Mage::getModel('operator/operator')->getCollection()->getData();
        foreach ($data as $key => $item) {
            $providerModel = Mage::getModel('operator/serviceProvider')->load($item['service_provider_id']);
            $serviceProviderCode = [
                0 => $providerModel->getCode(),
                1 => $providerModel->getName(),
            ];
            $newData[$key] = $item;
            $newData[$key]['service_provider'] = $serviceProviderCode[0];
        }

        return $newData;
    }

    /**
     * Return a list of the combinations that are allowed to have a dot in the NP contract number
     *
     * @return string
     */
    public function getNpExceptions()
    {
        $result = [];

        $dotAllowed = Mage::getModel('operator/operator')
            ->getCollection()
            ->addFieldToFilter('dot_allowed', 1)
            ->load();
        foreach ($dotAllowed as $combination) {
            $operatorCode = Mage::getModel('operator/networkOperator')->load($combination->getNetworkOperatorId())->getCode();
            $providerCode = Mage::getModel('operator/serviceProvider')->load($combination->getServiceProviderId())->getCode();
            $result[] = $operatorCode . "-" . $providerCode;
        }

        return $result;
    }

    /**
     * Generic method to validate a variable against a specified regex
     * @param string $regex
     * @param string $variable
     * @return bool
     */
    protected function testRegex($regex, $variable)
    {
        return is_string($variable) ? (strlen(trim($regex)) ? preg_match(trim($regex), trim($variable)) === 1 : true) : false;
    }

    /**
     * Returns the value of a rule based on the type of rule and store
     * @param string $field
     * @param null|string|integer $store
     * @return null|string
     */
    public function getRule($field, $store = null)
    {
        $rule = Mage::getStoreConfig(sprintf('%s%s', self::FIELD_RULES_CONFIG_PATH, $field), $store);

        return strlen(trim($rule)) > 0 ? $rule : '/^.*$/';
    }

    /**
     * Returns the path of the field rules configuration
     * @param $field
     * @param null $store
     * @return mixed
     */
    public function getConfigOptions($field, $store = null)
    {
        return Mage::getStoreConfig(sprintf('%s%s', self::FIELD_RULES_CONFIG_PATH, $field), $store);
    }

    /**
     * Returns the path of the fixed field rules configuration
     * @param $field
     * @param null $store
     * @return mixed|string
     */
    public function getFixedRule($field, $store = null)
    {
        $rule = Mage::getStoreConfig(sprintf('%s%s', self::FIXED_RULES_CONFIG_PATH, $field), $store);

        return strlen(trim($rule)) > 0 ? $rule : '/^.*$/';
    }

    /**
     * Generic method to escape a string
     * @param $value
     * @return mixed
     */
    protected function escape($value)
    {
        $chars = array(
            '\\' => '\\\\',
            '~' => '\~',
            '`' => '\`',
            '!' => '\!',
            '@' => '\@',
            '#' => '\#',
            '$' => '\$',
            '%' => '\%',
            '^' => '\^',
            '&' => '\&',
            '*' => '\*',
            '(' => '\(',
            ')' => '\)',
            '_' => '\_',
            '+' => '\+',
            '=' => '\=',
            '{' => '\{',
            '}' => '\}',
            '[' => '\[',
            ']' => '\]',
            ';' => '\;',
            ':' => '\:',
            '\'' => '\\\'',
            '"' => '\"',
            '|' => '\|',
            '<' => '\<',
            '>' => '\>',
            ',' => '\,',
            '.' => '\.',
            '?' => '\?',
            '/' => '\/',
        );

        return str_replace(array_keys($chars), array_values($chars), $value);
    }

    /**
     * Validates the provided legitimatie number against a specific regex
     * @param $number
     * @return bool
     */
    protected function validateLegitimationPNL($number)
    {
        return $this->testRegex('/[A-Z]/', $number[0]) && strlen($number) === 9 && $this->testRegex('/[0-9]/', substr($number, 1)) && !strstr(strtolower($number), 'o');
    }

    /**
     * Validates the provided legitimatie number against a specific regex
     * @param $number
     * @return bool
     */
    protected function validateLegitimationType1To4($number)
    {
        $number = $this->stripNLD($number);

        return strlen($number) === 8 && $this->testRegex('/[0-9]/', $number) && !$this->testRegex('/^(\d)\1*$/', mb_substr($number, strlen($number) - 5));
    }

    /**
     * Validates the provided legitimatie number against a specific regex
     * @param $number
     * @return bool
     */
    protected function validateLegitimationR($number)
    {
        return strlen($number) === 10 && $this->testRegex('/[0-9]/', $number);
    }

    /**
     * Validates the provided legitimatie number against a specific regex
     * @param $number
     * @return bool
     */
    protected function validateLegitimationN($number)
    {
        return $this->testRegex('/[A-Z]/', $number[0]) && strlen($number) === 9 && $this->testRegex('/[0-9]/', substr($number, 1));
    }

    /**
     *  Strips the 'NLD' string in front of the input.
     * @param  $string string
     * @return string
     */
    public function stripNLD($string)
    {
        return (strpos(mb_strtoupper($string), 'NLD') === 0) ? substr($string, 3) : $string;
    }

    /**
     * Validates that a set of fields from the address object were filled in
     * @param Mage_Customer_Model_Address $address
     * @throws Exception
     * @throws Zend_Validate_Exception
     */
    public function validateAddressData($address)
    {
        if (!Zend_Validate::is($address->getFirstname(), 'NotEmpty')) {
            $address->addError(Mage::helper('customer')->__('Please enter the first name.'));
        }

        if (!Zend_Validate::is($address->getLastname(), 'NotEmpty')) {
            $address->addError(Mage::helper('customer')->__('Please enter the last name.'));
        }

        if (!Zend_Validate::is($address->getStreet(1), 'NotEmpty')) {
            $address->addError(Mage::helper('customer')->__('Please enter the street.'));
        }

        if (!Zend_Validate::is($address->getCity(), 'NotEmpty')) {
            $address->addError(Mage::helper('customer')->__('Please enter the city.'));
        }

        $_havingOptionalZip = Mage::helper('directory')->getCountriesWithOptionalZip();
        if (!in_array($address->getCountryId(), $_havingOptionalZip)
            && !Zend_Validate::is($address->getPostcode(), 'NotEmpty')
        ) {
            $address->addError(Mage::helper('customer')->__('Please enter the zip/postal code.'));
        }

        if (!Zend_Validate::is($address->getCountryId(), 'NotEmpty')) {
            $address->addError(Mage::helper('customer')->__('Please enter the country.'));
        }

        if ($address->getCountryModel()->getRegionCollection()->getSize()
            && !Zend_Validate::is($address->getRegionId(), 'NotEmpty')
            && Mage::helper('directory')->isRegionRequired($address->getCountryId())
        ) {
            $address->addError(Mage::helper('customer')->__('Please enter the state/province.'));
        }
    }

    /**
     * Returns the cache singleton
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }
}
