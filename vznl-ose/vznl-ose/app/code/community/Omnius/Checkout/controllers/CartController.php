<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php';

/**
 * Class Omnius_Checkout_Checkout_CartController
 */
class Omnius_Checkout_CartController extends Mage_Checkout_CartController
{
    /**
     * Retrieve checkout session model
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * @return Mage_Core_Model_Abstract|Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        if (Mage::getSingleton('customer/session')->getOrderEdit()) {
            $quote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
            if ($quote->getId()) {
                return $quote;
            }
        }

        return parent::_getQuote();
    }

    public function removeCtnAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $ctn = $this->getRequest()->getParam('ctn');
        $packageId = $this->getRequest()->getParam('packageId');

        if ($ctn && $packageId) {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $packageInfo = Mage::getModel('package/package')
                ->getPackages(null, $quote->getId(), $packageId)
                ->getFirstItem();
            $ctns = ($packageInfo->getId() && $packageInfo->getCtn()) ? explode(',', $packageInfo->getCtn()) : array();

            if (!count($ctns)) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(
                            array(
                                'error' => false,
                                'message' => 'No CTN to remove'
                            )
                        )
                    );
                return;
            }

            if (false !== ($index = array_search($ctn, $ctns))) {
                unset($ctns[$index]);
            }

            $packageInfo->setCtn(join(',', $ctns));
            $packageInfo->save();

            $result = array(
                'error' => false,
                'message' => 'CTN removed',
                'right_sidebar' => Mage::getSingleton('core/layout')->createBlock('core/template')->setTemplate('customer/right_sidebar.phtml')->toHtml(),
                'totals' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                'rightBlock' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->setCurrentPackageId($this->_getQuote()->getActivePackageId())->toHtml(),
            );
        } else {
            $result = array(
                'error' => true,
                'message' => 'Please provide the CTN number and package ID'
            );
        }
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Save customer shopping cart for later usage
     */
    public function persistCartAction()
    {
        if (!($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest())) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array('error' => true, 'message' => 'Method not supported')));
        }
        try {
            $vars = array();
            /** @var Omnius_Checkout_Model_Sales_Quote $theQuote */
            $theQuote = $this->_getCart()->getQuote();
            $quotePackages = $theQuote->getPackages();
            if ($isGuest = (bool) $this->getRequest()->get('guest', null)) {
                $vars['title'] = $this->getRequest()->get('title', null);
                $vars['initial'] = $this->getRequest()->get('initial', null);
                $vars['firstname'] = $this->getRequest()->get('firstname', null);
                $vars['lastname'] = $this->getRequest()->get('lastname', null);
                $vars['dob'] = $this->getRequest()->get('dob', null);
                $errors = array();
                $validators = Mage::helper('omnius_validators');
                foreach ($vars as $k => $var) {
                    $this->validateFormKey($var, $k, $validators, $errors);
                }

                if (!Mage::helper('omnius_validators')->validateEmailSyntax($this->getRequest()->get('email_send', null))) {
                    $errors[] = $this->__('Please provide a valid email address');
                } else {
                    $col = Mage::getModel('customer/customer')->getCollection()
                        ->addAttributeToFilter('is_prospect', true)
                        ->addAttributeToFilter('additional_email', array('like' => '%' . $this->getRequest()->get('email_send')));
                    if (count($col) > 0) {
                        $errors[] = $this->__('A prospect with this email address already exists');
                    } elseif (count($errors) === 0) {
                        $this->createProspectFromCart($vars, $quotePackages);
                    }
                }

                if (count($errors) > 0) {
                    $result = array(
                        'error' => true,
                        'message' => implode('<br />', $errors)
                    );
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(Mage::helper('core')->jsonEncode($result));

                    return;
                }
            }

            if ($this->getRequest()->get('to_save', null) != 'false') {
                $theQuote->setCartStatus(Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED)
                    ->setChannel(Mage::app()->getWebsite()->getCode())
                    ->setCartStatusStep(Omnius_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CONFIGURATOR);
                if ($isGuest) {
                    $customer = Mage::getSingleton('customer/session')->getCustomer();
                    $customerId = $customer->getId() ?: null;
                    $theQuote->setCustomerId($customerId)
                        ->setCustomerFirstname($vars['firstname'])
                        ->setCustomerEmail($this->getRequest()->get('email_send'))
                        ->setAdditionalEmail($this->getRequest()->get('email_send'))
                        ->setCustomerDob(date('Y-m-d H:i:s', strtotime($vars['dob'])))
                        ->setCustomerLastname($vars['lastname'])
                        ->setCustomerGender($vars['title'])
                        ->setCustomerMiddlename($vars['initial']);

                }
                if (count($quotePackages) < 1) {
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(
                            Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $this->__('You have no packages in the current cart.')))
                        );

                    return;
                }
                $incrementId = Mage::helper('omnius_checkout')->getQuoteIncrementPrefix() . $theQuote->getId();
                $theQuote->setData('prospect_saved_quote', $incrementId);
                $vars['increment_id'] = $incrementId;
                $theQuote->save();
            }

            if ($this->getRequest()->get('to_send', null) != null && $this->getRequest()->get('to_send', null) != 'false') {
                $result = $this->sendCartEmail($theQuote);
            } else {
                $result = array(
                    'error' => false,
                    'message' => $this->__('Saved')
                );
            }

                /** @var Omnius_Package_Model_Mysql4_Package_Collection $packages */
                $packages = Mage::getModel('package/package')->getCollection();
                $packages->addFieldToFilter('quote_id', $theQuote->getId());

                $packageIds = [];
                /** @var Dyna_Bundles_Model_Package $package */
                foreach ($packages as $package) {
                    if (!$package->getCoupon() || !$package->getIsFfCode()) {
                        continue;
                    }

                    $packageIds[] = $package->getId();
                }

                if ($this->getRequest()->get('new_cart', null) != null && $this->getRequest()->get('new_cart', null) != 'false' ) {
                    Mage::helper('omnius_checkout')->createNewQuote();
                    Mage::helper('pricerules')->decrementAll($theQuote);
                } elseif ($this->getRequest()->get('new_cart', null) != null && $this->getRequest()->get('new_cart', null) == 'false') {
                    // clone quote and set it as current quote so that many users can edit the same saved quote
                    $newQuote = $theQuote
                        ->cloneQuote()
                        ->setIsActive(true)
                        ->setCartStatus(null)
                        ->setProspectSavedQuote(null)
                        ->setStoreId(Mage::app()->getStore(true)->getId())
                        ->save();
                    
                    $theQuote->setIsActive(false)->save();

                    $this->_getCart()->setQuote($newQuote);
                    Mage::getSingleton('checkout/session')->setQuoteId($newQuote->getId());
                }
            } catch (Exception $e) {
                $result = array(
                    'error' => true,
                    'message' => sprintf('Action failed with message "%s"', $e->getMessage())
                );
            }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($result));
    }
    
    /**
     * Sends current cart email
     */
    public function emailQuoteAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return;
        }

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($this->getRequest()->getParam('cartId'));
        
        if ($customer->getId() != null) {
            $address = $customer->getCorrespondanceEmail();
        }else{
            $address = $quote->getCustomerEmail();
        }

        $valid = Mage::helper('omnius_validators')->validateEmailSyntax($address);

        $result = array(
            'error' => false,
            'message' => $this->__('An email was sent to the provided address.') . '(' . $address . ')'
        );


        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
    }

    /**
     * Sets as active the quote with the given "quote_id"
     */
    public function activateCartAction()
    {
        try {
            if (!($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest())) {
                throw new LogicException($this->__('Method not supported'));
            }
            $params = new Varien_Object($this->getRequest()->getParams());
            /** @var Mage_Customer_Model_Session $customerSession */
            $customerSession = Mage::getSingleton('customer/session');

            if (!$customerSession->getId() && !$customerSession->getProspect()) {
                throw new LogicException($this->__('No customer currently logged in'));
            }

            $actualQuote = Mage::getSingleton('checkout/cart')->getQuote();

            if ($params->hasData('quote_id')) {
                $quote = $this->getCartQuote($params);

                $isOffer = $params->hasData('is_offer');

                if ($isOffer) {
                    $this->rememberData($quote);
                    $actualQuote->setIsActive(false)->save();
                    // Offer mode
                    $quote
                        ->setIsActive(1)
                        ->setCurrentStep(null)
                        ->setStoreId(Mage::app()->getStore(true)->getId())
                        ->save();

                    $this->_getCart()->setQuote($quote);
                    $this->getCheckoutSession()->setQuoteId($quote->getId());
                    Mage::getSingleton('customer/session')->setIsOffer($quote->getId());

                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(
                            Mage::helper('core')->jsonEncode(
                                array(
                                    'error' => false,
                                    'message' => $this->__('Offer successfully loaded'),
                                    'packageId' => $quote->getActivePackageId(),
                                )
                            )
                        );

                    return;
                }

                if ($quote->getStoreId() == Mage::app()->getStore(true)->getId()) {

                    $actualQuote->setIsActive(false)->save();

                    /** @var Omnius_Checkout_Model_Sales_Quote $quote */
                    $newQuote = $quote->cloneQuote(false);
                    $newQuote = Mage::helper('pricerules')->reinitSalesRules($newQuote);

                    $newQuote->setIsActive(true)
                        ->setCartStatus(null)
                        ->setStoreId(Mage::app()->getStore(true)->getId())
                        ->setProspectSavedQuote(null)
                        ->save();

                    $this->_getCart()->setQuote($newQuote);
                    $this->getCheckoutSession()->setQuoteId($newQuote->getId());

                    $newQuotePackages = Mage::getModel('package/package')
                        ->getPackages(null, $newQuote->getId());

                    /** @var Omnius_Package_Model_Package $p */
                    foreach ($newQuotePackages as $package) {
                        $packageStatus = Mage::helper('omnius_checkout')->checkPackageStatus($package->getPackageId());
                        // Update status
                        $package->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED === $packageStatus
                            ? Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED
                            : Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
                        $package->save();
                    }

                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(
                            Mage::helper('core')->jsonEncode(
                                array(
                                    'error' => false,
                                    'message' => $this->__('Quote successfully loaded'),
                                    'packageId' => $quote->getActivePackageId(),
                                )
                            )
                        );
                    } else {
                        $this->getResponse()
                            ->setHeader('Content-Type', 'application/json')
                            ->setBody(
                                Mage::helper('core')->jsonEncode(
                                    array('error' => true, 'message' => $this->__('Given quote belongs to another store'))
                                )
                            );
                    }
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $e->getMessage())));
            }
    }

    public function cartPackagesAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        // Check if there are any incomplete packages

        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $inProgressPackages = array();
        $isOneOff = false;
        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($quote->getPackages() as $packageId => $package) {
            if($package['current_status'] !== Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED) {
                $inProgressPackages[$packageId][] = current($package['items']);
            }

            if($package['one_of_deal']) {
                $isOneOff = true;
            }
        }

        $show = false;
        if (count($inProgressPackages) > 0) {
            $show = true;
        }
        
        $response = array(
            'error' => false,
            'show'  => $show, 
            'modal' => Mage::getSingleton('core/layout')->createBlock('core/template')->setTemplate(
                    'checkout/cart/cart_packages.phtml'
                )->toHtml(),
        );

        if ($isOneOff && $show) {
            $response = array(
                'error'     => true,
                'message'   => $this->__('You can\'t continue an one-off deal order with incomplete packages.')
            );
        }
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function editSuperorderFromCreditCheckAction()
    {
        $orderIds = Mage::getSingleton('core/session')->getOrderIds();
        $lastOrderId = reset($orderIds);
        $lastOrder = Mage::getModel('sales/order')->load($lastOrderId, 'increment_id');

        $newQuote = Mage::helper('omnius_checkout')->cloneQuote($lastOrder->getQuoteId());
        
        // Create the packages for the new quote
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $packages = Mage::getModel('package/package')
            ->getPackages($lastOrder->getSuperorderId());

        $packagesData = $conn->fetchAll($packages->getSelect());
        foreach($packagesData as $packageData) {
            Mage::getModel('package/package')
                ->setData($packageData)
                ->setEntityId(null)
                ->setStatus(null)
                ->setCreditcheckStatus(null)
                ->setPortingStatus(null)
                ->setQuoteId($newQuote->getId())
                ->setOrderId(null)
                ->save();
        }
        
        $this->_getCart()->setQuote($newQuote)->save();
        Mage::getSingleton('checkout/session')->setQuoteId($newQuote->getId());
        $this->_redirect('/');
    }

    public function editSuperorderAction()
    {
        $superOrderNo = $this->getRequest()->get('superorderId');
        $redirect = $this->getRequest()->getParam('redirect', true);

        if (!$superOrderNo) {
            throw new Exception('Order not found');
        }
        /** @var Omnius_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo);
        if (!$superOrder->getId()) {
            return $this->_redirect('checkout/cart/');
        }

        $cancelledPackages = array();
        $quoteShippingData = array('pakket' => array(), 'payment' => array());

        // Discard any open order edits
        Mage::helper('omnius_checkout')->exitSuperOrderEdit();
        Mage::getSingleton('customer/session')->setLastSuperOrder(null);
        /** @var Mage_Sales_Model_Convert_Order $orderConverter */
        $orderConverter = Mage::getModel('sales/convert_order');

        // Load all the Delivery orders
        /** @var Varien_Data_Collection $deliveryOrders */
        $deliveryOrders = $superOrder->getOrders(true)->getItems();
        if (!count($deliveryOrders)) {
            // TODO:  return error message that there are no delivery orders for this super Order
            throw new Exception('Super order does not have any delivery orders');
        }

        /** @var Omnius_Checkout_Model_Sales_Order $firstOrder */
        $firstOrder = array_shift($deliveryOrders);

        /** @var Omnius_Checkout_Model_Sales_Quote $newQuote */
        $newQuote = $orderConverter->toQuote($firstOrder)->save();
        /** @var Mage_Sales_Model_Order_Item $orderItem */

        /** @var Mage_Sales_Model_Order_Address $firstOrderShippingAddress */
        $firstOrderShippingAddress = clone $firstOrder->getShippingAddress();
        $firstOrderShippingAddress->unsetData('entity_id');
        $firstOrderShippingAddress->unsetData('parent_id');
        $firstOrderShippingAddress = Mage::getModel('sales/quote_address')
            ->addData($firstOrderShippingAddress->getData());

        /** @var Mage_Sales_Model_Order_Address $firstOrderBillingAddress */
        $firstOrderBillingAddress = clone $firstOrder->getBillingAddress();
        $firstOrderBillingAddress->unsetData('entity_id');
        $firstOrderBillingAddress->unsetData('parent_id');
        $firstOrderBillingAddress = Mage::getModel('sales/quote_address')
            ->addData($firstOrderBillingAddress->getData());

        $newQuote->setShippingAddress($firstOrderShippingAddress);
        $newQuote->setBillingAddress($firstOrderBillingAddress);

        $newQuote->setPayment($orderConverter->paymentToQuotePayment($firstOrder->getPayment()));

        array_unshift($deliveryOrders, $firstOrder);
        $addedPackages = $this->getAddedPackages($deliveryOrders);


        $superorderPackages = $superOrder->getPackages();
        $orderPackagesCount = $superorderPackages->getFirstItem()->getOneOfDeal() ? 1 : count($superorderPackages);

        foreach ($deliveryOrders as $order) {
            $this->parseDeliveryOrder($order, $cancelledPackages, $newQuote, $orderPackagesCount, $addedPackages, $quoteShippingData);
        }

        $newQuote
            ->setIsActive(false)
            ->setIsOrderEdit(true)
            ->setSuperOrderEditId($superOrder->getId());

        $packagesFromModel = Mage::getModel('package/package')
            ->getPackages($superOrder->getId());

        foreach ($packagesFromModel as $package) {
            $newPackage = Mage::getModel('package/package');
            $newPackage->setData($package->getData())
                ->setId(null)
                ->setOrderId(null)
                ->setQuoteId($newQuote->getId())
                ->save();

            if (
                $package->getStatus() == Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED ||
                ($package->getStatus() == Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_RETURNED && $package->getCancellationDate())
            ) {
                $cancelledPackages[] = $package->getPackageId();
            }
        }

        if ($remoteAddr = Mage::helper('core/http')->getRemoteAddr()) {
            $newQuote->setRemoteIp($remoteAddr);
            $xForwardIp = Mage::app()->getRequest()->getServer('HTTP_X_FORWARDED_FOR');
            $newQuote->setXForwardedFor($xForwardIp);
        }

        $newQuote
            ->setShippingData(Mage::helper('core')->jsonEncode($quoteShippingData))
            ->setStoreId(Mage::app()->getStore()->getId())
            ->save();

        Mage::getSingleton('customer/session')->setOrderEdit($newQuote->getId());

        foreach (array_unique($cancelledPackages) as $packageId) {
            Mage::helper('omnius_package')->updateCancelledPackages($packageId);
        }

        Mage::getSingleton('core/session')->setOrderIds(null);
        if ($redirect) {
            $this->_redirect('checkout/cart/');
        }
    }

    public function lockSuperorderAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $customerSession = Mage::getSingleton('customer/session');
        if($superOrderId = $this->getRequest()->get('orderId')) {
            /** @var Omnius_Checkout_Helper_Data $checkoutHelper */
            $checkoutHelper = Mage::helper('omnius_checkout');
            $state = $this->getRequest()->get('state');

            $agent = $customerSession->getAgent(true);

            $bool = true;

            if($state == 'locked') {
                $bool = false;

                $packageIds = $checkoutHelper->getModifiedPackages();
                $cancelledPackagesNow = Mage::helper('omnius_package')->getCancelledPackagesNow();

                $response = array(
                    'error' => false,
                    'message' => 'Order unlocked'
                );
                
                if (!empty($packageIds) || count($cancelledPackagesNow) > 0) {
                    $response['edited'] = 1;
                    Mage::helper('omnius_checkout')->exitSuperOrderEdit();
                }
            } else {
                $error = false;
                if($lockAgentId = $checkoutHelper->checkOrder($superOrderId, false)) {
                    if ($lockAgentId === 'crossChannel') {
                        // disable order lock/edit if the cross chanel edit is disabled (order created in Indirect)
                        $error = true;
                        $bool = false;
                        $message = $this->__('Changing orders created in the indirect channel is not permitted from other channels');
                    } else {
                        // if $lockAgentId === true (in override mode only), consider the order as locked
                        $message = $this->__('Order locked');
                    }
                    $response = array(
                        'error' => $error,
                        'message' => $message,
                        'locked' => true
                    );
                }
            }

            $customerSession->setOrderEditMode($bool);

        } else {
            $response = array(
                'error' => true,
                'message' => 'Order not found'
            );
        }
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(
                Mage::helper('core')->jsonEncode($response)
            );
    }

    public function exitChangeOrderAction()
    {
        Mage::helper('omnius_checkout')->exitSuperOrderEdit();
        $this->_redirect('/');
    }


    public function cancelEditOrderAction()
    {
        $redirect = $this->getRequest()->getParam('redirect', true);
        if (Mage::getSingleton('checkout/cart')->getQuote(true)->getSuperOrderEditId()) {
            Mage::helper('omnius_checkout')->releaseLockOrder(Mage::getSingleton('checkout/cart')->getQuote(true)->getSuperOrderEditId());
        }
        Mage::helper('omnius_checkout')->exitSuperOrderEdit();
        if ($redirect) {
            $this->_redirect('/');
        }
    }

    public function leavePageDialogSwitcherAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $data = $this->getRequest()->get('locked');
        $this->getCheckoutSession()->setLeavePageDialogSwitcher($data);
    }

    public function saveEditedQuoteAction()
    {
        if (Mage::getSingleton('checkout/session')->getIsEditMode()) {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $superQuote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
            /** @var Omnius_Checkout_Helper_Data $checkoutHelper */
            $checkoutHelper = Mage::helper('omnius_checkout');
            /**
             * Check if current order is not locked by another agent
             */
            $checkoutHelper->checkOrder($superQuote->getSuperOrderEditId());

            $quote->setSuperOrderEditId($superQuote->getSuperOrderEditId());
            $quote->setSuperQuoteId($superQuote->getId());
            $quote->setIsActive(0)->save();
            Mage::helper('omnius_package')->savePackageDifferences($quote->getActivePackageId());
            Mage::getSingleton('checkout/cart')->exitEditMode();
        }
        $this->_redirect('checkout/cart/');
    }

    public function indexAction()
    {
        try {
            if ($this->getCheckoutSession()->getLeavePageDialogSwitcher() === 'true') {
                // If the agent is pending for a polling screen clear the quote on page refresh
                Mage::helper('omnius_checkout')->exitSuperOrderEdit();
                $this->getCheckoutSession()->setLeavePageDialogSwitcher(false);
                Mage::helper('omnius_checkout')->createNewQuote();
                Mage::getSingleton('customer/session')->setLastSuperOrder(null);
            }

            // If customer not synced with DA
            if ((!Mage::getSingleton('customer/session')->getOrderEdit()
                    && Mage::getSingleton('customer/session')->getCustomer()->getId()
                    && !Mage::getSingleton('customer/session')->getCustomerInfoSync())
                || Mage::getSingleton('customer/session')->getCustomer()->isInMigration()
            ) {
                throw new Exception('redirect');
            }
            if (Mage::getSingleton('checkout/session')->getIsEditMode()) {
                $this->clearConfiguratorEditMode();
            }

            if ($this->_getQuote()->getCustomer()->getId()) {
                $customer = true;
            } else {
                $customer = false;
            }

            $request = $this->getRequest();
            $orderNo = $request->get('orderId');
            if (!empty($orderNo) && $customer && Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNo)) {
                if (!Mage::getSingleton('customer/session')->getOrderEdit()) {
                    Mage::getSingleton('core/session')->setGeneralError(
                        Mage::helper('omnius_checkout')->__('To deliver an order you must be in order edit mode.')
                    );
                    //redirect back to referer or home
                    throw new Exception('redirect');
                }
                throw new RuntimeException('render');
            }

            $this->parseQuote($request);
        } catch (RuntimeException $e) {
            // Do nothing
        } catch (Exception $e) {
            // Redirect back to referer or home
            $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
            $this->_redirectUrl($url);

            return;
        }
        $this->renderIndexLayout();
    }

    public function printAction()
    {
        try {
            $orderIncrementId = $this->getRequest()->getParam('order_id', false);
            $superOrderId = $this->getRequest()->getParam('superorder_id', false);
            if (!$orderIncrementId && !$superOrderId) {
                throw new Exception('no order found');
            }

            if ($orderIncrementId) {
                $order = Mage::getModel('sales/order')->getFirstNonEditedOrder($orderIncrementId);
                if (!$order->getId()) {
                    throw new Exception('no order found');
                }

                // Check if the superorder contract can be printed
                if (count(Mage::getModel('superorder/superorder')->belongsToCustomer($order->getSuperorderId())) <= 0) {
                    throw new Exception('no order found');
                }

                // Generate delivery contract for telesales store
                $printOutput = $this->getLayout()
                    ->createBlock('omnius_checkout/pdf_contract')
                    ->init($order)
                    ->setTemplate('checkout/cart/pdf/contract.phtml')
                    ->setSuperorder(Mage::getModel('superorder/superorder')->load($order->getSuperorderId()));
            } else {

                try {
                    $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId, 'order_number');
                    // Check if the superorder contract can be printed
                    if (count(Mage::getModel('superorder/superorder')->belongsToCustomer($superOrder->getId())) <= 0) {
                        $this->_redirect('/');

                        return false;
                    }
                } catch (Exception $e) {
                    // Do nothing
                }

                if (!$superOrder || !$superOrder->getId()) {
                    throw new Exception('no order found');
                }

                // Generate single contract for retail stores
                /** @var Omnius_Checkout_Block_Pdf_Contract $printOutput */
                $printOutput = Mage::app()->getLayout()
                    ->createBlock('omnius_checkout/pdf_contract')
                    ->setTemplate('checkout/cart/pdf/contract.phtml')
                    ->setSuperorder($superOrder)
                    ->superOrder($superOrder);
            }
            if (!$printOutput->getAllPackages()) {
                $this->getResponse()->setBody(
                    $this->__('The order content has just been changed, please reopen the order, blank page')
                    . '<br /><button type="button" onclick="window.open(\'\', \'_self\', \'\'); window.close();">'
                    . $this->__('Close window')
                    . '</button>'
                );

                return;
            }
        } catch (Exception $e) {
            $this->_redirect('/');

            return false;
        }

        $pdfInstance = new VFDOMPDF;

        $pdfInstance->load_html($printOutput->toHtml());
        $pdfInstance->render();
        $pdfOutput = $pdfInstance->output();

        $this->getResponse()->setHeader('Content-type', 'application/pdf');
        $this->getResponse()->setHeader('Content-Disposition', 'inline; filename="contract_' . ($orderIncrementId ? $order->getId() : $superOrder->getId()) . '.pdf"');
        $this->getResponse()->setHeader('Content-Length', strlen($pdfOutput));

        $this->getResponse()->setBody($pdfOutput);
    }

    /**
     * Get all the hardware items of the package
     * Return format JSON : [ [sku => name], [sku => name] ... ]
     */
    public function getPackageHardwareAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $packageId = $request->getPost('package_id');

                $superQuote = $this->_getQuote();
                /** @var Omnius_Package_Model_Package $package */
                $package = Mage::getModel('package/package')->getPackages($superQuote->getSuperOrderEditId(), null, $packageId)->getFirstItem();
                $response = [
                    'error'     => false,
                    'data'      => $package->getHardwareItems(),
                    'notice'    => $package->hasDOADeletedItems()
                ];

                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($response));

            } catch (Exception $e) {
                Mage::helper('omnius_core')->returnError($e);
                return;
            }
        }
    }

    public function beforeEditSuperorderPackageAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutSession = $this->getCheckoutSession();

                // Clear prev data
                $checkoutSession->unsDoaItems();
                $checkoutSession->unsRefundReason();

                $packageId = $request->getPost('package_id');
                $refundReason = $request->getPost('refund_reason');

                $checkoutSession->setRefundReason($refundReason);

                // Save refund reason for cancel package usage
                $cancelRefundReason = $checkoutSession->getCancelRefundReason() ?: [];
                $cancelRefundReason[$packageId] = $refundReason;

                $checkoutSession->setCancelRefundReason($cancelRefundReason);

                $itemsDoa = $request->getPost('item_doa', []);
                if (count($itemsDoa)) {
                    $checkoutSession->setDoaItems(serialize($itemsDoa));

                    // Save doa items for cancel package usage
                    $cancelDoaItems = $checkoutSession->getCancelDoaItems() ?: [];
                    $cancelDoaItems[$packageId] = serialize($itemsDoa);

                    $checkoutSession->setCancelDoaItems($cancelDoaItems);
                }

            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                Mage::helper('omnius_core')->returnError($e);
                return;
            }
        }
    }

    public function editSuperorderPackageAction()
    {
        /** @var Omnius_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('omnius_checkout');
        $packageId = $this->getRequest()->getParam('packageId');

        $force = $this->getRequest()->getParam('force', 0);
        if(!$packageId) {
            Mage::throwException('Invalid package id');
        }
        $session = Mage::getSingleton('customer/session');
        $cartSession = Mage::getSingleton('checkout/cart');
        $checkoutSession = $this->getCheckoutSession();
        $checkoutSession->unsInitialItems();
        if( ! $session->getOrderEditMode()) {
            $session = Mage::getSingleton('core/session');
            $session->setData('redirect_message', $this->__("You were redirected to home page because you left order edit mode"));
            $this->getResponse()
                ->setRedirect(Mage::getBaseUrl())
                ->sendResponse();
            exit;
        }

        /** @var Omnius_Checkout_Model_Sales_Quote $currentQuote */
        $currentQuote = Mage::getModel('sales/quote')->load($session->getOrderEdit());
        if ( ! $currentQuote->getId()) {
            Mage::throwException('No active super quote found.');
        }

        /**
         * Check if current order is not locked by another agent
         */
        $checkoutHelper->checkOrder($currentQuote->getSuperOrderEditId());

        $quoteItems = $currentQuote->getPackageItems($packageId);

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $session->getCustomer();

        $oldQuotePackage = Mage::getModel('package/package')
            ->getPackages(null, $currentQuote->getId(), $packageId)
            ->getFirstItem();

        /** @var Omnius_Checkout_Model_Sales_Quote $newQuote */
        $newQuote = Mage::getModel('sales/quote')
            ->setCustomer($customer)
            ->setBillingAddress(Mage::getModel('sales/quote_address')->setData($customer->getDefaultBillingAddress() ? $customer->getDefaultBillingAddress()->getData() : []))
            ->setShippingAddress(Mage::getModel('sales/quote_address')->setData($customer->getDefaultShippingAddress() ? $customer->getDefaultShippingAddress()->getData() : []))
            ->setSuperQuoteId($currentQuote->getId())
            ->setStoreId($currentQuote->getStoreId())
            ->setIsOverrulePromoMode($oldQuotePackage->isDelivered())
            ->save();

        $doaItems = $checkoutSession->getDoaItems() ? unserialize($checkoutSession->getDoaItems()) : [];
        $hwItemCount = 0;
        $initialItems = [];
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $quoteItem */
        foreach ($quoteItems as $quoteItem) {
            if ($oldQuotePackage->isDelivered()) {
                $initialItems[$quoteItem->getProductId()] = $quoteItem->getSku();
            } elseif ($quoteItem->isPromo()) {
                continue;
            }

            if ($quoteItem->getProduct()->isOfHardwareType()) {
                $hwItemCount++;
            }
            Mage::getModel('sales/quote_item')
                ->setData($quoteItem->getData())
                ->unsetData('item_id')
                ->setItemDoa(in_array($quoteItem->getSku(), $doaItems) ? 1 : null)
                ->setQuote($newQuote)
                ->save();
        }
        $checkoutSession->setInitialItems($initialItems ?: false);
        $hasDoa = count($doaItems);

        $refundReason = $checkoutSession->getRefundReason();

        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')
            ->setData($oldQuotePackage->getData())
            ->setId(null)
            ->setQuoteId($newQuote->getId())
            ->setOrderId(null)
            ->setRefundReason($refundReason)
            ->save();

        $superOrderEditId = ($this->_shouldShowConfigurator($refundReason, $hasDoa)) ? null : $currentQuote->getSuperOrderEditId();

        // When change before delivery, make sure prices are updated accordingly
        if (!$oldQuotePackage->isDelivered()) {
            foreach ($newQuote->getAllVisibleItems() as $item) {
                Mage::dispatchEvent(
                    'checkout_cart_clone_product_after',
                    ['product' => $item->getProduct(), 'quote_item' => $item]
                );
            }
        }

        $newQuote->setIsSuperMode(true)
            ->setActivePackageId($packageId)
            ->setCurrentPackageId($packageId)
            ->setSuperOrderEditId($superOrderEditId)
            ->setTotalsCollectedFlag(false)
            ->collectTotals()
            ->save();
        $newQuote->unsIsOverrulePromoMode();
        $newQuote = Mage::getResourceModel('sales/quote_collection')
            ->addFieldToFilter('entity_id', $newQuote->getId())
            ->getFirstItem();

        $cartSession->setEditMode($newQuote);

        $checkoutSession->unsDoaItems();
        $checkoutSession->unsRefundReason();
        $packageModel
            ->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED === Mage::helper('omnius_checkout')->checkPackageStatus($packageId)
                ? Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED
                : Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN)
            ->save();
            
        if ($this->_shouldShowConfigurator($refundReason, $hasDoa) || $force) {
            $this->_redirectUrl(trim(Mage::getBaseUrl(), '/') . '?packageId='. $packageId);
        } else {
            Mage::helper('omnius_package')->savePackageDifferences($newQuote->getActivePackageId());
            $this->_redirectUrl(Mage::getUrl('checkout/cart'));
        }
    }

    /**
     * Check if the configurator is needed in DOA flow
     *
     * @param string $reason
     * @param bool $allDoaItems
     * @return bool
     */
    private function _shouldShowConfigurator($reason, $hasDoa)
    {
        switch ($reason) {
            case 'ONTEVREDEN':
            case 'DOA/SWAP':
                return true;
            default:
                return !$hasDoa;
        }
    }

    /**
     * Cancel configurator edit mode
     */
    public function cancelEditSuperorderPackageAction()
    {
        /** @var Omnius_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        Mage::helper('pricerules')->decrementAll($quote);
        $quote->setSuperQuoteId(null)
            ->setSuperOrderEditId(null);
        $this->clearConfiguratorEditMode();

        $this->_redirect('checkout/cart');
    }

    public function cancelDeliverSuperorderAction()
    {
        Mage::helper('omnius_checkout')->exitSuperOrderEdit();

        $this->_redirect('/');
    }

    /**
     * @throws Exception
     *
     * Removes a package from the cart
     */
    public function removePackageAction()
    {
        //store current packages as cancelled now to know this wasn't cancelled in previous changes
        Mage::helper('omnius_package')->updateCancelledPackagesNow($this->getRequest()->getParam('packageId'));
        $this->_redirect('checkout/cart');
    }

    /**
     * Un-sets the packages that are set to be removed
     */
    public function stopRemovePackageAction()
    {
        Mage::helper('omnius_package')->resetCancelledPackagesNow();
        Mage::getSingleton('customer/session')->unsCancelledPackagesNow();
        $this->_redirect('checkout/cart');
    }

    /**
     * @param $orderConverter
     * @param $orderItem
     * @param $newQuote
     * @param $order
     *
     * Convert order item to quote item
     */
    private function convertOrderItemToQuoteItem($orderItem, $newQuote, $order)
    {
        if ($orderItem->getProduct()->getId()) {
            /** @var Omnius_Checkout_Model_Sales_Quote_Item $quoteItem */
            $quoteItem = Mage::getModel('sales/quote_item')
                ->setStoreId($orderItem->getOrder()->getStoreId())
                ->setQuote($newQuote)
                ->setEditOrderId($order->getId())
                ->setCustomPrice($orderItem->getData('row_total'))
                ->setOriginalCustomPrice($orderItem->getData('row_total_incl_tax'))
                ->setProductId($orderItem->getProductId())
                ->setParentProductId($orderItem->getParentProductId())
                ->setQuoteItemId($orderItem->getId());

            Mage::helper('core')->copyFieldset('sales_convert_order_item_editorder', 'to_quote_item', $orderItem, $quoteItem);
            $quoteItem->save();
        }
    }

    /**
     * Set shipping data to display on the order display
     *
     * @param $quoteShippingData
     * @param Omnius_Checkout_Model_Sales_Order $order
     * @param int $packageId
     * @param int $multiplePackages
     */
    private function setShippingData(&$quoteShippingData, $order, $packageId, $multiplePackages = null)
    {
        if($multiplePackages && $multiplePackages>1) {
            if (! isset($quoteShippingData['pakket'][$packageId])) {
                $shippingData = $this->getShippingDataArray($order);
                $quoteShippingData['pakket'][$packageId] =  $shippingData['deliver'];
                $quoteShippingData['payment'][$packageId] = $shippingData['payment'];
            }
        } else {
            $quoteShippingData = $this->getShippingDataArray($order);
        }
    }

    /**
     * Create quote shipping data array from order information
     *
     * @param Omnius_Checkout_Model_Sales_Order $order
     * @return array
     */
    private function getShippingDataArray($order)
    {
        $shippingData = array(
            'deliver' => array(
                'address' => array(
                    'street' => $order->getShippingAddress()->getStreet(),
                    'postcode' => $order->getShippingAddress()->getPostcode(),
                    'city' => $order->getShippingAddress()->getCity(),
                    'address' => $order->getShippingAddress()->getDeliveryType(),
                    'store_id' => $order->getShippingAddress()->getDeliveryStoreId()
                )
            ),
            'payment' => $order->getPayment()->getMethod()
        );

        return $shippingData;
    }

    /**
     * Just render the layout for the indexAction
     */
    private function renderIndexLayout()
    {
        $this
            ->loadLayout()
            ->_initLayoutMessages('checkout/session')
            ->_initLayoutMessages('catalog/session')
            ->getLayout()->getBlock('head')->setTitle($this->__('Shopping Cart'));
        $this->renderLayout();
    }

    /**
     * @param $quote
     *
     * Keep offer quote in session in order to overwrite any possible changes
     */
    public function rememberData($quote)
    {
        Mage::getSingleton('customer/session')->setOfferteData($quote);
    }
    
    /**
     * Cancel configurator edit mode
     */
    protected function clearConfiguratorEditMode()
    {
        /** @var Omnius_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        if($quote) {
            $quote->setIsActive(0)
                ->save();
        }

        Mage::getSingleton('checkout/session')->setIsEditMode(false);
    }

    /**
     * Restart home delivery process
     */
    public function restartHomeDeliveryAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $orderNumber = $this->getRequest()->getPost('orderNumber');
        $deliveryOrderId = $this->getRequest()->getPost('deliveryId');
        $deliverOrder = Mage::getModel('sales/order')->load($deliveryOrderId);
        $superorder = Mage::getModel('superorder/superorder')->load($deliverOrder->getSuperorderId());

        if ($deliverOrder->getId()) {

            // Create a new delivery order used to restart home delivery
            try {
                /** @var Omnius_Checkout_Model_Sales_Order $newOrder */
                $newOrder = Mage::getModel('sales/order');

                $newOrder->addStatusHistoryComment($deliverOrder->getStatus(), '[Notice] - Restart home delivery');
                $newOrder->setData($deliverOrder->getData());
                $newOrder->unsetData('parent_id');
                $newOrder->unsetData('entity_id');
                $newOrder->unsetData('increment_id');
                $newOrder->unsetData('contract_sign_date');
                $newOrder->unsetData('status');
                $newOrder->unsetData('reason_code');
                $newOrder->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);

                foreach ($deliverOrder->getAllItems() as $item) {
                    $orderItem = Mage::getModel('sales/order_item');
                    $orderItem->setData($item->getData());
                    $orderItem->unsetData('item_id');
                    $orderItem->unsetData('order_id');
                    $orderItem->setOrder($newOrder);

                    $newOrder->addItem($orderItem);
                }

                $address = Mage::getModel('sales/order_address');
                $address->setData($deliverOrder->getBillingAddress()->getData());
                $address->unsetData('entity_id');
                $address->unsetData('parent_id');
                $address->setStreet($address->getStreet());
                $newOrder->setBillingAddress($address);

                $address = Mage::getModel('sales/order_address');
                $address->setData($deliverOrder->getShippingAddress()->getData());
                $address->unsetData('entity_id');
                $address->unsetData('parent_id');
                $address->setStreet($address->getStreet());
                $newOrder->setShippingAddress($address);

                $payment = Mage::getModel('sales/order_payment');
                $payment->setData($deliverOrder->getPayment()->getData());
                $payment->unsetData('entity_id');
                $payment->unsetData('parent_id');
                $newOrder->setPayment($payment);

                $newOrder->setEdited(0);
                $newOrder->setEcomStatus(null);
                $newOrder->save();

                if ($newOrder->getId()) {
                    // Set old delivery order as edited
                    $deliverOrder->setEdited(1)->save();

                    // Regenerate contracts because phone numbers might have changed
                    /** @var Omnius_Checkout_Helper_Pdf $pdfHelper */
                    $pdfHelper = Mage::helper('omnius_checkout/pdf');
                    $printOutput = Mage::app()->getLayout()
                        ->createBlock('omnius_checkout/pdf_contract')
                        ->setTemplate('checkout/cart/pdf/contract.phtml')
                        ->setSuperorder($superorder)
                        ->init($newOrder)
                        ->toHtml();
                    $pdfInstance = new VFDOMPDF;
                    $pdfInstance->load_html($printOutput);
                    $pdfInstance->render();
                    $pdfOutput = $pdfInstance->output();

                    $pdfHelper->saveContract($pdfOutput, $newOrder->getIncrementId(), 'pdf', false);

                    $response = array(
                        'error' => false,
                        'message' => sprintf($this->__('The packages for home delivery (%s) have been relisted.%s New home delivery ID: %s'), $deliverOrder->getIncrementId(), "<br>", $newOrder->getIncrementId()),
                    );

                } else {
                    $response = array(
                        'error' => true,
                        'message' => $this->__('Could not create a new delivery order'),
                    );
                }

            } catch (Exception $e) {
                Mage::helper('omnius_core')->returnError($e);
                return;
            }
        } else {
            $response = array(
                'error' => true,
                'message' => $this->__('Delivery order not found'),
            );
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function changeCustomerEmailAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $email = $this->getRequest()->getParam('email');

        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        $quote->setAdditionalEmail($email);
        $this->_getCart()->save();

        $output = $this->getLayout()->createBlock('core/template')->setTemplate('checkout/cart/steps/save_overview.phtml')->toHtml();

        $this->getResponse()->setBody($output);
    }

    /**
     * @param $request
     * @throws Exception
     */
    protected function parseQuote($request)
    {
        if (Mage::getSingleton('customer/session')->getOrderEdit() && !$request->isPost()) {
            throw new RuntimeException('render');
        }
        if ($request->isPost()) {
            Mage::getSingleton('customer/session')->unsLastSuperOrder();
            Mage::getSingleton('customer/session')->unsOrderEdit();
            Mage::helper('omnius_checkout')->processNotesAndPickup($request);
        }

        $complete = Mage::helper('omnius_checkout')->checkOneCompletedPackage();
        if (!$complete) {
            Mage::getSingleton('core/session')->setGeneralError(Mage::helper('omnius_checkout')->__('You must have at least one complete package'));
            //redirect back to referer or home
            throw new Exception('redirect');
        }

        // Make sure there are no incomplete packages when arriving to the checkout
        Mage::helper('omnius_checkout')->splitQuote();

        /**
         * No need to recollect totals as when arriving on this page,
         * the totals are already collected
         */
        $this->_getQuote()->setTotalsCollectedFlag(true);

        if ($this->_getQuote()->getCartStatusStep() != Omnius_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CHECKOUT) {
            $this->_getCart()->getQuote()->updateFields(array('cart_status_step' => Omnius_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CHECKOUT));
        }
    }

    /**
     * @param $addedPackages
     * @param $order
     * @param $orderPackagesCount
     * @param $newQuote
     * @param $cancelledPackages
     * @param $quoteShippingData
     */
    protected function parseOldOrder($addedPackages, $order, $orderPackagesCount, $newQuote, &$cancelledPackages, &$quoteShippingData)
    {
        $oldAddedPackages = $addedPackages;
        $oldOrder = Mage::getModel('sales/order')->load($order->getParentId());
        while ($oldOrder) {
            $tmpAddedPackages = [];
            foreach ($oldOrder->getAllItems() as $oldOrderItem) {
                if (in_array($oldOrderItem->getPackageId(), $oldAddedPackages) === false) {
                    $this->setShippingData($quoteShippingData, $oldOrder, $oldOrderItem->getPackageId(), $orderPackagesCount);
                    $this->convertOrderItemToQuoteItem($oldOrderItem, $newQuote, $order);
                    $tmpAddedPackages[] = $oldOrderItem->getPackageId();
                    $package = Mage::getModel('package/package')
                        ->getCollection()
                        ->addFieldToSelect('status')
                        ->addFieldToFilter('order_id', $oldOrder->getSuperorderId())
                        ->addFieldToFilter('package_id', $oldOrderItem->getPackageId())
                        ->getFirstItem();

                    if ($package->getStatus() == Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED && in_array($oldOrderItem->getPackageId(), $cancelledPackages) === false) {
                        $cancelledPackages[] = $oldOrderItem->getPackageId();
                    }
                }
            }
            $tmpAddedPackages = array_unique($tmpAddedPackages);
            $oldAddedPackages = array_merge($oldAddedPackages, $tmpAddedPackages);
            $oldOrder = $oldOrder->getParentId() ? Mage::getModel('sales/order')->load($oldOrder->getParentId()) : null;
        }
    }

    /**
     * @param $order
     * @param $cancelledPackages
     * @param $newQuote
     * @param $orderPackagesCount
     * @param $addedPackages
     * @param $quoteShippingData
     */
    protected function parseDeliveryOrder($order, &$cancelledPackages, $newQuote, $orderPackagesCount, $addedPackages, &$quoteShippingData)
    {
        foreach ($order->getAllItems() as $orderItem) {
            // If order is cancelled, set it's packages as cancelled
            if (strtolower($order->getStatus()) == strtolower(Omnius_Checkout_Model_Sales_Order::STATUS_CANCELLED) && in_array($orderItem->getPackageId(), $cancelledPackages) === false) {
                $cancelledPackages[] = $orderItem->getPackageId();
            }
            $this->convertOrderItemToQuoteItem($orderItem, $newQuote, $order);
            $this->setShippingData($quoteShippingData, $order, $orderItem->getPackageId(), $orderPackagesCount);
        }
        // Add also possible previously cancelled packages
        if ($order->getParentId()) {
            $this->parseOldOrder($addedPackages, $order, $orderPackagesCount, $newQuote, $cancelledPackages, $quoteShippingData);
        }
    }

    /**
     * @param $deliveryOrders
     * @return array
     */
    protected function getAddedPackages($deliveryOrders)
    {
        /** @var Omnius_Checkout_Model_Sales_Order $order */
        $addedPackages = array();
        foreach ($deliveryOrders as $order) {
            foreach ($order->getAllItems() as $orderItem) {
                if (in_array($orderItem->getPackageId(), $addedPackages) !== false) {
                    continue;
                }
                $addedPackages[] = $orderItem->getPackageId();
            }
        }
        $addedPackages = array_unique($addedPackages);

        return $addedPackages;
    }

    /**
     * @param $params
     * @return mixed
     */
    protected function getCartQuote($params)
    {
        $quote = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('entity_id', $params['quote_id'])
            ->getFirstItem();

        if (!$quote) {
            throw new LogicException($this->__('The specified quote could not be found'));
        }

        // If the quote has an order it means it was already completed, and can't be loaded again
        if ($quote->getReservedOrderId()) {
            throw new LogicException($this->__('Quote was already completed'));
        }

        return $quote;
    }

    /**
     * @param $var
     * @param $k
     * @param $validators
     * @param $errors
     */
    protected function validateFormKey($var, $k, $validators, &$errors)
    {
        $var = trim($var);

        if ($k == 'dob') {
            $k = 'date of birth';
            if (!$validators->validateIsDate($var)) {
                $errors[] = Mage::helper('omnius_checkout')->__(
                    'The date format is invalid. Please use the following format: dd-mm-yyyy'
                );
            }

            if (!$validators->validateMinAge($var)) {
                $errors[] = Mage::helper('omnius_checkout')->__(
                    'The customer must be older than 18 years.'
                );
            }

            if (!$validators->validateMaxAge($var)) {
                $errors[] = Mage::helper('omnius_checkout')->__(
                    'The customer must be younger than 100 years.'
                );
            }
        } else {

            if (!$validators->validateName($var)) {
                $errors[] = Mage::helper('omnius_checkout')->__(
                    sprintf('The %s field contains invalid characters.', $k)
                );
            }
        }

        if ($k != 'initial' && ($var == null || empty($var))) {
            $errors[] = sprintf('The %s field is empty.', Mage::helper('omnius_checkout')->__($k));
        }
    }

    /**
     * @param $vars
     * @param $quotePackages
     */
    protected function createProspectFromCart($vars, $quotePackages)
    {
        switch ($vars['title']) {
            case 2:
                $prefix = Mage::helper("omnius_checkout")->__('Mrs.');
                break;
            case 1:
                $prefix = Mage::helper("omnius_checkout")->__('Mr.');
                break;
            default:
                $prefix = '';
                break;
        }

        //If cart is empty, no customer should be saved
        /** @var Omnius_Customer_Model_Customer_Customer $customer */
        if (count($quotePackages) >= 1) {
            $customer = Mage::getModel('customer/customer')
                ->setFirstname($vars['firstname'])
                ->setLastname($vars['lastname'])
                ->setDob($vars['dob'])
                ->setAdditionalEmail($this->getRequest()->get('email_send'))
                ->setGender($vars['title'])
                ->setPrefix($prefix)
                ->setMiddlename($vars['initial'])
                ->setEmail(uniqid() . Omnius_Customer_Model_Customer_Customer::DEFAULT_EMAIL_SUFFIX)
                ->setIsProspect(true)
                ->save();
            Mage::getSingleton('customer/session')->setCustomer($customer);
            Mage::getSingleton('customer/session')->setCustomerInfoSync($customer->getIsProspect());
        }
    }

    /**
     * @param $theQuote
     * @return array
     */
    protected function sendCartEmail($theQuote)
    {
        if ($this->getRequest()->get('email_send', null) != null) {
            $address = $this->getRequest()->get('email_send');
        } else {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $address = $customer->getCorrespondanceEmail();
        }
        
        return array(
            'error' => false,
            'message' => $this->__('Saved and sent') . ' (' . $address . ')',
        );
    }
}
