<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Observer
 */
class Omnius_Checkout_Model_Observer extends Mage_Payment_Model_Observer
{

    /**
     * Set forced canCreditmemo flag
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Payment_Model_Observer
     */
    public function salesOrderBeforeSave($observer)
    {
        /** @var Omnius_Checkout_Model_Sales_Order $order */
        $order = $observer->getEvent()->getOrder();
        $isNotFree = $order->getPayment() && $order->getPayment()->getMethodInstance() && ($order->getPayment()->getMethodInstance()->getCode() != 'free');
        $isNotClosed = $order->isCanceled() || ($order->getState() === Mage_Sales_Model_Order::STATE_CLOSED);
        /**
         * Allow forced creditmemo just in case if it wasn't defined before
         */
        if (!$isNotFree && !$order->canUnhold() && !$isNotClosed && !$order->hasForcedCanCreditmemo()) {
            $order->setForcedCanCreditmemo(true);
        }

        return $this;
    }

    /**
     * Removes previous service items
     * @param $observer
     */
    public function removeServiceItems($observer)
    {
        $quote = $observer->getQuote();
        $existing = array();
        $quoteItems = $quote->getAllItems();
        foreach ($quoteItems as $i) {
            $existing[$i->getPackageId()][] = $i->getProductId();
        }

        foreach ($quoteItems as $item) {
            if ($item->getProduct()->isServiceItem() && !in_array($item->getTargetId(), $existing[$item->getPackageId()])) {
                $quote->removeItem($item->getId());
            }
        }
    }

    /**
     * Rewrites the order of the Adyen total collectors.
     * @param $observer
     */
    public function rewritePaymentFeeCalculation($observer)
    {
        $config = Mage::getConfig();
        $adyenNode = $config->getNode()->xpath('//modules/Adyen_Payment/active');
        if (is_array($adyenNode) && count($adyenNode)) {
            $adyenNode = trim((string)$adyenNode[0]);
            if ($adyenNode == 'true') {
                $paymentFeeNode = $config->getNode()->xpath('global/sales/quote/totals/payment_fee');
                unset($paymentFeeNode[0]->before);
                $paymentFeeNode[0]->after[0] = 'maf_grand_total';
            }
        }
    }

    /**
     * Clean the reason code cache.
     */
    public function cleanReasonCodeCache()
    {
        Mage::app()->getCacheInstance()->getFrontend()->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(Omnius_Checkout_Model_Reason::REASON_TAG));
    }
}
