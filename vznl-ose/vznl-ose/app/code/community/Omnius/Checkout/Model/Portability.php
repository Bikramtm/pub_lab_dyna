<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Shopping cart model
 *
 * @category    Omnius
 * @package     Omnius_Checkout
 */
class Omnius_Checkout_Model_Portability extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("omnius_checkout/portability");
    }

    public function removePortabilityInfoWithCatalogPackageId($catalogPackageId)
    {
        $portabilityExisting = $this->getCollection()
            ->addFieldToFilter('catalog_package_id', $catalogPackageId);
        foreach ($portabilityExisting as $item) {
            // @codingStandardsIgnoreStart
            $item->delete();
            // @codingStandardsIgnoreEnd
        }
        return true;
    }

    public function insertPortabilityInfo($info)
    {
        foreach ($info as $arr) {
            $this->setData($arr);
            // @codingStandardsIgnoreStart
            $this->save();
            // @codingStandardsIgnoreEnd
        }
    }

    public function retrievePortingInfo($quote_id, $package_id = null)
    {
        $collection = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote_id);
        // to recieve packages which has valid portability info
        $collection->addFieldToFilter('portability.entity_id', ['neq' => 'NULL']);

        if ($package_id) {
            $collection->addFieldToFilter('package_id', $package_id);
        }

        $portability = Mage::getSingleton('core/resource')->getTableName('omnius_checkout/portability');

        $collection->getSelect()->joinLeft(array('portability' => $portability), '`main_table`.`entity_id` = `portability`.`catalog_package_id`');

        return $collection->getData();
    }
}
