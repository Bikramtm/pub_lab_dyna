<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
// @codingStandardsIgnoreFile

class Omnius_Checkout_Model_Mysql4_Portability_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Contructor override
     */
    public function _construct()
    {
        $this->_init("omnius_checkout/portability");
    }
}