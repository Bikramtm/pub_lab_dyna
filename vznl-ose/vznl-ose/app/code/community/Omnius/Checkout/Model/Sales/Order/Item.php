<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Model_Sales_Order_Item
 * @method      int getPackageId()
 * @method      float|null getMaf()
 * @method      float|null getMafInclTax()
 * @method      float|null getMafDiscountAmount()
 * @method      float|null getMafRowTotal()
 * @method      float|null getMafRowTotalInclTax()
 * @method      float|null getHiddenMafTaxAmount()
 * @method      int getPrijsPromoDuurMaanden()
 */
class Omnius_Checkout_Model_Sales_Order_Item extends Mage_Sales_Model_Order_Item
{
    public static $_field_overwrite = array(
        'maf',
        'base_maf',
        'maf_incl_tax',
        'base_maf_incl_tax',
        'maf_row_total',
        'maf_tax_amount',
        'maf_row_total_incl_tax',
        'base_maf_row_total_incl_tax',
        'base_maf_tax_amount',
        'price',
        'base_price',
        'original_price',
        'row_total',
        'base_row_total',
        'tax_amount',
        'base_tax_amount',
        'hidden_maf_tax_amount',
        'base_hidden_maf_tax_amount',
        'connection_cost',
        'base_weee_tax_applied_amount',
        'base_weee_tax_applied_row_amnt',
        'weee_tax_applied_amount',
        'weee_tax_applied_row_amount',
        'weee_tax_applied',
        'weee_tax_disposition',
        'weee_tax_row_disposition',
        'base_weee_tax_disposition',
        'base_weee_tax_row_disposition',
        'maf_discount_amount',
        'base_maf_discount_amount',
        'maf_tax_before_discount',
        'base_maf_tax_before_discount',
        'applied_maf_promo_amount',
        'base_applied_maf_promo_amount',
        'mixmatch_subtotal',
        'mixmatch_tax'
    );

    /**
     * @param bool $customFormat
     * @return bool|string
     *
     * Return custom date format when needed
     */
    public function getContractEndDate($customFormat = false)
    {
        return $customFormat 
            ? date('c', strtotime(parent::getContractEndDate())) 
            : parent::getContractEndDate();
    }

    /**
     * @return float
     */
    public function getFinalPrice()
    {
        $priceWithTax = 0;
        if ($this->getRowTotalInclTax()) {
            $priceWithTax = $this->getRowTotalInclTax();
        }

        $discount = 0;
        if ($this->getDiscountAmount()) {
            $discount = $this->getDiscountAmount();
        }

        $weeeTaxApplied = unserialize($this->getWeeeTaxApplied());
        $taxes = 0;
        if (!empty($weeeTaxApplied)) {
            foreach ($weeeTaxApplied as $taxApplied) {
                $taxes += $taxApplied['base_amount'];
            }
        }

        return (float)$priceWithTax - $discount + $taxes;
    }

    /**
     * @return string|void
     */
    public function getWeeeTaxAppliedUnserialized()
    {
        $weeeTaxField = $this->_getData('weee_tax_applied');
        if ($weeeTaxField) {
            return unserialize($weeeTaxField);
        }
        return array();
    }

    /**
     * Returns the fixed product tax including VAT
     * @return float|int
     */
    public function getFixedProductTaxInclVat(){
        $total = 0;

        foreach($this->getWeeeTaxAppliedUnserialized() as $tax){
            $total += $tax['amount_incl_tax'];
        }

        return $total;
    }

    /**
     * Check if order item is promotional
     * @return bool
     */
    public function isPromo() {
        return $this->getData('is_promo') || $this->getProduct()->isPromo();
    }

    /**
     * @return Omnius_Catalog_Model_Product
     */
    public function getProduct()
    {
        if (!$this->getData('product')) {
            /** @var Omnius_Catalog_Model_Product $product */
            $product = null;
            $product = Mage::getModel('catalog/product')->load($this->getProductId());
            $this->setProduct($product);

            if ($product->getId() === null) {
                $product = $this->getDeletedProduct();
                $this->setProduct($product);
            }
        }

        return $this->getData('product');
    }

    /**
     * @return Omnius_Catalog_Model_Product
     */
    public function getDeletedProduct()
    {
        /** @var Omnius_Catalog_Model_Product $deletedProduct */
        $deletedProduct = Mage::getModel('catalog/product');
        $aItemPrices = Mage::helper('core')->jsonDecode($this->getOrderItemPrices());
        $deletedProduct->setIsDeleted(true);
        $deletedProduct->setData('old_id', $this->getProductId());
        $deletedProduct->setType(array('0' => $this->getPackProductType()));
        $deletedProduct->setSku($this->getSku());
        $deletedProduct->setHasOptions($this->getProductOptions());
        $deletedProduct->setCreatedAt($this->getCreatedAt());
        $deletedProduct->setStatus($this->getCustomStatus());
        $deletedProduct->setUpdateAt($this->getUpdatedAt());
        $deletedProduct->setStatus($this->getCustomStatus());
        $deletedProduct->setName($this->getName());
        $deletedProduct->setPrijsAansluitPromoBedrag(isset($aItemPrices['product']['prijs_aansluit_promo_bedrag']) ?: 0);
        $deletedProduct->setPrijsAansluitPromoProcent(isset($aItemPrices['product']['prijs_aansluit_promo_procent']) ?: 0);
        $deletedProduct->getPrijsMafDiscountPercent(isset($aItemPrices['product']['prijs_maf_discount_percent']) ?: 0);
        $deletedProduct->getPrijsMafNewAmount(isset($aItemPrices['product']['prijs_maf_new_amount']) ?: 0);
        $deletedProduct->setGiftMessageAvailable($this->getGiftMessageId());
        $deletedProduct->setData('purged_product', true);
        $deletedProduct->setIsPromo($this->getIsPromo());
        if ($aItemPrices) {
            $deletedProduct->setTaxClassId($aItemPrices['product']['tax_class_id']);
            $deletedProduct->setPrice($aItemPrices['price']);
            $deletedProduct->setSpecialPrice($aItemPrices['product']['special_price']);
        }

        return $deletedProduct;
    }

    /**
     * Trigger beforeSave event to handle offers
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        /** @var Omnius_Checkout_Model_Sales_Quote $tmpQuote */
        $tmpQuote = Mage::getModel('sales/quote')->load($this->getOrder()->getQuoteId());
        if($tmpQuote->getId() && $tmpQuote->getIsOffer()){
            foreach($tmpQuote->getAllItems() as $quoteItem){
                if($quoteItem->getPackageId() == $this->getPackageId() && $quoteItem->getProductId() == $this->getProductId()){
                    foreach(self::$_field_overwrite as $field){
                        $this->setData($field, $quoteItem->getData($field));
                    }
                }
            }
        }
    }

    /**
     * @param bool|true $abs
     * @return int
     */
    public function getTotalPaidByMaf($abs = true)
    {
        $subscription = null;
        $subscriptionProduct = null;
        /** @var Omnius_Checkout_Model_Sales_Order_Item $item */
        foreach ($this->getOrder()->getItemsCollection() as $item) {
            /** @var Omnius_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            if (
                $product->is(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)
                && $item->getPackageId() == $this->getPackageId()
            ) {
                $subscription = $item;
                $subscriptionProduct = $product;
                break;
            }
        }

        $multiplier = $abs ? 1 : -1;
        return $subscriptionProduct && $subscription && $subscription->getProductId()
            ? $subscriptionProduct->getSubscriptionDuration() * $this->getItemFinalMafInclTax() * $multiplier
            : 0;
    }

    /**
     * @return float|null
     */
    public function getItemFinalMafInclTax()
    {
        return $this->getMafRowTotalInclTax() - $this->getMafDiscountAmount();
    }

    /**
     * @return float|null
     */
    public function getItemFinalMafExclTax()
    {
        return $this->getMafRowTotal() - ($this->getMafDiscountAmount() - $this->getHiddenMafTaxAmount());
    }

    /**
     * @return float
     */
    public function getItemFinalPriceInclTax()
    {
        return $this->getRowTotalInclTax() - $this->getDiscountAmount();
    }

    /**
     * @return float
     */
    public function getItemFinalPriceExclTax()
    {
        return $this->getRowTotal() - ($this->getDiscountAmount() - $this->getHiddenTaxAmount());
    }
}
