<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Model_SalesRule_Resource_Rule
 */
class Omnius_Checkout_Model_SalesRule_Resource_Rule extends Mage_SalesRule_Model_Resource_Rule
{
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Get all existing rule labels
     *
     * @param int $ruleId
     * @return array
     */
    public function getStoreLabels($ruleId)
    {
        $key = sprintf('salesrules_resource_store_labels_%s', $ruleId);
        if (!($result = (unserialize($this->getCache()->load($key))))) {
            $select = $this->_getReadAdapter()->select()
                ->from($this->getTable('salesrule/label'), array('store_id', 'label'))
                ->where('rule_id = :rule_id');
            $result = $this->_getReadAdapter()->fetchPairs($select, array(':rule_id' => $ruleId));
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Get rule label by specific store id
     *
     * @param int $ruleId
     * @param int $storeId
     * @return string
     */
    public function getStoreLabel($ruleId, $storeId)
    {
        $key = sprintf('salesrules_resource_store_label_%s_%s', $ruleId, $storeId);
        if (!($result = (unserialize($this->getCache()->load($key))))) {
            $select = $this->_getReadAdapter()->select()
                ->from($this->getTable('salesrule/label'), 'label')
                ->where('rule_id = :rule_id')
                ->where('store_id IN(0, :store_id)')
                ->order('store_id DESC');
            $result = $this->_getReadAdapter()->fetchOne($select, array(':rule_id' => $ruleId, ':store_id' => $storeId));
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Return codes of all product attributes currently used in promo rules for specified customer group and website
     *
     * @param int $websiteId
     * @param int $customerGroupId
     * @return mixed
     */
    public function getActiveAttributes($websiteId, $customerGroupId)
    {
        $key = sprintf('salesrules_resource_active_attr_%s_%s', $websiteId, $customerGroupId);
        if (!($result = (unserialize($this->getCache()->load($key))))) {
            $read = $this->_getReadAdapter();
            $select = $read->select()
                ->from(array('a' => $this->getTable('salesrule/product_attribute')),
                    new Zend_Db_Expr('DISTINCT ea.attribute_code'))
                ->joinInner(array('ea' => $this->getTable('eav/attribute')), 'ea.attribute_id = a.attribute_id', array());
            $result = $read->fetchAll($select);
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Collect all product attributes used in serialized rule's action or condition
     *
     * @param string $serializedString
     *
     * @return array
     */
    public function getProductAttributes($serializedString)
    {
        $key = md5($serializedString);
        if (!($result = (unserialize($this->getCache()->load($key))))) {
            $result = [];
            if (preg_match_all('~s:32:"salesrule/rule_condition_product";s:9:"attribute";s:\d+:"(.*?)"~s',
                $serializedString, $matches)) {
                foreach ($matches[1] as $attributeCode) {
                    $result[] = $attributeCode;
                }
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }
}
