<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Model_Sales_InlifeOrder
 */
class Omnius_Checkout_Model_Sales_InlifeOrder extends Varien_Object
{
    /**
     * orderDescription and flow
     * @var array
     */
    protected $_descriptionMapping = array(
        'PR' => 'Provide',
        'CH' => 'Change',
        'SU' => 'Suspend',
        'RS' => 'Resume',
        'CE' => 'Cease',
        'ES' => 'Reestablish',
        'CW' => 'Change Ownership',
    );

    /**
     * orderStatus
     * @var array
     */
    protected $_statusMapping = array(
        'CA' => 'Cancelled',
        'DC' => 'Discontinued',
        'EP' => 'Expired',
        'IN' => 'Initial',
        'DO' => 'Closed',
        'EX' => 'Open',
        'FU' => 'Future', //*Not in use
        'SB' => 'Submitted',
        'SR' => 'Submit Requested',
        'ER' => 'In Error',
        'RE' => 'Reject', //*Not in use
        'TC' => 'To Be Cancelled',
    );

    /**
     * portingStatus
     * @var array
     */
    protected $_portingMapping = array(
        'A' => 'Approved',
        'R' => 'Rejected',
        'P' => 'Pending',
        'CA' => 'Cancelled',
        'OD' => 'Overdue',
    );

    /**
     * @return mixed
     */
    public function getOrderDescription()
    {
        return isset($this->_descriptionMapping[$this->getData('order_description')])
            ? $this->_descriptionMapping[$this->getData('order_description')]
            : $this->getData('order_description');
    }

    /**
     * @return mixed
     */
    public function getFlow()
    {
        return isset($this->_descriptionMapping[$this->getData('flow')])
            ? $this->_descriptionMapping[$this->getData('flow')]
            : $this->getData('flow');
    }

    /**
     * @return mixed
     */
    public function getPortingStatus()
    {
        return isset($this->_portingMapping[$this->getData('porting_status')])
            ? $this->_portingMapping[$this->getData('porting_status')]
            : $this->getData('porting_status');
    }

    /**
     * @return mixed
     */
    public function getOrderStatus()
    {
        return isset($this->_statusMapping[$this->getData('order_status')])
            ? $this->_statusMapping[$this->getData('order_status')]
            : $this->getData('order_status');
    }

    /**
     * @return string
     */
    public function getAssignedProductID()
    {
        return $this->getData('assigned_product_i_d');
    }

    /**
     * @return string
     */
    public function getContactID()
    {
        return $this->getData('contact_i_d');
    }

    /**
     * @return string
     */
    public function getCustomerID()
    {
        return $this->getData('customer_i_d');
    }
} 