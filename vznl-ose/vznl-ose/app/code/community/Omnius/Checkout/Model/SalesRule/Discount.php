<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Model_SalesRule_Discount
 */
class Omnius_Checkout_Model_SalesRule_Discount extends Mage_SalesRule_Model_Quote_Discount
{
    /**
     * Collect address discount amount
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_SalesRule_Model_Quote_Discount
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $this->_setAddress($address);
        /**
         * Reset amounts
         */
        $this->_setAmount(0);
        $this->_setBaseAmount(0);

        $store = Mage::app()->getStore($address->getQuote()->getStoreId());

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }

        $address->setDiscountDescription(array());
        $quote = $address->getQuote();
        $prevActivePackageId = $quote->getActivePackageId();
        $packages = $quote->getPackageModels();
        foreach ($items as $item) {
            if ($item->getNoDiscount()) {
                $item->setDiscountAmount(0);
                $item->setBaseDiscountAmount(0);
            } else {
                $packageModel = $packages->getItemByColumnValue('package_id', $item->getPackageId());

                //workaround for packages having id as ctn - to be removed
                if (!$packageModel || !$packageModel->getId()) {
                    $packageModel = $packages->getItemByColumnValue('ctn', $item->getPackageId());
                }
                if (!$packageModel) {
                    continue;
                }

                $quote->setActivePackageId($item->getPackageId());
                /**
                 * Child item discount we calculate for parent
                 */
                if ($item->getParentItemId()) {
                    continue;
                }
                $this->_calculator
                    ->setActivePackage($packageModel)
                    ->setCouponCode($packageModel->getCoupon())
                    ->setWebsiteId($store->getWebsiteId())
                    ->setCustomerGroupId($quote->getCustomerGroupId())
                ;

                if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                    foreach ($item->getChildren() as $child) {
                        $this->_calculator->processItem($child);
                        $eventArgs['item'] = $child;
                        Mage::dispatchEvent('sales_quote_address_discount_item', $eventArgs);

                        $this->_aggregateItemDiscount($child);
                    }
                } else {
                    $this->_calculator->processItem($item);
                    $this->_aggregateItemDiscount($item);
                }
            }
        }
        $quote->setActivePackageId($prevActivePackageId);
        /**
         * process weee amount
         */
        if (Mage::helper('weee')->isEnabled() && Mage::helper('weee')->isDiscounted($store)) {
            $this->_calculator->processWeeeAmount($address, $items);
        }

        /**
         * Process shipping amount discount
         */
        $address->setShippingDiscountAmount(0);
        $address->setBaseShippingDiscountAmount(0);
        if ($address->getShippingAmount()) {
            $this->_calculator->processShippingAmount($address);
            $this->_addAmount(-$address->getShippingDiscountAmount());
            $this->_addBaseAmount(-$address->getBaseShippingDiscountAmount());
        }

        $this->_calculator->prepareDescription($address);

        return $this;
    }
} 