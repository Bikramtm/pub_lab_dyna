<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Dyna
 * @package     Omnius_Checkout
 */


class Omnius_Checkout_Model_Sales_Quote_Address_Total_Subtotal extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    /**
     * Collect address subtotal
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Subtotal
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $packageId = Mage::registry('add_multi_package_id');
        $this->_setAddress($address);

        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $quote = $address->getQuote();
        /**
         * Reset amounts
         */
        $this->_setMafAmount(0)->_setBaseMafAmount(0);

        $address->setTotalQty(0);
        /** @var Omnius_Package_Model_Mysql4_Package_Collection $packages */
        $packages = Mage::getModel('package/package')->getPackages(null, $quote->getId());
        $quote->setPackageModels($packages);
        $baseVirtualAmount = $virtualAmount = 0;

         //Process address items
        $items = $this->_getAddressItems($address);

        $isOverridden = Mage::registry('is_override_promos') || $quote->getIsOverrulePromoMode();
        $initialItems = Mage::getSingleton('customer/session')->getOrderEditMode() ? Mage::getSingleton('checkout/session')->getInitialItems() : [];
        foreach ($items as $item) {
            $valid = $this->checkIfValid($packages, $item, $quote, $isOverridden, $initialItems);
            if ($valid && $this->_initItem($address, $item) && $item->getQty() > 0) {
                /**
                 * Separately calculate subtotal only for virtual products
                 */
                if ($item->getProduct()->isVirtual()) {
                    $virtualAmount += $item->getMafRowTotal();
                    $baseVirtualAmount += $item->getBaseMafRowTotal();
                }
            } elseif (!$packageId || $item->getPackageId() === $packageId) {
                $this->_removeItem($address, $item);
            }
        }

        $address->setBaseMafVirtualAmount($baseVirtualAmount);
        $address->setMafVirtualAmount($virtualAmount);

        return $this;
    }

    /**
     * Address item initialization
     *
     * @param  $item
     * @return bool
     */
    protected function _initItem($address, $item)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
            $quoteItem = $item->getAddress()->getQuote()->getItemById($item->getQuoteItemId());
        }
        else {
            $quoteItem = $item;
        }
        $product = $quoteItem->getProduct();
        $product->setCustomerGroupId($quoteItem->getQuote()->getCustomerGroupId());

        /**
         * Quote super mode flag mean what we work with quote without restriction
         */
        if ($item->getQuote()->getIsSuperMode()) {
            if (!$product) {
                return false;
            }
        }
        else {
            if (!$product || !$product->isVisibleInCatalog()) {
                return false;
            }
        }

        if ($quoteItem->getParentItem() && $quoteItem->isChildrenCalculated()) {
            $finalPrice = $quoteItem->getParentItem()->getProduct()->getPriceModel()->getChildFinalMaf(
               $quoteItem->getParentItem()->getProduct(),
               $quoteItem->getParentItem()->getQty(),
               $quoteItem->getProduct(),
               $quoteItem->getQty()
            );
            $item->setMaf($finalPrice)
                ->setBaseOriginalMaf($finalPrice);
            $item->calcMafRowTotal();
        } else if (!$quoteItem->getParentItem()) {
            $finalPrice = $product->getFinalMaf($quoteItem->getQty());
            $item->setMaf($finalPrice)
                ->setBaseOriginalMaf($finalPrice);
            $item->calcMafRowTotal();
            $this->_addMafAmount($item->getMafRowTotal());
            $this->_addBaseMafAmount($item->getBaseMafRowTotal());
            $address->setTotalQty($address->getTotalQty() + $item->getQty());
        }

        return true;
    }

    /**
     * Remove item
     *
     * @param  $address
     * @param  $item
     * @return Mage_Sales_Model_Quote_Address_Total_Subtotal
     */
    protected function _removeItem($address, $item)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Item) {
            $address->removeItem($item->getId());
            if ($address->getQuote()) {
                $address->getQuote()->removeItem($item->getId());
            }
        }
        elseif ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
            $address->removeItem($item->getId());
            if ($address->getQuote()) {
                $address->getQuote()->removeItem($item->getQuoteItemId());
            }
        }

        return $this;
    }

    /**
     * Assign subtotal amount and label to address object
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Subtotal
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $address->addMafTotal(array(
            'code'  => $this->getCode(),
            'title' => Mage::helper('sales')->__('Maf Subtotal'),
            'value' => $address->getMafSubtotal()
        ));
        return $this;
    }

    /**
     * Get Subtotal label
     *
     * @return string
     */
    public function getLabel()
    {
        return Mage::helper('sales')->__('Maf Subtotal');
    }


    /**
     * Set total model amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _setMafAmount($amount)
    {
        if ($this->_canSetAddressAmount) {
            $this->_getAddress()->setMafTotalAmount($this->getCode(), $amount);
        }
        return $this;
    }

    /**
     * Set total model base amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _setBaseMafAmount($baseAmount)
    {
        if ($this->_canSetAddressAmount) {
            $this->_getAddress()->setBaseMafTotalAmount($this->getCode(), $baseAmount);
        }
        return $this;
    }

    /**
     * Add total model amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _addMafAmount($amount)
    {
        if ($this->_canAddAmountToAddress) {
            $this->_getAddress()->addMafTotalAmount($this->getCode(),$amount);
        }
        return $this;
    }

    /**
     * Add total model base amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _addBaseMafAmount($baseAmount)
    {
        if ($this->_canAddAmountToAddress) {
            $this->_getAddress()->addBaseMafTotalAmount($this->getCode(), $baseAmount);
        }
        return $this;
    }

    /**
     * @param $packages
     * @param $item
     * @param $quote
     * @param $isOverridden
     * @param $initialItems
     * @return bool
     */
    protected function checkIfValid($packages, $item, $quote, $isOverridden, $initialItems)
    {
        $isDelivered = $packages->getItemByColumnValue('package_id', $item->getPackageId())->isDelivered();
        $editProductId = Mage::getSingleton('customer/session')->getOrderEditMode() && isset($initialItems[$item->getProductId()]) && $quote->hasProductId($item->getProductId());
        $deliveredAndNotPromo = $isDelivered && !$item->isPromo() && $editProductId;
        $editTargetId = Mage::getSingleton('customer/session')->getOrderEditMode() && isset($initialItems[$item->getTargetId()]) && $quote->hasProductId($item->getTargetId());
        $deliveredAndPromo = $isDelivered && $item->isPromo() && $editTargetId;
        $promoOrOverriden = !$item->isPromo() || $quote->getData('collect_totals_cache_items') || $isOverridden;
        if ($promoOrOverriden || $deliveredAndNotPromo || $deliveredAndPromo) {
            $valid = true;
        } else {
            $valid = false;
        }

        return $valid;
    }
}
