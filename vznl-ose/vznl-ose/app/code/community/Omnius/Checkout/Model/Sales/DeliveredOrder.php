<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Model_Sales_DeliveredOrder extends Omnius_Checkout_Model_Sales_Order
{
    /** @var Omnius_Checkout_Model_Sales_Quote[] */
    protected $_buildOldQuotes = [];

    protected $newPackageIds = [

    ];
    /**
     * @var Omnius_Superorder_Model_Superorder
     */
    protected $_newSuperorder = null;

    /**
     * Builds orders and super-orders after separating the packages.
     * @return $this
     * @throws Exception
     */
    public function build()
    {
        parent::build();


        $allPackages = array_merge($this->_processedPackages, $this->_cancelledPackages, $this->_untouchedPackages);
        if (count($this->_processedPackages) > 0) {
            /** @var Omnius_Package_Model_Package $package */
            foreach ($this->_processedPackages as $package) {
                $this->addToNew($package);
                $this->addToOld($package);
            }

            //Create new superOrder if there are edited packages.
            $newSuperOrder = $this->getNewSuperorder() ? $this->getNewSuperorder() : Mage::getModel('superorder/superorder')->createNewSuperorder($this->getSuperorder());
            $newSuperOrder->save();
            $this->setNewSuperorder($newSuperOrder);
            if (count($this->_cancelledPackages) > 0) {
                /** @var Omnius_Package_Model_Package $package */
                foreach ($this->_cancelledPackages as $package) {
                    $this->addToNew($package);
                }

                /** @var Omnius_Package_Model_Package $package */
                foreach ($this->_processedPackages as $package) {
                    $this->addToOld($package);
                }
            }
        }

        /** @var Omnius_Package_Model_Package $package */
        foreach ($this->_untouchedPackages as $up) {
            $this->addToOld($up);
        }
        $this->processCancelled();

        $newOrders = [];
        if ($this->getNewSuperorder()) {
            $newOrders = $this->buildOrders($this->_buildQuotes, $this->getNewSuperorder()->getId(), true);
        }
        $oldOrders = $this->buildOrders($this->_buildOldQuotes, $this->getSuperorderId());

        if (count($allPackages) == count($this->_cancelledPackages)) {
            $this->setStatus(self::STATUS_CANCELLED)->save();
        } else {
            // set a flag that is used to determine if approveOrder call should be made or not
            Mage::register('order_has_other_packages', true);
        }

        $incrementIds = [];
        foreach ($newOrders as $orderIncrement) {
            $incrementIds[] = $orderIncrement->getIncrementId();
        }

        if (count($oldOrders) + count($newOrders) > 0) {
            $this->setEditedOrder(1);
        }

        return $incrementIds;
    }

    /**
     * @param Omnius_Package_Model_Package $package
     * @return $this
     */
    protected function addToNew($package)
    {
        //if there is no quote generated for the address hash, generate a new one.
        if (!isset($this->_buildQuotes[$package->getOrderHash()])) {
            $newQuote = $this->_packageHelper->buildQuoteFromPackage($package, $this);
        } else {
            /** @var Omnius_Checkout_Model_Sales_Quote $newQuote */
            $newQuote = $this->_buildQuotes[$package->getOrderHash()];
        }

        /** @var Omnius_Package_Model_Package $newPackage */
        $newPackage = Mage::getModel('package/package');

        $lvlUp = count($this->newPackageIds) > 0 ? max($this->newPackageIds) + 1 : 1;
        $this->newPackageIds[$package->getPackageId()] = $lvlUp;
        $newPackage->setData($package->getData())
            ->setId(null)
            ->setOrderId(null)
            ->setQuoteId($newQuote->getId())
            ->setOldPackageId($package->getPackageId())
            ->setPackageId($lvlUp);

        if ($package->getIsCancelled()) {
            $newPackage->setStatus(Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED);
        }
        $newPackage->save();

        foreach ($package->getItems() as $item) {
            /** @var Mage_Sales_Model_Quote_Item $quoteItem */
            $quoteItem = clone $item;
            $quoteItem->setPackageId($newPackage->getPackageId());
            $quoteItem->setQuote($newQuote);
            $quoteItem->save();
        }
        $newPackages = $newQuote->getNewPackages();
        $newPackages[] = $newPackage;
        $newQuote->setNewPackages($newPackages);

        $this->_buildQuotes[$package->getOrderHash()] = $newQuote;
        return $this;
    }

    /**
     * @param Omnius_Package_Model_Package $package
     * @return $this
     */
    public function addToOld($package)
    {
        //if there is no quote generated for the address hash, generate a new one.
        if (!isset($this->_buildOldQuotes['_______OLD________'])) {
            $newQuote = $this->_packageHelper->buildQuoteFromPackage($package, $this, true);
        } else {
            /** @var Omnius_Checkout_Model_Sales_Quote $newQuote */
            $newQuote = $this->_buildOldQuotes['_______OLD________'];
        }
        $package->setQuoteId($newQuote->getId())->setOrderId(null)->save();

        foreach ($package->getOldItems() as $item) {
            /** @var Mage_Sales_Model_Quote_Item $quoteItem */
            $quoteItem = clone $item;
            $quoteItem->setPackageId($package->getPackageId());
            $quoteItem->setQuote($newQuote);
            $quoteItem->save();
        }

        $this->_buildOldQuotes['_______OLD________'] = $newQuote;
        return $this;
    }

    /**
     * @return Omnius_Superorder_Model_Superorder
     */
    public function getNewSuperorder()
    {
        return $this->_newSuperorder;
    }

    /**
     * @param Omnius_Superorder_Model_Superorder $newSuperorder
     * @return $this
     */
    public function setNewSuperorder($newSuperorder)
    {
        $this->_newSuperorder = $newSuperorder;
        return $this;
    }
}