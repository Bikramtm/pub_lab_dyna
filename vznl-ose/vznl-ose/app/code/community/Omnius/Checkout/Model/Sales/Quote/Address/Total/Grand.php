<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Model to calculate grand total or an order
 *
 * @category    Dyna
 * @package     Omnius_Checkout
 */
class Omnius_Checkout_Model_Sales_Quote_Address_Total_Grand extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    /**
     * Collect grand total address amount
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Grand
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $grandTotal     = $address->getMafGrandTotal();
        $baseGrandTotal = $address->getBaseMafGrandTotal();

        $store      = $address->getQuote()->getStore();
        $totals     = array_sum($address->getAllMafTotalAmounts());
        $totals     = $store->roundPrice($totals);
        $baseTotals = array_sum($address->getAllBaseMafTotalAmounts());
        $baseTotals = $store->roundPrice($baseTotals);


        $address->setInitialMixmatchTotal(0);
        $address->setInitialMixmatchSubtotal(0);
        $address->setInitialMixmatchTax(0);

        $address->getQuote()->setInitialMixmatchTotal(0);
        $address->getQuote()->setInitialMixmatchSubtotal(0);
        $address->getQuote()->setInitialMixmatchTax(0);

        $mixmatchSubtotal = 0;
        $mixmatchTax = 0;
        $mixmatchTotal = 0;

        foreach($this->_getAddressItems($address) as $item) {
            if ($item->getMixmatchSubtotal() !== null) {
                $mixmatchSubtotal += $item->getMixmatchSubtotal() - ($item->getDiscountAmount() - $item->getHiddenTaxAmount());
                $mixmatchTax += $item->getMixmatchTax() - $item->getHiddenTaxAmount();
                $mixmatchTotal += $item->getMixmatchSubtotal() + $item->getMixmatchTax() - $item->getDiscountAmount();
            } else {
                $mixmatchSubtotal += $item->getItemFinalPriceExclTax();
                $mixmatchTax += $item->getTaxAmount();
                $mixmatchTotal += $item->getItemFinalPriceInclTax();
            }
        }

        $address->setInitialMixmatchTotal($mixmatchTotal);
        $address->setInitialMixmatchSubtotal($mixmatchSubtotal);
        $address->setInitialMixmatchTax($mixmatchTax);

        $address->getQuote()->setInitialMixmatchTotal($mixmatchTotal);
        $address->getQuote()->setInitialMixmatchSubtotal($mixmatchSubtotal);
        $address->getQuote()->setInitialMixmatchTax($mixmatchTax);

        $address->setMafGrandTotal($grandTotal+$totals);
        $address->setBaseMafGrandTotal($baseGrandTotal+$baseTotals);
        return $this;
    }

    /**
     * Add grand total information to address
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Grand
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $address->addMafTotal(array(
            'code'  => $this->getCode(),
            'title' => Mage::helper('sales')->__('Maf Grand Total'),
            'value' => $address->getMafGrandTotal(),
            'area'  => 'footer',
        ));

        $address->addTotal(array(
            'code' => $this->getCode(),
            'title' => Mage::helper('tax')->__('Total'),
            'value' => $address->getInitialMixmatchTotal()
        ));
        return $this;
    }

}
