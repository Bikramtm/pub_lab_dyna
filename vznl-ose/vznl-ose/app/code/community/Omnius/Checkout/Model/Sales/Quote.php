<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Model_Sales_Quote
 * @method string|null getCorrespondenceEmail()
 * @method bool|null getIsOffer()
 * @method int getActivePackageId()
 * @method int getSuperOrderEditId()
 * @method int getSuperQuoteId()
 */
class Omnius_Checkout_Model_Sales_Quote extends Mage_Sales_Model_Quote
{
    const CART_STATUS_SAVED = 1;
    const CART_STATUS_RISK = 2;
    const CART_STATUS_STEP_CONFIGURATOR = 'configurator';
    const CART_STATUS_STEP_CHECKOUT = 'checkout';
    private $_myCached = array();
    public static $_quoteData = ['items_count', 'items_qty', 'grand_total', 'base_grand_total', 'applied_rule_ids', 'coupon_code', 'subtotal', 'base_subtotal', 'subtotal_with_discount', 'base_subtotal_with_discount', 'maf_grand_total', 'base_maf_grand_total', 'maf_subtotal', 'base_maf_subtotal', 'maf_subtotal_with_discount', 'base_maf_subtotal_with_discount'];

    const VALIDATION = 'Waiting for Validation';
    /**
     * Clones current quote
     *
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    public function __clone()
    {
        $quote = new self;
        $quote->setData($this->getData());
        $quote->unsetData('entity_id');
        $quote->merge($this);
        return $quote;
    }

    /**
     * @param int $packageId
     * @return Omnius_Checkout_Model_Sales_Quote_Item[]
     */
    public function getPackageItems($packageId)
    {
        if ( ! $packageId) {
            return array();
        }
        $packageItems = array();
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->getItemsCollection() as $item) {
            if ($item->getPackageId() == $packageId && !$item->isDeleted()) {
                $packageItems[] = $item;
            }
        }
        return $packageItems;
    }

    /**
     * @return array
     *
     * Get a list of product ids from a package
     */
    public function getPackageProductIds()
    {
        $packageId = Mage::app()->getRequest()->getParam('packageId', false);
        if(!$packageId) {
            return array();
        }
        $ids = array();
        foreach($this->getPackageItems($packageId) as $item) {
            if(!$item->getProduct()->isSim() && !$item->isPromo()) {
                $ids[] = $item->getProductId();
            }
        }

        $ids = array_unique($ids);
        sort($ids);
        return $ids;
    }

    /**
     * @param null $packageId
     * @return mixed
     */
    public function getPackageType($packageId = null)
    {
        $packageId = $packageId ?: $this->getActivePackageId();
        $package = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId())->addFieldToFilter('package_id', $packageId)->getFirstItem();
       
        return $package->getType();
    }

    /**
     * @param null $packageId
     * @return mixed
     */
    public function getPackageSaleType($packageId = null)
    {
        $packageId = $packageId ?: $this->getActivePackageId();
        $package = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId())->addFieldToFilter('package_id', $packageId)->getFirstItem();
        
        return $package->getSaleType();
    }

    /**
     * @param $packageId int
     * @return Omnius_Package_Model_Package
     */
    public function getPackageById($packageId)
    {
        return Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();
    }

    /**
     * 
     * @param $packageId
     * @return array
     */
    public function getPackageCtns($packageId = null)
    {
        $packageId = $packageId ?: $this->getActivePackageId();
        $ctns = array();
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId())->addFieldToFilter('package_id', $packageId)->getFirstItem();
        if($packages->getCtn()) {
            $ctns = explode(',',$packages->getCtn());
        }
        
        return array_unique($ctns);
    }

    /**
     * Adding catalog product object data to quote
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  Mage_Sales_Model_Quote_Item
     */
    protected function _addCatalogProduct(Mage_Catalog_Model_Product $product, $qty = 1)
    {
        $newItem = false;
        /**
         * force creation of a new quote item even if the same product already exists in the quote
         */
        $item = false;
        if (!$item) {
            /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
            $item = Mage::getModel('sales/quote_item');
            $item->setQuote($this);
            if (Mage::app()->getStore()->isAdmin()) {
                $item->setStoreId($this->getStore()->getId());
            } else {
                $item->setStoreId(Mage::app()->getStore()->getId());
            }
            $newItem = true;
        }

        /**
         * We can't modify existing child items
         */
        if ($item->getId() && $product->getParentProductId()) {
            return $item;
        }

        $item->setOptions($product->getCustomOptions())
            ->setProduct($product);

        // Add only item that is not in quote already (there can be other new or already saved item
        if ($newItem) {
            $this->addItem($item);
        }

        return $item;
    }

    /**
     * @return array
     * @param string $status
     * Get list of packages for a certain quote
     */
    public function getPackages($status = null)
    {
        $key = 'quote_packages_' . $this->getId() . '_' . md5($this->getUpdatedAt() . $status);
        if (($packages = Mage::registry($key)) === null) {
            $packages = [];
            // Set packages type and sale type
            if ($packagesInfo = $this->getPackagesInfo()) {
                foreach ($packagesInfo as $package) {
                    $packageId = $package['package_id'];
                    $packages[$packageId] = $package;
                    $packages[$packageId]['items'] = array();
                }
            }

            /** @var Omnius_Checkout_Model_Sales_Quote_Item[] $quoteItems */
            $quoteItems = Mage::getResourceModel('sales/quote_item_collection')
                ->setQuote($this)
                ->addOrder('package_id', 'asc');

            // Ensure the items for the package come in order, Sub, Sim, Device, Addon, Acc
            $packagesAll = $this->getPackageDataByType($status, $quoteItems, $packages);

            foreach ($packagesAll as $packageId => $tempPackage) {
                $items = [];
                foreach($tempPackage as $type => $i) {
                    $items += $tempPackage[$type];
                }
                $packages[$packageId]['items'] = $items;
            }

            if ($this->getId()) {
                Mage::unregister($key);
                Mage::register($key, $packages);
            }
        }

        return $packages;
    }

    /**
     * Collect totals
     *
     * @return Mage_Sales_Model_Quote
     */
    public function collectTotals()
    {
        /**
         * Protect double totals collection
         */
        if ($this->getTotalsCollectedFlag()) {
            return $this;
        }

        $isMultishipping = $this->getIsMultiShipping();
        $this->setData('collect_totals_cache_items', $isMultishipping);
        Mage::unregister('collect_totals_quote');
        Mage::register('collect_totals_quote', $this);

        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_before', array($this->_eventObject => $this));

        if (Mage::getSingleton('customer/session')->getOrderEdit() || $this->getIsOffer()) {
            $this->setIsSuperMode(true);
        }

        $this->setSubtotal(0);
        $this->setBaseSubtotal(0);

        $this->setSubtotalWithDiscount(0);
        $this->setBaseSubtotalWithDiscount(0);

        $this->setGrandTotal(0);
        $this->setBaseGrandTotal(0);

        $this->setMafSubtotal(0);
        $this->setBaseMafSubtotal(0);

        $this->setMafSubtotalWithDiscount(0);
        $this->setBaseMafSubtotalWithDiscount(0);

        $this->setMafGrandTotal(0);
        $this->setBaseMafGrandTotal(0);

        /** @var Omnius_Checkout_Model_Sales_Quote_Address $address */
        foreach ($this->getAllShippingAddresses() as $address) {

            $address->setSubtotal(0);
            $address->setBaseSubtotal(0);

            $address->setGrandTotal(0);
            $address->setBaseGrandTotal(0);

            $address->setMafSubtotal(0);
            $address->setBaseMafSubtotal(0);

            $address->setMafGrandTotal(0);
            $address->setBaseMafGrandTotal(0);

            $address->collectTotals();


            $this->setSubtotal((float)$this->getSubtotal() + $address->getSubtotal());
            $this->setBaseSubtotal((float)$this->getBaseSubtotal() + $address->getBaseSubtotal());

            $this->setSubtotalWithDiscount(
                (float)$this->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
            );
            $this->setBaseSubtotalWithDiscount(
                (float)$this->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
            );

            $this->setGrandTotal((float)$this->getGrandTotal() + $address->getGrandTotal());
            $this->setBaseGrandTotal((float)$this->getBaseGrandTotal() + $address->getBaseGrandTotal());


            $this->setMafSubtotal((float)$this->getMafSubtotal() + $address->getMafSubtotal());
            $this->setBaseMafSubtotal((float)$this->getBaseMafSubtotal() + $address->getBaseMafSubtotal());

            $this->setMafSubtotalWithDiscount(
                (float)$this->getMafSubtotalWithDiscount() + $address->getMafSubtotalWithDiscount()
            );
            $this->setBaseMafSubtotalWithDiscount(
                (float)$this->getBaseMafSubtotalWithDiscount() + $address->getBaseMafSubtotalWithDiscount()
            );

            $this->setMafGrandTotal((float)$this->getMafGrandTotal() + $address->getMafGrandTotal());
            $this->setBaseMafGrandTotal((float)$this->getBaseMafGrandTotal() + $address->getBaseMafGrandTotal());
        }
        Mage::helper('sales')->checkQuoteAmount($this, $this->getGrandTotal());
        Mage::helper('sales')->checkQuoteAmount($this, $this->getBaseGrandTotal());

        $this->setItemsCount(0);
        $this->setItemsQty(0);
        $this->setVirtualItemsQty(0);

        foreach ($this->getAllVisibleItems() as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            $children = $item->getChildren();
            if ($children && $item->isShipSeparately()) {
                foreach ($children as $child) {
                    if ($child->getProduct()->getIsVirtual()) {
                        $this->setVirtualItemsQty($this->getVirtualItemsQty() + $child->getQty());
                    }
                }
            }

            if ($item->getProduct()->getIsVirtual()) {
                $this->setVirtualItemsQty($this->getVirtualItemsQty() + $item->getQty());
            }
            $this->setItemsCount($this->getItemsCount() + 1);
            $this->setItemsQty((float) $this->getItemsQty() + $item->getQty());
        }

        $this->setData('trigger_recollect', 0);
        $this->_validateCouponCode();
        $this->setIsMultiShipping($isMultishipping);
        $this->unsetData('collect_totals_cache_items');
        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_after', array($this->_eventObject => $this));
        Mage::unregister('collect_totals_quote');
        $this->setTotalsCollectedFlag(true);

        return $this;
    }

    /**
     * Get all quote totals (sorted by priority)
     * Method process quote states isVirtual and isMultiShipping
     *
     * @return array
     */
    public function getMafTotals()
    {
        /**
         * If quote is virtual we are using totals of billing address because
         * all items assigned to it
         */
        if ($this->isVirtual()) {
            return $this->getBillingAddress()->getMafTotals();
        }

        $shippingAddress = $this->getShippingAddress();
        $totals = $shippingAddress->getMafTotals();
        // Going through all quote addresses and merge their totals
        foreach ($this->getAddressesCollection() as $address) {
            if ($address->isDeleted() || $address === $shippingAddress) {
                continue;
            }
            foreach ($address->getMafTotals() as $code => $total) {
                if (isset($totals[$code])) {
                    $totals[$code]->merge($total);
                } else {
                    $totals[$code] = $total;
                }
            }
        }

        $sortedTotals = array();
        foreach ($this->getBillingAddress()->getTotalModels() as $total) {
            /* @var $total Mage_Sales_Model_Quote_Address_Total_Abstract */
            if (isset($totals[$total->getCode()])) {
                $sortedTotals[$total->getCode()] = $totals[$total->getCode()];
            }
        }
        return $sortedTotals;
    }

    /**
     * @return $this|Mage_Sales_Model_Quote
     */
    public function save()
    {
        /**
         * Assure that current agent and dealer are persisted with the quote
         */
        /** @var Mage_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');
        $agent = $session->getAgent(true);
        if($agent && !$this->getIsOffer()) {
            $this->setData('agent_id', $agent->getId());
            $agentDealer = $agent->getDealer();
            if ($agentDealer) {
                $this->setData('dealer_id', $agentDealer->getId());
            }
            $superAgent = $session->getSuperAgent();
            if ($superAgent) {
                $this->setData('super_agent_id', $superAgent->getId());
            }
        }

        if ($this->_preventSaving) {
            return $this;
        }
        return parent::save();
    }

    /**
     * Gets the quotes for a prospect.
     * @param $prospect
     * @return mixed
     */
    public function getQuotesForProspect($prospect) {
        $quotes = $this
            ->getCollection()
            ->addFieldToFilter('customer_email', $prospect)
            ->addFieldToFilter('prospect_saved_quote', array('neq' => null))
            ->addFieldToFilter('store_id', Mage::app()->getStore()->getId());
        $quotes->getSelect()->where('customer_id IS NULL');
        return $quotes;
    }

    /**
     * Returns the value from the quote or the default value from the backend
     *
     * @param string $field
     * @param null $section
     * @return mixed|null
     */
    public function getDefaultValueForField($field, $section = null)
    {
        if ($this->getData($field) != null) {
            return $this->getData($field);
        }
        
        return Mage::helper('omnius_checkout')->getDefaultValueForField($field, $section);
    }

    /**
     * Checks if the quote has packages of the specified type(s).
     *
     * @param array $categories
     * @return bool
     */
    public function hasItemsOfCategory($categories = array())
    {
        foreach($this->getAllItems() as $item) {
            /** @var Mage_Catalog_Model_Product $catalogProduct */
            $catalogProduct = Mage::getModel('catalog/product')->load($item->getProduct()->getId());

            $productCategories = $catalogProduct->getCategoryIds();
            /** @var Mage_Catalog_Model_Category $category */
            foreach($productCategories as $category) {
                if(in_array(Mage::getModel('catalog/category')->load($category)->getName(), $categories)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $groupType
     * @param $packageId
     * @return bool
     *
     * Check if a certain package from the quote has a product of a specific type
     */
    public function hasItemGroupInPackage($groupType, $packageId)
    {
        return $this->hasGroupPackage($groupType, $packageId);
    }

    /**
     * @param $groupType
     * @param $packageId
     * @return bool
     */
    public function hasGroupPackage($groupType, $packageId)
    {
        /**@var Omnius_Checkout_Model_Sales_Quote_Item $item*/
        foreach ($this->getAllItems() as $item) {
            if ($item->getPackageId() == $packageId && in_array($groupType, $item->getProduct()->getType())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     *
     * Removed quote items that are set to cancelled
     */
    public function getAllVisibleItems()
    {
        $key = __METHOD__ . ' ' . (int) $this->getId();
        if (Mage::registry('freeze_models') === true && isset($this->_myCached[$key])) {
            return $this->_myCached[$key];
        }

        $items = parent::getAllVisibleItems();
        foreach ($items as $key => $item) {
            if ($item->getIsCancelled() == 1) {
                unset($items[$key]);
            }
        }

        $this->_myCached[$key] = $items;

        return $items;
    }

    /**
     * Refresh item collection for the quote
     */
    public function refreshItemsCollection()
    {
        $this->_items = Mage::getModel('sales/quote_item')->getCollection();
        $this->_items->setQuote($this);
    }

    /**
     * Retrieve quote items array
     *
     * @return Omnius_Checkout_Model_Sales_Quote_Item[]
     */
    public function getAllItems()
    {
        $key = __METHOD__ . ' ' . (int) $this->getId();

        if (Mage::registry('freeze_models') === true && isset($this->_myCached[$key])) {
            return $this->_myCached[$key];
        }
        
        $items = array();
        foreach ($this->getItemsCollection() as $item) {
            if (!$item->isDeleted()) {
                $items[] = $item;
            }
        }
        
        $this->_myCached[$key] = $items;
        return $items;
    }

    /**
     * @param int|null $packageId
     * @param bool|false $isOffer
     * @return array
     */
    public function calculateTotalsForPackage($packageId = null, $isOffer = false)
    {
        $items = $this->getAllItems();
        $subtotalPrice = 0;
        $subtotalMaf = 0;
        $taxPrice = 0;
        $taxMaf = 0;
        $total = 0;
        $totalMaf = 0;
        $noPromoMaf = 0;
        $noPromoTax = 0;
        foreach ($items as $item) {
            $correctId = $item->getPackageId() == $packageId || $packageId == null;
            $isOfferOrPromo = !($item->isPromo() || $isOffer);
            if ($correctId && $isOfferOrPromo) {
                //SREQ-6384 - Removed the else
                $rowTotal = ceil($item->getRowTotal()) ? $item->getItemFinalPriceExclTax() : 0;
                $subtotalPrice += $rowTotal;
                $noPromoMaf += $item->getMaf();
                $noPromoTax += $item->getMafInclTax();
                $rowTotalMaf = ceil($item->getMafRowTotal()) ? $item->getItemFinalMafExclTax() : 0;
                $subtotalMaf += $rowTotalMaf;
                $mafTaxAmmount = ceil($item->getMafTaxAmount()) ? $item->getMafTaxAmount() : 0;
                $taxMaf += $mafTaxAmmount;
                $taxAmmount = ceil($item->getTaxAmount()) ? $item->getTaxAmount() : 0;
                $taxPrice += $taxAmmount;
                $total += ($rowTotal + $taxAmmount);
                $totalMaf += ($rowTotalMaf + $mafTaxAmmount);
            }
        }

        return array(
            'no_promo_maf' => $noPromoMaf,
            'no_promo_maf_tax' => $noPromoTax,
            'no_promo_tax_amount' => ($noPromoTax - $noPromoMaf),
            'subtotal_price' => $isOffer && $this->getInitialMixmatchSubtotal() !== null ? $this->getInitialMixmatchSubtotal() : $subtotalPrice ,
            'subtotal_maf' => $subtotalMaf,
            'tax_price' => $isOffer && $this->getInitialMixmatchSubtotal() !== null ? $this->getInitialMixmatchTax() : $taxPrice,
            'tax_maf' => $taxMaf,
            'total' => $isOffer && $this->getInitialMixmatchSubtotal() !== null ? $this->getInitialMixmatchTotal() : $total,
            'total_maf' => $totalMaf,
        );
    }

    /**
     * Check if the quote has at least one package with number porting
     * Optionally, it can be used to check if a specific package has number porting by sending the $packageId
     *
     * @param array $packages
     * @param int $packageId
     * @return bool
     */
    public function containsNumberPorting(array $packages, $packageId = null)
    {
        foreach ($packages as $id => $package) {
            if ($packageId) {
                if ($id == $packageId) {
                    if (isset($package['current_number']) && !empty($package['current_number'])) {
                        return true;
                    }
                } else {
                    continue;
                }
            } else {
                if (isset($package['current_number']) && !empty($package['current_number'])) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Clones an existing quote
     * @param bool $recollectTotals
     * @throws RuntimeException
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    public function cloneQuote($recollectTotals = true)
    {
        /** @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getResourceModel('core/transaction');
        
        // clone the quote
        /** @var Dyna_Checkout_Model_Sales_Quote $newQuote */
        $newQuote = Mage::getModel('sales/quote')
            ->setData($this->getData())
            ->setActivePackageId(1)
            ->setCurrentPackageId(1)
            ->unsetData('entity_id')
            ->unsetData('created_at')
            ->setQuoteParentId($this->getId())
            ->save();
        
        // clone items
        /**@var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->getAllItems() as $item) {
            /**
             * Check if the product is available for the current store.
             */
            if (!in_array(Mage::app()->getStore()->getId(), $item->getProduct()->getStoreIds())) {
                continue;
            }
            /** @var Omnius_Checkout_Model_Sales_Quote_Item $newItem */
            $newItem = Mage::getModel('sales/quote_item')
                ->setData($item->getData())
                ->unsetData('item_id')
                ->setQuote($newQuote);
            $transaction->addObject($newItem);
        }
        
        // clone shipping address
        /**@var Omnius_Checkout_Model_Sales_Quote_Address $address */
        foreach ($this->getAllAddresses() as $address) {
            $newAddress = Mage::getModel('sales/quote_address');
            $newAddress->setData($address->getData());
            $newAddress->unsetData('address_id');
            $newAddress->setQuoteId($newQuote->getId());
            $transaction->addObject($newAddress);
        }

        //clone payment
        $oldPayment = $this->getPayment();

        $newPayment = Mage::getModel('sales/quote_payment');
        $newPayment->setData($oldPayment->getData());
        $newPayment->setQuoteId($newQuote->getId());
        $transaction->addObject($newPayment);

        // Create packages
        $packagesModelOld = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->getId());
        foreach ($packagesModelOld as $packageModelOld) {
            $packageModelNew = Mage::getModel('package/package');
            $packageModelNew->setData($packageModelOld->getData())->setQuoteId($newQuote->getId())->setOrderId(null)->setEntityId(null);
            $transaction->addObject($packageModelNew);
        }
        try {
            $transaction->save();
            // If all the products have been successfully saved, update their prices
            foreach ($newQuote->getAllVisibleItems() as $item) {
                Mage::dispatchEvent(
                    'checkout_cart_clone_product_after',
                    ['product' => $item->getProduct(), 'quote_item' => $item]
                );
            }
        } catch (Exception $e) {
            $newQuote->delete();
            throw new \RuntimeException('Cannot clone order');
        }
        
        // Assure prices
        if ($recollectTotals) {
            $newQuote->getBillingAddress();
            $newQuote->getShippingAddress()->setCollectShippingRates(true);
            $newQuote
                ->setTotalsCollectedFlag(false)
                ->collectTotals()
                ->save();
        }

        return $newQuote;
    }

    /**
     * @return $this
     */
    protected function _validateCouponCode()
    {
        return $this;
    }

    /**
     * Return packages info
     * @return array
     */
    public function getPackagesInfo()
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $result = array();
        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->getId())
            ->setOrder('package_id', 'ASC');
        $packages = $conn->fetchAll($packages->getSelect());
        foreach ($packages as $package) {
            $result[$package['package_id']] = $package;
        }
        return $result;
    }
    
    /**
     * Quote save method triggers many other events so in case you only need to update a field that should not
     * affect other data, this function can be used as it is much faster
     */
    public function updateFields($data)
    {
        if ($data && $this->getId()) {
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            $query = "update sales_flat_quote set ";
            $i = 0;
            foreach ($data as $field => $value) {
                $this->setData($field, $value);
                $value = is_numeric($value) ? $value : "'" . $value . "'";
                $query .= ($i ? ',' : '') . $field . '=' . $value;
                $i++;
            }
            $query .= " where entity_id = " . $this->getId();
            $write->query($query);
        }
        
        return $this;
    }

    /**
     * get all the information for every cnt in every package
     * @return array
     */
    public function getCTNsDetails() 
    {
        $ctns = array();
        //get all ctns from packages
        $packages = $this->getPackages();
        foreach ($packages as $key => &$package) {
            $ctns[$key] = array(
                'text'        => '',
                'description' => '',
                'packageCTNs' => array(),
            );
            // add cnt package title text (1 ctn => nt, 2+ ctns => n CTN's) 
            if (isset($package['ctn']) && strpos($package['ctn'], ',') !== false) {
                // more than 1 ctns
                $ctns[$key]['text'] = count(explode(',', $package['ctn'])) . 
                    'CTN\'s' . '<span style="display:none">' . $package['ctn'] . '</span>';
                $ctns[$key]['packageCTNs'] = array_map('trim', explode(',', $package['ctn']));
            } elseif (isset($package['ctn'])) {
                // 1 ctn
                $ctns[$key]['text'] = $package['ctn'];
                $ctns[$key]['packageCTNs'] = array(trim($package['ctn']));
            }
        }
        unset($package);
        unset($packages);
        
        // get only the ctns number for description 
        $allCtns = array_reduce($ctns, function($carry, $current) {
                return array_merge($carry, $current['packageCTNs']);
        }, array());

        // add the cnts list
        $ctns['allCtns'] = $allCtns;
        
        return $ctns;
    }

    /**
     * @param $packageId
     *
     * Save the package id and other possible data through a custom query to prevent quote->save method which is very heavy
     * @param array $additionalData
     */
    public function saveActivePackageId($packageId, $additionalData = array())
    {
        $this->setActivePackageId($packageId);
        if ($this->getId()) {
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            $data = array("active_package_id" => $packageId) + $additionalData;
            $where = "entity_id = " . $this->getId();
            $resource = Mage::getSingleton('core/resource');
            $tableName = $resource->getTableName('sales/quote');
            $write->update($tableName, $data, $where);
        }
    }

    /**
     * Updates package ids for quote_items including alternate_quote items after reindexPackageIds
     *
     * @param $packageId
     * @param array $packageIds
     */
    public function updatePackageIds($packageId, $packageIds = array())
    {
        if (!empty($packageIds)) {
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            $data = array("package_id" => $packageId);
            $where = "item_id IN (" . implode(", ",$packageIds) . ")";
            $resource = Mage::getSingleton('core/resource');
            $tableName = $resource->getTableName('sales/quote_item');
            $write->update($tableName, $data, $where);
        }
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @return $this
     */
    public function merge(Mage_Sales_Model_Quote $quote)
    {
        // Also add the packages when merging a quote
        $quotePackages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId());
        foreach ($quotePackages as $quotePackage) {
            Mage::getModel('package/package')
                ->setData($quotePackage->getData())
                ->setEntityId(null)
                ->setQuoteId($this->getId())
                ->save();
        }

        Mage::dispatchEvent(
            $this->_eventPrefix . '_merge_before',
            array(
                $this->_eventObject => $this,
                'source' => $quote
            )
        );

        foreach ($quote->getAllVisibleItems() as $item) {
            $newItem = clone $item;
            $this->addItem($newItem);
            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $newChild = clone $child;
                    $newChild->setParentItem($newItem);
                    $this->addItem($newChild);
                }
            }
        }

        /**
         * Init shipping and billing address if quote is new
         */
        if (!$this->getId()) {
            $this->getShippingAddress();
            $this->getBillingAddress();
        }

        if ($quote->getCouponCode()) {
            $this->setCouponCode($quote->getCouponCode());
        }

        Mage::dispatchEvent(
            $this->_eventPrefix . '_merge_after',
            array(
                $this->_eventObject => $this,
                'source' => $quote
            )
        );

        return $this;
    }

    /**
     * @param $superOrderEditId
     * @param $getFromSession
     * @return mixed
     */
    public function getListOfModifiedQuotes($superOrderEditId, $getFromSession = true)
    {
        $quoteCondition = $getFromSession
            ? array('eq' => Mage::getSingleton('customer/session')->getOrderEdit())
            : array('null' => true);
        return $this->getCollection()
            ->addFieldToFilter('super_quote_id', $quoteCondition)
            ->addFieldToFilter('super_order_edit_id', array('eq' => $superOrderEditId));
    }

    /**
     * @param bool $exclTax
     * @return float|mixed
     */
    public function getTotalAmount($exclTax = false)
    {
        if (! $exclTax) {
            return $this->getGrandTotal();
        }

        $total = 0;
        $address = $this->getShippingAddress();
        if ($address) {
            $total = $address->getGrandTotal() - $address->getTaxAmount();
        }

        return max($total, 0);
    }

    /**
     * @return float
     */
    public function getGrandTotalForShipping() 
    {
        if(Mage::getSingleton('customer/session')->getOrderEdit()) {
            $allPackages = $this->getPackages();
            $packageDatas = array();
            foreach($allPackages as $packageId => $package) {
                $packageModel = Mage::getModel('package/package')->getPackages($this->getSuperOrderEditId(), null, $packageId)->getFirstItem();
                $packageDatas[] = array($packageModel, $package['items']);
            }

            $editTotals = Mage::helper('omnius_checkout')->getEditTotals($packageDatas);
            return $editTotals['total_price'];
        } else {
            return $this->getGrandTotal();
        }
    }

    /**
     * Trigger before save even to set offer
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $tmpQuote = Mage::getSingleton('customer/session')->getOfferteData();
        if($this->getIsOffer() && $tmpQuote && $this->getId() == $tmpQuote->getId()) {
            foreach(static::$_quoteData as $field) {
                $this->setData($field, $tmpQuote->getData($field));
            }
        }
    }

    /**
     * @return bool
     */
    public function hasRetention()
    {
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId());
        /** @var Omnius_Package_Model_Package $p */
        foreach($packages as $p) {
            if($p->isRetention()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the quote contains at least one Business type subscription
     *
     * @return bool
     */
    public function containsBusinessSubscription()
    {
        $bool = false;
        $consValue = Mage::helper('omnius_configurator/attribute')->getSubscriptionIdentifier(Omnius_Catalog_Model_Product::IDENTIFIER_BUSINESS);

        $items = $this->getAllVisibleItems();
        if (count($items)) {
            foreach ($items as $item) {
                $prod = $item->getProduct();
                if ($prod->is(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) && $prod->getData(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE) == $consValue) {
                    $bool = true;
                    break;
                }
            }
        }

        return $bool;
    }

    /**
     * Check if the quote is a risk
     *
     * @return bool
     */
    public function checkRisk()
    {
        if (
            Mage::getSingleton('customer/session')->getAgent(true)
            && Mage::getSingleton('customer/session')->getAgent()->getId()
            && Mage::getSingleton('customer/session')->getAgent()->canSubmitRiskOrders()
        ) {
            return false;
        }
        $result = false;
        $packages = Mage::getModel('package/package')->getPackages(null, $this->getId())->getItems();
        foreach ($packages as $package) {
            // RFC-140722 REQ2.1 Check if delivery address is not billing address and package is retention
            $changeWithRetention = ($package->getSaleType() == Omnius_Checkout_Model_Sales_Quote_Item::RETENTION) && ($this->getPackageShippingData($package->getPackageId())['address'] != 'billing_address');
            // RFC-140722 REQ2.3 Check if any of the packages have number porting
            $hasPorting = $package->getSaleType() == Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION && $package->getCurrentNumber();
            // RFC-140722 REQ2.4 Acquisition package with Pay on Bill and more than 3 package
            $acqWithThreePackages = (count($packages) > 3) && ($package->getSaleType() == Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION) && ($this->getPackageShippingData($package->getPackageId())['payment'] == 'checkmo');
            // RFC-150064 REQ1: Change conditions for creating risk order at Telesales business customer acquisition as a risk condition
            $businessAcq = ($package->getSaleType() == Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION) && $this->getCustomerIsBusiness();
            if ($changeWithRetention || $hasPorting || $acqWithThreePackages || $businessAcq) {
                $result = true;
                break;
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getTypeOfCartLiteral()
    {
        if ($this->isRisk()) {
            $result = 'risk';
        } elseif ($this->isOffer()) {
            $result = 'offer';
        } elseif ($this->isSavedCart()) {
            $result = 'cart';
        } else {
            $result = 'generic';
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isRisk()
    {
        return $this->getIsOffer() && $this->getCartStatus() == self::CART_STATUS_RISK;
    }

    /**
     * @return bool
     */
    public function isOffer()
    {
       return $this->getIsOffer() && $this->getCartStatus() != self::CART_STATUS_RISK;
    }

    /**
     * @return bool
     */
    public function isSavedCart()
    {
        return $this->getCartStatus() == self::CART_STATUS_SAVED;
    }

    /**
     * Returns the shipping type and delivery type for a package
     *
     * @param int $packageId
     * @throws Exception
     * @return array
     */
    public function getPackageShippingData($packageId)
    {
        $deliveryInfo = Mage::helper('core')->jsonDecode($this->getShippingData());

        if (isset($deliveryInfo['deliver'])) {
            return array('address' => $deliveryInfo['deliver']['address']['address'], 'payment' => $deliveryInfo['payment']);
        } else {
            if (isset($deliveryInfo['pakket'][$packageId]) && isset($deliveryInfo['payment'][$packageId])) {
                return array(
                    'address' => $deliveryInfo['pakket'][$packageId]['address']['address'],
                    'payment' => $deliveryInfo['payment'][$packageId]
                );
            } else {
                throw new Exception('Invalid package id');
            }
        }
    }

    /**
     * Trigger collect totals for the quote
     */
    public function clearQuote()
    {
        foreach ($this->getAllItems() as $item) {
            $item->isDeleted(1);
        }
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId());
        foreach ($packages as $package) {
            $package->delete();
        }

        $this->collectTotals();
    }

    /**
     * Reload quote to refresh items
     * @param int $id
     * @param null $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        $this->_beforeLoad($id, $field);

        $coll = Mage::getModel('sales/quote')->getCollection();

        if ($field) {
            $coll->addFieldToFilter($field, $id);
        } else {
            $coll->addFieldToFilter('entity_id', $id);

        }
        $q = $coll->getFirstItem();
        $this->setData($q->getData());

        $this->_afterLoad();
        $this->setOrigData();
        $this->_hasDataChanges = false;
        return $this;
    }

    /**
     * @param $status
     * @param $quoteItems
     * @param $packages
     * @return array
     */
    protected function getPackageDataByType($status, $quoteItems, $packages)
    {
        $packagesAll = array();
        foreach ($quoteItems as $item) {
            if ($item->isDeleted()) {
                continue;
            }
            $packageId = $item->getPackageId();
            if ($status) {
                if ($packages[$packageId]['current_status'] == $status) {
                    if ($item->isPromo()) {
                        $packagesAll[$packageId][Omnius_Catalog_Model_Type::SUBTYPE_PROMOTION][$item->getId()] = $item;
                    } else {
                        $packagesAll[$packageId][current($item->getProduct()->getType())][$item->getId()] = $item;
                    }
                }
            } else {
                if ($item->getProduct()) {
                    if ($item->isPromo()) {
                        $packagesAll[$packageId][Omnius_Catalog_Model_Type::SUBTYPE_PROMOTION][$item->getId()] = $item;
                    } else {
                        $packagesAll[$packageId][current($item->getProduct()->getType())][$item->getId()] = $item;
                    }
                }
            }
        }

        return $packagesAll;
    }
}
