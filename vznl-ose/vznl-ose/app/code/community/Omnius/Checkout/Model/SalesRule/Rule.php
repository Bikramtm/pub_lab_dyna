<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Model_SalesRule_Discount
 */
class Omnius_Checkout_Model_SalesRule_Rule extends Mage_SalesRule_Model_Rule
{
    /**
     * reset rule address to empty array
     */
    public function resetRuleAddress(){
        $this->_validatedAddresses = array();
    }
    
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Get sales rule customer group Ids
     *
     * @return array
     */
    public function getCustomerGroupIds()
    {
        if (!$this->hasCustomerGroupIds()) {
            $key = sprintf('salesrule_customer_groups_%s', $this->getId());
            if ($result = ((array)unserialize($this->getCache()->load($key)))) {
                $this->setData('customer_group_ids', $result);
                return $result;
            } else {
                $result = (array) $this->_getResource()->getCustomerGroupIds($this->getId());
                $this->setData('customer_group_ids', $result);
                $this->getCache()->save(serialize($result), $key, array(Mage_Customer_Model_Customer::CACHE_TAG), $this->getCache()->getTtl());
                return $result;
            }
        }
        return $this->_getData('customer_group_ids');
    }

    /**
     * Set if not yet and retrieve rule store labels
     *
     * @return array
     */
    public function getStoreLabels()
    {
        if (!$this->hasStoreLabels()) {
            $key = sprintf('salesrule_store_labels_%s', $this->getId());
            if ($labels = (unserialize($this->getCache()->load($key)))) {
                $this->setStoreLabels($labels);
                return $labels;
            } else {
                $labels = $this->_getResource()->getStoreLabels($this->getId());
                $this->setStoreLabels($labels);
                $this->getCache()->save(serialize($labels), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
                return $labels;
            }
        }

        return $this->_getData('store_labels');
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * Assure item has an unique_id
     *
     * @return Omnius_Checkout_Model_SalesRule_Rule
     */
    protected function _beforeSave()
    {
        if ( ! $this->getData('unique_id')) {
            $this->setData('unique_id', hash('sha256', (time() . rand(0, 100) . __FILE__)));
        }
        return parent::_beforeSave();
    }

    /**
     * Save method triggers many other events so in case you only need to update a field that should not
     * affect other data, this function can be used as it is much faster
     */
    public function updateFields($data)
    {
        if($data && $this->getId()) {
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            $query = "update salesrule set ";
            $i=0;
            foreach($data as $field => $value) {
                $this->setData($field, $value);
                $value = is_numeric($value) ? $value : "'" . $value . "'";
                $query .= ($i ? ',' : '') . $field . '=' . $value;
                $i++;
            }
            $query .= " where rule_id = " . $this->getId();
            $write->query($query);
        }
    }
}
