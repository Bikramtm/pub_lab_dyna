<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Discount calculation model
 *
 * @category    Omnius
 * @package     Omnius_Checkout
 */
class Omnius_Checkout_Model_SalesRule_Quote_Discount extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    /**
     * @var Omnius_PriceRules_Model_Validator
     */
    protected $_calculator;

    protected $_promos = null;

    /**
     * Initialize discount collector
     */
    public function __construct()
    {
        $this->setCode('maf_discount');
        $this->_calculator = Mage::getSingleton('salesrule/validator');
    }

    /**
     * Collect address discount amount
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_SalesRule_Model_Quote_Discount
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
        $this->_setAddress($address);
        /**
         * Reset amounts
         */
        $this->_setMafAmount(0);
        $this->_setBaseMafAmount(0);

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }

        $quote = $address->getQuote();
//        $couponsPerPackage = $quote->getCouponsPerPackage();
        //Remember the previous active package id.
        $prevActivePackageId = $quote->getActivePackageId();
        /*$store = Mage::app()->getStore($quote->getStoreId());

        $packages = $quote->getPackageModels();

        $this->_calculator->init($store->getWebsiteId(), $quote->getCustomerGroupId(), '', $packages, $address);

        foreach ($items as $item) {
            $packageModel = $packages->getItemByColumnValue('package_id', $item->getPackageId());

            //workaround for packages having id as ctn - to be removed
            if (!$packageModel || !$packageModel->getId()) {
                $packageModel = $packages->getItemByColumnValue('ctn', $item->getPackageId());
            }
            if (!$packageModel) {
                continue;
            }

            $quote->setActivePackageId($item->getPackageId());
            $coupon = isset($couponsPerPackage[$item->getPackageId()]) ? $couponsPerPackage[$item->getPackageId()] : $packageModel->getCoupon();
            $this->_calculator
                ->setActivePackage($packageModel)
                ->setCouponCode($coupon)
                ->setWebsiteId($store->getWebsiteId())
                ->setCustomerGroupId($quote->getCustomerGroupId())
            ;


            $eventArgs = array(
                'website_id' => $store->getWebsiteId(),
                'customer_group_id' => $quote->getCustomerGroupId(),
                'coupon_code' => $packageModel->getCoupon(),
            );

            $eventArgs['item'] = $item;
            Mage::dispatchEvent('sales_quote_address_discount_item', $eventArgs);

            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $this->_calculator->process($child);
                }
            } else {
                $this->_calculator->process($item);
            }
        }
*/
        $address->unsetData('cached_items_nonnominal');
        $address->unsetData('cached_items_all');
        $address->unsetData('cached_items_nominal');
        $this->_initPromos($address);

        foreach($items as $item) {
            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $this->process($child);
                    $this->processMaf($child);
                    $this->_aggregateItemDiscount($child);
                }
            } else {
                $this->process($item);
                $this->processMaf($item);
                $this->_aggregateItemDiscount($item);
            }
        }

        $quote->setActivePackageId($prevActivePackageId);

        /*$itemIds = array();
        foreach ($packages as $pack) {
            if (
                isset($this->_calculator->couponsApplied[$pack->getPackageId()])
                && $this->_calculator->couponsApplied[$pack->getPackageId()] != $pack->getCoupon()
            ) {
                Mage::helper('pricerules')->incrementCoupon($this->_calculator->couponsApplied[$pack->getPackageId()], $quote->getCustomer()->getId());
                $pack->setCoupon($this->_calculator->couponsApplied[$pack->getPackageId()])->save();
            } elseif (
                !isset($this->_calculator->couponsApplied[$pack->getPackageId()])
                && $pack->getCoupon() != null
            ) {
                Mage::helper('pricerules')->decrementCoupon($pack->getCoupon(), $quote->getCustomer()->getId());
                $itemIds[] = $pack->getId();
            }
        }
        if (count($itemIds)) {
            $conn->update('catalog_package', array('coupon' => null, 'updated_at' => now()), sprintf('entity_id in (%s)', join(',', $itemIds)));
        }*/

        return $this;
    }

    /**
     * Quote item discount calculation process
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @return  Mage_SalesRule_Model_Validator
     */
    public function process(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $item->setDiscountAmount(0);
        $item->setBaseDiscountAmount(0);

        $itemPrice = $this->_getItemPrice($item);

        if ($itemPrice < 0) {
            return $this;
        }

        $connectionCost = $item->getProduct()->getData('prijs_aansluitkosten');
        $qty = $item->getTotalQty();

        if(isset($this->_promos[$item->getPackageId()]) &&
            Mage::helper('omnius_catalog')->is(
                array(
                    Omnius_Catalog_Model_Type::SUBTYPE_ADDON,
                    Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                ), $item->getProduct()
            )
        ) {

            foreach($this->_promos[$item->getPackageId()] as $promo) {
                $promoProd = $promo->getProduct();
                if ($promo->getTargetId() == $item->getProductId()){
                    if($promoProd->getData('prijs_aansluit_promo_bedrag')) {
                        if($promoProd->getData('prijs_aansluit_promo_bedrag') < $item->getProduct()->getData('prijs_aansluitkosten')) {
                            $quoteAmount        = $promoProd->getData('prijs_aansluit_promo_bedrag');
                            $connectionCost     = $qty * $quoteAmount;
                        }
                    }
                    elseif($promoProd->getData('prijs_aansluit_promo_procent')) {
                        if($promoProd->getData('prijs_aansluit_promo_procent') > 0 && $promoProd->getData('prijs_aansluit_promo_procent') <= 100) {
                            $connectionCost    = ($qty * $item->getProduct()->getData('prijs_aansluitkosten')) * (float)$promoProd->getData('prijs_aansluit_promo_procent') / 100;
                        }
                    }
                }
            }
        }

        $connectionCost = min($connectionCost, $item->getProduct()->getData('prijs_aansluitkosten') * $qty);

        $item->setConnectionCost($connectionCost);

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return $this
     */
    public function processMaf(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $item->setMafDiscountAmount(0);
        $item->setBaseMafDiscountAmount(0);
        $quote      = $item->getQuote();

        $itemPrice              = $this->_getItemMaf($item);
        $baseItemPrice          = $this->_getItemBaseMaf($item);

        if ($itemPrice < 0) {
            return $this;
        }

        $discount = $item->getProduct()->getDiscountMaf();
        $quoteAmount        = $quote->getStore()->convertPrice($item->getProduct()->getDiscountMaf());

        if(isset($this->_promos[$item->getPackageId()]) &&
            Mage::helper('omnius_catalog')->is(array(Omnius_Catalog_Model_Type::SUBTYPE_ADDON, Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $item->getProduct())
        ) {

            $discount = 0;
            $qty = $item->getTotalQty();
            foreach($this->_promos[$item->getPackageId()] as $promo) {
                $promoProd = $promo->getProduct();
                $unitDiscount = 0;

                if ($promo->getTargetId() == $item->getProductId()){
                    if($promoProd->getData('prijs_maf_new_amount')) {
                        if($promoProd->getData('prijs_maf_new_amount') < $itemPrice) {
                            $unitDiscount = ($itemPrice - $promoProd->getData('prijs_maf_new_amount')) * $qty;
                        }
                    }
                    elseif($promoProd->getData('prijs_maf_discount_percent')) {
                        if($promoProd->getData('prijs_maf_discount_percent') > 0 && $promoProd->getData('prijs_maf_discount_percent') <= 100) {
                            $unitDiscount = ($promoProd->getData('prijs_maf_discount_percent') * $itemPrice * $qty) / 100;
                        }
                    }
                }

                $discount += $unitDiscount;

                // store also on promo maf
                $promo->setAppliedMafPromoAmount($quote->getStore()->roundPrice($quote->getStore()->convertPrice($unitDiscount)));
                $promo->setBaseAppliedMafPromoAmount($quote->getStore()->roundPrice($unitDiscount));
            }

            $quoteAmount = $quote->getStore()->convertPrice($discount);
        }

        $qty = $item->getTotalQty();

        $discountAmount = $qty * $quoteAmount;
        $baseDiscountAmount = $qty * $discount;
        $discountAmount = $quote->getStore()->roundPrice($discountAmount);
        $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);

        /**
         * We can't use row total here because row total not include tax
         * Discount can be applied on price included tax
         */

        $itemDiscountAmount = $item->getMafDiscountAmount();
        $itemBaseDiscountAmount = $item->getBaseMafDiscountAmount();

        $discountAmount = min($itemDiscountAmount + $discountAmount, $itemPrice * $qty);
        $baseDiscountAmount = min($itemBaseDiscountAmount + $baseDiscountAmount, $baseItemPrice * $qty);

        $item->setMafDiscountAmount($discountAmount);
        $item->setBaseMafDiscountAmount($baseDiscountAmount);

        return $this;
    }

    /**
     * Return item price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemMaf($item)
    {
        $price = $item->getDiscountCalculationMaf();
        $calcPrice = $item->getCalculationMaf();

        return ($price !== null) ? $price : $calcPrice;
    }

    /**
     * Return item base price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemBaseMaf($item)
    {
        $price = $item->getDiscountCalculationMaf();

        return ($price !== null) ? $item->getBaseDiscountCalculationMaf() : $item->getBaseCalculationMaf();
    }

    /**
     * Return item price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemPrice($item)
    {
        $price = $item->getDiscountCalculationPrice();
        $calcPrice = $item->getCalculationPrice();

        return ($price !== null) ? $price : $calcPrice;
    }

    /**
     * Return item base price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemBasePrice($item)
    {
        $price = $item->getDiscountCalculationPrice();

        return ($price !== null) ? $item->getBaseDiscountCalculationPrice() : $item->getBaseCalculationPrice();
    }


    /**
     * Set total model amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _setMafAmount($amount)
    {
        if ($this->_canSetAddressAmount) {
            $this->_getAddress()->setMafTotalAmount($this->getCode(), $amount);
        }

        return $this;
    }

    /**
     * Set total model base amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _setBaseMafAmount($baseAmount)
    {
        if ($this->_canSetAddressAmount) {
            $this->_getAddress()->setBaseMafTotalAmount($this->getCode(), $baseAmount);
        }

        return $this;
    }

    /**
     * Aggregate item discount information to address data and related properties
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @return  Mage_SalesRule_Model_Quote_Discount
     */
    protected function _aggregateItemDiscount($item)
    {
        $this->_addAmount(-$item->getDiscountAmount());
        $this->_addBaseAmount(-$item->getBaseDiscountAmount());

        $this->_addMafAmount(-$item->getMafDiscountAmount());
        $this->_addBaseMafAmount(-$item->getBaseMafDiscountAmount());

        return $this;
    }

    /**
     * Add discount total information to address
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_SalesRule_Model_Quote_Discount
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount = $address->getMafDiscountAmount();
        if ($amount != 0) {
            $address->addMafTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('sales')->__('Discount'),
                'value' => $amount
            ));
        }

        return $this;
    }

    /**
     * Add total model amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _addMafAmount($amount)
    {
        if ($this->_canAddAmountToAddress) {
            $this->_getAddress()->addMafTotalAmount($this->getCode(), $amount);
        }

        return $this;
    }

    /**
     * Add total model base amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _addBaseMafAmount($baseAmount)
    {
        if ($this->_canAddAmountToAddress) {
            $this->_getAddress()->addBaseMafTotalAmount($this->getCode(), $baseAmount);
        }

        return $this;
    }

    /**
     * @param $address
     * @return array|null
     */
    protected function _initPromos($address)
    {
        $this->_promos = array();
        $items = $this->_getAddressItems($address);
        foreach ($items as $item) {
            if (!$item->isDeleted() && $item->isPromo()) {
                $promoTargetKey = $item->getProductId() . '_' . $item->getData('target_id');
                $this->_promos[$item->getPackageId()][$promoTargetKey] = $item;
            }
        }

        return $this->_promos;
    }

    /**
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param $promo
     * @param $promoProd
     * @param $itemPrice
     * @param $qty
     * @return float|int
     */
    protected function getUnitDiscount(Mage_Sales_Model_Quote_Item_Abstract $item, $promo, $promoProd, $itemPrice, $qty)
    {
        $unitDiscount = 0;

        if ($promo->getTargetId() == $item->getProductId()) {
            if ($promoProd->getData('prijs_maf_new_amount')) {
                if ($promoProd->getData('prijs_maf_new_amount') < $itemPrice) {
                    $unitDiscount = ($itemPrice - $promoProd->getData('prijs_maf_new_amount')) * $qty;
                }
            } elseif ($promoProd->getData('prijs_maf_discount_percent')) {
                if ($promoProd->getData('prijs_maf_discount_percent') > 0 && $promoProd->getData('prijs_maf_discount_percent') <= 100) {
                    $unitDiscount = ($promoProd->getData('prijs_maf_discount_percent') * $itemPrice * $qty) / 100;
                }
            }
        }

        return $unitDiscount;
    }

    /**
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param $promoProd
     * @param $qty
     * @return float
     */
    protected function getConnectionCost(Mage_Sales_Model_Quote_Item_Abstract $item, $promoProd, $qty, $connectionCost)
    {
        if ($promoProd->getData('prijs_aansluit_promo_bedrag')) {
            if ($promoProd->getData('prijs_aansluit_promo_bedrag') < $item->getProduct()->getData('prijs_aansluitkosten')) {
                $quoteAmount = $promoProd->getData('prijs_aansluit_promo_bedrag');
                $connectionCost = $qty * $quoteAmount;
            }
        } elseif ($promoProd->getData('prijs_aansluit_promo_procent')) {
            if ($promoProd->getData('prijs_aansluit_promo_procent') > 0 && $promoProd->getData('prijs_aansluit_promo_procent') <= 100) {
                $connectionCost = ($qty * $item->getProduct()->getData('prijs_aansluitkosten')) * (float) $promoProd->getData('prijs_aansluit_promo_procent') / 100;
            }
        }

        return $connectionCost;
    }

    /**
     * @param $item
     */
    protected function collectProcessDiscount($item)
    {
        if ($item->getHasChildren() && $item->isChildrenCalculated()) {
            foreach ($item->getChildren() as $child) {
                //$this->_calculator->process($child);
                $this->processMaf($child);
                $this->_aggregateItemDiscount($child);
            }
        } else {
            //$this->_calculator->process($item);
            $this->processMaf($item);
            $this->_aggregateItemDiscount($item);
        }
    }

    /**
     * @param $item
     */
    protected function collectProcessItem($item)
    {
        if ($item->getHasChildren() && $item->isChildrenCalculated()) {
            foreach ($item->getChildren() as $child) {
                $this->_calculator->process($child);
            }
        } else {
            $this->_calculator->process($item);
        }
    }
}
