<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
// @codingStandardsIgnoreFile

class Omnius_Checkout_Model_Mysql4_Portability extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("omnius_checkout/portability", "entity_id");
    }
}
