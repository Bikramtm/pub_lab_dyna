<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();
$tables = ['quote', 'order', 'quote_address'];

$options = [
    'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'precision' => 12,
    'scale' => 4,
    'required' => false
];

$attributes = [
    'initial_mixmatch_subtotal' => 'Initial mixmatch subtotal',
    'initial_mixmatch_tax'      => 'Initial mixmatch tax',
    'initial_mixmatch_total'    => 'Initial mixmatch grand total'

];

foreach($tables as $table) {
    foreach($attributes as $key => $comment){
        $tempOps = $options;
        $tempOps['comment'] = $comment;
        $salesInstaller->addAttribute($table, $key, $tempOps);
    }
}
/* end QUOTE and ORDER item attributes */
$salesInstaller->endSetup();
