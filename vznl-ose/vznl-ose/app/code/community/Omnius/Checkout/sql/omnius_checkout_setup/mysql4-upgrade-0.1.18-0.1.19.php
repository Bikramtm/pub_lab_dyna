<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$quoteOrderItemAttr = array(
   
    'contractant_id_type' => array(
        'comment'       => 'Contractant Id Type',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'contractant_issuing_country' => array(
        'comment'       => 'Contractant Country',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'contractant_id_number' => array(
        'comment'       => 'Contractant Id Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}

$salesInstaller->endSetup();
