<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Omnius
 * @package     Omnius_Checkout
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$tables = [
    'sales_flat_order',
    'sales_flat_order_item'
];

foreach ($tables as $value) {
    $installer->getConnection()->addColumn($this->getTable($value), 'additional_attributes', 'TEXT');
}

$installer->endSetup();
