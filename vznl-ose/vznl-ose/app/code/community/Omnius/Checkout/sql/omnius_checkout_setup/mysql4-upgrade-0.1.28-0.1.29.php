<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();


/* start ORDER attributes */
$quoteOrderItemAttr = array(

    'esb_order_status' => array(
        'comment'       => 'ESB Order Status',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),

);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}
/* end ORDER attributes */

$salesInstaller->endSetup();
