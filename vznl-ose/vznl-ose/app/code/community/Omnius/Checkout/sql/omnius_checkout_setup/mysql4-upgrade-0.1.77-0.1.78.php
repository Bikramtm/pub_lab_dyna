<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Get the resource model
 */
$resource = Mage::getSingleton('core/resource');
/**
 * Retrieve the write connection
 */
$writeConnection = $resource->getConnection('core_write');

$table = $resource->getTableName('sales/quote');
$query = "UPDATE eav_attribute set backend_model= 'eav/entity_attribute_backend_array' WHERE attribute_code = 'identifier_simcard_allowed'";
$writeConnection->query($query);
