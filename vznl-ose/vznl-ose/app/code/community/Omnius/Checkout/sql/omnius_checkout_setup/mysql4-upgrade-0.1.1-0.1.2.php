<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE and ORDER attributes */
$quoteOrderItemAttr = array(
    'package_id' => array(
        'comment'       => 'Package ID',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order_item', $attributeCode, $attributeProp);
}
/* end QUOTE and ORDER attributes */

/* start QUOTE attributes */
$quoteOrderItemAttr = array(
    'active_package_id' => array(
        'comment'       => 'Active Package ID',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote', $attributeCode, $attributeProp);
}
/* end QUOTE attributes */

$salesInstaller->endSetup();