<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();
$salesInstaller->getConnection()->addColumn($salesInstaller->getTable('sales/quote'), 'hawaii_shopping_order_id', 'VARCHAR(255)');
$salesInstaller->endSetup();