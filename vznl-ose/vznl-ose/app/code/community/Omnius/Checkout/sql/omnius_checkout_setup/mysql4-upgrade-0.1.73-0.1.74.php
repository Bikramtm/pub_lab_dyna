<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'is_offer', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment'   => 'Quote is offer flag',
        'required'  => false,
        'default'   => 0
    ));
$installer->endSetup();
