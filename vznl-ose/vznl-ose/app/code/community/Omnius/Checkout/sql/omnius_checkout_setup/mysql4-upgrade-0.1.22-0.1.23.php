<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE and ORDER item attributes */
$quoteOrderItemAttr = array(
    'new_holland' => array(
        'comment'       => 'New Holland',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order_item', $attributeCode, $attributeProp);
}
/* end QUOTE and ORDER item attributes */

/* start QUOTE and ORDER attributes */
$quoteOrderItemAttr = array(

    'additional_telephone' => array(
        'comment'       => 'Additional phone numbers separated by ;',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'additional_fax' => array(
        'comment'       => 'Additional faxes separated by ;',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'additional_email' => array(
        'comment'       => 'Additional emails separated by ;',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}
/* end QUOTE and ORDER attributes */

$salesInstaller->endSetup();
