<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$attributeProp = array(
    'comment'       => 'Is package cancelled',
    'type'          => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'required'      => false
);
$salesInstaller->addAttribute('quote_item', 'is_cancelled', $attributeProp);

$salesInstaller->endSetup();