<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();
$customerInstaller->getConnection()->addIndex($this->getTable('sales/order'), 'IDX_SALES_FLAT_PARENT_ID', 'parent_id');
$customerInstaller->endSetup();