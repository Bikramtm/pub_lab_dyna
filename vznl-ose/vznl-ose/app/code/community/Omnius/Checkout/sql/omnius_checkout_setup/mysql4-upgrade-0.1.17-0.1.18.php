<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

// add quote item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'maf_discount_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Discount Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'base_maf_discount_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Discount Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'maf_row_total_with_discount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Row Total With Discount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'maf_tax_before_discount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Tax Before Discount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'base_maf_tax_before_discount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Tax Before Discount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




// add quote address item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'maf_discount_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Discount Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'base_maf_discount_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Discount Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'maf_row_total_with_discount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Row Total With Discount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));



// add quote address item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'maf_discount_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Discount Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'base_maf_discount_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Discount Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'maf_tax_before_discount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Tax Before Discount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'base_maf_tax_before_discount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Tax Before Discount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




// quote columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'maf_subtotal_with_discount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Subtotal With Discount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'base_maf_subtotal_with_discount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Subtotal With Discount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




// quote address columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'maf_discount_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Discount Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'base_maf_discount_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Discount Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'maf_subtotal_with_discount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Subtotal With Discount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'base_maf_subtotal_with_discount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Subtotal With Discount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




// order columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'maf_discount_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Discount Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'base_maf_discount_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Discount Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));



$installer->endSetup();