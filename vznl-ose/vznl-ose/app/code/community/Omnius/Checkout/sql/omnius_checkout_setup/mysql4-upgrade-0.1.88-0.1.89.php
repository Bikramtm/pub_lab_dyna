<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();

$connection = $customerInstaller->getConnection();
$connection->modifyColumn($this->getTable('sales/quote'), 'cart_status',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 20,
        'nullable' => true,
        'required' => false,
        'comment' => 'Cart status'
    ]
);
$connection->addIndex($this->getTable('sales/quote'), 'IDX_SALES_FLAT_QUOTE_CARTSTATUS', 'cart_status');
$connection->modifyColumn($this->getTable('sales/quote'), 'quote_parent_id',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length' => 11,
        'nullable' => true,
        'required' => false,
        'comment' => 'Quote parent id',
        'unsigned' => true
    ]
);
$connection->addIndex($this->getTable('sales/quote'), 'IDX_SALES_FLAT_QUOTE_QUOTEPARENTID', 'quote_parent_id');

$customerInstaller->endSetup();