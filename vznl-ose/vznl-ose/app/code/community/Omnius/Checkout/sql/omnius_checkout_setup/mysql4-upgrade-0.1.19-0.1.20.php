<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE and ORDER attributes */
$quoteOrderItemAttr = array(
    'fixed_to_mobile' => array(
        'comment'       => 'Fixed to mobile ID',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'vodafone_home' => array(
        'comment'       => 'Fixed to mobile ID',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
);

foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order_item', $attributeCode, $attributeProp);
}
/* end QUOTE and ORDER attributes */

$salesInstaller->endSetup();
