<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* Add QUOTE and ORDER attribute */
$attributeProp = array(
    'comment'       => 'Sale type (retention, acquisition, inlife)',
    'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'size'          => 100,
    'required'      => false
);
$salesInstaller->addAttribute('quote_item', 'sale_type', $attributeProp);
$salesInstaller->addAttribute('order_item', 'sale_type', $attributeProp);

$salesInstaller->endSetup();