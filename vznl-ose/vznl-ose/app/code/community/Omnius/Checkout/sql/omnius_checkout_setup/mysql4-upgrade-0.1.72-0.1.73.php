<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$salesInstaller->getConnection()->dropColumn('sales_flat_order', 'custom_increment_id');

/* end ORDER_ADDRESS attributes */
$salesInstaller->endSetup();