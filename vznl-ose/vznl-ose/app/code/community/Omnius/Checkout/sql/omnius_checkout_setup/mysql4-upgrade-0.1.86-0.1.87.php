<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$connection->addColumn($this->getTable('salesrule/rule'), 'unique_id',
    [
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment'   => 'Unique id',
        'length' => 64,
        'nullable' => true,
        'required' => false,
    ]
);
$connection->addColumn($this->getTable('salesrule/rule'), 'unique_id',
    [
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment'   => 'Locked',
        'nullable' => true,
        'required' => false,
    ]
);
$connection->addIndex($this->getTable('salesrule/rule'), 'IDX_SALESRULE_UNIQUE_ID', 'unique_id');

/**
 * Update existing rules
 */
$updates = array();
$hashes = array();

$collection = Mage::getResourceModel('salesrule/rule_collection');
$collection->setPageSize(100);
$currentPage = 1;
$pages = $collection->getLastPageNumber();
do {
    $collection->setCurPage($currentPage);
    $collection->load();
    foreach ($collection->getData() as $item) {
        $hash = hash('sha256', $item['rule_id']);
        if (in_array($hash, $hashes)) { //if there's a change we have hash collisions
            $hash = substr_replace($hash, substr($hash, 0, 10), -10); //this should assure uniqueness in our limited set
        }
        $updates[] = sprintf("UPDATE `salesrule` SET `unique_id`='%s' WHERE `rule_id`='%s';", $hash, $item['rule_id']);
        $hashes[] = $hash;
    }
    $currentPage++;
    $collection->clear();
} while ($currentPage <= $pages);

unset($hashes);
if (count($updates)) {
    $installer->getConnection()->query(join('', $updates));
}

$installer->endSetup();
