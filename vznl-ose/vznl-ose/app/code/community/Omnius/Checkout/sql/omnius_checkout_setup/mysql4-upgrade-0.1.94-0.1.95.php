<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($installer->getTable('sales/order'), 'reason_code',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment'   => 'Status Reason Code',
        'length'    => 50,
        'required'  => false,
        'after'     => 'status',
        'nullable'  => true
    )
);
$installer->endSetup();