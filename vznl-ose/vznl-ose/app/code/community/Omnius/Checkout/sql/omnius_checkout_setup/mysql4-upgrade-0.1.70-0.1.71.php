<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup  $installer */
$installer = $this;

$installer->startSetup();

$setup = $installer->getConnection();

$setup->modifyColumn($installer->getTable('sales/quote'), 'contractant_valid_until', Varien_Db_Ddl_Table::TYPE_DATETIME);
$setup->modifyColumn($installer->getTable('sales/quote'), 'customer_valid_until', Varien_Db_Ddl_Table::TYPE_DATETIME);
$setup->modifyColumn($installer->getTable('sales/order'), 'contractant_valid_until', Varien_Db_Ddl_Table::TYPE_DATETIME);
$setup->modifyColumn($installer->getTable('sales/order'), 'customer_valid_until', Varien_Db_Ddl_Table::TYPE_DATETIME);

$installer->endSetup();
