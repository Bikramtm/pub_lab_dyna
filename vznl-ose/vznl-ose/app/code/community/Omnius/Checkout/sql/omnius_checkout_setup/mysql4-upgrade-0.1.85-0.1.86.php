<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();
$setup = Mage::getSingleton('core/resource')->getConnection('core_read');

$Idxfound = false;
foreach($setup->fetchAll('SHOW INDEX FROM sales_flat_quote') as $indexes)
{
    if($indexes['Key_name'] == 'IDX_SALES_FLAT_QUOTE_SUPERORDERID') {
        $Idxfound = true;
    }
}
if(!$Idxfound) {
    $customerInstaller->run("ALTER TABLE `sales_flat_quote`
      CHANGE COLUMN `superorder_number` `superorder_number` VARCHAR(64) NULL DEFAULT NULL COMMENT 'Superorder order number' ,
      ADD INDEX `IDX_SALES_FLAT_QUOTE_SUPERORDERID` (`superorder_number` ASC)");
}


$customerInstaller->endSetup();