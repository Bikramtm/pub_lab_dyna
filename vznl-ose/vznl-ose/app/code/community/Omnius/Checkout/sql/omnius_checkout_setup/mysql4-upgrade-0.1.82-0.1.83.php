<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();
$setup = Mage::getSingleton('core/resource')->getConnection('core_read');
$exists = $setup->fetchPairs('SELECT \'0\',COUNT(1) cnt FROM INFORMATION_SCHEMA.STATISTICS WHERE table_name = \'sales_flat_quote\' AND index_name = \'IDX_SUPERQUOTEID\'');
if(!current($exists)){
    $customerInstaller->run("ALTER TABLE `sales_flat_quote` ADD INDEX `IDX_SUPERQUOTEID` (`super_order_edit_id` ASC, `super_quote_id` ASC)");
}
$exists = $setup->fetchPairs('SELECT \'0\',COUNT(1) cnt FROM INFORMATION_SCHEMA.STATISTICS WHERE table_name = \'sales_flat_order\' AND index_name = \'IDX_SALES_FLAT_SUPERORDERID\'');
if(!current($exists)){
    $customerInstaller->run("ALTER TABLE `sales_flat_order` ADD INDEX `IDX_SALES_FLAT_SUPERORDERID` (`superorder_id` ASC);");
}

$customerInstaller->endSetup();