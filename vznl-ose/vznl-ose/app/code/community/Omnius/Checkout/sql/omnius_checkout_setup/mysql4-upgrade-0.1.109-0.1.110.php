<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$connection = $this->getConnection();
$table = $this->getTable('package_porting_info');

$connection->addColumn($table, 'validation_type', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 50,
    'comment' => 'Validation type',
    'nullable' => true,
    'required' => false,
]);
$connection->addColumn($table, 'pass_code', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 100,
    'comment' => 'Pass code',
    'nullable' => true,
    'required' => false,
]);

$this->endSetup();
