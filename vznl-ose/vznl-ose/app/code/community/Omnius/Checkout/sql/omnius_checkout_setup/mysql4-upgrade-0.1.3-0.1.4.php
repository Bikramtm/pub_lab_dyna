<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

// add quote item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'maf', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'base_maf', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'maf_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Including Tax',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'base_maf_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Including Tax',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'base_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'maf_row_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'base_maf_row_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'maf_row_total_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'base_maf_row_total_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));



// add quote address item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'maf', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'base_maf', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'maf_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Including Tax',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'base_maf_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Including Tax',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'base_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'maf_row_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'base_maf_row_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'maf_row_total_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'base_maf_row_total_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


// add quote address item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'maf', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'base_maf', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'maf_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Including Tax',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'base_maf_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Including Tax',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'base_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'maf_row_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'base_maf_row_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'maf_row_total_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'base_maf_row_total_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Row Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




// quote columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'maf_grand_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Grand Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'base_maf_grand_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Grand Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'maf_subtotal', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Subtotal',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'base_maf_subtotal', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Subtotal',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




// quote address columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'maf_grand_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Grand Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'base_maf_grand_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Grand Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'maf_subtotal', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Subtotal',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'base_maf_subtotal', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Subtotal',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'base_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'maf_subtotal_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Subtotal Including Tax',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'base_maf_subtotal_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Subtotal Including Tax',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




// order columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'maf_grand_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Grand Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'base_maf_grand_total', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Grand Total',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'maf_subtotal', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Subtotal',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'base_maf_subtotal', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Subtotal',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'base_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'maf_subtotal_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Monthly Annual Fee Subtotal Including Tax',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'base_maf_subtotal_incl_tax', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Monthly Annual Fee Subtotal Including Tax',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));



$installer->endSetup();