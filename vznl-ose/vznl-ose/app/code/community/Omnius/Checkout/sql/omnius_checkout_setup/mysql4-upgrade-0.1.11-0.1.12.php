<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE_ITEM and ORDER_ITEM attributes */
$quoteItemAttr = array(

    'tel_number' => array(
        'comment'       => 'Telephone Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'sim_number' => array(
        'comment'       => 'SIM Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'current_number' => array(
        'comment'       => 'Current Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'current_simcard_number' => array(
        'comment'       => 'Current SIM Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'contract_nr' => array(
        'comment'       => 'Contract Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'network_provider' => array(
        'comment'       => 'Network Provider',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'connection_type' => array(
        'comment'       => 'Subscription Type',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'contract_end_date' => array(
        'comment'       => 'Contract End Date',
        'type'          => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'required'      => false
    ),
    'sale_type' => array(
        'comment'       => 'Sale type (retention, acquisition, inlife)',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 100,
        'required'      => false
    ),
    'package_id' => array(
        'comment'       => 'Package ID',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'package_type' => array(
        'comment'       => 'Package type',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 50,
        'required'      => false
    ),
    'package_status' => array(
        'comment'       => 'Package status',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 10,
        'required'      => true,
        'default'       => Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN
    ),
    'ctn' => array(
        'comment'       => 'Customer CTN',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 50,
        'required'      => false
    ),
);
foreach ($quoteItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_address_item', $attributeCode, $attributeProp);
}
/* end QUOTE_ITEM and ORDER_ITEM attributes */

$salesInstaller->endSetup();
