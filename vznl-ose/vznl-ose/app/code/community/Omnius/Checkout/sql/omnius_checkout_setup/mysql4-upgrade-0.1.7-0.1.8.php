<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE and ORDER attributes */
$attributeProp = array(
    'comment'       => 'Customer CTN',
    'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'size'          => 50,
    'required'      => false
);
$salesInstaller->addAttribute('quote_item', 'ctn', $attributeProp);
$salesInstaller->addAttribute('order_item', 'ctn', $attributeProp);
/* end QUOTE and ORDER attributes */

$salesInstaller->endSetup();