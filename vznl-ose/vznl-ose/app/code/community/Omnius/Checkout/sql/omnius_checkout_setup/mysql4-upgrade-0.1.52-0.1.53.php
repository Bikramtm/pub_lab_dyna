<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE_ITEM and ORDER_ITEM attributes */
$quoteItemAttr = array(
    'network_operator' => array(
        'comment'       => 'Network Provider',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    )
);
foreach ($quoteItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_address_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('quote_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order_item', $attributeCode, $attributeProp);
}
/* end QUOTE_ITEM and ORDER_ITEM attributes */

$salesInstaller->endSetup();
