<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start ORDER_ADDRESS attributes */
$salesAddressAttr = array(
    'delivery_type' => array(
        'comment'       => 'Type of the delivery',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false,
    ),
    'payment_type' => array(
        'comment'       => 'Type of payment',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false,
    ),
);

foreach ($salesAddressAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('order_address', $attributeCode, $attributeProp);
}

/* end ORDER_ADDRESS attributes */

$salesInstaller->endSetup();