<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$sql = <<<SQL
ALTER TABLE `sales_flat_quote_item` CHANGE COLUMN `sale_type` `sale_type` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Sale type (retetion, acquisition, in-life)';
SQL;

$sql .= <<<SQL
ALTER TABLE `sales_flat_order_item` CHANGE COLUMN `imei` `imei` VARCHAR(17) NULL DEFAULT NULL COMMENT 'Phone IMEI';
ALTER TABLE `sales_flat_quote_item` CHANGE COLUMN `imei` `imei` VARCHAR(17) NULL DEFAULT NULL COMMENT 'Phone IMEI';
SQL;

$sql .= <<<SQL
ALTER TABLE `sales_flat_order` CHANGE COLUMN `contractant_id_type` `contractant_id_type` VARCHAR(1) NULL DEFAULT NULL COMMENT 'Contractant Id Type';
ALTER TABLE `sales_flat_quote` CHANGE COLUMN `contractant_id_type` `contractant_id_type` VARCHAR(1) NULL DEFAULT NULL COMMENT 'Contractant Id Type';

ALTER TABLE `sales_flat_order` CHANGE COLUMN `contractant_issuing_country` `contractant_issuing_country` VARCHAR(2) NULL DEFAULT NULL COMMENT 'Contractant Country';
ALTER TABLE `sales_flat_quote` CHANGE COLUMN `contractant_issuing_country` `contractant_issuing_country` VARCHAR(2) NULL DEFAULT NULL COMMENT 'Contractant Country';

ALTER TABLE `sales_flat_order` CHANGE COLUMN `contractant_id_number` `contractant_id_number` VARCHAR(20) NULL DEFAULT NULL COMMENT 'Contractant Id Number';
ALTER TABLE `sales_flat_quote` CHANGE COLUMN `contractant_id_number` `contractant_id_number` VARCHAR(20) NULL DEFAULT NULL COMMENT 'Contractant Id Number';
SQL;

$sql .= <<<SQL
ALTER TABLE `sales_flat_quote` CHANGE COLUMN `cart_status_step` `cart_status_step` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Step on which the cart must resume';
SQL;

$sql .= <<<SQL
ALTER TABLE `sales_flat_quote` CHANGE COLUMN `current_step` `current_step` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Current step';
SQL;

$sql .= <<<SQL
ALTER TABLE `sales_flat_quote` CHANGE COLUMN `prospect_saved_quote` `prospect_saved_quote` INTEGER(11) NULL DEFAULT NULL COMMENT 'Prospect save quote id';
SQL;

$sql .= <<<SQL
ALTER TABLE `sales_flat_order_address` CHANGE COLUMN `delivery_type` `delivery_type` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Type of the delivery';
ALTER TABLE `sales_flat_order_address` CHANGE COLUMN `payment_type` `payment_type` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Type of payment';
SQL;

$sql .= <<<SQL
ALTER TABLE `sales_flat_quote`
CHANGE COLUMN `privacy_sms` `privacy_sms` TINYINT(1) NULL DEFAULT NULL COMMENT 'Privacy SMS' ,
CHANGE COLUMN `privacy_email` `privacy_email` TINYINT(1) NULL DEFAULT NULL COMMENT 'Privacy Email' ,
CHANGE COLUMN `privacy_mailing` `privacy_mailing` TINYINT(1) NULL DEFAULT NULL COMMENT 'Privacy Mailing' ,
CHANGE COLUMN `privacy_telemarketing` `privacy_telemarketing` TINYINT(1) NULL DEFAULT NULL COMMENT 'Privacy Telemarketing' ,
CHANGE COLUMN `privacy_number_information` `privacy_number_information` TINYINT(1) NULL DEFAULT NULL COMMENT 'Privacy Number Information' ,
CHANGE COLUMN `privacy_phone_books` `privacy_phone_books` TINYINT(1) NULL DEFAULT NULL COMMENT 'Privacy Phone Books' ,
CHANGE COLUMN `privacy_disclosure_commercial` `privacy_disclosure_commercial` TINYINT(1) NULL DEFAULT NULL COMMENT 'Privacy Disclosure Commercial' ,
CHANGE COLUMN `privacy_market_research` `privacy_market_research` TINYINT(1) NULL DEFAULT NULL COMMENT 'Privacy Market Research' ,
CHANGE COLUMN `privacy_sales_activities` `privacy_sales_activities` TINYINT(1) NULL DEFAULT NULL COMMENT 'Privacy Sales Activities' ,
CHANGE COLUMN `privacy_mms` `privacy_mms` TINYINT(1) NULL DEFAULT NULL COMMENT 'Privacy MMS' ;
SQL;


$sql .= <<<SQL
ALTER TABLE `sales_flat_order` CHANGE COLUMN `account_validated` `account_validated` TINYINT(1) NULL DEFAULT NULL COMMENT 'Fraud suspect check';
ALTER TABLE `sales_flat_quote` CHANGE COLUMN `account_validated` `account_validated` TINYINT(1) NULL DEFAULT NULL COMMENT 'Fraud suspect check';
SQL;

$sql .= <<<SQL
ALTER TABLE `sales_flat_order_item` CHANGE COLUMN `network_operator` `network_operator` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Network Provider';
ALTER TABLE `sales_flat_order_item` CHANGE COLUMN `network_operator` `network_operator` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Network Provider';
ALTER TABLE `sales_flat_quote_address_item` CHANGE COLUMN `network_operator` `network_operator` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Network Provider';
SQL;

$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();
$salesInstaller->run($sql);
$salesInstaller->endSetup();
