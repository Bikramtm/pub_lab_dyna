<?php
$installer = $this;

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId = $setup->getEntityTypeId('customer');
$attributeSetId = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
$newAttributeName = 'additional_attributes';

$setup->addAttribute('customer', $newAttributeName, [
    'type' => 'text',
    'note' => 'Additional attributes',
    'visible' => true,
    'required' => false,
    'default' => null,
    'unique' => false
]);

$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $newAttributeName);

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    $newAttributeName,
    '100'
);

$attribute->setData('used_in_forms', ['adminhtml_customer'])
    ->setData('is_used_for_customer_segment', true)
    ->setData('is_system', 0)
    ->setData('is_user_defined', 1)
    ->setData('is_visible', 1)
    ->setData('sort_order', 100);
$attribute->save();

$installer->endSetup();
