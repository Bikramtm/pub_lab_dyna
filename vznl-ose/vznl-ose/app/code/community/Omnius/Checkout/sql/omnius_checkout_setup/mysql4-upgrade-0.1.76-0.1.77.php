<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @var $helper Omnius_Catalog_Helper_Setup
 */
$helper = Mage::helper('omnius_catalog/setup');

//if attributes don't exist, create them and associate them to the corresponding attribute sets
foreach(array('identifier_simcard_formfactor' => 'Simkaart formaat', 'identifier_simcard_allowed' => 'Toegestaan Simkaart') as $attr => $label){
    $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
        ->setCodeFilter($attr)
        ->getFirstItem();
    $newAttributeId = $attributeInfo->getId();

    if(!$newAttributeId){
        $helper->createAttribute(
            $attr,
            $label,
            $attr == 'identifier_simcard_formfactor' ? 'select' : 'multiselect',
            array(
                'simple',
                'configurable',
                'virtual'
            )
        );

        $helper->addAttributeToAttributeSet(
            'catalog_product',
            $attr,
            $attr == 'identifier_simcard_formfactor'
                ? array('voice_device', 'data_device', 'default', 'simcard')
                : array('voice_subscription', 'data_subscription', 'default')
        );
    }
}
