<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Checkout_Model_Resource_Setup */
$installer = new Mage_Checkout_Model_Resource_Setup('core_setup');
$installer->startSetup();

$installer->getConnection()->modifyColumn($this->getTable('sales/quote'), 'cart_status',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable' => true,
        'required' => false,
    ]
);

$installer->endSetup();