<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Sales_Model_Entity_Setup */

$this->startSetup();

$this->getConnection()
    ->addColumn($this->getTable('sales/quote'), 'dummy_email', array(
        'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment' => 'dummy email checkout checkbox field',
        'required' => false,
    ));
$this->endSetup();
