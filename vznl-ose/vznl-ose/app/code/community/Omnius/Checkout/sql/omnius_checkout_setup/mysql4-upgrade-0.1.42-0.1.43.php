<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start ORDER_ADDRESS attributes */
$salesAddressAttr = array(
    'custom_increment_id' => array(
        'comment'       => 'Custom id for the delivery order',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false,
    ),
);

foreach ($salesAddressAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}

/* end ORDER_ADDRESS attributes */
$salesInstaller->endSetup();
