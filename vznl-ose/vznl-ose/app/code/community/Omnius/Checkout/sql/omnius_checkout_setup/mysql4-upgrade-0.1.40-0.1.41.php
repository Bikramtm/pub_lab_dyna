<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();
$salesInstaller->getConnection()->dropColumn($salesInstaller->getTable('sales/quote'), 'edit_order_id');
$salesInstaller->getConnection()->addColumn($salesInstaller->getTable('sales/quote'), 'super_quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER);
$salesInstaller->endSetup();