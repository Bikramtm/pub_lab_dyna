<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'target_id', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'Item on which the promotion applies to',
        'default'   => 0,
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'target_id', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'Item on which the promotion applies to',
        'default'   => 0,
    ));

// add quote address item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'target_id', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'Item on which the promotion applies to',
        'default'   => 0,
    ));

$installer->endSetup();