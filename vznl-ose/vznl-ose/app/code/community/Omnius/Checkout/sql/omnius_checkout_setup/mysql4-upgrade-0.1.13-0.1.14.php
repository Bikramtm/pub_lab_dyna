<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE_ITEM and ORDER_ITEM attributes */
$quoteItemAttr = array(
    'i_ban',
    'acount'
);
foreach ($quoteItemAttr as $attributeCode) {
    $salesInstaller->getConnection()->dropColumn($salesInstaller->getTable('sales/quote_address_item'), $attributeCode);
}
/* end QUOTE_ITEM and ORDER_ITEM attributes */

/* start ORDER_ADDRESS & QUOTE_ADDRESS attributes */
$salesAddressAttr = array(
    'i_ban',
    'account'
);

foreach ($salesAddressAttr as $attributeCode) {
    $salesInstaller->getConnection()->dropColumn($salesInstaller->getTable('sales/order_address'), $attributeCode);
    $salesInstaller->getConnection()->dropColumn($salesInstaller->getTable('sales/quote_address'), $attributeCode);
}
/* end ORDER_ADDRESS & QUOTE_ADDRESS attributes */

$salesInstaller->endSetup();
