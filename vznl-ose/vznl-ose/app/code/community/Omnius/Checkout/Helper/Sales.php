<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Sales
 */
class Omnius_Checkout_Helper_Sales extends Mage_Core_Helper_Data
{
    /**
     * @param Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Quote
     */
    public function orderToQuote(Mage_Sales_Model_Order $order, $quote = null)
    {
        /** @var Mage_Sales_Model_Convert_Order $orderConverter */
        $orderConverter = Mage::getModel('sales/convert_order');

        if (!($quote instanceof Mage_Sales_Model_Quote)) {
            $quote = Mage::getModel('sales/quote');
        }
        $packagesArray = array();
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $orderConverter->toQuote($order, $quote);
        $quote->save();
        foreach ($order->getAllItems() as $orderItem) {
            /** @var Mage_Sales_Model_Quote_Item $quoteItem */
            $quoteItem = $orderConverter->itemToQuoteItem($orderItem);
            $quoteItem->setQuote($quote);
            $quoteItem->setCustomPrice($orderItem->getData('row_total'));
            $quoteItem->setOriginalCustomPrice($orderItem->getData('row_total_incl_tax'));
            $packagesArray[] = $quoteItem->getPackageId();
            $quoteItem->save();
        }
        /** @var Mage_Sales_Model_Order_Address $shippingAddress */
        $shippingAddress = clone $order->getShippingAddress();
        /** @var Mage_Sales_Model_Order_Address $billingAddress */
        $billingAddress = clone $order->getBillingAddress();
        $shippingAddress->unsetData('entity_id');
        $shippingAddress->unsetData('parent_id');
        $billingAddress->unsetData('entity_id');
        $billingAddress->unsetData('parent_id');
        $shippingAddress = Mage::getModel('sales/quote_address')
            ->setData($shippingAddress->getData());
        $billingAddress = Mage::getModel('sales/quote_address')
            ->setData($billingAddress->getData());
        $quote->setShippingAddress($shippingAddress);
        $quote->setBillingAddress($billingAddress);
        $packagesArray = array_unique($packagesArray);

        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $order->getSuperorderId())
            ->addFieldToFilter('package_id', array('in' => $packagesArray));

        foreach ($packages as $pack) {
            Mage::getModel('package/package')
                ->setData($pack->getData())
                ->setEntityId(null)
                ->setQuoteId($quote->getId())
                ->setOrderId(null)
                ->save();
        }

        return $quote;
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @return Mage_Sales_Model_Order
     */
    public function quoteToOrder(Mage_Sales_Model_Quote $quote, $order = null, $includeShipping = false)
    {
        /** @var Mage_Sales_Model_Convert_Quote $quoteConverter */
        $quoteConverter = Mage::getModel('sales/convert_quote');

        /** @var Mage_Sales_Model_Order $order */
        if (!($order instanceof Mage_Sales_Model_Order)) {
            $order = Mage::getModel('sales/order');
        }
        $order->setIncrementId($quote->getReservedOrderId())
            ->setStoreId($quote->getStoreId())
            ->setQuoteId($quote->getId())
            ->setQuote($quote)
            ->setCustomer($quote->getCustomer());

        Mage::helper('core')->copyFieldset('sales_convert_quote', 'to_order', $quote, $order);

        foreach ($quote->getAllItems() as $quoteItem) {
            /** @var Mage_Sales_Model_Order_Item $orderItem */
            $orderItem = $quoteConverter->itemToOrderItem($quoteItem);
            $orderItem->setOrder($order);
            $order->addItem($orderItem);
        }

        if ($includeShipping) {
            /** @var Mage_Sales_Model_Order_Address $shippingAddress */
            $shippingAddress = clone $quoteConverter->addressToOrderAddress($quote->getShippingAddress());
            $shippingAddress->unsetData('entity_id');
            $shippingAddress->unsetData('parent_id');
            $shippingAddress = Mage::getModel('sales/order_address')
                ->addData($shippingAddress->getData());
            $order->setShippingAddress($shippingAddress);
        }
        /** @var Mage_Sales_Model_Order_Address $billingAddress */
        $billingAddress = clone $quoteConverter->addressToOrderAddress($quote->getBillingAddress());

        $billingAddress->unsetData('entity_id');
        $billingAddress->unsetData('parent_id');

        $billingAddress = Mage::getModel('sales/order_address')
            ->addData($billingAddress->getData());
        $order->setBillingAddress($billingAddress);
        $order->setCustomerId($quote->getCustomerId());

        return $order;
    }

    /**
     * @param $superOrder
     * @param bool $fromShell
     */
    public function _generatateContracts($superOrder, $fromShell = false)
    {
        $pdfHelper = Mage::helper('omnius_checkout/pdf');

        // Generate contracts based on address as before when on telesales
        foreach ($superOrder->getOrders(true)->getItems() as $order) {
            $printOutput = Mage::app()->getLayout()
                ->createBlock('omnius_checkout/pdf_contract')
                ->init($order)
                ->setTemplate('checkout/cart/pdf/contract.phtml')
                ->setSuperorder($superOrder)
                ->toHtml();
            $pdfInstance = new VFDOMPDF;
            $pdfInstance->load_html($printOutput);
            $pdfInstance->render();
            $pdfOutput = $pdfInstance->output();

            $path = $pdfHelper->saveContract($pdfOutput, $order->getIncrementId());

            if (!file_exists($path)) {
                Mage::log("ORDER " . $superOrder->getOrderNumber() . ": Seems that our contract has not been saved to disk");
            }
        }
    }
}
