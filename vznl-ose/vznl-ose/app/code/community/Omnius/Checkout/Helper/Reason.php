<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Helper_Reason extends Mage_Core_Helper_Abstract
{
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * @return array|mixed
     */
    public function getManualActivationForDropdown()
    {
        $key = md5(__METHOD__);
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $reasons = Mage::getResourceModel('omnius_checkout/reason_collection')->load();
            $result = [['key' => '', 'value' => Mage::helper('omnius_checkout')->__('Select an option'), 'remark' => false]];
            foreach ($reasons as $key => $reason) {
                $result[] = ['key' => $reason->getName(), 'value' => $reason->getName(), 'remark' => $reason->getRequiresInput()];
            }

            $this->getCache()->save(serialize($result), $key, array(Omnius_Checkout_Model_Reason::REASON_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }
}
