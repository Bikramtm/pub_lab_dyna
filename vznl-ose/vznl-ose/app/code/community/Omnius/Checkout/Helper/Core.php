<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Helper_Core
 */
class Omnius_Checkout_Helper_Core extends Mage_Core_Helper_Data
{
    /**
     * PHP 7 backward compatibility fix
     *
     * @param string $encodedValue
     * @param int $objectDecodeType
     * @return mixed|null
     */
    public function jsonDecode($encodedValue, $objectDecodeType = Zend_Json::TYPE_ARRAY)
    {
        if (empty($encodedValue)) {
            return null;
        }

        return parent::jsonDecode($encodedValue, $objectDecodeType);
    }
}
