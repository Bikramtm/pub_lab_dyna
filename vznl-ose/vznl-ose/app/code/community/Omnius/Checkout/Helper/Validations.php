<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Helper_Validations
 */
class Omnius_Checkout_Helper_Validations extends Mage_Core_Helper_Abstract
{
    /** @var Omnius_Checkout_Helper_Data $checkoutHelper */
    protected $checkoutHelper;

    /**
     * Omnius_Checkout_Helper_Validations constructor.
     */
    public function __construct()
    {
        $this->checkoutHelper = Mage::helper('omnius_checkout');
    }

    /**
     * @param $address
     * @return mixed
     * @throws Exception
     */
    public function _getBusinessAddressByType($address)
    {
        switch ($address['company_address']) {
            case 'foreign_address' :
                if (!isset($address['foreignAddress'])) {
                    throw new Exception($this->__('Incomplete data'));
                }
                $temp = $address['foreignAddress'];
                $result = $temp;
                if (isset($temp['extra'], $temp['extra'][0]) && !empty($temp['extra'][0])) {
                    $result['company_extra_lines'] = join(';', array_filter($temp['extra'], 'strlen'));
                }
                break;
            case 'other_address' :
                if (!isset($address['otherAddress'])) {
                    throw new Exception($this->__('Incomplete data'));
                }
                $result = $address['otherAddress'];
                // Default country is NL
                $result['company_country_id'] = Omnius_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;
                break;
            default:
                throw new Exception($this->__('Incomplete data'));
        }

        return $result;
    }

    /**
     * @param $data
     * @return array
     * @throws Exception
     */
    public function _validateBillingAddressData($data)
    {
        $errors = array();
        $validations = $this->hasSubscription ? array(
            'account_no' => array(
                'required' => true,
                'validation' => array(
                    'validateBicIban'
                )
            ),
            'account_holder' => array(
                'required' => true,
                'validation' => array(
                    'validateName',
                )
            ),
            'fax' => array(
                'required' => false,
                'validation' => array(
                    'validateName',
                )
            ),
        )
            : array();

        $addressValidations = array(
            'street' => array(
                'required' => true,
                'validation' => array()
            ),
            'houseno' => array(
                'required' => false,
                'validation' => array(
                    'validateNumber'
                )
            ),
            'addition' => array(
                'required' => false,
                'validation' => array()
            ),
            'postcode' => array(
                'required' => true,
                'validation' => array(
                )
            ),
            'city' => array(
                'required' => true,
                'validation' => array()
            ),
        );

        switch ($data['address']) {
            case 'other_address' :
                $errors += $this->checkoutHelper->_performValidate($data['otherAddress'], $addressValidations,
                    'address[otherAddress][', ']');
                break;
            case 'foreign_address' :
                $addressValidations['extra'] = $addressValidations['region'] = $addressValidations['country_id'] =
                    array(
                        'required' => false,
                        'validation' => array()
                    );
                $errors += $this->checkoutHelper->_performValidate($data['foreignAddress'], $addressValidations,
                    'address[foreignAddress][', ']');
                break;
            default :
                throw new Exception($this->__('Missing billing address data'));
        }

        $errors += $this->checkoutHelper->_performValidate($data, $validations, 'address[', ']');

        return $errors;
    }

}
