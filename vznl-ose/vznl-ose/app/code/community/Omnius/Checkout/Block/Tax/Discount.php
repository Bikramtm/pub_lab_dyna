<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Dyna
 * @package     Omnius_Checkout
 */

/**
 * Discount
 */

class Omnius_Checkout_Block_Tax_Discount extends Mage_Tax_Block_Checkout_Discount
{
    /**
     * @param $total
     * @return $this
     */
    public function setMafTotal($total)
    {
        $this->setData('maf_total', $total);
        if ($total->getAddress()) {
            $this->_store = $total->getAddress()->getQuote()->getStore();
        }
        return $this;
    }
}
