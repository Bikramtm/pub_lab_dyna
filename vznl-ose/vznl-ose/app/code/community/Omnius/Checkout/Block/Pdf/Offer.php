<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Block_Pdf_Offer extends Mage_Page_Block_Html
{
    /** @var Omnius_Checkout_Model_Sales_Quote */
    private $quote = null;

    /** @var Omnius_Customer_Model_Customer_Customer */
    private $customer = null;

    /**
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    public function getQuote()
    {
        if ($this->quote == null) {
            Mage::getSingleton('checkout/session')->getQuote();
        }

        return $this->quote;
    }

    /**
     * @return Omnius_Customer_Model_Customer_Customer|Mage_Core_Model_Abstract
     */
    public function getCustomer()
    {
        if ($this->customer == null) {
            $this->customer = Mage::getModel('customer/customer')->load($this->getQuote()->getCustomerId());
        }

        return $this->customer;
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @return $this
     */
    public function setQuote(Mage_Sales_Model_Quote $quote)
    {
        $this->quote = $quote;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackages()
    {
        $packages = Mage::getModel('package/package')->getPackages(null, $this->getQuote()->getId());

        return $packages;
    }

    /**
     * @return string
     */
    public function getCustomerFullname()
    {
        $customer = $this->getCustomer();

        return Mage::helper('omnius_checkout')->__($customer->getContractantPrefix()) . ' ' . $customer->getContractantFirstname() . ' ' . $customer->getContractantMiddlename() . ' ' . $customer->getContractantLastname();
    }

    /**
     * @param DateTime|string $date
     * @return null|string
     */
    public function stripTimeFromDate($date)
    {
        if (!$date) {
            $date = null;
        } elseif (is_object($date)) {
            $date = $date->format('d/m/Y');
        } elseif (is_string($date)) {
            $date = new DateTime($date);

            $date = $date->format('d/m/Y');
        }

        return $date;
    }

    /**
     * @return null|string
     */
    public function getCreationDate()
    {
        return $this->stripTimeFromDate($this->getQuote()->getCreatedAt());
    }

    /**
     * @return array
     */
    public function getTotals()
    {
        return $this->getQuote()->calculateTotalsForPackage(null, true);
    }

    /**
     * @return bool
     */
    public function cartHasAcquisition()
    {
        foreach ($this->getPackages() as $package) {
            if ($package->getSaleType() == Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION) {
                return true;
            }
        }

        return false;
    }

} 
