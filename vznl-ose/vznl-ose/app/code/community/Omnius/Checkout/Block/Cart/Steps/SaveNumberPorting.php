<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**  */
class Omnius_Checkout_Block_Cart_Steps_SaveNumberPorting extends Omnius_Checkout_Block_Cart
{
    protected $_editedPackagesIds = array();
    protected $_disabledPackages = array();
    protected $_orderEditId = null;
    protected $_superorder = null;
    protected $_isCrossStore = null;

    /**
     * Omnius_Checkout_Block_Cart_Steps_SaveNumberPorting constructor.
     */
    public function __construct()
    {
        if ($this->_isOrderEdit()) {
            // TODO: get this values from parent block instead of registry
            $this->_editedPackagesIds = Mage::registry('editedPackages');
            $this->_disabledPackages = Mage::registry('disabledPackages');
        }

        parent::__construct();
    }

    /**
     * Verify if order edit is active
     *
     * @return int|false (Returns ID of the superOrder if order edit is active)
     */
    protected function _isOrderEdit()
    {
        if (is_null($this->_orderEditId)) {
            $this->_orderEditId = $this->getQuote() && $this->getQuote()->getSuperOrderEditId() ? $this->getQuote()->getSuperOrderEditId() : false;
        }
        return $this->_orderEditId;
    }

    /**
     * Override the default cartPackages with additional info needed for the number porting template
     *
     * @return array|null
     */
    public function getCartPackages()
    {
        $cartPackages = [];
        $packages = $this->getPackages();

        if ($packages) {
            foreach ($packages as $packageId => $package) {
                $ctns = $this->getQuote()->getPackageCtns($packageId);
                // NumberPorting not applicable if package already contains CTNs!
                if (count($ctns) > 0) {
                    continue;
                }
                $cartPackages[$packageId] = $this->getCartPackageData($package, $ctns);
            }
        }

        ksort($cartPackages);

        return $cartPackages;
    }

    /**
     * Get list of network operators as defined in backend
     *
     * @return mixed
     */
    public function getOperators()
    {
        return Mage::getModel('operator/operator')->getOperatorsWithProviders();
    }

    /**
     * Get list of network providers as defined in backend
     *
     * @return mixed
     */
    public function getProviders()
    {
        return Mage::helper('operator')->getProviderOptions();
    }

    /**
     * Get SIM card regex condition for each network provider
     *
     * @return string
     */
    public function getNetworkProviderSimRanges()
    {
        return json_encode(Mage::helper('omnius_validators')->getDatabaseSimRegex());
    }

    /**
     * Determine if number porting can apply for a specific package
     *
     * @return bool
     */
    public function isPackagePortingDisabled()
    {
        if ($this->_isOrderEdit()) {
            if (!$this->canAgentEdit()) {
                return true;
            }

            // Number porting edit is disabled in all scenarios
            $portingDisabled = true;
        } else {
            $portingDisabled = false;
        }

        return $portingDisabled;
    }

    /**
     * Retrieve superorder, only on order edit!
     * 
     * @return null
     */
    protected function getSuperorder()
    {
        if ($this->_superorder) {
            return $this->_superorder;
        }

        if ($this->_isOrderEdit()) {
            $this->_superorder = Mage::getModel('superorder/superorder')->load($this->getQuote()->getSuperOrderEditId());
        }

        return $this->_superorder;
    }

    /**
     * @return bool|null
     */
    protected function getIsCrossStore()
    {
        if (!isset($this->_isCrossStore)) {
            $this->_isCrossStore = $this->getCustomerSession()->getOrderEdit() && ($this->getSuperOrder()->getCreatedWebsiteId() != Mage::app()->getWebsite()->getId());
        }

        return $this->_isCrossStore;
    }

    /**
     * @param $package
     * @param $ctns
     * @return array
     */
    protected function getCartPackageData($package, $ctns)
    {
        $packageItems = '';
        $imageUrl = null;
        foreach ($package['items'] as $item) {
            $packageItems .= $item->getName() . ', ';
            $imageUrl = $item->getProduct()->getImageUrl();
        }
        $data = [
            'name' => Mage::helper('omnius_configurator')->getPackageTitle($ctns, $package['type']),
            'sale_type' => $package['sale_type'],
            'items' => rtrim($packageItems, ', '),
            'current_number' => $package['current_number'],
            'current_simcard_number' => $package['current_simcard_number'],
            'contract_nr' => $package['contract_nr'],
            'network_provider' => $package['network_provider'],
            'network_operator' => $package['network_operator'],
            'connection_type' => $package['connection_type'],
            'contract_end_date' => $package['contract_end_date'],
            'actual_porting_date' => $package['actual_porting_date'],
            'number_porting_type' => $package['number_porting_type'],
            'image_url' => $imageUrl
        ];

        return $data;
    }
}
