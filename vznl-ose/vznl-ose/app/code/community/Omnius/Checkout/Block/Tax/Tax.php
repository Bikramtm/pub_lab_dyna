<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Block_Tax_Tax
 */
class Omnius_Checkout_Block_Tax_Tax extends Mage_Tax_Block_Checkout_Tax
{
    /**
     * @return mixed
     */
    public function getDeviceTax()
    {
        return $this->getTotal()->getAddress()->getInitialMixmatchTax() != null ? $this->getTotal()->getAddress()->getInitialMixmatchTax() : $this->getTotal()->getAddress()->getTaxAmount();
    }
}