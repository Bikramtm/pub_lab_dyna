<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Block_Adminhtml_Activationreason_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Omnius_Checkout_Block_Adminhtml_Activationreason_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('activationReasonGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('omnius_checkout/reason_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Create the admin images list
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('omnius_checkout')->__('ID'),
            'align'     =>'right',
            'width'     => '30px',
            'index'     => 'entity_id',
        ));

        $this->addColumn('name', array(
            'header'        => Mage::helper('omnius_checkout')->__('Reason'),
            'align'         => 'left',
            'index'         => 'name'
        ));

        $this->addColumn('input', array(
            'header'    => Mage::helper('omnius_checkout')->__('Requires input from agent'),
            'align'     => 'left',
            'index'     => 'requires_input',
            'type'  => 'options',
            'options' => [
                0 => Mage::helper('omnius_checkout')->__('No'),
                1 => Mage::helper('omnius_checkout')->__('Yes'),
            ]
        ));

        return parent::_prepareColumns();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $row->getEntityId()));
    }
}
