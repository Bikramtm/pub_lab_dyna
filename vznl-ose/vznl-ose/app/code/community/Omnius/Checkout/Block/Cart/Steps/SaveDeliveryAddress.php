<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Block_Cart_Steps_SaveDeliveryAddress
 */
class Omnius_Checkout_Block_Cart_Steps_SaveDeliveryAddress extends Omnius_Checkout_Block_Cart
{
    protected $_editedPackages;

    /**
     * @return mixed
     */
    public function getEditedPackages()
    {
        if(!$this->_editedPackages) {
            $this->_editedPackages = Mage::registry('editedPackages');
        }
        return $this->_editedPackages;
    }

    /**
     * @param bool $forceOnBill
     * @return array
     */
    public function getShippingData($forceOnBill = false)
    {
        $checkoutHelper = $this->getCheckoutHelper();
        $shipping_data = Mage::helper('core')->jsonDecode($this->getQuote()->getDefaultValueForField('shipping_data'));
        $multiplePackages = $this->quoteHasMultiplePackages();
        $cartPackages = $this->getCartPackages();
        reset($cartPackages);
        $firstPackage = current($cartPackages);
        $isOneOff = $firstPackage['one_of_deal'];
        $emptyAddress = array(
            'street' => array(0 => '', 1 => '', 2 => ''),
            'street_name' => '',
            'houseno' => '',
            'addition' => '',
            'postcode' => '',
            'city' => '',
            'html' => ''
        );

        if (!$shipping_data && $multiplePackages) {
            $shipping_data = [];
            foreach ($cartPackages as $packageId => $package) {
                $storeId = Mage::getSingleton('customer/session')->getAgent(true)->getDealer()->getId();
                $shipping_data['pakket'][$packageId] = [
                    'address' => Mage::helper('omnius_checkout')->_getAddressByType([
                        'address' => 'store',
                        'store_id' => $storeId,
                    ])
                ];
                $shipping_data['payment'][$packageId] = 'payinstore';
            }
        }

        if (!$shipping_data) {
            $shipping_data['billingAddress'] = $emptyAddress;
            $shipping_data['otherAddress'] = $emptyAddress;
            $shipping_data['delivery']['method'] = 'deliver';
            $shipping_data['delivery']['address'] = 'billing_address';
            $shipping_data['payment'] = array(
                'method' => $forceOnBill ? 'checkmo' : 'cashondelivery'
            );
        } else {
            // On order edit, display delivery and payment for each package if the packages had the same delivery address
            if ($this->getIsOrderEdit() && $multiplePackages && !isset($shipping_data['pakket']) && !$isOneOff) {
                $paymentDefault = $shipping_data['payment'];
                unset($shipping_data['payment']);
                foreach ($cartPackages as $packageId => $package) {
                    $shipping_data['pakket'][$packageId] = $shipping_data['deliver'];
                    $shipping_data['payment'][$packageId] = $paymentDefault;
                }
                unset($shipping_data['deliver']);
            }

            if (is_array($shipping_data['payment'])) {
                $shipping_data['payment']['method'] = 'split-payment';
            } else {
                $payment = $shipping_data['payment'];
                $shipping_data['payment'] = array(
                    'method' => $payment
                );
            }

            // If split delivery for packages
            if (isset($shipping_data['pakket'])) {
                $shipping_data = $this->getSplitShippingData($shipping_data, $checkoutHelper);
            } else {
                $shipping_data = $this->getSingleDeliveryData($shipping_data, $checkoutHelper, $emptyAddress);
            }
        }

        return $shipping_data;
    }

    /**
     * Compare edited packages and check which ones have modified hardware
     *
     * @return Omnius_Package_Model_Package[]
     */
    protected function getEditedPackagesWithoutHardware()
    {
        $result = [];
        // Will hold all the hardware items the new packages have
        $newContents = [];
        // Will hold all the hardware items the old package had
        $oldContents = [];
        $editedPackages = $this->getEditedPackages();

        foreach ($this->getQuote()->getAllItems() as $item) { // Skip items that are not in the edited packages
            $packageId = $item->getPackageId();
            if (!in_array($packageId, $editedPackages)) {
                continue;
            }
            if (!isset($oldContents[$packageId])) {
                $oldContents[$packageId] = [];
            }
            /** @var Omnius_Catalog_Model_Product $product */
            $product = $item->getProduct();
            if ($product->isOfHardwareType()) {
                $oldContents[$packageId][] = $product->getId();
            }
        }

        /** @var Omnius_Checkout_Model_Sales_Quote $modifiedQuote */
        foreach ($this->getModifiedQuotes() as $modifiedQuote) {
            $items = $modifiedQuote->getAllItems();
            /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
            foreach ($items as $item) {
                $packageId = $item->getPackageId();

                // Skip items that are not in the edited packages
                if (!in_array($packageId, $editedPackages)) {
                    continue;
                }
                if (!isset($newContents[$packageId])) {
                    $newContents[$packageId] = [];
                }
                /** @var Omnius_Catalog_Model_Product $product */
                $product = $item->getProduct();
                if ($product->isOfHardwareType()) {
                    $newContents[$packageId][] = $product->getId();
                }

                // If the packages has a DOA item, this means hardware delivery, and must be skipped
                if ($item->getItemDoa()) {
                    $newContents[$packageId][] = 'doa';
                }
            }
            // If there are no hardware changes
            if (!empty($packageId) && !array_diff($newContents[$packageId], $oldContents[$packageId])) {
                $result[$packageId] = Mage::getModel('package/package')
                    ->getPackages(null, $modifiedQuote->getId(), $packageId)
                    ->getFirstItem();
            }
        }

        return $result;
    }

    /**
     * Return a list of the packages that should not have the rambours payment option for Telesales according to DF-005526
     * Package should be: virtual/on telesales/change after delivery/have amount to pay to be on this list
     *
     * @return int[]
     */
    public function getRamboursRestrictions()
    {
        $result = [];
        $editedPackages = $this->getEditedPackagesWithoutHardware();
        foreach ($editedPackages as $package) {
            $packageId = $package->getPackageId();
            if ($package->isDelivered() && (Mage::helper('omnius_package')->getPackageDifferences($packageId) > 0)) {
                $result[$packageId] = true;
            }
        }

        return $result;
    }

    /**
     * @param $shipping_data
     * @param $checkoutHelper
     * @return array
     */
    protected function getSplitShippingData($shipping_data, $checkoutHelper)
    {
        // Check if in this shop for retail and bc
        foreach ($shipping_data['pakket'] as &$package) {
            if (($package['address']['address'] == 'store') && ($package['address']['store_id'] == $this->getCustomerSession()->getAgent()->getDealer()->getId())) {
                $package['address']['address'] = 'direct';
            }
        }
        unset($package);

        foreach ($shipping_data['pakket'] as $packageId => $package) {
            if ($package['address']['address'] == 'billing_address' || $package['address']['address'] == 'other_address' && is_array($package['address']['street'])) {
                $shipping_data['pakket'][$packageId]['address']['street_name'] = isset($package['address']['street'][0]) ? $checkoutHelper->getTextValue($package['address']['street'][0]) : '';
                $shipping_data['pakket'][$packageId]['address']['houseno'] = isset($package['address']['street'][1]) ? $checkoutHelper->getTextValue($package['address']['street'][1]) : '';
                $shipping_data['pakket'][$packageId]['address']['addition'] = isset($package['address']['street'][2]) ? $checkoutHelper->getTextValue($package['address']['street'][2]) : '';
            }
        }

        $shipping_data['delivery']['method'] = 'split';
        $shipping_data['delivery']['address'] = 'billing_address';

        return $shipping_data;
    }

    /**
     * @param $shipping_data
     * @param $checkoutHelper
     * @param $emptyAddress
     * @return mixed
     */
    protected function getSingleDeliveryData($shipping_data, $checkoutHelper, $emptyAddress)
    {
        if ($shipping_data['deliver']['address']['address'] == 'billing_address') {
            $shipping_data['delivery']['method'] = 'deliver';
            $shipping_data['delivery']['address'] = 'billing_address';
            $shipping_data['billingAddress'] = $shipping_data['deliver']['address'];
            if (is_array($shipping_data['billingAddress']['street'])) {
                $shipping_data['billingAddress']['street_name'] = isset($shipping_data['billingAddress']['street'][0]) ? $checkoutHelper->getTextValue($shipping_data['billingAddress']['street'][0]) : '';
                $shipping_data['billingAddress']['houseno'] = isset($shipping_data['billingAddress']['street'][1]) ? $checkoutHelper->getTextValue($shipping_data['billingAddress']['street'][1]) : '';
                $shipping_data['billingAddress']['addition'] = isset($shipping_data['billingAddress']['street'][2]) ? $checkoutHelper->getTextValue($shipping_data['billingAddress']['street'][2]) : '';
            }
            $shipping_data['otherAddress'] = $emptyAddress;
            unset($shipping_data['billingAddress']['address']);
        } elseif ($shipping_data['deliver']['address']['address'] == 'other_address') {
            $shipping_data['delivery']['method'] = 'deliver';
            $shipping_data['delivery']['address'] = 'other_address';
            $shipping_data['otherAddress'] = $shipping_data['deliver']['address'];
            if (is_array($shipping_data['otherAddress']['street'])) {
                $shipping_data['otherAddress']['street_name'] = isset($shipping_data['otherAddress']['street'][0]) ? $checkoutHelper->getTextValue($shipping_data['otherAddress']['street'][0]) : '';
                $shipping_data['otherAddress']['houseno'] = isset($shipping_data['otherAddress']['street'][1]) ? $checkoutHelper->getTextValue($shipping_data['otherAddress']['street'][1]) : '';
                $shipping_data['otherAddress']['addition'] = isset($shipping_data['otherAddress']['street'][2]) ? $checkoutHelper->getTextValue($shipping_data['otherAddress']['street'][2]) : '';
            }
            $shipping_data['otherAddress']['html'] = $checkoutHelper->formatAddressAsHtml($shipping_data['otherAddress']);
            $shipping_data['billingAddress'] = $emptyAddress;
            unset($shipping_data['otherAddress']['address']);
        } elseif ($shipping_data['deliver']['address']['address'] == 'store') {
            if (
                ($shipping_data['deliver']['address']['store_id'] == $this->getCustomerSession()->getAgent(true)->getDealer()->getId())
            ) {
                // In this shop   
                $shipping_data['delivery']['method'] = 'direct';
                $shipping_data['delivery']['address'] = '';
                $shipping_data['direct']['store'] = $shipping_data['deliver']['address'];
                unset($shipping_data['pickup']['store']['address']);
            } else {
                // In another shop
                $shipping_data['delivery']['method'] = 'pickup';
                $shipping_data['delivery']['address'] = '';
                $shipping_data['pickup']['store'] = $shipping_data['deliver']['address'];
                unset($shipping_data['pickup']['store']['address']);
                $shipping_data['pickup']['store_id'] = $shipping_data['deliver']['address']['store_id'];
            }
        }

        if (empty($shipping_data['delivery']['address'])) {
            $shipping_data['delivery']['address'] = 'billing_address';
        }
        unset($shipping_data['deliver']);

        return $shipping_data;
    }
}
