<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Block_Adminhtml_Activationreason_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Omnius_Checkout_Block_Adminhtml_Activationreason_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'omnius_checkout';
        $this->_controller = 'adminhtml_activationreason';

        $this->_updateButton('save', 'label', Mage::helper('omnius_checkout')->__('Save reason'));

        $objId = $this->getRequest()->getParam($this->_objectId);

        if (!empty($objId)) {
            $this->_addButton('delete', array(
                'label' => Mage::helper('omnius_checkout')->__('Delete'),
                'class' => 'delete',
                'onclick' => 'deleteConfirm(\''
                    . Mage::helper('core')->jsQuoteEscape(
                        Mage::helper('omnius_checkout')->__('Are you sure you want to do this?')
                    )
                    . '\', \''
                    . $this->getDeleteUrl()
                    . '\')',
            ));
        }
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('reason_data') && Mage::registry('reason_data')->getEntityId()) {
            return Mage::helper('omnius_checkout')->__("Edit manual activation reason with id '%s'", $this->escapeHtml(Mage::registry('reason_data')->getEntityId()));
        } else {
            return Mage::helper('omnius_checkout')->__('Add new manual activation reason');
        }
    }
}
