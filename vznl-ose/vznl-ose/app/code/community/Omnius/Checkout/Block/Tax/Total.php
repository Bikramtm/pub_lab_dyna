<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Dyna
 * @package     Omnius_Checkout
 */

/**
 * Ttotal Total Row Renderer
 */

class Omnius_Checkout_Block_Tax_Total extends Mage_Checkout_Block_Total_Default
{
    protected $_template = 'tax/checkout/total.phtml';

    /**
     * Get subtotal excluding tax
     *
     * @return float
     */
    public function getMafSubtotalExclTax()
    {
        // this should not be a negative value, but if it is show it in front-end to check who sets it negative
        return $this->getTotal()->getAddress()->getInitialMixmatchMafTotal() != null ? $this->getTotal()->getAddress()->getInitialMixmatchMafTotal() : $this->getTotal()->getAddress()->getMafGrandTotal();
    }

    /**
     * Get subtotal excluding tax
     *
     * @return float
     */
    public function getDeviceTotal()
    {
        return $this->getTotal()->getAddress()->getInitialMixmatchTotal() != null ? $this->getTotal()->getAddress()->getInitialMixmatchTotal() : $this->getTotal()->getAddress()->getGrandTotal();
    }
}
