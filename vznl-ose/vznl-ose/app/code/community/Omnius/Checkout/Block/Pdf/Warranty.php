<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * One page checkout status
 *
 * @category   Dyna
 * @package    Omnius_Checkout
 */
class Omnius_Checkout_Block_Pdf_Warranty extends Mage_Page_Block_Html
{
    /** @var Omnius_Checkout_Model_Sales_Order  */
    protected $_order = null;
    /** @var Omnius_Checkout_Model_Sales_Order_Item|array*/
    protected $_packageItems = array();
    /** @var array */
    protected $_packageId = array();
    /** @var Omnius_Package_Model_Package null  */
    protected $_package = null;

    protected $_allWarranties = null;
    /** @var Omnius_Checkout_Model_Sales_Order_Item|null */
    protected $_warranty = null;
    /** @var Omnius_Checkout_Model_Sales_Order_Item|null */
    protected $_device = null;

    /**
     * @param Omnius_Checkout_Model_Sales_Order $order
     * @param Omnius_Checkout_Model_Sales_Order_Item[] $package
     * @param int $packageId
     *
     * @return Omnius_Checkout_Block_Pdf_Warranty
     */
    public function init($order, $package, $packageId)
    {
        $this->_order = $order;
        $this->_packageItems = $package;
        $this->_packageId = $packageId;

        $this->_initWarrantyProducts();

        return $this;
    }

    /**
     * @return $this
     */
    protected function _initWarrantyProducts() {
        $this->_allWarranties = array();

        foreach($this->_packageItems as $item) {
            if($item->getProduct() && (strtolower($item->getProduct()->getData(Omnius_Catalog_Model_Product::ADDON_TYPE_ATTR)) == 'garant'
                    || strtolower($item->getProduct()->getAttributeText(Omnius_Catalog_Model_Product::ADDON_TYPE_ATTR))=='garant'))
            {
                $this->_allWarranties[$item->getId()] = $item;
            }
        }

        return $this;
    }

    /**
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        if (!$this->_order->getCustomer() && $this->_order->getCustomerId()) {
            $customer = Mage::getModel('customer/customer')->load($this->_order->getCustomerId());
            $this->_order->setCustomer($customer);
        }

        return $this->_order->getCustomer();
    }

    /**
     * @return mixed
     */
    public function getCustomerIsBusiness()
    {
        return $this->getCustomer()->getIsBusiness();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getCustomerIsBusiness()
            ?  $this->_order->getCompanyName()
            :  join(' ', array_filter(array($this->__($this->_order->getCustomerPrefix()), $this->_order->getCustomerFirstname(), $this->_order->getCustomerMiddlename(), $this->_order->getCustomerLastname())));
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        if($this->getCustomerIsBusiness()) {
            return $this->_order->getCompanyStreet()  . ' ' . $this->_order->getCompanyHouseNr() . ', ' . $this->_order->getCompanyPostcode() . ' '. $this->_order->getCompanyCity();
        }

        $fullAddress = $this->_order->getBillingAddress();
        if ($fullAddress) {
            return $fullAddress->getStreetFull() . ', ' . $fullAddress->getPostcode() . ' ' . $fullAddress->getCity();
        } else {
            return 'no address';
        }
    }

    /**
     * @return null|string
     */
    public function getBirthDate()
    {
        return $this->getCustomerIsBusiness()
            ? $this->stripTimeFromDate($this->_order->getContractantDob()) : $this->stripTimeFromDate($this->_order->getCustomerDob());
    }

    /**
     * @return string
     */
    public function getIncrementId() {
        return $this->_order->getIncrementId();
    }

    /**
     * @return null|string
     */
    public function getOrderDate()
    {
        return $this->stripTimeFromDate($this->_order->getCreatedAt());
    }

    /**
     * @return null
     */
    public function getWarrantyProducts() {
        return $this->_allWarranties;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Order_Item|null
     */
    public function getWarrantyProduct()
    {
        return $this->_warranty;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Order_Item $item
     * @return $this
     */
    public function setWarrantyProduct($item) {
        $this->_warranty = $item;
        return $this;
    }

    /**
     * @return bool|float|null
     */
    public function getWarrantyPrice() {
        if(!$this->_warranty) {
            return false;
        }

        return $this->getCustomerIsBusiness()
            ? $this->_warranty->getItemFinalMafExclTax()
            : $this->_warranty->getItemFinalMafInclTax();
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Order_Item|mixed|null
     */
    public function getDeviceProduct() {
        if(!$this->_device) {
            foreach($this->_packageItems as $item) {
                $this->_device = $item;
                break;
            }
        }


        return $this->_device;
    }

    /**
     * @return Omnius_Package_Model_Package
     */
    public function getPackageModel()
    {
        if (!$this->_package) {
            //TODO also read items with the packages
            $this->_package = Mage::getModel('package/package')
                ->getPackages($this->_order->getSuperorderId(), null, $this->_packageId)
                ->getFirstItem();
        }

        return $this->_package;
    }

    /**
     * @return null|string
     */
    public function getEffectiveDate()
    {
        $date = '';

        $package = $this->getPackageModel();
        if(!$package) {
            return $date;
        }

        if($package->getCtn()) {
            // retention
            $ctnModel = $this->getCustomer()->getCustomerCtn($package->getCtn());
            $date = ($ctnModel && $ctnModel->getEndDate() && strtotime($ctnModel->getEndDate()->format('d-m-Y')) > time()) ? $ctnModel->getEndDate()->format('d-m-Y') : $this->getSigningDate();
        }
        elseif($package->getCurrentNumber()) {
            // number porting
            $date = $this->stripTimeFromDate($package->getContractEndDate());
        }
        elseif($package->getTelNumber()) {
            // new number
            $date = $this->getSigningDate();
        }

        return $date;
    }

    /**
     * @return null|string
     */
    public function getSigningDate() {
        if(!$this->_order->getContractSignDate()) {
            return $this->stripTimeFromDate(date("Y-m-d"));
        }

        return $this->stripTimeFromDate($this->_order->getContractSignDate());
    }

    /**
     * @param $date
     * @return null|string
     */
    public function stripTimeFromDate($date)
    {
        if (!$date) {
            return null;
        }
        $date = new DateTime($date);

        return $date->format('d-m-Y');
    }
}
