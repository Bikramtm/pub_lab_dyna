<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @class Omnius_Checkout_Block_Cart_Steps_SaveCustomer
 */
class Omnius_Checkout_Block_Cart_Steps_SaveCustomer extends Omnius_Checkout_Block_Cart
{
    /**
     * Logic for the dummy email checkbox to be displayed.
     * Extend to add functionality to this method.
     * @return bool
     */
    public function canShowDummyCheckbox()
    {
        return true;
    }

    /**
     * Check whether the bank account fields should be displayed.
     * Extend method to implement custom logic.
     * @return bool
     */
    public function shouldShowAccountDetails()
    {
        return true;
    }

    public function shouldShowPrivacyOptions()
    {
        return true;
    }

    public function canChangeEmail()
    {
        return $this->existingCustomerIsLogged() && !Mage::getSingleton('customer/session')->getOrderEdit();
    }

    public function shouldHideFields()
    {
        return !$this->existingCustomerValidated() && $this->existingCustomerIsLogged();
    }
}