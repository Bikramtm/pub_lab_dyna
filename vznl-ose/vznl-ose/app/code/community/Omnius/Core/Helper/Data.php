<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Core_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Returns array with key=>value format with available keys: status, body
     *
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     * @return array
     */
    public function jsonResponse($data, $statusCode = 200)
    {
        if ($data instanceof Varien_Data_Collection) {
            $items = array();
            foreach ($data->getItems() as $item) {
                /** @var Varien_Object $item */
                array_push($items, $item->toArray());
            }
        } elseif ($data instanceof Varien_Object) {
            $items = $data->toArray();
        } elseif (is_array($data)) {
            $items = $this->getDataFromArray($data);
        } else {
            $items = array(
                'error' => true,
                'message' => 'An error has been encountered',
            );
            $status = 400;
        }

        return [
            'status' => isset($status) ? $status : $statusCode,
            'body' => Mage::helper('core')->jsonEncode($items)
        ];
    }

    /**
     * @param $data
     * @return array
     */
    public function toArray($data)
    {
        $items = [];
        if (is_null($data) || is_scalar($data)) {
            $items = $data;
        } elseif (is_array($data) || ($data instanceof Varien_Data_Collection)) {
            foreach ($data as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif ($data instanceof Varien_Object) {
            foreach ($data->getData() as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif ($data instanceof SimpleXMLElement) {
            foreach (json_decode(json_encode($data), true) as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif (is_object($data)) {
            foreach (get_object_vars($data) as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        }

        return $items;
    }

    /**
     * Converts encoding of string to other encoding.
     * Default encoding is UTF-8
     * @param string $string
     * @param string $encoding
     * @return string
     */
    public function convertEncoding($string, $encoding = 'UTF-8')
    {
        if (function_exists('iconv')) {
            return iconv(mb_detect_encoding($string, mb_detect_order(), true), $encoding, $string);
        } else {
            return mb_convert_encoding($string, $encoding, mb_detect_encoding($string, mb_detect_order(), true));
        }
    }

    /**
     * Trims the pipe and id of the product from the product SKU
     * @param string $sku
     * @return mixed
     */
    public function trimSku($sku)
    {
        $temp = explode('|', $sku);

        return $temp[0];
    }

    /**
     * @param $data
     * @return array
     */
    protected function getDataFromCollection($data)
    {
        $items = [];
        /** @var Varien_Data_Collection $coll */
        foreach ($data as $key => $coll) {
            $items[$key] = array();
            /** @var Varien_Object $item */
            foreach ($coll->getItems() as $item) {
                $items[$key][] = $item->toArray();
            }
        }

        return $items;
    }

    /**
     * @param $data
     * @return array
     */
    protected function getDataFromArray($data)
    {
        $dataValues = array_values($data);
        $el = array_shift($dataValues);
        if ($el instanceof Varien_Data_Collection) {
            $items = $this->getDataFromCollection($data);
        } elseif ($el instanceof Varien_Object) {
            $items = array();
            /** @var Varien_Object $coll */
            foreach ($data as $key => $coll) {
                $items[$key][] = $coll->toArray();
            }
        } else {
            $items = $data;
        }

        return $items;
    }

    public function compareArrays($array1, $array2)
    {
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key]) && !is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = $this->compareArrays($value, $array2[$key]);
                    if ($new_diff !== 0) {
                        $difference[$key] = $new_diff;
                    }
                }
            } elseif (!array_key_exists($key, $array2) || $array2[$key] != $value) {
                $difference[$key] = $value;
            }
        }

        return !isset($difference) ? 0 : $difference;
    }

    /**
     * @param Exception $exception
     */
    public function returnError(\Exception $exception)
    {
        $return = array(
            'error' => true,
            'message' => $exception->getMessage(),
        );

        Mage::app()->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($return)
        );
    }
}
