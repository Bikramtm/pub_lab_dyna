<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Campaign
 */
class Omnius_Campaign_Model_Campaign extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('campaign/campaign');
    }

    /**
     * @param int $id
     * @param null $field
     * @return $this|Mage_Core_Model_Abstract
     */
    public function load($id, $field=null)
    {
        $key = 'campaign_' . md5(serialize(array($id, $field)));
        if ($product = Mage::objects()->load($key)) {
            $this->setData($product->getData());
            return $this;
        }

        parent::load($id, $field);
        Mage::objects()->save($this, $key);
        return $this;
    }

    /**
     * @return Varien_Data_Collection
     */
    public function getCustomers()
    {
        return $this->getData('customers');
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     */
    public function addCustomer(Mage_Customer_Model_Customer $customer)
    {
        /** @var Varien_Data_Collection $customers */
        $customers = $this->getCustomers();
        if ( ! in_array($customer->getEntityId(), $this->getCustomers()->getAllIds())) {
            $customers->addItem($customer);
            $this->setCustomers($customers);
        }
    }

    /**
     * @param $customer
     * @return bool
     */
    public function hasCustomer($customer)
    {
        return $this->getCustomers()->getItemById(is_numeric($customer) ? $customer : $customer->getId()) ? true : false;
    }

    /**
     * Get customer info by id
     *
     * @param $customerId
     * @return Varien_Object
     */
    public function getCustomer($customerId)
    {
        return $this->getCustomers()->getItemById($customerId);
    }

    /**
     * Import new campaigns and link them to the customer
     *
     * @param int $customerId
     * @param string $campaignCode
     */
    public function importCampaign($customerId, $campaignCode)
    {
        $campaignCode = trim($campaignCode);
        if (empty($campaignCode)) {
            Mage::log('Empty campagin code. [Customer id: ' . $customerId . ']', null, 'campaigns_import_' . date("Y-m-d") . '.log');
        } else {
            $campaign = Mage::getModel('campaign/campaign');
            $campaign->setName($campaignCode);
            $campaign->setCode($campaignCode);
            $error = false;

            try {
                $campaign->save();
            } catch (Exception $ex) {
                $error = true;
                Mage::log($ex->getMessage(), null, 'campaigns_import_' . date("Y-m-d") . '.log');
            }

            if (!$error && $campaign->getId()) {
                $this->linkCampaign($customerId, $campaign->getId());
            }
        }
    }

    /**
     * Link campaigns to customers
     *
     * @param int $customerId
     * @param int|null $campaignId
     * @param string|null $campaignCode
     */
    public function linkCampaign($customerId, $campaignId = null, $campaignCode = null)
    {
        if(!$campaignId) {
            $campaign = Mage::getModel('campaign/campaign')->getCollection()->addFieldToFilter('code', $campaignCode)->getFirstItem();
            $campaignId = $campaign->getId();
        }

        try {
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            $query = "INSERT INTO `customer_campaign_link` (customer_id, campaign_id) VALUES (:customer_id, :campaign_id)";
            $binds = array(
                'customer_id'      => $customerId,
                'campaign_id'     => $campaignId,
            );

            $write->query($query, $binds);
        }
        catch (Exception $ex) {
            Mage::log($ex->getMessage(), null, 'campaigns_import_'.date("Y-m-d").'.log');
        }
    }

    public function getCampaignForDropdown(){
        $productsCollection = $this->getCollection();
        $selectable_collection = array( -1 => '');
        foreach($productsCollection as $product)
        {
            $selectable_collection[] = array(
                'label' => $product->getName(),
                'value' => $product->getEntityId()
            );
        }
        return $selectable_collection;
    }
}
