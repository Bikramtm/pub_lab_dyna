<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Customer_Model_Resource_Setup */
$this->startSetup();
$this->getConnection()->addColumn($this->getTable('superorder'), 'new_customer', [
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'nullable' => true,
    'default' => 0,
    'comment' => "New customer flag",
]);
$this->endSetup();
