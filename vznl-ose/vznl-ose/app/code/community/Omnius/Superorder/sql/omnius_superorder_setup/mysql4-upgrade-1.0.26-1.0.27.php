<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();
$customerInstaller->getConnection()->addIndex($customerInstaller->getTable('superorder'), 'IDX_SUPERORDER_NUMBER', 'order_number');
$customerInstaller->endSetup();