<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
if (!$installer->getConnection()->isTableExists($installer->getTable('status_history'))) {
    /** Status history table */
    $statusHistoryTable = new Varien_Db_Ddl_Table();
    $statusHistoryTable->setName('status_history');

    $statusHistoryTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    );

    $statusHistoryTable->addColumn(
        'superorder_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $statusHistoryTable->addColumn(
        'package_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $statusHistoryTable->addColumn(
        'order_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $statusHistoryTable->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        30
    );

    $statusHistoryTable->addColumn(
        'type',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        30
    );

    $statusHistoryTable->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME
    );

    $statusHistoryTable->setOption('type', 'InnoDB');
    $statusHistoryTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($statusHistoryTable);
}
$installer->endSetup();