<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
if (!$installer->getConnection()->isTableExists($installer->getTable('superorder_change_history'))) {
    /** Status history table */
    $superorderChangeHistory = new Varien_Db_Ddl_Table();
    $superorderChangeHistory->setName('superorder_change_history');

    $superorderChangeHistory->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    );

    $superorderChangeHistory->addColumn(
        'superorder_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $superorderChangeHistory->addColumn(
        'agent_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $superorderChangeHistory->addColumn(
        'impersonated_agent_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $superorderChangeHistory->addColumn(
        'current_time',
        Varien_Db_Ddl_Table::TYPE_DATETIME
    );

    $superorderChangeHistory->addColumn(
        'website_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        5,
        array('unsigned' => true, 'nullable' => false),
        'Channel'
    );

    $superorderChangeHistory->setOption('type', 'InnoDB');
    $superorderChangeHistory->setOption('charset', 'utf8');
    $this->getConnection()->createTable($superorderChangeHistory);
}
$installer->endSetup();