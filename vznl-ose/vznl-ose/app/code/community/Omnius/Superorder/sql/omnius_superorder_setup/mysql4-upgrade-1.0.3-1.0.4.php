<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('superorder'), 'error_code', Varien_Db_Ddl_Table::TYPE_INTEGER);
$installer->endSetup();