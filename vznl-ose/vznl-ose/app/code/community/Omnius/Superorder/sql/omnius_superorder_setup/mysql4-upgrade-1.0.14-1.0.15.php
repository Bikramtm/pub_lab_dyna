<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$this->getConnection()->dropColumn($this->getTable('superorder'), 'return_email_sent');
$installer->endSetup();
