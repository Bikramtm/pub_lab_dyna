<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

// Synchronize table indexes with production Db
/* @var $installer Mage_Customer_Model_Resource_Setup */
$installer = new Mage_Customer_Model_Resource_Setup('core_setup');
$installer->startSetup();

$setup = Mage::getSingleton('core/resource')->getConnection('core_read');
$idxArray = [];

$idxArray['status_history'] = [
    'IDX_STATUS_HISTORY_SUPERORDERTYPE' => ['superorder_id', 'type'],
    'IDX_STATUS_HISTORY_PACKAGETYPE' => ['package_id', 'type'],
];

$idxArray['superorder'] = [
    'IDX_SUPERORDER_DATE' => ['created_at'],
];


foreach ($idxArray as $table => $idx) {
    $currentIdx = $setup->fetchAll(sprintf('SHOW INDEX FROM `%s`', $table));
    foreach ($idx as $idxName => $columns) {
        $idxFound = false;
        foreach ($currentIdx as $indexes) {
            if (strtoupper($indexes['Key_name']) == strtoupper($idxName)) {
                $idxFound = true;
            }
        }
        if (!$idxFound) {
            $fields = implode(', ', $columns);
            $installer->run(sprintf("ALTER TABLE `%s` ADD INDEX `%s` (%s)", $table, $idxName, $fields));
        }

    }
}

$installer->endSetup();
