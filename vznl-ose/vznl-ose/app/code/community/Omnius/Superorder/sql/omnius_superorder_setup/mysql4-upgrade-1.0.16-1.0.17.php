<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

// Add flag to indicate if the superorder was created in the ILS change flow
$installer = $this;
$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('superorder'), 'is_ils', Varien_Db_Ddl_Table::TYPE_BOOLEAN);
$installer->endSetup();
