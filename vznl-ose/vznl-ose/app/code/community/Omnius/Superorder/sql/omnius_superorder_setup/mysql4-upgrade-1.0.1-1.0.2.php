<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('superorder'), 'parent_id', Varien_Db_Ddl_Table::TYPE_INTEGER);
$this->getConnection()->addIndex($this->getTable('superorder'), 'IDX_SUPERORDER_PARENT_ID', 'parent_id');
$installer->endSetup();