<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Add locked at column in Superorder table
 */
/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('superorder'), 'locked_at', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'after' => 'locked_by',
    'nullable' => true,
    'comment' => 'Order lock date-time'
));
$installer->endSetup();
