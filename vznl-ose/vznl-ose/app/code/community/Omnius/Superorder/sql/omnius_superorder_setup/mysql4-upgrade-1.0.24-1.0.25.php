<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
* Add Front Wave imported order column in Superorder table
*/
/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('superorder'), 'frontwave_order_number', 'varchar(255)');
$installer->endSetup();