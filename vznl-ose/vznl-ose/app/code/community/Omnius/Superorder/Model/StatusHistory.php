<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Model_StatusHistory
 */
class Omnius_Superorder_Model_StatusHistory extends Mage_Core_Model_Abstract
{
    const PACKAGE_STATUS = 'status';
    const PACKAGE_CC_STATUS = 'creditcheck_status';
    const PACKAGE_VF_STATUS_CODE = 'vf_status_code';
    const PACKAGE_PORTING_STATUS = 'porting_status';
    const PACKAGE_ESB_STATUS_CODE = 'esb_status_code';
    const PACKAGE_STOCK_STATUS = 'stock_status';
    const PACKAGE_CREDIT_CODE = 'vf_creditcode';
    const SUPERORDER_STATUS = 'order_status';
    const ORDER_STATUS = 'order_status';
    const ORDER_ESB_ORDER_STATUS = 'esb_order_status';

    /**
     * Constructor initialization
     */
    protected function _construct()
    {
        $this->_init('superorder/statusHistory');
    }
} 