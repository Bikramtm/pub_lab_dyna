<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Model_Mysql4_StatusHistory
 */
class Omnius_Superorder_Model_Mysql4_StatusHistory extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('superorder/statusHistory', 'entity_id');
    }
}