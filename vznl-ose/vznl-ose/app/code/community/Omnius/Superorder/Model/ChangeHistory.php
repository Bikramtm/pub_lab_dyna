<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Model_ChangeHistory
 */
class Omnius_Superorder_Model_ChangeHistory extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('superorder/changeHistory');
    }
} 