<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Superorder_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Returns the package content for a given package and given superorder. Only works if an ajax call
     */
    public function checkPackageAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        if ($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()) {
            try {
                $params = new Varien_Object($this->getRequest()->getParams());
                $packageId = $params->getData('packageId', null);
                /** @var Omnius_Superorder_Model_Superorder $superOrder */
                $superOrder = Mage::getModel('superorder/superorder')
                    ->getSuperOrderByOrderNumber($params->getData('orderNumber'));
                if (!($superOrder && $superOrder->getId())) {
                    throw  new Exception($this->__('Order not found'));
                }
                /** @var Omnius_Package_Model_Package $packageModel */
                $packageModels = Mage::getModel('package/package')
                    ->getPackages($superOrder->getId(), null, $packageId);
                $html = [];
                foreach ($packageModels as $packageModel) {
                    $html[$packageModel->getPackageId()] = Mage::helper('superorder/package')->getPackageAvailability($superOrder, $packageModel);
                }
                $result = ['error' => false, 'html' => $html];
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($result));
            } catch (Exception $e) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode(['error' => true, 'message' => $e->getMessage()]));
            }
        }
    }
}
