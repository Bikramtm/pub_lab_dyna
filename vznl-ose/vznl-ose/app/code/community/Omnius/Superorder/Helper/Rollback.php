<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Helper_Rollback
 */
class Omnius_Superorder_Helper_Rollback
{
    /** @var Varien_Db_Adapter_Pdo_Mysql */
    protected $_connection;

    /**
     * Rolls back the changes of a delivered edited superorder in case the customer sent the hardware back in 14 days
     * @param Omnius_Superorder_Model_Superorder $superOrder
     * @param null $snapshot
     */
    public function rollbackCancelled(Omnius_Superorder_Model_Superorder $superOrder, $snapshot = null)
    {
        $children = $superOrder->getChildren();

        $this->rollbackEdited($superOrder, $snapshot);
    }

    /**
     * Checks if a collection have children created after a given date
     * @param Varien_Data_Collection $children
     * @param null $date
     * @return Varien_Data_Collection
     * @throws Exception
     */
    protected function _getFreshChildren(Varien_Data_Collection $children, $date = null)
    {
        $freshChildren = new Varien_Data_Collection();
        $date = $date ? strtotime($date) : strtotime(now());
        foreach ($children as $child) {
            if (strtotime($child->getCreatedAt()) >= $date) {
                $freshChildren->addItem($child);
            }
        }

        return $freshChildren;
    }

    /**
     * Rolls back the changes (adn history) of a delivered edited superorder in case the customer sent the hardware back in 14 days
     * @param Omnius_Superorder_Model_Superorder $superOrder
     * @param null $snapshot
     */
    public function rollbackEdited(Omnius_Superorder_Model_Superorder $superOrder, $snapshot = null)
    {
        $freshChildren = $this->_getFreshChildren($superOrder->getChildren());
        /** @var Omnius_Superorder_Model_Superorder $childOrder */
        foreach ($freshChildren as $childOrder) {
            if ((in_array(strtolower($childOrder->getOrderStatus()), [
                    strtolower(Omnius_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI),
                    strtolower(Omnius_Superorder_Model_Superorder::SO_INITIAL),
                    strtolower(Omnius_Superorder_Model_Superorder::SO_PRE_INITIAL),
                    strtolower(Omnius_Superorder_Model_Superorder::SO_WAITING_FOR_VALIDATION)
                ]))
                && (false == $childOrder->getData('axi_processed'))
                && (0 === $childOrder->getChildren()->count())
            ) {// need to be deleted if they have no children and have not been processed
                $this->_cleanUp($childOrder);
            } else { //the superorder has fresh children or it was processed
                Mage::log(sprintf('Exceptional rollback case not currently supported for order %s (parent: %s)', $childOrder->getId(), $superOrder->getId()), null, 'rollback_exceptional.log', true);
                //TODO handle case when children orders are processed and can not be deleted
            }
        }
    }

    /**
     * Removes customer ctn from an order which will be rolled back
     * @param Omnius_Superorder_Model_Superorder $superOrder
     */
    protected function _cleanUp(Omnius_Superorder_Model_Superorder $superOrder)
    {
        foreach ($superOrder->getPackages() as $package) {
            /** @var Omnius_Package_Model_Package $package */
            $this->_getConn()->exec(sprintf(
                'DELETE FROM `customer_ctn` WHERE ctn="%s" AND customer_id="%s";',
                $package->getTelNumber(),
                $superOrder->getCustomerId()
            ));
            $package->delete();
        }

        foreach ($superOrder->getOrders() as $order) {
            /** @var Omnius_Checkout_Model_Sales_Order $order */
            $order->delete();
        }

        $superOrder->delete();
    }

    /**
     * Returns the Db adapter resource
     * @return Varien_Db_Adapter_Interface|Varien_Db_Adapter_Pdo_Mysql
     */
    protected function _getConn()
    {
        if (!$this->_connection) {
            return $this->_connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        }

        return $this->_connection;
    }
} 
