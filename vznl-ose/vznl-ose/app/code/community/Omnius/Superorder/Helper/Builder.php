<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Superorder_Helper_Builder extends Mage_Core_Helper_Data
{
    /** @var Omnius_Customer_Model_Session */
    protected $_customerSession = null;

    /** @var Mage_Checkout_Model_Session */
    protected $_checkoutSession = null;

    /** @var Omnius_Superorder_Model_Superorder */
    protected $_superOrder = null;

    /** @var Omnius_Checkout_Model_Sales_Quote */
    protected $_superQuote = null;

    /** @var Omnius_Checkout_Model_Sales_Quote[] */
    protected $_modifiedQuotes = null;

    /** @var array */
    protected $_cancelledPackageIds = null;

    /** @var Omnius_Package_Helper_Data */
    protected $_packageHelper = null;

    /** @var array */
    protected $_packageIds = [];

    /** @var Omnius_Core_Helper_Data */
    protected $_coreHelper = null;

    /** @var Omnius_Checkout_Model_Sales_Order[] */
    protected $_uneditedOrders = null;

    /** @var array */
    protected $_packagesForOrders = null;

    /** @var Omnius_Checkout_Model_Sales_Quote_Address[] */
    protected $_newAddresses = null;

    /** @var array */
    protected $_payments = [];

    /** @var Omnius_Package_Model_Package[] */
    protected $_builtPackages = [];

    /** @var array */
    protected $_basePackageIds = [];

    /** @var Omnius_Checkout_Model_Sales_Order[] */
    protected $_orders = [];

    /** @var Omnius_Superorder_Model_Superorder|null */
    protected $_newSuperorder = null;

    /** @var bool */
    protected $_onlyPaymentChanged = false;

    /** @var array */
    protected $_refundAmounts = [
        'payments' => [],
        'refundAmount' => []
    ];

    /** @var array */
    protected $_ordersWithAddressChanged = [];

    /** @var array */
    protected $_ordersWithEditedPackages = [];
    /** @var array */
    protected $newIncrementIds = [];

    /** @var array */
    protected $_editedPackageIds = [];

    /** @var array */
    protected $_deliveredOrders = [];

    /** @var  Dyna_Service_Helper_Data */
    protected $_serviceHelper;

    /** @var  Omnius_Checkout_Helper_Data */
    protected $checkoutHelper;

    /**
     * Override constructor to initialize helpers
     * Omnius_Superorder_Helper_Builder constructor.
     */
    public function __construct()
    {
        $this->_customerSession = Mage::getSingleton('customer/session');
        $this->_checkoutSession = Mage::getSingleton('checkout/session');
        $this->_packageHelper = Mage::helper('omnius_package');
        $this->_coreHelper = Mage::helper('omnius_core');
        $this->checkoutHelper = Mage::helper('omnius_checkout');
    }

    /**
     * Initialize method to retrieve cancelled packages,unedited orders and superorder objects that will be used
     * @throws Exception
     */
    protected function init()
    {
        Mage::register('is_override_promos', true);
        if (!$this->_customerSession->getOrderEditMode()) {
            throw new Exception($this->__('Order is locked!'));
        }
        //Get super quote
        $superQuoteId = $this->_customerSession->getOrderEdit();
        $this->_superQuote = Mage::getModel('sales/quote')->load($superQuoteId);
        $superOrderId = $this->_superQuote->getSuperOrderEditId();

        //Get superorder
        $this->_superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);

        //Get package entity_id for each package so we can load them as a specific type of package.
        $this->processBasePackageIds();

        //Get modified quotes
        $theModifiedQuotes = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('super_quote_id', $superQuoteId)
            ->addFieldToFilter('super_order_edit_id', array('eq' => $superOrderId));

        foreach ($theModifiedQuotes as $quote) {
            $this->_modifiedQuotes[$quote->getActivePackageId()] = $quote;
        }

        //Get cancelled packages in session.
        $this->_cancelledPackageIds = Mage::helper('omnius_package')->getCancelledPackagesNow();

        //Get starting orders
        $this->_uneditedOrders = Mage::getModel('sales/order')->getNonEditedOrderItems($this->_superOrder->getId());

        //Build package ids
        $superQuoteItems = $this->_superQuote->getAllItems();
        foreach ($superQuoteItems as $item) {
            $this->_packageIds[$item->getPackageId()][] = $item;
        }
    }

    /**
     * Start the building process which persists data for the package, delivery orders and calculates possible refunds
     * @return array
     * @throws Exception
     */
    public function build()
    {
        $this->init();
        $this->buildPackages();
        $this->buildOrders();
        $this->calculateRefund();
        $result = $this->processResult();
        $this->processPackagesAfter();
        Mage::getSingleton('customer/session')->unsOrderEdit();

        return $result;
    }

    /**
     * Builds the packages by persisting the address, payment and items
     * @return $this
     */
    protected function buildPackages()
    {
        $this->buildChangedAddresses();

        /** @var Omnius_Package_Helper_Builder $packageBuilder */
        $packageBuilder = Mage::helper('omnius_package/builder');
        $packageBuilder->setSuperQuote($this->_superQuote);
        /**
         * @var int $packageId
         * @var Omnius_Checkout_Model_Sales_Quote_Item[] $itemsArray
         */
        foreach ($this->_packageIds as $packageId => $itemsArray) {
            $this->_builtPackages[$packageId] = $packageBuilder->buildPackage(
                $packageId,
                $itemsArray,
                $this->getPayment($packageId),
                $this->getModifiedQuote($packageId),
                $this->getNewAddress($packageId),
                $this->getPackageIsCancelled($packageId)
            );
        }

        return $this;
    }

    /**
     * Returns the new address of a package, if any exists
     * @param int $packageId
     * @return Omnius_Checkout_Model_Sales_Quote_Address|null
     */
    public function getNewAddress($packageId)
    {
        return isset($this->_newAddresses[$packageId]) ? $this->_newAddresses[$packageId] : null;
    }

    /**
     * Returns the payment object for a given package
     * @param int $packageId
     * @return array|null
     */
    public function getPayment($packageId)
    {
        return isset($this->_payments[$packageId]) ? $this->_payments[$packageId] : null;
    }

    /**
     * Retrieves the changed quote for the packageId.
     * @param $packageId
     * @return Omnius_Checkout_Model_Sales_Quote|null
     */
    public function getModifiedQuote($packageId)
    {
        return isset($this->_modifiedQuotes[$packageId]) ? $this->_modifiedQuotes[$packageId] : null;
    }

    /**
     * Checks if the package is cancelled or not
     * @param int $packageId
     * @return bool
     */
    public function getPackageIsCancelled($packageId)
    {
        return in_array($packageId, $this->_cancelledPackageIds);
    }

    /**
     * Retrieves the last non-edited delivery order and persist its data
     */
    protected function buildOrders()
    {
        /** @var Omnius_Checkout_Model_Sales_Order $order */
        foreach ($this->_uneditedOrders as $order) {
            $modelPrefix = $this->isDelivered($order) ? '' : 'un';
            /** @var Omnius_Checkout_Model_Sales_Order $tempOrder */
            $tempOrder = Mage::getModel('omnius_checkout/sales_' . $modelPrefix . 'deliveredOrder')->load($order->getId());
            $processedPackages = $this->getProcessedPackageForOrder($order);
            /** @var Omnius_Package_Model_Package $package */
            foreach ($processedPackages as $package) {
                $package->setProcessedOrder($tempOrder)
                    ->build();
                $tempOrder->addProcessedPackage($package);
                if (in_array($package->getBinaryState(), [Omnius_Package_Helper_Builder::EDITED_WITH_ADDRESS_PACKAGE, Omnius_Package_Helper_Builder::EDITED_PACKAGE])) {

                    $this->_ordersWithEditedPackages[$order->getId()] = $order;
                    $this->_editedPackageIds[] = $package->getPackageId();
                }
            }

            $tempOrder->setBaseQuote($this->_superQuote)
                ->setNewSuperorder($this->_newSuperorder)
                ->setSuperorder($this->_superOrder);
            $incrementIds = $tempOrder->build();
            $this->newIncrementIds = array_merge($this->newIncrementIds, $incrementIds);

            if ($tempOrder->getNewSuperorder() && $tempOrder->getNewSuperorder()->getId()) {
                $this->_newSuperorder = $tempOrder->getNewSuperorder();
            }

            $this->_orders[] = $tempOrder;
        }
        Mage::getSingleton('customer/session')->setLastSuperOrder($this->_newSuperorder ? $this->_newSuperorder->getId() : $this->_superOrder->getId());
    }

    /**
     * Returns an array of packages for a given delivery order
     * @param Omnius_Checkout_Model_Sales_Order $order
     * @return Omnius_Package_Model_Package[]
     */
    protected function getProcessedPackageForOrder($order)
    {
        $packageArray = [];
        foreach ($this->_packagesForOrders[$order->getId()] as $packageId) {
            $packageArray[] = $this->_builtPackages[$packageId];
        }

        return $packageArray;
    }

    /**
     * Gets the entity_id for each package. Used for the loading of each package
     * @return $this
     */
    protected function processBasePackageIds()
    {
        $adapter = Mage::getModel('core/resource')->getConnection('core_write');
        $results = $adapter->fetchAll('SELECT * FROM catalog_package WHERE order_id = :order_id', array(':order_id' => $this->_superOrder->getId()));
        foreach ($results as $result) {
            $this->_basePackageIds[$result['package_id']] = $result['entity_id'];
        }

        return $this;
    }

    /**
     * Processes each address for package and compares it with the addresses set in session to detect address changes.
     * @return $this
     */
    protected function buildChangedAddresses()
    {
        $this->_newAddresses = [];
        $shippingData = Mage::helper('core')->jsonDecode($this->_superQuote->getData('shipping_data'));
        foreach ($this->_uneditedOrders as $order) {
            $orderPackages = $this->getPackagesForOrders($order);
            foreach ($orderPackages[$order->getId()] as $packageId) {
                $currentAddress = $this->buildCurrentAddress($order);
                $initialAddress = $this->buildInitialAddress($shippingData, $packageId);
                if (!$initialAddress) {
                    continue;
                }

                /**
                 * If array_diff returns anything, the address has been changed
                 * or payment is different
                 */
                if (
                    (
                        $this->_coreHelper->compareArrays($initialAddress, $currentAddress)
                        || $this->_coreHelper->compareArrays($currentAddress, $initialAddress)
                        || $this->_payments[$packageId]['method'] != $order->getPayment()->getMethod()
                    )
                    && !array_key_exists($packageId, $this->_newAddresses)
                ) {
                    $this->_newAddresses[$packageId] = $this->generateNewAddress($initialAddress, $currentAddress, $order);
                }
            }
        }

        return $this;
    }

    /**
     * Assigns each package to the delivery order from which it comes.
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function getPackagesForOrders(Mage_Sales_Model_Order $order)
    {
        if ($this->_packagesForOrders == null) {
            $this->_packagesForOrders = array();

            /** @var Omnius_Checkout_Model_Sales_Order_Item $orderItem */
            foreach ($order->getAllItems() as $orderItem) {
                $this->_packagesForOrders[$order->getId()][] = $orderItem->getPackageId();
            }
            $this->_packagesForOrders[$order->getId()] = array_unique($this->_packagesForOrders[$order->getId()]);
        }

        return $this->_packagesForOrders;
    }

    /**
     * Checks if a given delivery order is delivered or not
     * @param Omnius_Checkout_Model_Sales_Order $order
     * @return bool
     */
    protected function isDelivered($order)
    {
        if (!isset($this->_deliveredOrders[$order->getId()])) {
            $this->_deliveredOrders[$order->getId()] = $this->_builtPackages[reset($this->_packagesForOrders[$order->getId()])]->isDelivered();
        }
        return $this->_deliveredOrders[$order->getId()];
    }

    /**
     * Processes the result of the save and if needed, also triggers an approveOrder call
     * @return array
     */
    protected function processResult()
    {
        $result = [];
        $result['orders'] = $this->_superOrder->getOrderNumber();
        $esbSuperOrder = $this->_superOrder;
        $esbOldSuperOrder = null;
        if ($this->_newSuperorder) {
            $result['new_order'] = $this->_newSuperorder->getOrderNumber();
            $result['new_order_id'] = $this->_newSuperorder->getId();
            $esbSuperOrder = $this->_newSuperorder;
            $esbOldSuperOrder = $this->_superOrder;
        }
        // Set updated at timestamp
        $this->_superOrder->setUpdatedAt(now())->save();
        // From this point on cache all models for faster processing, since they won't be changed
        if (!Mage::registry('freeze_models')) {
            Mage::unregister('freeze_models');
            Mage::register('freeze_models', true);
        }

        Mage::unregister('is_override_promos');

        /** @var Dyna_Service_Model_Client_EbsOrchestrationClient $client */
        try {
            $toBePaidAmount = Mage::getSingleton('customer/session')->getToPayAmount();
            $client = $this->getServiceHelper()->getClient('ebs_orchestration');

            $returnOnlyXml = false;

            $esbNewOrders = [];

            $changedDeliveredPackages = false;
            foreach ($this->_orders as $order) {
                if ($this->isDelivered($order)) {
                    $changedDeliveredPackages = true;
                }

                foreach ($order->getChildOrders() as $child) {
                    $esbNewOrders[$order->getId()][$child->getId()] = $child;
                }
            }

            $esbSuperOrder->setOnlyPaymentChanged($this->_onlyPaymentChanged);

            $editedOrders = (!empty($this->_ordersWithEditedPackages) || !empty($esbNewOrders)) ? array($this->_ordersWithEditedPackages, $esbNewOrders) : null;
            $poolId = $client->processCustomerOrder(
                $esbSuperOrder,
                $esbOldSuperOrder,
                $editedOrders,
                $this->_editedPackageIds,
                $this->_ordersWithAddressChanged,
                $this->_cancelledPackageIds,
                $this->_refundAmounts,
                Mage::app()->getRequest()->getPost('refund_method'),
                null,
                $toBePaidAmount,
                $returnOnlyXml
            ); //call esb

            if ($returnOnlyXml) {
                $esbSuperOrder->setXmlToBeSent($poolId)->save();
                $poolId = null;
            }

            $esbChangedPackageIds = $this->_modifiedQuotes ? array_keys($this->_modifiedQuotes) : [];
            $allModifiedPackages = array_merge($this->_cancelledPackageIds, $esbChangedPackageIds);

            $this->_checkoutSession->setData('super_quote_id', $this->_superQuote->getId());
            $this->_checkoutSession->setData('superorder_id', $this->_superOrder->getId());
            $this->_checkoutSession->setData('process_order_job_id', $poolId);

            $data = array(
                'esb_changed_package_ids' => $this->_editedPackageIds,
                'order_ids_array' => $this->newIncrementIds,
                'all_modified_packages' => $allModifiedPackages,
                'esb_old_super_order' => $esbOldSuperOrder ? $esbOldSuperOrder->getId() : null,
                'esb_super_order' => $esbSuperOrder->getId(),
            );
            $this->_checkoutSession->setData('save_superorder_without_canceled', $data);
            $orderHasOtherPackages = Mage::registry('order_has_other_packages');
            $approveOrderTriggered = Mage::registry('approve_order_already_triggered');
            if (!$changedDeliveredPackages && ($esbChangedPackageIds || $this->_ordersWithAddressChanged || $orderHasOtherPackages) && !$approveOrderTriggered) {
                $serviceHelper = $this->getServiceHelper();
                /** @var $approvalClient Dyna_Service_Model_Client_ApprovalClient */
                $approvalClient = $serviceHelper->getClient('approval');
                $approvalClient->approveOrder($esbSuperOrder);
            }

            $result['error'] = false;
            $result['pool_id'] = array($poolId);

            $orderedItems = $esbSuperOrder->getOrderedItems(true, false, $this->newIncrementIds, $allModifiedPackages);
            if ($this->checkoutHelper->canProcessDeliverySteps($orderedItems)) {
                $result['contract_page'] = Mage::app()->getLayout()
                    ->createBlock('core/template')
                    ->setShowStep(true)
                    ->setOrderIds($this->newIncrementIds)
                    ->setTemplate('checkout/cart/steps/save_contract.phtml')
                    ->toHtml();
            }

            if ($this->checkoutHelper->canProcessDeviceSelectionStep($orderedItems)) {
                Mage::unregister('order');
                Mage::register('order', $esbSuperOrder);
                $result['device_selection'] = Mage::app()->getLayout()->createBlock('core/template')
                    ->setShowStep(true)
                    ->setTemplate('checkout/cart/steps/save_device_selection.phtml')
                    ->toHtml();
            }
        } catch (Exception $e) {
            // Set fail response for other errors
            $result['error'] = true;
            $result['message'] = $this->__($e->getMessage());
            if ($this->getServiceHelper()->isDev()) {
                $result['trace'] = $e->getTrace();
            }
        }

        return $result;
    }

    /**
     * Sets the statuses of the old packages and cleans CTNs.
     */
    protected function processPackagesAfter()
    {
        if ($this->_cancelledPackageIds) {
            foreach ($this->_cancelledPackageIds as $packageId) {
                $packageIdField = 'package_id';
                $soId = $this->_superOrder->getId();

                if ($this->_newSuperorder) {
                    $packageIdField = 'old_package_id';
                    $soId = $this->_newSuperorder->getId();
                }

                /** @var Omnius_Package_Model_Package $packageModel */
                $packageModel = Mage::getModel('package/package')->getCollection()
                    ->addFieldToFilter('order_id', $soId)
                    ->addFieldToFilter($packageIdField, $packageId)
                    ->getFirstItem();

                if ($packageModel->getId()) {
                    // Delete CTN from customer_ctn table if it was not a retention package
                    if (!$packageModel->isRetention()) {
                        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                        $write->exec("DELETE FROM customer_ctn WHERE ctn = '" . $packageModel->getTelNumber() . "' AND customer_id = '" . $this->_superOrder->getCustomerId() . "'");
                    }
                    // Set package status to cancelled only if there is no new superorder created
                    if ($this->_newSuperorder === null) {
                        $packageModel
                            ->setStatus(Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED)
                            ->save();
                    }
                }
            }
        }
    }

    /**
     * Returns the helper model
     * @return Dyna_Service_Helper_Data
     */
    protected function getServiceHelper()
    {
        if (!$this->_serviceHelper) {
            /** @var Dyna_Service_Helper_Data $serviceHelper */
            $this->_serviceHelper = Mage::helper('omnius_service');
        }

        return $this->_serviceHelper;
    }

    /**
     * Returns the Customer model that is currently active on the session
     * @return Omnius_Customer_Model_Session
     */
    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

    /**
     * Saves a customer model on the session to mark it as active
     * @param Omnius_Customer_Model_Session $customerSession
     */
    public function setCustomerSession($customerSession)
    {
        $this->_customerSession = $customerSession;
    }

    /**
     * Returns the initial address data of the specified package
     * @param $shippingData
     * @param $packageId
     * @return mixed
     */
    protected function buildInitialAddress($shippingData, $packageId)
    {
        if (isset($shippingData['pakket']) && isset($shippingData['pakket'][$packageId]) && isset($shippingData['pakket'][$packageId]['address'])) {
            $initialAddress = $shippingData['pakket'][$packageId]['address'];
            $this->_payments[$packageId] = array(
                'method' => $shippingData['payment'][$packageId],
                'check' => Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_MULTISHIPPING
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                    | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX,
            );
        } elseif (isset($shippingData['deliver'])) {
            //this means all packages have the same address (no order split)
            $initialAddress = $shippingData['deliver']['address'];
            $this->_payments[$packageId] = array(
                'method' => $shippingData['payment'],
                'check' => Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_MULTISHIPPING
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                    | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX,
            );
        } else { //BC as some past orders may exist with no shipping_data
            $this->_payments[$packageId] = array(
                'method' => isset($shippingData['deliver']) ? $shippingData['payment'] : $shippingData['payment'][$packageId],
                'check' => Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_MULTISHIPPING
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                    | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX
            );
            Mage::log(sprintf('Backward compatibility: Super quote (id:%s) does not have "shipping_data". Skipping address change check.',
                $this->_superQuote->getId()), Zend_Log::EMERG);
            return false;
        }
        if (!isset($initialAddress['store_id'])) {
            $initialAddress['store_id'] = null;
        }

        if (!isset($initialAddress['street'][2])) {
            $initialAddress['street'][2] = '';
            return $initialAddress;
        }
        return $initialAddress;
    }

    /**
     * Returns the current shipping address of the provided delivery order
     * @param $order
     * @return array
     */
    protected function buildCurrentAddress($order)
    {
        $tmpCurrentAddress = $order->getShippingAddress();

        $currentAddress = array(
            'street' => array(
                0 => $tmpCurrentAddress->getStreet(1),
                1 => $tmpCurrentAddress->getStreet(2),
                2 => $tmpCurrentAddress->getStreet(3),
            ),
            'postcode' => $tmpCurrentAddress->getPostcode(),
            'city' => $tmpCurrentAddress->getCity(),
            'address' => $tmpCurrentAddress['delivery_type'],
            'store_id' => $tmpCurrentAddress['delivery_store_id']
        );

        unset($tmpCurrentAddress);
        return $currentAddress;
    }

    /**
     * Generates a new address for a provided delivery order
     * @param $initialAddress
     * @param $currentAddress
     * @param $order
     * @return Mage_Sales_Model_Quote_Address
     */
    protected function generateNewAddress($initialAddress, $currentAddress, $order)
    {
        $this->_onlyPaymentChanged = true;
        if ($this->_coreHelper->compareArrays($initialAddress, $currentAddress)
            || $this->_coreHelper->compareArrays($currentAddress, $initialAddress)
        ) {
            $this->_onlyPaymentChanged = false;
        }
        $changedAddress = Mage::getModel('sales/quote_address');
        foreach ($order->getShippingAddress()->getData() as $key => $value) {
            if ($key != 'entity_id' && $key != 'parent_id') {
                $changedAddress[$key] = $value;
            }
        }

        foreach ($initialAddress as $key => $value) {
            if ($key == 'street') {
                $value = implode("\n", $value);
            }
            $changedAddress->setData($key, $value);
        }
        $this->_ordersWithAddressChanged[$order->getId()] = $order->getId();
        $changedAddress->setDeliveryType($initialAddress['address']);
        if ($initialAddress['address'] == 'store') {
            $changedAddress->setDeliveryStoreId($initialAddress['store_id']);
            return $changedAddress;
        } else {
            $changedAddress->unsDeliveryStoreId();
            return $changedAddress;
        }
    }

    /**
     * Calculate refund amounts.
     */
    protected function calculateRefund()
    {
        //Add the refund amounts for the changed packages to the cancelled packages amounts
        foreach ($this->_cancelledPackageIds as $packageId)
        {
            $packageTotalsPayments = Mage::helper('omnius_configurator')->calculateRefundAmounts(
                $this->_superQuote->getId(),
                $packageId
            );

            $packageTotalsRefund = Mage::helper('omnius_configurator')->getActivePackageTotals(
                $packageId,
                true,
                $this->_superQuote->getId()
            );

            $this->_refundAmounts['payments'][$packageId] = $packageTotalsPayments;
            $this->_refundAmounts['refundAmount'][$packageId] = $packageTotalsRefund['totalprice'];
        }
        $refundAmountsChanged = Mage::helper('superorder')->calculatePackageDifference();
        $this->_refundAmounts['payments'] = $this->_refundAmounts['payments'] + $refundAmountsChanged;
        $this->_refundAmounts['refundAmount'] = $this->_refundAmounts['refundAmount'] + $refundAmountsChanged;
    }
}
