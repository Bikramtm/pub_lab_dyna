<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Helper_Data
 */
class Omnius_Superorder_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Returns the price difference for each package after edit.
     * @return array
     */
    public function calculatePackageDifference()
    {
        $editedPackages = Mage::helper('omnius_checkout')->getModifiedPackages();
        $differeceAmounts = array();

        /** @var  Omnius_Package_Model_Package $packagesModel */
        $packagesModel = Mage::getModel('package/package')->getPackages(Mage::getSingleton('checkout/cart')->getQuote(true)->getSuperOrderEditId());

        foreach ($packagesModel as $pack) {
            if (in_array($pack->getPackageId(), $editedPackages) && $pack->isDelivered()) {
                $packageDiffPrice = Mage::helper('omnius_package')->getPackageDifferences($pack->getPackageId());
                if ($packageDiffPrice < 0) {
                    $differeceAmounts[$pack->getPackageId()] = $packageDiffPrice * -1;
                }
            }
        }

        return $differeceAmounts;
    }

    /**
     * Returns an array with the number porting information for a given collection of items
     * @param $portingsCollection
     * @return array
     */
    public function numberPortingExtraInfo($portingsCollection)
    {
        $superOrderporting = array();
        foreach ($portingsCollection as $portingItem) {
            $orders = Mage::getModel('superorder/superorder')->load($portingItem->getOrderId())->getOrders();

            if (count($orders)) {
                foreach ($orders as $order) {
                    $this->parseNumberPortingForOrder($order, $superOrderporting, $portingItem);
                }
            }
        }

        return $superOrderporting;
    }

    /**
     * Checks if the status changed for a given object by comparing the new and old data
     * @param $origData
     * @param $newData
     * @param $types
     * @param $entity
     */
    public function checkStatusesChanged($origData, $newData, $types, $entity)
    {
        $resourceName = $entity->getResourceName();
        $entityResource = ucfirst(substr($resourceName, strpos($resourceName, "/") + 1));
        $setter = 'set' . $entityResource . 'Id';
        foreach ($types as $field) {
            if (isset($newData[$field])) {
                $key = 'check_' . $field . $setter . $newData[$field] . $entity->getId();
                $origDataCond = is_null($origData)
                    || (isset($origData[$field]) && $origData[$field] !== $newData[$field])
                    || !isset($origData[$field]);
                if ($origDataCond && !Mage::registry($key)) {
                    //RFC-150742 : Save agent id in status history for cancelled orders
                    $agentId = null;
                    if (($entityResource == "Superorder") && ($newData[$field] == Omnius_Superorder_Model_Superorder::SO_CANCELLED)) {
                        $customerSession = Mage::getSingleton('customer/session');
                        $agent = $customerSession->getAgent(true);
                        $superAgent = $customerSession->getSuperAgent();
                        $agentId = ($superAgent && $superAgent->getId()) ? $superAgent->getId() : $agent->getId();
                    }
                    Mage::unregister($key);
                    Mage::register($key, true);
                    Mage::getModel('superorder/statusHistory')
                        ->setType($field)
                        ->$setter($entity->getId())
                        ->setStatus($newData[$field])
                        ->setAgentId($agentId)
                        ->setCreatedAt(now())
                        ->save();
                }
            }
        }
    }

    /**
     * Registers agent changes made on the superorder.
     * @param $origData
     * @param $newData
     */
    public function checkIfChanged($origData, $newData)
    {
        $customerSession = Mage::getSingleton('customer/session');
        $websiteId = Mage::app()->getWebsite()->getId();
        $key = 'superorder_change_check_' . $newData['entity_id'];
        unset($origData['locked_at'], $origData['locked_by']);
        unset($newData['locked_at'], $newData['locked_by'], $newData['skip_updated_at']);
        if ($origData !== $newData && ! Mage::registry($key)) {
            $agent = $customerSession->getAgent(true);
            $superAgent = $customerSession->getSuperAgent();
            if ($agent && $agent->getId()) {
                $historyEntity = Mage::getModel('superorder/changeHistory')->setSuperorderId($newData['entity_id'])
                    ->setCurrentTime(now())
                    ->setWebsiteId($websiteId)
                    ->setDealerCode($agent->getDealerCode());
                if ($superAgent && $superAgent->getId()) {
                    $historyEntity->setAgentId($superAgent->getId())
                        ->setImpersonatedAgentId($agent->getId());
                } else {
                    $historyEntity->setAgentId($agent->getId());
                }
                $historyEntity->save();
                Mage::unregister($key);
                Mage::register($key, true);
            }
        }
    }

    /**
     * Return the current website id or the website id of the edited superorder
     */
    public function shouldEmulateStore($currentWebsiteId)
    {
        // current store
        $website = Mage::getModel('core/website');
        /* @var $website Mage_Core_Model_Store */
        if (is_numeric($currentWebsiteId)) {
            $id = $currentWebsiteId;
        } elseif (is_string($currentWebsiteId)) {
            $website->load($currentWebsiteId, 'code');
            $id = $website->getId();
        }
        // If during order edit, and order is created in Online, and it`s edited in Telesales, return the online id
        if ($so = $this->checkCrossStoreEdit($id)) {
            $currentWebsiteId = $so->getCreatedWebsiteId();
        }

        return $currentWebsiteId;
    }

    /**
     * Check if order was created on a different website than it's edited on
     *
     * @return bool|Omnius_Superorder_Model_Superorder
     */
    public function checkCrossStoreEdit($websiteId = null)
    {
        if (!$websiteId) {
            // current store
            $websiteId = Mage::app()->getWebsite()->getId();
        }

        $quoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        if (!$quoteId) {
            return false;
        }

        $key = __METHOD__ . '|OrderEdit|' . $quoteId;
        if (!($so = Mage::registry($key))) {
            /** @var Omnius_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            $so = Mage::getModel('superorder/superorder')->load($quote->getSuperOrderEditId());
            if ($so && $so->getId()) {
                Mage::unregister($key);
                Mage::register($key, $so);
            }
        }

        return $so;
    }

    /**
     * Creates an asociated array with the porting data for the provided superorder
     * @param $order
     * @param $superOrderporting
     * @param $portingItem
     */
    protected function parseNumberPortingForOrder($order, &$superOrderporting, $portingItem)
    {
        $superOrderporting[$order->getSuperorderId()]['customer'] = preg_replace('/\s+/', ' ', (
        $order->getCustomerIsBusiness()
            ? ($order->getContractantFirstname()
            ? ($order->getContractantFirstname() . ($order->getContractantMiddlename() ? ' ' . $order->getContractantMiddlename() : '') . ' ' . $order->getContractantLastname())
            : ($order->getCustomerFirstname()))
            : ($order->getCustomerFirstname() . ' ' . $order->getCustomerMiddlename() . ' ' . $order->getCustomerLastname())

        ));
        foreach ($order->getAllVisibleItems() as $orderItem) {
            if ($orderItem->getPackageId() == $portingItem->getPackageId()) {
                $superOrderporting[$order->getSuperorderId()]['current_number'][$portingItem->getPackageId()] = $portingItem->getCurrentNumber() ? $portingItem->getCurrentNumber() : $portingItem->getTelNumber();
                $superOrderporting[$order->getSuperorderId()]['delivery_order'][$portingItem->getPackageId()] = $order->getId();
                break;
            }
        }
    }
}
