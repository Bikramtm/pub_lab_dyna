<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Configurator_Model_CartObserver
 */
class Omnius_Configurator_Model_CartObserver extends Mage_Core_Model_Abstract
{
    /**
     * Executed when an item is added to the quote with
     * the responsibility to link the newly added item to
     * an existing quote item, if this is possible
     *
     * @param Varien_Event_Observer $observer
     * @return bool
     */
    public function linkProducts(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Quote_Item $addedQuoteItem */
        $addedQuoteItem = $observer->getQuoteItem();
        /** @var Mage_Catalog_Model_Product $addedProduct */
        $addedProduct = $addedQuoteItem->getProduct();
        $quote = $addedQuoteItem->getQuote();

        /**
         * If the quote does not have an active package id
         * we get the biggest package id as active
         */
        if (!$quote->getActivePackageId()) {
            $quote->saveActivePackageId(self::getBiggestPackageId($quote));
        }
        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', Mage::getSingleton('checkout/cart')->getQuote()->getId())
            ->addFieldToFilter('package_id', $quote->getActivePackageId())
            ->getFirstItem();
        $saleType = $quote->getPackageSaleType($quote->getActivePackageId()) ?: $addedProduct->getSaleType();
        if (!$quote->getIsOrderEdit()) {
            $addedQuoteItem
                ->setPackageId($quote->getActivePackageId())
                ->setCtn($package->getCtn())
                ->setPackageType($addedProduct->getPackageType())
                ->setSaleType($saleType)
                ->save();
        }
    }

    /**
     * Calculate the biggest package ID for a quote
     * 
     * @param Mage_Sales_Model_Quote $quote
     * @return int
     */
    public static function getBiggestPackageId(Mage_Sales_Model_Quote $quote)
    {
        $packageIds = static::getPackageIds($quote);

        return $packageIds ? max($packageIds) : 1;
    }

    /**
     * Calculate the new package ID to be used for a quote
     * 
     * @param  Mage_Sales_Model_Quote $quote
     * @return int
     */
    public static function getNewPackageId(Mage_Sales_Model_Quote $quote)
    {
        $packageIds = static::getPackageIds($quote);

        return $packageIds ? (max($packageIds) + 1) : 1;
    }

    /**
     * Get the package IDs
     * 
     * @param  Mage_Sales_Model_Quote $quote
     * @return int[]
     */
    public static function getPackageIds(Mage_Sales_Model_Quote $quote)
    {
        $packageIds = array();

        foreach ($quote->getPackagesInfo() as $package) {
            $packageIds[] = $package['package_id'];
        }

        return $packageIds;
    }
}
