<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Template
 */
class Omnius_Configurator_Model_Template extends Mage_Core_Model_Abstract
{
    protected $_sections = array(
        'subscription',
        'device',
        'addon',
        'accessoire'
    );

    protected $_templatePath = 'configurator/%s/sections/%s.phtml';

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * @param array $sections
     * @return $this
     */
    public function setSections(array $sections)
    {
        $this->_sections = $sections;
        return $this;
    }

    /**
     * @param $path
     * @return $this
     */
    public function setTemplatePath($path)
    {
        $this->_templatePath = $path;
        return $this;
    }

    /**
     * @param $type
     * @return array|mixed
     */
    public function getAllTemplates($type)
    {
        $key = sprintf('_cache_templates_%s_%s', $type, Mage::app()->getWebsite()->getCode());
        if ($templates = unserialize($this->getCache()->load($key))) {
            return $templates;
        } else {
            $templates = array();
            $block = new Mage_Page_Block_Html();
            $allowedSections = array_diff(
                Mage::getSingleton('omnius_configurator/catalog')->getOptionsForPackageType($type),
                Mage::getSingleton('omnius_configurator/catalog')->getOrderRestrictions(Mage::app()->getWebsite()->getCode())
            );
            $blockSections = array_intersect(
                $this->_sections,
                array_map('strtolower', $allowedSections)
            );
            
            $packageType = Mage::getModel('package/packageType')->getCollection()->loadByCode($type);

            foreach ($blockSections as $section) {
                /** @var Omnius_Package_Model_PackageSubtype $subtype */
                $subtype = $packageType->getSubtypes($section)->getFirstItem();

                $addMulti = 'true';
                if ( $subtype && !$subtype->isAddMultiAllowed() ) {
                    $addMulti = 'false';
                }

                $templates[$section] = $block
                    ->setTemplate(sprintf($this->_templatePath, $type, $section))
                    ->setSection($section)
                    ->setType($type)
                    ->setAddMulti($addMulti)
                    ->toHtml();
            }
            $templates['partials'] = $this->getPartials($type);

            $this->getCache()->save(serialize($templates), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $templates;
        }
    }

    /**
     * @param $type
     * @param $section
     * @return string
     */
    public function getTemplate($type, $section)
    {
        return Mage::getBlockSingleton('page/html')
            ->setTemplate(sprintf($this->_templatePath, $type, $section))
            ->toHtml();
    }

    /**
     * @param $type
     * @return array|mixed
     */
    public function getPartials($type)
    {
        $key = sprintf('_cache_partials_%s_%s', $type, Mage::app()->getWebsite()->getCode());
        if ($partials = unserialize($this->getCache()->load($key))) {
            return $partials;
        } else {
            $path = sprintf('configurator/%s/sections/partials', $type);
            if ( ! $type) {
                return array();
            }
            $partials = array();
            $iterator = new RecursiveDirectoryIterator(Mage::getDesign()->getTemplateFilename($path), FilesystemIterator::SKIP_DOTS);
            $iterator = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);
            $block = new Mage_Page_Block_Html();

            foreach ($iterator as $file)
            {
                if (preg_match('/^(.*)\/(.*)\/(.*)\.phtml$/', $file->getPathName(), $matches)) {
                    preg_match('/(.*)\/configurator\//', $file->getPathName(), $pathParts);
                    $templatePath = trim(str_replace($pathParts[1], '', $file->getPathName()), DS);
                    $partials[$matches[2]][$matches[3]] = $block
                        ->setTemplate($templatePath)
                        ->setSection($matches[3])
                        ->toHtml();
                }
            }

            $this->getCache()->save(serialize($partials), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $partials;
        }
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }
}