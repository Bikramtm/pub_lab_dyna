<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Data
 */
class Omnius_Configurator_Helper_Data extends Mage_Core_Helper_Abstract
{
    /** @var Omnius_Configurator_Helper_Attribute */
    protected $_attrHelper;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * @param $data
     * @return array
     */
    public function toArray($data)
    {
        if ($data instanceof Varien_Data_Collection) {
            $items = array();
            foreach ($data->getItems() as $item) {
                array_push($items, $item->toArray());
            }
        } elseif ($data instanceof Varien_Object) {
            $items = $data->toArray();
        } elseif (is_array($data)) {
            $datavalues = array_values($data);
            $el = array_shift($datavalues);
            if ($el instanceof Varien_Data_Collection) {
                $items = $this->getItemsFromCollection($data);
            } elseif ($el instanceof Varien_Object) {
                $items = [];
                foreach ($data as $key => $coll) {
                    $items[$key][] = $coll->toArray();
                }
            } else {
                $items = $data;
            }
        } else {
            return $data;
        }

        return $items;
    }

    /**
     * @return Omnius_Configurator_Helper_Attribute
     */
    protected function getAttrHelper()
    {
        if (!$this->_attrHelper) {
            $this->_attrHelper = Mage::helper('omnius_configurator/attribute');
        }

        return $this->_attrHelper;
    }

    /**
     * Get totals for a cart package (default the active package being configured)
     */
    public function getActivePackageTotals($packageId = null, $withTax = true, $quoteId = false)
    {
        $totalPrice = $totalMaf = 0;
        if (!$quoteId) {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
        } else {
            $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($quoteId);
        }

        if ($packageId || $quote->getActivePackageId()) {
            $packageItems = $quote->getPackageItems($packageId ?: $quote->getActivePackageId());

            return $this->calculatePackageItemsTotals($packageItems, $withTax);
        }

        // return 0 values as fallback
        return array('totalmaf' => $totalMaf, 'totalprice' => $totalPrice);
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item[] $packageItems
     * @param bool|true $withTax
     * @param bool|false $skipDiscounts
     * @return array
     */
    public function calculatePackageItemsTotals($packageItems, $withTax = true, $skipDiscounts = false)
    {
        $totalPrice = $totalMaf = 0;
        foreach ($packageItems as $item) {

            if ($withTax) {
                $mafUnit = $skipDiscounts ? $item->getMafInclTax() : $item->getItemFinalMafInclTax();
                $priceUnit = $item->getItemFinalPriceInclTax();
            } else {
                $mafUnit = $skipDiscounts ? $item->getMaf() : $item->getItemFinalMafExclTax();
                $priceUnit = $item->getItemFinalPriceExclTax();
            }

            $weeeTax = 0;
            if ($withTax) {
                if ($item->getWeeeTaxAppliedRowAmount()) {
                    foreach ($item->getWeeeTaxAppliedUnserialized() as $attribute) {
                        $weeeTax += $attribute['row_amount_incl_tax'];
                    }
                }
            } else {
                if ($item->getWeeeTaxAppliedRowAmount()) {
                    $weeeTax = $item->getWeeeTaxAppliedRowAmount();
                }
            }

            $priceUnit = round($priceUnit + $weeeTax, 2);

            $totalMaf += ($item->getQty() ? $item->getQty() : $item->getQtyOrdered()) * $mafUnit;
            $totalPrice += ($item->getQty() ? $item->getQty() : $item->getQtyOrdered()) * $priceUnit;
        }

        return array('totalmaf' => $totalMaf, 'totalprice' => $totalPrice);
    }

    /**
     * @param int $quoteId
     * @param float $packageId
     * @param bool $withTax
     * @return int
     */
    public function calculateRefundAmounts($quoteId, $packageId, $withTax = true)
    {
        if (!$quoteId) {
            /** @var Omnius_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
        } else {
            /** @var Omnius_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($quoteId);
        }
        $packageItems = [];
        if ($packageId || $quote->getActivePackageId()) {
            $packageItems = $quote->getPackageItems($packageId ?: $quote->getActivePackageId());
        }

        $totalPrice = 0;
        foreach ($packageItems as $item) {
            if ($withTax) {
                $priceUnit = $item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE)
                        ? $item->getMixmatchSubtotal() + $item->getMixmatchTax() - $item->getDiscountAmount()
                        : $item->getItemFinalPriceInclTax();
            } else {
                $priceUnit = $item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE)
                        ? $item->getMixmatchSubtotal() - ($item->getDiscountAmount() - $item->getHiddenTaxAmount())
                        : $item->getItemFinalPriceExclTax();
            }

            $totalPrice += ($item->getQty() ? $item->getQty() : $item->getQtyOrdered()) * $priceUnit;
        }

        return $totalPrice;
    }

    /**
     * @return array
     *
     * Retrieve all attributes sets as an array with id=>name pairing
     */
    public function getAllAtributeSets()
    {
        $attributeSets = array();
        foreach (Mage::getModel('eav/entity_attribute_set')->getCollection() as $attributeSet) {
            $attributeSets[$attributeSet->getId()] = $attributeSet->getAttributeSetName();
        }

        return $attributeSets;
    }

    /**
     * Get correct package title based on the number of CTNs and the package type
     *
     * @param array $ctns
     * @param string $type
     * @return string
     */
    public function getPackageTitle($ctns, $type)
    {
        return $this->__($type) . ' ' . $this->__('package'); //DF-000834
    }

    /**
     * @param $saleType
     * @return string
     */
    public function getPackageDescription($saleType)
    {
        switch (strtolower($saleType)) {
            case Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION:
                return $this->__('Acquisition');
            case Omnius_Checkout_Model_Sales_Quote_Item::RETENTION:
                return $this->__('Retention');
            default:
                // do nothing
                break;
        }

        return ucfirst($saleType);
    }

    /**
     * Add an item tax percent to a price
     * @param Mage_Catalog_Model_Product_Configuration_Item_Interface $product
     * @param $priceWithoutBtw
     * @param bool $rounded
     * @return float
     */
    public function calculatePriceWithBTW(
        Mage_Catalog_Model_Product_Configuration_Item_Interface $product,
        $priceWithoutBtw,
        $rounded = true
    ) {
        $priceWithoutBtw = (float)$priceWithoutBtw;
        $percent = (float)$product->getTaxPercent();

        return $priceWithoutBtw + Mage::getSingleton('tax/calculation')->calcTaxAmount($priceWithoutBtw, $percent, false, $rounded);
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @return array
     */
    public function getBlockRowData($item)
    {
        /** @var Omnius_Catalog_Model_Product $product */
        $product = $item->getProduct();
        $originalItem = $item;
        $result = array(
            'name' => $product->getName(),
            'hashed' => false,
        );

        if ($product->isPromo()) {
            $result = $this->getPromoProductBlock($item, $product, $originalItem, $result);
        } elseif ($item->isPromo()) {
            $result['price'] = Mage::helper('tax')->getPrice($product, $item->getRowTotal(), false);
            $result['priceTax'] = Mage::helper('tax')->getPrice($product, $item->getRowTotalInclTax(), false);
            $result['maf'] = Mage::helper('tax')->getPrice($product, $item->getMaf(), false);
            $result['mafTax'] = Mage::helper('tax')->getPrice($product, $item->getMafInclTax(), true);
        }

        if ($product->isPromo() || $item->isPromo()) {
            return $result;
        }

        // add maf
        if (!(
            $product->is(Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY) ||
            $product->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE)
        )
        ) {
            $result['maf'] = (float) $item->getMaf();
            $result['mafTax'] = (float) $item->getMafInclTax();
        }


        $result['price'] = (float) $item->getPrice();
        $result['priceTax'] = (float) $item->getPriceInclTax();


        if (!(
            $product->is(Omnius_Catalog_Model_Type::SUBTYPE_ADDON) || $product->isPromo()
        )
        ) {
            $result['price'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getSpecialPrice() ?: $product->getPrice()) : $item->getRowTotalInclTax(), //todo something with attr price
                false
            );
            $result['priceTax'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getSpecialPrice() ?: $product->getPrice()) : $item->getRowTotalInclTax(),
                true
            );
        }

        return $result;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item[] $items
     * @return Omnius_Checkout_Model_Sales_Quote_Item|null
     */
    public function extractSubscriptionFromPackageItems($items)
    {
        foreach ($items as $item) {
            if (
                Mage::helper('omnius_catalog')->is(
                    array(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION),
                    $item->getProduct()
                )
            ) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Get list of categories that are marked as mandatory
     */
    public function getMandatoryCategories()
    {
        $key = serialize(array(strtolower(__METHOD__)));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = array();
            $mandatory_categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addFieldToFilter('is_family', 1)
                ->addFieldToFilter('is_active', array('eq' => '1'))
                ->addAttributeToSelect('*');

            foreach ($mandatory_categories as $mandatory_category) {
                $result[] = $mandatory_category->getId();
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * @return Dyna_Cache_Model_Cache|Mage_Core_Model_Abstract
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * @param $data
     * @return array
     */
    protected function getItemsFromCollection($data)
    {
        $items = [];
        foreach ($data as $key => $coll) {
            $items[$key] = [];
            if ($coll instanceof Varien_Data_Collection) {
                foreach ($coll->getItems() as $item) {
                    $items[$key][] = $item->toArray();
                }
            } else {
                $items[$key][] = $coll;
            }
        }

        return $items;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param $product
     * @param $originalItem
     * @param $result
     * @return mixed
     */
    protected function getPromoProductBlock(Omnius_Checkout_Model_Sales_Quote_Item $item, $product, $originalItem, $result)
    {
        $allItems = $item->getQuote()->getAllItems();
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($allItems as $item) {
            $catalogModelTypeCond = $item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)
                || $item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_ADDON);

            $mafDiscountCond = $product->getPrijsMafNewAmount() || $product->getPrijsMafDiscountPercent();
            if ($catalogModelTypeCond && $originalItem->getTargetId() == $item->getProductId() &&
                $originalItem->getPackageId() == $item->getPackageId()
                && $mafDiscountCond
            ) {
                $result['mafTax'] = $item->getItemFinalMafInclTax();
                $result['maf'] = $item->getItemFinalMafExclTax();

                $result['price'] = $item->getItemFinalPriceExclTax();
                $result['priceTax'] = $item->getItemFinalPriceInclTax();

                break;
            }
        }

        return $result;
    }
} 
