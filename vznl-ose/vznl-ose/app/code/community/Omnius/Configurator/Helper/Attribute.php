<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Attribute
 */
class Omnius_Configurator_Helper_Attribute extends Mage_Core_Helper_Data
{
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Returns the attribute set id of the provided
     * attribute set name
     *
     * @param $setName
     * @return mixed|null
     */
    public function getAttributeSetId($setName)
    {
        $key = sprintf('%s_set_id', $setName);
        if ($result = $this->getCache()->load($key)) {
            return $result;
        } else {
            try {
                $id = Mage::getModel('eav/entity_attribute_set')
                    ->load($setName, 'attribute_set_name')
                    ->getAttributeSetId();
                $this->getCache()->save($id, $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
                return $id;
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                return null;
            }
        }
    }

    /**
     * @param $setId
     * @return false|mixed|null
     */
    public function getAttributeSetCode($setId)
    {
        $key = sprintf('%s_set_name', $setId);
        if ($result = $this->getCache()->load($key)) {
            return $result;
        } else {
            try {
                $name = Mage::getModel('eav/entity_attribute_set')
                    ->load($setId)
                    ->getAttributeSetName();
                $this->getCache()->save($name, $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
                return $name;
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                return null;
            }
        }
    }

    /**
     * Renders the available filters
     * @param Varien_Data_Collection $collection
     * @return array|mixed
     */
    public function getAvailableFilters(Varien_Data_Collection $collection)
    {
        $key = serialize($collection->getAllIds());
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $filters = array();
            /** @var Omnius_Catalog_Model_Product $item */
            foreach ($collection->getItems() as $item) {
                $filters = $this->array_merge_recursive($filters, $item->getAvailableFilters());
            }
            uasort(
                $filters,
                function ($a, $b) {
                    return $a['position'] > $b['position'] ? -1 : 1;
                }
            );

            foreach ($filters as $key => $props) {
                if (count($props['options']) < 2) {
                    unset($filters[$key]);
                }
                if (!$this->isAssoc($props['options'])) {
                    $filters[$key]['options'][""] = '';
                }
            }

            $this->getCache()->save(serialize($filters), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $filters;
        }
    }

    /**
     * Checks whether an array is associative or not
     * @param $array
     * @return bool
     */
    public function isAssoc($array)
    {
        return array_keys($array) !== range(0, count($array) - 1);
    }

    /**
     * Merges and flattens 2 or more arrays
     *
     * @return array
     */
    public function array_merge_recursive()
    {
        $arrays = func_get_args();
        $base = array_shift($arrays);
        foreach($arrays as $array) {
            reset($base);
            while(list($key, $value) = @each($array)) {
                if(is_array($value) && @is_array($base[$key])) {
                    $base[$key] = $this->array_merge_recursive($base[$key], $value);
                }
                else {
                    $base[$key] = $value;
                }
            }
        }
        return $base;
    }
 
    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * Retrieve all available sim card types defined in admin
     *
     * @return array
     */
    public function getSimCardTypes()
    {
        $website_id = Mage::app()->getWebsite()->getId();

        $key = __METHOD__ . '_' . $website_id;
        if ($collection = unserialize($this->getCache()->load($key))) {
            return $collection;
        } else {
            $code = Omnius_Catalog_Model_Product::AVAILABLE_SIM_TYPES_ATTRIBUTE;
            $attribute_details = Mage::getSingleton("eav/config")->getAttribute("catalog_product", $code);

            $options = $attribute_details->getSource()->getAllOptions();
            $items = array();
            foreach ($options as $option) {
                $simName = trim($option["label"]);
                if (strlen($simName)) {

                    // RFC-151057: Check if SIM product is available in website
                    $simProd = Mage::getModel('catalog/product')->loadByAttribute('name', $simName);

                    if ($simProd && $simProd->getId() && in_array($website_id, $simProd->getWebsiteIds())) {
                        $items[$option["label"]] = $option["label"];
                    }
                }
            }
            $result =  (count($items) > 0) ? $items : null;

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $result;
        }
    }

    /**
     * Check if a certain simcard type is available for a product
     * 
     * @param $product
     * @param $sim
     * @return bool
     */
    public function checkSimTypeAvailable($product, $sim)
    {
        $key = serialize(array(__METHOD__ , $product->getId() , $sim));
        if ($collection = unserialize($this->getCache()->load($key))) {
            return $collection;
        } else {

            $productModel = Mage::getModel('catalog/product');
            $attr = $productModel->getResource()->getAttribute(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE);
            if ($attr->usesSource()) {
                $simId = $attr->getSource()->getOptionId($sim);
            }

            $result = false;
            $productCollection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addAttributeToFilter(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE, $simId)->load();
            $allowedSims = $product->load($product->getId())->getAttributeText(Omnius_Catalog_Model_Product::AVAILABLE_SIM_TYPES_ATTRIBUTE);
            $allowedSims = is_array($allowedSims) ? $allowedSims : array($allowedSims);

            foreach($productCollection as $prod){
                if(in_array($prod->getName(), $allowedSims)){
                    $result = true;
                }
            }

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $result;

        }

    }
    
    /**
     * Return the value of simcard by Id
     *
     * @param $id
     * @return null
     */
    public function getSimCardTypeById($id)
    {
        $key = serialize(array(__METHOD__, $id));

        if ($v = Mage::registry($key)) {
            return $v !== '_nf_' ? $v : null;
        }

        if ($collection = $this->getCache()->load($key)) {
            return unserialize($collection);
        } else {
            $code = Omnius_Catalog_Model_Product::AVAILABLE_SIM_TYPES_ATTRIBUTE;
            $attributeDetails = Mage::getSingleton("eav/config")->getAttribute("catalog_product", $code);
            $allSims = $attributeDetails->getSource()->getAllOptions();
            foreach ($allSims as &$sim) {
                if ($sim['value'] == $id) {
                    $this->getCache()->save(serialize($sim['label']), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
                    Mage::register($key, $sim['label'], true);

                    return $sim['label'];
                }
            }
            $this->getCache()->save("N;", $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            Mage::register($key, '_nf_', true);

            return null;
        }
    }

    /**
     * @param $subscription
     * @param $sim
     * @return null
     *
     * Check if currently selected sim is allowed for the subscription
     */
    public function checkSubscriptionAllowsSim($subscription, $sim)
    {
        $simOptions = $subscription->getSimOptions();
        if (! $simOptions || ! is_array($simOptions)) {
            return null;
        }

        return in_array($sim, $simOptions) ? $sim : null;
    }

    /**
     * @param null $consumerType
     * @return string
     */
    public function getSubscriptionIdentifier($consumerType = null)
    {
        if ( ! $consumerType ) {
            $consumerType = Omnius_Catalog_Model_Product::IDENTIFIER_CONSUMER;
        }

        $key = sprintf('identifier_consumer_%s', $consumerType);
        $result = $this->getCache()->load($key);

        if ($result !== false) {
            return $result;
        } else {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE);
            $consValue = (string)Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $consumerType);
            $this->getCache()->save($consValue, $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $consValue;
        }
    }
}
