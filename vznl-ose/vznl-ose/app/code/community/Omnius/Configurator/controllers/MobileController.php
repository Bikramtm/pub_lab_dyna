<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Configurator_MobileController
 */

require_once Mage::getModuleDir('controllers', 'Omnius_Configurator').DS.'BaseController.php';

class Omnius_Configurator_MobileController extends Omnius_Configurator_BaseController
{
}