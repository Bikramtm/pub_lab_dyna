<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Proxy_Model_Proxy
 *
 * @method string getParent()
 * @method string getClass()
 * @method string getClassRef()
 * @method string getPath()
 * @method string getXpath()
 * @method string getModule()
 * @method string getType()
 * @method string getCode()
 * @method string getProxyDir()
 * @method ReflectionClass getReflection()
 * @method Omnius_Proxy_Model_Proxy setCode($code)
 */
class Omnius_Proxy_Model_Proxy extends Varien_Object
{
    const PROXY_DIR = 'CachedProxies';

    /**
     * @param $path
     * @return bool
     * @throws Exception
     */
    public static function create($path)
    {
        $basePath = Mage::getBaseDir('app') . DS . 'code' . DS;
        $subPath = trim(preg_replace('/^(local|community|core)/', '', str_replace($basePath, '', $path)), DS);
        list($prefix, $module, $type) = explode(DS, $subPath);
        $data = array(
            'module' => $prefix . '_' . $module,
            'type' => strtolower($type),
            'parent_path' => $path,
            'parent' => str_replace('_controllers_', '_', preg_replace('/\.php$/', '', str_replace(DS, '_', $subPath))),
        );

        $isController = ('controllers' === $data['type']);

        if ($isController && ! realpath($controllerDir = Mage::getModuleDir('controllers', 'Omnius_Proxy'))) {
            if (false == @mkdir($controllerDir)) {
                throw new Exception('Could not create controllers dir for Omnius_Proxy. Please check permissions or create it manually.');
            }
        }

        if ('block' === $data['type'] && ! realpath($blocksDir = (Mage::getModuleDir(false, 'Omnius_Proxy') . DS . 'Block'))) {
            if (false == @mkdir($blocksDir)) {
                throw new Exception('Could not create blocks dir for Omnius_Proxy. Please check permissions or create it manually.');
            }
        }

        try {
            if ($isController) {
                require_once $data['parent_path'];
            }
            $reflectionClass = new ReflectionClass($data['parent']);
        } catch (Exception $e) {
            //the class could not be loaded
            Mage::getSingleton('core/logger')->logException($e);
            return false;
        }

        $classDefinition = Mage::getConfig()->getNode()->xpath(sprintf("//class[starts-with(text(),'%s')]", $data['module']));
        if ($classDefinition) {
            $moduleDefinition = $classDefinition[0]->xpath('..');
            $data['module_ref'] = (string) $moduleDefinition[0]->getName();
        } else {
            return false;
        }

        $data['proxy_dir'] = sprintf(join(DS, array(Mage::getModuleDir(false, 'Omnius_Proxy'), ($data['type'] == 'controllers' ? 'controllers' : ucfirst($data['type'])), 'CachedProxies')));
        $hash = md5_file($data['parent_path']);
        $data['path'] = join(DS, array($data['proxy_dir'], join('', array(str_replace('_', '', $data['parent']), $hash)))) . '.php';
        preg_match('/\_(.*)/', $data['module'], $moduleSubDir);
        if ($isController) {
            $prefix = sprintf('Omnius_Proxy_%sCachedProxies_', join('_', array_filter(array($data['type']))) . '_');
            list($base, $classPath) = explode(DS.$data['type'].DS, $data['parent_path']);
            $parts = explode(DS, $classPath);
            array_pop($parts);
            $data['proxy_dir'] = $data['proxy_dir'] . DS . join(DS, array_filter(array(array_shift($parts), $moduleSubDir[1])));
            $data['sub_folder'] = join(DS, array_filter(array(array_shift($parts), $moduleSubDir[1])));
            $data['path'] = join(DS, array_filter(array(Mage::getModuleDir('controllers', 'Omnius_Proxy'), 'CachedProxies', $data['sub_folder'], $classPath)));
            $data['class'] = str_replace('_controllers_', '_', $prefix . $data['sub_folder'] . '_' . substr(str_replace(DS, '_', $classPath), 0, -4));
        } else {
            $prefix = sprintf('Omnius_Proxy_%sCachedProxies_', ucfirst($data['type']) . '_');
            list($base, $classPath) = explode(DS.ucfirst($data['type']).DS, $data['parent_path']);
            $parts = explode(DS, $classPath);
            array_pop($parts);
            $data['proxy_dir'] = join(DS, array_filter(array($data['proxy_dir'], $moduleSubDir[1], join(DS, $parts))));
            $data['class'] = join('_', array(rtrim($prefix, '_'), $moduleSubDir[1], substr(str_replace(DS, '_', $classPath), 0, -4)));
            $data['path'] = join(DS, array(Mage::getModuleDir(false, 'Omnius_Proxy'), ucfirst($data['type']), 'CachedProxies', $moduleSubDir[1], $classPath));
            $data['sub_folder'] = join(DS, array_filter(array(array_shift($parts), $moduleSubDir[1])));
        }
        $data['class_ref'] = strtolower(str_replace(join('_', array($data['module'], ucfirst($data['type']))) . '_', '', $data['parent']));;
        $data['xpath'] = sprintf('global/%ss/%s/class', strtolower($data['type']), strtolower($data['module_ref']));

        $data['is_internal'] = $reflectionClass->isInternal();
        $data['is_interface'] = $reflectionClass->isInterface();
        $data['is_final'] = $reflectionClass->isFinal();

        $methods = array();
        foreach ($reflectionClass->getMethods() as $method) {
            if ($method->getDeclaringClass()->getName() !== $reflectionClass->getName()) {
                /**
                 * Handle only the methods that
                 * are present in the current class
                 */
                continue;
            }

            $methodData = new Varien_Object();
            $methodData->setName($method->getName());
            $methodData->setIsPrivate($method->isPrivate());
            $methodData->setIsFinal($method->isFinal());
            $methodData->setDocComment($method->getDocComment());
            $methodData->setParameters(array_map(function($param){ return sprintf('$%s', $param->getName()); }, $method->getParameters()));
            $methodData->setFileName($method->getFileName());
            $methodData->setStartLine($method->getStartLine());
            $methodData->setEndLine($method->getEndLine());
            $methods[] = $methodData;
        }
        $data['methods_info'] = $methods;

        return Mage::getModel('proxy/proxy')->addData($data);
    }

    /**
     * @param $methodBody
     * @param $methodName
     * @return $this
     */
    public function addMethod($methodBody, $methodName)
    {
        if ( ! is_array($this->getData('methods_body'))) {
            $this->setData('methods_body', array($methodName => $methodBody));
        }
        $methods = $this->getData('methods_body');
        $methods[$methodName] = $methodBody;
        $this->setData('methods_body', $methods);

        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getMethods()
    {
        if ( ! is_array($this->getData('methods_body'))) {
            $this->setData('methods_body', array());
            return array();
        }
        return $this->getData('methods_body');
    }

    /**
     * @return bool|int
     */
    public function hasMethods()
    {
        if ( ! is_array($this->getData('methods_body'))) {
            return false;
        }
        return count($this->getData('methods_body')) !== 0;
    }

    /**
     * @return bool
     */
    public function isController()
    {
        return 'controllers' === strtolower($this->getType());
    }
}