<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Proxy_Model_Observer
 */
class Omnius_Proxy_Model_Observer {

    /**
     * Generates proxies and injects them in the config xml
     */
    public function generateProxies()
    {
        try {
            /** @var Omnius_Proxy_Model_Manager $manager */
            if ($manager = Mage::getSingleton('proxy/manager')) {
                $manager->createProxies();
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

}