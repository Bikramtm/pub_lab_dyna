<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Proxy_Model_Manager
 */
class Omnius_Proxy_Model_Manager
{
    /** @var Omnius_Proxy_Helper_Data */
    protected $_helper;

    /** @var Omnius_Proxy_Helper_File */
    protected $_fileHelper;

    /** @var Omnius_Proxy_Model_Writer */
    protected $_writer;

    /** @var Omnius_Proxy_Model_Injector */
    protected $_injector;

    /** @var Omnius_Proxy_Model_Parser */
    protected $_parser;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;


    /**
     * @return $this
     */
    public function createProxies()
    {
        if ( ! Mage::getConfig()->getXpath('proxy_injected')) {
            $this->cleanUp();
            $this->_getInjector()->injectProxies($this->generateProxies());
        }
        return $this;
    }

    /**
     * @return array
     */
    protected function generateProxies()
    {
        $proxies = array();
        foreach ($this->_getFile()->gatherFiles($this->_getHelper()->getModules()) as $path) {
            if ($proxy = $this->_createProxy($path)) { /** @var Omnius_Proxy_Model_Proxy $proxy */
                if ($proxy->getIsInternal()
                    || $proxy->getIsInterface()
                    || $proxy->getIsFinal()
                ) {
                    continue; //skip classes that do not need handling
                }

                foreach ($proxy->getMethodsInfo() as $method) {
                    if ( ! ($method->getIsPrivate() || $method->getIsFinal())
                        && ($annotations = $this->_getParser()->parseAnnotation($method->getDocComment()))
                    ) {
                        $proxy->addMethod($this->_getWriter()->getRewriteMethod($method, array_map(function($anno) use ($proxy) { return $anno->setIsController($proxy->isController()); }, $annotations), $proxy), $method->getName());
                    }
                }

                if ($proxy->hasMethods()) {
                    $proxies[] = $proxy->setCode($this->_getWriter()->rewriteClass($proxy));
                }
            }
        }
        return $proxies;
    }

    /**
     * Removes unused proxies
     */
    protected function cleanUp()
    {
        $types = array(
            'Block',
            'Model',
            'Helper',
            'controllers',
        );
        foreach ($types as $type) {
            $path = join(DS, array(Mage::getModuleDir(false, 'Omnius_Proxy'), $type, 'CachedProxies'));
            if (realpath($path) && is_dir($path)) {
                foreach ($this->_getFile()->findFiles($path) as $proxyFile) {
                    @unlink($proxyFile);
                }
            }
        }
    }

    /**
     * @param $path
     * @return bool|false|Mage_Core_Model_Abstract|mixed
     */
    protected function _createProxy($path)
    {
        $key = 'createProxy_' . md5_file($path);
        if ($proxy = $this->getCache()->load($key)) {
            return unserialize($proxy);
        } else {
            $proxy = Omnius_Proxy_Model_Proxy::create($path);
            $this->getCache()->save(serialize($proxy), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $proxy;
        }
    }

    /**
     * @return Omnius_Proxy_Model_Parser
     */
    protected function _getParser()
    {
        if ( ! $this->_parser) {
            $this->_parser = Mage::getSingleton('proxy/parser');
            $this->_parser->addSupportedAnnotations(Mage::helper('proxy')->getAnnotations());
        }
        return $this->_parser;
    }

    /**
     * @return Omnius_Proxy_Model_Injector
     */
    protected function _getInjector()
    {
        if ( ! $this->_injector) {
            return $this->_injector = Mage::getSingleton('proxy/injector');
        }
        return $this->_injector;
    }

    /**
     * @return Omnius_Proxy_Model_Writer
     */
    protected function _getWriter()
    {
        if ( ! $this->_writer) {
            return $this->_writer = Mage::getModel('proxy/writer');
        }
        return $this->_writer;
    }

    /**
     * @return Omnius_Proxy_Helper_Data
     */
    protected function _getHelper()
    {
        if ( ! $this->_helper) {
            return $this->_helper = Mage::helper('proxy');
        }
        return $this->_helper;
    }

    /**
     * @return Omnius_Proxy_Helper_File
     */
    protected function _getFile()
    {
        if ( ! $this->_fileHelper) {
            return $this->_fileHelper = Mage::helper('proxy/file');
        }
        return $this->_fileHelper;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }
} 
