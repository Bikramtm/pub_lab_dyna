<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Proxy_Model_Injector
 */
class Omnius_Proxy_Model_Injector
{
    /** @var Mage_Core_Model_Config */
    protected $_config;

    /**
     * @param array $proxies
     */
    public function injectProxies(array $proxies)
    {
        $this->_getConfig()
            ->cleanCache(); //reinit config

        foreach ($proxies as $proxy) {
            /** @var Omnius_Proxy_Model_Proxy $proxy */
            if (!file_exists($proxy->getPath())) {
                if (!realpath($proxy->getProxyDir())) {
                    if (false == (@mkdir($proxy->getProxyDir(), 0777, true))) {
                        Mage::throwException(sprintf('Could not create proxy directory at "%s". Please check permissions.', $proxy->getProxyDir()));
                    }
                }

                if (is_file($proxy->getProxyDir())) {
                    Mage::throwException(sprintf('Expected "%s" to be a directory. Please set proxy directory path to a directory.', $proxy->getProxyDir()));
                }

                if (false === @file_put_contents($proxy->getPath(), $proxy->getCode())) {
                    Mage::throwException('Could not write proxy to disk');
                }
            }
            if ($proxy->isController()) {
                $this->updateConfigForController($proxy);
            } else {
                $this->updateConfig($proxy);
            }
        }

        $this->_getConfig()
            ->getNode()
            ->addChild('proxy_injected', 1); //set flag to indicate successful injection

        $this->_getConfig()
            ->removeCache()
            ->saveCache(); //save changes made to the cache
    }

    /**
     * Insert in the config the class rewrite so that
     * when requesting a certain class, it's proxy is returned
     *
     * @param Omnius_Proxy_Model_Proxy $proxy
     */
    protected function updateConfig(Omnius_Proxy_Model_Proxy $proxy)
    {
        if (count($existingRewrite = $this->_getConfig()->getNode()->xpath(sprintf("//*[text() = '%s']", $proxy->getParent())))) { //already has a rewrite
            $existingRewrite[0][0] = $proxy->getClass();
        } else { //normal case
            $config = $this->_getConfig()->getNode()->xpath($proxy->getXpath());
            $moduleDefinition = $config[0]->xpath('..');
            $moduleDefinition = $moduleDefinition[0];
            if (!strlen((string)$moduleDefinition->rewrite->asXML())) {
                $moduleDefinition->addChild('rewrite');
            }
            $moduleDefinition->rewrite->addChild($proxy->getClassRef(), $proxy->getClass());
        }
    }

    /**
     * TODO simplify/cleanup
     * @param Omnius_Proxy_Model_Proxy $proxy
     */
    protected function updateConfigForController(Omnius_Proxy_Model_Proxy $proxy)
    {
        if ($n = $this->_getConfig()->getNode()->xpath(sprintf('frontend/routers/*/args/modules/*[text() = "%s"]', $proxy->getModule()))) {
            $n = $n[0]->xpath('..');
            $parts = explode('_', $proxy->getClass());

            array_pop($parts);
            $location = join('_', $parts);
            $existingRewrite = $n[0]->xpath(sprintf('//Omnius_Proxy[@before="%s"]', $proxy->getModule()));

            if (!($existingRewrite && $location === (string)$existingRewrite[0])) {
                $rewrite = $n[0]->addChild('Omnius_Proxy', $location);
                $rewrite->addAttribute('before', $proxy->getModule());
            }
        } elseif ($n = $this->_getConfig()->getNode()->xpath(sprintf('frontend/routers/*/args/module[text() = "%s"]', $proxy->getModule()))) {
            $n = $n[0]->xpath('..');
            $module = $n[0]->xpath('..');
            $use = $module[0]->xpath('use');
            unset($use[0][0]);
            $modules = $n[0]->addChild('modules');
            $parts = explode('_', $proxy->getClass());

            array_pop($parts);
            $rewrite = $modules->addChild('Omnius_Proxy', join('_', $parts));
            $rewrite->addAttribute('before', $proxy->getModule());
            $module[0]->addChild('use', 'standard');
        } elseif ($n = $this->_getConfig()->getNode()->xpath(sprintf('admin/routers/*/args/modules/*[text() = "%s"]', $proxy->getModule()))) {
            $n = $n[0]->xpath('..');
            $parts = explode('_', $proxy->getClass());
            array_pop($parts);
            $location = join('_', $parts);
            $existingRewrite = $n[0]->xpath(sprintf('//Omnius_Proxy[@before="%s"]', $proxy->getModule()));
            if (!($existingRewrite && $location === (string)$existingRewrite[0])) {
                $rewrite = $n[0]->addChild('Omnius_Proxy', join('_', $parts));
                $rewrite->addAttribute('before', $proxy->getModule());
            }
        } elseif ($n = $this->_getConfig()->getNode()->xpath(sprintf('admin/routers/*/args/module[text() = "%s"]', $proxy->getModule()))) {
            $n = $n[0]->xpath('..');
            $module = $n[0]->xpath('..');
            $use = $module[0]->xpath('use');
            unset($use[0][0]);
            $modules = $n[0]->addChild('modules');
            $parts = explode('_', $proxy->getClass());
            array_pop($parts);
            $rewrite = $modules->addChild('Omnius_Proxy', join('_', $parts));
            $rewrite->addAttribute('before', $proxy->getModule());
            $module[0]->addChild('use', 'admin');
        }
    }

    /**
     * @return Mage_Core_Model_Config
     */
    protected function _getConfig()
    {
        if (!$this->_config) {
            return $this->_config = Mage::getConfig();
        }
        return $this->_config;
    }
}