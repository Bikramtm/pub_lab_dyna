<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Proxy_Model_Annotation_AbstractAnnotation
 */
abstract class Omnius_Proxy_Model_Annotation_AbstractAnnotation extends Varien_Object
{
    /** @var string */
    protected $_varName = '';

    public function _construct()
    {
        parent::_construct();

        $this->_varName = sprintf('$annotation%s%s', get_called_class(), md5(time() . spl_object_hash($this)));
    }

    /**
     * Executed before the method that has
     * assigned this annotation. If the
     * method returns any result that passes
     * the truth check (==), that result will be returned.
     * To allow the normal method to be executed,
     * return a value that validates as false.
     *
     * @throws DomainException
     * @return mixed
     */
    abstract public function canProceed();

    /**
     * @return string
     */
    public function initString()
    {
        $params = str_replace(PHP_EOL, '', var_export($this->getData(), true));
        return sprintf("        %s = new %s(%s);", $this->_varName, get_called_class(), $params);
    }

    /**
     * @return string|void
     */
    public function callString()
    {
        return sprintf('%s->canProceed()', $this->_varName);
    }

    /**
     * @return string
     */
    public function getReferer()
    {
        $request = Mage::app()->getRequest();
        $refererUrl = $request->getServer('HTTP_REFERER');
        if ($url = $request->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_REFERER_URL)) {
            $refererUrl = $url;
        }
        if ($url = $request->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_BASE64_URL)) {
            $refererUrl = Mage::helper('core')->urlDecode($url);
        }
        if ($url = $request->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_URL_ENCODED)) {
            $refererUrl = Mage::helper('core')->urlDecode($url);
        }

        if (!$this->_isUrlInternal($refererUrl)) {
            $refererUrl = Mage::app()->getStore()->getBaseUrl();
        }
        return $refererUrl;
    }

    /**
     * Check url to be used as internal
     *
     * @param   string $url
     * @return  bool
     */
    protected function _isUrlInternal($url)
    {
        if (strpos($url, 'http') !== false) {
            /**
             * Url must start from base secure or base unsecure url
             */
            if ((strpos($url, Mage::app()->getStore()->getBaseUrl()) === 0)
                || (strpos($url, Mage::app()->getStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK, true)) === 0)
            ) {
                return true;
            }
        }
        return false;
    }
}