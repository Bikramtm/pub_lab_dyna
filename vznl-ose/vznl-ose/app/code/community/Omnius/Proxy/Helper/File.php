<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Proxy_Helper_File
 */
class Omnius_Proxy_Helper_File
{
    /**
     * @param $modules
     * @return array
     */
    public function gatherFiles($modules)
    {
        $paths = array();
        foreach ($modules as $module) {
            /**
             * Handle only models, blocks, helpers and controllers
             */
            $modelPath = Mage::getModuleDir(false, $module);
            $paths[] = $modelPath . DS . 'Model';
            $paths[] = $modelPath . DS . 'Helper';
            $paths[] = $modelPath . DS . 'Block';
            $paths[] = $modelPath . DS . 'controllers';
        }

        $files = array();
        foreach ($paths as $path) {
            foreach ($this->findFiles($path) as $filePath) {
                $files[] = $filePath;
            }
        }

        return $files;
    }

    /**
     * @param $path
     * @return array
     */
    public function findFiles($path)
    {
        $paths = array();
        if (realpath($path)) {
            $flags = FilesystemIterator::SKIP_DOTS | FilesystemIterator::FOLLOW_SYMLINKS;
            $iterator = new \RecursiveDirectoryIterator($path, $flags);
            $iterator = new \RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);

            foreach ($iterator as $file) /** @var SplFileInfo $file */
            {
                if ($file->isFile() && (substr($file->getBasename(), -4) === '.php')) {
                    $paths[] = $file->getRealPath();
                }
            }
        }
        return $paths;
    }
} 