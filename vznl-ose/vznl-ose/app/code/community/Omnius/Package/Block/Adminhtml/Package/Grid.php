<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Package_Block_Adminhtml_Package_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $packageTypes = null;

    public function __construct()
    {
        parent::__construct();
        $this->setId("packageGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("package/packageSubtype")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn("entity_id", array(
            "header" => Mage::helper("omnius_package")->__("ID"),
            "index" => "entity_id",
            "width" => "50px",
            "type" => "number",
        ));
        $this->addColumn("package_code", array(
            "header" => Mage::helper("omnius_package")->__("Package subtype code"),
            "index" => "package_code",
        ));
        $this->addColumn("front_end_name", array(
            "header" => Mage::helper("omnius_package")->__("Frontend GUI name"),
            "index" => "front_end_name",
        ));
        $this->addColumn("front_end_position", array(
            "header" => Mage::helper("omnius_package")->__("Frontend position"),
            "index" => "front_end_position",
        ));
        $this->addColumn("cardinality", array(
            "header" => Mage::helper("omnius_package")->__("Package cardinality"),
            "index" => "cardinality",
        ));
        $this->addColumn("type_package_id", array(
            "header" => Mage::helper("omnius_package")->__("Package type"),
            "index" => "type_package_id",
            "renderer" => "omnius_package/adminhtml_package_type",
            'type' => 'options',
            'options' => self::getPackageTypes(),
        ));
        $this->addColumn("is_visible", array(
            "header" => Mage::helper("omnius_package")->__("Is visible"),
            "index" => "is_visible",
            "renderer" => "omnius_package/adminhtml_package_visible",
            'type' => 'options',
            'options' => self::getVisibilityOptions(),
        ));

        return parent::_prepareColumns();
    }

    public static function getVisibilityOptions()
    {
        return array(
            1 => "Visible",
            0 => "Not visible",
        );
    }

    public function getPackageTypes()
    {
        if ($this->packageTypes == null) {
            $packageTypes = Mage::getModel('package/packageType')
                ->getCollection();
            foreach ($packageTypes as $packageType) {
                $this->packageTypes[$packageType->getId()] = $packageType->getPackageCode();
            }
        }

        return $this->packageTypes;
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()
            ->addItem('hide_packages', array(
                    'label' => Mage::helper('omnius_package')->__('Hide packages'),
                    'url' => $this->getUrl('/package/hidePackages'),
                    'confirm' => Mage::helper('omnius_package')->__('Are you sure?')
                ))
            ->addItem('show_packages', array(
                'label' => Mage::helper('omnius_package')->__('Make them visible'),
                'url' => $this->getUrl('/package/showPackages'),
            ))
        ;
        return $this;
    }
}
