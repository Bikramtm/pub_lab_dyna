<?php

class Omnius_Package_Block_Adminhtml_PackageType_Visible extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Renders grid column for package type visibility
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        return $row->getIsVisible() ? "Yes" : "No";
    }
}
