<?php

class Omnius_Package_Adminhtml_PackageTypeController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("catalog/package")->_addBreadcrumb(Mage::helper("adminhtml")->__("Packages types configuration"), Mage::helper("adminhtml")->__("Manage packages types"));

        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Omnius package types"));

        $this->_initAction();
        $this->renderLayout();
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("package/packageType")->load($id);

        if ($model->getId()) {
            Mage::register("package_model_type", $model);

            $this->_title($this->__("Package type"));
            $this->_title($this->__("Edit package types"));

            $this->_initAction();
            $this->renderLayout();
        } else {
            $this->_initAction();
            $this->renderLayout();
        }
    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            $model = Mage::getModel("package/packageType");

            if ($this->getRequest()->getParam("id")) {
                $existingOption = $model->load($this->getRequest()->getParam("id"));
                $existingOption->addData($postData);

                $existingOption->save();
            } else {
                $model->setData($postData);
                $model->save();
            }
        }

        $this->_redirect("*/*/");
    }

    public function hidePackagesAction()
    {
        $postData = $this->getRequest()->getPost();

        $packages = Mage::getModel('package/packageType')
            ->getCollection()
            ->addFieldToFilter('entity_id', ['in' => $postData['entity_ids']]);
        foreach ($packages as $package) {
            $package->setIsVisible(0)
                ->save();
        }

        $this->_redirect("*/*/");
    }

    public function showPackagesAction()
    {
        $postData = $this->getRequest()->getPost();

        $packages = Mage::getModel('package/packageType')
            ->getCollection()
            ->addFieldToFilter('entity_id', ['in' => $postData['entity_ids']]);
        foreach ($packages as $package) {
            $package->setIsVisible(1)
                ->save();
        }

        $this->_redirect("*/*/");
    }
	    
	/**
     * Method that handles deletion of package type
     */
    public function deleteAction()
    {
        $params = $this->getRequest()->getParam("id");
        Mage::getModel('package/packageType')
            ->load($params)
            ->delete();

        $this->_redirect("*/*/");
    }
}
