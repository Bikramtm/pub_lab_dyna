<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Package_Helper_Builder
 * Builds the
 */
class Omnius_Package_Helper_Builder extends Mage_Core_Helper_Data
{
    const UNTOUCHED_PACKAGE             = 0b0;
    const EDITED_PACKAGE                = 0b1;
    const ADDRESS_PACKAGE               = 0b10;
    const EDITED_WITH_ADDRESS_PACKAGE   = 0b11;
    const CANCELLED_PACKAGE             = 0b100;

    /** @var Omnius_Checkout_Model_Sales_Quote|null  */
    private $_superQuote = null;

    private $_arrayPackageModels = [];

    /**
     * @param Omnius_Checkout_Model_Sales_Quote $superQuote
     * @return $this
     */
    public function setSuperQuote(Omnius_Checkout_Model_Sales_Quote $superQuote)
    {
        $this->_superQuote = $superQuote;
        return $this;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Quote|null
     */
    public function getSuperQuote()
    {
        return $this->_superQuote;
    }

    /**
     * Omnius_Package_Helper_Builder constructor.
     */
    public function __construct()
    {
        $this->_arrayPackageModels = [
            self::UNTOUCHED_PACKAGE             => 'package/untouchedPackage',
            self::EDITED_PACKAGE                => 'package/editedPackage',
            self::ADDRESS_PACKAGE               => 'package/addressPackage',
            self::EDITED_WITH_ADDRESS_PACKAGE   => 'package/addressEditedPackage',
            self::CANCELLED_PACKAGE             => 'package/cancelledPackage'
        ];
    }

    /**
     * @param int $packageId
     * @param Omnius_Checkout_Model_Sales_Quote_Item[] $items
     * @param array|null $payment
     * @param Omnius_Checkout_Model_Sales_Quote $changedQuote
     * @param Omnius_Checkout_Model_Sales_Quote_Address $newAddress
     * @param bool $cancelled
     * @return Omnius_Package_Model_Package
     */
    public function buildPackage($packageId, $items, $payment = null, $changedQuote = null, $newAddress = null, $cancelled = null)
    {
        $packageModelType =
            self::UNTOUCHED_PACKAGE
            | ($changedQuote ? self::EDITED_PACKAGE : self::UNTOUCHED_PACKAGE)
            | ($newAddress ? self::ADDRESS_PACKAGE : self::UNTOUCHED_PACKAGE)
            | ($cancelled ? self::CANCELLED_PACKAGE : self::UNTOUCHED_PACKAGE);


        /** @var Omnius_Package_Model_Package $package */
        $packageBase = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $changedQuote && $changedQuote->getId() ? $changedQuote->getId() : $this->getSuperQuote()->getId())
            ->addFieldToFilter('package_id', $packageId)->getFirstItem();
        
        $package = Mage::getModel($this->_arrayPackageModels[$packageModelType])->setData($packageBase->getData());
        $package
            ->setModifiedQuote($changedQuote)
            ->setNewAddress($newAddress)
            ->setPayment($payment)
            ->setOldItems($items)
        ;

        return $package;
    }
}
