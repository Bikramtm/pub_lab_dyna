<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'one_of_deal', 'varchar(20) null');
$connection->addColumn($this->getTable('catalog_package'), 'retainable', 'varchar(20) null');
$installer->endSetup();
