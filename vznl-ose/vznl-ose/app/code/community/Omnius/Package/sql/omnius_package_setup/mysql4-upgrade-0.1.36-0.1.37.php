<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$this->startSetup();
$connection = $this->getConnection();
$items = [
    'manual_activation_reason' => [
        'comment' => 'Manual activation reason picked by the agent from the dropdown',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'required' => false,
    ],
    'manual_activation_user_input' => [
        'comment' => 'Manual activation reason input from the agent',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'required' => false,
    ]
];
foreach ($items as $code => $data) {
    $connection->addColumn($this->getTable('package/package'), $code, $data);
}
$this->endSetup();
