<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'creditcheck_status_updated', 'TIMESTAMP');
$connection->addColumn($this->getTable('catalog_package'), 'porting_status_updated', 'TIMESTAMP');
$installer->endSetup();