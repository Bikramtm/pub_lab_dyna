<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Omnius
 * @package     Omnius_Package
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$connection = $installer->getConnection();
$connection->dropForeignKey('catalog_package','catalog_package_order_id');
$connection->dropForeignKey('catalog_package','catalog_package_quote_id');
$connection->dropColumn('catalog_package','quote_id');
$connection->addForeignKey(
    'fk_catalog_package_order_id',
    'catalog_package',
    'order_id',
    'superorder',
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_CASCADE
);

$installer->endSetup();
