<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$this->startSetup();
$connection = $this->getConnection();

$connection->addColumn($this->getTable('package/package'), 'actual_porting_date',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'comment' => 'Actual porting date',
        'required' => false,
        'after' => 'contract_end_date',
        'nullable' => true
    )
);


$this->endSetup();