<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$connection = $this->getConnection();
$connection->addIndex($this->getTable('sales_flat_order_address'), 'IDX_SALES_FLAT_ORDER_ADDRESS_POSTCODE', 'postcode');
$connection->addIndex($this->getTable('catalog_package'), 'IDX_CATALOG_PACKAGE_PORTING_STATUS', 'porting_status');
$connection->addIndex($this->getTable('catalog_package'), 'IDX_CATALOG_PACKAGE_PORTING_UPDATE', 'porting_status_updated');
$connection->addColumn($this->getTable('package/package'), 'actual_porting_date',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'comment' => 'Actual porting date',
        'required' => false,
        'after' => 'contract_end_date',
        'nullable' => true
    )
);

$customerInstaller->endSetup();