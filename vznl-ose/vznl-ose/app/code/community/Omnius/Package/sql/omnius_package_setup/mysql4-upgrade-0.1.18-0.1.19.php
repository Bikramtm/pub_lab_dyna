<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('catalog_package'), 'contract_nr', 'varchar(50) null');

$installer->endSetup();