<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('catalog_package'), 'hardware_change', Varien_Db_Ddl_Table::TYPE_BOOLEAN);
$installer->endSetup();
