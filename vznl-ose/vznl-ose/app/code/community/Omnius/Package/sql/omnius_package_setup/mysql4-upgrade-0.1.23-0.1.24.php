<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$this->getConnection()->addIndex($this->getTable('catalog_package'), 'IDX_CATALOG_PACKAGE_ESB_ORDER_NUMBER', 'package_esb_number');
$this->endSetup();
