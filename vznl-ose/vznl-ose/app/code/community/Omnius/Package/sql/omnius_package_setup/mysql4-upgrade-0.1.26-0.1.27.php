<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
/**
 * Add package approve_order_job_id codes field used for ApproveOrder functionality
 */
$installer = $this;
$installer->startSetup();
$connection = $this->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'approve_order_job_id', 'VARCHAR(40) NULL');
$installer->endSetup();