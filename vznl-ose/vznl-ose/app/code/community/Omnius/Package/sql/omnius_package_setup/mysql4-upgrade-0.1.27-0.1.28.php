<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Add package approve_order_job_id codes field used for ApproveOrder functionality
 */
$installer = $this;
$installer->startSetup();
$connection = $this->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'created_at', 'datetime null');
$connection->addColumn($this->getTable('catalog_package'), 'updated_at', 'datetime null');

$installer->endSetup();
