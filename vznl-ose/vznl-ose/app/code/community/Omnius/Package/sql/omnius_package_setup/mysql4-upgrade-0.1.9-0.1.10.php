<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'sale_type', 'varchar(20) null');
$connection->addColumn($this->getTable('catalog_package'), 'ctn', 'varchar(20) null');
$connection->addColumn($this->getTable('catalog_package'), 'current_products', 'varchar(255) null');
$installer->endSetup();
