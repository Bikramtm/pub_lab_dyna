<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;

$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'creditcheck_status', 'varchar(100) null');
$connection->addColumn($this->getTable('catalog_package'), 'porting_status', 'varchar(100) null');

$installer->endSetup();