<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'tel_number', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'current_number', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'sim_number', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'current_simcard_number', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'network_provider', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'connection_type', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'contract_end_date', 'datetime null');
$connection->addColumn($this->getTable('catalog_package'), 'number_porting_type', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'network_operator', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'imei', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'fixed_to_mobile', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'vodafone_home', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'new_holland', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'credit_note', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'manual_pickup', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'manual_pickup_remarks', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'old_sim', 'varchar(50) null');
$installer->endSetup();
