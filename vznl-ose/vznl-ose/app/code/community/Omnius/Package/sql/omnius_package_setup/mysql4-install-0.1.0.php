<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Omnius
 * @package     Omnius_Package
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();
if (!$this->getConnection()->isTableExists($this->getTable('catalog_package'))) {
    $table = new Varien_Db_Ddl_Table();
    $table->setName('catalog_package');

    /**
     * Create table
     */
    $table->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'ID')
        ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true,
            'unsigned' => true,
        ), 'Order id')
        ->addColumn('package_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false
        ), 'Package id')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
            'nullable' => false
        ), 'Status from ESB')
        ->addForeignKey('catalog_package_order_id', 'order_id', 'superorder', 'entity_id',
            Varien_Db_Ddl_Table::ACTION_SET_NULL, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Packages');

    $installer->getConnection()->createTable($table);
}
$installer->endSetup();
