<?php

class Omnius_Package_Model_Configuration extends Mage_Core_Model_Abstract
{
    /** @var $package Omnius_Package_Model_Package */
    protected $package;

    /**
     * Set the package model to witch current instance is attached
     * @param Omnius_Package_Model_Package $package
     * @return $this
     */
    public function setPackage(Omnius_Package_Model_Package $package)
    {
        $this->package = $package;

        return $this;
    }

    /**
     * Get configuration for current package
     * @param string $package
     * @return Omnius_Package_Model_Mysql4_PackageSubtype_Collection
     */
    public function getConfiguration()
    {
        $packageType = Mage::getModel('package/packageType')
            ->getCollection()
            ->loadByCode($this->package->getType());
        /** @var Omnius_Package_Model_PackageSubtype $packageType */
        if (!$packageType || !$packageType->getId()) {
            Mage::throwException("Requested section type: " . $this->package->getType() . " does not exist in package types configuration (check import files: products, package types). Cannot continue building package status ...");
        }

        /** @var Omnius_Package_Model_Mysql4_PackageSubtype_Collection $packageSubtypes */
        $packageSubtypes = Mage::getModel('package/packageSubtype')
            ->getCollection()
            ->loadByPackageType($packageType);

        return $packageSubtypes;
    }

    /**
     * Get the minimum number of items required
     * @param $cardinality 1..n
     * @return int
     */
    public function getMinProducts($cardinality) {
        $regexp = '/^(\d+)\.+([\d]*|[n])$/';
        $match = preg_match($regexp, $cardinality, $card);

        if ( $match && isset($card[1]) ) {
            if($card[1] == 'n') {
                $card[1] = -1;
            }
            return (int)$card[1];
        } else {
            return 0;
        }
    }

    /**
     * Get the maximum number of items required
     * @param $cardinality 1..n
     * @return int
     */
    public function getMaxProducts($cardinality) {
        $regexp = '/^(\d+)\.+([\d]*|[n])$/';
        $match = preg_match($regexp, $cardinality, $card);

        if ( $match && isset($card[2])) {
            if($card[2] == 'n') {
                $card[2] = -1;
            }

            return (int)$card[2];
        } else {
            return 0;
        }
    }
}
