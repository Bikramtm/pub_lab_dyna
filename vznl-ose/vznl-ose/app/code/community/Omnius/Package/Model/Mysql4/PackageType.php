<?php
/**
 * Class Omnius_Package_Model_Mysql4_PackageType
 */
class Omnius_Package_Model_Mysql4_PackageType extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init("package/type", "entity_id");
    }
}
