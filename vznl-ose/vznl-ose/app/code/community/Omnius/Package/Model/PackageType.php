<?php

class Omnius_Package_Model_PackageType extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("package/packageType");
    }

    /**
     * @param string|array $codes
     * @return Omnius_Package_Model_Mysql4_PackageSubtype_Collection
     */
    public function getSubtypes($codes)
    {
        /** @var Omnius_Package_Model_Mysql4_PackageSubtype_Collection $packageSubtypes */
        $packageSubtypes = Mage::getModel('package/packageSubtype')
            ->getCollection()
            ->loadByPackageTypeAndCode($this, $codes);

        return $packageSubtypes;
    }
}
