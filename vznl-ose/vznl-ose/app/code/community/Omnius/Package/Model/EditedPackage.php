<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Package_Model_EditedPackage extends Omnius_Package_Model_Package
{
    public function _construct()
    {
        $this->_binaryState = Omnius_Package_Helper_Builder::EDITED_PACKAGE;
        parent::_construct();
    }

    /**
     * @return $this
     */
    public function build()
    {
        $this->setItems($this->getModifiedQuote()->getAllItems());
        // Set old address as the package address.
        $address = $this->getProcessedOrder()->getShippingAddress()->getData();
        //Unset indexes as these addresses are used to generate hashes based on which we generate the final orders.
        unset($address['entity_id'], $address['parent_id'], $address['quote_address_id']);
        $this->setAddress($address);
        parent::build();

        return $this;
    }
}