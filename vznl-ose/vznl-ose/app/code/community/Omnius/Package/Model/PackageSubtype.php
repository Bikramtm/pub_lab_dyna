<?php

class Omnius_Package_Model_PackageSubtype extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("package/packageSubtype");
    }

    public function isAddMultiAllowed()
    {
        $regexp = '/^(\d+)\.+([\d]*|[n])$/';
        $match = preg_match($regexp, $this->getCardinality(), $card);

        if ( $match && isset($card[2]) && $card[2] != 'n' && $card[2] <= 1 ) {
            return false;
        }

        return true;
    }
}
