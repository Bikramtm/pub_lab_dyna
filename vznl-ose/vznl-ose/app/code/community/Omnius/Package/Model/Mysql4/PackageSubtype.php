<?php
/**
 * Class Omnius_Package_Model_Mysql4_PackageSubtype
 */
class Omnius_Package_Model_Mysql4_PackageSubtype extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init("package/subtype", "entity_id");
    }
}
