<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Package_Model_Mysql4_Package_Collection
 */
class Omnius_Package_Model_Mysql4_Package_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("package/package");
    }

    /**
     * Count all results from a query
     *
     * @return int
     */
    public function getCount()
    {
        return count($this->getConnection()->fetchAll($this->getSelectCountSql()));
    }
}
