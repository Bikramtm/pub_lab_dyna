<?php

/**
 * Class Omnius_Package_Model_Import_Package
 */
class Omnius_Package_Model_Import_Package extends Omnius_Import_Model_Import_ImportAbstract
{
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ';';
    /** @var string $_logFileName */
    protected $_logFileName = "package_types_import";

    /** @var array $packageTypes */
    // This variable will store every package type instance by code index after saving / loading
    protected $packageTypes = [];

    /**
     * Omnius_Package_Model_Import_Package constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper->setImportLogFile($this->_logFileName . '_' . date("Y-m-d") . '.log');
        $this->_qties = 0;
    }

    /**
     * Import cable product
     *
     * @param array $data
     * @return void
     */
    public function importPackage($data)
    {
        try {
            $data = array_combine($this->_header, $data);

            // Let's make use of a Varien_Object
            /**
             * @method getPackageSubtypeName()
             */
            $importData = new Varien_Object();
            $importData->setData($data);

            $packageTypeCode = strtoupper($importData->getPackageTypeCode());
            if (empty($this->packageTypes[$packageTypeCode])) {
                // Let's try loading it
                $packageTypeModel = Mage::getModel('package/packageType')
                    ->getCollection()
                    ->addFieldToFilter('package_code', ['eq' => $packageTypeCode])
                    ->getFirstItem();
                if (!$packageTypeModel || !$packageTypeModel->getId()) {
                    $packageTypeModel = Mage::getModel('package/packageType');
                }
                // If exists, update its data
                $packageTypeModel
                    ->setFrontEndName($importData->getPackageTypeName())
                    ->setPackageCode($packageTypeCode)
                    ->save();

                $this->packageTypes[$packageTypeCode] = $packageTypeModel;
            } else {
                $packageTypeModel = $this->packageTypes[$packageTypeCode];
            }

            // Try loading package subtype by code
            $packageSubTypeModel = Mage::getModel('package/packageSubtype')
                ->getCollection()
                ->addFieldToFilter('package_code', ['eq' => $importData->getPackageSubtypeCode()])
                ->addFieldToFilter('type_package_id', ['eq' => $packageTypeModel->getId()])
                ->getFirstItem();
            if (!$packageTypeModel || !$packageTypeModel->getId()) {
                $packageSubTypeModel = Mage::getModel('package/configuration');
            }

            // Set data
            $packageSubTypeModel
                ->setTypePackageId($packageTypeModel->getId())
                ->setPackageCode($importData->getPackageSubtypeCode())
                ->setFrontEndName($importData->getPackageSubtypeName())
                ->setCardinality($importData->getPackageSubtypeCardinality());

            $cardinality = mb_convert_encoding($packageSubTypeModel->getCardinality(), "UTF-8");
            // Dot sign in example files has an invalid character converted to question sign
            // Doesn't matter how many points are in cardinality, they will be trimmed
            $cardinality = str_replace("?", "...", $cardinality);
            $packageSubTypeModel->setCardinality($cardinality);

            // Set visibility
            $packageSubTypeModel->setVisibleConfigurator(
                strtolower($importData->getPackageSubtypeHideConfigurator()) == "yes" ? 0 : 1
            )->setVisibleCart(
                ($importData->getPackageSubtypeHideCart() == "yes") ? 0 : 1
            );
            // Set position
            $packageSubTypeModel->setFrontEndPosition(
                // If no position defined, set the max (least) one
                (int)$importData->getPackageSubtypePosition() ?: 10
            );

            $packageSubTypeModel->save();

            $this->_log('Finished importing: ' . $packageTypeModel->getFrontEndName() . " >> " . $packageSubTypeModel->getFrontEndName());
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }

    /**
     * Set header on current import instance
     * @param $header array
     * @return $this
     */
    public function setHeader($header)
    {
        $builtHeader = [];

        $mapping = array(
            "package_type" => "package_type_code",
            "package_type_gui_name" => "package_type_name",
            "package_type_gui-name" => "package_type_name",
            "package_subtype" => "package_subtype_code",
            "package_subtype_gui_name" => "package_subtype_name",
            "package_subtype_sorting" => "package_subtype_position",
            "package_subtype_gui_sorting" => "package_subtype_position",
            "sorting" => "package_subtype_position",
            "cardinality" => "package_subtype_cardinality"
        );

        foreach ($header as $columnHead) {
            $builtHeader[] = array_key_exists($columnHead, $mapping) ? $mapping[$columnHead] : $columnHead;
        }

        $this->_header = $builtHeader;

        return $this;
    }
}
