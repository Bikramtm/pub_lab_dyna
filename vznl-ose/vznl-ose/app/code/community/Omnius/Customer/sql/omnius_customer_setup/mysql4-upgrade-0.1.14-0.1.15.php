<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();
$connection = $customerInstaller->getConnection();

$connection->addColumn($this->getTable('customer/entity'), 'ban', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 15,
    'nullable' => true,
    'required' => false,
    'comment' => "Customer ban"
]);

$connection->addIndex($this->getTable('customer/entity'), 'IDX_CUSTOMER_ENTITY_BAN', 'ban');
$connection->addIndex($this->getTable('customer_entity_datetime'), 'IDX_CUSTOMER_ENTITY_DATETIME__ATTRIBUTE_ID_VALUE', 'value');
$connection->addIndex($this->getTable('customer_entity_varchar'), 'IDX_CUSTOMER_ENTITY_VARCHAR_ATTRIBUTE_ID_VALUE', 'value');
$connection->addIndex($this->getTable('customer_address_entity_varchar'), 'IDX_CUSTOMER_ADDRESS_ENTITY_VARCHAR_ATTRIBUTE_ID_VALUE', array('value', 'attribute_id'));

$customerInstaller->endSetup();