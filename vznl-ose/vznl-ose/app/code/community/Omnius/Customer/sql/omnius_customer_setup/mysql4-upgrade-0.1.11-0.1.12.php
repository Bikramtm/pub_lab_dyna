<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/***************************************************************************
 * Customer attributes
 ***************************************************************************/
/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();

$attributesInfo = array(
    'customer_label' => array(
        'label' => 'Customer Label',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
);

foreach ($attributesInfo as $attributeCode => $attributeParams) {
    $customerInstaller->addAttribute('customer', $attributeCode, $attributeParams);
}

$customerInstaller->endSetup();
