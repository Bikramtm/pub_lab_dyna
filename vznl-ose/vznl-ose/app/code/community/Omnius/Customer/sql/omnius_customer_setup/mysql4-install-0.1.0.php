<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = new Mage_Customer_Model_Entity_Setup('core_setup');
$installer->startSetup();

$attributes = [
    [
        'entity' => 'customer',
        'attr_code' => 'customer_ctn',
        'data' => [
            'group' => 'Default',
            'type' => 'varchar',
            'label' => 'CTN',
            'note' => 'Customer Primary Telephone Number',
            'input' => 'text',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required' => 0,
            'visible_on_front' => 1,
            'used_for_price_rules' => 1,
            'adminhtml_only' => 0,
            'sort_order' => 200,
        ]
    ],
    [
        'entity' => 'customer',
        'attr_code' => 'customer_ban',
        'data' => [
            'group' => 'Default',
            'type' => 'varchar',
            'label' => 'BAN',
            'note' => 'Customer Business Account Number',
            'input' => 'text',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required' => 0,
            'visible_on_front' => 1,
            'used_for_price_rules' => 1,
            'adminhtml_only' => 0,
            'sort_order' => 210,
        ]
    ],
    [
        'entity' => 'customer',
        'attr_code' => 'value_segment',
        'data' => [
            'group' => 'Default',
            'type' => 'int',
            'label' => 'Value Segment',
            'note' => 'Customer Value Segment',
            'input' => 'select',
            'source' => 'eav/entity_attribute_source_boolean',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required' => 0,
            'visible_on_front' => 1,
            'used_for_price_rules' => 1,
            'adminhtml_only' => 0,
            'sort_order' => 220,
        ]
    ],
    [
        'entity' => 'customer',
        'attr_code' => 'customer_value',
        'data' => [
            'group' => 'Default',
            'type' => 'varchar',
            'label' => 'Value',
            'note' => 'Customer Value',
            'input' => 'select',
            'source' => 'eav/entity_attribute_source_table',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required' => 0,
            'visible_on_front' => 1,
            'used_for_price_rules' => 1,
            'adminhtml_only' => 0,
            'sort_order' => 230,
            'option' => [
                'values' => [
                    'WA6A' => 'WA6A',
                    'WB6B' => 'WB6B',
                    'WC6C' => 'WC6C',
                    'WD6D' => 'WD6D',
                    'WE6E' => 'WE6E',
                    'WF6F' => 'WF6F',
                    'WG6G' => 'WG6G',
                ]
            ],
        ],
    ],
    [
        'entity' => 'customer',
        'attr_code' => 'is_business',
        'data' => [
            'group' => 'Default',
            'type' => 'int',
            'label' => 'Business',
            'note' => 'Is Business Customer',
            'input' => 'select',
            'source' => 'eav/entity_attribute_source_boolean',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required' => 0,
            'visible_on_front' => 1,
            'used_for_price_rules' => 1,
            'adminhtml_only' => 0,
            'sort_order' => 240,
        ]
    ],
    [
        'entity' => 'customer',
        'attr_code' => 'company_name',
        'data' => [
            'group' => 'Default',
            'type' => 'varchar',
            'label' => 'Company Name',
            'note' => 'Customer Company Name',
            'input' => 'text',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required' => 0,
            'visible_on_front' => 1,
            'used_for_price_rules' => 0,
            'adminhtml_only' => 0,
            'sort_order' => 260,
        ]
    ],
    [
        'entity' => 'customer',
        'attr_code' => 'company_coc',
        'data' => [
            'group' => 'Default',
            'type' => 'varchar',
            'label' => 'Company CoC',
            'note' => 'Customer Company CoC',
            'input' => 'text',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required' => 0,
            'visible_on_front' => 1,
            'used_for_price_rules' => 0,
            'adminhtml_only' => 0,
            'sort_order' => 270,
        ]
    ],
];

foreach ($attributes as $attribute) {
    if (!$installer->getAttribute($attribute['entity'], $attribute['attr_code'])) {
        $installer->addAttribute($attribute['entity'], $attribute['attr_code'], $attribute['data']);

        $oAttribute = Mage::getSingleton('eav/config')->getAttribute($attribute['entity'], $attribute['attr_code']);
        $oAttribute->setData('used_in_forms', [
            'adminhtml_customer',
            'checkout_register',
            'customer_account_create',
            'customer_account_edit',
        ]);
        $oAttribute->save();
    }
}

$installer->endSetup();
