<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');
$query = 'UPDATE core_config_data SET value="~!@#$%^&*()_+=?/\":,<>\'" WHERE path="form_validations/form_fields/name_fields" AND scope_id="0"';
$writeConnection->query($query);
