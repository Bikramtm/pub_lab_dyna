<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/***************************************************************************
 * Make sure contact_id attribute is associated to the customer
 ***************************************************************************/

Mage::getSingleton('core/resource')->getConnection('core_write')->exec(
    'INSERT INTO customer_eav_attribute (attribute_id,is_visible) SELECT attribute_id,1 FROM eav_attribute WHERE attribute_code LIKE \'contact_id\' ON DUPLICATE KEY UPDATE is_visible=is_visible'
);