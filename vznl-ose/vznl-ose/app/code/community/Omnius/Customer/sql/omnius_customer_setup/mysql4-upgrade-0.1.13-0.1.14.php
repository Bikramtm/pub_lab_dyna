<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/***************************************************************************
 * Customer attributes
 ***************************************************************************/
/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();

$attributesInfo = array(
    'contractant_valid_until',
);

foreach ($attributesInfo as $attributeCode) {
    $attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $attributeCode);
    $attribute->setData(
        'input_filter', 'date'
    );
    $attribute->setData(
        'frontend_input', 'date'
    );
    $attribute->save();
}

$customerInstaller->endSetup();
