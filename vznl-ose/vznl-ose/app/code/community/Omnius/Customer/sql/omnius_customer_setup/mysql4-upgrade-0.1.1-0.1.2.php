<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/***************************************************************************
 * Customer attributes
 ***************************************************************************/
/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();

$attributesInfo = array(
    'privacy_market_research' => array(
        'label' => 'Market Research',
        'type' => 'int',
        'input' => 'boolean',
        'backend' => 'customer/attribute_backend_data_boolean',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'privacy_sales_activities' => array(
        'label' => 'Sales Activities',
        'type' => 'int',
        'input' => 'boolean',
        'backend' => 'customer/attribute_backend_data_boolean',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'privacy_number_information' => array(
        'label' => 'Number Information',
        'type' => 'int',
        'input' => 'boolean',
        'backend' => 'customer/attribute_backend_data_boolean',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'privacy_phone_books' => array(
        'label' => 'Phone Books',
        'type' => 'int',
        'input' => 'boolean',
        'backend' => 'customer/attribute_backend_data_boolean',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'privacy_disclosure_commercial' => array(
        'label' => 'Disclosure Commercial',
        'type' => 'int',
        'input' => 'boolean',
        'backend' => 'customer/attribute_backend_data_boolean',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'privacy_sms' => array(
        'label' => 'SMS',
        'type' => 'int',
        'input' => 'boolean',
        'backend' => 'customer/attribute_backend_data_boolean',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'privacy_email' => array(
        'label' => 'E-Mail',
        'type' => 'int',
        'input' => 'boolean',
        'backend' => 'customer/attribute_backend_data_boolean',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_legal_form' => array(
        'label' => 'Company Legal Form',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'privacy_mailing' => array(
        'label' => 'Mailing',
        'type' => 'int',
        'input' => 'boolean',
        'backend' => 'customer/attribute_backend_data_boolean',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'privacy_telemarketing' => array(
        'label' => 'Telemarketing',
        'type' => 'int',
        'input' => 'boolean',
        'backend' => 'customer/attribute_backend_data_boolean',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_coc' => array(
        'label' => 'Company CoC',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_vat_id' => array(
        'label' => 'Company VAT',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_legal_form' => array(
        'label' => 'Company Legal Form',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_country_id' => array(
        'label' => 'Company Country',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_postcode' => array(
        'label' => 'Company Zipcode',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_house_nr' => array(
        'label' => 'Company House Number',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_house_nr_addition' => array(
        'label' => 'Company House Number Addition',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_street' => array(
        'label' => 'Company Street',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_city' => array(
        'label' => 'Company City',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_date' => array(
        'label' => 'Company Creation Date',
        'type' => 'datetime',
        'input_filter' => 'date',
        'validate_rules' => 'a:1:{s:16:"input_validation";s:4:"date";}',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'contractant_dob' => array(
        'label' => 'Contractant birth date',
        'type' => 'datetime',
        'input_filter' => 'date',
        'validate_rules' => 'a:1:{s:16:"input_validation";s:4:"date";}',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'contractant_lastname' => array(
        'label' => 'Contractant Lastname',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'contractant_middlename' => array(
        'label' => 'Contractant middlename',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'contractant_firstname' => array(
        'label' => 'Contractant Firstname',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'contractant_prefix' => array(
        'label' => 'Contractant prefix',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
);

foreach ($attributesInfo as $attributeCode => $attributeParams) {
    $customerInstaller->addAttribute('customer', $attributeCode, $attributeParams);
}

foreach ($attributesInfo as $attributeCode => $attributeParams) {
    $attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $attributeCode);
    $attribute->setData(
        'used_in_forms',
        array(
            'adminhtml_customer',
            'checkout_register',
            'customer_account_create',
            'customer_account_edit',
        )
    );
    $attribute->save();
}

$customerInstaller->endSetup();
