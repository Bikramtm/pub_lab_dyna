<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Data
 */
class Omnius_Customer_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Get refund reasons for change from config
     *
     * @param null $store
     * @return array|bool
     */
    public function getRefundReasonsChangeOptions($store = null)
    {
        return $this->_prepareOptions(
            Mage::getStoreConfig('checkout/cart/refund_reason_change', $store)
        );
    }

    /**
     * Get refund reasons for cancel from config
     *
     * @param null $store
     * @return array|bool
     */
    public function getRefundReasonsCancelOptions($store = null)
    {
        return $this->_prepareOptions(
            Mage::getStoreConfig('checkout/cart/refund_reason_cancel', $store)
        );
    }

    /**
     * Unserialize and clear options
     *
     * @param string $options
     * @return array|bool
     */
    protected function _prepareCsvOptions($options)
    {
        $options = trim($options);
        if (empty($options)) {
            return false;
        }
        $result = array();
        $options = explode("\n", $options);
        foreach ($options as $value) {
            $values = str_getcsv(trim($value));
            if(!strlen($values[0])) {
                $values[0] = trim($values[1]);
            }
            $code = $this->escapeHtml(trim($values[0]));
            $result[$code] = $values;
        }
        return $result;
    }

    /**
     * Unserialize and clear options
     *
     * @param string $options
     * @return array|bool
     */
    protected function _prepareOptions($options)
    {
        $options = trim($options);
        if (empty($options)) {
            return false;
        }
        $result = array();
        $options = explode("\n", $options);
        foreach ($options as $value) {
            $values = explode(':', trim($value));
            $code = '';
            $label = '';

            if(strlen($values[0])) {
                $code = $this->escapeHtml(trim($values[0]));
                $label = $this->escapeHtml(trim($values[0]));
            }

            if(isset($values[1])) {
                $label = $this->escapeHtml(trim($values[1]));
            }

            $result[$code] = $this->__($label);
        }
        return $result;
    }

    /**
     * Get retainable ctns from provided quote's customer
     *
     * @param $quote
     * @return array
     */
    public function getRetainableCtns($quote)
    {
        $customer = $quote->getCustomer();

        if ( ! $customer) {
            return array(
                'error' => true,
                'message' => $this->__('No customer currently logged in.')
            );
        }

        try {
            $result = Mage::getResourceModel('ctn/ctn_collection')->addFieldToFilter('customer_id', $customer->getId())->toArray();
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            return array(
                'error'        => true,
                'message'      => $e->getMessage(),
            );
        }


        return array(
            'ctns' => $result,
            'retainable' => $result
        );
    }

    /**
     * @param $quote
     * @return array
     */
    public function filterRetainableCtns($quote)
    {
        $getRetainableCtns = $this->getRetainableCtns($quote);

        /*
        retainability_indicator
        Can be;
        - Retainable
        - Block Over Ruleable (= overuleable by backoffice)
        - Blocked (= not retainable)
        */
        // get all CTNs
        $ctns = isset($getRetainableCtns['ctns']) ? $getRetainableCtns['ctns'] : array();
        // retrieve all CTNS in a package
        $ctnInPackages = $this->getCTNsFromPackages($quote);
        // remove ctns already in a package
        $ctnsNotInPackages  = array_filter(
            $ctns,
            function ($ctn) use ($ctnInPackages) {
                return !in_array($ctn['ctn'], $ctnInPackages);
            }
        );
        // get retainable CTNs
        $retainable = isset($getRetainableCtns['retainable']) ? $getRetainableCtns['retainable'] : array();
        $allRetainableCount = count($retainable);

        // check if one-of-deal is active
        $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();
        if ($isOneOfDeal) {
            // if already on of deal return all ctns not in packages as retainable
            $oneOfDeal = false;
            $responseCTNs = $ctnsNotInPackages;
        } else {
            $retainableNotInPackages = array_filter(
                $retainable,
                function ($ctn) use ($ctnInPackages) {
                    return !in_array($ctn['ctn'], $ctnInPackages);
                }
            );
            if ($allRetainableCount != count($retainableNotInPackages)) {
                // there are retainables in a package, so no oneOff deal
                $oneOfDeal = false;
            } else {
                $oneOfDeal = Mage::getModel('ctn/ctn')->setCustomerId($quote->getCustomerId())->canActivateOneOffDeal(
                    count($ctns),       // all CTNs
                    $allRetainableCount
                );
            }
            $responseCTNs = $retainableNotInPackages;
        }

        return array(
            'creditProfileCheckResult' => isset($getRetainableCtns) ? $getRetainableCtns : [],
            'oneOffDeal' => $oneOfDeal,
            'ctns' => array_values($responseCTNs),
        );
    }

    /**
     * @param $quote
     * @return array
     * Return a list of ctns that are already in a package
     */
    private function getCTNsFromPackages($quote)
    {
        // get ctns already in one of the cart packages
        $ctnInPackages = array();
        $packages = $quote->getPackages();
        foreach ($packages as $package) {
            $packageCtns = isset($package['ctn']) ? [$package['ctn']] : [];
            if (count($packageCtns)) {
                $ctnInPackages = array_merge($ctnInPackages, $packageCtns);
            }
        }

        return $ctnInPackages;
    }

    /**
     * Convert number format to 31xxx
     *
     * @param $value
     * @return string
     */
    public function parseCtn($value)
    {
        if (strpos($value, '06') === 0) {
            $value = '31' . substr($value, 1);
        } elseif (strpos($value, '0031') === 0) {
            $value = '31' . substr($value, 4);
        } elseif (strpos($value, '+31') === 0) {
            $value = '31' . substr($value, 3);
        }

        return $value;
    }

    /**
     * Format the given post code
     * It will return an array with 2 formats
     *
     * @param $value string
     * @return array -- ['{number}{letters}', '{number} {letters}']
     */
    public function formatPostCode($value)
    {
        preg_match('/(\d+)/', $value, $numberMatches);
        preg_match('/([a-zA-Z]+)/', $value, $letterMatches);
        return array(
            ($numberMatches ? $numberMatches[0] : '') . ($letterMatches ? $letterMatches[0] : ''),
            ($numberMatches ? $numberMatches[0] : '') . ($letterMatches ? ' ' . $letterMatches[0] : ''),
        );
    }

    /**
     * Format the given house number.
     *
     * @param $value string
     * @return string
     */
    public function formatHouseNumber($value)
    {
        preg_match('/(\d+)/', $value, $numberMatches);
        preg_match('/([a-zA-Z]+)/', $value, $letterMatches);
        $toSearch = '';

        if (count($numberMatches) > 0) {
            $toSearch .= "%\n" . $numberMatches[0] . (count($letterMatches) > 0 ? "\n" . $letterMatches[0] : '') . '%';
        }

        return $toSearch;
    }

    /**
     * Retrieve adminhtml session model object
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

    /**
     * @param array $data
     * @param Mage_Core_Controller_Request_Http $request
     * @return Omnius_Customer_Model_Customer_Customer
     * @throws Mage_Customer_Exception
     */
    public function validateCustomerData($data, $request)
    {
        /** @var $customer Mage_Customer_Model_Customer */
        $customer = Mage::registry('current_customer');

        /** @var $customerForm Mage_Customer_Model_Form */
        $customerForm = Mage::getModel('customer/form');
        $customerForm->setEntity($customer)
            ->setFormCode('adminhtml_customer')
            ->ignoreInvisible(false)
        ;
        $formData = $customerForm->extractData($request, 'account');

        // Handle 'disable auto_group_change' attribute
        if (isset($formData['disable_auto_group_change'])) {
            $formData['disable_auto_group_change'] = empty($formData['disable_auto_group_change']) ? '0' : '1';
        }
        $this->validateCustomerFormData($customerForm, $formData);

        $customerForm->compactData($formData);
        // Unset template data
        if (isset($data['address']['_template_'])) {
            unset($data['address']['_template_']);
        }

        $modifiedAddresses = array();
        if (!empty($data['address'])) {
            /** @var $addressForm Mage_Customer_Model_Form */
            $addressForm = Mage::getModel('customer/form');
            $addressForm->setFormCode('adminhtml_customer_address')->ignoreInvisible(false);

            foreach (array_keys($data['address']) as $index) {
                $this->buildCustomerAddress($data, $request, $customer, $index, $addressForm, $modifiedAddresses);
            }
        }

        // Default billing and shipping
        if (isset($data['account']['default_billing'])) {
            $customer->setData('default_billing', $data['account']['default_billing']);
        }
        if (isset($data['account']['default_shipping'])) {
            $customer->setData('default_shipping', $data['account']['default_shipping']);
        }
        if (isset($data['account']['confirmation'])) {
            $customer->setData('confirmation', $data['account']['confirmation']);
        }

        // Set customer ban
        if (isset($data['account']['ban'])) {
            $customer->setData('ban', $data['account']['ban']);
        }

        // Mark not modified customer addresses for delete
        foreach ($customer->getAddressesCollection() as $customerAddress) {
            if ($customerAddress->getId() && !in_array($customerAddress->getId(), $modifiedAddresses)) {
                $customerAddress->setData('_deleted', true);
            }
        }

        if (Mage::getSingleton('admin/session')->isAllowed('customer/newsletter')
            && !$customer->getConfirmation()
        ) {
            $customer->setIsSubscribed(isset($data['subscription']));
        }

        if (isset($data['account']['sendemail_store_id'])) {
            $customer->setSendemailStoreId($data['account']['sendemail_store_id']);
        }

        return $customer;
    }

    /**
     * Generate a 15 chars id + suffix
     *
     * @return string
     */
    public function generateDummyEmail()
    {
        return uniqid(base_convert(rand(1, 1295), 10, 36)) . Omnius_Customer_Model_Customer_Customer::DUMMY_EMAIL_SUFFIX;
    }

    /**
     * Validate customer form data
     * If data is not valid, register the errors in session and throws exception
     *
     * @param Mage_Customer_Model_Form $customerForm
     * @param $formData
     * @throws Mage_Customer_Exception
     */
    protected function validateCustomerFormData($customerForm, $formData)
    {
        $errors = $customerForm->validateData($formData);
        if ($errors !== true) {
            foreach ($errors as $error) {
                $this->_getSession()->addError($error);
            }
            throw new Mage_Customer_Exception('Failed customer validation');
        }
    }

    /**
     * Validate customer form data
     * If data is not valid, register the errors in session and throws exception
     *
     * @param Mage_Customer_Model_Form $addressForm
     * @param $formData
     * @throws Mage_Customer_Exception
     */
    protected function validateAddressFormData($addressForm, $formData)
    {
        $errors = $addressForm->validateData($formData);
        if ($errors !== true) {
            foreach ($errors as $error) {
                $this->_getSession()->addError($error);
            }
            throw new Mage_Customer_Exception('Failed address validation');
        }
    }

    /**
     * @param $data
     * @param $request
     * @param $customer
     * @param $index
     * @param $addressForm
     * @param $modifiedAddresses
     * @throws Mage_Customer_Exception
     */
    protected function buildCustomerAddress($data, $request, $customer, $index, $addressForm, &$modifiedAddresses)
    {
        $address = $customer->getAddressItemById($index);
        if (!$address) {
            $address = Mage::getModel('customer/address');
        }

        $requestScope = sprintf('address/%s', $index);
        $formData = $addressForm->setEntity($address)
            ->extractData($request, $requestScope);

        // Set default billing and shipping flags to address
        $isDefaultBilling = isset($data['account']['default_billing']) && ($data['account']['default_billing'] == $index);
        $address->setIsDefaultBilling($isDefaultBilling);
        $isDefaultShipping = isset($data['account']['default_shipping']) && ($data['account']['default_shipping'] == $index);
        $address->setIsDefaultShipping($isDefaultShipping);

        $this->validateAddressFormData($addressForm, $formData);

        $addressForm->compactData($formData);

        // Set post_index for detect default billing and shipping addresses
        $address->setPostIndex($index);

        if ($address->getId()) {
            $modifiedAddresses[] = $address->getId();
        } else {
            $customer->addAddress($address);
        }
    }
}
