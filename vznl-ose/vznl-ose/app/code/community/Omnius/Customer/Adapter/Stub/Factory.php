<?php

class Omnius_Customer_Adapter_Stub_Factory implements Omnius_Customer_Adapter_FactoryInterface
{
    public static function create()
    {
        return new Omnius_Customer_Adapter_Stub_Adapter();
    }
}
