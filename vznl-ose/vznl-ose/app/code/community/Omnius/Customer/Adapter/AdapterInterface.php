<?php
interface Omnius_Customer_Adapter_AdapterInterface
{
	public function getProfile(Dyna_Customer_Model_Customer $customer) : Dyna_Customer_Model_Customer;

	public function getInstalledBase(Dyna_Customer_Model_Customer $customer);

}
