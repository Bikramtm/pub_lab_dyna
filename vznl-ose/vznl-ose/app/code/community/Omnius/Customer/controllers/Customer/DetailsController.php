<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Customer_Customer_DetailsController
 */
class Omnius_Customer_Customer_DetailsController extends Mage_Core_Controller_Front_Action
{
    const CTN_STATUS_ACTIVE = 'ACTIVE';

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Get current active quote instance
     *
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        return $this->_getCheckoutSession()->getQuote();
    }

    /**
     * @return bool
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function loadCustomerAction()
    {
        $parameters = new Varien_Object($this->getRequest()->getParams());
        /** @var Omnius_PriceRules_Helper_Data $priceRulesHelper */
        $priceRulesHelper = Mage::helper('pricerules');

        $customer = Mage::getModel('customer/customer')->load($parameters->getData('id'));
        if (!$customer->getId()) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array('message' => $this->__('Customer not found'))));

            return true;
        }

        $session = $this->_getCustomerSession();
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        if (!$customer->getIsBusiness() && $quote && $quote->containsBusinessSubscription()) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array(
                    'error' => true,
                    'message' => $this->__('It is not possible to select \'Private\' because the shopping cart contains a Business Subscription. Change the shopping cart or enter business customer information.')
                )));

            return true;
        } elseif ($parameters->getData('cart')) {
            $session->setLoadedCustomer($customer->getId());
            $priceRulesHelper->decrementAll($quote);
            $priceRulesHelper->reinitSalesRules($quote);

            $response = array(
                'error' => false,
                'message' => $this->__('Customer successfully loaded')
            );
        } else {
            $response = $this->loadCustomerOnSession($customer);
        }

        $quote->updateFields(array('suspect_fraud' => 0));

        $session->setCustomerInfoSync($customer->getIsProspect());
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));

        return true;
    }

    /**
     * @param $customer
     * @return array
     */
    protected function loadCustomerOnSession($customer)
    {
        /** @var Omnius_PriceRules_Helper_Data $priceRulesHelper */
        $priceRulesHelper = Mage::helper('pricerules');

        //remember current quote as this will need to be either removed or associated to the loaded customer
        /** @var Omnius_Checkout_Model_Sales_Quote $currentQuote */
        $currentQuote = Mage::getSingleton('checkout/cart')->getQuote();

        //if customer exists, load the entity to retrieve the campaigns, ctn records and other data
        $session = $this->_getCustomerSession();
        $session->setCustomer($customer);
        $session->unsBtwState();

        $unloadQuote = $this->getRequest()->get('unload_quote') === 'true' ? true : false;
        $customerId = $customer->getId();

        $billingAddressData = $customer->getDefaultBillingAddress() ? $customer->getDefaultBillingAddress()->getData() : array();
        $shippingAddressData = $customer->getDefaultShippingAddress() ? $customer->getDefaultShippingAddress()->getData() : array();

        if (!$unloadQuote) {
            // If quote is assigned to a customer, we clone it to the new customer removing Retention and any promos, else we just assign the quote to the customer
            if ($currentQuote->getCustomerId() && $currentQuote->getCustomerId() != $customerId) {
                Mage::helper('omnius_checkout')->cloneQuoteWithoutCustomerData($currentQuote);
            } else {
                $currentQuote->setCustomerId($customerId);
                $currentQuote->setBillingAddress(Mage::getModel('sales/quote_address')->setData($billingAddressData));
                $currentQuote->setShippingAddress(Mage::getModel('sales/quote_address')->setData($shippingAddressData));
                $currentQuote->getBillingAddress()->save();
                $currentQuote->getShippingAddress()->save();
                $currentQuote->save();

                Mage::getSingleton('checkout/cart')->setQuote($currentQuote);
                Mage::getSingleton('checkout/session')->setQuoteId($currentQuote->getId());
            }
        } else {
            $newQuote = Mage::getModel('sales/quote');
            $theQuote = Mage::getSingleton('checkout/cart')->getQuote();
            $newQuote->setIsActive(true)->save();
            $theQuote->setIsActive(false)->save();
            Mage::getSingleton('checkout/cart')->setQuote($newQuote);
            Mage::getSingleton('checkout/session')->setQuoteId($newQuote->getId());
        }

        $activePackageId = Mage::getSingleton('checkout/cart')->getQuote()->getActivePackageId();
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $priceRulesHelper->decrementAll($quote);
        $priceRulesHelper->reinitSalesRules($quote);

        return array(
            'sticker' => $this->getLayout()->createBlock('core/template')->setTemplate('customer/left_details.phtml')->setData('hideCounters', true)->toHtml(),
            'rightSidebar' => $this->loadLayout('page_three_columns')->getLayout()->getBlock('cart_sidebar')->setActivePackageId($activePackageId)->toHtml(),
            'saveCartModal' => $this->getLayout()->createBlock('core/template')->setTemplate('page/html/save_cart_modal.phtml')->toHtml(),
            'customerData' => $customer->getCustomData(),
            'addressData' => (! empty($billingAddressData)) ? $billingAddressData : '',
            'doAjaxUpdate' => ($this->getRequest()->get('doAjaxUpdate')) ? true : false,
        );
    }

    public function getStickerAction()
    {
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode(array(
                'sticker' => $this->getLayout()->createBlock('core/template')->setTemplate('customer/left_details.phtml')->setData('hideCounters', false)->toHtml(),
            )));
    }

    public function setCustomerSyncAction()
    {
        $this->_getCustomerSession()->setCustomerInfoSync(true);
    }

    /**
     * Move the quote from the existing customer to a new one
     */
    public function moveQuoteToNewCustomerAction()
    {
        $this->_getCustomerSession()->unsetCustomer();
        $this->_getCustomerSession()->unsOrderEdit();
        $this->_getCustomerSession()->unsProspect();
        $this->_getCheckoutSession()->unsIsEditMode();

        Mage::helper('omnius_checkout')->cloneQuoteWithoutCustomerData($this->getQuote());
        /** @var Omnius_PriceRules_Helper_Data $priceRulesHelper */
        $priceRulesHelper = Mage::helper('pricerules');
        $quote = $this->getQuote();
        $priceRulesHelper->decrementAll($quote);
        $priceRulesHelper->reinitSalesRules($quote);
        $this->_redirect('checkout/cart');

    }

    /**
     * Performs an update on the current logged in customer
     * by retrieving the information from the DA and mapping
     * it against the customer entity
     */
    public function poolCustomerDataAction()
    {
        $error = false;
        $message = 'Success';
        $customerData = null;

        if ($this->getRequest()->getPost('cart')) {
            /** @var Mage_Customer_Model_Customer $customer */
            $customerId = $this->_getCustomerSession()->getLoadedCustomer();
        } else {
            $customerId = $this->_getCustomerSession()->getCustomerId();
        }

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($customerId);

        $pin = $this->getRequest()->getPost('pin');
        if ($pin) {
            $customer->setPin($pin);
        }

        $this->_getCustomerSession()->setCustomerInfoSync(true);
        if ($customer->getId()) {
            /**
             * If customer is Unify, retrieve all inlife orders for its CTNs
             */
                        $customerData = Mage::helper('omnius_configurator')->toArray($customer);
            $customerData['ctn'] = Mage::helper('omnius_configurator')->toArray($customer->getCtn());
            $customerData['campaigns'] = Mage::helper('omnius_configurator')->toArray($customer->getCampaigns());
            $customerData['billing'] = Mage::helper('omnius_configurator')->toArray($customer->getPrimaryBillingAddress());
            $customerData['shipping'] = Mage::helper('omnius_configurator')->toArray($customer->getPrimaryShippingAddress());
            $customerData['dob'] = $customer->hasDob() ? date('Y-m-d', strtotime($customer->getDob())) : false;
            $customerData['customer_label'] = $customer->getCustomerLabel() ?: '';
            $customerData['dob_label'] = $customer->hasDob() ? Mage::helper('omnius_configurator')->__('Date of birth') : '';
            $customerData['gender'] = Mage::helper('omnius_configurator')->__($customer->getPrefix());
            $customerData['customer_value'] =
                Mage::helper('omnius_configurator')->__('Value') .
                ': ' .
                $customer->getResource()
                    ->getAttribute('customer_value')
                    ->getFrontend()
                    ->getValue($customer)
            ;
            $customerData['campaign'] = $this->__('Campaign') . ': ' . $customer->getResource()->getAttribute('campaign')->getFrontend()->getValue($customer);
            $customerData['ctn_overlay'] = $this->getLayout()->createBlock('core/template')
                ->setTemplate('customer/partials/ctn_details.phtml')
                ->setCustomer($customer)
                ->setSection('products-content')
                ->toHtml();

            unset($customerData['password_hash']);
        }

        $response = array(
            'error' => $error,
            'message' => $message,
            'data' => $customerData,
            'complete' => Mage::helper('omnius_checkout')->canCompleteOrder(),
        );

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    /**
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function unloadCustomerAction()
    {
        $this->_getCustomerSession()->unsetCustomer();
        $this->_getCustomerSession()->unsOrderEdit();
        $this->_getCustomerSession()->unsOrderEditMode();
        $this->_getCustomerSession()->unsNewEmail();
        $this->_getCustomerSession()->unsWelcomeMessage();
        $this->_getCustomerSession()->unsProspect();
        $this->_getCheckoutSession()->unsIsEditMode();
        $this->_getCheckoutSession()->setCustomerInfoSync(false);
        $this->_getCheckoutSession()->clear();

        $response = array(
            'sticker' => $this->getLayout()->createBlock('core/template')->setTemplate('customer/left_search.phtml')->toHtml(),
            'saveCartModal' => $this->getLayout()->createBlock('core/template')->setTemplate('page/html/save_cart_modal.phtml')->toHtml(),
        );
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    /**
     * Retrieve the list of ctns for a customer
     */
    public function showRetainableAction()
    {
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode(
                Mage::helper('omnius_customer')->filterRetainableCtns(Mage::getSingleton('checkout/session')->getQuote())
            ));
    }

    public function showCustomerPanelAction()
    {
        $customer_id = Mage::getSingleton('customer/session')->getId();
        $customer = Mage::getModel('customer/customer')->load($customer_id);

        if ($customer) {
            /** RFC-150641 - Customer screen redesigned, rest of screens remain the same */
            if ($this->getRequest()->getParam('section') == 'orders-content') {
                //Getting orders for this customer

                $response = array(
                    'error'         => false,
                    'customerPanel' => Mage::getBlockSingleton('page/html')
                        ->setTemplate('customer/partials/orders.phtml')
                        ->setCustomer($customer)
                        ->setSection($this->getRequest()->getParam('section'))
                        ->toHtml(),
                );

            } else {
                $response = array(
                    'error'         => false,
                    'customerPanel' => Mage::getBlockSingleton('page/html')
                        ->setTemplate('customer/partials/ctn_details.phtml')
                        ->setCustomer($customer)
                        ->setSection($this->getRequest()->getParam('section'))
                        ->toHtml(),
                );
            }
        } else {
            $response = array(
                'error'   => true,
                'message' => $this->__('Invalid customer or customer not found')
            );
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }


    /**
     * Creates an one of deal with all ctns packages
     */
    public function oneOffDealAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $nonRetainableCTNs = $this->getRequest()->getParam('nonRetainable', array());

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json');

        $customer_id = Mage::getSingleton('customer/session')->getId();
        /** @var Omnius_Customer_Model_Customer_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($customer_id);
        $customerCTNs = $customer->getCtn();
        Mage::getSingleton('checkout/cart')->getQuote()->clearQuote();
        $ctns = array();
        foreach ($customerCTNs as $ctn) {
            $additional = $ctn->getAdditional();
            $ctns[] = array(
                'ctn' => $ctn->getCtn(),
                'additional' => isset($additional['products']) ? array_reduce(
                        $additional['products'],
                        function ($p1, $p2) {
                            $separator = $p1 && $p2 ? ', ' : '';
                            return ($p1 ?: '') . $separator . ($p2 ? $p2['name']: '');
                        }
                    ) : '',
            );
        }

        $packageIds = array();

        foreach($ctns as $ctn) {
            $id = Mage::helper('omnius_configurator/cart')->initNewPackage(
                Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE,
                Omnius_Checkout_Model_Sales_Quote_Item::RETENTION,
                $ctn['ctn'],
                $ctn['additional'],
                array('retainable' => !in_array($ctn['ctn'], $nonRetainableCTNs))
            );
            $packageIds[$ctn['ctn']] = array(
                'id'         => $id,
                'additional' => $ctn['additional']
            );
        }

        Mage::getSingleton('checkout/cart')->getQuote()->save();

        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode(array(
                'rightSidebar' => $this->loadLayout('page_three_columns')->getLayout()->getBlock('cart_sidebar')->toHtml(),
                'packageIds' => $packageIds,
                'saleType'   => Omnius_Checkout_Model_Sales_Quote_Item::RETENTION,
                'type'       => Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE
            ))
        );
    }


    /**
     * Set the show BTW
     */
    public function setBTWStateAction()
    {
       if (!$this->getRequest()->isPost()) {
            return;
        }

        /** @var Omnius_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');

        $state = $this->getRequest()->getParam('state', 0);
        $session->setBtwState($state == 1 || $state == 'true');

        $this->getResponse()->setHeader('Content-Type', 'application/json')
            ->setBody(
                Mage::helper('core')->jsonEncode(
                    array(
                        'btw' => $session->getBtwState(),
                    )
                )
            );
    }
   /**
     * @param $result
     */
    protected function setJsonResponse($result)
    {
        $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(json_encode($result))
                ->setHttpResponseCode(200);
    }

    /*

    */
    public function loadAction()
    {
        $request = $this->getRequest()->getPost();
        if (!empty($request)) {
            try {
                $customer = Mage::getModel('customer/customer');
                $infoHelper = Mage::helper('dyna_customer/info');

                // Map search to model
                $requestData = json_decode($request['data'], true);
                $data = $customer->mapSearchFields($requestData);
                $customer->addData($data);

                // Retrieve customer profile
                $adapterFactoryName = $this->getAdapterInfo();
                $adapter = $adapterFactoryName::create();
                $customer = $adapter->getProfile($customer);

                // Save all returned results to session
                $session = $this->_getCustomerSession();
                $session->setCustomer($customer);
                $session->unsBtwState();
                $basicInfo = $infoHelper->buildCustomerDetails($customer, 1);
                $additionalCustomerInfo = $infoHelper->buildCustomerDetails($customer, 2);

                // Should run after the session is set. Specifically for bundle Info of the selected customer
                $responseEligibleBundles = [];
                $bundlesHelper = Mage::helper('dyna_bundles');
                $eligibleBundles = $bundlesHelper->getFrontEndEligibleBundles(true);
                $bundlesHelper->filterBundlesBySubscriptionStatus($eligibleBundles);
                $responseEligibleBundles['eligibleBundles'] = $eligibleBundles;

                /** @var Dyna_Customer_Helper_Data $customerHelper */
                $responseOpenOrders = [];
                $customerHelper = Mage::helper('dyna_customer');
                $responseOpenOrders['has_open_orders'] = [
                    strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::getMobilePackages()),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::getMobilePackages()),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::getFixedPackages()),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_LTE) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::getFixedPackages()),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::TYPE_CABLE_TV),

                ];

                $response = array(
                    'customerData' => array_merge($basicInfo, $additionalCustomerInfo, $responseEligibleBundles, $responseOpenOrders),
                    'totals' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml()
                );
            } catch(Exception $e) {
                Mage::logException($e);
                $response = array(
                    'error' => true,
                    'message' => $e->getMessage()
                );
            }
        } else {
            $response = array(
                'error'   => true,
                'message' => $this->__('Method should be a POST request')
            );

        }
        $this->setJsonResponse($response);
    }

    public function getAdapterInfo()
    {
        return Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/customer_search_adapter');
    }
}
