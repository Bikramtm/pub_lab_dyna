<?php

class Omnius_Customer_Model_System_Config_Adapter
{
    const STUB_ADAPTER = 'Omnius_Customer_Adapter_Stub_Factory';

    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::STUB_ADAPTER,
                'label' => 'Stub'
            )
        );
    }
}
