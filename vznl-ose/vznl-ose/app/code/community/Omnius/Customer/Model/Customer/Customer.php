<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Customer
 */
class Omnius_Customer_Model_Customer_Customer extends Mage_Customer_Model_Customer
{
    const TYPE_PERSONAL = 0;
    const TYPE_BUSINESS = 1;
    const TYPE_GOVERNMENT = 2;
    const LABEL_GEMINI = 'Gemini';
    const LABEL_UNIFY = 'Unify';
    const LABEL_IN_MIGRATION = 'In-Migration';
    const BAN_FIELD = 'ban';
    const DEFAULT_EMAIL_SUFFIX = '@omnius.com';
    const DUMMY_EMAIL_SUFFIX = '_geen@omnius.com';
    const IGNORED_VALUE_FOR_FIELDS = '@@@__DO_NOT_SAVE__@@@';

    /**
     * @return Varien_Data_Collection
     */
    public function getCampaigns()
    {
        return $this->getData('campaigns');
    }

    /**
     * Add campaign to current campaign collection
     *
     * @param Mage_Core_Model_Abstract $campaign
     */
    public function addCampaign(Mage_Core_Model_Abstract $campaign)
    {
        /** @var Varien_Data_Collection $campaigns */
        $campaigns = $this->getCampaigns();
        if (!in_array($campaign->getEntityId(), $this->getCampaigns()->getAllIds())) {
            $campaigns->addItem($campaign);
            $this->_hasDataChanges = true;
        }
    }

    /**
     * Check has the provided campaign
     *
     * @param $campaign
     * @return bool
     */
    public function hasCampaign($campaign)
    {
        $campaigns = $this->getCampaigns();
        return $campaigns ? $this->getCampaigns()->getItemById(
            is_numeric($campaign) ? $campaign : $campaign->getId()
        ) ? true : false : false;
    }

    /**
     * Get customer campaign by id
     *
     * @param $campaignId
     * @return Varien_Object
     */
    public function getCampaign($campaignId)
    {
        $campaigns = $this->getCampaigns();
        return $campaigns ? $campaigns->getItemById($campaignId) : null;
    }

    /**
     * Return the number of active ctns for this customer
     *
     * @return mixed
     */
    public function getCustomerCtnCount()
    {
        $ctns = Mage::getResourceModel('ctn/ctn_collection')->addFieldToFilter('customer_id', $this->getId())->addFieldToFilter('status', 'ACTIVE');
        return $ctns->count();
    }

    /**
     * The customer ctns.
     *
     * @param null $ctn
     * @param null $code
     * @return null
     */
    public function getCustomerCtn($ctn = null, $code = null)
    {
        $ctns = Mage::getResourceModel('ctn/ctn_collection')->addFieldToFilter('customer_id', $this->getId());
        if ($ctn) {
            $ctns->addFieldToFilter('ctn', $ctn);
        }
        if ($code) {
            $ctns->addFieldToFilter('code', $code);
        }
        $ctns->load();

        if ($ctns->count()) {
            return $ctns->getLastItem();
        }
        return null;
    }

    /**
     * Load entity by attribute
     *
     * @param Mage_Eav_Model_Entity_Attribute_Interface|integer|string|array $attribute
     * @param null|string|array $value
     * @param string $additionalAttributes
     * @return bool|Mage_Catalog_Model_Abstract
     */
    public function loadByAttribute($attribute, $value, $additionalAttributes = '*')
    {
        $customer = $this->getResourceCollection()
            ->addAttributeToSelect($additionalAttributes)
            ->addAttributeToFilter('website_id', Mage::app()->getStore()->getWebsiteId())
            ->addAttributeToFilter($attribute, $value)
            ->load()
            ->getFirstItem();

        return $customer ? $customer : false;
    }

    /**
     * Retrieve the list of saved shopping carts
     *
     * @param bool $includeOffers
     * @return array
     */
    public function getShoppingCarts($includeOffers = false)
    {
        /** @var Mage_Sales_Model_Resource_Quote_Collection $carts */
        $carts = Mage::getModel('sales/quote')->getCollection();
        $carts->addFieldToFilter('main_table.customer_id', Mage::getSingleton('customer/session')->getId()); //getSelectedCustomer
        $canSubmitRiskOrders = Mage::getSingleton('customer/session')->getAgent(true)->canSubmitRiskOrders();
        if ($includeOffers) {
            if ($canSubmitRiskOrders) {
                $carts->getSelect()->where(
                    '(-- saved shopping cart
                        main_table.cart_status = \'' . Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED . '\'
                          AND main_table.is_offer = \'0\'
                        ) 
                        OR (
                          -- offer
                          main_table.cart_status IS NULL 
                          AND main_table.is_offer = \'1\'
                          AND main_table.store_id = ' . Mage::app()->getStore()->getId() . '
                        ) 
                        OR (
                          -- risk
                          main_table.cart_status = \'' . Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK . '\'
                          AND main_table.is_offer = \'1\'
                        
                    )'
                );
            } else {
                $carts->getSelect()->where(
                    '(
                        -- saved shopping cart
                        main_table.cart_status = \'' . Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED . '\'
                          AND main_table.is_offer = \'0\'
                        ) 
                        OR (
                          -- offer
                          main_table.cart_status IS NULL 
                          AND main_table.is_offer = \'1\'
                          AND main_table.store_id = ' . Mage::app()->getStore()->getId() . '
                        )'
                );
            }
        } else {
            // Check if agent can also see the risk saved shopping carts
            if ($canSubmitRiskOrders) {
                $carts->addFieldToFilter('main_table.cart_status', array('in', array(Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED, Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK)));
            } else {
                $carts->addFieldToFilter('main_table.cart_status', Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED);
            }
        }
        $carts->setOrder('main_table.created_at', 'desc');

        $carts->getSelect()->joinLeft(array('orders' => 'sales_flat_order'), 'main_table.entity_id = orders.quote_id', array('orders.entity_id as order_id'));
        $carts->getSelect()->group('main_table.entity_id');

        $cartsArray = array();

        if ($includeOffers) {
            $offerLifetime = Mage::helper('omnius_checkout')->getOfferLifetime();
            $nowTimestamp = Mage::getModel('core/date')->timestamp();
        }
        foreach ($carts as $cart) {
            $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $cart->getId());
            if (count($packages) == 0) {
                continue;
            }
            if (!$cart->getOrderId()) {
                if ($cart->getIsOffer()) {
                    if (!$canSubmitRiskOrders && $cart->getCartStatus() == Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK) {
                        // Ensure no offer with risk is shown
                        continue;
                    }
                    $quoteTimestamp = Mage::getModel('core/date')->timestamp(strtotime($cart->getCreatedAt()));
                    $quoteTimestamp = strtotime("+$offerLifetime days", $quoteTimestamp);
                    $quoteTimestamp = strtotime(date("Y-m-d 23:59:59", $quoteTimestamp));
                    $days_between = ceil(($quoteTimestamp - $nowTimestamp) / 86400);
                    $cart->setOfferExpireDays($days_between);
                }

                $cartsArray[] = $cart;
            }
        }

        return $cartsArray;
    }

    //TODO - implement
    /**
     * @return array
     */
    public function getCheckouts()
    {
        return array(1, 2, 3, 4);
    }

    //TODO - implement
    /**
     * Get all sales orders for current customer
     *
     * @return mixed
     */
    public function getOrders()
    {
        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->setOrder('created_at', 'desc')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId()); //getSelectedCustomer

        return $orders;
    }

    /**
     * Get all sales orders for current customer
     *
     * @return mixed
     */
    public function getOrdersGrouped()
    {
        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->setOrder('created_at', 'desc')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId()); //getSelectedCustomer

        return $orders;
    }

    //TODO - implement
    /**
     * @return bool
     */
    public function isOverdue()
    {
        return true;
    }

    //TODO - implement and remove dummy data
    /**
     * @return array
     */
    public function getMemos()
    {
        $memos = array();
        for ($i = 0; $i < 10; $i++) {
            $memos[$i] = array(
                'title' => 'Memo test ' . $i,
                'name' => 'James Doe',
                'date' => '21-12-2013 14:22:12',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut felis tellus, faucibus non est sit amet, lacinia varius est'
            );
        }

        return $memos;
    }

    //TODO - implement and remove dummy data
    /**
     * @return array
     */
    public function getMemoCases()
    {
        $memos = array();
        for ($i = 0; $i < 10; $i++) {
            $memos[$i] = array(
                'title' => 'Case titel',
                'name' => 'James Doe',
                'date' => '21-12-2013 14:22:12',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut felis tellus, faucibus non est sit amet, lacinia varius est'
            );
        }

        return $memos;
    }

    /**
     * Return customer data
     * Formats all dates fields
     *
     * @return mixed
     */
    public function getCustomData()
    {
        $data = $this->getData();
        if (isset($data['dob']) && !empty($data['dob'])) {
            $data['dob'] = date('d-m-Y', strtotime($data['dob']));
        }

        if (isset($data['contractant_dob']) && !empty($data['contractant_dob'])) {
            if (!is_numeric($data['contractant_dob'])) {
                $data['contractant_dob'] = strtotime($data['contractant_dob']);
            }
            $data['contractant_dob'] = date('d-m-Y', $data['contractant_dob']);
        }
        if (isset($data['valid_until']) && !empty($data['valid_until'])) {
            $data['valid_until'] = date('d-m-Y', strtotime($data['valid_until']));
        }
        if (isset($data['contractant_valid_until']) && !empty($data['contractant_valid_until'])) {
            $data['contractant_valid_until'] = date('d-m-Y', strtotime($data['contractant_valid_until']));
        }
        if (isset($data['company_date']) && !empty($data['company_date'])) {
            $data['company_date'] = date('d-m-Y', strtotime($data['company_date']));
        }

        $data['email'] = (isset($data['additional_email']) && $data['additional_email']) ? $data['additional_email'] : '';

        // Default setting is non business customer
        if (!isset($data['is_business'])) {
            $data['is_business'] = 0;
        }
        $fields = array(
            'entity_id' => '',
            'email' => '',
            'prefix' => '',
            'firstname' => '',
            'middlename' => '',
            'lastname' => '',
            'customer_label' => '',
            'suffix' => '',
            'id_number' => '',
            'id_type' => '',
            'issuing_country' => '',
            'account_holder' => '',
            'account_no' => '',
            'dob' => '',
            'valid_until' => '',
            'gender' => '',
            'is_business' => '',
            'customer_ctn' => '',
            'ban' => '',
            'company_name' => '',
            'company_coc' => '', //kwk
            'company_postcode' => '',
            'company_house_nr' => '',
            'company_house_nr_addition' => '',
            'company_street' => '',
            'company_date' => '',
            'company_city' => '',
            'company_legal_form' => '',
            'company_vat_id' => '', //bwb

            'contractant_lastname' => '',
            'contractant_middlename' => '',
            'contractant_firstname' => '',
            'contractant_dob' => '',
            'contractant_prefix' => '',
            'contractant_gender' => '',
            'contractant_id_type' => '',
            'contractant_id_number' => '',
            'contractant_valid_until' => '',
            'contractant_issuing_country' => '',
            'privacy_market_research' => '',
            'privacy_sales_activities' => '',
            'privacy_number_information' => '',
            'privacy_phone_books' => '',
            'privacy_sms' => '',
            'privacy_disclosure_commercial' => '',
            'privacy_email' => '',
            'privacy_mailing' => '',
            'privacy_telemarketing' => '',
            'additional_email' => '',
            'additional_fax' => '',
            'additional_telephone' => '',
        );

        $data = array_intersect_key($data, $fields);
        return $data;
    }

    /**
     * @return bool|string
     *
     * Format date prior to displaying it
     */
    public function getDob()
    {
        if (parent::getDob()) {
            return date("d-m-Y", strtotime(parent::getDob()));
        } else {
            return parent::getDob();
        }
    }

    /**
     * @return bool|string
     *
     * Format date prior to displaying it
     */
    public function getContractantDob()
    {
        if (parent::getContractantDob()) {
            return date("d-m-Y", strtotime(parent::getContractantDob()));
        } else {
            return parent::getContractantDob();
        }
    }

    /**
     * Get customers for dropdown
     *
     * @return array -- [-1 => 'Please select a customer..', [label => {productName}, value => {$productId}], ...]
     */
    public function getCustomersForDropdown()
    {
        $productsCollection = $this->getCollection()
            ->addAttributeToSelect('firstname')
            ->addAttributeToSelect('lastname');
        $selectable_collection = array(-1 => 'Please select a customer..');
        foreach ($productsCollection as $product) {
            $selectable_collection[] = array(
                'label' => $product->getFirstname() . $product->getLastname(),
                'value' => $product->getId()
            );
        }
        return $selectable_collection;
    }

    /**
     * Get the correspondance email
     *
     * @param $orderOrQuote Omnius_Checkout_Model_Sales_Quote|Omnius_Checkout_Model_Sales_Order
     * @return mixed
     */
    public function getCorrespondanceEmail($orderOrQuote = null)
    {
        $coresspondenceEmail = null;
        if ($orderOrQuote !== null && $orderOrQuote->getId()){
            $coresspondenceEmail = $orderOrQuote->getCorrespondenceEmail();
        }

        if (!empty($coresspondenceEmail)) {
            return $orderOrQuote->getCorrespondenceEmail();
        } else {
            $emailAddressArr = explode(';', $this->getData('additional_email'));
            return $emailAddressArr[0];
        }
    }

    /**
     * Get pim
     * This is always converted to uppercase
     *
     * @return string
     */
    public function getPin()
    {
        return strtoupper(parent::getPin());
    }

    /**
     * Return the customer id number
     *
     * @param bool $customFormat
     * @return mixed
     */
    public function getIdNumber($customFormat = false)
    {
        if (!$customFormat) {
            return parent::getIdNumber();
        }

        return Mage::helper('omnius_validators')->stripNLD(parent::getIdNumber());
    }

    /**
     * Get customer salutation based on gender
     *
     * @return string
     */
    public function getSalutation()
    {
        $gender = ($this->getIsBusiness() && $this->getContractantGender() != null) ? $this->getContractantGender() : $this->getGender();
        return Mage::helper('omnius_checkout')->__(($gender == 2 ? 'madam' : 'mister'));
    }

    /**
     * Get customer appelative by possession and type (business or normal customer)
     *
     * @param bool $possession
     * @return string
     */
    public function getAppelative($possession = true)
    {
        return Mage::helper('omnius_checkout')->__(
            $possession
                ? ($this->getIsBusiness() ? 'your_business' : 'your_consumer')
                : ($this->getIsBusiness() ? 'you_business' : 'you_consumer')
        );
    }

    /**
     * Check if customer is in migration
     *
     * @return bool
     */
    public function isInMigration()
    {
        return $this->getCustomerLabel() == self::LABEL_IN_MIGRATION;
    }

    /**
     * Set customer ctn
     *
     * @param $currentCtns
     */
    public function setCtn($currentCtns)
    {
        $changes = false;
        /** @var Varien_Data_Collection $oldCtns */
        $oldCtns = $this->getData('ctn');
        $newCtns = new Varien_Data_Collection();
        if ($currentCtns instanceof Varien_Data_Collection) {
            // compare collections
            $changes = $this->setCollectionCtns($currentCtns, $oldCtns, $newCtns);
        } elseif (is_array($currentCtns)) {
            $changes = $this->setArrayCtns($currentCtns, $oldCtns, $newCtns);
        }
        unset($currentCtns);
        unset($oldCtns);

        // Only if changes are found set the new ctns.
        if ($changes) {
            $this->setData('ctn', $newCtns);
        }
    }

    /**
     * Check if the customer or the backend are set to gemini
     *
     * @return bool
     */
    public function getIsGemini()
    {
        if ($this->getId()) {
            $result = strtolower($this->getCustomerLabel()) == strtolower(self::LABEL_GEMINI);
        } else {
            $result = strtolower(Mage::getModel('omnius_customer/system_config_backend')->getSelected()) == strtolower(self::LABEL_GEMINI);
        }

        return $result;
    }

    /**
     * Set the collections of ctns
     * Returns true if any current ctn has changed, false otherwise
     *
     * @param $currentCtns
     * @param $oldCtns
     * @param $newCtns
     * @return bool
     */
    protected function setCollectionCtns($currentCtns, $oldCtns, $newCtns)
    {
        $changes = false;
        foreach ($oldCtns as $id => $oldCtn) {
            if ($currentCtn = $currentCtns->getItemById($id)) {
                if ($oldCtn->getChecksum() !== $currentCtn->getChecksum()) {
                    $changes = true;
                    $newCtns->addItem(Mage::getModel('ctn/ctn')->setData($currentCtn->getData())->save());
                } else {
                    $newCtns->addItem($oldCtn);
                }
                $currentCtns->removeItemByKey($id);
            } else {
                $oldCtn->delete();
            }
        }
        if (count($currentCtns)) {
            foreach ($currentCtns as $newCtn) {
                $newCtns->addItem(Mage::getModel('ctn/ctn')->setData($newCtn->getData())->setCustomerId($this->getId())->save());
            }
        }

        return $changes;
    }

    /**
     * Set the arrays of ctns
     * Returns true if any current ctn has changed, false otherwise
     *
     * @param $currentCtns
     * @param $oldCtns
     * @param $newCtns
     * @return array
     */
    protected function setArrayCtns($currentCtns, $oldCtns, $newCtns)
    {
        $changes = false;
        if ($oldCtns) {
            foreach ($oldCtns as $oldCtn) {
                if (isset($currentCtns[$oldCtn->getCtn()])) {
                    if ($oldCtn->getChecksum() !== $currentCtns[$oldCtn->getCtn()]->getChecksum()) {
                        $changes = true;
                        $newCtns->addItem(Mage::getModel('ctn/ctn')->setData($currentCtns[$oldCtn->getCtn()]->getData())->setId($oldCtn->getId())->save());
                    } else {
                        $newCtns->addItem($oldCtn);
                    }
                    unset($currentCtns[$oldCtn->getCtn()]);
                } else {
                    $changes = true;
                    // Delete ctns that were removed
                    $oldCtn->delete();
                }
            }
        }

        // If new ctns are found
        if (count($currentCtns)) {
            $changes = true;
            foreach ($currentCtns as $newCtn) {
                $newCtns->addItem(Mage::getModel('ctn/ctn')->setData($newCtn->getData())->save());
            }

            return array($currentCtns, $changes);
        }

        return $changes;
    }

    /**
     * mapping the search response fields to the corresponding ones in OSE
     * @param array $requestData
     * @return array
     */
    public function mapSearchFields(array $requestData) : array
    {
        $data = [
            'customer_number' => $requestData['_accountNumber'],
            'firstname' => $this->mapCustomerFirstName($requestData['_party']),
            'lastname' => $this->mapCustomerLastName($requestData['_party']),
            'dob' => $requestData['_party']['_dateOfBirth'] ?? "",
            'customer_status' => $requestData['_accountStatus'] === 'active' ? true : false,
            'account_category' => $requestData['_lineOfBusiness'] === 'mobile' ? 'KIAS' : $requestData['_lineOfBusiness'],
            'phone' => $requestData['_party']['_groupedDetails']['phone'][0]['number'] ?? "",
            'email' => $requestData['_party']['_groupedDetails']['email'][0]['emailAddress'] ?? ""
        ];
        return $data;
    }

    /**
     * @param array $party
     * @return string
     */
    protected function mapCustomerFirstName(array $party) : string
    {
        if (isset($party['_name']) && $party['_name'] !="") {
           $customerContactModel['PartyName']['Name'] = $party['_name'];
        } else {
           $customerContactModel['PartyName']['Name'] = $party['_firstName'];
        }

        return $customerContactModel['PartyName']['Name'];
    }

    /**
     * @param array $party
     * @return string
     */
    protected function mapCustomerLastName(array $party) : string
    {
        if (isset($party['_name']) && $party['_name'] !="") {
           $customerContactModel['PartyName']['Name'] = $party['_name'];
        } else {
           $customerContactModel['PartyName']['Name'] = $party['_lastName'];
        }

        return $customerContactModel['PartyName']['Name'];
    }
}
