<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Customer_Model_Address
 */
class Omnius_Customer_Model_Address extends Mage_Customer_Model_Address
{

    /**
     * Overrides the validator of the abstract class.
     */
    protected function _basicCheck()
    {
        Mage::helper('omnius_validators')->validateAddressData($this);
    }

    /**
     * Get trimmed streen name
     *
     * @return string
     */
    public function getStreetName()
    {
        return trim($this->getStreet1());
    }

    /**
     * Get trimmed house no.
     *
     * @return string
     */
    public function getHouseNo()
    {
        return trim($this->getStreet2());
    }

    /**
     * Get trimmed house no. addition
     *
     * @return string
     */
    public function getHouseAdd()
    {
        return trim($this->getStreet3());
    }

    /**
     * Get customer data
     *
     * @return array
     */
    public function getCustomData()
    {
        $data = $this->getData();
        $data['street_name'] = $this->getStreetName();
        $data['houseno'] = $this->getHouseNo();
        $data['addition'] = $this->getHouseAdd();

        return $data;
    }

    /**
     * Returns the value from the quote or the default value from the backend
     *
     * @param string $field
     * @param null $section
     * @return mixed|null
     */
    public function getDefaultValueForField($field, $section = null)
    {
        if ($this->getData($field) != null) {
            return $this->getData($field);
        }

        return Mage::helper('omnius_checkout')->getDefaultValueForField($field, $section);
    }
}