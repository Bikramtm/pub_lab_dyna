<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Customer_Model_Session
 */
class Omnius_Customer_Model_Session extends Mage_Customer_Model_Session
{
    /**
     * Unsets the customer session
     */
    public function unsetCustomer()
    {
        $this->_customer = null;
        $this->setId(null);
        return true;
    }

    /**
     * Get superorder id from order edit
     *
     * @return mixed
     */
    public function getSuperOrderId()
    {
        $superQuoteId = $this->getOrderEdit();

        return Mage::getModel('sales/quote')->load($superQuoteId)->getSuperOrderEditId();
    }

    /**
     * Get superorder from order edit
     *
     * @return Omnius_Superorder_Model_Superorder
     */
    public function getSuperOrder()
    {
        $superOrderId = $this->getSuperOrderId();
        $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);
        return $superOrder;
    }

    /**
     * Check if the customer is business
     * @return bool
     */
    public function isBusinessCustomer()
    {
        return (bool) ($this->getCustomer() && $this->getCustomer()->getIsBusiness());
    }

    /**
     * Check if the price should be with BTW
     * @return bool
     */
    public function showPriceWithBtw()
    {
        return $this->hasBtwState() ? $this->getBtwState() : !$this->isBusinessCustomer();
    }

    /**
     * @return mixed
     */
    public function getProspect()
    {
        return $this->getCustomer()->getIsProspect();
    }

    /**
     * Return the logged in agent, or agent that created the superorder if webshop order edited on telesales
     * 
     * @return Dyna_Agent_Model_Agent
     */
    public function getAgent($test = false) {
        return parent::getAgent();
    }

}