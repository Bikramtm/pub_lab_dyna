<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'Mage/Adminhtml/controllers/Promo/QuoteController.php';

class Omnius_PriceRules_Adminhtml_Promo_QuoteController extends Mage_Adminhtml_Promo_QuoteController
{
    /**
     * Import action for price rules
     */
    public function importAction()
    {
        $this->_initAction()
            ->_title($this->__('Import Rules'));

        $this->_addContent($this->getLayout()->createBlock('pricerules/adminhtml_promo_quote_import'));

        $this->renderLayout();
    }

    /**
     * Mass import for price rules records
     */
    public function massImportAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
                    try {
                        $path = Mage::getBaseDir('media') . DS . 'import' . DS . 'coupon' . DS; 
                        // File name
                        $fname = $_FILES['file']['name'];
                        // Load class
                        $uploader = new Varien_File_Uploader('file');
                        // Allowed extension for file
                        $uploader->setAllowedExtensions(array('CSV', 'csv'));
                        // For creating the directory if not exists
                        $uploader->setAllowCreateFolders(true); 
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $uploader->save($path, $fname);

                        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pricerules')->__("File has been uploaded successfully."));
                        $this->_redirect('*/*/import');

                        return;

                    } catch (Exception $e) {
                        $fileType = "Invalid file format";
                    }
                }

                if (isset($fileType) && ($fileType == "Invalid file format")) {
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('foundation')->__($fname . " Invalid file format"));
                    $this->_redirect('*/*/');

                    return;
                }

                $result = Mage::getSingleton('pricerules/coupon')->import($data['file']);

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pricerules')->__('%s rule(s) imported successfully.<br/> %s rule(s) already existed.',
                    $result['count'], $result['countNotImpt']));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/import');
            }

        }
    }

    /**
     * Export action to export records in a csv file
     */
    public function exportCsvAction()
    {
        $fileName = 'export_coupon.csv';
        $content = $this->getLayout()
            ->createBlock('pricerules/adminhtml_promo_quote_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    /**
     * Export action to export records in a xml file
     */
    public function exportXmlAction()
    {
        $fileName = 'coupon.xml';
        $content = $this->getLayout()
            ->createBlock('pricerules/adminhtml_promo_quote_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    /**
     * @param $fileName
     * @param $content
     * @param string $contentType
     */
    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
