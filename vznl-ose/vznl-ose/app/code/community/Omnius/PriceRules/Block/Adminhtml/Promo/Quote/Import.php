<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_PriceRules_Block_Adminhtml_Promo_Quote_Import extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected $_buttomLabel = '';

    /**
     * Omnius_PriceRules_Block_Adminhtml_Promo_Quote_Import constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_promo_quote';
        $this->_mode = 'import';
        $this->_blockGroup = 'pricerules';

        $this->getDictionaries();

        $this->_updateButton('save', 'label', $this->getLabel());

        return $this;
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('pricerules')->__('Import Rules');
    }

    protected function getDictionaries()
    {
        $values = array();
        $path = Mage::getBaseDir('media') . DS . 'import' . DS . 'coupon' . DS;
        if (file_exists($path)) {
            if ($handle = opendir($path)) {
                while (false !== ($entry = readdir($handle))) {
                    if ((substr($entry, 0, 1) != '.') && (!is_dir($path . $entry))) {
                        Mage::helper('pricerules')->getValues($entry, $path, $values);
                    }
                }
                closedir($handle);
            }
            if (count($values) == 0) {
                $this->_buttomLabel = Mage::helper('pricerules')->__('Save & Continue');

                return;
            }
        } else {
            $this->_buttomLabel = Mage::helper('pricerules')->__('Save & Continue');

            return;
        }
        $this->_buttomLabel = Mage::helper('pricerules')->__('Import Rules');

        return;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->_buttomLabel;
    }
}
