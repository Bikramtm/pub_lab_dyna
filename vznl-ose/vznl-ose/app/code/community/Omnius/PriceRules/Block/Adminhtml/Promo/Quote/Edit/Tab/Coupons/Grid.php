<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Override base block to remove the filter for auto-generated coupons -> times_used as it was generating times_used 1 on the salesrule on each save.
 * Class Omnius_PriceRules_Block_Adminhtml_Promo_Quote_Edit_Tab_Coupons_Grid
 */
class Omnius_PriceRules_Block_Adminhtml_Promo_Quote_Edit_Tab_Coupons_Grid extends Mage_Adminhtml_Block_Promo_Quote_Edit_Tab_Coupons_Grid
{
    /**
     * Define grid columns
     *
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $timesUsedData = $this->_columns['times_used']->getData();
        $this->removeColumn('times_used');
        $timesUsedData['filter'] = false;
        $this->addColumn('times_used', $timesUsedData);
    }
}