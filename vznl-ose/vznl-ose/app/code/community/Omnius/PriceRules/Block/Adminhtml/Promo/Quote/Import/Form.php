<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_PriceRules_Block_Adminhtml_Promo_Quote_Import_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
        ));

        $fieldset = $form->addFieldset('general', array('legend' => Mage::helper('pricerules')->__('Import')));

        $dictionaries = $this->getDictionaries();
        if (!is_array($dictionaries)) {
            $fieldset->addField('label', 'label', array(
                'value' => '',
                'note' => $dictionaries,
            ));
            $fieldset->addField('file', 'file', array(
                'name' => 'file',
                'label' => Mage::helper('pricerules')->__('File'),
                'value' => '',
                'note' => 'Please upload CSV file.',
            ));
        } else {
            $fieldset->addField('file', 'select', array(
                'name' => 'file',
                'label' => Mage::helper('pricerules')->__('File'),
                'required' => true,
                'values' => $dictionaries,
            ));
        }


        $form->setAction($this->getUrl('*/*/massImport'));
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @return array|string
     */
    protected function getDictionaries()
    {
        $values = array();
        $path = Mage::getBaseDir('media') . DS . 'import' . DS . 'coupon' . DS;
        if (file_exists($path)) {
            if ($handle = opendir($path)) {
                while (false !== ($entry = readdir($handle))) {
                    if ((substr($entry, 0, 1) != '.') && ! is_dir($path . $entry)) {
                        Mage::helper('pricerules')->getValues($entry, $path, $values);
                    }
                }
                closedir($handle);
            }
            if (count($values) == 0) {
                $values = "Can't find any files in folder $path";
            }
        } else {
            $values = "Can't find folder $path";
        }

        return $values;
    }

    /**
     * Omnius_PriceRules_Block_Adminhtml_Promo_Quote_Import_Form constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('promo_quote_form');
        $this->setTitle(Mage::helper('salesrule')->__('Rule Information'));
    }
}
