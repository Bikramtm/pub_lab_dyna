<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_Validator
 */
class Omnius_PriceRules_Model_Validator extends Mage_SalesRule_Model_Validator
{

    /** @var array */
    protected $_promoRules = array();

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /** @var null/int $_activePackageId */
    private $_activePackage = null;

    protected static $_resultCache = [];

    protected $_removeCoupon = [];

    protected $_validated = [];

    /**
     * Coupons applied per package.
     * @var array
     */
    public $couponsApplied = array();

    /**
     * @var Omnius_PriceRules_Helper_Data
     */
    private $priceRulesHelper = null;

    /**
     * Init validator
     * Init process load collection of rules for specific website,
     * customer group and coupon code
     *
     * @param   int $websiteId
     * @param   int $customerGroupId
     * @param   string $couponCode
     * @param   Omnius_Package_Model_Mysql4_Package_Collection[] $packages
     * @param   Omnius_Checkout_Model_Sales_Quote_Address $address
     * @return  Mage_SalesRule_Model_Validator
     */
    public function init($websiteId, $customerGroupId, $couponCode, $packages = [], $address = null)
    {
        if (!$address || count($packages) == 0) {
            return $this;
        }

        $couponsPerPackage = $address->getQuote()->getCouponsPerPackage();
        foreach ($packages as $package) {
            $couponCode = (is_array($couponsPerPackage) && isset($couponsPerPackage[$package->getPackageId()])) ? $couponsPerPackage[$package->getPackageId()] : $package->getCoupon();
            $key = $websiteId . '_' . $customerGroupId . '_' . $couponCode . '_' . $package->getPackageId();
            if (!isset($this->_rules[$key])) {
                $this->_rules[$key] = [];
                $allRules = Mage::helper('rulematch')
                    ->reset()
                    ->setActivePackage($package)
                    ->getApplicableRules(true, $couponCode);

                if (!count($allRules)) {
                    continue;
                }

                //Set the active package id, as some validators need to be aware of the current package.
                $address->getQuote()->setActivePackageId($package->getPackageId());

                /** @var Omnius_Checkout_Model_SalesRule_Rule $rule */
                foreach ($allRules as $rule) {
                    if ($this->_canProcessRule($rule, $address, $package->getPackageId())) {
                        $this->_rules[$key][] = $rule;
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Returns available price rules for current package in collect totals.
     * @return array
     */
    protected function _getRules()
    {
        $key = $this->getWebsiteId() . '_' . $this->getCustomerGroupId() . '_' . $this->getCouponCode() . '_' . $this->getActivePackage()->getPackageId();
        return isset($this->_rules[$key]) ? $this->_rules[$key] : [];
    }

    /**
     * @return Omnius_PriceRules_Helper_Data
     */
    public function getPriceRulesHelper()
    {
        if ($this->priceRulesHelper === null) {
            $this->priceRulesHelper = Mage::helper('pricerules');
        }

        return $this->priceRulesHelper;
    }

    /**
     * @param $activePackage
     * @return $this
     */
    public function setActivePackage($activePackage)
    {
        $this->_activePackage = $activePackage;

        return $this;
    }

    /**
     * @return Omnius_Package_Model_Package
     */
    public function getActivePackage()
    {
        return $this->_activePackage;
    }

    /**
     * Public accessor for the _canProcessRule method.
     * @param Omnius_Checkout_Model_SalesRule_Rule $rule
     * @param Omnius_Checkout_Model_Sales_Quote_Address $address
     * @param int $activePackageId
     * @return bool
     *
     * Public method for canProcessRule
     */
    public function canProcessRules($rule, $address, $activePackageId)
    {
        return $this->_canProcessRule($rule, $address, $activePackageId);
    }

    /**
     * Check if rule can be applied for specific address/quote/customer
     *
     * @param   Mage_SalesRule_Model_Rule $rule
     * @param   Mage_Sales_Model_Quote_Address $address
     * @param   int $packageId
     * @return  bool
     */
    protected function _canProcessRule($rule, $address, $packageId = null)
    {
        $existingRules = [];
        foreach ($address->getQuote()->getAllItems() as $item) {
            if ($item->getPackageId() == $packageId) {
                $existingRules = array_merge($this->getPriceRulesHelper()->explodeTrimmed($item->getRemoveRuleId() ? $item->getRemoveRuleId() : $item->getAppliedRuleIds()), $existingRules);
            }
        }

        $resultCacheKey = sprintf('%d_%d_%d', $rule->getId(), $address->getId(), $packageId);
        if (isset(self::$_resultCache[$resultCacheKey])) {
            return self::$_resultCache[$resultCacheKey];
        }

        /**
         * Check if the rule is available
         */
        if (($rule->getFromDate() && strtotime($rule->getFromDate()) > time())
            || ($rule->getToDate() && strtotime($rule->getToDate()) + 86400 < time())
        ) {
            self::$_resultCache[$resultCacheKey] = false;
            return false;
        }

        /**
         * check per coupon usage limit
         */
        if ($rule->getCouponType() != Mage_SalesRule_Model_Rule::COUPON_TYPE_NO_COUPON) {
            $couponCode = $this->getCouponCode();
            if (strlen($couponCode)) {
                $coupon = Mage::getModel('salesrule/coupon');
                $coupon->load($couponCode, 'code');

                if ($coupon->getId() && $this->getActivePackage()->getCoupon() != $couponCode) {
                    // check entire usage limit
                    if ($coupon->getUsageLimit() && $coupon->getTimesUsed() >= $coupon->getUsageLimit()) {
                        self::$_resultCache[$resultCacheKey] = false;
                        return false;
                    }
                    // check per customer usage limit
                    $customerId = $address->getCustomerId();
                    if ($customerId && $coupon->getUsagePerCustomer()) {
                        $couponUsage = new Varien_Object();
                        Mage::getResourceModel('salesrule/coupon_usage')->loadByCustomerCoupon($couponUsage, $customerId, $coupon->getId());
                        if ($couponUsage->getCouponId() &&
                            $couponUsage->getTimesUsed() >= $coupon->getUsagePerCustomer()
                        ) {
                            self::$_resultCache[$resultCacheKey] = false;
                            return false;
                        }
                    }
                }
            }
        }

        /**
         * check per rule usage limit
         */
        $ruleId = $rule->getId();
        if ($ruleId && $rule->getUsesPerCustomer()) {
            $customerId = $address->getCustomerId();
            $ruleCustomer = Mage::getModel('salesrule/rule_customer');
            $ruleCustomer->loadByCustomerRule($customerId, $ruleId);
            if ($ruleCustomer->getId()
                && !in_array($rule->getId(), $existingRules)
                && ($ruleCustomer->getTimesUsed() >= $rule->getUsesPerCustomer())
            ) {
                self::$_resultCache[$resultCacheKey] = false;
                return false;
            }
        }
        $rule->afterLoad();
        /**
         * quote does not meet rule's conditions
         */
        if (!$rule->validate($address)) {
            self::$_resultCache[$resultCacheKey] = false;
            return false;
        }

        /**
         * passed all validations, remember to be valid
         */
        self::$_resultCache[$resultCacheKey] = true;

        return true;
    }

    /**
     * Quote item discount calculation process
     *
     * @param   Omnius_Checkout_Model_Sales_Quote_Item|Mage_Sales_Model_Quote_Item_Abstract $item
     * @return  Mage_SalesRule_Model_Validator
     */
    public function process(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        if ($item->getIsPromo()) {
            return $this;
        }

        if ($item->getPackageId() == $this->getActivePackage()->getPackageId()) {
            //get applied rules ids from active package
            $appliedRuleIds = array_map('trim', explode(',', $this->getActivePackage()->getAppliedRuleIds()));
            $priceHelper = $this->getPriceRulesHelper();
            //we have a max accepted iteration for the no of applied rules per packet
            if(count($appliedRuleIds) >=  $priceHelper::$maxIterations){
                return $this;
            }

            $allRules = $this->_getRules();
            if (!count($allRules)) {
                return $this;
            }

            $address = $this->_getAddress($item);

            /** @var Omnius_Checkout_Model_Sales_Quote $quote */
            $quote = $address->getQuote();
            $initialItems = Mage::getSingleton('customer/session')->getOrderEditMode() ? Mage::getSingleton('checkout/session')->getInitialItems() : [];
            $existingRules = $this->getPriceRulesHelper()->explodeTrimmed($item->getAppliedRuleIds());
            $isDelivered = $this->getActivePackage()->isDelivered();
            $deliveredNotPromoCond = $isDelivered && Mage::getSingleton('customer/session')->getOrderEditMode() && !$item->isPromo();
            $deliveredPromoCond = $isDelivered && Mage::getSingleton('customer/session')->getOrderEditMode() && $item->isPromo();
            $itemProdCond = $deliveredNotPromoCond && isset($initialItems[$item->getProductId()]) && $quote->hasProductId($item->getProductId());
            $itemTargetCond = $deliveredPromoCond && isset($initialItems[$item->getTargetId()]) && $quote->hasProductId($item->getTargetId());

            $overrideCond = Mage::registry('is_override_promos') || $quote->getIsOverrulePromoMode();

            $isOverridden = $overrideCond || $itemProdCond || $itemTargetCond;
            $rules = $this->composeRules($item, $isOverridden, $allRules, $existingRules, $address);

            // overwrite rules
            $key = $this->getWebsiteId() . '_' . $this->getCustomerGroupId() . '_' . $this->getCouponCode() . '_' . $item->getPackageId() . '_' . $item->getProductId();
            $this->_rules[$key] = $rules;

            $allowedActions = [
                'ampromo_items' =>"promo_process_items_after",
                'ampromo_remove_items' =>"promo_process_items_remove",
                'ampromo_replace_items' =>"promo_process_items_replace",

                'process_add_product' => "process_add_product",
                'process_remove_product' => "process_remove_product",
                'process_replace_product' =>"process_replace_product"
            ];

            $productActions = [
                'process_add_product', 'process_remove_product', 'process_replace_product'
            ];

            foreach ($rules as $rule) {
                if (!in_array($rule->getSimpleAction(),array_keys($allowedActions))
                    || (in_array($rule->getId(), $appliedRuleIds) && in_array($rule->getSimpleAction(), $productActions))
                ) {
                    continue;
                }

                Mage::dispatchEvent($allowedActions[$rule->getSimpleAction()], array(
                    'rule' => $rule,
                    'item' => $item,
                    'quote' => $quote,
                    'address' => $address
                ));

                $this->getActivePackage()->setAppliedRuleIds($this->mergeIds($this->getActivePackage()->getAppliedRuleIds(), $rule->getId()))->save();
            }
        }

        return $this;
    }

    /**
     * Promo rule validation for items and caches the result for the current collect totals execution.
     * @param Omnius_Checkout_Model_SalesRule_Rule $rule
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @return bool
     */
    public function validateItemByRule($rule, $item)
    {
        if (isset($this->_validated[$rule->getId()]) && isset($this->_validated[$rule->getId()][$item->getProductId()])) {
            return $this->_validated[$rule->getId()][$item->getProductId()];
        } else {
            $this->_validated[$rule->getId()][$item->getProductId()] = $rule->getActions()->validate($item);
            return $this->_validated[$rule->getId()][$item->getProductId()];
        }
    }

    /**
     * Quote item discount calculation process
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @return  Mage_SalesRule_Model_Validator
     */
    public function processItem(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $key = $this->getWebsiteId() . '_' . $this->getCustomerGroupId() . '_' . $this->getCouponCode() . '_' . $item->getPackageId() . '_' . $item->getProductId();
        $item->setDiscountAmount(0);
        $item->setBaseDiscountAmount(0);
        $item->setDiscountPercent(0);
        $quote = $item->getQuote();
        $address = $this->_getAddress($item);

        $itemPrice = $this->_getItemPrice($item);
        $baseItemPrice = $this->_getItemBasePrice($item);
        $itemOriginalPrice = $this->_getItemOriginalPrice($item);
        $baseItemOriginalPrice = $this->_getItemBaseOriginalPrice($item);

        if ($itemPrice < 0) {
            return $this;
        }

        $appliedRuleIds = array();
        if (isset($this->_rules[$key])) {
            /** @var Omnius_Checkout_Model_SalesRule_Rule $rule */
            foreach ($this->_rules[$key] as $rule) {
                $qty = $this->_getItemQty($item, $rule);
                $rulePercent = min(100, $rule->getDiscountAmount());

                $discountAmount = 0;
                $baseDiscountAmount = 0;
                //discount for original price
                $originalDiscountAmount = 0;
                $baseOriginalDiscountAmount = 0;

                switch ($rule->getSimpleAction()) {
                    case Mage_SalesRule_Model_Rule::TO_PERCENT_ACTION:
                        $rulePercent = max(0, 100 - $rule->getDiscountAmount());
                    //no break;
                    case Mage_SalesRule_Model_Rule::BY_PERCENT_ACTION:
                        $step = $rule->getDiscountStep();
                        if ($step) {
                            $qty = floor($qty / $step) * $step;
                        }
                        $_rulePct = $rulePercent / 100;
                        $discountAmount = ($qty * $itemPrice - $item->getDiscountAmount()) * $_rulePct;
                        $baseDiscountAmount = ($qty * $baseItemPrice - $item->getBaseDiscountAmount()) * $_rulePct;
                        //get discount for original price
                        $originalDiscountAmount = ($qty * $itemOriginalPrice - $item->getDiscountAmount()) * $_rulePct;
                        $baseOriginalDiscountAmount =
                            ($qty * $baseItemOriginalPrice - $item->getDiscountAmount()) * $_rulePct;

                        if (!$rule->getDiscountQty() || $rule->getDiscountQty() > $qty) {
                            $discountPercent = min(100, $item->getDiscountPercent() + $rulePercent);
                            $item->setDiscountPercent($discountPercent);
                        }
                        break;
                    case Mage_SalesRule_Model_Rule::TO_FIXED_ACTION:
                        $quoteAmount = $quote->getStore()->convertPrice($rule->getDiscountAmount());
                        $discountAmount = $qty * ($itemPrice - $quoteAmount);
                        $baseDiscountAmount = $qty * ($baseItemPrice - $rule->getDiscountAmount());
                        //get discount for original price
                        $originalDiscountAmount = $qty * ($itemOriginalPrice - $quoteAmount);
                        $baseOriginalDiscountAmount = $qty * ($baseItemOriginalPrice - $rule->getDiscountAmount());
                        break;

                    case Mage_SalesRule_Model_Rule::BY_FIXED_ACTION:
                        $step = $rule->getDiscountStep();
                        if ($step) {
                            $qty = floor($qty / $step) * $step;
                        }
                        $quoteAmount = $quote->getStore()->convertPrice($rule->getDiscountAmount());
                        $discountAmount = $qty * $quoteAmount;
                        $baseDiscountAmount = $qty * $rule->getDiscountAmount();
                        break;

                    case Mage_SalesRule_Model_Rule::CART_FIXED_ACTION:
                        if (empty($this->_rulesItemTotals[$rule->getId()])) {
                            Mage::throwException(Mage::helper('salesrule')->__('Item totals are not set for rule.'));
                        }

                        /**
                         * prevent applying whole cart discount for every shipping order, but only for first order
                         */
                        if ($quote->getIsMultiShipping()) {
                            $usedForAddressId = $this->getCartFixedRuleUsedForAddress($rule->getId());
                            if ($usedForAddressId && $usedForAddressId != $address->getId()) {
                                break;
                            } else {
                                $this->setCartFixedRuleUsedForAddress($rule->getId(), $address->getId());
                            }
                        }
                        $cartRules = $address->getCartFixedRules();
                        if (!isset($cartRules[$rule->getId()])) {
                            $cartRules[$rule->getId()] = $rule->getDiscountAmount();
                        }

                        if ($cartRules[$rule->getId()] > 0) {
                            if ($this->_rulesItemTotals[$rule->getId()]['items_count'] <= 1) {
                                $quoteAmount = $quote->getStore()->convertPrice($cartRules[$rule->getId()]);
                                $baseDiscountAmount = min($baseItemPrice * $qty, $cartRules[$rule->getId()]);
                            } else {
                                $discountRate = $baseItemPrice * $qty /
                                    $this->_rulesItemTotals[$rule->getId()]['base_items_price'];
                                $maximumItemDiscount = $rule->getDiscountAmount() * $discountRate;
                                $quoteAmount = $quote->getStore()->convertPrice($maximumItemDiscount);

                                $baseDiscountAmount = min($baseItemPrice * $qty, $maximumItemDiscount);
                                $this->_rulesItemTotals[$rule->getId()]['items_count']--;
                            }

                            $discountAmount = min($itemPrice * $qty, $quoteAmount);
                            $discountAmount = $quote->getStore()->roundPrice($discountAmount);
                            $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);

                            //get discount for original price
                            $originalDiscountAmount = min($itemOriginalPrice * $qty, $quoteAmount);
                            $baseOriginalDiscountAmount = $quote->getStore()->roundPrice($baseItemOriginalPrice);

                            $cartRules[$rule->getId()] -= $baseDiscountAmount;
                        }
                        $address->setCartFixedRules($cartRules);

                        break;

                    case Mage_SalesRule_Model_Rule::BUY_X_GET_Y_ACTION:
                        $x = $rule->getDiscountStep();
                        $y = $rule->getDiscountAmount();
                        if (!$x || $y > $x) {
                            break;
                        }
                        $buyAndDiscountQty = $x + $y;

                        $fullRuleQtyPeriod = floor($qty / $buyAndDiscountQty);
                        $freeQty = $qty - $fullRuleQtyPeriod * $buyAndDiscountQty;

                        $discountQty = $fullRuleQtyPeriod * $y;
                        if ($freeQty > $x) {
                            $discountQty += $freeQty - $x;
                        }

                        $discountAmount = $discountQty * $itemPrice;
                        $baseDiscountAmount = $discountQty * $baseItemPrice;
                        //get discount for original price
                        $originalDiscountAmount = $discountQty * $itemOriginalPrice;
                        $baseOriginalDiscountAmount = $discountQty * $baseItemOriginalPrice;
                        break;
                    default:
                        break;
                }

                if (
                    $discountAmount > $item->getRowTotalInclTax()
                    && $rule->getCouponType() != Omnius_Checkout_Model_SalesRule_Rule::COUPON_TYPE_NO_COUPON
                ) {
                    $discountAmount = $item->getRowTotalInclTax();
                }

                $result = new Varien_Object(array(
                    'discount_amount' => $discountAmount,
                    'base_discount_amount' => $baseDiscountAmount,
                ));
                Mage::dispatchEvent('salesrule_validator_process', array(
                    'rule' => $rule,
                    'item' => $item,
                    'address' => $address,
                    'quote' => $quote,
                    'qty' => $qty,
                    'result' => $result,
                ));

                $discountAmount = $result->getDiscountAmount();
                $baseDiscountAmount = $result->getBaseDiscountAmount();

                $percentKey = $item->getDiscountPercent();
                /**
                 * Process "delta" rounding
                 */
                if ($percentKey) {
                    $delta = isset($this->_roundingDeltas[$percentKey]) ? $this->_roundingDeltas[$percentKey] : 0;
                    $baseDelta = isset($this->_baseRoundingDeltas[$percentKey])
                        ? $this->_baseRoundingDeltas[$percentKey]
                        : 0;
                    $discountAmount += $delta;
                    $baseDiscountAmount += $baseDelta;

                    $this->_roundingDeltas[$percentKey] = $discountAmount -
                        $quote->getStore()->roundPrice($discountAmount);
                    $this->_baseRoundingDeltas[$percentKey] = $baseDiscountAmount -
                        $quote->getStore()->roundPrice($baseDiscountAmount);
                    $discountAmount = $quote->getStore()->roundPrice($discountAmount);
                    $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);
                } else {
                    $discountAmount = $quote->getStore()->roundPrice($discountAmount);
                    $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);
                }

                /**
                 * We can't use row total here because row total not include tax
                 * Discount can be applied on price included tax
                 */

                $itemDiscountAmount = $item->getDiscountAmount();
                $itemBaseDiscountAmount = $item->getBaseDiscountAmount();

                $discountAmount = min($itemDiscountAmount + $discountAmount, $itemPrice * $qty);
                $baseDiscountAmount = min($itemBaseDiscountAmount + $baseDiscountAmount, $baseItemPrice * $qty);

                $item->setDiscountAmount($discountAmount);
                $item->setBaseDiscountAmount($baseDiscountAmount);

                $item->setOriginalDiscountAmount($originalDiscountAmount);
                $item->setBaseOriginalDiscountAmount($baseOriginalDiscountAmount);

                $appliedRuleIds[$rule->getRuleId()] = $rule->getRuleId();

                $this->_maintainAddressCouponCode($address, $rule);
                $this->_addDiscountDescription($address, $rule);

                if ($rule->getStopRulesProcessing()) {
                    break;
                }
            }
            $item->setAppliedRuleIds(join(',', $appliedRuleIds));
            $address->setAppliedRuleIds($this->mergeIds($address->getAppliedRuleIds(), $appliedRuleIds));
            $quote->setAppliedRuleIds($this->mergeIds($quote->getAppliedRuleIds(), $appliedRuleIds));
        }

        return $this;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * Quote item free shipping ability check
     * This process not affect information about applied rules, coupon code etc.
     * This information will be added during discount amounts processing
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @return  Mage_SalesRule_Model_Validator
     * @todo For the moment, free shipping is disabled, but it might be required in the future version
     */
    public function processFreeShipping(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $item->setFreeShipping(false);

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Quote_Address $address
     * @return $this|Mage_SalesRule_Model_Validator
     */
    public function reset(Mage_Sales_Model_Quote_Address $address)
    {
        parent::reset($address);
        $this->_rules = array();

        return $this;
    }

    /**
     * Return discount item qty
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param Mage_SalesRule_Model_Rule $rule
     * @return int
     */
    protected function _getItemQty($item, $rule)
    {
        $qty = $item->getTotalQty();

        return !is_object($rule->getData('discount_qty')) && $rule->getData('discount_qty') ? min($qty, $rule->getData('discount_qty')) : $qty;
    }

    /**
     * Return item price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemPrice($item)
    {
        $price = $item->getDiscountCalculationPrice();
        $calcPrice = $item->getCalculationPrice();
        if ($item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE)) {
            return $item->getMixmatchSubtotal() + $item->getMixmatchTax();
        }
        return ($price !== null) ? $price : $calcPrice;
    }

    /**
     * @param int $packageId
     * @param $rule
     * @param $manualRuleId
     * @param $promoRules
     * @param $couponRules
     * @param $address
     * @param $rules
     */
    protected function processRule($packageId, $rule, $manualRuleId, $address, &$promoRules, &$couponRules, &$rules)
    {
        if ($rule->getIsPromo()) {
            if (in_array($rule->getId(), $manualRuleId)) {
                $promoRules[] = $rule;
            }
        } else {
            if (in_array($rule->getCouponType(), array(
                Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC,
                Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO
            ))) {
                $valid = false;
                foreach ($rule->getCoupons() as $c) {
                    if ($c->getCode() == $this->getCouponCode()) {
                        $valid = true;
                    }
                }

                if ($this->getCouponCode() == $rule->getCouponCode() || $valid) {
                    $couponRules[] = $rule;
                    $this->couponsApplied[$packageId] = $this->getCouponCode();
                    $resultCacheKey = sprintf('%d_%d_%d', $rule->getId(), $address->getId(), $packageId);
                    self::$_resultCache[$resultCacheKey] = true;
                }
            } else {
                $rules[] = $rule;
            }
        }
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item|Mage_Sales_Model_Quote_Item_Abstract $item
     * @param $isOverridden
     * @param $allRules
     * @param $appliedRuleIds
     * @param $address
     * @return array
     */
    protected function composeRules($item, $isOverridden, $allRules, $appliedRuleIds, $address)
    {
        if (!$isOverridden) {
            $promoRules = [];
            $rules = [];
            $couponRules = [];

            $quote = $address->getQuote();

            /** @var Omnius_Package_Model_Package $packageModel */
            $packageModel = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('package_id', $item->getPackageId());

            /** @var Omnius_Checkout_Model_SalesRule_Rule $rule */
            foreach ($allRules as $rule) {
                if (!$this->validateItemByRule($rule, $item)) {
                    continue;
                }
                $this->processRule($item->getPackageId(), $rule, $appliedRuleIds, $address, $promoRules, $couponRules, $rules);
                if ($this->getCouponCode()) {

                    /** @var Omnius_Package_Model_Package $packageModel */
                    $packageModel = Mage::getModel('package/package')->getCollection()
                        ->addFieldToFilter('package_id', $item->getPackageId());

                    if ($orderEditQuoteId = Mage::getSingleton('customer/session')->getOrderEdit()) {
                        $orderEditQuote = Mage::getModel('sales/quote')->load($orderEditQuoteId);
                        $packageModel->addFieldToFilter('order_id', $orderEditQuote->getSuperOrderEditId());
                    } else {
                        $packageModel->addFieldToFilter('quote_id', $quote->getId());
                    }
                }
            }

            $packageModel = $packageModel->getFirstItem();

            /** @var Omnius_PriceRules_Helper_External_Coupon_Validator $externalValidator */
            $externalValidator = Mage::helper('pricerules/external_coupon_validator');

            if($externalValidator->isValid($this->getCouponCode(), $quote, $packageModel)) {
                $this->couponsApplied[$item->getPackageId()] = $this->getCouponCode();
            }

            if (!empty($promoRules)) {
                $rules = array_merge($promoRules, $couponRules);
            } else {
                $rules = array_merge($rules, $couponRules);
            }
        } else {
            $appliedRuleIds = $this->getPriceRulesHelper()->explodeTrimmed($item->getAppliedRuleIds());
            if ($item->getAppliedRuleIds() == $item->getManualRuleId() && $item->getManualRuleId() != null) {
                $appliedRuleIds = $this->getPriceRulesHelper()->explodeTrimmed($item->getPreviousRules());
            }
            $rules = Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('rule_id', array('in' => $appliedRuleIds));
            $this->couponsApplied[$item->getPackageId()] = $this->getCouponCode();
        }

        return $rules;
    }
}
