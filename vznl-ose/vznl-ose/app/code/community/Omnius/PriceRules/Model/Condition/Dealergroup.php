<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_Condition_Dealergroup
 * Adds dealer group condition to the sales rules.
 */
class Omnius_PriceRules_Model_Condition_Dealergroup extends Omnius_PriceRules_Model_Condition_Abstract
{

    /**
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setupSelectCondition('Dealer Group', 'dealer_group');

        return $this;
    }

    /**
     * Returns available condition options.
     * @return mixed
     */
    public function getValueSelectOptions()
    {
        $segmentOptionsModel = Mage::getModel('pricerules/eav_entity_attribute_source_dealergroup');
        $options = $segmentOptionsModel->getAllOptions();

        return $this->setupOptions($options);
    }

    /**
     * Validates condition.
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        /** @var Dyna_Agent_Model_Agent $agent */
        if ($agent = Mage::getSingleton('customer/session')->getAgent()) {
            /** @var Dyna_Agent_Model_Dealer $dealer */
            $dealer = $agent->getDealer();

            return $this->validateAttribute($dealer->getDealerGroups()->getAllIds());
        }

        return false;
    }

    /**
     * Returns types of condition operators.
     * @return array
     */
    public function getOperatorSelectOptions()
    {
        $options = array(
            '==' => Mage::helper('rule')->__('is'),
            '!=' => Mage::helper('rule')->__('is not'),
            '()' => Mage::helper('rule')->__('is one of'),
            '!()' => Mage::helper('rule')->__('is not one of')
        );

        $operatorOptions = array();
        foreach ($options as $value => $label) {
            $operatorOptions[] = array(
                'value' => $value,
                'label' => $label,
            );
        }

        return $operatorOptions;
    }

    /**
     * @return string
     */
    public function getInputType()
    {
        return 'multiselect';
    }

    /**
     * @return string
     */
    public function getValueElementType()
    {
        return 'multiselect';
    }
}
