<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_BaseObserver
 * @inheritdoc Mage_SalesRule_Model_Observer
 */
class Omnius_PriceRules_Model_BaseObserver extends Mage_SalesRule_Model_Observer
{
    /**
     * Removes the increment usage functionality from the Magento core.
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function sales_order_afterPlace($observer) {
        return $this;
    }
}
