<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_Condition_Paidcode
 * Adds paidcode condition to the sales rules.
 */
class Omnius_PriceRules_Model_Condition_Paidcode  extends Omnius_PriceRules_Model_Condition_Abstract{

    /**
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setupTextCondition('Paidcode', 'paidcode');
        return $this;
    }

    /**
     * Returns customer session.
     * @return mixed
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Validates condition.
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        $dealerPaidCode = $this->_getSession()->getAgent()->getDealer()->getPaidCode();
        return $this->validateAttribute($dealerPaidCode);
    }
} 