<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_Condition_Customersegment
 * Adds customer segment condition to the sales rules.
 */
class Omnius_PriceRules_Model_Condition_Customersegment extends Omnius_PriceRules_Model_Condition_Abstract
{

    /**
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setupSelectCondition('Customer segment', 'customer_segment');
        return $this;
    }

    /**
     * Returns available condition options.
     * @return mixed
     */
    public function getValueSelectOptions()
    {
        $segmentOptionsModel = Mage::getModel('pricerules/eav_entity_attribute_source_segment');
        $options = $segmentOptionsModel->getAllOptions();
        return $this->setupOptions($options);
    }

    /**
     * Validates condition.
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return $this->validateAttribute($customer->getData('segment'));
    }
}
