<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_Observer
 */
class Omnius_PriceRules_Model_Observer extends Mage_Core_Model_Abstract
{
    const COPY_LEVY_PREFIX = 'checkout/copy_levy/copy_levy_';

    protected $_cache = array();

    /**
     * Process sales rule form creation
     * @param   Varien_Event_Observer $observer
     */
    public function handleFormCreation($observer)
    {
        $actionsSelect = $observer->getForm()->getElement('simple_action');
        if ($actionsSelect) {
            $vals = $actionsSelect->getValues();

            $vals[] = array(
                'value' => 'process_add_product',
                'label' => Mage::helper('ampromo')->__('Auto add products'),
            );

            $vals[] = array(
                'value' => 'process_remove_product',
                'label' => Mage::helper('ampromo')->__('Auto remove products'),

            );

            $vals[] = array(
                'value' => 'process_replace_product',
                'label' => Mage::helper('ampromo')->__('Auto replace products'),

            );
            /** Amasty fields  */
            $vals[] = array(
                'value' => 'ampromo_items',
                'label' => Mage::helper('ampromo')->__('Auto add promo items with products'),

            );

            $vals[] = array(
                'value' => 'ampromo_remove_items',
                'label' => Mage::helper('ampromo')->__('Auto remove promo items'),

            );

            $vals[] = array(
                'value' => 'ampromo_replace_items',
                'label' => Mage::helper('ampromo')->__('Auto replace promo items'),

            );
            /** Amasty fields*/

            $actionsSelect->setValues($vals);
            $actionsSelect->setOnchange('promo_check()');

            $fldSet = $observer->getForm()->getElement('action_fieldset');

            $fldSet->addField('promo_sku_replace', 'text', array(
                'name' => 'promo_sku_replace',
                'label' => Mage::helper('ampromo')->__('Remove Products'),
                'note' => Mage::helper('ampromo')->__('Comma separated list of the SKUs'),
            ),
                'promo_sku'
            );

            $fldSet->addField('promo_sku', 'text', array(
                'name' => 'promo_sku',
                'label' => Mage::helper('ampromo')->__('Products'),
                'note' => Mage::helper('ampromo')->__('Comma separated list of the SKUs'),
            ),
                'simple_action'
            );

        }

        return $this;
    }

    /**
     * @param $observer
     * @return mixed
     */
    public function addConditionToSalesRule($observer)
    {
        $additional = $observer->getAdditional();
        $conditions = (array)$additional->getConditions();

        $conditions = array_merge_recursive($conditions, array(
            array('label' => Mage::helper('pricerules')->__('Channel'), 'value' => 'pricerules/condition_channel'),
            array('label' => Mage::helper('pricerules')->__('Dealer'), 'value' => 'pricerules/condition_dealer'),
            array('label' => Mage::helper('pricerules')->__('Dealer Group'), 'value' => 'pricerules/condition_dealergroup'),
            array('label' => Mage::helper('pricerules')->__('Segment'), 'value' => 'pricerules/condition_customersegment'),
            array('label' => Mage::helper('pricerules')->__('Value'), 'value' => 'pricerules/condition_value'),
            array('label' => Mage::helper('pricerules')->__('Campaign'), 'value' => 'pricerules/condition_campaign'),
            array('label' => Mage::helper('pricerules')->__('Customer segment'), 'value' => 'pricerules/condition_customersegment'),
            array('label' => Mage::helper('pricerules')->__('Lifecycle'), 'value' => 'pricerules/condition_lifecycle'),
            array('label' => Mage::helper('pricerules')->__('Paidcode'), 'value' => 'pricerules/condition_paidcode'),
        ));

        $additional->setConditions($conditions);
        $observer->setAdditional($additional);

        return $observer;
    }

    /**
     * @param $observer
     * @return bool
     */
    public function addPromo($observer)
    {
        $rule = $observer->getRule();

        $promoSku = $rule->getPromoSku();
        if (!$promoSku) {
            return false;
        }

        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $item = $observer->getItem();
        $address = $observer->getAddress();
        $quote = $address->getQuote();

        $allItems = $quote->getAllVisibleItems();

        $existing = array();
        foreach ($allItems as $i) {
            $existing[$i->getPackageId()][$i->getSku()][] = $i->getTargetId();
        }
        unset($i);

        $promoSku = explode(',', $promoSku);
        foreach ($promoSku as $sku) {
            $sku = trim($sku);
            if (!$sku) {
                continue;
            }

            if (isset($existing[$item->getPackageId()][$sku]) && is_array($existing[$item->getPackageId()][$sku]) && in_array($item->getProductId(), $existing[$item->getPackageId()][$sku])) {
                continue;
            }

            $product = $this->_loadProduct($sku);
            if (!$product) {
                continue;
            }

            if (Mage_Catalog_Model_Product_Status::STATUS_ENABLED != $product->getStatus()) {
                continue;
            }

            $promoItem = $quote->addProduct($product, 1);

            // set active package id to the promo product
            $promoItem->setTargetId($item->getProductId());
            $promoItem->setIsPromo(true);
            $promoItem->setPackageId($item->getPackageId());
            $promoItem->setPackageType($item->getPackageType());
            $promoItem->setEditOrderId($item->getEditOrderId());
            $promoItem->setCustomPrice(0);
            $promoItem->setOriginalCustomPrice(0);

            try {
                $promoItem->save();
                $quote->addItem($promoItem);
            } catch (Exception $e) {
                // Do nothing
            }
        }

        $address->unsetData('cached_items_nonnominal');
        $address->unsetData('cached_items_all');
        $address->unsetData('cached_items_nominal');

        return true;
    }

    /**
     * @param $observer
     * @return bool
     */
    public function removePromo($observer)
    {
        $rule = $observer->getRule();
        $promoSku = $rule->getPromoSku();

        if (!$promoSku) {
            return false;
        }

        $quote = $observer->getQuote();
        $allItems = $quote->getAllVisibleItems();

        if($promoSku = trim(array_shift(explode(',', $promoSku)))){
            foreach ($allItems as $item){
                if($item->getSku() == $promoSku){
                    $quote->removeItem($item->getId());
                }
            }
        }
        return true;
    }

    /**
     * @param $observer
     * @return bool
     */
    public function replacePromo($observer)
    {
        $rule = $observer->getRule();
        $promoSku = $rule->getPromoSku();
        $promoSkuReplace = $rule->getPromoSkuReplace();

        if (!$promoSku || !$promoSkuReplace) {
            return false;
        }

        $promoSku = trim(array_shift(explode(',', $promoSku)));
        $promoSkuReplace = trim(array_shift(explode(',', $promoSkuReplace)));

        if(!$promoSku || !$promoSkuReplace){
            return false;
        }

        $quote = $observer->getQuote();
        $item  = $observer->getItem();
        $allItems = $quote->getAllVisibleItems();

        foreach ($allItems as &$i) {
            if ($i->getSku() == $promoSku) {
                try {
                    $quote->removeItem($i->getId());
                } catch (Exception $e) {
                    Mage::log($e->getMessage(),null, "exception.log");
                }
                $newProduct = $this->_loadProduct($promoSkuReplace);
                $promoItem = $quote->addProduct($newProduct, 1);

                $promoItem->setTargetId($item->getProductId());
                $promoItem->setIsPromo(true);
                $promoItem->setPackageId($item->getPackageId());
                $promoItem->setPackageType($item->getPackageType());
                $promoItem->setEditOrderId($item->getEditOrderId());
                $promoItem->setCustomPrice(0);
                $promoItem->setOriginalCustomPrice(0);

                try {
                    $promoItem->save();
                    $quote->addItem($promoItem);
                    //Mage::dispatchEvent('sales_quote_address_mixmatch_here', ['quote_item' => $promoItem,"callback_method"=>'observer']);
                } catch (Exception $e) {
                    Mage::log($e->getMessage(),null, "exception.log");
                }

            }
        }
        unset($i);
        return true;

    }

    /**
     * Add new products to active package from quote
     *
     * @param $observer
     * @return bool
     */
    public function addProduct($observer)
    {
        //get rule
        $rule = $observer->getRule();
        //parse sku input, transform into array and trim
        $skuAdd = array_map('trim', explode(',',$rule->getPromoSku()));
        //check if we have data
        if (!$skuAdd && !empty($skuAdd)) {
            return false;
        }
        //get quote
        $quote = $observer->getQuote();
        //get item that triggered this rule
        $item = $observer->getItem();
        //get all visible items from quote
        $allItems = $quote->getAllVisibleItems();
        //add products from quote
        $this->add($allItems, $skuAdd, $item);

        return true;
    }

    /**
     * Remove products to active package from quote
     *
     * @param $observer
     * @return bool
     */
    public function removeProduct($observer)
    {
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        $item = $observer->getItem();

        // get the sales rule
        /** @var Omnius_Checkout_Model_SalesRule_Rule $rule */
        $rule = $observer->getRule();

        // check if there is a list of skus that specifically need to be removed from cart or whether current event item met the action's condition
        $skuRemove = array_map('trim', explode(',', $rule->getPromoSku()));
        if ($item->getSku()) {
            $skuRemove[] = $item->getSku();
        }
        $skuRemove = array_filter(array_unique($skuRemove));
        // check if there is something to remove from cart
        if (empty($skuRemove)) {
            return false;
        }
        // get quote
        $quote = $observer->getQuote();
        // get all visible items from quote
        $allItems = $quote->getAllVisibleItems();
        // remove products from quote by sku
        $this->remove($quote, $allItems, $skuRemove);

        return true;
    }

    /**
     * Remove and adds products in active package from quote
     *
     * @param $observer
     * @return bool
     */
    public function replaceProduct($observer)
    {
        $rule = $observer->getRule();
        $skuAdd = array_map('trim', explode(',',$rule->getPromoSku()));
        $skuRemove  = array_map('trim', explode(',',$rule->getPromoSkuReplace()));
        //check if we have data
        if ((!$skuRemove && !empty($skuRemove)) || (!$skuAdd && !empty($skuAdd))) {
            return false;
        }
        //get quote
        $quote = $observer->getQuote();
        //get item that triggered this rule
        $item  = $observer->getItem();
        //get all visible items from quote
        $allItems = $quote->getAllVisibleItems();
        //remove products from quote by sku
        $this->remove($quote, $allItems, $skuRemove);
        //add products from quote
        $this->add($allItems, $skuAdd, $item);

        return true;
    }

    /**
     * If items match the skus given, remove them from quote
     *
     * @param $quote
     * @param $items
     * @param $skus
     */
    protected function remove($quote, $items, $skus)
    {
        //parse items from quote
        foreach ($items as $i) {
            //delete if match the sku array given
            if (in_array($i->getSku(), $skus)) {
                try {
                    $quote->removeItem($i->getId());
                } catch (Exception $e) {
                    Mage::log($e->getMessage(),null, "exception.log");
                }
            }
        }
        //save quote after remove items
        $quote->save();
    }

    /**
     * Add new products to quote
     *
     * @param $items
     * @param $skus
     * @param $parentItem
     */
    protected function add($items, $skus, $parentItem)
    {
        //existing sku in given package
        $existing = array();
        //parse item and build existing array
        foreach ($items as &$i) {
            $existing[$i->getPackageId()][$i->getSku()][] = $i->getTargetId();
        }
        unset($i);

        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        //parse skus given for add
        foreach ($skus as $sku){
            //check if this sku exists already in this package
            if (isset($existing[$parentItem->getPackageId()][$sku]) || is_array($existing[$parentItem->getPackageId()][$sku])) {
                continue;
            }
            //Get product id by sku
            $id = Mage::getModel('catalog/product')->getIdBySku($sku);
            $item = $cart->addProduct($id);
            if(!$item->getId()){
                $item->save();
                $cart->getQuote()->addItem($item);
            }
        }
        //save quote after remove items
        $cart->getQuote()->save();
    }

    /**
     * @param $observer
     * @return bool
     */
    public function addCopyLevyProduct($observer)
    {
        /** @var Varien_Db_Adapter_Interface $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $quote = $observer->getQuote();
        $allItems = $quote->getAllVisibleItems();
        $existing = $addedThk = [];
        foreach($allItems as $i) {
            $existing[$i->getPackageId()][$i->getSku()][] = $i->getTargetId();
            if (in_array($i->getSku(), array('4600037', '4600036'))) {
                $addedThk[$i->getPackageId()][$i->getTargetId()] = $i->getProductId();
            }
        }

        $attrTextSql = <<<SQL_LABEL
SELECT
    IF(tsv.value_id > 0,
        tsv.value,
        tdv.value) AS `value`
FROM
    `eav_attribute_option` AS `main_table`
        INNER JOIN
    `eav_attribute_option_value` AS `tdv` ON tdv.option_id = main_table.option_id
        LEFT JOIN
    `eav_attribute_option_value` AS `tsv` ON tsv.option_id = main_table.option_id
        AND tsv.store_id = '0'
WHERE
    (attribute_id = (SELECT
            `eav_attribute`.attribute_id
        FROM
            `eav_attribute`
        WHERE
            (`eav_attribute`.`attribute_code` = '%s')
                AND (entity_type_id = '4')))
        AND (tdv.store_id = '0')
        AND (tdv.option_id = (SELECT
            value
        FROM
            catalog_product_entity_int
        WHERE
            entity_id = '%%s'
                AND attribute_id = (SELECT
                    `eav_attribute`.attribute_id
                FROM
                    `eav_attribute`
                WHERE
                    (`eav_attribute`.`attribute_code` = '%s')
                        AND (entity_type_id = '4'))))
ORDER BY main_table.sort_order ASC , value ASC
SQL_LABEL;

        $attributeCode = 'prijs_thuiskopieheffing_type';
        $attrTextSql = sprintf($attrTextSql, $attributeCode, $attributeCode);

        foreach ($allItems as $item) {
            $thuisKopieHeffingType = $connection->fetchOne(sprintf($attrTextSql, $item->getProductId()));

            if (in_array($thuisKopieHeffingType, array('Standaard', 'Hoog', 'Laag'))) {
                $weeeTaxApplied = $item->getWeeeTaxAppliedUnserialized();

                $sku = $this->getLevyProductSku($weeeTaxApplied, $item, $thuisKopieHeffingType);

                if (!$sku) {
                    continue;
                }

                $itemExists = isset($existing[$item->getPackageId()][$sku]) &&
                    is_array($existing[$item->getPackageId()][$sku]) &&
                    in_array($item->getProductId(), $existing[$item->getPackageId()][$sku]);
                if ($itemExists || isset($addedThk[$item->getPackageId()][$item->getProductId()])) {
                    continue;
                }

                $product = $this->_loadProduct($sku);

                if (!$product) {
                    continue;
                }
                $levyItem = $quote->addProduct($product, 1);

                // set active package id to the promo product
                $levyItem->setPackageId($item->getPackageId());
                $levyItem->setPackageType($item->getPackageType());
                $levyItem->setEditOrderId($item->getEditOrderId());
                $levyItem->setTargetId($item->getProductId());

                try {
                    $levyItem->save();
                    $quote->addItem($levyItem);
                    $addedThk[$item->getPackageId()][$item->getProductId()] = $levyItem->getProductId();
                } catch (Exception $e) {
                    Mage::getSingleton('core/logger')->logException($e);
                }
            }
        }

        return true;
    }

    /**
     * @param $sku
     * @return bool|Mage_Catalog_Model_Product
     */
    public function _loadProduct($sku)
    {
        if (empty($this->_cache[$sku])) {
            $productCollection = Mage::getModel('catalog/product')->getCollection()
                ->addFieldToFilter('sku', array('eq' => $sku))
                ->addAttributeToSelect(Mage::getSingleton('sales/quote_config')->getProductAttributes());

            Mage::getSingleton('cataloginventory/stock')
                ->addInStockFilterToCollection($productCollection)
                ->addItemsToProducts($productCollection);

            if ($productCollection->count() == 0) {
                return $this->_cache[$sku] = false;
            }

            $product = $productCollection->getFirstItem();

            if (Mage_Catalog_Model_Product_Status::STATUS_ENABLED != $product->getStatus()) {
                return $this->_cache[$sku] = false;
            }
            $this->_cache[$sku] = $product;
        }

        return $this->_cache[$sku];
    }

    /**
     * Observer function that decrements all the sales rules times used when an item is removed.
     * @param $observer
     */
    public function decrementTimesUsed($observer)
    {
        $item = $observer->getQuoteItem();
        Mage::helper('pricerules')->decrementTimesUsedOfRule($item);
    }

    /**
     * @param $weeeTaxApplied
     * @param $item
     * @param $thuisKopieHeffingType
     * @return bool|mixed|string
     */
    protected function getLevyProductSku($weeeTaxApplied, $item, $thuisKopieHeffingType)
    {
        $sku = false;
        if (!empty($weeeTaxApplied)) {
            foreach ($weeeTaxApplied as $taxApplied) {
                $connCost = Mage::helper('tax')->getPrice(
                    $item->getProduct(),
                    $taxApplied['base_amount_incl_tax'],
                    true
                );

                if ($connCost == 6.05) {
                    // TKH high
                    // We add a new line item with service article SKU 4600037
                    $sku = '4600037';
                } elseif ($connCost == 3.03) {
                    // TKH low
                    // We add a new line item with service article SKU 4600036
                    $sku = '4600036';
                }
            }

            return $sku;
        } else {
            $sku = ($this->_cache[$thuisKopieHeffingType] = isset($this->_cache[$thuisKopieHeffingType]) ? $this->_cache[$thuisKopieHeffingType] : Mage::getStoreConfig(self::COPY_LEVY_PREFIX . strtolower($thuisKopieHeffingType)));

            return $sku;
        }
    }

    /**
     * Checks whether the coupon code is valid.
     * 
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function validateCoupon($observer)
    {
        /** @var Varien_Event $event */
        $event = $observer->getEvent();

        /** @var Mage_SalesRule_Model_Coupon $coupon */
        $coupon = Mage::getModel('salesrule/coupon')->load($event->getCoupon(), 'code');

        if ($coupon->getId()) {
            /** @var Closure $closure */
            $closure = $event->getValidate();
            $closure('Omnius_PriceRules');
        }
    }

    /**
     * Remove the coupon code.
     * 
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function removeCoupon($observer)
    {
        $observer->getPackage()
            ->setCoupon(null)
            ->save();
    }
}
