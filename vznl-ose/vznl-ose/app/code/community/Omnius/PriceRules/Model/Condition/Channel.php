<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_Condition_Channel
 * Adds website condition to the sales rules.
 */
class Omnius_PriceRules_Model_Condition_Channel extends Omnius_PriceRules_Model_Condition_Abstract
{

    /**
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setupSelectCondition('Channel', 'channel');
        return $this;
    }

    /**
     * Returns available values for condition.
     * @return mixed
     */
    public function getValueSelectOptions()
    {
        $websites = Mage::getSingleton('adminhtml/system_store')->getWebsiteCollection();
        $options = array();
        foreach ($websites as $ws) {
            $options[] = array(
                'value' => $ws->getCode(),
                'label' => $ws->getName()
            );
        }
        return $this->setupOptions($options);
    }

    /**
     * Validates condition.
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        return $this->validateAttribute(Mage::app()->getWebsite()->getCode());
    }

}
