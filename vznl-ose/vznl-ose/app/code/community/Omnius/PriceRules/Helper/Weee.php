<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_PriceRules_Helper_Weee extends Mage_Weee_Helper_Data{

    const SWITCH_TAX_TIME = '2015-01-01 00:00';

    /**
     * Check if fixed taxes are used in system
     *
     * @param Mage_Core_Model_Store $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        if (strtotime(now()) > strtotime(self::SWITCH_TAX_TIME)){
            return false;
        }
        if ($store == null && $this->_store) {
            //This is needed when order is created from backend
            $store = $this->_store;
        }
        return Mage::getStoreConfig(self::XML_PATH_FPT_ENABLED, $store);
    }
} 