<?php
$installer = $this;
// Sales rule table db
$salesTable = $installer->getTable('salesrule/rule');
$packageTable = $installer->getTable('catalog_package');
$connection = $installer->getConnection();

// Used as input for add products when apply a promotion
if (!$connection->tableColumnExists($salesTable, 'promo_sku_replace')) {
    $connection
        ->addColumn($salesTable, 'promo_sku_replace',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Used as input for products when apply a promotion',
                'nullable' => true,
                'default' => null,
                'length' => 255
            )
        );
}

if (!$connection->tableColumnExists($packageTable, 'applied_rule_ids')) {
    $connection
        ->addColumn($packageTable, 'applied_rule_ids',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Holds the applied rules per package',
                'nullable' => true,
                'default' => null,
                'length' => 255
            )
        );
}

//end setup installer
$installer->endSetup();
