<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Data
 */
class Omnius_ProductMatchRule_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_websites = [];

    /** @var Varien_Db_Adapter_Interface */
    protected $connection;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /** @var array */
    protected $productCount = [];

    /** @var array */
    protected $websiteProductIds = [];

    /** @var array */
    protected $warnings = [];

    /**
     * Returns operation types.
     * @return array
     */
    public function getOperationTypes()
    {
        return array(
            1 => 'PRODUCT-PRODUCT',
            2 => 'PRODUCT-CATEGORY',
            3 => 'CATEGORY-CATEGORY',
            4 => 'CATEGORY-PRODUCT'
        );
    }

    /**
     * Returns allowed operations.
     * @return array
     */
    public function getOperations()
    {
        return array(
            0 => 'Not Allowed',
            1 => 'Allowed',
            2 => 'Defaulted',
            3 => 'Obligated'
        );
    }

    /**
     * @param $productIds
     * @param null $websiteId
     * @return array
     */
    public function getDefaultRules($productIds, $websiteId = null)
    {
        if ($websiteId == null) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }
        /** @var Omnius_ProductMatchRule_Model_Defaulted $defaultedModel */
        $defaultedModel = Mage::getModel('productmatchrule/defaulted');
        $result = [];
        foreach ($productIds as $prod) {
            $result[$prod] = $defaultedModel->getDefaultedProductsFor($prod, $websiteId);
        }

        return $result;
    }

    /**
     * @return Varien_Db_Adapter_Interface
     */
    protected function getConnection()
    {
        if ($this->connection == null) {
            /** @var Mage_Core_Model_Resource $resource */
            $resource = Mage::getSingleton('core/resource');
            $this->connection = $resource->getConnection('core_read');
        }
        return $this->connection;
    }

    /**
     * @param $data
     * @param array $oldWebsiteIds
     * @return array|string
     */
    public function setRuleData($data, $oldWebsiteIds = [])
    {
        $websiteIds = $data['website_id'];
        $models = [];
        /** @var Mage_Adminhtml_Model_Session $adminSession */
        $adminSession = Mage::getSingleton('adminhtml/session');

        foreach ($websiteIds as $websiteId) {
            $model = !empty($oldWebsiteIds[$websiteId]) ? $oldWebsiteIds[$websiteId] : Mage::getModel('productmatchrule/rule');
            $model->getMaxPriority();

            /** RFC-150019 saving title and description for each rule match */
            if (isset($data['rule_title'])) {
                $model->setRuleTitle($data['rule_title']);
            }

            if (isset($data['rule_description'])) {
                $model->setRuleDescription($data['rule_description']);
            }
            try {
                $this->validateRuleData($data, $websiteId);
            } catch (Omnius_ProductMatchRule_Model_RuleException $e) {
                foreach ($this->warnings as $warn) {
                    $adminSession->addWarning($warn);
                }
                return $e->getMessage();
            }
            $model->addData($data)->setWebsiteId($websiteId);
            $models[] = $model;
        }

        foreach ($this->warnings as $warn) {
            $adminSession->addWarning($warn);
        }

        return $models;
    }

    /**
     * Get the website name by Id
     *
     * @param $websiteId
     * @return string
     */
    public function getWebsiteById($websiteId)
    {
        if (!isset($this->_websites[$websiteId])) {
            $this->_websites[$websiteId] = Mage::app()->getWebsite($websiteId)->getName();
        }

        return $this->_websites[$websiteId];
    }

    /**
     * Fetches all simple products as an associative array where the
     * key is the product id and the value is the product name.
     * @return array
     */
    public function getProductsForDropdown()
    {
        $key = md5(__CLASS__ . 'products_for_dropdown');
        $options = unserialize($this->getCache()->load($key));
        if (empty($options)) {
            $products = $this->getProductCollection();
            $options = array(-1 => 'Please select a product..');
            foreach ($products as $productId => $product) {
                $options[] = array(
                    'label' => $product["type_id"] == "configurable" ? sprintf("%s (configurable)", $product['name']) : $product["name"],
                    'value' => $productId
                );
            }
            unset($products);

            $this->getCache()->save(
                serialize($options),
                $key,
                [Dyna_Cache_Model_Cache::CACHE_TAG],
                $this->getCache()->getTtl()
            );
        }

        return $options;
    }

    /**
     * Fetches all simple products as an associative array where the
     * key is the product id and the value is the product sku.
     * @param bool $forJs
     * @return array
     */
    public function getProductSkusForDropdown($forJs = false)
    {
        $key = md5(__CLASS__ . 'products_for_dropdown_sku' . (int) $forJs);
        $options = unserialize($this->getCache()->load($key));

        if (empty($options)) {
            $products = $this->getProductCollection();
            $options = [];
            $counter = 0;
            foreach ($products as $productId => $product) {
                $index = $forJs ? $counter : $productId;
                $options[$index] = array(
                    'value' => $product['sku'],
                    'data' => $productId
                );
                ++$counter;
            }
            unset($products);

            $this->getCache()
                ->save(
                    serialize($options),
                    $key,
                    [Dyna_Cache_Model_Cache::CACHE_TAG],
                    $this->getCache()->getTtl()
                );
        }

        return $options;
    }

    /**
     * Fetches all the categories as an associative array where the
     * key is the category id and the value is the category name.
     *
     * @return array
     */
    public function getCategoriesForDropdown()
    {
        /** @var Mage_Catalog_Model_Resource_Category_Collection $categoriesCollection */
        $categoriesCollection = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('name');
        $selectable_collection = array(-1 => 'Please select a category...');
        foreach ($categoriesCollection as $category) {
            $selectable_collection[$category->getId()] = $category->getName();
        }

        return $selectable_collection;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * Get a list of product ids and skus that are associated with this category: $product[$productId]=$productSku;
     *
     * @return array
     * @param category int
     */
    public function getProductIdsAndSkus($categoryId)
    {
        $result = [];
        $query = "SELECT
                    t1.`category_id`,
                    t2.`entity_id`,
                    t2.`sku`
                  FROM
                    catalog_category_product t1
                  RIGHT JOIN catalog_product_entity t2
                  ON t1.`product_id` = t2.`entity_id`
                  WHERE t1.`category_id`='{$categoryId}'";
        $allRows =  $this->getConnection()->fetchAll($query);
        foreach ($allRows as $row) {
            $result[$row['entity_id']] = $row['sku'];
        }

        return $result;
    }

    /**
     * Get a list of product ids for a specified website
     *
     * @param $websiteId int
     * @return array
     */
    public function getWebsitesProductsId($websiteId)
    {
        if (!isset($this->websiteProductIds[$websiteId])) {
            $this->websiteProductIds[$websiteId] = [];
            $query = "select product_id from catalog_product_website where website_id='{$websiteId}'";
            $allRows = $this->getConnection()->fetchAll($query);
            foreach ($allRows as $row) {
                $this->websiteProductIds[$websiteId][$row['product_id']] = $row['product_id'];
            }
        }

        return $this->websiteProductIds[$websiteId];
    }

    /**
     * @return array
     */
    public function getProductCollection()
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $products */
        $products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('name', array('neq' => ''))
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToSelect('name')
            ->setOrder('name', 'asc');

        return $this->getConnection()->fetchAssoc($products->getSelect());
    }

    /**
     * @param $data
     * @param $websiteId
     * @throws Omnius_ProductMatchRule_Model_RuleException
     */
    public function validateRuleData($data, $websiteId)
    {
        switch($data['operation_type']){
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                $this->validateP2P($data, $websiteId);
                break;
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                $this->validateP2C($data, $websiteId);
                break;
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                $this->validateC2P($data, $websiteId);
                break;
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                $this->validateC2C($data, $websiteId);
                break;
        }

        Mage::dispatchEvent('validate_product_match_rule', ['data_array' => $data, 'website_id' => $websiteId]);
    }

    /**
     * @param $data
     * @param $websiteId
     * @throws Omnius_ProductMatchRule_Model_RuleException
     */
    protected function validateP2P($data, $websiteId)
    {
        /** @var Mage_Catalog_Model_Product $first_product */
        $first_product = Mage::getModel('catalog/product')->load($data['left_id']);
        /** @var Mage_Catalog_Model_Product $second_product */
        $second_product = Mage::getModel('catalog/product')->load($data['right_id']);

        if ($data['operation'] == Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED) {
            throw new Omnius_ProductMatchRule_Model_RuleException("Invalid operation: A product can't be set as mandatory.");
        }

        $intersected = array_intersect($first_product->getWebsiteIds(), $second_product->getWebsiteIds());
        if (empty($intersected)) {
            $this->warnings[] = 'The products must belong at least to one common website.';
        }

        $this->validateWebsiteAvailability($first_product, $websiteId);
        $this->validateWebsiteAvailability($second_product, $websiteId);
    }

    /**
     * @param $data
     * @param $websiteId
     * @throws Omnius_ProductMatchRule_Model_RuleException
     */
    protected function validateP2C($data, $websiteId)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->load($data['left_id']);
        if ($data['operation'] == Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED) {
            throw new Omnius_ProductMatchRule_Model_RuleException("Invalid operation: A category can't be set as defaulted.");
        }

        $this->validateWebsiteAvailability($product, $websiteId);
        $categoryProductsIds = $this->getCategoryProductIds($data['right_id']);
        //Continuing checking for each product visibility in focused store if category has at least one
        if ($this->productCount[$data['right_id']]) {
            $this->validateCategoryWebsite($categoryProductsIds, $websiteId);
        }
    }

    /**
     * @param $data
     * @param $websiteId
     * @throws Omnius_ProductMatchRule_Model_RuleException
     */
    protected function validateC2P($data, $websiteId)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->load($data['right_id']);

        $this->validateWebsiteAvailability($product, $websiteId);

        if ($data['operation'] == Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED) {
            throw new Omnius_ProductMatchRule_Model_RuleException("Invalid operation: A product can't be set as mandatory.");
        }
        $categoryProductsIds = $this->getCategoryProductIds($data['left_id']);

        //Continuing checking for each product visibility in focused store if category has at least one
        if ($this->productCount[$data['left_id']]) {
            $this->validateCategoryWebsite($categoryProductsIds, $websiteId);
        }
    }

    /**
     * @param $data
     * @param $websiteId
     * @throws Omnius_ProductMatchRule_Model_RuleException
     */
    protected function validateC2C($data, $websiteId)
    {
        if ($data['operation'] == Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED) {
            throw new Omnius_ProductMatchRule_Model_RuleException("Invalid operation: A category can't be set as defaulted.");
        }

        $categoryProductsIds = $this->getCategoryProductIds($data['left_id']);
        $categoryProductsIds2 = $this->getCategoryProductIds($data['right_id']);

        if ($this->productCount[$data['left_id']]) {
            $this->validateCategoryWebsite($categoryProductsIds, $websiteId);
        }

        if ($this->productCount[$data['right_id']]) {
            $this->validateCategoryWebsite($categoryProductsIds2, $websiteId);
        }
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param int $websiteId
     */
    protected function validateWebsiteAvailability($product, $websiteId)
    {
        if (!in_array($websiteId, $product->getWebsiteIds())) {
            $this->warnings[] = sprintf('The website %s must include this product sku: %s', $this->getWebsiteById($websiteId), $product->getSku());
        }
    }

    /**
     * @param $categoryId
     * @return array
     */
    protected function getCategoryProductIds($categoryId)
    {
        $categoryProductsIds = [];
        //Checking if category product ids have been loaded
        if (!isset($this->productCount[$categoryId])) {
            $categoryProductsIds = $this->getProductIdsAndSkus($categoryId);
            $this->productCount[$categoryId] = count($categoryProductsIds);
            if (!$this->productCount[$categoryId]) {
                /** @var Dyna_Catalog_Model_Category $category */
                $category = Mage::getModel('catalog/category')->load($categoryId);
                $this->warnings[] = sprintf('Category %s does not have products.', $category->getName());
            }
        }
        return $categoryProductsIds;
    }

    /**
     * @param $categoryProductsIds
     * @param $websiteId
     */
    protected function validateCategoryWebsite($categoryProductsIds, $websiteId)
    {
        $websiteProductIds = $this->getWebsitesProductsId($websiteId);
        foreach ($categoryProductsIds as $productId => $sku) {
            if (!isset($websiteProductIds[$productId])) {
                //This product from focused category doesn't belong to this website
                $this->warnings[] = sprintf('The website %s must include this product sku: %s', $this->getWebsiteById($websiteId), $sku);
            }
        }
    }
}
