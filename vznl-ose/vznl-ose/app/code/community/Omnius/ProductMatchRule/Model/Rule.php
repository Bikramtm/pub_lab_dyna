<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_ProductMatchRule_Model_Rule extends Mage_Core_Model_Abstract
{
    const OP_TYPE_P2P = 1;
    const OP_TYPE_P2C = 2;
    const OP_TYPE_C2C = 3;
    const OP_TYPE_C2P = 4;

    const OP_NOTALLOWED = 0;
    const OP_ALLOWED = 1;
    const OP_DEFAULTED = 2;
    const OP_OBLIGATED = 3;

    const ORIGIN_USER = 0;
    const ORIGIN_IMPORT = 1;

    // added to remove Dyna_Sandbox_Model_Sandbox dependency
    const UNIQUE_ID_REFERENCE = 'unique_id_replication_reference';

    protected $_maxPriority = 0;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    protected function _construct()
    {
        $this->_init('productmatchrule/rule');
    }

    /**
     * Fetches the highest priority from the product_match_rule table.
     * @return int
     */
    public function getMaxPriority()
    {
        if (!$this->_maxPriority) {
            $query = $this->getCollection()->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('MAX(priority) as max');
            $max = $query->query()->fetchAll();
            if (is_array($max) && count($max) > 0) {
                return $this->_maxPriority = (int) $max[0]['max'];
            } else {
                return $this->_maxPriority = 0;
            }
        }

        return $this->_maxPriority + 1;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * Gets a list of categories and associated products
     * @param int|null $websiteId
     * @return array
     */
    public function getAllCategoryProducts($websiteId)
    {
        $productCategories = array();
        $categories = Mage::getModel('catalog/category')->getCollection();
        foreach ($categories as $category) {
            $categoryProductIds = Mage::getResourceModel('catalog/product_collection')
                ->addWebsiteFilter(array($websiteId))
                ->addCategoryFilter($category)
                ->getAllIds();
            $productCategories[$category->getId()] = $categoryProductIds;
        }

        return $productCategories;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        if (!$this->getData('unique_id')) {
            $this->setData('unique_id', hash('sha256', (time() . uniqid(rand(0, 1000)) . __FILE__)));
        }

        return parent::_beforeSave();
    }

    /**
     * Processing object after save data
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        /**
         * Do not clean cache (heavy I/O) after each save
         */
        return $this;
    }

    /**
     *
     * @param int $id
     * @return Mage_Core_Model_Abstract
     */
    public function loadAll($id = null)
    {
        $id = $id ?: $this->getProductMatchRuleId();
        $model = $this->load($id);
        return $this->getCollection()
            ->addFieldToFilter('operation_type', $model->getData('operation_type'))
            ->addFieldToFilter('left_id', $model->getData('left_id'))
            ->addFieldToFilter('right_id', $model->getData('right_id'))
            ->addFieldToFilter('operation', $model->getData('operation'))
            ->addFieldToFilter('priority', $model->getData('priority'))
            ->load();
    }

    /**
     * @return int
     */
    public function getRuleOrigin()
    {
        return (int)parent::getRuleOrigin();
    }
}
