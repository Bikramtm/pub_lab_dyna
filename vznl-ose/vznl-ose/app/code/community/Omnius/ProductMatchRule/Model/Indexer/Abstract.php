<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Model_Indexer_Abstract
 */
abstract class Omnius_ProductMatchRule_Model_Indexer_Abstract extends Mage_Index_Model_Indexer_Abstract
{
    /** @var Magento_Db_Adapter_Pdo_Mysql $_connection */
    protected $_connection;
    /** @var  Zend_Db_Statement_Pdo $_query */
    protected $_query;

    protected $_flushOnMemory = 512000000; //512MB used when cannot read value from machine
    protected $_maxPacketsLength = 5000000; //5MB used when cannot read value from machine
    protected $_maxPacketsLengthAllowed = 32000000; //32MB max value for packet size

    const BATCH_SIZE = 500;

    /**
     * Initiate indexer
     */
    public function __construct()
    {
        parent::__construct();

        $this->productMatchRuleHelper = Mage::helper('productmatchrule');
        $this->_connection = Mage::getModel('core/resource')->getConnection('core_write');
    }

    abstract protected function _applyChanges();

    /**
     * Setup memory limits for execution
     */
    protected function initMemoryLimits()
    {
        /** convert memory limit to bytes */
        // php memory_limit should be at least 512MB even 1024MB for processes that run during the night
        $value = trim(trim(ini_get('memory_limit')));
        if ($value == -1) {
            $value = $this->_getMachineMaxMemory();
        } else {
            $unit = strtolower(substr($value, -1, 1));
            $value = (int)$value;
            switch ($unit) {
                case 'g':
                    $value *= 1024;
                // no break (cumulative multiplier)
                case 'm':
                    $value *= 1024;
                // no break (cumulative multiplier)
                case 'k':
                    $value *= 1024;
                default:
            }
        }

        //25% less than limit
        $this->_flushOnMemory = (int)($value - (0.25 * $value));

        unset($value);
        unset($unit);

        $data = $this->getConnection()->fetchAll('SHOW VARIABLES;');
        foreach ($data as &$var) {
            //mysql max_allowed_packet should be at least 512MB even 1024MB
            if ($var['Variable_name'] == 'max_allowed_packet') {
                $limit = (int)$var['Value'];
                //25% less then limit
                $newPackLength = (int)($limit - (0.25 * $limit));
                $this->_maxPacketsLength = ($newPackLength > $this->_maxPacketsLengthAllowed) ? $this->_maxPacketsLengthAllowed : $newPackLength;
                break;
            }
        }
        unset($var);
        unset($data);
        unset($limit);
    }

    /**
     * Limit to 1GB if cannot be accessed
     * @return float
     */
    protected function _getMachineMaxMemory()
    {
        if (!is_readable("/proc/meminfo")) {
            return 1024 * 1024 * 1024;
        }

        $data = explode(PHP_EOL, file_get_contents("/proc/meminfo"));
        $memInfo = [];
        foreach ($data as $line) {
            $values = explode(":", $line);
            if (count($values) == 2) {
                list($key, $val) = $values;
                $memInfo[$key] = trim($val);
            } else {
                $memInfo[$line] = null;
            }
        }
        if (empty($memInfo['MemTotal'])) {
            $memInfo['MemTotal'] = 0;
        }
        $maxMemory = 1024 * ((int) trim(trim($memInfo['MemTotal'], 'kb')));

        return max($maxMemory * 0.2, 1024 * 1024 * 1024);
    }

    /**
     * Close DB connection
     */
    protected function closeConnection() {
        $this->_query = null;
        $this->_connection = null;
    }

    /**
     * @return Magento_Db_Adapter_Pdo_Mysql
     */
    public function getConnection()
    {
        if ($this->_connection == null) {
            $this->_connection = Mage::getModel('core/resource')->getConnection('core_write');
        }
        return $this->_connection;
    }

    /**
     * Executed after processing the collection
     */
    public function end()
    {
        $this->closeConnection();
    }

    /**
     * Check is we approach the PHP memory
     * limit. If the current used memory exceeds
     * the current limit, all gathered data until
     * this moment will be flushed to the database
     */
    protected function _assertMemory()
    {
        if ($this->isMemoryExceeded()) {
            $this->productMatchRuleHelper->logIndexer('[Productmatch] Memory Exceeded = ' . memory_get_usage() . ', ' . $this->_flushOnMemory);
            $this->_applyChanges();
            $this->productMatchRuleHelper->logIndexer('[Productmatch] New Memory value = ' . memory_get_usage() . ', ' . $this->_flushOnMemory);
        }
    }

    /**
     * @return bool
     */
    protected function isMemoryExceeded()
    {
        return memory_get_usage() >= $this->_flushOnMemory;
    }
}
