<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Model_RuleException
 */
class Omnius_ProductMatchRule_Model_RuleException extends Exception
{
}