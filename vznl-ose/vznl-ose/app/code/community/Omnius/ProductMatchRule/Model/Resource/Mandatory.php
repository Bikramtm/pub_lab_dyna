<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_ProductMatchRule_Model_Resource_Mandatory extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('productmatchrule/mandatory', 'entity_id');
    }
}