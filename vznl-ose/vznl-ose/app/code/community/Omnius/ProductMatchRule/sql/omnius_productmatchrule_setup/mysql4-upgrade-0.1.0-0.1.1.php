<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$this->startSetup();
if (!$this->getConnection()->isTableExists($this->getTable('product_match_whitelist_index'))) {
    // Defining table structure via object
    $table = $this->getConnection()
        ->newTable($this->getTable('product_match_whitelist_index'))
        ->addColumn('whitelist_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'auto_increment' => true
            ))
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ))
        ->addColumn('source_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ))
        ->addColumn('target_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ))
        ->addForeignKey(
            $this->getFkName('product_match_whitelist_index_website', 'website_id', 'core/website', 'website_id'),
            'website_id', $this->getTable('core/website'), 'website_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $this->getFkName('product_match_whitelist_index_product_source', 'source_product_id', 'catalog/product', 'entity_id'),
            'source_product_id', $this->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $this->getFkName('product_match_whitelist_index_product_target', 'target_product_id', 'catalog/product', 'entity_id'),
            'target_product_id', $this->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        );

    // Executing table creation object
    $this->getConnection()->createTable($table);
}

$this->endSetup();