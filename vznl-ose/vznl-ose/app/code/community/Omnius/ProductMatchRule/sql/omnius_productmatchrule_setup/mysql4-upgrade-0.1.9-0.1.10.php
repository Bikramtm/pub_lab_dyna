<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$tableName = "product_match_mandatory_index";

if ($installer->getConnection()->isTableExists($tableName) != true) {
    //Defining table structure via object
    $table = $installer->getConnection()
        ->newTable($installer->getTable($tableName))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'auto_increment' => true
            ))
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ))
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ))
        ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ))
        ->addForeignKey(
            $installer->getFkName('product_match_mandatory_index_website', 'website_id', 'core/website', 'website_id'),
            'website_id', $installer->getTable('core/website'), 'website_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('product_match_mandatory_index_product', 'product_id', 'catalog/product', 'entity_id'),
            'product_id', $installer->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('product_match_mandatory_index_category', 'category_id', 'catalog/category', 'entity_id'),
            'category_id', $installer->getTable('catalog/category'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        );

    //Executing table creation object
    $installer->getConnection()->createTable($table);
    $installer->getConnection()->addIndex(
        $installer->getTable('product_match_mandatory_index'),
        'IDX_MANDATORY_WEBSITE_LEFT_RIGHT_UNIQUE_INDEXER',
        array('website_id', 'product_id', 'category_id'),
        'unique'
    );
}

$installer->endSetup();