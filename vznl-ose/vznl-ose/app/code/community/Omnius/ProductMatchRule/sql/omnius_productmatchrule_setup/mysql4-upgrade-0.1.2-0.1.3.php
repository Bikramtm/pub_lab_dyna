<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$table = $this->getTable('product_match_rule');

$connection->addColumn($table, 'rule_origin', 'TINYINT(1) DEFAULT 0');

$installer->endSetup();