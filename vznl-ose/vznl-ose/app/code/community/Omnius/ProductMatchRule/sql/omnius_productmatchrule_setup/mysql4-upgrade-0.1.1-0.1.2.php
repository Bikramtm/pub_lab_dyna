<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$table = $this->getTable('product_match_rule');

$connection->addColumn($table, 'website_id', 'integer');
$connection->addForeignKey(
    'fk_productmatchrule_core_website',
    $table,
    'website_id',
    'core_website',
    'website_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_CASCADE
);

$installer->endSetup();