<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addIndex(
    $installer->getTable('product_match_whitelist_index'),
    'IDX_WEBSITE_LEFT_RIGHT_UNIQUE_INDEXER',
    array('website_id', 'source_product_id', 'target_product_id'),
    'unique'
);

$installer->endSetup();
