<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;

$installer->startSetup();
$installer->getConnection()->addColumn($this->getTable('product_match_rule'), 'locked', [
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'nullable' => true,
    'required' => false,
    'default' => 0,
    'comment' => "Locked",
]);

$installer->endSetup();
