<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Block_Adminhtml_Catalog_Product_Tree_Category
 */
class Omnius_ProductMatchRule_Block_Adminhtml_Catalog_Product_Tree_Category
    extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Categories
{
    /**
     * Specify template to use
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('productmatchrule/categories/tree.phtml');
    }

    /**
     * Returns URL for loading tree
     *
     * @return string
     */
    public function getLoadTreeUrlMatch()
    {
        return $this->getUrl('adminhtml/catalog_product/categoriesJson', array('_current' => true));
    }
}