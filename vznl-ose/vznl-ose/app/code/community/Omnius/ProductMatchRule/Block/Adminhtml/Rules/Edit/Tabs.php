<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_ProductMatchRule_Block_Adminhtml_Rules_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Omnius_ProductMatchRule_Block_Adminhtml_Rules_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('rules_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('productmatchrule')->__('Item Information'));
    }

    /**
     * @return mixed
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('productmatchrule')->__('Item Information'),
            'title' => Mage::helper('productmatchrule')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('productmatchrule/adminhtml_rules_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}