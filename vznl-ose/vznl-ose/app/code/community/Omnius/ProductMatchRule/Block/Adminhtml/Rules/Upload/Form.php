<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Block_Adminhtml_Rules_Upload_Form
 */
class Omnius_ProductMatchRule_Block_Adminhtml_Rules_Upload_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                "id" => "upload_form",
                "action" => $this->getUrl("*/*/upload"),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset("matchrules_form", array("legend" => Mage::helper("productmatchrule")->__("MatchRules File Info")));

        $fieldset->addField('file', 'file', array(
            'label' => Mage::helper('productmatchrule')->__('CSV File'),
            'name' => 'file',
            "class" => "required-entry",
            "required" => true,
        ));

        return parent::_prepareForm();
    }
}