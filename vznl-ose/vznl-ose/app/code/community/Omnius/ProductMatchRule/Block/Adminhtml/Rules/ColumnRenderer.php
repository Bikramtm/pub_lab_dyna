<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_ProductMatchRule_Block_Adminhtml_Rules_ColumnRenderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    protected $_isExport = false;

    /**
     * Renders the columns for the grid based on the operation type. 
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        if ($this->getColumn()->getIndex() == 'left_id') {
            switch ($row->getOperationType()) {
                case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                    $model = 'product';
                    $exportField = 'sku';
                    break;
                case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                    $model = 'category';
                    $exportField = 'url_key';
                    break;
                default:
                    break;
            }

            $item = Mage::getModel('catalog/' . $model)->load($row->getLeftId());

            return $this->_isExport ? $item->getData($exportField) : ($item->getTypeId() == "configurable" ? sprintf("%s (configurable)",
                $item->getName()) : $item->getName());
        } else {
            switch ($row->getOperationType()) {
                case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                    $model = 'product';
                    $exportField = 'sku';
                    break;
                case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                    $model = 'category';
                    $exportField = 'url_key';
                    break;
                default:
                    break;

            }
            $item = Mage::getModel('catalog/' . $model)->load($row->getRightId());

            return $this->_isExport ? $item->getData($exportField) : ($item->getTypeId() == "configurable" ? sprintf("%s (configurable)",
                $item->getName()) : $item->getName());
        }
    }

    /**
     * @param Varien_Object $row
     * @return string
     */
    public function renderExport(Varien_Object $row)
    {
        $this->_isExport = true;

        return $this->render($row);
    }
}
