<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Block_Adminhtml_Rules_WebsiteRenderer
 */
class Omnius_ProductMatchRule_Block_Adminhtml_Rules_WebsiteRenderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    protected $_isExport = false;

    /**
     * Renders the website column of the product match rules grid. 
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $websitesNames = "";

        $ruleCollection = Mage::getModel('productmatchrule/rule')
            ->getCollection()
            ->addFieldToFilter('operation_type', $row->getOperationType())
            ->addFieldToFilter('left_id', $row->getLeftId())
            ->addFieldToFilter('right_id', $row->getRightId())
            ->addFieldToFilter('operation', $row->getOperation())
            ->load();

        foreach ($ruleCollection as $rule) {
            $website = Mage::getModel('core/website')->load($rule->getWebsiteId());

            // When exporting, export the website id, not website name
            if ($this->_isExport) {
                if ($website->getId()) {
                    $ruleWebsite = $website->getId();
                    $websitesNames = $websitesNames ? $websitesNames . "," . $ruleWebsite : $ruleWebsite;
                }
            } else {
                if ($website->getName()) {
                    $ruleWebsite = $website->getName();
                    $websitesNames = $websitesNames ? $websitesNames . ",<br>" . $ruleWebsite : $ruleWebsite;
                }
            }

        }

        return $websitesNames;
    }

    /**
     * Render column for export
     *
     * @param Varien_Object $row
     * @return string
     */
    public function renderExport(Varien_Object $row)
    {
        $this->_isExport = true;
        return $this->render($row);
    }
}