<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_MatchController
 */
class Omnius_ProductMatchRule_MatchController extends Mage_Core_Controller_Front_Action
{
    /** @var Omnius_ProductMatchRule_Model_Rule */
    protected $_ruler;

    /**
     * @return Omnius_ProductMatchRule_Model_Rule
     */
    protected function getRuler()
    {
        if (!$this->_ruler) {
            $this->_ruler = Mage::getSingleton('productmatchrule/rule');
        }

        return $this->_ruler;
    }

    /**
     * @param $body
     * @param $error
     * @param string $type
     * @return bool
     *
     * Return response based on type, body or error
     */
    protected function returnResponse($body, $error = null, $type = 'application/json')
    {
        $this->getResponse()
            ->setHeader('Content-Type', $type)
            ->setBody(Mage::helper('core')->jsonEncode(
                $error ? array('error' => $error) : array($body)
            ));

        return true;
    }
}
