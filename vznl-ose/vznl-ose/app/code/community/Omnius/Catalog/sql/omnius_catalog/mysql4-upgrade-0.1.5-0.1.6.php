<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();
$entityTypeId = $installer->getEntityTypeId('catalog_product');

$attributes = [
    Omnius_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR => [
        'group' => 'General',
        'label' => 'Identifier package type',
        'input' => 'select',
        'type' => 'int',
        'backend' => 'eav/entity_attribute_backend_array',
        'option' => [
            'values' => Omnius_Catalog_Model_Product::$sections,
        ],
    ],
    Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR => [
        'group' => 'General',
        'label' => 'Identifier package subtype',
        'input' => 'select',
        'type' => 'int',
        'backend' => 'eav/entity_attribute_backend_array',
    ],
];

foreach ($attributes as $attributeCode => $options) {
    $options['user_defined'] = 1;
    if (!$installer->getAttribute($entityTypeId, $attributeCode)) {
        $installer->addAttribute($entityTypeId, $attributeCode, $options);
    }
}

$installer->endSetup();
