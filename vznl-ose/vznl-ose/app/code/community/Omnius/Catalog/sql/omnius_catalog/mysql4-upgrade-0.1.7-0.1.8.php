<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$attributeCode = "identifier_commitment_months";

if (! $installer->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $attributeCode)) {
    $attributeData = [
        'group' => 'General',
        'label' => 'Contractduur in maanden',
        'input' => 'select',
        'type' => 'int',
        'backend' => 'eav/entity_attribute_backend_array',
        'required' => true,
        'option' => [
            'values' => [
                "addon",
                "1",
                "12",
                "24",
                "36",
            ],
        ],
    ];

    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode, $attributeData);
}

$installer->endSetup();
