<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->updateAttribute('catalog_product', 'agent_groups_visibility', 'apply_to', 'grouped');

$installer->endSetup();
