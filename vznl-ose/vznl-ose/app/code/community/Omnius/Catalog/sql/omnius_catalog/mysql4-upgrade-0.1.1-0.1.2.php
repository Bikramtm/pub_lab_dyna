<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$this->startSetup();

$connection = $this->getConnection();
$connection->addColumn($this->getTable('catalog/category'), 'unique_id',
    [
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment'   => 'Unique Id',
        'length' => 64,
        'required'  => false,
        'after'     => 'entity_id',
        'nullable'  => true
    ]
);

$connection->addIndex(
    $this->getTable('catalog/category'),
    'IDX_CATALOG_CATEGORY_ENTITY_UNIQUE_ID',
    'unique_id'
    );
$this->endSetup();