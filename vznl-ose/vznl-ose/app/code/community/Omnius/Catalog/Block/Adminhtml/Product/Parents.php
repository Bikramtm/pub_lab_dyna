<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Catalog_Block_Adminhtml_Product_Parents
 */
class Omnius_Catalog_Block_Adminhtml_Product_Parents extends Mage_Adminhtml_Block_Widget_Grid
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    private $product;

    public function __construct()
    {
        parent::__construct();
        $this->setId("parentsGrid");
        $this->setProductId($this->getRequest()->getParam('id'));
        $this->product = Mage::getModel('catalog/product')->load($this->getProductId());
        $this->setDefaultSort("product_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    /**
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('omnius_catalog')->__('Parent Products');
    }

    /**
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('omnius_catalog')->__('Parent Products');
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Retrieve Grid JavaScript object name
     *
     * @return string
     */
    public function getGridJsObject()
    {
        return $this->getChild('grid')->getJsObjectName();
    }

    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        if ($this->product->getId()) {
            $siblingIds = [];
            // Getting parent products ids
            $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($this->product->getId());

            $siblingIds = array_merge($siblingIds, $parentIds);
            // For each of the parents get the other children
            foreach ($parentIds as $parentId) {
                $siblings = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($parentId);
                $siblingIds += $siblings[0];
            }
            // Remove this product from the list, so it does not show as sibling
            foreach ($siblingIds as $key => $sib) {
                if ($sib == $this->product->getId()) {
                    unset($siblingIds[$key]);
                }
            }
            /** @var Omnius_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getModel("catalog/product")->getCollection();
            $collection
                ->addAttributeToSelect('*')
                ->addFieldToFilter('entity_id', ['in' => $siblingIds]);
            $this->setCollection($collection);
        }
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Prepare columns
     *
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn("entity_id", array(
            "header" => Mage::helper("catalog")->__("ID"),
            "align" => "right",
            "width" => "60px",
            "type" => "number",
            "index" => "entity_id",
        ));

        $this->addColumn("name", array(
            "header" => Mage::helper("catalog")->__("Name"),
            "align" => "left",
            "type" => "text",
            "index" => "name",
        ));

        $sets = Mage::getModel('eav/entity_attribute_set')->getCollection()
            ->setEntityTypeFilter($this->product->getResource()->getTypeId())
            ->load()
            ->toOptionHash();
        $this->addColumn('set_name',
            array(
                'header'=> Mage::helper('catalog')->__('Attrib. Set Name'),
                'width' => '130px',
                'index' => 'attribute_set_id',
                'type'  => 'options',
                'options' => $sets,
            ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('catalog')->__('SKU'),
            'width'     => '80px',
            'index'     => 'sku'
        ));

        $this->addColumn('price', array(
            'header'    => Mage::helper('catalog')->__('Price'),
            'type'      => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index'     => 'price'
        ));

        $this->addColumn('is_saleable', array(
            'header'    => Mage::helper('catalog')->__('Inventory'),
            'renderer'  => 'adminhtml/catalog_product_edit_tab_super_config_grid_renderer_inventory',
            'filter'    => 'adminhtml/catalog_product_edit_tab_super_config_grid_filter_inventory',
            'index'     => 'is_saleable'
        ));

        return parent::_prepareColumns();
    }
}
