<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Catalog_Helper_Setup extends Mage_Core_Helper_Abstract
{
    /**
     * @param string $code Attribute code
     * @param string $label Attribute label
     * @param string $attribute_type Attribute type ( text, textarea, dropdown .. )
     * @param array $product_type The type of products to which to apply the attribute.
     */
    public function createAttribute($code, $label, $attribute_type, $product_type)
    {
        $_attribute_data = array(
            'attribute_code' => $code,
            'is_global' => '1',
            'frontend_input' => $attribute_type,
            'default_value_text' => '',
            'default_value_yesno' => '0',
            'default_value_date' => '',
            'default_value_textarea' => '',
            'is_unique' => '0',
            'is_required' => '0',
            'apply_to' => $product_type,
            'is_configurable' => '0',
            'is_searchable' => '0',
            'is_visible_in_advanced_search' => '0',
            'is_comparable' => '0',
            'is_used_for_price_rules' => '0',
            'is_wysiwyg_enabled' => '0',
            'is_html_allowed_on_front' => '1',
            'is_visible_on_front' => '1',
            'used_in_product_listing' => '1',
            'used_for_sort_by' => '0',
            'frontend_label' => array($label)
        );
        $model = Mage::getModel('catalog/resource_eav_attribute');
        if (!isset($_attribute_data['is_configurable'])) {
            $_attribute_data['is_configurable'] = 0;
        }
        if (!isset($_attribute_data['is_filterable'])) {
            $_attribute_data['is_filterable'] = 0;
        }
        if (!isset($_attribute_data['is_filterable_in_search'])) {
            $_attribute_data['is_filterable_in_search'] = 0;
        }
        if (is_null($model->getIsUserDefined()) || $model->getIsUserDefined() != 0) {
            $_attribute_data['backend_type'] = $model->getBackendTypeByInput($_attribute_data['frontend_input']);
        }

        if ($_attribute_data['frontend_input'] == 'weee') {
            $_attribute_data['backend_model'] = 'weee/attribute_backend_weee_tax';
        }

        $model->addData($_attribute_data);
        $model->setEntityTypeId(Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId());
        $model->setIsUserDefined(1);
        try {
            $model->save();
        } catch (Exception $e) {
            echo '<p>Sorry, error occured while trying to save the attribute. Error: ' . $e->getMessage() . '</p>';
        }
    }

    /**
     * Sets the attribute to the designated attribute sets.
     * @param string $entity_type Entity t
     * @param string $attribute_code
     * @param array|null $attrSets
     */
    public function addAttributeToAttributeSet($entity_type, $attribute_code, $attrSets = null)
    {
        $attSet = Mage::getModel('eav/entity_type')->getCollection()->addFieldToFilter('entity_type_code', $entity_type)->getFirstItem(); // This is because the you adding the attribute to catalog_products entity ( there is different entities in magento ex : catalog_category, order,invoice... etc )
        $attSetCollection = Mage::getModel('eav/entity_type')->load($attSet->getId())->getAttributeSetCollection(); // this is the attribute sets associated with this entity
        $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setCodeFilter($attribute_code)
            ->getFirstItem();
        $attId = $attributeInfo->getId();
        foreach ($attSetCollection as $a) {
            $set = Mage::getModel('eav/entity_attribute_set')->load($a->getId());
            $setId = $set->getId();
            if ((is_array($attrSets) && in_array($set->getAttributeSetName(), $attrSets)) || $attrSets == null) {
                $group = Mage::getModel('eav/entity_attribute_group')->getCollection()->addFieldToFilter('attribute_set_id', $setId)->setOrder('sort_order', 'ASC')->getFirstItem();
                $groupId = $group->getId();
                $newItem = Mage::getModel('eav/entity_attribute');
                $newItem
                // catalog_product eav_entity_type id ( usually 10 )
                    ->setEntityTypeId($attSet->getId())
                // Attribute Set ID
                ->setAttributeSetId($setId)
                // Attribute Group ID ( usually general or whatever based on the query i automate to get the first attribute group in each attribute set )
                ->setAttributeGroupId($groupId)
                // Attribute ID that need to be added manually
                ->setAttributeId($attId)
                // Sort Order for the attribute in the tab form edit
                ->setSortOrder(10)
                ->save();
            }
        }
    }

    /**
     * Removes an attribute from an attribute set.
     * @param string $attributeSetCode
     * @param string $attributeCode
     * @return bool
     */
    public function removeAttributeFromAttributeSet($attributeSetCode, $attributeCode)
    {
        $attributeSet = Mage::getModel('eav/entity_attribute_set')->getCollection()
            ->addFieldToFilter('attribute_set_name', $attributeSetCode)
            ->addFieldToFilter('entity_type_id', 4)
            ->getFirstItem();

        $attribute = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setCodeFilter($attributeCode)
            ->getFirstItem();

        return Mage::getModel('catalog/product_attribute_set_api')
            ->attributeRemove($attribute->getId(), $attributeSet->getId());
    }

    /**
     * Changes options for attributes
     * @param $attributeCode string
     * @param $options array
     */
    public function changeAttributeOptions($attributeCode, $options)
    {
        $attribute = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setCodeFilter($attributeCode)
            ->getFirstItem();

        if ($attribute->getId()) {
            foreach ($options as $key => $value) {
                $attribute->setData($key, $value)->save();
            }
        }
    }
}
