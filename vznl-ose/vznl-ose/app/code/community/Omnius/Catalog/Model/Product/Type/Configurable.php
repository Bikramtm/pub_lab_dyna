<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Catalog_Model_Product_Type_Configurable extends Mage_Catalog_Model_Product_Type_Configurable
{
    /**
     * Save configurable product depended data
     *
     * @param  Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Model_Product_Type_Configurable
     */
    public function save($product = null)
    {
        parent::save($product);

        /**
         * Save product relations
         */
        $data = $this->getProduct($product)->getDefaultCombinationData();
        Mage::getResourceModel('catalog/product_type_configurable')
            ->saveDefaultCombination($this->getProduct($product), $data);
        return $this;
    }
}