<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Catalog_Model_Resource
 */
class Omnius_Catalog_Model_Resource_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{
    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        /**
         * Assure attributes for package type and subtype load by default
         */

        $this->addAttributeToSelect(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR)
            ->addAttributeToSelect(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);

        return $this;
    }

    /**
     * Get available filters
     *
     * @return array
     */
    public function getAvailableFilters()
    {
        $filters = array();
        /** @var Omnius_Catalog_Model_Product $item */
        foreach ($this->getItems() as $item) {
            $filters = $this->array_merge_recursive($filters, $item->getAvailableFilters());
        }
        return $filters;
    }

    /**
     * Merges two or more arrays and flattens them
     *
     * @return mixed
     */
    protected function array_merge_recursive()
    {
        $arrays = func_get_args();
        $base = array_shift($arrays);
        foreach ($arrays as $array) {
            reset($base);
            while (list($key, $value) = @each($array)) {
                if (is_array($value) && @is_array($base[$key])) {
                    $base[$key] = $this->array_merge_recursive($base[$key], $value);
                } else {
                    $base[$key] = $value;
                }
            }
        }

        return $base;
    }
}
