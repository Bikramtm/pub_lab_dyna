<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Product type price model
 *
 * @category    Dyna
 * @package     Omnius_Catalog
 */
class Omnius_Catalog_Model_Product_Type_Price extends Mage_Catalog_Model_Product_Type_Price
{

    /**
     * Default action to get price of product
     *
     * @return decimal
     */

    /**
     * Get base price with apply Group, Tier, Special prises
     *
     * @param Mage_Catalog_Model_Product $product
     * @param float|null $qty
     *
     * @return float
     */
    public function getBaseMaf($product, $qty = null)
    {
        return (float) $product->getMaf();
    }


    /**
     * Retrieve product final price
     *
     * @param float|null $qty
     * @param Mage_Catalog_Model_Product $product
     * @return float
     */
    public function getFinalMaf($qty, $product)
    {
        if (is_null($qty) && !is_null($product->getCalculatedFinalMaf())) {
            return $product->getCalculatedFinalMaf();
        }

        $finalPrice = $this->getBaseMaf($product, $qty);
        $product->setFinalMaf($finalPrice);

        return $finalPrice;
    }

    /**
     * @param $product
     * @param $productQty
     * @param $childProduct
     * @param $childProductQty
     * @return float
     */
    public function getChildFinalMaf($product, $productQty, $childProduct, $childProductQty)
    {
        return $this->getFinalMaf($childProductQty, $childProduct);
    }

    /**
     * Calculate product price based on price rules
     *
     * @param   float $basePrice
     * @param   float|null|false $rulePrice
     * @param   mixed $wId
     * @param   mixed $gId
     * @param   null|int $productId
     * @return  float
     */
    public static function calculateMaf($basePrice,
            $rulePrice = false, $wId = null, $gId = null, $productId = null)
    {
        Varien_Profiler::start('__PRODUCT_CALCULATE_MAF__');
        if ($wId instanceof Mage_Core_Model_Store) {
            $sId = $wId->getId();
            $wId = $wId->getWebsiteId();
        } else {
            $sId = Mage::app()->getWebsite($wId)->getDefaultGroup()->getDefaultStoreId();
        }

        $finalPrice = $basePrice;
        if ($gId instanceof Mage_Customer_Model_Group) {
            $gId = $gId->getId();
        }

        if ($rulePrice === false) {
            $storeTimestamp = Mage::app()->getLocale()->storeTimeStamp($sId);
            $rulePrice = Mage::getResourceModel('catalogrule/rule')
                ->getRulePrice($storeTimestamp, $wId, $gId, $productId);
        }

        if ($rulePrice !== null && $rulePrice !== false) {
            $finalPrice = min($finalPrice, $rulePrice);
        }

        $finalPrice = max($finalPrice, 0);
        Varien_Profiler::stop('__PRODUCT_CALCULATE_MAF__');
        return $finalPrice;
    }
}
