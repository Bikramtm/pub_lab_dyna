<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Catalog_Model_Resource_Product_Type_Configurable extends Mage_Catalog_Model_Resource_Product_Type_Configurable
{
    /**
     * Save default combination
     *
     * @param $mainProduct
     * @param $defaultProductId
     * @return $this
     */
    public function saveDefaultCombination($mainProduct, $defaultProductId)
    {
        $isProductInstance = false;
        if ($mainProduct instanceof Mage_Catalog_Model_Product) {
            $mainProductId = $mainProduct->getId();
            $isProductInstance = true;
        } else {
            $mainProductId = $mainProduct;
        }

        if ($defaultProductId && $isProductInstance) {
            $mainProduct->setIsRelationsChanged(true);
        }

        $newDefaultLink = $this->_getReadAdapter()->select()
            ->from($this->getMainTable(), array('product_id'))
            ->where('parent_id = ?', $mainProductId)
            ->where('product_id = ?', $defaultProductId);

        if ( $this->_getReadAdapter()->fetchOne($newDefaultLink) ) {
            //unset old default combination
            $where = array(
                'parent_id = ?' => $mainProductId,
                'is_default_combination = ?' => true,
                'product_id != ?' => (int)$defaultProductId
            );
            $this->_getWriteAdapter()->update($this->getMainTable(), array( 'is_default_combination' => false ), $where);

            //set new default combination
            $where = array(
                'parent_id = ?'     => $mainProductId,
                'product_id = ?'    => (int)$defaultProductId
            );
            $this->_getWriteAdapter()->update($this->getMainTable(), array( 'is_default_combination' => true ), $where);
        }

        return $this;
    }

    /**
     * Get default combination
     *
     * @param $product
     * @return string
     */
    public function getDefaultCombination( $product )
    {
        $sql = $this->_getReadAdapter()->select()
            ->from($this->getMainTable(), array('product_id'))
            ->where('parent_id = ?', $product->getId())
            ->where('is_default_combination = ?', true);

        $id = $this->_getReadAdapter()->fetchOne($sql);
        return $id;
    }
}