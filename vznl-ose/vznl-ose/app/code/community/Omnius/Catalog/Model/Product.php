<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Product
 */
class Omnius_Catalog_Model_Product extends Mage_Catalog_Model_Product
{
    CONST CATALOG_PACKAGE_TYPE_ATTR = 'identifier_package_type';
    CONST CATALOG_PACKAGE_SUBTYPE_ATTR = 'identifier_package_subtype';
    CONST CATALOG_CONSUMER_TYPE = 'identifier_cons_bus';
    CONST SIM_ONLY_KEY = 'identifier_subscr_category';
    CONST POSTP_SIMONLY_KEY = 'identifier_subsc_postp_simonly';
    CONST SIM_ONLY_VALUE = 'Sim Only';
    CONST IDENTIFIER_CONSUMER = 'Consumer';
    CONST IDENTIFIER_BUSINESS = 'Business';
    CONST SIM_TYPE_ATTRIBUTE = 'identifier_simcard_type';
    CONST MEASUREMENT_UNIT = 'prodspecs_afmetingen_unit';
    CONST OPERATING_SYSTEM_ATTRIBUTE = 'prodspecs_besturingssysteem';
    CONST DEFAULT_SIM_ATTRIBUTE = 'identifier_simcard_formfactor';
    CONST ADDON_TYPE_ATTR = 'identifier_addon_type';
    CONST AVAILABLE_SIM_TYPES_ATTRIBUTE = 'identifier_simcard_allowed';
    CONST REASONCODE_TYPE_ATTRIBUTE = 'identifier_reasoncode';
    CONST SIM_SELECTION_ATTRIBUTE = 'identifier_simcard_selector';
    CONST SUBSCRIPTION_DURATION = 'identifier_commitment_months';
    CONST SIM_SELECTION_MULTISIM = 'multisim';
    CONST SIM_SELECTION_SIMSWAP = 'simswap';

    public static $sections = [
        Omnius_Catalog_Model_Type::SUBTYPE_DEVICE => [Omnius_Catalog_Model_Type::SUBTYPE_DEVICE],
        Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION => [Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION],
        Omnius_Catalog_Model_Type::SUBTYPE_ADDON => [Omnius_Catalog_Model_Type::SUBTYPE_ADDON],
        Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY => [Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY]
    ];

    /** @var Omnius_Configurator_Helper_Attribute */
    protected $_attrHelper;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /** @var string */
    protected $_type = null;

    /**
     * Get product subtype (addon, device, subscription, etc)
     */
    public function getType()
    {
        if ($this->getId() !== null) {
            if ($this->_type === null) {
                $key = sprintf('%s_product_type_%s', $this->getId(), Mage::app()->getWebsite()->getId());
                if (!($type = $this->getCache()->load($key))) {
                    $attribute = $this->getResource()->getAttribute(static::CATALOG_PACKAGE_SUBTYPE_ATTR);
                    $type = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, $this->getData(static::CATALOG_PACKAGE_SUBTYPE_ATTR));
                    $this->getCache()->save($type, $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
                }
                $this->_type = json_decode($type, true);
            }
        } else {
            // This is a fake product, deleted from our database
            if ($this->_type === null) {
                $this->_type = $this->getData('type');
            }
        }

        return $this->_type ?: [];
    }

    /**
     * @return bool|false|Mage_Core_Model_Abstract|mixed
     *
     * Get the attribute set of a product
     */
    public function getAttributeSet()
    {
        $key = sprintf('%s_attr_set', $this->getId());
        if (!($group = $this->getCache()->load($key))) {
            $group = Mage::getModel('eav/entity_attribute_set')
                ->load($this->getAttributeSetId())
                ->getAttributeSetName();
            $this->getCache()->save("{$group}", $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $group;
    }

    /**
     * Determines whether a product is a simcard or not. This is based on the "identifier_package_subtype" attribute
     *
     * @return bool
     */
    public function isSim()
    {
        return in_array(Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD, $this->getType());
    }

    /**
     * Checks if a product is of type Promotional. This is determined based on the attribute set
     *
     * @return bool
     */
    public function isPromo()
    {
        return $this->getAttributeSet() == Omnius_Configurator_Model_AttributeGroup::ATTR_GROUP_PROMOTIONS;
    }

    /**
     * Checks if a product is a service item. This is determined based on the attribute set
     *
     * @return bool
     */
    public function isServiceItem()
    {
        return $this->getAttributeSet() == Omnius_Configurator_Model_AttributeGroup::ATTR_SERVICE_ITEM;
    }

    /**
     * @return array|mixed
     */
    public function getAttributesInfo()
    {
        $key = sprintf('%s_attributes_info', $this->getId());
        if (!($data = unserialize($this->getCache()->load($key)))) {
            $data = [];
            /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
            foreach ($this->getAttributes() as $attribute) {
                $value = $attribute->getFrontend()->getValue($this);
                if ($attribute->getFrontendInput() == 'price') {
                    $value = Mage::app()->getStore()->convertPrice($value, true);
                } elseif ($attribute->getAttributeCode() == 'weight') {
                    $value = Zend_Locale_Format::toFloat($value, array('locale' => Mage::app()->getLocale()->getLocaleCode(), 'precision' => 0));
                } elseif (!$attribute->getIsHtmlAllowedOnFront()) {
                    $value = Mage::helper('core')->escapeHtml($value);
                }
                $groupName = Mage::getModel('eav/entity_attribute_group')->load($attribute->getData('attribute_group_id'))
                    ->getAttributeGroupName();
                if ( ! empty($groupName)) {
                    $data[$attribute->getFrontend()->getLabel()] = array(
                        'label' => $attribute->getFrontend()->getLabel(),
                        'value' => $value,
                        'code'  => $attribute->getAttributeCode(),
                        'group'  => strtolower($groupName),
                        'type'  => $attribute->getFrontendInput(),
                    );
                }
            }
            $this->getCache()->save(serialize($data), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $data;
    }

    /**
     * Check if a product is any of the hardware types (device/sim/accesory)
     */
    public function isOfHardwareType()
    {
        return $this->is(Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD) || $this->is(Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY) || $this->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE);
    }

    /**
     * Check if product can be Dead On Arrival.
     *
     * @return bool
     */
    public function canBeDOA()
    {
        return $this->is(Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY) || $this->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE);
    }

    /**
     * @return bool
     */
    public function isEndOfLife()
    {
        $stockStatuses = $this->getStockStatuses();
        $stockStatusAttributeId = $this->getData('stock_status');
        $storeIds = $this->getStoreIds();
        $currentStoreId = Mage::app()->getStore()->getId();

        $hasStatus = $this->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED
            || ($stockStatusAttributeId != null && $stockStatuses[$stockStatusAttributeId]);

        return ((bool) $this->getIsDeleted())
            || $hasStatus
            || !in_array($currentStoreId, $storeIds);
    }

    /**
     * Gathers all the stocks statuses available for the products.
     * @return array
     * @throws Mage_Core_Exception
     */
    public function getStockStatuses()
    {
        $stockStatusAttribute = Mage::getResourceModel('catalog/product')->getAttribute('stock_status');
        $stockStatusOptions = $stockStatusAttribute->getSource()->getAllOptions(true, true);
        $stockStatuses = array();
        foreach ($stockStatusOptions as $ct) {
            if ($ct['value']) {
                $stockStatuses[$ct['value']] = strpos(strtolower('End of Life (Niet meer leverbaar)'), strtolower($ct['label'])) !== false ? true : false;
            }
        }

        return $stockStatuses;
    }

    /**
     * Get available filters
     *
     * @return array|mixed
     */
    public function getAvailableFilters()
    {
        $key = sprintf('%s_available_filters', $this->getId());
        if (!($filters = unserialize($this->getCache()->load($key)))) {
            $filters = [];
            $store = Mage::app()->getStore();
            $localeCode = Mage::app()->getLocale()->getLocaleCode();
            /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attr */
            foreach ($this->getAttributes() as $attr) {
                if ($attr->getIsFilterable()) {
                    Mage::helper('omnius_catalog')->addFilter($attr, $filters, $store, $localeCode, $this);
                }
            }
            $this->getCache()->save(serialize($filters), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $filters;
    }

    /**
     * @return Omnius_Configurator_Helper_Attribute
     */
    protected function getAttrHelper()
    {
        if ( ! $this->_attrHelper) {
            $this->_attrHelper = Mage::helper('omnius_configurator/attribute');
        }

        return $this->_attrHelper;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * Get product final maf
     *
     * @param double $qty
     * @return double
     */
    public function getFinalMaf($qty=null)
    {
        $price = $this->_getData('final_maf');
        if ($price !== null) {
            return $price;
        }
        return $this->getPriceModel()->getFinalMaf($qty, $this);
    }

    /**
     * Returns calculated final maf
     *
     * @return float
     */
    public function getCalculatedFinalMaf()
    {
        return $this->_getData('calculated_final_maf');
    }

    /**
     * Get regular maf price or maf price if exists
     */
    public function getMaf()
    {
        if (($price = $this->_getData('maf')) !== null) {
            return $price;
        }

        return $this->getPromotionalMaf() ?: ($this->getRegularMaf() ?: false);
    }

    /**
     * Fetches all simple products as an associative array where the
     * key is the product id and the value is the product sku.
     * @return array
     */
    public function getProductSkusForDropdown($forJs = false)
    {
        $key = md5('products_for_dropdown_sku' . (int) $forJs);
        if (!($options = (unserialize($this->getCache()->load($key))))) {
            $products = Mage::helper('omnius_catalog')->getProductCollection();
            $options = [];
            $counter = 0;
            foreach ($products as $productId => $product) {
                $index = $forJs ? $counter : $productId;
                $options[$index] = array(
                    'value' => $product['sku'],
                    'data' => $productId
                );
                ++$counter;
            }
            unset($products);
            $this->getCache()->save(serialize($options), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $options;
    }

    /**
     * Check if the product is a simonly product
     */
    public function getSimOnly()
    {
        return (strtolower($this->getAttributeText(static::POSTP_SIMONLY_KEY)) == strtolower(static::SIM_ONLY_VALUE));
    }

    /**
     * Return the possible options for the product
     */
    public function getSimOptions()
    {
        return $this->getAttributeText(static::AVAILABLE_SIM_TYPES_ATTRIBUTE);
    }

    /**
     * Returns an array containing all default product types
     *
     * @param null $id
     * @return array|mixed
     */
    protected function _getDefaultTypes($id = null)
    {
        $key = md5(serialize(array(__METHOD__, $id)));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $typeAttribute = Mage::getResourceModel('catalog/product')->getAttribute(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $result = $typeAttribute->getSource()->getAllOptions(false, true);
            if ($id) {
                $result = isset($result[$id]) ? $result[$id] : null;
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Returns an array containing all default product subtypes
     *
     * @param null $id
     * @return array|mixed
     */
    protected function _getDefaultSubTypes($id = null)
    {
        $key = md5(serialize(array(__METHOD__, $id)));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $subTypeAttribute = Mage::getResourceModel('catalog/product')->getAttribute(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
            $result = $subTypeAttribute->getSource()->getAllOptions(false, true);
            if ($id) {
                $result = isset($result[$id]) ? $result[$id] : null;
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Get the value of the measurement unit
     */
    public function getUnitValue()
    {
        return $this->getAttributeText(static::MEASUREMENT_UNIT);
    }

    /**
     * Get the operating system value
     */
    public function getOperatingSystem()
    {
        return $this->getAttributeText(static::OPERATING_SYSTEM_ATTRIBUTE);
    }

    /**
     * Return the price as retrieved from db without any calculations
     * @return mixed
     */
    public function getBasicPrice()
    {
        return $this->getData('price');
    }

    /**
     * Check if a product is of a specific type
     * @param $type
     * @return bool
     */
    public function is($type)
    {
        return in_array($type, $this->getType());
    }

    /**
     * Logically delete a product and update the fields that are unique suffixing the product id to ensure uniqueness
     *
     * @return $this
     */
    public function doDelete()
    {
        /**
         * Keep initial SKU reference as
         * it will be changed later on
         */
        $sku = $this->getSku();
        $newSku = sprintf('%s|%s', $sku, $this->getId());

        /**
         * Handle SKU update separately as it is used
         * as the identifier to match replica products
         * if we update it together with the other unique attrs
         * we will lose the link between master and replica product
         */
        $this->getResource()->getWriteConnection()->query(sprintf('UPDATE `catalog_product_entity` SET sku="%s" WHERE sku="%s"', $newSku, $sku));
        $this->setSku($newSku);

        // Get all stores
        $allStoreIds = array();
        foreach (Mage::app()->getStores() as $store) {
            $allStoreIds[$store->getId()] = $store->getId();
        }
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $codes = Mage::helper('omnius_catalog')->getCodesCollection();
        $codesArr = $codes->getColumnValues('attribute_code');
        foreach ($allStoreIds as $storeId) {
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($codesArr)
                ->addFieldToFilter('entity_id', $this->getId())
                ->getFirstItem();
            foreach ($codes as $code) {
                $attrCode = $code->getData('attribute_code');
                $attrType = $code->getData('frontend_input');
                if ($attrType == 'select' || $attrType == 'multiselect') {
                    // For these types set to 0
                    $product->setData($attrCode, 0);
                } else {
                    // Update the unique fields with the product ID to maintain uniqueness
                    $product->setData($attrCode, $product->getData($attrCode) . '|' . $product->getId());
                }
            }
            $product->save();
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
        foreach ($codes as $code) {
            $attrCode = $code->getData('attribute_code');
            $attrType = $code->getData('frontend_input');
            if ($attrType == 'select' || $attrType == 'multiselect') {
                // For these types set to 0
                $this->setData($attrCode, 0);
            } else {
                // Update the unique fields with the product ID to maintain uniqueness
                $this->setData($attrCode, $this->getData($attrCode) . '|' . $this->getId());
            }
        }
        $this->setData('is_deleted', true);

        $this->save();

        return $this;
    }

    /**
     * Restores a logically deleted product
     *
     * @return $this
     * @throws Exception
     */
    public function doRestore()
    {
        /**
         * Keep initial SKU reference as
         * it will be changed later on
         */
        $sku = $this->getSku();
        $temp = explode('|', $sku);
        $newSku = reset($temp);

        /**
         * Handle SKU update separately as it is used
         * as the identifier to match replica products
         * if we update it together with the other unique attrs
         * we will lose the link between master and replica product
         */
        $this->getResource()->getWriteConnection()->query(sprintf('UPDATE `catalog_product_entity` SET sku="%s" WHERE sku="%s"', $newSku, $sku));
        $this->setSku($newSku);

        // Get all stores
        $allStoreIds = array();
        foreach (Mage::app()->getStores() as $store) {
            $allStoreIds[$store->getId()] = $store->getId();
        }
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $codes = Mage::helper('omnius_catalog')->getCodesCollection();
        $codesArr = $codes->getColumnValues('attribute_code');
        foreach ($allStoreIds as $storeId) {
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($codesArr)
                ->addFieldToFilter('entity_id', $this->getId())
                ->getFirstItem();
            foreach ($codes as $code) {
                $attrCode = $code->getData('attribute_code');
                $attrType = $code->getData('frontend_input');
                if ($attrType != 'select' && $attrType != 'multiselect') {
                    // Update the unique fields with the product ID to maintain uniqueness
                    $temp = explode('|', $product->getData($attrCode));
                    $product->setData($attrCode, reset($temp));
                }
            }
            $product->save();
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
        foreach ($codes as $code) {
            $attrCode = $code->getData('attribute_code');
            $attrType = $code->getData('frontend_input');
            if ($attrType != 'select' && $attrType != 'multiselect') {
                // Update the unique fields with the product ID to maintain uniqueness
                $temp = explode('|', $this->getData($attrCode));
                $this->setData($attrCode, reset($temp));
            }
        }
        $this->setData('is_deleted', 0);

        /**
         * go ahead and save
         */
        $this->save();

        return $this;
    }

    /**
     * @return float
     *
     * Return tax rate for product
     */
    public function getTaxRate()
    {
        $store = Mage::app()->getStore('default');
        $taxCalculation = Mage::getModel('tax/calculation');
        $request = $taxCalculation->getRateRequest(null, null, null, $store);
        $taxClassId = $this->getTaxClassId();
        $percent = $taxCalculation->getRate($request->setProductClassId($taxClassId));

        return $percent;
    }

    /**
     * @param $stock
     * @param $backorders
     * @return bool
     */
    public function isSellable($stock, $backorders)
    {
        if (!$this->getStockStatus()) {
            return true;
        }

        return Mage::helper('omnius_catalog')->checkIfSellable($stock, $backorders, $this);
    }

    /**
     * Create duplicate
     *
     * @return Mage_Catalog_Model_Product
     */
    public function duplicate()
    {
        $this->getWebsiteIds();
        $this->getCategoryIds();

        $initStatus = $this->getStatus();
        $initVisibility = $this->getVisibility() ?: null;

        /* @var $newProduct Mage_Catalog_Model_Product */
        $newProduct = Mage::getModel('catalog/product')->setData($this->getData())
            ->setIsDuplicate(true)
            ->setOriginalId($this->getId())
            ->setSku(null)
            ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED)
            ->setCreatedAt(null)
            ->setUpdatedAt(null)
            ->setId(null)
            ->setStoreId(Mage::app()->getStore()->getId());

        Mage::dispatchEvent(
            'catalog_model_product_duplicate',
            array('current_product' => $this, 'new_product' => $newProduct)
        );

        /* Prepare Related*/
        $data = array();
        $this->getLinkInstance()->useRelatedLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getRelatedLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setRelatedLinkData($data);

        /* Prepare UpSell*/
        $data = array();
        $this->getLinkInstance()->useUpSellLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getUpSellLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setUpSellLinkData($data);

        /* Prepare Cross Sell */
        $data = array();
        $this->getLinkInstance()->useCrossSellLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getCrossSellLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setCrossSellLinkData($data);

        /* Prepare Grouped */
        $data = array();
        $this->getLinkInstance()->useGroupedLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getGroupedLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setGroupedLinkData($data);

        /**
         * Add a random SKU so that replication
         * will work normally
         */
        $newProduct
            ->setSku(function_exists('mt_rand') ? mt_rand(0, 9999999999999) : rand(0, 9999999999999))
            ->setStatus(1 === (int) $initStatus ? Mage_Catalog_Model_Product_Status::STATUS_DISABLED : Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE)
            ->setIsDeleted(0)
        ;

        $newProduct->save();

        $this->getOptionInstance()->duplicate($this->getId(), $newProduct->getId());
        $this->getResource()->duplicate($this->getId(), $newProduct->getId());

        /**
         * Replication fix
         */
        $p = Mage::getModel('catalog/product')->load($newProduct->getId());
        $p
            ->setStatus($initStatus)
            ->setVisibility($initVisibility)
            ->setIsDeleted(0)
            ->save();

        return $newProduct;
    }

    /**
     * Get the product for a sim
     *
     * @param $subscription
     * @param $simType
     * @param $simProd
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getSimProduct($subscription, $simType, $simProd)
    {
        $key = sprintf('%s_product_%s_type_%s_sim', $subscription->getProductId(), $simType, $simProd);
        if ($result = $this->getCache()->load($key)) {
            return $this->load($result);
        } else {
            if (!$simProd) {
                $productModel = Mage::getModel('catalog/product');
                $attr = $productModel->getResource()->getAttribute(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE);
                if ($attr->usesSource()) {
                    $simId = $attr->getSource()->getOptionId($simType);
                }

                $collection = Mage::getResourceSingleton('catalog/product_collection')
                    ->addAttributeToFilter('name', array('in' => $subscription->getProduct()->load($subscription->getProductId())->getAttributeText(Omnius_Catalog_Model_Product::AVAILABLE_SIM_TYPES_ATTRIBUTE)))
                    ->addAttributeToFilter(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE, $simId)
                    ->addOrder('name', Varien_Data_Collection::SORT_ORDER_ASC)
                    ->setPageSize(1)
                    ->setCurPage(1)
                    ->load();
            } else {
                $collection = Mage::getResourceSingleton('catalog/product_collection')
                    ->addAttributeToFilter('name', $simProd)
                    ->setPageSize(1)
                    ->setCurPage(1)
                    ->load();
            }

            if ($collection->count() < 1) {
                Mage::throwException('Invalid SIM type provided');
            }
            $result = $collection->getFirstItem();
            $this->getCache()->save($result->getId(), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * Return the text of the type (Device.. etc) instead of an array
     *
     * @return string
     */
    public function getTypeText()
    {
        if (!isset($this->_typeText)) {
            $type = $this->getType();
            if (is_array($type)) {
                $this->_typeText = array_pop($type);
            } else {
                $this->_typeText = $type;
            }
        }

        return $this->_typeText;
    }

    /**
     * Checks whether the product type belongs to the provided section.
     * @param $section
     * @return bool
     */
    public function isInSection($section)
    {
        $sections = $this->getSections();
        $type = $this->getType();

        return count(array_intersect(($sections[$section] ?? []), $type)) > 0;
    }

    /**
     * Returns sections array configurations for the frontend configurator.
     * @return array
     */
    public function getSections()
    {
        $cacheKey = 'product_sections_config';
        if ($result = $this->getCache()->load($cacheKey)) {
            return unserialize($result);
        } else {
            $result = [];
            foreach (static::$sections as $key => $section) {
                $result[strtolower($key)] = $section;
            }
        }
        $this->getCache()->save(serialize($result), $cacheKey, [Dyna_Cache_Model_Cache::CACHE_TAG], $this->getCache()->getTtl());

        return $result;
    }

    /**
     * Validate Product Data
     *
     * @return Omnius_Catalog_Model_Product
     * @throws Mage_Core_Exception
     */
    public function validate()
    {
        /**
         * Initialize product categories if is a save request
         */
        $categoryIds = Mage::app()->getRequest()->getPost('category_ids');
        if (null !== $categoryIds) {
            if (empty($categoryIds)) {
                $categoryIds = array();
            }
            $this->setCategoryIds($categoryIds);
        }

        // Ensure the product is assigned to only one family category
        if (count($this->getCategoryIds(true)) > 1) {
            throw new Mage_Core_Exception(Mage::helper('catalog')->__('The product can only be part of one family.'));
        }

        return parent::validate();
    }

    /**
     * Retrieve assigned category Ids
     * @param int $websiteId
     * @param $subtype string Subtype of the product
     * @return array
     */
    public function getStockCall($websiteId, $subtype, $axiStoreId)
    {
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        $product = Mage::helper('omnius_catalog')->getProductForStock($websiteId, $subtype, $cache);
        $products = Mage::helper('omnius_catalog')->getProductsForStock($websiteId, $subtype, $cache, $product);
        $result = [];

        foreach ($products as $id => $item) {
            $result = Mage::helper('omnius_catalog')->buildStockItem($result, $id, $item, []);
        }

        return $result;
    }

    /**
     * @param bool $onlyFamily
     * @return array|mixed
     */
    public function getCategoryIds($onlyFamily = false)
    {
        $key = md5(serialize([strtolower(__METHOD__), $this->getId(), $onlyFamily]));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            if ($onlyFamily === false) {
                $result = parent::getCategoryIds();
            } else {
                $all = parent::getCategoryIds();
                $collection = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToFilter('is_family', 1)
                    ->addFieldToFilter('entity_id', ['in' => $all])->load();
                $result = [];
                foreach ($collection as $category) {
                    $result[] = $category->getId();
                }
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * RFC-150046 - don't save product when sandbox it's activated and attributes and attribute sets differ from replicas
     *
     * @param string $exceptionMessage
     * @throws Mage_Adminhtml_Exception
     * @return Omnius_Catalog_Model_Product
     */
    public function cancelSave($exceptionMessage)
    {
        //Prevent product saving
        $this->_dataSaveAllowed = false;
        $this->setData('save_cancelled', true);

        throw new Mage_Adminhtml_Exception($exceptionMessage);
    }

    /**
     * @return int
     */
    public function getSubscriptionDuration()
    {
        return (int) $this->getAttributeText(static::SUBSCRIPTION_DURATION);
    }

    /**
     * Retrieve sku through type instance
     *
     * @return string
     */
    public function getSku()
    {
        $sku = parent::getSku();
        if ($this->getIsDeleted()) {
            // Make sure we return the proper sku when product is deleted
            $sku = explode("|", $sku)[0];
        }

        return $sku;
    }

    /**
     * @param $request
     */
    public function updateDataFromRequest($request)
    {
        /**
         * Check "Use Default Value" checkboxes values
         */
        if ($useDefaults = $request->getPost('use_default')) {
            foreach ($useDefaults as $attributeCode) {
                $this->setData($attributeCode, false);
            }
        }

        /**
         * Init product links data (related, upsell, crosssel)
         */
        $links = $request->getPost('links');
        if (isset($links['related']) && !$this->getRelatedReadonly()) {
            $this->setRelatedLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['related']));
        }
        if (isset($links['upsell']) && !$this->getUpsellReadonly()) {
            $this->setUpSellLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['upsell']));
        }
        if (isset($links['crosssell']) && !$this->getCrosssellReadonly()) {
            $this->setCrossSellLinkData(Mage::helper('adminhtml/js')
                ->decodeGridSerializedInput($links['crosssell']));
        }
        if (isset($links['grouped']) && !$this->getGroupedReadonly()) {
            $this->setGroupedLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['grouped']));
        }

        /**
         * Initialize product categories
         */
        $categoryIds = $request->getPost('category_ids');
        if (null !== $categoryIds) {
            if (empty($categoryIds)) {
                $categoryIds = array();
            }
            $this->setCategoryIds($categoryIds);
        }

        /**
         * Initialize data for configurable product
         */
        if (($data = $request->getPost('configurable_products_data'))
            && !$this->getConfigurableReadonly()
        ) {
            $this->setConfigurableProductsData(Mage::helper('core')->jsonDecode($data));
        }
        if (($data = $request->getPost('configurable_attributes_data'))
            && !$this->getConfigurableReadonly()
        ) {
            $this->setConfigurableAttributesData(Mage::helper('core')->jsonDecode($data));
        }
        if (($data = $request->getPost('is_default_combination'))
            && !$this->getConfigurableReadonly()
        ) {
            $this->setDefaultCombinationData($data);
        }

        $this->setCanSaveConfigurableAttributes(
            (bool) $request->getPost('affect_configurable_product_attributes')
            && !$this->getConfigurableReadonly()
        );
    }

    /**
     * Escape quotes from the product descriptions to avoid frontend crash
     */
    public function escapeData()
    {
        foreach ($this->getData() as $key => $value) {
            if (is_string($value)) {
                $this->setData($key, htmlentities($value, ENT_QUOTES));
            }
        }
    }

    /**
     * Update sim type
     */
    public function updateSimType()
    {
        if ($simType = $this->getResource()->getAttribute(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE)) {
            $simOptions = [];
            foreach ($simType->getFrontend()->getSelectOptions() as $simOption) {
                $simOptions[] = $simOption['label'];
            }
            $itemValue = $simType->getFrontend()->getValue($this);
            $itemValue = in_array($itemValue, $simOptions) ? $itemValue : '';
            $this->setData('sim_type', $itemValue);
        }
    }
}
