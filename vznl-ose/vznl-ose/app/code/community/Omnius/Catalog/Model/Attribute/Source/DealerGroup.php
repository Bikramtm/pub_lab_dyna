<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Catalog_Model_Attribute_Source_DealerGroup
 */
class Omnius_Catalog_Model_Attribute_Source_DealerGroup extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Get the list of all dealer groups
     *
     * @return array -- [[label => {name}, value => {group_id}], ....]
     */
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = Mage::getResourceModel('agent/dealergroup_collection')->getData();
            foreach ($this->_options as &$row) {
                $row['label'] = $row['name'];
                $row['value'] = $row['group_id'];
                unset($row['name']);
                unset($row['group_id']);
            }
        }

        return $this->_options;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}