<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Catalog_Model_Observer
 */
class Omnius_Catalog_Model_Observer
{
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Triggered after a product is saved
     */
    public function afterProductSave()
    {
        // cache clearing should be made manually to reduce the performance loss of clearing after every save
        return;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * Fix bug with saving only first 1000 products in a category
     * It's related to default php config 'max_input_vars' = 1000
     *
     * @param Varien_Event_Observer $observer
     */
    public function onCatalogCategoryPrepareSave(Varien_Event_Observer $observer)
    {
        $phpDefaultMaxInputVars = (int)ini_get('max_input_vars');
        /** @var Mage_Core_Controller_Request_Http $request */
        $request = $observer->getRequest();
        $dataProducts = $request->getPost('category_products');
        $dataProducts = preg_split('/&/', $dataProducts, -1, PREG_SPLIT_NO_EMPTY);
        /** @var Mage_Catalog_Model_Category $category */
        $category = $observer->getCategory();

        if ($dataProducts && !$category->getProductsReadonly() && count($dataProducts) > $phpDefaultMaxInputVars) {
            $products = array();
            foreach ($dataProducts as $_product) {
                $_product = trim($_product);
                if (preg_match('/([0-9]*)=([\-]?[0-9]*)/', $_product, $matches)) {
                    $products[$matches[1]] = $matches[2];
                }
            }
            $category->setPostedProducts($products);
        }
    }

    /**
     * @param $observer
     * @throws Dyna_ProductMatchRule_Model_RuleException
     */
    public function validateMatchRule($observer)
    {
        $data = $observer->getDataArray();

        // Only allow addons as the defaulted product
        if (
            $data['operation'] == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED
            && in_array($data['operation_type'], [Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P, Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P])
        ) {
            /** @var Omnius_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product')->load($data['right_id']);
            if ($product->is(Omnius_Catalog_Model_Type::SUBTYPE_ADDON)) {
                throw new Dyna_ProductMatchRule_Model_RuleException("The target product must be an Addon");
            }
        }
    }
}