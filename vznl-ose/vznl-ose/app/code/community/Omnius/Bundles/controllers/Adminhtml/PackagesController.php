<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * class Omnius_Bundles_Adminhtml_PackagesController
 *
 */
class Omnius_Bundles_Adminhtml_PackagesController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('bundles/packages')
            ->_addBreadcrumb(Mage::helper('bundles')->__('Manage Package Templates'),
                Mage::helper('bundles')->__('Manage Package Templates'));

        return $this;
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Gather data and generate edit form
     *
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('entity_id');
        /** @var Omnius_Bundles_Model_Package $model */
        $model = Mage::getModel('bundles/package')->load($id);

        if ($model->getEntityId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->addData($data);
            }

            Mage::register('packages_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('bundles/packages');

            $this->_addContent($this->getLayout()->createBlock('bundles/adminhtml_packages_edit'))
                ->_addLeft($this->getLayout()->createBlock('bundles/adminhtml_packages_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                list($model, $data, $deleteImage, $warnings, $exclude, $deleteMarketingIcon) = $this->_updatePackageData($data);
                /** @var Omnius_Bundles_Model_Package $model */
                if ($model->getId()) {
                    // When an existing item is changed, only update image if changes have been performed
                    if ($deleteImage) {
                        $model->deleteImage(null);
                    } elseif (isset($_FILES['image']['name']) && file_exists($_FILES['image']['tmp_name']) && $_FILES['image']['error'] == 0) {
                        // When a new image request is received
                        $model->uploadImage();
                    }
                    if ($deleteMarketingIcon) {
                        $model->deleteMarketingIcon();
                    } elseif (isset($_FILES['marketing_icon']['name']) && file_exists($_FILES['marketing_icon']['tmp_name']) && $_FILES['marketing_icon']['error'] == 0) {
                        $model->uploadMarketingIcon();
                    }
                } else {
                    // No mandatory image for template package
                    if (isset($_FILES['image']['name']) && file_exists($_FILES['image']['tmp_name']) && $_FILES['image']['error'] == 0) {
                        $model->uploadImage();
                    }
                    if (isset($_FILES['marketing_icon']['name']) && file_exists($_FILES['marketing_icon']['tmp_name']) && $_FILES['marketing_icon']['error'] == 0) {
                        $model->uploadMarketingIcon();
                    }
                }

                //display warnings
                if ( !empty($warnings) ) {
                    foreach ( $warnings as $warning ) {
                        Mage::getSingleton('adminhtml/session')->addWarning(Mage::helper('bundles')->__($warning));
                    }
                }

                //exclude accessories and addons that can not be excluded
                if ( !empty($exclude) ) {
                    $modelSku = $model->getSku();
                    $modelSku = array_diff($modelSku, $exclude);
                    $model->setSku($modelSku);
                }

                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('The package template was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['entity_id' => $model->getEntityId()]);

                    return;
                }
            } catch (Exception $e) {
                $this->_throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('entity_id') > 0) {
            try {
                $model = Mage::getModel('bundles/package')->load($this->getRequest()->getParam('entity_id'));
                if (!$model->getId()) {
                    throw new Exception('Package template with the provided ID not found');
                }
                $model->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit',
                    array('entity_id' => $this->getRequest()->getParam('entity_id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Display the admin grid (table) with all the announcements
     */
    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    public function validatePackageAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $this->_updatePackageData($data);
                $result['error'] = false;
            } catch (Exception $e) {
                $result['error'] = true;
                if (!empty($e->AllErrors)) {
                    $result['messages'] = $e->AllErrors;
                } else {
                    $result['messages'] = $e->getMessage();
                }

            }
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        }
    }

    /**
     * @param $data
     * @return array
     * @throws Exception
     */
    protected function _updatePackageData($data)
    {
        $entityId = $this->getRequest()->getParam('entity_id');
        /** @var $model Omnius_Bundles_Model_Package */
        if ($entityId) {
            $model = Mage::getModel('bundles/package')->load($entityId);
        } else {
            $model = Mage::getModel('bundles/package');
        }
        $data['website_id'] = "|" . implode('|', $data['website_ids']) . "|";
        $data['category'] = "|" . isset($data['category']) && !empty($data['category']) ? implode('|', $data['category']) : '' . "|";
        $deleteImage = isset($data['image']['delete']) && $data['image']['delete'];
        $deleteMarketingIcon = isset($data['marketing_icon']['delete']) && $data['marketing_icon']['delete'];
        unset($data['image']);
        unset($data['marketing_icon']);
        $model->addData($data);

        $validModel = $model->validateData();
        if (!empty($validModel['errors'])) {
            $ex = new Exception(implode(', ', $validModel['errors']));
            $ex->AllErrors = $validModel['errors'];
            throw $ex;
        }

        return array($model, $data, $deleteImage, $validModel['warnings'], $validModel['exclude'], $deleteMarketingIcon);
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     */
    private function _throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
    }
}
