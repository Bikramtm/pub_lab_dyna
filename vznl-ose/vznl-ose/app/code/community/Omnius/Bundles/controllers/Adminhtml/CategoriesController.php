<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * class Omnius_Bundles_Adminhtml_CategoriesController
 *
 */
class Omnius_Bundles_Adminhtml_CategoriesController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('bundles/categories')
            ->_addBreadcrumb(Mage::helper('bundles')->__('Manage Categories'),
                Mage::helper('bundles')->__('Manage Categories'));

        return $this;
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Gather data and generate edit form
     *
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('entity_id');
        /** @var Omnius_Bundles_Model_Package $model */
        $model = Mage::getModel('bundles/category')->load($id);

        if ($model->getEntityId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->addData($data);
            }

            Mage::register('categories_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('bundles/categories');

            $this->_addContent($this->getLayout()->createBlock('bundles/adminhtml_categories_edit'))
                ->_addLeft($this->getLayout()->createBlock('bundles/adminhtml_categories_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $id = $this->getRequest()->getParam('entity_id');
                /** @var Omnius_Bundles_Model_Category $model */
                $model = Mage::getModel('bundles/category')->load($id);
                $model->addData($data);
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('The category was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['entity_id' => $model->getEntityId()]);

                    return;
                }
            } catch (Exception $e) {
                $this->_throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('entity_id') > 0) {
            try {
                $model = Mage::getModel('bundles/category')->load($this->getRequest()->getParam('entity_id'));
                if (!$model->getId()) {
                    throw new Exception('Category with the provided ID not found');
                }
                $model->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit',
                    array('entity_id' => $this->getRequest()->getParam('entity_id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Display the admin grid (table) with all the announcements
     */
    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     */
    private function _throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
    }
}
