<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Model_Item extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('bundles/item');
    }
}
