<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Model_Resource_Package extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('bundles/package', 'entity_id');
    }
}
