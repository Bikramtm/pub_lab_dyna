<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Model_Package extends Mage_Core_Model_Abstract
{
    /** @var  Omnius_Bundles_Model_Resource_Category_Collection */
    protected $_categories;
    protected $_items = null;
    /** @var array */
    protected $_bundleIds = null;
    /** @var  Omnius_Bundles_Model_Resource_Bundle_Collection */
    protected $_bundles = null;

    private $_matchRules = [];

    protected $_devices = array();
    protected $_subscriptions = array();
    protected $_rcs = array();
    protected $_configurableDevice = array();

    private $_websitesCodes = [];

    private $_combinedSubs = [];

    private $_combinedDevSubs = [];

    protected function _construct()
    {
        $this->_init('bundles/package');
    }

    /**
     * Get the list of website ids for the current package
     *
     * @return array -- [{websiteId} => {websiteName}]
     */
    public function getWebsitesCodes()
    {
        $websiteIds = $this->getWebsiteId();
        if ( !empty($websiteIds) && empty($this->_websitesCodes) ) {
            foreach ( $websiteIds as $websiteId ) {
                $this->_websitesCodes[$websiteId] = Mage::app()->getWebsite($websiteId)->getName();
            }
        }

        return $this->_websitesCodes;
    }

    /**
     * When package is being edited show the temporary skus in frontend, else show the actual items
     * @return array
     */
    public function getItemsForDisplay()
    {
        if ($this->getSku()) {
            return $this->getSku();
        }
        $res = [];
        foreach ($this->getItems() as $item) {
            $res[] = $item->getSku();
        }

        return $res;
    }

    /**
     * Return the items for the current package
     * 
     * @return Omnius_Bundles_Model_Resource_Item_Collection
     */
    public function getItems()
    {
        if (!$this->_items && $this->getId()) {
            $this->_items = Mage::getResourceModel('bundles/item_collection')->addFieldToFilter('package_id', $this->getId())->load();
        } elseif (!$this->getId()) {
            $this->_items = [];
        }

        return $this->_items;
    }

    /**
     * @param $items
     * @return $this
     */
    public function setItems($items)
    {
        $this->_items = $items;

        return $this;
    }

    /**
     * @return Omnius__Bundles_Model_Resource_Category_Collection
     */
    public function getCategories()
    {
        if ($this->_categories === null) {
            $this->_categories = Mage::getResourceModel('bundles/category_collection')->addFieldToFilter('entity_id', ['in' => $this->getCategoryIds()]);
        }

        return $this->_categories;
    }

    /**
     * Returns an array with package/category ids for the current package
     *
     * @return array
     */
    public function getCategoryIds()
    {
        if ($this->getData('category_ids') === null) {
            $this->setData('category_ids', Mage::helper('bundles')->getPackagesCategoryLink($this->getId(), null));
        }

        return $this->getData('category_ids');
    }

    /**
     * When category ids are changed, also reset the category collection
     *
     * @return mixed
     */
    public function setCategoryIds()
    {
        $this->_packages = null;

        return parent::setPackageIds();
    }

    /**
     * Validates the data for the model
     *
     * @return array
     */
    public function validateData()
    {
        $errors = [];
        $warnings = [];
        $exclude = [];

        return array(
            'errors' => $errors,
            'warnings' => $warnings,
            'exclude' => $exclude
        );
    }

    /**
     * @return array
     */
    public function getPackagesForDropdown()
    {
        $res = [
            [
                'value' => '',
                'label' => 'Please select a package..',
            ]
        ];
        foreach ($this->getCollection() as $package) {
            $res[] = [
                'value' => $package->getId(),
                'label' => sprintf("%s (%s)", $package->getName(), $package->getType()),
            ];
        }

        return $res;
    }

    /**
     * Delete categories that are no longer used for this package and add the new ones
     *
     * @return Omnius_Bundles_Model_Package
     */
    public function _afterSave()
    {
        if ($this->hasDataChanges()) {
            // Parse the raw data of the products and see if they changed
            $currentProducts = $this->getSku();
            $oldProducts = $this->getItems();
            foreach ($oldProducts as $oldProduct) {
                if (!in_array($oldProduct->getSku(), $currentProducts)) {
                    $oldProduct->delete();
                } else {
                    unset($currentProducts[array_search($oldProduct->getSku(), $currentProducts)]);
                }
            }
            foreach ($currentProducts as $added) {
                Mage::getModel('bundles/item')
                    ->setPackageId($this->getId())
                    ->setSku($added)
                    ->save();
            }

            // Parse the raw data of the categories and see if they changed
            $currentCategories = $this->getCategoryIds();
            $oldCategories = Mage::helper('bundles')->getPackagesCategoryLink($this->getId());
            $toDelete = [];
            foreach ($oldCategories as $oldCategory) {
                if (!in_array($oldCategory, $currentCategories)) {
                    $toDelete[] = $oldCategory;
                } else {
                    unset($currentCategories[array_search($oldCategory, $currentCategories)]);
                }
            }
            /** @var Varien_Db_Adapter_Interface $connection */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $connection->beginTransaction();
            try {
                if ($toDelete) {
                    $connection->delete(Mage::getSingleton('core/resource')->getTableName('template_package_category_link'), sprintf('package_id = %s AND category_id IN (%s)', $this->getId(), implode(',', $toDelete)));
                }
                foreach ($currentCategories as $added) {
                    $connection->insert(Mage::getSingleton('core/resource')->getTableName('template_package_category_link'), ['package_id' => $this->getId(), 'category_id' => $added]);
                }
                $connection->commit();
            } catch (Exception $e) {
                $connection->rollBack();
                Mage::getSingleton('core/logger')->logException($e);
            }
        }

        return parent::_afterSave();
    }

    /**
     * Perform cleanup before deleting
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeDelete()
    {
        // Physically remove the image
        if (file_exists($this->getImageRealPath()) && is_file($this->getImageRealPath())) {
            unlink($this->getImageRealPath());
        }

        return parent::_beforeDelete();
    }

    /**
     * Get the URL path of the package image
     *
     * @return null|string
     */
    public function getImageUrlPath()
    {
        if (!$this->getImage()) {
            return null;
        }

        return Omnius_Bundles_Helper_Data::MEDIA_FOLDER_NAME . $this->getImage();
    }

    /**
     * Get the URL path of the marketing icon
     *
     * @return null|string
     */
    public function getMarketingIconUrlPath()
    {
        if (!$this->getMarketingIcon()) {
            return null;
        }

        return Omnius_Bundles_Helper_Data::MEDIA_FOLDER_NAME . $this->getMarketingIcon();
    }

    /**
     * Get file path of the package image
     *
     * @return string
     */
    public function getImageRealPath()
    {
        return Mage::helper('bundles')->getMediaFolder() . $this->getImage();
    }

    /**
     * Get the file path of the marketing icon
     *
     * @return string
     */
    public function getMarketingIconRealPath()
    {
        return Mage::helper('bundles')->getMediaFolder() . $this->getMarketingIcon();
    }

    /**
     * Return the list of website ids for this package
     *
     * @return array
     */
    public function getWebsiteId()
    {
        return array_filter(explode('|', $this->getData('website_id')), function ($el) {
            if ($el != "") {
                return true;
            }

            return false;
        });
    }

    /**
     * Add the list of website ids
     *
     * @param array|string $websites -- if string is used, the ids should be delimited by "|". E.g. |1|2|3|4|....|
     */
    public function setWebsiteId($websites)
    {
        if (is_array($websites)) {
            $this->setData('website_id', "|" . implode('|', $websites) . "|");
        } else {
            $this->setData('website_id', $websites);
        }
    }

    /**
     * @return Omnius_Bundles_Model_Resource_Bundle_Collection
     */
    public function getBundles()
    {
        if ($this->_bundles === null) {
            $this->_bundles = Mage::getResourceModel('bundles/bundle_collection')->addFieldToFilter('entity_id', ['in' => $this->getBundleIds()]);
        }

        return $this->_bundles;
    }

    /**
     * @return array
     */
    public function getBundleIds()
    {
        if ($this->_bundleIds === null) {
            $this->_bundleIds = Mage::helper('bundles')->getBundlesPackagesLink(null, $this->getId());
        }

        return $this->_bundleIds;
    }

    /**
     * Delete the package image, if a file exists for the current image, it will delete it
     *
     * @return $this
     */
    public function deleteImage()
    {
        // When a delete request is received also remove the old unused file
        if (file_exists($this->getImageRealPath()) && is_file($this->getImageRealPath())) {
            unlink($this->getImageRealPath());
        }
        $this->setImage(null);

        return $this;
    }

    /**
     * Delete the package image, if a file exists for the current image, it will delete it
     *
     * @return $this
     */
    public function deleteMarketingIcon()
    {
        if (file_exists($this->getMarketingIconRealPath()) && is_file($this->getMarketingIconRealPath())) {
            unlink($this->getMarketingIconRealPath());
        }
        $this->setMarketingIcon('');

        return $this;
    }

    /**
     * Upload the current package image
     * This will delete the old image, if any
     *
     * @throws Exception
     */
    public function uploadImage()
    {

        try {
            $uploader = new Varien_File_Uploader('image');
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png'])
                ->setAllowRenameFiles(true)
                ->setFilesDispersion(true)
                ->save(Mage::helper('bundles')->getMediaFolder());
            // Also delete the old image since it's not used
            if (file_exists($this->getImageRealPath()) &&  is_file($this->getImageRealPath())) {
                unlink($this->getImageRealPath());
            }
            $this->setImage($uploader->getUploadedFileName());
        } catch (Exception $e) {
            throw new Exception(Mage::helper('bundles')->__('Unable to save the image'));
        }
    }

    /**
     * Upload the current marketing icon
     * This will delete the old marketing icon, if any
     *
     * @throws Exception
     */
    public function uploadMarketingIcon()
    {
        try {
            $uploader = new Varien_File_Uploader('marketing_icon');
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png'])
                ->setAllowRenameFiles(true)
                ->setFilesDispersion(true)
                ->save(Mage::helper('bundles')->getMediaFolder());
            // Also delete the old image since it's not used
            if (file_exists($this->getMarketingIconRealPath()) && is_file($this->getMarketingIconRealPath())) {
                unlink($this->getMarketingIconRealPath());
            }
            $this->setMarketingIcon($uploader->getUploadedFileName());
        } catch (Exception $e) {
            throw new Exception(Mage::helper('bundles')->__('Unable to save the marketing icon'));
        }
    }

    /**
     * @return Omnius_Bundles_Model_Package
     */
    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }
}
