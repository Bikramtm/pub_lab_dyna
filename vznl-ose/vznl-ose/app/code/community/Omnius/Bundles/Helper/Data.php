<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Bundles_Helper_Data
 */
class Omnius_Bundles_Helper_Data extends Mage_Core_Helper_Abstract
{
    const MEDIA_FOLDER_NAME = 'bundles';
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Get the media folder path
     *
     * @return string
     */
    public function getMediaFolder()
    {
        return Mage::getBaseDir('media') . DS . self::MEDIA_FOLDER_NAME;
    }

    /**
     * Returns an array with the package/bundle ids
     *
     * @param null $bundleId
     * @param null $packageId
     * @return array
     */
    public function getBundlesPackagesLink($bundleId = null, $packageId = null)
    {
        if (!$bundleId && !$packageId) {
            return [];
        }

        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        if ($bundleId) {
            return $connection->query('SELECT DISTINCT package_id FROM template_bundle_package_link WHERE bundle_id = :id', array(':id' => $bundleId))
                ->fetchAll(PDO::FETCH_COLUMN);
        } else {
            return $connection->query('SELECT DISTINCT bundle_id FROM template_bundle_package_link WHERE package_id = :id', array(':id' => $packageId))
                ->fetchAll(PDO::FETCH_COLUMN);
        }

    }

    /**
     * Returns an array with package/category ids
     *
     * @param $packageId
     * @return array
     */
    public function getPackagesCategoryLink($packageId)
    {
        if (!$packageId) {
            return [];
        }

        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        return $connection
            ->query('SELECT DISTINCT category_id FROM template_package_category_link WHERE package_id = :id', array(':id' => $packageId))
            ->fetchAll(PDO::FETCH_COLUMN);

    }

    /**
     * @return array
     */
    public function getLockedForDropdown()
    {
        return [
            0 => 'Unlocked',
            1 => 'Locked',
        ];
    }

    /**
     * Checks if an array has any duplicate entries
     *
     * @param $inputArray
     * @return bool
     */
    public function hasDuplicates($inputArray = array())
    {
        $tempArray = [];
        foreach ($inputArray as $value) {
            if (isset($tempArray[$value])) {
                return true;
            }
            $tempArray[$value] = true;
        }

        return false;
    }

    /**
     * Fetches all the products as an associative array where the
     * key is the product sku and the value is the product name.
     * @return array
     */
    public function getProductsForDropdownBySku()
    {
        $key = md5('products_for_dropdown_by_sku');
        if (!($options = (unserialize($this->getCache()->load($key))))) {
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            $products = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('name', array('neq' => ''))
                ->addAttributeToSelect('name')
                ->setOrder('name', 'asc');
            $products = $conn->fetchAssoc($products->getSelect());

            $options = array(-1 => 'Please select a product..');
            foreach ($products as $product) {
                $options[] = array(
                    'label' => $product['name'],
                    'value' => $product['sku'],
                );
            }
            unset($products);

            $this->getCache()->save(serialize($options), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $options;
    }

    /**
     * Fetches all the products as an associative array where the
     * key is the product id and the value is the product sku.
     * @return array
     */
    public function getProductSkusForDropdown($forJs = false)
    {
        $key = md5('products_for_dropdown_sku' . (int) $forJs);
        if (!($options = (unserialize($this->getCache()->load($key))))) {
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            $products = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('name', array('neq' => ''))
                ->addAttributeToSelect('name')
                ->setOrder('name', 'asc');
            $products = $conn->fetchAssoc($products->getSelect());
            $options = [];
            $counter = 0;
            foreach ($products as $productId => $product) {
                $index = $forJs ? $counter : $productId;
                $options[$index] = array(
                    'value' => $product['sku'],
                    'data' => $productId,
                );
                ++$counter;
            }
            unset($products);

            $this->getCache()->save(serialize($options), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }
        
        return $options;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }
}
