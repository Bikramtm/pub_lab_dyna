<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Bundles_Helper_Packages
 */
class Omnius_Bundles_Helper_Packages extends Mage_Core_Helper_Abstract
{

    /**
     * @return array
     */
    public function getTypesForDropdown()
    {
        return [
            Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE => 'Mobile',
            Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE => 'Hardware',
            Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID => 'Prepaid',
        ];
    }

    /**
     * Return the list of categories for dropdown;
     *
     * @return array -- [{id} => {name}, ....]
     */
    public function getCategoriesForDropdown()
    {
        $cats = Mage::getResourceModel('bundles/category_collection')->load();
        $res = [];
        foreach ($cats as $cat) {
            $res[$cat->getId()] = $cat->getName();
        }

        return $res;
    }

    /**
     * Return the list of categories for form;
     *
     * @return array -- [[label => {name}, value => {id}], ....]
     */
    public function getCategoriesForForm()
    {
        $res = [];
        foreach ($this->getCategoriesForDropdown() as $val => $cat) {
            $res[] = [
                'label' => $cat,
                'value' => $val,
            ];
        }

        return $res;
    }
}
