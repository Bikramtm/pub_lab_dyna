<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$connection = $this->getConnection();

/** Start template_bundle table */
if (!$connection->isTableExists('template_bundle')) {

    $bundleTemplateTable = new Varien_Db_Ddl_Table();
    $bundleTemplateTable->setName('template_bundle');
    $bundleTemplateTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        ['unsigned' => true, 'primary' => true, 'auto_increment' => true]
    );
    $bundleTemplateTable->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        100
    );
    $bundleTemplateTable->addColumn(
        'image',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    );
    $bundleTemplateTable->addColumn(
        'website_id',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    );
    $bundleTemplateTable->addColumn(
        'locked',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        1
    );
    $bundleTemplateTable->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null
    );
    $bundleTemplateTable->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null
    );

    $bundleTemplateTable->setOption('type', 'InnoDB');
    $bundleTemplateTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($bundleTemplateTable);
    /** End template_bundle Table */
}

/** Start template_package table */
if (!$connection->isTableExists('template_package')) {
    $packageTemplateTable = new Varien_Db_Ddl_Table();
    $packageTemplateTable->setName('template_package');
    $packageTemplateTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        ['unsigned' => true, 'primary' => true, 'auto_increment' => true]
    );
    $packageTemplateTable->addColumn(
        'type',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        100
    );
    $packageTemplateTable->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        100
    );
    $packageTemplateTable->addColumn(
        'image',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    );
    $packageTemplateTable->addColumn(
        'website_id',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    );
    $packageTemplateTable->addColumn(
        'locked',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        1
    );
    $packageTemplateTable->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null
    );
    $packageTemplateTable->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null
    );
    $packageTemplateTable->setOption('type', 'InnoDB');
    $packageTemplateTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($packageTemplateTable);
    /** End template_package Table */
}
if (!$connection->isTableExists('template_package_item_link')) {

    $itemPackageTable = new Varien_Db_Ddl_Table();
    $itemPackageTable->setName('template_package_item_link');
    $itemPackageTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        ['unsigned' => true, 'primary' => true, 'auto_increment' => true]
    );

    $itemPackageTable->addColumn(
        'package_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        4
    );
    $itemPackageTable->addColumn(
        'sku',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        20
    );
    $itemPackageTable->addForeignKey(
        'fk_package_id_item_link',
        'package_id',
        Mage::getSingleton('core/resource')->getTableName('bundles/package'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );

    $itemPackageTable->setOption('type', 'InnoDB');
    $itemPackageTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($itemPackageTable);
    /** End template_package_item_link Table */
}
if (!$connection->isTableExists('template_category')) {

    $categoryTable = new Varien_Db_Ddl_Table();
    $categoryTable->setName('template_category');
    $categoryTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        ['unsigned' => true, 'primary' => true, 'auto_increment' => true]
    );

    $categoryTable->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    );
    $categoryTable->addColumn(
        'identifier',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    );
    $categoryTable->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null
    );
    $categoryTable->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null
    );
    $categoryTable->setOption('type', 'InnoDB');
    $categoryTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($categoryTable);
    /** End template_category Table */
}

if (!$connection->isTableExists('template_bundle_package_link')) {
    $packageBundleTable = new Varien_Db_Ddl_Table();
    $packageBundleTable->setName('template_bundle_package_link');
    $packageBundleTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        ['unsigned' => true, 'primary' => true, 'auto_increment' => true]
    );

    $packageBundleTable->addColumn(
        'package_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        4
    );
    $packageBundleTable->addColumn(
        'bundle_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        4
    );
    $packageBundleTable->addForeignKey(
        'fk_bundle_id_package_link',
        'bundle_id',
        Mage::getSingleton('core/resource')->getTableName('bundles/bundle'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );
    $packageBundleTable->addForeignKey(
        'fk_bundle_package_id_link',
        'package_id',
        Mage::getSingleton('core/resource')->getTableName('bundles/package'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );

    $packageBundleTable->setOption('type', 'InnoDB');
    $packageBundleTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($packageBundleTable);
    /** End template_bundle_package Table */
}
if (!$connection->isTableExists('template_package_category_link')) {
    $packageCategoryTable = new Varien_Db_Ddl_Table();
    $packageCategoryTable->setName('template_package_category_link');
    $packageCategoryTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        ['unsigned' => true, 'primary' => true, 'auto_increment' => true]
    );

    $packageCategoryTable->addColumn(
        'package_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        4
    );
    $packageCategoryTable->addColumn(
        'category_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        4
    );
    $packageCategoryTable->addForeignKey(
        'fk_package_id_category_link',
        'package_id',
        Mage::getSingleton('core/resource')->getTableName('bundles/package'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );
    $packageCategoryTable->addForeignKey(
        'fk_package_category_id_link',
        'category_id',
        Mage::getSingleton('core/resource')->getTableName('bundles/category'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );

    $packageCategoryTable->setOption('type', 'InnoDB');
    $packageCategoryTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($packageCategoryTable);
    /** End template_package_category_link Table */

}
$this->endSetup();
