<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Adminhtml_Packages_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $productsCollection = Mage::helper('bundles')->getProductsForDropdownBySku();
        $skusCollectionJs = Mage::helper('bundles')->getProductSkusForDropdown(true);
        $skusCollection = Mage::helper('bundles')->getProductSkusForDropdown();
        /** @var Omnius_Bundles_Model_Package $data */
        $data = Mage::registry('packages_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $dataFieldset = $form->addFieldset('products', array('legend' => Mage::helper('bundles')->__('Package details')));
        // Use our own class so there is no delete button shown
        $dataFieldset->addType('omnius_bundles_image', 'Omnius_Bundles_Block_Helper_Form_Image');
        $items = $data->getItemsForDisplay();
        $dataFieldset->addField('name', 'text', array(
            'label' => Mage::helper('bundles')->__('Name'),
            'name' => 'name',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getName(),
            'after_element_html' => sprintf('<script>window.itemsCount = %d; var skusAutocomplete = %s; var skusDropdown = %s;</script>', count($items), Mage::helper('core')->jsonEncode($skusCollectionJs), Mage::helper('core')->jsonEncode($skusCollection)),
        ));
        $dataFieldset->addField('image', 'omnius_bundles_image', array(
            'name' => 'image',
            'label' => Mage::helper('bundles')->__('Image'),
            'title' => Mage::helper('bundles')->__('Image'),
            'value' => $data->getImageUrlPath(),
        ));
        $dataFieldset->addField('marketing_icon', 'omnius_bundles_image', array(
            'name' => 'marketing_icon',
            'label' => Mage::helper('bundles')->__('Marketing Icon'),
            'title' => Mage::helper('bundles')->__('Marketing Icon'),
            'value' => $data->getMarketingIconUrlPath(),
        ));
        $dataFieldset->addField('category_ids', 'multiselect', array(
            'label' => Mage::helper('bundles')->__('Category'),
            'values' => Mage::helper('bundles/packages')->getCategoriesForForm(),
            'name' => 'category_ids',
            'value' => $data->getCategoryIds(),
        ));
        $dataFieldset->addField('type', 'select', array(
            'label' => Mage::helper('bundles')->__('Type'),
            'values' => Mage::helper('bundles/packages')->getTypesForDropdown(),
            'name' => 'type',
            "required" => true,
            'value' => $data->getType(),
            'after_element_html' => sprintf('<div class="add-button"><button type="button" class="scalable add bundles-template" onclick="addProduct()"><span>%s</span></button></div>', $this->__('Add product'))
        ));

        $key = 1;
        foreach ($items as $item) {
            $itemFieldset = $dataFieldset->addFieldset('product_' . $key, array('legend' => Mage::helper('bundles')->__('Product details'), 'class' => 'inner-fieldset'));

            $itemFieldset->addField('product_name_' . $key, 'select', array(
                'label' => Mage::helper('bundles')->__('Product Name'),
                'values' => $productsCollection,
                'name' => 'product[]',
                "required" => true,
                'value' => $item,
                'after_element_html' => sprintf('<button type="button" class="scalable cancel bundles-template" onclick="deleteProduct(this)"><span>%s</span></button>', $this->__('Remove product'))
            ));
            $itemFieldset->addField('product_sku_' . $key, 'text', array(
                'label' => Mage::helper('bundles')->__('Product Sku'),
                'name' => 'sku[]',
                "class" => "required-entry",
                "required" => true,
                'value' => $item
            ));
            ++$key;
        }

        // Also add a template which can be used to add products
        $itemFieldset = $dataFieldset->addFieldset('product_template', array('legend' => Mage::helper('bundles')->__('Product details'), 'class' => 'inner-fieldset hidden'));
        $itemFieldset->addField('product_name_template', 'select', array(
            'label' => Mage::helper('bundles')->__('Product Name'),
            'values' => $productsCollection,
            'name' => 'product[]',
            'required' => true,
            'disabled' => true,
            'value' => null,
            'after_element_html' => sprintf('<button type="button" class="scalable cancel bundles-template" onclick="deleteProduct(this)"><span>%s</span></button>', $this->__('Remove product'))
        ));
        $itemFieldset->addField('product_sku_template', 'text', array(
            'label' => Mage::helper('bundles')->__('Product Sku'),
            'name' => 'sku[]',
            'disabled' => true,
            'required' => true,
            'value' => null,
            'after_element_html' => sprintf('<script>jQuery(\'#product_template\').prev().addClass(\'hidden\'); </script>'),
        ));

        $websiteFieldset = $form->addFieldset('websites', array('legend' => Mage::helper('bundles')->__('Websites')));
        $websiteFieldset->addField('website_ids', 'multiselect', array(
            'label' => Mage::helper('bundles')->__('Website(s)'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(),
            'name' => 'website_ids',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getWebsiteId()
        ));
        $lock_fieldset = $form->addFieldset('lock_form', array('legend' => Mage::helper('bundles')->__('Lock state')));

        $lock_fieldset->addField('locked', 'select', array(
            'label' => Mage::helper('bundles')->__('Locked'),
            'values' => Mage::helper('bundles')->getLockedForDropdown(),
            'name' => 'locked',
            "class" => "required-entry",
            "required" => true,
            'value' => (int) $data->getLocked()
        ));

        return parent::_prepareForm();
    }
}
