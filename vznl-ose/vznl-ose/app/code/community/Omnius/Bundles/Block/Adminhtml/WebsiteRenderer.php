<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Adminhtml_WebsiteRenderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    private $_isExport = false;

    /**
     * @param Varien_Object $row
     * @return mixed|string
     */
    public function render(Varien_Object $row)
    {
        $websitesNames = "";

        $websites =  $row->getWebsiteId();
        foreach ($websites as $item) {
            $website = Mage::getModel('core/website')->load($item);

            // When exporting, export the website id, not website name
            if ($this->_isExport) {
                if ($website->getId()) {
                    $ruleWebsite = $website->getId();
                    $websitesNames = $websitesNames ? $websitesNames . "," . $ruleWebsite : $ruleWebsite;
                }
            } else {
                if ($website->getName()) {
                    $ruleWebsite = $website->getName();
                    $websitesNames = $websitesNames ? $websitesNames . ",<br>" . $ruleWebsite : $ruleWebsite;
                }
            }
        }

        return $websitesNames;
    }

    /**
     * Render column for export
     *
     * @param Varien_Object $row
     * @return string
     */
    public function renderExport(Varien_Object $row)
    {
        $this->_isExport = true;

        return $this->render($row);
    }
}
