<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Adminhtml_Bundles extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_bundles';
        $this->_blockGroup = 'bundles';
        $this->_headerText = Mage::helper('bundles')->__('Manage Bundle Templates');
        $this->_addButtonLabel = Mage::helper('bundles')->__('Add Bundle');
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }
}
