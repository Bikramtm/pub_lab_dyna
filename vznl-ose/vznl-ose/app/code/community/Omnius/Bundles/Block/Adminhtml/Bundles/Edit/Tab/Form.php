<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Adminhtml_Bundles_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $packagesCollection = Mage::getModel('bundles/package')->getPackagesForDropdown();
        /** @var Omnius_Bundles_Model_Bundle $data */
        $data = Mage::registry('bundles_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $dataFieldset = $form->addFieldset('bundles', array('legend' => Mage::helper('bundles')->__('Bundle details')));
        // Use our own class so there is no delete button shown
        $dataFieldset->addType('omnius_bundles_image', 'Omnius_Bundles_Block_Helper_Form_Image');
        $items = $data->getPackageIds();
        $dataFieldset->addField('name', 'text', array(
            'label' => Mage::helper('bundles')->__('Name'),
            'name' => 'name',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getName(),
            'after_element_html' => sprintf('<script>window.packagesCount = %d; </script>', count($items)),
        ));
        $dataFieldset->addField('image', 'omnius_bundles_image', array(
            'name' => 'image',
            'label' => Mage::helper('bundles')->__('Image'),
            'title' => Mage::helper('bundles')->__('Image'),
            'value' => $data->getImageUrlPath(),
            'after_element_html' => sprintf('<div class="add-button"><button type="button" class="scalable add bundles-template" onclick="addPackage()"><span>%s</span></button></div>', $this->__('Add package'))
        ));
        
        $key = 1;
        foreach ($items as $item) {
            $itemFieldset = $dataFieldset->addFieldset('package_' . $key, array('legend' => Mage::helper('bundles')->__('Package details'), 'class' => 'inner-fieldset'));

            $itemFieldset->addField('package_name_' . $key, 'select', array(
                'label' => Mage::helper('bundles')->__('Package Name'),
                'values' => $packagesCollection,
                'name' => 'package_ids[]',
                "required" => true,
                'value' => $item,
                'after_element_html' => sprintf('<button type="button" class="scalable cancel bundles-template" onclick="deletePackage(this)"><span>%s</span></button>', $this->__('Remove package'))
            ));
            ++$key;
        }

        // Also add a template which can be used to add packages
        $itemFieldset = $dataFieldset->addFieldset('package_template', array('legend' => Mage::helper('bundles')->__('Package details'), 'class' => 'inner-fieldset hidden'));
        $itemFieldset->addField('package_name_template', 'select', array(
            'label' => Mage::helper('bundles')->__('Package Name'),
            'values' => $packagesCollection,
            'name' => 'package_ids[]',
            'required' => true,
            'disabled' => true,
            'value' => null,
            'after_element_html' => sprintf('<button type="button" class="scalable cancel bundles-template" onclick="deleteProduct(this)"><span>%s</span></button><script>jQuery(\'#package_template\').prev().addClass(\'hidden\'); </script>', $this->__('Remove package'))
        ));

        $websiteFieldset = $form->addFieldset('websites', array('legend' => Mage::helper('bundles')->__('Websites')));
        $websiteFieldset->addField('website_id', 'multiselect', array(
            'label' => Mage::helper('bundles')->__('Website(s)'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(),
            'name' => 'website_id',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getWebsiteId()
        ));
        $lock_fieldset = $form->addFieldset('lock_form', array('legend' => Mage::helper('bundles')->__('Lock state')));

        $lock_fieldset->addField('locked', 'select', array(
            'label' => Mage::helper('bundles')->__('Locked'),
            'values' => Mage::helper('bundles')->getLockedForDropdown(),
            'name' => 'locked',
            "class" => "required-entry",
            "required" => true,
            'value' => (int) $data->getLocked()
        ));

        return parent::_prepareForm();
    }

}
