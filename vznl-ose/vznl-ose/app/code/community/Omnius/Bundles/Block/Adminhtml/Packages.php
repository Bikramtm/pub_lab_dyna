<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Adminhtml_Packages extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_packages';
        $this->_blockGroup = 'bundles';
        $this->_headerText = Mage::helper('bundles')->__('Manage Package Templates');
        $this->_addButtonLabel = Mage::helper('bundles')->__('Add Package');
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }
}
