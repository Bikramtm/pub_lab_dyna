<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Model_Mapper extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("multimapper/mapper");
    }
}
