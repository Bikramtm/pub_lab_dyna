<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Model_Mysql4_Reasoncode_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Override constructor
     */
    public function _construct()
    {
        $this->_init("multimapper/reasoncode");
    }
}
