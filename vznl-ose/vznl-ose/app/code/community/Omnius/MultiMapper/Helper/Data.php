<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param $priceplanId
     * @param $promoId
     * @return false|Mage_Core_Model_Abstract
     */
    public function getMappingInstanceById($priceplanId, $promoId)
    {
        return $this->_getMappingInstanceByField($priceplanId, $promoId, 'priceplan_id', 'promo_id');
    }

    /**
     * @param $priceplanId
     * @param $promoId
     * @param $priceplanField
     * @param $promoField
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getMappingInstanceByField($priceplanId, $promoId, $priceplanField, $promoField)
    {
        if (!empty($promoId)) {
            $priceplanPromoMatch = Mage::getModel('multimapper/mapper')->getCollection()
                ->addFieldToFilter($priceplanField, $priceplanId)
                ->addFieldToFilter($promoField, $promoId);

            if ($priceplanPromoMatch->getSize() > 0) {
                return $priceplanPromoMatch->getLastItem();
            }
        } else {
            /** @var Omnius_MultiMapper_Model_Mapper $pricePlanMatch */
            $pricePlanMatch = Mage::getModel('multimapper/mapper')->getCollection()
                ->addFieldToFilter($priceplanField, $priceplanId)
                ->addFieldToFilter($promoField, null);

            if ($pricePlanMatch->getSize() > 0) {
                return $pricePlanMatch->getLastItem();
            }
        }

        return Mage::getModel('multimapper/mapper');
    }

    /**
     * @param $priceplanSku
     * @param $promoSku
     */
    public function getMappingInstanceBySku($priceplanSku, $promoSku)
    {
        return $this->_getMappingInstanceByField($priceplanSku, $promoSku, 'priceplan_sku', 'promo_sku');
    }
}
