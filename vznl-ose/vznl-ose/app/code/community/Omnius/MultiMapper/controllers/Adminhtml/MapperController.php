<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Adminhtml_MapperController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("multimapper/mapper")->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper  Manager"), Mage::helper("adminhtml")->__("Mapper Manager"));

        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("MultiMapper"));
        $this->_title($this->__("Manager Mapper"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("MultiMapper"));
        $this->_title($this->__("Mapper"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("multimapper/mapper")->load($id);
        if ($model->getId()) {
            Mage::register("mapper_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("multimapper/mapper");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper Manager"), Mage::helper("adminhtml")->__("Mapper Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper Description"), Mage::helper("adminhtml")->__("Mapper Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit"))->_addLeft($this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("multimapper")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {

        $this->_title($this->__("MultiMapper"));
        $this->_title($this->__("Mapper"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("multimapper/mapper")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("mapper_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("multimapper/mapper");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper Manager"), Mage::helper("adminhtml")->__("Mapper Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper Description"), Mage::helper("adminhtml")->__("Mapper Description"));


        $this->_addContent($this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit"))->_addLeft($this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit_tabs"));

        $this->renderLayout();

    }

    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data) {

            try {
                $model = Mage::getModel("multimapper/mapper")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Mapper was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setMapperData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));

                    return;
                }
                $this->_redirect("*/*/");

                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setMapperData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));

                return;
            }

        }
        $this->_redirect("*/*/");
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("multimapper/mapper");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('entity_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("multimapper/mapper");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'mapper.csv';
        /** @var Omnius_MultiMapper_Block_Adminhtml_Mapper_Grid $grid */
        $grid = $this->getLayout()->createBlock('multimapper/adminhtml_mapper_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'mapper.xml';
        $grid = $this->getLayout()->createBlock('multimapper/adminhtml_mapper_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Import MultiMapper
     */
    public function importAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Upload and run import file
     */
    public function uploadAction()
    {
        if ($this->getRequest()->getPost()) {
            if (isset($_FILES['file']['tmp_name'])) {
                try {
                    $this->parseFile();
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError(
                        $this->__($e->getMessage())
                    );
                    $this->_redirect('*/*');
                }
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
            );
            $this->_redirect('*/*');
        }
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }

    protected function parseFile()
    {
        if ($file = $_FILES['file']['tmp_name']) {
            $uploader = new Mage_Core_Model_File_Uploader('file');
            $uploader->setAllowedExtensions(array('csv'));
            $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
            $uploader->save($path);
            if ($uploadFile = $uploader->getUploadedFileName()) {
                $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
                rename($path . $uploadFile, $path . $newFilename);
            }
        }

        if (isset($newFilename) && $newFilename) {
            /** @var Omnius_MultiMapper_Model_Importer $importer */
            $importer = Mage::getModel('multimapper/importer');
            $success = $importer->import($path . $newFilename);

            if ($success) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('Import file successfully parsed and imported into database')
                );
            }

            $this->_redirect('*/*');
        }
    }
}
