<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Block_Adminhtml_Reasoncode extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = "adminhtml_reasoncode";
        $this->_blockGroup = "multimapper";
        $this->_headerText = Mage::helper("multimapper")->__("Reasoncode Manager");
        $this->_addButtonLabel = Mage::helper("multimapper")->__("Add New Item");
        parent::__construct();

    }

}