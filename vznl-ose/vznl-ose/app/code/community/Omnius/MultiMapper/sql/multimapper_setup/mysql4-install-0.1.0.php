<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$sql=<<<SQLTEXT
CREATE TABLE IF NOT EXISTS `dyna_multi_mapper` (
  `entity_id` INT NOT NULL AUTO_INCREMENT,
  `priceplan_id` INT NOT NULL,
  `promo_id` INT NOT NULL,
  `priceplan_sku` VARCHAR(64) NOT NULL,
  `promo_sku` VARCHAR(64) NOT NULL,
  `acq_socs` VARCHAR(500) NULL,
  `ret_socs` VARCHAR(500) NULL,
  `inl_socs` VARCHAR(500) NULL,
  `comment` VARCHAR(500) NULL,
  PRIMARY KEY (`entity_id`));
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
