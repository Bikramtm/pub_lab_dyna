<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

if (!$installer->getConnection()->tableColumnExists('dyna_multi_mapper', 'addon1_name')) {

    $sql = <<<SQLTEXT
ALTER TABLE `dyna_multi_mapper` 
DROP COLUMN `inl_socs`,
DROP COLUMN `ret_socs`,
DROP COLUMN `acq_socs`,
ADD COLUMN `addon1_name` VARCHAR(255) NULL AFTER `comment`,
ADD COLUMN `addon1_desc` VARCHAR(255) NULL AFTER `addon1_name`,
ADD COLUMN `addon1_nature` VARCHAR(45) NULL AFTER `addon1_desc`,
ADD COLUMN `addon1_soc` VARCHAR(45) NULL AFTER `addon1_nature`,
ADD COLUMN `addon2_name` VARCHAR(255) NULL AFTER `addon1_soc`,
ADD COLUMN `addon2_desc` VARCHAR(255) NULL AFTER `addon2_name`,
ADD COLUMN `addon2_nature` VARCHAR(45) NULL AFTER `addon2_desc`,
ADD COLUMN `addon2_soc` VARCHAR(45) NULL AFTER `addon2_nature`,
ADD COLUMN `addon3_name` VARCHAR(255) NULL AFTER `addon2_soc`,
ADD COLUMN `addon3_desc` VARCHAR(255) NULL AFTER `addon3_name`,
ADD COLUMN `addon3_nature` VARCHAR(45) NULL AFTER `addon3_desc`,
ADD COLUMN `addon3_soc` VARCHAR(45) NULL AFTER `addon3_nature`,
ADD COLUMN `addon4_name` VARCHAR(255) NULL AFTER `addon3_soc`,
ADD COLUMN `addon4_desc` VARCHAR(255) NULL AFTER `addon4_name`,
ADD COLUMN `addon4_nature` VARCHAR(45) NULL AFTER `addon4_desc`,
ADD COLUMN `addon4_soc` VARCHAR(45) NULL AFTER `addon4_nature`,
ADD COLUMN `addon5_name` VARCHAR(255) NULL AFTER `addon4_soc`,
ADD COLUMN `addon5_desc` VARCHAR(255) NULL AFTER `addon5_name`,
ADD COLUMN `addon5_nature` VARCHAR(45) NULL AFTER `addon5_desc`,
ADD COLUMN `addon5_soc` VARCHAR(45) NULL AFTER `addon5_nature`,
ADD COLUMN `addon6_name` VARCHAR(255) NULL AFTER `addon5_soc`,
ADD COLUMN `addon6_desc` VARCHAR(255) NULL AFTER `addon6_name`,
ADD COLUMN `addon6_nature` VARCHAR(45) NULL AFTER `addon6_desc`,
ADD COLUMN `addon6_soc` VARCHAR(45) NULL AFTER `addon6_nature`,
ADD COLUMN `addon7_name` VARCHAR(255) NULL AFTER `addon6_soc`,
ADD COLUMN `addon7_desc` VARCHAR(255) NULL AFTER `addon7_name`,
ADD COLUMN `addon7_nature` VARCHAR(45) NULL AFTER `addon7_desc`,
ADD COLUMN `addon7_soc` VARCHAR(45) NULL AFTER `addon7_nature`,
ADD COLUMN `addon8_name` VARCHAR(255) NULL AFTER `addon7_soc`,
ADD COLUMN `addon8_desc` VARCHAR(255) NULL AFTER `addon8_name`,
ADD COLUMN `addon8_nature` VARCHAR(45) NULL AFTER `addon8_desc`,
ADD COLUMN `addon8_soc` VARCHAR(45) NULL AFTER `addon8_nature`,
ADD COLUMN `addon9_name` VARCHAR(255) NULL AFTER `addon8_soc`,
ADD COLUMN `addon9_desc` VARCHAR(255) NULL AFTER `addon9_name`,
ADD COLUMN `addon9_nature` VARCHAR(45) NULL AFTER `addon9_desc`,
ADD COLUMN `addon9_soc` VARCHAR(45) NULL AFTER `addon9_nature`,
ADD COLUMN `addon10_name` VARCHAR(255) NULL AFTER `addon9_soc`,
ADD COLUMN `addon10_desc` VARCHAR(255) NULL AFTER `addon10_name`,
ADD COLUMN `addon10_nature` VARCHAR(45) NULL AFTER `addon10_desc`,
ADD COLUMN `addon10_soc` VARCHAR(45) NULL AFTER `addon10_nature`;
SQLTEXT;

    $installer->run($sql);
}
$installer->endSetup();