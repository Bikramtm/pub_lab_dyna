<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$sql = <<<SQLTEXT
CREATE TABLE IF NOT EXISTS `dyna_reasoncodes` (
  `entity_id` INT AUTO_INCREMENT,
  `customer_value_option_id` INT NULL,
  `catalog_product_reasoncode_option_id` INT NULL,
  `reason_code` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`entity_id`));

SQLTEXT;

$installer->run($sql);

$reasonIndexes = $installer->getConnection()->getIndexList('dyna_reasoncodes');
$keys = array_keys($reasonIndexes);
if (!in_array('IDX_DYNA_REASONCODES_CUSTOMER_VALUE_REASONCODE_TYPE', $keys)) {
    $sql = <<<SQLTEXT
ALTER TABLE `dyna_reasoncodes` 
ADD UNIQUE INDEX `IDX_DYNA_REASONCODES_CUSTOMER_VALUE_REASONCODE_TYPE` (`customer_value_option_id` ASC, `catalog_product_reasoncode_option_id` ASC);

SQLTEXT;

    $installer->run($sql);
}
$reasonIndexes = $installer->getConnection()->getIndexList('dyna_multi_mapper');
$keys = array_keys($reasonIndexes);
if (!in_array('IDX_DYNA_MULTIMAPPER_PRICEPLAN_ID_PROMO_ID', $keys)) {

    $sql = <<<SQLTEXT
ALTER TABLE `dyna_multi_mapper` 
ADD UNIQUE INDEX `IDX_DYNA_MULTIMAPPER_PRICEPLAN_ID_PROMO_ID` (`priceplan_id` ASC, `promo_id` ASC);

SQLTEXT;

    $installer->run($sql);
}

if (!in_array('IDX_DYNA_MULTIMAPPER_PRICEPLAN_ID', $keys)) {
    $sql = <<<SQLTEXT
ALTER TABLE `dyna_multi_mapper` 
ADD INDEX `IDX_DYNA_MULTIMAPPER_PRICEPLAN_ID` (`priceplan_id` ASC);

SQLTEXT;

    $installer->run($sql);
}
$installer->endSetup();