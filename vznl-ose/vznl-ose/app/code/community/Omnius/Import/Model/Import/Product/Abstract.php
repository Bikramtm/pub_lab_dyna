<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Import_Model_Import_Product_Abstract
 */
abstract class Omnius_Import_Model_Import_Product_Abstract extends Mage_Core_Model_Abstract
{
    protected $stack;
    protected $_qties;
    protected $_attributeOptions;
    protected $_currentProductSku;
    protected $_multiselectSplit = ",";
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ',';

    /** @var bool $_debug */
    protected $_debug = false;

    /** @var array $_attrs */
    protected $_attrs = [];

    /** @var array $_tagRelations */
    public $_tagRelations = [];

    /** @var array $_attributeTypes */
    protected $_attributeTypes = [];

    /** @var array $_websites */
    protected $_websites = [];

    /** @var array $_attributeSets */
    protected $_attributeSets = [];

    /** @var array */
    protected $_reservedAttributeCodes = [
        'status',
        'visibility',
        'tax_class_id',
        'Product in websites',
        'Product Dealer Visibility',
        'product_visibility_strategy',
        '_product_websites',
        'attribute_set',
        'vat',
    ];

    /** @var int $_totalFileRows The no of total rows of the imported file (used for logging purposes)*/
    public $_totalFileRows = 0;
    /** @var int $_skippedFileRows The no of skipped rows of the imported file (used for logging purposes)*/
    public $_skippedFileRows = 0;

    /**
     * Omnius_Import_Model_Import_Product_Abstract constructor.
     */
    public function __construct()
    {
        parent::__construct();
        /** @var Omnius_Import_Helper_Data _helper */
        $this->_helper = Mage::helper("omnius_import/data");
    }

    /**
     * @param array $data
     * @return mixed
     */
    abstract public function importProduct($data);

    /**
     * Set debug mode
     *
     * @param bool $debug
     */
    public function setDebug($debug)
    {
        $this->_debug = $debug;
    }

    /**
     * Get debug mode
     *
     * @return bool
     */
    public function getDebug()
    {
        return $this->_debug;
    }

    /**
     * Match each attribute type and set its data
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     * @param string $code
     */
    protected function _setData($product, $data, $code)
    {
        if (!array_key_exists($code, $this->_attributeTypes)) {
            $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $code);
            if (!$attribute || !$attribute->getId()) {
                $this->_log('Unknown attribute, attribute_code specified: ' . $code);

                return;
            }
            $this->_attributeTypes[$code] = $attribute->getFrontendInput();
        }

        $this->_log('Setting attribute ' . $code . ' with type ' . $this->_attributeTypes[$code], true);
        switch ($this->_attributeTypes[$code]) {
            case 'text':
            case 'textarea':
            case 'date':
            case 'datetime':
            case 'price':
                $this->_setScalarData($product, $data, $code);
                break;
            case 'boolean':
                $this->_setYesNoData($product, $data, $code);
                break;
            case 'multiselect':
                $this->_setMultiSelectData($product, $data, $code);
                break;
            case 'select':
                $this->_setDropdownData($product, $data, $code);
                break;
            default:
                $this->_log('Unsupported attribute frontend type, type found: ' . $this->_attributeTypes[$code]);

                return;
        }
    }

    /**
     * Log function (if debug is disabled)
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if (!$this->_debug || ($verbose && $this->_debug)) {
            $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
        }
    }

    /**
     * Log error
     *
     * @param $msg
     */
    protected function _logError($msg)
    {
        $this->_helper->logMsg('[ERROR] ' . $msg);
    }

    /**
     * Log exception
     *
     * @param $ex
     */
    protected function _logEx($ex)
    {
        $this->_log('Product import threw an exception: ');
        $this->_helper->log($ex);
    }

    /**
     * Set data to simple attributes (text)
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     * @param string $code
     */
    protected function _setScalarData($product, $data, $code)
    {
        $this->_log('Setting scalar attribute ' . $code . ' to value ' . $data[$code], true);
        $product->setData($code, trim($data[$code]));
    }

    /**
     * Save data for multiselect attributes after mapping the provided values to their corresponding option_id
     * If the provided value does not exist, it is added to the attribute option list
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     * @param string $code
     */
    protected function _setMultiSelectData($product, $data, $code)
    {
        $this->_log('Setting multiselect attribute ' . $code . ' to value ' . $data[$code], true);
        $split = preg_quote($this->_multiselectSplit);
        $values = preg_split("/{$split}/", $data[$code]);
        if (empty($values)) {
            $this->_log('No values found for multiselect attribute ' . $code, true);

            return;
        }
        $valueCodes = [];
        if (!isset($this->_attributeOptions[$code])) {
            $this->_attributeOptions[$code] = $this->_getAttrOptions($code);
        }
        foreach ($values as $value) {
            $value = strtolower(trim($value));
            if (!isset($this->_attributeOptions[$code][$value])) {
                $optionId = $this->_addAttrOption($code, $value);
                if ($optionId) {
                    $this->_attributeOptions[$code][$value] = $optionId;
                } else {
                    continue;
                }
            }
            $valueCodes[] = $this->_attributeOptions[$code][$value];
        }

        $product->setData($code, $valueCodes);
    }

    /**
     * Set data for dropdown attribute (singe option only)
     * If the provided value does not exist, it is added to the dropdown attribute option list
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     * @param string $code
     */
    protected function _setDropdownData($product, $data, $code)
    {
        $this->_log('Setting dropdown attribute ' . $code . ' to value ' . $data[$code], true);
        if (empty($data[$code])) {
            $this->_log('No values found for dropdown attribute ' . $code, true);

            return;
        }
        if (!isset($this->_attributeOptions[$code])) {
            $this->_attributeOptions[$code] = $this->_getAttrOptions($code);
        }
        $value = strtolower(trim($data[$code]));
        if (!isset($this->_attributeOptions[$code][$value])) {
            $optionId = $this->_addAttrOption($code, $value);
            if ($optionId) {
                $this->_attributeOptions[$code][$value] = $optionId;
            }
        }

        $product->setData($code, $this->_attributeOptions[$code][$value]);
    }

    /**
     * Set simple yes/no data to attribute
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     * @param string $code
     */
    protected function _setYesNoData($product, $data, $code)
    {
        $this->_log('Setting yes/no attribute ' . $code . ' to value ' . $data[$code], true);
        switch (strtolower($data[$code])) {
            case 'ja':
            case 'y':
            case 'yes':
            case '1':
            case 'ok':
            case 'true':
                $product->setData($code, 1);
                break;
            case 'nee':
            case 'n':
            case 'no':
            case '0':
            case 'x':
            case 'false':
                $product->setData($code, 0);
                break;
            default:
                break;
        }
    }

    /**
     * Returns the number of imported records
     *
     * @return mixed
     */
    public function getImportCount()
    {
        return $this->_qties;
    }

    /**
     * Add a new option to the attribute
     *
     * @param string $attrCode
     * @param string $value
     * @return null|int
     */
    protected function _addAttrOption($attrCode, $value)
    {
        if (empty($value)) {
            return null;
        }
        $this->_log('Adding attribute option for attribute ' . $attrCode . ' with value ' . $value, true);

        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_write');
        $optionTable = $resource->getTableName('eav/attribute_option');
        $optionValueTable = $resource->getTableName('eav/attribute_option_value');

        $connection->insert($optionTable, [
            'attribute_id' => $this->_getAttrId($attrCode),
            'sort_order' => 0
        ]);
        $optionId = $connection->lastInsertId($optionTable);

        $connection->insert($optionValueTable, [
            'option_id' => $optionId,
            'store_id' => 0,
            'value' => $value
        ]);

        return $optionId;
    }

    /**
     * Get attribute model instance by attribute code
     *
     * @param string $attrCode
     * @return mixed
     */
    protected function _getAttr($attrCode)
    {
        if (!isset($this->_attrs[$attrCode])) {
            $attr = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attrCode);
            $this->_attrs[$attrCode] = $attr;
        }

        return $this->_attrs[$attrCode];
    }

    /**
     * Get attribute id by code
     *
     * @param string $attrCode
     * @return int
     */
    protected function _getAttrId($attrCode)
    {
        return $this->_getAttr($attrCode)->getId();
    }

    /**
     * Get all options of a dropdown or multiselect attribute.
     * Format returned: [label1 => option_id1, label2 => option_id2, ...]
     *
     * @param string $attrCode
     * @return array
     */
    protected function _getAttrOptions($attrCode)
    {
        $attr = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attrCode);
        $options = $attr->getSource()->getAllOptions(false);
        $result = [];
        foreach ($options as $option) {
            $result[strtolower($option['label'])] = $option['value'];
        }

        return $result;
    }

    /**
     * Clear gallery images for a product
     *
     * @param Mage_Catalog_Model_Product $product
     */
    protected function _clearGallery($product)
    {
        $mediaGalleryAttribute = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', 'media_gallery');
        $gallery = $product->getMediaGalleryImages();
        foreach ($gallery as $image) {
            $mediaGalleryAttribute->getBackend()->removeImage($product, $image->getFile());
        }
    }

    /**
     * Add gallery images for a product
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $dir
     * @param string $images
     */
    protected function _addGalleyImages($product, $dir, $images)
    {
        if (strlen(trim($images)) > 0) {
            $fileNamesArr = explode(",", $images);
            foreach ($fileNamesArr as $filename) {
                if (strlen($filename) > 4) {
                    $filename = trim($filename);
                    if (file_exists($dir . $filename)) {
                        $product->addImageToMediaGallery($dir . $filename, [], false, false);
                    }
                }
            }
        }
    }

    /**
     * Set the main product image
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $dir
     * @param string $filename
     */
    protected function _addMainImage($product, $dir, $filename)
    {
        if (file_exists($dir . $filename) && strlen($filename) > 4) {
            $product->addImageToMediaGallery($dir . $filename, ['image', 'small_image', 'thumbnail'], false, false);
        }
    }

    /**
     * Save product tags
     */
    public function saveTags()
    {
        foreach ($this->_tagRelations as $tagData) {
            $tagRelation = Mage::getModel('tag/tag_relation');
            $tagRelation->addRelations($tagData['tag'], $tagData['products']);
        }
    }

    /**
     * Set tags to a product
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $tagCsv
     */
    protected function _setTags($product, $tagCsv)
    {
        $tagNames = explode(',', trim($tagCsv));
        foreach ($tagNames as $tagName) {
            if ($tagName == '') {
                return;
            }
            if (!isset($this->_tagRelations[$tagName])) {
                $tag = Mage::getModel('tag/tag');
                $tag->loadByName($tagName);
                if (!$tag->getId()) {
                    $tag
                        ->setName($tagName)
                        ->setStatus($tag->getApprovedStatus())
                        ->setFirstStoreId(0)
                        ->save();
                }
                $tag->setStore(0);

                $this->_tagRelations[$tagName] = ['products' => [], 'tag' => $tag];
            }

            $this->_tagRelations[$tagName]['products'][] = $product->getId();
        }
    }

    /**
     * Get a list of all website codes with their corresponding ids'
     *
     * @throws Mage_Core_Exception
     */
    protected function _getWebsiteCodes()
    {
        $websites = array_keys(Mage::app()->getWebsites());
        foreach ($websites as $id) {
            $this->_websites[Mage::app()->getWebsite($id)->getCode()] = $id;
        }
    }

    /**
     * Determine website id from its code. Returns false if website is not found.
     *
     * @param string $code
     * @return bool|int
     */
    protected function _getWebsiteIdByCode($code)
    {
        if (empty($this->_websites)) {
            $this->_getWebsiteCodes();
        }

        if (array_key_exists($code, $this->_websites)) {
            return $this->_websites[$code];
        }

        return false;
    }

    /**
     * Set websites for a product based on the values from the CSV file
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     * @return bool
     */
    protected function _setWebsitesByCsvString($product, $data)
    {
        $websiteCodes = explode(',', $data['_product_websites']);
        $websiteCodes = array_map('trim', $websiteCodes);
        $completed = false;

        if (!empty($websiteCodes)) {
            foreach ($websiteCodes as $code) {
                $websiteId = $this->_getWebsiteIdByCode($code);

                if ($websiteId) {
                    $websiteIds[] = $websiteId;
                } else {
                    return false;
                }
            }
            if (!empty($websiteIds)) {
                $product->setWebsiteIds($websiteIds);

                $completed = true;
            } else {
                $completed = false;
            }
        }

        return $completed;
    }

    /**
     * Set product class id based on the provided tax_class_id in the CSV file
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     * @return bool
     */
    protected function _setTaxClassIdByName($product, $data)
    {
        $productTaxClass = Mage::getModel('tax/class')
            ->getCollection()
            ->addFieldToFilter('class_name', $data['tax_class_id'])
            ->load()
            ->getFirstItem();
        if (!$productTaxClass || !$productTaxClass->getId()) {
            return false;
        }
        $product->setTaxClassId($productTaxClass->getId());

        return true;
    }

    /**
     * Create product stock
     *
     * @param Mage_Catalog_Model_Product $product
     */
    protected function _createStockItem($product)
    {
        $product->save(); // TODO Apparently this is need, why?

        $product->setStockData([
            'inventory_use_config_manage_stock' => 1,
            'inventory_use_config_min_sale_qty' => 1,
            'inventory_use_config_max_sale_qty' => 1,
            'inventory_use_config_enable_qty_increments' => 1,
            'is_in_stock' => 0,
            'qty' => 0,
            'manage_stock' => 0
        ]);
    }

    /**
     * Call _setData method for all attributes that are not found in the _reservedAttributeCodes list
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setProductData($product, $data)
    {
        foreach ($data as $code => $value) {
            if (!in_array($code, $this->_reservedAttributeCodes)) {
                $this->_setData($product, $data, $code);
            }
        }
    }

    /**
     * Set product status
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setStatus($product, $data)
    {
        if (array_key_exists('status', $data) && $data['status']) {
            $product->setStatus(1);
        } else {
            $product->setStatus(2);
        }
    }

    /**
     * Set the product attribute set based on the provided values in CSV file
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     * @return bool
     */
    protected function _setAttributeSetByCode($product, $data)
    {
        $attributeSet = $data['attribute_set'];
        if (empty($this->_attributeSets[$attributeSet])) {
            $attributeSetModel = Mage::getModel("eav/entity_attribute_set")->getCollection()
                ->addFieldToFilter("attribute_set_name", $data['attribute_set'])
                ->getFirstItem();

            $this->_attributeSets[$attributeSet] = $attributeSetModel;
        } else {
            $attributeSetModel = $this->_attributeSets[$attributeSet];
        }

        if ($attributeSetModel && $attributeSetModel->getAttributeSetId()) {
            $product->setAttributeSetId($attributeSetModel->getAttributeSetId());

            return true;
        }

        return false;
    }

    /**
     * Returns csv delimiter character
     *
     * @return string
     */
    public function getCsvDelimiter()
    {
        return $this->_csvDelimiter;
    }

    /**
     * Format CSV header columns to underscore format
     *
     * @param string $col
     * @return string
     */
    public function formatKey($col)
    {
        $col = trim($col);
        $col = str_replace(' ', '', $col);


        $tmpCol = str_replace('_', '', $col);
        if (ctype_upper($tmpCol)) {
            $col = strtolower($col);
        }

        $col = $this->_replaceUpper($col);
        $col = $this->_underscore($col);
        $col = str_replace(' ', '', $col);

        return $col;
    }

    /**
     * Convert consecutive uppercase letters to lowercase
     * e.g MWSTIndicator => mwst_indicator
     * e.g. AbcAAc => abc_a_ac
     * e.g. asda_SSConcs => asda_ss_concs
     *
     * @param $col string
     * @return string
     */
    protected function _replaceUpper($col)
    {
        $regex = '/(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])/x';

        return strtolower(implode("_", preg_split($regex, $col)));
    }

    public function setStack($stack) {
        return $this->stack = $stack;
    }
}
