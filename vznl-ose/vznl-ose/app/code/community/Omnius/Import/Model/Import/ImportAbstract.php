<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Import_Model_Import_ImportAbstract
 */
abstract class Omnius_Import_Model_Import_ImportAbstract extends Mage_Core_Model_Abstract
{
    protected $_qties;
    protected $stack;

    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ',';

    /** @var bool $_debug */
    protected $_debug = false;
    /**
     * @var Omnius_Import_Helper_Data
     */
    protected $_helper;

    protected $fields = [];
    protected $mapping = [];
    protected $header = [];

    protected $_currentProductSku;

    /** @var int $_totalFileRows The no of total rows of the imported file (used for logging purposes)*/
    public $_totalFileRows = 0;
    /** @var int $_skippedFileRows The no of skipped rows of the imported file (used for logging purposes)*/
    public $_skippedFileRows = 0;

    /**
     * Omnius_Import_Model_Import_Product_Abstract constructor.
     */
    public function __construct()
    {
        parent::__construct();
        /** @var Omnius_Import_Helper_Data _helper */
        $this->_helper = Mage::helper("omnius_import/data");
    }

    /**
     * Set debug mode
     *
     * @param bool $debug
     */
    public function setDebug($debug)
    {
        $this->_debug = $debug;
    }

    /**
     * Get debug mode
     *
     * @return bool
     */
    public function getDebug()
    {
        return $this->_debug;
    }

    /**
     * Returns the number of imported records
     *
     * @return mixed
     */
    public function getImportCount()
    {
        return $this->_qties;
    }

    /**
     * Format CSV header columns to underscore format
     *
     * @param string $col
     * @return string
     */
    public function formatKey($col)
    {
        $col = trim($col);
        $col = str_replace(' ', '', ucwords($col));

        $tmpCol = str_replace('_', '', $col);
        if (ctype_upper($tmpCol)) {
            $col = strtolower($col);
        }

        $col = $this->_replaceUpper($col);
        $col = $this->_underscore($col);
        $col = str_replace(' ', '', $col);

        return $col;
    }

    /**
     * Returns csv delimiter character
     *
     * @return string
     */
    public function getCsvDelimiter()
    {
        return $this->_csvDelimiter;
    }

    /**
     * Log function (if debug is disabled)
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if (!$this->_debug || ($verbose && $this->_debug)) {
            $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
        }
    }

    /**
     * Log error
     *
     * @param $msg
     */
    protected function _logError($msg)
    {
        $this->_helper->logMsg('[ERROR] ' . $msg);
    }

    /**
     * Log exception
     *
     * @param $ex
     */
    protected function _logEx($ex)
    {
        $this->_log('Import threw an exception: ');
        $this->_helper->log($ex);
    }

    /**
     * Convert consecutive uppercase letters to lowercase
     * e.g MWSTIndicator => mwst_indicator
     * e.g. AbcAAc => abc_a_ac
     * e.g. asda_SSConcs => asda_ss_concs
     *
     * @param $col string
     * @return string
     */
    protected function _replaceUpper($col)
    {
        $regex = '/(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])/x';

        return strtolower(implode("_", preg_split($regex, $col)));
    }

    public function setStack($stack) {
        return $this->stack = $stack;
    }

    /**
     * Truncate previous data
     * Set the table index to 1
     */
    public function removePreviousData($model) {
        $resource = $model->getResource();
        $connection = $resource->getReadConnection();
        $connection->exec("SET FOREIGN_KEY_CHECKS = 0;");
        /* @see Varien_Db_Adapter_Pdo_Mysql */
        $connection->truncateTable($resource->getMainTable());
        $connection->changeTableAutoIncrement($resource->getMainTable(), 1);
        $connection->exec("SET FOREIGN_KEY_CHECKS = 1;");
    }
}
