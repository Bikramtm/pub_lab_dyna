<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Service_Model_Logger
 */
class Omnius_Service_Model_Logger
{
    /** @var bool */
    protected $_allowLog;

    /** @var string */
    protected $_rootDir;

    /**
     * @param string $rootDir
     */
    public function __construct($rootDir = null)
    {
        $this->_rootDir = $rootDir ? $rootDir : Mage::getBaseDir("log");
        $this->_allowLog = (bool) Mage::helper('omnius_service')->getGeneralConfig('logging');
    }

    /**
     * @param $data
     * @param null $folder
     * @param null $identifier
     * @param null $format
     * @return string
     */
    public function logTransfer($data, $folder = null, $identifier = null, $format = null)
    {
        if (!$this->verifyLogging()) {
            return;
        }

        $filename = $this->createFilename($folder ? $folder : 'transfer', "transf", $identifier, $format);

        $this->loggerWrite($filename, (string) $data);

        return $filename;
    }

    /**
     * @return bool
     */
    protected function verifyLogging()
    {
        return $this->_allowLog;
    }

    /**
     * @param $folder
     * @param $type
     * @param $uniqueIdentifier
     * @param $format
     * @return string
     */
    protected function createFilename($folder, $type, $uniqueIdentifier, $format)
    {
        if (!is_dir("{$this->_rootDir}/{$folder}")) {
            mkdir("{$this->_rootDir}/{$folder}", 0777, true);
        }

        $uniqueIdentifier = ($uniqueIdentifier ? ".{$uniqueIdentifier}" : "");
        $format = ($format ? ".{$format}" : "");

        $filename = sprintf(
            "{$this->_rootDir}/{$folder}/%s{$uniqueIdentifier}.{$type}{$format}",
            (string) $this->udate("YmdHis.u")
        );

        return $filename;
    }

    /**
     * @param $format
     * @param null $utimestamp
     * @return bool|string
     */
    protected function udate($format, $utimestamp = null)
    {
        if (is_null($utimestamp)) {
            $utimestamp = microtime(true);
        }

        $timestamp = floor($utimestamp);
        $milliseconds = round(($utimestamp - $timestamp) * 10000);

        return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
    }

    /**
     * @param $filename
     * @param $input
     */
    protected function loggerWrite($filename, $input)
    {
        $this->log(
            Mage::helper('omnius_service/request')->convertEncoding($input),
            null,
            trim(str_replace($this->_rootDir, '', $filename), DS),
            true
        );
    }

    /**
     * @param $message
     * @param null $level
     * @param string $file
     * @param bool $forceLog
     */
    public function log($message, $level = null, $file = '', $forceLog = false)
    {
        try {
            $logActive = Mage::getStoreConfig('dev/log/active');
            if (empty($file)) {
                $file = Mage::getStoreConfig('dev/log/file');
            }
        } catch (Exception $e) {
            $logActive = true;
        }

        if (!$logActive && !$forceLog) {
            return;
        }

        static $loggers = array();

        $level = is_null($level) ? Zend_Log::DEBUG : $level;
        $file = empty($file) ? 'system.log' : $file;

        try {
            if (!isset($loggers[$file])) {
                $logDir = Mage::getBaseDir('var') . DS . 'log';
                $logFile = $logDir . DS . $file;

                if (!is_dir($logDir)) {
                    mkdir($logDir);
                    chmod($logDir, 0777);
                }

                if (!file_exists($logFile)) {
                    file_put_contents($logFile, '');
                    chmod($logFile, 0777);
                }

                $formatter = new Zend_Log_Formatter_Simple();
                $writerModel = (string) Mage::getConfig()->getNode('global/log/core/writer_model');
                if (!$writerModel) {
                    $writer = new Zend_Log_Writer_Stream($logFile);
                } else {
                    $writer = new $writerModel($logFile);
                }
                $writer->setFormatter($formatter);
                $loggers[$file] = new Zend_Log($writer);
            }

            if (is_array($message) || is_object($message)) {
                $message = print_r($message, true);
            }

            $loggers[$file]->log($message, $level);
        } catch (Exception $e) {
        }
    }

    /**
     * @param $request
     * @param null $folder
     * @param null $identifier
     * @param null $format
     * @return string
     */
    public function logRequest($request, $folder = null, $identifier = null, $format = null)
    {
        if (!$this->verifyLogging()) {
            return;
        }

        $filename = $this->createFilename($folder ? $folder : 'requests', "req", $identifier, $format);

        $this->loggerWrite($filename, (string) $request);

        return $filename;
    }

    /**
     * @param $response
     * @param null $folder
     * @param null $identifier
     * @param null $format
     * @return string
     */
    public function logResponse($response, $folder = null, $identifier = null, $format = null)
    {
        if (!$this->verifyLogging()) {
            return;
        }

        $filename = $this->createFilename($folder ? $folder : 'requests', "res", $identifier, $format);

        $this->loggerWrite($filename, (string) $response);

        return $filename;
    }
}
