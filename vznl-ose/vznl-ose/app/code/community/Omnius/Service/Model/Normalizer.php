<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Service_Model_Normalizer
 */
class Omnius_Service_Model_Normalizer
{
    /**
     * @param $data
     * @return Varien_Object
     */
    public function normalize($data)
    {
        return $this->_normalize($this->objectToArray($data));
    }

    /**
     * @param $data
     * @return Varien_Object
     */
    protected function _normalize($data)
    {
        $obj = new Varien_Object();
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $obj->setData($this->normalizeString($key), $this->_normalize($value));
            } else {
                $obj->setData($this->normalizeString($key), $value);
            }
        }

        return $obj;
    }

    /**
     * @param $str
     * @return string
     */
    public function normalizeString($str)
    {
        return strlen($str) > 2 ? strtolower(preg_replace('~(?<=\\w)([A-Z])~', '_$1', str_replace('_', '', $str))) : strtolower($str);
    }

    /**
     * @param $data
     * @return array
     */
    public function objectToArray($data)
    {
        if (is_object($data)) {
            $data = get_object_vars($data);
        }

        if (is_array($data)) {
            return array_map(array($this, __FUNCTION__), $data);
        } else {
            return $data;
        }
    }

    /**
     * @param $data
     * @return array
     */
    public function normalizeKeys($data)
    {
        $normalized = array();
        if (!is_array($data)) {
            return $normalized;
        }
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $normalized[$this->normalizeString($key)] = $this->normalizeKeys($value);
            } else {
                $normalized[$this->normalizeString($key)] = $value;
            }
        }

        return $normalized;
    }

    /**
     * @param $data
     * @return object
     */
    public function arrayToObject($data)
    {
        if (is_array($data)) {
            return (object) array_map(array($this, __FUNCTION__), $data);
        } else {
            return $data;
        }
    }
}
