<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Service_Helper_Data
 */
class Omnius_Service_Helper_Data extends Mage_Core_Helper_Data
{
    /** General settings */
    const GENERAL_CONFIG_PATH = 'omnius_service/general/';

    /** @var Omnius_Service_Model_ClientFactory */
    protected $factory;

    /** @var array */
    protected $_registry = array();

    /** @var Omnius_Cache_Model_Cache */
    protected $_cache;

    /**
     * @param $type
     * @param array $options
     * @return mixed
     */
    public function getClient($type, array $options = array())
    {
        $key = serialize(array('type' => $type, 'options' => $options));
        if (isset($this->_registry[$key])) {
            return $this->_registry[$key];
        } else {
            $default = $this->getDefaultOptions();

            foreach ($default as $namespace => $defaultOptions) {
                if (str_replace(" ", "", ucwords(strtr($type, "_-", "  "))) === str_replace(" ", "", ucwords(strtr($namespace, "_-", "  ")))) {
                    $options['options'] = array_merge(isset($options['options']) ? $options['options'] : array(), $defaultOptions);
                }
            }

            return $this->_registry[$key] = $this->_getFactory()->buildClient($type, $options);
        }
    }

    /**
     * General options for all services
     *
     * @return array
     */
    protected function getDefaultOptions()
    {
        return [];
    }

    /**
     * @return Omnius_Service_Model_ClientFactory
     */
    protected function _getFactory()
    {
        if (!$this->factory) {
            return $this->factory = Mage::getModel("omnius_service/clientFactory");
        }

        return $this->factory;
    }

    /**
     * @return bool
     */
    public function useStubs()
    {
        return (bool) $this->getGeneralConfig('use_stubs');
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getGeneralConfig($config)
    {
        return Mage::getStoreConfig(static::GENERAL_CONFIG_PATH . $config);
    }

    /**
     * @param $message
     * @return string
     */
    public function debug($message)
    {
        if ($this->isDev()) {
            return $message;
        }

        return '';
    }

    /**
     * @return bool
     */
    public function isDev()
    {
        return Mage::getStoreConfigFlag(Dyna_Cache_Model_Cache::ENVIRONMENT_MODE_CONFIG_PATH);
    }

    /**
     * @param SoapClient $client
     * @param string $methodName
     * @return bool
     */
    public function containsNode(SoapClient $client, $methodName)
    {
        try {
            $a = new SimpleXMLElement(trim($client->__getLastResponse()));
            $namespaces = $a->getNamespaces(true);
            if (count($namespaces)) {
                foreach ($namespaces as $prefix => $uri) {
                    if (strlen($prefix) == 0) {
                        continue;
                    }
                    $a->registerXPathNamespace($prefix, $uri);
                    if ($a->xpath(sprintf('//%s:%s', $prefix, $methodName))) {
                        return true;
                    }
                }
            }
            if ($a->xpath('//' . $methodName)) {
                return true;
            }
        } catch (Exception $e) {
            $client->log($e->getTraceAsString());
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isOverride()
    {
        return (bool) Mage::getStoreConfig(static::DEALER_ADAPTER_IS_OVERRIDE);
    }

    /**
     * Returns a json formatted exception
     * @param Exception $exception
     */
    public function returnServiceError(\Exception $exception)
    {
        if (!$this->isDev()) {
            $return = array(
                'error' => true,
                'serviceError' => $this->__('Service Call Error'),
                'message' => $this->__($exception->getMessage()),
            );

            if (property_exists($exception, 'responseCode')) {
                $return['message'] = $this->__(trim($exception->getMessage())) . ' (code: ' . $exception->responseCode . ')';
                $return['code'] = $exception->responseCode;
            }
        } else {
            $return = array(
                'error' => true,
                'serviceError' => $exception->getMessage(),
                'message' => sprintf('<pre class="service-call-error">%s</pre>', $exception->getTraceAsString()),
            );
        }

        Mage::app()->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($return));
    }

    /**
     * @param Exception $exception
     */
    public function returnError(\Exception $exception)
    {
        if (!$this->isDev()) {
            $return = array(
                'error' => true,
                'message' => $exception->getMessage(),
            );
        } else {
            $return = array(
                'error' => true,
                'message' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString(),
            );
        }

        Mage::app()->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($return));
    }

    /**
     * Check if specific error is an expected error and it should not be shown to the customer nor logged
     *
     * @param int $responseCode
     * @param string $code
     * @return bool
     */
    public function isExpectedError($responseCode, $code)
    {
        return false;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }
}
