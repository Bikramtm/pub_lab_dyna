<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_RemoteShell
 */
class Omnius_Sandbox_Model_RemoteShell extends Omnius_Sandbox_Model_Shell
{
    /** @var array */
    protected $_config = array();

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->_config = $config;
    }

    /**
     * @param $cmd
     * @return string
     */
    public function addRemoteParams($cmd)
    {
        return sprintf(
            "ssh -T -c arcfour -o Compression=no -x -p %s %s@%s '%s'",
            $this->_config['port'],
            $this->_config['user'],
            $this->_config['host'],
            $cmd
        );
    }

    /**
     * @param $cmd
     * @return mixed|string
     */
    protected function prepare($cmd)
    {
        return $this->addRemoteParams($cmd);
    }
} 
