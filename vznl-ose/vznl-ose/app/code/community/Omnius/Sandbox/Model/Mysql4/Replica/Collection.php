<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mysql4_Replica_Collection
 */
class Omnius_Sandbox_Model_Mysql4_Replica_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /** @var Omnius_Sandbox_Model_Config */
    protected $_config;

    protected $_itemObjectClass = 'Omnius_Sandbox_Model_Replica';

    /**
     * @param null $resource
     */
    public function __construct($resource = null)
    {
        $this->_setIdFieldName('name');
    }

    /**
     * Load data
     *
     * @param   bool $printQuery
     * @param   bool $logQuery
     *
     * @return  Varien_Data_Collection_Db
     */
    public function load($printQuery = false, $logQuery = false)
    {
        if ($this->isLoaded()) {
            return $this;
        }

        $this->_beforeLoad();

        $data = $this->getData();
        $this->resetData();

        if (is_array($data)) {
            foreach ($data as $row) {
                $item = $this->getNewEmptyItem();
                if ($this->getIdFieldName()) {
                    $item->setIdFieldName($this->getIdFieldName());
                }
                $item->addData($row);
                $this->addItem($item);
            }
        }

        $this->_setIsLoaded();
        $this->_afterLoad();
        return $this;
    }

    /**
     * Returns data property
     * @return array
     */
    public function getData()
    {
        if ($this->_data === null) {
            $this->_data = Mage::getSingleton('sandbox/config')->getReplicas(true);
            $this->_afterLoadData();
        }
        return $this->_data;
    }

    /**
     * Get collection size
     *
     * @return int
     */
    public function getSize()
    {
        if (is_null($this->_totalRecords)) {
            $this->_totalRecords = count($this->getData());
        }
        return intval($this->_totalRecords);
    }

    /**
     * Retrieve all ids for collection
     *
     * @return array
     */
    public function getAllIds()
    {
        return array_keys($this->getData());
    }

    /**
     * @return Omnius_Sandbox_Model_Config
     */
    protected function _getConfig()
    {
        if ( ! $this->_config) {
            return $this->_config = Mage::getSingleton('sandbox/config');
        }
        return $this->_config;
    }
}
