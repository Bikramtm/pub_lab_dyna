<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Config
 */
class Omnius_Sandbox_Model_Config
{

    /** @var Omnius_Core_Helper_Data */
    private $_dynaCoreHelper = null;

    public $_sandbox_config_cache_key;

    /** @var Varien_Simplexml_Config */
    protected $_config;

    /** @var Omnius_Sandbox_Helper_Data */
    protected $_helper;

    /** @var array */
    protected $_cache = array();

    /**
     * Loads the XML config
     */
    public function __construct()
    {
        $this->_sandbox_config_cache_key = 'sandbox_config_' . (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');
        $this->loadConfig();
        $this->_helper = Mage::helper('sandbox');
    }

    /**
     * Clears the config cache for the replicas and sandboxes.
     * @return $this
     */
    public function clearCache()
    {
        Mage::app()->getCacheInstance()->remove($this->_sandbox_config_cache_key);
        Mage::dispatchEvent('adminhtml_cache_refresh_type', array('type' => 'config'));
        Mage::dispatchEvent('adminhtml_cache_refresh_type', array('type' => 'config_api'));
        Mage::dispatchEvent('adminhtml_cache_refresh_type', array('type' => 'config_api2'));

        return $this;
    }

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('omnius_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * @param string $path
     * @return string
     */
    public function getValue($path)
    {
        return (string) $this->_config->getNode($path);
    }

    /**
     * @param $method
     * @param $args
     * @return mixed
     * @throws Exception
     */
    public function __call($method, $args)
    {
        if ( ! method_exists($this->_config, $method)) {
            throw new Exception(sprintf('Call to undefined method %s', $method));
        }
        return call_user_func_array(array($this->_config, $method), $args);
    }

    /**
     * @param bool $asArray
     * @return array
     */
    public function getReplicas($asArray = false)
    {
        $replicas = array();
        if ($replicaNodes = $this->getValues('replicas/replica')) {
            foreach ($replicaNodes as $replicaData) {
                if ( ! isset($replicaData['name'])) {
                    Mage::log('Invalid replica configuration');
                    continue;
                }
                $replicas[$replicaData['name']] = $asArray ? $replicaData : Mage::getModel('sandbox/replica')->setData($replicaData);
            }
        }
        return $replicas;
    }

    /**
     * @return array
     */
    public function getSandboxConnections()
    {
        $replicas = $this->getValues('sync_envs');
        if ( ! isset($replicas[0]) || ! isset($replicas[0]['replica'])) {
            return array();
        }

        $replicas = $replicas[0]['replica'];
        if (is_array($replicas)) {
            return array_filter(array_map('trim', $replicas));
        } else {
            return array($replicas);
        }
    }

    /**
     * @param $path
     * @return array
     */
    public function getValues($path)
    {
        return $this->getDynaCoreHelper()->toArray($this->_config->getXpath($path));
    }

    /**
     * @return Varien_Simplexml_Config
     */
    public function getConfig()
    {
        return $this->_config;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function saveReplicas()
    {
        if (Mage::app()->useCache('config')) {
            Mage::app()->saveCache(
                $this->_config->getXmlString(),
                $this->_sandbox_config_cache_key,
                array(Mage_Core_Model_Config::CACHE_TAG)
            );
        }

        $xml = "<?xml version=\"1.0\"?>\n<root>\n".$this->_config->getNode('replicas')->asNiceXml()."</root>";
        if (false == @file_put_contents(Mage::getBaseDir('etc').DS.'replicas.xml', $xml)) {
            throw new Exception('Could not save replica data. Please check file permissions.');
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function reload()
    {
        $this->loadConfig();
        return $this;
    }

    /**
     * @return Varien_Simplexml_Config
     */
    protected function loadConfig()
    {
        if ( ! $this->_config) {
            $cacheConfig = Mage::app()->loadCache($this->_sandbox_config_cache_key);
            if ($cacheConfig) {
                $config = new Varien_Simplexml_Config;
                $config->loadString($cacheConfig);
                return $this->_config = $config;
            } else {
                $configXml = new Omnius_Core_Model_SimpleDOM(file_get_contents(Mage::getBaseDir('etc').DS.'sandbox.xml'));
                $replicas = new Omnius_Core_Model_SimpleDOM(file_get_contents(Mage::getBaseDir('etc').DS.'replicas.xml'));
                $configXml->firstOf('releases')->insertAfterSelf($replicas->firstOf('replicas'));
                $config = new Varien_Simplexml_Config;
                $config->loadString($configXml->asXML());
                if (Mage::app()->useCache('config')) {
                    Mage::app()->saveCache(
                        $config->getXmlString(),
                        $this->_sandbox_config_cache_key,
                        array(Mage_Core_Model_Config::CACHE_TAG)
                    );
                }
                return $this->_config = $config;
            }
        }
        return $this->_config;
    }
}
