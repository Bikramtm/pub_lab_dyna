<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper
 */
abstract class Omnius_Sandbox_Model_Mapper
{
    /** @var Omnius_Sandbox_Helper_Remote */
    protected $_remote;

    /** @var Omnius_Sandbox_Model_Table */
    protected $_tableHelper;

    /** @var array */
    protected $_specialColumns = array();

    /** @var array */
    protected $_excludedColumns = array();

    /** @var array */
    protected $_cache = array();

    /** @var int */
    protected $_priority = 0;

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->_priority;
    }

    /**
     * @param $column
     * @return bool
     */
    protected function _isMappable($column)
    {
        if ( ! isset($this->_cache[__CLASS__][$column])) {
            $idCond = (0 === strpos($column, 'id_')) || (false !== strpos($column, '_id_')) || preg_match('/\_id$/', $column);

            return $this->_cache[__CLASS__][$column] = (! in_array($column, $this->_excludedColumns)) && ($idCond || in_array($column, $this->_specialColumns));
        }
        return $this->_cache[__CLASS__][$column];
    }

    /**
     * @return Omnius_Sandbox_Helper_Remote
     */
    protected function _getRemote()
    {
        if ( ! $this->_remote) {
            $this->_remote = Mage::helper('sandbox/remote');
        }
        return $this->_remote;
    }

    /**
     * @return Omnius_Sandbox_Model_Table
     */
    protected function _getTableHelper()
    {
        if ( ! $this->_tableHelper) {
            $this->_tableHelper = Mage::getSingleton('sandbox/table');
        }
        return $this->_tableHelper;
    }

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    abstract public function supports($table, $column);

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @return mixed
     */
    abstract public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null);
} 
