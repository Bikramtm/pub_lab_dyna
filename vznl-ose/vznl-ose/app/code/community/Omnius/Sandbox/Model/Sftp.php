<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Sftp
 */
class Omnius_Sandbox_Model_Sftp extends Varien_Io_Sftp
{
    /**
     * @return Net_SFTP
     */
    public function getConnection()
    {
        return $this->_connection;
    }

    /**
     * Write a file
     * @param $src Must be a local file name
     */
    public function write($filename, $src, $mode = NET_SFTP_STRING)
    {
        return $this->_connection->put($filename, $src, $mode);
    }
} 
