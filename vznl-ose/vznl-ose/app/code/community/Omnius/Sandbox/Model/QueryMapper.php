<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_QueryMapper
 */
class Omnius_Sandbox_Model_QueryMapper
{
    /** Uncomment the following protected variables to map tables */
    protected $_allowedTables = array (
//        0 => 'catalog_category_anc_categs_index_idx',
//        1 => 'catalog_category_anc_categs_index_tmp',
    );

    protected $_mediaTables = array(
//        'catalog_product_entity_media_gallery',
//        'catalog_product_entity_media_gallery_value',
    );

    protected $_allowedMethods = array(
        'update',
        'insert',
        'delete',
    );

    /** @var Omnius_Sandbox_Model_QueryPreprocessor */
    private $_preprocessor;

    /** @var Omnius_Sandbox_Model_Tokenizer */
    protected $_tokenizer;

    /** @var Omnius_Sandbox_Model_IdentifierMapper */
    protected $_identifierMapper;

    /**
     * @param string $query
     * @param array $bind
     * @param Varien_Db_Adapter_Pdo_Mysql $masterAdapter
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @return string
     */
    public function map($query, $bind, $masterAdapter, $slaveAdapter)
    {
        if ($result = $this->_map($query, $bind, $masterAdapter, $slaveAdapter)) {
            if ((false == is_array($result)) || (is_array($result) && array_key_exists('sql', $result) && !$result['sql'])) {
                return false;
            }
            return $result;
        }
        return false;
    }

    /**
     * @param string $query
     * @param array $bind
     * @param Varien_Db_Adapter_Pdo_Mysql $masterAdapter
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @return string
     */
    protected function _map($query, $bind, $masterAdapter, $slaveAdapter)
    {
        if ($this->_isAllowed($query) && $this->_tableExists($query, $slaveAdapter)) {

            $query = $this->prepareQuery($query, $bind, $masterAdapter, $slaveAdapter);

            if (count($bind)) {
                $queryParts = $this->_getTokenizer()->parse($query);

                /**
                 * The query is bounded but it also contains a WHERE condition
                 * and we first need to process that WHERE part
                 */
                if (isset($queryParts['where'])) {
                    unset($queryParts);
                    $mappedQuery = $this->_map($query, array(), $masterAdapter, $slaveAdapter);
                    $query = $mappedQuery['sql'];
                }

                return $this->mapBoundQuery($query, $bind, $masterAdapter, $slaveAdapter);
            }

            try {
                $table = $this->_getTable($query);

                $triggerMediaSync = false;
                if (in_array($table, $this->_mediaTables)) {
                    $triggerMediaSync = true;
                }
                $whereConditions = $this->_getTokenizer()->getConditions($query);
                foreach ($whereConditions as $sql => $subparts) {
                    $mapped = array();
                    foreach ($subparts['value'] as $id) {
                        $mapped[] = $this->_getIdentifierMapper()->map($table, $subparts['column'], $id, $masterAdapter, $slaveAdapter);
                    }

                    $enclosure = substr(trim($subparts['sql']), 0, 1);
                    if (!in_array($enclosure, array('\'', '"', '`', '('))) {
                        $enclosure = '';
                    }

                    $closure = function ($el) use ($enclosure) {
                        if (strlen($enclosure) && !is_numeric($el)) {
                            if (substr($el, 0, 1) != $enclosure) {
                                if ($enclosure == '(') {
                                    $el = sprintf('%s%s%s', '(', $el, '');
                                } else {
                                    $el = sprintf('%s%s%s', $enclosure, $el, $enclosure);
                                }
                            }
                        } elseif (!is_numeric($el)) {
                            // value is not numeric but we did not manage to detect the enclosing
                            // so we need to assure that the value is not confused for a column
                            $el = sprintf('%s%s%s', '\'', $el, '\'');
                        }

                        return $el;
                    };
                    $query = str_replace(
                        $sql,
                        str_replace($subparts['sql'], join(',', array_map($closure, $mapped)), $sql),
                        $query
                    );
                    unset($mapped);
                    unset($enclosure);
                }

                return $this->_getPreprocessor()->process(Mage::helper('sandbox/category')->remapCategoryPath($query, $slaveAdapter), $bind, $triggerMediaSync);

            } catch (Exception $e) {
                //log?
            }
        }

        return false;
    }

    /**
     * @param $query
     * @param array $bind
     * @param $masterAdapter
     * @param $slaveAdapter
     * @return mixed
     */
    protected function prepareQuery($query, array $bind, $masterAdapter, $slaveAdapter)
    {
        $initialQuery = $query;
        $table = $this->_getTable($query);
        $queryParts = $this->_getTokenizer()->parse($query);
        if (isset($queryParts['where'])) {
            unset($queryParts['where']);
        }

        if (strpos($table, 'catalog_product_entity') === 0 && isset($queryParts['select']) && strpos($queryParts['select'], ' AS `entity_id') !== false) {
            $as = substr($queryParts['select'], 0, strpos($queryParts['select'], 'AS `entity_id'));
            $as = explode(',', $as);
            $oldId = array_pop($as);
            $as = filter_var($oldId, FILTER_SANITIZE_NUMBER_INT);
            $newValue = $this->_getIdentifierMapper()->map('catalog_product_entity', 'entity_id', $as, $masterAdapter, $slaveAdapter);
            $newId = str_replace($as, $newValue, $oldId);
            $initialQuery = str_replace($oldId, $newId, $initialQuery);
        }

        $query = join(' ', $queryParts) . ',';
        if (false !== strpos($query, ' SET ')) {
            $query = ',' . preg_replace('/^.+?(?=SET)(SET)/i', '', $query);

            preg_match_all('/(?<=(?<![a-zA-z])\()[^)]+(?=\))/sm', $query, $parts);

            $tr = array(
                '\'' => '',
                '"' => '',
                '`' => '',
            );

            if (!count($parts[0])) {
                // The format is something like "UPDATE `eav_attribute` SET `entity_type_id` = ?, `attribute_code` = ?, `attribute_model` = ?, ...."
                preg_match_all('/(?<=\,)[^,]+(?=\,)/sm', $query, $subParts);
                $this->parseInitialQuery($masterAdapter, $slaveAdapter, $subParts, $tr, $table, $initialQuery);
            }
        }

        return $initialQuery;
    }

    /**
     * @param string $query
     * @param array $bind
     * @param Varien_Db_Adapter_Pdo_Mysql $masterAdapter
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @return array
     */
    protected function mapBoundQuery($query, array $bind, $masterAdapter, $slaveAdapter)
    {
        try {
            $table = $this->_getTable($query);
            $triggerMediaSync = false;
            if (in_array($table, $this->_mediaTables)) {
                $triggerMediaSync = true;
            }
            $chunks = $this->_getTokenizer()->getBoundConditions($query, $bind);

            $mappedBind = $this->getMappedBind($masterAdapter, $slaveAdapter, $chunks, $table);

            return $this->_getPreprocessor()->process(Mage::helper('sandbox/category')->remapCategoryPath($query, $slaveAdapter), $mappedBind, $triggerMediaSync);

        } catch (Exception $e) {
            //log?
            return false;
        }
    }

    /**
     * @param $query
     * @return bool
     */
    protected function _isAllowed($query)
    {
        return (in_array(strtolower($this->_getMethod($query)), $this->_allowedMethods) && in_array(strtolower($this->_getTable($query)), $this->_allowedTables));
    }

    /**
     * @param string $query
     * @param Varien_Db_Adapter_Pdo_Mysql $adapter
     * @return mixed
     */
    protected function _tableExists($query, $adapter)
    {
        return $adapter->isTableExists($this->_getTable($query));
    }

    /**
     * @param $query
     * @return bool|string
     */
    protected function _getMethod($query)
    {
        if (preg_match('/^(update|insert|delete)/i', trim($query), $matches)) {
            return strtolower($matches[0]);
        }
        return false;
    }

    /**
     * @param $query
     * @return string
     */
    protected function _getTable($query)
    {
        $method = $this->_getMethod($query);

        if (!$method) {
            return false;
        }

        $parts = explode(' ', trim($query));
        $translate = array(
            '\'' => '',
            '`' => '',
            '"' => '',
        );
        if ($method == 'update') {
            if (strtolower($parts[1]) === 'ignore') { //if next token is "ignore" then we have a "UPDATE IGNORE TABLE ..." query
                $return = strtr($parts[2], $translate);
            } else {
                $return = strtr($parts[1], $translate);
            }
        } else {
            $return = strtr($parts[2], $translate);
        }

        return $return;
    }

    /**
     * @return Omnius_Sandbox_Model_QueryPreprocessor
     */
    private function _getPreprocessor()
    {
        if ( ! $this->_preprocessor) {
            $this->_preprocessor = Mage::getSingleton('sandbox/queryPreprocessor');
        }
        return $this->_preprocessor;
    }

    /**
     * @return Omnius_Sandbox_Model_Tokenizer
     */
    protected function _getTokenizer()
    {
        if ( ! $this->_tokenizer) {
            $this->_tokenizer = Mage::getSingleton('sandbox/tokenizer');
        }
        return $this->_tokenizer;
    }

    /**
     * @return Omnius_Sandbox_Model_IdentifierMapper
     */
    protected function _getIdentifierMapper()
    {
        if ( ! $this->_identifierMapper) {
            $this->_identifierMapper = Mage::getSingleton('sandbox/identifierMapper');
        }
        return $this->_identifierMapper;
    }

    /**
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param $chunks
     * @param $table
     * @return array
     * @throws Exception
     */
    protected function getMappedBind($masterAdapter, $slaveAdapter, $chunks, $table)
    {
        $mappedBind = array();
        foreach ($chunks as &$set) {
            if (is_array($set)) {
                foreach ($set as $column => $value) {
                    if ($value === '@@__NEEDS_REMOVAL__@@') {
                        continue;
                    }
                    $mappedBind[] = $this->_getIdentifierMapper()->map($table, $column, $value, $masterAdapter, $slaveAdapter, $set);
                }
            } else {
                $mappedBind[] = $set;
            }
        }
        unset($set);

        return $mappedBind;
    }

    /**
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param $subParts
     * @param $tr
     * @param $table
     * @param $initialQuery
     * @throws Exception
     */
    protected function parseInitialQuery($masterAdapter, $slaveAdapter, $subParts, $tr, $table, &$initialQuery)
    {
        $prevCol = null;
        foreach ($subParts[0] as $columnReference) {
            $column = str_replace(' ', '', trim(strtr(trim($columnReference), $tr), '()'));
            if (false === strpos($column, '=?')) {
                $columnParts = explode('=', $column);
                $column = trim($columnParts[0]);
                if (isset($columnParts[1]) && trim($columnParts[1])) {
                    $value = trim($columnParts[1]);
                } else {
                    $value = $column;
                    $column = $prevCol;
                }

                $initialQuery = str_replace(
                    $columnReference,
                    str_replace($value, $this->_getIdentifierMapper()->map($table, $column, $value, $masterAdapter, $slaveAdapter), $columnReference),
                    $initialQuery
                );
                $prevCol = $column;
            }
        }
    }
} 
