<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_QueryPreprocessor
 */
class Omnius_Sandbox_Model_QueryPreprocessor
{
    /** @var Omnius_Sandbox_Model_Tokenizer */
    protected $_tokenizer;

    /**
     * @param $query
     * @param $bind
     * @param bool $triggerMediaSync
     * @return array
     */
    public function process($query, $bind, $triggerMediaSync = false)
    {
        $query = $this->preventDuplicateKeyWhenUpdate(trim($query));

        $mustHaveOnDuplicateExpr = false;
        $c1Cond = preg_match('/(\(\?\,[\)\(\s\,\?]*\?\))/i', $query) //example query: INSERT INTO `eav_entity_attribute` (`entity_type_id`, `attribute_set_id`, `attribute_group_id`, `attribute_id`, `sort_order`) VALUES (?, ?, ?, ?, ?)
            || preg_match('/SET(?:\s?)((?<=SET)(.*)(?:\s?)\=(?:\s?)\?)(?:\s?)WHERE/', $query); //example query: UPDATE `eav_attribute` SET `entity_type_id` = ? WHERE (attribute_id=\'128\')
        $c2Cond = count($bind) //is a bound query
            && !preg_match('/ON DUPLICATE/i', $query);  //doesn't have "ON DUPLICATE" statement already
        if (preg_match('/^insert/i', trim($query)) //it's an insert
            && $c2Cond
            && $c1Cond
        ) {
            $mustHaveOnDuplicateExpr = true;
        }

        if ($mustHaveOnDuplicateExpr) {
            $boundConditions = $this->_getTokenizer()->getBoundConditions($query, $bind);
            $columns = array_keys($boundConditions[0]);
            $query = sprintf('%s ON DUPLICATE KEY UPDATE %s', trim($query), $this->buildOnDuplicateDeclaration($columns));
            unset($boundConditions, $columns);
        }

        return array(
            'sql' => $query,
            'bind' => $bind,
            'media_sync' => $triggerMediaSync,
        );
    }

    /**
     * Adds "INGORE" statement to UPDATE queries
     * before: "UPDATE `eav_attribute_set` SET `entity_type_id` = ?, `attribute_set_name` = ? WHERE (attribute_set_id='?')"
     * after: "UPDATE IGNORE `eav_attribute_set` SET `entity_type_id` = ?, `attribute_set_name` = ? WHERE (attribute_set_id=\'71\')"
     *
     * @param $query
     * @return mixed
     */
    protected function preventDuplicateKeyWhenUpdate($query)
    {
        if (preg_match('/^update (?!ignore)/i', trim($query))) {
            $query = preg_replace('/^update\s/i', '$0IGNORE ', $query);
        }
        return $query;
    }

    /**
     * @param array $columns
     * @return string
     */
    protected function buildOnDuplicateDeclaration(array $columns)
    {
        $assignments = array();
        foreach ($columns as $column) {
            $assignments[] = sprintf('`%s` = VALUES(`%s`)', $column, $column);
        }
        return join(', ', $assignments);
    }

    /**
     * @return Omnius_Sandbox_Model_Tokenizer
     */
    protected function _getTokenizer()
    {
        if ( ! $this->_tokenizer) {
            $this->_tokenizer = Mage::getSingleton('sandbox/tokenizer');
        }
        return $this->_tokenizer;
    }
}
