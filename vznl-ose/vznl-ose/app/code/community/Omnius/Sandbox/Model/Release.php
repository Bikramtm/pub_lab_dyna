<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Release
 *
 * @method string getReplica()
 * @method string getStatus()
 * @method string getVersion()
 * @method string getIsHeavy()
 * @method int getWebsiteId()
 * @method Omnius_Sandbox_Model_Release setStatus()
 * @method Omnius_Sandbox_Model_Release setFinishedAt()
 * @method Omnius_Sandbox_Model_Release setExecutedAt()
 * @method Omnius_Sandbox_Model_Release setWebsiteId(int $website_id)
 */
class Omnius_Sandbox_Model_Release extends Mage_Core_Model_Abstract
{
    const STATUS_PENDING = 'pending';
    const STATUS_RUNNING = 'running';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    const MESSAGE_INFO = 'INFO';
    const MESSAGE_ERROR = 'ERROR';
    const MESSAGE_NOTICE = 'NOTICE';
    const MESSAGE_SUCCESS = 'SUCCESS';

    protected $_adapter = null;

    protected $_exportTables = array(
//        'catalog_category_anc_categs_index_idx',
//        'catalog_category_anc_categs_index_tmp',
    );

    /**
     * @var Mage_Core_Model_Website
     */
    protected $website;

    /**
     * Constructor override
     */
    protected function _construct()
    {
       $this->_init("sandbox/release");
    }

    /**
     * @return array
     */
    public function getTables()
    {
        $conn  = Mage::getSingleton('core/resource')->getConnection('core_read');
        $existingTables = array_keys($conn->fetchAssoc('SHOW TABLES'));

        $handledTables = $this->getExportTables();

        return array_intersect($existingTables, $handledTables); //remove tables that do not exist anymore
    }

    protected function getExportTables()
    {
        return $this->_exportTables;
    }

    /**
     * @param Omnius_Sandbox_Model_Replica $replica
     * @return bool|Varien_Db_Adapter_Pdo_Mysql
     */
    protected function getAdapter(Omnius_Sandbox_Model_Replica $replica)
    {
        if ($this->_adapter == null) {
            $defaultConf = array(
                'initStatements' => 'SET NAMES utf8',
                'type' => 'pdo_mysql',
                'charset' => 'utf8',
            );

            try {
                $mysqlConf = array(
                    'host' => $replica->getMysqlHost(),
                    'username' => $replica->getMysqlUser(),
                    'password' => $replica->getMysqlPassword(),
                    'dbname' => $replica->getMysqlDatabase()
                );
                $mysqlConf = array_merge($defaultConf, $mysqlConf);
                $this->_adapter = new Varien_Db_Adapter_Pdo_Mysql($mysqlConf);
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                return false;
            }
        }

        return $this->_adapter;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        if ( ! ($messages = json_decode($this->getData('messages'), true))) {
            return array();
        }
        return $messages;
    }

    /**
     * @param $message
     * @param string $type
     * @return $this
     */
    public function addMessage($message, $type = self::MESSAGE_INFO)
    {
        $this->setData('decoded_messages', $this->getMessages());

        $messages = is_array($this->getData('decoded_messages')) ? $this->getData('decoded_messages') : array();
        $messages[] = sprintf('[%s][%s] %s', date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())), $type, $message);

        $this->setData('decoded_messages', $messages);
        $this->setData('messages',json_encode($this->getData('decoded_messages')));

        return $this;
    }

    /**
     * @return $this
     */
    public function incrementTries()
    {
        $this->setData('tries', 1 + (int) $this->getData('tries'));
        return $this;
    }

    /**
     * Persists data in the database
     * @return $this|Mage_Core_Model_Abstract
     * @throws Exception
     */
    public function save()
    {
        //if status was already set to success, prevent other changes
        $temp = Mage::getModel('sandbox/release')->load($this->getId());
        if ($temp->getStatus() == 'success') {
            return $this;
        }

        if ( ! $this->hasData('created_at')) {
            $this->setData('created_at', now());
        }

        return parent::save();
    }

    /**
     * @param Mage_Core_Model_Website $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
        $this->setWebsiteId($website->getId());
    }
}
