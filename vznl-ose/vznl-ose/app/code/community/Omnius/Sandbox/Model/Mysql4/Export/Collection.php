<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mysql4_Export_Collection
 */
class Omnius_Sandbox_Model_Mysql4_Export_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Constructor override
     */
    public function _construct()
    {
        $this->_init('sandbox/export');
    }
}
