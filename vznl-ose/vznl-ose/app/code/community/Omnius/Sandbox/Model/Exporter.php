<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Exporter
 */
class Omnius_Sandbox_Model_Exporter
{
    /** @var Varien_Db_Adapter_Interface */
    protected $_conn;

    /** @var Omnius_Sandbox_Model_Sftp */
    protected $_sftpClient;

    /** @var Omnius_Sandbox_Model_CatalogExporter */
    protected $_catalogExporter;

    /**
     * @param Omnius_Sandbox_Model_Export $export
     * @return Omnius_Sandbox_Model_Export
     */
    public function export(Omnius_Sandbox_Model_Export $export)
    {
        if ($this->_lockExport($export->getId())) {
            $export
                ->setStatus(Omnius_Sandbox_Model_Export::STATUS_RUNNING) //in order to not load the release again
                ->addMessage('Export locked')
                ->save();

            try {
                /** @var Omnius_Sandbox_Model_ExportStrategy_ExportStrategy $strategy */
                if ( ! ($strategy = Mage::getSingleton(sprintf('sandbox/exportStrategy_%sXmlCatalog', $export->getExportType())))) {
                    throw new Exception('Invalid strategy set on export.');
                }

                $export->addMessage(sprintf('Starting %s export', $export->getExportType()));

                $this->getSftp()->open($export->getSftpArgs());
                $export->addMessage('SFTP connection made');

                $export
                    ->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                    ->save();
                if ($this->getSftp()->cd($export->getExportLocation())) {

                    $export->addMessage(sprintf('Cd to location %s', $export->getExportLocation()));

                    if (Omnius_Sandbox_Model_Export::DYNA_TYPE == $export->getExportType()) {
                        /**
                         * The export for dyna catalog must generate two files, one for belcompany
                         * and one for the retail
                         */
                        $storeIds = array_filter(array_map(function(Mage_Core_Model_Store $store){
                            if (in_array($store->getCode(), array('default', 'belcompany'))) {
                                return $store->getId();
                            }
                            return false;
                        }, Mage::app()->getStores()));

                        $appEmulation = Mage::getSingleton('core/app_emulation');
                        foreach ($storeIds as $storeId) {
                            $exportXml = null;
                            $remoteFilePath = $this->_addTimestamp($export->getExportFilename());
                            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
                            $collection = Mage::getModel('catalog/product')
                                ->getCollection()
                                ->addAttributeToSelect('*')
                                ->addAttributeToFilter('is_deleted', array('neq' => 1))
                                ->addAttributeToFilter('type_id', array('eq' => 'simple'))
                                ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                                ->addStoreFilter();

                            $exportXml = $this->getExporter()->export($collection, $strategy, $exportXml);
                            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

                            $export->addMessage(sprintf('Copying export file to %s', $export->getExportPath()));
                            $succeeded = $this->getSftp()->write(
                                $remoteFilePath,
                                str_replace(
                                    '<?xml version="1.0"?>',
                                    '<?xml version="1.0" encoding="UTF-8"?>',
                                    $exportXml->asPrettyXML()
                                )
                            );
                            if ( ! $succeeded) {
                                $this->getSftp()->close();
                                throw new Exception(sprintf('Could not copy export file ("%s").', $remoteFilePath));
                            } else {
                                $export->addMessage('Upload succeeded');
                            }
                        }

                    } else {
                        $remoteFilePath = $this->_addTimestamp($export->getExportFilename());

                        $appEmulation = Mage::getSingleton('core/app_emulation');
                        $exportXml = null;
                        foreach ($this->_getStoreIds($export) as $storeId) {
                            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

                            $collection = Mage::getModel('catalog/product')
                                ->getCollection()
                                ->addAttributeToSelect('*')
                                ->addAttributeToFilter('is_deleted', array('neq' => 1))
                                ->addAttributeToFilter('type_id', array('eq' => 'simple'))
                                ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                                ->addStoreFilter();

                            $exportXml = $this->getExporter()->export($collection, $strategy, $exportXml);

                            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
                        }

                        $export->addMessage(sprintf('Copying export file to %s', $export->getExportPath()));
                        $succeeded = $this->getSftp()->write(
                            $remoteFilePath,
                            str_replace(
                                '<?xml version="1.0"?>',
                                '<?xml version="1.0" encoding="UTF-8"?>',
                                $exportXml->asPrettyXML()
                            )
                        );

                        if ( ! $succeeded) {
                            $this->getSftp()->close();
                            throw new Exception(sprintf('Could not copy export file ("%s").', $remoteFilePath));
                        } else {
                            $export->addMessage('Upload succeeded');
                        }
                    }

                } else {
                    $this->getSftp()->close();
                    throw new Exception(sprintf('Could not cd to export path ("%s")', $export->getExportLocation()));
                }

                $this->getSftp()->close();
                $export->addMessage('Closed SFTP connection');

                /**
                 * Return release after updating finish timestamp and status
                 */
                return $export
                    ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                    ->setStatus(Omnius_Sandbox_Model_Export::STATUS_SUCCESS)
                    ->addMessage(sprintf('Export successfully made at %s', $export->getExportPath()), Omnius_Sandbox_Model_Export::MESSAGE_SUCCESS);
            } catch (Exception $e) {
                return $export
                    ->addMessage($e->getMessage(), Omnius_Sandbox_Model_Export::MESSAGE_ERROR)
                    ->setStatus(Omnius_Sandbox_Model_Release::STATUS_ERROR)
                    ->incrementTries()
                    ->save();
            }
        }
        return $export->addMessage(sprintf(
            'Could not acquire lock for export (ID: %s). Already running or successfully finished.',
            $export->getId()
        ), Omnius_Sandbox_Model_Export::MESSAGE_ERROR);
    }

    /**
     * @param Omnius_Sandbox_Model_Export $export
     * @return array
     */
	protected function _getStoreIds(Omnius_Sandbox_Model_Export $export)
	{
		$storeIds = array_filter(array_map(function(Mage_Core_Model_Store $store){
			if (in_array($store->getCode(), array('default', 'belcompany'))) {
				return $store->getId();
			}
			return false;
		}, Mage::app()->getStores()));

		sort($storeIds);

		return $storeIds;
	}

    /**
     * @param $filename
     * @param null $type
     * @return string
     */
    protected function _addTimestamp($filename, $type = null)
    {
        if (false !== strpos($filename, '.')) {
            $parts = explode('.', $filename);
            $filename = reset($parts);
            $ext = '.'.end($parts);
        } else {
            $ext = '';
        }
        return sprintf('%s_%s%s%s', $filename, strftime('%Y%m%d%H%M%S', time()), ($type ? sprintf('_%s', strtolower($type)) : ''), $ext);
    }

    /**
     * @param $exportId
     * @return bool
     */
    protected function _lockExport($exportId)
    {
        $tableName = Mage::getSingleton('core/resource')->getTableName('sandbox/export');
        $bind  = array('status' => Omnius_Sandbox_Model_Export::STATUS_RUNNING);
        $where = array('id = (?)' => $exportId, 'status NOT IN (?)' => array(Omnius_Sandbox_Model_Export::STATUS_RUNNING, Omnius_Sandbox_Model_Export::STATUS_SUCCESS));
        $rowsCount = $this->_getConnection()->update($tableName, $bind, $where);
        return $rowsCount > 0;
    }

    /**
     * @return Varien_Db_Adapter_Interface
     */
    protected function _getConnection()
    {
        if ( ! $this->_conn) {
            return $this->_conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        }
        return $this->_conn;
    }

    /**
     * @return Omnius_Sandbox_Model_Sftp
     */
    protected function getSftp()
    {
        if ( ! $this->_sftpClient) {
            $this->_sftpClient = new Omnius_Sandbox_Model_Sftp();
        }
        return $this->_sftpClient;
    }

    /**
     * @return Omnius_Sandbox_Model_CatalogExporter
     */
    protected function getExporter()
    {
        if ( ! $this->_catalogExporter) {
            return $this->_catalogExporter = Mage::getSingleton('sandbox/catalogExporter');
        }
        return $this->_catalogExporter;
    }
}
