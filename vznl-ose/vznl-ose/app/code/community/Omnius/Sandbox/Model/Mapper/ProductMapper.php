<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper_ProductMapper
 */
class Omnius_Sandbox_Model_Mapper_ProductMapper extends Omnius_Sandbox_Model_Mapper
{
    /** @var int */
    protected $_priority = 200;

    /** @var array */
    protected $_specialColumns = array(
        'sku',
    );

    /** @var array */
    protected $_excludedColumns = array(
        'website_id',
        'type_id',
        'store_id',
        'entity_type_id',
        'attribute_set_id',
        'product_match_rule_id',
        'rule_id',
        'group_id',
    );

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    public function supports($table, $column)
    {
        //TODO improve check
        return ((false !== strpos($table, 'product')) || (in_array($column, array('entity_id', 'product_id', 'sku'))))
            && ( ! in_array($column, $this->_excludedColumns)/* && ( ! 0 === strpos($table, 'catalog_product_entity_'))*/);
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return mixed
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        if ($table === 'catalog_product_agent_visibility' && $column === 'entity_id') {
            return $this->mapVisibilityGroups($table, $column, $value, $masterAdapter, $slaveAdapter, $row);
        }

        if ($this->_isMappable($column)) {
            if ($this->_getRemote()->isSlaveProductLocked($value, $column, $masterAdapter, $slaveAdapter)) {
                throw new Exception('Slave product is locked. No changes are allowed');
            }

            if ( ! ($result = $this->_getRemote()->getSlaveId($value, $column, $masterAdapter, $slaveAdapter, $row))) {
                $replicaConfig = $slaveAdapter->getConfig();
                $message = sprintf('%s: Item (%s.%s=%s) not present on replica (%s:%s)', get_class($this), $table, $column, $value, $replicaConfig['host'], $replicaConfig['dbname']);
                Mage::log($message, null, 'replication_missing.log', true);
                throw new Exception($message);
            }
            return $result;
        }
        return $value;
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return mixed
     */
    protected function mapVisibilityGroups($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        $value = trim($value, '\'"');
        if (($deletedItems = Mage::registry(Omnius_Sandbox_Model_Sandbox::DELETED_PRODUCT_FILTER_REGISTRY)) && isset($deletedItems[$value])) {
            $item = $deletedItems[$value];
            return $slaveAdapter
                ->fetchOne('
                SELECT entity_id 
                FROM `catalog_product_agent_visibility` 
                WHERE product_id=:product 
                AND group_id=:group',
                    array(
                        ':product' => $item['product_id'],
                        ':group' => $item['group_id']
                    )
                );
        }
        return $value;
    }
}
