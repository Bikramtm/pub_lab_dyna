<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper_MediaMapper
 */
class Omnius_Sandbox_Model_Mapper_MediaMapper extends Omnius_Sandbox_Model_Mapper
{
    /** @var int */
    protected $_priority = 1000;

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    public function supports($table, $column)
    {
        return false !== strpos($table, 'media_gallery');
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param Varien_Db_Adapter_Pdo_Mysql $masterAdapter
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return mixed
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        $valueId = @intval(strtr($value, array('\''=> '', '"' => '')));
        $items = Mage::registry('sandbox_media_deletion');
        if ($valueId && is_array($items) && trim($table) === 'catalog_product_entity_media_gallery' && $column == 'value_id') {
            if (($product = Mage::registry('current_product')) && $this->_getRemote()->isSlaveProductLocked($product->getId(), 'entity_id', $masterAdapter, $slaveAdapter, $row)) {
                throw new Exception('Product is locked. Media replication now allowed');
            }
            if ( ! isset($items[$valueId])) {
                return $value;
            }
            $row = $items[$valueId];
            $masterProductId = $row['entity_id'];
            $imageName = $row['value'];
            $attributeId = $row['attribute_id'];
            if ($slaveProductId = $this->_getRemote()->getSlaveId($masterProductId, 'entity_id', $masterAdapter, $slaveAdapter)) {
                $sql = sprintf(
                    'DELETE FROM `catalog_product_entity_media_gallery` WHERE entity_id="%s" AND value="%s" AND attribute_id="%s";',
                    $slaveProductId,
                    $imageName,
                    $attributeId
                );
                $slaveAdapter->query($sql);
            }
        }

        if ($column == 'entity_id') {
            return Mage::getSingleton('sandbox/mapper_productMapper')->map($table, $column, $value, $masterAdapter, $slaveAdapter);
        }

        return $value;
    }
}
