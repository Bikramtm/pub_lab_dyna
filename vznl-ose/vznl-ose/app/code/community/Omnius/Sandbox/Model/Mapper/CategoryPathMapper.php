<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper_CategoryPathMapper
 */
class Omnius_Sandbox_Model_Mapper_CategoryPathMapper extends Omnius_Sandbox_Model_Mapper
{
    /** @var int */
    protected $_priority = 501;

    /** @var array */
    protected $_excludedColumns = array(

    );

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    public function supports($table, $column)
    {
        return ( ! in_array($column, $this->_excludedColumns)) && (0 === strpos($table, 'catalog_category_') && $column === 'path');
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return mixed
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        $ids = explode('/', trim($value, '\'"'));
        foreach ($ids as &$categoryId) {
            if (is_numeric($categoryId)) {
                $categoryId = $this->_getRemote()->mapSlaveUsingUniqueId('catalog_category_entity', 'entity_id', $categoryId, $masterAdapter, $slaveAdapter);
            }
        }
        return join('/', $ids);
    }
}
