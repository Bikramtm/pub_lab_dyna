<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Tokenizer
 */
class Omnius_Sandbox_Model_Tokenizer
{
    protected $_actions = array('alter', 'create', 'drop', 'select', 'delete', 'insert', 'update', 'from', 'where', 'limit', 'order');
    protected $_operators = array('=', '<>', '!=', '<', '<=', '>', '>=', 'like', 'clike', 'slike', 'not', 'is', 'in', 'between');
    protected $_conjuctions = array('by', 'as', 'on', 'into', 'from', 'where', 'with');
    protected $_start = array('{', '(');
    protected $_end = array('}', ')');
    protected $_tokens = array(',', ' ');
    protected $_query = '';
    protected $_regex = '
        /
            (                                                                                                           #begin group
            (?:--|\\#)[\\ \\t\\S]*                                                                                      #inline comments
            |(?:<>|<=>|>=|<=|==|=|!=|!|<<|>>|<|>|\\|\\||\\||&&|&|-|\\+|\\*(?!\/)|\/(?!\\*)|\\%|~|\\^|\\?)               #logical operators
            |[\\[\\]\\(\\),;`]|\\\'\\\'(?!\\\')|\\"\\"(?!\\"")                                                          #empty quotes
            |".*?(?:(?:""){1,}"|(?<!["\\\\])"(?!")|\\\\"{2})|\'.*?(?:(?:\'\'){1,}\'|(?<![\'\\\\])\'(?!\')|\\\\\'{2})    #string quotes
            |\/\\*[\\ \\t\\n\\S]*?\\*\/                                                                                 #c comments
            |(?:[\\w:@]+(?:\\.(?:\\w+|\\*)?)*)|[\t\ ]+                                                                  #wordds, column strings, params
            |[\.]|[\s]                                                                                                  #period and whitespace
            )                                                                                                           #end group
        /smxi
    ';

    /**
     * @param $sql
     * @return array
     */
    public function parse($sql)
    {
        return $this->tokenize(ltrim(preg_replace('/[\\s]{2,}/', ' ', $sql)));
    }

    /**
     * @param $sql
     * @return array
     */
    public function getConditions($sql)
    {
        $parts = $this->tokenize(ltrim(preg_replace('/[\\s]{2,}/', ' ', $sql)));
        if (isset($parts['where'])) {
            return $this->extractWhere($parts['where']);
        }
        return array();
    }

    /**
     * @param $where
     * @return array
     */
    protected function extractWhere($where)
    {
        $where = preg_replace('/^where /i', '', trim($where));
        $separated = explode(' AND ', $where);
        $parts = [];
        foreach ($separated as $s) {
            $p = explode(' OR ', $s);
            foreach ($p as $r) {
                $parts[] = $r;
            }
        }

        $result = array();
        $tr = array(
            /*'\'' => '',
            '"' => '',
            '`' => '',*/
        );

        foreach ($parts as $part) {
            $subparts = preg_split('/((!=|=|<>|<|<=|>|>=)|(\s(not like|like|is not|not in|not|is|in|between)))/smi', $part);
            if (count($subparts) === 2) {
                $data = array(
                    'sql' => trim(trim($subparts[1]), '()'),
                );
                $subparts = array_map(function($el) use ($tr) {
                    return trim(strtr(trim($el), $tr), '()');
                }, $subparts);

                if (false !== strpos($subparts[0], '.')) {
                    preg_match('/\.(.*)/', $subparts[0], $match);
                    $subparts[0] = $match[1];
                }

                $data = array_merge($data, array(
                    'column' => trim($subparts[0], '\'\\`"'),
                    'value' => array_map('trim', explode(',', $subparts[1])),
                ));

                $result[$part] = $data;
            }
        }
        return $result;
    }

    /**
     * @param $query
     * @param $bind
     * @return array
     */
    public function getBoundConditions($query, $bind)
    {
        /**
         * If the given query has a WHERE part,
         * we need to remove it as it should already be
         * handled before calling this method and it eases the processing
         * This method only maps the bound part of the query
         */
        $queryParts = $this->tokenize($query);
        if (isset($queryParts['where'])) {
            unset($queryParts['where']);
        }

        $query = join(' ', $queryParts) . ',';
        if (false !== strpos($query, ' SET ')) {
            $query = ',' . preg_replace('/^.+?(?=SET )(SET )/i', '', $query);
        } else {
            $query = ',' . $query;
        }

        preg_match_all('/(?<=\()[^)]+(?=\))/sm', $query, $parts);
        $tr = array(
            '\'' => '',
            '"' => '',
            '`' => '',
        );

        if ( ! count($parts[0])) { //the format is something like "UPDATE `eav_attribute` SET `entity_type_id` = ?, `attribute_code` = ?, `attribute_model` = ?, ...."
            preg_match_all('/(?<=\,)[^,]+(?=\,)/sm', $query, $subParts);
            $columns = array();
            foreach ($subParts[0] as $columnReference) {
                $column = str_replace(' ', '', trim(strtr(trim($columnReference), $tr), '()'));
                if (false != strpos($column, '=?')) {
                    $column = str_replace('=?', '', $column);
                    if (false !== strpos($column, '.')) {
                        list($table, $column) = explode('.', $column);
                    }
                    array_push($columns, $column);
                }
            }

            if ( !count($columns)) { //special case
                return array();
            }
            $chunks = array_chunk($bind, count($columns));

        } else {
            $columns = trim(strtr(trim($parts[0][0]), $tr), '()');

            $cols = array();
            foreach (array_map('trim', explode(',', $columns)) as $column) {
                $cols[] = $column;
            }
            $columns = $cols;

            $chunks = $this->getChunks($bind, $parts, $tr);
        }

        foreach ($chunks as &$set) {
            if (isset($set[0]) && is_array($set[0]) && count($set) == 1) {
                $set = $set[0];
            }
            $set = array_combine($columns, $set);
        }

        return $chunks;
    }

    /**
     * @param $sql
     * @return array
     */
    protected function tokenize($sql)
    {
        $this->_query = array();
        preg_match_all($this->_regex, $sql, $parts);
        $this->_tokens = $parts[0];
        $tokenCount = count($this->_tokens);
        if (isset($this->_tokens[0]) === true) {
            $section = $this->_tokens[0];
        }

        for ($t = 0; $t < $tokenCount; $t++) {
            if (in_array($this->_tokens[$t], $this->_start)) {
                $sub = $this->tokenizeInner($this->_tokens, $t);
                $this->_query[$section] .= $sub;
            } else {
                if (in_array(strtolower($this->_tokens[$t]), $this->_actions) && !isset($this->_query[$this->_tokens[$t]])) {
                    $section = strtolower($this->_tokens[$t]);
                }
                if (!isset($this->_query[$section])) {
                    $this->_query[$section] = '';
                }
                $this->_query[$section] .= $this->_tokens[$t];
            }
        }

        return $this->_query;
    }

    /**
     * @param $tokens
     * @param $position
     * @return string
     */
    protected function tokenizeInner($tokens, &$position)
    {
        $sub = $tokens[$position];
        $tokenCount = count($tokens);
        $position++;
        while (!in_array($tokens[$position], $this->_end) && $position < $tokenCount) {

            if (in_array($tokens[$position], $this->_start)) {
                $sub .= $this->tokenizeInner($tokens, $position);
            } else {
                $sub .= $tokens[$position];
            }
            $position++;
        }
        $sub .= $tokens[$position];
        return $sub;
    }

    /**
     * @param $bind
     * @param $parts
     * @param $tr
     */
    protected function getChunks($bind, $parts, $tr)
    {
        $chunks = [];
        $k = 0;
        foreach ($parts[0] as $part) {
            if (preg_match('/^(.*)\?(\,?)(.*)$/', $part)) {
                $chunk = array_map(function ($el) use ($tr) {
                    return strtr($el, $tr);
                }, array_filter(array_map('trim', explode(',', $part))));

                foreach ($chunk as &$placeholder) {
                    if ($placeholder === '?') {
                        $placeholder = isset($bind[$k]) ? $bind[$k] : null;
                        $k++;
                    } else {
                        $placeholder = '@@__NEEDS_REMOVAL__@@';
                    }
                }
                $chunks[] = $chunk;
            }
        }
        
        return $chunks;
    }
}
