<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper_CategoryMapper
 */
class Omnius_Sandbox_Model_Mapper_CategoryMapper extends Omnius_Sandbox_Model_Mapper
{
    /** @var int */
    protected $_priority = 500;

    /** @var array */
    protected $_excludedColumns = array(
        'left_id',
        'right_id',
        'path',
    );

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    public function supports($table, $column)
    {
        return ( ! in_array($column, $this->_excludedColumns)) && ($column === 'category_id' || ((0 === strpos($table, 'catalog_category_')) && in_array($column, array('entity_id', 'category_id', 'parent_id'))));
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return mixed
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        $value = trim($value, '\'"`');
        if ($this->_isMappable($column)) {

            if ( ! ($result = $this->_getRemote()->mapSlaveUsingUniqueId('catalog_category_entity', 'entity_id', $value, $masterAdapter, $slaveAdapter))) {
                $replicaConfig = $slaveAdapter->getConfig();
                $message = sprintf('%s: Item (%s.%s=%s) not present on replica (%s:%s)', get_class($this), $table, $column, $value, $replicaConfig['host'], $replicaConfig['dbname']);
                Mage::log($message, null, 'replication_missing.log', true);
                throw new Exception($message);
            }
            return $result;
        }
        return $value;
    }
}
