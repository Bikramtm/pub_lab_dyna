<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mysql4_Export
 */
class Omnius_Sandbox_Model_Mysql4_Export extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Constructor override
     */
    protected function _construct()
    {
        $this->_init('sandbox/export', 'id');
    }
}
