<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Export
 *
 * @method string getStatus()
 * @method Omnius_Sandbox_Model_Release setStatus()
 * @method Omnius_Sandbox_Model_Release setFinishedAt()
 * @method Omnius_Sandbox_Model_Release setExecutedAt()
 */
class Omnius_Sandbox_Model_Export extends Mage_Core_Model_Abstract
{
    const STATUS_PENDING = 'pending';
    const STATUS_RUNNING = 'running';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    const MESSAGE_INFO = 'INFO';
    const MESSAGE_ERROR = 'ERROR';
    const MESSAGE_NOTICE = 'NOTICE';
    const MESSAGE_SUCCESS = 'SUCCESS';

    const AXI_EXPORT_TYPE = 1;
    const DYNA_EXPORT_TYPE = 2;
    const AXI_TYPE = 'Axi';
    const DYNA_TYPE = 'Dyna';

    const DEFAULT_TIMEOUT = 10;

    public static $_types = array(
        1 => 'Axi',
        2 => 'Dyna',
    );

    /**
     * Constructor override
     */
    protected function _construct()
    {
       $this->_init("sandbox/export");
    }

    /**
     * @return string
     */
    public function getExportType()
    {
        if ( ! isset(self::$_types[(int) $this->getType()])) {
            throw new RuntimeException('Export type "%s" is invalid', $this->getType());
        }
        return self::$_types[(int) $this->getType()];
    }

    /**
     * @return string
     */
    public function getExportLocation()
    {
        return DS . trim($this->getReplica()->getEnvironmentPath(), DS);
    }

    /**
     * @return string
     */
    public function getExportFilename()
    {
        return trim($this->getData('export_filename'), DS);
    }

    /**
     * @return string
     */
    public function getExportPath()
    {
        return join(DS, array($this->getExportLocation(), $this->getExportFilename()));
    }

    /**
     * @return Omnius_Sandbox_Model_Replica
     */
    public function getReplica()
    {
        if ( ! $this->hasData('replica_instance')) {
            $replica = Mage::getModel('sandbox/replica')->load($this->getData('replica'));
            $this->setData('replica_instance', $replica);
        }
        return $this->getData('replica_instance');
    }

    /**
     * @return array
     */
    public function getSftpArgs()
    {
        $replica = $this->getReplica();
        if ($replica->getId()) {
            return array_filter(array(
                'host' => $replica->getData('ssh_host'),
                'username' => $replica->getData('ssh_user'),
                'password' => $replica->getData('ssh_pass'),
                'port' => $replica->getData('ssh_port'),
                'timeout' => ($replica->hasData('ssh_timeout') && 0 <= (int) $this->hasData('ssh_timeout'))
                        ? $replica->hasData('ssh_timeout')
                        : self::DEFAULT_TIMEOUT,
            ));
        }
        return array();
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        if ( ! ($messages = json_decode($this->getData('messages'), true))) {
            return array();
        }
        return array_reverse($messages);
    }

    /**
     * @param $message
     * @param string $type
     * @return $this
     */
    public function addMessage($message, $type = self::MESSAGE_INFO)
    {
        if ( ! $this->getData('decoded_messages')) {
            $this->setData('decoded_messages', json_decode($this->getData('messages'), true));
        }

        $messages = is_array($this->getData('decoded_messages')) ? $this->getData('decoded_messages') : array();
        $messages[] = sprintf('[%s][%s] %s', date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())), $type, $message);

        $this->setData('decoded_messages', $messages);

        return $this;
    }

    /**
     * @return $this
     */
    public function incrementTries()
    {
        $this->setData('tries', 1 + (int) $this->getData('tries'));
        return $this;
    }

    /**
     * Persists data in the database and sets created_at if it doesn't exist
     * @return Mage_Core_Model_Abstract
     * @throws Exception
     */
    public function save()
    {
        $this->setData('messages', $this->hasData('decoded_messages') ? json_encode($this->getData('decoded_messages')) : '[]');

        if ( ! $this->hasData('created_at')) {
            $this->setData('created_at', now());
        }

        return parent::save();
    }
}
