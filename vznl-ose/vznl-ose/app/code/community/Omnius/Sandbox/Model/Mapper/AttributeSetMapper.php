<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper_AttributeSetMapper
 */
class Omnius_Sandbox_Model_Mapper_AttributeSetMapper extends Omnius_Sandbox_Model_Mapper
{
    /** @var int */
    protected $_priority = 800;

    /** @var array */
    protected $_specialColumns = array();

    /** @var array */
    protected $_excludedColumns = array();

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    public function supports($table, $column)
    {
        return $table == 'eav_attribute_set'
            || ($table == 'eav_attribute_group' && $column == 'attribute_set_id');
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @return mixed
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        $value = trim($value, '\'"');

        if ($column === 'attribute_set_id') {
            $sql = "SELECT attribute_set_name FROM `eav_attribute_set` WHERE attribute_set_id:attribute_set_id";
            $attrCode = $masterAdapter->fetchOne($sql, array('attribute_set_id' => $value));

            if ( ! $attrCode && ($deletedAttributesSets = Mage::registry(Omnius_Sandbox_Model_Sandbox::DELETED_ATTRIBUTES_SET_REGISTRY))) {
                $attrCode = isset($deletedAttributesSets[$value]) ? $deletedAttributesSets[$value] : $attrCode;
            }

            $sql = "SELECT entity_type_id FROM `eav_attribute_set` WHERE attribute_set_id=:attribute_set_id";
            $entityTypeId = $masterAdapter->fetchOne($sql, array('attribute_set_id' => $value));

            $entityTypeId = $entityTypeId
                ? $entityTypeId
                : ( ($row && isset($row['entity_type_id']))
                    ? $row['entity_type_id']
                    : null );

            $sql = "SELECT attribute_set_id FROM `eav_attribute_set` WHERE attribute_set_name:attribute_set_name";
            $binds = array('attribute_set_name' => $attrCode);

            if($entityTypeId) {
                $sql .= " AND entity_type_id=:entity_type_id";
                $binds += array('entity_type_id' => $entityTypeId);
            }

            return $slaveAdapter->fetchOne($sql, $binds);
        }

        return $value;
    }
}
