<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Demux
 */
class Omnius_Sandbox_Model_Demux
{
    CONST CATALOG_PRODUCT_TABLE = 'catalog_product_entity';

    /** @var bool */
    private $_gathered = false;

    /** @var Magento_Db_Adapter_Pdo_Mysql[] */
    private $_adapters = array();

    /** @var Magento_Db_Adapter_Pdo_Mysql */
    private $_masterAdapter;

    /** @var Omnius_Sandbox_Model_QueryMapper */
    private $_queryMapper;

    /** @var Omnius_Sandbox_Model_FileMapper */
    private $_fileMapper;

    /** @var Omnius_Sandbox_Model_Tokenizer */
    private $_tokenizer;

    /** @var Omnius_Sandbox_Model_Config */
    private $_config;

    /** @var  Magento_Db_Adapter_Pdo_Mysql[] */
    private $_disabledAdapters = [];

    /**
     * @param array $args
     */
    public function __construct(array $args = array())
    {
        if (isset($args['master_adapter']) && ($args['master_adapter'] instanceof Varien_Db_Adapter_Interface)) {
            $this->_masterAdapter = $args['master_adapter'];
        }
    }

    /**
     * @param Varien_Db_Adapter_Pdo_Mysql $adapter
     */
    public function setMasterAdapter($adapter)
    {
        $this->_masterAdapter = $adapter;
    }

    /**
     * @param $query
     * @param $bind
     */
    public function propagate($query, $bind)
    {
        if ($this->_isBackend() && !Mage::registry('bypass_replication') && !$this->_isCli()) {
            /** @var Varien_Db_Adapter_Pdo_Mysql $slaveAdapter */
            foreach ($this->getAdapters() as $slaveAdapter) {
                $this->parseSlaveAdapter($query, $bind, $slaveAdapter);
            }
        }
    }

    /**
     * Updates the auto increment ids for the disabled replicas.
     * @param string $table
     */
    public function updateAutoIncrementByTable($table)
    {
        if ($table == self::CATALOG_PRODUCT_TABLE) {
            $this->updateAutoIncrement();
        }
    }

    /**
     * Updates auto-increment for the catalog_product_entity table.
     * If the $includeEnabled flag is set to true, to propagation will occur on the enabled replicas. This is used for the saveAndNotReplicate functionality.
     * @param bool $includeEnabled
     */
    public function updateAutoIncrement($includeEnabled = false)
    {
        $connectionKeys = $this->_getConfig()->getSandboxConnections();
        $replicas = $this->_getConfig()->getReplicas(true);
        $replicasKeys = array_keys($replicas);
        $updateAutoIncrement = [];

        foreach ($replicasKeys as $replica) {
            if (!in_array($replica, $connectionKeys)) {
                $updateAutoIncrement[] = $replica;
            }
        }

        // Fetch the auto-increment key from the master adapter.
        $masterAdapterConfig = $this->_masterAdapter->getConfig();
        $increment = (int)$this->_masterAdapter
            ->fetchOne("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'catalog_product_entity' AND table_schema = ':schema';",
                array(
                    ':schema' => $masterAdapterConfig['dbname']
                )
            );

        // Update auto-increment for the enabled adapters(in case of a saveAndNotReplicate call).
        if ($includeEnabled) {
            foreach($this->getAdapters() as $adapter) {
                $this->parseReplica($adapter, $increment);
            }
        }

        // Update auto-increment for the disabled adapters.
        foreach ($updateAutoIncrement as $ai) {
            try {
                if (isset($replicas[$ai]['can_sync_ai']) && $replicas[$ai]['can_sync_ai']) {
                    $this->parseReplica($this->_disabledAdapters[$ai], $increment);
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
            }
        }
    }

    /**
     * @return Magento_Db_Adapter_Pdo_Mysql[]
     */
    public function getDisabledAdapters()
    {
        if ( ! $this->_gathered) {
            $this->_gatherAdapters();
        }

        return $this->_disabledAdapters;
    }

    /**
     * @param $new
     * @param $old
     * @return array
     */
    public function bindDiff($new, $old)
    {
        $result = array();
        foreach ($new as $k => $v) {
            if (array_key_exists($k, $old)) {
                if (is_array($v)) {
                    $diff = $this->bindDiff($v, $old[$k]);
                    if (count($diff)) {
                        $result[$k] = $diff;
                    }
                } else {
                    if ($v != $old[$k]) {
                        $result[$k] = array(
                            'i' => $old[$k],
                            'm' => $v,
                        );
                    }
                }
            } else {
                $result[$k] = array(
                    'i' => $v,
                    'm' => '__MISSING__',
                );
            }
        }

        return $result;
    }

    /**
     * Closes connections
     */
    public function closeConnections()
    {
        /** @var Varien_Db_Adapter_Pdo_Mysql $adapter */
        foreach ($this->getAdapters() as $adapter) {
            if ($adapter->isConnected()) {
                $adapter->closeConnection();
            }
        }
        $this->_adapters = array();
        $this->_gathered = false;
    }

    /**
     * @param $sql
     * @param array $bind
     * @return mixed
     */
    public function bindQuery($sql, array $bind)
    {
        foreach ($this->_prepareBindedValues($bind) as $value) {
            $sql = preg_replace('/\?/', $value, $sql, 1);
        }

        return $sql;
    }

    /**
     * @param array $bind
     * @return array
     * @throws Exception
     */
    private function _prepareBindedValues(array $bind)
    {
        foreach ($bind as $index => &$value) {
            if (is_numeric($value)) {
                $value = (int) $value;
            } elseif (is_bool($value)) {
                $value = $value ? 'true' : 'false';
            } elseif (is_null($value)) {
                $value = 'NULL';
            } elseif (is_string($value)) {
                $value = sprintf('"%s"', str_replace('"', '\\"', $value));
            } else {
                throw new Exception(sprintf('Expected binded value to be a scalar but received %s (at index %s).', gettype($value), $index));
            }
        }

        return $bind;
    }

    /**
     * @return Omnius_Sandbox_Model_Config
     */
    private function _getConfig()
    {
        if (!$this->_config) {
            return $this->_config = Mage::getSingleton('sandbox/config');
        }

        return $this->_config;
    }

    /**
     * @param array $slaveConfig
     */
    private function _mapMedia(array $slaveConfig)
    {
        $to = isset($slaveConfig['path']) ? $slaveConfig['path'] : false;
        if ($to) {
            $from = DS . trim(Mage::getBaseDir('media'), DS) . DS . 'catalog';
            $to = DS . trim($to, DS) . DS . join(DS, array('media', 'catalog'));
            $this->_getFileMapper()->mapMedia($from, $to, isset($slaveConfig['remote']) ? $slaveConfig['remote'] : null);
        }
    }

    /**
     * @return Omnius_Sandbox_Model_QueryMapper
     */
    private function _getMapper()
    {
        if (!$this->_queryMapper) {
            $this->_queryMapper = Mage::getSingleton('sandbox/queryMapper');
        }

        return $this->_queryMapper;
    }

    /**
     * Allow actions only when triggered from backend
     *
     * @return bool
     */
    private function _isBackend()
    {
        $old = Mage::registry('bypass_replication');
        try {
            Mage::register('bypass_replication', true, true);
            $ret = (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml');
        } catch (Exception $e) {
            $ret = false;
        }
        Mage::register('bypass_replication', $old, true);
        return $ret;
    }

    /**
     * @return array
     */
    public function getAdapters()
    {
        if (!$this->_gathered) {
            $this->_gatherAdapters();
        }

        return $this->_adapters;
    }

    /**
     * @return Omnius_Sandbox_Model_FileMapper
     */
    private function _getFileMapper()
    {
        if (!$this->_fileMapper) {
            return $this->_fileMapper = Mage::getSingleton('sandbox/fileMapper');
        }

        return $this->_fileMapper;
    }

    /**
     * @return Omnius_Sandbox_Model_Tokenizer
     */
    private function _getTokenizer()
    {
        if (!$this->_tokenizer) {
            $this->_tokenizer = Mage::getSingleton('sandbox/tokenizer');
        }

        return $this->_tokenizer;
    }

    /**
     * Retrieves a list of available adapters
     */
    private function _gatherAdapters()
    {
        $defaultConf = array(
            'initStatements' => 'SET NAMES utf8',
            'type' => 'pdo_mysql',
            'charset' => 'utf8',
        );

        $replicas = $this->_getConfig()->getReplicas(true);
        $sandboxConnections = $this->_getConfig()->getSandboxConnections();
        foreach ($replicas as $replica) {
            if (!in_array($replica['name'], $sandboxConnections)) {
                try {
                    $mysqlConf = array(
                        'host' => isset($replica['mysql_host']) ? $replica['mysql_host'] : null,
                        'username' => isset($replica['mysql_user']) ? $replica['mysql_user'] : null,
                        'password' => isset($replica['mysql_password']) ? $replica['mysql_password'] : null,
                        'dbname' => isset($replica['mysql_database']) ? $replica['mysql_database'] : null,
                    );
                    $mysqlConf = array_merge($defaultConf, $mysqlConf);
                    $this->_disabledAdapters[$replica['name']] = new Varien_Db_Adapter_Pdo_Mysql($mysqlConf);
                } catch (Exception $e) {
                    Mage::getSingleton('core/logger')->logException($e);
                }
            }
        }

        foreach ($sandboxConnections as $config) {
            if (!isset($replicas[$config])) {
                Mage::log(sprintf('Replica "%s" not found in replicas list', $config));
                continue;
            }
            try {
                $mysqlConf = array(
                    'host' => isset($replicas[$config]['mysql_host']) ? $replicas[$config]['mysql_host'] : null,
                    'username' => isset($replicas[$config]['mysql_user']) ? $replicas[$config]['mysql_user'] : null,
                    'password' => isset($replicas[$config]['mysql_password']) ? $replicas[$config]['mysql_password'] : null,
                    'dbname' => isset($replicas[$config]['mysql_database']) ? $replicas[$config]['mysql_database'] : null,
                );
                $mysqlConf = array_merge($defaultConf, $mysqlConf);
                $this->_adapters[$config] = new Varien_Db_Adapter_Pdo_Mysql($mysqlConf);
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
            }
        }
        $this->_gathered = true;
    }

    /**
     * @param $query
     * @param Varien_Db_Adapter_Pdo_Mysql $adapter
     */
    private function _logExecutedQueries($query, Varien_Db_Adapter_Pdo_Mysql $adapter)
    {
        if (Mage::getStoreConfigFlag('dev/debug/is_dev')) {
            $config = $adapter->getConfig();
            $host = str_replace(array('://'), '.', $config['host']);
            unset($config);
            $filename = sprintf('replication_queries_%s_%s.log', $host, date('Ymd'));
            Mage::log($query, null, $filename, true);
        }
    }

    /**
     * Destructor method override
     */
    public function __destruct()
    {
        $this->closeConnections();
    }

    /**
     * @param Varien_Db_Adapter_Pdo_Mysql $adapter
     * @param $increment
     */
    protected function parseReplica($adapter, $increment)
    {
        $adapterConfig = $adapter->getConfig();
        $oldValue = (int)$adapter
            ->fetchOne("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'catalog_product_entity' and table_schema = ':schema';",
                array(
                    ':schema' => $adapterConfig['dbname']
                )
            );

        if ($oldValue < $increment) {
            $adapter->query(sprintf("ALTER TABLE `catalog_product_entity` AUTO_INCREMENT=%s;", $increment));
        }
    }

    /**
     * @param $query
     * @param $bind
     * @param $slaveAdapter
     */
    protected function parseSlaveAdapter($query, $bind, $slaveAdapter)
    {
        try {
            if ($mappedQuery = $this->_getMapper()->map($query, $bind, $this->_masterAdapter, $slaveAdapter)) {
                if (!$slaveAdapter->isConnected()) {
                    $slaveAdapter->getConnection();
                }

                $slaveAdapter->beginTransaction();

                // Fix for product SKU change
                if ($changedSkus = Mage::registry('changed_skus_mapping')) {
                    foreach ($changedSkus as $initSku => $newSku) {
                        $slaveAdapter->query(sprintf('UPDATE `catalog_product_entity` SET sku="%s" WHERE sku="%s"', $newSku, $initSku));
                    }
                    Mage::unregister('changed_skus_mapping');
                }

                $slaveAdapter->query($mappedQuery['sql'], $mappedQuery['bind']);
                $slaveAdapter->commit();
                $this->_logExecutedQueries($this->bindQuery($mappedQuery['sql'], $mappedQuery['bind']), $slaveAdapter);

                if (isset($mappedQuery['media_sync']) && $mappedQuery['media_sync']) {
                    $this->_mapMedia($slaveAdapter->getConfig());
                }
            }
        } catch (Exception $e) {
            $slaveAdapter->rollback();

            $mappedBind = $this->_getTokenizer()->getBoundConditions($mappedQuery['sql'], $mappedQuery['bind']);
            $initialBind = $this->_getTokenizer()->getBoundConditions($query, $bind);

            $message = sprintf(
                'Exception occurred while trying to replicate the following query:' . PHP_EOL
                . 'Message: %s' . str_repeat(PHP_EOL, 2)
                . 'Mapped query: %s' . str_repeat(PHP_EOL, 2)
                . 'Initial query: %s' . str_repeat(PHP_EOL, 2)
                . 'Bind difference: %s' . str_repeat(PHP_EOL, 2)
                . 'Mapped bind: %s' . str_repeat(PHP_EOL, 2)
                . 'Initial bind: %s' . str_repeat(PHP_EOL, 2)
                . 'Trace: %s' . str_repeat(PHP_EOL, 2)
                . str_repeat(PHP_EOL, 4),
                $e->getMessage(),
                $this->bindQuery($mappedQuery['sql'], $mappedQuery['bind']),
                $this->bindQuery($query, $bind),
                Mage::helper('core')->jsonEncode($this->bindDiff($mappedBind, $initialBind)),
                Mage::helper('core')->jsonEncode($mappedBind),
                Mage::helper('core')->jsonEncode($initialBind),
                $e->getTraceAsString()
            );
            Mage::log($message, Zend_Log::CRIT, 'replication.log', true);
        }
    }

    /**
     * Checks whether the script is requested from the command line.
     * @return bool
     */
    private function _isCli()
    {
        return php_sapi_name() == 'cli';
    }
}
