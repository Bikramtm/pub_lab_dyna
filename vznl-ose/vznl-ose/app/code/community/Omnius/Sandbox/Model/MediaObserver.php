<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_MediaObserver
 */
class Omnius_Sandbox_Model_MediaObserver
{
    /** @var null|array */
    protected $_replicas;

    /** @var null|array */
    protected $_connections;

    /**
     * @param Varien_Object $observer
     * @return bool
     * @throws Exception
     */
    public function publishMediaFile(Varien_Object $observer)
    {
        if (Mage::registry('bypass_replication')) {
            // Disable replication
            return false;
        }

        /** @var Dyna_Catalog_Model_Product $product */
        $product = $observer->getProduct();
        /** @var Omnius_Sandbox_Helper_Remote $remote */
        $remote = Mage::helper('sandbox/remote');
        /** @var Varien_Db_Adapter_Pdo_Mysql $masterAdapter */
        $masterAdapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        /** @var Omnius_Sandbox_Model_Demux $demux */
        $demux = Mage::getSingleton('sandbox/demux');
        $slaveAdapters = $demux->getAdapters();
        $images = $observer->getImages();
        $images = $images['images'];
        /** @var Omnius_Sandbox_Model_Replica $replica */
        foreach ($this->_getReplicas() as $replica) {
            foreach ($images as $imageInfo) {
                $this->parseImage($slaveAdapters, $replica, $remote, $product, $masterAdapter, $imageInfo);
            }
        }
    }

    /**
     * @param Omnius_Sandbox_Model_Replica $replica
     * @param array $image
     * @throws Exception
     */
    protected function _addImage(Omnius_Sandbox_Model_Replica $replica, array $image)
    {
        $file = Mage::getBaseDir('media') . DS . 'catalog/product' . $image['file'];
        $mediaPath = DS . implode(DS, array(trim($replica->getEnvironmentPath(), DS), 'media', 'catalog', 'product'));
        $io = $this->_getConnection($replica->getName());
        $io->setAllowCreateFolders(true);
        if (realpath($file)) {
            if ($replica->isRemote()) {
                $this->addImageOnRemote($replica, $image, $io, $mediaPath, $file);
            } else {
                $this->addImageOnLocal($replica, $image, $mediaPath, $io, $file);
            }
        } else {
            throw new Exception(sprintf('Media file replication failed due to missing file (%s)', $file));
        }
    }

    /**
     * @param Omnius_Sandbox_Model_Replica $replica
     * @param array $image
     * @throws Exception
     */
    protected function _removeImage(Omnius_Sandbox_Model_Replica $replica, array $image)
    {
        $io = $this->_getConnection($replica->getName());
        $mediaPath = DS . implode(DS, array(trim($replica->getEnvironmentPath(), DS), 'media', 'catalog', 'product'));
        $imagePath = $mediaPath . DS . trim($image['file'], DS);

        $this->_preserveMediaItem($image);

        if ($replica->isRemote()) {
            $io->open($replica->getSshConfig());
            if ($io->cd(dirname($imagePath))) {
                $count = count(array_filter($io->ls(), function ($item) use ($image) {
                    return basename($image['file']) === $item['text'];
                }));
                if ($count && !$io->rm($imagePath)) {
                    throw new Exception(sprintf('Could not delete remote file (%s)', $imagePath));
                }
            }
        } else {
            $io->open($mediaPath);
            $throw = false;
            try {
                $io->cd(dirname($imagePath));
                $count = count(array_filter($io->ls(), function ($item) use ($image) {
                    return basename($image['file']) === $item['text'];
                }));
                if ($count && !$io->rm($imagePath)) {
                    $throw = true;
                }
            } catch (Exception $e) {
                //no-op
            }
            if ($throw) {
                throw new Exception(sprintf('Could not delete file (%s)', $imagePath));
            }
        }
    }

    /**
     * @param $item
     */
    protected function _preserveMediaItem($item)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $key = 'sandbox_media_deletion';
        $items = Mage::registry($key) ? Mage::registry($key) : array();

        $items[$item['value_id']] = $connection
            ->fetchRow('SELECT * FROM `catalog_product_entity_media_gallery` WHERE value_id=:id;',
                array(
                    ':id' => $item['value_id']
                )
            );

        Mage::unregister($key);
        Mage::register($key, $items);
    }

    /**
     * @return array
     */
    protected function _getReplicas()
    {
        if (null === $this->_replicas) {
            $this->_replicas = array();
            if ($supportedConnections = Mage::getSingleton('sandbox/config')->getSandboxConnections()) {
                $collection = Mage::getResourceModel('sandbox/replica_collection');
                foreach ($collection->getItems() as $replica) {
                    /** @var Omnius_Sandbox_Model_Replica $replica */
                    if (in_array($replica->getName(), $supportedConnections)) {
                        $this->_replicas[] = $replica;
                    }
                }
            }

            return $this->_replicas;
        }

        return $this->_replicas;
    }

    /**
     * @return array|null
     */
    protected function _getConnections()
    {
        if (null === $this->_connections) {
            foreach ($this->_getReplicas() as $replica) {
                /** @var Omnius_Sandbox_Model_Replica $replica */
                if ($replica->isRemote()) {
                    $this->_connections[$replica->getName()] = new Omnius_Sandbox_Model_Sftp();
                } else {
                    $this->_connections[$replica->getName()] = new Varien_Io_File();
                }
            }
        }

        return $this->_connections;
    }

    /**
     * @param $replicaName
     * @return Varien_Io_Abstract|null
     */
    protected function _getConnection($replicaName)
    {
        foreach ($this->_getConnections() as $replicaID => $connection) {
            if ($replicaName == $replicaID) {
                return $connection;
            }
        }

        return null;
    }

    /**
     * cleanup
     */
    public function __destruct()
    {
        if (is_array($this->_connections)) {
            foreach ($this->_connections as $connection) {
                /** @var Omnius_Sandbox_Model_Sftp $connection */
                try {
                    if ($connection instanceof Varien_Io_Sftp && $connection->getConnection()) {
                        $connection->getConnection()->disconnect();
                    }
                } catch (Exception $e) {
                    //no-op
                }
            }
        }
    }

    /**
     * Triest to add new images on a remote replica
     * @param Omnius_Sandbox_Model_Replica $replica
     * @param $image
     * @param $io
     * @param $mediaPath
     * @param $file
     * @throws Exception
     */
    protected function addImageOnRemote(Omnius_Sandbox_Model_Replica $replica, $image, $io, $mediaPath, $file)
    {
        $io->open($replica->getSshConfig());
        if ($io->cd($mediaPath)) {
            $productCatalog = $mediaPath . dirname($image['file']);
            if (!$io->cd($productCatalog)) {
                $parts = explode(DS, trim(dirname($image['file']), DS));
                $path = $mediaPath;
                foreach ($parts as $part) {
                    $path .= DS . $part;
                    if (!$io->cd($path) && !$io->mkdir($path, 0777, false)) {
                        throw new Exception(sprintf('Could not create folder on replica (%s)', $path));
                    }
                }
            }

            //by now we should be in the right dir
            if ($io->cd($productCatalog) && !$io->write(basename($file), $file, NET_SFTP_LOCAL_FILE)) {
                throw new Exception(sprintf('Media path on replica not present (%s)', $mediaPath));
            }
        } else {
            throw new Exception(sprintf('Media path on replica not present (%s)', $mediaPath));
        }
    }

    /**
     * Tries to add an image on the local server in the replica environment
     * @param Omnius_Sandbox_Model_Replica $replica
     * @param $image
     * @param $mediaPath
     * @param $io
     * @param $file
     * @throws Exception
     */
    protected function addImageOnLocal(Omnius_Sandbox_Model_Replica $replica, $image, $mediaPath, $io, $file)
    {
        $productCatalog = $mediaPath . dirname($image['file']);
        $io->open($replica->getEnvironmentPath());
        if ($io->cd($mediaPath)) {
            try {
                $io->cd($productCatalog);
            } catch (Exception $e) {
                $parts = explode(DS, trim(dirname($image['file']), DS));
                $path = $mediaPath;
                foreach ($parts as $part) {
                    $path .= DS . $part;
                    $this->walkInto($io, $path);
                }
            }

            //by now we should be in the right dir
            if ($io->cd($productCatalog) && !$io->cp($file, ($productCatalog . DS . basename($image['file'])))) {
                throw new Exception(sprintf('Media path on replica not present (%s)', $mediaPath));
            }
        } else {
            throw new Exception(sprintf('Media path on replica not present (%s)', $mediaPath));
        }
    }

    /**
     * Checks to determine if an image is locked or not
     * @param $slaveAdapters
     * @param $replica
     * @param $remote
     * @param $product
     * @param $masterAdapter
     * @param $imageInfo
     * @throws Exception
     */
    protected function parseImage($slaveAdapters, $replica, $remote, $product, $masterAdapter, $imageInfo)
    {
        if (($slaveAdapter = isset($slaveAdapters[$replica->getName()]) ? $slaveAdapters[$replica->getName()] : null)) {
            $productIsLocked = $remote->isSlaveProductLocked($product->getId(), 'entity_id', $masterAdapter, $slaveAdapter);
            if (!$productIsLocked) {
                if (isset($imageInfo['new_file'])) {
                    $this->_addImage($replica, $imageInfo);
                } elseif (isset($imageInfo['removed']) && $imageInfo['removed']) {
                    $this->_removeImage($replica, $imageInfo);
                }
            }
        } else {
            Mage::getSingleton('core/logger')->logException(new Exception(sprintf('Product media replication: Replicat %s has no adapter present.', $replica->getName())));
            //no-op
        }
    }

    /**
     * @param $io
     * @param $path
     * @throws Exception
     */
    protected function walkInto($io, $path)
    {
        try {
            $io->cd($path);
        } catch (Exception $e) {
            if (!$io->mkdir($path, 0777, false)) {
                throw new Exception(sprintf('Could not create folder on replica (%s)', $path));
            }
        }
    }
}
