<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Replica
 *
 * @method string getName()
 * @method string getEnvironmentPath()
 * @method bool hasMysqlHost()
 * @method string getMysqlHost()
 * @method bool hasMysqlUser()
 * @method string getMysqlUser()
 * @method bool hasMysqlPassword()
 * @method string getMysqlPassword()
 * @method bool hasMysqlDatabase()
 * @method string getMysqlDatabase()
 * @method bool hasMysqlPort()
 * @method string getMysqlPort()
 * @method bool hasSshHost()
 * @method string getSshHost()
 * @method bool hasSshPort()
 * @method string getSshPort()
 * @method bool hasSshUser()
 * @method string getSshUser()
 * @method bool hasSshPass()
 * @method string getSshPass()
 */
class Omnius_Sandbox_Model_Replica extends Mage_Core_Model_Abstract
{
    /** @var Omnius_Core_Helper_Data */
    private $_dynaCoreHelper = null;

    private $_sftpClient = null;

    /**
     * @return Omnius_Sandbox_Model_Sftp
     */
    protected function _getSftp()
    {
        if ( ! $this->_sftpClient) {
            return $this->_sftpClient = Mage::getModel('sandbox/sftp');
        }
        return $this->_sftpClient;
    }

    /** @var Omnius_Sandbox_Model_Config */
    protected $_config;

    /** @var string */
    protected $_replicaXml = '';
    const PHP_PATH = '/usr/local/zend/bin/php';

    /**
     * Constructor override
     */
    protected function _construct()
    {
        $this->setIdFieldName('name');
        $this->_resourceName = 'sandbox/replica';
        $this->_replicaXml = Mage::getBaseDir('etc').DS.'replicas.xml';
        $this->_config = Mage::getSingleton('sandbox/config');
    }


    /**
     * @return array
     */
    public function checkDifferences()
    {
        $slaveResult = $this->fetchProducts();
        //Check only products that have already been added to the slave sandbox.
        $maxId = max(array_keys($slaveResult));
        $masterAdapter = Mage::getModel('core/resource')->getConnection('write');
        if (!$maxId) {
            // If there are no products available on the slave sandbox, it cannot have any differences
            return [];
        }
        $masterResult = $masterAdapter
            ->fetchPairs("SELECT `entity_id`, `sku` FROM `catalog_product_entity` WHERE `entity_id` <= :id;",
                array(
                    ':id' =>$maxId
                )
            );

        //Keep reference to the old values so we can render them.
        $baseMasterResult = $masterResult;
        $baseSlaveResult = $slaveResult;
        $sandboxHelper = Mage::helper('sandbox');
        //Trim SKUs of ids in case the product was deleted.
        $slaveResult    = array_map(array($sandboxHelper, 'trimDeletedSku'), $slaveResult);
        $masterResult   = array_map(array($sandboxHelper, 'trimDeletedSku'), $masterResult);

        //Compare ids and SKUs
        $result = array_diff_assoc($slaveResult, $masterResult);
        $toReturn = [];

        foreach ($result as $key => $value) {
            $toReturn[$key] = [
                'entity_id' => $key,
                'slave_sku' => $baseSlaveResult[$key],
                'master_sku' => isset($baseMasterResult[$key]) ? $baseMasterResult[$key] : null
            ];
        }

        return $toReturn;
    }

    /**
     * Fetches the product id-sku associations from the replica. .
     * @return mixed
     * @throws Exception
     */
    public function fetchProducts()
    {
        //Fetch product id-sku associations from the replica via SFTP as some replicas don't give external access.
        $this->_getSftp()->open($this->getSshConfig());
        $remoteBase = rtrim($this->getEnvironmentPath(), DS) . DS;
        $differencesFile = 'products_replica_' . $this->getId() . '_' . time() . '.json';
        $toExec = sprintf('%s %sshell/get_products_for_compare.php --f %s', $this->getPhpPath(), $remoteBase, $differencesFile);
        $this->_getSftp()->getConnection()->exec($toExec);
        $jsonProducts = $this->_getSftp()->read('/tmp/differences/'. $differencesFile);
        $this->_getSftp()->close();
        return json_decode($jsonProducts, true);
    }

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('omnius_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * @return bool
     */
    public function isRemote()
    {
        return count($this->getSshConfig()) >= 3;
    }

    /**
     * @return array
     */
    public function getVarnishServers()
    {
        return array_filter(array_map('trim', explode(',', $this->getData('varnish_servers'))), function($server) {
            $r = '/^((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*)|((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}))$/';
            return preg_match($r, $server);
        });
    }

    /**
     * @return array
     */
    public function getMysqlConfig()
    {
        return array(
            'host' => $this->getMysqlHost(),
            'username' => $this->getMysqlUser(),
            'password' => $this->getMysqlPassword(),
            'dbname' => $this->getMysqlDatabase(),
            'port' => $this->hasMysqlPort() ? (int) $this->getMysqlPort() : 3306,
        );
    }

    /**
     * @return array
     */
    public function getSshConfig()
    {
        return array_filter(array(
            'host' => $this->getData('ssh_host'),
            'username' => $this->getData('ssh_user'),
            'password' => $this->getData('ssh_pass'),
            'port' => $this->getData('ssh_port'),
        ));
    }

    /**
     * Persists data in the database
     * @return $this|Omnius_Sandbox_Model_Replica|Mage_Core_Model_Abstract
     */
    public function save()
    {
        /**
         * Direct deleted items to delete method
         */
        if ($this->isDeleted()) {
            return $this->delete();
        }
        if (!$this->_hasModelChanged()) {
            return $this;
        }

        $this->_beforeSave();

        if ($name = $this->_config->getConfig()->getXpath(sprintf("//replica/name[text() = '%s']", $this->getId()))) {
            /** @var Varien_Simplexml_Element $replicaNode */
            $replicaNode = $name[0]->xpath('..');
            $replicaXml = new Varien_Simplexml_Element('<replica></replica>');
            $data = array_replace_recursive($replicaNode[0]->asArray(), $this->toArray());
            $this->arrayToXml($data, $replicaXml);
            $this->replaceNode($replicaNode[0], $replicaXml);
        } else { //no existing reference tot his replica in the xml
            $replicasNode = $this->_config->getConfig()->getXpath('replicas');
            /** @var Varien_Simplexml_Element $replicasNode */
            $replicasNode = $replicasNode[0];
            $replicaXml = new Varien_Simplexml_Element('<replica></replica>');
            $this->arrayToXml($this->toArray(), $replicaXml);
            $replicasNode->appendChild($replicaXml);
        }

        $this->_hasDataChanges = false;
        $this->_afterSave();

        return $this;
    }

    /**
     * @return $this
     */
    public function delete()
    {
        $this->_beforeDelete();
        if ($name = $this->_config->getConfig()->getXpath(sprintf("//replica/name[text() = '%s']", $this->getId()))) {
            $replicaNode = $name[0]->xpath('..');
            unset($replicaNode[0][0]);
        }
        $this->_afterDelete();
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function flush()
    {
        $this->_config->saveReplicas();
        return $this;
    }

    /**
     * @param $array
     * @param $xml
     */
    protected function arrayToXml($array, $xml)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml->addChild($key);
                    $this->arrayToXml($value, $subnode);
                } else {
                    $this->arrayToXml($value, $xml);
                }
            } else {
                $xml->addChild($key, $value);
            }
        }
    }

    /**
     * @param SimpleXMLElement $parent
     * @param SimpleXMLElement $element
     * @return SimpleXMLElement
     */
    protected function replaceNode(SimpleXMLElement $parent, SimpleXMLElement $element)
    {
        $dom = dom_import_simplexml($parent);
        $import = $dom->ownerDocument->importNode(dom_import_simplexml($element), true);
        $dom->parentNode->replaceChild($import, $dom);
        return $parent;
    }

    /**
     * @return string
     *
     * Get the php path of the replica if remote
     */
    public function getPhpPath()
    {
        $path = $this->isRemote() ? $this->getData('php_path') : Mage::helper('sandbox')->getBinary();
        return $path ?: self::PHP_PATH;
    }
}
