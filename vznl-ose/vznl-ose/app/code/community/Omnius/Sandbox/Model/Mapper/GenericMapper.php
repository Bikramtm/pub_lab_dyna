<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper_GenericMapper
 */
class Omnius_Sandbox_Model_Mapper_GenericMapper extends Omnius_Sandbox_Model_Mapper
{
    /** @var int */
    protected $_priority = 0;

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    public function supports($table, $column)
    {
        return true;
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @return mixed
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        if ($this->_isMappable($column)) {
            return $value;
        }
        return $value;
    }
}
