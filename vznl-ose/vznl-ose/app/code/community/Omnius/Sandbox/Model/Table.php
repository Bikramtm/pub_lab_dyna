<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Table
 */
class Omnius_Sandbox_Model_Table
{
    /** @var array */
    protected $_cache = array();

    /**
     * @param string $table
     * @param Varien_Db_Adapter_Pdo_Mysql $adapter
     * @return mixed
     */
    public function getTableDefinition($table, $adapter)
    {
        $cacheKey = $this->_cacheKey($table, $adapter);
        if ( ! isset($this->_cache[$cacheKey])) {
            return $this->_cache[$cacheKey] = $adapter->describeTable($this->getTable($table));
        }
        return $this->_cache[$cacheKey];
    }

    /**
     * @param string $table
     * @param string $column
     * @param Varien_Db_Adapter_Pdo_Mysql $adapter
     * @return bool
     */
    public function columnExists($table, $column, $adapter)
    {
        return $adapter->tableColumnExists($table, $column, null);
    }

    /**
     * @param string $table
     * @param Varien_Db_Adapter_Pdo_Mysql $adapter
     * @return mixed
     */
    public function tableExists($table, $adapter)
    {
        return $adapter->isTableExists($table);
    }

    /**
     * @param string $entityName
     * @return string
     */
    protected function getTable($entityName)
    {
        if (is_array($entityName) || strpos($entityName, '/') !== false) {
            return Mage::getResourceSingleton('core/store')->getTable($entityName);
        } else {
            return $entityName;
        }
    }

    /**
     * @param string $table
     * @param Varien_Db_Adapter_Pdo_Mysql $adapter
     * @return string
     */
    protected function _cacheKey($table, $adapter)
    {
        return md5(serialize(array($adapter->getConfig(), $table)));
    }
} 
