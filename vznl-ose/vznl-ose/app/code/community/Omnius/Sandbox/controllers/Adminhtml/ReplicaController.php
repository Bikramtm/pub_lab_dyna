<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Adminhtml_ReplicaController
 */
class Omnius_Sandbox_Adminhtml_ReplicaController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Initialize a certain action
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('sandbox/replica')->_addBreadcrumb(Mage::helper('adminhtml')->__('Replica  Manager'), Mage::helper('adminhtml')->__('Replica Manager'));

        return $this;
    }

    /**
     * Render the layout on default index action
     */
    public function indexAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Manager Replica'));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Trigger edit mode for a record
     */
    public function editAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Replica'));
        $this->_title($this->__('Edit Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sandbox/replica')->load($id);
        if ($model->getId()) {
            Mage::register('replica_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('sandbox/replica');
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Replica Manager'), Mage::helper('adminhtml')->__('Replica Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Replica Description'), Mage::helper('adminhtml')->__('Replica Description'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('sandbox/adminhtml_replica_edit'))->_addLeft($this->getLayout()->createBlock('sandbox/adminhtml_replica_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sandbox')->__('Item does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Initialize the addition of a new record
     */
    public function newAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Replica'));
        $this->_title($this->__('New Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sandbox/replica')->load($id);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('replica_data', $model);

        $this->loadLayout();
        $this->_setActiveMenu('sandbox/replica');

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Replica Manager'), Mage::helper('adminhtml')->__('Replica Manager'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Replica Description'), Mage::helper('adminhtml')->__('Replica Description'));


        $this->_addContent($this->getLayout()->createBlock('sandbox/adminhtml_replica_edit'))->_addLeft($this->getLayout()->createBlock('sandbox/adminhtml_replica_edit_tabs'));

        $this->renderLayout();

    }

    /**
     * Persist the data in the database
     */
    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();
        if ($post_data) {
            if (isset($post_data['mysql_password']) && false !== strpos($post_data['mysql_password'], '*')) {
                unset($post_data['mysql_password']);
            }
            if (isset($post_data['ssh_pass']) && false !== strpos($post_data['ssh_pass'], '*')) {
                unset($post_data['ssh_pass']);
            }
            try {
                $model = Mage::getModel('sandbox/replica')
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam('name'))
                    ->save()
                    ->flush();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Replica was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setReplicaData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));

                    return;
                }
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setReplicaData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Deletes a certain provided record
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('sandbox/replica');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete()
                    ->flush();

                //Deleting it from sandbox.xml
                $wasIn = 0;
                $sandId = $this->getRequest()->getParam('id');
                $sandbox = Mage::getSingleton('sandbox/config');
                $sandboxList = $sandbox->getSandboxConnections();
                if (($key = array_search($sandId, $sandboxList)) !== false) {
                    $wasIn = 1;
                    unset($sandboxList[$key]);
                }
                if ($wasIn) {
                    $this->parseSandbox($sandboxList);

                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Trigger the mass remove to delete multiple selected records
     */
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            $wasIn = 0;
            $sandbox = Mage::getSingleton('sandbox/config');
            $sandboxList = $sandbox->getSandboxConnections();

            foreach ($ids as $id) {
                /** @var Omnius_Sandbox_Model_Replica $model */
                $model = Mage::getModel('sandbox/replica');
                $model->setId($id)
                    ->delete();
                if (($key = array_search($id, $sandboxList)) !== false) {
                    $wasIn = 1;
                    unset($sandboxList[$key]);
                }
                //Remove from sandbox
            }
            if ($wasIn) {
                //Was in sandbox.xml, lets rebuild sandbox replicas without the deleted one
                $this->parseSandbox($sandboxList);
            }
            if (isset($model)) {
                $model->flush();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item(s) was successfully removed'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /*
     * Enables selected replicas
     */
    public function enableSelectedAction()
    {
        $replicasList = [];
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $replicasList[] = $id;
            }

            /** @var Omnius_Sandbox_Model_Config $sandbox */
            $sandbox = Mage::getSingleton('sandbox/config');
            $sandboxList = $sandbox->getSandboxConnections();
            //Merging might cause duplicate values in array
            $replica = array_merge($replicasList, $sandboxList);
            //So we uniquify it
            $replica = array_unique($replica);
            //Reading sandbox.xml
            $xmlSandFile = file_get_contents(Mage::getBaseDir('etc') . DS . 'sandbox.xml');
            $xmlSandFile = str_replace("<?xml version=\"1.0\"?>", "", $xmlSandFile);
            $xmlLoader = simplexml_load_string($xmlSandFile);
            //Dump replica node, wi'll rebuild it later
            unset($xmlLoader->sync_envs->replica);
            foreach ($replica as $rep) {
                $xmlLoader->sync_envs->addChild('replica', $rep);
            }
            //Using DOMDocument for a nice xml display with indentation
            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->loadXML($xmlLoader->asXML());

            if (false == @file_put_contents(Mage::getBaseDir('etc') . DS . 'sandbox.xml', $dom->saveXML())) {
                throw new Exception('Could not save sandbox data. Please check file permissions.');
            }

            Mage::getSingleton('sandbox/config')->clearCache();

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item(s) was successfully enabled'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /*
     * Disable selected replicas
     */
    public function disableSelectedAction()
    {

        $replicasList = [];
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $replicasList[] = $id;
            }
            /** @var Omnius_Sandbox_Model_Config $sandbox */
            $sandbox = Mage::getSingleton('sandbox/config');
            $sandboxList = $sandbox->getSandboxConnections();
            foreach ($replicasList as $replica) {
                if (($key = array_search($replica, $sandboxList)) !== false) {
                    unset($sandboxList[$key]);
                }
            }
            //Reading sandbox.xml
            $this->parseSandbox($sandboxList);

            Mage::getSingleton('sandbox/config')->clearCache();

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item(s) was successfully disabled'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'replica.csv';
        $grid = $this->getLayout()->createBlock('sandbox/adminhtml_replica_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'replica.xml';
        $grid = $this->getLayout()->createBlock('sandbox/adminhtml_replica_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * @param $sandboxList
     * @throws Exception
     */
    protected function parseSandbox($sandboxList)
    {
        //Was in sandbox.xml, lets rebuild sandbox replicas without the deleted one
        $xmlSandFile = file_get_contents(Mage::getBaseDir('etc') . DS . 'sandbox.xml');
        $xmlSandFile = str_replace("<?xml version=\"1.0\"?>", "", $xmlSandFile);
        $xmlLoader = simplexml_load_string($xmlSandFile);
        //Dump replica node from sandbox list, wi'll rebuild it later
        unset($xmlLoader->sync_envs->replica);
        foreach ($sandboxList as $replica) {
            $xmlLoader->sync_envs->addChild('replica', $replica);
        }
        //Using DOMDocument for a nice xml display with indentation
        $dom = new DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xmlLoader->asXML());
        if (false == @file_put_contents(Mage::getBaseDir('etc') . DS . 'sandbox.xml', $dom->saveXML())) {
            throw new Exception('Could not save sandbox data. Please check file permissions.');
        }
    }
}
