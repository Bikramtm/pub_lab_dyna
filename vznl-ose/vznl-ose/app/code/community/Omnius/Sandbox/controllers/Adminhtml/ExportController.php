<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Adminhtml_ExportController
 */
class Omnius_Sandbox_Adminhtml_ExportController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Initialize method to add breadcrumb in the export
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('sandbox/export')->_addBreadcrumb(Mage::helper('adminhtml')->__('Export  Manager'), Mage::helper('adminhtml')->__('Export Manager'));
        return $this;
    }

    /**
     * Render layout on default index action
     */
    public function indexAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Manager Export'));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Initialize edit mode for the export
     */
    public function editAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Export'));
        $this->_title($this->__('Edit Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sandbox/export')->load($id);
        if ($model->getId()) {
            $model->setDeadline(Mage::getModel('core/date')->date('Y-m-d H:i:s', $model->getDeadline()));
            Mage::register('export_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('sandbox/export');
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Export Manager'), Mage::helper('adminhtml')->__('Export Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Export Description'), Mage::helper('adminhtml')->__('Export Description'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('sandbox/adminhtml_export_edit'))->_addLeft($this->getLayout()->createBlock('sandbox/adminhtml_export_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sandbox')->__('Item does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Initialize the addition of a new record
     */
    public function newAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Export'));
        $this->_title($this->__('New Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sandbox/export')->load($id);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('export_data', $model);

        $this->loadLayout();
        $this->_setActiveMenu('sandbox/export');

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Export Manager'), Mage::helper('adminhtml')->__('Export Manager'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Export Description'), Mage::helper('adminhtml')->__('Export Description'));


        $this->_addContent($this->getLayout()->createBlock('sandbox/adminhtml_export_edit'))->_addLeft($this->getLayout()->createBlock('sandbox/adminhtml_export_edit_tabs'));

        $this->renderLayout();

    }

    /**
     * Persist the data in the database
     */
    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();
        if ($post_data) {
            try {
                if (isset($post_data['deadline'])) {
                    $a = new DateTime(Mage::getSingleton('core/date')->gmtDate());
                    $b = new DateTime(Mage::getSingleton('core/date')->date());
                    $diff = $a->diff($b);
                    $deadlineStr = $post_data['deadline'];
                    $deadline = new DateTime($deadlineStr);
                    if ($diff->invert) {
                        $deadline->add($diff);
                    } else {
                        $deadline->sub($diff);
                    }
                    $post_data['deadline'] = $deadline->format('Y-m-d H:i:s');
                }

                $model = Mage::getModel('sandbox/export')
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam('id'))
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Export was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setExportData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setExportData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

        }
        $this->_redirect('*/*/');
    }

    /**
     * Removes a specific record
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('sandbox/export');
                $model->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Trigger mass remove on multiple selected records
     */
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel('sandbox/export');
                $model->setId($id)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item(s) was successfully removed'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Trigger mass export action
     */
    public function massExportAction()
    {
        try {
            $ids = array_filter(array_map('intval', $this->getRequest()->getPost('ids', array())));
            /** @var Omnius_Sandbox_Model_Mysql4_Export_Collection $collection */
            $collection = Mage::getResourceModel('sandbox/export_collection')
                ->addFieldToFilter('id', array('in' => $ids))
                ->addFieldToFilter('tries', array('lt' => Mage::helper('sandbox')->getTriesLimit()))
                ->load();
            /** @var Omnius_Sandbox_Model_Exporter $exporter */
            $exporter = Mage::getSingleton('sandbox/exporter');
            foreach ($collection->getItems() as $export) { /** @var Omnius_Sandbox_Model_Export $export */
                switch($export->getStatus()) {
                    case Omnius_Sandbox_Model_Export::STATUS_SUCCESS:
                    case Omnius_Sandbox_Model_Export::STATUS_RUNNING:
                        // Do nothing
                        break;
                    case Omnius_Sandbox_Model_Export::STATUS_PENDING:
                    case Omnius_Sandbox_Model_Export::STATUS_ERROR:
                            $exporter->export($export)->save();
                        break;
                    default:
                        // Do nothing
                        break;
                }
            }

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Exports were successfully processed'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'export.csv';
        $grid = $this->getLayout()->createBlock('sandbox/adminhtml_export_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'export.xml';
        $grid = $this->getLayout()->createBlock('sandbox/adminhtml_export_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
