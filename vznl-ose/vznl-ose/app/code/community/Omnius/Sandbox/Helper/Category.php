<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Helper_Category
 */
class Omnius_Sandbox_Helper_Category
{
    /** @var Omnius_Sandbox_Model_Tokenizer */
    protected $_tokenizer;

    /** @var Omnius_Sandbox_Model_IdentifierMapper */
    protected $_identifierMapper;

    /**
     * @param string $query
     * @param Varien_Db_Adapter_Pdo_Mysql $masterAdapter
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @return string
     */
    public function remapCategoryPath($query, $slaveAdapter)
    {
        if (preg_match('/(?<=catalog_category_entity)(.*)REPLACE(\s?)\(([`\'"]?)path([`\'"]?)/smi', $query)) {

            $parts = $this->_getTokenizer()->parse($query);

            preg_match('/REPLACE([^\)].*\))/smi', $parts['update'], $pathParts);

            $pathParts = array_values(array_filter(array_map(function($el) {
                $el = trim($el, '\'`" ');
                if (false !== strpos($el, '/')) {
                    return $el;
                }
                return false;
            }, explode(',', trim($pathParts[1], '()')))));
            $oldPath = $pathParts[0];

            $catIds = array_filter(explode('/', $oldPath));
            $lastChild = end($catIds);
            if ($updatedPath = $slaveAdapter
                ->fetchOne('SELECT `path` FROM `catalog_category_entity` WHERE entity_id=:id', array( ':id' => $lastChild))
            ) {
                return str_replace($oldPath, rtrim($updatedPath, '/') . '/', $query);
            }
        }
        return $query;
    }

    /**
     * @return Omnius_Sandbox_Model_Tokenizer
     */
    protected function _getTokenizer()
    {
        if ( ! $this->_tokenizer) {
            $this->_tokenizer = Mage::getSingleton('sandbox/tokenizer');
        }
        return $this->_tokenizer;
    }

    /**
     * @return Omnius_Sandbox_Model_IdentifierMapper
     */
    protected function _getIdentifierMapper()
    {
        if ( ! $this->_identifierMapper) {
            $this->_identifierMapper = Mage::getSingleton('sandbox/identifierMapper');
        }
        return $this->_identifierMapper;
    }
}
