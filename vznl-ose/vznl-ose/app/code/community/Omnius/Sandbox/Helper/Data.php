<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Helper_Data
 */
class Omnius_Sandbox_Helper_Data extends Mage_Core_Helper_Data
{
    CONST SANDBOX_CONFIG_PATH = 'sandbox_configuration/connection_configuration/%s';
    CONST SANDBOX_RELEASES_CONFIG_PATH = 'sandbox_configuration/releases_configuration/%s';

    const DEFAULT_TRIES_LIMIT = 3;

    /**
     * Determines if the current environment is a master one
     */
    public function isMaster()
    {
        //prevent throwing error if replica/sandbox xmls are not found
        try {
            return 0 !== count(Mage::getSingleton('sandbox/config')->getSandboxConnections());
        }
        catch (Exception $e){
            return false;
        }
    }

    /**
     * @param $config
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function getSandboxConfig($config)
    {
        if (is_string($config)) {
            return Mage::getStoreConfig(sprintf(self::SANDBOX_CONFIG_PATH, (string) $config));
        }
        throw new InvalidArgumentException(sprintf('Invalid config path given. String expected, %s type given', gettype($config)));
    }

    /**
     * @return int
     */
    public function getTriesLimit()
    {
        $limit = Mage::getStoreConfig(sprintf(self::SANDBOX_RELEASES_CONFIG_PATH, 'tries_limit'));
        if ( ! is_numeric($limit)) {
            return self::DEFAULT_TRIES_LIMIT;
        }
        return (int) $limit;
    }

    /**
     * Get sandbox environments database connections
     * Returns an array for each connection: array(hostname, databasename, username, password)
     *
     * @return array|bool
     */
    public function getSandboxConnections()
    {
        return $this->_prepareCsvOptions($this->getSandboxConfig('connections'));
    }

    /**
     * Unserialize and clear options
     *
     * @param string $options
     * @return array|bool
     */
    protected function _prepareCsvOptions($options)
    {
        $options = trim($options);
        if (empty($options)) {
            return array();
        }
        $result = array();
        $delimiter = ';';
        $conn_keys = array(
            'host', 'dbname', 'username', 'password', 'path',
        );
        $options = explode("\n", $options);
        foreach ($options as $key => $value) {
            $values = str_getcsv(trim($value),$delimiter);
            if (count($values) === count($conn_keys)) {
                $values = array_combine($conn_keys, $values);
                $result[$key] = $values;
            }
        }
        return $result;
    }

    /**
     * @param bool $onlyIds
     * @return mixed
     */
    public function getLockedProducts($onlyIds = false)
    {
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToFilter('product_locked', 1)
            ->load();

        return $onlyIds ? $products->getAllIds() : $products;
    }

    /**
     * Splits SKU's and retrieves only the sku without the id in case the product has been deleted.
     * @param $sku
     * @return mixed
     */
    public function trimDeletedSku($sku)
    {
        $a = explode('|', $sku);
        return count($a) > 1 ? $a[0] : $sku;
    }


    /**
     * Determine where the php executable is located
     * @return string
     */
    public function getBinary()
    {
        if (defined('PHP_BINARY') && strlen(constant('PHP_BINARY'))) {
            return constant('PHP_BINARY');
        } else {
            if (trim ($output = $this->execute('which php'))) {
                return $output;
            } else {
                throw new RuntimeException('Consumer script binary could not be detected');
            }
        }
    }
}
