<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Helper_RemoteDatabase
 */
class Omnius_Sandbox_Helper_RemoteDatabase
{
    /**
     * @param array $config
     * @param array $tables
     * @param array $targetMysqlConfig
     * @param array $targetSshConfig
     * @return string
     */
    public function createDumpCommand(array $config, array $tables, array $targetMysqlConfig, array $targetSshConfig)
    {
        return sprintf(
            "mysqldump -u%s -p%s -h%s -P%s %s %s | gzip | ssh -T -c arcfour -o Compression=yes -x -p %s %s@%s sh -c 'gunzip | sed -e \'1 i SET SESSION INTERACTIVE_TIMEOUT=7200;\' | mysql -u%s -p%s -h%s -P%s %s'", //12
            $config['username'],
            $config['password'],
            $config['host'],
            isset($config['port']) ? $config['port'] : 3306,
            $config['dbname'],
            join(' ', $tables),

            $targetSshConfig['port'],
            $targetSshConfig['username'],
            $targetSshConfig['host'],


            $targetMysqlConfig['username'],
            $targetMysqlConfig['password'],
            $targetMysqlConfig['host'],
            isset($targetConfig['port']) ? $targetConfig['port'] : 3306,
            $targetMysqlConfig['dbname']
        );
    }
} 
