<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Helper_Remote
 */
class Omnius_Sandbox_Helper_Remote
{
    /**
     * @param string|int $masterId
     * @param string $column
     * @param Varien_Db_Adapter_Pdo_Mysql $masterAdapter
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @return mixed
     */
    public function isSlaveProductLocked($masterId, $column, $masterAdapter, $slaveAdapter)
    {
        if ( ! $masterId) {
            return false;
        }
        $lockedAttribute = $this->getLockedAttribute($slaveAdapter);
        if ($slaveId = $this->getSlaveId($masterId, $column, $masterAdapter, $slaveAdapter)) {
            $sql = 'SELECT value FROM catalog_product_entity_:entity WHERE attribute_id=:attr AND entity_id=:id';
            $binds = array(
                ':entity' => $lockedAttribute['backend_type'],
                ':attr' => $lockedAttribute['attribute_id'],
                ':id' => $slaveId
            );

            return $slaveAdapter->fetchOne($sql, $binds);
        }
        return false;
    }

    /**
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @return array
     */
    public function getLockedAttribute($slaveAdapter)
    {
        return $slaveAdapter
            ->fetchRow('SELECT attribute_id, backend_type FROM eav_attribute WHERE attribute_code=:code',
                array(
                    ':code' => Omnius_Sandbox_Model_Sandbox::PRODUCT_LOCKED_ATTRIBUTE
                )
            );
    }

    /**
     * @param string|int $identifier
     * @param string $column
     * @param Varien_Db_Adapter_Pdo_Mysql $masterAdapter
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @return mixed
     */
    public function getSlaveId($identifier, $column, $masterAdapter, $slaveAdapter)
    {
        $config = $slaveAdapter->getConfig();
        $prefix = $config['host'].'_'.$config['dbname'];

        $identifier = trim($identifier, '\'""');

        if ( ! $identifier) {
            return false;
        }

        if ($column == 'sku') {
            return $identifier;
        }

        $sku = $masterAdapter
            ->fetchOne('SELECT sku FROM catalog_product_entity WHERE entity_id=:id',
                array(
                    ':id' =>$identifier
                )
            );

        if($changedSku = Mage::registry('changed_skus_mapping')){
            $sku = current(array_keys($changedSku));
        }

        if($newId = Mage::registry($prefix . '_' . $identifier . '_' . $sku)){
            $id = $newId ;
        }
        else{
            $id= $slaveAdapter->fetchOne('SELECT entity_id FROM catalog_product_entity WHERE sku = :sku;',
                array(
                    ':sku' => $sku
                )
            );

            if($id){
                Mage::unregister($prefix . '_' . $identifier . '_' . $sku);
                Mage::register($prefix . '_' . $identifier . '_' . $sku, $id);
            }
        }

        return $id;
    }

    /**
     * @param string $table
     * @param string $column
     * @param string|int $value
     * @param Varien_Db_Adapter_Pdo_Mysql $masterAdapter
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @return mixed
     * @throws Exception
     */
    public function mapSlaveUsingUniqueId($table, $column, $value, $masterAdapter, $slaveAdapter)
    {
        if ($column == Omnius_Sandbox_Model_Sandbox::UNIQUE_ID_FIELD || !$value) {
            return $value;
        }

        /**
         * There is a change that this is a "delete" flow and the referenced row has been deleted from master
         * so we keep a reference to it on registry and try to map it using that reference.
         */

        $uniqueId = $masterAdapter
            ->fetchOne('SELECT `unique_id` FROM ' . $table . ' WHERE :column=:value',
                array(
                    ':column' => $column,
                    ':value' =>$value
                )
            );

        if ( !$uniqueId ) { //the master row has been deleted until reaching here
            $referenceUniqueIds = Mage::registry(sprintf('%s_%s', Omnius_Sandbox_Model_Sandbox::UNIQUE_ID_REFERENCE, $table));
            if (is_array($referenceUniqueIds)) {
                $uniqueId = isset($referenceUniqueIds[$value]) ? $referenceUniqueIds[$value] : $uniqueId;
            }
        }

        return $slaveAdapter
            ->fetchOne('SELECT `:col` FROM ' . $table . ' WHERE unique_id = :id;',
                array(
                    ':col' => $column,
                    ':id' => $uniqueId));
    }

    /**
     * @param string $table
     * @param string $column
     * @param string|int $value
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @return bool
     */
    public function isItemLocked($table, $column, $value, $slaveAdapter)
    {
        return (bool) $slaveAdapter
            ->fetchOne('SELECT locked FROM ' . $table . ' WHERE :col=":value',
                array(
                    ':col' => $column,
                    ':value' => $value
                )
            );
    }

    /**
     * @param string|int $identifier
     * @param string $column
     * @param Varien_Db_Adapter_Pdo_Mysql $masterAdapter
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @param int|string|null $entityTypeId
     * @return mixed
     */
    public function getSlaveAttributeId($identifier, $column, $masterAdapter, $slaveAdapter, $entityTypeId = null)
    {
        $identifier = trim($identifier, '\'""');
        $attrCode = $masterAdapter
            ->fetchOne('SELECT attribute_code FROM eav_attribute WHERE attribute_id=:id',
                array(
                    ':id' => $identifier
                )
            );

        if ( ! $attrCode && in_array($column, array('attribute_id', 'entity_id')) && ($deletedAttributes = Mage::registry(Omnius_Sandbox_Model_Sandbox::DELETED_ATTRIBUTES_REGISTRY))) {
            $attrCode = isset($deletedAttributes[$identifier]) ? $deletedAttributes[$identifier] : $attrCode;
        }

        if($entityTypeId){
            return $slaveAdapter
                ->fetchOne('SELECT attribute_id FROM eav_attribute WHERE attribute_code=:code;',
                    array(
                        ':code' => $attrCode
                    )
                );
        } else {
            return $slaveAdapter
                ->fetchOne('SELECT attribute_id FROM eav_attribute WHERE attribute_code=:code AND entity_type_id=:id;',
                    array(
                        ':code' => $attrCode,
                        ':id' => $entityTypeId
                    )
                );
        }
    }

    /**
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @return array
     */
    public function getSlaveAttributes($slaveAdapter)
    {
        $return['attributes'] = [];
        $return['attributeSets'] = [];
        $resultSet = $slaveAdapter->fetchAll("select * from eav_attribute where entity_type_id='4' order by attribute_id asc");
        foreach ($resultSet as $result) {
            $return['attributes'][$result['attribute_code']] = $result['attribute_id'];
        }
        $resultSet = $slaveAdapter->fetchAll("select * from eav_attribute_set where entity_type_id='4' order by attribute_set_id asc");
        foreach ($resultSet as $result) {
            $return['attributeSets'][$result['attribute_set_name']] = $result['attribute_set_id'];
        }

        return $return;
    }
}
