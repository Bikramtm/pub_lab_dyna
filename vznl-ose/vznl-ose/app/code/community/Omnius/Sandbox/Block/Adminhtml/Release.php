<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Block_Adminhtml_Release
 */
class Omnius_Sandbox_Block_Adminhtml_Release extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Override the constructor to customize the grid
     * Omnius_Sandbox_Block_Adminhtml_Release constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_release';
        $this->_blockGroup = 'sandbox';
        $this->_headerText = Mage::helper('sandbox')->__('Release Manager');
        $this->_addButtonLabel = Mage::helper('sandbox')->__('Add New Item');

        parent::__construct();
    }
}
