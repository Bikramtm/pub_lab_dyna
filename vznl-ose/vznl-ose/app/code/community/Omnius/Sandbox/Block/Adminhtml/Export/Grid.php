<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Block_Adminhtml_Export_Grid
 */
class Omnius_Sandbox_Block_Adminhtml_Export_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Override construct to customize grid
     * Omnius_Sandbox_Block_Adminhtml_Export_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('exportGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Initialize collection
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sandbox/export')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Override prepareColumns to customize grid and add custom columns
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('sandbox')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'type' => 'number',
            'index' => 'id',
        ));

        $this->addColumn('destination', array(
            'header' => Mage::helper('sandbox')->__('Destination'),
            'index' => 'replica',
            'type' => 'text',
        ));

        $this->addColumn('export_location', array(
            'header' => Mage::helper('sandbox')->__('Export path'),
            'index' => 'export_location',
            'type' => 'text',
            'frame_callback' => array($this, 'getExportPath'),
        ));

        $this->addColumn('deadline', array(
            'header' => Mage::helper('sandbox')->__('Deadline'),
            'index' => 'deadline',
            'type' => 'datetime',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sandbox')->__('Created At'),
            'index' => 'created_at',
            'default' => 'N/A',
            'type' => 'datetime',
        ));

        $this->addColumn('finished_at', array(
            'header' => Mage::helper('sandbox')->__('Finished At'),
            'index' => 'finished_at',
            'default' => 'N/A',
            'type' => 'datetime',
        ));

        $this->addColumn('messages', array (
            'header' => Mage::helper('sandbox')->__('Messages'),
            'index' => 'messages',
            'frame_callback' => array($this, 'extractMessages'),
        ));

        $this->addColumn('tries', array (
            'header' => Mage::helper('sandbox')->__('Failed Tries'),
            'index' => 'tries',
        ));

        $this->addColumn('status', array (
            'header' => Mage::helper('sandbox')->__('Status'),
            'index' => 'status',
            'frame_callback' => array($this, 'decorateStatus'),
            'type' => 'options',
            'options' => array(
                Omnius_Sandbox_Model_Release::STATUS_PENDING => Omnius_Sandbox_Model_Release::STATUS_PENDING,
                Omnius_Sandbox_Model_Release::STATUS_SUCCESS => Omnius_Sandbox_Model_Release::STATUS_SUCCESS,
                Omnius_Sandbox_Model_Release::STATUS_ERROR => Omnius_Sandbox_Model_Release::STATUS_ERROR,
                Omnius_Sandbox_Model_Release::STATUS_RUNNING => Omnius_Sandbox_Model_Release::STATUS_RUNNING,
            )
        ));
        
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    /**
     * Rertrieve the edit url for a certain record
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * Checks if export is remote or not
     * @param $data
     * @param Omnius_Sandbox_Model_Export $export
     * @return string
     */
    public function isRemote($data, Omnius_Sandbox_Model_Export $export)
    {
        return ($export->getSshHost() && $export->getSshUser())
            ? 'Remote'
            : 'Local';
    }

    /**
     * Trim messages from a provided list
     * @param $messages
     * @param Omnius_Sandbox_Model_Export $export
     * @return string
     */
    public function extractMessages($messages, Omnius_Sandbox_Model_Export $export)
    {
        if ( ! count($export->getMessages())) {
            return Mage::helper('core')->__('N/A');
        }
        $content = "";
        $maxLines = 10; //max lines that will be shown
        foreach ($export->getMessages() as $message) {
            if ($maxLines <= 0) {
                $content .= '<span class="log-message">...</span>';
                break;
            }
            $message = $this->prepareMessage($message);
            if (strlen($message) > 350) {
                $message = substr($message, 0, 350) . '...';
            }
            $content .= sprintf('<span class="log-message">%s</span><br/>', $message);
            --$maxLines;
        }
        return $content;
    }

    /**
     * Return the export path
     * @param $_
     * @param Omnius_Sandbox_Model_Export $export
     * @return string
     */
    public function getExportPath($_, Omnius_Sandbox_Model_Export $export)
    {
        return $export->getReplica()->getEnvironmentPath();
    }

    /**
     * Format a provided messages to a custom format
     * @param $message
     * @return string
     */
    protected function prepareMessage($message)
    {
        preg_match_all('/(?<=\[)[^]]+(?=\])/', $message, $parts);
        if (isset($parts[0]) && count($parts[0]) === 2) {
            $date = $parts[0][0];
            $type = $parts[0][1];
            $tr = array(
                sprintf('[%s]', $date) => sprintf('<span class="date-part">[%s]</span>', $date),
                sprintf('[%s]', $type) => sprintf('<span class="type-part type-part-%s">[%s]</span>', strtolower($type), $type),
            );
            return strtr($message, $tr);
        }
        return $message;
    }

    /**
     * Show statuses with different format based on status
     * @param $status
     * @return string
     */
    public function decorateStatus($status) {
        switch ($status) {
            case Omnius_Sandbox_Model_Release::STATUS_SUCCESS:
                $result = '<span class="bar-green"><span>'.$status.'</span></span>';
                break;
            case Omnius_Sandbox_Model_Release::STATUS_PENDING:
                $result = '<span class="bar-lightgray"><span>'.$status.'</span></span>';
                break;
            case Omnius_Sandbox_Model_Release::STATUS_RUNNING:
                $result = '<span class="bar-yellow"><span>'.$status.'</span></span>';
                break;
            case Omnius_Sandbox_Model_Release::STATUS_ERROR:
                $result = '<span class="bar-red"><span>'.$status.'</span></span>';
                break;
            default:
                $result = $status;
                break;
        }
        return $result;
    }

    /**
     * Customize the mass remove/process actions
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_export', array(
            'label' => Mage::helper('sandbox')->__('Remove Export'),
            'url' => $this->getUrl('*/adminhtml_export/massRemove'),
            'confirm' => Mage::helper('sandbox')->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('process_export', array(
            'label' => Mage::helper('sandbox')->__('Process Export'),
            'url' => $this->getUrl('*/adminhtml_export/massExport'),
            'confirm' => Mage::helper('sandbox')->__('Are you sure?')
        ));
        return $this;
    }
}
