<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Block_Adminhtml_Replica
 */
class Omnius_Sandbox_Block_Adminhtml_Replica extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Override the constructor to customzie the grid
     * Omnius_Sandbox_Block_Adminhtml_Replica constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_replica';
        $this->_blockGroup = 'sandbox';
        $this->_headerText = Mage::helper('sandbox')->__('Replica Manager');
        $this->_addButtonLabel = Mage::helper('sandbox')->__('Add New Item');

        parent::__construct();
    }
}
