<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Block_Adminhtml_Release_Grid
 */
class Omnius_Sandbox_Block_Adminhtml_Release_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Override the constructor to customize the grid
     * Omnius_Sandbox_Block_Adminhtml_Release_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('releaseGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Initialize the collection
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sandbox/release')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function addAllColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('sandbox')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'type' => 'number',
            'index' => 'id',
        ));

        $this->addColumn('replica_id', array(
            'header' => Mage::helper('sandbox')->__('Replica'),
            'index' => 'replica',
        ));

        $this->addColumn('version', array(
            'header' => Mage::helper('sandbox')->__('Version'),
            'index' => 'version',
        ));

        $this->addColumn('one_time_date', array(
            'header' => Mage::helper('sandbox')->__('Execution Date'),
            'index' => 'one_time_date',
            'frame_callback' => array($this, 'decorateExecutionDate'),
            'type' => 'datetime',
        ));

        $this->addColumn('deadline', array(
            'header' => Mage::helper('sandbox')->__('Deadline'),
            'index' => 'deadline',
            'type' => 'datetime',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sandbox')->__('Created At'),
            'index' => 'created_at',
            'default' => 'N/A',
            'type' => 'datetime',
        ));

        $this->addColumn('finished_at', array(
            'header' => Mage::helper('sandbox')->__('Finished At'),
            'index' => 'finished_at',
            'default' => 'N/A',
            'type' => 'datetime',
        ));

        $this->addColumn('messages', array (
            'header' => Mage::helper('sandbox')->__('Messages'),
            'index' => 'messages',
            'frame_callback' => array($this, 'extractMessages'),
        ));

        $this->addColumn('tries', array (
            'header' => Mage::helper('sandbox')->__('Failed Tries'),
            'index' => 'tries',
        ));

        $this->addColumn('status', array (
            'header' => Mage::helper('sandbox')->__('Status'),
            'index' => 'status',
            'frame_callback' => array($this, 'decorateStatus'),
            'type' => 'options',
            'options' => array(
                Omnius_Sandbox_Model_Release::STATUS_PENDING => Omnius_Sandbox_Model_Release::STATUS_PENDING,
                Omnius_Sandbox_Model_Release::STATUS_SUCCESS => Omnius_Sandbox_Model_Release::STATUS_SUCCESS,
                Omnius_Sandbox_Model_Release::STATUS_ERROR => Omnius_Sandbox_Model_Release::STATUS_ERROR,
                Omnius_Sandbox_Model_Release::STATUS_RUNNING => Omnius_Sandbox_Model_Release::STATUS_RUNNING,
            )
        ));
    }

    /**
     * Override the prepareColumns method to add custom methods
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addAllColumns();

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    /**
     * Get edit url for a record
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * Trim a provided message
     * @param $messages
     * @param Omnius_Sandbox_Model_Release $release
     * @return string
     */
    public function extractMessages($messages, Omnius_Sandbox_Model_Release $release)
    {
        if ( ! count($release->getMessages())) {
            return Mage::helper('core')->__('N/A');
        }
        $content = "";
        $maxLines = 50; //max lines that will be shown
        foreach ($release->getMessages() as $message) {
            if ($maxLines <= 0) {
                $content .= '<span class="log-message">...</span>';
                break;
            }
            $message = $this->prepareMessage($message);
            if (strlen($message) > 350) {
                $message = substr($message, 0, 350) . '...';
            }
            $content .= sprintf('<span class="log-message">%s</span><br/>', $message);
            --$maxLines;
        }
        return $content;
    }

    /**
     * Formats a provided message to show it in a custom way
     * @param $message
     * @return string
     */
    protected function prepareMessage($message)
    {
        preg_match_all('/(?<=\[)[^]]+(?=\])/', $message, $parts);
        if (isset($parts[0]) && count($parts[0]) === 2) {
            $date = $parts[0][0];
            $type = $parts[0][1];
            $tr = array(
                sprintf('[%s]', $date) => sprintf('<span class="date-part">[%s]</span>', $date),
                sprintf('[%s]', $type) => sprintf('<span class="type-part type-part-%s">[%s]</span>', strtolower($type), $type),
            );
            return strtr($message, $tr);
        }
        return $message;
    }

    /**
     * Display statuses differently based on value
     * @param $status
     * @return string
     */
    public function decorateStatus($status) {
        switch ($status) {
            case Omnius_Sandbox_Model_Release::STATUS_SUCCESS:
                $result = '<span class="bar-green"><span>'.$status.'</span></span>';
                break;
            case Omnius_Sandbox_Model_Release::STATUS_PENDING:
                $result = '<span class="bar-lightgray"><span>'.$status.'</span></span>';
                break;
            case Omnius_Sandbox_Model_Release::STATUS_RUNNING:
                $result = '<span class="bar-yellow"><span>'.$status.'</span></span>';
                break;
            case Omnius_Sandbox_Model_Release::STATUS_ERROR:
                $result = '<span class="bar-red"><span>'.$status.'</span></span>';
                break;
            default:
                $result = $status;
                break;
        }
        return $result;
    }

    /**
     * Customize mass remove/process actions
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_release', array(
            'label' => Mage::helper('sandbox')->__('Remove Release'),
            'url' => $this->getUrl('*/adminhtml_release/massRemove'),
            'confirm' => Mage::helper('sandbox')->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('process_release', array(
            'label' => Mage::helper('sandbox')->__('Process Release'),
            'url' => $this->getUrl('*/adminhtml_release/massProcess'),
            'confirm' => Mage::helper('sandbox')->__('Are you sure?')
        ));
        return $this;
    }

    /**
     * Display N/A when empty execution date is stored
     * @param $executionDate
     * @return string
     */
    public function decorateExecutionDate($executionDate) {
        if ($executionDate == '0000-00-00 00:00:00' || $executionDate == '30 nov. -1 00:00:00') {
            $result = 'N/A';
        } else {
            $result =  $executionDate;
        }

        return $result;
    }
}
