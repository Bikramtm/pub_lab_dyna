<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Block_Adminhtml_Export_Edit
 */
class Omnius_Sandbox_Block_Adminhtml_Export_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Override constructor to customize grid
     * Omnius_Sandbox_Block_Adminhtml_Export_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'sandbox';
        $this->_controller = 'adminhtml_export';
        $this->_updateButton('save', 'label', Mage::helper('sandbox')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('sandbox')->__('Delete Item'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('sandbox')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
    }

    /**
     * Adds custom header text for the new/edit form
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('export_data') && Mage::registry('export_data')->getId()) {
            return Mage::helper('sandbox')->__('Edit Item "%s"', $this->htmlEscape(Mage::registry('export_data')->getName()));
        } else {
            return Mage::helper('sandbox')->__('Add Item');
        }
    }
}
