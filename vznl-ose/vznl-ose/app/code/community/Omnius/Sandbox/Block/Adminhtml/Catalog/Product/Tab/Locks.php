<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Sandbox_Block_Adminhtml_Catalog_Product_Tab_Locks extends Mage_Adminhtml_Block_Template implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('adminhtml/products/locks.phtml');
    }

    /**
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Product Locks');
    }

    /**
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Product Locks');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        $modules = Mage::getConfig()->getNode('modules');
        if ($modules
            && $modules->Omnius_Sandbox
            && $modules->Omnius_Sandbox->is('active', 'true')
        ) { //check if module is enabled
            return Mage::helper('sandbox')->isMaster();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getTabClass()
    {
        return 'ajax';
    }

    /**
     * @return bool
     */
    public function getSkipGenerateContent()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getTabUrl()
    {
        return $this->getUrl('sandbox/adminhtml_catalog_product_locks', array('product_id' => Mage::registry('product')->getId()));
    }

    /**
     * @return mixed
     */
    public function getReplicas()
    {
        return Mage::getSingleton('sandbox/demux')->getAdapters();
    }

    /**
     * @param $productId
     * @param $slaveAdapter
     * @return mixed
     */
    public function isLocked($productId, $slaveAdapter)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        return (bool) Mage::helper('sandbox/remote')->isSlaveProductLocked($productId, 'entity_id', $connection, $slaveAdapter);
    }
}
