<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Block_Adminhtml_Replica_Edit_Tab_Form
 */
class Omnius_Sandbox_Block_Adminhtml_Replica_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        foreach ($this->getFields() as $fieldSetName => $setFields) {
            $fieldSet = $form->addFieldset(sprintf('sandbox_%s_form', strtolower($fieldSetName)), array('legend' => Mage::helper('sandbox')->__($fieldSetName)));
            foreach ($setFields as $name => $fieldDefinition) {
                $fieldSet->addField($name, $fieldDefinition['type'], $fieldDefinition['config']);
            }
        }

        if (Mage::getSingleton('adminhtml/session')->getReplicaData()) {
            $data = Mage::getSingleton('adminhtml/session')->getReplicaData();
            Mage::getSingleton('adminhtml/session')->setReplicaData(null);
        } elseif (Mage::registry('replica_data')) {
            $data = Mage::registry('replica_data')->getData();
        }
        if (!isset($data['php_path'])) {
            $data['php_path'] = Omnius_Sandbox_Model_Replica::PHP_PATH;
        }
        $form->setValues($data);

        return parent::_prepareForm();
    }

    /**
     * @return array
     */
    protected function getFields()
    {
        return array(
            'General' => array(
                'name' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Name'),
                        'name' => 'name',
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'environment_path' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Environment Path'),
                        'name' => 'environment_path',
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'varnish_servers' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Varnish Servers'),
                        'name' => 'varnish_servers',
                        'required' => false,
                        'after_element_html' => Mage::helper('sandbox')->__(
                            '<br/><span>IPs or Hostnames separated by comma (,)</span>'
                        )
                    ),
                ),
            ),
            'Database' => array(
                'mysql_host' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Mysql Host'),
                        'name' => 'mysql_host',
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'mysql_database' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Mysql Database'),
                        'name' => 'mysql_database',
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'mysql_user' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Mysql User'),
                        'name' => 'mysql_user',
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'mysql_password' => array(
                    'type' => 'obscure',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Mysql Password'),
                        'name' => 'mysql_password',
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'can_sync_ai' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Can sync auto-increment?'),
                        'name' => 'can_sync_ai',
                        'options' => array( 0 => Mage::helper('sandbox')->__('No'), 1 => Mage::helper('sandbox')->__('Yes'))
                    ),
                ),
            ),
            'SSH' => array(
                'note' => array(
                    'type' => 'note',
                    'config' => array(
                        'text' => Mage::helper('sandbox')->__('Fill the below fields only if the replica is on a remote system'),
                    ),
                ),
                'ssh_host' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('SSH Host'),
                        'name' => 'ssh_host',
                    ),
                ),
                'ssh_port' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('SSH Port'),
                        'name' => 'ssh_port',
                    ),
                ),
                'ssh_user' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('SSH User'),
                        'name' => 'ssh_user',
                    ),
                ),
                'php_path' => array (
                    'type' => 'text',
                    'config' => array (
                        'label' => Mage::helper('sandbox')->__('PHP binary path'),
                        'name' => 'php_path',
                    ),
                ),
                'ssh_pass' => array(
                    'type' => 'obscure',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('SSH Password'),
                        'name' => 'ssh_pass',
                    ),
                ),
            ),
        );
    }
}
