<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$releasesTableName = 'sandbox_release';

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

if (!$connection->isTableExists($releasesTableName)) {
// Create the releases table
    $releasesTable = new Varien_Db_Ddl_Table();
    $releasesTable->setName($releasesTableName);
        $releasesTable->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'auto_increment' => true,
                'nullable' => false,
                'comment' => 'Release ID',
            ))
        ->addColumn('replica', Varien_Db_Ddl_Table::TYPE_TEXT, 45,
            array(
                'primary' => true,
                'nullable' => false,
            ))
        ->addColumn('version', Varien_Db_Ddl_Table::TYPE_TEXT, 50,
            array(
            ))
        ->addColumn('tables', Varien_Db_Ddl_Table::TYPE_TEXT, null,
            array(
                'nullable' => false,
                'comment' => 'List of Handled Tables',
            ))
        ->addColumn('export_media', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null,
            array(
                'nullable' => true,
                'default' => true,
                'comment' => 'Handle Media Flag',
            ))
        ->addColumn('deadline', Varien_Db_Ddl_Table::TYPE_DATETIME, null,
            array(
                'nullable' => false,
                'comment' => 'Execution Deadline',
            ))
        ->addColumn('tries', Varien_Db_Ddl_Table::TYPE_TINYINT, null,
            array(
                'nullable' => false,
                'default' => 0,
                'comment' => 'Number of Failed Processes',
            ))
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
            array(
                'nullable' => false,
                'default'  => Varien_Db_Ddl_Table::TIMESTAMP_INIT,
                'comment' => 'Created at',
            ))
        ->addColumn('executed_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null,
            array(
                'nullable' => true,
                'comment' => 'Executed At',
            ))
        ->addColumn('finished_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null,
            array(
                'nullable' => true,
                'comment' => 'Finished At',
            ))
        ->addColumn('messages', Varien_Db_Ddl_Table::TYPE_BLOB, null,
            array(
                'nullable' => true,
                'comment' => 'Release messages',
            ))
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TEXT, 15,
            array(
                'nullable' => false,
                'default' => Omnius_Sandbox_Model_Release::STATUS_PENDING,
                'comment' => 'Release Status',
            ))
        ->addColumn('is_heavy', Varien_Db_Ddl_Table::TYPE_INTEGER, 1,
            array(
                'nullable' => false,
                'default' => 0,
            ))
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'comment' => 'Website',
                'required' => false,
                'nullable' => true,
                'unsigned' => true,
            ))
    ;
    $connection->createTable($releasesTable);
    /** @var Omnius_Sandbox_Model_Release $result */
}

$installer->endSetup();
