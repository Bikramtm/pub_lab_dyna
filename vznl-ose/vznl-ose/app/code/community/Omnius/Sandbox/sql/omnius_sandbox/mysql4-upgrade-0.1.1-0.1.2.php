<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$exportTableName = 'sandbox_export';

/** @var Mage_Sales_Model_Mysql4_Setup $this */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

if (!$connection->isTableExists($exportTableName)) {
// Create the catalog export table
    $catalogExportTable = new Varien_Db_Ddl_Table();
    $catalogExportTable->setName($exportTableName);
    $catalogExportTable->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'auto_increment' => true,
                'nullable' => false,
                'comment' => 'Release ID',
            ))
        ->addColumn('type', Varien_Db_Ddl_Table::TYPE_TINYINT, 1,
            array(
                'nullable' => false,
                'comment' => 'Export Type',
            ))
        ->addColumn('deadline', Varien_Db_Ddl_Table::TYPE_DATETIME, null,
            array(
                'nullable' => false,
                'comment' => 'Execution Deadline',
            ))
        ->addColumn('tries', Varien_Db_Ddl_Table::TYPE_TINYINT, null,
            array(
                'nullable' => false,
                'default' => 0,
                'comment' => 'Number of Failed Processes',
            ))
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
            array(
                'nullable' => false,
                'default'  => Varien_Db_Ddl_Table::TIMESTAMP_INIT,
                'comment' => 'Created at',
            ))
        ->addColumn('executed_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null,
            array(
                'nullable' => true,
                'comment' => 'Executed At',
            ))
        ->addColumn('finished_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null,
            array(
                'nullable' => true,
                'comment' => 'Finished At',
            ))
        ->addColumn('messages', Varien_Db_Ddl_Table::TYPE_BLOB, null,
            array(
                'nullable' => true,
                'comment' => 'Release messages',
            ))
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TEXT, 15,
            array(
                'nullable' => false,
                'default' => Omnius_Sandbox_Model_Release::STATUS_PENDING,
                'comment' => 'Release Status',
            ))
        ->addColumn('export_filename', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
            array(
                'nullable' => false,
            ))
        ->addColumn('sftp_host', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
            array(
                'nullable' => false,
            ))
        ->addColumn('sftp_user', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
            array(
                'nullable' => false,
            ))
        ->addColumn('sftp_pass', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
            array(
                'nullable' => false,
            ))
        ->addColumn('sftp_timeout', Varien_Db_Ddl_Table::TYPE_TINYINT, 2,
            array(
                'nullable' => true,
            ))
    ;
    $connection->createTable($catalogExportTable);
}

$installer->endSetup();