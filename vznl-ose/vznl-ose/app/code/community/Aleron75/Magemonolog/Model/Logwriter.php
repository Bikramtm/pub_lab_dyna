<?php

/** AUTOLOADER PATCH **/
if (file_exists($autoloaderPath = __DIR__ . DS . '../lib/autoload.php')
) {
    require_once $autoloaderPath;
}
/** AUTOLOADER PATCH **/

use Monolog\Logger;

class Aleron75_Magemonolog_Model_Logwriter
    extends Zend_Log_Writer_Abstract
{
    /**
     * @param string $logFile
     * @param string $handlerModel
     * @return bool
     */
    protected function isApproved($logFile, $handlerModel)
    {
        $substr = Mage::getStoreConfig('magemonolog/handlers/' . $handlerModel . '/deny');
        if(!empty($substr)) {
            foreach ($substr as $format) {
                if (trim($format) && strpos($logFile, trim($format)) !== false) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @var string
     */
    protected $_logFile = null;

    /**
     * @var Monolog\Logger
     */
    protected $_logger = null;

    /**
     * Array used to map Zend's log levels into Monolog's
     *
     * @var array
     */
    protected $_levelMap = array();

    public function __construct($logFile)
    {
        $this->_logFile = $logFile;

        // Force autoloading
        Mage::dispatchEvent('add_spl_autoloader');

        // Initialize level mapping only after the Logger class is loaded
        $this->__initLevelMapping();

        $this->_logger = Mage::getModel('aleron75_magemonolog/logger');

        $handlers = Mage::getStoreConfig('magemonolog/handlers');

        if (!is_null($handlers) && is_array($handlers))
        {
            foreach ($handlers as $handlerModel => $handlerValues)
            {
                $isActive = Mage::getStoreConfigFlag('magemonolog/handlers/'.$handlerModel.'/active');
                if (!$isActive || !$this->isApproved($logFile, $handlerModel))
                {
                    continue;
                }

                $args = array();
                if (array_key_exists('params', $handlerValues))
                {
                    $args = $handlerValues['params'];
                }
                if (empty($args['stream']))
                {
                    $args['stream'] = empty($this->_logFile) ? $args['defaultStream'] : $this->_logFile;
                }

                $handlerWrapper = Mage::getModel('aleron75_magemonolog/handlerWrapper_'.$handlerModel, $args);

                if (array_key_exists('formatter', $handlerValues)
                    && array_key_exists('class', $handlerValues['formatter']))
                {
                    $class = new ReflectionClass('\\Monolog\Formatter\\'.$handlerValues['formatter']['class']);
                    $formatter = $class->newInstanceArgs($handlerValues['formatter']['args']);
                    $handlerWrapper->setFormatter($formatter);
                }

                $this->_logger->pushHandler($handlerWrapper->getHandler());
            }
        }
    }

    /**
     * Get the logger
     * @return Aleron75_Magemonolog_Model_Logger
     */
    public function getLogger()
    {
        return $this->_logger;
    }

    /**
     * Initialize the array used to map Zend's log levels into Monolog's
     */
    private function __initLevelMapping()
    {
        $this->_levelMap = array(
            Zend_Log::EMERG     => Logger::EMERGENCY,
            Zend_Log::ALERT     => Logger::ALERT,
            Zend_Log::CRIT      => Logger::CRITICAL,
            Zend_Log::ERR       => Logger::ERROR,
            Zend_Log::WARN      => Logger::WARNING,
            Zend_Log::NOTICE    => Logger::NOTICE,
            Zend_Log::INFO      => Logger::INFO,
            Zend_Log::DEBUG     => Logger::DEBUG,
        );
    }

    /**
     * Write a message using Monolog.
     *
     * @param  array $event  event data
     * @return void
     */
    protected function _write($event)
    {
        $level = $this->_levelMap[$event['priority']];
        $message = $event['message'];
        $this->_logger->addRecord($level, $message);
    }

    /**
     * Create a new instance of Aleron75_Magemonolog_Model_Logwriter
     *
     * @param  array|Zend_Config $config
     * @return Aleron75_Magemonolog_Model_Logwriter
     * @throws Zend_Log_Exception
     */
    static public function factory($config)
    {
        return new self(self::_parseConfig($config));
    }

    public function setFormatter(Zend_Log_Formatter_Interface $formatter)
    {
        try {
            $this->_formatter = $formatter;
            $formatterReflection = new ReflectionClass($this->_formatter);
            $formatProperty = $formatterReflection->getProperty('_format');
            $formatProperty->setAccessible(true);
            $format = $formatProperty->getValue($formatter);
            foreach($this->_logger->getHandlers() as $handler)
            {
                $formatter = $handler->getFormatter();
                $formatterReflection = new ReflectionClass($formatter);
                $formatProperty = $formatterReflection->getProperty('format');
                $formatProperty->setAccessible(true);
                $formatProperty->setValue($formatter, $this->getMonologFormat($format));
            }
        }
        catch(\Exception $e) { }
        return $this;
    }

    /**
     * Replace Zend logging format with compatible Monolog placeholders
     *
     * @param $zendFormat
     *
     * @return string
     */
    private function getMonologFormat($zendFormat)
    {
        return str_replace(
            array('%timestamp%', '%priorityName%', '%priority%'),
            array('%datetime%', '%level_name%', '%level%'),
            $zendFormat
        );
    }
}
