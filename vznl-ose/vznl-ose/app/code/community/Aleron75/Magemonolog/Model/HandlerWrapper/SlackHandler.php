<?php
use Monolog\Logger;
use Monolog\Handler\SlackHandler;

class Aleron75_Magemonolog_Model_HandlerWrapper_SlackHandler
  extends Aleron75_Magemonolog_Model_HandlerWrapper_AbstractHandler
{
    public function __construct(array $args)
    {
        $this->_validateArgs($args);
        try
        {
            $this->_handler = new SlackHandler(
              $args['token'],
              $args['channel'],
              $args['username'],
              $args['useAttachment'],
              $args['iconEmoji'],
              $args['level'],
              $args['bubble'],
              $args['useShortAttachment']
            );
        }
        catch (\Exception $e) {}
    }

    protected function _validateArgs(array &$args)
    {
        parent::_validateArgs($args);

        $requiredArgs = array('token', 'channel', 'username');
        foreach ($requiredArgs as $arg)
        {
            if (!isset($args[$arg]))
            {
                throw new \Exception('Required argument '. $arg .' not found');
            }
        }
        $defaults = array(
            'useAttachment' => true,
            'iconEmoji' => null,
            'level' => Logger::CRITICAL,
            'bubble' => true,
            'useShortAttachment' => false,
            'includeContextAndExtra' => false,
        );
        $args = array_merge($defaults, $args);
    }


}
