<?php

/**
 * Class Aleron75_Magemonolog_Model_HandlerWrapper_VodafoneDEStreamHandler
 */
class Aleron75_Magemonolog_Model_HandlerWrapper_VodafoneDEStreamHandler
    extends Aleron75_Magemonolog_Model_HandlerWrapper_StreamHandler
{
    /**
     * @param array $args
     */
    protected function _validateArgs(array &$args)
    {
        parent::_validateArgs($args);

        // Stream
        $file = Mage::getStoreConfig('dev/log/file');
        if (isset($args['stream']))
        {
            $file = $args['stream'];
        }
        $logDir  = Mage::getBaseDir('var') . DS . 'log';
        $file = preg_replace('/^(' . preg_quote($logDir, '/') . '(\/)?)+/', '', $file);

        if (!empty($args['basePath'])) {
            $basePath = preg_replace('/^(' . preg_quote($logDir, '/') . '(\/)?)+/', '', $args['basePath']);
            $file = $basePath . DS . $file;
        }

        $args['stream'] =  $logDir . DS . $file;

        // File Permission
        $filePermission = null;
        if (isset($args['filePermission']) && is_numeric($args['filePermission']))
        {
            $filePermission = filter_var($args['filePermission'], FILTER_VALIDATE_INT);
        }
        $args['filePermission'] = $filePermission;

        // Use Locking
        $useLocking = false;
        if (isset($args['useLocking']))
        {
            $useLocking = filter_var($args['useLocking'], FILTER_VALIDATE_BOOLEAN);
        }
        $args['useLocking'] = $useLocking;
    }
}
