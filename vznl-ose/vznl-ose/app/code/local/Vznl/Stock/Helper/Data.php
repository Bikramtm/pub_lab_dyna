<?php


class Vznl_Stock_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Return stock collection for product
     * @param $product
     * @param null $axiStoreCode
     * @return mixed|false
     */
    public function getProductStockCollection($product, $axiStoreCode = null)
    {
        $prodStockCollection = false;

        if (!empty($product)) {
            $maxStoreDistance = Mage::getStoreConfig('vodafone_service/stock_settings/store_max_distance');
            if ($axiStoreCode) {
                $prodStockCollection = Mage::getModel('stock/stock')->getLiveStockForItem($product, $axiStoreCode, true, $maxStoreDistance);
            } else {
                $prodStockCollection = Mage::getModel('stock/stock')->getLiveStockForItem($product, null, true, $maxStoreDistance);
            }
        }

        return $prodStockCollection;
    }

    /**
     * Added for next sprint issues - will be changed
     * @return mixed
     */
    public function getPackageStockStatus()
    {
        return Vznl_Stock_Model_Stock::STATUS_IN_STOCK; //temporary fix until getStock call finalised
    }

    /**
     * Added for next sprint issues - will be changed
     * @param $productId
     * @param $storeId
     * @return int
     */
    public function getStockInStore($productId, $storeId)
    {
        /** @var Varien_Db_Adapter_Interface $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $sth = $connection->prepare("SELECT `main_table`.qty FROM `dyna_store_stock` AS `main_table` WHERE (product_id = ?) AND (store_id = ?)");
        $sth->execute(array((int) $productId, (int) $storeId));

        if ($stock = $sth->fetch()) {
            return $stock['qty'];
        }

        return 0;
    }

    /**
     * Added for next sprint issues - will be changed
     * Get the stock amount of all stores except the store with the given store id.
     * @param $productId The id of the product.
     * @param $axiStoreCode The store code.
     * @param $storeId The store id.
     * @return integer The amount of stock for all other stores.
     */
    public function getStockOtherStores($productId, $axiStoreCode, $storeId)
    {
        $warehouseStoreId = Mage::getStoreConfig('vodafone_service/axi/vodafone_warehouse');

        /** @var Vznl_Stock_Model_Mysql4_Storestock_Collection $storeStockCollection */
        $storeStockCollection = Mage::getModel('stock/storestock')->getCollection();
        $storeStockCollection->addFieldToFilter('product_id', $productId);
        $storeStockCollection->addFieldToFilter('main_table.store_id', ['nin' => [$axiStoreCode, $warehouseStoreId]]);
        $storeStockCollection->getSelect()->group('store_id');
        $storeStockCollection->join(['d' => 'agent/dealer'], 'main_table.store_id=d.axi_store_code', ['dealer_store_id' => 'd.store_id']);
        $storeStockCollection->addFieldToFilter('d.store_id', $storeId);

        $stockCollection = $storeStockCollection->getColumnValues('qty');
        return array_sum($stockCollection);
    }

    /**
     * Added for next sprint issues - will be changed
     * @param array $productIds
     * @param $storeId
     * @return array
     */
    public function getStockForProducts(array $productIds, $storeId)
    {
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $col = Mage::getResourceModel('stock/storestock_collection')
            ->addFieldToFilter('product_id', array('in' => $productIds))
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToSelect(array('product_id', 'qty', 'backorder_qty'));
        $data = array();

        $query = $adapter->query($col->getSelect()->reset('COLUMNS'));
        while ($row = $query->fetch()) {
            $data[$row['product_id']]['qty'] = $row['qty'];
            $data[$row['product_id']]['backorder_qty'] = isset($row['backorder_qty']) ? $row['backorder_qty'] : 0;
        }

        return $data;
    }

    /**
     * Added for next sprint issues - will be changed
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        return Mage::getSingleton('checkout/session')->getQuote();
    }
}
