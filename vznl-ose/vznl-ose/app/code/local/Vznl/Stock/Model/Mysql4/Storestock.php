<?php

/**
 * Class Vznl_Stock_Model_Mysql4_Storestock
 */
class Vznl_Stock_Model_Mysql4_Storestock extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("stock/storestock", "entity_id");
    }
}