<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

$installer = $this;

/**
 * Create table 'dyna_store_stock'
 */
if ( ! $installer->getConnection()->isTableExists($installer->getTable('stock/storestock'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('stock/storestock'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
            'nullable'  => false,
        ), 'Product Identifier')
        ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
            'nullable'  => false,
        ), 'Store Identifier')
        ->addColumn('qty', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'nullable'  => false
        ), 'Stock Quantity')
        ->addColumn('backorder_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'nullable'  => false
        ), 'Backorder Quantity')
        ->addColumn('last_modified', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'nullable'  => false,
        ), 'Modified date')
        ->setComment('Store Stock')
        ->setOption('charset', 'utf8');

    $installer->getConnection()->createTable($table);
}

/**
 * Create table 'catalog_product_stock'
 */
if ( ! $installer->getConnection()->isTableExists($installer->getTable('stock/stock'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('stock/stock'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
        ), 'Product Identifier')
        ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
        ), 'Store Identifier')
        ->addColumn('item_count', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'unsigned' => true
        ), 'Item Stock Count')
        ->addColumn('other_store_list', Varien_Db_Ddl_Table::TYPE_TEXT, 65535, array(
            'nullable'  => true,
        ), 'Other Store List')
        ->addColumn('warehouse_count', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'nullable'  => true,
        ), 'Other Store List')
        ->addIndex($installer->getIdxName('stock/stock', array('product_id', 'store_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('product_id', 'store_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('Catalog Product Stock')
        ->setOption('charset', 'utf8');

    $installer->getConnection()->createTable($table);
}


$installer->endSetup();
