<?php
/**
 * Class Vznl_Lucom_Helper_Data
 */
class Vznl_Lucom_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Get the store config values for the given name for Lucom
     * @param string $name The name of the config
     * @return mixed The value if any
     */
    public function getLucomConfig($name = 'use_stubs'){
        return Mage::getStoreConfig('vodafone_service/lucom/' . $name);
    }

    /**
     * Call Lucom's GenericDataService through soap
     * @param Vznl_Checkout_Model_Sales_Order $deliveryOrder The delivery order
     * @return string The url, empty when something went wrong
     */
    public function callLucom(Vznl_Checkout_Model_Sales_Order $deliveryOrder){
        $loggerWriter = new Aleron75_Magemonolog_Model_Logwriter('services/Lucom/'.\Ramsey\Uuid\Uuid::uuid4().'.log');
        $lucomAdapter = Vznl_Lucom_Adapter_Factory::create($this->getLucomConfig('wsdl'), $loggerWriter->getLogger(), $this->getDefaultOptions());
        $response = $lucomAdapter->send($deliveryOrder);

        /** @var Vznl_Transaction_Helper_Data $transactionHelper */
        $transactionHelper = Mage::helper('transaction');
        $customerSession = Mage::getSingleton('customer/session');
        $agent = $customerSession->getAgent();

        $code = ($response && isset($response['code'])) ? $response['code'] : (($lucomAdapter->getLastException()) ? $lucomAdapter->getLastException()->getCode() : '');
        $message = null;

        if ($response) {
            $message = ($code > 0 && isset($response['message'])) ? $response['message'] : null;
        } else {
            $message = ($lucomAdapter->getLastException()) ? $lucomAdapter->getLastException()->getCode() : '';
        }

        $transactionHelper->save(
            Vznl_Transaction_Helper_Data::CONTRACT_SIGN_TYPE,
            $code,
            $agent,
            $message,
            $deliveryOrder
        );

        if (!$response || $code > 0) {
            return '';
        }
        return isset($response['message']) ? $response['message'] : null;
    }

    /**
     * @return array
     */
    protected function getDefaultOptions()
    {
        return [
            'wsdl' => $this->getLucomConfig('wsdl'),
            'endpoint' => $this->getLucomConfig('usage_url'),
            'timeout' => $this->getLucomConfig('timeout'),
            'username' => $this->getLucomConfig('username'),
            'password' => $this->getLucomConfig('password'),
        ];
    }
}
