<?php

/**
 * Lucom default front controller
 */
class Vznl_Lucom_IndexController extends Mage_Core_Controller_Front_Action
{
    /** @var Vznl_Lucom_Helper_Data|null */
    private $_lucomHelper = null;

    /** @var Vznl_Checkout_Helper_Data|null */
    private $_checkoutHelper = null;

    /** @var Vznl_Core_Helper_Data|null */
    private $_dynaCoreHelper = null;

    /**
     * Get the lucom helper
     * @return Vznl_Lucom_Helper_Data The lucom helper
     */
    protected function getLucomHelper()
    {
        if ($this->_lucomHelper === null) {
            $this->_lucomHelper = Mage::helper('lucom');
        }

        return $this->_lucomHelper;
    }

    /**
     * Get the checkout helper
     * @return Vznl_Checkout_Helper_Data The checkout helper
     */
    protected function getCheckoutHelper()
    {
        if ($this->_checkoutHelper === null) {
            $this->_checkoutHelper = Mage::helper('vznl_checkout');
        }

        return $this->_checkoutHelper;
    }

    /**
     * Get the dyna core helper
     * @return Vznl_Core_Helper_Data The dyna core helper
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('vznl_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * Sends all needed information towards Lucom and retrieves the url to sign the contract if successful.
     */
    public function signContractAction()
    {
        $incrementId = $this->getRequest()->getParam('order_id');
        $deliveryOrder = Mage::getModel('sales/order')->loadByIncrementId($incrementId);

        // Check if the delivery order is found
        if (!$deliveryOrder || !$deliveryOrder->getId()) {
            throw new Exception(sprintf($this->getCheckoutHelper()->__('Order with number %s not found'), $incrementId));
        }

        // Get the url
        $url = $this->getLucomHelper()->callLucom($deliveryOrder);
        if (!$url) {
            return $this->jsonResponse([
                'error' => true,
                'message' => $this->getCheckoutHelper()->__('Failed to retrieve data from Lucom'),
            ]);
        }
        return $this->jsonResponse([
            'url' => $url,
            'error' => false,
        ]);
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = $this->getDynaCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);
    }
}
