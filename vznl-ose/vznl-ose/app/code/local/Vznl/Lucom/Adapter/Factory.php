<?php

use Psr\Log\LoggerInterface;
use Zend\Soap\Client;

/**
 * Class Vznl_Lucom_Adapter_Factory
 */
class Vznl_Lucom_Adapter_Factory
{
    /**
     * Create an adapter with the given variables
     * @param string $wsdl The wsdl
     * @param LoggerInterface $logger The logger to use
     * @param array $options An array with options
     * @return Dyna_Lucom_Adapter_Adapter The adapter
     */
    public static function create(string $wsdl, LoggerInterface $logger, array $options = [])
    {
        /** @var Dyna_Lucom_Helper_Data $lucomHelper */
        $lucomHelper = Mage::helper('lucom');

        $stubMode = $lucomHelper->getLucomConfig('use_stubs');
        $soapClient = new Client($wsdl, self::getClientOptions());
        return new Vznl_Lucom_Adapter_Adapter($soapClient, $logger, $options, $stubMode);
    }

    /**
     * Gets the client options for this service.
     * @return array The option array
     */
    protected static function getClientOptions() : array
    {
        $options = Mage::helper('vznl_core/service')->getZendSoapClientOptions('lucom') ? : [];
        $options['soap_version'] = SOAP_1_1;

        return $options;
    }
}
