<?php

/**
 * Class Vznl_Inlife_Model_Client_InlifeClient
 */
class Vznl_Inlife_Model_Client_InlifeClient extends Vznl_Service_Model_Client_Client
{
    const MASK_ALL = 'ALL';
    const MASK_AVAILABLE = 'AVAILABLE';
    const MASK_ASSIGNED = 'ASSIGNED';

    const ELIGIBLE_COMPONENTS = 'eligible_components';
    const ASSIGNED_COMPONENTS = 'assigned_components';

    const MODULE_NAME = 'Vznl_Inlife';

    /**
     * @param $assignedProductId
     * @param $mask
     *
     * @return mixed
     */
    public function getInlifeAddons($assignedProductId, $mask)
    {
        $params = [
            'assigned_product_id' => $assignedProductId,
            'configuration_mask' => $mask,
        ];

        return $this->GetAddons($params);
    }

    /**
     * Should add an addon
     *
     * @param string $assignedProductId
     * @param bool $simulateOnly
     * @param bool $returnEligibleAddons
     * @param bool $isQuotationRequired
     * @param array $addons
     *
     * @return mixed
     * @throws Exception
     * @throws Zend_Soap_Client_Exception
     */
    public function addInlifeAddon(
        $assignedProductId,
        $simulateOnly,
        $returnEligibleAddons,
        $isQuotationRequired,
        $addons
    ) {
        $nestedAddon = function (&$addonNode, $addon) use (&$nestedAddon) {

            if (isset($addon['billing_offer_code']) && $addon['billing_offer_code']) {
                $addonNode->addChild('billing_offer_code', $addon['billing_offer_code']);
            }

            if (isset($addon['component_code']) && $addon['component_code']) {
                $addonNode->addChild('component_code', $addon['component_code']);
            }

            if (isset($addon['assigned_component_id']) && $addon['assigned_component_id']) {
                $addonNode->addChild('assigned_component_id', $addon['assigned_component_id']);
            }

            if (isset($addon['service_id']) && $addon['service_id']) {
                $addonNode->addChild('service_id', $addon['service_id']);
            }

            if (isset($addon['parent_assigned_component_id']) && $addon['parent_assigned_component_id']) {
                $addonNode->addChild('parent_assigned_component_id', $addon['parent_assigned_component_id']);
            }

            if (isset($addon['context']) && $addon['context']) {
                $settings = $addonNode->addChild('AddOnContext');
                foreach ((isset($addon['context']) && is_array($addon['context']) ? $addon['context'] : []) as $component => $value) {
                    $settings->addChild(Mage::helper('vznl_core')->snakeToCamel($component), $value);
                }
            }

            if (isset($addon['component_settings']) && $addon['component_settings']) {
                $settings = $addonNode->addChild('component_settings');
                foreach ((isset($addon['component_settings']) && is_array($addon['component_settings']) ? $addon['component_settings'] : []) as $component => $value) {
                    $value = trim($value);
                    if (!empty($value)) {
                        $setting = $settings->addChild('component_setting', null);
                        $setting->addChild('component_attribute', $component);
                        $setting->addChild('value', $value);
                    }
                }
            }

            if (isset($addon['AddOnsData'])) {
                foreach ($addon['AddOnsData'] as $nestedNode) {
                    $addonSubNode = $addonNode->addChild('AddOnsData');
                    $nestedAddon($addonSubNode, $nestedNode);
                }
            }
        };

        $params = [
            'assigned_product_id' => $assignedProductId,
            'simulate_only' => $simulateOnly ? 'true' : 'false',
            'return_eligible_addons' => $returnEligibleAddons ? 'true' : 'false',
            'is_quotation_required' => $isQuotationRequired ? 'true' : 'false',
            'AddOnsData' => function (array $node) use ($addons, $nestedAddon) {
                $addonsXml = new Omnius_Core_Model_SimpleDOM('<root></root>');

                if (isset($addons['vom_addon'])) {
                    if (!isset($addons['connected_vom'])) {
                        $addonNode = $addonsXml->addChild('AddOnsData');
                        $nestedAddon($addonNode, $addons);
                    } else {
                        unset($addons['connected_vom']);
                        $nestedAddon($addonsXml, $addons);
                    }
                    unset($addons['vom_addon']);
                } else {
                    foreach ($addons as $addon) {
                        $addonNode = $addonsXml->addChild('AddOnsData');
                        $nestedAddon($addonNode, $addon);
                    }
                }

                foreach ($addonsXml->children() as $addon) {
                    $node[0]->insertAfterSelf($addon);
                }
                unset($node[0][0]);

            }
        ];

        return $this->addAddOn($params);
    }

    /**
     * Should remove an addon
     *
     * @param $assignedProductId
     * @param bool $simulateOnly
     * @param bool $returnEligibleAddons
     * @param array $addons
     * @param bool $isQuotationRequired
     *
     * @return mixed
     */
    public function removeInlifeAddon(
        $assignedProductId,
        $simulateOnly,
        $returnEligibleAddons,
        $addons,
        $isQuotationRequired = true
    ) {
        $params = [
            'assigned_product_id' => $assignedProductId,
            'is_quotation_required' => $isQuotationRequired ? 'true' : 'false',
            'AddOnRemoveData' => function (array $node) use ($addons) {
                $addonsXml = new Omnius_Core_Model_SimpleDOM('<root></root>');
                foreach ($addons as $addon) {
                    $addonNode = $addonsXml->addChild('AddOnRemoveData');
                    $addonNode->addChild('billing_offer_code', $addon['billing_offer_code']);

                    if (isset($addon['assigned_component_id']) && $addon['assigned_component_id']) {
                        $addonNode->addChild('assigned_component_id', $addon['assigned_component_id']);
                    }
                }

                foreach ($addonsXml->children() as $addon) {
                    $node[0]->insertAfterSelf($addon);
                }
                unset($node[0][0]);
            },
            'simulate_only' => $simulateOnly ? 'true' : 'false',
            'return_eligible_addons' => $returnEligibleAddons ? 'true' : 'false',
        ];

        return $this->RemoveAddon($params);
    }

    /**
     * Adds Inlife client related headers to the requests
     */
    protected function addInlifeHeaders()
    {
        /** @var Dyna_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');
        $agent = $session->getSuperAgent() ? $session->getSuperAgent() : $session->getAgent(true);

        $dealerCode = '';
        if (Mage::registry('job_additional_information')) {
            $additionalInfo = Mage::registry('job_additional_information');
            $dealerCode = $additionalInfo['dealer_code'];
            Mage::unregister('job_additional_information');
        }
        if (!$dealerCode) {
            if (isset($this->_options['dealer_code']) && $this->_options['dealer_code']) {
                $dealerCode = $this->_options['dealer_code'];
            } elseif ($agent) {
                $dealerCode = $agent->getDealer() ? $agent->getDealer()->getVfDealerCode() : '';
            }
        }

        $header = new Omnius_Core_Model_SimpleDOM('<root></root>');
        /** @var Omnius_Core_Model_SimpleDOM $bslHeader */
        $bslHeader = $header->addChild('com:BSLHeader', null, 'http://com.amdocs.bss.bsl/');

        $headerParams = new Omnius_Core_Model_SimpleDOM('<root></root>');
        $headerParams->addChild('Username', $this->_options['bsl_username']);
        $headerParams->addChild('Password', $this->_options['bsl_password']);
        $headerParams->addChild('MessageCreationTimestamp', date('c'));
        $headerParams->addChild('DealerCode', $dealerCode ?: '0');
        $headerParams->addChild('MessageID', str_replace('.', '', microtime(true)));
        $headerParams->addChild('EndUserName', self::STATIC_USERNAME_BSL);

        $bslHeader->cloneChildrenFrom($headerParams);

        $var = new SoapVar($bslHeader->asXML(), XSD_ANYXML);
        $header = new SOAPHeader('http://com.amdocs.bss.bsl/', 'BSLHeader', $var);
        parent::__setSoapHeaders([$header]);
    }

    /**
     * @param $customerId
     * @return mixed
     */
    public function searchInlifeOrder($customerId)
    {
        $data = new Varien_Object();
        if (is_array($customerId)) {
            $data->addData($customerId);
        } else {
            $data->setCustomerId($customerId);
        }

        $params = array(
            'customerID' => $data->getCustomerId(),
            'email' => function (array $node) use ($data) {
                if ($data->getData('email')) {
                    $node[0][0] = $data->getData('email');
                } else {
                    unset($node[0][0]);
                }
            },
            'pinCode' => function (array $node) use ($data) {
                if ($data->getData('pin_code')) {
                    $node[0][0] = $data->getData('pin_code');
                } else {
                    unset($node[0][0]);
                }
            },
            'orderID' => function (array $node) use ($data) {
                if ($data->getData('order_id')) {
                    $node[0][0] = $data->getData('order_id');
                } else {
                    unset($node[0][0]);
                }
            },
            'serviceID' => function (array $node) use ($data) {
                if ($data->getData('service_id')) {
                    $node[0][0] = $data->getData('service_id');
                } else {
                    unset($node[0][0]);
                }
            },
            'zipCode' => function (array $node) use ($data) {
                if ($data->getData('zip_code')) {
                    $node[0][0] = $data->getData('zip_code');
                } else {
                    unset($node[0][0]);
                }
            },
            'orderStatus' => function (array $node) use ($data) {
                if ($data->getData('order_status')) {
                    $node[0][0] = $data->getData('order_status');
                } else {
                    unset($node[0][0]);
                }
            },
            'orderSubStatus' => function (array $node) use ($data) {
                if ($data->getData('order_sub_status')) {
                    $node[0][0] = $data->getData('order_sub_status');
                } else {
                    unset($node[0][0]);
                }
            },
            'fromDate' => function (array $node) use ($data) {
                if ($data->getData('from_date')) {
                    $node[0][0] = $data->getData('from_date');
                } else {
                    unset($node[0][0]);
                }
            },
            'toDate' => function (array $node) use ($data) {
                if ($data->getData('to_date')) {
                    $node[0][0] = $data->getData('to_date');
                } else {
                    unset($node[0][0]);
                }
            },
            'dealerCode' => function (array $node) use ($data) {
                if ($data->getData('dealer_code')) {
                    $node[0][0] = $data->getData('dealer_code');
                } else {
                    unset($node[0][0]);
                }
            },
            'portingStatus' => function (array $node) use ($data) {
                if ($data->getData('porting_status')) {
                    $node[0][0] = $data->getData('porting_status');
                } else {
                    unset($node[0][0]);
                }
            },
        );
        return $this->SearchOrder($params);
    }

    protected function needsILSHeader()
    {
        return true;
    }

    /**
     * @return Omnius_Service_Helper_Request
     */
    protected function _getRequestBuilder()
    {
        if (!$this->_requestBuilder) {
            $this->_requestBuilder = Mage::helper('vznl_inlife/request');
        }

        return $this->_requestBuilder;
    }

    /**
     * @param $assignedProductID
     * @return mixed
     */
    public function getInlifeProductSettings($assignedProductID)
    {
        $params = [
            'assigned_product_id' => $assignedProductID,
        ];

        return $this->GetProductSettings($params);
    }

    /**
     * @param $assignedProductID
     * @param $assignedComponentID
     * @param $parentAssignedProductID
     * @param $newCtn
     * @param string $simulateOnly
     * @param bool $returnEligibleSettings
     * @return mixed
     */
    public function updateInlifeProductSettings(
        $assignedProductID,
        $assignedComponentID,
        $parentAssignedProductID,
        $newCtn,
        $simulateOnly = 'true',
        $returnEligibleSettings = true
    ) {
        $componentSettings = [
            'RLC_Voice Line MSISDN' => $newCtn,
            'PortingInd' => 'N',
        ];

        $params = [
            'assigned_product_id' => $assignedProductID,
            'action' => 'SET', // static
            'component_code' => 'Primary_MSISDN', // static
            'assigned_component_id' => $assignedComponentID,
            'parent_assigned_product_id' => $parentAssignedProductID,
            'component_settings' => function (array $node) use ($componentSettings) {
                $addonsXml = new Omnius_Core_Model_SimpleDOM('<root></root>');

                $settings = $addonsXml->addChild('component_settings');
                foreach ($componentSettings as $component => $value) {
                    $setting = $settings->addChild('component_setting', null);
                    $setting->addChild('settingcode', $component);
                    $setting->addChild('value', $value);
                }

                foreach ($addonsXml->children() as $addon) {
                    $node[0]->insertAfterSelf($addon);
                }
                unset($node[0][0]);
            },
            'simulate_only' => $simulateOnly,
            'return_eligible_settings' => $returnEligibleSettings ? 'true' : 'false'
        ];

        return $this->updateProductSettings($params);
    }

    /**
     * @param $assignedProductID
     * @return mixed
     */
    public function getAssignedInlifeProductDetails($assignedProductID)
    {
        $params = array(
            'retrieveAssignedProductInputData/assignedProductIDList' => $assignedProductID,
        );

        return $this->GetAssignedProductDetails($params);
    }

    /**
     * OrderSIM is called when SimSwap via Hawaii.
     * OrderSIM registers the SimSwap where the sim activation happens by the end-user himself.
     *
     * @param $assignedProductId
     * @param $simNumber
     * @param string $componentCode
     * @param null $serviceId
     * @param null $parentAssignedProductId
     */
    public function orderInlifeSim($assignedProductId, $simNumber, $componentCode = 'SIM_Card', $serviceId = null, $parentAssignedProductId = null)
    {
        $params = array(
            'assigned_product_id' => $assignedProductId,
            'component_code' => $componentCode,
            'service_id' => $serviceId,
            'parent_assigned_product_id' => function (array $node) use ($parentAssignedProductId) {
                if ($parentAssignedProductId) {
                    $node[0][0] = $parentAssignedProductId;
                } else {
                    unset($node[0][0]);
                }
            },
            'sim_number' => $simNumber,
        );
        return $this->OrderSIM($params);
    }

    /**
     * ChangeSIM activates the new sim immediately via Online.
     *
     * @param $assignedProductId
     * @param $simNumber
     * @param $newSimNumber
     * @param $contactId
     */
    public function changeInlifeSim($assignedProductId, $simNumber, $newSimNumber, $contactId)
    {
        $params = array(
            'assigned_product_id' => $assignedProductId,
            'sim_number' => $simNumber,
            'new_sim_number' => $newSimNumber,
            'contact_id' => $contactId,
        );

        return $this->changeSIM($params);
    }
}
