<?php

/**
 * Class Vznl_Inlife_IndexController
 */
class Vznl_Inlife_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * @var Dyna_Customer_Model_Session
     */
    protected $customerSession;

    /**
     * @var Dyna_Customer_Model_Customer
     */
    protected $currentCustomer;

    /**
     * Predispatch: should set layout area
     *
     * @return Vznl_Inlife_IndexController
     */
    public function preDispatch()
    {
        parent::preDispatch();

        $this->customerSession = Mage::getSingleton('customer/session');
        $this->currentCustomer = $this->customerSession->getCustomer();

        return $this;
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Show the main page for ILS
     *
     * @return void - in case of a not logged customer
     */
    public function indexAction()
    {
        // Check if the customer is logged in
        if (!$this->currentCustomer->getId()) {
            $this->_redirect('/');
            return;
        }

        $this->loadLayout();
        $block = $this->getLayout()->createBlock('core/template', 'inlife.index');

        try {
            $ctn = $this->getRequest()->get('ctn');
            $ctn = Mage::getModel('ctn/ctn')->loadCtn($ctn, $this->currentCustomer->getId());
            if (!$ctn) {
                // If no ctn redirect with message
                $this->customerSession->unsAssignedProductId();
            }
            // Check CTN belongs to customer
            $customerCtn = $this->validateCustomerCtn($ctn->getCode());
            $key = 'current_subscription_' . $ctn->getCode();

            if (!$currentSubscription = Mage::app()->getCache()->load($key)) {
                $currentSubscription = $customerCtn->getCurrentSubscription(true);
            }

            $block->setCtn($ctn)
                ->setCustomerCtn($customerCtn)
                ->setCustomer($this->currentCustomer)
                ->setCurrentSubscription($currentSubscription);

            $serviceError = false;
        } catch (Exception $e) {
            if (!empty($e->response)) {
                $errorResponse = $e->response;
                $serviceError = $errorResponse['body']['fault'];
                $serviceError['operation'] = $errorResponse['header']['operation'];
                $serviceError['request_id'] = $errorResponse['header']['request_id'];
            } else {
                $serviceError = $this->__($e->getMessage());
            }
        }

        Mage::log($serviceError, null, 'ctn.log');

        $block->setServiceError($serviceError)
            ->setTemplate('inlife/ctn_details.phtml');

        $this->getLayout()->getBlock('root')->setIsCheckout(true)->setIsInlife(true)->setTemplate('page/3columns.phtml');
        $this->getLayout()->getBlock('content')->append($block);
        $this->_initLayoutMessages('core/session');

        $this->renderLayout();
    }

    /**
     * Process the AJAX call and call the external service for the provided CTN in order to receive possible addons
     *
     * @return Zend_Controller_Response_Abstract
     */
    public function getInlifeCtnDataAction()
    {
        try {
            if ($this->getRequest()->isPost()) {
                $ctnCode = $this->getRequest()->getPost('ctn_code');

                $customerCtn = $this->validateCustomerCtn($ctnCode);
                $this->customerSession->setAssignedProductId($ctnCode);
                session_write_close();

                $client = $this->getInlifeClient();
                $allAddons = $client->getInlifeAddons($ctnCode, Vznl_Inlife_Model_Client_InlifeClient::MASK_ALL);

                $eligibleAddons = $allAddons[Vznl_Inlife_Model_Client_InlifeClient::ELIGIBLE_COMPONENTS] ?? [];
                if (isset($eligibleAddons['component_identifier'])) {
                    // Only one item
                    $eligibleAddons = [$eligibleAddons];
                }

                $removableAddons = $allAddons[Vznl_Inlife_Model_Client_InlifeClient::ASSIGNED_COMPONENTS] ?? [];
                if (isset($removableAddons['component_identifier'])) {
                    // Only one item
                    $removableAddons = [$removableAddons];
                }

                /** @var Vznl_Inlife_Helper_Data $inlineHelper */
                $inlineHelper = Mage::helper('vznl_inlife');

                $eligibleAddonsArray = $inlineHelper->processAddonData($eligibleAddons);
                $this->setCurrentSessionAddons($eligibleAddonsArray, 'eligible', $ctnCode);

                $removableAddonsArray = $inlineHelper->processAddonData($removableAddons);
                $this->setCurrentSessionAddons($removableAddonsArray, 'removable', $ctnCode);

                // Render the block that will contain the data and send it as response
                $block = $this->getLayout()
                    ->createBlock(
                        'Mage_Core_Block_Template',
                        'inlife.index',
                        ['template' => 'inlife/ctn_details_block.phtml']
                    )
                    ->addData(
                        array(
                            'service_error' => false,
                            'eligible_addons' => $eligibleAddonsArray,
                            'removable_addons' => $removableAddonsArray,
                            'ctn_code' => $customerCtn->getCode(),
                            'customer' => $this->currentCustomer,
                        )
                    );

                $result = [
                    'error' => false,
                    'html' => $block->toHtml()
                ];
            } else {
                throw new Exception('Invalid request');
            }
        } catch (Zend_Soap_Client_Exception $e) {
            if (!empty($e->response)) {
                $errorResponse = $e->response;
                $serviceError = $errorResponse['body']['fault'];
                $serviceError['operation'] = isset($errorResponse['header']['operation'])
                    ? $errorResponse['header']['operation'] : '';
                $serviceError['request_id'] = isset($errorResponse['header']['request_id'])
                    ? $errorResponse['header']['request_id'] : '';
                $message = $this->__($e->getMessage());
            } else {
                $message = $this->__($e->getMessage());
            }
            $block = $this->getLayout()->createBlock(
                'Mage_Core_Block_Template',
                'inlife.index',
                ['template' => 'inlife/ctn_details_block.phtml']
            );
            $block->setServiceError($serviceError);
            $block->setCtnCode($customerCtn->getCode());

            $result = [
                'error' => false,
                'html' => $message ?: $block->toHtml()
            ];
        } catch (Exception $e) {
            Mage::logException($e);
            $result = [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }

        return $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }

    /**
     * Retrieve a specific template block based on the requested block parameter
     *
     * @return void
     */
    public function getInlifeBlockAction()
    {
        if ($this->getRequest()->isPost()) {
            $message = '';
            $error = false;
            $blockName = $this->getRequest()->getPost('block');
            $assignedProductId = $ctnCode = $this->customerSession->getAssignedProductId(); // ctn_code for the current CTN

            // Render the block that will contain the data and send it as response
            $block = $this->getLayout()->createBlock(
                'Mage_Core_Block_Template',
                'inlife.index',
                ['template' => 'inlife/' . $blockName . '.phtml']
            );

            if ($this->getRequest()->getPost('edit') && !$this->getRequest()->getPost('simulate')) {
                if ($this->getRequest()->getPost('connected_vom')) {
                    $addon = $this->getCurrentSessionAddons('removable', $this->getRequest()->getPost('billingOfferCode'), $ctnCode);
                    $block->setAddon($addon);

                    // GET product settings
                    $client = $this->getInlifeClient();
                    session_write_close(); //release session lock
                    $currentlyConnectedVom = 0;
                    $currentlyConnectedVomFaxes = 0;

                    $settings = $client->getInlifeProductSettings($assignedProductId);
                    if ($settings && is_array($settings)) {
                        $settings = isset($settings['components_with_settings']) ? $settings['components_with_settings'] : null;
                        if ($settings && is_array($settings)) {
                            $this->getCurrentlyConnectedVom($settings, $currentlyConnectedVom, $currentlyConnectedVomFaxes);
                        }
                    }
                    $block->setConnectedVom($currentlyConnectedVom);
                    $block->setConnectedFaxes($currentlyConnectedVomFaxes);
                } else {
                    $addon = $this->getCurrentSessionAddons('eligible', $this->getRequest()->getPost('billingOfferCode'), $ctnCode);
                    $block->setAddon($addon);
                }
            } else {
                $block->setData($this->getRequest()->getPost());
            }

            if ($this->getRequest()->getPost('action') == 'add' && $this->getRequest()->getPost('simulate') == true) {
                // If we try to add an AddOn, perform AddAddon call with ValidateOnly = true
                $billingOfferCode = $this->getRequest()->getPost('billingOfferCode');
                $componentCode = $this->getRequest()->getPost('componentCode');
                $assignedComponentId = $this->getRequest()->getPost('assignedComponentId');
                $parentAssignedComponentId = $this->getRequest()->getPost('parentAssignedComponentId');

                $serviceError = false;

                try {
                    $response = $this->callAddAddon(
                        $assignedProductId,
                        $billingOfferCode,
                        $componentCode,
                        $assignedComponentId,
                        $parentAssignedComponentId,
                        $simulateOnly = true
                    );

                    $this->processCallResponse($block, $response);

                    if (isset($response['order_i_d'])) {
                        $block->setData('order_id', $response['order_i_d']);
                    }

                    if (isset($response['service_required_date'])) {
                        $effective_date = Mage::app()->getLocale()->date(
                            substr($response['service_required_date'], 0, 10), null, null, false
                        )->toString('d-MM-y');
                        $block->setData('effective_date', $effective_date);
                    }
                } catch (Exception $e) {
                    $message = $this->__($e->getMessage());
                    if (!empty($e->response)) {
                        $errorResponse = $e->response;
                        $serviceError = $errorResponse['body']['fault'];
                        $serviceError['operation'] = isset($errorResponse['header']['operation'])
                            ? $errorResponse['header']['operation'] : '';
                        $serviceError['request_id'] = isset($errorResponse['header']['request_id'])
                            ? $errorResponse['header']['request_id'] : '';
                    } else {
                        $error = true;
                    }
                }

                $block->setServiceError($serviceError);
            }

            if ($this->getRequest()->getPost('action') == 'remove' && $this->getRequest()->getPost('simulate') == true) {
                $billingOfferCode = $this->getRequest()->getPost('billingOfferCode');
                $assignedComponentId = $this->getRequest()->getPost('assignedComponentId');

                $serviceError = false;

                try {
                    $response = $this->callRemoveAddon($assignedProductId, $billingOfferCode, $assignedComponentId, $simulateOnly = true);
                    $this->processCallResponse($block, $response);
                } catch (Exception $e) {
                    $message = $this->__($e->getMessage());
                    if (!empty($e->response)) {
                        $errorResponse = $e->response;
                        $serviceError = $errorResponse['body']['fault'];
                        $serviceError['operation'] = isset($errorResponse['header']['operation'])
                            ? $errorResponse['header']['operation'] : '';
                        $serviceError['request_id'] = isset($errorResponse['header']['request_id'])
                            ? $errorResponse['header']['request_id'] : '';
                    } else {
                        $error = true;
                    }
                }

                $block->setServiceError($serviceError);
            }

            $result = [
                'error' => $error,
                'html' => $message ?: $block->toHtml(),
                'message' => $message
            ];

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        }
        //return;
    }

    /**
     * Get currently connected Vom
     *
     * @param $settings
     * @param $currentlyConnectedVom
     * @param $currentlyConnectedVomFaxes
     */
    protected function getCurrentlyConnectedVom($settings, &$currentlyConnectedVom, &$currentlyConnectedVomFaxes)
    {
        foreach ($settings as $setting) {
            $identifier = $setting['component_identifier'];
            if ($identifier['catalog_code'] == 'Vast_Op_Mobiel_Number') {
                if (isset($identifier['item_identifier_relation']) && isset($identifier['item_identifier_relation']['currenly_assigned'])) {
                    $currentlyConnectedVom += intval($identifier['item_identifier_relation']['currenly_assigned']);
                }
                if (isset($identifier['item_attributes'])) {
                    if (isset($identifier['item_attributes']['catalog_id'])) {
                        $identifier['item_attributes'] = [$identifier['item_attributes']];
                    }
                    foreach ($identifier['item_attributes'] as $itemAttribute) {
                        if ($itemAttribute['catalog_code'] == 'Required_for_Fax') {
                            $currentlyConnectedVomFaxes += (isset($itemAttribute['selected_value']) && $itemAttribute['selected_value'] == 'Yes') ? 1 : 0;
                        }
                    }
                }
            }
        }
    }

    /**
     * Process the AJAX call and call the external service for the provided CTN in order to add an addon
     *
     * @return Zend_Controller_Response_Abstract
     */
    public function addInlifeAddonAction()
    {
        $block = $this->getLayout()->createBlock('core/template');
        $serviceError = false;
        $error = false;

        try {
            $request = $this->getRequest();
            if ($request->isPost()) {
                $assignedProductId = $this->customerSession->getAssignedProductId(); // ctn_code for the current CTN
                $billingOfferCode = $request->getPost('billingOfferCode');
                $componentCode = $request->getPost('componentCode');
                $assignedComponentId = $request->getPost('assignedComponentId');
                $parentAssignedComponentId = $request->getPost('parentAssignedComponentId');

                $this->callAddAddon($assignedProductId, $billingOfferCode, $componentCode, $assignedComponentId, $parentAssignedComponentId, $simulateOnly = false);
            } else {
                throw new Exception('Invalid request');
            }
        } catch (Exception $e) {
            $error = true;
            if (!empty($e->response)) {
                $errorResponse = $e->response;
                $serviceError = $errorResponse['body']['fault'];
                $serviceError['operation'] = !empty($errorResponse['header']['operation'])
                    ? $errorResponse['header']['operation'] : "";
                $serviceError['request_id'] = !empty($errorResponse['header']['request_id'])
                    ? $errorResponse['header']['request_id'] : "";
            } else {
                $serviceError = $this->__($e->getMessage());
            }
        }

        $block->setServiceError($serviceError)
            ->setOverviewButton(true)
            ->setTemplate('inlife/partials/service_error.phtml');

        $result = [
            'error' => $error,
            'html' => $block->toHtml()
        ];

        return $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }

    /**
     * Execute the service call to add a new addon
     *
     * @param $assignedProductId
     * @param $billingOfferCode
     * @param $componentCode
     * @param $assignedComponentId
     * @param $parentAssignedComponentId
     * @param bool $simulateOnly
     * @param bool $returnEligibleAddons
     * @param bool $isQuotationRequired
     * @param array $componentSettings
     *
     * @return bool|mixed
     * @throws Exception
     */
    private function callAddAddon(
        $assignedProductId,
        $billingOfferCode,
        $componentCode,
        $assignedComponentId,
        $parentAssignedComponentId,
        $simulateOnly = true,
        $returnEligibleAddons = false,
        $isQuotationRequired = true,
        $componentSettings = []
    ) {
        $request = $this->getRequest();

        // Validate input data
        if (!trim($billingOfferCode) || !trim($componentCode)) {
            throw new Exception('Invalid input data');
        }

        // Garant addon settings
        if (($Device_Insurance_Category = $request->getPost('Device_Insurance_Category')) &&
            ($Insurance_Service_Variant = $request->getPost('Insurance_Service_Variant')) &&
            ($Device_IMEI = $request->getPost('Device_IMEI'))
        ) {
            $componentSettings = [
                'Device_Insurance_Category' => $Device_Insurance_Category,
                'Insurance_Service_Variant' => is_array($Insurance_Service_Variant)
                    ? $Insurance_Service_Variant[$Device_Insurance_Category] : $Insurance_Service_Variant,
                'Device_IMEI' => $Device_IMEI,
                'Commitment_Period' => $request->getPost('Commitment_Period')
            ];
        }

        $rawAddon = $this->getCurrentSessionAddons('eligible', $billingOfferCode, $assignedProductId);

        $addons[] = [
            'billing_offer_code' => $billingOfferCode,
            'component_code' => $componentCode,
            'assigned_component_id' => $assignedComponentId,
            'parent_assigned_component_id' => $parentAssignedComponentId,
            'component_settings' => $componentSettings,
            'context' => $rawAddon['context']
        ];

        // Multisim addon settings
        if ($multisimData = $request->getPost('multisim')) {
            $sim = $multisimData['simcard'];
            if (!$simulateOnly && Mage::helper('agent')->isTelesalesChannel()) {
                if (!$sim || empty($sim)) {
                    throw new Exception('Could not find SIM value in ProcessOrderResponse');
                }
            }

            $addons = [];
            $multisimAddons = $multisimData['addon'];
            $prefixMultisim = $request->getPost('prefix_multisim');
            $postfixMultisim = $request->getPost('postfix_multisim');

            foreach ($multisimAddons as $addon_billing_offer_code) {
                $addonOptions = [
                    'billing_offer_code' => $addon_billing_offer_code,
                    'component_code' => $componentCode,
                    'assigned_component_id' => $assignedComponentId,
                    'parent_assigned_component_id' => $parentAssignedComponentId,
                    'context' => $rawAddon['context']
                ];

                if (($prefixMultisim && $postfixMultisim) || $sim) {
                    if ($prefixMultisim && $postfixMultisim) {
                        $prefix = $prefixMultisim[$addon_billing_offer_code];
                        $postfix = $postfixMultisim[$addon_billing_offer_code];
                        $simCardNumber = $prefix . $postfix;
                    } elseif ($sim) {
                        $simCardNumber = isset($sim[$addon_billing_offer_code]) ? $sim[$addon_billing_offer_code] : reset($sim);
                    }

                    $componentSettings = [
                        'RLC_SIM card number' => str_replace(' ', '', $simCardNumber)
                    ];

                    $addonOptions['component_settings'] = $componentSettings;
                }

                $addons[] = $addonOptions;
            }
        }

        // Handle Vom settings
        if ($fixed = $request->getPost('fixed')) {
            if ($request->getPost('connected')) {
                $addons = [
                    'vom_addon' => true,
                    'connected_vom' => true
                ];
            } else {
                $addons = [
                    'vom_addon' => true,
                    'billing_offer_code' => $billingOfferCode,
                    'component_code' => $componentCode,
                    //'assigned_component_id' => $assignedComponentId,
                    'parent_assigned_component_id' => $parentAssignedComponentId,
                    'context' => $rawAddon['context'],
                ];
            }

            $this->getFixedAddons($fixed, $rawAddon, $componentCode, $addons);
        }

        /** @var Vznl_Inlife_Model_Client_InlifeClient $client */
        $client = $this->getInlifeClient();

        $orderId = $request->getPost('order_id');
        if ($multisimData && $orderId) {
            $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderId);
            if ($superOrder->getId()) {
                // For Telesales and Indirect we call orderSim with the SIM returned by processOrder
                if (!(
                    Mage::helper('agent')->isTelesalesChannel()
                    || Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE)
                ) {
                    $isJob = true;
                    $info = serialize(
                        [
                            'dealer_code' => $this->customerSession->getAgent(true)->getDealer()->getVfDealerCode(),
                            'hardware_order_id' => $superOrder->getId(),
                            'method' => 'addInlifeAddon',
                            'arguments' => [
                                $assignedProductId,
                                $simulateOnly,
                                $returnEligibleAddons,
                                $isQuotationRequired,
                                $addons,
                                $isJob
                            ]
                        ]
                    );
                    $superOrder->setXmlToBeSent($info)
                        ->setIsIls(true)
                        ->save();

                    return true;
                }

            } else {
                throw new Exception(sprintf('Order with number %s not found', $orderId));
            }
        }

        session_write_close(); //release session lock
        return $client->addInlifeAddon(
            $assignedProductId,
            $simulateOnly,
            $returnEligibleAddons,
            $isQuotationRequired,
            $addons
        );
    }

    /**
     * Map fixed addons
     *
     * @param $fixed
     * @param $rawAddon
     * @param $componentCode
     * @param $addons
     *
     * @throws Exception
     */
    protected function getFixedAddons($fixed, $rawAddon, $componentCode, &$addons)
    {
        foreach ($fixed as $index => $vom) {
            if (empty($vom)) {
                continue;
            }

            // validate vom addresses information
            $errors = $this->_validateVomData($vom);

            if (!empty($errors)) {
                throw new Exception('Please make sure that the addresses have valid information!');
            }

            $vomAddress = $vom['address'];
            $fixedTelephoneNumber = ($vom['porting'] == 1) ? $vom['number'] : $vom['new_number'];
            $componentSettings = [
                'LR_Fixed_Telephone_Number' => $fixedTelephoneNumber,
                'House_Number_TN' => trim(
                    $vom['fixed_address'][$vomAddress]['houseno'] .
                    ' ' . $vom['fixed_address'][$vomAddress]['addition']
                ),
                'Zip_Code_TN' => $vom['fixed_address'][$vomAddress]['postcode'],
                'Required_for_Fax' => $vom['fax'],
                'PortingInd' => ($vom['porting'] == 0) ? 'N' : 'I',
            ];

            // General AddOnsData settings
            $context = $rawAddon['context'];
            $context['parent_cataloge_code'] = $componentCode;

            $tmpAddon = [
                'component_code' => 'Vast_Op_Mobiel_Number',
                'context' => $context,
                'component_settings' => $componentSettings,
                //'parent_assigned_component_id' => $parentAssignedComponentId,
            ];

            unset($context);

            $addonContext = $rawAddon['context'];
            $addonContext['parent_cataloge_code'] = 'Vast_Op_Mobiel_Number';

            // If Fax to email is selected
            if ($vom['fax'] == 'Yes') {
                $tmpAddon['AddOnsData'][] = [
                    'component_code' => 'Fax_To_Email',
                    'context' => $addonContext,
                    'component_settings' => [
                        'Email_Address' => trim($vom['fax_email'])
                    ]
                ];
            }

            // If directory listing is selected
            if ($vom['phonebook'] == 'Yes') {
                $ctnCode = $this->currentCustomer->getAssignedProductId();
                $key = sprintf('%s_%s_%s_%s', Mage::getSingleton('core/session')->getEncryptedSessionId(), $this->currentCustomer->getBan(), $ctnCode, 'inlife_vom_billingAddress');
                $billingAddress = unserialize(Mage::app()->getCache()->load($key));

                $tmpAddon['AddOnsData'][] = [
                    'component_code' => 'CTN_Directory_Listing',
                    'context' => $addonContext,
                    'component_settings' => [
                        'Last_Name' => trim($this->currentCustomer->getLastname()),
                        'Postal_Code' => trim($billingAddress['postcode']),
                        'City' => trim($billingAddress['city']),
                    ]
                ];
            }

            // If porting has been selected for the fixed number
            if ($vom['porting'] == 1) {
                $contractAddress = $vom['contract_address'];
                $componentSettings = [];
                $componentSettings['Porting_Wish_Date_Time'] = date('Y-m-d', strtotime($vom['porting_date'])) . 'T00:00:00+02:00';
                $componentSettings['DonorNP'] = $vom['current_provider'];
                if (trim($vom['customer_number_provider'])) {
                    $componentSettings['Contract_number_with_Donor'] = trim($vom['customer_number_provider']);
                }
                $componentSettings['Contract_holder_name'] = trim($vom['customer_firstname']);
                $componentSettings['Contract_holder_surname'] = trim($vom['customer_lastname']);
                $componentSettings['Contract_Holder_Name_Prefix']
                    = ($vom['customer_gender'] == 1) ? 'Dhr.' : 'Mevr.';
                $componentSettings['Contract_Holder_Name_Initials'] = strtoupper($vom['customer_firstname'][0]);
                $componentSettings['Contract_Holder_Country_Code'] = 'NLD'; // TODO: determine if static or not
                $componentSettings['Contract_Holder_Street_Name']
                    = $vom['address_provider'][$contractAddress]['street'];
                $componentSettings['Contract_Holder_Postal_Code']
                    = $vom['address_provider'][$contractAddress]['postcode'];
                $componentSettings['Contract_Holder_House_Number']
                    = $vom['address_provider'][$contractAddress]['houseno'];
                $componentSettings['Contract_Holder_Additional_House_Number']
                    = $vom['address_provider'][$contractAddress]['addition'];
                $componentSettings['Contract_Holder_City'] = $vom['address_provider'][$contractAddress]['city'];
                $componentSettings['Service_Number_Indicator']
                    = (isset($vom['service_number']) && $vom['service_number']) ? 'Yes' : 'No';

                // Business customer
                if ($vom['customer_type'] == 1) {
                    $componentSettings['Organization_name'] = trim($vom['customer_organization']);
                    if (trim($vom['customer_contract_end_date'])) {
                        $componentSettings['Contract_End_Date']
                            = date('Y-m-d', strtotime(trim($vom['customer_contract_end_date'])))
                            . 'T00:00:00+02:00';
                    }
                }

                $tmpAddon['AddOnsData'][] = [
                    'component_code' => 'Fixed_Number_Porting',
                    'context' => $addonContext,
                    'component_settings' => $componentSettings,
                ];
                unset($context);
            }

            $addons['AddOnsData'][] = $tmpAddon;

            unset($addonContext);
            unset($tmpAddon);
        }
    }

    /**
     * Process the AJAX call and call the external service for the provided CTN in order to remove an addon
     *
     * @return Zend_Controller_Response_Abstract
     */
    public function removeInlifeAddonAction()
    {
        $block = $this->getLayout()->createBlock('core/template');
        $serviceError = false;
        $error = false;

        try {
            if ($this->getRequest()->isPost()) {
                $assignedProductId = $this->customerSession->getAssignedProductId(); // ctn_code for the current ctn
                $billingOfferCode = $this->getRequest()->getPost('billingOfferCode'); // addon id
                $assignedComponentId = $this->getRequest()->getPost('assignedComponentId');

                // Validate input data
                if (!trim($billingOfferCode) || !trim($assignedComponentId)) {
                    throw new Exception('Invalid input data');
                }

                $this->callRemoveAddon($assignedProductId, $billingOfferCode, $assignedComponentId);

            } else {
                throw new Exception('Invalid request');
            }
        } catch (Exception $e) {
            $error = true;
            if (!empty($e->response)) {
                $errorResponse = $e->response;
                $serviceError = $errorResponse['body']['fault'];
                $serviceError['operation'] = !empty($errorResponse['header']['operation'])
                    ? $errorResponse['header']['operation'] : "";
                $serviceError['request_id'] = !empty($errorResponse['header']['request_id'])
                    ? $errorResponse['header']['request_id'] : "";
            } else {
                $serviceError = $this->__($e->getMessage());
            }
        }

        $block->setServiceError($serviceError)
            ->setTemplate('inlife/partials/service_error.phtml');

        $result = [
            'error' => $error,
            'html' => $block->toHtml()
        ];

        return $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }

    /**
     * Execute the service call to remove an addon
     *
     * @param $assignedProductId
     * @param $billingOfferCode
     * @param $assignedComponentId
     * @param bool $simulateOnly - simulate only
     * @param bool $returnEligibleAddons - also return eligible addons
     *
     * @return mixed
     * @throws Exception
     */
    private function callRemoveAddon(
        $assignedProductId,
        $billingOfferCode,
        $assignedComponentId,
        $simulateOnly = false,
        $returnEligibleAddons = false
    )
    {
        $this->validateCustomerCtn($assignedProductId);

        $addons[] = [
            'billing_offer_code' => $billingOfferCode,
            'assigned_component_id' => $assignedComponentId,
        ];

        /** @var Vznl_Inlife_Model_Client_InlifeClient $client */
        $client = $this->getInlifeClient();
        session_write_close(); //release session lock

        return $client->removeInlifeAddon($assignedProductId, $simulateOnly, $returnEligibleAddons, $addons);
    }

    /**
     * @param Mage_Core_Block_Abstract $block
     * @param array $response
     */
    protected function processCallResponse($block, $response)
    {
        if (isset($response['quotation_information'])) {
            $block->setData('quotation_information', $response['quotation_information']);
        } elseif (isset($response['quotationinformation'])) {
            $block->setData('quotation_information', $response['quotationinformation']);
        }

        if (isset($response['changed_pricings'])) {
            if (isset($response['changed_pricings']['catalog_pricing'])) {
                $response['changed_pricings'] = [$response['changed_pricings']];
            }
            $addedAddons = $removedAddons = [];
            foreach ($response['changed_pricings'] as $item) {
                if ($item['action_type'] == 'AD') {
                    $addedAddons[] = $item;
                } elseif ($item['action_type'] == 'RM') {
                    $removedAddons[] = $item;
                }
            }
            $block->setData('added_addons', $addedAddons);
            $block->setData('removed_addons', $removedAddons);
        }
    }

    /**
     * Initializing layout messages by message storage(s), loading and adding messages to layout messages block
     *
     * @param string|array $messagesStorage
     * @return Vznl_Inlife_IndexController
     */
    protected function _initLayoutMessages($messagesStorage)
    {
        if (!is_array($messagesStorage)) {
            $messagesStorage = [$messagesStorage];
        }
        foreach ($messagesStorage as $storageName) {
            $storage = Mage::getSingleton($storageName);
            if ($storage) {
                $block = $this->getLayout()->getMessagesBlock();
                $block->addMessages($storage->getMessages(true));
                $block->setEscapeMessageFlag($storage->getEscapeMessages(true));
                $block->addStorageType($storageName);
            } else {
                Mage::throwException(
                    Mage::helper('core')->__('Invalid messages storage "%s" for layout messages initialization', (string)$storageName)
                );
            }
        }
        return $this;
    }

    /**
     * Save the getAddons results to local cache
     *
     * @param array $addons
     * @param string $type
     * @param string $ctnCode - customer ctn code
     */
    protected function setCurrentSessionAddons($addons, $type, $ctnCode)
    {
        try {
            $key = sprintf('%s_%s_%s_%s', Mage::getSingleton('core/session')->getEncryptedSessionId(), $this->customerSession->getCustomer()->getBan(), $ctnCode, 'inlife_' . $type . 'Addons');
            Mage::app()->getCache()->save(serialize($addons), $key, [Dyna_Cache_Model_Cache::PRODUCT_TAG]);
        } catch (Exception $e) {
            $output = $e->getCode() . ': ' . $e->getMessage();
            Mage::logException($output);
        }
    }

    /**
     * Retrieve addons from local cache
     *
     * @param string $type
     * @param null|string $code - Addon code, if only one specific, addon is needed
     * @param null|string $ctnCode
     *
     * @return null|string
     */
    public function getCurrentSessionAddons($type = 'removable', $code = null, $ctnCode = null)
    {
        $key = sprintf('%s_%s_%s_%s', Mage::getSingleton('core/session')->getEncryptedSessionId(), $this->customerSession->getCustomer()->getBan(), $ctnCode, 'inlife_' . $type . 'Addons');

        if ($eligibleAddons = Mage::app()->getCache()->load($key)) {
            if (!$code) {
                return unserialize($eligibleAddons);
            } else {
                $addons = unserialize($eligibleAddons);
                foreach ($addons as $addon) {
                    if ($addon['addon_options'][0]['catalog_pricing_code'] == $code) {
                        return $addon;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Verify if the CTN is assigned to the current customer
     *
     * @param $ctnCode
     *
     * @return Omnius_Ctn_Model_Ctn
     *
     * @throws Exception
     */
    protected function validateCustomerCtn($ctnCode)
    {
        if (!trim($ctnCode)) {
            throw new Exception('Missing data');
        }

        // Check CTN belongs to customer
        $customerCtn = $this->currentCustomer->getCustomerCtn(null, $ctnCode);

        if (!$customerCtn) {
            throw new Exception('Invalid CTN');
        }

        return $customerCtn;
    }

    /**
     * Get inlife service call client
     *
     * @return Vznl_Inlife_Model_Client_InlifeClient
     */
    private function getInlifeClient()
    {
        /** @var Vznl_Inlife_Helper_Data $helper */
        $helper = Mage::helper('vznl_inlife/data');
        return $helper->getClient('inlife', ['options' => array('soap_version' => SOAP_1_2)]);
    }

    /**
     * @param array $vom
     *
     * @return array
     */
    private function _validateVomData($vom)
    {
        $addresses = ['address' => 'fixed_address'];
        $errors = [];

        if ($vom['porting'] == 1) {
            $addresses['contract_address'] = 'address_provider';
        }

        /*// TODO: enable data validations
        foreach ($addresses as $addressNode => $addressType) {
            $vomAddress = $vom[$addressNode];
            $address = $vom[$addressType][$vomAddress];
            $address['address'] = strtolower(preg_replace('/([A-Z])/', '_$1', $vomAddress));

            $addressErrors = Mage::helper('dyna_checkout')->_validateDeliveryAddressData($address, 'fixed[' . $index . '][' . $addressType . '][' . $vomAddress . '][');
            $errors = array_merge($errors, $addressErrors);
        }*/

        return $errors;
    }

    /**
     * Fetches the replacement numbers for change MSISDN action
     *
     * @return void|Zend_Controller_Response_Abstract
     */
    public function retrieveNumberListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $lastCtn = $this->getRequest()->getPost('last_ctn');
                // Set successful response
                $result['error'] = false;
                $result['numbers'] = Mage::helper('vznl_checkout')->getAllAvailablePhoneNumbers($lastCtn);
                return $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if (Mage::helper('dyna_service')->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                return $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }
        }
    }

    /**
     * @return void|Zend_Controller_Response_Abstract
     */
    public function updateProductSettingsAction()
    {
        $logFile = 'inlife_updateproductsettings.log';
        $request = $this->getRequest();
        $block = $this->getLayout()->createBlock('core/template');
        $serviceError = false;
        $error = false;
        $updateSettings = false;

        if ($request->isPost()) {
            try {
                $assignedProductId = $this->customerSession->getAssignedProductId();
                $newCtn = $request->getPost('new_ctn');
                $currentCtn = $request->getPost('current');
                $simulateOnly = $request->getPost('simulate');
                $result['error'] = false;

                $client = $this->getInlifeClient();
                Mage::log(sprintf('Starting update product settings for: %s', $assignedProductId), null, $logFile);

                if ($simulateOnly) {
                    Mage::log('Simulate only', null, $logFile);
                    session_write_close(); //release session lock
                    // Map product settings to update method
                    $settings = $client->getInlifeProductSettings($assignedProductId);
                    if ($settings && is_array($settings)) {
                        $settings = isset($settings['components_with_settings'])
                            ? $settings['components_with_settings'] : null;
                        if ($settings && is_array($settings)) {
                            foreach ($settings as $setting) {
                                if ($setting['component_identifier']['catalog_code'] == 'Primary_MSISDN') {
                                    $assignedId = $setting['component_identifier']['assigned_id'];
                                    $assignedParentId = $setting['component_identifier']['assigned_parent_id'];
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    $assignedId = $request->getPost('assignedId');
                    $assignedParentId = $request->getPost('assignedParentId');
                }

                Mage::log(sprintf('AssignedId: %s, AssignedProductId: %s', $assignedId, $assignedParentId), null, $logFile);
                if ($assignedId && $assignedParentId) {
                    session_write_close(); //release session lock
                    $updateSettings = $client->updateInlifeProductSettings(
                        $assignedProductId,
                        $assignedId,
                        $assignedParentId,
                        $newCtn,
                        $simulateOnly,
                        $returnEligibleSettings = false
                    );

                    if ($simulateOnly == 'true' && $updateSettings) {
                        if (isset($updateSettings['quotation_information'])) {
                            $block->setData('quotation_information', $updateSettings['quotation_information']);
                        } elseif (isset($updateSettings['quotationinformation'])) {
                            $block->setData('quotation_information', $updateSettings['quotationinformation']);
                        }
                        $block->setCurrent($currentCtn);
                    } elseif ($updateSettings) {
                        // Success = display confirmation in page, update ctn value in local DB (and on ILS overview)
                        $ctnModel = Mage::getModel('ctn/ctn')->loadCtn($currentCtn, $this->currentCustomer->getId());
                        if ($ctnModel->getId()) {
                            $ctnModel->setCtn($newCtn)->save();

                            //Let's send an e-mail to client, telling him that the subscription has canged
                            $payload = Mage::helper('communication/email')->getIlsChangeNumberPayload($this->currentCustomer, $newCtn, $updateSettings);
                            /** @var Vznl_Communication_Model_Pool $mailPoll */
                            $mailPoll = Mage::getSingleton('communication/pool');
                            $mailPoll->add($payload);
                        }
                    }
                }
            } catch (Exception $e) {
                $error = true;
                if (!empty($e->response)) {
                    $errorResponse = $e->response;
                    $serviceError = $errorResponse['body']['fault'];
                    $serviceError['operation'] = isset($errorResponse['header']['operation'])
                        ? $errorResponse['header']['operation'] : '';
                    $serviceError['request_id'] = isset($errorResponse['header']['request_id'])
                        ? $errorResponse['header']['request_id'] : '';
                    $message = $this->__($e->getMessage());
                } else {
                    $message = $this->__($e->getMessage());
                }
            }

            $block->setServiceError($serviceError);
            $block->setTemplate('inlife/partials/quote_ctn.phtml');
            $block->setData([
                'current' => $currentCtn,
            ]);

            $result = [
                'error' => $error,
                'html' => $message ?: $block->toHtml()
            ];

            if ($simulateOnly && $updateSettings) {
                $result['data'] = [
                    'assignedId' => $assignedId,
                    'assignedParentId' => $assignedParentId,
                    'current' => $currentCtn
                ];
            }

            return $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        }

        //return;
    }

    /**
     * Change sim action
     *
     * @return void
     */
    public function editSimAction()
    {
        if ($this->getRequest()->isPost()) {
            $message = '';
            $error = false;
            $serviceError = false;
            $currentSims = array();

            // Render the block that will contain the data and send it as response
            $block = $this->getLayout()->createBlock(
                'Mage_Core_Block_Template',
                'inlife.index',
                array('template' => 'inlife/partials/edit_sim.phtml')
            );
            $block->setData($this->getRequest()->getPost());
            $currentCustomer = $this->_getCustomerSession()->getCustomer();
            $ctn = $this->getRequest()->get('ctn');
            $ctn = Mage::getModel('ctn/ctn')->loadCtn($ctn, $currentCustomer->getId());
            try {
                // Check CTN belongs to customer
                $this->validateCustomerCtn($ctn->getCode());

                $client = $this->getInlifeClient();
                $assignedProductDetails = $client->getAssignedInlifeProductDetails($ctn->getCode());
                $currentSims = Mage::helper('vznl_configurator/inlife')->inlifeFetchCurrentSim($assignedProductDetails);
            } catch (Exception $e) {
                if (!empty($e->response)) {
                    $errorResponse = $e->response;
                    $serviceError = isset($errorResponse['body']['fault']) ? $errorResponse['body']['fault'] : '';
                    $serviceError['operation'] = isset($errorResponse['header']['operation'])
                        ? $errorResponse['header']['operation'] : '';
                    $serviceError['request_id'] = isset($errorResponse['header']['request_id'])
                        ? $errorResponse['header']['request_id'] : '';
                } else {
                    $error = true;
                    $serviceError = $this->__($e->getMessage());
                }
            }
            $block->setCurrentSims($currentSims);
            $block->setServiceError($serviceError);

            $result = array(
                'error' => $error,
                'html' => $block->toHtml(),
                'message' => $message
            );
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        }
       // return;
    }

    /**
     * Order polling
     *
     * @return void|Zend_Controller_Response_Abstract
     */
    public function orderPollingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $id = trim($request->getPost('polling_id'));
                if (!$id) {
                    $result = array(
                        'error' => false,
                        'performed' => true
                    );
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }

                /** @var Vznl_Job_Model_Pool $resultsPool */
                $resultsPool = Mage::getSingleton('job/pool');
                $response = $resultsPool->getResult($id);
                if ($response === null) {
                    $result = array(
                        'error' => false,
                        'performed' => false
                    );
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                } elseif ($response === false) {
                    $result = array(
                        'error' => true,
                        'message' => $this->__('There is no such job')
                    );
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }

                $result = array(
                    'error' => false,
                    'performed' => true
                );

                if (Mage::helper('vznl_agent')->isTelesalesLine()
                    || Mage::helper('vznl_agent')->isIndirectStore()
                ) {
                    // For Telesales and Indirect channels, use the SIM from processOrderResponse in the orderSim call
                    if (isset($response['order_result']['sales_ordernumber'])) {
                        $result['sim'] = $this->_fetchSimFromProcessOrderResult($response);
                    }
                }

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                Mage::helper('dyna_service')->returnServiceError($e);
                return;
            }
        }
    }

    /**
     * @param $result
     * @return array
     */
    private function _fetchSimFromProcessOrderResult($result)
    {
        $sims = array();
        if (isset($result['order_result']['packages'])) {
            $package = $result['order_result']['packages']['package'];
            $packageLines = $package['package_lines']['package_line'];
            if (isset($packageLines['linenumber'])) {
                $packageLines = array($packageLines);
            }
            foreach ($packageLines as $packageLine) {
                if (isset($packageLine['product_references']['product_reference']['reference_type'])) {
                    $reference = $packageLine['product_references']['product_reference'];
                    if (!empty($reference['reference_type']) && $reference['reference_type'] == 'SIM') {
                        $sims[] = $reference['reference_i_d'];
                    }
                }
            }
        }

        return $sims;
    }

        /**
     * SIM Swap action
     *
     * @return void|Zend_Controller_Response_Abstract
     */
    public function simSwapAction()
    {
        $block = $this->getLayout()->createBlock('core/template');
        $serviceError = false;
        $error = false;
        $pollingId = null;
        try {
            if ($this->getRequest()->isPost()) {
                /**
                 * Call changeInlifeSim
                 */
                $currentCustomer = $this->_getCustomerSession()->getCustomer();

                $simNumber = preg_replace('/[^0-9]/', '', $this->getRequest()->getPost('current_simcard'));
                $newSimNumber = preg_replace('/[^0-9]/', '', $this->getRequest()->getPost('new_sim_number'));
                $contactId = $currentCustomer->getContactId(); //ContactID is the ID of the customer contact created in Amdocs CRM.
                $orderId = $this->getRequest()->getPost('order_id');
                if (empty($contactId)) {
                    throw new Exception('Customer Contact ID is empty');
                }
                $assignedProductId = $this->_getCustomerSession()->getAssignedProductId(); // ctn_code for the current ctn
                $ctnObj = $this->validateCustomerCtn($assignedProductId);
                $serviceId = $ctnObj->getCtn(); // service id is the customer CTN (optional)

                $this->setJobAdditionalInfo();
                $client = $this->getInlifeClient();
                $websiteCode = Mage::app()->getWebsite()->getCode();
                if ($websiteCode == Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE) {
                    // For Hawaii call orderSim
                    // todo: determine $componentCode for Hawaii. $parentAssignedProductId is optional
                    $client->orderInlifeSim($assignedProductId, $newSimNumber, $componentCode = 'SIM_Card', $serviceId, $parentAssignedProductId = null);
                } elseif (Mage::helper('vznl_agent')->isIndirectStore()) {
                    $activateNow = $this->getRequest()->getPost('activation');
                    if ($activateNow == 1) {
                        $client->changeInlifeSim($assignedProductId, $simNumber, $newSimNumber, $contactId);
                    } else {
                        $pollingId = $client->orderInlifeSim($assignedProductId, $newSimNumber, $componentCode = 'SIM_Card', $serviceId, $parentAssignedProductId = null);
                    }
                } else {
                    // Online simSwap handling
                    if ($orderId) {
                        $this->updateSimSwapOrder($orderId, $assignedProductId, $serviceId, $simNumber, $newSimNumber, $contactId);
                    }
                }
            } else {
                throw new Exception('Invalid request');
            }
        } catch (Exception $e) {
            $error = true;
            if (!empty($e->response)) {
                $errorResponse = $e->response;
                $serviceError = $errorResponse['body']['fault'];
                $serviceError['operation'] = isset($errorResponse['header']['operation'])
                    ? $errorResponse['header']['operation'] : '';
                $serviceError['request_id'] = isset($errorResponse['header']['request_id'])
                    ? $errorResponse['header']['request_id'] : '';
            } else {
                $serviceError = $this->__($e->getMessage());
            }
        }

        $block->setServiceError($serviceError)
            ->setOverviewButton(true)
            ->setTemplate('inlife/partials/service_error.phtml');

        $result = array(
            'error' => $error,
            'html' => $block->toHtml()
        );

        if ($pollingId) {
            $result['polling_id'] = $pollingId;
        }

        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
     //   return;
    }

    /**
     * @param $orderId
     * @param $assignedProductId
     * @param $serviceId
     * @param $simNumber
     * @param $newSimNumber
     * @param $contactId
     *
     * @throws Exception
     */
    protected function updateSimSwapOrder($orderId, $assignedProductId, $serviceId, $simNumber, $newSimNumber, $contactId)
    {
        $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderId);
        if ($superOrder->getId()) {
            // For Telesales and Indirect we call orderSim with the SIM returned by processOrder
            if (Mage::helper('agent')->isTelesalesLine()) {
                // For Telesales channels we call orderSim
                $info = serialize(array(
                    'dealer_code' => $this->_getCustomerSession()->getAgent(true)->getDealer()->getVfDealerCode(),
                    'hardware_order_id' => $superOrder->getId(),
                    'method' => 'orderInlifeSim',
                    'arguments' => array($assignedProductId, 'SIM', $componentCode = 'SIM_Card', $serviceId, $parentAssignedProductId = null)
                ));

                $superOrder->setXmlToBeSent($info)
                    ->setIsIls(true)
                    ->save();
            } else {
                // For Retail channels we call changeSim
                $info = serialize(array(
                    'dealer_code' => $this->_getCustomerSession()->getAgent(true)->getDealer()->getVfDealerCode(),
                    'hardware_order_id' => $superOrder->getId(),
                    'method' => 'changeInlifeSim',
                    'arguments' => array($assignedProductId, $simNumber, $newSimNumber, $contactId)
                ));

                $superOrder->setXmlToBeSent($info)
                    ->setIsIls(true)
                    ->save();
            }
        } else {
            throw new Exception(sprintf('Order with number %s not found', $orderId));
        }
    }

    /**
     * SIM Swap save action
     *
     * @return void|Zend_Controller_Response_Abstract
     * @throws Exception
     */
    public function createSimSwapOrderAction()
    {
        if (!$this->getRequest()->isPost()) {
            throw new Exception('Invalid request');
        }

        $block = $this->getLayout()->createBlock('core/template');
        $serviceError = false;
        $error = false;
        $orderResult = array(
            'polling_id' => null,
            'order_id' => null,
        );
        try {
            $contactId = $this->_getCustomerSession()->getCustomer()->getContactId(); //ContactID is the ID of the customer contact created in Amdocs CRM.
            if (empty($contactId)) {
                throw new Exception('Customer Contact ID is empty');
            }
            $products = array($this->getRequest()->getPost('sim_id'));
            $payment = $this->getRequest()->getPost('payment');
            $paymentMethod = $payment['method'];

            $delivery = $this->getRequest()->getPost('delivery');
            $deliveryMethod = $delivery['method'];
            $shippingAdrress = null;
            $orderResult = $this->createSimOrderInlife($products, $delivery, $deliveryMethod, $paymentMethod);
        } catch (Exception $e) {
            $error = true;
            if (!empty($e->response)) {
                $errorResponse = $e->response;
                $serviceError = $errorResponse['body']['fault'];
                $serviceError['operation'] = $errorResponse['header']['operation'];
                $serviceError['request_id'] = $errorResponse['header']['request_id'];
            } else {
                $serviceError = $this->__($e->getMessage());
            }
        }

        $block->setServiceError($serviceError)
            ->setOverviewButton(true)
            ->setTemplate('inlife/partials/service_error.phtml');

        $result = array(
            'error' => $error,
            'polling_id' => $orderResult['polling_id'],
            'order_id' => $orderResult['order_id'],
            'html' => $block->toHtml()
        );

        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
       // return;
    }

    /**
     * Create Sim Order
     *
     * @param array $products
     * @param array $delivery
     * @param string $deliveryMethod
     * @param string $paymentMethod
     * @return array|bool
     * @throws Exception
     */
    protected function createSimOrderInlife($products, $delivery, $deliveryMethod, $paymentMethod)
    {
        switch ($deliveryMethod) {
            case 'direct':
                $addressData = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();

                $shippingAddress = Mage::helper('dyna_checkout')->_getAddressByType(array(
                    'address' => 'store',
                    'store_id' => $addressData,
                ));

                break;
            case 'deliver':
                if (!isset($delivery['deliver']['address'])) {
                    throw new Exception($this->__('Missing shipping address info'));
                }

                $shippingAddress = Mage::helper('dyna_checkout')->_getAddressByType(array(
                    'address' => $delivery['deliver']['address'],
                    'billingAddress' => !empty($delivery['billingAddress']) ? $delivery['billingAddress'] : [],
                    'otherAddress' => $delivery['otherAddress'],
                ));

                break;
            case 'virtual':
                $customerSession = $this->_getCustomerSession();
                $dealer = $customerSession->getAgent(true)->getDealer();
                $shippingAddress = Mage::helper('dyna_checkout')->getVirtualDeliveryAddress($dealer);
                break;
        }

        try {
            $orderResult = $this->createSimOrder($products, $paymentMethod, $shippingAddress);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            throw $e;
        }

        if (!is_array($orderResult) || empty($orderResult)) {
            throw new Exception('Could not create processOrder request');
        }
        return $orderResult;
    }

    /**
     * Create the hardware only order locally and call processCustomerOrder
     *
     * @param array $products
     * @param string $paymentMethod
     * @param array|null $shippingAdrress
     * @return array|bool
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    protected function createSimOrder($products, $paymentMethod, $shippingAdrress = null)
    {
        $customerObj = $this->_getCustomerSession()->getCustomer();

        // assign this customer to quote object, before any type of magento order, first create quote.
        $quoteObj = Mage::getModel('sales/quote')->assignCustomer($customerObj);
        $quoteObj = $quoteObj->setStoreId(Mage::app()->getStore()->getId());
        $quoteObj->setActivePackageId(1);
        $checksum = array();
        foreach ($products as $simId) {
            array_push($checksum, $simId);
            // add sim product to quote
            $productModel = Mage::getModel('catalog/product');
            $productObj = $productModel->load($simId);
            $quoteObj->addProduct($productObj, 1);
        }

        // set shipping address to quote
        $quoteShippingAddress = new Mage_Sales_Model_Quote_Address();

        $customerShippingAddressId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultShipping();
        $customer_address = Mage::getModel('customer/address')->load($customerShippingAddressId);

        if (!$shippingAdrress) {
            $quoteShippingAddress->setData($customer_address->getData());
            $shippingData = array(
                "street" => $customer_address->getStreet(),
                "postcode" => $customer_address->getPostcode(),
                "city" => $customer_address->getCity(),
                "store_id" => null,
                "address" => "other_address"
            );
        } else {
            if (!isset($shippingAdrress['store_id'])) {
                $shippingAdrress['store_id'] = null;
            }
            $shippingData = $shippingAdrress;
            if (is_array($shippingAdrress['street'])) {
                $tmp = $shippingAdrress['street'];
                $shippingAdrress['street'] = implode("\n", $tmp);
            }

            $quoteShippingAddress->setData($shippingAdrress);
        }

        // set billing address to quote
        $customerBillingAddressId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBilling();
        if ($customerShippingAddressId != $customerBillingAddressId) {
            $customer_address = Mage::getModel('customer/address')->load($customerBillingAddressId);
        } else {
            if (!$shippingAdrress) {
                $shippingData['address'] = 'billing_address';
            }
        }

        $shippingData = array(
            'deliver' => array('address' => $shippingData),
            'payment' => $paymentMethod
        );

        $quoteObj->setShippingData(json_encode($shippingData));
        $quoteObj->setShippingAddress($quoteShippingAddress);

        $quoteBillingAddress = new Mage_Sales_Model_Quote_Address();
        $quoteBillingAddress->setData($customer_address->getData());
        $quoteObj->setBillingAddress($quoteBillingAddress);
        $quoteObj->setTotalsCollectedFlag(true);
        $quoteObj->save();

        // create package
        $package = Mage::getModel('package/package')
            ->setPackageId(1)
            ->setType(Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE)
            ->setSaleType(Vznl_Checkout_Model_Sales_Quote_Item::INLIFE)
            ->setQuoteId($quoteObj->getId())
            ->setStatus(Dyna_Package_Model_Package::ESB_PACKAGE_STATUS_INITIAL)
            ->setCurrentStatus(Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
            ->setCreditcheckStatus(Dyna_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL)
            ->setChecksum(implode(',', $checksum))
            ->save();

        $items = $quoteObj->getAllItems();

        foreach ($items as $item) {
            $item->setPackageId(1)
                ->setPackageType(Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE)
                ->setSaleType(Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE)
                ->save();
        }

        $quoteObj->setTotalsCollectedFlag(false);
        $quoteObj->collectTotals();
        $quoteObj->save();

        // handle order creation
        $transaction = Mage::getModel('core/resource_transaction');
        if ($quoteObj->getCustomerId()) {
            $transaction->addObject($quoteObj->getCustomer());
        }
        $transaction->addObject($quoteObj);
        $quoteObj->reserveOrderId();
        // set payment method
        $quotePaymentObj = $quoteObj->getPayment();
        $quotePaymentObj->setMethod($paymentMethod);
        $quoteObj->setPayment($quotePaymentObj);

        $convertQuoteObj = Mage::getSingleton('sales/convert_quote');
        $orderObj = $convertQuoteObj->addressToOrder($quoteObj->getShippingAddress());
        $orderShippingAddress = $convertQuoteObj->addressToOrderAddress($quoteObj->getShippingAddress());
        $orderShippingAddress->setData('delivery_store_id', $shippingData['deliver']['address']['store_id']);
        $orderShippingAddress->setData('delivery_type', $shippingData['deliver']['address']['address']);
        $orderObj->setBillingAddress($convertQuoteObj->addressToOrderAddress($quoteObj->getBillingAddress()));
        $orderObj->setShippingAddress($orderShippingAddress);
        $orderObj->setPayment($convertQuoteObj->paymentToOrderPayment($quoteObj->getPayment()));

        foreach ($items as $item) {
            //@var $item Mage_Sales_Model_Quote_Item
            $orderItem = $convertQuoteObj->itemToOrderItem($item);
            if ($item->getParentItem()) {
                $orderItem->setParentItem($orderObj->getItemByQuoteItemId($item->getParentItem()->getId()));
            }
            $orderObj->addItem($orderItem);
        }

        $orderObj->setCanShipPartiallyItem(false);
        $packageTotalsTax = Mage::helper('dyna_configurator')->getActivePackageTotals($package->getPackageId(), true, $quoteObj->getId());
        // create new superorder
        $superOrder = Mage::getModel('superorder/superorder')->createNewSuperorder();
        $superOrder->setIsIls(true)->save();

        $orderObj->setSuperorderId($superOrder->getId());
        $orderObj->setEdited(0);

        $package->setOrderId($superOrder->getId())
            ->setQuoteId(null)
            ->save();
        $transaction->addObject($orderObj);
        $transaction->addCommitCallback(array($orderObj, 'place'));
        $transaction->addCommitCallback(array($orderObj, 'save'));

        try {
            $transaction->save();
        } catch (Exception $e) {
            Mage::throwException('Cannot create hardware order.');
        }
        $quoteObj->setIsActive(0);
        $quoteObj->save();

        //calling API for update order in elastic search
        $searchHelper = Mage::helper('vznl_customer/search');
        $searchHelper->updateOrderNumberApiCall($superOrder, $superOrder->getCustomer());

        /** @var Vznl_Service_Helper_Data $serviceHelper */
        $serviceHelper = Mage::helper('vznl_service');

        /**
         * Call CreateOrder
         */
        try {
            /** @var Vznl_Service_Model_Client_EbsOrchestrationClient $client */
            $client = $serviceHelper->getClient('ebs_orchestration');

            return array(
                'polling_id' => $client->processCustomerOrder($superOrder, null, null, null, null, null, null, null, null, [$package->getPackageId() => $packageTotalsTax['totalprice']]),
                'order_id' => $superOrder->getOrderNumber()
            );
        } catch (\Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            throw $e;
        }
    }

    /**
     * Create Mult Sim Order
     *
     * @return json
     */
    public function createMultisimOrderAction()
    {
        $block = $this->getLayout()->createBlock('core/template');
        $serviceError = false;
        $error = false;
        $orderResult = array(
            'polling_id' => null,
            'order_id' => null,
        );
        try {
            $request = $this->getRequest();

            if ($request->isPost() && $request->getPost('multisim')) {

                $multisimData = $request->getPost('multisim');
                $multisimAddons = $multisimData['addon'];
                $products = array();

                foreach ($multisimAddons as $addon_billing_offer_code) {
                    $products[] = $multisimData['simcard'][$addon_billing_offer_code];
                }

                $payment = $request->getPost('payment');
                $paymentMethod = $payment['method'];
                $shippingAdrress = null;

                $deliveryMethod = $multisimData['delivery'];
                $orderResult = $this->createSimOrderInlife($products, $multisimData, $deliveryMethod, $paymentMethod);
            } else {
                throw new Exception('Invalid request');
            }
        } catch (Exception $e) {
            $error = true;
            if (!empty($e->response)) {
                $errorResponse = $e->response;
                $serviceError = $errorResponse['body']['fault'];
                $serviceError['operation'] = $errorResponse['header']['operation'];
                $serviceError['request_id'] = $errorResponse['header']['request_id'];
            } else {
                $serviceError = $this->__($e->getMessage());
            }
        }

        $block->setServiceError($serviceError)
            ->setOverviewButton(true)
            ->setTemplate('configurator/inlife/partials/service_error.phtml');

        $result = array(
            'error' => $error,
            'polling_id' => $orderResult['polling_id'],
            'order_id' => $orderResult['order_id'],
            'html' => $block->toHtml()
        );

        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
        return;
    }

    private function setJobAdditionalInfo()
    {
        $additionalInfo = Mage::registry('job_additional_information') ?? [];
        $additionalInfo['dealer_code'] = $this->_getCustomerSession()->getAgent(true)->getDealer()->getVfDealerCode();
        Mage::register('job_additional_information', $additionalInfo);
    }
}
