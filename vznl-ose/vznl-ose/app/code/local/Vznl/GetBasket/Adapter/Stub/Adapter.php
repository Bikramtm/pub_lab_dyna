<?php

/**
 * Class Vznl_GetBasket_Adapter_Adapter
 */
class Vznl_GetBasket_Adapter_Stub_Adapter
{
    CONST NAME = 'GetBasket';
    /**
     * @param $lastName
     * @param $addressId
     * @return $responseData
     */
    public function call($customerId = null, $targetAddressId = null, $crossFootPrint = null, $orderType = null)
    {
        $stubClient = Mage::helper('vznl_getbasket')->getStubClient();
        $stubClient->setNamespace('Vznl_GetBasket');
        $arguments = array($customerId, $targetAddressId, $crossFootPrint, $orderType);
        $responseData = $stubClient->call(self::NAME, $arguments);
        Mage::helper('vznl_getbasket')->transferLog($arguments, $responseData, self::NAME);
        return $responseData;
    }
}
