<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Vznl_GetBasket_Adapter_Peal_Adapter
 */
class Vznl_GetBasket_Adapter_Peal_Adapter
{
    CONST name = "GetBasket";
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $cty;

    /**
     * @var string
     */
    private $executionTimeId;

    /**
     * @var string
     */
    private $requestTimeId;

    /**
     * Adapter constructor.
     *
     * @param Client $client
     * @param string $endpoint
     * @param string $channel
     * @param string $cty
     */
    public function __construct(Client $client, string $endpoint, string $channel, string $cty)
    {
        $this->endpoint = $endpoint;
        $this->channel = $channel;
        $this->cty = $cty;
        $this->client = $client;
    }

    /**
     * @param $customerId
     * @param $targetAddressId
     * @param $crossFootPrint
     * @param $orderType
     * @return string|array
     * @throws GuzzleException
     */
    public function call($customerId = null, $targetAddressId = null, $crossFootPrint = null, $orderType = null)
    {
        $this->executionTimeId = $this->getHelper()->initTimeMeasurement();

        try {
            $userName = $this->getHelper()->getLogin();
            $password = $this->getHelper()->getPassword();
            $verify = $this->getHelper()->getVerify();
            $proxy = $this->getHelper()->getProxy();
            $requestLog['url'] = $this->getUrl($customerId, $targetAddressId, $crossFootPrint, $orderType);
            if ($orderType == 'PRODUCT_ORDER') {
                $requestLog['body']=['customerId'=>$customerId, 'orderType'=>$orderType];
            } else {
                $requestLog['body']=['customerId'=>$customerId, 'moveAddressId'=>$targetAddressId, 'crossFootPrint'=>$crossFootPrint, 'orderType'=>$orderType];
            }

            $this->requestTimeId = $this->getHelper()->initTimeMeasurement();

            if ($userName != '' && $password != '') {
                $response = $this->client->request('GET', $this->getUrl($customerId, $targetAddressId, $crossFootPrint, $orderType), ['auth' => [$userName, $password], 'verify' => $verify, 'proxy' => $proxy]);
            } else {
                $response = $this->client->request('GET', $this->getUrl($customerId, $targetAddressId, $crossFootPrint, $orderType), ['verify' => $verify, 'proxy' => $proxy]);
            }

            $this->getHelper()->transferLog(
                json_encode($requestLog),
                json_encode(json_decode($response->getBody())),
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $source = json_decode($response->getBody(), true);
        } catch (ClientException|RequestException $exception) {
            $this->getHelper()->transferLog(
                $exception->getRequest(),
                ['msg' => $exception->getMessage(), 'code' => $exception->getCode()],
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $result['error'] = true;
            $result['message'] = $exception->getMessage();
            $result['code'] = $exception->getCode();
            return $result;
        }
        return $source;
    }

    /**
     * @param $customerId
     * @param $targetAddressId
     * @param $crossFootPrint
     * @param $orderType
     * @return string
     */
    public function getUrl($customerId = null, $targetAddressId = null, $crossFootPrint = null, $orderType = null)
    {
        if ($crossFootPrint) {
            $crossFootPrint = "true";
        } else {
            $crossFootPrint = "false";
        }

        if ($orderType == 'PRODUCT_ORDER') {
            return $this->endpoint . $customerId .
                "?cty=" . $this->cty .
                "&chl=" . $this->channel .
                "&orderType=" . $orderType ;
        } else {
            return $this->endpoint . $customerId .
                "?cty=" . $this->cty .
                "&chl=" . $this->channel .
                "&orderType=" . $orderType .
                "&crossFootPrint=" . (string)$crossFootPrint .
                "&moveAddressId=" . $targetAddressId;
        }
    }

    /**
     * @return object Vznl_GetBasket_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('vznl_getbasket');
    }
}
