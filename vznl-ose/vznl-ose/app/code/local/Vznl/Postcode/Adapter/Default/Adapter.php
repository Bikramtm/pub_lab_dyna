<?php

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Zend\Soap\Client;

/**
 * Class Vznl_Postcode_Adapter_Default_Adapter
 */
class Vznl_Postcode_Adapter_Default_Adapter extends Omnius_Service_Model_Client_Client
{
    /** @var Dyna_Configurator_Model_Cache */
    protected $_cache;

    /**
     * @return Dyna_Configurator_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_configurator/cache');
        }
        return $this->_cache;
    }

    public function send($postcode, $houseNumber)
    {
        // Trim spaces from zipcode as requested in RFC-160607
        $postcode = str_replace(' ', '', $postcode);
        $key = md5(serialize(array($this->getHelper()->getPostcodeConfig('endpoint'), $postcode, $houseNumber)));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        }
        $defaultParams = $this->_getDefaultParams();
        $values = $this->executeRequest('ZCGetLocationByAddress', $this->_getRequestBuilder()
            ->create(
                $this->getNamespace(),
                'GetLocationByAddress',
                array_merge(array('PostCode' => $postcode, 'HouseNumber' => $houseNumber), $defaultParams)
            )
        );
        $response = reset($values);

        try {
            $response = new SimpleXMLElement($this->getAccessor()->getValue($response, 'string.1'));
            $result = $this->getNormalizer()
                ->normalizeKeys(
                    $this->getNormalizer()
                        ->objectToArray($this->object_to_array($response->children())));

            if(!isset($result['success']) || $result['success'] != 'True'){
                Mage::throwException(isset($result['error']['errors']) ? $result['error']['errors'] : $result['error']['message']);
            }

            if (isset($result['list_of_resource']['resource'][0])) {
                $result = $result['list_of_resource']['resource'][0];
            } else {
                $result = $result['list_of_resource']['resource'];
            }

            $this->getCache()->save(serialize($result), $key, array(Dyna_Configurator_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        } catch (Exception $e) {
            $message = "\n" . $e->__toString();
            $message .= "\nMethod: GetPostcodeDetails";
            $message .= "\nArguments: postcode=" . $postcode . ' and house number=' . $houseNumber;
            $message .= "\nResponse: " . json_encode($response);
            $message .= "\nSoap request: " . parent::__getLastRequest();
            $message .= "\nSoap request headers: " . parent::__getLastRequestHeaders();
            $message .= "\nSoap response: " . parent::__getLastResponse();
            $message .= "\nSoap response headers: " . parent::__getLastResponseHeaders();
            $message .= "\n\n\n\n";
            $this->log($message, Zend_Log::CRIT);
            throw $e;
        }

        return $result;
    }

    protected function executeRequest($functionality, $request)
    {
        $params = array(
            'functionalityName' => $functionality,
            'loginName' => $this->getHelper()->getPostcodeConfig('login'),
            'password' => $this->getHelper()->getPostcodeConfig('password'),
            'request' => $request,
        );

        return $this->ExecuteRequestWithLogin($params);
    }

    protected function object_to_array($obj) {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = $this->object_to_array($val);
            }
        }
        else $new = $obj;

        return $new;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return 'Postcode';
    }

    public function getUseStubs(){
        return false;
    }
}
