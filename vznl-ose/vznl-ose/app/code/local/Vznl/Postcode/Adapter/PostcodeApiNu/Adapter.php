<?php

use Dynacommerce\Zipcode\Service\AddressLookup\AddressLookupService;
use Dynacommerce\Zipcode\Address\CountryFactory;
use Dynacommerce\Zipcode\ExternalAdapter\PostcodesNu\Factory;
use Dynacommerce\Zipcode\Address\AddressSpecification;

/**
 * Class Vznl_Postcode_Adapter_PostcodeApiNu_Adapter
 */
class Vznl_Postcode_Adapter_PostcodeApiNu_Adapter
{
    /**
     * @var array
     */
    private $options = [];

    public function __construct(array $options=[])
    {
        $this->options = array_merge($this->options, $options);
    }

    public function send(string $postcode, int $houseNumber): array
    {
        $clientOptions = isset($this->options['client_options']) ? $this->options['client_options'] : [];

        $addressLookupService = new AddressLookupService(new CountryFactory());
        $factory = new Factory();
        $addressLookupService->attachAdapter(1, $factory->create($this->options['api_key'], $clientOptions));
        $spec = new AddressSpecification($this->options['country'], $postcode, $houseNumber);
        $address = $addressLookupService->lookup($spec);

        return array(
            "street" => $address[0]->part('street')->value(),
            "house_number" => $houseNumber,
            "postcode" => $address[0]->postalCode(),
            "town" => $address[0]->part('city')->value()
        );
    }
}
