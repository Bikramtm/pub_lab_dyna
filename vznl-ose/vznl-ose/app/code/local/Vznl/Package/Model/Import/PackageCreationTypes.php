<?php

/**
 * Class Vznl_Package_Model_Import_PackageCreationTypes
 */
class Vznl_Package_Model_Import_PackageCreationTypes extends Dyna_Package_Model_Import_PackageCreationTypes {

    protected $_logFileExtension= "log";
    public $_logFileName = "packagecreation_import";

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper("vznl_import");
    }

    /**
     * Import the creation type and creation group
     *
     * @param $data
     */
    public function importCreationType($data)
    {
        $data = array_combine($this->_header, $data);
        $data['stack'] = $this->stack;
        $this->_totalFileRows++;

        if (!$this->validateRequiredData($data)) {
            $this->_skippedFileRows++;
            return;
        }

        $packageTypeId = Mage::getModel('dyna_package/packageType')
            ->getCollection()
            ->addFieldToFilter('package_code', $data['package_type_id'])
            ->setPageSize(1, 1)
            ->getLastItem()
            ->getId();
        if ($packageTypeId) {
            $creationGroupId = $this->getCreationGroupId($data);

            $packageCreationType = Mage::getModel('dyna_package/packageCreationTypes')
                ->getCollection()
                ->addFieldToFilter('name', $data['package_creation_name'])
                ->addFieldToFilter('gui_name', $data['package_creation_name'])
                ->addFieldToFilter('package_type_code', $data['package_creation_type_id'])
                ->addFieldToFilter('package_type_id', $packageTypeId)
                ->addFieldToFilter('stack', $data['stack'])
                ->setPageSize(1, 1)
                ->getLastItem();

            if(!$packageCreationType->getId()){
                $packageCreationType = Mage::getModel('dyna_package/packageCreationTypes');
            }

            $packageCreationType
                ->setData('name', $data['package_creation_name'])
                ->setData('gui_name', $data['package_creation_name'])
                ->setData('sorting', $data['sorting'])
                ->setData('filter_attributes', $data['filter_attributes'])
                ->setData('package_subtype_filter_attributes', $data['package_subtype_filter_attributes'])
                ->setData('package_type_code', $data['package_creation_type_id'])
                ->setData('package_type_id', $packageTypeId)
                ->setData('package_creation_group_id', $creationGroupId)
                ->setData('stack', $data['stack']);

            if (!empty($data['requires_serviceability'])) {
                $packageCreationType->setData('requires_serviceability', $data['requires_serviceability']);
            }

            /** @var Dyna_Catalog_Model_ProcessContext $processContext */
            $processContextModel = Mage::getModel('dyna_catalog/processContext');
            try {
                // save package creation
                $packageCreationType->save();

                foreach ($processContextModel->getCollection() as $processContext) {
                    $key = 'filter_attributes_' . strtolower($processContext->getCode());

                    if (array_key_exists($key, $data) && !empty($data[$key])) {
                        $packageCreationTypeProcessContext = Mage::getModel('dyna_package/packageCreationTypesProcessContext');
                        $packageCreationTypeProcessContext
                            ->setData('package_creation_type_id', $packageCreationType->getId())
                            ->setData('process_context_id', $processContext->getId())
                            ->setData('filter_attributes', $data[$key]);

                        // save package creation process context
                        $packageCreationTypeProcessContext->save();
                    }
                }
            } catch (Exception $e) {
                $this->_skippedFileRows++;
                //var_dump($e->getMessage());
                $this->_logError($e);
            }
        } else {
            $this->_logError(sprintf("The referenced package type %s was not found in catalog_package_types", $data['package_type_id']));
            $this->_skippedFileRows++;
        }
    }

    public function getFileLogName()
    {
        return strtolower($this->stack) . '_' . $this->_logFileName . '.' . $this->_logFileExtension;
    }
}