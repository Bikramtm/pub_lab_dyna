<?php

/**
 * Class Vznl_Package_Model_Package
 */
class Vznl_Package_Model_Package extends Dyna_Package_Model_Package
{
    const ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION = 'Waiting for Validation';
    const ESB_PACKAGE_STATUS_PORTING_PENDING = 'Porting Pending';
    const ESB_PACKAGE_STATUS_PORTING_REJECTED = 'Porting Rejected';
    const ESB_PACKAGE_STATUS_PORTING_APPROVED = 'Porting Approved';
    const ESB_PACKAGE_STATUS_PORTING_CANCELLED = 'Porting cancelled';
    const ESB_PACKAGE_STATUS_CANCELLED = 'Cancelled';
    const ESB_PACKAGE_STATUS_RETURNED = 'Returned';
    const ESB_PACKAGE_STATUS_PACKAGE_VALIDATED = 'Package Validated';
    const ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED = 'All Resources Reserved';
    const ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT = 'Ready for fulfillment';
    const ESB_PACKAGE_STATUS_FAILED = 'Validation Failed';
    const ESB_PACKAGE_STATUS_DELIVERED = 'Delivered';
    const ESB_PACKAGE_STATUS_NOT_DELIVERED = 'Not delivered';
    const ESB_PACKAGE_STATUS_BACK_ORDER = 'Back Order';

    const ESB_PACKAGE_NETWORK_STATUS_CC_PENDING = 'PENDING';
    const ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL = 'PARTIAL';
    const ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED = 'REFERRED';
    const ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED = 'ADDITIONNAL_INFO_REQUIRED';
    const ESB_PACKAGE_NETWORK_STATUS_CC_REJECTED = 'REJECTED';
    const ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED = 'APPROVED';
    const ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED = "VALIDATION_FAILED";
    const ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL = "INITIAL";

    // Number Porting
    const ESB_PACKAGE_NETWORK_STATUS_NP_PENDING = 'PENDING';
    const ESB_PACKAGE_NETWORK_STATUS_NP_NOT_SOLVABLE = 'NOT_SOLVABLE';
    const ESB_PACKAGE_NETWORK_STATUS_NP_AUTHORIZED = 'APPROVED';
    const ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED = 'REJECTED';
    const ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED_CANCELLED = 'REJECTED & CANCELLED';
    const ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL = 'INITIAL';

    const ESB_PACKAGE_NETWORK_STATUSES_CC_RUNNING = [
        self::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL,
        self::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING,
        self::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED,
        self::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED,
    ];

    const ESB_PACKAGE_NETWORK_STATUSES_NP_RUNNING = [
        self::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL,
        self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING,
    ];

    const NP_VALIDATION_TYPE_SMART_VALIDATION = 'SmartVal';
    const NP_VALIDATION_TYPE_CLIENT_ID = 'CustomerID';

    const NP_VALIDATION_TYPE_API_CLIENT_ID = 'CUSTOMER_NUMBER';

    const PACKAGE_RETURN_GRACEPERIOD = 'graceperiod';
    const PACKAGE_RETURN_SHOP = 'shop';
    const PACKAGE_RETURN_TRANSPORT = 'transport';

    protected $acceptedSections = [
        'credit-checks',
        'number-porting',
        'failed-validations',
        'open-orders',
        'customer-screen-orders',
    ];

    /**
     * return array of acceptd sections
     */
    public function getAcceptedSections()
    {
    	return $this->acceptedSections;
    }

    /**
     * Returns results for Orders Section
     * @param $agentId
     * @param $customerId
     * @param $dealerId
     * @param $section = open-orders
     * @param $websiteId
     * @param $cluster (error|ready|check|other)
     * @param $clusterOrderField (id|mobile_nr|status|package_nr)
     * @param $clusterOrderType (asc|desc)
     * @param $clusterPage
     * @param $clusterResultsPerPage
     * @param $customerScreen
     *
     * @return Omnius_Package_Model_Mysql4_Package_Collection
     *
     * @throws Exception
     */
    public function getOrdersPackages(
        $agentId,
        $customerId,
        $dealerId,
        $section,
        $websiteId,
        $cluster,
        $clusterOrderField,
        $clusterOrderType,
        $clusterPage,
        $clusterResultsPerPage,
        $customerScreen
    ) {
        if (!in_array($section, $this->acceptedSections)) {
            throw new InvalidArgumentException(sprintf('Invalid section provided. Expected one of %s, got %s', join(', ', $this->acceptedSections), $section));
        }
        $collection = $this->getCollection();
        $collection->getSelect()
            ->join(['s' => 'superorder'], 'main_table.order_id = s.entity_id', [
                'super_entity_id' => 'entity_id',
                'super_order_number' => 'order_number',
                'super_order_status' => 'order_status',
                'super_order_created' => 'created_at',
                'super_customer_id' => 'customer_id',
                'super_error_code' => 'error_code',
                'super_error_detail' => 'error_detail',
            ])
            ->join(['c' => 'customer_entity'], 's.customer_id = c.entity_id', [
                'customer_ban' => 'ban',
            ])
            ->join(['o' => 'sales_flat_order'], 's.entity_id = o.superorder_id', [
                'customer_prefix',
                'customer_middle_name' => 'customer_middlename',
                'customer_lastname',
                'customer_firstname',
                'company_name',
                'fixed_telephone_number'
            ])
            ->join(['oa' => 'sales_flat_order_address'], 'o.shipping_address_id = oa.entity_id', [
                'delivery_store_id' => 'delivery_store_id',
            ])
            ->where('main_table.checksum is not null')
            ->where('o.edited = ?', 0)
            ->group('s.entity_id');

        switch ($section) {
            case 'credit-checks' :
                switch ($cluster) {
                    case 'cc-rejected' :
                        $collection->getSelect()->where('main_table.creditcheck_status = ?', self::ESB_PACKAGE_NETWORK_STATUS_CC_REJECTED);
                        break;

                    case 'cc-pending' :
                        $collection->getSelect()
                            ->where('main_table.creditcheck_status IN(?)', [
                                self::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING,
                                self::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED,
                            ])
                            ->where('s.order_status IN (?)', [
                                Vznl_Superorder_Model_Superorder::SO_WAITING_FOR_VALIDATION,
                                Vznl_Superorder_Model_Superorder::SO_STATUS_REFERRED,
                            ]);
                        break;

                    case 'cc-additionalinforequired' :
                        $collection->getSelect()
                            ->where('main_table.creditcheck_status = ?', self::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED)
                            ->where('s.order_status IN (?)', [
                                Vznl_Superorder_Model_Superorder::SO_WAITING_FOR_VALIDATION,
                                Vznl_Superorder_Model_Superorder::SO_ADDITIONAL_INFO_REQUIRED,
                            ]);
                        break;

                    case 'cc-approved' :
                        $collection->getSelect()->where('main_table.creditcheck_status = ?', self::ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED);
                        break;

                    case 'cc-partial' :
                        $collection->getSelect()->where('main_table.creditcheck_status = ?', self::ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL);
                        break;

                    default:
                        throw new Exception('Unknown cluster set for this section');
                }

                if (!$customerScreen) {
                    $collection->getSelect()->where('main_table.creditcheck_status_updated >= ?', $this->getDateLimit());
                }

                break;

            case 'number-porting':
                switch ($cluster) {
                    case 'np-rejectedcancelled':
                        $collection->getSelect()
                            ->where('main_table.porting_status in(?)', [
                                self::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED_CANCELLED,
                                self::ESB_PACKAGE_NETWORK_STATUS_NP_NOT_SOLVABLE,
                            ]);
                        break;

                    case 'np-rejected' :
                        $collection->getSelect()->where('main_table.porting_status = ?', self::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED);
                        break;

                    case 'np-pending':
                        $collection->getSelect()->where('main_table.porting_status = ?', self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING);
                        break;

                    case 'np-approved' :
                        $collection->getSelect()->where('main_table.porting_status = ?', self::ESB_PACKAGE_NETWORK_STATUS_NP_AUTHORIZED);
                        break;

                    default:
                        throw new Exception('Unknown cluster set for this section');
                }

                if (!$customerScreen) {
                    $collection->getSelect()->where('main_table.porting_status_updated >= ?', $this->getDateLimit());
                }

                break;

            case 'failed-validations' :
                switch ($cluster) {
                    case 'fv-declined' :
                        $collection->getSelect()->where('main_table.creditcheck_status <> ?', self::ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED);
                        break;

                    case 'fv-failed' :
                        $collection->getSelect()->where('main_table.creditcheck_status = ?', self::ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED);
                        break;

                    default :
                        throw new Exception('Unknown cluster set for this section');
                }

                if (!$customerScreen) {
                    $collection->getSelect()->where('s.entity_id >= (SELECT MIN(entity_id) FROM superorder WHERE created_at >= ?)', $this->getDateLimit());
                }
                $collection->getSelect()->where('s.error_detail IS NOT NULL');

                break;

            case 'open-orders' :
                if (!$customerScreen) {
                    $collection->getSelect()->where('s.entity_id >= (SELECT MIN(entity_id) FROM superorder WHERE created_at >= ?)', $this->getDateLimit());
                }

                switch ($cluster) {
                    case 'oo-other-store' :
                        $session = Mage::getSingleton('customer/session');
                        $agent = $session->getSuperAgent() ?: $session->getAgent(true);
                        $dealerIdNow = $agent->getDealer()->getDealerId();
                        $telesalesStoreId = Mage::app()->getWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE)->getId();
                        $collection->getSelect()
                            ->where('oa.delivery_store_id = ?', $dealerIdNow)
                            ->where('(s.created_website_id = ? OR s.dealer_id != oa.delivery_store_id)', $telesalesStoreId)
                            ->where('s.order_status = ?', Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS)
                            ->where('main_table.creditcheck_status = ?', self::ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED);
                        break;
                    case 'oo-error' :
                        $collection->getSelect()
                            ->where('s.order_status not in(?)', [
                                Vznl_Superorder_Model_Superorder::SO_CANCELLED,
                                Vznl_Superorder_Model_Superorder::SO_CLOSED,
                                Vznl_Superorder_Model_Superorder::SO_FULFILLED,
                                Vznl_Superorder_Model_Superorder::SO_STATUS_CANCELLED,
                                Vznl_Superorder_Model_Superorder::SO_STATUS_REJECTED,
                                Vznl_Superorder_Model_Superorder::SO_VALIDATION_FAILED,
                            ])
                            ->where('main_table.status <> ?', self::ESB_PACKAGE_STATUS_DELIVERED)
                            ->where("s.error_detail IS NOT NULL OR main_table.vf_status_code IS NOT NULL AND TRIM(main_table.vf_status_code) <> ''");
                        break;

                    case 'oo-ready' :
                        $collection->getSelect()
                            ->where('s.order_status in(?)', [
                                Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS,
                            ])
                            ->where("s.error_detail IS NULL OR main_table.vf_status_code IS NULL AND TRIM(main_table.vf_status_code) = ''");
                        break;

                    case 'oo-check' :
                        $ccStatuses = self::ESB_PACKAGE_NETWORK_STATUSES_CC_RUNNING;
                        $ccStatuses[] = self::ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL;

                        $npStatuses = self::ESB_PACKAGE_NETWORK_STATUSES_NP_RUNNING;
                        $npStatuses[] = self::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED;

                        $collection->getSelect()
                            ->where('s.order_status not in(?)', [
                                Vznl_Superorder_Model_Superorder::SO_CANCELLED,
                                Vznl_Superorder_Model_Superorder::SO_CLOSED,
                                Vznl_Superorder_Model_Superorder::SO_FULFILLED,
                                Vznl_Superorder_Model_Superorder::SO_STATUS_CANCELLED,
                                Vznl_Superorder_Model_Superorder::SO_STATUS_REJECTED,
                                Vznl_Superorder_Model_Superorder::SO_VALIDATION_FAILED,
                                Vznl_Superorder_Model_Superorder::SO_FAILED,
                                Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS,
                                Vznl_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI,
                            ])
                            ->where('s.error_detail IS NULL')
                            ->where('main_table.creditcheck_status in(?) OR main_table.porting_status in(?)', [
                                $ccStatuses,
                                $npStatuses
                            ]);
                        break;

                    case 'oo-other' :
                        $collection->getSelect()
                            ->where('s.order_status in(?)', [
                                Vznl_Superorder_Model_Superorder::SO_CANCELLED,
                                Vznl_Superorder_Model_Superorder::SO_CLOSED,
                                Vznl_Superorder_Model_Superorder::SO_FULFILLED,
                                Vznl_Superorder_Model_Superorder::SO_VALIDATED,
                                Vznl_Superorder_Model_Superorder::SO_STATUS_CANCELLED,
                                Vznl_Superorder_Model_Superorder::SO_STATUS_REJECTED,
                                Vznl_Superorder_Model_Superorder::SO_VALIDATION_FAILED,
                            ]);
                            /**
                             * Order should be visible regardless of the combination of status.
                            ->where('(main_table.porting_status not in(?) or s.order_status in(?) or s.peal_order_id not in(null)) or main_table.porting_status is null', [
                                [
                                    self::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL,
                                    self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING,
                                    self::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED,
                                ],
                                [
                                    Vznl_Superorder_Model_Superorder::SO_CANCELLED,
                                    Vznl_Superorder_Model_Superorder::SO_STATUS_CANCELLED,
                                ]
                            ]);*/
                        break;

                    case 'oo-other-open' :
                        $collection->getSelect()
                            ->where('s.order_status in(?)', [
                                Vznl_Superorder_Model_Superorder::SO_VALIDATED,
                                Vznl_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI,
                                Vznl_Superorder_Model_Superorder::SO_FAILED,
                                Vznl_Superorder_Model_Superorder::SO_INITIAL
                            ])
                            ->where('main_table.porting_status not in(?) or main_table.porting_status is null', [
                                [
                                    self::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL,
                                    self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING,
                                    self::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED,
                                ]
                            ])
                            ->where('((s.error_code is null or s.error_code = "") and (main_table.vf_status_code is null or main_table.vf_status_code = ""))');
                        break;

                    case 'oo-imei' :
                        // Add extra joins that are required to check for a device product
                        $collection->getSelect()->where('s.oo_imei = ?', 1);

                        // Orders should either not have an actual porting date yet or the porting date should be in the future.
                        $collection->getSelect()
                            ->where('main_table.device_name is not null')
                            ->where('main_table.imei is null')
                            ->where('main_table.porting_status = ?', self::ESB_PACKAGE_NETWORK_STATUS_NP_AUTHORIZED)
                            ->where('(main_table.actual_porting_date is null OR ? < main_table.actual_porting_date)', now());
                        break;

                    case 'oo-inlife' :
                        throw new Exception('Inlife cluster is not supported by this method');

                    default:
                        throw new Exception('Unknown cluster set for this section');
                }
                break;

            default:
                throw new Exception('Section not found');
        }

        // Custom filters
        if ($agentId) {
            if (is_array($agentId)) {
                $collection->getSelect()->where('s.created_agent_id in(?)', $agentId);
            } else {
                $collection->getSelect()->where('s.created_agent_id = ?', $agentId);
            }
        }

        if ($customerId) {
            $collection->getSelect()->where('s.customer_id = ?', $customerId);
        }

        if ($dealerId) {
            if (is_array($dealerId)) {
                $collection->getSelect()->where('(s.created_dealer_id in(?) OR oa.delivery_store_id in(?))', $dealerId, $dealerId);
            } else {
                $collection->getSelect()->where('(s.created_dealer_id = ? OR oa.delivery_store_id = ?)', $dealerId, $dealerId);
            }
        }

        if (empty($agentId) && empty($dealerId) && Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
            $collection->getSelect()->where('s.created_agent_id = ?', Mage::getSingleton('customer/session')->getAgent(true)->getId());
            $collection->getSelect()->where('s.created_website_id = ?', Mage::app()->getWebsite()->getId());
        }

        //If website id is true, the agent may search orders of ALL websites. The filter should not be applied.
        if ($websiteId && $websiteId != true) {
            if (is_array($websiteId)) {
                $collection->getSelect()->where('s.created_website_id in(?)', $websiteId);
            } else {
                $collection->getSelect()->where('s.created_website_id = ?', $websiteId);
            }
        }

        $currentWebsite = Mage::app()->getWebsite();
        if ($currentWebsite->getCode() != Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE) {
            $collection->getSelect()->where('s.is_vf_only = ?', 1);
        }

        // add given sorting
        switch ($clusterOrderField) {
            case 'id':
                $collection->getSelect()->order(['s.entity_id ' . $clusterOrderType]);
                break;
            case 'status':
                $collection->getSelect()->order(['s.order_status ' . $clusterOrderType]);
                break;
            case 'package_nr':
                $collection->getSelect()->order(['total_packages ' . $clusterOrderType]);
                break;
            case 'cn':
                $collection->getSelect()->order(['customer_lastname ' . $clusterOrderType]);
                break;
        }

        // add sorting per section
        if ($section == 'number-porting' || $section == 'credit-checks') {
            $statusOrderString = $section == 'credit-checks' ? 'main_table.creditcheck_status_updated' : 'main_table.porting_status_updated';
            $collection->getSelect()
                ->order($statusOrderString, 'desc')
                ->order('s.updated_at', 'desc')
                ->order('s.created_at', 'desc');
        }

        if (is_numeric($clusterResultsPerPage)) { // Limit on value
            $collection->getSelect()->limit($clusterResultsPerPage, ($clusterPage - 1) * $clusterResultsPerPage);
        }

        return $collection;
    }

    /**
     * Check if package has a deviceRC
     * @return bool
     */
    public function hasDeviceRC() : bool
    {
        $orderItems = $this->getPackageItems();
        foreach ($orderItems as $item) {
            $flippedTypes = array_flip($item->getProduct()->getType());
            if (isset($flippedTypes[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION])){
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     * @throws Varien_Exception
     */
    public function isNumberPorting()
    {
        return (bool)$this->getCurrentNumber();
    }

    /**
     * @return bool
     */
    public function isRetention()
    {
        return (bool)($this->getCtn() && in_array($this->getSaleType(), [
                Dyna_Checkout_Model_Sales_Quote_Item::RETENTION,
                Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE,
                Omnius_Checkout_Model_Sales_Quote_Item::RETENTION,
            ]));
    }

    /**
     * @return bool
     */
    public function isAcquisition()
    {
        return (bool)(!$this->getCtn() && in_array($this->getSaleType(), [
                Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE,
                Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION,
            ]));
    }

    /**
     * Determines if a packages contains at least one hardware item
     */
    public function containsHardware($skipSim = false)
    {
        foreach ($this->getPackageItems() as $item) {
            if ($item->getProduct()->isOfHardwareType($skipSim)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the subscription start date
     * @return DateTime The date time.
     */
    public function getSubscriptionStartDate()
    {
        if ($this->getCurrentNumber()) {
            $date = new DateTime($this->getContractEndDate());
        } elseif ($this->getTelNumber() && $this->isAcquisition()) {
            $date = new DateTime($this->getSigningDate());
        } elseif ($this->isRetention()) {
            $ctnEndDate = $this->getCtnOriginalEndDate();
            $ctnStartDate = $this->getCtnOriginalStartDate();
            if ($ctnEndDate && $ctnStartDate) {
                if (strtotime($ctnStartDate->format('d-m-Y')) < time()) {
                    if (strtotime($ctnEndDate->format('d-m-Y')) < time()) {
                        // ctn start and end date in past
                        $date = new DateTime();
                    } else {
                        // ctn start date in past but end date in future
                        $date = new DateTime($ctnEndDate->format('d-m-Y'));
                    }
                } else {
                    // Ctn start date still in future
                    $date = new DateTime($ctnStartDate->format('d-m-Y'));
                }
            } else { // Previous contract end date
                $date = new DateTime($this->getSigningDate());
            }
        } else {
            $date = new DateTime($this->getCreatedAt());
        }
        return $date;
    }

    /**
     * Return all visible items for the package
     * @return array
     */
    public function getAllVisibleItems() : array
    {
        $items = array();
        foreach ($this->getPackageItems() as $item) {
            if (!$item->isDeleted() && !$item->getParentItemId()) {
                $items[] =  $item;
            }
        }
        return $items;
    }

    /**
     * Gets an array with all details from the handset
     * @param Vznl_Checkout_Model_Sales_Order $deliveryOrder The delivery order to use
     * @return array The handset details
     */
    public function getHandsetDetails($deliveryOrder = null)
    {
        $deliveryOrder = $deliveryOrder ?: $this->getDeliveryOrder();
        $handsetDetails = [];
        // Check if indirect order
        if ($deliveryOrder->isOrderedInIndirect()) {
            // Indirect order
            $deviceName = $this->getData('device_name');
            $arr = explode(' ', $deviceName, 2);
            $manufacturer = $arr ? $arr[0] : '';

            $handsetDetails['name'] = $deviceName;
            $handsetDetails['manufacturer'] = 'Overig';

            // Check all possible manufacturer names
            $manufacturerNames = Mage::helper('vznl_catalog')->getPossibleProdspecsBrandValues();
            foreach ($manufacturerNames as $manufacturerName) {
                if (strtolower($manufacturer) == strtolower($manufacturerName)) {
                    $handsetDetails['manufacturer'] = $manufacturerName;
                    break;
                }
            }
        } else {
            // Other order
            $device = null;
            /** @var Vznl_Checkout_Model_Sales_Order_Item $packageItem */
            foreach ($this->getPackageItems() as $packageItem) {
                if ($packageItem->getProduct()->isDevice()) {
                    $device = $packageItem->getProduct();
                }
            }
            if ($device) {
                /** @var Vznl_Catalog_Model_Product $device */
                $handsetDetails['name'] = $device->getName();
                $handsetDetails['manufacturer'] = $device->getAttributeText('prodspecs_brand');
            }
        }
        $handsetDetails['category'] = 'SMRT';

        return $handsetDetails;
    }

    /**
     * @param $packages
     */
    protected function handleEditedPackages(&$packages)
    {
        if (!$packages) {
            $quote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
            $deliveryOrders = Mage::getModel('sales/order')->getNonEditedOrderItems($quote->getSuperOrderEditId());
            if ($deliveryOrders) {
                foreach ($deliveryOrders as $deliveryOrder) {
                    foreach ($deliveryOrder->getAllItems() as $deliveryOrderItem) {
                        if (isset($packages[$deliveryOrderItem->getPackageId()])) {
                            continue;
                        }
                        $packages[$deliveryOrderItem->getPackageId()] = $deliveryOrder->getId();
                    }
                }
            }
            Mage::getSingleton('core/session')->setData('packageOrderCorrespondence' . Mage::getSingleton('customer/session')->getOrderEdit(), $packages);
        }
    }

    /**
     * @param $productType
     * @param $allItems
     * @return array
     *
     * Check for current changes or history orders and display the corresponding striked information
     */
    public function getDisplayFor($productType, $allItems)
    {
        $allItems = $allItems ?? [];
        $quoteItems = array();
        $orderItems = array();
        $this->removedProducts = array();
        $changedIds = array();
        $cancelOrders = array();
        $changedPackagesIds = array();
        $oldOrdersPackages = array();
        $cartHelper = Mage::helper('dyna_configurator/cart');

        if (!$this->getIsExtendedCart()) {
            //check existence of order history, first identifying which Magento/delivery order corresponds to that package
            $packages = Mage::getSingleton('core/session')->getData('packageOrderCorrespondence' . Mage::getSingleton('customer/session')->getOrderEdit()) ?: array();
            $this->handleEditedPackages($packages);
        }

        //Checking if packages exist, otherwise this might be just a quote
        if (isset($packages[$this->getPackageId()])) {
            $orderObj = Mage::getModel('sales/order')->load($packages[$this->getPackageId()]);
            $allOrderItems = Mage::getModel('sales/order_item')
                ->getCollection()
                ->addAttributeToFilter('order_id', $orderObj->getId())
                ->addAttributeToFilter('package_id', $this->getPackageId())
                ->load();

            foreach ($allOrderItems as $oneOrderItem) {
                $product = $oneOrderItem->getProduct();
                if (($product->getId() === null || $product->getId() === '')) {
                    if (($oneOrderItem->getPackProductType() == $productType)) {
                        $allItems[] = $oneOrderItem;
                    }
                }
            }
        }

        $changedIdsCount = 0;

        //check if user changed something in the current session
        if (($changedItems = $this->getChanges()) && !$this->getIsExtendedCart()) {
            // Get new package items in the format: [product_id => item_doa]
            $changedIds = Dyna_Checkout_Helper_Data::getOrderQuoteItemIds($changedItems);
            $changedIdsCount = count($changedIds);
            $changedItems = $cartHelper->getSortedCartProducts($changedItems, true);
            foreach ($changedItems as $changedItem) {
                $changedItem->setTelNumber('');//tel number should only be displayed under device
                $flippedChangedItem = array_flip($changedItem->getProduct()->getType());
                if (isset($flippedChangedItem[$productType])) {
                    list($newImg, $newName, $newMaf, $newPrice) = $this->getContentFor($changedItem, $changedItems);
                    $quoteId = $changedItem->getData('quote_id');
                    if (!isset($quoteItems[$changedItem->getData('quote_id')])) {
                        $quoteItems[$changedItem->getData('quote_id')] = array(
                            'image' => $newImg,
                            'name' => $newName,
                            'maf' => $newMaf,
                            'price' => $newPrice,
                            'iltRequired' => $changedItem->getProduct()->isIltRequired()
                        );
                    } else {
                        $quoteItems[$quoteId]['image'] = array_merge($newImg, $quoteItems[$quoteId]['image']);
                        $quoteItems[$quoteId]['name'] = array_merge($newName, $quoteItems[$quoteId]['name']);
                        $quoteItems[$quoteId]['maf'] = array_merge($newMaf, $quoteItems[$quoteId]['maf']);
                        $quoteItems[$quoteId]['price'] = array_merge($newPrice, $quoteItems[$quoteId]['price']);
                        $quoteItems[$quoteId]['iltRequired'] = $changedItem->getProduct()->isIltRequired();
                    }
                    $changedPackagesIds[$changedItem->getPackageId()] = $changedItem->getPackageId();
                }
            }
        }

        $currentSimcard = array();
        //display current package details
        $changedIdsCount = count($changedIds) ;
        if($allItems) {
            $allItems = $cartHelper->getSortedCartProducts($allItems);
        }
        foreach ($allItems as $item) {
            $flippedType = array_flip( $item->getProduct()->getType());
            if (isset($flippedType[Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD]) && $item->getPackageId() == $this->getPackageId()) {
                $currentSimcard[$this->getPackageId()] = $item;
            }
            if (isset($flippedType[Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION]) && $item->getPackageId() == $this->getPackageId()) {
                $currentSubscription[$this->getPackageId()] = $item;
            }
            $productTypeLowerCase = strtolower($productType);
            $flippedProductTypeLowerCase = array_flip(array_map('strtolower', $item->getProduct()->getType()));
            if (isset($flippedProductTypeLowerCase[$productTypeLowerCase])) {
                if ($item->getProduct()->isPromo() && !is_null($item->getTargetId())) {
                    continue;
                }

                if (($changedIdsCount == 0) || (!array_key_exists($item->getProductId(), $changedIds)) || (isset($changedIds[$item->getProductId()]) && $item->getItemDoa() != $changedIds[$item->getProductId()])) {
                    $cancelOrders = array_merge(Mage::helper('dyna_package')->getCancelledPackages(), Mage::helper('dyna_package')->getCancelledPackagesNow());
                    $flippedCancelOrders = array_flip($cancelOrders);
                    $itemPackageId = $item->getPackageId();
                    $wasDeleted =  ((isset($flippedCancelOrders[$itemPackageId]) && $item->getIsCancelled()) ||
                    ($changedIdsCount > 0 && (!array_key_exists($item->getProductId(), $changedIds) || (isset($changedIds[$item->getProductId()]) && $item->getItemDoa() != $changedIds[$item->getProductId()]))));
                    $quoteId = $item->getData('quote_id') ? $item->getData('quote_id') : $item->getData('alternate_quote_id');
                    list($currentImg, $currentName, $currentMaf, $currentPrice, $circle_style) = $this->getContentFor($item, $allItems, $wasDeleted);
                    if (!isset($quoteItems[$quoteId])) {
                        $quoteItems[$quoteId] = array(
                            'image' => $currentImg,
                            'name' => $currentName,
                            'maf' => $currentMaf,
                            'price' => $currentPrice,
                            'iltRequired' => $item->getProduct()->isIltRequired(),
                            'circle_style' => $circle_style
                        );
                    } else {
                        $quoteItems[$quoteId]['image'] = array_merge($quoteItems[$quoteId]['image'], $currentImg);
                        $quoteItems[$quoteId]['name'] = array_merge($quoteItems[$quoteId]['name'], $currentName);
                        $quoteItems[$quoteId]['maf'] = array_merge($quoteItems[$quoteId]['maf'], $currentMaf);
                        $quoteItems[$quoteId]['price'] = array_merge($quoteItems[$quoteId]['price'], $currentPrice);
                        $quoteItems[$quoteId]['circle_style'] = array_merge($quoteItems[$quoteId]['circle_style'], $circle_style);
                        $quoteItems[$quoteId]['iltRequired'] = $item->getProduct()->isIltRequired();
                    }

                    // Unset price if type is device subscription
                    if ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                        unset($quoteItems[$quoteId]['price']);
                    }
                }
            }
        }

        $subscriptionsChanged = array();
        $simcardChanged = array();
        $oldOrders = array();

        if (!$this->getIsExtendedCart()) {
            if (isset($packages[$this->getPackageId()]) && $packages[$this->getPackageId()]) {
                $orderObj = Mage::getModel('sales/order')->load($packages[$this->getPackageId()]);
                if ($orderObj->getParentId()) {
                    $oldOrder = Mage::getModel('sales/order')->load($orderObj->getParentId());
                    $parentOrderLevel = 0;
                    while ($oldOrder) {
                        $parentOrderLevel++;
                        $hasSubscriptionChanged = false;
                        $oldOrderItems = $oldOrder->getAllItems(true);
                        foreach ($oldOrderItems as $oldOrderItem) {
                            $flippedOldOrderItemProductType = array_flip($oldOrderItem->getProduct()->getType());
                            if (
                                isset($flippedOldOrderItemProductType[Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD])
                                && $oldOrderItem->getPackageId() == $this->getPackageId()
                            ) {
                                if (!isset($subscriptionsChanged[$this->getPackageId()])) {
                                    if (isset($currentSimcard[$oldOrderItem->getPackageId()])) {
                                        if ($currentSimcard[$oldOrderItem->getPackageId()]->getProductId() != $oldOrderItem->getProductId()) {
                                            if ($parentOrderLevel > 1) {
                                                $oldOrders[$orderObj->getId()] = $orderObj->getAllItems(true);
                                            } else {
                                                $oldOrders[$oldOrder->getId()] = $oldOrderItems;
                                            }
                                            $oldOrderItem->setParentOrderLevel($parentOrderLevel);
                                            $simcardChanged[$this->getPackageId()][$oldOrder->getId()] = $oldOrderItem;
                                        }
                                    }
                                } else {
                                    if (isset($currentSimcard[$oldOrderItem->getPackageId()])) {
                                        if ($currentSimcard[$oldOrderItem->getPackageId()]->getProductId() != $oldOrderItem->getProductId()) {
                                            $simcardChanged[$this->getPackageId()][$oldOrder->getId()] = $oldOrderItem;
                                        }
                                    }
                                }
                                $currentSimcard[$oldOrderItem->getPackageId()] = $oldOrderItem;
                            }

                            if (isset($flippedOldOrderItemProductType[Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION])
                                && $oldOrderItem->getPackageId() == $this->getPackageId()
                            ) {
                                if ($oldOrderItem->getPackageId()) {
                                    if (isset($currentSubscription[$oldOrderItem->getPackageId()]) && $currentSubscription[$oldOrderItem->getPackageId()]->getProductId() != $oldOrderItem->getProductId()) {
                                        $currentSubscription[$oldOrderItem->getPackageId()] = $oldOrderItem;
                                        $hasSubscriptionChanged = true;
                                    }
                                }
                            }

                            $order_id = $oldOrderItem->getData('order_id');
                            if (isset($flippedOldOrderItemProductType[$productType]) && $oldOrderItem->getPackageId() == $this->getPackageId()) {
                                list($oldImg, $oldName, $oldMaf, $oldPrice) = $this->getContentFor($oldOrderItem, $oldOrderItems, true, $orderObj->getAllItems(true));

                                if (!empty($oldName)) {
                                    if ($productType == Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) {
                                        if (!isset($subscriptionsChanged[$this->getPackageId()])) {
                                            $subscriptionsChanged[$this->getPackageId()] = 1;
                                        }
                                        unset($simcardChanged[$this->getPackageId()][$oldOrder->getId()]);
                                    }
                                    if (!isset($orderItems[$order_id])) {
                                        $orderItems[$order_id] = array(
                                            'image' => $oldImg,
                                            'name' => $oldName,
                                            'maf' => $oldMaf,
                                            'price' => $oldPrice,
                                        );
                                    } else {
                                        $orderItems[$order_id]['image'] = array_merge($oldImg, $orderItems[$order_id]['image']);
                                        $orderItems[$order_id]['name'] = array_merge($oldName, $orderItems[$order_id]['name']);
                                        $orderItems[$order_id]['maf'] = array_merge($oldMaf, $orderItems[$order_id]['maf']);
                                        $orderItems[$order_id]['price'] = array_merge($oldPrice, $orderItems[$order_id]['price']);
                                    }
                                    $oldOrdersPackages[$oldOrderItem->getPackageId()] = $oldOrderItem->getPackageId();

                                    // Unset price if type is device subscription
                                    if ($oldOrderItem->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                                        unset($orderItems[$order_id]['price']);
                                    }
                                }
                            }
                        }
                        if ($hasSubscriptionChanged) {
                            unset($simcardChanged[$this->getPackageId()][$oldOrder->getId()]);
                        }
                        $orderObj = $oldOrder;
                        $oldOrder = $oldOrder->getParentId() ? Mage::getModel('sales/order')->load($oldOrder->getParentId()) : null;
                        unset($subscriptionsChanged[$this->getPackageId()]);
                    }
                }
            }
        }

        $this->quoteItems = $quoteItems;
        if (isset($orderItems)) {
            $this->orderItems = $orderItems;
        }

        if ($productType == Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) {
            reset($allItems);
            $firstQuoteItem = current($allItems);
            $appendToQuoteId = null;
            if ($changedItems) {
                reset($changedItems);
                $firstChangedItem = current($changedItems);
                if (!isset($quoteItems[$firstQuoteItem->getQuoteId()]) && isset($quoteItems[$firstChangedItem->getQuoteId()])) {
                    $appendToQuoteId = $firstChangedItem->getQuoteId();
                }
            }

            $doExtraLines = false;
            foreach ($allItems as $currentItem) {
                $quoteId = $currentItem->getQuoteId();
                if (!array_key_exists($currentItem->getProductId(), $changedIds)) {
                    if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $currentItem->getProduct()) || $currentItem->getProduct()->isSim()) {
                        $doExtraLines = true;
                    }
                }
            }

            if ($changedItems) {
                // Set extra lines (sim) for the edited packages quote items
                $this->setExtraLines($changedItems, 'quote', array());
            }

            // Set extra lines (promo, sim) for the current super quote items
            if ($doExtraLines) {
                $this->setExtraLines($allItems, 'quote', array_merge($changedPackagesIds, $cancelOrders), $appendToQuoteId);
            }

            if (isset($oldOrders)) {
                foreach ($oldOrders as $key => $oldOrderItems) {
                    // Set extra lines (sim) for the old orders items
                    if (!isset($simcardChanged[$this->getPackageId()][$key])) {
                        $this->setExtraLines($oldOrderItems, 'order', $oldOrdersPackages);
                    }
                }
            }

            if (
                isset($simcardChanged[$this->getPackageId()])
                && isset($quoteId)
            ) {
                $old_class = ' striked old_package';
                $old_class .= ($parentOrderLevel > 1) ? ' striked' : '';
                $old_price_class = ' class="striked old_package"';
                $affected_id = $quoteId;
                $affected = 'quoteItems';
                if(isset($oldOrders)) {
                    $flippedOldOrders = array_flip($oldOrders);
                }
                foreach ($simcardChanged[$this->getPackageId()] as $orderId => $item) {
                    if (!isset($oldOrders) || !isset($flippedOldOrders[$orderId])) {
                        if ($item->getParentOrderLevel() > 1) {
                            $affected_id = $orderId;
                            $affected = 'orderItems';
                        }
                        $this->{$affected}[$affected_id]['image'][] = '';
                        $this->{$affected}[$affected_id]['name'][] = '<span class="orderline-section-table-subline ' . $old_class . '">' . $item->getName() . '</span>';
                        $this->{$affected}[$affected_id]['maf'][] = '<label></label>';
                        $this->{$affected}[$affected_id]['price'][] = '<label' . $old_price_class . '>' . Mage::helper('core')->currency($item->getRowTotal(), true, false) . '</label>';
                    }
                }
            }
        }

        if (isset($orderItems)) {
            $historyItems = array_merge($this->quoteItems, $this->orderItems);
        } else {
            $historyItems = $this->quoteItems;
        }

        /* if (!empty($deletedItems)) {
             $historyItems = array_merge($historyItems, $deletedItems);
         }*/
        return $historyItems;
    }


    /**
     * @param Dyna_Checkout_Model_Sales_Quote_Item $item
     * @param Dyna_Checkout_Model_Sales_Quote_Item[] $allItems
     * @param bool|false $wasDeleted
     * @param Dyna_Checkout_Model_Sales_Order_Item[] $parentOrderItems
     * @return array
     */
    public function getContentFor($item, $allItems, $wasDeleted = false, $parentOrderItems = array())
    {
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        if ($wasDeleted && (end($this->removedProducts) == $item->getProductId()) && $item->getItemDoa()) {
            //don't display the same product as removed twice
            return array(array(), array(), array(), array());
        }
        if ($parentOrderItems && $wasDeleted) {
            foreach ($parentOrderItems as $parentItem) {
                if ($item->getPackageId() == $parentItem->getPackageId() && $item->getProductId() == $parentItem->getProductId()) {
                    // Product was not changed so we skip it
                    return array(array(), array(), array(), array());
                }
            }
        }

        $image = array();
        $name = array();
        $maf = array();
        $price = array();
        $circle_style = array();
        $this->removedProducts[] = $item->getProductId();
        $orderOverView = Mage::registry('orderOverView');
        if (!$this->getIsExtendedCart() || $orderOverView) {
            $this->setWithBtw(true);
            $itemProps = json_decode($item->getData('order_item_prices'), true);
            $prodPrice = $itemProps['product']['special_price'] ?: $itemProps['product']['price'];
            $mixMatch = isset($itemProps['mixmatch']) ? $itemProps['mixmatch'] : null;
            if (isset($itemProps['mixmatch_subtotal']) && $itemProps['mixmatch_subtotal'] != null) {
                $mixMatch = $itemProps['mixmatch_subtotal'] + $itemProps['mixmatch_tax'];
            }
            $customer = Mage::getModel('customer/session')->getCustomer();
            if ($customer->getIsBusiness()) {
                $this->setWithBtw(false);
                if ($mixMatch) {
                    $mixMatch = Mage::helper('tax')->getPrice($item->getProduct(), $mixMatch, false);
                }
                $prodPrice = Mage::helper('tax')->getPrice($item->getProduct(), $prodPrice, false);
            }

            $imgWH = 25;
        } else {
            $jsonData = json_decode($item->getOrderItemPrices(), true);
            if ($quote->getIsOffer() && isset($jsonData['mixmatch_excl_tax'])) {
                $mixMatch =
                    isset($jsonData['mixmatch_subtotal'])
                    && $jsonData['mixmatch_subtotal'] != null
                        ? $jsonData['mixmatch_subtotal']
                        : $jsonData['mixmatch_excl_tax'];

            } elseif (Mage::helper('tax')->getPrice($item->getProduct(), $item->getProduct()->getFinalPrice(), false) - $item->getRowTotal() != 0) {
                $item->setHasMixMatch(true);
                $mixMatch = $this->getWithBtw()
                    ? ($item->getMixmatchSubtotal() != null ? $item->getMixmatchSubtotal() + $item->getMixmatchTax() : $item->getRowTotalInclTax())
                    : ($item->getMixmatchSubtotal() != null ? $item->getMixmatchSubtotal() : $item->getRowTotal());
            }
            $productPrices = Mage::helper('dyna_configurator')->getBlockRowData($item);
            $prodPrice = $this->getWithBtw() ? (isset($productPrices['priceTax']) ? $productPrices['priceTax'] : 0) : $productPrices['price'];
            if ($quote->getIsOffer()) {
                $tmpPriceData = json_decode($item->getData('order_item_prices'), true);
                $prodPrice = isset($tmpPriceData['product']['price_excl']) ? $tmpPriceData['product']['price_excl'] : $prodPrice;
            }

            $imgWH = 50;
        }

        $productType = $item->getProduct()->getType();

        //stiked
        $striked = "";
        if($item->getAlternateQuoteId()!= null && $item->getPreselectProduct())
            $striked = "striked";

        $productCircleStyle = 'default-none';
        $preselectProduct = $item->getPreselectProduct();
        $systemAction = $item->getSystemAction();
        $isContractDrop = $item->getIsContractDrop();
        $isDefaulted = $item->getIsDefaulted();
        if (in_array($item->getSaleType(), array_merge(Vznl_Catalog_Model_ProcessContext::ILSProcessContexts(), Vznl_Catalog_Model_ProcessContext::getProlongationProcessContexts(), Vznl_Catalog_Model_ProcessContext::MoveProcessContexts()))) {
            if ($systemAction == 'add' && !$preselectProduct) {
                $productCircleStyle = 'added-blue';
            } elseif ($isContractDrop) {
                if ($systemAction != 'N/A') {
                    $productCircleStyle = 'removed-red';
                }
            } elseif ($preselectProduct) {
                $productCircleStyle = 'keep-grey';
            }
        }

        $circle_style[] = $productCircleStyle;

        switch (current($productType)) {
            case Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION:
            case Vznl_Catalog_Model_Type::SUBTYPE_ADDON:
            case Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION:

                if ($item->getProduct()->isServiceItem()) {
                    break;
                }

                if (!$this->getIsExtendedCart()) {
                    $regularMaf = (Mage::helper('tax')->getPrice(
                        $item->getProduct(),
                        $itemProps['product']['regular_maf'],
                        $this->getWithBtw()
                    ))?:$this->getWithBtw()
                        ? $item->getMafRowTotalInclTax()
                        : $item->getMaf();
                } else {
                    $regularMaf = $this->getWithBtw()
                        ? $item->getMafRowTotalInclTax()
                        : $item->getMaf();
                }

                if ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                    $prodPrice = $item->getTotalPaidByMaf(false);
                }

                //add name and regular maf
                $image[] = '<img src="' . Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize($imgWH, $imgWH) . '"/>';
                $name[] = '<span class="orderline-section-table-orderline">' . $item->getName() . '</span>';
                $maf[] = '<label class="orderlineprice">' . Mage::helper('core')->currency($regularMaf, true, false) . '</label>';
                $price[] = (int)$prodPrice ? ('<label class="orderlineprice">' . Mage::helper('core')->currency($prodPrice, true, false) . '</label>') : '<label></label>';
                end($maf);         // move the internal pointer to the end of the array
                $tempKey = key($maf);

                // reorder applied promos by their selection in the stackable promos drawer
                Mage::helper('vznl_package')->orderAppliedPromosBySelection($item->getPackageId(), $allItems);

                foreach ($allItems as $cloneItem) {
                    $product = $cloneItem->getProduct();
                    $rightTarget = ($cloneItem->getTargetId() == $item->getProductId());
                    $rightPackageId = ($cloneItem->getPackageId() == $item->getPackageId());
                    $productIsPromo = $item->getMafDiscountAmount() >= 0 || $product->getPrijsAansluitPromoBedrag() || $product->getPrijsAansluitPromoProcent();
                    if ($cloneItem->isPromo() && $rightTarget && $rightPackageId && ($productIsPromo)) {
                        $mafAfterDiscount = $this->getWithBtw()
                            ? $item->getItemFinalMafInclTax()
                            : $item->getItemFinalMafExclTax();
                        $image[] = '';
                        $name[] = '<span class="orderline-section-table-subline">' . $cloneItem->getName() . '</span>';
                        // get prices for each promo
                        Mage::helper('vznl_package')->getPromoProductsDisplayPrices($item, $cloneItem, $tempKey, $maf, $price, $mafAfterDiscount);
                    }
                }
                unset($tempKey);

                if ($item->getData('connection_cost') && $item->getData('connection_cost') > 0) {
                    /** Vznl_Checkout_Model_Sales_Quote_Item */
                    $item->hasConnectionCosts($this->getWithBtw(), $image, $name, $maf, $price);
                }
                if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $productType)) {
                    if (!$wasDeleted && $this->getPhoneNumber() && current($productType) !== Vznl_Catalog_Model_Type::SUBTYPE_ADDON) {
                        $image[] = '';
                        $name[] = '<span class="orderline-section-table-subline">' . Mage::helper('dyna_checkout')->__('Mobile number') . ': ' . Mage::helper('dyna_checkout')->formatCtn($this->getPhoneNumber()) . '</span>';
                        $maf[] = '<label></label>';
                        $price[] = '<label></label>';
                    }
                    if (!$wasDeleted && $this->getSimNumber() && current($productType) !== Vznl_Catalog_Model_Type::SUBTYPE_ADDON) {
                        $image[] = '';
                        $name[] = '<span class="orderline-section-table-subline">' . Mage::helper('dyna_checkout')->__('SIMNR') . ': ' . $this->getSimNumber() . '</span>';
                        $maf[] = '<label></label>';
                        $price[] = '<label></label>';
                    }

                    //display imei
                    if (!$item->getItemDoa() && $this->getImei()) {
                        $image[] = '';
                        $name[] = '<span class="orderline-section-table-subline">' . Mage::helper('dyna_checkout')->__('IMEI') . ': ' . $this->getImei() . '</span>';
                        $maf[] = '<label></label>';
                        $price[] = '<label></label>';
                    }
                }
                break;
            case Vznl_Catalog_Model_Type::SUBTYPE_DEVICE:
            case Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY:

                if ($item->getProduct()->isServiceItem()) {
                    break;
                }

                if (isset($mixMatch) && $mixMatch !== null && in_array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE, $productType)) {
                    $image[] = '<img src="' . Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize($imgWH, $imgWH) . '"/>';
                    $name[] = '<span class="orderline-section-table-orderline">' . $item->getName() . '</span>';
                    $price[] = '<label class=" orderlineprice striked">' . Mage::helper('core')->currency($prodPrice, true, false) . '</label>';
                    $maf[] = '<label></label>';

                    $image[] = '';
                    $name[] = '<span class="orderline-section-table-subline">' . Mage::helper('dyna_checkout')->__('Combination price') . '</span>';
                    $price[] = '<label>' . Mage::helper('core')->currency($mixMatch, true, false) . '</label>';
                    $maf[] = '<label></label>';
                } else {
                    $image[] = '<img src="' . Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize($imgWH, $imgWH) . '"/>';
                    $name[] = '<span class="orderline-section-table-orderline">' . $item->getName() . '</span>';
                    $price[] = '<label class="orderlineprice">' . Mage::helper('core')->currency($prodPrice, true, false) . '</label>';
                    $maf[] = '<label></label>';
                }

                if ($item->getDiscountAmount() != null && $item->getDiscountAmount() > 0) {
                    $price[count($price) - 1] = str_replace('<label>', '<label class="striked">', $price[count($price) - 1]);
                    $appliedRuleIds = Mage::helper('pricerules')->explodeTrimmed($item->getAppliedRuleIds());
                    $appliedRules = Mage::getModel('salesrule/rule')->getCollection()
                        ->addFieldToFilter('simple_action', array('neq' => 'ampromo_items'))
                        ->addFieldToFilter('rule_id', array('in' => $appliedRuleIds));
                    $discountNames = array();

                    foreach ($appliedRules as $rule) {
                        $discountNames[] = $rule->getName();
                    }

                    $deviceItem = isset($mixMatch) && $mixMatch !== null ? $mixMatch : $prodPrice;
                    $discount = (float)$item->getDiscountAmount();
                    $deviceItem -= $this->getWithBtw() ? $discount : ($discount - $item->getHiddenTaxAmount());

                    $image[] = '';
                    $name[] = '<span class="orderline-section-table-subline">' . Mage::helper('dyna_checkout')->__(implode(', ', $discountNames)) . '</span>';
                    $price[] = '<label>' . Mage::helper('core')->currency($deviceItem, true, false) . '</label>';
                    $maf[] = '<label></label>';
                }

                //check if has promotional product with maf discount
                if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE, $productType)) {
                    $priceCount = count($price);
                    foreach ($allItems as $cloneItem) {
                        if ($cloneItem->isPromo() && (int)$cloneItem->getRowTotalInclTax()) {
                            $image[] = '';
                            $name[] = '<span class="orderline-section-table-subline">' . $cloneItem->getName() . '</span>';
                            $priceCount = count($price);
                            $price[$priceCount - 1] = str_replace('<label>', '<label class="striked">', $price[$priceCount - 1]);
                            $price[] = '<label>' . Mage::helper('core')->currency($cloneItem->getRowTotalInclTax(), true, false) . '</label>';
                            $maf[] = '<label></label>';
                        }

                        if ($cloneItem->getTargetId() == $item->getProductId() && $cloneItem->getProduct()->isServiceItem() && $item->getPackageId() == $cloneItem->getPackageId()) {
                            $copyLevy = $this->getWithBtw() ? $cloneItem->getRowTotalInclTax() : $cloneItem->getRowTotal();
                            if ($copyLevy > 0) {
                                $image[] = '';
                                $name[] = '<span class="orderline-section-table-subline">' . Mage::helper('dyna_checkout')->__($cloneItem->getName()) . '</span>';
                                $price[] = '<label>' . Mage::helper('core')->currency($copyLevy, true, false) . '</label>';
                                $maf[] = '<label></label>';
                            }
                        }

                        if ($cloneItem->getId() == $item->getId()) {
                            $weeeTaxApplied = $cloneItem->getWeeeTaxAppliedUnserialized();

                            if (!empty($weeeTaxApplied)) {
                                foreach ($weeeTaxApplied as $taxApplied) {
                                    $connCost = Mage::helper('core')->currency(
                                        Mage::helper('tax')->getPrice(
                                            $item->getProduct(),
                                            $taxApplied['base_row_amount_incl_tax'],
                                            $this->getWithBtw()
                                        ),
                                        true,
                                        false
                                    );
                                    $image[] = '';
                                    $name[] = '<span class="orderline-section-table-subline">' . Mage::helper('dyna_checkout')->__($taxApplied['title']) . '</span>';
                                    $price[] = '<label>' . $connCost . '</label>';
                                    $maf[] = '<label></label>';
                                }
                            }
                        }
                    }
                }
                //display sim and imei
                if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE, $productType) && !$item->getItemDoa()) {
                    if ($item->getEditOrderId() && $this->getImei()) {
                        $image[] = '';
                        $name[] = '<span class="orderline-section-table-subline">' . Mage::helper('dyna_checkout')->__('IMEI') . ': ' . $this->getImei() . '</span>';
                        $maf[] = '<label></label>';
                        $price[] = '<label></label>';
                    }
                }
                break;

            case Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE:
                $image[] = '<img src="' . Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize($imgWH, $imgWH) . '"/>';
                $name[] = '<label class="orderline-section-table-package-name '.$striked.'">' . $item->getName() . '</label>';
                $regularMaf = $this->getWithBtw()
                    ? $item->getMafRowTotalInclTax()
                    : $item->getMaf();
                $maf[] = '<label class="orderlineprice '.$striked.'">' . Mage::helper('core')->currency($regularMaf, true, false) . '</label>';
                end($maf);
                $tempKey = key($maf);

                foreach ($allItems as $cloneItem) {
                    $product = $cloneItem->getProduct();
                    $rightTarget = ($cloneItem->getTargetId() == $item->getProductId());
                    $rightPackageId = ($cloneItem->getPackageId() == $item->getPackageId());
                    $productIsPromo = $item->getMafDiscountAmount() >= 0 || $product->getPrijsAansluitPromoBedrag() || $product->getPrijsAansluitPromoProcent();
                    if ($cloneItem->getTargetId() == $item->getProductId() && $cloneItem->getProduct()->getHideOriginalPrice()) {
                        $item->setHideOriginalPrice(true);
                        $maf[$tempKey] = '<label>' . Mage::helper('core')->currency($item->getItemFinalMafInclTax(), true, false) . '</label>';
                    }
                    if (($cloneItem->isPromo() || $item->isBundlePromo()) && $rightTarget && $rightPackageId && ($productIsPromo) && !$item->getHideOriginalPrice()) {
                        $mafAfterDiscount = null;
                        $image[] = '';
                        $name[] = '<span class="orderline-section-table-subline">' . $cloneItem->getName() . '</span>';
                        // get prices for each promo
                        Mage::helper('vznl_package')->getPromoProductsDisplayPrices($item, $cloneItem, $tempKey, $maf, $price, $mafAfterDiscount, $this->getWithBtw());
                    }
                }
                unset($tempKey);
                break;
            case Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS:
            case Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_ADDONS:
            case Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS:
                if (!$this->getIsExtendedCart()) {
                    $regularMaf = Mage::helper('tax')->getPrice(
                        $item->getProduct(),
                        $itemProps['product']['regular_maf'],
                        $this->getWithBtw()
                    );
                } else {
                    $regularMaf = $this->getWithBtw()
                        ? $item->getMafRowTotalInclTax()
                        : $item->getMaf();
                }

                //add name and regular maf
                $image[] = '<img src="' . Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize($imgWH, $imgWH) . '"/>';
                $name[] = '<span class="orderline-section-table-fixed-orderline '.$striked.'">' . $item->getName() . '</span>';
                if (intval($regularMaf) == 0 && intval($prodPrice) != 0) {
                    $maf[] = '';
                } else {
                    $maf[] = '<label class="orderlineprice '.$striked.'">' . Mage::helper('core')->currency($regularMaf, true, false) . '</label>';
                }
                $price[] = (int)$prodPrice ? ('<label class="orderlineprice">' . Mage::helper('core')->currency($prodPrice, true, false) . '</label>') : '<label></label>';
                end($maf);         // move the internal pointer to the end of the array

                if ($item->getData('connection_cost') && $item->getData('connection_cost') > 0) {
                    /** Vznl_Checkout_Model_Sales_Quote_Item */
                    $item->hasConnectionCosts($this->getWithBtw(), $image, $name, $maf, $price);
                }

                $tempKey = key($price);
                foreach ($allItems as $cloneItem) {
                    $product = $cloneItem->getProduct();
                    $rightTarget = ($cloneItem->getTargetId() == $item->getProductId());
                    $rightPackageId = ($cloneItem->getPackageId() == $item->getPackageId());
                    $productIsPromo = ($item->getDiscountAmount() >= 0);
                    if (($cloneItem->isPromo() || $item->isBundlePromo()) && $rightTarget && $rightPackageId && ($productIsPromo)) {
                        $priceAfterDiscount = null;
                        $image[] = '';
                        $name[] = '<span class="orderline-section-table-subline">' . $cloneItem->getName() . '</span>';
                        // get prices for each promo
                        if(intval($regularMaf) != 0) {
                            Mage::helper('vznl_package')->getPromoProductsDisplayPrices($item, $cloneItem, $tempKey, $maf, $price, $priceAfterDiscount, $this->getWithBtw());
                        } elseif(intval($prodPrice) != 0) {
                            Mage::helper('vznl_package')->getPromoProductsDisplayOneTimePrices($item, $cloneItem, $tempKey, $price, $priceAfterDiscount, $this->getWithBtw());
                        }
                    }
                }
                unset($tempKey);
                break;
        }

        //strike all prices for history or old changes
        if ($wasDeleted) {
            foreach ($name as &$label) {
                $label = str_replace('<label>', '<label class="old_package">', $label);
                $label = str_replace('orderline-section-table-subline', 'orderline-section-table-subline striked old_package', $label);
                $label = str_replace('orderline-section-table-orderline', 'orderline-section-table-orderline striked old_package', $label);
            }
            unset($label);
            foreach ($price as &$label) {
                $label = str_replace('striked', 'striked old_package', $label);
                $label = str_replace('<label>', '<label class="striked old_package">', $label);
                $label = str_replace('orderlineprice', 'orderlineprice striked old_package', $label);
            }
            unset($label);
            foreach ($maf as &$label) {
                $label = str_replace('striked', 'striked old_package', $label);
                $label = str_replace('<label>', '<label class="striked old_package">', $label);
                $label = str_replace('orderlineprice', 'orderlineprice striked old_package', $label);
            }
            unset($label);
        }

        return array($image, $name, $maf, $price, $circle_style);
    }

    /**
     * @param bool $useQuote
     * @return bool
     */
    public function needsCreditCheck($useQuote = false)
    {
        $key = __METHOD__ . ' ' . $this->getId();

        if (Mage::registry('freeze_models') === true) {
            $data = Mage::registry($key);
            if ($data !== null) {
                return $data;
            }
        }

        if ($useQuote) {
            $items = Mage::getSingleton('checkout/session')->getQuote()->getPackageItems($this->getPackageId());
        } else {
            $items = $this->getPackageItems();
        }
        $hasVfNwProduct = false;
        $hasRetention = false;
        foreach ($items as $item) {
            if ($item->getProduct()->isNetwork()) {
                $hasVfNwProduct = true;
                break;
            }
        }

        if ($this->isRetention()) {
            $hasRetention = true;
        }

        $deliveryOrder = $this->getDeliveryOrder();
        $retailHomeDelivery = !empty($deliveryOrder) && $deliveryOrder->isHomeDelivery() && $deliveryOrder->isOrderedInStore();

        // Packages with Vodafone network products need a CreditCheck
        // Packages that are retention packages do not need CreditCheck
        $needsCreditCheck = $hasVfNwProduct && (!$hasRetention || $retailHomeDelivery);
        if (Mage::registry('freeze_models') === true) {
            Mage::unregister($key);
            Mage::register($key, $needsCreditCheck);
        } else {
            Mage::unregister($key);
        }

        return $needsCreditCheck;
    }

    /**
     * Determines whether or not current package is of type fixed
     * @return bool
     */
    public function isFixed()
    {
        return in_array(strtolower($this->getType()), Vznl_Catalog_Model_Type::getFixedPackages());
    }

    /**
     * @return array
     * @param bool $simcard
     * Get product order used in saved shopping cart and order confirmation emails
     */
    public static function getEmailProductOrder($simcard = false)
    {
        $subtypes = array(
            Vznl_Catalog_Model_Type::SUBTYPE_DEVICE,
            Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
            Vznl_Catalog_Model_Type::SUBTYPE_ADDON,
            Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_ADDONS,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_GOODIES,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_REPLACEMENT_MATERIALS
        );
        if ($simcard) {
            return array_merge(
                array_slice($subtypes, 0, 1, true),
                array(Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD),
                array_slice($subtypes, 1, count($subtypes), true)
            );
        }

        return $subtypes;
    }

    /**
     * @return bool
     */
    public function hasSubscription($quote = null)
    {
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        if (!$quote) {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
        }

        foreach ($quote->getAllVisibleItems() as $item) {
            if (!$item->isDeleted()
                && $item->getProduct()->isSubscription()
                && $item->getPackageId() == $this->getPackageId()
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param null $quote
     * @return bool
     */
    public function hasHardwareWithoutSim($quote = null)
    {
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        if (!$quote) {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
        }

        $hasHardware = false;
        $subscription = false;
        foreach ($quote->getAllVisibleItems() as $item) {
            $itemProduct = $item->getProduct();
            if ($itemProduct->is(Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD)
                && $item->getPackageId() == $this->getPackageId()
            ) {
                return false;
            }

            if ($itemProduct->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE)
                && $item->getPackageId() == $this->getPackageId()
            ) {
                $hasHardware = true;
                $product = Mage::getModel('catalog/product')->load($itemProduct->getId());
                $simType = $product->getAttributeText(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE);
            }

            if ($itemProduct->isSubscription()
                && $item->getPackageId() == $this->getPackageId()
            ) {
                $subscription = $item;
            }
        }

        if ($hasHardware && $subscription) {
            return isset($simType) && Mage::helper('omnius_configurator/attribute')->checkSimTypeAvailable($subscription->getProduct(), $simType) ? $simType : false;
        }

        return false;
    }

    /**
     * @return bool|null
     */
    public function needsApprovePackage()
    {
        if ($this->_needsApprove === null) {
            $orderItems = $this->getPackageItems();
            $hasNetworkProduct = false;
            foreach ($orderItems as $orderItem) {
                /** @var Dyna_Catalog_Model_Product $product */
                $product = $orderItem->getProduct();
                if ($product->isNetwork() && $product->isNetworkProduct()) {
                    $hasNetworkProduct = true;
                }
            }
            $this->_needsApprove = $hasNetworkProduct;
        }

        return $this->_needsApprove;
    }

    /**
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @return bool
     */
    public function hasHollandsNieuwe($quote)
    {
        foreach ($quote->getAllVisibleItems() as $item) {
            if ($this->getPackageId() == $item->getPackageId()
                && $item->getProduct()->isNetworkProduct()
                && $item->getProduct()->isHollandsNieuweNetworkProduct()
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     *
     * Check if a package is payed
     */
    public function isPayed()
    {
        if ($this->isDelivered() || $this->wasDelivered()) {
            // @todo is this still needed or is the base_total_due set to 0 on cashondelivery
            return true;
        } else {
            if ($this->getOrderId()) {
                $deliveryOrders = Mage::getModel('sales/order')->getNonEditedOrderItems($this->getOrderId(), true, true);
                /** @var Dyna_Checkout_Model_Sales_Order $deliveryOrder */
                foreach ($deliveryOrders as $deliveryOrder) {
                    foreach ($deliveryOrder->getAllItems() as $item) {
                        if ($item->getPackageId() == $this->getPackageId()) {
                            if ($deliveryOrder->getBaseTotalDue() > 0) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    }
                }

                $so = Mage::getModel('superorder/superorder')->load($this->getOrderId());
                if ($so->hasParents()) {
                    return $this->wasPayed($so);
                }
            }

            return false;
        }
    }

    /**
     * @return array
     *
     * Get default package type
     */
    public function getDefaultPackageType()
    {
        $types = array(
            Vznl_Catalog_Model_Type::TYPE_MOBILE,
            Vznl_Catalog_Model_Type::TYPE_FIXED
        );

        return $types;
    }


    /**
     * @return string
     *
     * Get package status text
     */
    public function getFullStatusText() {
        if ($this->isFixed()) {
            return $this->getSuperorder()->getFixedSubStatusTranslated();
        }
        return $this->getStatus();
    }

    /**
     * Gets the most accurate phone number to use.
     * In order: current_number > tel_number > ctn
     * @param boolean $skipCurrentNumber <true> means that the current number will not be checked, <false> will mean it will be checked.
     * @return string|boolean The telephone number or false if none defined.
     */
    public function getTelephoneNumber($skipCurrentNumber = false)
    {
        if (!$skipCurrentNumber && $this->getCurrentNumber()) {
            $telephoneNumber = $this->getCurrentNumber();
        } elseif ($this->getTelNumber()) {
            $telephoneNumber = $this->getTelNumber();
        } else {
            $telephoneNumber = $this->getCtn();
        }

        return $telephoneNumber;
    }


    /**
     * Set the extra lines (promo, sim) info to the items
     *
     * @param array $items
     * @param string $affected
     * @param array $changedPackagesIds
     * @param int $affected_id
     */
    protected function setExtraLines($items, $affected, $changedPackagesIds, $affected_id = null)
    {
        $old_class = $old_price_class = '';
        $affectedItems = $affected . 'Items';
        $flippedChangedPackagesIds = array_flip($changedPackagesIds);

        foreach ($items as $item) {
            $itemPackageId = $item->getPackageId();
            if (isset($flippedChangedPackagesIds[$itemPackageId])) {
                $old_class = ' old_package';
                $old_price_class = ' class="striked old_package"';
            }
            if ($item->getProduct()->isSim() && $item->getPackageId() == $this->getPackageId()) {
                if (!$this->getIsExtendedCart()) {
                    $itemProps = json_decode($item->getData('order_item_prices'), true);
                    $prodPrice = $itemProps['product']['price'];
                } else {
                    $prodPrice = $item->getProduct()->getPrice();
                }

                if (!$affected_id) {
                    $affected_id = $item->getData($affected . '_id');
                }
                $this->{$affectedItems}[$affected_id]['image'][] = '';
                $this->{$affectedItems}[$affected_id]['name'][] = '<span class="orderline-section-table-subline' . $old_class . '">' . $item->getName() . '</span>';
                $this->{$affectedItems}[$affected_id]['maf'][] = '<label></label>';
                $this->{$affectedItems}[$affected_id]['price'][] = '<label' . $old_price_class . '>' . Mage::helper('core')->currency($prodPrice, true, false) . '</label>';
            }
        }
    }

    /**
     * Determines whether or not current package is of type mobile
     * @return bool
     */
    public function isMobile()
    {
        return in_array(strtolower($this->getType()), Vznl_Catalog_Model_Type::getMobilePackages());
    }

    /**
     * Determines whether or not current package has completed credit check step
     * @return bool
     */
    public function creditCheckCompleted()
    {
        if (!in_array($this->getCreditcheckStatus(), [Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED, Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL])) {
            return false;
        }
        return true;
    }

    /**
     * Determines whether current package has referred or more-info-needed status
     * @return bool
     */
    public function creditCheckReferredOrMoreInfoNeeded() {
        if (!in_array($this->getCreditcheckStatus(), [Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED, Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED])) {
            return false;
        }
        return true;
    }

    /**
     * Determines whether current package has referred status
     * @return bool
     */
    public function creditCheckReferred() {
        return $this->getCreditcheckStatus() == Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED;
    }

    /**
     * Determines whether or not current package is of type hardware
     * @return bool
     */
    public function isHardware()
    {
        return strtolower($this->getType()) == strtolower(Vznl_Catalog_Model_Type::TYPE_HARDWARE);
    }

    /**
     * Get package_type_id of current package
     * @return mixed|null
     */
    public function getPackageTypeId()
    {
        return Mage::getModel('dyna_package/packageType')->getPackageTypeIdByCode($this->getType());
    }

    /**
     * Determine whether or not current package is part of a MobileFixed bundle
     *
     * @return bool
     */
    public function isPartOfMobileFixed()
    {
        foreach ($this->getBundles() as $bundle) {
            if ($bundle->isMobileFixed()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return whether or not package is ILS
     * @return bool
     */
    public function isILS()
    {
        return in_array($this->getSaleType(), array_merge(Vznl_Catalog_Model_ProcessContext::ILSProcessContexts()));
    }

    /**
     * Return whether or not package is Move
     * @return bool
     */
    public function isMove()
    {
        return in_array($this->getSaleType(), array_merge(Vznl_Catalog_Model_ProcessContext::MoveProcessContexts()));

    }

    /**
     * Get One time charges of old Package
     * @param int $parentId
     * @param int $packageId
     */
    public function getOldPackageOneTimeCharge($parentId, $packageId)
    {
        $subtotalPrice = 0;
        $subtotalMaf = 0;
        $taxPrice = 0;
        $taxMaf = 0;
        $total = 0;
        $totalMaf = 0;
        $noPromoMaf = 0;
        $noPromoTax = 0;
        $additionalSubtotal = 0;
        $additionalTotal = 0;
        $additionalTax = 0;

        $allOrderItems = Mage::getModel('sales/order_item')
            ->getCollection()
            ->addAttributeToFilter('order_id', $parentId)
            ->addAttributeToFilter('package_id', $packageId)
            ->load();

        foreach ($allOrderItems as $item) {
            $this->parsePricesForItem($item, $total, $subtotalPrice, $noPromoMaf, $noPromoTax, $subtotalMaf, $taxMaf, $taxPrice, $totalMaf, $additionalSubtotal, $additionalTax, $additionalTotal);
        }
        return [
            'subtotal_price' => $this->roundFloat($subtotalPrice),
            'subtotal_maf' => $this->roundFloat($subtotalMaf),
            'no_promo_maf' => $this->roundFloat($noPromoMaf),
            'no_promo_maf_tax' => $this->roundFloat($noPromoTax),
            'no_promo_tax_amount' => $this->roundFloat($noPromoTax - $noPromoMaf),
            'tax_price' => $this->roundFloat($taxPrice),
            'tax_maf' => $this->roundFloat($taxMaf),
            'total' => $this->roundFloat($total),
            'total_maf' => $this->roundFloat($totalMaf),
            'additional_subtotal' => $this->roundFloat($additionalSubtotal),
            'additional_total' => $this->roundFloat($additionalTotal),
            'additional_tax' => $this->roundFloat($additionalTax),
        ];
    }

    /**
     * Return packages that have cancelled type
     * @return array
     */
    public static function getCancelledTypePackages():array {
        return [
            self::ESB_PACKAGE_STATUS_CANCELLED,
            self::ESB_PACKAGE_STATUS_PORTING_CANCELLED
        ];
    }

}
