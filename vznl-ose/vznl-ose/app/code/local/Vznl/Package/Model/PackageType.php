<?php

/**
 * Class Vznl_Package_Model_PackageType
 */
class Vznl_Package_Model_PackageType extends Dyna_Package_Model_PackageType
{
    /**
     * @return array|mixed
     */
    public function getAllPackageTypesForForm()
    {
        $packageTypes = $this->getAllPackageTypesIdCodePairs();

        // cache key
        $key = 'package_types_form_names_' . md5(serialize(array(__CLASS__, __METHOD__)));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            foreach ($packageTypes as $packageTypeId => $packageTypeCode) {
                $result[] = array(
                    'label' => $packageTypeCode,
                    'value' => $packageTypeId
                );
            }

            $this->getCache()->save(
                serialize($result),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );

            return $result;
        }
    }

    /**
     * @return mixed
     */
    public function getAllPackageTypesIdCodePairs()
    {
        // cache key
        $key = 'package_type_names_' . md5(serialize(array(__CLASS__, __METHOD__)));

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $packageTypes = $this->getCollection();
            foreach ($packageTypes as $packageType) {
                $result[$packageType->getId()] = $packageType->getPackageCode();
            }

            $this->getCache()->save(
                serialize($result),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );

            return $result;
        }
    }
}
