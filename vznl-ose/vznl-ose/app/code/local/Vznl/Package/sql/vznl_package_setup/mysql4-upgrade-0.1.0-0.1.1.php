<?php

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$packageTable = $this->getTable('package/package');

if (!$this->getConnection()->tableColumnExists($packageTable, 'applied_promos_order')) {
    $this->getConnection()
        ->addColumn(
            $packageTable,
            'applied_promos_order',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'The selection order of manually (stackable) applied promo rules',
                'nullable' => true,
                'default' => null
            )
        );
}

$installer->endSetup();

