<?php
/**
 * Created by PhpStorm.
 * User: keerthism
 * Date: 17/09/19
 * Time: 12:19 PM
 */

$installer = $this;
$installer->startSetup();

$packageTable = $this->getTable('package/package');

if (!$this->getConnection()->tableColumnExists($packageTable, 'is_offer')) {
    $this->getConnection()->addColumn($packageTable, 'is_offer', "TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Package is added from offer or not'");
}

$installer->endSetup();