<?php

/**
 * Class Vznl_Superorder_Model_Errors
 */
class Vznl_Superorder_Model_Errors extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('superorder/errors');
    }
}