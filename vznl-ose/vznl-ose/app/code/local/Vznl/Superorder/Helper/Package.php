<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_Superorder_Helper_Package
 */
class Vznl_Superorder_Helper_Package extends Omnius_Superorder_Helper_Package
{
    /**
     * @param $superOrder
     * @param $packageModel
     * @return mixed
     */
    public function getPackageAvailability($superOrder, $packageModel)
    {
        $orders = $superOrder->getOrders();
        $deliveryInThisStore = array();
        $session = Mage::getSingleton('customer/session');
        $agent = $session->getAgent(true);
        $packageItems = [];
        /** @var Dyna_Checkout_Model_Sales_Order $deliveryOrder */
        foreach ($orders as $deliveryOrder) {
            $packageIds = $deliveryOrder->getPackageIds();
            $flippedPackageIds = array_flip($packageIds);
            // Check if the order has delivery in this retail store
            if ($deliveryOrder->isStoreDelivery()
                && $deliveryOrder->getShippingAddress()
                && $deliveryOrder->getShippingAddress()->getDeliveryStoreId() == $agent->getDealer()->getId()
            ) {
                $deliveryInThisStore = array_merge($deliveryInThisStore, $packageIds);
            }
            // If the package belongs to this delivery order, also get the items
            $packageId = $packageModel->getPackageId();
            if (isset($flippedPackageIds[$packageId])) {
                foreach ($deliveryOrder->getAllItems() as $item) {
                    if ($item->getPackageId() == $packageModel->getPackageId()) {
                        $packageItems[] = $item;
                    }
                }
            }
        }
        Mage::register('isIndirect', Mage::helper('vznl_agent')->isIndirectStore());
        Mage::register('creditCheckPerformed', $superOrder->creditCheckCompleted());
        /** @var Dyna_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('dyna_checkout');
        $isLocked = $checkoutHelper->checkOrder($superOrder->getId(), false) === true;
        $html = Mage::getSingleton('core/layout')
            ->createBlock('core/template')
            ->setPackage($packageModel)
            ->setItems($packageItems)
            ->setTemplate('checkout/cart/steps/edit_order_package.phtml')
            ->setModified(false)
            ->setModifiedPackageIds([])
            ->setEditedPackageStatus(null)
            ->setCancelledPackageIds([])
            ->setIsLocked($isLocked)
            ->setSuperOrder($superOrder)
            ->setIsDeliveredEdited($packageModel->getNewPackageId())
            ->setDeliveryInThisStore(in_array($packageModel->getPackageId(), $deliveryInThisStore))
            ->setOnlyShowButtons(true)
            ->toHtml();
        Mage::unregister('isIndirect');
        Mage::unregister('creditCheckPerformed');

        return $html;
    }
}