<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

$installer->startSetup();

if ($installer->tableExists('superorder')) {

    $connection
        ->addIndex(
            'superorder',
            "IDX_DYNA_SUPERORDER_ORDER_STATUS",
            ['order_status'],
            Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
        );

}

if ($installer->tableExists('sales_flat_order')) {

    $connection
        ->addForeignKey(
            $this->getFkName('sales_flat_order', 'superorder_id', 'superorder', 'entity_id'),
            'sales_flat_order',
            'superorder_id',
            'superorder',
            'entity_id',
            Varien_Db_Ddl_Table::ACTION_SET_NULL,
            Varien_Db_Ddl_Table::ACTION_CASCADE
        );

}

$installer->endSetup();
