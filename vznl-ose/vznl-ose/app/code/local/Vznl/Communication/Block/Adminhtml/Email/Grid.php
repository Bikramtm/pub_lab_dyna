<?php

/**
 * Class Vznl_Communication_Block_Adminhtml_Email_Grid
 */
class Vznl_Communication_Block_Adminhtml_Email_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('emailGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('communication/job_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('communication/job')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'type' => 'number',
            'index' => 'id',
        ));

        $this->addColumn('email_code', array(
            'header' => Mage::helper('communication/job')->__('Email Code'),
            'index' => 'email_code',
        ));

        $this->addColumn('receiver', array(
            'header' => Mage::helper('communication/job')->__('Receiver'),
            'index' => 'receiver',
            //'frame_callback' => array($this, 'unserializeParams'),
            //'column_css_class' => 'contained-column'
        ));

        $this->addColumn('deadline', array(
            'header' => Mage::helper('communication/job')->__('Deadline'),
            'index' => 'deadline',
            'type' => 'datetime',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('communication/job')->__('Created At'),
            'index' => 'created_at',
            'default' => 'N/A',
            'type' => 'datetime',
        ));

        $this->addColumn('executed_at', array(
            'header' => Mage::helper('communication/job')->__('Executed At'),
            'index' => 'executed_at',
            'default' => 'N/A',
            'type' => 'datetime',
        ));

        $this->addColumn('finished_at', array(
            'header' => Mage::helper('communication/job')->__('Finished At'),
            'index' => 'finished_at',
            'default' => 'N/A',
            'type' => 'datetime',
        ));

        $this->addColumn('messages', array (
            'header' => Mage::helper('communication/job')->__('Messages'),
            'index' => 'messages',
            'frame_callback' => array($this, 'extractMessages'),
        ));

        $this->addColumn('tries', array (
            'header' => Mage::helper('communication/job')->__('Failed Tries'),
            'index' => 'tries',
        ));

        $this->addColumn('status', array (
            'header' => Mage::helper('communication/job')->__('Status'),
            'index' => 'status',
            'frame_callback' => array($this, 'decorateStatus'),
            'type' => 'options',
            'options' => array(
                Vznl_Communication_Model_Job::STATUS_PENDING => Vznl_Communication_Model_Job::STATUS_PENDING,
                Vznl_Communication_Model_Job::STATUS_SUCCESS => Vznl_Communication_Model_Job::STATUS_SUCCESS,
                Vznl_Communication_Model_Job::STATUS_ERROR => Vznl_Communication_Model_Job::STATUS_ERROR,
                Vznl_Communication_Model_Job::STATUS_RUNNING => Vznl_Communication_Model_Job::STATUS_RUNNING,
            )
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return '#';
    }

    /**
     * @param $messages
     * @param Vznl_Communication_Model_Job $email
     * @return string
     */
    public function extractMessages($messages, Vznl_Communication_Model_Job $email)
    {
        if ( ! count($email->getMessages())) {
            return Mage::helper('core')->__('N/A');
        }
        $content = "";
        $maxLines = 20; //max lines that will be shown
        foreach ($email->getMessages() as $message) {
            if ($maxLines <= 0) {
                $content .= '<span class="log-message">...</span>';
                break;
            }
            $message = $this->prepareMessage($message);
            if (strlen($message) > 350) {
                $message = substr($message, 0, 350) . '...';
            }
            $content .= sprintf('<span class="log-message">%s</span><br/>', $message);
            --$maxLines;
        }
        return $content;
    }

    /**
     * @param $message
     * @return string
     */
    protected function prepareMessage($message)
    {
        preg_match_all('/(?<=\[)[^]]+(?=\])/', $message, $parts);
        if (isset($parts[0]) && count($parts[0]) === 2) {
            $date = $parts[0][0];
            $type = $parts[0][1];
            $tr = array(
                sprintf('[%s]', $date) => sprintf('<span class="date-part">[%s]</span>', $date),
                sprintf('[%s]', $type) => sprintf('<span class="type-part type-part-%s">[%s]</span>', strtolower($type), $type),
            );
            return strtr($message, $tr);
        }
        return $message;
    }

    /**
     * @param $status
     * @return string
     */
    public function decorateStatus($status) {
        switch ($status) {
            case Vznl_Communication_Model_Job::STATUS_SUCCESS:
                $result = '<span class="bar-green"><span>'.$status.'</span></span>';
                break;
            case Vznl_Communication_Model_Job::STATUS_PENDING:
                $result = '<span class="bar-lightgray"><span>'.$status.'</span></span>';
                break;
            case Vznl_Communication_Model_Job::STATUS_RUNNING:
                $result = '<span class="bar-yellow"><span>'.$status.'</span></span>';
                break;
            case Vznl_Communication_Model_Job::STATUS_ERROR:
                $result = '<span class="bar-red"><span>'.$status.'</span></span>';
                break;
            default:
                $result = $status;
                break;
        }
        return $result;
    }

    public function unserializeParams($encoded)
    {
        $parameters = json_decode(html_entity_decode($encoded), true);
        if (is_array($parameters)) {
            return '<pre>'. utf8_encode(htmlentities(var_export($parameters, true))) .'</pre>';
        }
        return htmlentities("array(\n)");
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_email', array(
            'label' => Mage::helper('communication/job')->__('Remove Email'),
            'url' => $this->getUrl('*/adminhtml_email/massRemove'),
            'confirm' => Mage::helper('communication/job')->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('process_email', array(
            'label' => Mage::helper('communication/job')->__('Process Email'),
            'url' => $this->getUrl('*/adminhtml_email/massProcess'),
            'confirm' => Mage::helper('communication/job')->__('Are you sure?')
        ));
        return $this;
    }
}
