<?php

/**
 * Class Vznl_Communication_Adminhtml_EmailController
 */
class Vznl_Communication_Adminhtml_EmailController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('communication/job')->_addBreadcrumb(Mage::helper('adminhtml')->__('Emails Manager'), Mage::helper('adminhtml')->__('Emails Manager'));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('Email'));
        $this->_title($this->__('Emails Manager'));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__('Edit Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('communication/job')->load($id);
        if ($model->getId()) {
            Mage::register('email_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('communication/job');
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Emails Manager'), Mage::helper('adminhtml')->__('Emails Manager'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('communication/adminhtml_email_edit'))->_addLeft($this->getLayout()->createBlock('communication/adminhtml_email_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('communication/job')->__('Item does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_title($this->__('New Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('communication/job')->load($id);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('email_data', $model);

        $this->loadLayout();
        $this->_setActiveMenu('communication/email');

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Emails Manager'), Mage::helper('adminhtml')->__('Emails Manager'));

        // Commented away the addLeft since this doesn't work since the block is missing
        $this->_addContent($this->getLayout()->createBlock('communication/adminhtml_email_edit'));//->_addLeft($this->getLayout()->createBlock('communication/adminhtml_email_edit_tabs'));

        $this->renderLayout();
    }

    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();
        if ($post_data) {
            try {
                $model = Mage::getModel('communication/job')
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam('id'))
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Email was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setReleaseData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setEmailData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

        }
        $this->_redirect('*/*/');
    }


    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('communication/job');
                $model->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }


    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel('communication/job');
                $model->setId($id)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item(s) was successfully removed'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    public function massProcessAction()
    {
        try {
            /** @var Vznl_Communication_Model_Cron $cron */
            $cron = Mage::getSingleton('communication/cron');
            $ids = array_filter(array_map('intval', $this->getRequest()->getPost('ids', array())));
            $cron->processJobs($ids);
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Emails were successfully processed'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'emails.csv';
        $grid = $this->getLayout()->createBlock('communication/adminhtml_email_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'email.xml';
        $grid = $this->getLayout()->createBlock('communication/adminhtml_email_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }
}

