<?php

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Zend\Soap\Client;

/**
 * Class Vznl_Communication_Adapter_Sms_SendOutBoundNotificationAdapter
 */
class Vznl_Communication_Adapter_Sms_SendOutBoundNotificationAdapter implements Vznl_Communication_Adapter_AdapterInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Exception
     */
    private $exception;

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var Vznl_Communication_Model_Job
     */
    private $job;

    public function __construct(Client $client, LoggerInterface $logger, array $options=[])
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->options = array_merge($this->options, $options);
    }

    /**
     * Get the last exception
     * @return Exception
     */
    public function getLastException()
    {
        return $this->exception;
    }

    /**
     * Send data towards BSL's SendOutBoundNotification service
     * @param Vznl_Communication_Model_Job $job
     * @return bool <true> if successful, <false> if something completely went wrong
     */
    public function send(Vznl_Communication_Model_Job $job)
    {
        $this->job = $job;
        $requestData = $this->buildRequest();

        ini_set('default_socket_timeout', isset($this->options['timeout']) ? $this->options['timeout'] : 10);
        try {
            $this->client->setLocation(isset($this->options['endpoint']) ? $this->options['endpoint'] : '');
            $this->client->addSoapInputHeader($this->buildBslRequestHeader());
            $this->client->addSoapInputHeader($this->buildWorkContextRequestHeader());
            $this->client->sendOutBoundNotification($requestData);
            $this->log();
        } catch(Exception $e) {
            $this->log($e);
            $this->exception = $e;
            return false;
        }
        return true;
    }

    /**
     * Create the work context request header
     * @param bool $asString Whether or not the response should be a xml string.
     * @return SoapHeader|string The request header
     */
    protected function buildWorkContextRequestHeader($asString = false)
    {
        $root = new SimpleXMLElement('<root/>');
        $workContext = 'rO0ABXf7ABh3ZWJsb2dpYy5hcHAudXJhcGktdmxpdmUAAADWAAAAI3dlYmxvZ2ljLndvcmthcmVhLlN0cmluZ1dvcmtDb250ZXh0AAcyLjE2LjI5ACZ3ZWJsb2dpYy5kaWFnbm9zdGljcy5EaWFnbm9zdGljQ29udGV4dAAAAX8AAAAyd2VibG9naWMuZGlhZ25vc3RpY3MuY29udGV4dC5EaWFnbm9zdGljQ29udGV4dEltcGwAAAA8NTg0NjU1NDg3ZGIwODMxODoyMDg4YzY2ZToxNTBiMDc1NDQwODotODAwMC0wMDAwMDAwMDAwMDZlNGNmAAAAAgAAAAAAAAA=';
        $workHeader = $root->addChild('work:WorkContext', $workContext, 'http://oracle.com/weblogic/soap/workarea/');

        if($asString){
            return $workHeader->asXML();
        }
        $var = new SoapVar($workHeader->asXML(), XSD_ANYXML);
        $soapHeader = new SOAPHeader('http://oracle.com/weblogic/soap/workarea/', 'WorkContext', $var);
        return $soapHeader;
    }

    /**
     * Build the bsl request headers
     * @param bool $asString Whether or not the response should be a xml string.
     * @return SOAPHeader|string The request header
     */
    protected function buildBslRequestHeader($asString = false)
    {
        $dealerCode = $this->getJobParameter('dealer_code');

        $header = new Omnius_Core_Model_SimpleDOM('<root></root>');
        /** @var Omnius_Core_Model_SimpleDOM $bslHeader */
        $bslHeader = $header->addChild('ns2:BSLHeader', null, 'http://com.amdocs.bss.bsl/');

        $headerParams = new Omnius_Core_Model_SimpleDOM('<root></root>');
        $headerParams->addChild('Username', isset($this->options['username']) ? $this->options['username'] : '');
        $headerParams->addChild('Password', isset($this->options['password']) ? $this->options['password'] : '');
        $headerParams->addChild('MessageCreationTimestamp', date('c'));
        $headerParams->addChild('DealerCode', $dealerCode ?: '0');
        $headerParams->addChild('MessageID', str_replace('.', '', microtime(true)));
        $headerParams->addChild('EndUserName', isset($this->options['enduser']) ? $this->options['enduser'] : '');

        $bslHeader->cloneChildrenFrom($headerParams);

        if($asString){
            return $bslHeader->asXML();
        }
        $var = new SoapVar($bslHeader->asXML(), XSD_ANYXML);
        return new SOAPHeader('http://com.amdocs.bss.bsl/', 'BSLHeader', $var);
    }

    /**
     * Build the request
     * @return array The request data
     */
    protected function buildRequest()
    {
        $notificationTargets = [];
        $notificationTargets[] = [
            'entityType' => 'SB',
            'entityID' => $this->job->getReceiver(),
            'overrideMethod' => [
                'deliveryMethod' => 'SMS',
                'targetResource' => $this->job->getReceiver(),
            ]
        ];

        $templateParameters = [];
        $jobTps = $this->getJobParameter('template_parameters');
        foreach ($jobTps as $key => $value) {
            $templateParameters[] = [
                'key' => $key,
                'value' => $value
            ];
        }

        return [
            'notificationTargets' => $notificationTargets,
            'templateName' => $this->job->getEmailCode(),
            'templateParameters' => $templateParameters
        ];
    }

    /**
     * Log the request/response of the actual performed call
     * @param null|Exception $e The exception if any
     */
    protected function log($e=null)
    {
        $log = "\n";
        $log .= sprintf("Request headers:\n%s\n", $this->client->getLastRequestHeaders());
        $log .= sprintf("Request:\n%s\n", $this->format($this->client->getLastRequest()));
        $log .= sprintf("Response headers:\n%s\n", $this->client->getLastResponseHeaders());
        $responseMessage = $this->client->getLastResponse();
        $responseMessage = $responseMessage ? $this->format($responseMessage) : $responseMessage;
        if($e != null){
            if(empty($responseMessage)){
                $responseMessage = $e->getMessage()."\n";
                $responseMessage .= $e->getTraceAsString();
            }
        }
        $log .= sprintf("Response:\n%s", $responseMessage);
        $this->logger->log(LogLevel::DEBUG, $log);
    }

    /**
     * Format the given xml string
     * @param $mixed The xml string
     * @return string The formatted xml string
     */
    protected function format($mixed)
    {
        try {
            $dom = new DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = false;
            if (!empty($mixed)) {
                $dom->loadXML($mixed);
            }
            return preg_replace("/\n/", "", $dom->saveXML());
        }
        catch (Exception $e){
            Mage::logException($e);
            return $mixed;
        }
    }

    /**
     * Gets the parameter value from the job
     * @param $name The parameter to find
     * @return mixed The value
     */
    protected function getJobParameter($name)
    {
        $parameters = $this->job->getParameters();
        return isset($parameters[$name]) ? $parameters[$name] : '';
    }
}
