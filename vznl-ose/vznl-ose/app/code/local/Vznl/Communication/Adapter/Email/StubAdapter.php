<?php

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Class Vznl_Communication_Adapter_Email_StubAdapter
 */
class Vznl_Communication_Adapter_Email_StubAdapter implements Vznl_Communication_Adapter_AdapterInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Vznl_Communication_Adapter_Email_StubAdapter constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Vznl_Communication_Model_Job $job
     * @return bool
     */
    public function send(Vznl_Communication_Model_Job $job)
    {
        $request = var_export($job->getData(), true);
        if ($job->getReceiver() === 'no@email.com') {
            $response = false;
        } else{
            $response = true;
        }
        $this->log($request, $response);
        return $response;
    }

    /**
     * @return null|Exception
     */
    public function getLastException()
    {
        return null;
    }

    /**
     * @param $request
     * @param $response
     */
    protected function log($request, $response)
    {
        $log = "\n";
        $log .= sprintf("Request headers:\n%s", '');
        $log .= sprintf("Request:\n%s\n\n", $request);
        $log .= sprintf("Response headers:\n%s", '');
        $log .= sprintf("Response:\n%s", 'Stubbed call which returned: '.$response ? 'true' : 'false');
        $this->logger->log(LogLevel::DEBUG, $log);
    }
}
