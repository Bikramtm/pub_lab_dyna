<?php

use Psr\Log\LoggerInterface;

/**
 * Class Vznl_Communication_Adapter_Email_SMTPeterAdapterFactory
 */
class Vznl_Communication_Adapter_Email_SMTPeterAdapterFactory implements Vznl_Communication_Adapter_AdapterFactoryInterface
{
    /**
     * Create an adapter with the given variables
     * @param string $wsdl The wsdl
     * @param LoggerInterface $logger The logger to use
     * @param array $options An array with options
     * @return Vznl_Communication_Adapter_Email_SMTPeterAdapter The adapter
     */
    public static function create(array $options) : Vznl_Communication_Adapter_AdapterInterface
    {
        $options = array_merge(self::getOptions(), $options);
        return new Vznl_Communication_Adapter_Email_SMTPeterAdapter($options['logger'], $options);
    }

    /**
     * @return array
     */
    public static function getOptions() : array
    {
        return array(
            'access_token' => Mage::getStoreConfig('vodafone_service/communication_email/token'),
            'endpoint' => Mage::getStoreConfig('vodafone_service/communication_email/usage_url'),
            'proxy_usage' => Mage::getStoreConfig('vodafone_service/communication_email/proxy_usage'),
            'proxy_url' => Mage::getStoreConfig('vodafone_service/communication_email/proxy_url'),
            'proxy_port' => Mage::getStoreConfig('vodafone_service/communication_email/proxy_port'),
            'ssl_peer' => Mage::getStoreConfig('vodafone_service/communication_email/ssl_peer'),
        );
    }
}
