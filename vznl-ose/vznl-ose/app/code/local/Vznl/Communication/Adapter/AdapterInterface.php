<?php
/**
 * Interface AdapterInterface
 * @package Vznl\Communication\Model\Adapter
 */
interface Vznl_Communication_Adapter_AdapterInterface
{
    public function send(Vznl_Communication_Model_Job $job);

    public function getLastException();
}
