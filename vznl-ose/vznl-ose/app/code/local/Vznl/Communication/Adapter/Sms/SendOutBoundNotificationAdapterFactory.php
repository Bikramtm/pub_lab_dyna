<?php

use Psr\Log\LoggerInterface;
use Zend\Soap\Client;

/**
 * Class Vznl_Communication_Adapter_Sms_SendOutBoundNotificationAdapterFactory
 */
class Vznl_Communication_Adapter_Sms_SendOutBoundNotificationAdapterFactory implements Vznl_Communication_Adapter_AdapterFactoryInterface
{
    /**
     * Create an adapter with the given variables
     * @param string $wsdl The wsdl
     * @param LoggerInterface $logger The logger to use
     * @param array $options An array with options
     * @return Vznl_Communication_Adapter_Sms_SendOutBoundNotificationAdapter The adapter
     */
    public static function create(array $options) : Vznl_Communication_Adapter_AdapterInterface
    {
        $options = array_merge(self::getOptions(), $options);
        $soapClient = new Client($options['wsdl'], self::getClientOptions());
        return new Vznl_Communication_Adapter_Sms_SendOutBoundNotificationAdapter($soapClient, $options['logger'], $options);
    }

    /**
     * @return array
     */
    public static function getOptions() : array
    {
        return array(
            'wsdl' => Mage::getStoreConfig('vodafone_service/communication_sms/wsdl'),
            'endpoint' => Mage::getStoreConfig('vodafone_service/communication_sms/usage_url'),
            'timeout' => Mage::getStoreConfig('vodafone_service/communication_sms/timeout'),
            'username' => Mage::getStoreConfig('vodafone_service/communication_sms/username'),
            'password' => Mage::getStoreConfig('vodafone_service/communication_sms/password'),
        );
    }

    /**
     * Gets the client options for this service.
     * @return array The option array
     */
    protected static function getClientOptions() : array
    {
        $options = Mage::helper('vznl_core/service')->getZendSoapClientOptions('communication_sms') ? : [];
        $options['soap_version'] = SOAP_1_2;

        return $options;
    }
}
