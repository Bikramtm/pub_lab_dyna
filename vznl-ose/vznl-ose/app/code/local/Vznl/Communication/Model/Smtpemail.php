<?php
class Vznl_Communication_Model_Smtpemail extends Mage_Core_Model_Email {

    public function send() {

        $_helper = Mage::helper('communication/smtp');

        // If it's not enabled, just return the parent result.
        if (!$_helper->isEnabled()) {
            return parent::send();
        }

        $mail = new Zend_Mail();

        if (strtolower($this->getType()) == 'html') {
            $mail->setBodyHtml($this->getBody());
        } else {
            $mail->setBodyText($this->getBody());
        }

        $mail->setFrom($this->getFromEmail(), $this->getFromName())
            ->addTo($this->getToEmail(), $this->getToName())
            ->setSubject($this->getSubject());

        if ($attachements = $this->getAttachments()) {
            if (is_array($attachements)) {
                foreach ($attachements as $attachement) {
                    $mail->addAttachment($attachement);
                }
            } else {
                $mail->addAttachment($attachements);
            }
        }

        $transport = new Varien_Object(); // for observers to set if required
        Mage::dispatchEvent('email_smtp_before_send', array(
            'mail' => $mail,
            'email' => $this,
            'transport' => $transport
        ));

        if ($transport->getTransport()) { // if set by an observer, use it
            $mail->send($transport->getTransport());
        } else {
            $mail->send();
        }

        Mage::dispatchEvent('email_smtp_after_send', array(
            'to' => $this->getToName(),
            'subject' => $this->getSubject(),
            'template' => "n/a",
            'html' => (strtolower($this->getType()) == 'html'),
            'email_body' => $this->getBody()));

        $mail->clearRecipients();

        return $this;
    }
}