<?php
class Vznl_Communication_Model_System_Config_Source_Smtp_Option
{
    public function toOptionArray()
    {
        return array(
            "disabled"   => Mage::helper('adminhtml')->__('Disabled'),
            "smtp"   => Mage::helper('adminhtml')->__('SMTP')
        );
    }
}