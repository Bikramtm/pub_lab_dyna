<?php

class Vznl_Communication_Model_Log extends Mage_Core_Model_Abstract
{
    const TYPE_EMAIL = 'email';
    const TYPE_SMS = 'sms';

    protected function _construct()
    {
       $this->_init("communication/log");
    }
}
