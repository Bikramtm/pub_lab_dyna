<?php

/**
 * Class Vznl_Communication_Model_Pool
 */
class Vznl_Communication_Model_Pool
{
    const EMAIL_LOG = 'email_payload.log';
    /**
     * @param array|Vznl_Communication_Model_Job $email
     */
    public function add($email, $websiteCode = null)
    {
        $websiteCode = $websiteCode ?: Mage::app()->getWebsite()->getCode();
        if (is_array($email)) {
            if (!isset($email['email_code']) || !isset($email['receiver'])) {
                Mage::log('Missing email code or receiver');
                Mage::log(mageDebugBacktrace(true, false, true), null, 'EmailErrorLog.log', true);
                Mage::log(var_export($email, true), null, 'EmailErrorLog.log', true);
            } else {
                if ($this->checkEmailCodeForIndirect($email['email_code'], $websiteCode)) {
                    Mage::getModel('communication/job')
                        ->addData($email)
                        ->save();
                } else {
                    Mage::log('Email not added to pool because detected as Indirect or email code is not allowed', null, 'EmailErrorLog.log', true);
                    Mage::log(var_export($email, true), null, 'EmailErrorLog.log', true);
                }
            }
        } elseif ($email instanceof Vznl_Communication_Model_Job) {
            if ($this->checkEmailCodeForIndirect($email->getTemplateCode(), $websiteCode)) {
                $email->save();
            }
        } else {
            Mage::throwException(sprintf('Invalid argument provided. Expected either an array or an instance of Vznl_Communication_Model_Job, got %s.', gettype($email)));
        }
    }

    /**
     * Checks if email is eligible to be sent.
     * @param $code
     * @return bool
     */
    private function checkEmailCodeForIndirect($code, $websiteCode)
    {
        if (
            $websiteCode !== Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE
            ||
            (
                in_array(
                    $code,
                    array(
                        Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_PASS_RESET,
                        Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_PASS_AGENT_RESET,
                        Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_INDIRECT_CONTRACT,
                        Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_INDIRECT_CONTRACT_BUSINESS,
                        Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ILT_FILL_IN_LATER
                    )
                )
            )
        ) {
            return true;
        } else {
            return false;
        }

    }
}