<?php
class Vznl_Communication_Model_Transports_Smtp extends Vznl_Communication_Model_Transports_Basesmtp {

    public function getName($storeId) {
        return "Custom SMTP";
    }
    public function getEmail($storeId) {
        return Mage::helper('communication/smtp')->getSMTPSettingsUsername($storeId);
    }
    public function getPassword($storeId) {
        return Mage::helper('communication/smtp')->getSMTPSettingsPassword($storeId);
    }
    public function getHost($storeId) {
        return Mage::helper('communication/smtp')->getSMTPSettingsHost($storeId);
    }
    public function getPort($storeId) {
        return Mage::helper('communication/smtp')->getSMTPSettingsPort($storeId);
    }
    public function getAuth($storeId) {
        return Mage::helper('communication/smtp')->getSMTPSettingsAuthentication($storeId);
    }
    public function getSsl($storeId) {
        return Mage::helper('communication/smtp')->getSMTPSettingsSSL($storeId);
    }
}