<?php

/**
 * Class Vznl_Communication_Model_Job
 *
 * @method string getEmailId()
 * @method string getTemplateCode()
 * @method string getTemplateId()
 * @method string getLocale()
 * @method string getReceiver()
 * @method string getStatus()
 * @method Vznl_Communication_Model_Job setStatus()
 * @method Vznl_Communication_Model_Job setFinishedAt()
 * @method Vznl_Communication_Model_Job setExecutedAt()
 */
class Vznl_Communication_Model_Job extends Mage_Core_Model_Abstract
{
    const STATUS_PENDING = 'pending';
    const STATUS_RUNNING = 'running';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    const MESSAGE_INFO = 'INFO';
    const MESSAGE_ERROR = 'ERROR';
    const MESSAGE_NOTICE = 'NOTICE';
    const MESSAGE_SUCCESS = 'SUCCESS';

    const TYPE_EMAIL = 'email';
    const TYPE_SMS = 'sms';

    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_ACQ_NON_PORTING = 'VodaP85OrderConfirmAcqNonPorting';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_ACQ_PORTING = 'VodaP85OrderConfirmAcqPorting';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_RET = 'VodaP85OrderConfirmRet';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_RET_SO = 'VodaP85OrderConfirmRet_SO';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_ACQ_FIXED = 'VodaP85OrderConfirmAcqFixed';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRMATION = 'VodaP85OrderConfirmation';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRMATION_CREATE = 'VodaP85OrderConfirmation_Create';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRMATION_CHANGE = 'VodaP85OrderConfirmation_Change';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRMATION_OMNICHANNEL = 'VodaP85OrderConfirmation_OmniChannel';

    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_COD = 'VodaP85OrderConfirmChangeBeforeCOD';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_COD_POSITIVE = 'VodaP85OrderConfirmChangeBeforeCODPos';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_COD_NEGATIVE = 'VodaP85OrderConfirmChangeBeforeCODNeg';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEAFTER_COD_POSITIVE = 'VodaP85OrderConfirmChangeAfterCODPos';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEAFTER_COD_NEGATIVE = 'VodaP85OrderConfirmChangeAfterCODNeg';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_PAYONBILL = 'VodaP85OrderConfirmChangeBeforePOB';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_PAYONBILL_POSITIVE = 'VodaP85OrderConfirmChangeBeforePOBPos';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_PAYONBILL_NEGATIVE = 'VodaP85OrderConfirmChangeBeforePOBNeg';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEAFTER_PAYONBILL_POSITIVE = 'VodaP85OrderConfirmChangeAfterPOBPos';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEAFTER_PAYONBILL_NEGATIVE = 'VodaP85OrderConfirmChangeAfterPOBNeg';

    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_ACQ_NON_PORTING = 'VodaP85OrderConfirmBusinessAcqNonPorting';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_ACQ_PORTING = 'VodaP85OrderConfirmBusinessAcqPorting';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_RET = 'VodaP85OrderConfirmBusinessRet';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_RET_SO = 'VodaP85OrderConfirmBusinessRet_SO';

    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_STOREDELIVERY = 'VodaP85OrderConfirmStoreDelivery';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_STOREDELIVERY_BUSINESS = 'VodaP85OrderConfirmStoreDeliveryBusiness';

    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_COD = 'VodaP85OrderConfirmBusinessChangeBeforeCOD';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_COD_POSITIVE = 'VodaP85OrderConfirmBusinessChangeBeforeCODPos';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_COD_NEGATIVE = 'VodaP85OrderConfirmBusinessChangeBeforeCODNeg';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEAFTER_COD_POSITIVE = 'VodaP85OrderConfirmBusinessChangeAfterCODPos';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEAFTER_COD_NEGATIVE = 'VodaP85OrderConfirmBusinessChangeAfterCODNeg';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_PAYONBILL = 'VodaP85OrderConfirmBusinessChangeBeforePOB';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_PAYONBILL_POSITIVE = 'VodaP85OrderConfirmBusinessChangeBeforePOBPos';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_PAYONBILL_NEGATIVE = 'VodaP85OrderConfirmBusinessChangeBeforePOBNeg';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEAFTER_PAYONBILL_POSITIVE = 'VodaP85OrderConfirmBusinessChangeAfterPOBPos';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEAFTER_PAYONBILL_NEGATIVE = 'VodaP85OrderConfirmBusinessChangeAfterPOBNeg';

    const TEMPLATE_ROW_KEY_SHOPPING_CART = 'VodaP88ConfirmShoppingCartV2';
    const TEMPLATE_ROW_KEY_SHOPPING_CART_BUSINESS = 'VodaP88ConfirmShoppingCartV2Business';
    const TEMPLATE_ROW_KEY_SHOPPING_CART_ONLINE = 'VodaP88bConfirmShoppingCartOnline';
    const TEMPLATE_ROW_KEY_SHOPPING_CART_ONLINE_BUSINESS = 'VodaP88bConfirmShoppingCartOnlineBusiness';
    const TEMPLATE_ROW_KEY_OFFER_MAIL = 'VodaP106bOfferteZakelijkeKlanten';

    const TEMPLATE_ROW_KEY_READY4PICKUP = 'VodaP34OrderReadyForPickup';
    const TEMPLATE_ROW_KEY_READY4PICKUP_BUSINESS = 'VodaP34OrderReadyForPickupBusiness';
    const TEMPLATE_ROW_KEY_READY4PICKUP_NOTICE_STORE = 'VodaP34AOmniChannelOrderNoticeShop';
    const TEMPLATE_ROW_KEY_READY4PICKUP_CANCELLATION_STORE = 'VodaP34BOmniChannelOrderCancellationShop';

    const TEMPLATE_ROW_KEY_PASS_RESET = 'VodaResetPassword';
    const TEMPLATE_ROW_KEY_PASS_AGENT_RESET = 'VodaResetSSPassword';
    const TEMPLATE_ROW_KEY_GARANT_MAIL = 'VodaP74bGarantMail';
    const TEMPLATE_ROW_KEY_GARANT_MAIL_BUSINESS = 'VodaP74bGarantMailBusiness';

    const TEMPLATE_ROW_KEY_SOFTCOPY_CONTRACT = 'VodaP83SoftcopyContract';
    const TEMPLATE_ROW_KEY_SOFTCOPY_CONTRACT_BUSINESS = 'VodaP83SoftcopyContractBusiness';
    const TEMPLATE_ROW_KEY_BELCOMPANY_CONTRACT = 'VodaP83cContractMailBelCompany';
    const TEMPLATE_ROW_KEY_BELCOMPANY_CONTRACT_BUSINESS = 'VodaP83cContractMailBelCompanyBusiness';
    const TEMPLATE_ROW_KEY_INDIRECT_CONTRACT = 'VodaP83dContractMailIndirect';
    const TEMPLATE_ROW_KEY_INDIRECT_CONTRACT_BUSINESS = 'VodaP83dContractMailIndirectBusiness';

    const TEMPLATE_ROW_KEY_RETURN_GOODS = 'Voda868VOLRetourBinnenZichttermijn';
    const TEMPLATE_ROW_KEY_RETURN_GOODS_BUSINESS = 'Voda868VOLRetourBinnenZichttermijnBusiness';

    const TEMPLATE_ROW_KEY_CHANGE_ILS_MSISDN = 'VodaP115ChangeMSISDN1';
    const TEMPLATE_ROW_KEY_CHANGE_CANCEL = 'Voda112ChangeCancel';
    const TEMPLATE_ROW_KEY_CHANGE_CANCEL_BUSINESS = 'Voda112ChangeCancelBusiness';

    const TEMPLATE_ROW_KEY_FF_CONFIRM_CODE = 'VodaFF001SendApproval';
    const TEMPLATE_ROW_KEY_FF_CONFIRM_CODE_RESEND = 'VodaFF002ReSendApproval';
    const TEMPLATE_ROW_KEY_FF_CODE_REQUESTER = 'TEMPLATE_ROW_KEY_FF_CODE_REQUESTER';
    const TEMPLATE_ROW_KEY_FF_CODE_USER = 'VodaFF005CodeToEndUser';
    const TEMPLATE_ROW_KEY_FF_CODE_USED = 'VodaFF003SendCode';
    const TEMPLATE_ROW_KEY_FF_REQUESTER_BLOCKED = 'TEMPLATE_ROW_KEY_FF_REQUESTER_BLOCKED';
    const TEMPLATE_ROW_KEY_FF_REQUESTER_UNKNOWN = 'TEMPLATE_ROW_KEY_FF_REQUESTER_UNKNOWN';
    const TEMPLATE_ROW_KEY_FF_REQUESTER_CODES_USAGE_REPORT = 'VodaFF004CodeOverview';
    const TEMPLATE_ROW_KEY_FF_CODE_USE_REMINDER_REQUESTER = 'TEMPLATE_ROW_KEY_FF_CODE_USE_REMINDER_REQUESTER';
    const TEMPLATE_ROW_KEY_FF_CODE_USE_REMINDER_USER = 'TEMPLATE_ROW_KEY_FF_CODE_USE_REMINDER_USER';

    const TEMPLATE_ROW_KEY_ILT_FILL_IN_LATER = 'VodaInkomstenLastenToets';

    const TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_ACQUISITION_SINGLE = 'VodaP83AcquisitionSinglePackage';
    const TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_PORTING_SINGLE = 'VodaP83PortingSinglePackage';
    const TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_RETENTION_SINGLE = 'VodaP83RetentionSinglePackage';
    const TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_MULTI_PACKAGE = 'VodaP83MultiPackage';
    const TEMPLATE_ROW_KEY_SOFTCONTRACT_BUSINESS_MULTI_PACKAGE = 'VodaP83MultiPackageBusiness';
    const TEMPLATE_ROW_KEY_SOFTCONTRACT_BUSINESS_ACQUISITION_SINGLE = 'VodaP83AcquisitionSinglePackageBusiness';
    const TEMPLATE_ROW_KEY_SOFTCONTRACT_BUSINESS_RETENTION_SINGLE = 'VodaP83RetentionSinglePackageBusiness';

    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_ACQ_PORTING_MOBILE_MOVE = 'VodaP85OrderConfirmAcqPortingZMM';
    const TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_ACQ_PORTING_MOBILE_MOVE = 'VodaP85OrderConfirmBusinessAcqPortingZMM';

    const TEMPLATE_ROW_KEY_CC_REJECTED = 'Voda4312CreditReject';
    const TEMPLATE_ROW_KEY_CC_REJECTED_BUSINESS = 'Voda431CreditRejectBusiness';

    const TEMPLATE_ROW_KEY_CANCEL_DURING_PROVISIONING = 'Voda587AnnuleringTijdensProvisioning';
    const TEMPLATE_ROW_KEY_CANCEL_DURING_PROVISIONING_BUSINESS = 'Voda587AnnuleringTijdensProvisioningBusiness';

    const TEMPLATE_ROW_KEY_BACKORDER_POSTPAID = 'Voda679BackorderPostpaid';
    const TEMPLATE_ROW_KEY_BACKORDER_POSTPAID_BUSINESS = 'Voda679BackorderPostpaidBusiness';
    const TEMPLATE_ROW_KEY_BACKORDER_POSTPAID_REMINDER = 'Voda684BackorderPostpaidReminder';
    const TEMPLATE_ROW_KEY_BACKORDER_POSTPAID_REMINDER_BUSINESS = 'Voda684BackorderPostpaidReminderBusiness';
    const TEMPLATE_ROW_KEY_BACKORDER_PREPAID = 'Voda680BackorderPrepaid';
    const TEMPLATE_ROW_KEY_BACKORDER_PREPAID_BUSINESS = 'Voda680BackorderPrepaidBusiness';
    const TEMPLATE_ROW_KEY_BACKORDER_PREPAID_REMINDER = 'Voda685BackorderPrepaidReminder';
    const TEMPLATE_ROW_KEY_BACKORDER_PREPAID_REMINDER_BUSINESS = 'Voda685BackorderPrepaidReminderBusiness';

    const TEMPLATE_ROW_KEY_ORDER_VALIDATION_FAILED = 'VodaP37MobileValidationFailed';

    const ROWKEYS_ALL_BACKORDER_EMAILS = [
        self::TEMPLATE_ROW_KEY_BACKORDER_POSTPAID,
        self::TEMPLATE_ROW_KEY_BACKORDER_POSTPAID_BUSINESS,
        self::TEMPLATE_ROW_KEY_BACKORDER_POSTPAID_REMINDER,
        self::TEMPLATE_ROW_KEY_BACKORDER_POSTPAID_REMINDER_BUSINESS,
        self::TEMPLATE_ROW_KEY_BACKORDER_PREPAID,
        self::TEMPLATE_ROW_KEY_BACKORDER_PREPAID_BUSINESS,
        self::TEMPLATE_ROW_KEY_BACKORDER_PREPAID_REMINDER,
        self::TEMPLATE_ROW_KEY_BACKORDER_PREPAID_REMINDER_BUSINESS,
    ];

    const ROWKEYS_ALL_BACKORDER_EMAILS_REQUIRE_DEALER_INFO = [
        self::TEMPLATE_ROW_KEY_BACKORDER_POSTPAID,
        self::TEMPLATE_ROW_KEY_BACKORDER_POSTPAID_BUSINESS,
        self::TEMPLATE_ROW_KEY_BACKORDER_PREPAID,
        self::TEMPLATE_ROW_KEY_BACKORDER_PREPAID_BUSINESS,

    ];

    const ROWKEYS_ALLOWED_TO_SEND_CONTRACTS = [
        self::TEMPLATE_ROW_KEY_SOFTCOPY_CONTRACT,
        self::TEMPLATE_ROW_KEY_SOFTCOPY_CONTRACT_BUSINESS,
        self::TEMPLATE_ROW_KEY_BELCOMPANY_CONTRACT,
        self::TEMPLATE_ROW_KEY_BELCOMPANY_CONTRACT_BUSINESS,
        self::TEMPLATE_ROW_KEY_INDIRECT_CONTRACT,
        self::TEMPLATE_ROW_KEY_INDIRECT_CONTRACT_BUSINESS,
        self::TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_ACQUISITION_SINGLE,
        self::TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_PORTING_SINGLE,
        self::TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_RETENTION_SINGLE,
        self::TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_MULTI_PACKAGE,
        self::TEMPLATE_ROW_KEY_SOFTCONTRACT_BUSINESS_MULTI_PACKAGE,
        self::TEMPLATE_ROW_KEY_SOFTCONTRACT_BUSINESS_ACQUISITION_SINGLE,
        self::TEMPLATE_ROW_KEY_SOFTCONTRACT_BUSINESS_RETENTION_SINGLE,
    ];

    protected function _construct()
    {
       $this->_init("communication/job");
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        if ( ! is_array($messages = $this->getData('messages'))) {
            if ($messages = json_decode($messages, true)) {
                return $messages;
            }
            return array();
        }
        return $this->getData('messages');
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        if ( ! is_array($parameters = $this->getData('parameters'))) {
            if ($parameters = json_decode($parameters, true)) {
                return $parameters;
            }
            return array();
        }
        return $this->getData('parameters');
    }

    /**
     * @return array
     */
    public function getAttachments()
    {
        if ( ! is_array($attachments = $this->getData('attachments'))) {
            if ($attachments = json_decode($attachments, true)) {
                return $attachments;
            }
            return array();
        }
        return $this->getData('attachments');
    }

    public function getAdditional()
    {
        if ( ! is_array($additional = $this->getData('additional'))) {
            if ($additional = unserialize($additional)) {
                return $additional;
            }
            return array();
        }
        return $this->getData('additional');
    }

    /**
     * @return array
     */
    public function getReports()
    {
        if ( ! is_array($reports = $this->getData('reports'))) {
            if ($reports = json_decode($reports, true)) {
                return $reports;
            }
            return array();
        }
        return $this->getData('reports');
    }

    /**
     * @param $message
     * @param string $type
     * @return $this
     */
    public function addMessage($message, $type = self::MESSAGE_INFO)
    {
        if ( ! $this->getData('decoded_messages')) {
            $this->setData('decoded_messages', json_decode($this->getData('messages'), true));
        }

        $messages = is_array($this->getData('decoded_messages')) ? $this->getData('decoded_messages') : array();
        $messages[] = sprintf('[%s][%s] %s', strftime('%Y-%m-%d %H:%M:%S', time()), $type, $message);

        $this->setData('decoded_messages', $messages);

        return $this;
    }

    /**
     * @param $attachment
     * @return $this
     */
    public function addAttachment($attachment)
    {
        $attachments = $this->getAttachments();
        $attachments[] = $attachment;
        $this->setAttachments($attachments);
        return $this;
    }

    /**
     * @return $this
     */
    public function incrementTries()
    {
        $this->setData('tries', 1 + (int) $this->getData('tries'));
        return $this;
    }

    public function save()
    {
        $this->_preSave();
        return parent::save();
    }

    protected function _preSave()
    {
        if ( ! $this->getData('deadline')) {
            $this->setData('deadline', strftime('%Y-%m-%d %H:%M:%S', time()));
        }

        if (is_array($this->getData('parameters'))) {
            $this->setData('parameters', json_encode($this->getData('parameters')));
        }

        if (is_array($this->getData('attachments'))) {
            $this->setData('attachments', json_encode($this->getData('attachments')));
        }

        if (is_array($this->getData('additional'))) {
            $this->setData('additional', serialize($this->getData('additional')));
        }

        if (is_array($this->getData('reports'))) {
            $this->setData('reports', json_encode($this->getData('reports')));
        }

        $this->setData('messages', $this->hasData('decoded_messages') ? json_encode($this->getData('decoded_messages')) : '[]');
    }
}
