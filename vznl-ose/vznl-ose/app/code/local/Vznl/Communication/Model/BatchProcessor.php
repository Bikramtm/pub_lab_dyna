<?php

/**
 * Class Vznl_Communication_Model_BatchProcessor
 */
class Vznl_Communication_Model_BatchProcessor
{
    const BATCH_SIZE = 50;

    /**
     * @param $collection
     * @param $processor The processor
     * @return mixed
     */
    public function process($collection, $processor)
    {
        $this->walk($collection, array($processor, 'process'));
    }

    /**
     * @param Varien_Data_Collection $collection
     * @param array $itemCallback
     */
    protected function walk(Varien_Data_Collection $collection, array $itemCallback)
    {
        $collection->setPageSize(self::BATCH_SIZE);

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();

        do {
            $collection->setCurPage($currentPage);
            $collection->load();
            foreach ($collection as $item) {
                call_user_func($itemCallback, $item);
            }
            $currentPage++;
            $collection->clear();
        } while ($currentPage <= $pages);
    }
}
