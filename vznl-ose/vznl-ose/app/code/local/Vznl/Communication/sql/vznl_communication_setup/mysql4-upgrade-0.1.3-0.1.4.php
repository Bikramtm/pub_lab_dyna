<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;

$installer->startSetup();

$communicationSql = <<<SQL
ALTER TABLE `dyna_communication_job`
  ADD COLUMN `from` VARCHAR(45) NULL AFTER `culture_code`;
SQL;

$installer->getConnection()->query($communicationSql);

$installer->endSetup();
