<?php

/**
 * Class Vznl_Communication_Helper_Job
 */
class Vznl_Communication_Helper_Job extends Mage_Core_Helper_Data
{
    const VODAFONE_EMAIL_PROCESS_CONFIG_PATH = 'email_configuration/email_configuration/%s';

    const DEFAULT_STALE_LIMIT = 3;
    const DEFAULT_TRIES_LIMIT = 3;

    /**
     * @return int
     */
    public function getStaleLimit()
    {
        $limit = Mage::getStoreConfig(sprintf(self::VODAFONE_EMAIL_PROCESS_CONFIG_PATH, 'stale_limit'));
        if ( ! is_numeric($limit)) {
            return self::DEFAULT_STALE_LIMIT;
        }
        return (int) $limit;
    }

    /**
     * @return int
     */
    public function getTriesLimit()
    {
        $limit = Mage::getStoreConfig(sprintf(self::VODAFONE_EMAIL_PROCESS_CONFIG_PATH, 'tries_limit'));
        if ( ! is_numeric($limit)) {
            return self::DEFAULT_TRIES_LIMIT;
        }
        return (int) $limit;
    }

    /**
     * @return int
     */
    public function getCronInterval()
    {
        return (int) Mage::getStoreConfig(sprintf(self::VODAFONE_EMAIL_PROCESS_CONFIG_PATH, 'process_timing'));
    }
}
