<?php

/**
 * Class Vznl_Communication_Helper_Data
 */
class Vznl_Communication_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get the store config values for the given name for Lucom
     * @param string $name The name of the config
     * @param string $type The type to use
     * @return mixed The value if any
     */
    public function getCommunicationConfig($name = 'use_stubs', $type = 'email'){
        $type = $type ? '_' . $type : '';
        return Mage::getStoreConfig('vodafone_service/communication' . $type . '/' . $name);
    }
}
