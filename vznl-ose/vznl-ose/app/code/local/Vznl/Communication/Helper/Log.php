<?php

/**
 * Class Vznl_Communication_Helper_Log
 */
class Vznl_Communication_Helper_Log extends Mage_Core_Helper_Data
{
    const SUBTYPE_GARANT = 'garant';

    /**
     * @param $recipient
     * @param $subType
     * @param $rowkey
     * @param Vznl_Package_Model_Package $package
     * @return Mage_Core_Model_Abstract
     */
    public function logEmail($recipient, $subType, $rowkey, Vznl_Superorder_Model_Superorder $superorder)
    {
        $log = Mage::getModel('communication/log');
        $log->setRecipient($recipient);
        $log->setType(Vznl_Communication_Model_Log::TYPE_EMAIL);
        $log->setSubType($subType);
        $log->setRowkey($rowkey);
        $log->setSuperorderId($superorder->getId());
        $log->setCreatedAt(date('Y-m-d H:i:s'));
        $log->save();
    }
}
