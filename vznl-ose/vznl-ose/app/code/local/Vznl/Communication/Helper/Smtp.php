<?php

/**
 * Class Vznl_Communication_Helper_Smtp
 */
class Vznl_Communication_Helper_Smtp extends Mage_Core_Helper_Data
{

    const LOG_FILE = 'email_smtp.log';

    // General config
    public function isEnabled($storeId = null)
    {
        return Mage::getStoreConfig('email_configuration/smtpsettings/option', $storeId) != "disabled";
    }

    public function getTransport($storeId = null)
    {
        $option = Mage::getStoreConfig('email_configuration/smtpsettings/option', $storeId);

        if (!empty($option) && $option != "disabled") {
            return Mage::getModel("communication/transports_$option")->getTransport($storeId);
        } else {
            return false;
        }
    }


    public function log($m)
    {
        Mage::log($m, null, self::LOG_FILE);
    }

    public function logEmailSent($to, $template, $subject, $email, $isHtml)
    {
        $this->log(
            implode(' | ', array(
                'To: ' . (is_array($to) ? implode(',', $to) : $to),
                'Template: ' . $template,
                'Subject: ' . $subject,
                'Body: ' . ($isHtml ? $email : nl2br($email)),
            ))
        );
        return $this;
    }

    public function getSMTPSettingsHost($storeId = null)
    {
        return trim(Mage::getStoreConfig('email_configuration/smtpsettings/host', $storeId));
    }

    public function getSMTPSettingsPort($storeId = null)
    {
        return Mage::getStoreConfig('email_configuration/smtpsettings/port', $storeId);
    }

    public function getSMTPSettingsUsername($storeId = null)
    {
        return Mage::getStoreConfig('email_configuration/smtpsettings/username', $storeId);
    }

    public function getSMTPSettingsPassword($storeId = null)
    {
        return Mage::getStoreConfig('email_configuration/smtpsettings/password', $storeId);
    }

    public function getSMTPSettingsSSL($storeId = null)
    {
        return Mage::getStoreConfig('email_configuration/smtpsettings/ssl', $storeId);
    }

    public function getSMTPSettingsAuthentication($storeId = null)
    {
        return Mage::getStoreConfig('email_configuration/smtpsettings/authentication', $storeId);
    }

}
