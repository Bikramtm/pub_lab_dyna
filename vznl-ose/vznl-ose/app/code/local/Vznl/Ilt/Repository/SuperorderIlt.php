<?php
use Omnius\InkomensLastenToets\Repository\RepositoryInterface;
use Omnius\InkomensLastenToets\Model\DataInterface;
use Omnius\InkomensLastenToets\Model\Data;
use Omnius\InkomensLastenToets\Model\Address;
use Omnius\InkomensLastenToets\Model\Party;

class Vznl_Ilt_Repository_SuperorderIlt implements RepositoryInterface
{

    /**
     * Create a new ILT entry
     * @param DataInterface $data
     * @return DataInterface
     */
    public function create(DataInterface $data) : DataInterface
    {
        return $this->save($data);
    }

    /**
     * Update an existing ILT entry
     * @param DataInterface $data
     * @return DataInterface
     */
    public function save(DataInterface $data) : DataInterface
    {
        $superOrder = Mage::getModel('superorder/superorder')->load((string) $data->getOrdernumber(), 'order_number');
        $ilt = Mage::getModel('ilt/ilt')->load($superOrder->getId(), 'superorder_id');

        $ilt->setSuperorderId($superOrder->getId());
        $ilt->setFamilyType($data->getFamilyType());
        $ilt->setIncome($data->getIncome());
        $ilt->setHousingCosts($data->getHousingcosts());
        $ilt->setBirthMiddlename($data->getMiddlename());
        $ilt->setBirthLastname($data->getLastname());
        $ilt->setCreatedAt(date('Y-m-d H:i:s'));
        $ilt->save();

        return $data;
    }

    /**
     * @param string $orderNumber
     */
    public function get(string $orderNumber)
    {
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load((string) $orderNumber, 'order_number');
        foreach($superOrder->getOrders(true) as $order) {
            $billingAddress = $order->getBillingAddress();
            break;
        }
        $ilt = Mage::getModel('ilt/ilt')->load($superOrder->getId(), 'superorder_id');
        if($ilt->getId()) {
            $localeList = Zend_Locale_Data::getList('NL', 'alpha3toterritory');

            $streetArray = $billingAddress->getStreet();
            $street = isset($streetArray[0]) ? $streetArray[0] : '';
            $street1 = isset($streetArray[1]) ? $streetArray[1] : '';
            $street2 = isset($streetArray[2]) ? $streetArray[2] : null;
            $address = new Address(
                $street,
                $street1,
                $street2,
                $billingAddress->getCity(),
                $billingAddress->getPostcode(),
                $localeList[$billingAddress->getCountryId()]
            );
            $customerBan = null;
            if ($superOrder->getCustomer() && $ban = $superOrder->getCustomer()->getBan()) {
                $customerBan = $ban;
            }
            $party = new Party(
                $order->getCustomer()->getLastname(),
                $order->getCustomer()->getMiddlename(),
                $order->getCustomer()->getFirstname(),
                $order->getCustomer()->getGender() == 1 ? 'male' : 'female',
                new \DateTime($order->getCustomer()->getDob()),
                $ilt->getBirthLastname(),
                $ilt->getBirthMiddlename(),
                $address,
                $customerBan
            );
            return new Data(
                $orderNumber,
                $ilt->getFamilyType(),
                $ilt->getIncome(),
                $ilt->getHousingCosts(),
                $party
            );
        }
        return null;
    }

    /**
     * @param DataInterface $data
     * @return bool
     */
    public function delete(DataInterface $data) : bool
    {
        $superOrder = Mage::getModel('superorder/superorder')->load((string) $data->getOrdernumber(), 'order_number');
        $ilt = Mage::getModel('ilt/ilt')->load($superOrder->getId(), 'superorder_id');
        if($ilt->getEntityId() && $ilt->delete()) {
            return true;
        }
        return false;
    }
}
