<?php

$tableName = 'superorder_ilt';

$this->startSetup();

$this->getConnection()->query(sprintf('DROP TABLE IF EXISTS %s;', $tableName));

// Create the email table
$table = $this->getConnection()
    ->newTable($this->getTable($tableName))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned' => true,
            'primary' => true,
            'auto_increment' => true,
            'comment' => 'ID',
        ))
    ->addColumn('superorder_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'nullable' => false,
            'comment' => 'FK to superorder',
        ))
    ->addColumn('family_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255,
        array(
            'nullable' => false,
            'comment' => 'Family Type',
        ))
    ->addColumn('income', Varien_Db_Ddl_Table::TYPE_INTEGER, 255,
        array(
            'nullable' => false,
            'comment' => 'Customer income during ordering',
        ))
    ->addColumn('housing_costs', Varien_Db_Ddl_Table::TYPE_INTEGER, 255,
        array(
            'nullable' => false,
            'comment' => 'Customer housing costs during ordering',
        ))
    ->addColumn('birth_middlename', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255,
        array(
            'nullable' => true,
            'comment' => 'Customer middlename for ILT check',
        ))
    ->addColumn('birth_lastname', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255,
        array(
            'nullable' => false,
            'comment' => 'Customer Lastname for ILT check',
        ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
        array(
            'nullable' => false,
            'default'  => Varien_Db_Ddl_Table::TIMESTAMP_INIT,
            'comment' => 'Created At',
        ))
;
$this->getConnection()->createTable($table);

$this->getConnection()->addIndex(
    $this->getTable('ilt/ilt'),
    'idx_ilt_superorder',
    ['superorder_id'],
    Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
);

$this->endSetup();
