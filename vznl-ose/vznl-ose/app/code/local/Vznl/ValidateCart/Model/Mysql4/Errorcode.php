<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Model_Mysql4_Errorcode extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("validatecart/errorcode", "entity_id");
    }
}
