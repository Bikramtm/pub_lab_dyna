<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Vznl
 * @package     Vznl_Validate
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table
 */
if (!$installer->tableExists($installer->getTable('validatecart/whitelistaction'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('validatecart/whitelistaction'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('technical_id', Varien_Db_Ddl_Table::TYPE_TEXT, 50, array(
        ), 'Action')
        ->addColumn('action', Varien_Db_Ddl_Table::TYPE_TEXT, 50, array(
        ), 'Action')
        ->addColumn('direction', Varien_Db_Ddl_Table::TYPE_TEXT, 50, array(
        ), 'Direction')
        ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_INTEGER , null , array(
        ), 'Sku')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'Created')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'Updated')

        ->addIndex($installer->getIdxName($installer->getTable('validatecart/whitelistaction'), array('sku'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('sku'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('Vznl Whitelist Actions SKU');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();