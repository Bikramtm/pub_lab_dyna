<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 *  Installer to drop index
 */

/**
 * @category    Vznl
 * @package     Vznl_ValidateCart
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->getConnection()->dropIndex(
    $installer->getTable('validatecart/whitelistaction'),
    'UNQ_VZNL_WHITELIST_ACTION_SKU'
);

$installer->endSetup();
