<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Vznl
 * @package     Vznl_ValidateCart
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$this->getConnection()->modifyColumn($this->getTable('validatecart/whitelistaction'), 'sku', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 100,
    'nullable' => true,
    'required' => false
]);
$this->endSetup();