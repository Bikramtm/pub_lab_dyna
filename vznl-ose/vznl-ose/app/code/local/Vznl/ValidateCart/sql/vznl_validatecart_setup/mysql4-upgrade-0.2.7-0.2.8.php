<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Vznl
 * @package     Vznl_Validate
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$removeIndex = $installer->getTable('validatecart/errorcode');

$installer->getConnection()->addColumn($installer->getTable("validatecart/errorcode"), "service", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255,
    "after" => "entity_id",
    "comment" => 'service name',
]);

$installer->getConnection()->addColumn($installer->getTable("validatecart/errorcode"), "translation", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255,
    "after" => "error",
    "comment" => 'Translation',
]);

$installer->getConnection()->dropColumn($installer->getTable("validatecart/errorcode"), 'code', $schemaName = null);


$installer->getConnection()->addColumn($installer->getTable("validatecart/errorcode"), "code", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 255,
    "comment" => 'Code',
]);

/**
 * Create table
 */
if (!$installer->tableExists($installer->getTable('validatecart/validationerrorcode'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('validatecart/validationerrorcode'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('code', Varien_Db_Ddl_Table::TYPE_TEXT, 250, array(
        ), 'Code')
        ->addColumn('error', Varien_Db_Ddl_Table::TYPE_TEXT, 250, array(
        ), 'Error')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'Created')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'Updated')

        ->addIndex($installer->getIdxName($installer->getTable('validatecart/validationerrorcode'), array('code'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('code'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('Vznl Peal Validation Error Codes');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();