<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Vznl
 * @package     Vznl_Validate
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table
 */
if (!$installer->tableExists($installer->getTable('validatecart/mapper'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('validatecart/mapper'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_TEXT, 64, array(
        ), 'SKU')
        ->addColumn('service_expression', Varien_Db_Ddl_Table::TYPE_TEXT, 250, array(
        ), 'Service Expression')
        ->addColumn('priority', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        ), 'priority')
        ->addColumn('stop_execution', Varien_Db_Ddl_Table::TYPE_TEXT, 100, array(
        ), 'Stop Execution')
        ->addColumn('technical_id', Varien_Db_Ddl_Table::TYPE_TEXT, 100, array(
        ), 'Technical Id')
        ->addColumn('nature_code', Varien_Db_Ddl_Table::TYPE_TEXT, 200, array(
        ), 'Nature Code')
        ->addColumn('backend', Varien_Db_Ddl_Table::TYPE_TEXT, 100, array(
        ), 'Backend')
        ->addColumn('direction', Varien_Db_Ddl_Table::TYPE_TEXT, 100, array(
        ), 'Direction')
        ->addColumn('component_type', Varien_Db_Ddl_Table::TYPE_TEXT, 250, array(
        ), 'Component Type')
        ->addColumn('process_context', Varien_Db_Ddl_Table::TYPE_TEXT, 250, array(
        ), 'Process Context')

        ->addIndex($installer->getIdxName($installer->getTable('validatecart/mapper'), array('sku', 'technical_id', 'direction'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('sku', 'technical_id', 'direction'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('Vznl Peal Multimapper Table');

    $installer->getConnection()->createTable($table);
}

/**
 * insert data to table
 */
$data = array(
    array(
        'sku'   => 'z_4001311',
        'service_expression'   => '',
        'priority'     => '',
        'stop_execution'      => '',
        'technical_id'      => '4001311',
        'nature_code'      => 'peal_code',
        'backend'      => 'PEAL',
        'direction'      => 'both',
        'component_type'      => '',
        'process_context'      => 'ACQ'
    ),
    array(
        'sku'   => 'z_91489',
        'service_expression'   => '',
        'priority'     => '',
        'stop_execution'      => '',
        'technical_id'      => '91489',
        'nature_code'      => 'peal_code',
        'backend'      => 'PEAL',
        'direction'      => 'both',
        'component_type'      => '',
        'process_context'      => 'ACQ'
    )
);
$installer->getConnection()->insertMultiple($installer->getTable('validatecart/mapper'), $data);

$installer->endSetup();

