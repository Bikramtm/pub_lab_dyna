<?php

/**
 * Class Vznl_ValidateCart_Helper_Data Helper
 */
class Vznl_ValidateCart_Helper_Data implements Vznl_ValidateCartInterface_Helper_ValidateCartInterface
{
    /**
     * getCartProduct from the cart
     *
     * @param string $mapper
     * @return array object
     */
    public function getCartProduct($mapper = 'both', $processContextId = null)
    {
        $cartProducts = [];
        foreach (Mage::getModel('checkout/session')->getQuote()->getCartPackages() as $package) {
            $packageItems = $package->getItems();
            $alternateItems = $package->getAlternateItems();
            if (Mage::helper('vznl_utility')->getArrayCount($alternateItems)) {
                // Inject ILS changed items in extended cart
                $packageItems = array_merge($alternateItems, $packageItems);
            }

            $sortAddons = [];
            foreach ($packageItems as $item) {
                $mapperAddons = $this->getMultiMappers($item, $mapper, $processContextId);
                if (!empty($mapperAddons)) {
                    foreach ($mapperAddons as $key => $addons) {
                        $sortAddons[$item->getSku()] = [
                             'action' => $item->getQuoteId() ? 'add' : ($item->getAlternateQuoteId() ? 'remove' : 'add'),
                             'priority' => $addons['priority'] ?? '1',
                        ];
                    }
                }
            }

            $cartProducts[$package->getPackageId()] = $this->createPrioritizedList($packageItems, $sortAddons);
        }

        return $cartProducts;
    }

    /**
     * createBasket will create the basket
     *
     * @return basketId
     */
    public function createBasket()
    {
        $basketId = null;
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $serviceAddress = unserialize($quote->getServiceAddress());
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $lastName = ($customer->getLastName()) ? $customer->getLastName() : 'lastname'.base64_encode($quote->getId());
        $data = Mage::helper('vznl_createbasket')->createBasket($lastName,$serviceAddress['addressId']);
        if (isset($data['error'])) {
            $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($data["code"],$data['message'],Vznl_CreateBasket_Adapter_Peal_Adapter::name);
            $data = array(
                'error' => true,
                'message' => $pealErrorCode['translation'],
            );
            return $data;
        }
        if ($data) {
            foreach ($data as $key => $value) {
                if ($value["basketId"]) {
                    $basketId = $value["basketId"];
                }
            }
            return $basketId;
        }
        return false;
    }

    /**
     * getBasketId will get the basket id from quote
     *
     * @return basketId
     */
    public function getBasketId()
    {
        if (Mage::app()->getStore()->getIsActive() && Mage::getSingleton('checkout/cart')->getQuote()) {
            return Mage::getSingleton('checkout/cart')->getQuote()->getBasketId();
        }
        return null;
    }

    /**
     * setBasketId will set the basket id to the quote
     *
     * @return boolean
     */
    public function setBasketId($basketId = null)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quote->setBasketId($basketId);
        $quote->save();

        return true;
    }

    /**
     * setBasketId will set the Delivery Method to the quote
     *
     * @return boolean
     */
    public function setDeliveryMethod($deliveryMethod = null)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quote->setFixedDeliveryCheck($deliveryMethod);
        $quote->save();

        return true;
    }

    /**
     * setBasketId will set the Delivery Type to the quote
     *
     * @return boolean
     */
    public function setDeliveryType($deliveryType = null)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quote->setFixedDeliveryType($deliveryType);
        $quote->save();

        return true;
    }

    /**
     * setBasketId will set if the Separate Delivery is Allowed to the quote
     * @param $separateDeliveryAllowed
     * @return boolean
     */
    public function setSeparateDeliveryAllowed($separateDeliveryAllowed = null)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quote->setFixedSeparateDeliveryAllowed($separateDeliveryAllowed);
        $quote->save();

        return true;
    }

    /**
     * setPealOutbondFlag in quote
     * @param boolean $value
     */
    public function setOutBondFlag($value)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quote->setPealOutbondFlag($value);
        $quote->save();
        return true;
    }

    /**
     * getConsolidate will get the consolidate from quote
     *
     * @return basketId
     */
    public function getConsolidatedata()
    {
        $consolidate = Mage::getSingleton('checkout/cart')->getQuote()->getConsolidate();
        if (is_null($consolidate)) {
            return null;
        }

        return $consolidate ? 1 : 0;
    }

    /**
     * setConsolidate will set the consolidate to the quote
     *
     * @return boolean
     */
    public function setConsolidatedata($consolidate = 1)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quote->setConsolidate($consolidate);
        $quote->save();

        return true;
    }

    /**
     * createPrioritizedList will create the prioritized list of items to add/remove
     *
     * @param Vznl_Checkout_Model_Sales_Quote_Item[] $items
     * @param $sortAddons array with the action and priority for the product
     * @return array
     */
    public function createPrioritizedList(array $items, $sortAddons)
    {
        if (empty($items)) {
            return [];
        }

        usort($items, function ($item, $toCompare) use ($sortAddons) {

            $sku = $item->getSku();
            $action = isset($sortAddons[$sku]) ? $sortAddons[$sku]['action'] ?? 'add' : 'add';
            $priority = isset($sortAddons[$sku]) ? $sortAddons[$sku]['priority'] ?? '1' : '1';

            $skuToCompare= $toCompare->getSku();
            $actionToCompare = isset($sortAddons[$skuToCompare]) ? $sortAddons[$skuToCompare]['action'] ?? 'add' : 'add';
            $priorityToCompare = isset($sortAddons[$skuToCompare]) ? $sortAddons[$skuToCompare]['priority'] ?? '1' : '1';

            if ($action != $actionToCompare) {
                return ($action != 'add');
            }

            if ($action == 'add') {
                return ($priority > $priorityToCompare);
            } else if ($action == 'remove') {
                return ($priority < $priorityToCompare);
            }

            return 0;
        });

        return $items;
    }

    /**
     * addProduct will add product to the basket
     * @param $basketId
     * @param $technicalId
     * @return array
     */
    public function addProduct($basketId = null, $technicalId = null)
    {
        $additemtoBasket = Mage::helper('vznl_additemtobasket')->addItemToBasket($basketId, $technicalId);
        return $additemtoBasket;
    }

    /**
     * removeProduct will remove product from the basket
     * @param $basketId
     * @param $technicalId
     * @return boolean
     */
    public function removeProduct($basketId = null, $technicalId = null, $removeReasons = [])
    {
        $removeitemfrombasket = Mage::helper('vznl_removeitemfrombasket')->removeitemfrombasket($basketId ,$technicalId, $removeReasons);
        return $removeitemfrombasket;
    }

    /**
     * clearCart will clear the basket
     *
     * @return boolean
     */
    public function clearBasket($basketId = null)
    {
        $resetBasket = Mage::helper('vznl_resetbasket')->resetBasket($basketId);
        if (!empty($resetBasket)) {
            if (isset($resetBasket['error'])) {
                $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($resetBasket["code"],$resetBasket['message'],Vznl_ResetBasket_Adapter_Peal_Adapter::name);
                $resetBasket = array(
                    'error' => true,
                    'message' => $pealErrorCode['translation'],
                );
                return $resetBasket;
            }
            return true;
        }
        return true;
    }

    /**
     * getMultiMappers will get the multi mapper add on
     *
     * @return array
     */
    public function getMultiMappers($package = null, $mapper = 'both', $processContextId = null)
    {

        $mapperData = Mage::getModel('dyna_multimapper/mapper')->getTechnicalIdWithDirection($package->getSku(), $mapper, $processContextId)->getData();

        foreach ($mapperData as $key => $mapper) {
            if (!isset($mapper['action'])) {
                $mapperData[$key]['action'] = "add";
            }
        }
        return $mapperData;
    }

    /**
     * getWhiteListData will get the white list data
     *
     * @param $package
     * @param $mapper
     * @return array
     */
    public function getWhiteListData($package = null, $mapper = 'both')
    {

        $whiteListData = Mage::getModel('validatecart/whitelistaction')->getCollection()->addFieldToSelect('technical_id')
            ->addFieldToFilter('sku', array('IN' => [$package->getSku(), '*']), 'direction', array('IN' => ['both', $mapper]))->getData();

        $whiteList = array();
        foreach ($whiteListData as $key=>$value) {
            $whiteList[]=$value['technical_id'];
        }

        return $whiteList;
    }

    /**
     * getWhiteListData will get the white list data
     *
     * @param $technicalId
     * @param $sku
     * @param $mapper
     * @return array
     */
    public function getWhiteListSkuData($technicalId = null, $sku, $mapper = 'both')
    {
        $whiteListSkuData = Mage::getModel('validatecart/whitelistaction')->getCollection()
            ->addFieldToFilter('technical_id', array('IN' => [$technicalId, '*']))
            ->addFieldToFilter('sku', array('IN' => [$sku] ))
            ->addFieldToFilter('direction', array('IN' => ['both', $mapper]))->getData();

       if ($whiteListSkuData) {
           return true;
       }

        return false;
    }

    /**
     * validateBasket will validate the basket
     * @param $basketId
     * @param $consolidate
     * @return array
     */
    public function validateBasket($basketId = null, $consolidate = 0, $removeReasons = [])
    {
        $param = array();
        if ($consolidate === 1) {
            $consolidate = 'CONSOLIDATE_YES';
        } else if ($consolidate === 0) {
            $consolidate = 'CONSOLIDATE_NO';
        } else {
            $consolidate = 'CONSOLIDATE_NONE';
        }

        $param['consolidate'] = $consolidate;
        $param['serviceAddress'] = $this->getServiceAddress();
        $param['agentName'] = $this->getAgentName();
        $param['customerType'] = $this->getCustomerType();
        $orderType = $this->getQuoteOrderType();
        $lastAddRemoveResponse = Mage::getSingleton('customer/session')->getLastAddRemoveResponse();
        $validateBasketResponse = Mage::helper('vznl_validatebasket')->getValidateBasket($basketId, $param, $orderType, $removeReasons, $lastAddRemoveResponse);
        $validateBasket = $validateBasketResponse['source'];
        if (isset($validateBasket['error'])) {
            $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($validateBasket["code"], $validateBasket['message'], Vznl_ValidateBasket_Adapter_Peal_Adapter::name);
            $validateBasket = array(
                'error' => true,
                'message' => $pealErrorCode['translation'],
            );
            return $validateBasket;
        }
        $compareObj = $this->compareItems($validateBasket);
        $consolidation = null;
        $this->setOutBondFlag(false);
        if ($compareObj['comparison']) {
            if (array_key_exists('validationItems', $validateBasket)) {
                foreach ($validateBasket['validationItems'] as $items) {
                    if ($items['validationType'] == 'PCMS_RULES_ERROR' && $items['validationSource'] == 'PCMS_RULES_VALIDATION') {
                        $pealErrorCode = Mage::getModel('validatecart/validationerrorcode')->getByValidationCode($items["validationResultCode"]);
                        $validateBasketResponse = array(
                            'error' => true,
                            'message' => Mage::helper('vznl_checkout')->__("Ziggo basket is not valid"),
                            'validateErrorMessage' => $pealErrorCode->getError() ?? false,
                            'errorType' => 'validate-error'
                        );
                        return $validateBasketResponse;
                    }
                    if ($items['validationType'] == 'DELIVERY_METHOD_CHECK') {
                        $this->setDeliveryMethod($items["validationResultCode"]);
                    }
                    if ($items['validationType'] == 'DELIVERY_TYPE_CHECK') {
                        $this->setDeliveryType($items["validationResultCode"]);
                    }
                    if ($items['validationType'] == 'CONSOLIDATION' && $items['validationResultCode'] == 'TRUE') {
                        $consolidation = 'CONSOLIDATION';
                    } elseif ($items['validationType'] == 'CONSOLIDATION' && $items['validationResultCode'] == 'FALSE') {
                        $consolidation = 'CONSOLIDATIONFALSE';
                    }
                    if ($items['validationType'] == 'CUSTOMER_APPROVAL_REQUIRED_IF_OUTBOUND_CALL' && $items['validationResultCode'] == 'CUSTOMER_APPROVAL_REQUIRED_IF_OUTBOUND_CALL_YES') {
                        $this->setOutBondFlag(true);
                    }

                    if ($items['validationType'] == 'SEPARATE_DELIVERY_ADDRESS_ALLOWED') {
                        $this->setSeparateDeliveryAllowed($items["validationResultCode"]);
                    }
                }
            }
            if ($consolidation) {
                if(!$this->getConsolidatedata()) {
                    $this->setConsolidatedata(0);
                }
                return $consolidation;
            }
            $this->setConsolidatedata(null);
            return "TRUE";
        } else if ($compareObj['errorType'] == 'basket-unsynced-error') {
            $validateBasketResponse = array(
                'error' => true,
                'message' =>  Mage::helper('vznl_checkout')->__("Please check your package configuration or 'more info' for full details"),
                'errorType' => $compareObj['errorType'],
                'unsyncedBasketItems' => $compareObj['unsyncedBasketItems'],
                'button'=> $this->getMoreInfoButton($compareObj["errorType"])
            );
            return $validateBasketResponse;
        }
        return Mage::helper('vznl_checkout')->__("Ziggo basket is not valid");
    }

    /**
     * validate will communicate with all other services
     *
     * @return boolean
     */
    public function validate($validateCancelledItems = false, $terminationReasons = [])
    {
        $basketId = $this->getBasketId();

        if (!$basketId) {
            $basketId = $this->createBasket();
            if ($basketId) {
                if (isset($basketId['error'])) {
                    return $basketId;
                }
                $this->setBasketId($basketId);
            } else {
                return false;
            }
        }

        // Clear the basket only if there are no cancelled items
        if (!$validateCancelledItems) {
            $clearBasket = $this->clearBasket($basketId);
            if (isset($clearBasket['error'])) {
                return $clearBasket;
            }
            $this->setConsolidatedata(null);
            $firstloop =  false;
            if ($clearBasket) {
                $quote = Mage::getSingleton('checkout/cart')->getQuote();
                $fixedProcessContextId = $quote->getFixedProcessContextId();
                $cartProducts = $this->getCartProduct('out', $fixedProcessContextId);
                $resultArray = array();
                $lastAddRemoveResponse = array();
                $processContext = $quote->isFixedIlsMove();
                Mage::log(" basketId : ".$basketId. "", null, 'pealvalidatecalls.log');

                if ($processContext) {
                    //on MOVE OR ISL getting resetBasketData from session
                    $resetBasketResponse = Mage::getSingleton('core/session')->getResetResultArray();
                    $pealHardwareDetail = Mage::getSingleton('customer/session')->getHardwareDetailsIls();
                    if (isset($resetBasketResponse["basketId"]) && $resetBasketResponse["basketId"] == $basketId && !empty($pealHardwareDetail)) {
                        $resultArray = $resetBasketResponse;
                    } else {
                        $resetBasket = Mage::helper('vznl_resetbasket')->resetBasket($basketId);

                        if (!empty($resetBasket)) {
                            if (isset($resetBasket['error'])) {
                                $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($resetBasket["code"],$resetBasket['message'],Vznl_ResetBasket_Adapter_Peal_Adapter::name);
                                $resetBasket = array(
                                    'error' => true,
                                    'message' => $pealErrorCode['translation'],
                                );
                                return $resetBasket;
                            }
                        }
                        $resultArray = array();
                        $resultArray = $this->getResultArray($resetBasket['itemsList'], $resultArray);
                        $resultArray["basketId"] = $basketId;
                        Mage::getSingleton('core/session')->setResetResultArray($resultArray);
                        Mage::log(" BasketId : ".$basketId." : ". json_encode($resultArray) . "\n\n", null, 'resultinvalidate.log');

                        $hardwareDetail = array();
                        $oseResetItemList = array();
                        $productSkus = array();
                        $technicalIds = Mage::helper('vznl_resetbasket')->getOseItemList($resetBasket['itemsList'], $oseResetItemList);
                        foreach ($technicalIds as $technicalId => $value) {
                            $commercialSkus = Mage::getModel('dyna_multimapper/mapper')->getSkusForSoc($value['offerId'], $productSkus);
                            $productModel = Mage::getModel('catalog/product');
                            $productSkus = array_merge($productSkus, $commercialSkus);
                            if (isset($commercialSkus[0])) {
                                foreach ($commercialSkus as $productSku) {
                                    $productId = $productModel->getIdBySku($productSku);
                                    $hardwareDetail[$productSku]=$value;
                                    $hardwareDetail[$productSku]["productId"]=$productId;
                                }
                            }
                        }

                        Mage::getSingleton('customer/session')->setHardwareDetailsIls($hardwareDetail);
                    }
                }

                $oseItemList = $this->getItemListTechnicleId($cartProducts, $fixedProcessContextId);

                foreach ($cartProducts as $packageId => $package) {
                    foreach ($package as $product) {

                        Mage::log(" Product : ".$product->getSku(). "", null, 'pealvalidatecalls.log');
                        $mapperAddons = $oseItemList[$product->getSku()]["MM"] ?? [];
                        if (!empty($mapperAddons)) {
                            foreach ($mapperAddons as $key => $addons) {
                                $technicalId = $addons['technical_id'];
                                if ($product->getAlternateQuoteId()) {
                                    $action = 'remove';
                                } else {
                                    $action = 'add';
                                }
                                Mage::log(" technicalId : ".$technicalId. "", null, 'pealvalidatecalls.log');
                                Mage::log(" action : ".$action. "", null, 'pealvalidatecalls.log');
                                if ($technicalId) {

                                    $performAction = $this->getPerformAction($oseItemList["ose_list"], $resultArray, $technicalId, $action);
                                    if ($action == 'add') {
                                        if ($performAction) {
                                            continue;
                                        }
                                        $resultArray = array();
                                        $addProductData = $this->addProduct($basketId, $technicalId);
                                        if (isset($addProductData['error'])) {
                                            $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($addProductData["code"],$addProductData['message'],Vznl_AddItemToBasket_Adapter_Peal_Adapter::name);
                                            $addProductData = array(
                                                'error' => true,
                                                'message' => $pealErrorCode['translation'],
                                            );
                                            return $addProductData;
                                        }
                                        $lastAddRemoveResponse = $addProductData;
                                        $resultArray = $this->getResultArray($addProductData['itemsList'], $resultArray);
                                        $firstloop = true;
                                        Mage::log(" resultArray : ". json_encode($resultArray). "", null, 'pealvalidatecalls.log');
                                    } elseif ($action == 'remove') {
                                        if (!$performAction) {
                                            continue;
                                        }
                                        $resultAction =  Mage::getSingleton('customer/session')->getHardwareDetailsIls();
                                        Mage::log(" resultArray : ". json_encode($resultAction). "\n", null, 'pealvalidatecalls.log');
                                        if(isset($resultAction[$product->getSku()])) {
                                            $basketItemId = $resultAction[$product->getSku()]['basketItemId'];
                                        }
                                        $resultArray = array();
                                        $removeProductData = $this->removeProduct($basketId, $basketItemId);
                                        if (isset($removeProductData['error'])) {
                                            $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($removeProductData["code"],$removeProductData['message'],Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter::name);
                                            $removeProductData = array(
                                                'error' => true,
                                                'message' => $pealErrorCode['translation'],
                                            );
                                            return $removeProductData;
                                        }
                                        $lastAddRemoveResponse = $removeProductData;
                                        $resultArray = $this->getResultArray($removeProductData['itemsList'], $resultArray);
                                        $firstloop = true;
                                        Mage::log(" resultArray : ". json_encode($resultArray). "\n", null, 'pealvalidatecalls.log');
                                    }
                                }
                            }
                        }
                    }

                    $cancelArray = array();
                    if ($processContext && !empty($lastAddRemoveResponse)) {
                        $cancelledItemFound = $this->isItemCancelled($lastAddRemoveResponse['itemsList'], $cancelArray);
                        Mage::getSingleton('customer/session')->setLastAddRemoveResponse($lastAddRemoveResponse);
                        $itemCancelled = false;
                        $itemCancelledArray = array();
                        $i = 0;

                        foreach ($cancelledItemFound as $cancelKey => $cancelValue) {
                            if ($cancelValue["productName"]) {
                                $itemCancelled = true;
                                $itemCancelledArray[$i]["offerId"] = $cancelValue["offerId"];
                                $itemCancelledArray[$i]["productName"] = $cancelValue["productName"];
                                $i++;
                            }
                        }

                        if ($itemCancelled) {
                            return array(
                                'isItemCancelled' => true,
                                'itemCancelledArray' => $itemCancelledArray
                            );
                        }
                    } else {
                        Mage::getSingleton('customer/session')->setLastAddRemoveResponse(null);
                    }
                }
            }
        } else {
            // Because the basket was already got cleared
            $clearBasket = true;
        }

        if ($clearBasket) {
            $consolidate = $this->getConsolidatedata();
            $validate = $this->validateBasket($basketId, $consolidate, $terminationReasons);
            if ($validate === 'CONSOLIDATION' || $validate == 'CONSOLIDATIONFALSE') {
                return $validate;
            }
            if ($validate === 'TRUE') {
                return 'TRUE';
            } else {
                $this->clearBasket($basketId);
                return $validate;
            }
        }

        return false;
    }

    public function getItemListTechnicleId($cartProducts, $fixedProcessContextId)
    {
        $oseItemList = [];
        foreach ($cartProducts as $packageId => $package) {
            foreach ($package as $product) {
                $mapperAddons = $this->getMultiMappers($product, 'out', $fixedProcessContextId);
                $i = 0;
                if (!empty($mapperAddons)) {
                    foreach ($mapperAddons as $key=>$addons) {
                        $technicalId = $addons['technical_id'];
                        if ($product->getAlternateQuoteId()) {
                            $action='remove';
                        } else {
                            $action='add';
                        }

                        $oseItemList[$product->getSku()]["action"] = $action;

                        if ($technicalId) {
                            $oseItemList[$product->getSku()]["MM"][$i]["technical_id"] = $technicalId;

                            if (!isset($oseItemList["ose_list"][$action][$technicalId])) {
                                $oseItemList["ose_list"][$action][$technicalId] = 0;
                            }
                            $oseItemList["ose_list"][$action][$technicalId]++;
                        }
                        $i++;
                    }
                }
            }
        }

        return $oseItemList;
    }

    /**
     * @param array $oseItemList
     * @param array $resultArray
     * @param string $technicalId
     * @param string $action
     * @return bool
     */
    public function getPerformAction(array $oseItemList, array $resultArray, string $technicalId, string $action): bool
    {
        $pealList = [];
        foreach ($resultArray as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $id => $offerId) {
                    if ($key == 'CANCEL') {
                        if (!isset($pealList['remove'][$offerId])) {
                            $pealList['remove'][$offerId] = 0;
                        }
                        $pealList['remove'][$offerId]++;
                    } else {
                        if (!isset($pealList['add'][$offerId])) {
                            $pealList['add'][$offerId] = 0;
                        }
                        $pealList['add'][$offerId]++;
                    }
                }
            }
        }

        if (isset($pealList[$action][$technicalId])
            && $pealList[$action][$technicalId] == $oseItemList[$action][$technicalId]
        ) {
            return true;
        }

        if($action == "add" && isset($pealList[$action][$technicalId])
            && $pealList[$action][$technicalId] >= $oseItemList[$action][$technicalId]) {
            return true;
        }

        if ($action == "remove" && isset($pealList["add"][$technicalId])) {
            return true;
        } else {
            if (isset($pealList["remove"][$technicalId])) {
                return false;
            }
        }

        return false;
    }

    public function getResultArray($addProductData, &$resultArray)
    {
        foreach ($addProductData as $item) {
            if (!isset($resultArray[$item["offerId"]])) {
                $resultArray[$item["action"]][] = $item['offerId'];
                if (isset($item["bundledItems"])) {
                    $this->getResultArray($item["bundledItems"], $resultArray);
                }
                if (isset($item["childItems"])) {
                    $this->getResultArray($item["childItems"], $resultArray);
                }
            }
        }

        return $resultArray;
    }

    public function isItemCancelled($items, &$cancelArray)
    {
        foreach ($items as $item) {
            if ($item["action"] == "CANCEL") {
                $cancelArray[] = array("offerId" => $item["offerId"], "productName" => $item["productName"]);
            } else {
                $cancelArray[] = array("offerId" => $item["offerId"], "productName" => false);;
            }
            if(isset($item["bundledItems"])) {
                $this->isItemCancelled($item["bundledItems"], $cancelArray);
            }
            if(isset($item["childItems"])) {
                $this->isItemCancelled($item["childItems"], $cancelArray);
            }
        }

        return $cancelArray;
    }

    public function consolidateValidate($consolidate = 1, $terminationReasons = array())
    {
        $basketId = $this->getBasketId();
        $this->setConsolidatedata($consolidate);
        $validate = $this->validateBasket($basketId, $consolidate, $terminationReasons);
        if ($validate === 'TRUE' || $validate === 'CONSOLIDATION' || $validate === 'CONSOLIDATIONFALSE') {
            return "TRUE";
        } elseif (isset($validate['error'])) {
            $this->clearBasket($basketId);
            return $validate;
        } else {
            $this->clearBasket($basketId);
            $this->setConsolidatedata(null);
            return false;
        }
    }

    public function compareItems($itemList)
    {
        $count = 0;
        $other = 0;
        $oseItemList = array();
        $oseItemListWithSku = array();
        $pealItemList = array();
        $skuMMList = array();
        $whiteList = array();
        $whiteListArray = array();
        $fixedProcessContextId = Mage::getSingleton('checkout/cart')->getQuote()->getFixedProcessContextId();
        $cartProducts = $this->getCartProduct('in', $fixedProcessContextId);
        $basketId = $this->getBasketId();
        $HappyFlow = Mage::helper('vznl_validatebasket')->getHappyFlow();
        $invalidSku = Mage::helper('vznl_validatebasket')->getInvalidSku();
        $response = array(
            'comparison' => false
        );
        foreach ($cartProducts as $packageId => $package) {
            foreach ($package as $product) {
                if ($product->getQuoteId()) {
                    $mapperAddons=$this->getMultiMappers($product, 'in', $fixedProcessContextId);
                    $whiteList=$this->getWhiteListData($product, 'in');
                    if (!empty($mapperAddons)) {
                        foreach ($mapperAddons as $key=>$addons) {
                            $technicalId=$addons['technical_id'];
                            $sku=$addons['sku'];
                            if ($technicalId) {
                                $oseItemList[]=$technicalId;
                                if (isset($skuMMList[$technicalId])) {
                                    $skuMMList[$technicalId."||".$sku] = $sku;
                                } else {
                                    $skuMMList[$technicalId] = $sku;
                                }
                                $oseItemListWithSku[$sku]["name"] = $product->getName();
                                $oseItemListWithSku[$sku]["offerId"] = $technicalId;
                            }
                        }
                    }
                    if (!empty($whiteList)) {
                        foreach ($whiteList as $key=>$technicalId) {
                            if (in_array($technicalId, $whiteListArray)) {
                                continue;
                            }
                            $whiteListArray[]=$technicalId;
                        }
                    }
                }
            }
        }
        $result["count"] = 0;
        $result["other"] = 0;
        $result["unsyncedItems"] = [];
        $result["pealItemList"] = [];
        if (!isset($result["bomIds"])) {
            $result["bomIds"] = [];
        }

        $result = $this->getCount($itemList['itemsList'], $oseItemList, $skuMMList, $whiteListArray, $result);

        Mage::log(" oselist : ". json_encode($oseItemList). "\n\n", null, 'pealvalidatecalls.log');

        foreach ($oseItemListWithSku as $sku => $name) {
            if (!isset($result["pealItemList"][$sku])) {
                $skuWhiteList = $this->getWhiteListSkuData($name["offerId"], $sku, 'in');
                if($skuWhiteList) {
                    $result["count"]++;
                } else {
                    $result['unsyncedItems'][]=array("offerId"=>$name["offerId"], "productName"=>$name["name"]);
                }
            }
        }

        if (!empty($result["unsyncedItems"]) && !$HappyFlow) {
            $response['errorType'] = 'basket-unsynced-error';
            $response['unsyncedBasketItems'] = $result['unsyncedItems'];
            return $response;
        }

        if ($HappyFlow) {
            if ($invalidSku) {
                $invalidBasket = $this->checkSku($invalidSku);
                if ($invalidBasket) {
                    return $response;
                }
            }
            $this->updateStub();
            $response['comparison'] = true;
            return $response;
        }

        if ($result["count"] == count($oseItemList) && $result["other"] == 0) {

            if(count($result["bomIds"]) > 0) {
                $this->addProductOnBomId($result["bomIds"], $result["bomIdName"]);
            }
            $this->setItemDetails($result["pealItemList"]);
            $response['comparison'] = true;
            return $response;
        }
        return $response;
    }

    /**
     * based on validatebasket bomid add product to the cart
     */
    public function addProductOnBomId($bomIds, $bomIdName = null)
    {
        $packageId = $this->getFixedPackageId();
        $multiMapperHelper = Mage::helper('vznl_multimapper/expressionLanguage');
        $quoteProductCheck = Mage::getModel('vznl_configurator/expression_order');
        $cart = Mage::helper('vznl_checkout/cart');
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $quoteItems = $quote->getAllItems();
        $productIds = array();
        $insertedBomIds = array();

        foreach ($quoteItems as $item) {
            $productSubtype = $item->getProduct()->getAttributeText('package_subtype');
            if($productSubtype == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS) {
                $productIds[] = $item->getProductId();
            }
        }
        foreach ($bomIds as $bomId => $limit) {
            $bomSkuData = Mage::getModel('vznl_multimapper/mapper')->getSkuOnBomId(trim($bomId), $limit);
            foreach ($bomSkuData as $data) {
                if ($data['service_expression'] != '') {
                    if ($multiMapperHelper->evaluate(trim($data['service_expression']))) {
                        $sku = trim($data['sku']);
                        $product = Mage::getModel('catalog/product')->loadByAttribute("sku", $sku);
                        if ($product && $product->getAttributeText('package_subtype') == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS
                            && !$quoteProductCheck->packageContainsProduct($sku)
                        ) {
                            $productIds[] = $product->getId();
                            $proData = array(
                                'products' => $productIds,
                                'type' => 'int_tv_fixtel',
                                'section' => $product->getAttributeText('package_subtype'),
                                'simType' => '',
                                'oldSim' => false,
                                'packageId' => $packageId,
                                'clickedItemId' => $product->getId(),
                                'clickedItemSelected' => true,
                                'addressData' => ''
                            );
                            $cart->processAddMulti($proData);
                            $insertedBomIds[$sku] = $bomId;
                        } else {
                            if (!$product) {
                                echo $msg = 'Peal is trying to added an invalid sku ' . $sku;
                                Mage::log($msg);
                            }
                        }
                    }
                }
            }
        }
        foreach (Mage::getSingleton('checkout/cart')->getQuote()->getCartPackages() as $packageModel)
        {
            $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED);
            Mage::helper('vznl_utility')->saveModel($packageModel);
        }
        $this->updateBomId($insertedBomIds, $bomIdName);
    }

    /**
     * From the current quote finds the fixed package id
     * @return $pacakgeId
     */
    public function getFixedPackageId()
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $quote->getId());
        /** @var Vznl_Package_Model_Package $package */
        foreach ($packages as $package) {
            if ($package->isFixed()) {
                $packageId = $package->getData('package_id');
                return $packageId;
            }
        }
    }
    /**
     * save details from validate basket response to quote item
     */
    public function setItemDetails($pealItemList)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quoteItem = $quote->getAllItems();

        foreach ($quoteItem as $key => $item) {
            $sku = $item->getSku();

            if (isset($pealItemList[$sku])) {
                $item->setBasketItemId($pealItemList[$sku]["basketItemId"]);
                $item->setOfferId($pealItemList[$sku]["offerId"]);
                $item->setAction($pealItemList[$sku]["action"]);
                $item->setBomId($pealItemList[$sku]["bomId"]);
                $item->setProductName($pealItemList[$sku]["productName"]);
                $item->setOfferType($pealItemList[$sku]["offerType"]);
                $item->setDispatchMethod(serialize($pealItemList[$sku]["dispatchMethod"]));
                $item->setPaymentMethod(serialize($pealItemList[$sku]["paymentMethod"]));
                $item->setHardwareName(($pealItemList[$sku]["hardwareName"]));
                $item->setSerialNumber(($pealItemList[$sku]["serialNumber"]));
                $item->setNewHardware(($pealItemList[$sku]["newHardware"]));
            }
        }
        $quote->save();
    }

    public function updateBomId($insertedBomIds, $bomIdName = null)
    {
        $quoteId = Mage::getSingleton('checkout/cart')->getQuote()->getId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        $quoteItem = $quote->getAllItems();

        foreach ($quoteItem as $key => $item) {
            $sku = $item->getSku();

            if (isset($insertedBomIds[$sku])) {
                $item->setBomId($insertedBomIds[$sku]);
                $item->setAddedViaPeal(1);

                if(isset($bomIdName[$insertedBomIds[$sku]][0])) {
                    $item->setHardwareName($bomIdName[$insertedBomIds[$sku]][0]);
                    unset($bomIdName[$insertedBomIds[$sku]][0]);
                    $bomIdName[$insertedBomIds[$sku]] = array_values($bomIdName[$insertedBomIds[$sku]]);
                }
            }
        }
        $quote->save();
    }

    public function getCount($itemList, &$oseItemList, $skuMMList, $whiteListArray, &$result)
    {
        $bomId = array();
        $flippedOseItemList = array_flip($oseItemList);
        $flippedWhiteListArray = array_flip($whiteListArray);
        foreach ($itemList as $item) {
            $offerId = $item["offerId"] ?? null;
            Mage::log(" offerId : " . $offerId . "", null, 'pealvalidatecalls.log');
            if (isset($item["action"]) && ($item["action"] == 'ADD' || $item["action"] == 'NONE' || $item["action"] == 'CHANGE' || $item["action"] == 'MOVE')) {
                if (isset($flippedOseItemList[$offerId])) {
                    if (!empty($item["bundledItems"])) {
                        $this->getCount($item["bundledItems"], $oseItemList, $skuMMList, $whiteListArray, $result);
                    }
                    if (!empty($item["childItems"])) {
                        $this->getCount($item["childItems"], $oseItemList, $skuMMList, $whiteListArray, $result);
                    }

                    if (isset($result["pealItemList"][$skuMMList[$item["offerId"]]])) {
                        foreach ($skuMMList as $key => $value) {
                            if (strpos($key, '||') !== false) {
                                $offerIdArray = explode('||', $key);
                                if ($offerIdArray['0'] == $item["offerId"]) {
                                    $offerKey = $key;
                                    if (!isset($result["pealItemList"][$skuMMList[$offerKey]])) {
                                        $result["pealItemList"][$skuMMList[$offerKey]]=[
                                            "basketItemId"=>$item["basketItemId"],
                                            "offerId"=>$item["offerId"],
                                            "action"=>$item["action"],
                                            "bomId"=>$item["bomId"],
                                            "productName"=>$item["productName"],
                                            "offerType"=>$item["offerType"],
                                            "dispatchMethod"=>$item["dispatchMethod"],
                                            "paymentMethod"=>$item["paymentMethod"],
                                            "hardwareName"=>$item["hardwareName"],
                                            "newHardware"=>$item["newHardware"],
                                        ];
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        $result["pealItemList"][$skuMMList[$item["offerId"]]]=[
                            "basketItemId"=>$item["basketItemId"],
                            "offerId"=>$item["offerId"],
                            "action"=>$item["action"],
                            "bomId"=>$item["bomId"],
                            "productName"=>$item["productName"],
                            "offerType"=>$item["offerType"],
                            "dispatchMethod"=>$item["dispatchMethod"],
                            "paymentMethod"=>$item["paymentMethod"],
                            "hardwareName"=>$item["hardwareName"],
                            "newHardware"=>$item["newHardware"],
                        ];
                    }

                    $itemBomId = trim($item["bomId"]);
                    if ($itemBomId) {
                        if (array_key_exists($itemBomId, $result['bomIds'])) {
                            $result['bomIds'][$itemBomId] = $result['bomIds'][$itemBomId] + 1;
                        } else {
                            $result['bomIds'][$itemBomId] = 1;
                        }

                        $result['bomIdName'][$itemBomId][] = $item["hardwareName"];
                    }

                    $result["count"]++;
                } elseif (isset($flippedWhiteListArray[$offerId])) {
                    if (!empty($item["bundledItems"])) {
                        $this->getCount($item["bundledItems"], $oseItemList, $skuMMList, $whiteListArray, $result);
                    }
                    if (!empty($item["childItems"])) {
                        $this->getCount($item["childItems"], $oseItemList, $skuMMList, $whiteListArray, $result);
                    }
                    $oseItemList[] = $item["offerId"];
                    $result["count"]++;
                } else {
                    $result["other"]++;
                    $result["unsyncedItems"][] = [
                        'offerId' => $offerId,
                        'action' => $item["action"],
                        'productName' => $item["productName"]
                    ];
                }
            }
        }
        return $result;
    }

    /**
     * @return service address which is in quote
     */
    public function getServiceAddress()
    {
        $quoteId = Mage::getSingleton('checkout/cart')->getQuote()->getId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        return unserialize($quote->getServiceAddress());
    }

    /**
     * @return logged in agent name
     */
    public function getAgentName()
    {
        return Mage::helper('agent')->getAgentName();
    }

    /**
     * @return customer type from session
     */
    public function getCustomerType()
    {
        return (Mage::getSingleton('customer/session')->getCustomer()->getIsSoho()) ? 'SoHo' : 'Residential';
    }

    /**
     * @return order type from quote
     */
    private function getQuoteOrderType()
    {
        return Mage::getSingleton('checkout/cart')->getQuote()->getOrderType();
    }

    /**
     * save details stub to quote item
     */
    public function updateStub()
    {
        $i = 0;
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quoteItem = $quote->getAllItems();
        foreach ($quoteItem as $key => $item) {
            $i++;
            if  ($i <= 2) {
                $item->setBasketItemId('12355');
                $item->setOfferId('111');
                $item->setAction('add');
                $item->setBomId('112233');
                $item->setProductName('product name');
                $item->setHardwareName('hardware name');
                $item->setNewHardware(false);
                $item->setOfferType('triggerbindingonowned');
            }
            if($item->getPackageType() == Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_FIXED) {
                $item->setDispatchMethod(serialize(["Automatically defined", "Electronic bill", "Paper bill", "Electronic and paper bill", "Digital Note"]));
                $item->setPaymentMethod(serialize(["DIRECT_DEBIT", "ACCEPTGIRO"]));
            }
        }
        $quote->save();
    }

    /**
     * @param $invalidSku
     * check sku in cart
     * @return  boolean
     */
    public function checkSku($invalidSku)
    {
        $cartItems = $this->getCartProduct();
        foreach ($cartItems as $packageId => $package) {
            foreach ($package as $product) {
                if ($product->getSku() == $invalidSku) {
                    return true;
                }
            }
        }
    }

    /**
     * remove peal added product from cart
     */
    public function removePealAddedProduct()
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        foreach ($quote->getAllItems() as $key => $item) {
            if ($item->getData('added_via_peal')) {
                $itemId = $item->getData('item_id');
                $quote->removeItem($itemId);
                Mage::helper('vznl_utility')->saveModel($quote);
            }
        }
    }

    public function processValidateCart($validateCart, $callValidatecart) {
        $consolidation = false;
        $packageName = '';
        $hasHardwareProduct = false;
        $response = array();
        foreach (Mage::getModel('checkout/session')->getQuote()->getAllItems() as $item) {
            if($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE)) {
                $packageName = $item->getName();
            }
            if ($item->getAddedViaPeal()) {
                $hasHardwareProduct = true;
            }
        }

        $html = Mage::getSingleton('core/layout')->createBlock('dyna_configurator/rightsidebar')
            ->setTemplate('customer/expanded_cart.phtml')->toHtml();

        if ($hasHardwareProduct) {
            $hasHardwareProduct = $packageName;
        }

        if($validateCart === 'CONSOLIDATION' || $validateCart === 'CONSOLIDATIONFALSE') {
            $consolidation = true;
        }

        if ($validateCart === 'TRUE' || $validateCart === 'CONSOLIDATION' || $validateCart === 'CONSOLIDATIONFALSE') {
            $response["reload"] = false;
            $response["callValidatecart"] = $callValidatecart;
            $response["consolidation"] = $consolidation;
            $response["consolidationFalse"] = ($validateCart === 'CONSOLIDATIONFALSE') ? true : false;
            $response["hasHardwareProduct"] = $hasHardwareProduct;
            $response["html"] = $html;
        } else if (isset($validateCart["error"])) {
            $response["error"] = true;
            $response["message"] = $validateCart["message"];
            $response["button"] = $this->getMoreInfoButton($validateCart["errorType"]);
            $response["validateErrorMessage"] = $validateCart["validateErrorMessage"] ?? "";
            $response["unsyncedBasketItems"] = $validateCart["unsyncedBasketItems"] ?? array();
        } else {
            $response["reload"] = true;
            $response["callValidatecart"] = true;
            $response["message"] = $validateCart;
            $response["hasHardwareProduct"] = false;
            $response["html"] = $html;
        }

        return $response;
    }

    /**
     * @param errorType
     * @return string
     */
    public function getMoreInfoButton($errorType)
    {
        $html = '';
        switch ($errorType) {
            case "service-error":
              $html = '<div class="btn" onclick="showServiceErrorModal()">More info</div>';
              break;
            case "validate-error":
              $html = '<div class="btn" onclick="showValidateErrorModal()">More info</div>';
              break;
            case "basket-unsynced-error":
              $html = '<div class="btn" onclick="showBasketUnsyncedModal()">More info</div>';
              break;
        }

        return $html;
    }
}
