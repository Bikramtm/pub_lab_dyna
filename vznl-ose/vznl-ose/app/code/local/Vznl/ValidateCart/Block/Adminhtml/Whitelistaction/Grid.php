<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Whitelistaction_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('whiteListActionGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('validatecart/whitelistaction_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Create the white list action on sku
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('vznl_checkout')->__('ID'),
            'align'     =>'right',
            'width'     => '30px',
            'align'     =>'right',
            'index'     => 'entity_id'
        ));

        $this->addColumn('technical_id', array(
            'header'    => Mage::helper('vznl_checkout')->__('Technical ID'),
            'align'     =>'left',
            'index'     => 'technical_id'
        ));

        $this->addColumn('sku', array(
            'header'        => Mage::helper('vznl_checkout')->__('Product SKU'),
            'align'         => 'left',
            'index'         => 'sku'
        ));

        $this->addColumn('action', array(
            'header'    => Mage::helper('vznl_checkout')->__('Action'),
            'align'     => 'left',
            'index'     => 'action',
            'type'  => 'options',
            'options' => [
                'add' => Mage::helper('vznl_checkout')->__('Add'),
                'remove' => Mage::helper('vznl_checkout')->__('Remove'),
            ]
        ));

        $this->addColumn('direction', array(
            'header'    => Mage::helper('vznl_checkout')->__('Direction'),
            'align'     => 'left',
            'index'     => 'direction',
            'type'  => 'options',
            'options' => [
                'in' => Mage::helper('vznl_checkout')->__('In'),
                'out' => Mage::helper('vznl_checkout')->__('Out'),
                'both' => Mage::helper('vznl_checkout')->__('Both')
            ]
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $row->getEntityId()));
    }
}
