<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateCart_Block_Adminhtml_Pealsubstatus_Edit_Tab_Form
 */
class Vznl_ValidateCart_Block_Adminhtml_Pealsubstatus_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $data = Mage::registry('peal_substatus_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $dataFieldset = $form->addFieldset('reason', array('legend' => Mage::helper('vznl_checkout')->__('Peal Sub Status details')));

        $dataFieldset->addField('substatus', 'text', array(
            'label' => Mage::helper('vznl_checkout')->__('Sub Status'),
            'name' => 'substatus',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getSubstatus(),
        ));

        $dataFieldset->addField('translation', 'text', array(
            'label' => Mage::helper('vznl_checkout')->__('Translated Values'),
            'name' => 'translation',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getTranslation(),
        ));

        return parent::_prepareForm();
    }
}
