<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Pealvalidationerrorcode_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $data = Mage::registry('peal_validationerrorcode_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $dataFieldset = $form->addFieldset('reason', array('legend' => Mage::helper('vznl_checkout')->__('Peal Validation Error Code details')));

        $dataFieldset->addField('code', 'text', array(
            'label' => Mage::helper('vznl_checkout')->__('Error Code'),
            'name' => 'code',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getCode(),
        ));

        $dataFieldset->addField('error', 'text', array(
            'label' => Mage::helper('vznl_checkout')->__('Error Description'),
            'name' => 'error',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getError(),
        ));

        return parent::_prepareForm();
    }
}
