<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateCart_Block_Adminhtml_Pealsubstatus
 */
class Vznl_ValidateCart_Block_Adminhtml_Pealsubstatus extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_pealsubstatus';
        $this->_blockGroup = 'validatecart';
        $this->_headerText = 'Peal Sub Statuses';
        $this->_addButtonLabel = 'Add new sub status';
        parent::__construct();
    }
}
