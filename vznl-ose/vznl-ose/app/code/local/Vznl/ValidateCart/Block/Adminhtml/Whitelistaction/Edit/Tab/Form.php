<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Whitelistaction_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $data = Mage::registry('whitelist_action_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $dataFieldset = $form->addFieldset('reason', array('legend' => Mage::helper('vznl_checkout')->__('White List Action Details')));

        $dataFieldset->addField('technical_id', 'text', array(
            'label' => Mage::helper('vznl_checkout')->__('Technical Id'),
            'name' => 'technical_id',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getTechnicalId(),

        ));

        $dataFieldset->addField('sku', 'text', array(
            'label' => Mage::helper('vznl_checkout')->__('Product SKU'),
            'name' => 'sku',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getSku(),
        ));

        $dataFieldset->addField('action', 'select', array(
            'label' => Mage::helper('vznl_checkout')->__('Action'),
            'name' => 'action',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getAction(),
            'values' => [
                'add' => Mage::helper('vznl_checkout')->__('Add'),
                'remove' => Mage::helper('vznl_checkout')->__('Remove'),
            ]
        ));

        $dataFieldset->addField('direction', 'select', array(
            'label' => Mage::helper('vznl_checkout')->__('Direction'),
            'name' => 'direction',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getDirection(),
            'values' => [
                'both' => Mage::helper('vznl_checkout')->__('Both'),
                'in' => Mage::helper('vznl_checkout')->__('In'),
                'out' => Mage::helper('vznl_checkout')->__('Out')
            ]
        ));

        return parent::_prepareForm();
    }
}
