<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Whitelistaction extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Vznl_Validatecart_Block_Adminhtml_Whitelistaction constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_whitelistaction';
        $this->_blockGroup = 'validatecart';
        $this->_headerText = 'Whitelist actions on SKU';
        $this->_addButtonLabel = 'Add new Whitelist actions on SKU';
        parent::__construct();
    }
}