<?php

/**
 * Class Vznl_Order_Helper_Data
 */
class Vznl_Order_Helper_Data extends Dyna_Order_Helper_Data
{
    const RETRY_TIME = 'vodafone_service/webshop/order_get_retry_time';
    const RETRY_ITERATIONS = 'vodafone_service/webshop/order_get_iterations';

    /**
     * @var Vznl_Superorder_Model_Superorder
     */
    protected $superOrder;

    /**
     * @var Mage_Sales_Model_Resource_Order_Collection
     */
    protected $orders;

    /**
     * @var Vznl_Customer_Model_Customer
     */
    protected $customer;

    /**
     * @param string $orderNumber
     * @throws \Exception
     */
    public function submitOrder(string $orderNumber)
    {
        $this->loadSuperOrder($orderNumber);
        $this->process();
    }

    /**
     * @param string $orderNumber
     * @throws \Exception
     */
    public function loadSuperOrder(string $orderNumber)
    {
        $iterations = 0;
        while (true) {
            try {
                $this->superOrder = Mage::getModel('superorder/superorder')->load((string) $orderNumber, 'order_number');
            } catch (\Exception $e) {
                // just ignore the exception, nothing to do
                $this->superOrder = Mage::getModel('superorder/superorder');
            }

            // Check that the superOrder exists
            if (!$this->superOrder || !$this->superOrder->getId()) {
                /** @var Mage_Sales_Model_Quote $quote */
                $quote = Mage::getModel('sales/quote')
                    ->getCollection()
                    ->addFieldToFilter('superorder_number', $orderNumber)
                    ->getFirstItem();
                $exception = $quote->getHawaiiOrderException();
                if (!empty($exception)) {
                    throw new \Exception($exception);
                }
            } else {
                return $this->superOrder;
            }

            if ($iterations > $this->getRetryIterations()) {
                throw new \Exception('Order not found');
            }
            $iterations++;
            usleep($this->getRetryTime()*1000000);
        }

        // Check that the superOrder has valid orders
        $this->orders = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('superorder_id', $this->superOrder->getId())
            ->addFieldToFilter('edited', ['neq' => 1]);

        if (count($this->orders) < 1) {
            throw new \Exception('Order not found');
        }

        $this->customer = Mage::getModel('customer/customer')->load($this->superOrder->getCustomerId());

        if (!$this->customer->getId()) {
            throw new \Exception('Customer not found');
        }
    }

    /**
     * Process the HTTP request.
     *
     * @throws \Exception
     */
    public function process()
    {
        /** @var Vznl_Service_Helper_Data $serviceHelper */
        $serviceHelper = Mage::helper('vznl_service');

        /** @var Vznl_Service_Model_Client_EbsOrchestrationClient $client */
        $client = $serviceHelper->getClient('ebs_orchestration');

        try {
            $this->initAgent($this->superOrder->getAgentId(), $this->superOrder->getDealerId());

            // Call ESB
            $response = $client->processCustomerOrder(
                $this->superOrder,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                $this->getAmountToPay()
            );

            if (!empty($response)) {
                // Save order confirmation email in queue
                /** @var Vznl_Communication_Helper_Email $emailHelper */
                $emailHelper = Mage::helper('communication/email');
                /** @var Vznl_Communication_Model_Pool $emailPool */
                $emailPool = Mage::getSingleton('communication/pool');
                $emailPool->add(
                    $emailHelper->getOrderConfirmationPayload($this->superOrder)
                );

                /** @var Vznl_Communication_Helper_Sms $smsHelper */
                $smsHelper = Mage::helper('communication/sms');
                $smsHelper->createOrderConfirmationSms($this->superOrder);

                Mage::getSingleton('customer/session')->setCurrentSuperOrderId($this->superOrder->getId());
            } else {
                throw new \Exception('Error while processing order');
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param $agentId
     * @param $dealerId
     */
    protected function initAgent($agentId, $dealerId)
    {
        /** @var Vznl_Agent_Model_Dealer $dealer */
        $dealer = Mage::getModel('agent/dealer')->load($dealerId);

        /** @var Vznl_Agent_Model_Agent $agent */
        $agent = Mage::getModel('agent/agent')->load($agentId);
        $agent->setDealer($dealer);

        Mage::getSingleton('customer/session')->setAgent($agent);
    }

    /**
     * @return mixed
     */
    protected function getRetryIterations()
    {
        return Mage::getStoreConfig(self::RETRY_ITERATIONS);
    }

    /**
     * @return mixed
     */
    protected function getRetryTime()
    {
        return Mage::getStoreConfig(self::RETRY_TIME);
    }

    /**
     * @return array
     */
    protected function getAmountToPay()
    {
        $amountToPay = [];

        /** @var Vznl_Checkout_Model_Sales_Order $order */
        foreach ($this->superOrder->getOrders(true)->getItems() as $order) {
            if ($order->getPayment()->getMethod() !== 'adyen_hpp') {
                // Set total amount
                /** @var Omnius_Package_Model_Package $package */
                foreach ($order->getDeliveryOrderPackages() as $package) {
                    $totals = $package->getPackageTotals($order->getAllItems());
                    if (isset($amountToPay[$package->getPackageId()])) {
                        $amountToPay[$package->getPackageId()] += $totals['total'];
                    } else {
                        $amountToPay[$package->getPackageId()] = $totals['total'];
                    }
                }
            }
        }

        return $amountToPay;
    }
}
