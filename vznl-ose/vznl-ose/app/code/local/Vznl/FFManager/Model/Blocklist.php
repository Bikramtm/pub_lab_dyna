<?php

/**
 * Class Vznl_FFManager_Model_Blocklist
 *
 * @method string getEmail()
 * @method int createdAt()
 * @method Vznl_FFManager_Model_Blocklist setEmail(string $email)
 * @method Vznl_FFManager_Model_Blocklist setCreatedAt(int $createdAt)
 */
class Vznl_FFManager_Model_Blocklist extends Mage_Core_Model_Abstract
{
    /**
     * @var Dyna_FFManager_Model_Relation
     */
    protected $relation;

    protected function _construct(){
        $this->_init("ffmanager/blocklist");
    }
    public function _beforeSave()
    {
        $dateTime = Zend_Date::now();
        if (!$this->getId()) {
            $this->setCreatedAt($dateTime->getTimestamp());
        }

        parent::_beforeSave();
    }

    public function removeFromBlockList()
    {
        /**
         * @var Dyna_FFHandler_Model_Resource_Request_Collection $requestCollection
         */
        $requestCollection = Mage::getModel("ffhandler/request")->getCollection();

        $requestCollection->markThresholdNotCountable($this->getEmail());
        Mage::log('[model Blocklist][getRelation] remove from block list the following = ' . var_export($this->toArray(), true));
        return $this->delete();
    }
}