<?php

class Vznl_FFManager_Model_Resource_Relation_Auth_Value_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("ffmanager/relation_auth_value");
    }

    /**
     * @param int $relationId
     * @return $this
     */
    public function filterByRelationId($relationId)
    {
        $this->addFieldToFilter("relation_id", $relationId);

        return $this;
    }
}