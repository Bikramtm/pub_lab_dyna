<?php

class Vznl_FFManager_Model_Resource_Blocklist_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("ffmanager/blocklist");
    }

    /**
     * @param $email
     * @return $this
     */
    public function filterByEmail($email)
    {
        $this->addFieldToFilter("email", $email);

        return $this;
    }
}