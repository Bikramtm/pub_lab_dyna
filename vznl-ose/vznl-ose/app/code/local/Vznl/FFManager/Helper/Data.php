<?php

class Vznl_FFManager_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getAuthTypes()
    {
        return array(
            Vznl_FFManager_Model_Relation::AUTH_TYPE_CODE_EMAIL_EXT => $this->__('Email Extension'),
            Vznl_FFManager_Model_Relation::AUTH_TYPE_CODE_EMAIL => $this->__('Unique Email'),
            Vznl_FFManager_Model_Relation::AUTH_TYPE_CODE_CUSTOMER_REF => $this->__('Customer reference'),
            Vznl_FFManager_Model_Relation::AUTH_TYPE_GENERIC_CODE => $this->__('General code')
        );
    }

    /**
     * Returns the domain from a given email
     * This will also keep @ (Ex. @gmail.com) character at the beginning of the domain
     *
     * @param $email
     * @return string
     */
    public function getDomainFromEmail($email)
    {
		$vArr = explode('@', $email);
		return sprintf("@%s", array_pop($vArr));
    }

    public function validateDomain($domain)
    {
        $re = "/^@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i";
        return preg_match($re, $domain);
    }

    /**
     * Validates the e-mail address to not be empty and to be a valid email address
     *
     * @access public
     * @param string $emailAddress
     * @return bool
     */
    public function validateEmail($emailAddress)
    {
        if (!$emailAddress || !filter_var($emailAddress, FILTER_VALIDATE_EMAIL))
        {
            Mage::log('[Helper manager data][validateEmail] email '.$emailAddress. ' is invalid' );
            return false;
        }

        return true;
    }

    /**
     * Parses the array given with e-mail addresses and returns an array that contains the following values:
     * - invalidEmails: will contain only the e-mails that were found as not valid from the given list
     * - validEmails: will contain only the valid e-mails from the given list
     * - totalNoEmails: will contain the total no of e-mails (valid and invalid)
     *
     * @access public
     * @param array $emailAddressesList
     * @return array
     */
    public function getInvalidAndValidEmailsFromList($emailAddressesList)
    {
        $invalidEmailAddresses = [];
        $validEmailAddresses = [];
        $emailAddressesList = explode(',', $emailAddressesList);
        $noInvalidEmails = 0;
        $noValidEmails = 0;
        foreach($emailAddressesList as $emailAddress)
        {
            $email = trim($emailAddress);
            if(!$this->validateEmail($email))
            {
                $invalidEmailAddresses[$noInvalidEmails] = $emailAddress;
                $noInvalidEmails ++;
            }
            else
            {
                $validEmailAddresses[$noValidEmails] = $emailAddress;
                $noValidEmails ++;
            }
        }

        return ['invalidEmails' => $invalidEmailAddresses, 'validEmails' => $validEmailAddresses, 'totalNoEmails' => count($emailAddressesList)];
    }
}