<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

/**
 * Class Vznl_Customer_Helper_Search Helper
 */
class Vznl_Customer_Helper_Search extends Dyna_Customer_Helper_Search
{
    const API_CREATE_CUSTOMER = 'create/party/account';
    const API_CUSTOMER_LOG = 'customer_search_api.log';

    const API_CUSTOMER_TYPE_ORGANIZATION = 'organization';
    const API_CUSTOMER_TYPE_INDIVIDUAL = 'Individual';

    const API_CUSTOMER_STATUS_ACTIVE = 'active';
    const API_CUSTOMER_STATUS_INACTIVE = 'inactive';

    const API_CUSTOMER_CTN_TYPE_MOBILE = 'mobile';
    const API_CUSTOMER_CTN_TYPE_FIXED = 'fixed';

    const JWT_CUSTOMER_SEARCH_SECRET = 'secret';

    const API_CUSTOMER_CHANNEL_DEFAULT = 'default';

    const LOG_FAILED_ORDERS = 'api_failed_orders.log';
    const LOG_RETRIEVE_CUSTOMER_CALLED_ORDERS = 'api_failed_orders.log';

    /**
     * Parsing the form input search fields and validating them;
     * If the fields are not valid => the error messages will be returned
     *
     * @param array $params
     * @return array
     */
    public function parseCustomerSearchParams($params)
    {
        $searchParams = $this->removeEmptyParameters($params);

        if (!$searchParams) {
            return [
                'error' => 1,
                "message" => $this->__('Please refine your search. No parameters given.')
            ];
        }

        /**
         * @var $validationHelper Vznl_Customer_Helper_Validation
         */
        $validationHelper = Mage::helper('vznl_customer/validation');
        // todo validate the parameters
        return $validationHelper->validateCustomerSearchParameters($searchParams);
    }

    /**
     * Parse and validate the input for an advanced search request.
     * If the fields are not valid error messages will be returned.
     *
     * @param array $params
     * @return array
     */
    public function parseCustomerSearchAdvancedParams($params)
    {
        $searchParams = $this->removeEmptyParameters($params);

        /**
         * @var $validationHelper Vznl_Customer_Helper_Validation
         */
        $validationHelper = Mage::helper('vznl_customer/validation');

        return $validationHelper->validateCustomerAdvancedSearchParameters($searchParams);
    }

    /**
     * @param array $params
     * @return array
     */
    private function removeEmptyParameters($params)
    {
        $searchParams = [];

        foreach ($params as $parameter => $value) {
            if (!empty(trim($value))) {
                $searchParams[$parameter] = $value;
            }
        }

        return $searchParams;
    }

    /**
     * Map customer fields on save and send to Prospect API
     *
     * @param array $customer
     * @param string $lineOfBusiness
     * @return array
     */
    public function mapSaveCustomerFields($customer, $lineOfBusiness)
    {
        $address = Mage::getModel('customer/address')->load($customer->getDefaultServiceAddressId());
        $customerData = [];
        $customerEmail =  $customer->getAdditionalEmail();
        $customerPhone = $customer->getPhoneNumber();
        $customerData['customerType'] = $customer->getIsBusiness() ? self::API_CUSTOMER_TYPE_ORGANIZATION : self::API_CUSTOMER_TYPE_INDIVIDUAL;
        if ($customer->getIsBusiness()) {
            $customerData['organizationName'] = $customer->getCompanyName();
            $customerData['salesChannel'] = Mage::app()->getWebsite()->getCode();
        } else {
            $customerData['firstName'] = $customer->getFirstname();
            $customerData['lastName'] = $customer->getLastname();
            $customerData['dateOfBirth'] = $customer->getDob();
            $customerData['gender'] = ($customer->getGender() == 1)? 'male' : 'female';
            $customerData['salutation'] = $customer->getSalutation();
        }
        $customerData['contactDetails'] = [];

        if ($customerEmail!="") {
            array_push($customerData['contactDetails'], array(
                    'contactType' => 'email',
                    'value' => "$customerEmail",
                )
            );
        }

        if ($customerPhone != "") {
            array_push($customerData['contactDetails'],array(
                'contactType' => 'phone',
                'type' => 'mobile',
                'value' => "$customerPhone",
            ));
        }

        if ($address) {
            array_push($customerData['contactDetails'],array(
                'contactType' => 'address',
                'city' => $address->getCity(),
                'street' => $address->getStreet1(),
                'houseNumber' => $address->getStreet2(),
                'postalCode' => $address->getPostcode(),
                'country' => $address->getCountryId(),
                'type' => 'billing_address'
            ));
        }

        $customerData['metaData']             = (object) [];
        $customerData['roleType']             = 'role';
        $customerData['lineOfBusiness']       = $lineOfBusiness;
        $customerData['accountStatus']        = self::API_CUSTOMER_STATUS_ACTIVE;
        $customerData['orderNumbers']         = [];

        return $customerData;
    }

    /**
     * Map customer fields on saveCustomer in checkout page to Prospect API
     *
     * @param array $customer
     * @param array $customerAddress
     * @param array $quote
     * @param string $lineOfBusiness
     * @return array
     */
    public function mapSaveCheckoutFields($customer, $customerAddress, $quote, $lineOfBusiness)
    {
        $customerStreet = $customerAddress->getStreet1();
        $customerHouseNo = $customerAddress->getStreet2();
        //house no extension
        if ($customerAddress->getStreet3()!="") {
            $customerHouseNo = $customerHouseNo . " " .$customerAddress->getStreet3();
        }
        $customerCity = $customerAddress->getData('city');
        $customerCountry = $customerAddress->getData('country_id');
        $customerPhone = $customerAddress->getData('telephone');
        $customerSalutation = $customerAddress->getData('prefix');
        $postcode = $customerAddress->getData('postcode');
        $dateOfBirth = $customer->getDob();
        $customerEmail = $customer->getAdditionalEmail();

        $isFixed = $quote->hasFixed();

        $customerData = [];
        $customerData['customerType'] = $customer->getIsBusiness()? self::API_CUSTOMER_TYPE_ORGANIZATION : self::API_CUSTOMER_TYPE_INDIVIDUAL;
        if ($customer->getIsBusiness()) {
            $customerData['organizationName'] = $customer->getCompanyName();
            $customerData['chamberOfCommerce'] = $customer->getCompanyCoc();
            $customerData['salesChannel'] = Mage::app()->getWebsite()->getCode();
        } else {
            $customerData['firstName'] = $customer->getFirstname();
            $customerData['lastName'] = $customer->getLastname();
            $customerData['dateOfBirth'] = date("Y-m-d", strtotime($dateOfBirth));
            $customerData['gender'] = ($customer->getGender() == 1) ? "male" : "female";
            $customerData['salutation'] = $customerSalutation;
        }
        $customerData['contactDetails']   = [];

        if ($customerEmail!="") {
            array_push($customerData['contactDetails'],array(
                'contactType' => 'email',
                'value' => "$customerEmail",
            ));
        }

        if ($customerPhone!="") {
            array_push($customerData['contactDetails'],array(
                'contactType' => 'phone',
                'type' => 'mobile',
                'value' => "$customerPhone",
            ));
        }

        array_push($customerData['contactDetails'],array(
            'contactType' => 'address',
            'city' => "$customerCity",
            'street' => "$customerStreet",
            'houseNumber' => "$customerHouseNo",
            'postalCode' => "$postcode",
            'country' => "$customerCountry",
            'type' => 'billing_address'
        ));

        $customerData['metaData']             = (object) [];
        $customerData['roleType']             = 'party';
        $customerData['lineOfBusiness']       = $lineOfBusiness;
        $customerData['accountStatus']        = self::API_CUSTOMER_STATUS_ACTIVE;
        $customerData['orderNumbers']         = [];
        return $customerData;
    }

    /**
     * Map customer fields on save shopping cart for existing customer
     *
     * @param array $customer
     * @param array $quote
     * @param string $lineOfBusiness
     * @return array
     */
    public function mapSaveExistingFields($customer,$quote, $lineOfBusiness)
    {
        $address = Mage::getModel('customer/address')->load($customer->getData('default_billing'));
        $customerStreet = $address->getStreet1();
        $customerHouseNo = $address->getStreet2();
        $customerCity = $address->getCity();
        $postcode = $address->getPostcode();
        $customerCountry = $address->getCountry();
        $customerPhone = $customer->getPhoneNumber();
        $customerSalutation = $customer->getSalutation();
        $dateOfBirth = $customer->getDob();
        $customerEmail = array_unique(explode(';',$customer->getAdditionalEmail()));

        if($customerCity == "") {
            $billing = $quote->getBillingAddress();
            $customerCity = $billing->getCity();
            $customerStreet = $billing->getStreet1();
            $customerHouseNo = $billing->getStreet2();
            $postcode = $billing->getPostcode();
            $customerCountry = $billing->getData('country_id') == "" ? Vznl_Checkout_Model_Sales_Order::DEFAULT_COUNTRY : $billing->getData('country_id') ;
        }

        $customerData = [];
        $customerData['customerType']     = $customer->getIsBusiness()? self::API_CUSTOMER_TYPE_ORGANIZATION : self::API_CUSTOMER_TYPE_INDIVIDUAL;

        switch ($lineOfBusiness) {
            case 'mobile':
                $customerData['accountNumber'] = $customer->getBan();
                break;
            case 'fixed':
                $customerData['accountNumber'] = $customer->getPan();
                break;
        }

        if ($customer->getIsBusiness()) {
            $customerData['organizationName']          = $customer->getCompanyName();
            $customerData['chamberOfCommerce']         = $customer->getCompanyCoc();
            $customerData['salesChannel']              = Mage::app()->getWebsite()->getCode();
        } else {
            $customerData['firstName'] = $customer->getFirstname();
            $customerData['lastName'] = $customer->getLastname();
            $customerData['dateOfBirth'] = date("Y-m-d", strtotime($dateOfBirth));
            $customerData['gender'] = ($customer->getGender() == 1) ? "male" : "female";
            $customerData['salutation'] = $customerSalutation;
        }

        $customerDetails = array();
        //Loop to create an array of additional customer emails.
        $customerEmailCount = count($customerEmail);
        for ($i = 0; $i < $customerEmailCount; $i++) {
            if($customerEmail[$i]!="") {
                $customerDetails[] = array(
                    'contactType' => 'email',
                    'value' => "$customerEmail[$i]",
                );
            }
        }

        if ($customerPhone!="") {
            $customerDetails[] = array(
                'contactType' => 'phone',
                'type' => 'mobile',
                'value' => "$customerPhone",
            );
        }

        if ($customerCity!="" && $customerStreet!="") {
            $customerDetails[] = array(
                'contactType' => 'address',
                'city' => "$customerCity",
                'street' => "$customerStreet",
                'houseNumber' => "$customerHouseNo",
                'postalCode' => "$postcode",
                'country' => "$customerCountry",
                'type' => 'type'
            );
        }

        $customerData['contactDetails']       = $customerDetails;
        $customerData['metaData']             = (object) [];
        $customerData['roleType']             = 'party';
        $customerData['lineOfBusiness']       = $lineOfBusiness;
        $customerData['accountStatus']        = self::API_CUSTOMER_STATUS_ACTIVE;
        $customerData['orderNumbers']         = [];
        return $customerData;
    }

    /**
     * Customer search API call - create a prospect in Elastic search
     *
     * @param array $customer
     * @param array $CustomerAddress
     * @param $quote
     * @param string $type
     * @return string
     */
    public function apiCall($customer,$CustomerAddress,$quote,$type,$lineOfBusiness)
    {
        switch ($type) {
            case 'SaveCustomerFields':
                $data = $this->mapSaveCustomerFields($customer, $lineOfBusiness);
                break;
            case 'SaveCheckoutFields':
                $data = $this->mapSaveCheckoutFields($customer,$CustomerAddress,$quote, $lineOfBusiness);
                break;
            case 'SaveExistingFields':
                $data = $this->mapSaveExistingFields($customer,$quote, $lineOfBusiness);
                break;
            default:
                Mage::throwException('Unsupported generator format');
        }
        $endpoint = $this->getConfigForEndpoint();
        $api_url = $endpoint . DS . Vznl_Customer_Helper_Search::API_CREATE_CUSTOMER;

        $options = [
            'verify' => $this->getConfigForVerify(),
            'proxy' => $this->getConfigForProxy(),
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'body' => json_encode($data),
            //'http_errors' => false,
        ];
        try {
            $client = new Client();
            $res = $client->request('POST', $api_url, $options);
            $response = json_decode($res->getBody());
            Mage::log("Create Customer : $api_url\n\n".json_encode($data)."\n\nCreate Customer Response :\n\n" . $res->getBody() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
            return $response->data;
        } catch (Exception $e) {
            Mage::log("Create Customer : $api_url\n\n".json_encode($data)."\n\nCreate Customer Error : \n\n" . $e->getMessage() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        }
    }

    /**
     * Customer search API call - convert prospect to active account.
     * @param Vznl_Customer_Model_Customer $customer
     */
    public function updateProspectApiCall($customer)
    {
        $accountNumber = $customer->getBan();
        $customerIdentifier = $customer->getAccountIdentifierMobile();
        $endpoint = $this->getConfigForEndpoint();
        $url = $endpoint . DS . 'prospect/account/'. $customerIdentifier .'/account-number/'. $accountNumber .'/lob/mobile';

        $options = [
            'verify' => $this->getConfigForVerify(),
            'proxy' => $this->getConfigForProxy(),
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ];

        try {
            $client = new Client();
            $response = $client->request('PUT', $url, $options);
            Mage::log("Convert Prospect Customer : " . $url . "\n\nConvert Prospect Customer Response : \n\n". $response->getBody() . "\n\n",null,Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        } catch (Exception $e) {
            Mage::log("Convert Prospect Customer : " . $url . "\n\nConvert Prospect Customer Error : \n\n". $e->getMessage() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        }

        return $customer;
    }

    /**
     * Customer search API call - Send Order Number.
     *
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param Vznl_Customer_Model_Customer $customer
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateOrderNumberApiCall($superOrder, $customer)
    {
        $orderNumber = $superOrder->getOrderNumber();

        $customerIdentifier = null;

        if ($superOrder->hasFixed()) {
            $customerIdentifier = $customer->getAccountIdentifierFixed();
        }
        if ($superOrder->hasMobilePackage()) {
            $customerIdentifier = $customer->getAccountIdentifierMobile();
        }
        if (empty($customerIdentifier)) {
            $customerIdentifier = $customer->getAccountIdentifierFixed() ?? $customer->getAccountIdentifierMobile();
        }

        if ($customerIdentifier) {
            $endpoint = $this->getConfigForEndpoint();
            $url=$endpoint . DS . 'account/' . $customerIdentifier . '/indexOrders';
            $data['orderNumbers'] = array($orderNumber);

            $options=[
                'verify' => $this->getConfigForVerify(),
                'proxy' => $this->getConfigForProxy(),
                'headers'=> [
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode($data),
            ];

            try {
                $client = new Client();
                $response = $client->request('POST', $url, $options);
                Mage::log("IndexOrder : $url\n\n" . json_encode($data) . "\n\nIndexOrder Response : \n\n" . $response->getBody() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
            } catch (Exception $e) {
                Mage::log("IndexOrder : $url\n\n" . json_encode($data) . "\n\nIndexOrder Error : \n\n" . $e->getMessage() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
            }
        } else {
            Mage::log(
                "UpdateOrderNumber Error, missing  AccountIdentifier for order: $orderNumber\n\n",
                null,
                Vznl_Customer_Helper_Search::API_CUSTOMER_LOG
            );
        }
    }

    /**
     * Customer and prospect link API call - Elastic search
     *
     * @param array $customer
     * @param string $eventIdentifier
     * @param string $newEventIdentifier
     * @return string
     */
    public function linkCustomerProspectApiCall($customer,$eventIdentifier,$newEventIdentifier)
    {
        $storeId = $customer->getStoreId();
        $agentId = Mage::helper('agent')->getAgentId();
        $salesChannel = Mage::app()->getWebsite()->getCode();
        $endpoint = $this->getConfigForEndpoint();
        $url = $endpoint . DS . 'account/link';
        $data = [];
        $data['storeId'] = $storeId;
        $data['agentId'] = $agentId;
        $data['salesChannel'] = $salesChannel;
        $data['customerAccountIdentifiers'] = array
        (
            $eventIdentifier,
            $newEventIdentifier
        );

        $options = [
            'verify' => $this->getConfigForVerify(),
            'proxy' => $this->getConfigForProxy(),
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'body' => json_encode($data),
        ];

        try {
            $client = new Client();
            $res = $client->request('POST', $url, $options);
            $response = json_decode($res->getBody());
            Mage::log("Link customer and prospect : $url\n\n" . json_encode($data) . "\n\nLink customer and prospect Response : \n\n" . $res->getBody() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
            return $response->eventIdentifier;
        } catch (Exception $e) {
            Mage::log("Link customer and prospect : $url\n\n" . json_encode($data) . "\n\nLink customer and prospect Error : \n\n" . $e->getMessage() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        }
    }

    /**
     * Create customer link
     *
     * @param Vznl_Customer_Model_Customer $customer
     * @param string $customerBilling
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param string $type
     * @return object
     */
    public function createApiCustomer($customer, $customerBilling, $quote, $type) {
        $linkId = $customer->getAccountIdentifierLinkId();
        $accountIdentifierMobile = $customer->getAccountIdentifierMobile();
        $accountIdentifierFixed = $customer->getAccountIdentifierFixed();
        $isFixed = $quote->hasFixed();
        $isMobile = $quote->hasMobilePackage();
        if (!$linkId) {
            if (!$accountIdentifierMobile || !$accountIdentifierFixed) {
                $eventIdentifier = $newEventIdentifier = "";
                if ($isFixed && !isset($accountIdentifierFixed)) {
                    $eventIdentifier = $accountIdentifierMobile;
                    // API call to create a prospect fixed customer for existing mobile customer - elastic search.
                    $data = $this->apiCall($customer, $customerBilling, $quote, $type, 'fixed');
                    $accountIdentifier = is_object($data) ? $data->accountIdentifier : '';
                    $partyIdentifier = is_object($data) ? $data->partyIdentifier : '';
                    $customer
                        ->setAccountIdentifierFixed($accountIdentifier)
                        ->setPartyIdentifierFixed($partyIdentifier)
                        ->save();
                }
                if ($isMobile && !isset($accountIdentifierMobile)) {
                    $eventIdentifier = $accountIdentifierFixed;
                    // API call to create a prospect mobile customer for existing fixed customer - elastic search.
                    $data = $this->apiCall($customer, $customerBilling, $quote, $type, 'mobile');
                    $accountIdentifier = is_object($data) ? $data->accountIdentifier : '';
                    $partyIdentifier = is_object($data) ? $data->partyIdentifier : '';
                    $customer
                        ->setAccountIdentifierMobile($accountIdentifier)
                        ->setPartyIdentifierMobile($partyIdentifier)
                        ->save();
                }
                if($customer->getAccountIdentifierMobile() && $customer->getAccountIdentifierFixed()) {
                    // API call to create link between customer account and prospect account
                    $newLinkId = $this->linkCustomerProspectApiCall($customer, $customer->getAccountIdentifierMobile(), $customer->getAccountIdentifierFixed());
                    $customer->setAccountIdentifierLinkid($newLinkId)->save();
                }
            }
        }

        return $customer;
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Order $salesOrder
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function saveDataForExternalServices($salesOrder)
    {
        // Get customer
        /** @var Vznl_Customer_Model_Customer $customer */
        $customer = Mage::getModel('vznl_customer/customer')->load($salesOrder->getCustomerId());

        // Make call to customer search API
        $this->createApiCustomer($customer, '', $salesOrder, 'SaveExistingFields');

        // Get superOrder
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($salesOrder->getSuperorderId());

        // Update all customer data if BAN is provided
        $this->fetchCustomerFromRetrieveCustomerInfo($customer, $superOrder, $salesOrder);
        $this->fetchCustomerCtnsFromRetrieveCustomerInfo($customer, $superOrder);

        // Make call to order number API
        $this->updateOrderNumberApiCall($superOrder, $customer);

        // Make call to ILT API
        Mage::helper('ilt')->sendParsedIltData($superOrder, $customer);
    }

    /**
     * Add customer CTN API call - Elastic search
     * @param Vznl_Customer_Model_Customer_Customer $customer
     * @param bool $status
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @return string
     */
    public function updateCustomerStatusApiCall(
        Vznl_Customer_Model_Customer_Customer $customer,
        bool $status
    ):string {
        $customerIdentifier = $customer->getAccountIdentifierMobile();

        $endpoint = $this->getConfigForEndpoint();
        $url = $endpoint . DS . 'account' . DS . $customerIdentifier . DS . 'status/change';

        $data = [
            'accountStatus' => $status ? self::API_CUSTOMER_STATUS_ACTIVE : self::API_CUSTOMER_STATUS_INACTIVE
        ];

        $options = [
            'verify' => $this->getConfigForVerify(),
            'proxy' => $this->getConfigForProxy(),
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'body' => json_encode($data),
        ];

        $result = '';
        try {
            $client = new Client();
            $res = $client->request('POST', $url, $options);
            $response = json_decode($res->getBody());
            $result = $response->eventIdentifier;
            Mage::log("Update customer status : $url\n\n" . json_encode($data) . "\n\nUpdate customer status Response : \n\n" . $res->getBody() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        } catch (Exception $e) {
            Mage::log("Update customer status : $url\n\n" . json_encode($data) . "\n\nUpdate customer status Error : \n\n" . $e->getMessage() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        }
        return $result;

    }

    /**
     * Add customer CTN API call - Elastic search
     * @param Vznl_Customer_Model_Customer_Customer $customer
     * @param string $ctn
     * @param string $ctnType
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @return string
     */
    public function addCustomerCtnApiCall(
        Vznl_Customer_Model_Customer_Customer $customer,
        string $ctn,
        string $ctnType
    ):string {
        $customerType = $customer->getIsBusiness()? self::API_CUSTOMER_TYPE_ORGANIZATION : self::API_CUSTOMER_TYPE_INDIVIDUAL;
        $partyIdentifier = '';

        switch ($ctnType) {
            case self::API_CUSTOMER_CTN_TYPE_MOBILE:
                $partyIdentifier = $customer->getPartyIdentifierMobile();
                $accountIdentifier = $customer->getAccountIdentifierMobile();

                if (empty($partyIdentifier) && !empty($accountIdentifier)) {
                    $partyIdentifier = $this->getPartyIdentificationForCustomerApiCall($accountIdentifier);
                    $customer
                        ->setPartyIdentifierMobile($partyIdentifier)
                        ->save();
                }
                break;

            case self::API_CUSTOMER_CTN_TYPE_FIXED:
                $partyIdentifier = $customer->getPartyIdentifierFixed();
                $accountIdentifier = $customer->getAccountIdentifierFixed();

                if (empty($partyIdentifier) && !empty($accountIdentifier)) {
                    $partyIdentifier = $this->getPartyIdentificationForCustomerApiCall($accountIdentifier);
                    $customer
                        ->setPartyIdentifierFixed($partyIdentifier)
                        ->save();
                }
                break;
        }

        $endpoint = $this->getConfigForEndpoint();
        $url = $endpoint . DS . $customerType . DS . $partyIdentifier . DS . 'contact/phone';

        $data = [
            'type' => $ctnType,
            'value' => $ctn
        ];

        $options = [
            'verify' => $this->getConfigForVerify(),
            'proxy' => $this->getConfigForProxy(),
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'body' => json_encode($data),
        ];

        $result = '';
        try {
            $client = new Client();
            $res = $client->request('POST', $url, $options);
            $response = json_decode($res->getBody());
            $result = $response->eventIdentifier;
            Mage::log("Add customer newCTN : $url\n\n" . json_encode($data) . "\n\nAdd customer newCTN Response : \n\n" . $res->getBody() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        } catch (Exception $e) {
            Mage::log("Add customer newCTN : $url\n\n" . json_encode($data) . "\n\nAdd customer newCTN Error : \n\n" . $e->getMessage() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        }
        return $result;
    }

    /**
     * Get partyID based on customerID from vNext search
     * @param string $customerId
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @return string
     */
    public function getPartyIdentificationForCustomerApiCall(
        string $customerId
    ):string {
        $endpoint = $this->getConfigForEndpoint();
        $url = $endpoint . DS . 'search/telesales';

        $jsonString = '{"query":{"bool":{"should":[{"match":{"_id":"' . $customerId . '"}}]}}}';

        $options = [
            'verify' => $this->getConfigForVerify(),
            'proxy' => $this->getConfigForProxy(),
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->getCustomerSearchJWT()
            ],
            'body' => $jsonString,
        ];

        $result = '';
        try {
            $client = new Client();
            $res = $client->request('POST', $url, $options);
            $response = json_decode($res->getBody());
            if (isset($response->hits->hits[0]) && isset($response->hits->hits[0]->_source->partyInfo[0]->identifier)) {
                $result = $response->hits->hits[0]->_source->partyInfo[0]->identifier;
            }
            Mage::log("Get customer partyID : $url\n\n" . $jsonString . "\n\nGet customer partyID Response : \n\n" . $res->getBody() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        } catch (Exception $e) {
            Mage::log("Get customer partyID : $url\n\n" . $jsonString . "\n\nGet customer partyID Error : \n\n" . $e->getMessage() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        }
        return $result;
    }

    /**
     * Get customer info based on customer BAN from vNext search
     * @param string $customerBan
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @return Dyna_Customer_Model_Customer|null
     */
    public function getCustomerByBanApiCall(string $customerBan): ?Dyna_Customer_Model_Customer
    {
        $endpoint = $this->getConfigForEndpoint();
        $url = $endpoint . DS . 'search/' . self::API_CUSTOMER_CHANNEL_DEFAULT;
        $payload = [
            'query' => [
                'bool' => [
                    'must' => [
                        ['nested' => [
                            'path' => 'customerAccountInfo',
                            'query' => [
                                'bool' => [
                                    'must' => [
                                        ['match' => ['customerAccountInfo.accountNumber' => $customerBan]],
                                        ['exists' => ['field' => 'customerAccountInfo.accountNumber']]
                                    ]
                                ]
                            ]
                        ]]
                    ]
                ]
            ]
        ];

        $options = [
            'verify' => $this->getConfigForVerify(),
            'proxy' => $this->getConfigForProxy(),
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->getCustomerSearchJWT()
            ],
            'body' => json_encode($payload),
        ];

        $result = null;
        try {
            $client = new Client();

            /** @var Response $res */
            $res = $client->request('POST', $url, $options);
            if ($res->getStatusCode() == 200) {
                $response = json_decode($res->getBody(), true);
                $sourcePart = $response['hits']['hits'][0]['_source'] ?? [];
                if (!empty($sourcePart)) {
                    /** @var Vznl_Customer_Model_Customer_Customer $customer */
                    $customer = Mage::getModel('vznl_customer/customer');
                    // Map search to model
                    $customerSearchData = $this->mapCustomerSearchFields($response['hits']['hits'][0]['_source']);
                    $customer->addData($customerSearchData);

                    return $customer;
                }
            }
        } catch (\Throwable $e) {
            Mage::log("Get customer partyID : $url\n\n" . json_encode($payload) . "\n\nGet customer partyID Error : \n\n" . $e->getMessage() . "\n\n", null, Vznl_Customer_Helper_Search::API_CUSTOMER_LOG);
        }
        return $result;
    }

    /**
     * Get config for endpoint
     * @return string
     */
    private function getConfigForEndpoint():string {
        return (string) Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/rest_api_end_point');
    }

    /**
     * Get config for verify peer
     * @return bool
     */
    private function getConfigForVerify():bool {
        return (bool) Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/verify');
    }

    /**
     * Get config for proxy
     * @return bool
     */
    private function getConfigForProxy():bool {
        return (bool) Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/proxy');
    }

    /**
     * Get JWT for customer search
     * @return bool
     */
    private function getCustomerSearchJWT():string {
        $agent = Mage::getSingleton('customer/session')->getAgent();
        $agentId = -1;
        $scopes = [
            'customer:search:bsl',
            'customer:search:peal',
            'customer:search:quick'
        ];
        if (!empty($agent)) {
            $agentId = $agent->getId();
            $scopes = $agent->getJwtDealerScopes();
        }
        $signer = new Sha256();

        return (new Builder())
            ->set('id', $agentId)
            ->set('scopes', $scopes)
            ->sign($signer, self::JWT_CUSTOMER_SEARCH_SECRET)
            ->getToken();
    }

    /**
     * @param array $response
     * @return array
     */
    private function mapCustomerSearchFields(array $response): array
    {
        switch (strtolower($response['partyInfo'][0]['gender']) ?? null) {
            case 'female':
                $gender = 2;
                break;
            case 'male':
                $gender = 1;
                break;
            default:
                $gender = null;
                break;
        }

        $data = [
            'customer_number' => $response['customerAccountInfo']['accountNumber'] ?? null,
            'firstname' => $response['partyInfo'][0]['firstName'] ?? null,
            'lastname' => $response['partyInfo'][0]['lastName'] ?? null,
            'gender' => $gender,
            'company_name' => $response['partyInfo'][0]['organizationName'] ?? null,
            'dob' => $response['partyInfo'][0]['dateOfBirth'] ?? null,
            'company_coc' => $response['partyInfo'][0]['chamberOfCommerce'] ?? null,
            'is_business' => (boolean)($response['partyInfo'][0]['type'] == self::API_CUSTOMER_TYPE_ORGANIZATION),
            'linkid' => $response['identifier'] ?? null,
        ];
        $contactDetails = $response['partyInfo'][0]['contactDetails'] ?? [];
        foreach ($contactDetails as $contactDetail) {
            if (!isset($data['phone']) && $contactDetail['contactType'] == 'phone') {
                $data['phone'] = $contactDetail['value'];
                continue;
            }
            if (!isset($data['email']) && $contactDetail['contactType'] == 'email') {
                $data['email'] = $contactDetail['value'];
                continue;
            }
        }

        return $data;
    }

    /**
     * Loads customer details from RetrieveCustomerInfo
     *
     * @param Vznl_Customer_Model_Customer $customer
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param Dyna_Checkout_Model_Sales_Order $salesOrder
     */
    protected function fetchCustomerFromRetrieveCustomerInfo($customer, $superOrder, $salesOrder)
    {
        $customerHasMissingData =
            ($customer->getIsBusiness() && (empty($customer->getCompanyName()) || empty($customer->getCompanyCoc()) || empty($customer->getCompanyVatId())))
            || (empty($customer->getFirstName()) || empty($customer->getMiddlename()) || empty($customer->getLastName()));

        if (!empty($customer->getData('ban')) && $customerHasMissingData) {
            try {
                // Retrieve customer profile
                $adapterFactoryName = Mage::getStoreConfig(
                    'omnius_service/customer_search_implementation_configuration/customer_search_adapter'
                );
                $adapter = $adapterFactoryName::create();
                $customerData = $adapter->getProfile($customer);

                Mage::log(
                    sprintf(
                        '%s called for OrderNumber: %s, internal customer ID: %s',
                        $adapterFactoryName,
                        $superOrder->getOrderNumber(),
                        $customer->getId()
                    ),
                    null,
                    self::LOG_RETRIEVE_CUSTOMER_CALLED_ORDERS
                );

                // When there is no customer data from OIL, we fail
                if ($customerData instanceof Exception) {
                    Mage::logException($customerData);
                    Mage::log(
                        'RetrieveCustomer (mode 1) API error for OrderNumber: ' . $superOrder->getOrderNumber(),
                        null,
                        self::LOG_FAILED_ORDERS
                    );
                    Mage::log($customerData->getMessage(), null, self::LOG_FAILED_ORDERS);
                } else {
                    // Update the local DB with the latest data for the customer from the service call
                    $updatedCustomer = $customer->updateCustomerInfo(
                        $customer->getId(),
                        !is_array($customerData) ? $customerData->getData() : $customerData
                    );

                    if ($customer->getIsBusiness()) {
                        $salesOrder
                            ->setContractantFirstname($updatedCustomer->getContractantFirstname())
                            ->setContractantMiddlename($updatedCustomer->getContractantMiddlename())
                            ->setContractantLastname($updatedCustomer->getContractantLastname())
                            ->setCompanyName($updatedCustomer->getCompanyName())
                            ->setCompanyCoc($updatedCustomer->getCompanyCoc());
                    } else {
                        $salesOrder
                            ->setCustomerFirstname($updatedCustomer->getFirstName())
                            ->setCustomerMiddlename($updatedCustomer->getMiddlename())
                            ->setCustomerLastname($updatedCustomer->getLastName());
                    }
                    $salesOrder->save();
                }
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::log(
                    'Fetch (mode 1) failed for OrderNumber: ' . $superOrder->getOrderNumber(),
                    null,
                    self::LOG_FAILED_ORDERS
                );
                Mage::log($e->getMessage(), null, self::LOG_FAILED_ORDERS);
            }
        }
    }

    /**
     * Loads customer CTNs from RetrieveCustomerInfo
     *
     * @param Vznl_Customer_Model_Customer $customer
     * @param Vznl_Superorder_Model_Superorder $superOrder
     */
    protected function fetchCustomerCtnsFromRetrieveCustomerInfo($customer, $superOrder)
    {
        if (!empty($customer->getData('ban'))) {
            try {
                // Retrieve customer installed base
                $adapterFactoryName = Mage::getStoreConfig(
                    'omnius_service/customer_search_implementation_configuration/customer_search_adapter'
                );
                $adapter = $adapterFactoryName::create();
                $customerData = $adapter->getInstalledBase($customer);

                // When there is no customer data from OIL, we fail
                if ($customerData instanceof Exception) {
                    Mage::logException($customerData);
                    Mage::log(
                        'RetrieveCustomer (mode 2) API error for OrderNumber: ' . $superOrder->getOrderNumber(),
                        null,
                        self::LOG_FAILED_ORDERS
                    );
                    Mage::log($customerData->getMessage(), null, self::LOG_FAILED_ORDERS);
                } else {
                    // Store customer contract id
                    Mage::helper('vznl_customer/panel')->saveCustomerCtns($customer);
                }
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::log(
                    'Fetch (mode 2) failed for OrderNumber: ' . $superOrder->getOrderNumber(),
                    null,
                    self::LOG_FAILED_ORDERS
                );
                Mage::log($e->getMessage(), null, self::LOG_FAILED_ORDERS);
            }
        }
    }
}
