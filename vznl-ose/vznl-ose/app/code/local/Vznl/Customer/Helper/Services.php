<?php

/**
 * Map customer data from RetrieveCustomerInfo service call to magento customer model / session model
 * Class Vznl_Customer_Helper_Services
 */

class Vznl_Customer_Helper_Services extends Dyna_Customer_Helper_Services
{
    CONST ACCOUNT_CATEGORY_BSL = "BSL";
    CONST ACCOUNT_CATEGORY_PEAL = "PEAL";
}