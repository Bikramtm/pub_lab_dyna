<?php

/**
 * Get customer data from customer model and prepare it to be sent to frontend templates
 * Class Vznl_Customer_Helper_Info
 */

class Vznl_Customer_Helper_Info extends Dyna_Customer_Helper_Info
{
    /**
     * Return an array containing customer data needed for left_details.phtml template
     * Called from Customer/DetailController loadCustomerOnSession()
     */
    public function buildCustomerDetails(
        Dyna_Customer_Model_Customer $customer = null,
        $mode = null,
        $prospectCustomer = false
    ) {
        /**
         * @var $panelHelper Dyna_Customer_Helper_Panel
         */
        $panelHelper = Mage::helper('dyna_customer/panel');
        $customerSession = Mage::getSingleton('customer/session');

        // If not customer sent as parameter, load the one from session
        // If it's passed, set it on the session as the current one that's
        // present on the session does not have all the data assigned to it
        // and it's used further in the execution to make the service calls
        if (!$customer) {
            $customer = $customerSession->getCustomer();
        } else {
            $customerSession->setCustomer($customer);
        }
        $fixed_customer = $customer && $customer->getPan();

        // Using Varien_Object data array
        $customerData = new Varien_Object();

        if($customer->getData('ban') == "" && $customer->getData('pan') == "") {
            $prospectCustomer = true;
        }

        if (($mode != 1) && (!$prospectCustomer)) {
            $panelHelper->getOrdersCount($customer);
            $customerData->setData('orders_count', Mage::helper("vznl_customer")->getCustomerOrderCount());
            $customerData->setData('products_count', $panelHelper->getProductsCount());
            $customerData->setData('permissionMessages', $panelHelper->getStackPermissionMessages());
            if ($fixed_customer) {
                $customerData->setData('fixedHasInstalledBase', $panelHelper->getFixedHasInstalledBase($customer));
            }

            if ($mode == 2) {
                return $customerData->getData();
            }
        } else {
            $customerData->setData('orders_count', Mage::helper("vznl_customer")->getCustomerOrderCount());
            $customerData->setData('products_count', 0);
        }

        $contractantPrefix = Mage::helper('vznl_checkout')->getGenderPrefix($customer->getContractantPrefix());
        $customerData->setData('id', $customer->getId());
        $customerData->setData('hasMobile', $this->currentCustomerHasMobile());
        $customerData->setData('hasCable', $this->currentCustomerHasCable());
        $customerData->setData('hasFixed', $this->currentCustomerHasFixed());
        $customerData->setData("isSoho", (bool) $customer->getIsBusiness());
        $customerData->setData("title", $this->__($customer->getPrefix()));
        $customerData->setData("salutation", $customer->getData('salutation'));
        $customerData->setData("isProspect", $prospectCustomer);
        $customerData->setData("first_name", $customer->getFirstName());
        $customerData->setData("middle_name", $customer->getData('middlename'));
        $customerData->setData("last_name", $customer->getLastName());
        $customerData->setData("street", $customer->getStreet());
        $customerData->setData("no", $customer->getHouseNo());
        $customerData->setData("house_addition", $customer->getHouseAddition());
        $customerData->setData("postal_code", $customer->getPostalCode());
        $customerData->setData("city", $customer->getCity());
        $customerData->setData("state", $customer->getState());
        $customerData->setData("contact_number_prefix", $customer->getLocalAreaCode());
        $customerData->setData("contractant_prefix", $contractantPrefix ? $this->__($contractantPrefix) : $this->__($customer->getContractantPrefix()));
        $email = current(explode(";",$customer->getAdditionalEmail()));
        if (trim($email) == Vznl_Customer_Model_Customer::DUMMY_EMAIL_ADDRESS) {
            $customerData->setData("email", null);
        } else {
            $customerData->setData("email", $email);
        }

        if ($customer->getDob()) {
            $customerData->setData("birth_date", date('d-m-Y', strtotime($customer->getDob())));
        }
        $customerData->setData('valid_until' ,$customer->getValidUntil());
        $customerData->setData('issue_date' ,$customer->getIssueDate());
        $customerData->setData('id_type' ,$customer->getIdType());
        $customerData->setData('id_number' ,$customer->getIdNumber());
        $customerData->setData('issuing_country' ,$customer->getIssuingCountry());
        $customerData->setData('iban' ,$customer->getAccountNumber());
        $customerData->setData("kvk_number", $customer->getCompanyRegistrationNumber());
        $customerData->setData("company_coc", $customer->getCompanyCoc());
        $customerData->setData("customer_number", $customer->getCustomerNumber());
        $customerData->setData("global_id", $customer->getCustomerGlobalId());
        $customerData->setData("ban", $customer->getBan());
        $customerData->setData("pan", $customer->getPan());
        $customerData->setData("isLinked", $customer->getBan() && $customer->getPan());
        $customerData->setData("service_address_id", $customer->getServiceAddressId());
        $customerData->setData("company_name", $customer->getCompanyName());
        $customerData->setData("contactPerson", [
            "title" => $customer->getTitle(),
            "first_name" => $customer->getFirstName(),
            "last_name" => $customer->getLastName(),
        ]);
        $customerData->setData("allow_ziggo", $customerSession->getAgent()->isGranted(Vznl_Agent_Model_Agent::ALLOW_ZIGGO));

        $customerData->setData("sell_fixed_to_blacklisted", $customerSession->getAgent()->isGranted(Vznl_Agent_Model_Agent::SELL_FIXED_TO_BLACKLISTED));
        $customerData->setData("no_ordering_allowed", $customer->getData('no_ordering_allowed'));

        $customerData->setData('carts_count', count($customer->getShoppingCarts(true)));
        $customerData->setData('in_dunning', $customer->getDunningStatus());
        if ($legalAddress = $customer->getAddress()) {
            $customerData->setData('legal_address_id', $customer->getLegalAddressId());
        }

        $customerType = $customer->getAccountCategory();
        $customerData->setData('customer_type', $customerType);
        $customerData->setData('customer_subtype', $customer->getData('party_subtype'));
        $customerData->setData('peal_party_subtype', $customer->getData('peal_party_subtype'));
        // Legal address should be used
        if ($legalAddress) {
            $customerData->setData("legal_address", [
                "id" => $customer->getLegalAddressId(),
                "street" => $legalAddress->getData('street'),
                "no" => $legalAddress->getHouseNumber(),
                "postal_code" => $legalAddress->getPostcode(),
                "city" => $legalAddress->getCity(),
                "house_addition" => $legalAddress->getHouseAddition(),
                "contact_number" => $legalAddress->getTelephone(),
            ]);
        } else {
            // If no real legal address was found fall back to customer address (prevents the usage of service address)
            $address = Mage::getModel('customer/address')->load($customer->getData('default_billing'));

            $customerData->setData("legal_address", [
                "id" => '',
                "street" => $address->getStreet(1),
                "no" => $address->getStreet(2),
                "postal_code" => $address->getData('postcode'),
                "city" => $address->getData('city'),
                "house_addition" => $address->getStreet(3),
                "contact_number" => $address->getData('telephone'),
            ]);
        }

        $customerData->setData(
            'has_excluded_cable_account',
            Mage::getSingleton('dyna_customer/session')->getHasExcludedCableAccount()
        );
        Mage::getSingleton('dyna_customer/session')->unsetData('has_excluded_cable_account');
        $customerData->setData(
            'has_excluded_mobile_account',
            Mage::getSingleton('dyna_customer/session')->getHasExcludedMobileAccount()
        );
        Mage::getSingleton('dyna_customer/session')->unsetData('has_excluded_mobile_account');

        // set counter for displaying in left panel
        $customerData->setData('household_members_count', $customerSession->getData('household_members_count') ?? 0);

        $service_addressId = $customer->getData('default_service_address_id');
        $serviceAddress = Mage::getModel('customer/address')->load($service_addressId);
        if ($serviceAddress) {
            $customerData->setData("service_address", [
                "id" => $customer->getServiceAddressId(),
                "street" => $serviceAddress->getStreet(1),
                "no" => $serviceAddress->getStreet(2),
                "postal_code" => $serviceAddress->getPostcode(),
                "city" => $serviceAddress->getCity(),
                "house_addition" => $serviceAddress->getStreet(3),
                "contact_number" => $serviceAddress->getTelephone(),
            ]);
        }

        return $customerData->getData();
    }
}
