<?php
require_once Mage::getModuleDir('controllers', 'Dyna_Customer')
    . DS
    . '/SearchController.php';

/**
 * Class Vznl_Customer_SearchController
 */
class Vznl_Customer_SearchController extends Dyna_Customer_SearchController
{
    const MAX_SEARCH_RESULTS = 10;

    /**
     * @var array
     */
    private $acceptedFields = array(
        'customer_ctn',
        'company_coc',
        'pin',
        'customer_type',
        'billing_postcode',
        'billing_street',
        'company_name',
        'ban',
        'firstname',
        'middlename',
        'lastname',
        'email',
        'dob',
        'entity_id',
        'order_number'
    );

    /**
     * @var array
     */
    private $_attributeIds = array();

    /**
     * @var int|null
     */
    private $_limit = null;

    /**
     * @var bool
     */
    protected $exactSearch = false;

    /**
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function getCustomerResultsAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $data = $this->getRequest()->getPost();

        // Set search mode to advanced search when applicable
        if (isset($data['customer_type'])) {
            $this->exactSearch = true;
        }

        $this->searchCustomer();
    }

    /**
     * @return array
     */
    protected function searchCustomer()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $this->searchHelper = Mage::helper('vznl_customer/search');
                $this->allCustomers = [];
                $errorFields = [];

                // Parse and validate the filters
                if ($this->legacySearch) {
                    $response = $this->searchHelper
                        ->parseCustomerSearchLegacyParams($postData);
                } elseif ($this->exactSearch) {
                    $response = $this->searchHelper
                        ->parseCustomerSearchAdvancedParams($postData);
                } else {
                    $response = $this->searchHelper
                        ->parseCustomerSearchParams($postData);
                }

                // if there is a validation error or no parameters are given
                // => skip search and display the error
                if (!empty($response['error'])) {
                    $this->setJsonResponse($response);
                    return;
                }

                if ($this->legacySearch) {
                    $this->legacySearch = $response['params']['legacy_type'];
                }

                $response = $this->getDbAndServiceCustomers($response['params']);
            } catch (\Exception $e) {
                $response = [
                    "error" => true,
                    "message" => $e->getMessage()
                ];
            }
            return $this->setJsonResponse($response);
        }
    }

    /**
     * Get the backend limit for the customer search
     *
     * @return mixed
     */
    protected function _getLimit()
    {
        if ($this->_limit === null) {
            $storeConfig = (int)trim(Mage::getStoreConfig(self::LIMIT_PATH));
            $this->_limit = $storeConfig ?: self::MAX_SEARCH_RESULTS;
        }

        return $this->_limit;
    }

    private function _getAttributeId($handle, $typeId)
    {
        if (isset($this->_attributeIds[$typeId . '_' . $handle])) {
            return $this->_attributeIds[$typeId . '_' . $handle];
        }

        $key = serialize([__METHOD__, $handle, $typeId]);
        if ($attributeId = $this->getCache()->load($key)) {
            $this->_attributeIds[$typeId . '_' . $handle] = $attributeId;
            return $attributeId;
        } else {
            /** @var Mage_Eav_Model_Entity_Attribute $attribute */
            $attribute = Mage::getModel('eav/entity_attribute')->getCollection()
                ->addFieldToFilter('entity_type_id', $typeId)
                ->addFieldToFilter('attribute_code', $handle)
                ->addFieldToSelect('attribute_id')
                ->setPageSize(1, 1)
                ->getLastItem();

            $attributeId = $attribute->getAttributeId() ? : 0;
            $this->getCache()->save($attributeId,
                $key,
                [Vznl_Configurator_Model_Cache::CACHE_TAG],
                $this->getCache()->getTtl());
        }
        $this->_attributeIds[$typeId . '_' . $handle] = $attributeId;

        return $attributeId;
    }

    /**
     * @param $collection Omnius_Customer_Model_Customer_Resource_Customer_Collection
     * @param $fields
     */
    protected function _getHits(&$collection, $fields, $addGroupBy = true)
    {
        $addGroupBy = '';
        $select = $collection->getSelect();
        $select->joinLeft(['cae' => 'customer_address_entity'],
            'cae.parent_id = e.entity_id', '');
        foreach ($fields as $field => $value) {
            switch ($field) {
                case 'customer_ctn':
                    $select->joinLeft(['cc' => 'customer_ctn'],
                        'cc.customer_id = e.entity_id', "");
                    $select->where('cc.ctn = ?', $value);
                    break;
                case 'billing_postcode':
                    $select->joinLeft(['caev' => 'customer_address_entity_varchar'],
                        'caev.entity_id = cae.entity_id AND caev.attribute_id = '
                        . $this->_getAttributeId('postcode', 2), '');
                    $select->where('caev.value = "'
                        . $value[0] . '" OR caev.value = "' . $value[1] . '"');
                    break;
                case 'billing_street':
                    $select->joinLeft(['caev2' => 'customer_address_entity_text'],
                        'caev2.entity_id = cae.entity_id AND caev2.attribute_id = '
                        . $this->_getAttributeId('street', 2), '');
                    $select->where('caev2.value LIKE ?', $value);
                    break;
                case 'company_name':
                    $select->joinLeft(['cev1' => 'customer_entity_varchar'],
                        '(cev1.entity_id = e.entity_id) AND (cev1.attribute_id='
                        . $this->_getAttributeId('company_name', 1) . ')', "");
                    $select->where('cev1.value LIKE ?', $value . '%');
                    break;
                case 'ban':
                    $select->where('e.ban = ?', $value);
                    break;
                case 'firstname':
                    $select->joinLeft(['cev2' => 'customer_entity_varchar'],
                        '(cev2.entity_id = e.entity_id) AND (cev2.attribute_id='
                        . $this->_getAttributeId('firstname', 1) . ')', "");
                    $select->where('cev2.value LIKE ?', $value . '%');
                    break;
                case 'lastname':
                    $select->joinLeft(['cev3' => 'customer_entity_varchar'],
                        '(cev3.entity_id = e.entity_id) AND (cev3.attribute_id='
                        . $this->_getAttributeId('lastname', 1) . ')', "");
                    $select->where('cev3.value LIKE ?', $value . '%');
                    break;
                case 'email':
                    $select->joinLeft(['cev4' => 'customer_entity_varchar'],
                        '(cev4.entity_id = e.entity_id) AND (cev4.attribute_id='
                        . $this->_getAttributeId('additional_email', 1) . ')', "");
                    $select->where('cev4.value LIKE ?', $value . '%');
                    break;
                case 'dob':
                    $select->joinLeft(['ced' => 'customer_entity_datetime'],
                        '(ced.entity_id = e.entity_id) AND (ced.attribute_id='
                        . $this->_getAttributeId('dob', 1) . ')', "");
                    $select->where('ced.value = ?', $value);
                    break;
                case 'company_coc':
                    $select->joinLeft(['cev5' => 'customer_entity_varchar'],
                        '(cev5.entity_id = e.entity_id) AND (cev5.attribute_id='
                        . $this->_getAttributeId('company_coc', 1) . ')', "");
                    $select->where('cev5.value LIKE ?', $value . '%');
                    break;
                case 'order_number':
                    $select->joinLeft(['so' => 'superorder'],
                        'so.customer_id = e.entity_id', "");
                    $select->where('so.order_number = ?', $value);
                    break;
            }
        }
        $select->group('e.entity_id');
        if ($this->_getLimit()) {
            $select->limit($this->_getLimit() + 1);
        }
    }

    /**
     * Customer search
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function indexAction()
    {
        $this->_readConnection = Mage::getSingleton('core/resource')
            ->getConnection('core_read');
        $result = array();
        $errorFields = array();
        $requestParameters = $this->getRequest()->getParams();

        $dataToCollect = array(
            'entity_id',
            'is_prospect',
            'customer_type',
            'additional_email',
            'firstname',
            'lastname',
            'middlename',
            'customer_ctn',
            'ban',
            'company_coc',
            'billing_address',
            'billing_street',
            'billing_city',
            'billing_postcode',
            'company_name',
            'is_business',
            'gender',
            'customer_label',
            'prefix'
        );

        //remove empty parameters
        $flippedAcceptedFields = array_flip($this->acceptedFields);
        foreach ($requestParameters as $key => $value) {
            if (!isset($flippedAcceptedFields) || !trim($value)) {
                unset($requestParameters[$key]);
            }
        }

        /** @var Vznl_Validators_Helper_Data $validator */
        $validator = Mage::helper('vznl_validators');
        foreach ($requestParameters as $key => $value) {
            if (!empty($value) && trim($value)) {
                switch ($key) {
                    case 'dob':
                        if (!$validator->validateIsNLDate($value)) {
                            unset($requestParameters['dob']);
                            $errorFields[] = $this->__("Invalid date of birth");
                        }
                        break;
                    case 'email' :
                        if (!$validator->validateEmailSyntax($value)) {
                            unset($requestParameters['email']);
                            $errorFields[] = $this->__("Invalid e-mail address");
                        }
                        break;
                    case 'customer_ctn' :
                        if (!$validator->validateIsNumber($value)) {
                            unset($requestParameters['customer_ctn']);
                            $errorFields[] = $this->__("Invalid mobile number");
                        }
                        break;
                    case 'ban' :
                        if (!$validator->validateIsNumber($value)) {
                            unset($requestParameters['ban']);
                            $errorFields[] = $this->__("Invalid ban number");
                        }
                        break;
                    case 'order_number' :
                        if (!$validator->validateIsNumber($value)) {
                            unset($requestParameters['order_number']);
                            $errorFields[] = $this->__("Invalid order number");
                        }
                        break;
                }
            }
        }

        $parameters = new Varien_Object($requestParameters);
        if (empty($errorFields)
            && (count($parameters->getData())
                || $this->getRequest()->getPost('quote_number'))) {
            /** @var Vznl_Customer_Helper_Data $customerHelper */
            $customerHelper = Mage::helper('vznl_customer');
            //filter through each provided parameters
            $searchFields = [];
            foreach ($this->acceptedFields as $field) {
                if ($value = $parameters->getData($field)) {
                    switch ($field) {
                        case 'customer_ctn':
                            $value = $customerHelper->parseCtn($value);
                            break;
                        case 'billing_postcode':
                            $value = $customerHelper->formatPostCode($value);
                            break;
                        case 'billing_street':
                            $value = $customerHelper->formatHouseNumber($value);
                            break;
                        case 'dob':
                            $date = new DateTime($value);
                            $value = $date->format('Y-m-d H:i:s');
                            break;
                    }
                    $searchFields[$field] = $value;
                }
            }

            //loop through the customer data and
            // filter if parameters is in the allowed list
            if ($parameters->hasData('customer_type')) {
                $this->indirectAction();
                return;
            }

            if (count($parameters->getData())) {
                /** @var Omnius_Customer_Model_Customer_Resource_Customer_Collection
                 * $customerCollection */
                $customerCollection = Mage::getResourceSingleton(
                    'customer/customer_collection')
                    ->addAttributeToSelect($dataToCollect)
                    ->joinAttribute('is_prospect_3',
                        'customer/is_prospect',
                        'is_prospect',
                        null,
                        'left')
                    ->joinAttribute('billing_street',
                        'customer_address/street',
                        'default_billing',
                        null,
                        'left')
                    ->joinAttribute('billing_postcode',
                        'customer_address/postcode',
                        'default_billing',
                        null,
                        'left')
                    ->joinAttribute('billing_city',
                        'customer_address/city',
                        'default_billing',
                        null,
                        'left');
                $this->_getHits($customerCollection, $searchFields);

                $result = $customerCollection
                    ->load()
                    ->toArray($dataToCollect);
            }

            //serialize result or show No results found
            $result = $result ? array_values($result)
                : array('message' => $this->__('No results found'));

            foreach ($result as &$record) {
                if (is_array($record)) {
                    $record['prefix'] = (isset($record['prefix'])
                        && $record['prefix'])
                        ? $this->__($record['prefix'])
                        : '';
                    $record['customer_label'] = (isset($record['customer_label'])
                        && $record['customer_label'])
                        ? $record['customer_label']
                        : '';
                    $record['is_prospect'] = (isset($record['is_prospect'])
                        && $record['is_prospect'])
                        ? $record['additional_email']
                        : '0';
                    if (isset($record['additional_email'])) {
                        unset($record['additional_email']);
                    }
                }
            }

            // If the limit was breached, show warning
            if (count($result) === $this->_getLimit() + 1) {
                array_pop($result);
                $result[] = ['limit_reached' => sprintf($this
                    ->__('Please note: only the first %s results are displayed.
                Please use additional or different search criteria
                 when the desired customer is not displayed.'),
                    $this->_getLimit())];
            }
        } else {
            //return No search filter provided
            // and the array with error inputted fields
            $result = array(
                'message' => $this->__('No search filter provided'),
                'error_fields' => $errorFields,
            );
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($result));

        return true;
    }

    /**
     * Search customer for the Indirect store.
     * IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function indirectAction()
    {
        $requestParameters = $this->getRequest()->getParams();
        $orderFound = true;

        $dataToCollect = array('customer_ctn',
            'dob',
            'ban',
            'company_coc',
            'pin',
            'order_number');

        $flippedDataToCollect = array_flip($dataToCollect);
        //remove empty parameters
        foreach ($requestParameters as $key => $value) {
            if ($value != '0'
                && (!isset($flippedDataToCollect[$key])
                    || !trim($value))) {
                unset($requestParameters[$key]);
            }
        }

        if (
        !(
            (array_key_exists('customer_ctn',
                    $requestParameters)
                && array_key_exists('dob',
                    $requestParameters))
            || (array_key_exists('customer_ctn',
                    $requestParameters)
                && array_key_exists('company_coc',
                    $requestParameters))
            || (array_key_exists('company_coc',
                    $requestParameters)
                && array_key_exists('ban',
                    $requestParameters))
            || (array_key_exists('order_number',
                $requestParameters))
        )
        ) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => true,
                            'message' => $this->__('Customers can search sole basis of CTN 
                            + Date of birth, CTN 
                            + CoC number, BAN 
                            + CoC number'),
                        )
                    )
                );
            return;
        }

        /** Validate customer CTN */
        if (isset($requestParameters['customer_ctn'])) {
            $ctnToValidate = trim($requestParameters['customer_ctn']);
            if (!$this->checkCtn($ctnToValidate)) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(
                            array(
                                'error' => true,
                                'message' => $this->__('The CTN format is invalid.'),
                            )
                        )
                    );
                return;
            }
        }

        /** Validate customer BAN */
        if (isset($requestParameters['ban'])) {
            $banToValidate = trim($requestParameters['ban']);
            if (!is_numeric($banToValidate)) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(
                            array(
                                'error' => true,
                                'message' => $this->__('The BAN format is invalid.'),
                            )
                        )
                    );
                return;
            }
        }

        if (isset($requestParameters['order_number'])) {
            $orderNumber = trim($requestParameters['order_number']);
            unset($requestParameters['order_number']);
        }

        if (isset($orderNumber) && $orderNumber) {
            $dealerId = Mage::getSingleton('customer/session')
                ->getAgent(true)->getDealer()->getId();
            try{
                $superOrder = Mage::getModel('superorder/superorder')
                    ->load($orderNumber, 'order_number');
                if ($superOrder->getCreatedDealerId() != $dealerId) {
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(
                            Mage::helper('core')->jsonEncode(
                                array(
                                    'error' => true,
                                    'message' => $this
                                        ->__('Order created by different dealer,
                                         no authorisation to view order.'),
                                )
                            )
                        );
                    return;
                }
                $orderFound = true;
                $requestParameters['entity_id'] = $superOrder->getCustomerId();
            } catch (Exception $ex){
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(
                            array(
                                'error' => true,
                                'message' => $this->__('Order not found'),
                            )
                        )
                    );
                return;
            }
        }

        if (count($requestParameters)) {
            try {
                $requestParameters['advanced_search'] = true;
                try {
                    $customer = $this->fetchCustomer($requestParameters);
                } catch (Exception $e) {
                    // When logging the customer on indirect
                    // if the search was done by order number, let the customer login
                    if (isset($orderNumber) && $orderNumber && $orderFound) {
                        $customer = Mage::getModel('customer/customer')
                            ->load($superOrder->getCustomerId());
                    } else {
                        throw $e;
                    }
                }
                $returnArray = $this->logCustomer($customer);
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($returnArray));
            } catch (Exception $e) {
                Mage::helper('dyna_service')->returnServiceError($e);
                return;
            }
        } else {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => true,
                            'message' => Mage::helper('dyna_customer')
                                ->__(!$orderFound
                                ? 'Order not found'
                                : 'Please fill the form fields.'),
                        )
                    )
                );
            return;
        }
    }

    /**
     * @param array $requestParameters
     * @return Mage_Core_Model_Abstract|Mage_Customer_Model_Customer|mixed
     * @throws Exception
     */
    protected function fetchCustomer(array $requestParameters)
    {
        // Validate date format
        if (!empty($requestParameters['dob'])
            && !Mage::helper('dyna_validators')
                ->validateIsNLDate($requestParameters['dob'])) {
            throw new Exception($this->__('The date is of an invalid format.'));
        }

        if (isset($requestParameters['company_coc'])
            && (trim($requestParameters['company_coc'])
                || $requestParameters['company_coc'] === '0')) {
            $requestParameters['is_business'] = true;
        }

        return Mage::helper('vznl_customer')->fetchCustomer($requestParameters);
    }

    /**
     * Sets up the current customer session
     * @param $customer
     * @param $customerId
     * @param $currentQuote
     * @return array
     */
    private function logCustomer($customer)
    {
        $customerId = $customer->getId();
        $currentQuote = Mage::getSingleton('checkout/cart')->getQuote();
        //if customer exists, load the entity to retrieve the campaigns,
        // ctn records and other data
        $session = Mage::getSingleton('customer/session');
        $session->setCustomer($customer);
        $session->setCustomerInfoSync(true);

        /*
         * and if customer was already logged out when loading a new customer,
         *  this quote will be lost,
         * otherwise, if this quote was not associated to any customer, it will
         *  be associated to this customer that is about to be loaded
         */
        if ($currentQuote->getCustomerId()) {
            Mage::helper('dyna_checkout')->createNewQuote();
        } else {
            $currentQuote->setCustomerId($customerId)->save();
            Mage::getSingleton('checkout/cart')->setQuote($currentQuote);
            Mage::getSingleton('checkout/session')
                ->setQuoteId($currentQuote->getId());
        }

        $defaultBillingAddress = Mage::getModel('customer/address')
            ->load($customer->getData('default_billing'));

        //saving customer product & order counter value in session
        $customer['products_count'] = count($customer->getdata('ctn'));
        $customer['orders_count'] = Mage::helper("vznl_customer")
            ->getCustomerOrderCount();
        $customer['carts_count'] = count($customer->getShoppingCarts(true));
        $session->setCustomer($customer);

        // update current quote with customer data
        $currentQuote->setCustomer($customer);
        $currentQuote->save();

        return [
            'sticker' => $this->loadLayout('page_three_columns')
                ->getLayout()
                ->getBlock('customer.details')
                ->toHtml(),
            'saveCartModal' => $this->getLayout()
                ->createBlock('core/template')
                ->setTemplate('page/html/save_cart_modal.phtml')->toHtml(),
            'customerData' => $customer->getCustomData(),
            'addressData' => $defaultBillingAddress->getData()
        ];
    }

    /**
     * Checks CTN validity
     * @param $value
     * @return bool
     */
    private function checkCtn($value)
    {
        $valid = false;
        if (strpos($value, '097') == 0
            || strpos($value, '06') === 0
            || strpos($value, '0031') === 0
            || substr($value, 0, 2) == '31'
            || strpos($value, '+31') === 0) {
            $valid = true;
        }

        return strpos($value, ' ') === false && $valid;
    }
}
