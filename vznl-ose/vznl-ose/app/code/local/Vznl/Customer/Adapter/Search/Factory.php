<?php

class Vznl_Customer_Adapter_Search_Factory implements Vznl_Customer_Adapter_FactoryInterface
{
    public static function create()
    {
        return new Vznl_Customer_Adapter_Search_Adapter();
    }
}
