<?php

class Vznl_Customer_Adapter_Search_Adapter implements Vznl_Customer_Adapter_AdapterInterface
{

    const STATUS_REASON_CODE = "OK";
    const PHONE_PRIVATE = "PRIVATE";
    const ADDRESS_TYPE_C = "C";
    const ADDRESS_TYPE_LO = "LO";
    const ADDRESS_TYPE_DL = "DL";
    const ADDRESS_TYPE_LEGAL = "L"; //@todo clarify what type of adress is this
    const ADDRESS_TYPE_USER = "U"; //@todo clarify what type of adress is this
    const ADDRESS_TYPE_BILLING = "B"; //@todo clarify what type of adress is this
    const ADDRESS_TYPE_SERVICE = "S";
    const ADDRESS_TYPE_SHIPPING = "SH";
    const ALL = 'ALL';
    const PARTY_TYPE_PRIVATE = "PRIVATE";
    const PARTY_TYPE_SOHO = "SOHO"; //@todo clarify what address type return the services for customer shipping address
    const PARTY_TYPE_SHORT_PRIVATE = "P";
    const PARTY_TYPE_SHORT_SOHO = "S";
    const CUSTOMER_SALUTATION_MR = "R";
    const CUSTOMER_SALUTATION_MRS = "S";
    const CUSTOMER_SALUTATION_NONE = "N";
    const CUSTOMER_SALUTATION_FIRMA = "F";
    const NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE = "Mobile";
    const NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE_POSTPAID = "Mobile_Postpaid";
    const NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE_PREPAID = "Mobile_Prepaid";
    const NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE = "Cable";
    const NON_OSF_ORDER_LINE_OF_BUSINESS_DSL = "DSL";
    const NON_OSF_ORDER_TYPE_BAN = "BAN";
    const NON_OSF_ORDER_TYPE_SUBSCRIBER = "SUBSCRIBER";
    const NON_OSF_ORDER_ACTIVITY_SUSPENDED = "SUS";
    const NON_OSF_ORDER_ACTIVITY_CANCELLATION = "CAN";
    const NON_OSF_ORDER_ACTIVITY_RESTORE_FROM_SUSPENDED = "RSP";
    const KD_DEVICE_ENABLED_FEATURES = "Enabled features";
    const KD_DEVICE_SUPPORTED_FEATURES = "Supported features";
    const CUSTOMER_SEARCH_STACK_PATH = 'omnius_service/customer_search_implementation_configuration/customer_search_stack';
    const VODAFONE_CUSTOMER_STACK = 'BSL';
    const ZIGGO_CUSTOMER_STACK = 'PEAL';
    const VODAFONE_PARTY_TYPE_PRIVATE = 'I';
    const ZIGGO_PARTY_TYPE_PRIVATE = 'RESIDENTIAL';
    const VODAFONE_PARTY_TYPE_BUSINESS = 'B';
    const ZIGGO_PARTY_TYPE_BUSINESS = 'SoHo';
    const VODAFONE_PARTY_TYPE_BUSINESS_CORPORATE = 'C';

    protected $customersPriority = [
        Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL,
        Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL,
        Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_KD,
        Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_FN,
        Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS,
    ];
    protected $data = [];
    public $accountCategory = null;
    public $sharingGroupsDetails = [];
    public $customerDevices = [];
    protected static $blockPartyType = [
        'GGZ',
        'GGZ external',
        'Onderwijs',
        'GT Fixed',
        'B2B External'
    ];

    /** @var Vznl_Customer_Model_Client_RetrieveCustomerInfoClient */
    protected static $client;

    /**
     * Get RetrieveCustomerInfo client
     * @return Vznl_Customer_Model_Client_RetrieveCustomerInfoClient
     */
    protected static function getRetrieveCustomerInfoClient()
    {
        if ( ! static::$client) {
            static::$client = Mage::helper('vznl_service')->getClient('retrieve_customer_info');
        }

        return static::$client;
    }

    /**
     * Get the customer ID specific to either Vodafone or Ziggo stack
     * as per the customer search configuration in backoffice
     *
     * @param $customerData
     * @param $customerSearchStack
     * @return $customerId
     */
  public function getCustomerSearchStackSpecificCustomer($customerData, $customerSearchStack = null)
  {
      if (!$customerSearchStack) {
          $customerSearchStack = Mage::getStoreConfig(self::CUSTOMER_SEARCH_STACK_PATH) ?? self::VODAFONE_CUSTOMER_STACK;
      }

      if (isset($customerData[$customerSearchStack])) {
          $result = array_shift($customerData[$customerSearchStack]);
      } else {
          $tempResult = array_shift($customerData);
          $result = array_shift($tempResult);
      }

      $result['peal_party_subtype'] = false;
      if (isset($customerData[self::ZIGGO_CUSTOMER_STACK])) {
          $pealCustomerData = array_shift($customerData[self::ZIGGO_CUSTOMER_STACK]);
          if (in_array($pealCustomerData['party_subtype'], static::$blockPartyType)) {
              $result['peal_party_subtype'] = true;
          }
          $result['no_ordering_allowed'] = $pealCustomerData['no_ordering_allowed'];
      }

      return $result;
  }

    /**
     * Process function
     * @param Vznl_Customer_Model_Customer $customer
     * @return Vznl_Customer_Model_Customer
     */
	public function getProfile(Vznl_Customer_Model_Customer $customer)
	{
	    try {
            $params["ban"] = $customer->getData('ban');
            $params["pan"] = $customer->getData('pan');
            $params["dob"] = $customer->getData('dob');
            $params["kvk"] = $customer->getData('kvk');
            $params["phone"] = $customer->getData('phone');
            $params["customer_password"] = $customer->getData('search_customer_password');

            $data = $this->getRetrieveCustomerInfoClient()->executeRetrieveCustomerInfo($params, 1);

            if (!isset($data['Customer'])) {
                $message = null;
                foreach ($data['Status'] as $status) {
                    if ($status['StatusReasonCode'] != 'ERR-0000') {
                        $message = $status['StatusReason'];
                        break;
                    }
                }
                if(!$message) {
                    $message="No customer data received after executing RetrieveCustomerInfo service call on mode 1";
                }
                return new Exception($message);
            } else {
                // Map stub file data to customer model
                return $this->mapCustomer($data, $customer, 1);
            }
        } catch (Exception $e) {
	        Mage::logException($e);
            return $e;
        }
    }

    /**
     * Process function
     * @param Dyna_Customer_Model_Customer $customer
     * @return Dyna_Customer_Model_Customer
     */
    public function getInstalledBase(Dyna_Customer_Model_Customer $customer)
    {
        try {
            $params["ban"] = $customer->getData('ban');
            $params["pan"] = $customer->getData('pan');
            $params["dob"] = $customer->getData('dob');
            $params["kvk"] = $customer->getData('kvk');
            $params["phone"] = $customer->getData('phone');
            $params["customer_password"] = $customer->getData('pin');

            $data = $this->getRetrieveCustomerInfoClient()->executeRetrieveCustomerInfo($params, 2);
            $this->mapCustomer($data, $customer, 2);
            return $data;
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

     /**
     * get stub for mode one
     * @return array
     */
    public function getStubOfModeOne() : array
    {
        $path = Mage::getBaseDir('var'). DS . 'stubs' . DS . 'default' . DS . 'Vznl' . DS . 'Customer' . DS . 'RetrieveCustomerInfo';
        $stubData = json_decode(Mage::helper("vznl_data/data")->getFileContents($path. DS . 'mode1.json'), true);

        return $stubData;
    }

    /**
     * get stub for mode one
     * @return array
     */
    public function getStubOfModeTwo() : array
    {
        $path = Mage::getBaseDir('var'). DS . 'stubs' . DS . 'default' . DS . 'Vznl' . DS . 'Customer' . DS . 'RetrieveCustomerInfo';
        $stubData = json_decode(Mage::helper("vznl_data/data")->getFileContents($path. DS . 'mode2.json'), true);

        return $stubData;
    }

    /**
     * mapCustomer function
     * @param array $data
     * @param Vznl_Customer_Model_Customer $customerObject
     * @return Vznl_Customer_Model_Customer
     */
    public function mapCustomer(array $data, $customerObject, $mode = 1)
    {
        $this->data = $this->mapMode1($data);
        $this->setSessionForServiceCustomers();

        if ($mode == 2) {
            $this->data = $this->mapMode2($data);
            $this->setSessionForServiceCustomers();
        }

        $this->setSessionForServiceCustomers();

        /** @var Vznl_Customer_Model_Customer $customerObject */
        $currentStackCustomer = $this->getCustomerSearchStackSpecificCustomer($this->data);
        $customerObject->addData($currentStackCustomer);

        if ($mode == 1){
            return $currentStackCustomer;
        }
        return $customerObject;
    }

    /**
     * mapData function
     * @param $data
     * @return array
     */
    public function mapMode1(array $data)
    {
        // In case only one result is returned, change it to an array with one item
        if (empty($data['Customer'][0])) {
            $data['Customer'] = [$data['Customer']];
        }

        $serviceCustomers = $data['Customer'];

        $customers = [];
        $bans = $contractTypes = array();

        foreach ($serviceCustomers as $customerData) {
            $customerData['AccountCategory'] = isset($customerData['AccountCategory']) ? $customerData['AccountCategory'] : 'KIAS';
            $this->accountCategory = $customerData['AccountCategory'];
            $customers[$customerData['AccountCategory']][$customerData['PartyIdentification']['ID']] = array_merge(
                $this->mapCustomerData($customerData, $bans, $contractTypes),
                $this->mapBillingData($customerData),
                $this->mapCustomerAddressData($customerData),
                $this->mapContactData($customerData)
            );
        }

        return $customers;
    }


    /**
     * mapData function
     * @param $data
     * @return array
     */
    public function mapMode2(array $data)
    {
        $dot = $this->getAccessor();
        // In case only one result is returned, change it to an array with one item
        if (empty($data['Customer'][0])) {
            $data['Customer'] = [$data['Customer']];
        }
        /** get customers from service */
        $serviceCustomers = $data['Customer'];
        /** get saved customers from session */
        $sessionCustomers =  $this->getSessionForServiceCustomers() ?? [];

        /** parce service customers */
        foreach ($serviceCustomers as $customerData) {
            $customerData['AccountCategory'] = isset($customerData['AccountCategory']) ? $customerData['AccountCategory'] : 'KD';
            $this->accountCategory = $customerData['AccountCategory'];
            if (isset($customerData['SharingGroupDetails'])) {
                if (isset($customerData['SharingGroupDetails'][0]) && !is_array($customerData['SharingGroupDetails'][0])) {
                    $sharingGroupDetails = $customerData['SharingGroupDetails'];
                    unset($customerData['SharingGroupDetails']);
                    $customerData['SharingGroupDetails'][0] = $sharingGroupDetails;
                }

                $this->sharingGroupsDetails = $customerData['SharingGroupDetails'];
            }
            /**********************************************************
             * MAPPING CUSTOMER DEVICE
             *********************************************************/
            $devices = $dot->getValue($customerData, "Device");
            if ($devices) {
                $this->mapCustomerDevices($devices);
            }
            /**********************************************************
             * EOF MAPPING CUSTOMER DEVICE
             *********************************************************/
            if (isset($sessionCustomers[$customerData['AccountCategory']])) {
                if (empty($sessionCustomers[$customerData['AccountCategory']][$customerData['PartyIdentification']['ID']])) {
                    $sessionCustomers[$customerData['AccountCategory']][$customerData['PartyIdentification']['ID']] = array_merge(
                        $this->mapCustomerOrdersData($customerData),
                        $this->mapContracts($customerData, $customerData['AccountCategory'])
                    );
                } else {
                    $sessionCustomers[$customerData['AccountCategory']][$customerData['PartyIdentification']['ID']] += array_merge(
                        $this->mapCustomerOrdersData($customerData),
                        $this->mapContracts($customerData, $customerData['AccountCategory'])
                    );
                }
            }


            if ($customerData['AccountCategory'] == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL) {
                $this->mapContractType($sessionCustomers[$customerData['AccountCategory']][$customerData['PartyIdentification']['ID']]);
            }

        }

        return $sessionCustomers;
    }

    public function mapCustomerDevices($deviceData)
    {
        $dot = $this->getAccessor();
        $deviceDeliveryDate = false;
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');

        //Expecting an array of contracts
        if (!isset($deviceData[0])) {
            $deviceData = [$deviceData];
        }
        $customerDevices = [];
        foreach ($deviceData as $device) {
            $deviceType = $dot->getValue($device, 'CommodityClassification.NatureCode');
            $deviceName = null;
            $sku = null;
            $maf = false;
            $deviceCode = $dot->getValue($device, 'StandardItemIdentification.ID');
            $subventionCode = '';
            if (($components = $dot->getValue($device, 'Component'))
                && ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN)
            ) {
                // Get first component as there should not be multiple Component nodes for one Device
                if (isset($components[0])) {
                    $components = $components[0];
                }
                foreach ($components['ComponentAdditionalData'] as $additional) {
                    if ($additional['Code'] === 'articleName' && ($additional['AdditionalValue'] === '/functions/hardware/rentedHardwareConfiguration/articleName/existing'
                            || $additional['AdditionalValue'] === '/functions/hardware/hardwareConfiguration/articleName/existing')) {
                        $deviceName = $additional['Value'];
                    } elseif ($additional['Code'] === 'articleNumber' && ($additional['AdditionalValue'] === '/functions/hardware/rentedHardwareConfiguration/articleNumber/existing'
                            || $additional['AdditionalValue'] === '/functions/hardware/hardwareConfiguration/articleNumber/existing')) {
                        $deviceCode = $additional['Value'];
                    } elseif ($additional['Code'] === 'subventionCode' && ($additional['AdditionalValue'] === '/functions/hardware/rentedHardwareConfiguration/subventionCode/existing'
                            || $additional['AdditionalValue'] === '/functions/hardware/hardwareConfiguration/subventionCode/existing')) {
                        $subventionCode = $additional['Value'];
                    }

                    if (!empty($additional['Code']) && $additional['Code'] === 'deliveryDate') {
                        $deviceDeliveryDate = $additional['Value'];
                    }

                    if ($additional['Code'] === 'hardwareType' && $additional['AdditionalValue'] === '/functions/hardware/rentedHardwareConfiguration/hardwareType/existing') {
                        $deviceType = $additional['Value'];
                    }
                }
                $articleNumberAddons = $this->getArticleNumberAddons($deviceCode);
                $articleNumberMapperIds = [];
                foreach ($articleNumberAddons as $articleNumberAddon) {
                    $articleNumberMapperIds[] = $articleNumberAddon->getMapperId();
                }

                $subventionCodeAddons = $this->getsubventionCodeAddons($subventionCode);
                $subventionCodeMapperIds = [];
                foreach ($subventionCodeAddons as $subventionCodeAddon) {
                    $subventionCodeMapperIds[] = $subventionCodeAddon->getMapperId();
                }

                $mapperIds = array_intersect($articleNumberMapperIds, $subventionCodeMapperIds);
                if (empty($mapperIds)) {
                    continue;
                } else {
                    $mapperID = $mapperIds[0];
                }

                /** @var Dyna_MultiMapper_Helper_Data $h */
                $mapper = Mage::getModel('dyna_multimapper/mapper')->load($mapperID);
                if($mapper){
                    $sku = $mapper->getSku();
                    if($sku) {
                        /** @var Dyna_Catalog_Model_Product $product */
                        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                        if($product && $product->getId()) {
                            $maf = $helper->showNewFormattedPriced($product->getMaf(), true);
                            $deviceName = $product->getDisplayNameInventory() ?: $product->getName();
                            $productVisibility = Mage::helper('dyna_catalog')->getVisibility(Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY, $sku, $product);
                        }
                    }
                }

            } else {
                $deviceCode = $dot->getValue($device, 'StandardItemIdentification.ID');
            }

            $featureListType = $dot->getValue($device, 'FeatureListType');
            if ($featureListType && !isset($featureListType[0])) {
                $featureListType = [$featureListType];
            }

            if ($featureListType) {
                foreach ($featureListType as $feature) {
                    if ($feature['FeatureType'] == self::KD_DEVICE_SUPPORTED_FEATURES) {
                        $supportedFeatures = $feature['FeatureList'];
                        break;
                    }
                }

                foreach ($supportedFeatures as $supportedFeature) {
                    if ($supportedFeature['FeatureName'] === 'wlan') {
                        $wlanEnabled = $supportedFeature['FeatureValue'];
                    }
                    if ($supportedFeature['FeatureName'] === 'maxBandwidth') {
                        $maxBandwidth = $supportedFeature['FeatureValue'];
                    }
                }
            }
            $customerDevices[] = [
                'device_name' => $deviceName ?? $dot->getValue($device, 'Model'),
                'device_type' => $deviceType,
                'device_code' => $deviceCode,
                'device_delivery_date' => $deviceDeliveryDate,
                'customer_id' => $dot->getValue($device, 'BackEndCustomerID'),
                'service_line_id' => $dot->getValue($device, 'ServiceLineID'),
                'sku' => $sku,
                'maf' => $maf,
                'manufacturer' =>  $dot->getValue($device, 'Manufacturer'),
                'mac_address' =>  $dot->getValue($device, 'MacAddress'),
                'wlan_enabled' =>  $wlanEnabled ?? null,
                'max_bandwidth' =>  $maxBandwidth ?? null,
                'equipment_type' => $dot->getValue($device, 'EquipmentType'),
                'serial_number' => $dot->getValue($device, 'SerialNumber'),
                'ownership' => $dot->getValue($device, 'Ownership'),
                'feature_list_type' => $featureListType,
                'product_visibility' => $productVisibility ?? null,
            ];
        }
        $this->customerDevices = $customerDevices;
    }

    /**
     * Map contract type (from mode1) to mode2 for KIAS
     * @param $customerData
     * @return mixed
     */
    protected function mapContractType(&$customerData)
    {
        $bans = $customerData['bans'] ?? array();
        if (isset($customerData['contracts']) && !empty($customerData['contracts'])) {
            foreach ($customerData['contracts'] as &$contract) {
                foreach ($contract['subscriptions'] as &$subscription) {
                    foreach ($bans as $banKey => $ban) {
                        if ($subscription['id'] == $ban) {
                            $subscription['contract_type'] = $customerData['contract_types'][$banKey];
                            $contract['contract_type'] = $customerData['contract_types'][$banKey];
                        }
                    }
                }
            }
        }
    }

    /**
     * Map customer orders for mobile/cable/dsl from mode2
     *
     * @param $customerData
     * @return array
     */
    public function mapCustomerOrdersData($customerData)
    {
        $dot = $this->getAccessor();
        $data = [];
        if (array_key_exists("PendingOrder", $customerData)) {
            $pendingOrders = $customerData['PendingOrder'];

            // if is a single result make it an array with 1 element
            if ($dot->getValue($pendingOrders, 'LineOfBusiness')) {
                $pendingOrders = [0 => $pendingOrders];
            }

            foreach ($pendingOrders as $order) {
                switch (strtolower($dot->getValue($order, 'LineOfBusiness'))) {
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE):
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE_POSTPAID);
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE_PREPAID):
                        $data['orders'][strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE)][] = [
                            'created_date' => date("d.m.Y", strtotime($dot->getValue($order, 'CreationDate'))),
                            'planned_date' => date("d.m.Y", strtotime($dot->getValue($order, 'ExecutionPlannedDate'))),
                            'order_id' => $dot->getValue($order, 'OrderID'),
                            'type_request_level' => $dot->getValue($order, 'Detail.FutureRequestLevel'),
                            'subscriber_number' => $dot->getValue($order, 'Detail.SubscriberNumber'),
                            'activity_code' => $dot->getValue($order, 'OrderActivity'),
                            'activity_param' => $dot->getValue($order, 'Detail.FutureRequestAdditionalParam'),
                            'future_activity_code' => $dot->getValue($order, 'Detail.FutureRequestActivityCode'),
                            'future_reason_code' => $dot->getValue($order, 'Detail.FutureRequestReasonCode'),
                            'line_of_business' => $dot->getValue($order, 'LineOfBusiness'),
                            'legacy_customer_id' => $dot->getValue($order, 'LegacyCustomerID'),
                        ];
                        break;
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE):
                        $data['orders'][strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE)][] = [
                            'created_date' => date("d.m.Y", strtotime($dot->getValue($order, 'CreationDate'))),
                            'creator' => $dot->getValue($order, 'Creator'),
                            'order_id' => $dot->getValue($order, 'OrderID'),
                            'status' => $dot->getValue($order, 'Status'),
                            'kip_category' => $dot->getValue($order, 'Detail.KipCategory'),
                            'tv_category' => $dot->getValue($order, 'Detail.TvCategory'),
                            'cls_category' => $dot->getValue($order, 'Detail.ClsCategory'),
                            'line_of_business' => $dot->getValue($order, 'LineOfBusiness'),
                            'legacy_customer_id' => $dot->getValue($order, 'LegacyCustomerID'),
                            'subscriber_number' => $dot->getValue($order, 'Detail.SubscriberNumber'),
                        ];
                        break;
                    case strtolower(strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_DSL)):
                        $data['orders'][strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_DSL)][] = [
                            'created_date' => date("d.m.Y", strtotime($dot->getValue($order, 'CreationDate'))),
                            'planned_date' => date("d.m.Y", strtotime($dot->getValue($order, 'ExecutionPlannedDate'))),
                            'order_id' => $dot->getValue($order, 'OrderID'),
                            'status' => $dot->getValue($order, 'Status'),
                            'order_activity' => $dot->getValue($order, 'OrderActivity'),
                            'line_of_business' => $dot->getValue($order, 'LineOfBusiness'),
                            'subscriber_number' => $dot->getValue($order, 'Detail.SubscriberNumber'),
                            'legacy_customer_id' => $dot->getValue($order, 'LegacyCustomerID'),
                        ];
                        break;
                }
            }
        }

        return $data;
    }


    /**
     * Map contracts of customer
     * (mode1 is used to retrieve for KIAS the contractType per contract and later use it to make the request for mode2)
     * (mode2 is used to get all contracts an merge them with the ones from mode1)
     * @param $customerData
     * @param int $mode
     * @return array
     */
    public function mapContracts($customerData, $AccountCategory)
    {
        $dot = $this->getAccessor();

        /**********************************************************
         * PARSING CUSTOMER CONTRACTS
         *********************************************************/
        $customerContracts = [];
        if (isset($customerData['Contract'])
            && true === array_key_exists("ID", $customerData['Contract'])
        ) {
            $contractData = $dot->getValue($customerData, 'Contract');
        } elseif (isset($customerData['Contract']['Subscription'])
            && true === array_key_exists("ID", $customerData['Contract']['Subscription'])
        ) {
            $contractData = $dot->getValue($customerData['Contract'], 'Subscription');
        } else {
            $contractData = $dot->getValue($customerData, 'Contract');
        }

        //Expecting an array of contracts
        if (!isset($contractData[0])) {
            $contractData = [$contractData];
        }

        foreach ($contractData as $contract) {
            $contractInfo = $this->mapContract($contract, $AccountCategory);
            $customerContracts['contracts'][] = $contractInfo;
        }

        /**********************************************************
         * EOF PARSING CUSTOMER CONTRACTS
         *********************************************************/

        return $customerContracts;
    }

    /**
     * Map contract for customer
     * @param $contract
     * @param $mode
     * @return array
     */
    protected function mapContract($contract, $AccountCategory)
    {
        $dot = $this->getAccessor();

        $parsedContractData = [
            "contract_id" => $dot->getValue($contract, 'ID'),
            //Contract Type
            "contract_type" => $dot->getValue($contract, 'ContractType'),
            //Contract Retention Indicator
            "contract_retention_indicator" => $dot->getValue($contract, 'Retention.RetentionIndicator') ? $dot->getValue($contract, 'Retention.RetentionIndicator') : null,
            // Contract Start Date
            "contract_start_date" => $dot->getValue($contract, 'ValidityPeriod.StartDate') ? date_format(date_create($dot->getValue($contract, 'ValidityPeriod.StartDate')), 'Y-m-d') : null,
            // Contract End Date
            "contract_end_date" => $dot->getValue($contract, 'ValidityPeriod.EndDate') ? date_format(date_create($dot->getValue($contract, 'ValidityPeriod.EndDate')), 'Y-m-d') : null,
            // First Activation Date
            "contract_activation_date" => $dot->getValue($contract, 'ActivationDate') ? date_format(date_create($dot->getValue($contract, 'ActivationDate')), 'Y-m-d') : null,
            // Next Possible cancellation date
            "contract_possible_cancellation_date" => $dot->getValue($contract, 'PossibleCancellationDate') ? date_format(date_create($dot->getValue($contract, 'PossibleCancellationDate')), 'Y-m-d') : null,
            // Cancellation Notice Period
            "contract_cancellation_notice_period" => $dot->getValue($contract, 'CancellationNoticePeriod'),
            // Cancellation Notice Unit (D,W,M,Y)
            "contract_cancellation_notice_unit" => $dot->getValue($contract, 'CancellationNoticeUnit'),
            "contract_last_notification_date" => $dot->getValue($contract, 'LastNotificationDate'),
        ];

        // Adding subscription to current contract
        // @todo Example contains two subscription nodes - which still needs to be clarified as for a contract there should be only one subscription
        $subscriptions = $dot->getValue($contract, 'Subscription');
        $parsedContractData['subscriptions'] = [];

        if ($subscriptions) {
            if (!isset($subscriptions[0])) {
                $subscriptions = [$subscriptions];
            }
            foreach ($subscriptions as $subscription) {
                $this->subscriptionCtn = null;
                $parsedContractData['subscriptions'][] = $this->mapSubscription($subscription, $AccountCategory);
            }
        }

        // OMNVFDE-3013: Map FN Contract->Product nodes that contain the FN subscriptions
        if ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
            $products = $dot->getValue($contract, 'Product');

            if ($products) {
                if (!isset($products[0])) {
                    $products = [$products];
                }
                foreach ($products as $product) {
                    $parsedContractData['products'][] = $this->mapProduct($product);
                }
            }
        }

        return $parsedContractData;
    }


    protected function mapSubscription($subscription, $AccountCategory)
    {
        $dot = $this->getAccessor();
        if ($AccountCategory == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL) {
            $ProductName = $dot->getValue($subscription, 'Product.Name');
        } elseif ($AccountCategory == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL) {
            $ProductName = $dot->getValue($subscription, 'Product.ProductName');
        }

        if ($AccountCategory == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL) {
            $ProductName = $dot->getValue($subscription, 'Product.Name');
        } elseif ($AccountCategory == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL) {
            $ProductName = $dot->getValue($subscription, 'Product.ProductName');
        }

        $subscriptionData = [
            // Reference of the Customer ID in the backend.
            "id" => $dot->getValue($subscription, 'ID'),
            // Reference of the CTN
            // Subscriber Value (R,S,G,P,T,B)
            "value_indicator" => $dot->getValue($subscription, 'Valueindicator'),
            "puk" => $dot->getValue($subscription, 'PUK'),
            "product_name" => $ProductName,
            "contact" => $dot->getValue($subscription, 'Contact'),
            // Contract Start Date
            "contract_start_date" => $dot->getValue($subscription, 'ValidityPeriod.StartDate'),
            // Contract End Date
            "contract_end_date" => $dot->getValue($subscription, 'ValidityPeriod.EndDate'),
            "last_subsidy_date" => $dot->getValue($subscription, 'LastSubsidyDate'),
            "actual_month_of_contract" => $dot->getValue($subscription, 'ActualMonthOfContract'),
            "sim_number" => $dot->getValue($subscription, 'SimNumber'),
            "serial_number" => $dot->getValue($subscription, 'SerialNumber'),
            "in_minimum_duration" => (int)(strtolower($dot->getValue($subscription, 'InMinimumDurationPeriod')) === "y"),
        ];

        if (!empty($subscriptionData['contact']) && !array_filter($subscriptionData['contact'])) {
            unset($subscriptionData['contact']);
        }

        $products = $dot->getValue($subscription, 'Product');
        if ($products) {
            // Make sure that we are parsing an array of products
            if (!isset($products[0])) {
                $products = [$products];
            }
            $subscriptionProducts = array();

            foreach ($products as $product) {
                if ($mappedProduct = $this->mapProduct($product, $AccountCategory)) {
                    $subscriptionProducts[] = $mappedProduct;
                }

                $subscriptionData['standard_item_identification'] = $dot->getValue($product, 'StandardItemIdentification.ID');
                if ($AccountCategory == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL) {
                    $subscriptionData['product_offer_id'] = $dot->getValue($product, 'OfferId');
                    $subscriptionData['product_parent_bundle_offer_id'] = $dot->getValue($product, 'ParentBundleOfferId');
                   $subscriptionData['product_offer_type'] = $dot->getValue($product, 'Type');
                }
            }

            $subscriptionData['products'] = $subscriptionProducts;
        }

        $sharingGroup = $dot->getValue($subscription, 'SharingGroupSubscriberDetails');
        if ($sharingGroup) {
            $subscriptionData['sharing_group'] = $this->mapSharingGroup($sharingGroup);
        }

        // For Cable (KD) and Fixed (FN) er get the addresses from Customer -> Contract -> Subscription -> Address
        $subscriptionAddresses = $dot->getValue($subscription, 'Address');

        if (!isset($subscriptionAddresses[0])) {
            $subscriptionAddresses = [$subscriptionAddresses];
        }

        // Building addresses for current subscription
        if (isset($subscriptionAddresses) && count($subscriptionAddresses)) {
            // Parse each addresses
            foreach ($subscriptionAddresses as $addressArray) {
                $subscriptionData["addresses"][] = $this->mapAddress($addressArray);
            }

            if (!array_filter($subscriptionData["addresses"])) {
                unset($subscriptionData["addresses"]);
            }
        }
        // EOF building addresses
        // get the ctn: for cable & FN is the first TN that will be found on a Subscription/Product/Component/TN node (in mode 1 like all the other details);
        // for KIAS is the Subscription/Ctn node (in mode2 - like the majority of details)
        if (!$this->subscriptionCtn) {
            $this->subscriptionCtn = $dot->getValue($subscription, 'Ctn');
        }
        $subscriptionData['ctn'] = $this->subscriptionCtn;

        return $subscriptionData;
    }


    public function mapSharingGroup($sharingGroupDetails)
    {
        $dot = $this->getAccessor();

        // build sharing group data from the SharingGroupSubscriberDetails nodes
        $sharingGroupData = [
            'id' => $dot->getValue($sharingGroupDetails, 'GroupID'),
            'legacy_customer_id' => $dot->getValue($sharingGroupDetails, 'LegacyCustomerID'),
            'skeleton_contract_number' => $dot->getValue($sharingGroupDetails, 'SkeletonContractNumber'),
            'ban' => $dot->getValue($sharingGroupDetails, 'Ban'),
            'group_tariff_type' => $dot->getValue($sharingGroupDetails, 'GroupTariffType'),
            'member_type' => $dot->getValue($sharingGroupDetails, 'MemberType'),
            'tariff_option' => $dot->getValue($sharingGroupDetails, 'TariffOption'),
            'effective_date' => date('Y-m-d', strtotime($dot->getValue($sharingGroupDetails, 'EffectiveDate'))),
            'expiration_date' => date('Y-m-d', strtotime($dot->getValue($sharingGroupDetails, 'ExpirationDate')))
        ];

        // Get more info from Customer -> SharingGroupDetails saved before
        if ($this->sharingGroupsDetails) {
            foreach ($this->sharingGroupsDetails as $group) {
                if ($group['GroupID'] == $sharingGroupData['id']) {
                    $sharingGroupData['status'] = $group['GroupStatus'];
                    $sharingGroupData['group_type'] = $group['GroupType'];
                    $sharingGroupData['number_of_owners'] = (int) $group['NumberOfOwner'];
                    $sharingGroupData['number_of_members'] = (int) $group['NumberOfMember'];
                    $sharingGroupData['group_effective_date'] = date('Y-m-d', strtotime($group['EffectiveDate']));
                    $sharingGroupData['group_expiration_date'] = date('Y-m-d', strtotime($group['ExpirationDate']));

                    break;
                }
            }
        }

        return $sharingGroupData;
    }

    protected function mapProduct($product, $AccountCategory)
    {
        $dot = $this->getAccessor();
        $components = $dot->getValue($product, 'Component');
        $components = (isset($components[0])) ? $components : [$components];

        $subProducts = $dot->getValue($product, 'SubProduct');
        $subProducts = (isset($subProducts[0])) ? $subProducts : [$subProducts];

        $subProductsArray = array();
        if ($AccountCategory == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL) {
            $subProductsArray = array();
            $subProductsArray = $this->mapBslSubProduct($subProducts, $subProductsArray);
        } else {
            foreach ($subProducts as $subProduct) {
                if ($mappedSubProduct=$this->mapSubProduct($subProduct)) {
                    $subProductsArray[]=$mappedSubProduct;
                }
            }
        }

        // Parse contact data for every subscription as it is needed in checkout flow
        $contact = [];
        $rawContactData = $dot->getValue($product, 'Contact');
        isset($rawContactData[0]) ?: $rawContactData = [$rawContactData];
        foreach ($rawContactData as $contactData) {
            $contact[] = $this->mapPerson($dot->getValue($contactData, 'Role.Person'));
        }

        $productId = $dot->getValue($product, 'CatalogueItemIdentification.ID');
        $productOfferId = '';

        if ($AccountCategory == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL) {
            $productName = $dot->getValue($product, 'Name');
        } elseif ($AccountCategory == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL) {
            $productName = $dot->getValue($product, 'ProductName');
            $productOfferId = $dot->getValue($product, 'OfferId');
            $productParentBundleOfferId = $dot->getValue($product, 'ParentBundleOfferId');
            $productOfferType = $dot->getValue($product, 'Type');
        }

        $additionalItem = $dot->getValue($product, 'AdditionalItemIdentification');
        $pricingElement = [];
        $productData = [
            'product_id' => $productId,
            'additional_Item' => $additionalItem,
            'pricing_element' =>$pricingElement,
            'product_offer_id' => $productOfferId,
            'product_parent_bundle_offer_id' => $productParentBundleOfferId,
            'product_offer_type' => $productOfferType,
            'product_name' => $productName,
            'product_status' => $dot->getValue($product, 'Status.Text'),
            'components' => $components,
            'subproducts' => $subProductsArray,
            'contact' => $contact,
            'service_line_id' => $dot->getValue($product, 'CatalogueItemIdentification.ID')
        ];

        if (!array_filter($productData['contact'])) {
            unset($productData['contact']);
        }

        if ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD
            || $this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN
        ) {
            // get the ctn: for cable is the first TN that will be found on a Subscription/Product/Component/TN node (in mode 1 like all the other details);
            // for KIAS is the Subscription/Ctn node (in mode2 - like the majority of details)
            if (count($components)) {
                foreach ($components as $component) {
                    if (!empty($component['TN'])) {
                        if (is_array($component['TN']) && isset($component['TN'][0])) {
                            $component['TN'] = $component['TN'][0];
                        }
                        $this->subscriptionCtn = $component['TN'];
                        break;
                    }
                }
            }

            // map devices to products
            $deviceId = $dot->getValue($product, 'CatalogueItemIdentification.ID');
            $backEndCustomerId = $dot->getValue($product, 'BackEndCustomerID');

            $productData['devices'] = [];

            if ($deviceId && $this->customerDevices) {
                foreach ($this->customerDevices as $customerDevice) {
                    if ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                        /**
                         * according to the mapping and OMNVFDE-3013 we have to map:
                         * Device->ServiceLineID points to Component->ComponentAdditionalData->Value (with code == 'function@ID')
                         */
                        if (isset($components[0])) {
                            $components = $components[0];
                        }
                        foreach ($components as $component) {
                            foreach ($component as $additional) {
                                if (isset($additional['Code']) && $additional['Code'] == "function@ID") {
                                    if ($additional['Value'] == $customerDevice['service_line_id']) {
                                        $productData['devices'][] = $customerDevice;
                                        break(2);
                                    }
                                }
                            }
                        }
                    } else {
                        /**
                         * according to the mapping and OMNVFDE-1976 we have to map:
                         * "Device Element has 2 pointers!
                         * 1. BackendCustomerID points to Customer/ParentAccountNumber
                         * 2. ServiceLineID points to lc:retrievecustomerinforesponse>customer>Contract>subscription>product>standarditemidentification>id, withing Pointer 1"
                         */
                        if ($customerDevice['service_line_id'] == $deviceId &&
                            $customerDevice['customer_id'] == $backEndCustomerId
                        ) {
                            $productData['devices'][] = $customerDevice;
                        }
                    }

                }
            }
        } else {
            $devicesData = $dot->getValue($product, 'Device');
            $productData['devices'] = $this->mapDevices($devicesData);
        }

        // Map hardware and feature categories to products
        if ($AccountCategory == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL) {
            $hardwareData = $dot->getValue($product, 'Hardware');
            $productData['hardware'] = $this->mapHardware($hardwareData);

            $featureCategoriesData = $dot->getValue($product, 'FeatureCategories');
            $productData['feature_categories'] = $this->mapFeatureCategories($featureCategoriesData);
            $productData['smart_card_Details'] = $this->mapSmartCardDetails($featureCategoriesData);

            $pricesData = $dot->getValue($product, 'ProductPrices');
            $productData['product_prices'] = $this->mapProductPrices($pricesData);
        }

        if (!array_filter($productData['devices'])) {
            unset($productData['devices']);
        }

        return $productData;
    }

    protected function mapSubProduct($subProduct)
    {
        $dot = $this->getAccessor();

        $additionalItem = $dot->getValue($subProduct, 'AdditionalItemIdentification');
        $pricingElement = $dot->getValue($subProduct, 'PricingElement');
        if (!isset($pricingElement['0']) && $pricingElement) {
            $pricingElementarray[]=$pricingElement;
        } else {
            $pricingElementarray = $pricingElement;
        }
        $subProductData = [
            'name' => $dot->getValue($subProduct, 'Name'),
            'catalog_id' => $dot->getValue($subProduct, 'CatalogueItemIdentification.ID'),
            'additional_id' => $dot->getValue($subProduct, 'AdditionalItemIdentification.ID'),
            'additional_item' => $additionalItem,
            'pricing_element' => $pricingElementarray
        ];

        return $subProductData;
    }

    protected function mapBslSubProduct($subProducts, &$result)
    {
        if(!isset($subProducts[0])) {
            $subProducts = array($subProducts);
        }
        foreach ($subProducts as $subProduct) {
            if ($mappedSubProduct=$this->mapSubProduct($subProduct)) {
                $result[]=$mappedSubProduct;

                if (!empty($subProduct["SubProduct"])) {
                    $this->mapBslSubProduct($subProduct["SubProduct"], $result);
                }
            }
        }
        return $result;
    }

    protected function mapPerson($person)
    {
        $dot = $this->getAccessor();
        $mappedPerson = [
            "firstname" => $dot->getValue($person, "FirstName"),
            "lastname" => $dot->getValue($person, "FamilyName"),
            "nationality_id" => $dot->getValue($person, "NationalityID"),
            "dob" => $dot->getValue($person, "BirthDate"),
            "phone_number" => $dot->getValue($person, "ContactPhoneNumber"),
            "title" => $dot->getValue($person, "Title"),
            "salutation" => $dot->getValue($person, "Salutation"),
            "status" => [
                "text" => $dot->getValue($person, "Status.Text"),
            ],

        ];

        if (!array_filter($mappedPerson['status'])) {
            unset($mappedPerson['status']);
        }
        if (!array_filter($mappedPerson)) {
            return [];
        }

        return $mappedPerson;
    }

    protected function mapHardware($hardwareData)
    {
        $dot = $this->getAccessor();

        /**********************************************************
         * PARSING PRODUCT HARDWARE
        *********************************************************/

        $productHardware = [];
        if (!$hardwareData) {
            return $productHardware;
        }

        $productHardware['hardware_type'] = $dot->getValue($hardwareData, 'HardwareType');
        $productHardware['serial_number'] = $dot->getValue($hardwareData, 'SerialNumber');
        $productHardware['hardware_name'] = $dot->getValue($hardwareData, 'HardwareName');

        /**********************************************************
         * EOF PARSING PRODUCT HARDWARE
         *********************************************************/

        return $productHardware;
    }

    protected function mapFeatureCategories($featureCategoriesData)
    {
        $dot = $this->getAccessor();

        /**********************************************************
         * PARSING PRODUCT FEATURE CATEGORIES
        *********************************************************/

        $productFeatureCategories = [];
        if (!$featureCategoriesData) {
            return $productFeatureCategories;
        }

        //Expecting an array of feature categories
        if (!isset($featureCategoriesData[0])) {
            $featureCategoriesData = [$featureCategoriesData];
        }

        foreach ($featureCategoriesData as $featureCategory) {
            $features = $dot->getValue($featureCategory, 'Features');

            foreach ($features as $key => $value) {
                if ($key == 'Key' && $value == 'Tel_main_number') {
                    $productFeatureCategories['tel_main_number'] = $dot->getValue($features, 'FeatureLabelValue');
                    break;
                }
            }
        }

        /**********************************************************
         * EOF PARSING PRODUCT FEATURE CATEGORIES
         *********************************************************/
        return $productFeatureCategories;
    }

    protected function mapSmartCardDetails($featureCategoriesData)
    {
        $dot = $this->getAccessor();

        /**********************************************************
         * PARSING PRODUCT FEATURE CATEGORIES
         *********************************************************/

        $smartCardDetails = [];
        if (!$featureCategoriesData) {
            return $smartCardDetails;
        }

        //Expecting an array of feature categories
        if (!isset($featureCategoriesData[0])) {
            $featureCategoriesData = [$featureCategoriesData];
        }

        foreach ($featureCategoriesData as $featureCategory) {
            $features = $dot->getValue($featureCategory, 'Features');

            if ($featureCategory["Type"] == "Devices") {
                foreach ($features as $key => $value) {
                    if ($key == "FeatureInfo" && (isset($value["SmartcardId"]) || isset($value["Type"]) || isset($value["SerialNumber"]) || isset($value["ExtResourceSpec"]) )) {
                        $smartCardDetails = $value;
                        break;
                    }
                }
            }
        }

        return $smartCardDetails;
    }

    protected function mapDevices($deviceData)
    {
        $dot = $this->getAccessor();

        /**********************************************************
         * PARSING PRODUCTS DEVICES
         *********************************************************/
        $customerDevices = [];
        if (!$deviceData) {
            return $customerDevices;
        }
        //Expecting an array of contracts
        if (!isset($deviceData[0])) {
            $deviceData = [$deviceData];
        }

        foreach ($deviceData as $device) {
            $customerDevices[] = [
                'device_type' => $dot->getValue($device, 'CommodityClassification.NatureCode'),
                'device_code' => $dot->getValue($device, 'StandardItemIdentification.ID'),
            ];
        }
        /**********************************************************
         * EOF PARSING PRODUCTS DEVICES
         *********************************************************/

        return $customerDevices;
    }

    /**
     * Map contact data
     * @param $customerData
     * @return array
     */
    public function mapContactData($customerData)
    {
        $dot = $this->getAccessor();
        // Customer phone numbers
        $customerMapping["contact_phone_numbers"] = [];
        $contactPhoneNumbers = $dot->getValue($customerData, "ContactPhoneNumber");

        if (!empty($contactPhoneNumbers)) {
            $contactPhoneNumbers = isset($contactPhoneNumbers[0]) ? $contactPhoneNumbers : [$contactPhoneNumbers];

            foreach ($contactPhoneNumbers as $phoneNumber) {
                $phoneData["country_code"] = isset($phoneNumber["CountryCode"]) ? $phoneNumber["CountryCode"] : null;
                $phoneData["local_area_code"] = !empty($phoneNumber["LocalAreaCode"]) ? $phoneNumber["LocalAreaCode"] : null;
                // In case when CountryCode and AreaCode fields are empty this field contains FullPhoneNumber.
                $phoneData["phone_number"] = !empty($phoneNumber["PhoneNumber"]) ? $phoneNumber["PhoneNumber"] : null;
                $phoneData["type"] = !empty($phoneNumber["Type"]) ? $phoneNumber["Type"] : null;

                $customerMapping["contact_phone_numbers"][] = $phoneData;

                // Set omnius customer phone number
                if (!empty($phoneData["phone_number"])) {
                    $customerMapping["phone_number"] = $phoneData["phone_number"];
                }
                if (!empty($phoneData["local_area_code"])) {
                    $customerMapping["local_area_code"] = $phoneData["local_area_code"];
                }
                if (!empty($phoneData["type"])) {
                    $customerMapping["phone_type"] = $phoneData["type"];
                }
            }
        }

        return $customerMapping;
    }

    /**
     * map Address function
     * @param $address
     * @return array
     */
    protected function mapAddress($address)
    {
        $dot = $this->getAccessor();
        $addressTypes = self::getAddressMapping();

        $addressType = null;
        if (isset($addressTypes[$dot->getValue($address, 'AddressTypeCode')])) {
            $addressType = $addressTypes[$dot->getValue($address, 'AddressTypeCode')];
        }

        $addressData = [
            // Unique address ID
            "address_id" => $dot->getValue($address, 'ID'),
            // Type of address (Billing, Legal etc)
            "address_type" => $addressType,
            "postcode" => $dot->getValue($address, 'PostalZone'),
            "post_box" => $dot->getValue($address, 'Postbox'),
            "street" => $dot->getValue($address, 'StreetName'),
            "house_number" => $dot->getValue($address, 'BuildingNumber'),
            "house_addition" => $dot->getValue($address, 'HouseNumberAddition'),
            "city" => $dot->getValue($address, 'CityName'),
            "district" => $dot->getValue($address, 'District') ?: $dot->getValue($address, 'Region'),
            "state" => $dot->getValue($address, 'CountrySubentity'),
            "country" => $dot->getValue($address, 'Country'),
            "footprint" => $dot->getValue($address, 'FootPrint'),
        ];

        if (!array_filter($addressData)) {
            return [];
        }
        return $addressData;
    }

    /**
     * getAddressMapping function
     * @return array
     */
    public static function getAddressMapping()
    {
        return array(
            static::ADDRESS_TYPE_LEGAL => Dyna_Customer_Model_Customer::DEFAULT_ADDRESS,
            static::ADDRESS_TYPE_C => Dyna_Customer_Model_Customer::DEFAULT_ADDRESS,
            static::ADDRESS_TYPE_LO => Dyna_Customer_Model_Customer::DEFAULT_ADDRESS,
            static::ADDRESS_TYPE_USER => Dyna_Customer_Model_Customer::USER_ADDRESS,
            static::ADDRESS_TYPE_BILLING => Mage_Customer_Model_Address_Abstract::TYPE_BILLING,
            static::ADDRESS_TYPE_SERVICE => Dyna_Customer_Model_Customer::SERVICE_ADDRESS,
            static::ADDRESS_TYPE_SHIPPING => Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING,
            static::ADDRESS_TYPE_DL => Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING,
        );
    }


    /**
     * PARSING POSTAL ADDRESS
     * Used for check-serviceablity and checkout
     */
    public function mapPostalAddressData($customerData)
    {
        $dot = $this->getAccessor();

        $postalAddresses = $dot->getValue($customerData, "Addresses");
        $customerMappedAddresses = [];
        isset($postalAddresses[0]) ?: $postalAddresses = [$postalAddresses];
        foreach ($postalAddresses as $arrayData) {
            $postalAddress = new Varien_Object();
            $postalAddress->addData($this->mapAddress($arrayData));
            $customerMappedAddresses[] = $postalAddress;
        }

        return [
            "postal_addresses" => $customerMappedAddresses,
        ];
    }

    /**
     * PARSING POSTAL ADDRESS
     * Used for check-serviceablity and checkout
     */
    public function mapCustomerAddressData($customerData)
    {
        $dot = $this->getAccessor();
        $postalAddresses = $dot->getValue($customerData, "Addresses");
        return [
            "postal_addresses" => $postalAddresses,
        ];

    }

    /**
     * Map Billing function
     * @param type $customerData
     * @return array
     */
    protected function mapBilling($customerData)
    {
        $dot = $this->getAccessor();

        $billingAccountData = $dot->getValue($customerData, 'BillingAccount');
        $iban = $dot->getValue($billingAccountData, 'ID');
        $bic = $dot->getValue($billingAccountData, 'BIC');
        $bankName = $dot->getValue($billingAccountData, 'FinancialInstitutionBranch.Name');
        $bankID = $dot->getValue($billingAccountData, 'FinancialInstitutionBranch.ID');
        $bankNumber = $dot->getValue($billingAccountData, 'FinancialInstitutionBranch.FinancialInstitution.ID');
        $password = $dot->getValue($customerData, 'CustomerPassword');
        $paymentMethod = $dot->getValue($billingAccountData, 'PaymentMethod');
        $partyIdentyfication = $dot->getValue($billingAccountData, 'PartyIdentyfication.ID');
        $nextBillCycleStartDate = $dot->getValue($billingAccountData, 'NextBillCycleStartDate');
        return [
            'bic' => $bic,
            'iban' => $iban,
            'bank_name' => $bankName,
            'bank_id' => $bankID,
            'bank_number' => $bankNumber,
            'password' => $password,
            'payment_method' => $paymentMethod,
            'party_identyfication' => $partyIdentyfication,
            'next_bill_cycle_start_date' => $nextBillCycleStartDate
        ];
    }

    /**
     * Map billing information Account & Billing list
     * @param $customerData
     * @return array
     */
    public function mapBillingData($customerData)
    {
        $dot = $this->getAccessor();

        $billingDetails = $dot->getValue($customerData, 'BillingAccount');
        return [
            "billing_account" => $billingDetails,
            'last_billing_amount' => $dot->getValue($customerData, 'BillingDetails.LastBillAmount'),
            'last_billing_due_amount' => $dot->getValue($customerData, 'BillingDetails.LastBillDueAmount'),
            'last_back_out_amount' => $dot->getValue($customerData, 'BillingDetails.LastBackOutAmount'),
            'legacy_dunning_amount' => $dot->getValue($customerData, 'DunningAmountSystem.LegacyDunningAmount'),
            'display_dunning_amount' => $dot->getValue($customerData, 'DunningAmountSystem.DisplayDunningAmount')
        ];
    }

    /**
     * map CustomerData fucntion
     * @param $customerData
     * @param &$bans
     * @param &$contractTypes
     * @return array
     */
    public function mapCustomerData($customerData, &$bans, &$contractTypes)
    {
        $dot = $this->getAccessor();
        $bans[$customerData['AccountCategory']][] = $dot->getValue($customerData, 'PartyIdentification.ID');
        $contractTypes[$customerData['AccountCategory']][] = $dot->getValue($customerData, 'Contract.ContractType');

        $customerMapping = array(
            'bans' => $bans[$customerData['AccountCategory']],
            'contract_types' => $contractTypes[$customerData['AccountCategory']]
        );

        $customerType = $dot->getValue($customerData, 'PartyType');

        if (!$customerType) {
            // Set Default Customer Party Type - Private
            $customerType = ($customerData['AccountCategory'] == static::VODAFONE_CUSTOMER_STACK) ? static::VODAFONE_PARTY_TYPE_PRIVATE : static::ZIGGO_PARTY_TYPE_PRIVATE;
        }

        $isBusinessCustomer = ($customerType == static::ZIGGO_PARTY_TYPE_BUSINESS)
            || ($customerType == static::VODAFONE_PARTY_TYPE_BUSINESS)
            || ($customerType == static::VODAFONE_PARTY_TYPE_BUSINESS_CORPORATE);

        if ($isBusinessCustomer) {
            $customerMapping += [
                "company_name" => $dot->getValue($customerData, 'PartyLegalEntity.RegistrationName'),
                "company_date" => $dot->getValue($customerData, 'PartyLegalEntity.RegistrationDate'),
                "company_id" => $dot->getValue($customerData, 'PartyLegalEntity.CompanyID'),
                "company_legal_form" => Mage::helper('vznl_checkout')->getBusinessLegalFormsKey($dot->getValue($customerData, 'PartyLegalEntity.BusinessInformation')),
                "company_additional_name" => $dot->getValue($customerData, 'PartyLegalEntity.CompanyAdditionalName'),
                "company_city" => $dot->getValue($customerData, 'PartyLegalEntity.RegistrationAddress.CityName'),
            ];
        }

        if ($dot->getValue($customerData, 'NoOrderingAllowed') == "true") {
            $NoOrderingAllowed = (boolean)true;
        } else {
            $NoOrderingAllowed = (boolean)false;
        }

        $identificationInformation = $dot->getValue($customerData, 'IdentificationInformation');

        if (isset($identificationInformation[0])) {
            $identificationInformation = $identificationInformation[0];
        }

        $customerMapping += [
            "is_business" => $isBusinessCustomer,
            "party_type" => $customerType,
            "party_subtype" => $dot->getValue($customerData, 'PartySubType'),
            "peal_party_subtype" => $dot->getValue($customerData, 'peal_party_subtype'),
            "account_class" => $dot->getValue($customerData, 'AccountClass'),
            "title" => $dot->getValue($customerData, 'Role.Person.Title'),
            "firstname" => $dot->getValue($customerData, 'Role.Person.FirstName'),
            "middlename" => $dot->getValue($customerData, 'Role.Person.MiddleName'),
            "lastname" => $dot->getValue($customerData, 'Role.Person.FamilyName'),
            "mobile_email" => $dot->getValue($customerData, 'Contact.ElectronicMail'),
            "mobile_telephone" => $dot->getValue($customerData, 'Contact.PhoneNumber'),
            "dob" => $dot->getValue($customerData, 'Role.Person.BirthDate'),
            "global_id" => $dot->getValue($customerData, 'PartyIdentification.ID'),
            "customer_number" => $dot->getValue($customerData, 'PartyIdentification.ID'),
            "customer_password" => $dot->getValue($customerData, 'CustomerPassword'),
            "life_cycle_status" => $dot->getValue($customerData, 'CustomerLifeCycleStatus'),
            "customer_status" => !empty($dot->getValue($customerData, 'CustomerLifeCycleStatus')) ? $dot->getValue($customerData, 'CustomerLifeCycleStatus') : "",
            "dunning_status" => str_replace(array(' ', "\n"), array('', ''), $dot->getValue($customerData, 'DunningStatus')),
            "account_category" => $dot->getValue($customerData, 'AccountCategory'),
            "valid_until" => $dot->getValue($customerData, 'IdentificationInformation.ValidFor.EndDateTime'),
            "issue_date" => $dot->getValue($customerData, 'IdentificationInformation.IssueDate'),
            "id_type" => $dot->getValue($customerData, 'IdentificationInformation.IdentificationType'),
            "id_number" => $dot->getValue($customerData, 'IdentificationInformation.IdentificationNr'),
            "issuing_country" => $dot->getValue($customerData, 'IdentificationInformation.IssuingCountry'),
            "nationality_id" => $dot->getValue($customerData, 'Role.Person.NationalityID'),
            "identification_number" => $dot->getValue($customerData, 'Role.Person.IdentificationTypeNumber'),
            "salutation" => $dot->getValue($customerData, 'Role.Person.Salutation'),
            'last_billing_amount' => $dot->getValue($customerData, 'BillingDetails.LastBillAmount'),
            'last_billing_due_amount' => $dot->getValue($customerData, 'BillingDetails.LastBillDueAmount'),
            'last_back_out_amount' => $dot->getValue($customerData, 'BillingDetails.LastBackOutAmount'),
            'legacy_dunning_amount' => $dot->getValue($customerData, 'DunningAmountSystem.LegacyDunningAmount'),
            'display_dunning_amount' => $dot->getValue($customerData, 'DunningAmountSystem.DisplayDunningAmount'),
            'IdentificationInformation' => $identificationInformation,
            'ContactInformation' => $dot->getValue($customerData, 'Contact'),
            'PartyChooser' => $dot->getValue($customerData, 'PartyChooser'),
            'CustomerRank' => $dot->getValue($customerData, 'CustomerRank'),
            'BillingArrangementId' => $dot->getValue($customerData, 'BillingArrangementId'),
            'ProductInvolvementRole' => $dot->getValue($customerData, 'ProductInvolvementRole'),
            "no_ordering_allowed" => $NoOrderingAllowed,
        ];

        $customerMapping['is_temporary'] =
            false !== strpos($customerMapping['customer_number'], Dyna_Customer_Helper_Customer::TEMPORARY_CUSTOMER_PREFIX)
                ? 1 : 0;

        return $customerMapping;
    }


    /**
     * @return Omnius_Service_Model_DotAccessor
     */
    public function getAccessor()
    {
        return Mage::getSingleton('omnius_service/dotAccessor');
    }

    public function setSessionForServiceCustomers()
    {
        Mage::getSingleton('dyna_customer/session')->setServiceCustomers($this->data);
    }

    public function getSessionForServiceCustomers()
    {
        return Mage::getSingleton('dyna_customer/session')->getServiceCustomers();
    }

    public function getArticleNumberAddons($deviceCode)
    {
        return Mage::getModel('dyna_multimapper/addon')->getCollection()
                    ->addFieldToFilter(
                        [
                            'addon_additional_value',
                            'addon_additional_value'
                        ],
                       [
                            ['eq' => "/functions/hardware/rentedHardwareConfiguration/articleNumber/existing"],
                            ['eq' => "/functions/hardware/hardwareConfiguration/articleNumber/existing"]
                       ])
                    ->addFieldToFilter('addon_name', ['eq' => "articleNumber"])
                    ->addFieldToFilter('addon_value', ['eq' => $deviceCode])
                    ->addFieldToFilter('direction', ['in' => [Dyna_MultiMapper_Model_Mapper::DIRECTION_IN, Dyna_MultiMapper_Model_Mapper::DIRECTION_BOTH]]);
    }

    public function getsubventionCodeAddons($subventionCode)
    {
        return Mage::getModel('dyna_multimapper/addon')->getCollection()
                    ->addFieldToFilter(
                        [
                            'addon_additional_value',
                            'addon_additional_value'
                        ],
                        [
                            ['eq' => "/functions/hardware/rentedHardwareConfiguration/subventionCode/existing"],
                            ['eq' => "/functions/hardware/hardwareConfiguration/subventionCode/existing"]
                        ])
                    ->addFieldToFilter('addon_name', ['eq' => "subventionCode"])
                    ->addFieldToFilter('addon_value', ['eq' => $subventionCode])
                    ->addFieldToFilter('direction', ['in' => [Dyna_MultiMapper_Model_Mapper::DIRECTION_IN, Dyna_MultiMapper_Model_Mapper::DIRECTION_BOTH]]);
    }

    protected function mapProductPrices($pricesData)
    {
        $dot = $this->getAccessor();

        /**********************************************************
         * PARSING PRODUCT PRICES
         *********************************************************/

        $productPrices = [];
        if (!$pricesData) {
            return $productPrices;
        }

        $productPrices['price_id'] = $dot->getValue($pricesData, 'PriceId');
        $productPrices['type'] = $dot->getValue($pricesData, 'Type');
        $productPrices['amount'] = $dot->getValue($pricesData, 'Amount');
        $productPrices['name'] = $dot->getValue($pricesData, 'Name');

        /**********************************************************
         * EOF PARSING PRODUCT PRICES
         *********************************************************/

        return $productPrices;
    }
}
