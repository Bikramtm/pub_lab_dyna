<?php

class Vznl_Customer_Model_Session extends Dyna_Customer_Model_Session
{
    /**
     * Add a hint to the list of hints
     * @return $this
     */
    public function addHint($hintMessage, $onKey = false)
    {
        $hints = $this->getHints();

        foreach ($hints as $hint){
            //Add hint of a rule once per action
            if ( isset($hint['ruleId']) && isset($hint['packageId'])
                && $hint['ruleId'] == $hintMessage['ruleId']
                && $hint['packageId'] == $hintMessage['packageId']
            ) {
                $this->setData(static::KEY_HINT_MESSAGE, $hints);
                return $this;
            }
        }

        $onKey === false ? $hints[] = $hintMessage : $hints[$onKey] = $hintMessage;
        $this->setData(static::KEY_HINT_MESSAGE, $hints);

        return $this;
    }
}
