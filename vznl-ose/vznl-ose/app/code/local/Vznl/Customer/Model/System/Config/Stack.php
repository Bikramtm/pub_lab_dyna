<?php

class Vznl_Customer_Model_System_Config_Stack
{
    const VODAFONE_STACK = 'BSL'; // BSL is the API used by Vodafone for the mobile stack
    const ZIGGO_STACK = 'PEAL'; // PEAL is the API used by Ziggo for fixed stack

    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::VODAFONE_STACK,
                'label' => 'Vodafone'
            ),
            array(
                'value' => self::ZIGGO_STACK,
                'label' => 'Ziggo'
            )
        );
    }
}
