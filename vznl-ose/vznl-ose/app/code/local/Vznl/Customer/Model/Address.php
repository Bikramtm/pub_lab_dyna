<?php

class Vznl_Customer_Model_Address extends Omnius_Customer_Model_Address
{
    /**
     * set address street informa
     *
     * @param unknown_type $street
     * @param boolean $notrim
     * @return unknown
     */
    public function setStreet($street, $noTrim = false)
    {
        if (is_array($street)) {
            if ($noTrim) {
                $street=implode("\n", $street);
            } else {
                $street=trim(implode("\n", $street));
            }
        }
        $this->setData('street', $street);
        return $this;
    }
}
