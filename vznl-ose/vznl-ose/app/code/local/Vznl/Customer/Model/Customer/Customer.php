<?php

/**
 * Class Vznl_Customer_Model_Customer_Customer
 */
class Vznl_Customer_Model_Customer_Customer extends Dyna_Customer_Model_Customer
{
    CONST MASK_CHAR = '****';
    CONST UNMASK_CHAR_LENGTH = -4;

    protected $returnNumberic;
    /**
     * Builds the last name of the customer based on business identifier
     * @return string
     */
    public function getCompleteLastName(): string
    {
        return $this->getIsBusiness()
            ? ($this->getContractantMiddlename() ? $this->getContractantMiddlename() . ' ' : '') . $this->getContractantLastname()
            : ($this->getMiddlename() ? $this->getMiddlename() . ' ' : '') . $this->getLastname();
    }

    /**
     * Overriding the getBan method of parent class, as we need the ban and not the customer_number
     * @return string
     */
    public function getBan()
    {
        return $this->getData('ban');
    }

     /**
     * Retrieve the list of saved shopping carts
     *
     * @param bool $includeOffers
     * @return array
     */
    public function getShoppingCarts($includeOffers = false)
    {
        /** @var Mage_Sales_Model_Resource_Quote_Collection $carts */
        $carts = Mage::getModel('sales/quote')->getCollection();
        $carts->addFieldToFilter('main_table.customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId()); //getSelectedCustomer
        $canSubmitRiskOrders = Mage::getSingleton('customer/session')->getAgent(true)->canSubmitRiskOrders();
        if ($includeOffers) {
            if ($canSubmitRiskOrders) {
                $carts->getSelect()->where(
                    '(-- saved shopping cart
                        main_table.cart_status = \'' . Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED . '\'
                          AND main_table.is_offer = \'0\'
                        ) 
                        OR (
                          -- offer
                          main_table.cart_status IS NULL 
                          AND main_table.is_offer = \'1\'
                          AND main_table.store_id = ' . Mage::app()->getStore()->getId() . '
                        ) 
                        OR (
                          -- risk
                          main_table.cart_status = \'' . Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK . '\'
                          AND main_table.is_offer = \'1\'
                        
                    )'
                );
            } else {
                $carts->getSelect()->where(
                    '(
                        -- saved shopping cart
                        main_table.cart_status = \'' . Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED . '\'
                          AND main_table.is_offer = \'0\'
                        ) 
                        OR (
                          -- offer
                          main_table.cart_status IS NULL 
                          AND main_table.is_offer = \'1\'
                          AND main_table.store_id = ' . Mage::app()->getStore()->getId() . '
                        )'
                );
            }
            $offerLifetime = Mage::helper('omnius_checkout')->getOfferLifetime();
            $nowTimestamp = Mage::getModel('core/date')->timestamp();
        } else {
            // Check if agent can also see the risk saved shopping carts
            if ($canSubmitRiskOrders) {
                $carts->addFieldToFilter('main_table.cart_status', array('in', array(Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED, Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK)));
            } else {
                $carts->addFieldToFilter('main_table.cart_status', Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED);
            }
        }
        $carts->addFieldToSelect(array('entity_id', 'is_offer', 'cart_status', 'created_at', 'store_id', 'updated_at'));

        $carts->getSelect()->join(array('pkg' => 'catalog_package'), 'main_table.entity_id = pkg.quote_id', array('pkg.entity_id AS package_id'));
        $carts->getSelect()->joinLeft(array('orders' => 'sales_flat_order'), 'main_table.entity_id = orders.quote_id', array('orders.entity_id AS order_id'));

        $carts->setOrder('main_table.created_at', 'desc');
        $carts->getSelect()->group('main_table.entity_id');
        $carts->getSelect()->having('(order_id IS NULL AND package_id IS NOT NULL)');

        $cartsArray = array();
        foreach ($carts as $cart) {
            if (!$cart->getOrderId() && $cart->getPackageId()) {
                if ($cart->getIsOffer()) {
                    if (!$canSubmitRiskOrders && $cart->getCartStatus() == Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK) {
                        // Ensure no offer with risk is shown
                        continue;
                    }
                    $quoteTimestamp = Mage::getModel('core/date')->timestamp(strtotime($cart->getCreatedAt()));
                    $quoteTimestamp = strtotime("+$offerLifetime days", $quoteTimestamp);
                    $quoteTimestamp = strtotime(date("Y-m-d 23:59:59", $quoteTimestamp));
                    $daysBetween = ceil(($quoteTimestamp - $nowTimestamp) / 86400);
                    $cart->setOfferExpireDays($daysBetween);
                }

                $cartsArray[] = $cart;
            }
        }

        return $cartsArray;
    }

    /**
     * @return string
     */
    public function getSalutation($returnNumberic = false)
    {
        $this->returnNumberic = $returnNumberic;
        $gender = ($this->getIsBusiness() && $this->getContractantGender() != null) ? $this->getContractantGender() : $this->getGender();
        return Mage::helper('vznl_checkout')->__(($gender == 2) ? 'madam' : 'mister');
    }


    /**
     * Gets shopping carts summary for shopping carts list
     * @return array
     */
    public function mapShoppingCarts() {
        $session = Mage::getSingleton('dyna_customer/session');
        $checkoutHelper = Mage::helper('vznl_checkout');
        $agent = $session->getAgent();
        $isViewSavedOffer = $agent->isGranted(Vznl_Agent_Model_Agent::VIEW_SAVED_OFFER);
        $isDeleteSavedOffer = $agent->isGranted(Vznl_Agent_Model_Agent::DELETE_SAVED_OFFER);
        $isChangeSavedOffer = $agent->isGranted(Vznl_Agent_Model_Agent::CHANGE_SAVED_OFFER);
        $isViewSavedCart = $agent->isGranted(Vznl_Agent_Model_Agent::VIEW_SAVED_SHOPPING_CART);
        $isDeleteSavedCart = $agent->isGranted(Vznl_Agent_Model_Agent::DELETE_SAVED_SHOPPING_CART);
        $isChangeSavedCart = $agent->isGranted(Vznl_Agent_Model_Agent::CHANGE_SAVED_SHOPPING_CART);
        $basketsList = [ 'offers' => [], 'riskOrders' => [], 'baskets' => [], 'permissions' => [] ];
        $basketsList['permissions'] = array(
            'view_saved_cart' => $isViewSavedCart,
            'delete_saved_cart' => $isDeleteSavedCart,
            'change_saved_cart' => $isChangeSavedCart,
            'view_saved_offer' => $isViewSavedOffer,
            'delete_saved_offer' => $isDeleteSavedOffer,
            'change_saved_offer' => $isChangeSavedOffer
        );
        $carts = $this->getShoppingCarts(true);
        $isReadonly = false;
        $isExpired = false;
        foreach( $carts as $cart ) {
            switch(true) {
                case $cart->isOffer():
                    $basketType = Mage::helper('vznl_checkout')->__("Offer");
                    $isExpired = $checkoutHelper->isOfferExpired($cart->getUpdatedAt()??$cart->getCreatedAt());
                    $isReadonly = $checkoutHelper->isOfferReadonly($cart->getUpdatedAt()??$cart->getCreatedAt());
                    $key = 'offers';
                    break;
                case $cart->isRisk():
                    $basketType = Mage::helper('vznl_checkout')->__("Risk order");
                    $key = 'riskOrders';
                    break;
                case $cart->isSavedCart():
                    $basketType = Mage::helper('vznl_checkout')->__("Shopping cart");
                    $isExpired = $checkoutHelper->isCartExpired($cart->getUpdatedAt()??$cart->getCreatedAt());
                    $isReadonly = $checkoutHelper->isCartReadonly($cart->getUpdatedAt()??$cart->getCreatedAt());
                    $key = 'baskets';
                    break;
                default:
                    continue 2;
            }
            if( !$agent->canSubmitRiskOrders() && $key == 'riskOrders' ) continue;
            $basketsList[$key][] = array(
                'item_content' => $key,
                'id' => $cart->getId(),
                'cart_type' => $cart->getTypeOfCartLiteral(),
                'is_offer' => $cart->getIsOffer(),
                'created_date' =>Mage::app()->getLocale()->date(Mage::getModel('core/date')->timestamp(strtotime($cart->getUpdatedAt())), null, null, false)->toString('dd-MM-y'),
                'item_type' => $basketType,
                'is_expired' => $isExpired,
                'is_readonly' => $isReadonly,
                'channel' => $cart->getStoreId() ? Mage::getModel('core/store')->load($cart->getStoreId())->getWebsite()->getName() : '-',
                'lob' => [
                    'has_mobile' => $cart->hasMobilePackage(),
                    'has_fixed' => $cart->hasFixed()
                ],
                'package_count' => count($cart->getPackages())
            );
        }
        return $basketsList;
    }

    /**
     * Checks the is_business indicator of the customer and casts the result to a boolean
     * @return bool
     */
    public function getIsBusiness() : bool
    {
        return (bool)$this->getData('is_business');
    }

    /**
     * Get customer SOHO status from customer attributes
     * @return boolean
     */
    public function getIsSoho()
    {
        return (bool)$this->getData("is_business");
    }

    /**
     * checks if customer isProspect
     * @return boolean
     */
    public function checkIsProspect()
    {
        if ($this->getPan() == "" && $this->getBan() == "") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return customer data
     * Formats all dates fields
     *
     * @return mixed
     */
    public function getCustomData()
    {
        $data = $this->getData();
        if (isset($data['dob']) && !empty($data['dob'])) {
            $data['dob'] = date('d-m-Y', strtotime($data['dob']));
        }

        if (isset($data['contractant_dob']) && !empty($data['contractant_dob'])) {
            if (!is_numeric($data['contractant_dob'])) {
                $data['contractant_dob'] = strtotime($data['contractant_dob']);
            }
            $data['contractant_dob'] = date('d-m-Y', $data['contractant_dob']);
        }
        if (isset($data['valid_until']) && !empty($data['valid_until'])) {
            $data['valid_until'] = date('d-m-Y', strtotime($data['valid_until']));
        }
        if (isset($data['issue_date']) && !empty($data['issue_date'])) {
            $data['issue_date'] = date('d-m-Y', strtotime($data['issue_date']));
        }
        if (isset($data['contractant_valid_until']) && !empty($data['contractant_valid_until'])) {
            $data['contractant_valid_until'] = date('d-m-Y', strtotime($data['contractant_valid_until']));
        }
        if (isset($data['contractant_issue_date']) && !empty($data['contractant_issue_date'])) {
            $data['contractant_issue_date'] = date('d-m-Y', strtotime($data['contractant_issue_date']));
        }
        if (isset($data['company_date']) && !empty($data['company_date'])) {
            $data['company_date'] = date('d-m-Y', strtotime($data['company_date']));
        }

        if (isset($data['contractant_id_number']) && !empty($data['contractant_id_number'])) {
            $data['contractant_id_number'] = $this->getMaskIdNumber();
        }
        if (isset($data['id_number']) && !empty($data['id_number'])) {
            $data['id_number'] = $this->getMaskIdNumber();
        }
        if (isset($data['account_no']) && !empty($data['account_no'])) {
            $data['account_no'] = $this->getMaskAccountNumber();
        }
        
        $data['email'] = (isset($data['additional_email']) && $data['additional_email']) ? $data['additional_email'] : '';

        // Default setting is non business customer
        if (!isset($data['is_business'])) {
            $data['is_business'] = 0;
        }
        $fields = array(
            'entity_id' => '',
            'email' => '',
            'prefix' => '',
            'firstname' => '',
            'middlename' => '',
            'lastname' => '',
            'customer_label' => '',
            'suffix' => '',
            'id_type' => '',
            'id_number' => '',
            'valid_until' => '',
            'issuing_country' => '',
            'issue_date' => '',
            'account_holder' => '',
            'account_no' => '',
            'dob' => '',
            'gender' => '',
            'is_business' => '',
            'customer_ctn' => '',
            'ban' => '',
            'company_name' => '',
            'company_coc' => '', //kwk
            'company_postcode' => '',
            'company_house_nr' => '',
            'company_house_nr_addition' => '',
            'company_street' => '',
            'company_date' => '',
            'company_city' => '',
            'company_legal_form' => '',
            'company_vat_id' => '', //bwb

            'contractant_lastname' => '',
            'contractant_middlename' => '',
            'contractant_firstname' => '',
            'contractant_dob' => '',
            'contractant_prefix' => '',
            'contractant_gender' => '',
            'contractant_id_type' => '',
            'contractant_id_number' => '',
            'contractant_valid_until' => '',
            'contractant_issuing_country' => '',
            'contractant_issue_date' => '',
            'privacy_market_research' => '',
            'privacy_sales_activities' => '',
            'privacy_number_information' => '',
            'privacy_phone_books' => '',
            'privacy_sms' => '',
            'privacy_disclosure_commercial' => '',
            'privacy_email' => '',
            'privacy_mailing' => '',
            'privacy_telemarketing' => '',
            'additional_email' => '',
            'additional_fax' => '',
            'additional_telephone' => '',
        );

        $data = array_intersect_key($data, $fields);
        return $data;
    }

    /**
     * Check if customer is logged in
     */
    public function isCustomerLoggedIn()
    {
        if($this->isProspect()) {
            return $this->getId();
        }

        return $this->getContactId() || $this->getEntityId();
    }

    public function getMaskIdNumber()
    {
        return $this->getIsSoho() ? self::MASK_CHAR . substr($this->getContractantIdNumber(), self::UNMASK_CHAR_LENGTH) : self::MASK_CHAR . substr($this->getIdNumber(), self::UNMASK_CHAR_LENGTH);
    }

    public function getMaskAccountNumber()
    {
        return self::MASK_CHAR . substr($this->getAccountNo(), self::UNMASK_CHAR_LENGTH);
    }
}
