<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 *
 * Adding missing indexes
 * 
 */

/* @var $installer Mage_Customer_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

$connection->addIndex($this->getTable('customer/entity'),'IDX_CUSTOMER_ENTITY_PAN_ENTITY_TYPE_ID', array('pan', 'entity_type_id'));
$connection->addIndex($this->getTable('customer/entity'),'IDX_CUSTOMER_ENTITY_BAN_ENTITY_TYPE_ID', array('ban', 'entity_type_id'));
$connection->addIndex($this->getTable('customer/entity'),'IDX_CUSTOMER_ENTITY_ACCOUNT_IDENTIFIER_MOBILE_ENTITY_TYPE_ID',
	array('account_identifier_mobile', 'entity_type_id'));
$connection->addIndex($this->getTable('customer/entity'),'IDX_CUSTOMER_ENTITY_ACCOUNT_IDENTIFIER_FIXED_ENTITY_TYPE_ID',
	array('account_identifier_fixed','entity_type_id'));

$installer->endSetup();