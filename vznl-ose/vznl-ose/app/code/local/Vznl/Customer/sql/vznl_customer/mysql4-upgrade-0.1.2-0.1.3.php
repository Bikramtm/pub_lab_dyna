<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 *
 * pan - Peal Account Number
 * account_identifier_mobile - Mobile Account Identifier
 * account_identifier_fixed - Fixed Account Identifier
 * account_identifier_linkid - Link ID
 * 
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = $this;
$customerInstaller->startSetup();
$connection = $customerInstaller->getConnection();

$connection->addColumn($this->getTable('customer/entity'), 'pan', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 15,
    'nullable' => true,
    'required' => false,
    'comment' => "Customer Peal Account Number"
]);

$connection->addColumn($this->getTable('customer/entity'), 'account_identifier_mobile', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 50,
    'nullable' => true,
    'required' => false,
    'comment' => "Customer Account Identifier for Mobile"
]);

$connection->addColumn($this->getTable('customer/entity'), 'account_identifier_fixed', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 50,
    'nullable' => true,
    'required' => false,
    'comment' => "Customer Account Identifier for Fixed"
]);

$connection->addColumn($this->getTable('customer/entity'), 'account_identifier_linkid', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 50,
    'nullable' => true,
    'required' => false,
    'comment' => "Link Id"
]);

$connection->addIndex($this->getTable('customer/entity'), 'IDX_CUSTOMER_ENTITY_PAN', 'pan');
$connection->addIndex($this->getTable('customer/entity'), 'IDX_CUSTOMER_ENTITY_PAN', 'account_identifier_mobile');
$connection->addIndex($this->getTable('customer/entity'), 'IDX_CUSTOMER_ENTITY_PAN', 'account_identifier_fixed');
$connection->addIndex($this->getTable('customer/entity'), 'IDX_CUSTOMER_ENTITY_PAN', 'account_identifier_linkid');
$customerInstaller->endSetup();