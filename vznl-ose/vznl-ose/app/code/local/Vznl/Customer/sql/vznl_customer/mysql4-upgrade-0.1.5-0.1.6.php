<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 *
 * party_identifier_mobile - Mobile Party Identifier
 * party_identifier_fixed - Fixed Party Identifier
 *
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = $this;
$customerInstaller->startSetup();
$connection = $customerInstaller->getConnection();

$connection->addColumn($this->getTable('customer/entity'), 'party_identifier_mobile', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 50,
    'nullable' => true,
    'required' => false,
    'comment' => "Party Account Identifier for Mobile"
]);

$connection->addColumn($this->getTable('customer/entity'), 'party_identifier_fixed', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 50,
    'nullable' => true,
    'required' => false,
    'comment' => "Party Account Identifier for Fixed"
]);

$connection->addIndex($this->getTable('customer/entity'), 'IDX_CUSTOMER_ENTITY_PIM', 'party_identifier_mobile');
$connection->addIndex($this->getTable('customer/entity'), 'IDX_CUSTOMER_ENTITY_PIF', 'party_identifier_fixed');
$customerInstaller->endSetup();
