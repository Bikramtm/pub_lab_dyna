<?php

/**
 * Class Vznl_FFHandler_Model_Resource_Request_Customer_Auth_Value
 */
class Vznl_FFHandler_Model_Resource_Request_Customer_Auth_Value extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init("ffhandler/request_customer_auth_value", "request_customer_auth_value_id");
    }
}