<?php

/**
 * Class Vznl_FFHandler_Model_Request_Customer_Auth_Value
 *
 * @method int getRequestId()
 * @method string getValue()
 * @method Vznl_FFHandler_Model_Request_Customer_Auth_Value setRequestId(int $requestId)
 * @method Vznl_FFHandler_Model_Request_Customer_Auth_Value setValue(string $value)
 */
class Vznl_FFHandler_Model_Request_Customer_Auth_Value extends Mage_Core_Model_Abstract
{
    /**
     * @var Vznl_FFHandler_Model_Request
     */
    protected $request;

    protected function _construct()
    {
        $this->_init("ffhandler/request_customer_auth_value");
    }

    /**
     * @return Vznl_FFHandler_Model_Request
     */
    public function getRequest()
    {
        if (!$this->getRequestId()) {
            Mage::log('Request not found');
            return null;
        }

        if (!$this->request) {
            /**
             * @var Vznl_FFHandler_Model_Request $requestModel
             */
            $requestModel = Mage::getModel("ffhandler/request");
            $requestModel->load($this->getRequestId());
            $this->request = $requestModel;
        }

        return $this->request;
    }

    public function setRequest(Vznl_FFHandler_Model_Request $request)
    {
        $this->request = $request;

        return $this->setRequestId($request->getId());
    }
}