<?php

class Vznl_FFHandler_Model_Resource_Request_Usage extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init("ffhandler/request_usage", "request_usage_id");
    }
}