<?php

/**
 * Resource Model for Friends and Family Request Handlers
 *
 * @category    Vznl
 * @package     Vznl_FFHandler
 */
class Vznl_FFHandler_Model_Resource_Request_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Initialize resource
     *
     */
    protected function _construct()
    {
        $this->_init('ffhandler/request');
    }

    /**
     * Get all requests made by a requester for a specific relation that are not canceled
     *
     * @param string $requesterEmail
     * @param int $relationId
     * @return Vznl_FFHandler_Model_Resource_Request_Collection
     */
    public function filterCountableValidByRequestEmailAndRelationId($requesterEmail, $relationId)
    {
        $this->addFieldToFilter("requester_email", $requesterEmail);
        $this->addFieldToFilter("relation_id", $relationId);
        $this->addFieldToFilter("threshold_countable", true);
        $this->addFieldToFilter("status", array(Vznl_FFHandler_Model_Request::STATUS_WAITING, Vznl_FFHandler_Model_Request::STATUS_COMPLETED));

        return $this;
    }

    public function filterByRelationId($relationId)
    {
        $this->addFieldToFilter("relation_id", $relationId);

        return $this;
    }

    public function filterCanceled()
    {
        $this->addFieldToFilter("status", Vznl_FFHandler_Model_Request::STATUS_CANCELED);

        return $this;
    }
    public function filterWaiting()
    {
        $this->addFieldToFilter("status", Vznl_FFHandler_Model_Request::STATUS_WAITING);

        return $this;
    }
    public function filterCompleted()
    {
        $this->addFieldToFilter("status", Vznl_FFHandler_Model_Request::STATUS_COMPLETED);

        return $this;
    }

    /**
     * Mark all emails for a provided requester email as not countable for threshold
     *
     * @param string $requesterEmail
     * @return $this
     */
    public function markThresholdNotCountable($requesterEmail)
    {
        $this->addFieldToFilter("requester_email", $requesterEmail);
        $this->addFieldToFilter("threshold_countable", true);

        Mage::log(sprintf("Mark all previous requests for requester '%s' as not countable for threshold", $requesterEmail));

        /**
         * TODO: Optimize query
         * @var Vznl_FFHandler_Model_Request $request
         */
        foreach ($this as $request) {
            $request->setThresholdCountable(false);
            $request->save();
        }

        return $this;
    }

    public function filterValidNotUsedToRemind( $reminderInterval )
    {
        $reminderInterval = !$reminderInterval && $reminderInterval == '' ? 1 : (int)$reminderInterval;

        $crtDate = strftime('%Y-%m-%d %H:%M:%S', time());

        $this
            ->addFieldToFilter('status', Vznl_FFHandler_Model_Request::STATUS_COMPLETED)
            ->addFieldToFilter('valid_from', array('to' => $crtDate))
            ->addFieldToFilter('valid_to', array('from' => $crtDate))

            ->getSelect()->joinLeft(array('u' => 'ff_requests_usage'),
                'main_table.request_id = u.request_id',
                array('count_usage_per_code' => 'COUNT(u.request_usage_id)'))
            ->where("DATEDIFF('$crtDate', created_at) > ?", 0)
            ->where("MOD(DATEDIFF('$crtDate', created_at), $reminderInterval)= ?", 0)
            ->group('main_table.request_id')
            ->having('count_usage_per_code = ?', 0);

        return $this;
    }

    public function filterWaitingForApproval( $resendInterval )
    {
        $resendInterval = !$resendInterval && $resendInterval == '' ? 1 : (int)$resendInterval;

        $crtDate = strftime('%Y-%m-%d %H:%M:%S', time());

        // get all ff codes where
        // request status = STATUS_WAITING
        // are still valid and
        // compare with $resendInterval => send email to confirm request to generate ff code
        $this
            ->addFieldToFilter('status', Vznl_FFHandler_Model_Request::STATUS_WAITING)
            ->addFieldToFilter('valid_from', array('to' => $crtDate))
            ->addFieldToFilter('valid_to', array('from' => $crtDate))
            ->getSelect()
            ->having("CAST((TIME_TO_SEC(TIMEDIFF('$crtDate', created_at )) / 3600) AS UNSIGNED) > ?", 0)
            ->having("MOD(CAST((TIME_TO_SEC(TIMEDIFF('$crtDate', created_at )) / 3600) AS UNSIGNED), $resendInterval) = ?", 0);

        return $this;
    }

    public function filterCompletedByRequesterEmail( $requesterEmail )
    {
        $this
        ->addFieldToFilter('customer_auth_type', Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_EMAIL)
        ->addFieldToFilter('status', Vznl_FFHandler_Model_Request::STATUS_COMPLETED)
        ->addFieldToFilter('requester_email', $requesterEmail)
        ->getSelect()->reset('columns')
        ->joinLeft('ff_requests_usage',
            'main_table.request_id = ff_requests_usage.request_id',
            array('main_table.code', 'main_table.status', 'main_table.created_at', 'ff_requests_usage.request_usage_id', 'ff_requests_usage.client')
        );

        return $this;
    }

    public function getRequestersWithCompletedRequestsBetweenDates( $first, $last )
    {
        $this
        ->addFieldToSelect('requester_email')
            ->addFieldToFilter('created_at', array('gteq' =>$first))
            ->addFieldToFilter('created_at', array('lteq' =>$last))
        ->addFieldToFilter('customer_auth_type', Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_EMAIL)
        ->addFieldToFilter('status', Vznl_FFHandler_Model_Request::STATUS_COMPLETED)
        ->getSelect()
        ->group('requester_email');

        return $this;
    }

    public function filterCompletedByRequesterBetweenDates( $requesterEmail, $first, $last )
    {
        $this
            ->addFieldToFilter('created_at', array('gteq' =>$first))
            ->addFieldToFilter('created_at', array('lteq' =>$last))
            ->addFieldToFilter('customer_auth_type', Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_EMAIL)
            ->addFieldToFilter('status', Vznl_FFHandler_Model_Request::STATUS_COMPLETED)
            ->addFieldToFilter('requester_email', $requesterEmail)
            ->getSelect()->reset('columns')
            ->joinLeft('ff_requests_usage',
                'main_table.request_id = ff_requests_usage.request_id',
                array('main_table.code', 'main_table.status', 'main_table.created_at', 'ff_requests_usage.request_usage_id', 'ff_requests_usage.client')
            )
            ->order(array('ff_requests_usage.client ASC'));

        return $this;
    }
}
