<?php

/**
 * Class Vznl_Service_Model_Client_WorkItemClient
 */
class Vznl_Service_Model_Client_WorkItemClient extends Vznl_Service_Model_Client_Client
{
    const WORK_ITEM_TYPE = 405;

    public function executeWorkItem($salesOrderNumber, $packageId)
    {
        $businessIdentifier = Mage::app()->getStore()->getCode() == Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE ? 'BelCompany' : 'Vodafone';
        $params = array(
            'SalesOrdernummer' => $salesOrderNumber,
            'WorkItemType' => self::WORK_ITEM_TYPE,
            'BusinessIdentifier' => $businessIdentifier,
            'PackageID' => $packageId
        );

        return $this->SendWorkItem($params);
    }
} 