<?php

/**
 * Class ApprovalClient
 */
class Vznl_Service_Model_Client_ApprovalClient extends Vznl_Service_Model_Client_Client
{
	/**
	 * method to get the agent
	 */
	protected function _getAgent(&$agentId)
	{
	    if ($agentId) {
            /** @var Dyna_Agent_Model_Agent $agent */
            $agent = Mage::getModel('agent/agent')->load($agentId);
        } else {
            $agent = Mage::helper('agent')->getAgent();
            $agentId = $agent->getId();
        }
        
        return $agent;
	}
		
	/**
     * Method approveOrder
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param null $agentId
     * @param bool $returnParams
     * @param bool $checkLocks
     * @param null $lockUser
     * @return array
     * @throws Varien_Exception
     * @throws Zend_Soap_Client_Exception
     */
    public function approveOrder(Vznl_Superorder_Model_Superorder $superOrder, $agentId = null, $returnParams = false, 
    	$checkLocks = false , $lockUser = null, $incrementId = null, &$deliveryOrder = null)
    {       
        $websiteCode = Mage::app()->getWebsite($superOrder->getWebsiteId())->getCode();
        $isFixed = $superOrder->hasFixed();        
        $agent = $this->_getAgent($agentId);

        if (!$deliveryOrder) {
        	$orders = $superOrder->getOrders(true);
        	foreach ($orders as $order) {
        		if (($order->isStoreDelivery() && $order->getShippingAddress() && $order->getShippingAddress()->getDeliveryStoreId() == $agent->getDealer()->getId())
        			|| ($websiteCode == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) || ($isFixed && $websiteCode == Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE)
        		) {
        			$deliveryOrder = $order;
        			if(($incrementId != null) && ($incrementId != $deliveryOrder->getIncrementId())) {
        				continue;
        			}
        	
        			break;
        		}
        	}
        }       
        
        if (!$deliveryOrder) {
            return false;
        }
        
        $packagesWithApproval = array();
        $removePayment = $deliveryOrder->isVirtualDelivery();
        $needAproval = false;
        $deliveryOrderPackages = $deliveryOrder->getDeliveryOrderPackages();

        foreach ($deliveryOrderPackages as $orderPackage) {
            if (!$orderPackage->isRetention()) {
                $removePayment = false;
            }

            if (!$orderPackage->getApproveOrder() && ($orderPackage->needsApprovePackage() || $orderPackage->isRetention() || $isFixed)) {
                $packagesWithApproval[] = $orderPackage->getPackageId();
                $needAproval = true;
            }
        }

        if (!$needAproval) {
            return false;
        }

        $daDealercode = '00830000';

        if ($agent && ($dealer = $agent->getDealer())) {
            $daDealercode = $dealer->getVfDealerCode();
            $axiCode = $dealer->getAxiStoreCode();
        } elseif ($agent && ($dealer = Mage::getModel('agent/dealer')->load($agent->getDealerId()))) {
            $daDealercode = $dealer->getVfDealerCode();
            $axiCode = $dealer->getAxiStoreCode();
        }

        // Update approve_order_job_id fields when the call is added in Redis
        $updatePackages = [];

        $parameters = [
            'n1:Header/tns:TransactionId' => time(),
            'n1:Header/tns:PartyName' => Mage::getStoreConfig('vodafone_service/general/partyname'),
            'n1:Header/tns:UserId' => $agentId,
            'n1:Channel/tns:ChannelName' => Mage::getStoreConfig('vodafone_service/general/channelname'),
            'n1:DealerInformation/tns:DealerCode' => $daDealercode,
            'SalesOrderNumber' => $superOrder->getOrderNumber(),
            'DeliveryOrderID' => $deliveryOrder->getIncrementId(),
            'StoreID' => ($websiteCode == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) ? Mage::helper('agent')->getWharehouseCode() : $axiCode,
            'StoreUserID' => $agentId,
            'Packages/Package' => function ($packages, $ns) use ($superOrder, $deliveryOrder, $deliveryOrderPackages, &$updatePackages) {
                /** @var Omnius_Core_Model_SimpleDOM $basePackage */
                $basePackage = array_shift($packages);
                foreach ($packages as $pack) {
                    unset($pack[0]);
                }
                /** @var Dyna_Package_Model_Package $orderPackage */
                foreach ($deliveryOrderPackages as $orderPackage) {
                    $packageItems = $orderPackage->getPackageItems();
                    $hasNumberPorting = false;
                    $subscription = null;
                    $isFixed = $orderPackage->isFixed();

                    /** @var Vznl_Checkout_Model_Sales_Order_Item $packageItem */
                    foreach ($packageItems as $packageItem) {
                        /** @var Vznl_Catalog_Model_Product $product */
                        $product = $packageItem->getProduct();
                        if ($product->isNetwork()) {
                            $subscription = $packageItem;
                            $hasNumberPorting = (strlen($orderPackage->getCurrentNumber()) > 0); // Numberporting hack for indirect, there is no porting, but this will cause confirmorder to fire
                        }
                    }

                    if (($orderPackage->needsApprovePackage() || $orderPackage->isRetention()) && !$orderPackage->getApproveOrder() || $isFixed) {
                        $packageNode = new Omnius_Core_Model_SimpleDOM('<root></root>');
                        $package = $packageNode->addChild('Package', null, 'http://DynaLean/ProcessOrder/ApprovePackages/V1-0');
                        $ns = 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0';
                        $package->addChild('PackageID', $superOrder->getOrderNumber() . '.' . $orderPackage->getPackageId(), $ns);

                        $updatePackages[] = $orderPackage;
                        if ($subscription || $isFixed) {
                            $package->addChild('DAOrderID', $orderPackage->getPackageEsbNumber(), $ns);

                            if (!$hasNumberPorting) {
                                $package->addChild('MSISDN', $orderPackage->getTelNumber() ? $orderPackage->getTelNumber() : $orderPackage->getCtn(), $ns); // Chosen from nr pool or default
                            } else {
                                $package->addChild('MSISDN', $orderPackage->getCurrentNumber(), $ns); // Current mobile nr, port-in CTN
                            }
                            $package->addChild('NumberPortingRequired', $hasNumberPorting ? 'true' : 'false', $ns);

                            if ($hasNumberPorting || Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE || $isFixed) {
                                $handsetDetails = $orderPackage->getHandsetDetails();

                                $confirmationLines = $package->addChild('ConfirmationLines', null, $ns);
                                $confirmationLine = $confirmationLines->addChild('ConfirmationLine', null, $ns);
                                $confirmationLine->addChild('FulfillmentGroupID', 0, $ns);
                                $lineItem = $confirmationLine->addChild('LineItem', null, $ns);
                                $item = $lineItem->addChild('Item', null, $ns);
                                $inventoryItem = $item->addChild('InventoryItem', null, $ns);
                                if ($hasNumberPorting || Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                                    $inventoryItem->addChild('IMEI', $orderPackage->getImei() ?: null, $ns);
                                    $inventoryItem->addChild('SEHandsetCategory', isset($handsetDetails['category']) ? $handsetDetails['category'] : '', $ns);
                                    $inventoryItem->addChild('SEManufacturer', isset($handsetDetails['manufacturer']) ? $handsetDetails['manufacturer'] : '', $ns);
                                    $inventoryItem->addChild('SEModelName', isset($handsetDetails['name']) ? $handsetDetails['name'] : '', $ns);
                                    $inventoryItem->addChild('SIMnumber', $orderPackage->getSimNumber(), $ns);
                                }
                                if ($isFixed) {
                                    foreach ($packageItems as $packageItem) {
                                        $bomId = $packageItem->getBomId();
                                        $offerId = $packageItem->getOfferId();
                                        if($bomId && ($packageItem->getBoxSerialNumber() || $packageItem->getEanNumber() || $packageItem->getSerialNumber())) {
                                            $fixedHardwareDetails = $inventoryItem->addChild('FixedHardwareDetails', null, $ns);
                                            $fixedHardwareDetails->addChild('HardwareName', $packageItem->getHardwareName(), $ns);
                                            if ($packageItem->getBoxSerialNumber()) {
                                                $serialNumberDetails = $fixedHardwareDetails->addChild('SerialNumberDetails', null, $ns);
                                                $serialNumberDetails->addChild('SerialNumberID', $packageItem->getBoxSerialNumber(), $ns);
                                                $serialNumberDetails->addChild('SerialNumberType', 'mediabox', $ns);
                                            }
                                            if ($packageItem->getEanNumber()) {
                                                $serialNumberDetails = $fixedHardwareDetails->addChild('SerialNumberDetails', null, $ns);
                                                $serialNumberDetails->addChild('SerialNumberID', $packageItem->getEanNumber(), $ns);
                                                $serialNumberDetails->addChild('SerialNumberType', 'EAN', $ns);
                                            }
                                            if ($packageItem->getSerialNumber()) {
                                                $serialNumberDetails = $fixedHardwareDetails->addChild('SerialNumberDetails', null, $ns);
                                                $serialNumberDetails->addChild('SerialNumberID', $packageItem->getSerialNumber(), $ns);
                                                $serialNumberDetails->addChild('SerialNumberType', 'smartcardnumber', $ns);
                                            }
                                            $fixedHardwareDetails->addChild('BomID', $bomId, $ns);
                                            $fixedHardwareDetails->addChild('ProductID', $packageItem->getSku(), $ns);
                                        }
                                    }
                                }
                            }
                        }
                        $basePackage->insertAfterSelf($package);
                    }
                }
                unset($basePackage[0]);
            },
            'Payments/Payment' => function ($payments, $ns) use ($superOrder, $deliveryOrder, $removePayment) {
                if ($removePayment) {
                    $parent = $payments[0]->xpath('..');
                    unset($parent[0][0]);

                    return;
                }

                /** @var Omnius_Core_Model_SimpleDOM $baseAddress */
                $basePayment = array_shift($payments);
                foreach ($payments as $pay) {
                    unset($pay[0]);
                }

                $ns = 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0';

                $hasPayments = false;
                $counter = 1;
                if ($deliveryOrder && $deliveryOrder->getPayment() && !$deliveryOrder->hasFixed()
                    && $deliveryOrder->getPayment()->getMethodInstance()
                    && $deliveryOrder->getPayment()->getMethodInstance()->getCode() === 'checkmo'
                    && $deliveryOrder->getGrandTotal() > 0
                ) {
                    $paymentNode = new Omnius_Core_Model_SimpleDOM('<root></root>');
                    $payment = $paymentNode->addChild('Payment', null, 'http://DynaLean/ProcessOrder/ApprovePackages/V1-0');
                    $payment->addChild('State', 'Add', $ns);
                    $payment->addChild('PaymentId', $counter++, $ns);
                    $payment->addChild('Amount', round($deliveryOrder->getGrandTotal(), 2), $ns);
                    $payment->addChild('Tender', 'REKUBUY', $ns);
                    $basePayment->insertAfterSelf($payment);
                    $hasPayments = true;
                }

                if (!$hasPayments) {
                    $basePayments = $basePayment->xpath('..');
                    unset($basePayments[0][0]);

                    //$this->removePaymentNodes();
                    return;
                }

                unset($basePayment[0]);
            },
        ];

        if ($returnParams) {
            return $parameters;
        }        
        
        $this->_lockOrderCheckout($superOrder, $checkLocks, $lockUser);
        
        // Set flag to check for order lock and release frontend order lock, backend lock should be removed by AXI after processing the job
        Mage::register(
            'job_additional_information',
            [
                'run_check_lock' => true,
                'do_hand_over' => true,
                'hand_over_from' => $agentId,
                'hand_over_to' => 'ESB',
                'agent_id' => $agentId,
                'run_release_lock' => true,
                'superorder_id' => $superOrder->getId(),
                'superorder_number' => $superOrder->getOrderNumber(),
                'do_approve_package' => count($packagesWithApproval) ? $packagesWithApproval : false,
            ]
        );

        $jobId = $this->ReceiveResponseApprovePackage($parameters);
        $this->_updatePackageJobId($updatePackages, $jobId);        

        return $jobId;
    }
    
    protected function _lockOrderCheckout(&$superOrder, $checkLocks, $lockUser)
    {
    	if ($checkLocks && !$lockUser) {
    		Mage::helper('dyna_checkout')->lockOrder($superOrder->getId(), null, 'Approve Order');
    	}
    }
    
    /**
     * method to update job id to the packages
     * @param unknown $updatePackages
     * @param unknown $jobId
     */
    protected function _updatePackageJobId($updatePackages, $jobId)
    {
    	if (count($updatePackages)) {
    		foreach ($updatePackages as $updatePackage) {
    			$updatePackage->setApproveOrderJobId($jobId);
    			Mage::helper('vznl_utility')->saveModel($updatePackage);
    		}
    	}
    }
}
