<?php

/**
 * Class Vznl_Service_Model_Client_CancelDuringCreditCheckClient
 */
class Vznl_Service_Model_Client_CancelDuringCreditCheckClient extends Vznl_Service_Model_Client_Client
{
    /**
     * @param string|array $orderIdentifier
     * @return mixed
     */
    public function cancelSuperOrder($orderIdentifier)
    {
        return $this->CancelDuringCreditCheck(array(
            'SalesOrderNumber' => $orderIdentifier,
        ));
    }

    /**
     * Check if the client should send the special headers
     *
     * @return bool
     */
    protected function needsWSAHeader()
    {
        return true;
    }

    /**
     * Send the special WSA headers
     */
    protected function addWSAHeader()
    {
        $header = new Vznl_Service_Model_SimpleDOM('<root></root>');
        $header->addChild('wsa:Action', 'CancelDuringCreditCheck', "http://www.w3.org/2005/08/addressing");
        $header->addChild('wsa:To', Mage::helper('dyna_service')->getCancelDuringCreditCheckConfig('usage_url'), "http://www.w3.org/2005/08/addressing");
        $xml = '';
        foreach ($header->children('wsa', true) as $child) {
            $xml .= $child->asXml();
        }
        $var = new SoapVar($xml, XSD_ANYXML);
        $header = new SOAPHeader('http://DynaLean.UBUY.CancelDuringCreditCheck/V1-0/CancelDuringCreditCheck/CancelDuringCreditCheckRequest', 'Header', $var);

        parent::__setSoapHeaders(array($header));

        return $this;
    }
}
