<?php

class Vznl_Service_Model_Client_EbsOrchestrationClient_Package extends Mage_Core_Model_Abstract
{
    private $_changeType = ''; // Before or after delivery
    private $_action = ''; // reserve, cancel, change, addresschange
    private $_saleType = ''; // retention - aquisition - inlife
    private $_packageType = ''; // mobile/hardware
    private $_packageId = 0; // Package ID before change
    private $_oldPackage = null; // This old package
    private $_packageModel = null; // Native package model
    private $_deliveryType = ''; // home delivery, other store, order store
    private $_refundReason = null;
    private $_deliveryDealerCode = ''; // Dealercode to be deliverd to
    private $_deliveryStoreId = ''; // Store ID to be deliverd to, if not current store or home delivery
    private $_axiState = ''; // Cancel, Nothing, Reserve
    private $_vfSubscription = null; // VF description
    private $_transactionType = ''; // acquisition - retention
    private $_returnPackageId = '';
    private $_packageItems = array(); // Array with package items
    private $_oldPackageItems = array(); // Array with package items
    private $_deliveryOrder = null;
    private $_skus = array();
    private $_skusAssoc = array();
    private $_activateExceptionalCase = null;
    private $_comments = null;
    private $_hasInsurance = false;
    private $_orderAxiState = null;

    public function hasInsurance()
    {
        return $this->_hasInsurance;
    }

    public function getComments()
    {
        return $this->_comments;
    }

    public function getActivateExceptionalCase()
    {
        return $this->_activateExceptionalCase;
    }

    public function getSkus()
    {
        return $this->_skus;
    }

    public function getSkusAssoc()
    {
        return $this->_skusAssoc;
    }

    public function getOldPackage()
    {
        return $this->_oldPackage;
    }

    /**
     * @return Vznl_Checkout_Model_Sales_Order
     */
    public function getDeliveryOrder()
    {
        return $this->_deliveryOrder;
    }

    public function getOldPackageItems()
    {
        return $this->_oldPackageItems;
    }

    public function getPackageId()
    {
        return $this->_packageId;
    }

    public function setPackageId($packageId)
    {
        $this->_packageId = $packageId;
    }

    public function getReturnPackageId()
    {
        return $this->_returnPackageId;
    }

    public function getTransactionType()
    {
        return $this->_transactionType;
    }

    public function getRefundReason()
    {
        return $this->_refundReason;
    }

    public function getDeliveryType()
    {
        return $this->_deliveryType;
    }

    public function getDeliveryDealerCode()
    {
        return $this->_deliveryDealerCode;
    }

    public function getDeliveryStoreId()
    {
        return $this->_deliveryStoreId;
    }

    public function getChangeType()
    {
        return $this->_changeType;
    }

    public function getAction()
    {
        return $this->_action;
    }

    public function hasSubscriptionChange()
    {
        if ($this->_vfSubscription === null || $this->_oldPackage === null) {
            return false;
        }

        return !in_array($this->_vfSubscription->getSku(), $this->_oldPackage->getSkus());
    }

    /**
     * @return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem[]
     */
    public function getPackageItems()
    {
        return $this->_packageItems;
    }

    public function isChanged()
    {

    }

    public function getPackageType()
    {
        return $this->_packageType;
    }

    public function getPackageDescription()
    {
        return 'Pakket ' . $this->_packageId;
    }

    /**
     * @return Vznl_Package_Model_Package
     */
    public function getPackageModel()
    {
        return $this->_packageModel;
    }

    public function getVfSubscription()
    {
        return $this->_vfSubscription;
    }

    public function getAxiState()
    {
        return $this->_axiState;
    }

    public function getOrderAxiState()
    {
        return $this->_orderAxiState;
    }

    public function getItemBySku($sku)
    {
        foreach ($this->_packageItems as $packageItem) {
            if ($packageItem->getSku() === $sku) {
                return $packageItem;
            }
        }

        return null;
    }

    private function _parseAxiType()
    {
        switch ($this->_action) {
            case 'cancel':
                switch ($this->_changeType) {
                    case 'before':
                        return 'Cancel';
                    case 'after':
                        if ($this->getOrderAxiState() == 'Reserve') {
                            return 'Reserve';
                        }

                        return 'Return';
                }
                break;
            case 'addresschange':
                return 'Nothing';
            case 'change':
                switch ($this->_changeType) {
                    case 'before':
                        return 'Nothing';
                    case 'after':
                        return 'Reserve';
                }
                break;
            case 'reserve':
                return 'Reserve';
            case 'nothing':
                return 'Nothing';
        }
    }

    public static function _lineItemHasAttributeValue($item, $attributeName, $value)
    {
        if ($attr = $item->getProduct()->getResource()->getAttribute($attributeName)) {
            if (strtolower($value) == strtolower($attr->getFrontend()->getValue($item->getProduct()->load($item->getProductId())))) {
                return true;
            }
        }

        return false;
    }

    private function _isSinglePositionType($packageLineItem)
    {
        return Mage::helper('dyna_catalog')->is(
            array(
                Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                Vznl_Catalog_Model_Type::SUBTYPE_DEVICE,
                Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD,
                Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION,
            ),
            $packageLineItem->getProduct()
        );
    }

    private function _getPackageStructure($packageItems)
    {
        $packageStructure['bytype'][Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION] = '';
        $packageStructure['bytype'][Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION] = '';
        $packageStructure['bytype'][Vznl_Catalog_Model_Type::SUBTYPE_DEVICE] = '';
        $packageStructure['bytype'][Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD] = '';
        $packageStructure['bytype'][Vznl_Catalog_Model_Type::SUBTYPE_ADDON] = array();
        $packageStructure['bytype'][Vznl_Catalog_Model_Type::SUBTYPE_PROMOTION] = array();
        $packageStructure['bytype'][Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY] = array();
        $packageStructure['skus'] = array();

        foreach ($packageItems as $packageItem) {
            $prodType = $packageItem->getProduct()->getType();
            if (is_array($prodType)) {
                $prodType = reset($prodType);
            }
            if(!$prodType){
                continue;
            }
            if ($this->_isSinglePositionType($packageItem)) {
                $packageStructure['bytype'][$prodType] = $packageItem->getSku();
            } else {
                $packageStructure['bytype'][$prodType][] = $packageItem->getSku();
            }
            $packageStructure['skus'][] = $packageItem->getSku();
        }

        return $packageStructure;
    }

    private function _getItemBySku($packageItems, $sku)
    {
        foreach ($packageItems as $packageItem) {
            if ($packageItem->getSku() === $sku) {
                return $packageItem;
            }
        }

        return null;
    }

    /**
     * @param integer $packageId
     * @param array $packageItems
     * @param array $oldPackageItems
     * @param Vznl_Checkout_Model_Sales_Order $deliveryOrder
     * @param string $action
     * @param string $orderAxiState
     * @param $esbSuperorder
     */
    public function parse($packageId, $packageItems, $oldPackageItems, $deliveryOrder, $action, $orderAxiState, $esbSuperorder)
    {
        $this->_action = $action;
        $this->_packageId = $packageId;
        $this->_deliveryOrder = $deliveryOrder;
        $this->_orderAxiState = $orderAxiState;
        $this->_packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $deliveryOrder->getSuperorderId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();

        $firstItem = reset($packageItems);
        $this->_packageType = strtoupper($firstItem->getPackageType());
        $this->_refundReason = $this->_packageModel->getRefundReason();

        if (in_array($this->_packageModel->getData('type'), Vznl_Catalog_Model_Type::getMobilePackages())) {
            if ($deliveryOrder->isVirtualDelivery()) {
                if (in_array($deliveryOrder->getWebsiteCode(), array(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE, Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE))) {
                    // Virtual deliveries at store always through cash register
                    $this->_deliveryType = 'order store';
                } else {
                    $this->_deliveryType = 'home delivery';
                }
            } elseif ($deliveryOrder->getShippingAddress() && $deliverStoreId = $deliveryOrder->getShippingAddress()->getDeliveryStoreId()) {
                /** @var Vznl_Agent_Model_Dealer $dealer */
                $dealer = Mage::getModel('agent/dealer')->load($deliverStoreId);
                if ($dealer->getAxiStoreCode() == Mage::helper('agent')->getAxiStore()) {
                    $this->_deliveryType = 'order store';
                } else {
                    $this->_deliveryType = 'other store';
                }
                $this->_deliveryStoreId = $dealer->getAxiStoreCode();
            } else {
                $this->_deliveryType = 'home delivery';
            }
        } else {
            if ($deliveryOrder->getShippingAddress() && $deliverStoreId = $deliveryOrder->getShippingAddress()->getDeliveryStoreId()) {
                /** @var Vznl_Agent_Model_Dealer $dealer */
                $dealer = Mage::getModel('agent/dealer')->load($deliverStoreId);
                if ($dealer->getAxiStoreCode() == Mage::helper('agent')->getAxiStore()) {
                    $this->_deliveryType = 'order store';
                } else {
                    $this->_deliveryType = 'other store';
                }
                $this->_deliveryStoreId = $dealer->getAxiStoreCode();
            } else {
                $this->_deliveryType = 'home delivery';
            }
        }

        // Set delivery dealer code
        if(is_null($deliveryOrder->getShippingAddress()->getDeliveryStoreId())){
            $this->_deliveryDealerCode = Mage::getModel('agent/dealer')->load($deliveryOrder->getDealerId())->getVfDealerCode();
        } else {
            $this->_deliveryDealerCode = Mage::getModel('agent/dealer')->load($deliveryOrder->getShippingAddress()->getDeliveryStoreId())->getVfDealerCode();
        }

        switch ($this->_packageModel->getSaleType()) {
            case 'acquisitie':
            case Dyna_Catalog_Model_ProcessContext::ACQ:
                $this->_transactionType = 'acquisition';
                break;
            case 'retentie':
                $this->_transactionType = 'retention';
                break;
            case 'inlife':
                $this->_transactionType = 'inlife';
                break;
            default:
                $this->_transactionType = 'retention'; //Fallback to retention as default as it was
                break;
        }

        $oldPackageStructure = null;
        if ($oldPackageItems !== null) {
            $oldPackageStructure = $this->_getPackageStructure($oldPackageItems);

            $this->_oldPackage = new Vznl_Service_Model_Client_EbsOrchestrationClient_Package();
            $parentDeliveryOrder = Mage::getModel('sales/order')->load($deliveryOrder->getParentId() ?: $deliveryOrder->getDeliveredParentId($packageId));
            if ($this->_packageModel->getOldPackageId()) {
                $packageId = $this->_packageModel->getOldPackageId();
            }
            $this->_oldPackage->parse($packageId, $oldPackageItems, null, $parentDeliveryOrder, null, $this->getOrderAxiState(), $esbSuperorder);
            $this->_oldPackageItems = $this->_oldPackage->getPackageItems();
            $this->_changeType = ($this->_oldPackage->getPackageModel()->isDelivered() ? 'after' : 'before');
        } else {
            $this->_changeType = ($this->_packageModel->isDelivered() ? 'after' : 'before');
        }

        $valueId = -1;
        $attr = Mage::getModel('catalog/product')->getResource()->getAttribute(Vznl_Catalog_Model_Product::ADDON_TYPE_ATTR);
        if ($attr && $attr->usesSource()) {
            $valueId = $attr->getSource()->getOptionId("Garant");
        }

        $this->_skus = array_map(function ($item) {
            return $item->getSku();
        }, $packageItems);
        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addFieldToFilter('sku', array('in' => $this->_skus))
            ->addAttributeToSelect('*');

        $packageStructure = $this->_getPackageStructure($packageItems);
        $newlyAddedItems = array();
        $hasDoa = false;
        foreach ($packageItems as $packageItem) {
            $productInstance = $productCollection->getItemByColumnValue('sku', $packageItem->getSku());
            if ($productInstance === null) {
                // In case the product is not found, check if the product is deleted, else ensure that an product object is returned
                $productInstance = Mage::getModel('catalog/product')->loadByAttribute('sku', $packageItem->getSku() . "|" . $packageItem->getProductId());
                // If product is still not found return an empty instance
                if (!$productInstance || !$productInstance->getId()) {
                    $productInstance = Mage::getModel('catalog/product');
                }
            }

            if (!isset($this->_skusAssoc[$packageItem->getSku()])) {
                $this->_skusAssoc[$packageItem->getSku()] = 0;
            }
            $this->_skusAssoc[$packageItem->getSku()]++;

            if ($productInstance->getData(Vznl_Catalog_Model_Product::ADDON_TYPE_ATTR) === $valueId) {
                $this->_hasInsurance = true;
            }

            $oldPackageItem = null;
            $oldPackageSku = null;
            $prodType = $productInstance->getType();
            if (is_array($productInstance->getType())) {
                $prodType = reset($prodType);
            }
            if ($oldPackageItems !== null && $oldPackageStructure !== null && in_array($prodType, array_keys($oldPackageStructure['bytype']))) {
                // Get old package item, we can determine if it has changed
                if ($this->_isSinglePositionType($packageItem)) {
                    $oldPackageSku = $oldPackageStructure['bytype'][$prodType];
                } else {
                    // Not possible to determine is item has change, only if it is still there
                    if (in_array($packageItem->getSku(), $oldPackageStructure['bytype'][$prodType])) {
                        $oldPackageSku = $packageItem->getSku(); // Item is still here
                    }
                }

                if ($oldPackageSku !== null) {
                    $oldPackageItem = $this->_getItemBySku($oldPackageItems, $oldPackageSku);
                }
            }

            //$item = Mage::getModel('vznl_service/client_ebsorchestrationclient_packageitem');
            $item = new Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem();
            $item->parse($packageItem, $oldPackageItem, $this->_packageModel, $this, $productInstance);

            if (self::_lineItemHasAttributeValue($packageItem, Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK, Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK_VODAFONE)
                && Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $productInstance)
            ) {
                $this->_vfSubscription = $packageItem;
            }

            $this->_packageItems[] = $item;
            if ($item->getItemDoa()) {
                $hasDoa = true;
                if (
                    $item->isDevice()
                    && $esbSuperorder->getId() == $this->_packageModel->getOrderId()
                ) {
                    $this->_packageModel->setImei(null)->save();
                }
            }

            // TODO refactor
            if ($item->getTkhInc()) {
                // TKH high
                $tkhItem = new Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem();
                $tkhItem->parseAsTkhServiceItem($item, $oldPackageItem, $this);

                $this->_skus[] = $tkhItem->getSku();

                if (!isset($this->_skusAssoc[$tkhItem->getSku()])) {
                    $this->_skusAssoc[$tkhItem->getSku()] = 0;
                }
                $this->_skusAssoc[$tkhItem->getSku()]++;

                $this->_packageItems[] = $tkhItem;
            }
        }

        $this->_axiState = $this->_parseAxiType();

        if ($this->_changeType === 'after' && ($this->_action === 'change' || $this->_action === 'cancel')) {
            $this->_returnPackageId = $packageId;
        }

        if ($this->_action == 'cancel' && $this->_changeType === 'after' && $hasDoa) {
            $this->_refundReason = 'DOA';
        }

        $this->_activateExceptionalCase = ($this->_packageModel->getManualPickup() !== null && $this->_packageModel->getManualPickup() !== '');
        $this->_comments = ($this->_packageModel->getManualPickupRemarks());
    }

    /**
     * @param $item
     * @return null|string
     */
    public function getReturnRefundReason($item=null)
    {
        if($this->getRefundReason() != null){
            return $this->getRefundReason();
        } else if(!is_null($item)) {
            return $item->getItemDoa() ? 'DOA' : ($this->getRefundReason() ?: 'INSTRUCTIE');
        }
        return null;
    }
}
