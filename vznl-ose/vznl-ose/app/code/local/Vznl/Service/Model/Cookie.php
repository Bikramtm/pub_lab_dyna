<?php

class Vznl_Service_Model_Cookie extends Mage_Core_Model_Cookie
{
    public function isSecure()
    {
        if ($this->getStore()->isAdmin() || Mage::helper('vznl_service')->checkSecureFrontend()) {
            return $this->_getRequest()->isSecure();
        }
        return false;
    }
}