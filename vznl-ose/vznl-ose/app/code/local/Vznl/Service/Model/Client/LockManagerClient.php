<?php

/**
 * Class Vznl_Service_Model_Client_LockManagerClient
 */
class Vznl_Service_Model_Client_LockManagerClient extends Vznl_Service_Model_Client_Client
{
    const LockType = 'UBUY';
    protected $retryLimit;
    protected $tries;
    protected $shouldStop;

    /**
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param integer $agentId
     * @param string $reason
     * @return mixed
     */
    public function lockSaleOrder($superOrder, $agentId, $reason = 'Test Reason')
    {
        $this->tries = 0;
        $orderStatus = $superOrder->getOrderStatus();

        while ($this->tries < $this->getRetryLimit()) {
            try {
                $informBackend = $orderStatus == Vznl_Superorder_Model_Superorder::SO_VALIDATED_PARTIALLY ? 'false' : 'true';
                $response = $this->LockObject(
                    array(
                        'LockedObjectID' => $superOrder->getOrderNumber(),
                        'UserID' => $agentId,
                        'LockType' => self::LockType,
                        'LockReason' => $reason,
                        'InformBackendSystem' => $informBackend,
                    )
                );

                if (isset($response['success']) && $response['success'] != 1 && $response['success'] != "true") {
                    throw new Exception($response['message']);
                }
                break;
            } catch (Exception $e) {
                $this->handleException($e);
            }
        }

        return $response;
    }

    public function unLockSaleOrder($superOrder, $agentId)
    {
        $this->tries = 0;
        $orderStatus = $superOrder->getOrderStatus();
        while ($this->tries < $this->getRetryLimit()) {
            try {
                $informBackend = $orderStatus === Vznl_Superorder_Model_Superorder::SO_VALIDATED_PARTIALLY ? 'false' : 'true';
                $response = $this->UnlockObject(
                    array(
                        'LockedObjectID' => $superOrder->getOrderNumber(),
                        'UserID' => $agentId,
                        'LockType' => self::LockType,
                        'InformBackendSystem' => $informBackend,
                    )
                );

                if (isset($response['success']) && $response['success'] != 1 && $response['success'] != "true") {
                    throw new Exception($response['message']);
                }
                break;
            } catch (Exception $e) {
                $this->handleException($e);
            }
        }

        return $response;
    }

    public function doHandOverLock($superOrderNumber, $agentFromId, $agentToId)
    {
        $this->tries = 0;
        while ($this->tries < $this->getRetryLimit()) {
            try {
                $response = $this->HandOverLock(
                    array(
                        'UserIDFrom' => $agentFromId,
                        'UserIDTo' => $agentToId,
                        'LockType' => self::LockType,
                        'LockedObjectID' => $superOrderNumber
                    )
                );

                if (isset($response['success']) && $response['success'] != 1 && $response['success'] != "true") {
                    throw new Exception($response['message']);
                }
                break;
            } catch (Exception $e) {
                $this->handleException($e);
            }
        }

        return $response;
    }

    public function doGetLockInfo($superOrder)
    {
        try {
            $response = $this->GetLockInfo(
                array(
                    'LockedObjectID' => $superOrder->getOrderNumber(),
                    'LockType' => self::LockType
                )
            );

            if (isset($response['success']) && $response['success'] != 1 && $response['success'] != "true") {
                throw new Exception($response['message']);
            }
        } catch (SoapFault $e) {
            $this->log($e->getMessage());
            throw new Exception($e->getMessage());
        }

        return $response;
    }

    public function doGetLockedObjectList()
    {
        try {
            $response = $this->GetLockedObjectList(
                array(
                    'LockType' => self::LockType
                )
            );

            if (isset($response['success']) && $response['success'] != 1 && $response['success'] != "true") {
                $this->log($response['message']);
                throw new Exception($response['message']);
            }
        } catch (SoapFault $e) {
            $this->log($e->getMessage());
            throw new Exception($e->getMessage());
        }

        return $response;
    }

    /**
     * @return mixed
     *
     * Get limit of retries for lock calls
     */
    protected function getRetryLimit()
    {
        if (!$this->retryLimit) {
            $queue = Mage::helper('vznl_job')->getQueue('default');
            $this->retryLimit = $queue->getConfig()->getData('retry_limit') ? (int) $queue->getConfig()->getData('retry_limit') : 1;
        }

        return $this->retryLimit;
    }

    /**
     * @param $e
     * @throws Exception
     *
     * Handle exceptions
     */
    protected function handleException($e)
    {
        $this->tries++;
        $this->log($e->getMessage());
        if ($this->tries == $this->getRetryLimit()) {
            throw new Exception($e->getMessage());
        }
    }
}