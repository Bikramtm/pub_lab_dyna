<?php

/**
 * Class Vznl_Service_Model_Client_SyncClient
 */
class Vznl_Service_Model_Client_SyncClient extends Vznl_Service_Model_Client_Client
{
    public function doRestartHomeDelivery($salesOrderNumber, $deliveryId, $oldDeliveryID)
    {
        $params = array(
            'SalesOrderNumber' => $salesOrderNumber,
            'DeliveryID' => $deliveryId,
            'OldDeliveryID' => $oldDeliveryID,
        );

        return $this->RestartHomeDelivery($params);
    }
}