<?php

class Vznl_Service_Model_Client_PrioritizeClient extends Vznl_Service_Model_Client_Client
{
    /**
     * @param $id
     * @return mixed
     */
    public function triggerPorting($id)
    {
        $params = [
            'LogicalPackageID' => $id
        ];

        return $this->Porting($params);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function triggerOrderValidation($id)
    {
        $params = [
            'LogicalPackageID' => $id
        ];

        return $this->ValidateOrder($params);
    }
}
