<?php

class Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem extends Mage_Core_Model_Abstract
{
    const PACKAGE_LINE_STATE_CANCEL = 'Cancel';
    const PACKAGE_LINE_STATE_NOTHING = 'Nothing';
    const PACKAGE_LINE_STATE_RESERVE = 'Reserve';
    const PACKAGE_LINE_STATE_RETURN = 'Return';
    const PACKAGE_LINE_STATE_CHANGE= 'Change';

    const PAYMENT_STATE_ADD = 'Add';
    const PAYMENT_STATE_CHANGE = 'Change';
    const PAYMENT_STATE_NOTHING = 'Nothing';
    const PAYMENT_STATE_CANCEL = 'Cancel';

    private $_sku = '';
    private $_prodId = '';
    private $_type = ''; // SIM, MSISDN or IMEI
    private $_reference = ''; // Reference data, could be SIM, MSISDN or IMEI
    private $_oldPackageLineItem = null; // The previous item
    private $_packageLineItem; // the current item
    private $_totals = null;
    private $_itemDoa = false;
    private $_parentPackage = null;
    private $_axiState = ''; // Cancel, Nothing, Reserve
    private $_natureCode = '';
    private $_productInstance = null;
    private $_packageId = null;
    private $_weeeTax = null;
    private $_deviceRCCumulated = null;
    private $_mixmatchSubtotal = null;
    private $_mixmatchTax = null;
    private $_isAikido = false;
    private $_deviceRCZero = false;
    private $_basket_item_id = null;
    private $_offer_id = null;
    private $_action = null;
    private $_bom_id = null;
    private $_product_name = null;
    private $_offer_type = '12121';
    private $_hardware_name = null;
    private $_serial_number = null;

    public function getTkhInc()
    {
        return isset($this->_weeeTax['incl_tax']) ? $this->_weeeTax['incl_tax'] : 0;
    }

    public function getTkhEx()
    {
        return isset($this->_weeeTax['excl_tax']) ? $this->_weeeTax['excl_tax'] : 0;
    }

    public function getTechnicalCodes($product = null)
    {
        if ($product) {
            /** @var Vznl_Catalog_Model_Product _productInstance */
            $this->_productInstance = Mage::getModel('catalog/product')->loadByAttribute('sku', $product);
        }
        $td = '';
        if ($this->_productInstance->getData('identifier_ean_or_upc_code')) {
            $td .= $this->_productInstance->getData('identifier_ean_or_upc_code') . ',';
        }
        if ($this->_productInstance->getData('identifier_sap_codes')) {
            $td .= $this->_productInstance->getData('identifier_sap_codes') . ',';
        }
        if ($this->_productInstance->getData('prodspecs_wms_codes')) {
            $td .= $this->_productInstance->getData('prodspecs_wms_codes') . ',';
        }

        $codes = array_map('trim', explode(',', $td));
        if ($this->_productInstance->getIsDeleted()) {
            // In case the product was deleted, make sure the data is sanitised and the "|" is removed
            foreach ($codes as &$code) {
                if (strpos($code, "|") !== false) {
                    $code = explode("|", $code)[0];
                }
            }
            unset($code);
        }

        return join(',', array_filter($codes));
    }

    /**
     * @return bool
     */
    public function isSubscription()
    {
        return Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $this->_productInstance);
    }

    /**
     * @return bool
     */
    public function isDevice()
    {
        return Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE), $this->_productInstance);
    }

    /**
     * @return bool
     */
    public function isPakket()
    {
        return Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE), $this->_productInstance);
    }

    /**
     * @return bool
     */
    public function isPakketDetails()
    {
        return Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS), $this->_productInstance);
    }

    /**
     * @return bool
     */
    public function isDeviceRCZero()
    {
        return $this->_deviceRCZero;
    }

    /**
     * @return bool
     */
    public function isAikido()
    {
        return $this->_isAikido;
    }

    /**
     * @return bool
     */
    public function isDeviceSubscription()
    {
        return Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION), $this->_productInstance);
    }

    /**
     * @return float
     */
    public function getDeviceRCCumulated()
    {
        return $this->_deviceRCCumulated;
    }

    /**
     * @return Vznl_Catalog_Model_Product
     */
    public function getProductInstance()
    {
        return $this->_productInstance;
    }

    /**
     * @return bool
     */
    public function isReturnable()
    {
        return !$this->_productInstance->isSim();
    }

    /**
     * @return bool
     */
    public function getItemDoa()
    {
        return $this->_itemDoa;
    }

    /**
     * @return bool
     */
    public function hasReference()
    {
        return (!empty($this->_reference) || $this->getReferenceType() !== null);
    }

    /**
     * @return string
     */
    public function getReferenceType()
    {
        return $this->_type;
    }

    /**
     * Retrieve the Sku of the target promo product
     *
     * @return bool
     */
    public function getTargetSku()
    {
        $targetId = $this->getPackageLineItem()->getTargetId();
        if (!$targetId || !$this->getProductInstance()->isPromo()) {
            return false;
        }
        $product = Mage::getModel('catalog/product')->load($targetId);

        return $product->getSku();
    }

    /**
     * @return mixed
     */
    public function getPackageLineItem()
    {
        return $this->_packageLineItem;
    }

    /**
     * @param mixed $packageLineItem
     * @return $this
     */
    public function setPackageLineItem($packageLineItem)
    {
        $this->_packageLineItem = $packageLineItem;

        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->_reference;
    }

    /**
     * @return string
     */
    public function getNatureCode()
    {
        return $this->_natureCode;
    }

    /**
     * @return string
     */
    public function getAxiState()
    {
        return $this->_axiState;
    }

    /**
     * @return array
     */
    public function getTotals()
    {
        return $this->_totals;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->_sku;
    }

    /**
     * @return int
     */
    public function getPackageId()
    {
        return $this->_packageId;
    }

    /**
     * @return float|null
     */
    public function getMixmatchSubtotal()
    {
        return $this->_mixmatchSubtotal;
    }

    /**
     * @return float|null
     */
    public function getMixmatchTax()
    {
        return $this->_mixmatchTax;
    }

    /**
     * @param $item
     */
    public function setOldPackageItem($item)
    {
        $this->_oldPackageLineItem = $item;
    }

    /**
     * Checks whether the line item has changed (sku wise)
     * @return bool <true> if changed, <false> if either a new line item or old same line item has the same sku.
     */
    public function hasChanged()
    {
        if($oldLineItem = $this->getOldLineItem()) {
            // Check if the current sku is different then the old sku
            return ($oldLineItem->getSku() !== $this->getSku());
        } else {
            // No old line item found, meaning that this is a new line
            return false;
        }
    }

    /**
     * @return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem|null
     */
    public function getOldLineItem() {
        return $this->_oldPackageLineItem;
    }

    public function parseAsTkhServiceItem($lineItem, $oldPackageLineItem, $parentPackage)
    {
        if ($lineItem->getTkhInc() == 6.05) { // TKH high
            // We add a new line item with service article SKU 4600037
            // TODO refactor to setting
            $this->_sku = '4600037';
            $this->_totals['total'] = 6.05;
            $this->_totals['subtotal_price'] = 5.00;
        } elseif ($lineItem->getTkhInc() == 3.03) { // TKH low
            // We add a new line item with service article SKU 4600036
            // TODO refactor to setting
            $this->_sku = '4600036';
            $this->_totals['total'] = 3.03;
            $this->_totals['subtotal_price'] = 2.50;
        } else {
            throw new Exception("Unsupported thuiskopieheffing amount: " . $lineItem->getTkhInc());
        }
        $this->_productInstance = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->_sku);
        $this->_totals['subtotal_maf'] = 0;
        $this->_totals['total_maf'] = 0;
        $this->_totals['tax_maf'] = 0;
        $this->_oldPackageLineItem = $oldPackageLineItem; // The previous item, TODO refactor, this could brake things
        $this->_packageId = $lineItem->getPackageId();
        $this->_parentPackage = $parentPackage;
        $this->_natureCode = 'tkh';
    }

    private function _setNatureCode()
    {
        $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
        $attributeSetModel->load($this->_productInstance->getAttributeSetId());
        $attributeSetName = $attributeSetModel->getAttributeSetName();
        if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_NONE), $this->_productInstance) && $this->_productInstance->isServiceItem()
        ) {
            $attributeSetName = 'tkh';
        }
        $this->_natureCode = $attributeSetName;
    }

    /**
     * @param Vznl_Checkout_Model_Sales_Order_Item $packageLineItem
     * @param $oldPackageLineItem
     * @param $packageModel
     * @param $parentPackage
     */
    public function parse($packageLineItem, $oldPackageLineItem, $packageModel, $parentPackage, $productInstance = null)
    {
        $this->_parentPackage = $parentPackage;
        $this->_packageId = $packageLineItem->getPackageId();
        $this->_weeeTax['incl_tax'] = $packageLineItem->getFixedProductTaxInclVat();
        $this->_weeeTax['excl_tax'] = $packageLineItem->getWeeeTaxAppliedAmount();
        $this->_oldPackageLineItem = $oldPackageLineItem;
        $this->_packageLineItem = $packageLineItem;
        $this->_sku = $packageLineItem->getSku();
        $this->_prodId = $packageLineItem->getProductId();
        $this->_mixmatchSubtotal = $packageLineItem->getMixmatchSubtotal() - ($packageLineItem->getDiscountAmount() - $packageLineItem->getHiddenTaxAmount());
        $this->_mixmatchTax = $packageLineItem->getMixmatchTax() - $packageLineItem->getHiddenTaxAmount();
        $this->_basket_item_id = $packageLineItem->getBasketItemId();
        $this->_offer_id = $packageLineItem->getOfferId();
        $this->_action = $packageLineItem->getAction();
        $this->_bom_id = $packageLineItem->getBomId();
        $this->_product_name = $packageLineItem->getProductName();
        $this->_offer_type = $packageLineItem->getOfferType();
        $this->_serial_number = $packageLineItem->getSerialNumber();
        $this->_hardware_name = $packageLineItem->getHardwareName();

        $this->_itemDoa = false;

        $doaItems = Mage::getSingleton('checkout/session')->getCancelDoaItems();
        // Get DOA items from checkout/session
        if ($doaItems) {
            $tempId = ($packageModel->getOldPackageId() && !$packageModel->getNewPackageId()) ? $packageModel->getOldPackageId() : $this->_packageId;
            $doaItems = isset($doaItems[$tempId]) ? unserialize($doaItems[$tempId]) : [];
            if (count($doaItems)) {
                if (in_array($this->_sku, $doaItems)) {
                    $this->_itemDoa = true;
                }
            }
        }

        /** @var Vznl_Checkout_Helper_Aikido $aikidoHelper */
        $aikidoHelper = Mage::helper('vznl_checkout/aikido');
        if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION), $productInstance)) {
            if (
                $packageLineItem->getSku() == $aikidoHelper->getDefaultAcquisitionSku()
                || $packageLineItem->getSku() == $aikidoHelper->getDefaultRetentionSku()
            ) {
                $this->_deviceRCZero = true;
            } else {
                $this->_deviceRCZero = false;
            }
            $this->_deviceRCCumulated = $packageLineItem->getTotalPaidByMaf();
            $this->_isAikido = true;
        }

        if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE), $productInstance)) {
            /** @var Vznl_Checkout_Model_Sales_Order_Item $sibling */
            foreach ($packageLineItem->getOrder()->getAllItems() as $sibling) {
                if (
                    Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION), $sibling->getProduct())
                    && $sibling->getPackageId() == $packageLineItem->getPackageId()
                ) {
                    if (
                        $sibling->getSku() == $aikidoHelper->getDefaultAcquisitionSku()
                        || $sibling->getSku() == $aikidoHelper->getDefaultRetentionSku()
                    ) {
                        $this->_deviceRCZero = true;
                    } else {
                        $this->_deviceRCZero = false;
                    }
                    $this->_deviceRCCumulated = $sibling->getTotalPaidByMaf();
                    $this->_isAikido = true;
                }
            }
        }

        $this->_productInstance = $productInstance !== null ?
            $productInstance : Mage::getModel('catalog/product')->loadByAttribute('sku', $this->_sku);
        if (!$this->_productInstance) {
            // If the sku is not found, check if the product can be found between the deleted products
            $tempProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->_sku . "|" . $this->_prodId);
            if ($tempProduct && $tempProduct->getId()) {
                $this->_productInstance = $tempProduct;
            }
        }
        if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $productInstance)
            && (Vznl_Service_Model_Client_EbsOrchestrationClient_Package::_lineItemHasAttributeValue($packageLineItem,
                    Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK, Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK_VODAFONE)
                || Vznl_Service_Model_Client_EbsOrchestrationClient_Package::_lineItemHasAttributeValue($packageLineItem,
                    Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK, Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK_HOLLANDSNIEUWE)
            )
        ) {
            $this->_reference = $packageModel->getTelNumber() ?: ($packageModel->getCurrentNumber() ?: ($packageModel->getCtn() ?: null));
            $this->_type = 'MSISDN';
        } elseif ($packageLineItem->getProduct()->isSim()) {
            $this->_reference = $packageModel->getSimNumber();
            if ($packageLineItem->getProduct()->isHollandsNieuweNetworkProduct()) {
                $this->_type = 'SIMHN';
            } else {
                $this->_type = 'SIM';
            }
        } elseif (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE), $packageLineItem->getProduct())) {
            $this->_reference = $packageModel->getImei();
            $this->_type = 'IMEI';
        }
        $this->_totals = $packageModel->getPackageTotals(array($packageLineItem), false);

        // Add the price to temporary table
        $totalHistory = ($this->_isAikido && !$this->_deviceRCZero && $this->isDevice()) ? $this->_totals['total'] + $this->_deviceRCCumulated : $this->_totals['total'];
        if ($oldPackageLineItem) {
            Mage::helper('vznl_service/price')->addPrice('new', $this->_sku, $this->_packageId, $totalHistory);
        } else {
            Mage::helper('vznl_service/price')->addPrice('old', $this->_sku, $this->_packageId, $totalHistory);
        }

        $this->_setNatureCode();
    }

    /**
     * Checks whether this package item should be send as payment.
     * @return bool <true> if changed according to the old line item or new and device rc not zero, <false> if neither
     */
    public function shouldSendAsPayment()
    {
        $shouldShowDeviceRc = false;
        $packageId = $this->getPackageId();
        $deliveryOrder = $this->_parentPackage->getDeliveryOrder();
        if(Mage::helper('vznl_package')->packageEverHadADeviceRc($packageId, $deliveryOrder)) {
            $shouldShowDeviceRc = true;
        }

        return ($shouldShowDeviceRc || !$this->isDeviceRCZero());
    }

    /**
     * @return string
     */
    public function getBasketItemId()
    {
        return $this->_basket_item_id;
    }

    /**
     * @return string
     */
    public function getOfferId()
    {
        return $this->_offer_id;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->_action;
    }

    /**
     * @return string
     */
    public function getBomId()
    {
        return $this->_bom_id;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->_product_name;
    }

    /**
     * @return string
     */
    public function getOfferType()
    {
        return $this->_offer_type;
    }

    /**
     * @return string
     */
    public function getHardwareName()
    {
        return $this->_hardware_name;
    }

    /**
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->_serial_number;
    }
}
