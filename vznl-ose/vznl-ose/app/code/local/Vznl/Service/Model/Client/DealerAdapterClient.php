<?php

/**
 * Class Vznl_Service_Model_Client_DealerAdapterClient
 */
class Vznl_Service_Model_Client_DealerAdapterClient extends Vznl_Service_Model_Client_Client
{
    const ORDER_STATUS = 'OrderStatus';
    const CREDIT_CHECK_STATUS = 'CreditCheckStatus';
    const NUMBER_PORTABILITY_STATUS = 'NumberPortabilityStatus';
    const ENV_VHOST_CODE = 'MAGE_ENV_CODE';
    protected $ns;

    /**
     * To retrieve information from a customer, the dealer will have to provide the following parameters in the request:
     *
     * Consumer customer: BAN + Date of Birth
     * Business customer: BAN + KvK number
     *
     * If the dealer special code is given, BSL will only validate a BAN with that dealer special code
     *
     * Exceptions:
     * Business customer ≤ 3 MSISDNs: CTN + KvK number <or>  BAN + KvK number
     * Government customer: BAN
     *
     * If a customer has blocked the BAN with a password, then this password needs to be entered in element <pin>.
     *
     * @param Mage_Customer_Model_Customer $customer optional
     * @param string $dealerSpecialCode optional
     * @return mixed
     * @throws \Exception
     */
    public function getCreditProfile(Mage_Customer_Model_Customer $customer, string $dealerSpecialCode = null)
    {
        if ($customer->getIsBusiness()) {
            $params = [
                'DealerCode' => $dealerSpecialCode ?? Mage::helper('agent')->getDaDealerCode(),
                'ID' => $customer->getBan(),
                'PIN' => function (array $node, $ns) use ($customer) {
                    if ($customer->getPin()) {
                        $node[0][0] = $customer->getPin();
                    } else {
                        unset($node[0][0]);
                    }
                    $this->ns = $ns;
                },
                'ValidationCriterial' => function (array $node, $ns) use ($customer) {
                    $children = $node[0]->children();
                    foreach ($children as $c) {
                        $node[0]->removeChild($c);
                    }
                    $kvk = $customer->getCompanyCoc();
                    if ($kvk != '') {
                        $organization = $node[0]->addChild('Organization');
                        $organization->addChild('PartyId', $kvk, 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                    } else {
                        $node[0][0] = '';
                    }
                    $this->ns = $ns;
                },
                'Individual' => function (array $node, $ns) {
                    unset($node[0][0]);
                    $this->ns = $ns;
                }
            ];
        } else {
            $dob = new DateTime($customer->getData('dob') instanceof DateTime ? $customer->getData('dob')->format('c') : $customer->getData('dob'));
            $params = [
                'DealerCode' => $dealerSpecialCode ?? Mage::helper('agent')->getDaDealerCode(),
                'ID' => $customer->getBan(),
                'PIN' => function (array $node, $ns) use ($customer) {
                    if ($customer->getPin()) {
                        $node[0][0] = $customer->getPin();
                    } else {
                        unset($node[0][0]);
                    }
                    $this->ns = $ns;
                },
                'ValidationCriterial' => function ($node, $ns) use ($customer, $dob) {
                    $children = $node[0]->children();
                    foreach ($children as $c) {
                        $node[0]->removeChild($c);
                    }
                    $individual = $node[0]->addChild('Individual');
                    $alive = $individual->addChild('AliveDuring', null, 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                    $alive->addChild('StartDateTime', $dob->format('c'), 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                    $this->ns = $ns;
                },
            ];
        }
        $params['TransactionId'] = uniqid();
        $params['Source'] = $this->_getSourceCodeVZ();
        $params['UserId'] = Mage::getSingleton('checkout/session')->getAgent(true) && Mage::getSingleton('checkout/session')->getAgent()->getId() ? Mage::getSingleton('checkout/session')->getAgent()->getUsername() : '';
        return $this->RetrieveCustomerCreditProfile($params, $customer->getIsBusiness());
    }

    protected function _getSourceCodeVZ()
    {
    	$sourceArray = $_SERVER;
    	$source = isset($sourceArray[self::ENV_VHOST_CODE]) && $sourceArray[self::ENV_VHOST_CODE]
    	    ? $sourceArray[self::ENV_VHOST_CODE]
    	    : Mage::helper('dyna_service')->getGeneralConfig('default_source_value');
    	return $source;
    }
    
    /**
     * To retrieve information from a customer, the dealer will have to provide the following parameters in the request:
     *
     * Consumer customer: CTN + Date of Birth
     * Business customer: CTN + KvK number
     *
     * If a customer has blocked the BAN with a password, then this password needs to be entered in element <pin>.
     *
     * @param array $data
     * @return mixed
     */
    public function getCreditProfileByCtn(array $data)
    {
        $customer = Mage::getModel('customer/customer')
            ->getCollection()
            ->addAttributeToSelect('pin')
            ->addFieldToSelect('ban')
            ->addAttributeToFilter('customer_ctn', array('like' => $data['ctn']))->load()->getLastItem();
    
        if ($data['isBusiness']) {
            $customerKvk = $data['kvk'];
            $params = [
                'DealerCode' => Mage::helper('agent')->getDaDealerCode(),
                'ID' => $customer->getBan(),
                'PIN' => function (array $node, $ns) use ($customer) {
                    if ($customer->getPin()) {
                        $node[0][0] = $customer->getPin();
                    } else {
                        unset($node[0][0]);
                    }
                    $ns = $ns;
                },
                'ValidationCriterial' => function (array $node, $ns) use ($customerKvk) {
                    $children = $node[0]->children();
                    $this->ns = $ns;
                    foreach ($children as $c) {
                        $node[0]->removeChild($c);
                    }
                    $kvk = $customerKvk;
                    if ($kvk != '') {
                        $organization = $node[0]->addChild('Organization');
                        $organization->addChild('PartyId', $kvk, 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                    } else {
                        $node[0][0] = '';
                    }
                },
                'Individual' => function (array $node, $ns) {
                    unset($node[0][0]);
                    $this->ns = $ns;
                },
                'Subscription/Ctn' => $data['ctn'],
            ];
        } else {
            $dob = new DateTime($data["dob"] instanceof DateTime ? $data["dob"]->format('c') : $data["dob"]);
            $params = [
                'DealerCode' => Mage::helper('agent')->getDaDealerCode(),
                'ID' => $customer->getBan(),
                'PIN' => function (array $node, $ns) use ($customer) {
                    if ($customer->getPin()) {
                        $node[0][0] = $customer->getPin();
                    } else {
                        unset($node[0][0]);
                    }
                    $ns = $ns;
                },
                'ValidationCriterial' => function ($node, $ns) use ($dob) {
                    $children = $node[0]->children();
                    $this->ns = $ns;
                    foreach ($children as $c) {
                        $node[0]->removeChild($c);
                    }
                    $individual = $node[0]->addChild('Individual');
                    $alive = $individual->addChild('AliveDuring', null, 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                    $alive->addChild('StartDateTime', $dob->format('c'), 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                },
                'Ctn' => $data['ctn'],
            ];
        }
        $params['TransactionId'] = uniqid();
        $params['Source'] = $this->_getSourceCodeVZ();
        $params['UserId'] = '';

        return $this->RetrieveCustomerCreditProfile($params, $data['isBusiness']);
    }

    /**
     * To retrieve information from a customer, the dealer will have to provide the following parameters in the request:
     *
     * Consumer customer (advanced search): MSISDN + Date of Birth
     * Consumer customer: BAN + Date of Birth
     * Business customer: BAN + KvK number
     *
     * Exceptions:
     * Business customer ≤ 3 MSISDNs : CTN + KvK number <or>  BAN + KvK number
     * Government customer: BAN
     *
     * If a customer has blocked the BAN with a password, then this password needs to be entered in element <pin>.
     *
     * @param Mage_Customer_Model_Customer $customer
     * @param bool $additionalInfo
     * @return mixed
     * @throws Exception
     */
    public function getCustomerInfo(Mage_Customer_Model_Customer $customer, $additionalInfo = true)
    {
        $customerCtn = $customer->getCustomerCtn();
        if ($customerCtn && $customerCtn->getId()) {
            $primaryCtn = $customer->getCustomerCtn()->getCtn();
        } else {
            $primaryCtn = Mage::helper('vznl_customer')->parseCtn($customer->getData('customer_ctn'));
        }
        if ($customer->getIsBusiness()) {
            $params = [
                'DealerCode' => Mage::helper('agent')->getDaDealerCode(),
                'ID' => $customer->getBan(),
                'PIN' => function ($node, $ns) use ($customer) {
                    if ($customer->getPin()) {
                        $node[0][0] = $customer->getPin();
                    } else {
                        unset($node[0][0]);
                    }
                    $this->ns = $ns;
                },
                'ValidationCriterial' => function ($node, $ns) use ($customer) {
                    $children = $node[0]->children();
                    foreach ($children as $c) {
                        $node[0]->removeChild($c);
                    }
                    $kvk = $customer->getCompanyCoc() == null ? 0 : $customer->getCompanyCoc();
                    if ($kvk != '') {
                        $organization = $node[0]->addChild('Organization');
                        $organization->addChild('PartyId', $kvk, 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                    } else {
                        $node[0][0] = '';
                    }
                    $this->ns = $ns;
                },
                'Subscription/Ctn' => $primaryCtn,
            ];
        } else {
            $dob = new DateTime($customer->getData('dob') instanceof DateTime ? $customer->getData('dob')->format('c') : $customer->getData('dob'));
            $params = [
                'DealerCode' => Mage::helper('agent')->getDaDealerCode(),
                'ID' => $customer->getBan(),
                'PIN' => function ($node, $ns) use ($customer) {
                    if ($customer->getPin()) {
                        $node[0][0] = $customer->getPin();
                    } else {
                        unset($node[0][0]);
                    }
                    $this->ns = $ns;
                },
                'ValidationCriterial' => function ($node, $ns) use ($customer, $dob) {
                    $children = $node[0]->children();
                    foreach ($children as $c) {
                        $node[0]->removeChild($c);
                    }
                    $individual = $node[0]->addChild('Individual');
                    $alive = $individual->addChild('AliveDuring', null, 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                    $alive->addChild('StartDateTime', $dob->format('c'), 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                    $this->ns = $ns;
                },
            ];

            // Advanced search: MSISDN + Date of Birth
            if ($customer->getAdvancedSearch()) {
                $params['ID'] = function ($node, $ns) {
                    unset($node[0][0]);
                    $this->ns = $ns;
                };
                $params['Subscription/Ctn'] = $primaryCtn;
            }
        }
        if ($additionalInfo) {
            $params['AdditionalDetailsNeeded'] = 1;
        }
        $params['TransactionId'] = uniqid();

        $params['Source'] = $this->_getSourceCodeVZ();
        $params['UserId'] = Mage::getSingleton('checkout/session')->getAgent(true) && Mage::getSingleton('checkout/session')->getAgent()->getId() ? Mage::getSingleton('checkout/session')->getAgent()->getUsername() : '';
        try {
            return $this->RetrieveCustomerInfo($params, $customer->getIsBusiness());
        } catch (Exception $e) {
            if ($e->getCode() == -4 && $e->responseCode == 'VOD-12-015-000') {
                $params['AdditionalDetailsNeeded'] = 0;
                $result = $this->RetrieveCustomerInfo($params, $customer->getIsBusiness());
                $subs = $result['party_end_user']['subscription'];
                $result['party_end_user']['subscription'] = [];
                $count = 0;
                $step = (int)Mage::helper('dyna_service')->getDealerAdapterConfig('customer_info_threshold');
                $adapter = Mage::getModel('core/resource')->getConnection('read');
                // Ensure minimum is 5, if left blank in backend
                if ($step <= 0) {
                    $step = 5;
                }
                while ($count < count($subs)) {
                    $subsNew = [];
                    for ($i = $count; $i < $count + $step; ++$i) {
                        if (isset($subs[$i]['ctn'])) {
                            $subsNew[] = $subs[$i]['ctn'];
                        }
                    }
                    $params['AdditionalDetailsNeeded'] = 1;
                    $params['EndUserDetailsToRetrieve'] = function ($node, $ns) use ($subsNew) {
                        /** @var Omnius_Core_Model_SimpleDOM $detailsToRetrieve */
                        $detailsToRetrieve = $node[0];
                        $this->ns = $ns;
                        $count = count($detailsToRetrieve->children());
                        for ($i = 0; $i < $count; ++$i) {
                            $c = $detailsToRetrieve->children();
                            $detailsToRetrieve->removeChild($c);
                        }
                        foreach ($subsNew as $subsCtn) {
                            $detailsToRetrieve->addChild('Subscription')
                                ->addChild('ID', $subsCtn, 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                        }
                    };
                    $count += $step;
                    try {
                        $adapter->fetchOne('SELECT 1;');
                        $data = $this->RetrieveCustomerInfo($params, $customer->getIsBusiness());
                        $flippedSubsNew = array_flip($subsNew);
                        foreach ($data['party_end_user']['subscription'] as $sub) {
                            $ctn = $sub['ctn'];
                            if (!isset($sub['ctn']) || !isset($flippedSubsNew[$ctn])) {
                                continue;
                            }
                            $result['party_end_user']['subscription'][] = $sub;
                        }
                    } catch (Exception $e) {
                        throw $e;
                    }
                }
                return $result;
            } else {
                throw $e;
            }
        }
    }

    /**
     * @param string|array $orderIdentifier
     * @return mixed
     */
    public function cancelSuperOrder($orderIdentifier)
    {
        /** @var Vznl_Agent_Model_Agent $agent */
        $agent = Mage::getSingleton('customer/session')->getAgent(true) && Mage::getSingleton('customer/session')->getAgent(true)->getId() ? Mage::getSingleton('customer/session')->getAgent(true) : Mage::getModel('agent/agent');

        return $this->CancelOrder([
            'OrderIdentifier' => $orderIdentifier,
            'TransactionId' => uniqid(),
            'UserId' => $agent->getUsername(),
            'DealerCode' => $agent->getDealerCode(),
        ]);
    }

    /**
     * @param string $orderIdentifier
     * @return mixed
     */
    public function getOrderDetails($orderIdentifier)
    {
        return $this->RetrieveOrderDetails([
            'OrderIdentifier' => $orderIdentifier,
            'TransactionId' => uniqid(),
            'UserId' => Mage::getSingleton('customer/session')->getAgent(true) && Mage::getSingleton('customer/session')->getAgent(true)->getId() ? Mage::getSingleton('customer/session')->getAgent(true)->getUsername() : '',
        ]);
    }

    /**
     * @param string $type Accepts "MSISDN" (mobile numbers) or "PSTN" (land line number)
     * @param null $number
     * @param null $areaCode
     * @return mixed
     */
    public function getAvailableResource($type = 'MSISDN', $number = null, $areaCode = null, $pool = null)
    {
        $source = $this->_getSourceCodeVZ();
        return $this->SearchAvailableLogicalResources(
            is_array($type)
                ? $type
                : [
                'DealerCode' => Mage::helper('agent')->getDaDealerCode(),
                'LogicalResource/logicalResPhoneNumber' => $number,
                'LogicalResource/logicalResourceType' => $type,
                'LogicalResource/logicalResourceAreaCode' => $areaCode,
                'LogicalResource/logicalResourcePool' => $pool,
                'TransactionId' => uniqid(),
                'Source' => $source,
                'Category' => $this->getNumberCategory($number),
                'UserId' => Mage::getSingleton('checkout/session')->getAgent(true) && Mage::getSingleton('checkout/session')->getAgent()->getId() ? Mage::getSingleton('checkout/session')->getAgent()->getUsername() : '',
            ]
        );
    }

    /**
     * Get the correct number type for 097/06 numbers
     * @param $number
     * @return string
     */
    protected function getNumberCategory($number)
    {
        if (substr($number, 0, 4) == '3197' || substr($number, 0, 3) == '097') {
            return '097';
        } else {
            return '06';
        }
    }

    /**
     * Can search by one status type only (Order, CreditCheck and NumberPortability), not combined
     * Accepts only a status found in the $validStatuses array
     * OrderStatus: IN-CAPTURE, CANCELLED, IN-PROCESS, COMPLETE
     * CreditCheckStatus: REFERRED, ADDITIONAL_INFO_REQUIRED, REJECTED, APPROVED
     * NumberPortabilityStatus: VALIDATION-PENDING, VALIDATION-FAILURE, VALIDATION-SUCCESS, EXPIRED,
     *                          DEALER-CANCELLED, CONFIRMED, NP_PENDING, NP-COMPLETE, COMPLETE
     *
     * @param $status
     * @param $type
     * @param null|string|DateTime $startDate
     * @param null|string|DateTime $endDate
     * @return mixed
     */
    public function getOrders($status, $type, $startDate = null, $endDate = null)
    {
        $validStatuses = [
            'IN-CAPTURE',
            'CANCELLED',
            'IN-PROCESS',
            'COMPLETE',
            'REFERRED',
            'ADDITIONAL_INFO_REQUIRED',
            'REJECTED',
            'APPROVED',
            'VALIDATION-PENDING',
            'VALIDATION-FAILURE',
            'VALIDATION-SUCCESS',
            'EXPIRED',
            'DEALER-CANCELLED',
            'CONFIRMED',
            'NP_PENDING',
            'NP-COMPLETE',
        ];

        $acceptedTypes = [
            self::ORDER_STATUS,
            self::CREDIT_CHECK_STATUS,
            self::NUMBER_PORTABILITY_STATUS
        ];

        if (!in_array($status, $validStatuses)) {
            Mage::throwException(sprintf('Invalid status given ("%s"). Must be one of: %s', $status, join(', ', $validStatuses)));
        }

        if (!in_array($type, $acceptedTypes)) {
            Mage::throwException(sprintf('Invalid type given ("%s"). Must be one of: %s', $type, join(', ', $acceptedTypes)));
        }

        $params = [
            $type => $status,
            'StartDate' => $startDate
                ? ($startDate instanceof DateTime
                    ? $startDate->format('c')
                    : date('c', strtotime($startDate)))
                : null,
            'EndDate' => $endDate
                ? ($endDate instanceof DateTime
                    ? $endDate->format('c')
                    : date('c', strtotime($endDate)))
                : null,
        ];

        return $this->SearchOrders($params);
    }

    /**
     * Get customer details (including BAN) for legacy customers
     *
     * @param string $username
     * @return array
     */
    public function retrieveAuthorizationDetails($username)
    {
        $source = $this->_getSourceCodeVZ();
        $params = ['UserName' => $username, 'Source' => $source];
        return $this->RetrieveAuthorization($params);
    }

    protected function _validateResponseCode($response)
    {
        $notAllowedVODCodes = [
            "DYNA-100002",
            "DYNA-100006",
            "DYNA-100029",
        ];
        $agent = Mage::getSingleton('customer/session')->getAgent(true);
        if ($agent && $agent->canManuallyActivate() && !empty($response['status']['code']) && in_array($response['status']['code'], $notAllowedVODCodes)) {
            $e = new LogicException('manual override');
            $e->logSoapException = $this->logSoapException;
            throw $e;
        }
    }
}
