<?php

/**
 * Class Vznl_Service_Model_Mapper_CustomerInfoMapper
 */
class Vznl_Service_Model_Mapper_CustomerInfoMapper extends Omnius_Service_Model_Mapper_AbstractMapper
{
    const ADDRESS_TYPE_C = "C";
    const ADDRESS_TYPE_LO = "LO";
    const ADDRESS_TYPE_DL = "DL";
    const ADDRESS_TYPE_LEGAL = "L";
    const ADDRESS_TYPE_USER = "U";
    const ADDRESS_TYPE_BILLING = "B";
    const ADDRESS_TYPE_SERVICE = "S";
    const ADDRESS_TYPE_SHIPPING = "SH";
    const ADDRESS_TYPE_OTHER = "O";
    /**
     * Maps the data on the given object and returns it
     *
     * @param array $response
     * @param Varien_Object $object
     * @return mixed
     */
    public function map(array $response, Varien_Object $object)
    {
        if (!$this->isSupported($object)) {
            Mage::throwException(sprintf('Object (%s) not supported by the %s mapper.', get_class($object), __CLASS__));
        }
        return $this->_map($response, $object);
    }

    /**
     * @param array $response
     * @param Mage_Customer_Model_Customer $customer
     * @return Varien_Object
     */
    protected function _map(array $response, Mage_Customer_Model_Customer $customer)
    {
        $isBusiness = isset($response['is_business']) && $response['is_business'] ? true : false;
        $cityName = null;
        $postalZone = null;
        $buildingNumber = null;
        $StreetName = null;
        $country = null;
        $buildingNumberAddition = null;
        $addressFound = false;
        foreach ($response['postal_addresses'] ?? [] as $addkey => $addvalue) {
            if ($addvalue['AddressTypeCode']=='C') {
                $addressFound = true;
                $cityName = isset($addvalue['CityName']) ? $addvalue['CityName'] : null;
                $postalZone = isset($addvalue['PostalZone']) ? $addvalue['PostalZone'] : null;
                $buildingNumber = isset($addvalue['BuildingNumber']) ? $addvalue['BuildingNumber'] : null;
                $StreetName = isset($addvalue['StreetName']) ? $addvalue['StreetName'] : null;
                $country = isset($addvalue['Country']['IdentificationCode']) ? $addvalue['Country']['IdentificationCode'] : null;
                $buildingNumberAddition = isset($addvalue['HouseNumberAddition']) ? $addvalue['HouseNumberAddition'] : null;
            }
        }
        if (!$addressFound) {
            foreach ($response['postal_addresses'] ?? [] as $addkey=>$addvalue) {
                if ($addvalue['AddressTypeCode'] == 'B') {
                    $cityName=isset($addvalue['CityName']) ? $addvalue['CityName'] : null;
                    $postalZone=isset($addvalue['PostalZone']) ? $addvalue['PostalZone'] : null;
                    $buildingNumber=isset($addvalue['BuildingNumber']) ? $addvalue['BuildingNumber'] : null;
                    $StreetName=isset($addvalue['StreetName']) ? $addvalue['StreetName'] : null;
                    $country=isset($addvalue['Country']['IdentificationCode']) ? $addvalue['Country']['IdentificationCode'] : null;
                    $buildingNumberAddition=isset($addvalue['HouseNumberAddition']) ? $addvalue['HouseNumberAddition'] : null;
                }
            }
        }

        $properties = array(
            'set_additional_email' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $email = isset($response['mobile_email']) ? $response['mobile_email'] : null;
                $emails = null;
                if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emails = array_map('trim', explode(';', $email));
                    if ($customer->getEmailOverwritten()) {
                        $oldEmails = explode(';', $customer->getAdditionalEmail());
                        if (!empty($oldEmails[0])) {
                            array_push($emails, $oldEmails[0]);
                        }
                    }
                    $emails = join(';', array_unique($emails));
                }

                if ($customer->getData('additional_email') != $emails) {
                    return $emails;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_gender' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $value = isset($response['title']) ? $response['title'] : null;

                if (empty($value)) {
                    $response = null;
                } elseif ('female' === strtolower($value) || 'mevr' === strtolower($value) || 'mevrouw' === strtolower($value)) {
                    $response = 2;
                } elseif ('male' === strtolower($value) || 'heer' === strtolower($value) || 'meneer' === strtolower($value) || 'dhr.' === strtolower($value)) {
                    $response = 1;
                } elseif ('dhr./mevr.' === strtolower($value) || 'dhr/mevr' === strtolower($value) || 'dhr./mevr' === strtolower($value) || 'dhr/mevr.' === strtolower($value) || 'aan' === strtolower($value) || 'aan.' === strtolower($value)) {
                    $response = 3;
                } else {
                    $response = null;
                }

                if ($customer->getData('gender') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_is_active' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $value = isset($response['PartyChooser']['PartyRole']['Status']) ? $response['PartyChooser']['PartyRole']['Status'] : null;
                if (empty($value)) {
                    $response = 0;
                } elseif ('open' === strtolower($value)) {
                    $response = 1;
                } else {
                    $response = 0;
                }

                if ($customer->getData('is_active') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_firstname' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                $firstname = isset($response['firstname']) ? $response['firstname'] : null;
                if ($isBusiness && empty($firstname)) {
                    $response = $customer['company_name'];
                } else {
                    $response = $firstname;
                }

                if ($customer->getData('firstname') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_prefix' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $gender = isset($response['title']) ? $response['title'] : null;
                if (empty($gender)) {
                    $prefix = '';
                } else {
                    switch (strtolower($gender)) {
                        case 'female':
                        case 'mevr':
                        case 'mevr.':
                        case 'mevrouw':
                            $prefix = 'Mrs.';
                            break;
                        case 'male':
                        case 'heer':
                        case 'meneer':
                        case 'dhr':
                        case 'dhr.':
                            $prefix = 'Mr.';
                            break;
                        case 'dhr./mevr.':
                        case 'dhr/mevr':
                        case 'dhr./mevr':
                        case 'dhr/mevr.':
                        case 'aan':
                        case 'aan.':
                            $prefix = 'Mr./Mrs.';
                            break;
                        default:
                            $prefix = '';
                            break;
                    }
                }

                if ($customer->getData('prefix') != $prefix) {
                    return $prefix;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_lastname' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                $lastname = isset($response['lastname']) ? $response['lastname'] : null;
                if ($isBusiness && empty($lastname)) {
                    $response = isset($customer['company_legal_form']) ? $customer['company_legal_form'] : null;
                } else {
                    $response = $lastname;
                }
                if ($customer->getData('lastname') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_middlename' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $response = isset($response['middlename']) ? $response['middlename'] : null;

                if ($customer->getData('middlename') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contractant_firstname' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['ContactInformation']['FirstName']) ? $response['ContactInformation']['FirstName'] : null;
                    if (trim($response) == '-') {
                        $response = '';
                    }
                } else {
                    $response = null;
                }

                if ($customer->getData('contractant_firstname') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contractant_dob' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['PartyChooser']['AliveDuring']['StartDateTime']) ? $response['PartyChooser']['AliveDuring']['StartDateTime'] : null;
                } else {
                    $response = null;
                }

                if ($customer->getData('contractant_dob') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contractant_prefix' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $gender = isset($response['title']) ? $response['title'] :null;
                    if (empty($gender) || trim($gender) == '-') {
                        $prefix = '';
                    } else {

                        switch (strtolower($gender)) {
                            case 'female':
                            case 'mevr':
                            case 'mevrouw':
                            case 'mevr.':
                                $prefix = '2';
                                break;
                            case 'male':
                            case 'heer':
                            case 'meneer':
                            case 'dhr':
                            case 'dhr.':
                                $prefix = '1';
                                break;
                            case 'dhr./mevr.':
                            case 'dhr/mevr':
                            case 'dhr./mevr':
                            case 'dhr/mevr.':
                            case 'aan':
                            case 'aan.':
                                $prefix = '3';
                                break;
                            default:
                                $prefix = '';
                                break;
                        }
                        return $prefix;
                    }
                } else {
                    $prefix = null;
                }

                if ($customer->getData('contractant_prefix') != $prefix) {
                    return $prefix;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contractant_middlename' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['ContactInformation']['MiddleName']) ? $response['ContactInformation']['MiddleName'] : null;
                } else {
                    $response = null;
                }

                if ($customer->getData('contractant_middlename') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contractant_lastname' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['ContactInformation']['LastName']) ? $response['ContactInformation']['LastName'] : null;
                    if (empty($response)) {
                        $response = $customer['company_legal_form'];
                    }
                    if (trim($response) == '-') {
                        $response = '';
                    }
                } else {
                    $response = null;
                }

                if ($customer->getData('contractant_lastname') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contractant_gender' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $gender = isset($response['title']) ? $response['title'] : null;
                    if (empty($gender)) {
                        $response = null;
                    } elseif ('female' === strtolower($gender) || 'mevr' === strtolower($gender) || 'mevrouw' === strtolower($gender) || 'mevr.' === strtolower($gender)) {
                        $response = 2;
                    } elseif ('male' === strtolower($gender) || 'heer' === strtolower($gender) || 'meneer' === strtolower($gender) || 'dhr.' === strtolower($gender) || 'dhr' === strtolower($gender)) {
                        $response = 1;
                    } elseif ('dhr./mevr.' === strtolower($gender) || 'dhr/mevr' === strtolower($gender) || 'dhr./mevr' === strtolower($gender) || 'dhr/mevr.' === strtolower($gender) || 'aan.' === strtolower($gender) || 'aan' === strtolower($gender)) {
                        $response = 3;
                    } elseif (trim($gender) == '-') {
                        $response = '';
                    }
                } else {
                    $response = null;
                }

                if ($customer->getData('contractant_gender') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contractant_id_type' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['IdentificationInformation']['IdentificationType']) ? $response['IdentificationInformation']['IdentificationType'] : null;
                } else {
                    $response = null;
                }

                if ($customer->getData('contractant_id_type') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contractant_issuing_country' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['IdentificationInformation']['IssuingCountry']) ? $response['IdentificationInformation']['IssuingCountry'] : null;
                } else {
                    $response = null;
                }

                if ($customer->getData('contractant_issuing_country') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contractant_id_number' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['IdentificationInformation']['IdentificationNr']) ? $response['IdentificationInformation']['IdentificationNr'] : null;
                } else {
                    $response = null;
                }

                if ($customer->getData('contractant_id_number') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contractant_valid_until' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['IdentificationInformation']['ValidFor']['EndDateTime']) ? $response['IdentificationInformation']['ValidFor']['EndDateTime'] : null;
                } else {
                    $response = null;
                }

                if ($customer->getData('contractant_valid_until') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_dob' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $value = isset($response['dob']) ? $response['dob'] : null;
                if ($value) {
                    $dob = explode('T', $value);
                    $response = date('Y-m-d H:i:s', strtotime($dob[0]));
                } else {
                    $response = null;
                }

                if ($customer->getData('dob') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_contact_id' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $response = isset($response['PartyChooser']['PartyId']) ? $response['PartyChooser']['PartyId'] : null;

                if ($customer->getData('contact_id') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_valid_until' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $response = isset($response['IdentificationInformation']['ValidFor']['EndDateTime']) ? $response['IdentificationInformation']['ValidFor']['EndDateTime'] : null;

                if ($customer->getData('valid_until') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_issue_date' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $response = isset($response['IdentificationInformation']['IssueDate']) ? $response['IdentificationInformation']['IssueDate'] : null;

                if ($customer->getData('issue_date') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_id_number' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $response =  isset($response['IdentificationInformation']['IdentificationNr']) ? $response['IdentificationInformation']['IdentificationNr'] : null;

                if ($customer->getData('id_number') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_id_type' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $response = isset($response['IdentificationInformation']['IdentificationType']) ? $response['IdentificationInformation']['IdentificationType'] : null;

                if ($customer->getData('id_type') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_issuing_country' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $response = isset($response['IdentificationInformation']['IssuingCountry']) ? $response['IdentificationInformation']['IssuingCountry'] : null;

                if ($customer->getData('issuing_country') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_account_holder' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $response = isset($response['billing_account']['AccountOwnerName']) ? $response['billing_account']['AccountOwnerName'] : null;
                if ($customer->getData('account_holder') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_account_no' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $response = isset($response['billing_account']['ID']) ? $response['billing_account']['ID'] : null;

                if ($customer->getData('account_no') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_company_name' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['company_name']) ? $response['company_name'] : null;
                } else {
                    $response = null;
                }

                if ($customer->getData('company_name') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_company_date' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['PartyChooser']['ValidFor']['StartDateTime']) ? $response['PartyChooser']['ValidFor']['StartDateTime'] : null;
                } else {
                    $response = null;
                }

                if (!empty($response) && $customer->getData('company_date') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_company_coc' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['company_id']) ? $response['company_id'] : null;
                } else {
                    $response = null;
                }

                if ($customer->getData('company_coc') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_company_street' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness, $StreetName) {
                if ($isBusiness) {
                    $response = $StreetName;
                } else {
                    $response = null;
                }

                if ($customer->getData('company_street') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_company_country_id' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = 'NL'; // TODO
                } else {
                    $response = null;
                }

                if ($customer->getData('company_country_id') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_company_house_nr' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness, $buildingNumber) {
                if ($isBusiness) {
                    $response = $buildingNumber;
                } else {
                    $response = null;
                }

                if ($customer->getData('company_house_nr') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_company_city' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness, $cityName) {
                if ($isBusiness) {
                    $response = $cityName;
                } else {
                    $response = null;
                }

                if ($customer->getData('company_city') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_company_postcode' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness, $postalZone) {
                if ($isBusiness) {
                    $response = $postalZone;
                } else {
                    $response = null;
                }

                if ($customer->getData('company_postcode') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_company_legal_form' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                if ($isBusiness) {
                    $response = isset($response['company_legal_form']) ? $response['company_legal_form'] : null;
                } else {
                    $response = null;
                }

                if ($customer->getData('company_legal_form') != $response) {
                    return $response;
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_customer_number' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $customerNumber = isset($response['customer_number']) ? $response['customer_number'] : null;

                if ($customer->getData('customer_number') == $customerNumber && $customerNumber) {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                } else {
                    return $customerNumber;
                }
            },
            'set_customer_ctn' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $ctn = isset($response['mobile_telephone']) ? $response['mobile_telephone'] : null;

                if ($customer->getData('customer_ctn') == $ctn && $ctn) {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                } else {
                    return $ctn;
                }
            },
            'set_billing_arrangement_id' => function(array $response, Mage_Customer_Model_Customer $customer) {
                $response = isset($response['BillingArrangementId']) ? $response['BillingArrangementId'] : null;
                if (!empty($response) && $customer->getData('billing_arrangement_id') != $response) {
                    return trim($response);
                } else {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                }
            },
            'set_customer_value' => function (array $response, Mage_Customer_Model_Customer $customer) {
                // TODO: Quickfix, refactor resolve value ID from DB
                $value = (isset($response['CustomerRank']) && !is_array($response['CustomerRank'])) ? trim($response['CustomerRank']) : null;
                $cvOid = null;
                switch ($value) {
                    case 'A':
                        $cvOid = 3;
                        break;
                    case 'B':
                        $cvOid = 4;
                        break;
                    case 'C':
                        $cvOid = 5;
                        break;
                    case 'D':
                        $cvOid = 6;
                        break;
                    case 'E':
                        $cvOid = 7;
                        break;
                    case 'F':
                        $cvOid = 8;
                        break;
                    case 'G':
                        $cvOid = 9;
                        break;
                }

                if ($customer->getData('customer_value') == $cvOid) {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                } else {
                    return $cvOid;
                }
            },
            'set_customer_label' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $label = isset($response['ProductInvolvementRole']) ? $response['ProductInvolvementRole'] : null;
                if ($customer->getData('customer_label') == $label) {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                } else {
                    return $label;
                }
            },
            'set_no_ordering_allowed' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $label = isset($response['NoOrderingAllowed']) ? $response['NoOrderingAllowed'] : null;
                if ($customer->getData('no_ordering_allowed') == $label) {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                } else {
                    return $label;
                }
            },
            'set_is_business' => function (array $response, Mage_Customer_Model_Customer $customer) use ($isBusiness) {
                $response = (int)$isBusiness;

                if ($customer->getData('is_business') == $response) {
                    return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
                } else {
                    return $response;
                }
            },
            'set_default_billing' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $address = Mage::getModel('customer/address')->load($customer->getData('default_billing'));
                $hasChanges = false;
                $addressFound = false;
                foreach ($response['postal_addresses'] ?? [] as $addkey => $addvalue) {
                    if ($addvalue['AddressTypeCode']=='B') {
                        $addressFound = true;
                        $addressId = isset($addvalue['ID']) ? $addvalue['ID'] : null;
                        $cityName = isset($addvalue['CityName']) ? $addvalue['CityName'] : null;
                        $postalZone = isset($addvalue['PostalZone']) ? $addvalue['PostalZone'] : null;
                        $buildingNumber = isset($addvalue['BuildingNumber']) ? $addvalue['BuildingNumber'] : null;
                        $StreetName = isset($addvalue['StreetName']) ? $addvalue['StreetName'] : null;
                        $country = isset($addvalue['Country']['IdentificationCode']) ? $addvalue['Country']['IdentificationCode'] : null;
                        $buildingNumberAddition = isset($addvalue['HouseNumberAddition']) ? $addvalue['HouseNumberAddition'] : null;
                    }
                }
                if (!$addressFound) {
                    foreach ($response['postal_addresses'] ?? [] as $addkey=>$addvalue) {
                        if ($addvalue['AddressTypeCode'] == 'L') {
                            $addressFound = true;
                            $addressId = isset($addvalue['ID']) ? $addvalue['ID'] : null;
                            $cityName=isset($addvalue['CityName']) ? $addvalue['CityName'] : null;
                            $postalZone=isset($addvalue['PostalZone']) ? $addvalue['PostalZone'] : null;
                            $buildingNumber=isset($addvalue['BuildingNumber']) ? $addvalue['BuildingNumber'] : null;
                            $StreetName=isset($addvalue['StreetName']) ? $addvalue['StreetName'] : null;
                            $country=isset($addvalue['Country']['IdentificationCode']) ? $addvalue['Country']['IdentificationCode'] : null;
                            $buildingNumberAddition=isset($addvalue['HouseNumberAddition']) ? $addvalue['HouseNumberAddition'] : null;
                        }
                    }
                }
                if (!$addressFound) {
                    foreach ($response['postal_addresses'] ?? [] as $addkey=>$addvalue) {
                        if ($addvalue['AddressTypeCode'] == 'C') {
                            $addressId = isset($addvalue['ID']) ? $addvalue['ID'] : null;
                            $cityName=isset($addvalue['CityName']) ? $addvalue['CityName'] : null;
                            $postalZone=isset($addvalue['PostalZone']) ? $addvalue['PostalZone'] : null;
                            $buildingNumber=isset($addvalue['BuildingNumber']) ? $addvalue['BuildingNumber'] : null;
                            $StreetName=isset($addvalue['StreetName']) ? $addvalue['StreetName'] : null;
                            $country=isset($addvalue['Country']['IdentificationCode']) ? $addvalue['Country']['IdentificationCode'] : null;
                            $buildingNumberAddition=isset($addvalue['HouseNumberAddition']) ? $addvalue['HouseNumberAddition'] : null;
                        }
                    }
                }

                $addressData = array(
                    'is_active' => 1,
                    'prefix' => null,
                    'firstname' => isset($response['ContactInformation']['FirstName']) ? $response['ContactInformation']['FirstName'] : null,
                    'middlename' => isset($response['ContactInformation']['MiddleName']) ? $response['ContactInformation']['MiddleName'] : null,
                    'lastname' => isset($response['ContactInformation']['LastName']) ? $response['ContactInformation']['LastName'] : null,
                    'suffix' => null,
                    'city' => $cityName ?? null,
                    'postcode' => $postalZone ?? null,
                    'country_id' => $country ?? null,
                    'telephone' => isset($response['ContactInformation']['PhoneNumber']) ? $response['ContactInformation']['PhoneNumber'] : null,
                    'parent_id' => $customer->getId()
                );

                foreach ($addressData as $key => $value) {
                    if ($address->getData($key) != $value) {
                        $address->setData($key, $value);
                        $hasChanges = true;
                    }
                }

                $street = array(
                    '0' => $StreetName ?? null,
                    '1' => $buildingNumber ?? null,
                    '2' => $buildingNumberAddition ?? null,
                    '3' => $addressId ?? null
                );

                if ($address->getStreet() != $street) {
                    if ($StreetName) {
                        $address->setStreet($street);
                    } else {
                        $address->setStreet($street, true);
                    }
                    $hasChanges = true;
                }

                $firstName = isset($response['ContactInformation']['FirstName']) ? $response['ContactInformation']['FirstName'] : '';
                $lastName = isset($response['ContactInformation']['LastName']) ? $response['ContactInformation']['LastName'] : '';
                $formatedName = $firstName ? $firstName." ". $lastName: $lastName;
                if (!$address->getData('firstname')) {
                    $address->setData('firstname', $formatedName);
                    $hasChanges = true;
                }

                if ($hasChanges) {
                    $address->setIsDefaultBilling(true)->save();
                }

                return $address->getId();
            },
            'set_default_shipping' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $address = Mage::getModel('customer/address')->load($customer->getData('default_shipping'));
                $hasChanges = false;
                $foundShipping = false;
                $cityName = null;
                $postalZone = null;
                $buildingNumber = null;
                $StreetName = null;
                $country = null;
                $buildingNumberAddition = null;
                foreach ($response['postal_addresses'] ?? [] as $addkey => $addvalue) {
                    if ($addvalue['AddressTypeCode'] == 'SH') {
                        $foundShipping = true;
                        $cityName = isset($addvalue['CityName']) ? $addvalue['CityName'] : null;
                        $postalZone = isset($addvalue['PostalZone']) ? $addvalue['PostalZone'] : null;
                        $buildingNumber = isset($addvalue['BuildingNumber']) ? $addvalue['BuildingNumber'] : null;
                        $StreetName = isset($addvalue['StreetName']) ? $addvalue['StreetName'] : null;
                        $country = isset($addvalue['Country']['IdentificationCode']) ? $addvalue['Country']['IdentificationCode'] : null;
                        $buildingNumberAddition = isset($addvalue['HouseNumberAddition']) ? $addvalue['HouseNumberAddition'] : null;
                    }
                }
                if (!$foundShipping) {
                    foreach ($response['postal_addresses'] ?? [] as $addkey => $addvalue) {
                        if ($addvalue['AddressTypeCode'] == 'B') {
                            $foundShipping = true;
                            $cityName = isset($addvalue['CityName']) ? $addvalue['CityName'] : null;
                            $postalZone = isset($addvalue['PostalZone']) ? $addvalue['PostalZone'] : null;
                            $buildingNumber = isset($addvalue['BuildingNumber']) ? $addvalue['BuildingNumber'] : null;
                            $StreetName = isset($addvalue['StreetName']) ? $addvalue['StreetName'] : null;
                            $country = isset($addvalue['Country']['IdentificationCode']) ? $addvalue['Country']['IdentificationCode'] : null;
                            $buildingNumberAddition = isset($addvalue['HouseNumberAddition']) ? $addvalue['HouseNumberAddition'] : null;
                        }
                    }
                }

                if (!$foundShipping) {
                    foreach ($response['postal_addresses'] ?? [] as $addkey => $addvalue) {
                        if ($addvalue['AddressTypeCode'] == 'C') {
                            $foundShipping = true;
                            $cityName = isset($addvalue['CityName']) ? $addvalue['CityName'] : null;
                            $postalZone = isset($addvalue['PostalZone']) ? $addvalue['PostalZone'] : null;
                            $buildingNumber = isset($addvalue['BuildingNumber']) ? $addvalue['BuildingNumber'] : null;
                            $StreetName = isset($addvalue['StreetName']) ? $addvalue['StreetName'] : null;
                            $country = isset($addvalue['Country']['IdentificationCode']) ? $addvalue['Country']['IdentificationCode'] : null;
                            $buildingNumberAddition = isset($addvalue['HouseNumberAddition']) ? $addvalue['HouseNumberAddition'] : null;
                        }
                    }
                }

                if ($foundShipping) {
                    $addressData=array(
                        'is_active'=>1,
                        'prefix'=>null,
                        'firstname'=>$response['ContactInformation']['FirstName'] ?? null,
                        'middlename'=>null,
                        'lastname'=>$response['ContactInformation']['LastName'] ?? null,
                        'suffix'=>null,
                        'city'=>$cityName,
                        'postcode'=>$postalZone,
                        'country_id'=>$country,
                        'telephone'=>$response['ContactInformation']['PhoneNumber'] ?? null,
                        'parent_id'=>$customer->getId(),
                    );

                    foreach ($addressData as $key=>$value) {
                        if ($address->getData($key) != $value) {
                            $address->setData($key, $value);
                            $hasChanges=true;
                        }
                    }

                    $street=array(
                        '0'=>$StreetName,
                        '1'=>$buildingNumber,
                        '2'=>$buildingNumberAddition
                    );

                    if ($address->getStreet() != $street) {
                        $address->setStreet($street);
                        $hasChanges=true;
                    }

                    $firstName = isset($response['ContactInformation']['FirstName']) ? $response['ContactInformation']['FirstName'] : '';
                    $lastName = isset($response['ContactInformation']['LastName']) ? $response['ContactInformation']['LastName'] : '';
                    $formatedName=$firstName ? $firstName . " " . $lastName : $lastName;
                    if (!$address->getData('firstname')) {
                        $address->setData('firstname', $formatedName);
                        $hasChanges=true;
                    }

                    if ($hasChanges) {
                        $address->setIsDefaultShipping(true)->save();
                    }
                }

                return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
            },
            'set_default_service_address' => function (array $response, Mage_Customer_Model_Customer $customer) {
                $service_addressId = $customer->getData('default_service_address_id');
                $address = Mage::getModel('customer/address')->load($service_addressId);
                $hasChanges = false;
                $foundService = false;
                $cityName = null;
                $postalZone = null;
                $buildingNumber = null;
                $StreetName = null;
                $country = null;
                $buildingNumberAddition = null;
                foreach ($response['postal_addresses'] ?? [] as $addkey => $addvalue) {
                    if ($addvalue['AddressTypeCode'] == 'S') {
                        $foundService = true;
                        $cityName = isset($addvalue['CityName']) ? $addvalue['CityName'] : null;
                        $postalZone = isset($addvalue['PostalZone']) ? $addvalue['PostalZone'] : null;
                        $buildingNumber = isset($addvalue['BuildingNumber']) ? $addvalue['BuildingNumber'] : null;
                        $StreetName = isset($addvalue['StreetName']) ? $addvalue['StreetName'] : null;
                        $country = isset($addvalue['Country']['IdentificationCode']) ? $addvalue['Country']['IdentificationCode'] : null;
                        $buildingNumberAddition = isset($addvalue['HouseNumberAddition']) ? $addvalue['HouseNumberAddition'] : null;
                    }
                }
                if (!$foundService) {
                    foreach ($response['postal_addresses'] ?? [] as $addkey => $addvalue) {
                        if ($addvalue['AddressTypeCode'] == 'C') {
                            $foundService = true;
                            $cityName = isset($addvalue['CityName']) ? $addvalue['CityName'] : null;
                            $postalZone = isset($addvalue['PostalZone']) ? $addvalue['PostalZone'] : null;
                            $buildingNumber = isset($addvalue['BuildingNumber']) ? $addvalue['BuildingNumber'] : null;
                            $StreetName = isset($addvalue['StreetName']) ? $addvalue['StreetName'] : null;
                            $country = isset($addvalue['Country']['IdentificationCode']) ? $addvalue['Country']['IdentificationCode'] : null;
                            $buildingNumberAddition = isset($addvalue['HouseNumberAddition']) ? $addvalue['HouseNumberAddition'] : null;
                        }
                    }
                }

                if ($foundService) {
                    $addressData=array(
                        'is_active'=>1,
                        'prefix'=>null,
                        'firstname'=>$response['ContactInformation']['FirstName'] ?? null,
                        'middlename'=>null,
                        'lastname'=>$response['ContactInformation']['LastName'] ?? null,
                        'suffix'=>null,
                        'city'=>$cityName,
                        'postcode'=>$postalZone,
                        'country_id'=>$country,
                        'telephone'=>$response['ContactInformation']['PhoneNumber'] ?? null,
                        'parent_id'=>$customer->getId(),
                    );

                    foreach ($addressData as $key=>$value) {
                        if ($address->getData($key) != $value) {
                            $address->setData($key, $value);
                            $hasChanges=true;
                        }
                    }

                    $street=array(
                        '0'=>$StreetName,
                        '1'=>$buildingNumber,
                        '2'=>$buildingNumberAddition
                    );

                    if ($address->getStreet() != $street) {
                        $address->setStreet($street);
                        $hasChanges=true;
                    }

                    $firstName = isset($response['ContactInformation']['FirstName']) ? $response['ContactInformation']['FirstName'] : '';
                    $lastName = isset($response['ContactInformation']['LastName']) ? $response['ContactInformation']['LastName'] : '';
                    $formatedName = $firstName ? $firstName . " " . $lastName : $lastName;
                    if (!$address->getData('firstname')) {
                        $address->setData('firstname', $formatedName);
                        $hasChanges=true;
                    }

                    if ($hasChanges) {
                        try {
                            $address->save();
                        } catch (Zend_Db_Statement_Exception $e) {
                            if (stripos($e->getMessage(), 'SQLSTATE[40001]') !== false) {
                                $retries = 2;
                                do {
                                    Mage::getSingleton('core/logger')->logException($e);
                                    sleep(1);
                                    try {
                                        $address->save();
                                        $retries = false;
                                        } catch (Zend_Db_Statement_Exception $e) {
                                            $retries--;
                                        }
                                } while ($retries > 0);

                                if ($retries !== false) {
                                    throw $e;
                                }
                            }
                        }                 
                    }

                    $customer->setDefaultServiceAddressId($address->getId())->save();
                }

                return Omnius_Customer_Model_Customer_Customer::IGNORED_VALUE_FOR_FIELDS;
            }
        );

        return $this->mapProperties($properties, $customer, $response);
    }

    /**
     * Assures that the given object is supported by the mapper
     *
     * @param $object
     * @return mixed
     */
    protected function isSupported($object)
    {
        return $object instanceof Mage_Customer_Model_Customer;
    }
}
