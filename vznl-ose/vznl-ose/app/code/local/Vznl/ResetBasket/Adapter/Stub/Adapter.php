<?php

/**
 * Class Vznl_ResetBasket_Adapter_Stub_Adapter
 *
 */
class Vznl_ResetBasket_Adapter_Stub_Adapter
{
    CONST NAME = 'ResetBasket';
    CONST CHANNEL = 'UBUY';
    CONST COUNTRY = 'NL';

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $cty;

    /**
     * Adapter constructor.
     *
     * @param string $channel
     * @param string $cty
     */
    public function __construct()
    {
        $this->channel = self::CHANNEL;
        $this->cty = self::COUNTRY;
    }

    /**
     * @param $basketId
     * @return $responseData
     */
    public function call($basketId, $arguments = array())
    {
        if (is_null($basketId)) {
            return 'No basketId provided to adapter';
        }
        $stubClient = Mage::helper('vznl_resetbasket')->getStubClient();
        $stubClient->setNamespace('Vznl_ResetBasket');
        if (empty($arguments)) {
            $arguments = array(
                'cty' => $this->cty,
                'chl' => $this->channel,
                'basketId' => $basketId
            );
        }
        $responseData = $stubClient->call(self::NAME, $arguments);
        Mage::helper('vznl_resetbasket')->transferLog($arguments, $responseData, self::NAME);
        return $responseData;
    }
}