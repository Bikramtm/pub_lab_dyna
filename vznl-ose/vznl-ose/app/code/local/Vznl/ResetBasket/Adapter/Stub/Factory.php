<?php

class Vznl_ResetBasket_Adapter_Stub_Factory
{
    public static function create()
    {
        return new Vznl_ResetBasket_Adapter_Stub_Adapter();
    }
}