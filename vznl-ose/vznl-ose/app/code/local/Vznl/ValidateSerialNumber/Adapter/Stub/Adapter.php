<?php

/**
 * Class Vznl_ValidateSerialNumber_Adapter_Stub_Adapter
 *
 */
class Vznl_ValidateSerialNumber_Adapter_Stub_Adapter
{
    CONST NAME = 'ValidateSerialNumber';
    /**
     * @param $serialNumber
     * @return $responseData
     */
    public function call($serialNumber, $arguments = array())
    {
        if (is_null($serialNumber)) {
            return 'No serial number provided to adapter';
        }
        $stubClient = Mage::helper('vznl_validateserialnumber')->getStubClient();
        $stubClient->setNamespace('Vznl_ValidateSerialNumber');
        $responseData = $stubClient->call(self::NAME, $arguments);
        Mage::helper('vznl_validateserialnumber')->transferLog($arguments, $responseData, self::NAME);
        return $responseData;
    }
}