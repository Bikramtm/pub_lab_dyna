<?php

class Vznl_ValidateBasket_Helper_Data extends Vznl_Core_Helper_Data
{
    /*
    * @return String
    * */
    public function getAdapterChoice()
    {
        return Mage::getStoreConfig('vodafone_service/validate_basket/adapter_choice');
    }

    /*
    * @return string peal endpoint url
    * */
    public function getEndPoint()
    {
        return Mage::getStoreConfig('vodafone_service/validate_basket/end_point');
    }

    /*
    * @return string peal username
    * */
    public function getLogin()
    {
        return Mage::getStoreConfig('vodafone_service/validate_basket/login');
    }

    /*
    * @return string peal password
    * */
    public function getPassword()
    {
        return Mage::getStoreConfig('vodafone_service/validate_basket/password');
    }

    /*
    * @return string peal cty
    * */
    public function getCountry()
    {
        return Mage::getStoreConfig('vodafone_service/validate_basket/country');
    }

    /*
    * @return string peal channel
    * */
    public function getChannel()
    {
        $agentHelper = Mage::helper('agent');
        $salesChannel = $agentHelper->getDealerSalesChannel();
        if ($salesChannel) {
            return trim($salesChannel);
        } else {
            return trim(Mage::getStoreConfig('vodafone_service/validate_basket/channel'));
        }
    }

    /*
    * @return object omnius_service/client_stubClient
    * */
    public function getStubClient()
    {
        return Mage::getModel('omnius_service/client_stubClient');
    }

    /*
    * @return boolean peal verify
    * */
    public function getVerify()
    {
        return Mage::getStoreConfigFlag('vodafone_service/validate_basket/verify');
    }

    /*
    * @return boolean peal proxy
    * */
    public function getProxy()
    {
        return Mage::getStoreConfigFlag('vodafone_service/validate_basket/proxy');
    }

    /*
    * @return boolean happy flow
    * */
    public function getHappyFlow()
    {
        $HappyFlow = Mage::getStoreConfigFlag('vodafone_service/validate_basket/happyflow');
        $AdapterChoice = $this->getAdapterChoice();
        return $HappyFlow && $AdapterChoice == Vznl_ValidateBasket_Model_System_Config_Source_ValidateBasket_Adapter::STUB_ADAPTER;
    }

    /*
    * @return sku
    * */
    public function getInvalidSku()
    {
        $invalidSku = Mage::getStoreConfig('vodafone_service/validate_basket/invalidsku');
        $AdapterChoice = $this->getAdapterChoice();
        if ($AdapterChoice == Vznl_ValidateBasket_Model_System_Config_Source_ValidateBasket_Adapter::STUB_ADAPTER) {
            return $invalidSku;
        }
        return false;
    }

    /*
     * @return object Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /*
     * @param $basketId
     * @param $consolidate
     * @param $orderType
     * @return array peal/stub response based on adapter choice
     */
    public function getValidateBasket($basketId, $consolidate = null, $orderType = 'PRODUCT_ORDER', $cancellationReasons = [], $lastAddRemoveResponse = null)
    {
        $factoryName = $this->getAdapterChoice();
        $validAdapters = Mage::getSingleton('validateBasket/system_config_source_validateBasket_adapter')->toArray();
        if (!in_array($factoryName, $validAdapters)) {
            return 'Either no adapter is chosen or the chosen adapter is no longer/not supported!';
        }
        $adapter = $factoryName::create();
        return $adapter->call($basketId, $consolidate, $orderType, $cancellationReasons, $lastAddRemoveResponse);
    }
}
