<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Model_Partialterminationsubreason
 */
class Vznl_ValidateBasket_Model_Partialterminationsubreason extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("validateBasket/partialterminationsubreason");
    }

    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }

}
