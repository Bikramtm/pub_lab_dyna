<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Model_Mysql4_Partialterminationsubreason
 */
class Vznl_ValidateBasket_Model_Mysql4_Partialterminationsubreason extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("validateBasket/partialterminationsubreason", "entity_id");
    }
}
