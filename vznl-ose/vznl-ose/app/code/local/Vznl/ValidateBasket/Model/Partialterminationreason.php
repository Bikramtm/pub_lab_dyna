<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Model_Partialterminationreason
 */
class Vznl_ValidateBasket_Model_Partialterminationreason extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("validateBasket/partialterminationreason");
    }

    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }

    /**
     * Load entity by partial termination reason
     *
     * @param string $partialTerminationReason
     * @return bool|Vznl_ValidateBasket_Model_Partialterminationreason
     */
    public function loadByPartialTerminationReason($partialTerminationReason)
    {
        $translation = $this->getCollection()
            ->addFieldToFilter('substatus', $partialTerminationReason)
            ->setPageSize(1, 1)
            ->getLastItem();
        return $translation ? $translation : false;
    }

    public function getPartialTerminationReasons()
    {
        $partialTerminationReasonItems = $this->getCollection()->getItems();
        $partialTerminationReasons = array();
        foreach ($partialTerminationReasonItems as $item) {
            $partialTerminationReasons[$item->getId()] = $item->getNlValue();
        }

        return $partialTerminationReasons;
    }

}
