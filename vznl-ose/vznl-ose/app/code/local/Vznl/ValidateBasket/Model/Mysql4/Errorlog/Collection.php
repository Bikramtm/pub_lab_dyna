<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateBasket_Model_Mysql4_Errorlog_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Override constructor
     */
    public function _construct()
    {
        $this->_init("validateBasket/errorlog");
    }
}
