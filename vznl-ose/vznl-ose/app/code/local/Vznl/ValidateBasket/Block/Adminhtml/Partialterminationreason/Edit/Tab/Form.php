<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationreason_Edit_Tab_Form
 */
class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationreason_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $data = Mage::registry('partial_termination_reason');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $dataFieldset = $form->addFieldset('reason', array('legend' => Mage::helper('vznl_validatebasket')->__('Partial termination reason')));

        $dataFieldset->addField('code', 'text', array(
            'label' => Mage::helper('vznl_validatebasket')->__('Code'),
            'name' => 'code',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getCode(),
        ));

        $dataFieldset->addField('en_value', 'text', array(
            'label' => Mage::helper('vznl_validatebasket')->__('English Value'),
            'name' => 'en_value',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getEnValue(),
        ));

        $dataFieldset->addField('nl_value', 'text', array(
            'label' => Mage::helper('vznl_validatebasket')->__('Dutch Value'),
            'name' => 'nl_value',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getNlValue(),
        ));

        return parent::_prepareForm();
    }
}
