<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationsubreason_Edit_Tabs
 */
class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationsubreason_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('partial_termination_subreason');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('operator')->__('Item Information'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('vznl_validatebasket')->__('Item Information'),
            'title' => Mage::helper('vznl_validatebasket')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('validatebasket/adminhtml_partialterminationsubreason_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
