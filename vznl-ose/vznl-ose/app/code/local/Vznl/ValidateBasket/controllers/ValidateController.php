<?php

/**
 * Class Vznl_ValidateBasket_ValidateController
 */
class Vznl_ValidateBasket_ValidateController extends Mage_Core_Controller_Front_Action
{
    /*
     * Log the errors from the Validate Basket response if any
     */
    public function logErrorAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost('data');
            $quote = Mage::getModel('checkout/session');
            $quoteId = $quote->getQuote()->getEntityId();
            $basketId = $quote->getQuote()->getData('basket_id');
            $errorLog = Mage::getModel('validateBasket/errorlog');

            //set data of the columns
            $errorLog->setData('quote_id', $quoteId);
            $errorLog->setData('basket_id', $basketId);
            $errorLog->setData('error_code', $data['code']);
            $errorLog->setData('error_description', $data['message']);

            //save in the values to the database.
            $errorLog->save();
        }
    }
}