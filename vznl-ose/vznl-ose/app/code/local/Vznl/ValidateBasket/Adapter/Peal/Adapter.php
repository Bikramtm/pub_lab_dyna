<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Vznl_ValidateBasket_Adapter_Peal_Adapter
 */
class Vznl_ValidateBasket_Adapter_Peal_Adapter
{
    CONST name = 'ValidateBasket';
    /**
     * @CONST string
     */
    CONST ORDER_TYPE = 'PRODUCT_ORDER';

    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $cty;

    /**
     * @var string
     */
    private $executionTimeId;

    /**
     * @var string
     */
    private $requestTimeId;

    /**
     * Adapter constructor.
     *
     * @param Client $client
     * @param string $endpoint
     * @param string $channel
     * @param string $cty
     */
    public function __construct(Client $client, string $endpoint, string $channel, string $cty)
    {
        $this->endpoint = $endpoint;
        $this->channel = $channel;
        $this->cty = $cty;
        $this->client = $client;
    }

    /**
     * @param $basketId
     * @param $consolidate
     * @param $orderType
     * @param $cancellationReasons
     * @param $lastAddRemoveResponse
     * @return array|string
     * @throws GuzzleException
     */
    public function call($basketId, $consolidate = null, $orderType = null, $cancellationReasons = [], $lastAddRemoveResponse = null)
    {
        $this->executionTimeId = $this->getHelper()->initTimeMeasurement();

        if (is_null($basketId)) {
            return 'No basketId provided to adapter';
        }
        try {
            $itemList = null;
            if ($cancellationReasons) {
                $itemList = $this->getItemList($cancellationReasons, $lastAddRemoveResponse['itemsList'], $itemList);
            }

            $userName = $this->getHelper()->getLogin();
            $password = $this->getHelper()->getPassword();
            $verify = $this->getHelper()->getVerify();
            $proxy = $this->getHelper()->getProxy();
            $serviceAddress = $consolidate['serviceAddress'];
            $customerProfile = array();
            $agentDetails = array();
            $agentDetails['userName'] = $consolidate['agentName'];
            $customerProfile['type'] = $consolidate['customerType'];
            //basketview
            $basketView['basketId'] = $basketId;
            $basketView['orderType'] = $orderType;
            $basketView['consolidate'] = $consolidate['consolidate'];
            $basketView['addressId'] = $serviceAddress['addressId'];
            $basketView['agentDetails'] = $agentDetails;
            $basketView['customerProfile'] = $customerProfile;
            $basketView['itemsList'] = $itemList;

            $requestLog['url'] = $this->getUrl($basketId);
            $requestLog['body'] = $basketView;

            $this->requestTimeId = $this->getHelper()->initTimeMeasurement();

            if ($userName != '' && $password != '') {
                $response = $this->client->request('POST', $this->getUrl($basketId),
                    [
                        'auth' => [$userName, $password],
                        'body' => json_encode($basketView),
                        'verify' => $verify,
                        'proxy' => $proxy,
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ]
                    ]
                );
            } else {
                $response = $this->client->request('POST', $this->getUrl($basketId),
                    [
                        'body' => json_encode($basketView),
                        'verify' => $verify,
                        'proxy' => $proxy,
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ]
                    ]
                );
            }

            $this->getHelper()->transferLog(
                json_encode($requestLog),
                json_encode(json_decode($response->getBody())),
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $source = json_decode($response->getBody(), true);
        } catch (ClientException|RequestException $exception) {
            $this->getHelper()->transferLog(
                json_encode($requestLog),
                ['msg' => $exception->getMessage(), 'code' => $exception->getCode()],
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );
            $result['error'] = true;
            $result['message'] = $exception->getMessage();
            $result['code'] = $exception->getCode();
            $result['statusCode'] = $exception->getCode();
            return $result;
        }
        return [
            'source' => $source,
            'error'=> false,
            'statusCode' => 200
        ];
    }

    public function getUrl($basketId)
    {
        return $this->endpoint . $basketId . "/validate?" .
            "cty=" . $this->cty .
            "&chl=" . $this->channel;
    }

    /**
     * @return object Vznl_ValidateBasket_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('vznl_validatebasket');
    }

    public function getItemList($cancellationReasons, $lastAddRemoveResponse, $itemList)
    {
        foreach ($lastAddRemoveResponse ?? [] as $key => $item) {
            $itemList[$key]["basketItemId"] = $item["basketItemId"];
            $itemList[$key]["productDescription"] = $item["productDescription"];
            $itemList[$key]["productName"] = $item["productName"];
            $itemList[$key]["action"] = $item["action"];
            $itemList[$key]["offerId"] = $item["offerId"];
            $itemList[$key]["parentBasketItemId"] = $item["parentBasketItemId"];

            if ($item["action"] === "CANCEL") {
                if (!empty($cancellationReasons)) {
                    $itemList[$key]['cancellationReason'] = $cancellationReasons['reason'];
                    $itemList[$key]['cancellationSubReason'] = $cancellationReasons['subreason'];
                    $itemList[$key]['competitorReason'] = $cancellationReasons['competitor'];
                }
            }

            if (!empty($item["bundledItems"])) {
                $itemList[$key]["bundledItems"] = $this->getItemList($cancellationReasons, $item["bundledItems"], $itemList);
            }
            if (!empty($item["childItems"])) {
                $itemList[$key]["childItems"] = $this->getItemList($cancellationReasons, $item["childItems"], $itemList);
            }
        }
        return $itemList;
    }
}
