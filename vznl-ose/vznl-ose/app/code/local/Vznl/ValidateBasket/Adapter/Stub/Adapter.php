<?php

/**
 * Class Vznl_ValidateBasket_Adapter_Stub_Adapter
 *
 */
class Vznl_ValidateBasket_Adapter_Stub_Adapter
{
    CONST NAME = 'ValidateBasket';

    /**
     * @param $basketId
     * @param $consolidate
     * @param $orderType
     * @return array
     */
    public function call($basketId, $consolidate = false, $orderType = null, $cancellationReasons = [], $lastAddRemoveResponse = null)
    {
        $itemList = null;
        if ($cancellationReasons) {
            $itemList = $this->getItemList($cancellationReasons, $lastAddRemoveResponse['itemsList'], $itemList);
        }

        if (is_null($basketId)) {
            return 'No basketId provided to adapter';
        }
        $stubClient = Mage::helper('vznl_validatebasket')->getStubClient();
        $stubClient->setNamespace('Vznl_ValidateBasket');
        $arguments = array($basketId, $consolidate, $orderType, $cancellationReasons, $itemList);
        $responseData = $stubClient->call(self::NAME, $arguments);
        Mage::helper('vznl_validatebasket')->transferLog($arguments, $responseData, self::NAME);
        return [
            'source' => $responseData,
            'error' => false,
            'statusCode' => 200
        ];
    }

    public function getItemList($cancellationReasons, $lastAddRemoveResponse, $itemList)
    {
        foreach ($lastAddRemoveResponse as $key => $item) {
            $itemList[$key]["basketItemId"] = $item["basketItemId"];
            $itemList[$key]["productDescription"] = $item["productDescription"];
            $itemList[$key]["productName"] = $item["productName"];
            $itemList[$key]["action"] = $item["action"];
            $itemList[$key]["offerId"] = $item["offerId"];
            $itemList[$key]["parentBasketItemId"] = $item["parentBasketItemId"];

            if ($item["action"] === "CANCEL") {
                if (!empty($cancellationReasons)) {
                    $itemList[$key]['cancellationReason'] = $cancellationReasons['reason'];
                    $itemList[$key]['cancellationSubReason'] = $cancellationReasons['subreason'];
                    $itemList[$key]['competitorReason'] = $cancellationReasons['competitor'];
                }
            }

            if (!empty($item["bundledItems"])) {
                $itemList[$key]["bundledItems"] = $this->getItemList($cancellationReasons, $item["bundledItems"], $itemList);
            }
            if (!empty($item["childItems"])) {
                $itemList[$key]["childItems"] = $this->getItemList($cancellationReasons, $item["childItems"], $itemList);
            }
        }
        return $itemList;
    }

}
