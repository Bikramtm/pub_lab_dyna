<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Vznl
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('validateBasket/errorlog'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'ID')
    ->addColumn('quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array('nullable' => false), 'Quote_Id')
    ->addColumn('basket_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(), 'Basket_Id')
    ->addColumn('error_code', Varien_Db_Ddl_Table::TYPE_INTEGER, 50, array(), 'Error_Code')
    ->addColumn('error_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Error_Description')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(), 'Created')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(), 'Updated')
    ->addIndex($installer->getIdxName($installer->getTable('validateBasket/errorlog'), array('quote_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        array('quote_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    ->setComment('Vznl ValidateBasket Error log');

$installer->getConnection()->createTable($table);

$installer->endSetup();
