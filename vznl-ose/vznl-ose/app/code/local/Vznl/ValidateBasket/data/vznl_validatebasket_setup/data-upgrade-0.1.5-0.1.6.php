<?php
/** @var $installer Mage_Tax_Model_Resource_Setup */
$installer = $this;

/**
 * Install 
 */
$partialTerminationSubreasons = [];
$rows = [
    [
      3,
      "SB_BANKRUPTCYCUST",
      "Bankruptcy of a customer / WSNP (Law remediation natural persons)",
      "Faillissement van een klant / WSNP",
      0
    ],
    [
      3,
      "SB_INCASSO",
      "Amicable process / collection agency",
      "Minnelijk traject / incassobureau",
      0
    ],
    [
      3,
      "SB_POLICY",
      "redressement",
      "Bewindvoering",
      0
    ],
    [
      4,
      "SB_HIGHUSUAGE",
      "High usage",
      "hoog gebruik",
      0
    ],
    [
      2,
      "SB_ADMINNOTFIXED",
      "To",
      "Admin probl niet opgelost",
      0
    ],
    [
      2,
      "SB_BILLIMGPROBLEM",
      "Admin problem unresolved",
      "Factuur problemen.",
      0
    ],
    [
      2,
      "SB_WRONGATTITUDE",
      "Improperly treated",
      "Onjuist bejegend",
      0
    ],
    [
      9,
      "SB_DECEASED",
      "Deceased",
      "Overleden",
      0
    ],
    [
      16,
      "SB_FAILURENOTFIXED",
      "Technical problem unresolved",
      "Techni. probl niet opgelost",
      0
    ],
    [
      16,
      "SB_INSTALLATIONFAILED",
      "Installation failed",
      "Installatie niet gelukt",
      0
    ],
    [
      16,
      "SB_POOR_QUALITY",
      "Interior Wiring poor quality",
      "Binnenhuisbekabeling slechte kwaliteit",
      0
    ],
    [
      16,
      "SB_SAMEFAILUREFIVE",
      "6 x same malfunction",
      "6 x dezelfde storing",
      0
    ],
    [
      16,
      "SB_SIGNAL_STRENGTH",
      "Low signal strength",
      "Signaal sterkte laag",
      0
    ],
    [
      1,
      "SB_HARDWARECUST",
      "Interior Cabling is bad",
      "Apparatuur klant slecht",
      0
    ],
    [
      1,
      "SB_INHOUSECABLINGBAD",
      "To",
      "Binnenhuisbekabeling slecht",
      0
    ],
    [
      12,
      "SB_CNC8DAYS",
      "Terminate within 14 days after installation",
      "Opzeggen bin 14 dgn na instal",
      0
    ],
    [
      12,
      "SB_LAWREMOTEBUY",
      "The sale law on distance",
      "De wet koop op afstand",
      0
    ],
    [
      12,
      "SB_LEGISLATION",
      "Not fulfill customer promise",
      "Niet nakomen klantbelofte",
      0
    ],
    [
      11,
      "SB_CUSOLDAGEHOME",
      "Elderly / nursing",
      "Bejaarden/verzorgingstehuis",
      0
    ],
    [
      11,
      "SB_LIVETOGETHER",
      "cohabit",
      "Samenwonen",
      0
    ],
    [
      11,
      "SB_MOVEINFOOTPRINT",
      "Non rfs within footprint",
      "Non rfs binnen footprint",
      0
    ],
    [
      11,
      "SB_MOVEOUTSIDEFOOTPRINT",
      "Moving outside footprint",
      "Verhuizing buiten footprint",
      0
    ],
    [
      17,
      "CONTENT_MEDIA_BOX_OFFER",
      "Content / media box offer",
      "Content / aanbod mediabox",
      0
    ],
    [
      17,
      "PRD_NOT_GBR_FRIENDLY",
      "Product not gebr. friendly",
      "Product niet gebr. vriendelijk",
      0
    ],
    [
      17,
      "PRD_NO_INSUFFIENT_USE",
      "Prod. no / insufficient use",
      "Prod. niet / onvoldoende gebruikt",
      0
    ],
    [
      17,
      "QUTY_PRD_INSUFFICENT",
      "Quality product insufficient",
      "Kwaliteit product onvoldoende",
      0
    ],
    [
      8,
      "SB_PONDRECEIVEDPROOF",
      "Pond Received",
      "Pond Received",
      0
    ],
    [
      15,
      "SB_COMETITIONBETTEROFFER",
      "Concur. has better offer",
      "Concur. heeft beter aanbod",
      1
    ],
    [
      15,
      "SB_COMETITIONBETTERPRODUCT",
      "Concur. has better product",
      "Concur. heeft beter product",
      1
    ],
    [
      15,
      "SB_COMPETITIONBETTERPRICE",
      "Concur. better price / quality",
      "Concur. betere prijs/kwaliteit",
      1
    ],
    [
      15,
      "SB_CUSTFINANCIAL",
      "Finance situation of the customer",
      "Financ. situatie van de klant",
      0
    ],
    [
      15,
      "SB_OVERSTAPPEN",
      "Overstapservice",
      "Overstapservice",
      0
    ],
    [
      15,
      "SB_PORTING_PHONE",
      "Uitportering telefoonnnummer",
      "Uitportering telefoonnnummer",
      0
    ],
    [
      15,
      "SB_UPCPRICETOHIGH",
      "Customer will price too high",
      "Klant vindt prijs te hoog",
      0
    ],
    [
      10,
      "SB_CHANGETERMS",
      "Change general vw",
      "Wijziging algemene vw.",
      0
    ],
    [
      10,
      "SB_PRICEINCREASE",
      "Price Increase",
      "Prijsverhoging",
      0
    ],
    [
      7,
      "SB_BADQUILITY",
      "Insufficient quality product",
      "Kwaliteit product onvoldoende",
      0
    ],
    [
      7,
      "SB_CONTENTMEDIABOX",
      "Content / aanbod mediabox",
      "Content / aanbod mediabox",
      0
    ],
    [
      7,
      "SB_NOTUSEDOFTEN",
      "Product not used often",
      "Prod.niet/onvoldoende gebruikt",
      0
    ],
    [
      7,
      "SB_NOTUSERFRIENDLY",
      "Product not User friendly",
      "Product niet gebr.vriendelijk",
      0
    ],
    [
      13,
      "SB_PRODUCTNOTRECEIVED",
      "Product not received",
      "Product niet ontvangen",
      0
    ],
    [
      5,
      "SB_CUSTWONTSAYREASON",
      "Customer does not want to reason",
      "Klant wil reden niet kwijt",
      0
    ],
    [
      19,
      "SB_SUB_RELOCATE_ZIGO",
      "Relocating to Ziggo",
      "Verhuizing naar Ziggo",
      0
    ],
    [
      18,
      "SB_MOVE_UPC",
      "Move to UPC",
      "Overstap naar UPC",
      0
    ],
    [
      14,
      "SB_NOTORDERED",
      "Not requested product",
      "Product niet aangevraagd",
      0
    ],
    [
      14,
      "SB_OUTPORTPHONENR",
      "OutPort Phone Number",
      "Uitportering telefoonnummer",
      0
    ],
    [
      14,
      "SB_WRONGDELIVERY",
      "Wrong product delivered",
      "Verkeerd product geleverd",
      0
    ],
    [
      14,
      "SB_WRONGSALESINFO",
      "Improper sales",
      "Onjuiste verkoop",
      0
    ],
    [
      6,
      "SB_UNAUTHORIZEDRETURN",
      "Unauthorized return",
      "ongeoorloofde terugkeer",
      0
    ]
];

foreach ($rows as $index => $row) {
    $obj = array(
        'entity_id' => $index + 1,
        'main_reason_id' => $row[0],
        'code' => $row[1],
        'en_value' => $row[2],
        'nl_value' => $row[3],
        'include_competitor' => $row[4]
    );
    array_push($partialTerminationSubreasons, $obj);
}

$installer->getConnection()->insertMultiple($installer->getTable('validateBasket/partialterminationsubreason'), $partialTerminationSubreasons);