<?php
/** @var $installer Mage_Tax_Model_Resource_Setup */
$installer = $this;

/**
 * Install 
 */
$partialTerminationReasons = [];
$rows = [
    [
      "SB_INHOUSEFAILURE",
      "Interior Fault",
      "Binnenhuisstoring"
    ],
    [
      "SB_CUSTOMERSERVICE",
      "Customer service",
      "Klantenservice"
    ],
    [
      "SB_COLLECTIONS",
      "Collections",
      "collecties"
    ],
    [
      "SB_COLLECTIONS2",
      "Collections 2",
      "collecties 2"
    ],
    [
      "SB_REASONUNKNOWN",
      "Reason unknown",
      "Reden onbekend"
    ],
    [
      "SB_UNAUTHORIZEDRETURN",
      "Unauthorized return",
      "ongeoorloofde terugkeer"
    ],
    [
      "SB_PRODUCT",
      "Product",
      "Product"
    ],
    [
      "SB_PONDRECEIVED",
      "A recovery from UPC",
      ""
    ],
    [
      "SB_DECEASED",
      "Deceased",
      "Overleden"
    ],
    [
      "SB_PRICEINCREASE",
      "Price Increase",
      "Prijsverhoging"
    ],
    [
      "SB_MOVED",
      "Move",
      "Verhuizing"
    ],
    [
      "SB_LAW",
      "Legislation",
      "Wetgeving"
    ],
    [
      "SB_PRODUCTNOTRECEIVED",
      "Product not received",
      "Product niet ontvangen"
    ],
    [
      "SB_SALEDELIVERY",
      "Sales / Delivery",
      "Verkoop / levering"
    ],
    [
      "SB_PRICECOMPETITION",
      "Price / Competition",
      "Prijs / Concurrentie"
    ],
    [
      "SB_FAILURES",
      "Faults",
      "Storingen"
    ],
    [
      "SB_NO_BINDING_FEE",
      "Downgrade ConPlay to ConPlay without Call",
      "Downgrade ConPlay naar ConPlay zonder Bellen"
    ],
    [
      "SB_RESELLER",
      "reseller",
      "Wederverkoper"
    ],
    [
      "SB_RELOCATE_ZIGO",
      "Relocating to Ziggo",
      "Verhuizing naar Ziggo"
    ]
];

foreach ($rows as $index => $row) {
    $obj = array(
        'entity_id' => $index + 1,
        'code' => $row[0],
        'en_value' => $row[1],
        'nl_value' => $row[2]
    );
    array_push($partialTerminationReasons, $obj);
}

$installer->getConnection()->insertMultiple($installer->getTable('validateBasket/partialterminationreason'), $partialTerminationReasons);
