<?php
/** @var $installer Mage_Tax_Model_Resource_Setup */
$installer = $this;

/**
 * Install 
 */
$fixedLineProviders = [];
$rows = [
    [
      "SB_KPNGLASVEZEL",
      "KPN : Fiberglass",
      1
    ],
    [
      "SB_TELFORTGLASVEZEL",
      "Telfort : Fiberglass",
      1
    ],
    [
      "SB_VODFGLASVEZEL",
      "Vodafone: Fiberglass",
      1
    ],
    [
      "SB_TELE2GLASVEZEL",
      "Tele 2: Fiberglass",
      1
    ],
    [
      "SB_ONSNETGLASVEZEL",
      "Onsnet: Fiberglass",
      1
    ],
    [
      "SB_XSALLGLASVEZEL",
      "Xs4ALL: Fiberglass",
      1
    ],
    [
      "SB_OVERIGEGLASVEZEL",
      "Remaining: Fiberglass",
      1
    ],
    [
      "SB_GEEN",
      "No",
      1
    ],
    [
      "SB_KPNADSL",
      "KPN: ADSL",
      1
    ],
    [
      "SB_TELFORTADSL",
      "Telfort: ADSL",
      1
    ],
    [
      "SB_VODFADSL",
      "Vodafone: ADSL",
      1
    ],
    [
      "SB_TELE2ADSL",
      "Tele 2: ADSL",
      1
    ],
    [
      "SB_ONLINEADSL",
      "Online: ADSL",
      1
    ],
    [
      "SB_XSALLADSL",
      "Xs4ALL: ADSL",
      1
    ],
    [
      "SB_OVERIGEADSL",
      "Remaining: ADSL",
      1
    ],
    [
      "SB_CANALDIGIADSL",
      "CanalDigitaal: ADSL",
      1
    ],
    [
      "SB_TVGLASVEZEL",
      "TV: Glasvezel",
      1
    ],
    [
      "SB_TVSATELIET",
      "TV: Sateliet",
      1
    ],
    [
      "SB_TVDIGITENNE",
      "TV: Digitenne",
      1
    ],
    [
      "SB_TVKPNINTERACTIVE",
      "TV: KPN interactive",
      1
    ],
    [
      "SB_TVTELE2",
      "TV: Tele2",
      1
    ],
    [
      "SB_TVOVERIG",
      "TV: Overig",
      1
    ],
    [
      "SB_TVONBEKEND",
      "TV: Unknown",
      1
    ],
    [
      "SB_INTGLASVEZEL",
      "Internet: Glasvezel",
      1
    ],
    [
      "SB_INTTELE2",
      "Internet: Tele2",
      1
    ],
    [
      "SB_INTALICE",
      "Internet: Alice",
      1
    ],
    [
      "SB_INTTELFORT",
      "Internet: Telfort",
      1
    ],
    [
      "SB_INTHETNET",
      "Internet: Het Net",
      1
    ],
    [
      "SB_INTKPNIP",
      "Internet: KPN I+P",
      1
    ],
    [
      "SB_INTONLINE",
      "Internet: Online",
      1
    ],
    [
      "SB_INTXS4ALL",
      "Internet: XS4all",
      1
    ],
    [
      "SB_INTOVERIG",
      "Internet: Overig",
      1
    ],
    [
      "SB_INTONBEKEND",
      "Internet: Unknown",
      1
    ],
    [
      "SB_TELGALSVEZEL",
      "Tel: Glasvezel",
      1
    ],
    [
      "SB_TELTELE2",
      "Tel: Tele2",
      1
    ],
    [
      "SB_TELALICE",
      "Tel: Alice",
      1
    ],
    [
      "SB_TELTELFORT",
      "Tel: Telfort",
      1
    ],
    [
      "SB_TELHETNET",
      "Tel: HetNet",
      1
    ],
    [
      "SB_TELKPNIP",
      "Tel: KPN I+P",
      1
    ],
    [
      "SB_TELONLINE",
      "Tel: Online",
      1
    ],
    [
      "SB_TELXS4ALL",
      "Tel: XS4all",
      1
    ],
    [
      "SB_TELKPNANALOOG",
      "Tel: KPN Analoog",
      1
    ],
    [
      "SB_TELOVERIG",
      "Tel: Overig",
      1
    ],
    [
      "SB_TELONBEKEND",
      "Tel: Onbekend",
      1
    ],
    [
      "SB_TELMOBTEL",
      "Tel: Mobile Telephony",
      1
    ],
    [
      "SB_WVK01",
      "WVK01",
      1
    ],
    [
      "SB_WVK02",
      "WVK02",
      1
    ],
    [
      "SB_WVK03",
      "WVK03",
      1
    ]
];

foreach ($rows as $row) {
    $obj = array(
        'code' => $row[0],
        'name' => $row[1],
        'fixed_line' => $row[2]
    );
    array_push($fixedLineProviders, $obj);
}

$installer->getConnection()->insertMultiple($installer->getTable('operator/operator_service_providers'), $fixedLineProviders);



