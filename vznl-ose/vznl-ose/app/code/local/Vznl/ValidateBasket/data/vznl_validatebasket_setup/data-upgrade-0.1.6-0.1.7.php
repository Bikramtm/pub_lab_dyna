<?php
/** @var $installer Mage_Tax_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->run("UPDATE partial_termination_subreason SET en_value = 'Admin problem unresolved' WHERE entity_id = 5");
$installer->run("UPDATE partial_termination_subreason SET en_value = 'Billing problems' WHERE entity_id = 6");
$installer->run("UPDATE partial_termination_subreason SET en_value = 'Hardware customer bad' WHERE entity_id = 14");
$installer->run("UPDATE partial_termination_subreason SET en_value = 'In-home cabling bad' WHERE entity_id = 15");
$installer->run("UPDATE partial_termination_subreason SET en_value = 'The Remote Buy law' WHERE entity_id = 17");
$installer->run("UPDATE partial_termination_reason SET en_value = 'Pond received', nl_value = 'Pond received' WHERE entity_id = 8");
$installer->run("UPDATE partial_termination_subreason SET en_value = 'Inhouse cabeling bad' WHERE entity_id = 15");
$installer->run("UPDATE partial_termination_subreason SET en_value = 'Admin probl not solved' WHERE entity_id = 5");

$installer->endSetup();
