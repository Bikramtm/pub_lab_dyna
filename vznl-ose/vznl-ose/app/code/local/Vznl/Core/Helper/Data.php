<?php

class Vznl_Core_Helper_Data extends Dyna_Core_Helper_Data
{
    const PACKAGE_EXPRESSION_REGISTRY_KEY = 'packageModelExpression';
    private static $supportedObjects = [
        'installBase',
        'catalog',
        'customer',
        'availability',
        'cart',
        'order',
        'agent',
        'serviceability',
        'activeInstallBase',
        'orderPackage'
    ];
    private $timeMeasurements = [];
    private $timeMeasurementsResults = [];

    private static $externalServicesCallCount = 0;

    /**
     * Generate a random Guid based on the current time
     * @return string
     */
    public function generateRandomGuid()
    {
        $uid = uniqid(NULL, TRUE);

        $rawid = strtoupper(sha1(uniqid(rand(), true)));

        $result = substr($uid, 6, 8);
        $result .= '-'.substr($uid, 0, 4);
        $result .= '-'.substr(sha1(substr($uid, 3, 3)), 0, 4);
        $result .= '-'.substr(sha1(substr(time(), 3, 4)), 0, 4);
        $result .= '-'.strtolower(substr($rawid, 10, 12));

        return $result;
    }

    /**
     * Convert snake_case words to camelCase
     *
     * @param $val
     * @return mixed|string
     */
    public function snakeToCamel($val)
    {
        $val = str_replace(' ', '', ucwords(str_replace('_', ' ', $val)));
        $val = strtolower(substr($val,0,1)).substr($val,1);
        return $val;
    }

    /**
     * Initialize pacakage model and pass it to the evaluator
     *
     * @param $data
     */
    private function initalizePackageExpressionModel(&$data)
    {
        if(!isset($data['package'])){
            $packageModel = Mage::registry(self::PACKAGE_EXPRESSION_REGISTRY_KEY);
            if(!$packageModel){
                $packageModel = Mage::getModel('dyna_configurator/expression_package');
                if($activePack = Mage::getSingleton('checkout/cart')->getQuote()->getActivePackage()) {
                    $packageModel->setPackage($activePack);
                }
                Mage::unregister(self::PACKAGE_EXPRESSION_REGISTRY_KEY);
                Mage::register(self::PACKAGE_EXPRESSION_REGISTRY_KEY, $packageModel);
            }

            $data['package'] = $packageModel;
        }

    }

    /**
     * Extend evaluateExpressionLanguage method from Dyna to instantiate expression models more dinamically
     *
     * @param $expression
     * @param $data
     * @param string $logFile
     * @return array|bool|string
     */
    public function evaluateExpressionLanguage($expression, $data, $logFile = "invalid_expression_rules.log")
    {
        //only initialize package expresion model if/when we find an expression that requires it
        if(strpos('package.', $expression) >= 0){
            $this->initalizePackageExpressionModel($data);
        }

        return parent::evaluateExpressionLanguage($expression, $data, $logFile);
    }

    /**
     * Override method to define all supported objects
     * @param $expression
     * @param $names
     * @param string $logFile
     * @return bool|null
     */
    public function parseExpressionLanguage($expression, $names, $logFile = "invalid_expression_rules.log"){
        return parent::parseExpressionLanguage($expression, self::$supportedObjects, $logFile);
    }

    /**
     * Save logs for API services
     * @param string $request
     * @param string|null $response
     * @param string $name
     * @param array $measurementsIds
     * @param string $apiType
     * @param string|null $requestHeader
     * @param string|null $responseHeader
     * @return void
     * @throws Mage_Core_Model_Store_Exception
     */
    public function transferLog(
        $request,
        $response,
        string $name,
        array $measurementsIds = [],
        string $apiType = 'API',
        ?string $requestHeader = null,
        ?string $responseHeader = null
    ):void {
        $requestData = sprintf(
            "Request Headers:\n%s\n\nRequest Body:\n%s\n\n",
            var_export($requestHeader, true),
            var_export($request, true)
        );
        $responseData = sprintf(
            "Response Headers:\n%s\n\nResponse Body:\n%s\n\n",
            var_export($responseHeader, true),
            var_export($response, true)
        );
        self::$externalServicesCallCount++;
        if (!empty($measurementsIds)) {
            $exceptionThrown = (
                (is_null($response) && is_null($responseHeader)) ||
                (is_array($response) && isset($response['msg']) && isset($response['code'])
                )
            ) ? 1 : 0;
            // Store code
            $storeCode = Mage::app()->getStore()->getIsActive() ? Mage::app()->getStore()->getCode() : null;
            // Measurements details
            $measurements = sprintf(
                Vznl_InfluxLogs_Model_Observer::LOG_FORMAT,
                $apiType . '_' . $name,
                $this->getNanotime(),
                $this->getTimeMeasure($measurementsIds['request']),
                $this->getTimeMeasure($measurementsIds['execution']),
                $this->getTimeMeasure($measurementsIds['execution']) - $this->getTimeMeasure($measurementsIds['request']),
                $exceptionThrown,
                self::$externalServicesCallCount,
                gethostname(),
                Mage::helper('core/http')->getRequestUri(true),
                $storeCode,
                Mage::helper('vznl_validatecart')->getBasketId()
            );
        }
        $this->getLogger()->logTransfer(
            sprintf(
                "\nSession ID: %s\n\n%s\n%s\n%s",
                Mage::getSingleton('core/session')->getEncryptedSessionId(),
                $requestData,
                $responseData,
                $measurements ?? ''
            ),
            'services' . DS . $name,
            time() . '.' . sha1(rand(0, 1000) . time()), 'log'
        );
    }

    /**
     * Init time measurement
     * @return string
     */
    public function initTimeMeasurement():string {
        $uid = uniqid();
        $this->timeMeasurements[$uid] = microtime(true);
        return $uid;
    }

    /**
     * Get time measurement
     * @param $measurementId string
     * @return float|null
     */
    public function getTimeMeasure(string $measurementId):?float {
        if (!isset($this->timeMeasurements[$measurementId])) {
            return null;
        }
        // Make sure measurement is saved to avoid different results when called multiple time
        if (!isset($this->timeMeasurementsResults[$measurementId])) {
            $this->timeMeasurementsResults[$measurementId] = microtime(true) - $this->timeMeasurements[$measurementId];
        }
        return $this->timeMeasurementsResults[$measurementId];
    }

    /**
     * Get nanotime without loosing precision to avoid duplicated times
     * @return string
     */
    public function getNanotime() {
        return microtime(true) * 1000000000;
    }

    /*
     * @return Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }
}
