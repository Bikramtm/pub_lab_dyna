<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'expiration_date', array(
    'group' => 'General',
    'input' => 'date',
    'type' => 'datetime',
    'label' => 'Expiration date',
    'visible' => true,
    'required' => false,
    'visible_on_front' => true,
    'unique' => false,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'source' => 'eav/entity_attribute_backend_datetime',
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'initial_selectable', array(
    'group' => 'General',
    'input' => 'boolean',
    'type' => 'int',
    'label' => 'Initial Selectable',
    'backend' => '',
    'frontend' => '',
    'visible' => true,
    'required' => false,
    'visible_on_front' => true,
    'unique' => false,
    'default' => '1',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'source' => 'eav/entity_attribute_source_boolean',
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'product_visibility', array(
    'group' => 'General',
    'input' => 'multiselect',
    'type' => 'varchar',
    'label' => 'Product frontend visibility',
    'visible' => true,
    'required' => true,
    'visible_on_front' => true,
    'unique' => false,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'backend' => 'eav/entity_attribute_backend_array',
    'option' => [
        'values' => [
            'Inventory',
            'Configurator',
            'Shopping cart',
            'Extended shopping cart',
            'Order overview',
            'Voice log',
            'Offer PDF',
            'Call summary',
            'not shown',
        ]
    ],
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'use_service_value', array(
    'group' => 'General',
    'input' => 'select',
    'type' => 'varchar',
    'label' => 'Use service value',
    'visible' => true,
    'required' => true,
    'visible_on_front' => false,
    'unique' => false,
    'default' => 'False',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'backend' => 'eav/entity_attribute_backend_array',
    'option' => [
        'values' => [
            'False',
            'True',
        ]
    ],
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'product_segment', array(
    'group' => 'General',
    'input' => 'select',
    'type' => 'varchar',
    'label' => 'Product segment',
    'visible' => true,
    'required' => true,
    'visible_on_front' => false,
    'unique' => false,
    'default' => 'Both',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'backend' => 'eav/entity_attribute_backend_array',
    'option' => [
        'values' => [
            'Business',
            'Privat',
            'Both',
        ]
    ],
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'sorting', array(
    'group' => 'General',
    'input' => 'text',
    'type' => 'text',
    'label' => 'Sorting',
    'visible' => true,
    'required' => false,
    'visible_on_front' => false,
    'unique' => false,
    'default' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'tags', array(
    'group' => 'General',
    'input' => 'text',
    'type' => 'text',
    'label' => 'Tags',
    'visible' => true,
    'required' => false,
    'visible_on_front' => false,
    'unique' => false,
    'default' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'package_type', array(
    'group' => 'General',
    'input' => 'multiselect',
    'type' => 'varchar',
    'label' => 'Package type',
    'visible' => true,
    'required' => true,
    'visible_on_front' => false,
    'unique' => false,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'backend' => 'eav/entity_attribute_backend_array',
    'option' => [
        'values' => [
            'Mobile',
            'Prepaid',
            'Hardware',
            'TV, Internet & Phone',
        ]
    ],
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'package_subtype', array(
    'group' => 'General',
    'input' => 'select',
    'type' => 'varchar',
    'label' => 'Package subtype',
    'visible' => true,
    'required' => true,
    'visible_on_front' => false,
    'unique' => false,
    'default' => 'Both',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'backend' => 'eav/entity_attribute_backend_array',
    'option' => [
        'values' => [
            'Tariff',
            'Device',
            'DeviceRC',
            'Addon',
            'Accessory',
        ]
    ],
));

$attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'use_service_value');
$useServiceValueOptions = $attribute->getSource()->getAllOptions();
$useServiceValueOption = null;
foreach($useServiceValueOptions as $option) {
    if($option['label'] == 'False') {
        $useServiceValueOption = $option['value'];
        break;
    }
}

$attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'product_visibility');
$options = $attribute->getSource()->getAllOptions();
$productVisibilityValue = [];
foreach($options as $option) {
    if(!in_array($option['label'], ['not shown', ''])) {
        $productVisibilityValue[] = $option['value'];
    }
}

$attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'identifier_consumer_business');
$options = $attribute->getSource()->getAllOptions();
foreach($options as $option) {
    switch($option['label']){
        case 'Consumer':
            $consbusConsumer = $option['value'];
            break;
        case 'Business':
            $consbusBusiness = $option['value'];
            break;
        case 'Both':
            $consbusBoth = $option['value'];
            break;
    }
}
$attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'product_segment');
$options = $attribute->getSource()->getAllOptions();
foreach($options as $option) {
    switch($option['label']){
        case 'Private':
            $segmentConsumer = $option['value'];
            break;
        case 'Business':
            $segmentBusiness = $option['value'];
            break;
        case 'Both':
            $segmentBoth = $option['value'];
            break;
    }
}

// Package Types
$attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'identifier_package_type');
$options = $attribute->getSource()->getAllOptions();
$identifierPackageTypeMobile = $identifierPackageTypePrepaid = $identifierPackageTypeHardware = null;
foreach($options as $option) {
    switch($option['label']){
        case 'Mobile':
            $identifierPackageTypeMobile = $option['value'];
            break;
        case 'Prepaid':
            $identifierPackageTypePrepaid = $option['value'];
            break;
        case 'Hardware':
            $identifierPackageTypeHardware = $option['value'];
            break;
    }
}

$attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'package_type');
$options = $attribute->getSource()->getAllOptions();
foreach($options as $option) {
    switch($option['label']){
        case 'Mobile':
            $packageTypeMobile = $option['value'];
            break;
        case 'Prepaid':
            $packageTypePrepaid = $option['value'];
            break;
        case 'Hardware':
            $packageTypeHardware = $option['value'];
            break;
        case 'TV, Internet & Phone':
            $packageTypeTvInternetPhone = $option['value'];
            break;
    }
}

$identifierPackageSubtypeSubscription = $identifierPackageSubtypeDevice = $identifierPackageSubtypeDeviceRC = null;
$identifierPackageSubtypeAddon = $identifierPackageSubtypeAccessory = null;
// Subtypes
$attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'identifier_package_subtype');
$options = $attribute->getSource()->getAllOptions();
foreach($options as $option) {
    switch($option['label']){
        case 'Subscription':
            $identifierPackageSubtypeSubscription = $option['value'];
            break;
        case 'Device':
            $identifierPackageSubtypeDevice = $option['value'];
            break;
        case 'DeviceSubscription':
            $identifierPackageSubtypeDeviceRC = $option['value'];
            break;
        case 'AddOn':
            $identifierPackageSubtypeAddon = $option['value'];
            break;
        case 'Accessoire':
            $identifierPackageSubtypeAccessory = $option['value'];
            break;
    }
}

$attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'package_subtype');
$options = $attribute->getSource()->getAllOptions();
$packageSubtypeTariff = null;
$packageSubtypeDevice = null;
foreach($options as $option) {
    switch($option['label']){
        case 'Tariff':
            $packageSubtypeTariff = $option['value'];
            break;
        case 'Device':
            $packageSubtypeDevice = $option['value'];
            break;
        case 'DeviceRC':
            $packageSubtypeDeviceRC = $option['value'];
            break;
        case 'Addon':
            $packageSubtypeAddon = $option['value'];
            break;
        case 'Accessory':
            $packageSubtypeAccessory = $option['value'];
            break;
    }
}

$products = Mage::getModel('catalog/product')->getCollection();
foreach($products as $product) {
    $product = Mage::getModel('catalog/product')->load($product->getId());

    $product->setInitialSelectable(true);
    $product->getResource()->saveAttribute($product, 'initial_selectable');

    $product->setStack('Mobile');
    $product->getResource()->saveAttribute($product, 'stack');

    $product->setUseServiceValue($useServiceValueOption);
    $product->getResource()->saveAttribute($product, 'use_service_value');

    $product->setProductVisibility(implode(',', $productVisibilityValue));
    $product->getResource()->saveAttribute($product, 'product_visibility');

    $product->setExpirationDate('01-01-9999');
    $product->getResource()->saveAttribute($product, 'expiration_date');

    // Product Segment
    if($product->getIdentifierConsumerBusiness() == $consbusConsumer) {
        $product->setProductSegment($segmentConsumer);
    } elseif($product->getIdentifierConsumerBusiness() == $consbusBusiness) {
        $product->setProductSegment($segmentBusiness);
    } else {
        $product->setProductSegment($segmentBoth);
    }
    $product->getResource()->saveAttribute($product, 'product_segment');

    // Package Type
    $identifierPackageType = explode(',', $product->getIdentifierPackageType());
    $packageTypes = [];
    foreach($identifierPackageType as $type) {
        if($type == $identifierPackageTypeMobile) {
            $packageTypes[] = $packageTypeMobile;
        }
        if($type == $identifierPackageTypePrepaid) {
            $packageTypes[] = $packageTypePrepaid;
        }
        if($type == $identifierPackageTypeHardware) {
            $packageTypes[] = $packageTypeHardware;
        }
    }
    $product->setPackageType(implode(',', $packageTypes));
    $product->getResource()->saveAttribute($product, 'package_type');

    // Package Subtyle
    $identifierPackageSubtype = $product->getIdentifierPackageSubtype();
    if($identifierPackageSubtype == $identifierPackageSubtypeSubscription) {
        $packageSubtype = $packageSubtypeTariff;
    } elseif($identifierPackageSubtype == $identifierPackageSubtypeDevice) {
        $packageSubtype = $packageSubtypeDevice;
    } elseif($identifierPackageSubtype == $identifierPackageSubtypeDeviceRC) {
        $packageSubtype = $packageSubtypeDeviceRC;
    } elseif($identifierPackageSubtype == $identifierPackageSubtypeAddon) {
        $packageSubtype = $packageSubtypeAddon;
    } elseif($identifierPackageSubtype == $identifierPackageSubtypeAccessory) {
        $packageSubtype = $packageSubtypeAccessory;
    } else {
        $packageSubtype = null;
    }
    if($packageSubtype != null) {
        $product->setPackageSubtype($packageSubtype);
        $product->getResource()->saveAttribute($product, 'package_subtype');
    }

    // Sorting
    $product->setSorting($product->getPriority());
    $product->getResource()->saveAttribute($product, 'sorting');
}

// Remove unneeded attributes
$installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY, 'identifier_consumer_business');
$installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY, 'identifier_package_type');
$installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY, 'identifier_package_subtype');
$installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY, 'priority');

$installer->endSetup();
