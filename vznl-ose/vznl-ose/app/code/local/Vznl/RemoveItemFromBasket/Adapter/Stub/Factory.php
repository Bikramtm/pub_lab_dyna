<?php

class Vznl_RemoveItemFromBasket_Adapter_Stub_Factory
{
    public static function create()
    {
        return new Vznl_RemoveItemFromBasket_Adapter_Stub_Adapter();
    }
}