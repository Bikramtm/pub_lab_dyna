<?php

/**
 * Class Vznl_RemoveItemFromBasket_Adapter_Stub_Adapter
 *
 */
class Vznl_RemoveItemFromBasket_Adapter_Stub_Adapter
{
    CONST NAME = 'RemoveItemFromBasket';

    /**
     * @param $basketId
     * @param $technicalId
     * @return $responseData
     */
    public function call($basketId, $technicalId = false)
    {
        if (is_null($basketId)) {
            return 'No basketId provided to adapter';
        }

        $stubClient = Mage::helper('vznl_removeitemfrombasket')->getStubClient();
        $stubClient->setNamespace('Vznl_RemoveItemFromBasket');
        $arguments = array($basketId,$technicalId);
        $responseData = $stubClient->call(self::NAME);
        Mage::helper('vznl_removeitemfrombasket')->transferLog($arguments, $responseData, self::NAME);

        return $responseData;
    }
}
