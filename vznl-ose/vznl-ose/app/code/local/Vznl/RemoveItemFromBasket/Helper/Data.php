<?php

class Vznl_RemoveItemFromBasket_Helper_Data extends Vznl_Core_Helper_Data
{
    /*
    * @return String
    * */
    public function getAdapterChoice()
    {
        return Mage::getStoreConfig('vodafone_service/removeitemfrombasket/adapter_choice');
    }

    /*
    * @return string peal endpoint url
    * */
    public function getEndPoint()
    {
        return trim(Mage::getStoreConfig('vodafone_service/removeitemfrombasket/end_point'));
    }

    /*
    * @return string peal username
    * */
    public function getLogin()
    {
        return trim(Mage::getStoreConfig('vodafone_service/removeitemfrombasket/login'));
    }

    /*
    * @return string peal password
    * */
    public function getPassword()
    {
        return trim(Mage::getStoreConfig('vodafone_service/removeitemfrombasket/password'));
    }

    /*
    * @return string peal cty
    * */
    public function getCountry()
    {
        return trim(Mage::getStoreConfig('vodafone_service/removeitemfrombasket/country'));
    }

    /*
    * @return string peal channel
    * */
    public function getChannel()
    {
        $agentHelper = Mage::helper('agent');
        $salesChannel = $agentHelper->getDealerSalesChannel();
        if ($salesChannel) {
            return trim($salesChannel);
        } else {
            return trim(Mage::getStoreConfig('vodafone_service/removeitemfrombasket/channel'));
        }
    }

    /*
    * @return object omnius_service/client_stubClient
    * */
    public function getStubClient()
    {
        return Mage::getModel('omnius_service/client_stubClient');
    }

    /*
    * @return boolean peal verify
    * */
    public function getVerify()
    {
        return Mage::getStoreConfigFlag('vodafone_service/removeitemfrombasket/verify');
    }

    /*
    * @return boolean peal proxy
    * */
    public function getProxy()
    {
        return Mage::getStoreConfigFlag('vodafone_service/removeitemfrombasket/proxy');
    }

    /*
     * @return object Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /*
    * @param $basketId
    * @param $technicalId
    * @return array peal/stub response based on adpter choice
    * */
    public function removeitemfrombasket($basketId, $technicalId = false)
    {
        $factoryName = $this->getAdapterChoice();
        $validAdapters = Mage::getSingleton('removeitemfrombasket/system_config_source_removeItem_adapter')->toArray();
        if (!in_array($factoryName, $validAdapters)) {
            return 'Either no adapter is chosen or the chosen adapter is no longer/not supported!';
        }
        $adapter = $factoryName::create();
        return $adapter->call($basketId, $technicalId);
    }

}
