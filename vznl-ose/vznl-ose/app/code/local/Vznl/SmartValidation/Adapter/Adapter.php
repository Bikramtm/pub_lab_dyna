<?php

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Zend\Soap\Client;

/**
 * Class Vznl_SmartValidation_Adapter_Adapter
 */
class Vznl_SmartValidation_Adapter_Adapter
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Exception
     */
    protected $exception;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var bool
     */
    protected $stubMode;


    protected $mobileNumber;
    protected $dealerCode;
    protected $validationCode = null;

    public function __construct(Client $client, LoggerInterface $logger, array $options=[], $stubbed = false)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->options = array_merge($this->options, $options);
        $this->stubMode = $stubbed;
    }

    /**
     * Get the last exception
     * @return Exception
     */
    public function getLastException()
    {
        return $this->exception;
    }

    /**
     * Send data towards the smart validation service
     * @param string $mobileNumber The mobile number
     * @param string $dealerCode The dealer code
     * @param string|null $validationCode The validation code or null
     * @return bool|string <false> if unsuccessful, the xml response if successful
     */
    public function send($mobileNumber, $dealerCode, $validationCode = null)
    {
        $this->mobileNumber = $mobileNumber;
        $this->dealerCode = $dealerCode;
        $this->validationCode = $validationCode;

        $requestData = $this->buildRequest();

        // Check if stub mode is on
        if($this->stubMode){
            $response = $this->getStubbedResponse();
            $requestHeader = $this->buildBslRequestHeader(true);
            $this->logStub($requestHeader, $requestData, $response);
            return $this->prepareResponse($response);
        }

        ini_set('default_socket_timeout', isset($this->options['timeout']) ? $this->options['timeout'] : 10);
        try {
            $this->client->setLocation($this->options['endpoint']);
            $this->client->addSoapInputHeader($this->buildBslRequestHeader());
            $this->client->smartVal($requestData);
            $this->log();
        } catch(Exception $e) {
            $this->log($e);
            $this->exception = $e;
            if(!empty($this->client->getLastResponse())) {
                return $this->prepareResponse($this->client->getLastResponse());
            }
            return false;
        }
        return $this->prepareResponse($this->client->getLastResponse());
    }

    /**
     * Build the bsl request headers
     * @param bool $asString Whether or not the response should be a xml string.
     * @return SOAPHeader|string The request header
     */
    protected function buildBslRequestHeader($asString = false){
        $header = new Omnius_Core_Model_SimpleDOM('<root></root>');
        /** @var Omnius_Core_Model_SimpleDOM $bslHeader */
        $bslHeader = $header->addChild('com:BSLHeader', null, 'http://com.amdocs.bss.bsl/');

        $headerParams = new Omnius_Core_Model_SimpleDOM('<root></root>');
        $headerParams->addChild('Username', $this->options['username']);
        $headerParams->addChild('Password', $this->options['password']);
        $headerParams->addChild('MessageID', str_replace('.', '', microtime(true)));
        $headerParams->addChild('EndUserName', $this->options['enduser']);
        $headerParams->addChild('DealerCode', $this->dealerCode ?: '0');

        $bslHeader->cloneChildrenFrom($headerParams);

        if($asString){
            return $bslHeader->asXML();
        }
        $var = new SoapVar($bslHeader->asXML(), XSD_ANYXML);
        return new SOAPHeader('http://com.amdocs.bss.bsl/', 'BSLHeader', $var);
    }

    /**
     * Build the request
     * @return array The request data
     */
    protected function buildRequest()
    {
        $agent = $this->getAgent();
        $userName = ($agent && $agent->getId()) ? $agent->getUsername() : '';

        $request = [
            'clientId' => 'UBuy',
            'msisdn' => $this->mobileNumber,
            'user' => $userName,
            'validationType' => 'SmartVal'
        ];
        if($this->validationCode){
            $request['passCode'] = $this->validationCode;
        }
        return $request;
    }

    /**
     * Get the stubbed response.
     * @return mixed The json response.
     */
    protected function getStubbedResponse(){
        $errorCode = null;
        $errorMessage = '';

        switch($this->mobileNumber){
            case '0600000001':
                $errorCode = 'BSL-60056';
                $errorMessage = 'Number of codes generated are greater than the configured amount';
                break;
            case '0600000002':
                $errorCode = 'BSL-60058';
                $errorMessage = 'Number is already smart validated';
                break;
            case '0600000003':
                $errorCode = 'BSL-60059';
                $errorMessage = 'Format of the input parameters is incorrect';
                break;
        }

        switch($this->validationCode){
            case 'NOK1':
                $errorCode = 'BSL-60057';
                $errorMessage = 'Number of attempts generated are greater than the configured amount';
                break;
            case 'NOK2':
                $errorCode = 'BSL-60061';
                $errorMessage = 'Passcode sent to validate is wrong';
                break;
        }

        $root = new SimpleXMLElement('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"/>');
        $headerNode = $root->addChild('soap:Header', null, 'http://schemas.xmlsoap.org/soap/envelope/');
        $bslHeaderNode = $headerNode->addChild('bsl:BSLResponseHeader', null, 'http://com.amdocs.bss.bsl/');
        $bslHeaderNode->addChild('ResponseTimeStamp', '2017-10-24T13:46:17.831+02:00');
        $bslHeaderNode->addChild('MessageID', 'sadsasd3321211');
        $bslHeaderNode->addChild('BSLServiceOperation', 'smartVal');
        if(!$errorCode) {
            $bodyNode = $root->addChild('soapenv:Body', null, 'http://schemas.xmlsoap.org/soap/envelope/');
            $responseNode = $bodyNode->addChild('wrap:smartValResponse', null, 'http://com.amdocs.bss.bsl/UBL/wrapper');
            $responseNode->addChild('urn:response', 'OK', 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2');
            $responseNode->addChild('urn:msisdn', $this->mobileNumber, 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2');
        } else {
            $bodyNode = $root->addChild('soapenv:Body', null, 'http://schemas.xmlsoap.org/soap/envelope/');
            $faultBodyNode = $bodyNode->addChild('soapenv:Fault');
            $faultBodyNode->addChild('faultCode', 'soapenv:Server');
            $faultBodyNode->addChild('faultstring', 'BSL exception');
            $detailNode = $faultBodyNode->addChild('detail');
            $errorNode = $detailNode->addChild('bsl:error', null, 'http://com.amdocs.bss.bsl/');
            $errorNode->addChild('errorCode', $errorCode);
            $errorNode->addChild('errorMessage', $errorMessage);
            $errorDetailNode = $errorNode->addChild('detail');
            $errorDetailNode->addChild('sourceSystem', 'MOSA');
        }
        return $root->asXML();
    }

    /**
     * Log the request/response of the actual performed call
     * @param null|Exception $exception The exception if any
     */
    protected function log($exception=null)
    {
        $log = "\n";
        $log .= sprintf("Request headers:\n%s\n", $this->client->getLastRequestHeaders());
        $log .= sprintf("Request:\n%s\n", $this->format($this->client->getLastRequest()));
        $log .= sprintf("Response headers:\n%s\n", $this->client->getLastResponseHeaders());
        $responseMessage = $this->client->getLastResponse();
        $responseMessage = $responseMessage ? $this->format($responseMessage) : $responseMessage;
        if($exception != null){
            if(empty($responseMessage)){
                $responseMessage = $exception->getMessage()."\n";
                $responseMessage .= $exception->getTraceAsString();
            }
        }
        $log .= sprintf("Response:\n%s", $responseMessage);
        $this->logger->log(LogLevel::DEBUG, $log);
    }

    /**
     * Log the stubbed request/response
     * @param $requestHeaders The request header
     * @param $requestData The request to log
     * @param $response The response to log
     */
    protected function logStub($requestHeader, $requestData, $response)
    {
        $dom = new DOMDocument();
        $dom->loadXML('<ENVELOPE><HEADER>' . $requestHeader . '</HEADER><BODY>' . json_encode($requestData) . '</BODY></ENVELOPE>');
        $request = $dom->saveXML();

        $log = "\n";
        $log .= sprintf("Request headers:\n%s\n", '');
        $log .= sprintf("Request:\n%s\n\n", $this->format($request));
        $log .= sprintf("Response headers:\n%s\n", '');
        $log .= sprintf("Response:\n%s", $this->format($response));
        $this->logger->log(LogLevel::DEBUG, $log);
    }

    /**
     * Format the given xml string
     * @param $mixed The xml string
     * @return string The formatted xml string
     */
    protected function format($mixed)
    {
        try {
            $dom = new DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = false;
            if (!empty($mixed)) {
                $dom->loadXML($mixed);
            }
            return preg_replace("/\n/", "", $dom->saveXML());
        }
        catch(Exception $e){
            Mage::logException($e);
            return $mixed;
        }
    }

    /**
     * Prepare the response to be returned as an array
     * @param $response The xml response
     * @return array The xml as array.
     */
    protected function prepareResponse($response){
        return Mage::helper('vznl_core/service')->xmlStringToArray($response, false);
    }

    protected function getAgent()
    {
        return Mage::getSingleton('customer/session')->getAgent();
    }
}
