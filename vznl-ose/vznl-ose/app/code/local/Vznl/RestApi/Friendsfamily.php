<?php

class Vznl_RestApi_Friendsfamily extends Vznl_RestApi_Abstract
{
    /**
     * @var array
     */
    public static $routes = array(
        array(
            'method' => 'POST',
            'path' => '/friendsfamily/request',
            'class' => 'Vznl_RestApi_Friendsfamily_Request',
            'log' => true,
            'active' => true,
        ),
        
        array(
            'method' => 'POST',
            'path' => '/friendsfamily/confirm',
            'class' => 'Vznl_RestApi_Friendsfamily_Confirm',
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/friendsfamily/cancel',
            'class' => 'Vznl_RestApi_Friendsfamily_Cancel',
            'log' => false,
            'active' => true,
        ),
        array(
            'method' => 'GET',
            'path' => '/friendsfamily/info/:token',
            'class' => 'Vznl_RestApi_Friendsfamily_Info',
            'log' => false,
            'active' => true,
        )
    );
}