<?php

/**
 * Class Vznl_RestApi_Catalogue_Get6
 */
class Vznl_RestApi_Catalogue_Get6
{
    public const DEFAULT_EFFECTIVE_DATE = '2000-01-01';
    public const DEFAULT_EXPIRATION_DATE = '2099-01-01';

    protected const MOBILE_STACK = 'MOBILE';
    protected const DEFAULT_MOBILE_LIFECYCLE = 'LIVE';

    /**
     * @var bool
     */
    protected $cache = true;

    /**
     * @var string
     */
    protected $cacheKeyPrefix = 'hawaii_rest_catalog_6_';

    /**
     * @var int
     */
    protected $cacheTTL = 1209600; // 2 weeks

    /**
     * @var array
     */
    protected $enabled = [
        'mobile' => false,
        'fixed' => false,
    ];

    /**
     * @var array
     */
    protected $errors;

    /**
     * @var array
     */
    protected $lifecycleMappings = [
        Dyna_Catalog_Model_ProcessContext::ACQ => 'acquisition',
        Dyna_Catalog_Model_ProcessContext::ILSBLU => 'ils',
        Dyna_Catalog_Model_ProcessContext::RETBLU => 'retention',
        Dyna_Catalog_Model_ProcessContext::MOVE => 'move',
    ];

    /**
     * @var array
     */
    protected $productSegmentMappings = [
        'both' => 'Business,Consumer',
        'business , consumer' => 'Business,Consumer',
        'residential' => 'Consumer',
        'soho' => 'Business',
    ];

    /**
     * @var Mage_Core_Controller_Request_Http
     */
    protected $request;

    /**
     * @var array
     */
    protected $usedCategories = [];

    /**
     * @var array
     */
    protected $usedProducts = [];

    /**
     * @var Vznl_RestApi_Catalogue_Helper_MobileParser|null
     */
    protected $mobileParser;

    /**
     * @var Vznl_RestApi_Catalogue_Helper_FixedParser|null
     */
    protected $fixedParser;

    /**
     * Vznl_RestApi_Catalogue_Get6 constructor.
     *
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct(Mage_Core_Controller_Request_Http $request)
    {
        $this->setRequest($request);
        $this->setStack($request->getParam('stack', 'fixed'));
        $this->mobileParser = new Vznl_RestApi_Catalogue_Helper_MobileParser();
        $this->fixedParser = new Vznl_RestApi_Catalogue_Helper_FixedParser();
    }

    /**
     * Store the request object to the class to extract values later on
     *
     * @param Mage_Core_Controller_Request_Http $request
     * @return Vznl_RestApi_Catalogue_Get6
     */
    public function setRequest(Mage_Core_Controller_Request_Http $request): Vznl_RestApi_Catalogue_Get6
    {
        $this->request = $request;
        return $this;
    }

    /**
     * Get the request
     *
     * @return Mage_Core_Controller_Request_Http
     */
    public function getRequest(): Mage_Core_Controller_Request_Http
    {
        return $this->request;
    }

    /**
     * Add an error
     *
     * @param string $sku
     * @param string $type
     * @param string $text
     * @return array
     */
    public function addError($sku, $type, $text): array
    {
        $this->errors[] = ['sku' => $sku, 'type' => $type, 'text' => $text];

        return $this->errors;
    }

    /**
     * @return Vznl_Configurator_Model_Cache
     */
    public function getCache(): Vznl_Configurator_Model_Cache
    {
        return Mage::getSingleton('vznl_configurator/cache');
    }

    /**
     * Get the cache key for the requested parameters
     *
     * @return string
     */
    protected function getCacheKey(): string
    {
        $cacheKeyParts = [];
        if ($this->getRequest()->getParam('versionOnly', false) == 'true') {
            $cacheKeyParts[] = 'versionOnly';
        } else {
            $cacheKeyParts[] = 'categories';
            $cacheKeyParts[] = 'products';
            $cacheKeyParts[] = 'compatibilities';
            $cacheKeyParts[] = 'promotions';
        }
        $cacheKeyParts[] = implode('_', $this->getStack());

        return $this->cacheKeyPrefix . implode('_', $cacheKeyParts);
    }

    /**
     * Parse the different type of products
     * @return string
     * @throws Exception
     */
    protected function parseProducts(): string
    {
        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->startElement('products');
            if ($this->enabled['mobile']) {
                foreach ($this->mobileParser->getProductsWithAttributeSet() as $productSet) {
                    $xml->writeRaw(
                        $this->handleProducts($productSet['products'], $productSet['attributes'])
                    );
                }
            }
            if ($this->enabled['fixed']) {
                foreach ($this->fixedParser->getProductsWithAttributeSet() as $productSet) {
                    $xml->writeRaw(
                        $this->handleProducts($productSet['products'], $productSet['attributes'])
                    );
                }
            }
        $xml->endElement();
        return $xml->outputMemory();
    }

    /**
     * Process the actual request
     *
     * @return string
     * @throws Exception
     */
    public function process(): string
    {
        if ($this->cache && $this->getRequest()->getParam('refreshcache', 'false') == 'false') {
            $xml = $this->getCache()->load($this->getCacheKey());
            if ($xml) {
                return $xml;
            }
        }

        $catalogXml = new XMLWriter();
        $catalogXml->openMemory();
        $catalogXml->setIndent(true);
        $catalogXml->startDocument('1.0', 'utf-8');
        $catalogXml->startElement('catalog');
        $catalogXml->writeRaw($this->setReferenceDataDeliveries());

        if ($this->getRequest()->getParam('versionOnly', 'false') == 'false') {
            $this->applyAttributesFiltering(); // Check if there are different attributes requested

            // Categories - 2
            $catalogXml->writeRaw($this->setCategories());

            // Products - 1
            $catalogXml->writeRaw($this->parseProducts());

            // Compatibilities - 4
            $catalogXml->writeRaw($this->setCompatibilities());

            // Promotions - 3
            $catalogXml->writeRaw($this->setPromotions());
        }

        // Show errors
        if ($this->getRequest()->getParam('errors', 'false') == 'true' && !empty($this->errors)) {
            $catalogXml->startElement('errors');
            foreach ($this->errors as $error) {
                $catalogXml->startElement('error');
                $this->writeCdataElement($catalogXml, 'sku', $error['sku']);
                $this->writeCdataElement($catalogXml, 'type', $error['type']);
                $this->writeCdataElement($catalogXml, 'text', $error['text']);
                $catalogXml->endElement();
            }
            $catalogXml->endElement();
        }

        $catalogXml->endElement();
        $catalogXml->endDocument();
        $response = $catalogXml->outputMemory();

        // Cache the XML
        if ($this->cache) {
            $this->getCache()->save(
                $response,
                $this->getCacheKey(),
                [Vznl_Configurator_Model_Cache::CACHE_TAG],
                $this->cacheTTL
            );
        }

        return $response;
    }

    /**
     * @return string
     */
    protected function setReferenceDataDeliveries(): string
    {
        $queryResult = Mage::getSingleton('core/resource')->getConnection('core_write')->query(
            sprintf(
                'SELECT * FROM catalog_import_log WHERE stack IN ("%s") ORDER BY `date` DESC;',
                implode('","', $this->getStack())
            )
        );

        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->startElement('referenceDataDeliveries');
        while ($row = $queryResult->fetch()) {
            $xml->startElement('referenceDataDelivery');
            $this->writeCdataElement($xml, 'stack', strtoupper($row['stack']));
            $this->writeCdataElement($xml, 'interfaceVersion', $row['interface_version']);
            $this->writeCdataElement($xml, 'referenceDataBuild', $row['reference_data_build']);
            $this->handleDateTime($xml, 'date', $row['date']);
            $this->writeCdataElement($xml, 'log', $row['log']);
            $this->writeCdataElement($xml, 'author', $row['author']);
            $xml->startElement('files');
            $this->writeCdataElement($xml, 'file', $row['file_name']);
            $xml->endElement();
            $xml->endElement();
        }
        $xml->endElement();

        return $xml->outputMemory();
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function setCategories(): string
    {
        $xml = new XMLWriter();
        $xml->openMemory();

        $categoryModel = Mage::getModel('catalog/category');

        /** @var Mage_Catalog_Model_Resource_Category_Collection $categories */
        $categories = $categoryModel
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('stack', $this->getStack())
            ->addAttributeToSort('entity_id', 'asc')
            ->addAttributeToSort('stack', 'asc')
            ->addIsActiveFilter();

        if ($categories->count()) {
            $xml->startElement('categories');

            /** @var Vznl_Catalog_Model_Category $category */
            foreach ($categories as $category) {
                $this->usedCategories[] = $category->getUniqueId();
                $parentCategory = $categoryModel
                    ->getCollection()
                    ->addFieldToFilter('entity_id', ['eq' => $category->getParentId()])
                    ->setPageSize(1, 1)
                    ->getLastItem();

                $xml->startElement('category');
                $this->writeCdataElement($xml, 'stack', strtoupper($category->getStack()));
                $this->writeCdataElement($xml, 'id', $category->getUniqueId());
                $this->writeCdataElement(
                    $xml,
                    'parentId',
                    !empty($parentCategory) ? $parentCategory->getUniqueId() : ''
                );
                $this->writeCdataElement($xml, 'name', $category->getName());
                $this->writeCdataElement(
                    $xml,
                    'mutualExclusive',
                    $category->getAllowMutualProducts() == 1 ? 'true' : 'false'
                );
                $this->writeCdataElement(
                    $xml,
                    'isAnchor',
                    $category->getIsAnchor() == 1 ? 'true' : 'false'
                );
                $this->writeCdataElement(
                    $xml,
                    'familyCategory',
                    $category->getIsFamily() == 1 ? 'true' : 'false'
                );
                $this->writeCdataElement($xml, 'familyCategoryName', $category->getData('display_name'));
                $xml->startElement('products');
                foreach ($category->getProductSkus() as $productSku) {
                    $this->writeCdataElement($xml, 'sku', $productSku);
                }
                $xml->endElement();
                $xml->endElement();
            }

            $xml->endElement();
        }

        return $xml->outputMemory();
    }

    /**
     * Select all compatibility rules
     *
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function setCompatibilities()
    {
        $operations = Mage::helper('dyna_productmatchrule')->getOperations();
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $queryResult = $write->query('
            SELECT
            pmr.product_match_rule_id AS ruleId,
            pmr.operation_type AS operationType,
            pmr.stack,
            pmr.effective_date AS effectiveDate,
            pmr.expiration_date AS expirationDate,
            pmr.left_id AS source,
            pmr.right_id AS target,
            pc.code AS processContext,
            pmr.operation,
            pmr.operation_value AS operationalValue,
            pmr.priority
            FROM product_match_rule pmr
            LEFT JOIN product_match_rule_website pmrw ON pmrw.rule_id = pmr.product_match_rule_id
            LEFT JOIN product_match_rule_process_context pmrpc ON pmrpc.rule_id = pmr.product_match_rule_id
            LEFT JOIN process_context pc ON pc.entity_id = pmrpc.process_context_id
            WHERE pmrw.website_id = ' . Mage::app()->getStore()->getWebsiteId() . '
            AND pmr.operation_type IN (1, 2, 3, 4)
            AND pc.code IN ("ACQ", "RETBLU", "ILSBLU", "MOVE")
            AND stack IN ("' . implode('","', $this->getStack()) . '")
            ORDER BY pc.code ASC, pmr.product_match_rule_id ASC
        ');

        // Setup xml data
        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->startElement('compatibilities');
        while ($row = $queryResult->fetch()) {
            $xml->startElement('compatibility');
            $this->writeCdataElement($xml, 'stack', strtoupper($row['stack']));
            $this->handleDateTime(
                $xml,
                'effectiveDate',
                !empty($row['effectiveDate']) ? $row['effectiveDate'] :  self::DEFAULT_EFFECTIVE_DATE,
                true
            );
            $this->handleDateTime(
                $xml,
                'expirationDate',
                !empty($row['expirationDate']) ? $row['expirationDate'] : self::DEFAULT_EXPIRATION_DATE,
                true
            );
            $this->getCompatibilitiesSourceTarget($xml, $row);
            $this->writeCdataElement(
                $xml,
                'processContext',
                $this->lifecycleMappings[$row['processContext']] ?? $row['processContext']
            );
            $this->writeCdataElement($xml, 'operation', $operations[$row['operation']] ?? null);
            // 5 - min, 6 - max, 7 - equal
            if (in_array($row['operation'], [5 ,6, 7])) {
                $this->writeCdataElement($xml, 'operationalValue', $row['operationalValue'] ?? null);
            }
            $xml->writeElement('priority', $row['priority']);
            $xml->endElement();
        }
        $xml->endElement();

        return $xml->outputMemory();
    }

    /**
     * Render the promotions
     *
     * @return string
     * @throws Mage_Core_Exception
     */
    protected function setPromotions()
    {
        $result = null;
        // Setup xml data
        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->startElement('promotions');

        // all promo rules
        $ruleCollection = Mage::getResourceModel('salesrule/rule_collection')
            ->addWebsiteFilter(Mage::app()->getWebsite()->getId())
            ->addFieldToFilter('is_active', true)
            ->addFieldToFilter('stack', $this->getStack());
        foreach ($ruleCollection as $rule) {
            try {
                $interpreter = new Vznl_RestApi_Catalogue_Helper_SalesRuleInterpreter();
                $dataRules = $interpreter->interpretConditions($rule);
                $dataActions = $interpreter->interpretActions($rule);
            } catch (Exception $e) {
                $this->addError(
                    $rule->getId(),
                    'promotion',
                    'Could not interpret rule ' . $rule->getId() . ': ' . $e->getMessage()
                );
                continue;
            }

            // Loop through rules
            foreach ($dataRules as $lifecycle => $data) {
                $data['customer_value'] = is_array($data['customer_value']) ? $data['customer_value'] : [$data['customer_value']];
                $data['value_segment'] = is_array($data['value_segment']) || is_null($data['value_segment']) ? $data['value_segment'] : [$data['value_segment']];
                $data['campaign'] = is_array($data['campaign'])? $data['campaign'] : [$data['campaign']];
                $data['post_codes'] = is_array($data['post_codes']) || is_null($data['post_codes']) ? $data['post_codes'] : [$data['post_codes']];
                $data['dealer_group'] = is_array($data['dealer_group']) || is_null($data['dealer_group']) ? $data['dealer_group'] : [$data['dealer_group']];
                $data['targets'] = $dataActions[$lifecycle]['products_categories'] ?? null;

                $skuCollection = $this->checkUnknownProducts($rule, $data['products']);
                $categoryIdCollection = $this->checkUnknownCategories($rule, $data['categories']);

                if (!empty($skuCollection) || !empty($categoryIdCollection)) {
                    $xml->startElement('promotion');
                    $xml->writeElement('salesRuleId', $rule->getId());
                    $this->writeCdataElement($xml, 'ruleTitle', $rule->getName());
                    $this->writeCdataElement($xml, 'stack', strtoupper($rule->getStack()));
                    $this->handleDateTime(
                        $xml,
                        'effectiveDate',
                        $rule->getFromDate() ?? self::DEFAULT_EXPIRATION_DATE,
                        true
                    );
                    $this->handleDateTime(
                        $xml,
                        'expirationDate',
                        $rule->getToDate() ?? self::DEFAULT_EXPIRATION_DATE,
                        true
                    );
                    $this->writeCdataElement($xml, 'processContext', ($this->lifecycleMappings[$lifecycle] ?? $lifecycle));
                    $xml->writeElement('promoPriority', $rule->getSortOrder());
                    $this->writeCdataElement($xml, 'promoType', $rule->getIsPromo() == 1 ? 'defaulted' : 'optional');
                    $this->writeCdataElement($xml, 'couponType', $rule->getCouponType() == 2 ? 'specific_coupon': 'false_coupon');
                    $coupon = Mage::getModel('salesrule/coupon')->load($rule->getId(), 'rule_id');
                    $this->writeCdataElement($xml, 'discountCode', $coupon->getCode());
                    $xml->writeElement('usesPerCoupon', $rule->getUsesPerCoupon());
                    $xml->writeElement('usesPerCustomer', $rule->getUsesPerCustomer());
                    $this->writeCdataElement($xml, 'couponDiscountType', $rule->getSimpleAction());
                    $this->writeCdataElement($xml, 'couponDiscountSize', $rule->getDiscountAmount());
                    $this->writeCdataElement($xml, 'maximumQtyDiscount', $rule->getDiscountQty());
                    $this->writeCdataElement($xml, 'discountShelfQuantity', $rule->getDiscountStep());
                    $this->writeCdataElement($xml, 'applyToShippingAmount', $rule->getApplyToShipping());
                    $this->writeCdataElement(
                        $xml,
                        'freeShipping',
                        $rule->getSimpleFreeShipping() == 1 ? 'true' : 'false'
                    );
                    $this->writeCdataElement(
                        $xml,
                        'stopFurtherRulesProcessing',
                        $rule->getStopRulesProcessing() == 1 ? 'true' : 'false'
                    );
                    $this->writeCdataElement($xml, 'promotionalLabel', $rule->getPromotionLabel());
                    $xml->startElement('condition');
                    if (!empty($data['products_categories'])) {
                        $xml->startElement('groups');
                        $this->writeAnyAllGroups($xml, $data['products_categories']);
                        $xml->endElement();
                    }
                    if (!empty($data['value_segment'])) {
                        $this->writeCdataElement($xml, 'customerType', implode(',', $data['value_segment']));
                    }
                    $this->writePostCodes($xml, $data['post_codes']);
                    if (!empty($data['dealer_group'])) {
                        $this->writeCdataElement($xml, 'dealerGroup', implode(',', $data['dealer_group']));
                    }
                    $xml->endElement();
                    $xml->startElement('action');
                    foreach (explode(',', $rule->getPromoSku()) as $sku) {
                        $this->writeCdataElement($xml, 'sku', trim($sku));
                    }
                    $xml->endElement();
                    $xml->startElement('targets');
                    $this->writeAnyAllGroups($xml, $data['targets'], false);
                    $xml->endElement();
                    $xml->endElement();
                } else {
                    $this->addError($rule->getId(), 'promotion', 'Rule ' . $rule->getId() . ' has no active products and/or categories');
                }
            }
        }

        $xml->endElement();

        return $xml->outputMemory();
    }

    /**
     * Render price block for given product and price
     *
     * @param Vznl_Catalog_Model_Product $product
     * @param float $priceStart
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function renderPrices(Vznl_Catalog_Model_Product $product, float $priceStart): string
    {
        $taxHelper = Mage::helper('tax');
        $store = Mage::app()->getStore();

        if (!$product->getData('tax_percent')) {
            $rateRequest = Mage::getSingleton('tax/calculation')
                ->getRateRequest(null, null, null, Mage::app()->getStore());
            $taxClassId = $product->getData('tax_class_id');
            $percent = Mage::getSingleton('tax/calculation')->getRate(
                $rateRequest->setProductClassId($taxClassId)
            );
            $product->setData('tax_percent', $percent);
        }

        $priceStart = $store->roundPrice($store->convertPrice($priceStart));
        $price = $taxHelper->getPrice($product, $priceStart, false);
        $priceWithTax = $taxHelper->getPrice($product, $priceStart, true);

        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->writeElement('priceInc', $priceWithTax * 10000);
        $xml->writeElement('priceEx', $price * 10000);
        $xml->writeElement('priceVat', ($priceWithTax - $price) * 10000);
        $xml->writeElement('priceVatPerc', round($product->getData('tax_percent'), 2));
        return $xml->outputMemory();
    }

    /**
     * @param Mage_Catalog_Model_Resource_Product_Collection $products
     * @param array|null $attributesGroup
     * @return string
     * @throws Exception
     */
    protected function handleProducts($products, ?array $attributesGroup = null): string
    {
        $taxHelper = Mage::helper('tax');
        $xml = new XMLWriter();
        $xml->openMemory();
        foreach ($products as $productSkeleton) {
            $product = Mage::getModel('catalog/product')->load($productSkeleton->getId());
            $this->usedProducts[] = $product->getSku();
//            $hawaiiChannel = $product->getProdspecsHawaiiChannel();
//            if (!empty($hawaiiChannel)) {
            $xml->startElement('product');
            $this->writeCdataElement($xml, 'stack', strtoupper($product->getStack()));
            $this->writeCdataElement($xml, 'sku', $product->getSku());
            $this->writeCdataElement($xml, 'name', $product->getName());
            $this->writeCdataElement($xml, 'description', $product->getDescription() ?? '');
            $this->writeCdataElement($xml, 'packageTypeId', strtoupper($product->getPackageType()));
            $this->writeCdataElement(
                $xml,
                'packageSubtypeId',
                strtoupper($product->getAttributeText('package_subtype'))
            );
            $this->handleDateTime(
                $xml,
                'effectiveDate',
                !empty($product->getEffectiveDate()) ? $product->getEffectiveDate() : self::DEFAULT_EFFECTIVE_DATE,
                true
            );
            $this->handleDateTime(
                $xml,
                'expirationDate',
                $product->getExpirationDate() ?? self::DEFAULT_EXPIRATION_DATE
            );
            $xml->startElement('pricingItems');
            $xml->startElement('recurringPrice');
            $mafIncl = $product->getMaf();
            $mafEx = $taxHelper->getPrice($product, $mafIncl, false);
            $devicePriceIncl = $product->getHawaiiSubscrDevicePrice();
            $devicePriceEx = $taxHelper->getPrice($product, $devicePriceIncl, false);

            $consBus = explode(',', $product->getProductSegment());
            // Consumer
            if (in_array(346, $consBus)) {
                $pricePlanPriceIncl = $mafIncl - $devicePriceIncl;
            } // Business
            else {
                $pricePlanPriceEx = $mafEx - $devicePriceEx;
                $pricePlanPriceIncl = $pricePlanPriceEx * ($product->getTaxPercent() / 100 + 1);
            }
            $xml->writeRaw($this->renderPrices($product, $pricePlanPriceIncl));
            $xml->endElement();
            $xml->startElement('onetimePrice');
            $xml->writeRaw($this->renderPrices($product, $product->getData('price')));
            $xml->endElement();
            $xml->endElement();
            $productSegmentRaw = strtolower($product->getAttributeText('product_segment'));
            $this->writeCdataElement(
                $xml,
                'productSegment',
                $this->productSegmentMappings[$productSegmentRaw] ?? $productSegmentRaw
            );
            $lifecycleStatus = strtoupper($product->getAttributeText('lifecycle_status'));
            $this->writeCdataElement(
                $xml,
                'lifecycleStatus',
                !empty($lifecycleStatus)
                    ? $lifecycleStatus
                    : ((strtoupper($product->getStack()) === self::MOBILE_STACK) ? self::DEFAULT_MOBILE_LIFECYCLE : '')
            );
            $this->writeCdataElement(
                $xml,
                'commitmentMonths',
                $product->getAttributeText('identifier_commitment_months') ?? ''
            );
            $this->writeCdataElement($xml, 'attributeSet', strtolower($product->getAttributeSet()));
            $xml->startElement('attributes');
            $xml->writeRaw($this->handleAttributes($product, $attributesGroup));
            $xml->endElement();
            $xml->endElement();
//            } else {
//                $this->addError($product->getSku(), '', 'Product ' . $product->getSku() . ' does not have a Hawaii Channel');
//            }
        }
        $xml->endElement();
        return $xml->outputMemory();
    }

    /**
     * @param Vznl_Catalog_Model_Product $product
     * @param null|array $attributesGroup
     * @return string
     * @throws Exception
     */
    protected function handleAttributes(Vznl_Catalog_Model_Product $product, ?array $attributesGroup = null): string
    {
        if (!empty($attributesGroup)) {
            $xml = new XMLWriter();
            $xml->openMemory();
            foreach ($attributesGroup as $attributeCode) {
                if ($product->hasData($attributeCode)) {
                    $xml->startElement('attribute');
                    $xml->writeElement('key', $attributeCode);
                    $this->handleAttributeValue($xml, $product, $attributeCode);
                    $xml->endElement();
                }
            }
            return $xml->outputMemory();
        }

        return '';
    }

    /**
     * Add the product's attribute text or data
     *
     * @param XMLWriter $xml
     * @param Vznl_Catalog_Model_Product $product
     * @param string $attributeName
     * @throws Exception
     */
    protected function handleAttributeValue(XMLWriter $xml, Vznl_Catalog_Model_Product $product, string $attributeName)
    {
        /** @var Mage_Eav_Model_Attribute $attributeModel */
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(4, $attributeName);

        $isDropdown = ($attributeModel->getFrontendInput() == 'multiselect' || $attributeModel->getFrontendInput() == 'select');
        $isBool = $attributeModel->getFrontendInput() == 'boolean';

        if ($isDropdown) {
            $value = $product->getAttributeText($attributeName);
            if (is_array($value)) {
                $value = implode(',', $value);
            }

            $this->writeCdataElement($xml, 'value', trim($value));
        } elseif ($isBool) {
            $value = trim($product->getData($attributeName) == 1 ? 'true' : 'false');
            $this->writeCdataElement($xml, 'value', $value);
        } else {
            $value = trim($product->getData($attributeName));
            if (is_string($value) && !is_numeric($value)) {
                $this->writeCdataElement($xml, 'value', $value);
            } else {
                $xml->writeElement('value', $value);
            }
        }
    }

    /**
     * @param XMLWriter $xml
     * @param string $attributeName
     * @param null|string $attributeValue
     * @param bool $forceReturn
     */
    protected function handleDateTime(
        XMLWriter $xml,
        string $attributeName,
        ?string $attributeValue = null,
        bool $forceReturn = false
    ) {
        if (!empty($attributeValue)) {
            $date = new \DateTime($attributeValue);
            $this->writeCdataElement($xml, $attributeName, $date->format('c'));
        } else {
            if ($forceReturn) {
                $xml->writeElement($attributeName, '');
            }
        }
    }

    /**
     * @param XMLWriter $xml
     * @param string $key
     * @param string $value
     */
    protected function writeCdataElement(XMLWriter $xml, string $key, ?string $value)
    {
        $xml->startElement($key);
        $xml->writeCdata($value ?? '');
        $xml->endElement();
    }

    /**
     * Add optional product's attributes to the extract
     */
    protected function applyAttributesFiltering()
    {
        $content = file_get_contents('php://input');
        if (!empty($content)) {
            try {
                $filtersXml = new \SimpleXMLElement($content, LIBXML_NOCDATA);
                foreach ($filtersXml->children() as $allowedAttributesGroup) {
                    if ($this->enabled['mobile']) {
                        $this->mobileParser->parseAllowedAttributesGroup($allowedAttributesGroup);
                    }
                    if ($this->enabled['fixed']) {
                        $this->fixedParser->parseAllowedAttributesGroup($allowedAttributesGroup);
                    }
                }
            } catch (Exception $exception) {
                $this->addError(
                    '',
                    'AttributesFiltering',
                    sprintf('Invalid filtering content. %s', $exception->getMessage())
                );
            }
        }
    }

    /**
     * @param XMLWriter $xml
     * @param array $row
     */
    private function getCompatibilitiesSourceTarget(XMLWriter $xml, array $row)
    {
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        switch ($row['operationType']) {
            case 1: // product-product
                $sourceTargetQuery = $write->query('
                    SELECT cpe.entity_id, cpe.sku AS name FROM catalog_product_entity cpe
                    WHERE cpe.entity_id IN (' . implode(',', [$row['source'], $row['target']]) . ')
                ');
                $sourceKey = $targetKey = 'sku';
                break;
            case 2: // product-category
                $sourceTargetQuery = $write->query('
                    SELECT cpe.entity_id, cpe.sku AS name FROM catalog_product_entity cpe
                    WHERE cpe.entity_id = "' . $row['source'] . '"
                    UNION
                    SELECT cce.entity_id, cce.unique_id AS name FROM catalog_category_entity cce
                    WHERE cce.entity_id = "' . $row['target'] . '"
                ');
                $sourceKey = 'sku';
                $targetKey = 'categoryId';
                break;
            case 3: // category-category
                $sourceTargetQuery = $write->query('
                    SELECT cce.entity_id, cce.unique_id AS name FROM catalog_category_entity cce
                    WHERE cce.entity_id IN (' . implode(',', [$row['source'], $row['target']]) . ')
                ');
                $sourceKey = $targetKey = 'categoryId';
                break;
            case 4: // category-product
                $sourceTargetQuery = $write->query('
                    SELECT cce.entity_id, cce.unique_id AS name FROM catalog_category_entity cce
                    WHERE cce.entity_id = "' . $row['source'] . '"
                    UNION
                    SELECT cpe.entity_id, cpe.sku AS name FROM catalog_product_entity cpe
                    WHERE cpe.entity_id = "' . $row['target'] . '"
                ');
                $sourceKey = 'categoryId';
                $targetKey = 'sku';
                break;
            default:
                $sourceKey = $targetKey = '';
        }

        if (!empty($sourceTargetQuery)) {
            $sourceTargetResult = $sourceTargetQuery->fetchAll();
        }

        $sourceTarget = [];
        foreach ($sourceTargetResult ?? [] as $item) {
            $sourceTarget[$item['entity_id']] = $item['name'];
        }

        if (!empty($sourceTarget)) {
            $xml->startElement('source');
            $this->writeCdataElement($xml, $sourceKey, $sourceTarget[$row['source']] ?? null);
            $xml->endElement();
            $xml->startElement('target');
            $this->writeCdataElement($xml, $targetKey, $sourceTarget[$row['target']] ?? null);
            $xml->endElement();
        }
    }

    /**
     * @param array $dataProvided
     * @return array
     */
    protected function parseAnyAllGroupsArray(array $dataProvided): array
    {
        $arrayAny = [];
        foreach ($dataProvided as $data) {
            $data = explode(',', $data);
            $dataAll = [];
            if (is_array($data)) {
                foreach ($data as $item) {
                    $dataAll[] = [
                        'type' => is_numeric($item) || is_numeric(substr($item, 2)) ? 'sku' : 'categoryId',
                        'value' => $item
                    ];
                }
                $arrayAny[] = $dataAll;
            } else {
                $arrayAny[] = [
                    'type' => is_numeric($data) || is_numeric(substr($data, 2)) ? 'sku' : 'categoryId',
                    'value' => $data
                ];
            }
        }

        return $arrayAny;
    }

    /**
     * @param XMLWriter $xml
     * @param array $data
     * @param bool $addParent
     */
    protected function writeAnyAllGroups(XMLWriter $xml, ?array $data, bool $addParent = true)
    {
        foreach ($this->parseAnyAllGroupsArray($data ?? []) as $items) {
            !$addParent ?: $xml->startElement('group');
            if (is_array($items)) {
                foreach ($items as $item) {
                    $this->writeCdataElement($xml, $item['type'], $item['value']);
                }
            } else {
                $this->writeCdataElement($xml, $items['type'], $items['value']);
            }
            !$addParent ?: $xml->endElement();
        }
    }

    /**
     * @param XMLWriter $xml
     * @param array $postCodesData
     */
    protected function writePostCodes(XMLWriter $xml, ?array $postCodesData)
    {
        if (!empty($postCodesData)) {
            $xml->startElement('postcodes');
            foreach ($postCodesData as $postCodes) {
                $postCodes = explode(',', $postCodes);
                foreach ($postCodes as $postCode) {
                    $this->writeCdataElement($xml, 'postcode', trim($postCode));
                }
            }
            $xml->endElement();
        }
    }

    /**
     * @param $rule
     * @param array $productsData
     * @return array
     */
    protected function checkUnknownProducts($rule, ?array $productsData)
    {
        $skuCollection = [];
        foreach ($productsData ?? [] as $products) {
            $products = explode(',', $products);
            foreach ($products as $sku) {
                $sku = trim($sku);
                if (in_array($sku, $this->usedProducts)) {
                    $skuCollection[] = $sku;
                } else {
                    $this->addError(
                        $rule->getId(),
                        'promotion',
                        sprintf(
                            'Rule %s has inactive product "%s"',
                            $rule->getId(),
                            $sku
                        )
                    );
                }
            }
        }

        return $skuCollection;
    }

    /**
     * @param $rule
     * @param array $categoriesData
     * @return array
     */
    protected function checkUnknownCategories($rule, ?array $categoriesData)
    {
        $categoryIdCollection = [];
        foreach ($categoriesData ?? [] as $categories) {
            $categories = explode(',', $categories);
            foreach ($categories as $categoryId) {
                $categoryId = trim($categoryId);
                if (in_array($categoryId, $this->usedCategories)) {
                    $categoryIdCollection[] = $categoryId;
                } else {
                    $this->addError(
                        $rule->getId(),
                        'promotion',
                        sprintf(
                            'Rule %s has inactive category "%s"',
                            $rule->getId(),
                            $categoryId
                        )
                    );
                }
            }
        }

        return $categoryIdCollection;
    }

    /**
     * @param string $stacks
     * @return void
     */
    protected function setStack(string $stacks)
    {
        foreach (explode(',', $stacks) ?? [] as $stack) {
            if (in_array($stack, ['mobile', 'fixed'])) {
                $this->enabled[$stack] = true;
            }
        }
    }

    /**
     * @return array
     */
    protected function getStack(): array
    {
        $return = [];
        foreach ($this->enabled ?? [] as $stack => $value) {
            if ($value === true) {
                $return[] = $stack;
            }
        }
        return $return;
    }
}
