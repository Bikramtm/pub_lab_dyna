<?php

/**
 * Dependencies:
 * Dyna_Aikido_Helper_Data
 * Dyna_Catalog_Model_Product
 * Dyna_Catalog_Model_Type
 * Dyna_Checkout_Helper_Data
 * Dyna_PriceRules_Model_Observer
 * Vznl_Configurator_Model_Cache
 * Omnius_MixMatch_Model_Resource_Price_Collection
 */
class Vznl_RestApi_Catalogue_GetNew extends Vznl_RestApi_Catalogue_Get
{
    /**
     * @var array
     */
    public $accesoryProductsAttributes = [
        'sku',
        'name',
        'product_segment',
        'prodspecs_brand',
        'description',
        'prodspecs_afmetingen_lengte',
        'prodspecs_afmetingen_breedte',
        'prodspecs_afmetingen_dikte',
        'prodspecs_afmetingen_unit',
        'prodspecs_weight_gram',
        'prodspecs_kleur',
        'identifier_hawaii_type',
        'identifier_hawaii_id',
        'prodspecs_hawaii_preorder',
        'identifier_hawaii_type_name',
        'prodspecs_hawaii_html_kleurcode',
        'prodspecs_hawaii_usp1',
        'prodspecs_hawaii_usp2',
        'prodspecs_hawaii_usp3',
        'prodspecs_network',
        'prodspecs_network_3g',
        'prodspecs_network_4g',
        'prodspecs_ecoscore',
        'prodspecs_hawaii_os_versie',
        'identifier_hawaii_prep_orig_id',
        'prijs_thuiskopieheffing_bedrag',
        'prodspecs_hawaii_channel',
        'regular_price',
    ];

    /**
     * @var array
     */
    public $subscriptionProductsAttributes = [
        'sku',
        'name',
        'hawaii_subscr_family',
        'hawaii_subscr_kind',
        'product_segment',
        'description',
        'prodspecs_network',
        'prodspecs_4g_lte',
        'prodspecs_aantal_belminuten',
        'prodspecs_data_aantal_mb',
        'prodspecs_sms_amount',
        'identifier_hawaii_id',
        'identifier_hawaii_subs_formula',
        'identifier_hawaii_subscription',
        'prodspecs_hawaii_def_phone',
        'prodspecs_hawaii_usp1',
        'prodspecs_hawaii_usp2',
        'prodspecs_hawaii_usp3',
        'prijs_belminuut_buiten_bundel',
        'prodspecs_hawaii_channel',
        'hawaii_subscr_hidden',
    ];

    /**
     * @var array
     */
    public $internetSubscriptionProductsAttributes = [
        'sku',
        'name',
        'hawaii_subscr_family',
        'hawaii_subscr_kind',
        'product_segment',
        'description',
        'prodspecs_network',
        'prodspecs_4g_lte',
        'prodspecs_aantal_belminuten',
        'prodspecs_data_aantal_mb',
        'prodspecs_sms_amount',
        'identifier_hawaii_id',
        'identifier_hawaii_subscr_orig',
        'identifier_hawaii_subs_formula',
        'identifier_hawaii_subscription',
        'prodspecs_hawaii_def_phone',
        'prodspecs_hawaii_usp1',
        'prodspecs_hawaii_usp2',
        'prodspecs_hawaii_usp3',
        'prodspecs_hawaii_channel',
        'hawaii_subscr_hidden',
    ];

    public $priceplanChildAttributes = [
        "sku",
        "name",
        "identifier_hawaii_id",
        "identifier_commitment_months",
        "priceplan_price",
        "device_price",
        "maf",
        "prijs_aansluitkosten",
        "prijs_aansluitkosten_ret",
        "identifier_subsc_postp_simonly",
        "aikido",
    ];

    public $addonProductAttributes = [
        "sku",
        "name",
        "identifier_commitment_months",
        "description",
        "prodspecs_4g_lte",
        "identifier_hawaii_type",
        "identifier_hawaii_id",
        "prodspecs_hawaii_usp1",
        "prodspecs_hawaii_usp2",
        "prodspecs_hawaii_usp3",
        "prodspecs_data_mb_config",
        "prodspecs_belmin_sms_config",
        "prijs_belminuut_buiten_bundel",
        "hawaii_hybride_type",
        "hawaii_hybride_voice_data",
        "hawaii_is_default",
        "prodspecs_hawaii_channel",
        "maf",
    ];

    public $deviceRCProductAttributes = [
        "sku",
        "name",
        "identifier_hawaii_id",
        "identifier_addon_type",
        "loan_indicator",
        "maf",
    ];

    public $cacheKeyPrefix = 'hawaii_rest_catalog_3_';

    protected $lifecycleMappings = [
        Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITION => Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE,
        Vznl_Checkout_Model_Sales_Quote_Item::RETENTION => Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE
    ];

    /**
     * Render the SSFE promotions and add them to a logic flat XML format
     * @return string
     */
    protected function handlePromotion()
    {
        $result = null;

        // all promo rules
        $ruleCollection = Mage::getResourceModel('salesrule/rule_collection')
            ->addWebsiteFilter(Mage::app()->getWebsite()->getId())
            ->addFieldToFilter('is_active', true)
            ->addFieldToFilter('coupon_type', 1); // Only no coupons
        foreach ($ruleCollection as $rule) {
            try {
                $interpreter = new Vznl_RestApi_Catalogue_SalesRuleInterpreter();
                $dataRules = $interpreter->interpret($rule);
            } catch (Exception $e) {
                $this->addError(
                    $rule->getId(),
                    'promotion',
                    'Could not interpret rule ' . $rule->getId() . ': ' . $e->getMessage()
                );
                continue;
            }

            // Loop through rules
            foreach ($dataRules as $lifecycle => $data) {
                $data['customer_value'] = is_array($data['customer_value']) ? $data['customer_value'] : [$data['customer_value']];
                $data['value_segment'] = is_array($data['value_segment']) ? $data['value_segment'] : [$data['value_segment']];
                $data['campaign'] = is_array($data['campaign']) ? $data['campaign'] : [$data['campaign']];
                $data['products'] = is_array($data['products']) ? $data['products'] : [$data['products']];

                $oncePromoPrice = 0;
                $onceDiscount = 0;
                $recurringPromoPrice = 0;
                $recurringDiscount = 0;

                // Check which action to render
                switch ($rule->getSimpleAction()) {
                    case 'by_fixed':
                        $promoProduct = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId()); // Fake the promo product as empty shell
                        $oncePromoPrice = -$rule->getDiscountAmount();
                        $onceDiscount = $rule->getDiscountAmount();
                        $promotionTitle = $rule->getName();
                        break;
                    case 'ampromo_items':
                        $parts = explode(',', $data['products'][0]);
                        $product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId());
                        $product = $product->load($product->getIdBySku(trim($parts[0])));
                        $promoProduct = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId());
                        $promoProduct = $promoProduct->load($promoProduct->getIdBySku($rule->getPromoSku()));
                        $promotionTitle = $promoProduct->getName();
                        if ($promoProduct->getPrijsMafDiscountPercent()) {
                            $recurringPromoPrice = $product->getMaf() - ($product->getMaf() / 100 * $promoProduct->getPrijsMafDiscountPercent());
                            $recurringDiscount = $product->getMaf() - $recurringPromoPrice;
                        }
                        if ($promoProduct->getPrijsMafNewAmount()) {
                            $recurringPromoPrice = $promoProduct->getPrijsMafNewAmount();
                            $recurringDiscount = $product->getMaf() - $recurringPromoPrice;
                        }
                        if ($promoProduct->getPrijsAansluitPromoBedrag()) {
                            $oncePromoPrice = $promoProduct->getPrijsAansluitPromoBedrag();
                            $onceDiscount = $product->getPrijsAansluitkosten() - $oncePromoPrice;
                        }
                        if ($promoProduct->getPrijsAansluitPromoProcent()) {
                            $oncePromoPrice = $product->getPrijsAansluitkosten() - ($product->getPrijsAansluitkosten() / 100 * $promoProduct->getPrijsAansluitPromoProcent());
                            $onceDiscount = $product->getPrijsAansluitkosten() - $recurringPromoPrice;
                        }
                        break;
                    default:
                        $this->addError(
                            $rule->getId(),
                            'promotion',
                            'Could not interpret rule type "' . $rule->getSimpleAction() . '" from rule ' . $rule->getId()
                        );
                        break;
                }

                // Check for unknown products
                $skuCollection = [];
                foreach ($data['products'] as $products) {
                    $products = explode(',', $products);
                    foreach ($products as $sku) {
                        $sku = trim($sku);
                        if (in_array($sku, $this->usedProducts)) {
                            $skuCollection[] = $sku;
                        } else {
                            $this->addError(
                                $rule->getId(),
                                'promotion',
                                'Rule ' . $rule->getId() . ' has inactive product "' . $sku . '"'
                            );
                        }
                    }
                }
                $skuCollectionCount = sizeof($skuCollection);
                if ($skuCollectionCount > 0) {
                    // Setup price blocks
                    $prices = null;
                    $prices .= '<one_time_promo_price>' . $this->renderPrice(
                            $result, $oncePromoPrice, $promoProduct
                        ) . '</one_time_promo_price>' . PHP_EOL;
                    $prices .= '<one_time_discount>' . $this->renderPrice(
                            $result, $onceDiscount, $promoProduct
                        ) . '</one_time_discount>' . PHP_EOL;
                    $prices .= '<recurring_promo_price>' . $this->renderPrice(
                            $result, $recurringPromoPrice, $promoProduct
                        ) . '</recurring_promo_price>' . PHP_EOL;
                    $prices .= '<recurring_discount>' . $this->renderPrice(
                            $result, $recurringDiscount, $promoProduct
                        ) . '</recurring_discount>' . PHP_EOL;

                    if (!is_null($prices)) {
                        foreach ($data['customer_value'] as $customerValue) {
                            foreach ($data['value_segment'] as $valueSegment) {
                                foreach ($data['campaign'] as $campaign) {
                                    foreach ($data['products'] as $products) {
                                        $result .= '<promo>' . PHP_EOL;
                                        $result .= '<sku>d_' . $promoProduct->getSku() . '</sku>' . PHP_EOL;
                                        $result .= '<sales_rule_id>' . $rule->getId() . '</sales_rule_id>' . PHP_EOL;
                                        $result .= '<start_date>' . (!is_null($rule->getFromDate()) ? strtotime($rule->getFromDate()) . '000' : '') . '</start_date>' . PHP_EOL;
                                        $result .= '<end_date>' . (!is_null($rule->getToDate()) ? strtotime($rule->getToDate()) . '000' : '') . '</end_date>' . PHP_EOL;
                                        $result .= '<duration>' . $promoProduct->getPrijsPromoDuurMaanden() . '</duration>' . PHP_EOL;
                                        $result .= '<text><![CDATA[' . $promotionTitle . ']]></text>' . PHP_EOL;
                                        $result .= '<customer_value>' . $customerValue . '</customer_value>' . PHP_EOL;
                                        $result .= '<customer_lifecycle><![CDATA[' . ($this->lifecycleMappings[$lifecycle] ?? $lifecycle) . ']]></customer_lifecycle>' . PHP_EOL;
                                        $result .= '<value_segment>' . $valueSegment . '</value_segment>' . PHP_EOL;
                                        $result .= '<campaign_code>' . $campaign . '</campaign_code>' . PHP_EOL;
                                        $result .= '<products>';
                                        foreach ($skuCollection as $sku) {
                                            $result .= '<sku>' . $sku . '</sku>';
                                        }
                                        $result .= '</products>' . PHP_EOL;
                                        $result .= $prices;
                                        $result .= '<add_product_sku>' . $rule->getPromoSku() . '</add_product_sku>' . PHP_EOL;
                                        $result .= '</promo>' . PHP_EOL;
                                    }
                                }
                            }
                        }
                    } else {
                        $this->addError(
                            $rule->getId(),
                            'promotion',
                            'Rule ' . $rule->getId() . ' could not render the given price products'
                        );
                    }
                } else {
                    $this->addError($rule->getId(), 'promotion', 'Rule ' . $rule->getId() . ' has no active products');
                }
            }
        }

        return $result;
    }

    /**
     * Get all simonly products the are connected to a parent configurable
     * @return string
     */
    protected function handleSimonlyMixmatch()
    {
        $i = 0;
        $checkoutHelper = Mage::helper('dyna_checkout');
        $result = null;
        // Device subscription
        foreach ($this->subscriptionProductsConfigurable as $configurableSubscription) {
            $subscriptionConf = Mage::getModel('catalog/product_type_configurable')->setProduct($configurableSubscription);
            $subscriptionCol = $subscriptionConf
                ->getUsedProductCollection()
                ->addAttributeToSelect('sku')
                ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
                ->addFilterByRequiredOptions()
                ->addStoreFilter();
            foreach ($subscriptionCol as $simpleSubscription) {
                if ($this->debug && $i > self::DEBUG_INDEX) {
                    break;
                }
                $subscription = Mage::getModel('catalog/product')->load($simpleSubscription->getId());
                if ($subscription->getData('identifier_subsc_postp_simonly') == 221 || $subscription->getAikido()) {
                    $mixMatches = $checkoutHelper->getMixMatchPrices($subscription->getSku(), null);
                    $result .= '<match>' . PHP_EOL;
                    $result .= '<sku>s_' . $subscription->getSku() . '</sku>' . PHP_EOL;
                    $result .= '<subscription_sku>' . $subscription->getSku() . '</subscription_sku>' . PHP_EOL;
                    $result .= '<price>' . PHP_EOL;
                    $result .= $this->renderPrice(
                            $result,
                            $subscription->getPrice(),
                            $subscription,
                            $mixMatches[$subscription->getSku()]
                        ) . PHP_EOL;
                    $result .= '</price>' . PHP_EOL;
                    $result .= '</match>' . PHP_EOL;
                    $i++;
                }
            }
        }
        // Internet
        foreach ($this->internetSubscriptionProductsConfigurable as $configurableSubscription) {
            $subscriptionConf = Mage::getModel('catalog/product_type_configurable')->setProduct($configurableSubscription);
            $subscriptionCol = $subscriptionConf
                ->getUsedProductCollection()
                ->addAttributeToSelect('sku')
                ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
                ->addFilterByRequiredOptions()
                ->addStoreFilter();
            foreach ($subscriptionCol as $simpleSubscription) {
                if ($this->debug && $i > self::DEBUG_INDEX) {
                    break;
                }
                $subscription = Mage::getModel('catalog/product')->load($simpleSubscription->getId());
                if ($subscription->getData('identifier_subsc_postp_simonly') == 221) {
                    $mixMatches = $checkoutHelper->getMixMatchPrices($subscription->getSku(), null);
                    $result .= '<match>' . PHP_EOL;
                    $result .= '<sku>s_' . $subscription->getSku() . '</sku>' . PHP_EOL;
                    $result .= '<subscription_sku>' . $subscription->getSku() . '</subscription_sku>' . PHP_EOL;
                    $result .= '<price>' . PHP_EOL;
                    $result .= $this->renderPrice(
                            $result,
                            $subscription->getPrice(),
                            $subscription,
                            $mixMatches[$subscription->getSku()]
                        ) . PHP_EOL;
                    $result .= '</price>' . PHP_EOL;
                    $result .= '</match>' . PHP_EOL;
                    $i++;
                }
            }
        }

        return $result;
    }

    /**
     * Get all mimatch records from the SSFE mixmatch table
     * @return string
     */
    protected function handlePostpaidMixmatch()
    {
        $result = null;
        $request = Mage::getSingleton('tax/calculation')->getRateRequest(null, null, null, Mage::app()->getStore());
        $product = Mage::getModel('catalog/product');
        $i = 0;

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $queryResult = $write->query("select cm.*, cpei_tax.value as tax_class_id
            from catalog_mixmatch cm
            join catalog_product_entity cpe_device on cpe_device.sku = cm.target_sku
            join catalog_product_website cpw_device on cpe_device.entity_id = cpw_device.product_id
            join catalog_product_entity cpe_subscription on cpe_subscription.sku = cm.source_sku
            join catalog_product_website cpw_subscription on cpe_subscription.entity_id = cpw_subscription.product_id
            join catalog_product_entity_int cpei_tax on cpe_device.entity_id = cpei_tax.entity_id and cpei_tax.attribute_id = 121
            join catalog_product_entity_int cpei_devicestatus on cpei_devicestatus.entity_id = cpe_device.entity_id and cpei_devicestatus.attribute_id = 96
            join catalog_product_entity_int cpei_subscriptionstatus on cpei_subscriptionstatus.entity_id = cpe_subscription.entity_id and cpei_subscriptionstatus.attribute_id = 96
            where cm.website_id = " . Mage::app()->getStore()->getWebsiteId() . "
            and cpw_device.website_id = " . Mage::app()->getStore()->getWebsiteId() . "
            and cpw_subscription.website_id = " . Mage::app()->getStore()->getWebsiteId() . "
            and cpei_devicestatus.value = 1
            and cpei_subscriptionstatus.value = 1
        ");
        while ($row = $queryResult->fetch()) {
            $product->setData('tax_class_id', $row['tax_class_id']);
            $product->setData(
                'tax_percent',
                Mage::getSingleton('tax/calculation')->getRate($request->setProductClassId($row['tax_class_id']))
            );
            if ($this->debug && $i > self::DEBUG_INDEX) {
                break;
            }
            $result .= '<match>' . PHP_EOL;
            $result .= '<sku>' . implode('_',
                    array_filter(
                        [$row['target_sku'], $row['source_sku'], $row['device_subscription_sku']],
                        function ($entry) {
                            return (!empty($entry));
                        }
                    )
                ) . '</sku>' . PHP_EOL;
            $result .= '<phone_sku>' . $row['target_sku'] . '</phone_sku>' . PHP_EOL;
            $result .= '<subscription_sku>' . $row['source_sku'] . '</subscription_sku>' . PHP_EOL;
            $result .= '<devicerc_sku>' . $row['device_subscription_sku'] . '</devicerc_sku>' . PHP_EOL;
            $result .= '<customer_lifecycle><![CDATA[';
            if ($row['device_subscription_sku'] == Mage::helper('vznl_checkout/aikido')->getDefaultRetentionSku()) {
                $result .= Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE;
            } elseif ($row['device_subscription_sku'] == Mage::helper('vznl_checkout/aikido')->getDefaultAcquisitionSku()) {
                $result .= Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE;
            } else {
                $result .= '';
            }
            $result .= ']]></customer_lifecycle>' . PHP_EOL;

            $result .= '<price>' . PHP_EOL;
            $result .= $this->renderPrice($result, $row['price'], $product) . PHP_EOL;
            $result .= '</price>' . PHP_EOL;
            $result .= '</match>' . PHP_EOL;
            $i++;
        }

        return $result;
    }

    /**
     * Handle the mixmatch for all prepaid phones
     * @param $result
     * @return string
     */
    protected function handlePrepaidPhoneMixmatches($result)
    {
        $checkoutHelper = Mage::helper('dyna_checkout');
        $i = 0;
        foreach ($this->phoneProductsConfigurable as $configurablePhone) {
            $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($configurablePhone);
            $col = $conf
                ->getUsedProductCollection()
                ->addAttributeToSelect('sku')
                ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
                ->addFilterByRequiredOptions()
                ->addStoreFilter();
            foreach ($col as $simplePhone) {
                $phone = Mage::getModel('catalog/product')->load($simplePhone->getId());
                if ($phone->getAttributeText('identifier_device_post_prepaid') === 'prepaid') {
                    if ($this->debug && $i > self::DEBUG_INDEX) {
                        break;
                    }
                    $mixMatches = $checkoutHelper->getMixMatchPrices(null, $phone->getSku());
                    $result .= '<match>' . PHP_EOL;
                    $result .= '<sku>p_' . $phone->getSku() . '</sku>' . PHP_EOL;
                    $result .= '<phone_sku>' . $phone->getSku() . '</phone_sku>' . PHP_EOL;
                    $result .= '<price>' . PHP_EOL;
                    $result .= $this->renderPrice(
                            $result,
                            $phone->getPrice(),
                            $phone,
                            $mixMatches[$phone->getSku()]
                        ) . PHP_EOL;
                    $result .= '</price>' . PHP_EOL;
                    $result .= '</match>' . PHP_EOL;
                    $i++;
                }
            }
        }

        return $result;
    }

    /**
     * Handle the mixmatch for all data devices
     * @param $result
     * @return string
     */
    protected function handleDataDeviceMixmatches($result)
    {
        $checkoutHelper = Mage::helper('dyna_checkout');
        $i = 0;
        foreach ($this->internetProductsConfigurable as $configurable) {
            $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($configurable);
            $col = $conf
                ->getUsedProductCollection()
                ->addAttributeToSelect('sku')
                ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
                ->addFilterByRequiredOptions()
                ->addStoreFilter();
            foreach ($col as $simple) {
                $product = Mage::getModel('catalog/product')->load($simple->getId());
                if ($product->getAttributeText('identifier_device_post_prepaid') === 'prepaid') {
                    if ($this->debug && $i > self::DEBUG_INDEX) {
                        break;
                    }
                    $mixMatches = $checkoutHelper->getMixMatchPrices(null, $product->getSku());
                    $result .= '<match>' . PHP_EOL;
                    $result .= '<sku>p_' . $product->getSku() . '</sku>' . PHP_EOL;
                    $result .= '<phone_sku>' . $product->getSku() . '</phone_sku>' . PHP_EOL;
                    $result .= '<price>' . PHP_EOL;
                    $result .= $this->renderPrice(
                            $result,
                            $product->getPrice(),
                            $product,
                            $mixMatches[$product->getSku()]
                        ) . PHP_EOL;
                    $result .= '</price>' . PHP_EOL;
                    $result .= '</match>' . PHP_EOL;
                    $i++;
                }
            }
        }

        return $result;
    }

    /**
     * Create the mixmatch for all prepaid phones and data devices
     * @return string
     */
    protected function handlePrepaidMixmatch()
    {
        $result = null;

        // Phones prepaid
        $result = $this->handlePrepaidPhoneMixmatches($result);

        // Data devices prepaid
        $result = $this->handleDataDeviceMixmatches($result);

        // Prepaid simonly
        $i = 0;
        foreach ($this->prepaidSimProducts as $prepaidSim) {
            if ($this->debug && $i > self::DEBUG_INDEX) {
                break;
            }
            $result .= '<match>' . PHP_EOL;
            $result .= '<sku>p_' . $prepaidSim->getSku() . '</sku>' . PHP_EOL;
            $result .= '<phone_sku>' . $prepaidSim->getSku() . '</phone_sku>' . PHP_EOL;
            $result .= '<price>' . PHP_EOL;
            $result .= $this->renderPrice($result, $prepaidSim->getPrice(), $prepaidSim) . PHP_EOL;
            $result .= '</price>' . PHP_EOL;
            $result .= '</match>' . PHP_EOL;
            $i++;
        }

        return $result;
    }

    /**
     * @param $subscription
     * @param $device
     * @return array|bool
     */
    protected function productHasMixmatch($subscription, $device)
    {
        $subscriptionSku = is_null($subscription) ? null : $subscription->getSku();
        $deviceSku = is_null($device) ? null : $device->getSku();

        if (!is_null($subscriptionSku)) {
            $mixMatches = Mage::getResourceModel('omnius_mixmatch/price_collection')
                ->addFieldToSelect(['source_sku', 'target_sku', 'device_subscription_sku', 'price'])
                ->addFieldToFilter('website_id', Mage::app()->getWebsite()->getId())
                ->addFieldToFilter('source_sku', $subscriptionSku);
        } else {
            $mixMatches = Mage::getResourceModel('omnius_mixmatch/price_collection')
                ->addFieldToSelect(['source_sku', 'target_sku', 'device_subscription_sku', 'price'])
                ->addFieldToFilter('website_id', Mage::app()->getWebsite()->getId())
                ->addFieldToFilter('target_sku', $deviceSku);
        }

        if (count($mixMatches) == 0) {
            return false;
        }

        return $mixMatches;
    }

    /**
     * Render all product attributes.
     * @param object $product
     * @param array $attributes
     * @return string
     */
    protected function renderElements($product, $attributes, $handle = null)
    {
        $taxHelper = Mage::helper('tax');
        $store = Mage::app()->getStore();
        $handle = '';
        $result = null;
        foreach ($attributes as $attribute) {
            switch ($attribute) {
                case 'regular_price':
                    $result .= $this->getAttributeElementPrice($product, 'price', 'price', 'regular_price');
                    break;
                case 'prepaid_monthly_fee': // prepaid simonly
                case 'phone_price': // prepaid simonly
                    $result .= $this->getAttributeElementPrice($product, $attribute, null, $attribute);
                    break;
                case 'regular_price_priceplan':
                    $result .= $this->getAttributeElementPrice($product, 'maf', 'maf', 'regular_price');
                    break;
                case 'priceplan_price':
                    $mafIncl = $product->getMaf();
                    $mafEx = $taxHelper->getPrice($product, $mafIncl, false);
                    $devicePriceIncl = $product->getHawaiiSubscrDevicePrice();
                    $devicePriceEx = $taxHelper->getPrice($product, $devicePriceIncl, false);

                    $consBus = explode(',', $product->getProductSegment());
                    // Consumer
                    if (in_array(346, $consBus)) {
                        $priceplanPriceIncl = $mafIncl - $devicePriceIncl;
                    } // Business
                    else {
                        $priceplanPriceEx = $mafEx - $devicePriceEx;
                        $priceplanPriceIncl = $priceplanPriceEx * ($product->getTaxPercent() / 100 + 1);
                    }
                    $result .= $this->getAttributeElementPrice($product, $priceplanPriceIncl, null, $attribute);
                    break;
                case 'maf':
                    $result .= $this->getAttributeElementPrice($product, 'maf', 'maf', 'regular_maf');
                    break;
                case 'device_price':
                    $result .= $this->getAttributeElementPrice($product, 'hawaii_subscr_device_price',
                        'hawaii_subscr_device_price', $attribute);
                    break;
                case 'recurring_price':
                    $result .= $this->getAttributeElementPrice($product, 'maf', 'maf', $attribute);
                    break;
                case 'prijs_thuiskopieheffing_bedrag':
                    $thuisKopieHeffingType = $product->getAttributeText('prijs_thuiskopieheffing_type');
                    $sku = Mage::getStoreConfig(Dyna_PriceRules_Model_Observer::COPY_LEVY_PREFIX . strtolower($thuisKopieHeffingType));
                    if (!empty($sku)) {
                        $thuiskopieHeffing = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId());
                        $thuiskopieHeffing = $thuiskopieHeffing->load($thuiskopieHeffing->getIdBySku($sku));
                        $result .= $this->getAttributeElementPrice($product,
                            $store->roundPrice($thuiskopieHeffing->getPrice()), 'prijs_thuiskopieheffing_bedrag',
                            $attribute);
                    } else {
                        $result .= $this->getAttributeElementPrice($product, 0, 'prijs_thuiskopieheffing_bedrag',
                            $attribute);
                    }
                    break;
                case 'prijs_aansluitkosten':
                    $result .= $this->getAttributeElementPrice($product, 'prijs_aansluitkosten', 'prijs_aansluitkosten',
                        $attribute);
                    break;
                case 'prijs_aansluitkosten_ret':
                    $result .= $this->getAttributeElementPrice($product, 'prijs_aansluitkosten_ret', 'prijs_aansluitkosten_ret', $attribute);
                    break;
                case 'is_hybrid_sim':
                    $value = 0;
                    $types = explode(',', $product->getIdentifierPackageType());
                    if (in_array(250, $types)) {
                        $value = 1;
                    }
                    $result .= '<' . $attribute . '>' . $value . '</' . $attribute . '>';
                    break;
                case Vznl_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE:
                    $sim = Mage::getModel('catalog/product')
                        ->getCollection()
                        ->addFieldToFilter('attribute_set_id', 53)
                        ->addAttributeToFilter(Vznl_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE,
                            $product->getData($attribute))
                        ->addAttributeToFilter('type_id', ['eq' => 'simple'])
                        ->addAttributeToFilter('is_deleted', ['eq' => false])
                        ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
                        ->addStoreFilter()
                        ->setPageSize(1, 1)
                        ->getLastItem();
                    $result .= $this->createShell($attribute, ($sim ? $sim->getSku() : null));
                    break;
                // Cast to boolean
                case 'hawaii_is_default':
                    $result .= '<' . $attribute . '>' . (int)$product->getData($attribute) . '</' . $attribute . '>';
                    break;
                case 'identifier_subsc_postp_simonly':
                    $result .= '<' . $attribute . '>' . ($product->getData($attribute) == 221 ? 1 : 0) . '</' . $attribute . '>';
                    break;
                case 'hawaii_subscr_hidden':
                    $result .= '<' . $attribute . '>' . ($product->getData('hawaii_subscr_hidden') == '' ? '0' : $product->getData('hawaii_subscr_hidden')) . '</' . $attribute . '>';
                    break;
                case 'aikido':
                    $result .= '<' . $attribute . '>' . (int)$product->getData($attribute) . '</' . $attribute . '>';
                    break;
                case 'product_segment':
                    $result .= $this->printElement($product, $attribute, 'identifier_cons_bus');
                    break;
                default:
                    $result .= $this->printElement($product, $attribute);
                    break;
            }
        }

        return $result;
    }

    /**
     * Render price block for given price and product
     * @param string $result
     * @param float $priceStart
     * @param object $product
     * @return string
     */
    protected function renderPrice($result, $priceStart, $product)
    {
        $taxHelper = Mage::helper('tax');
        $store = Mage::app()->getStore();

        if (!$product->getData('tax_percent')) {
            $request = Mage::getSingleton('tax/calculation')->getRateRequest(null, null, null, Mage::app()->getStore());
            $taxclassid = $product->getData('tax_class_id');
            $percent = Mage::getSingleton('tax/calculation')->getRate($request->setProductClassId($taxclassid));
            $product->setData('tax_percent', $percent);
        }

        $priceStart = $store->roundPrice($store->convertPrice($priceStart));
        $price = $taxHelper->getPrice($product, $priceStart, false);
        $priceWithTax = $taxHelper->getPrice($product, $priceStart, true);
        $priceTax = round($priceWithTax - $price, 2);
        $priceVatPercentage = $product->getData('tax_percent');

        $result = '<price_inc>' . round($priceWithTax, 2) . '</price_inc>
            <price_ex>' . round($price, 2) . '</price_ex>
            <price_vat>' . $priceTax . '</price_vat>
            <price_vat_perc>' . round($priceVatPercentage, 2) . '</price_vat_perc>';

        return $result;
    }
}
