<?php

/**
 * Class Vznl_RestApi_Catalogue_Helper_Parser
 */
abstract class Vznl_RestApi_Catalogue_Helper_Parser
{
    /**
     * Set all products
     */
    protected abstract function loadProducts();

    /**
     * @return array
     */
    public abstract function getProductsWithAttributeSet(): array;

    /**
     * @param SimpleXMLElement $allowedAttributesGroup
     */
    public function parseAllowedAttributesGroup(?SimpleXMLElement $allowedAttributesGroup)
    {
        foreach ($allowedAttributesGroup ?? [] as $group => $allowedAttributes) {
            if (property_exists(static::class, $group)) {
                // Empty attributes at start
                $this->{$group} = [];
                foreach ($allowedAttributes as $allowedAttribute) {
                    if (!empty($allowedAttribute)) {
                        $this->{$group}[] = (string)$allowedAttribute;
                    }
                }
                $this->{$group} = array_unique($this->{$group});
            }
        }
    }
}
