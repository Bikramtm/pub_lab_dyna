<?php

/**
 * Dependencies:
 * Dyna_Aikido_Helper_Data
 * Dyna_Catalog_Model_Product
 * Dyna_Catalog_Model_Type
 * Dyna_Checkout_Helper_Data
 * Dyna_PriceRules_Model_Observer
 * Vznl_Configurator_Model_Cache
 * Omnius_MixMatch_Model_Resource_Price_Collection
 */
class Vznl_RestApi_Catalogue_Get
{
    /**
     * @var array
     */
    public $phoneProductsAttributes = array(
        "sku",
        "name",
        "prodspecs_brand",
        "description",
        "prodspecs_afmetingen_lengte",
        "prodspecs_afmetingen_breedte",
        "prodspecs_afmetingen_dikte",
        "prodspecs_afmetingen_unit",
        "prodspecs_weight_gram",
        "prodspecs_besturingssysteem",
        "prodspecs_processor",
        "prodspecs_schermdiagonaal_inch",
        "prodspecs_touchscreen",
        "prodspecs_schermresolutie",
        "prodspecs_display_features",
        "prodspecs_werkgeheugen_mb",
        "prodspecs_opslagcapaciteit",
        "prodspecs_opslag_uitbreidbaar",
        "prodspecs_camera",
        "prodspecs_megapixels_1e_cam",
        "prodspecs_flitser",
        "prodspecs_filmen_in_hd",
        "prodspecs_audio_out",
        "prodspecs_hdmi",
        "prodspecs_2e_camera",
        "prodspecs_4g_lte",
        "prodspecs_3g",
        "prodspecs_gprs_edge",
        "prodspecs_wifi",
        "prodspecs_wifi_frequenties",
        "prodspecs_bluetooth",
        "prodspecs_bluetooth_version",
        "prodspecs_nfc",
        "prodspecs_gps",
        "prodspecs_accu_verwisselb",
        "prodspecs_batterij",
        "prodspecs_standby_tijd",
        "prodspecs_spreektijd",
        "identifier_hawaii_type",
        "identifier_hawaii_id",
        "prodspecs_hawaii_preorder",
        "identifier_hawaii_type_name",
        "prodspecs_hawaii_usp1",
        "prodspecs_hawaii_usp2",
        "prodspecs_hawaii_usp3",
        "prodspecs_network",
        "prodspecs_network_3g",
        "prodspecs_network_4g",
        "prodspecs_ecoscore",
        "prodspecs_hawaii_os_versie",
        "identifier_hawaii_prep_orig_id",
        "prijs_thuiskopieheffing_bedrag",
        "prodspecs_hawaii_channel",
        "hawaii_def_subscr_cbu",
        "hawaii_def_subscr_ebu",
        "identifier_simcard_formfactor",
    );
    public $productChildAttributes = array(
        "sku",
        "name",
        "identifier_hawaii_id",
        "identifier_device_post_prepaid",
        "prodspecs_kleur",
        "prodspecs_hawaii_htmlkleurcode",
        "regular_price",
    );
    public $internetProductsAttributes = array(
        "sku",
        "name",
        "prodspecs_brand",
        "description",
        "prodspecs_afmetingen_lengte",
        "prodspecs_afmetingen_breedte",
        "prodspecs_afmetingen_dikte",
        "prodspecs_afmetingen_unit",
        "prodspecs_weight_gram",
        "prodspecs_besturingssysteem",
        "prodspecs_processor",
        "prodspecs_schermdiagonaal_inch",
        "prodspecs_touchscreen",
        "prodspecs_schermresolutie",
        "prodspecs_display_features",
        "prodspecs_werkgeheugen_mb",
        "prodspecs_opslagcapaciteit",
        "prodspecs_opslag_uitbreidbaar",
        "prodspecs_camera",
        "prodspecs_megapixels_1e_cam",
        "prodspecs_flitser",
        "prodspecs_filmen_in_hd",
        "prodspecs_audio_out",
        "prodspecs_hdmi",
        "prodspecs_2e_camera",
        "prodspecs_4g_lte",
        "prodspecs_3g",
        "prodspecs_gprs_edge",
        "prodspecs_wifi",
        "prodspecs_wifi_frequenties",
        "prodspecs_bluetooth",
        "prodspecs_bluetooth_version",
        "prodspecs_nfc",
        "prodspecs_gps",
        "prodspecs_accu_verwisselb",
        "prodspecs_batterij",
        "prodspecs_standby_tijd",
        "prodspecs_spreektijd",
        "identifier_hawaii_type",
        "identifier_hawaii_id",
        "prodspecs_hawaii_preorder",
        "identifier_hawaii_type_name",
        "prodspecs_hawaii_usp1",
        "prodspecs_hawaii_usp2",
        "prodspecs_hawaii_usp3",
        "prodspecs_network",
        "prodspecs_network_3g",
        "prodspecs_network_4g",
        "prodspecs_ecoscore",
        "prodspecs_hawaii_os_versie",
        "identifier_hawaii_prep_orig_id",
        "prijs_thuiskopieheffing_bedrag",
        "prodspecs_hawaii_channel",
        "hawaii_def_subscr_cbu",
        "hawaii_def_subscr_ebu",
        "identifier_simcard_formfactor",
    );
    public $accesoryProductsAttributes = array(
        "sku",
        "name",
        "product_segment",
        "prodspecs_brand",
        "description",
        "prodspecs_afmetingen_lengte",
        "prodspecs_afmetingen_breedte",
        "prodspecs_afmetingen_dikte",
        "prodspecs_afmetingen_unit",
        "prodspecs_weight_gram",
        "prodspecs_kleur",
        "identifier_hawaii_type",
        "identifier_hawaii_id",
        "prodspecs_hawaii_preorder",
        "identifier_hawaii_type_name",
        "prodspecs_hawaii_html_kleurcode",
        "prodspecs_hawaii_usp1",
        "prodspecs_hawaii_usp2",
        "prodspecs_hawaii_usp3",
        "prodspecs_network",
        "prodspecs_network_3g",
        "prodspecs_network_4g",
        "prodspecs_ecoscore",
        "prodspecs_hawaii_os_versie",
        "identifier_hawaii_prep_orig_id",
        "prijs_thuiskopieheffing_bedrag",
        "prodspecs_hawaii_channel",
        "regular_price",
    );
    public $subscriptionProductsAttributes = array(
        "sku",
        "name",
        "hawaii_subscr_family",
        "hawaii_subscr_kind",
        "product_segment",
        "description",
        "prodspecs_network",
        "prodspecs_4g_lte",
        "prodspecs_aantal_belminuten",
        "prodspecs_data_aantal_mb",
        "prodspecs_sms_amount",
        "identifier_hawaii_id",
        "identifier_hawaii_subs_formula",
        "identifier_hawaii_subscription",
        "prodspecs_hawaii_def_phone",
        "prodspecs_hawaii_usp1",
        "prodspecs_hawaii_usp2",
        "prodspecs_hawaii_usp3",
        "prijs_belminuut_buiten_bundel",
        "prodspecs_hawaii_channel",
        "hawaii_subscr_hidden",
    );
    public $priceplanChildAttributes = array(
        "sku",
        "name",
        "identifier_hawaii_id",
        "identifier_commitment_months",
        "priceplan_price",
        "device_price",
        "maf",
        "prijs_aansluitkosten",
        "identifier_subsc_postp_simonly",
        "aikido",
    );
    public $internetSubscriptionProductsAttributes = array(
        "sku",
        "name",
        "hawaii_subscr_family",
        "hawaii_subscr_kind",
        "product_segment",
        "description",
        "prodspecs_network",
        "prodspecs_4g_lte",
        "prodspecs_aantal_belminuten",
        "prodspecs_data_aantal_mb",
        "prodspecs_sms_amount",
        "identifier_hawaii_id",
        "identifier_hawaii_subscr_orig",
        "identifier_hawaii_subs_formula",
        "identifier_hawaii_subscription",
        "prodspecs_hawaii_def_phone",
        "prodspecs_hawaii_usp1",
        "prodspecs_hawaii_usp2",
        "prodspecs_hawaii_usp3",
        "prodspecs_hawaii_channel",
        "hawaii_subscr_hidden",
    );
    public $addonProductAttributes = array(
        "sku",
        "name",
        "identifier_commitment_months",
        "description",
        "prodspecs_4g_lte",
        "identifier_hawaii_type",
        "identifier_hawaii_id",
        "prodspecs_hawaii_usp1",
        "prodspecs_hawaii_usp2",
        "prodspecs_hawaii_usp3",
        "prodspecs_data_mb_config",
        "prodspecs_belmin_sms_config",
        "prijs_belminuut_buiten_bundel",
        "hawaii_hybride_type",
        "hawaii_hybride_voice_data",
        "hawaii_is_default",
        "prodspecs_hawaii_channel",
        "maf",
    );
    public $simProductsAttributes = array(
        "sku",
        "name",
        "identifier_hawaii_id",
    );
    public $simChildAttributes = array(
        "sku",
        "name",
        "identifier_hawaii_id",
        "is_hybrid_sim",
        "identifier_simcard_type",
    );
    public $prepaidSimProductsAttributes = array(
        "sku",
        "name",
        "identifier_hawaii_id",
        "is_hybrid_sim",
        "prijs_beltegoed_bedrag",
        "identifier_simcard_type",
    );
    public $deviceRCProductAttributes = array(
        "sku",
        "name",
        "identifier_hawaii_id",
        "identifier_addon_type",
        "loan_indicator",
        "maf",
    );
    protected $phoneProductsSimple;
    protected $subscriptionProductsSimple;
    protected $internetProductsSimple;
    protected $internetSubscriptionProductsSimple;
    protected $simProductsSimple;
    protected $phoneProductsConfigurable;
    protected $subscriptionProductsConfigurable;
    protected $internetProductsConfigurable;
    protected $internetSubscriptionProductsConfigurable;
    protected $simProductsConfigurable;
    protected $accesoryProducts;
    protected $addOnProducts;
    protected $addOnHybridProducts;
    protected $deviceRCProducts;
    protected $prepaidSimProducts;
    protected $errors = array();
    protected $usedProducts = array();
    public $parsedProducts;
    public $debug = false;
    public $cache = true;
    public $cacheKeyPrefix = 'hawaii_rest_catalog_';
    public $cacheTTL = 1209600; // 2 weeks

    const VERSION = 7;
    const DEBUG_INDEX = 1;

    protected $lifecycleMappings = [
        Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITION => Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE,
        Vznl_Checkout_Model_Sales_Quote_Item::RETENTION => Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE
    ];

    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct(Mage_Core_Controller_Request_Http $request)
    {
        $this->setRequest($request);

        // Enable debug if needed
        if ($request->getParam('debug', false) === 'true') {
            $this->debug = true;
        }
    }

    /**
     * Store the request object to the class to extract values later on
     *
     * @param Mage_Core_Controller_Request_Http $request
     * @return \Vznl_RestApi_Catalogue_Get
     */
    public function setRequest(Mage_Core_Controller_Request_Http $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get the request
     * @return Mage_Core_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Add an error
     *
     * @param string $sku
     * @param string $type
     * @param string $text
     * @return array
     */
    public function addError($sku, $type, $text)
    {
        $this->errors[] = array('sku' => $sku, 'type' => $type, 'text' => $text);

        return $this->errors;
    }

    /**
     *
     * @return typeReturn the cache object
     */
    public function getCache()
    {
        return Mage::getSingleton('vznl_configurator/cache');
    }

    /**
     * Get the cache key for the requested parameters
     * @return string
     */
    protected function getCacheKey()
    {
        $prefix = $this->cacheKeyPrefix;
        $cacheKeyParts = array();
        if ($this->getRequest()->getParam('products', false) == 'true') {
            $cacheKeyParts[] = 'products';
        }
        // Stock
        if ($this->getRequest()->getParam('stock', false) == 'true') {
            $this->cache = false;
            $cacheKeyParts[] = 'stock';
        }
        // Mixmatch
        if ($this->getRequest()->getParam('mixmatch', false) == 'true') {
            $cacheKeyParts[] = 'mixmatch';
        }
        // Compatibility
        if ($this->getRequest()->getParam('compatibility', false) == 'true') {
            $cacheKeyParts[] = 'compatibility';
        }
        // Promo
        if ($this->getRequest()->getParam('promotion', false) == 'true') {
            $cacheKeyParts[] = 'promotion';
        }

        return $this->cacheKeyPrefix . implode('_', $cacheKeyParts);
    }

    protected function loadProducts()
    {
        $this->phoneProductsConfigurable = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 35)->addAttributeToFilter('type_id', array('eq' => 'configurable'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->subscriptionProductsConfigurable = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 37)->addAttributeToFilter('type_id', array('eq' => 'configurable'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->internetProductsConfigurable = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 49)->addAttributeToFilter('type_id', array('eq' => 'configurable'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->internetSubscriptionProductsConfigurable = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 48)->addAttributeToFilter('type_id', array('eq' => 'configurable'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->simProductsConfigurable = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', array(53))->addAttributeToFilter('type_id', array('eq' => 'configurable'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();

        $this->phoneProductsSimple = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 35)->addAttributeToFilter('type_id', array('eq' => 'simple'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->internetProductsSimple = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 49)->addAttributeToFilter('type_id', array('eq' => 'simple'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->subscriptionProductsSimple = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 37)->addAttributeToFilter('type_id', array('eq' => 'simple'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->internetSubscriptionProductsSimple = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 48)->addAttributeToFilter('type_id', array('eq' => 'simple'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->simProductsSimple = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 53)->addAttributeToFilter('type_id', array('eq' => 'simple'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();

        $this->prepaidSimProducts = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 55)->addAttributeToFilter('type_id', array('eq' => 'simple'))->addAttributeToFilter('is_deleted',
            array('eq' => false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();

        $this->accesoryProducts = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 46)->addAttributeToFilter('is_deleted', array('eq' => false))->addAttributeToFilter('status',
            array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->addOnProducts = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 50)->addAttributeToFilter('is_deleted', array('eq' => false))->addAttributeToFilter('status',
            array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->addOnHybridProducts = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 47)->addAttributeToFilter('is_deleted', array('eq' => false))->addAttributeToFilter('status',
            array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
        $this->deviceRCProducts = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('attribute_set_id', 58)->addAttributeToFilter('type_id', array('eq' => 'simple'))->addAttributeToFilter('is_deleted', array('eq'=>false))->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addStoreFilter();
    }

    /**
     * Process the actual request
     * @return string
     */
    public function process()
    {
        if ($this->cache && $this->getRequest()->getParam('refreshcache', 'false') == 'false') {
            $xml = $this->getCache()->load($this->getCacheKey());
            if ($xml) {
                return $xml;
            }
        }

        $response = '<?xml version="1.0" encoding="utf-8"?>
        <catalog>
            <created>' . time() . '</created>
            <expires>' . strtotime(now() . ' + ' . $this->cacheTTL . ' second') . '</expires>
            <version>' . self::VERSION . '</version>
            <actor>api</actor>
            <source>uat01</source>' . PHP_EOL;

        // Products
        if ($this->getRequest()->getParam('products', false) == 'true') {
            $productsResponse = $this->parseProducts(); // Always collect to setup a list of active products
            $response .= $productsResponse;
        }
        // Stock
        if ($this->getRequest()->getParam('stock', false) == 'true') {
            $response .= $this->parseStock();
        }
        // Compatibility
        if ($this->getRequest()->getParam('compatibility', false) == 'true') {
            $productsResponse = $this->parseProducts(); // Always collect to setup a list of active products
            $response .= $this->parseCompatibility();
        }
        // Mixmatch
        if ($this->getRequest()->getParam('mixmatch', false) == 'true') {
            $productsResponse = $this->parseProducts(); // Always collect to setup a list of active products
            $response .= $this->parseMixmatch();
        }
        // Promo
        if ($this->getRequest()->getParam('promotion', false) == 'true') {
            $productsResponse = $this->parseProducts(); // Always collect to setup a list of active products
            $response .= $this->parsePromotion();
        }
        $response .= '<errors>';
        foreach ($this->errors as $error) {
            $response .= '<error>
                    <sku>' . $error['sku'] . '</sku>
                    <type><![CDATA[' . $error['type'] . ']]></type>
                    <text><![CDATA[' . $error['text'] . ']]></text>
                </error>' . PHP_EOL;
        }
        $response .= '</errors>';
        $response .= '</catalog>' . PHP_EOL;

        // Cache the XML
        if ($this->cache) {
            $this->getCache()->save($response, $this->getCacheKey(), array(Vznl_Configurator_Model_Cache::CACHE_TAG), $this->cacheTTL);
        }

        return $response;
    }

    /**
     * Parse the different type of producs
     * @return string
     */
    protected function parseProducts()
    {
        if (is_null($this->parsedProducts)) {
            $result = null;
            $this->loadProducts();

            $result .= $this->handleProducts($this->phoneProductsConfigurable, 'mobile_device', 'product', $this->phoneProductsAttributes, $this->productChildAttributes);
            $result .= $this->handleProducts($this->internetProductsConfigurable, 'internet_device', 'product', $this->internetProductsAttributes, $this->productChildAttributes);
            $result .= $this->handleProducts($this->simProductsConfigurable, 'sim', 'sim', $this->simProductsAttributes, $this->simChildAttributes);
            $result .= $this->handleProducts($this->prepaidSimProducts, 'prepaid_simonly', 'prepaid_simonly', $this->prepaidSimProductsAttributes);
            $result .= $this->handleProducts($this->accesoryProducts, 'accessory', 'accessory', $this->accesoryProductsAttributes);
            $result .= $this->handleProducts($this->subscriptionProductsConfigurable, 'mobile_priceplan', 'mobile_priceplan', $this->subscriptionProductsAttributes, $this->priceplanChildAttributes);
            $result .= $this->handleProducts($this->internetSubscriptionProductsConfigurable, 'internet_priceplan', 'mobile_priceplan', $this->internetSubscriptionProductsAttributes, $this->priceplanChildAttributes);
            $result .= $this->handleProducts($this->addOnProducts, 'addon', 'addon', $this->addonProductAttributes);
            $result .= $this->handleProducts($this->addOnHybridProducts, 'addon_hybrid', 'hybrid_addon', $this->addonProductAttributes);
            $result .= $this->handleProducts($this->deviceRCProducts, 'devicerc', 'devicerc', $this->deviceRCProductAttributes);
            $this->parsedProducts = $result;

            return $result;
        }

        return $this->parsedProducts;
    }

    /**
     * Parse the stock and create the shell
     * @return string
     */
    protected function parseStock()
    {
        $result = null;
        $result .= '<stock>' . PHP_EOL;
        $result .= $this->handleStock();
        $result .= '</stock>' . PHP_EOL;

        return $result;
    }

    /**
     * Create the mixmatch shell and add all data
     *  - Default mixmatch is put into postpaid mixmatch.
     *  - Simonly mixmatch contains all simonly products
     *  - Prepaid mixmatch contains all prepaid devices
     * @return string
     */
    protected function parseMixmatch()
    {
        $result = null;
        $result .= '<simonly_mixmatch>' . PHP_EOL;
        $result .= $this->handleSimonlyMixmatch();
        $result .= '</simonly_mixmatch>' . PHP_EOL;
        $result .= '<postpaid_mixmatch>' . PHP_EOL;
        $result .= $this->handlePostpaidMixmatch();
        $result .= '</postpaid_mixmatch>' . PHP_EOL;
        $result .= '<prepaid_mixmatch>' . PHP_EOL;
        $result .= $this->handlePrepaidMixmatch();
        $result .= '</prepaid_mixmatch>' . PHP_EOL;

        return $result;
    }

    /**
     * Create the compatibility shell
     * @return string
     */
    protected function parseCompatibility()
    {
        $result = null;
        $result .= '<compatibility>' . PHP_EOL;
        $result .= $this->handleCompatibility();
        $result .= '</compatibility>' . PHP_EOL;

        return $result;
    }

    /**
     * Create the promotions shell
     * @return string
     */
    protected function parsePromotion()
    {
        $result = null;
        $result .= '<promotions>' . PHP_EOL;
        $result .= $this->handlePromotion();
        $result .= '</promotions>' . PHP_EOL;

        return $result;
    }

    /**
     * Render the SSFE promotions and add them to a logic flat XML format
     * @return string
     * @todo Cannot test, test after database migration
     */
    protected function handlePromotion()
    {
        $store = Mage::app()->getStore();
        $result = null;

        // all promo rules
        $ruleCollection = Mage::getResourceModel('salesrule/rule_collection')
            ->addWebsiteFilter(Mage::app()->getWebsite()->getId())
            ->addFieldToFilter('is_active', true)
            ->addFieldToFilter('coupon_type', 1); // Only no coupons
        foreach ($ruleCollection as $key => $rule) {
            try {
                $interpreter = new Vznl_RestApi_Catalogue_SalesRuleInterpreter();
                $dataRules = $interpreter->interpret($rule);
            } catch (Exception $e) {
                $this->addError($rule->getId(), 'promotion', 'Could not interpret rule ' . $rule->getId() . ': ' . $e->getMessage());
                continue;
            }

            // Loop through rules
            foreach ($dataRules as $lifecycle => $data) {
                $data['customer_value'] = is_array($data['customer_value']) ? $data['customer_value'] : array($data['customer_value']);
                $data['value_segment'] = is_array($data['value_segment']) ? $data['value_segment'] : array($data['value_segment']);
                $data['campaign'] = is_array($data['campaign']) ? $data['campaign'] : array($data['campaign']);
                $data['products'] = is_array($data['products']) ? $data['products'] : array($data['products']);

                $oncePromoPrice = 0;
                $onceDiscount = 0;
                $recurringPromoPrice = 0;
                $recurringDiscount = 0;

                // Check which action to render
                switch ($rule->getSimpleAction()) {
                    case 'by_fixed':
                        $promoProduct = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId()); // Fake the promo product as empty shell
                        $oncePromoPrice = -$rule->getDiscountAmount();
                        $onceDiscount = $rule->getDiscountAmount();
                        $promotionTitle = $rule->getName();
                        break;
                    case 'ampromo_items':
                        $parts = explode(',', $data['products'][0]);
                        $product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId());
                        $product = $product->load($product->getIdBySku(trim($parts[0])));
                        $promoProduct = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId());
                        $promoProduct = $promoProduct->load($promoProduct->getIdBySku($rule->getPromoSku()));
                        $promotionTitle = $promoProduct->getName();
                        if ($promoProduct->getPrijsMafDiscountPercent()) {
                            $recurringPromoPrice = $product->getMaf() - ($product->getMaf() / 100 * $promoProduct->getPrijsMafDiscountPercent());
                            $recurringDiscount = $product->getMaf() - $recurringPromoPrice;
                        }
                        if ($promoProduct->getPrijsMafNewAmount()) {
                            $recurringPromoPrice = $promoProduct->getPrijsMafNewAmount();
                            $recurringDiscount = $product->getMaf() - $recurringPromoPrice;
                        }
                        if ($promoProduct->getPrijsAansluitPromoBedrag()) {
                            $oncePromoPrice = $promoProduct->getPrijsAansluitPromoBedrag();
                            $onceDiscount = $product->getPrijsAansluitkosten() - $oncePromoPrice;
                        }
                        if ($promoProduct->getPrijsAansluitPromoProcent()) {
                            $oncePromoPrice = $product->getPrijsAansluitkosten() - ($product->getPrijsAansluitkosten() / 100 * $promoProduct->getPrijsAansluitPromoProcent());
                            $onceDiscount = $product->getPrijsAansluitkosten() - $recurringPromoPrice;
                        }
                        break;
                    default:
                        $this->addError($rule->getId(), 'promotion', 'Could not interpret rule type "' . $rule->getSimpleAction() . '" from rule ' . $rule->getId());
                        break;
                }

                // Check for unknown products
                $skuCollection = [];
                foreach ($data['products'] as $products) {
                    $products = explode(',', $products);
                    foreach ($products as $sku) {
                        $sku = trim($sku);
                        if (in_array($sku, $this->usedProducts)) {
                            $skuCollection[] = $sku;
                        } else {
                            $this->addError($rule->getId(), 'promotion', 'Rule ' . $rule->getId() . ' has inactive product "' . $sku . '"');
                        }
                    }
                }
                if (!empty($skuCollection)) {
                    // Setup price blocks
                    $prices = null;
                    $prices .= '<one_time_promo_price>' . $this->renderPrice($result, $oncePromoPrice, $promoProduct) . '</one_time_promo_price>' . PHP_EOL;
                    $prices .= '<one_time_discount>' . $this->renderPrice($result, $onceDiscount, $promoProduct) . '</one_time_discount>' . PHP_EOL;
                    $prices .= '<recurring_promo_price>' . $this->renderPrice($result, $recurringPromoPrice, $promoProduct) . '</recurring_promo_price>' . PHP_EOL;
                    $prices .= '<recurring_discount>' . $this->renderPrice($result, $recurringDiscount, $promoProduct) . '</recurring_discount>' . PHP_EOL;

                    if (!is_null($prices)) {
                        foreach ($data['customer_value'] as $customerValue) {
                            foreach ($data['value_segment'] as $valueSegment) {
                                foreach ($data['campaign'] as $campaign) {
                                    foreach ($data['products'] as $products) {
                                        $result .= '<promo>' . PHP_EOL;
                                        $result .= '<sales_rule_id>' . $rule->getId() . '</sales_rule_id>' . PHP_EOL;
                                        $result .= '<start_date>' . (!is_null($rule->getFromDate()) ? strtotime($rule->getFromDate()) . '000' : '') . '</start_date>' . PHP_EOL;
                                        $result .= '<end_date>' . (!is_null($rule->getToDate()) ? strtotime($rule->getToDate()) . '000' : '') . '</end_date>' . PHP_EOL;
                                        $result .= '<duration>' . $promoProduct->getPrijsPromoDuurMaanden() . '</duration>' . PHP_EOL;
                                        $result .= '<text><![CDATA[' . $promotionTitle . ']]></text>' . PHP_EOL;
                                        $result .= '<customer_value>' . $customerValue . '</customer_value>' . PHP_EOL;
                                        $result .= '<customer_lifecycle><![CDATA[' . ($this->lifecycleMappings[$lifecycle] ?? $lifecycle) . ']]></customer_lifecycle>' . PHP_EOL;
                                        $result .= '<value_segment>' . $valueSegment . '</value_segment>' . PHP_EOL;
                                        $result .= '<campaign_code>' . $campaign . '</campaign_code>' . PHP_EOL;
                                        $result .= '<products>';
                                        $products = explode(',', $products);
                                        foreach ($skuCollection as $sku) {
                                            $result .= '<sku>' . $sku . '</sku>';
                                        }
                                        $result .= '</products>' . PHP_EOL;
                                        $result .= $prices;
                                        $result .= '<add_product_sku></add_product_sku>' . PHP_EOL;
                                        $result .= '</promo>' . PHP_EOL;
                                    }
                                }
                            }
                        }
                    } else {
                        $this->addError($rule->getId(), 'promotion', 'Rule ' . $rule->getId() . ' could not render the given price products');
                    }
                } else {
                    $this->addError($rule->getId(), 'promotion', 'Rule ' . $rule->getId() . ' has no active products');
                }
            }
        }

        return $result;
    }

    /**
     * Select all compatibility rules from the database manually since the collections will be too big
     * @return string
     */
    protected function handleCompatibility()
    {
        $i = 0;
        $allowedSourceSetIds = array(35, 49, 37, 48);
        $result = null;
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $queryResult = $write->query("select pmwi.*, cpe.sku as source_sku, cpe.attribute_set_id as source_attribute_set_id, cpet.sku as target_sku, cpet.attribute_set_id as target_attribute_set_id
            from product_match_whitelist_index pmwi
            join catalog_product_entity cpe on pmwi.source_product_id = cpe.entity_id
            join catalog_product_website cpw on cpe.entity_id = cpw.product_id
            join catalog_product_entity cpet on pmwi.target_product_id = cpet.entity_id
            join catalog_product_website cpwt on cpet.entity_id = cpwt.product_id
            join catalog_product_entity_int cpei on cpei.entity_id = cpe.entity_id and cpei.attribute_id = 96
             join catalog_product_entity_int cpeit on cpeit.entity_id = cpet.entity_id and cpeit.attribute_id = 96
            where pmwi.website_id = " . Mage::app()->getStore()->getWebsiteId() . "
            and cpw.website_id = " . Mage::app()->getStore()->getWebsiteId() . "
            and cpwt.website_id = " . Mage::app()->getStore()->getWebsiteId() . "
            and cpei.value = 1
            and cpeit.value = 1
            and(
                (
                    cpet.attribute_set_id in(37,48) and cpe.attribute_set_id in(46, 50)
                    or
                    cpe.attribute_set_id in(37,48) and cpet.attribute_set_id in(46, 50)
                )
                or
                (
                    cpet.attribute_set_id in(35,49) and cpe.attribute_set_id in(46,50)
                    or
                    cpe.attribute_set_id in(35,49) and cpet.attribute_set_id in(46,50)
                )
            )
            order by source_sku, target_sku
            ");

        // Create massive array
        $compatibilityData = array();
        while ($row = $queryResult->fetch()) {
            if (in_array($row['source_attribute_set_id'], $allowedSourceSetIds)) {
                if (in_array($row['source_sku'], $this->usedProducts) && in_array($row['target_sku'], $this->usedProducts)) {
                    if (!isset($compatibilityData[$row['source_sku']][$row['target_sku']])) {
                        if (!is_array($compatibilityData[$row['target_sku']]) || !in_array($row['target_sku'], $compatibilityData[$row['target_sku']])) {
                            $compatibilityData[$row['source_sku']][] = $row['target_sku'];
                        }
                    }
                }
            } elseif (in_array($row['target_attribute_set_id'], $allowedSourceSetIds)) {
                if (in_array($row['source_sku'], $this->usedProducts) && in_array($row['target_sku'], $this->usedProducts)) {
                    if (!isset($compatibilityData[$row['target_sku']][$row['source_sku']])) {
                        if (!is_array($compatibilityData[$row['target_sku']]) || !in_array($row['source_sku'], $compatibilityData[$row['target_sku']])) {
                            $compatibilityData[$row['target_sku']][] = $row['source_sku'];
                        }
                    }
                }
            }
        }

        // Setup xml data
        foreach ($compatibilityData as $sku => $matches) {
            if (!empty($matches)) {
                $result .= '<rule>' . PHP_EOL;
                $result .= '<sku>' . $sku . '</sku>' . PHP_EOL;
                $result .= '<matches>' . PHP_EOL;
                foreach ($matches as $match) {
                    $result .= '<match>' . $match . '</match>' . PHP_EOL;
                }
                $result .= '</matches>';
                $result .= '</rule>' . PHP_EOL;
            }
        }

        return $result;
    }

    /**
     * Get all the SSFE stock.
     *
     * @return string
     * @todo Cannot test, waiting for database migration
     */
    protected function handleStock()
    {
        $result = null;

        $i = 0;
        $productsCollection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('stock_status', true)
            ->addAttributeToSelect('axi_threshold', true)
            ->addStoreFilter()
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addAttributeToFilter('is_deleted', array('neq' => 1));
        $storeCode = Mage::getStoreConfig('vodafone_service/axi/vodafone_warehouse');
        $products = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAssoc($productsCollection->getSelect());
        $stocks = Mage::helper('stock')->getStockForProducts(array_keys($products), $storeCode); // Warehouse

        $cs = Mage::getSingleton('customer/session');
        $storeId = $cs->getAgent() && $cs->getAgent()->getDealer() ? $cs->getAgent()->getDealer()->getStoreId() : Mage::app()->getStore()->getId();

        foreach ($productsCollection as $index => $product) {
            if ($this->debug && $i > self::DEBUG_INDEX) {
                break;
            }
            if (in_array($product->getAttributeSetId(), array(35, 49, 48, 46, 55))) {
                $result .= '<stockitem>
                    <sku>' . $product->getSku() . '</sku>
                    <qty>' . (int) $stocks[$index]['qty'] . '</qty>
                    <available>' . (int) $product->isSellable($stocks[$index]['qty'], $stocks[$index]['backorder_qty'], $storeCode, $storeId) . '</available>
                </stockitem>' . PHP_EOL;
                $i++;
            }
        }

        return $result;
    }

    /**
     * Create an empty shell and add existing values if not empty
     * @param string $key
     * @param string $value
     * @return string
     */
    protected function createShell($key, $value)
    {
        if ($value) {
            return '<' . $key . '>' . $value . '</' . $key . '>' . PHP_EOL;
        }

        return '<' . $key . '/>' . PHP_EOL;
    }

    /**
     * Render price block for given price and product
     * @param string $result
     * @param float $priceStart
     * @param object $product
     * @return string
     */
    protected function renderPrice($result, $priceStart, $product)
    {
        $taxHelper = Mage::helper('tax');
        $store = Mage::app()->getStore();

        if (!$product->getData('tax_percent')) {
            $request = Mage::getSingleton('tax/calculation')->getRateRequest(null, null, null, Mage::app()->getStore());
            $taxclassid = $product->getData('tax_class_id');
            $percent = Mage::getSingleton('tax/calculation')->getRate($request->setProductClassId($taxclassid));
            $product->setData('tax_percent', $percent);
        }

        $priceStart = $store->roundPrice($store->convertPrice($priceStart));
        $price = $taxHelper->getPrice($product, $priceStart, false);
        $priceWithTax = $taxHelper->getPrice($product, $priceStart, true);
        $priceTax = round($priceWithTax - $price, 2);
        $priceVatPercentage = $product->getData('tax_percent');

        $result = '<price_inc>' . round($priceWithTax, 2) . '</price_inc>
            <price_ex>' . round($price, 2) . '</price_ex>
            <price_vat>' . $priceTax . '</price_vat>
            <price_vat_perc>' . round($priceVatPercentage, 2) . '</price_vat_perc>';

        return $result;
    }

    /**
     * Get all simonly products the are connected to a parent configurable
     * @return string
     */
    protected function handleSimonlyMixmatch()
    {
        $i = 0;
        $checkoutHelper = Mage::helper('dyna_checkout');
        $result = null;
        // Device subscription
        foreach ($this->subscriptionProductsConfigurable as $configurableSubscription) {
            $subscriptionConf = Mage::getModel('catalog/product_type_configurable')->setProduct($configurableSubscription);
            $subscriptionCol = $subscriptionConf->getUsedProductCollection()->addAttributeToSelect('sku')->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addFilterByRequiredOptions()->addStoreFilter();
            foreach ($subscriptionCol as $simpleSubscription) {
                if ($this->debug && $i > self::DEBUG_INDEX) {
                    break;
                }
                $subscription = Mage::getModel('catalog/product')->load($simpleSubscription->getId());
                if ($subscription->getData('identifier_subsc_postp_simonly') == 221 || $subscription->getAikido()) {
                    $mixMatches = $checkoutHelper->getMixMatchPrices($subscription->getSku(), null);
                    $result .= '<match>' . PHP_EOL;
                    $result .= '<subscription_sku>' . $subscription->getSku() . '</subscription_sku>' . PHP_EOL;
                    $result .= '<price>' . PHP_EOL;
                    $result .= $this->renderPrice($result, $subscription->getPrice(), $subscription, $mixMatches[$subscription->getSku()]) . PHP_EOL;
                    $result .= '</price>' . PHP_EOL;
                    $result .= '</match>' . PHP_EOL;
                    $i++;
                }
            }
        }
        // Internet
        foreach ($this->internetSubscriptionProductsConfigurable as $configurableSubscription) {
            $subscriptionConf = Mage::getModel('catalog/product_type_configurable')->setProduct($configurableSubscription);
            $subscriptionCol = $subscriptionConf->getUsedProductCollection()->addAttributeToSelect('sku')->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addFilterByRequiredOptions()->addStoreFilter();
            foreach ($subscriptionCol as $simpleSubscription) {
                if ($this->debug && $i > self::DEBUG_INDEX) {
                    break;
                }
                $subscription = Mage::getModel('catalog/product')->load($simpleSubscription->getId());
                if ($subscription->getData('identifier_subsc_postp_simonly') == 221) {
                    $mixMatches = $checkoutHelper->getMixMatchPrices($subscription->getSku(), null);
                    $result .= '<match>' . PHP_EOL;
                    $result .= '<subscription_sku>' . $subscription->getSku() . '</subscription_sku>' . PHP_EOL;
                    $result .= '<price>' . PHP_EOL;
                    $result .= $this->renderPrice($result, $subscription->getPrice(), $subscription, $mixMatches[$subscription->getSku()]) . PHP_EOL;
                    $result .= '</price>' . PHP_EOL;
                    $result .= '</match>' . PHP_EOL;
                    $i++;
                }
            }
        }

        return $result;
    }

    /**
     * Get all mimatch records from the SSFE mixmatch table
     * @return string
     */
    protected function handlePostpaidMixmatch()
    {
        $result = null;
        $request = Mage::getSingleton('tax/calculation')->getRateRequest(null, null, null, Mage::app()->getStore());
        $product = Mage::getModel('catalog/product');
        $i = 0;

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $queryResult = $write->query("select cm.*, cpei_tax.value as tax_class_id
            from catalog_mixmatch cm
            join catalog_product_entity cpe_device on cpe_device.sku = cm.target_sku
            join catalog_product_website cpw_device on cpe_device.entity_id = cpw_device.product_id
            join catalog_product_entity cpe_subscription on cpe_subscription.sku = cm.source_sku
            join catalog_product_website cpw_subscription on cpe_subscription.entity_id = cpw_subscription.product_id
            join catalog_product_entity_int cpei_tax on cpe_device.entity_id = cpei_tax.entity_id and cpei_tax.attribute_id = 121
            join catalog_product_entity_int cpei_devicestatus on cpei_devicestatus.entity_id = cpe_device.entity_id and cpei_devicestatus.attribute_id = 96
            join catalog_product_entity_int cpei_subscriptionstatus on cpei_subscriptionstatus.entity_id = cpe_subscription.entity_id and cpei_subscriptionstatus.attribute_id = 96
            where cm.website_id = " . Mage::app()->getStore()->getWebsiteId() . "
            and cpw_device.website_id = " . Mage::app()->getStore()->getWebsiteId() . "
            and cpw_subscription.website_id = " . Mage::app()->getStore()->getWebsiteId() . "
            and cpei_devicestatus.value = 1
            and cpei_subscriptionstatus.value = 1
        ");
        while ($row = $queryResult->fetch()) {
            $product->setData('tax_class_id', $row['tax_class_id']);
            $product->setData('tax_percent', Mage::getSingleton('tax/calculation')->getRate($request->setProductClassId($row['tax_class_id'])));
            if ($this->debug && $i > self::DEBUG_INDEX) {
                break;
            }
            $result .= '<match>' . PHP_EOL;
            $result .= '<phone_sku>' . $row['target_sku'] . '</phone_sku>' . PHP_EOL;
            $result .= '<subscription_sku>' . $row['source_sku'] . '</subscription_sku>' . PHP_EOL;
            $result .= '<devicerc_sku>' . $row['device_subscription_sku'] . '</devicerc_sku>' . PHP_EOL;
            $result .= '<customer_lifecycle>';
            if ($row['device_subscription_sku'] == Mage::helper('vznl_checkout/aikido')->getDefaultRetentionSku()) {
                $result .= 'retention';
            } elseif ($row['device_subscription_sku'] == Mage::helper('vznl_checkout/aikido')->getDefaultAcquisitionSku()) {
                $result .= 'acquisition';
            } else {
                $result .= '';
            }
            $result .= '</customer_lifecycle>' . PHP_EOL;

            $result .= '<price>' . PHP_EOL;
            $result .= $this->renderPrice($result, $row['price'], $product) . PHP_EOL;
            $result .= '</price>' . PHP_EOL;
            $result .= '</match>' . PHP_EOL;
            $i++;
        }

        return $result;
    }

    /**
     * Handle the mixmatch for all prepaid phones
     * @param $result
     * @return string
     */
    protected function handlePrepaidPhoneMixmatches($result)
    {
        $checkoutHelper = Mage::helper('dyna_checkout');
        $i = 0;
        foreach ($this->phoneProductsConfigurable as $configurablePhone) {
            $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($configurablePhone);
            $col = $conf->getUsedProductCollection()->addAttributeToSelect('sku')->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addFilterByRequiredOptions()->addStoreFilter();
            foreach ($col as $simplePhone) {
                $phone = Mage::getModel('catalog/product')->load($simplePhone->getId());
                if ($phone->getAttributeText('identifier_device_post_prepaid') === 'prepaid') {
                    if ($this->debug && $i > self::DEBUG_INDEX) {
                        break;
                    }
                    $mixMatches = $checkoutHelper->getMixMatchPrices(null, $phone->getSku());
                    $result .= '<match>' . PHP_EOL;
                    $result .= '<phone_sku>' . $phone->getSku() . '</phone_sku>' . PHP_EOL;
                    $result .= '<price>' . PHP_EOL;
                    $result .= $this->renderPrice($result, $phone->getPrice(), $phone, $mixMatches[$phone->getSku()]) . PHP_EOL;
                    $result .= '</price>' . PHP_EOL;
                    $result .= '</match>' . PHP_EOL;
                    $i++;
                }
            }
        }

        return $result;
    }

    /**
     * Handle the mixmatch for all data devices
     * @param $result
     * @return string
     */
    protected function handleDataDeviceMixmatches($result)
    {
        $checkoutHelper = Mage::helper('dyna_checkout');
        $i = 0;
        foreach ($this->internetProductsConfigurable as $configurable) {
            $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($configurable);
            $col = $conf->getUsedProductCollection()->addAttributeToSelect('sku')->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addFilterByRequiredOptions()->addStoreFilter();
            foreach ($col as $simple) {
                $product = Mage::getModel('catalog/product')->load($simple->getId());
                if ($product->getAttributeText('identifier_device_post_prepaid') === 'prepaid') {
                    if ($this->debug && $i > self::DEBUG_INDEX) {
                        break;
                    }
                    $mixMatches = $checkoutHelper->getMixMatchPrices(null, $product->getSku());
                    $result .= '<match>' . PHP_EOL;
                    $result .= '<phone_sku>' . $product->getSku() . '</phone_sku>' . PHP_EOL;
                    $result .= '<price>' . PHP_EOL;
                    $result .= $this->renderPrice($result, $product->getPrice(), $product, $mixMatches[$product->getSku()]) . PHP_EOL;
                    $result .= '</price>' . PHP_EOL;
                    $result .= '</match>' . PHP_EOL;
                    $i++;
                }
            }
        }

        return $result;
    }

    /**
     * Create the mixmatch for all prepaid phones and data devices
     * @return string
     */
    protected function handlePrepaidMixmatch()
    {
        $result = null;

        // Phones prepaid
        $result = $this->handlePrepaidPhoneMixmatches($result);

        // Data devices prepaid
        $result = $this->handleDataDeviceMixmatches($result);

        // Prepaid simonly
        $i = 0;
        foreach ($this->prepaidSimProducts as $prepaidSim) {
            $sim = Mage::getModel('catalog/product')->load($prepaidSim->getId());
            if ($this->debug && $i > self::DEBUG_INDEX) {
                break;
            }
            $result .= '<match>' . PHP_EOL;
            $result .= '<phone_sku>' . $prepaidSim->getSku() . '</phone_sku>' . PHP_EOL;
            $result .= '<price>' . PHP_EOL;
            $result .= $this->renderPrice($result, $prepaidSim->getPrice(), $prepaidSim) . PHP_EOL;
            $result .= '</price>' . PHP_EOL;
            $result .= '</match>' . PHP_EOL;
            $i++;
        }

        return $result;
    }

    /**
     * Loop through the product collection and add the product attributes including all children
     *
     * @param array $productCollection
     * @param string $handle
     * @param string $handleElement
     * @param array $attributes
     * @param array $childAttrbutes
     * @return string
     */
    protected function handleProducts($productCollection, $handle, $handleElement, $attributes, $childAttrbutes = array())
    {
        $result = null;
        $i = 0;

        $result .= '<' . $handle . '_products>' . PHP_EOL;
        foreach ($productCollection as $productSkeleton) {

            if ($this->debug && $i > self::DEBUG_INDEX) {
                break;
            }
            $product = Mage::getModel('catalog/product')->load($productSkeleton->getId());
            $this->usedProducts[] = $product->getSku();
            $hawaiiChannel = $product->getProdspecsHawaiiChannel();

            if (!empty($hawaiiChannel)) {
                $result .= '<' . $handleElement . '>' . PHP_EOL;
                $result .= $this->renderElements($product, $attributes, $handle);

                if ($product->getTypeId() === 'configurable') {
                    $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($product);
                    $col = $conf->getUsedProductCollection()->addAttributeToSelect('sku')->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))->addFilterByRequiredOptions()->addStoreFilter();
                    $result .= '<children>' . PHP_EOL;
                    foreach ($col as $simpleProduct) {
                        $child = Mage::getModel('catalog/product')->load($simpleProduct->getId());
                        if ($this->productIsValid($child)) {
                            $this->usedProducts[] = $child->getSku();
                            $result .= $this->handleProductChild($child, $childAttrbutes);
                        }
                    }
                    $result .= '</children>' . PHP_EOL;
                }
                $result .= '</' . $handleElement . '>' . PHP_EOL;
                $i++;
            } else {
                $this->addError($product->getSku(), $handle, 'Product ' . $product->getSku() . ' does not have a Hawaii Channel');
            }
        }
        $result .= '</' . $handle . '_products>' . PHP_EOL;

        return $result;
    }

    /**
     * Render the child product
     * @param object $child
     * @param array $childAttrbutes
     * @return string
     */
    protected function handleProductChild($child, $childAttrbutes)
    {
        $result = null;
        $result .= '<child>' . PHP_EOL;
        $result .= $this->renderElements($child, $childAttrbutes);
        $result .= '</child>' . PHP_EOL;

        return $result;
    }

    /**
     * Render all product attributes.
     * @param object $product
     * @param array $attributes
     * @return string
     */
    protected function renderElements($product, $attributes, $handle = null)
    {
        $taxHelper = Mage::helper('tax');
        $store = Mage::app()->getStore();
        $handle = '';
        $result = null;
        foreach ($attributes as $attribute) {
            switch ($attribute) {
                case 'regular_price':
                    $result .= $this->getAttributeElementPrice($product, 'price', 'price', 'regular_price');
                    break;
                case 'prepaid_monthly_fee': // prepaid simonly
                case 'phone_price': // prepaid simonly
                    $result .= $this->getAttributeElementPrice($product, $attribute, null, $attribute);
                    break;
                case 'regular_price_priceplan':
                    $result .= $this->getAttributeElementPrice($product, 'maf', 'maf', 'regular_price');
                    break;
                case 'priceplan_price':
                    $mafIncl = $product->getMaf();
                    $mafEx = $taxHelper->getPrice($product, $mafIncl, false);
                    $devicePriceIncl = $product->getHawaiiSubscrDevicePrice();
                    $devicePriceEx = $taxHelper->getPrice($product, $devicePriceIncl, false);

                    $consBus = explode(',', $product->getProductSegment());
                    // Consumer
                    if (in_array(346, $consBus)) {
                        $priceplanPriceIncl = $mafIncl - $devicePriceIncl;
                    } // Business
                    else {
                        $priceplanPriceEx = $mafEx - $devicePriceEx;
                        $priceplanPriceIncl = $priceplanPriceEx * ($product->getTaxPercent() / 100 + 1);
                    }
                    $result .= $this->getAttributeElementPrice($product, $priceplanPriceIncl, null, $attribute);
                    break;
                case 'maf':
                    $result .= $this->getAttributeElementPrice($product, 'maf', 'maf', 'regular_maf');
                    break;
                case 'device_price':
                    $result .= $this->getAttributeElementPrice($product, 'hawaii_subscr_device_price', 'hawaii_subscr_device_price', $attribute);
                    break;
                case 'recurring_price':
                    $result .= $this->getAttributeElementPrice($product, 'maf', 'maf', $attribute);
                    break;
                case 'prijs_thuiskopieheffing_bedrag':
                    $thuisKopieHeffingType = $product->getAttributeText('prijs_thuiskopieheffing_type');
                    $sku = Mage::getStoreConfig(Dyna_PriceRules_Model_Observer::COPY_LEVY_PREFIX . strtolower($thuisKopieHeffingType));
                    if (!empty($sku)) {
                        $thuiskopieHeffing = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId());
                        $thuiskopieHeffing = $thuiskopieHeffing->load($thuiskopieHeffing->getIdBySku($sku));
                        $result .= $this->getAttributeElementPrice($product, $store->roundPrice($thuiskopieHeffing->getPrice()), 'prijs_thuiskopieheffing_bedrag', $attribute);
                    } else {
                        $result .= $this->getAttributeElementPrice($product, 0, 'prijs_thuiskopieheffing_bedrag', $attribute);
                    }
                    break;
                case 'prijs_aansluitkosten':
                    $result .= $this->getAttributeElementPrice($product, 'prijs_aansluitkosten', 'prijs_aansluitkosten', $attribute);
                    break;
                case 'is_hybrid_sim':
                    $value = 0;
                    $types = explode(',', $product->getIdentifierPackageType());
                    if (in_array(250, $types)) {
                        $value = 1;
                    }
                    $result .= '<' . $attribute . '>' . $value . '</' . $attribute . '>';
                    break;
                case Vznl_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE:
                    $sim = Mage::getModel('catalog/product')
                        ->getCollection()
                        ->addFieldToFilter('attribute_set_id', 53)
                        ->addAttributeToFilter(Vznl_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE, $product->getData($attribute))
                        ->addAttributeToFilter('type_id', array('eq' => 'simple'))
                        ->addAttributeToFilter('is_deleted', array('eq' => false))
                        ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                        ->addStoreFilter()
                        ->setPageSize(1, 1)
                        ->getLastItem();
                    $result .= $this->createShell($attribute, ($sim ? $sim->getSku() : null));
                    break;
                // Cast to boolean
                case 'hawaii_is_default':
                    $result .= '<' . $attribute . '>' . (int) $product->getData($attribute) . '</' . $attribute . '>';
                    break;
                case 'identifier_subsc_postp_simonly':
                    $result .= '<' . $attribute . '>' . ($product->getData($attribute) == 221 ? 1 : 0) . '</' . $attribute . '>';
                    break;
                case 'hawaii_subscr_hidden':
                    $result .= '<' . $attribute . '>' . ($product->getData('hawaii_subscr_hidden') == '' ? '0' : $product->getData('hawaii_subscr_hidden')) . '</' . $attribute . '>';
                    break;
                case 'aikido':
                    $result .= '<' . $attribute . '>' .(int)$product->getData($attribute). '</' . $attribute . '>';
                    break;
                case 'product_segment':
                    $result .= $this->printElement($product, $attribute, 'identifier_cons_bus');
                    break;
                default:
                    $result .= $this->printElement($product, $attribute);
                    break;
            }
        }

        return $result;
    }

    /**
     * Print element text or data attribute
     * @param object $product
     * @param string $element
     * @param string $handle
     * @return string
     */
    protected function printElement($product, $element, $handle = null)
    {
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(4, $element);
        $isDropdown = ($attributeModel->getFrontendInput() == 'multiselect' || $attributeModel->getFrontendInput() == 'select');
        if ($isDropdown) {
            return $this->getAttributeElementText($product, $element, $handle);
        } else {
            return $this->getAttributeElementData($product, $element, $handle);
        }
    }

    /**
     * Render the attibute element as text
     * @param object $product
     * @param string $element
     * @param string $handle
     * @return string
     */
    protected function getAttributeElementText($product, $element, $handle = null)
    {
        if ($handle === null) {
            $handle = $element;
        }
        if ($product->getData($element)) {
            $text = $product->getAttributeText($element);
            if (is_array($text)) {
                $text = implode(',', $text);
            }

            return '<' . $handle . '><![CDATA[' . trim($text) . ']]></' . $handle . '>' . PHP_EOL;
        } else {
            return '<' . $handle . '></' . $handle . '>' . PHP_EOL;
        }
    }

    /**
     * Render the attribute element as data and put it in CDATA block
     * @param object $product
     * @param string $element
     * @param string $handle
     * @return string
     */
    protected function getAttributeElementData($product, $element, $handle = null)
    {
        if ($handle === null) {
            $handle = $element;
        }

        return '<' . $handle . '><![CDATA[' . trim($product->getData($element)) . ']]></' . $handle . '>' . PHP_EOL;
    }

    /**
     * Render the attibute element as price tag
     * @param object $product
     * @param string $element
     * @param string $promoElement
     * @param string $handle
     * @return string
     */
    protected function getAttributeElementPrice($product, $element, $promoElement, $handle = null)
    {
        if (!$handle) {
            $handle = $element;
        }
        $promoElement = '';
        $result = null;
        $result .= '<' . $handle . '>' . PHP_EOL;
        $value = is_numeric($element) ? $element : $product->getData($element);
        $result .= $this->renderPrice($result, $value, $product) . PHP_EOL;
        $result .= '</' . $handle . '>' . PHP_EOL;

        return $result;
    }

    /**
     * Check if the given product has a valid parent configurable product
     * @param object $product
     * @return boolean
     */

    protected function productHasParent($product)
    {
        $configurable = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());

        if (!$configurable) {
            return false;
        }
        if ($configurable->getIsDeleted() == true || $configurable->getStatus() != Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
            return false;
        }

        return true;
    }

    protected function productHasMixmatch($subscription, $device)
    {
        $subscriptionSku = is_null($subscription) ? null : $subscription->getSku();
        $deviceSku = is_null($device) ? null : $device->getSku();

        if (!is_null($subscriptionSku)) {
            $mixMatches = Mage::getResourceModel('omnius_mixmatch/price_collection')
                ->addFieldToSelect(['source_sku', 'target_sku', 'device_subscription_sku', 'price'])
                ->addFieldToFilter('website_id', Mage::app()->getWebsite()->getId())
                ->addFieldToFilter('source_sku', $subscriptionSku);
        } else {
            $mixMatches = Mage::getResourceModel('omnius_mixmatch/price_collection')
                ->addFieldToSelect(['source_sku', 'target_sku', 'device_subscription_sku', 'price'])
                ->addFieldToFilter('website_id', Mage::app()->getWebsite()->getId())
                ->addFieldToFilter('target_sku', $deviceSku);
        }

        if (count($mixMatches) == 0) {
            return false;
        }

        return $mixMatches;
    }

    protected function productIsValid($product)
    {
        $hawaiiChannel = $product->getProdspecsHawaiiChannel();
        if (empty($hawaiiChannel)) {
            $this->addError($product->getSku(), 'product', 'Product ' . $product->getSku() . ' does not have a Hawaii Channel');

            return false;
        } elseif (Mage::helper('dyna_catalog')->is(array(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $product) && $product->getData('identifier_subsc_postp_simonly') != 221) {
            if (!$this->productHasMixmatch($product, null)) {
                $this->addError($product->getSku(), 'product', 'Priceplan ' . $product->getSku() . ' does not have a MixMatch entry');

                return false;
            }
        } elseif (Mage::helper('dyna_catalog')->is(array(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE), $product)) {
            if (!$this->productHasMixmatch(null, $product) && $product->getData('identifier_device_post_prepaid') != 159) {
                $this->addError($product->getSku(), 'product', 'Device ' . $product->getSku() . ' does not have a MixMatch entry');

                return false;
            }
        }

        return true;
    }
}
