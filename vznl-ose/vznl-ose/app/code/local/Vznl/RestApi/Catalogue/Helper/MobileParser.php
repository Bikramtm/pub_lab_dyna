<?php

/**
 * Class Vznl_RestApi_Catalogue_Helper_MobileParser
 */
class Vznl_RestApi_Catalogue_Helper_MobileParser extends Vznl_RestApi_Catalogue_Helper_Parser
{
    public const DEVICE_ATTRIBUTE_SET = 35;
    public const INTERNET_ATTRIBUTE_SET = 49;
    public const SUBSCRIPTION_ATTRIBUTE_SET = 37;
    public const INTERNET_SUBSCRIPTION_ATTRIBUTE_SET = 48;
    public const SIM_ATTRIBUTE_SET = 53;
    public const PREPAID_SIM_ATTRIBUTE_SET = 55;
    public const ACCESSORY_ATTRIBUTE_SET = 46;
    public const ADDON_ATTRIBUTE_SET = 50;
    public const ADDON_HYBRID_ATTRIBUTE_SET = 47;
    public const DEVICE_RC_ATTRIBUTE_SET = 58;
    public const PROMOTION_ATTRIBUTE_SET = 54;

    /**
     * @var  Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $deviceProducts;

    /**
     * @var  Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $subscriptionProducts;

    /**
     * @var  Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $simProducts;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $accessoryProducts;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $addOnProducts;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $deviceRCProducts;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $promotionProducts;

    /**
     * @var array
     */
    protected $deviceProductAttributes = [
        'prodspecs_brand',
        'prodspecs_afmetingen_lengte',
        'prodspecs_afmetingen_breedte',
        'prodspecs_afmetingen_dikte',
        'prodspecs_afmetingen_unit',
        'prodspecs_weight_gram',
        'prodspecs_besturingssysteem',
        'prodspecs_processor',
        'prodspecs_schermdiagonaal_inch',
        'prodspecs_touchscreen',
        'prodspecs_schermresolutie',
        'prodspecs_display_features',
        'prodspecs_werkgeheugen_mb',
        'prodspecs_opslagcapaciteit',
        'prodspecs_opslag_uitbreidbaar',
        'prodspecs_camera',
        'prodspecs_megapixels_1e_cam',
        'prodspecs_flitser',
        'prodspecs_filmen_in_hd',
        'prodspecs_audio_out',
        'prodspecs_hdmi',
        'prodspecs_2e_camera',
        'prodspecs_4g_lte',
        'prodspecs_3g',
        'prodspecs_gprs_edge',
        'prodspecs_wifi',
        'prodspecs_wifi_frequenties',
        'prodspecs_bluetooth',
        'prodspecs_bluetooth_version',
        'prodspecs_nfc',
        'prodspecs_gps',
        'prodspecs_accu_verwisselb',
        'prodspecs_batterij',
        'prodspecs_standby_tijd',
        'prodspecs_spreektijd',
        'identifier_hawaii_type',
        'identifier_hawaii_id',
        'prodspecs_hawaii_preorder',
        'identifier_hawaii_type_name',
        'prodspecs_hawaii_usp1',
        'prodspecs_hawaii_usp2',
        'prodspecs_hawaii_usp3',
        'prodspecs_network',
        'prodspecs_network_3g',
        'prodspecs_network_4g',
        'prodspecs_ecoscore',
        'prodspecs_hawaii_os_versie',
        'identifier_hawaii_prep_orig_id',
        'prijs_thuiskopieheffing_bedrag',
        'prodspecs_hawaii_channel',
        'hawaii_def_subscr_cbu',
        'hawaii_def_subscr_ebu',
        'identifier_simcard_formfactor',
    ];

    /**
     * @var array
     */
    protected $subscriptionProductAttributes = [
        'hawaii_subscr_family',
        'hawaii_subscr_kind',
        'prodspecs_network',
        'prodspecs_4g_lte',
        'prodspecs_aantal_belminuten',
        'prodspecs_data_aantal_mb',
        'prodspecs_sms_amount',
        'identifier_hawaii_id',
        'identifier_hawaii_subs_formula',
        'identifier_hawaii_subscription',
        'prodspecs_hawaii_def_phone',
        'prodspecs_hawaii_usp1',
        'prodspecs_hawaii_usp2',
        'prodspecs_hawaii_usp3',
        'prijs_belminuut_buiten_bundel',
        'prodspecs_hawaii_channel',
        'hawaii_subscr_hidden',
    ];

    /**
     * @var array
     */
    protected $simProductAttributes = [
        'identifier_hawaii_id',
        'is_hybrid_sim',
        'identifier_simcard_type',
    ];

    /**
     * @var array
     */
    protected $accessoryProductAttributes = [
        'prodspecs_brand',
        'prodspecs_afmetingen_lengte',
        'prodspecs_afmetingen_breedte',
        'prodspecs_afmetingen_dikte',
        'prodspecs_afmetingen_unit',
        'prodspecs_weight_gram',
        'prodspecs_kleur',
        'identifier_hawaii_type',
        'identifier_hawaii_id',
        'prodspecs_hawaii_preorder',
        'identifier_hawaii_type_name',
        'prodspecs_hawaii_html_kleurcode',
        'prodspecs_hawaii_usp1',
        'prodspecs_hawaii_usp2',
        'prodspecs_hawaii_usp3',
        'prodspecs_network',
        'prodspecs_network_3g',
        'prodspecs_network_4g',
        'prodspecs_ecoscore',
        'prodspecs_hawaii_os_versie',
        'identifier_hawaii_prep_orig_id',
        'prijs_thuiskopieheffing_bedrag',
        'prodspecs_hawaii_channel',
    ];

    /**
     * @var array
     */
    protected $addonProductAttributes = [
        'identifier_commitment_months',
        'prodspecs_4g_lte',
        'identifier_hawaii_type',
        'identifier_hawaii_id',
        'prodspecs_hawaii_usp1',
        'prodspecs_hawaii_usp2',
        'prodspecs_hawaii_usp3',
        'prodspecs_data_mb_config',
        'prodspecs_belmin_sms_config',
        'prijs_belminuut_buiten_bundel',
        'hawaii_hybride_type',
        'hawaii_hybride_voice_data',
        'hawaii_is_default',
        'prodspecs_hawaii_channel',
    ];

    /**
     * @var array
     */
    protected $deviceRCProductAttributes = [
        'identifier_hawaii_id',
        'identifier_addon_type',
        'loan_indicator',
    ];

    /**
     * @var array
     */
    protected $promotionProductAttributes = [];

    /**
     * Set all mobile products
     */
    protected function loadProducts()
    {
        $this->deviceProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter(
                'attribute_set_id',
                [self::DEVICE_ATTRIBUTE_SET, self::INTERNET_ATTRIBUTE_SET]
            )
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->subscriptionProducts = Mage::getModel('catalog/product')
            ->getCollection()->addAttributeToSelect('*')
            ->addFieldToFilter(
                'attribute_set_id',
                [self::SUBSCRIPTION_ATTRIBUTE_SET, self::INTERNET_SUBSCRIPTION_ATTRIBUTE_SET]
            )
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->simProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter(
                'attribute_set_id',
                [self::SIM_ATTRIBUTE_SET, self::PREPAID_SIM_ATTRIBUTE_SET]
            )
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->accessoryProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', self::ACCESSORY_ATTRIBUTE_SET)
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->addOnProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter(
                'attribute_set_id',
                [self::ADDON_ATTRIBUTE_SET, self::ADDON_HYBRID_ATTRIBUTE_SET]
            )
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->deviceRCProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', self::DEVICE_RC_ATTRIBUTE_SET)
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->promotionProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', self::PROMOTION_ATTRIBUTE_SET)
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addAttributeToFilter('stack', ['eq' => 'mobile'])
            ->addStoreFilter();
    }

    /**
     * @return array
     */
    public function getProductsWithAttributeSet(): array
    {
        $this->loadProducts();

        return [
            [
                'products' => $this->deviceProducts,
                'attributes' => $this->deviceProductAttributes,
            ],
            [
                'products' => $this->subscriptionProducts,
                'attributes' => $this->subscriptionProductAttributes,
            ],
            [
                'products' => $this->simProducts,
                'attributes' => $this->simProductAttributes,
            ],
            [
                'products' => $this->accessoryProducts,
                'attributes' => $this->accessoryProductAttributes,
            ],
            [
                'products' => $this->addOnProducts,
                'attributes' => $this->addonProductAttributes,
            ],
            [
                'products' => $this->deviceRCProducts,
                'attributes' => $this->deviceRCProductAttributes,
            ],
            [
                'products' => $this->promotionProducts,
                'attributes' => $this->promotionProductAttributes,
            ],
        ];
    }
}
