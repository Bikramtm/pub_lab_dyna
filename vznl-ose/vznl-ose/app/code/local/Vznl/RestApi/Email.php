<?php

class Vznl_RestApi_Email extends Vznl_RestApi_Abstract
{
    /**
     * @var array
     */
    public static $routes = array(
         array(
            'method' => 'POST',
            'path' => '/email/resend-confirmation',
            'class' => 'Vznl_RestApi_Email_ResendConfirmation',
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'PUT',
            'path' => '/email',
            'class' => 'Vznl_RestApi_Email_Email',
            'log' => true,
            'active' => true,
        ),
    );
}
