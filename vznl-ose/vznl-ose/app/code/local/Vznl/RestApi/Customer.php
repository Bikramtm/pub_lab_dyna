<?php

class Vznl_RestApi_Customer extends Vznl_RestApi_Abstract
{
    /**
     * @var array
     */
    public static $routes = array(
        array(
            'method' => 'GET',
            'path' => '/customer/get/username/:username',
            'class' => 'Vznl_RestApi_Customer_GetByUsername',
            'log' => false,
            'active' => true,
        ),
    );
}
