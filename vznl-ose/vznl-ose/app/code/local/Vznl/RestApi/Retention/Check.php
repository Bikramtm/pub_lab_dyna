<?php

/**
 * Class RestApi_Retention_Check
 * This is used for LS to make the retainability check "RetrieveCustomerCreditProfile" towards DealerAdapter with
 * dob/kvk (depending if is a business customer or not) and ctn;
 * We have no customer/no ban in LS.
 */
class Vznl_RestApi_Retention_Check extends Vznl_RestApi_Quotes_Abstract
{
    /**
     * @var Mage_Core_Controller_Request_Http
     */
    protected $request;

    /**
     * @var array
     */
    private $postData;

    /**
     * Set the request received from LS
     * and get the post data from the Rest API call
     *
     * @param Mage_Core_Controller_Request_Http $request
     * @throws Zend_Json_Exception
     */
    public function __construct(Mage_Core_Controller_Request_Http $request)
    {
        $this->request = $request;
        $this->postData = Zend_Json::decode(file_get_contents("php://input"));
    }

    /**
     * Calling "RetrieveCustomerCreditProfile" from DealerAdapter with the post data received from LS (dob/kvk, ctn, isBusiness)
     * Returns the retainable numbers with the corresponding status
     *
     * @return array
     * @throws Exception
     */
    public function process()
    {
        $response = array();
        $ban = null;

        /** @var Dyna_Service_Model_DotAccessor $dot */
        $dot = Mage::getSingleton('dyna_service/dotAccessor');

        /** @var Vznl_Service_Model_Client_DealerAdapterClient $client */
        $client = Mage::helper('vznl_service')->getClient('dealer_adapter');

        try {
            $result = $client->getCreditProfileByCtn($this->postData);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            return ['errorMessage' => 'Error in connection with DealerAdapter. Please try again later.'];
        }

        // Check if the answer is : customer not found
        $foundCustomer = $this->isCustomerFound($dot, $response);

        if (!$foundCustomer) {
            return ['errorMessage' => 'Customer not found. The retention cannot be done.'];
        }

        /*
        retainability_indicator
        Can be;
        - Retainable
        - Block Over Ruleable (= overuleable by backoffice)
        - Blocked (= not retainable)
        */
        // get all CTNs
        $statuses = $this->getAllCTNs($dot, $result);

        // get retainable CTNs
        $retainable = array_filter($statuses, function($ctn) { return $ctn['retainable']; });

        $response['ctns'] = array_values($retainable);

        // Retainable errors on customer level
        $retainableError = $dot->getValue($result, 'party_customer.customer.customer_credit_profile.credit_profile_check_results.result_code');
        if ($retainableError) {
            foreach ($response['ctns'] as $key => $value) {
                $response['ctns'][$key]['retainable'] = false;
                $response['ctns'][$key]['retainableStatus'] = $retainableError;
            }
        }

        $ban = $this->getBanFromResponse($dot, $result);
        return ['subscriptions' => $response, 'ban' => $ban];
    }

    /**
     * This is the same method as in RestApi_Retention_Get and is used to parse the response from DealerAdapter "RetrieveCustomerCreditProfile"
     * in order to find the retainable numbers with the status
     *
     * @param Dyna_Service_Model_DotAccessor $dot
     * @param array $result
     * @return array
     * Parse the credit profile string and get all ctns
     */
    protected function getAllCTNs($dot, $result)
    {
        $statuses = array();
        foreach ($dot->getValue($result, 'party_end_user.subscription') as $key => $value) {
            if (is_numeric($key)) {
                $statuses[] = array(
                    'status'     => $dot->getValue($value, 'status'),
                    'ctn'        => $dot->getValue($value, 'ctn'),
                    'retainable' => $dot->getValue(
                            $value,
                            'customer_account.customer.customer_credit_profile.retainability_indicator'
                        ) == 'Retainable',
                    'retainableStatus' => $dot->getValue($value, 'customer_account.customer.customer_credit_profile.retainability_indicator'),
                    'reasoncode' => $dot->getValue($value, 'customer_account.customer.customer_credit_profile.credit_profile_check_results.result_code')
                );
            } else {
                $statuses = array(array(
                    'status'     => $dot->getValue($result, 'party_end_user.subscription.status'),
                    'ctn'        => $dot->getValue($result, 'party_end_user.subscription.ctn'),
                    'retainable' => $dot->getValue(
                            $result,
                            'party_end_user.subscription.customer_account.customer.customer_credit_profile.retainability_indicator'
                        ) == 'Retainable',
                    'retainableStatus' => $dot->getValue($result, 'party_end_user.subscription.customer_account.customer.customer_credit_profile.retainability_indicator'),
                    'reasoncode' => $dot->getValue($result, 'party_end_user.subscription.customer_account.customer.customer_credit_profile.credit_profile_check_results.result_code')
                ));
                return $statuses;
            }
        }

        return $statuses;
    }

    /**
     * This is the same method as in RestApi_Retention_Get and is used to parse the response from DealerAdapter "RetrieveCustomerCreditProfile"
     * in order to find the retainable numbers with the status
     *
     * @param Dyna_Service_Model_DotAccessor $dot
     * @param array $response
     * @return array
     */
    private function getBanFromResponse($dot, $response)
    {
        $ban = $dot->getValue($response, 'party_customer.customer.id');
        return $ban;
    }

    /**
     * Check the response to see if the customer is found or not.
     *
     * @param Dyna_Service_Model_DotAccessor $dot
     * @param array $response
     * @return bool
     */
    private function isCustomerFound($dot, $response)
    {
        $code = $dot->getValue($response, 'status.code');
        $responseCode = $dot->getValue($response, 'status.response_code');

        if ($responseCode == -8 && (strpos($code, 'DYNA-100029') || strpos($code, 'DYNA-100032') || strpos($code, 'DYNA-100003'))) {
            return false;
        }

        return true;
    }
}