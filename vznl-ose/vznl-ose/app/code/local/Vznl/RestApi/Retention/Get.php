<?php

class Vznl_RestApi_Retention_Get extends Vznl_RestApi_Quotes_Abstract
{

    /**
     * @var string
     */
    const CUSTOMER_DOB_DUMMY_VALUE = '1970-01-01T00:00:00';

    /**
     * @var string
     */
    const DEALER_SPECIAL_CODE_CONFIG = 'vodafone_service/dealer_adapter/dealer_special_code';

    /**
     * @var Vznl_Customer_Model_Customer
     */
    private $customer;

    /**
     * @var array
     */
    protected $errors;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @throws Exception
     */
    public function __construct($request = null)
    {
        $routeParts = explode('/', $request->getQuery('route'));
        $ban = $routeParts[3];

        $this->setCustomerByBan($ban);
    }

    /**
     * @param string $ban
     * @return void
     * @throws Exception
     */
    private function setCustomerByBan($ban)
    {
        if (empty($ban)) {
            throw new Exception('Please provide a BAN parameter');
        }
        /** @var Vznl_Customer_Model_Customer_Customer $customer */
        $customer = Mage::getModel('vznl_customer/customer');
        // Map search to model
        $customer->addData([
            'dob' => self::CUSTOMER_DOB_DUMMY_VALUE,
            'is_business' => false,
            'customer_number' => $ban,
        ]);

        $this->customer = $customer;
    }

    /**
     * Process the HTTP request.
     *
     * @return array
     * @throws Exception
     */
    public function process()
    {
        Mage::getSingleton('customer/session')->unsetCustomer();
        Mage::getSingleton('customer/session')->clear();

        // Set Segment/Value/Campaign info
        $response['customer'] = [
            'customer_value'=> null,
            'value_segment'=> null,
            'campaign' => null
        ];

        // Drop the local DB connection and call web service
        $db = Mage::getSingleton('core/resource')->getConnection('core_write');
        $db->closeConnection();

        /** @var Dyna_Service_Model_DotAccessor $dot */
        $dot = Mage::getSingleton('dyna_service/dotAccessor');

        /** @var Vznl_Service_Model_Client_DealerAdapterClient $client */
        $client = Mage::helper('vznl_service')->getClient('dealer_adapter');

        try {
            $specialDealerCode = Mage::getStoreConfig(self::DEALER_SPECIAL_CODE_CONFIG);
            $result = $client->getCreditProfile($this->getCustomer(), $specialDealerCode);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            throw new Exception('Error in connection with DealerAdapter: '. $e->getMessage());
        }

        /*
        Retainability_indicator
        Can be;
        - Retainable
        - Block Over Ruleable (= overuleable by backoffice)
        - Blocked (= not retainable)
        */
        // Get all CTNs
        $statuses = $this->getAllCTNs($dot, $result);

        // Get retainable CTNs
        $retainable = array_filter($statuses, function($ctn) { return $ctn['retainable']; });

        $activeCTNCount = count(
            array_filter(
                $statuses,
                function ($ctn) {
                    return $ctn['status'] == 'ACTIVE';
                }
            )
        );
        $allRetainableCount = count($retainable);

        if ($allRetainableCount != count($retainable)) {
            // There are retainables in a package, so no oneOff deal
            $oneOffDeal = false;
        } else {
            $oneOffDeal = Mage::getModel('ctn/ctn')->canActivateOneOffDeal(
                $activeCTNCount,
                $allRetainableCount
            );
        }

        // $response['oneOffDeal'] = $oneOffDeal;
        $response['ctns'] = array_values($retainable);

        // Retainable errors on customer level
        $retainableError = $dot->getValue($result, 'party_customer.customer.customer_credit_profile.credit_profile_check_results.result_code');
        if ($retainableError) {
            foreach ($response['ctns'] as $key => $value) {
                $response['ctns'][$key]['retainable'] = false;
                $response['ctns'][$key]['retainableStatus'] = $retainableError;
            }
        }
        return $response;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return Vznl_Customer_Model_Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }


    /**
     * @param Dyna_Service_Model_DotAccessor $dot
     * @param array $result
     * @return array
     * Parse the credit profile string and get all ctns
     */
    protected function getAllCTNs($dot, $result)
    {
        $statuses = array();
        foreach ($dot->getValue($result, 'party_end_user.subscription') as $key => $value) {
            if (is_numeric($key)) {
                $statuses[] = array(
                    'status'     => $dot->getValue($value, 'status'),
                    'ctn'        => $dot->getValue($value, 'ctn'),
                    'retainable' => $dot->getValue(
                            $value,
                            'customer_account.customer.customer_credit_profile.retainability_indicator'
                        ) == 'Retainable',
                    'retainableStatus' => $dot->getValue($value, 'customer_account.customer.customer_credit_profile.retainability_indicator'),
                    'reasoncode' => $dot->getValue($value, 'customer_account.customer.customer_credit_profile.credit_profile_check_results.result_code')
                );
            } else {
                $statuses = array(array(
                    'status'     => $dot->getValue($result, 'party_end_user.subscription.status'),
                    'ctn'        => $dot->getValue($result, 'party_end_user.subscription.ctn'),
                    'retainable' => $dot->getValue(
                            $result,
                            'party_end_user.subscription.customer_account.customer.customer_credit_profile.retainability_indicator'
                        ) == 'Retainable',
                    'retainableStatus' => $dot->getValue($result, 'party_end_user.subscription.customer_account.customer.customer_credit_profile.retainability_indicator'),
                    'reasoncode' => $dot->getValue($result, 'party_end_user.subscription.customer_account.customer.customer_credit_profile.credit_profile_check_results.result_code')
                ));
                return $statuses;
            }
        }

        return $statuses;
    }

}
