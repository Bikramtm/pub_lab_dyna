<?php

class Vznl_RestApi_Ping_Ping
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function process()
    {
        $response = [];
        $response['pong'] = time();
        return $response;
    }
}
