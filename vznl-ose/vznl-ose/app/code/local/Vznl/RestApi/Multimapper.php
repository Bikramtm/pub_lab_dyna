<?php

class Vznl_RestApi_Multimapper extends Vznl_RestApi_Abstract
{
    /**
     * @var array
     */
    public static $routes = [
        [
            'method' => 'POST',
            'path' => '/multimapper',
            'class' => 'Vznl_RestApi_Multimapper_Get',
            'log' => true,
            'active' => true,
        ],
        [
            'method' => 'GET',
            'path' => '/multimapper/extract',
            'class' => 'Vznl_RestApi_Multimapper_Extract',
            'log' => true,
            'active' => true,
        ],
    ];
}
