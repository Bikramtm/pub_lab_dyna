<?php

class Vznl_RestApi_Quotes
{
    /**
     * @var array
     */
    public static $routes = array(
        array(
            'method' => 'POST',
            'path' => '/quotes',
            'class' => 'Vznl_RestApi_Quotes_Save',
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/quotes/:quoteId/save-and-email',
            'class' => 'Vznl_RestApi_Quotes_SaveAndEmail',
            'validators' => array(
                'quoteId' => 'integer',
            ),
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'GET',
            'path' => '/quotes/:quoteId/:email',
            'class' => 'Vznl_RestApi_Quotes_Get',
            'validators' => array(
                'email' => 'email',
                'quoteId' => 'integer',
            ),
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'GET',
            'path' => '/quotes/:quoteId/shops',
            'class' => 'Vznl_RestApi_Quotes_Shops',
            'validators' => array(
                'quoteId' => 'integer',
            ),
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'GET',
            'path' => '/quotes/:email',
            'class' => 'Vznl_RestApi_Quotes_ListByEmail',
            'validators' => array(
                'email' => 'email',
            ),
            'log' => true,
            'active' => true,
        ),
    );
}
