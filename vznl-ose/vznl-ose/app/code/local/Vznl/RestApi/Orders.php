<?php

class Vznl_RestApi_Orders extends Vznl_RestApi_Abstract
{
    /**
     * @var array
     */
    public static $routes = array(
        array(
            'method' => 'POST',
            'path' => '/orders',
            'class' => 'Vznl_RestApi_Orders_Create',
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/orders/build',
            'class' => 'Vznl_RestApi_Orders_Build',
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/orders/:order_number/submit',
            'class' => 'Vznl_RestApi_Orders_Submit',
            'validators' => array(
                'order_number' => 'integer',
            ),
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'GET',
            'path' => '/orders/:order_id',
            'class' => 'Vznl_RestApi_Orders_GetHawaii',
            'validators' => array(
                'order_id' => 'integer',
            ),
            'log' => false,
            'active' => true,
        ),
        array(
            'method' => 'GET',
            'path' => '/orders/get/:order_number',
            'class' => 'Vznl_RestApi_Orders_Get',
            'validators' => array(
                'order_number' => 'integer',
            ),
            'log' => false,
            'active' => true,
        ),
        array(
            'method' => 'GET',
            'path' => '/orders/overview/:ban',
            'class' => 'Vznl_RestApi_Orders_Overview',
            'validators' => array(
                'ban' => 'integer',
            ),
            'log' => false,
            'active' => true,
        ),
        array(
            'method' => 'GET',
            'path' => '/orders/get/delivery/:id',
            'class' => 'Vznl_RestApi_Orders_GetDeliveryOrder',
            'validators' => array(
                'id' => 'integer',
            ),
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/orders/:order_id/modify',
            'class' => 'Vznl_RestApi_Orders_Modify',
            'validators' => array(
                'order_id' => 'integer',
            ),
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/orders/:order_number/cancel',
            'class' => 'Vznl_RestApi_Orders_Cancel',
            'validators' => array(
                'order_number' => 'integer',
            ),
            'log' => true,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/orders/search',
            'class' => 'Vznl_RestApi_Orders_Search',
            'log' => false,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/orders/:order_number/packages/:package_id/register-return',
            'class' => 'Vznl_RestApi_Orders_Packages_RegisterReturn',
            'validators' => array(
                'order_number' => 'integer',
                'package_id' => 'integer',
            ),
            'log' => true,
            'active' => true,
        ),
    );
}
