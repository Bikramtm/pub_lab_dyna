<?php
class Vznl_RestApi_Ping extends Vznl_RestApi_Abstract
{
    public static $routes = array(
         array(
            'method'=>'GET',
            'path'=>'/ping',
            'class'=> 'Vznl_RestApi_Ping_Ping',
            'log' => false,
            'active'=>true,
        ),
    );
}