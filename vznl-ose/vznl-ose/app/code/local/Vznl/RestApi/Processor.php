<?php

abstract class Vznl_RestApi_Processor
{
    /**
     * @var Mage_Core_Controller_Request_Http
     */
    protected $request;

    /**
     * @var array
     */
    protected $route;

    /**
     * @var null|array
     */
    protected $routeParams = null;

    /**
     * @var null|array
     */
    protected $bodyParams = null;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @param array $route
     */
    public function __construct(Mage_Core_Controller_Request_Http $request, array $route)
    {
        $this->request = $request;
        $this->route = $route;
    }

    /**
     * @return array
     */
    protected function getBodyParams()
    {
        if (!$this->bodyParams) {
            $output = json_decode($this->request->getRawBody(), true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                Mage::log(sprintf("Invalid body provided: %s", $this->request->getRawBody()), Zend_Log::NOTICE);

                $output = array();
            }

            if (!is_array($output)) {
                Mage::log(sprintf("Invalid body provided: %s", $this->request->getRawBody()), Zend_Log::NOTICE);
                // Always return an array
                $output = array();
            }

            $this->bodyParams = $output;
        }
        if (empty($this->bodyParams)) {
            $this->bodyParams = $this->request->getParams();
        }

        return $this->bodyParams;
    }

    /**
     * This will parse the route params.
     *
     * @return array
     */
    protected function getRouteParams()
    {
        if (!$this->routeParams) {
            $pathParts = explode("/", $this->route["path"]);
            $routeParts = explode("/", $this->request->getQuery("route"));

            $output = array();
            $pathPartsCount = count($pathParts);
            for ($i = 0; $i < $pathPartsCount; $i++) {
                if (substr($pathParts[$i], 0, 1) != ":") {
                    continue;
                }

                $output[substr($pathParts[$i], 1)] = $routeParts[$i];
            }

            $this->routeParams = $output;
        }

        return $this->routeParams;
    }

    /**
     * Process the HTTP request.
     *
     * @return array
     */
    public abstract function process();
}