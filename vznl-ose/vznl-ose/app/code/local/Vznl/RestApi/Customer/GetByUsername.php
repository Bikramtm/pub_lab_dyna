<?php

/**
 * Dependencies:
 * Vznl_Service_Helper_Data
 * Vznl_Service_Model_Client_DealerAdapterClient
 */
class Vznl_RestApi_Customer_GetByUsername extends Vznl_RestApi_Customer_Abstract
{
    /**
     * @var Mage_Core_Controller_Request_Http
     */
    protected $request;

    /**
     * @var string
     */
    protected $username;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct($request)
    {
        $this->request = $request;

        $routeParts = explode('/', $request->getQuery('route'));
        $this->username = $routeParts[4];
    }

    /**
     * @return mixed
     */
    public function process()
    {
        /** @var Vznl_Service_Helper_Data $serviceHelper */
        $serviceHelper = Mage::helper('vznl_service');
        
        /** @var Vznl_Service_Model_Client_DealerAdapterClient $client */
        $client = $serviceHelper->getClient('dealer_adapter');

        $response = $client->retrieveAuthorizationDetails($this->username);

        return $response;
    }
}
