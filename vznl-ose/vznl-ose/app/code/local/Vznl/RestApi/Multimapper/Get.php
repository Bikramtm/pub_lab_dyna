<?php

class Vznl_RestApi_Multimapper_Get extends Vznl_RestApi_Orders_Abstract
{
    /**
     * @var array
     */
    protected $postData;

    /**
     * @var Mage_Core_Controller_Request_Http
     */
    protected $request;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct($request)
    {
        $this->postData = Zend_Json::decode(file_get_contents("php://input"));
        $this->request = $request;
    }

    /**
     * @return array
     * @todo Dependent on database migration, test after
     */
    public function process()
    {
        try {
            return $this->map();
        } catch (Exception $e) {
            Mage::logException($e);
            return [
                'error' => true,
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    private function map()
    {
        $response = [];

        /**
         * @var Dyna_MultiMapper_Helper_Data
         */
        $multimapperHelper = Mage::helper('vznl_multimapper');

        foreach ($this->postData as $data) {
            $promoSku = isset($data['promo_sku']) ? $data['promo_sku'] : null;
            $multimapperCollection = [];
            $multimapperCollection[] = $multimapperHelper->getMappingInstanceBySku($data['priceplan_sku'], $promoSku);

            foreach ($multimapperCollection as $multimapper) {
                $components = [];
                for ($i = 1; $i < 10; $i++) {
                    if (is_null($multimapper->getData('oms_component_attr_'.$i)) &&
                        is_null($multimapper->getData('oms_attr_code_'.$i)) &&
                        is_null($multimapper->getData('oms_attr_value_'.$i))
                    ) {
                        continue;
                    } elseif( empty($multimapper->getData('oms_component_attr_'.$i)) ||
                        empty($multimapper->getData('oms_attr_code_'.$i)) ||
                        empty($multimapper->getData('oms_attr_value_'.$i))
                    ) {
                        throw new Exception('Invalid data in multimapper for sku:"'.$data['priceplan_sku'].'" and promo:"'.$data['promo_sku'].'". Error: component:"'.$multimapper->getData('oms_component_attr_' . $i).'", code:"'.$multimapper->getData('oms_attr_code_' . $i).'", value:"'.$multimapper->getData('oms_attr_value_' . $i).'"', 1);
                    } else {
                        $components[] = [
                            'oms_component_attr' => $multimapper->getData('oms_component_attr_' . $i),
                            'oms_component_code' => $multimapper->getData('oms_attr_code_' . $i),
                            'oms_component_value' => $multimapper->getData('oms_attr_value_' . $i),
                        ];
                    }
                }
                $response[] = [
                    'priceplan_sku' => $multimapper->getPpSku(),
                    'priceplan_description' => $multimapper->getPpSkuDescription(),
                    'promotion_sku' => $multimapper->getPromoSku(),
                    'promotion_description' => $multimapper->getPromoSkuDescription(),
                    'oms_mc_code' => $multimapper->getOmsMcCode(),
                    'oms_bo_code' => $multimapper->getOmsBoCode(),
                    'oms_bo_type' => $multimapper->getOmsBoType(),
                    'oms_pbo_code' => $multimapper->getOmsPboCode(),
                    'oms_pbo_type' => $multimapper->getOmsPboType(),
                    'oms_product_offering_code' => $multimapper->getOmsProductOfferingCode(),
                    'oms_product_offering_id' => $multimapper->getOmsProductOfferingId(),
                    'oms_product_code' => $multimapper->getOmsProductCode(),
                    'oms_comp_code_path' => $multimapper->getOmsCompCodePath(),
                    'oms_comp_id' => $multimapper->getOmsCompId(),
                    'components' => $components,
                ];
            }
        }

        return $response;
    }
}

