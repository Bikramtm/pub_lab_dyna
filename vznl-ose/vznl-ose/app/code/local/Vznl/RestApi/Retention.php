<?php

class Vznl_RestApi_Retention extends Vznl_RestApi_Abstract
{
    /**
     * @var array
     */
    public static $routes = array(
        array(
            'method' => 'GET',
            'path' => '/retention/get/:ban',
            'class' => 'Vznl_RestApi_Retention_Get',
            'validators' => array(
                'ban' => 'integer',
            ),
            'log' => false,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/retention/check',
            'class' => 'Vznl_RestApi_Retention_Check',
            'log' => false,
            'active' => true,
        ),
    );
}
