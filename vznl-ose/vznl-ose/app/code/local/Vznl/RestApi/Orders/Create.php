<?php

class Vznl_RestApi_Orders_Create extends Vznl_RestApi_Orders_Abstract
{
    /**
     * @var Mage_Core_Controller_Request_Http
     */
    protected $request;
    protected $postData;
    protected $quote;

    /**
     * Creates a new order.
     *
     * @param Mage_Core_Controller_Request_Http $request
     * @throws Exception
     */
    public function __construct($request)
    {
        $this->request = $request;
        $this->postData = Zend_Json::decode(file_get_contents("php://input"));

        if (is_null($this->postData['dynalean_quote_id']) || $this->postData['dynalean_quote_id'] === '') {
            throw new Exception('There is no quote for this cart instance yet');
        }
    }

    /**
     * Process the HTTP request.
     *
     * @return array
     */
    public function process()
    {
        if (!isset($this->postData['errors']) || !is_array($this->postData['errors'])) {
            $this->postData['errors'] = array();
        }

        /**
         * @var Mage_Sales_Model_Quote $originalQuote
         */
        $originalQuote = Mage::getModel('sales/quote')->load($this->postData['dynalean_quote_id']);

        if ($originalQuote->getId()) {
            $this->setIltData();
            $this->checkFFRequest($originalQuote, $this->postData['errors']);

            if ($originalQuote->getHawaiiQuoteErrors() !== null || count($this->getErrors()) > 0) {
                // Errors
                foreach (unserialize($originalQuote->getHawaiiQuoteErrors()) as $error) {
                    $key = str_replace('%INDEX%', $error['index'], $error['key']);
                    $this->postData['errors'][] = array("item_key" => $key, "message" => $error['message']);
                }
                foreach ($this->getErrors() as $row) {
                    $this->postData['errors'][] = $row;
                }
                return $this->postData;
            }
        }

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('sales/quote');
        $quote->setStoreId(Mage::app()->getStore()->getId());
        $quote->setCustomerId(null);
        $quote->setRemoteIp(Mage::helper('core/http')->getRemoteAddr());
        $quote->setTotalsCollectedFlag(true)->save();
        $this->_getCheckoutSession()->setQuoteId($quote->getId());
        try {
            $this->getCart()->getQuote()->setIsMultiShipping(true);
        } catch (Exception $e) {
            $this->setupErrors($e);
        }
        $this->getCart()->setQuote($quote);

        $superOrder = Mage::getModel('superorder/superorder')->createNewSuperorder();
        /**
         * @var Mage_Sales_Model_Convert_Quote
         */
        $convert = Mage::getModel('sales/convert_quote');

        $quote->getShippingAddress()->setPaymentMethod('checkmo');
        $quote->getShippingAddress()->setCollectShippingRates(false);
        $payment = $quote->getPayment();
        $payment->importData(array(
            'method' => 'checkmo'
        ));
        $quote->save();

        // Save customer details to vNext search
        Mage::helper('vznl_customer/search')->createApiCustomer($quote->getCustomer(), $quote->getCustomer()->getDefaultBillingAddress(), $quote, 'SaveExistingFields');

        $order = $convert->toOrder($quote);
        $shipping = $convert->addressToOrderAddress($quote->getShippingAddress());
        $billing = $convert->addressToOrderAddress($quote->getBillingAddress());
        $order->setShippingAddress($shipping);
        $order->setBillingAddress($billing);
        foreach ($quote->getAllItems() as $item){
            $orderItem = $convert->itemToOrderItem($item);
            if ($item->getParentItem()) {
                $orderItem->setParentItem($order->getItemByQuoteItemId($item->getParentItem()->getId()));
            }
            $order->addItem($orderItem);
        }

        if ($quote->isIltRequired()) {
            $iltHelper = Mage::helper('ilt');
            $iltHelper->storeIltData($superOrder, $this->postData['ilt']);
        }
        $payment = $convert->paymentToOrderPayment($quote->getPayment());

        if (isset($this->postData['adyen_payment_id']) && $this->postData['adyen_payment_id']) {
            $order->setIncrementId($this->postData['adyen_payment_id']);
        }

        $order->setPayment($payment);
        $order->save();

        $this->postData['dynalean_order_id'] = trim($superOrder->getOrderNumber());
        $this->postData['adyen_payment_id'] = trim($order->getIncrementId());

        Mage::unregister('isSecureArea');
        Mage::register('isSecureArea', true);
        $order->delete();
        $quote->setIsActive(false)->save();
        $quote->delete();
        $superOrder->delete();
        Mage::unregister('isSecureArea');
        Mage::register('isSecureArea', false);

        $this->_getCheckoutSession()->resetCheckout();
        $this->_getCheckoutSession()->clear();
        $this->_getCheckoutSession()->clearHelperData();

        try {
            $buildOrder = new Vznl_RestApi_Orders_Build(
                Mage::app()->getRequest(),
                $this->postData
            );
            $buildOrder->process();
        } catch (Exception $e) {
            $this->addError('orders.build', $e->getMessage());

            $this->postData['errors'] = array_merge(
                $this->errors,
                $this->postData['errors']
            );
        }

        return $this->postData;
    }

    /**
     * @param int| null $quoteId
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote($quoteId = null)
    {
        if (!is_null($quoteId)) {
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            if ($quote->getId()) {
                $this->_getCheckoutSession()->setLoadInactive(true);
                $this->_getCheckoutSession()->setQuoteId($quote->getId());
            }
        }
        return $this->_getCheckoutSession()->getQuote();
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @param $errors
     * @return null
     */
    protected function checkFFRequest(Mage_Sales_Model_Quote $quote, &$errors)
    {
        if (Mage::app()->getStore()->getCode() != Vznl_Agent_Model_Website::WEBSITE_FRIENDS_AND_FAMILY) {
            // Do nothing if the store is not Friends and Family
            return null;
        }

        $packages = Mage::getModel('package/package')->getPackages(null, $quote->getId());

        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            if (!$package->getCoupon()) {
                $errors[] = array(
                    "item_key" => "ff_code",
                    "message" => "You must provide a valid Friends and Family code."
                );
                return null;
            }

            /**
             * @var Dyna_FFHandler_Model_Request $requestModel
             */
            $requestModel = Mage::getModel("ffhandler/request");
            $requestModel->loadByCode($package->getCoupon());

            if (
                !$requestModel->getId() // Code does not exist
                || !$requestModel->canBeUsedByQuote($quote, $package->getId()) // Is allowed to be used by customer
            ) {
                $errors[] = array(
                    "item_key" => "ff_code",
                    "message" => "Invalid Friends and Family code provided"
                );
                return null;
            }
        }
    }

    /**
     * Check if the checkout flow is business
     *
     * @return bool
     */
    protected function checkoutIsBusiness()
    {
        if ($this->postData['is_business']) {
            return true;
        }
        if ($this->postData['chosen_order_type_is_business']) {
            return true;
        }
        return false;
    }

    /**
     * @return void
     */
    protected function setIltData()
    {
        if ($this->getQuote()->isIltRequired() || isset($this->postData['ilt'])) {
            $data = $this->postData['ilt'] ?: [];
            if (!isset($data['birth_lastname']) || $data['birth_lastname'] === '') {
                $this->postData['ilt']['birth_lastname'] = $this->checkoutIsBusiness()
                    ? $this->postData['business_details']['contracting_party']['last_name']
                    : $this->postData['contracting_party']['last_name'];
            }
            if (!isset($data['birth_middlename']) || $data['birth_middlename'] === '') {
                $this->postData['ilt']['birth_middlename'] = $this->checkoutIsBusiness()
                    ? $this->postData['business_details']['contracting_party']['last_name_prefix']
                    : $this->postData['contracting_party']['last_name_prefix'];
            }
            /** @var $helper Vznl_Ilt_Helper_Data */
            $helper = Mage::helper('ilt');
            $response = $helper->validateData($data);
            foreach ($response as $key => $value) {
                $this->addError($this->getMapping($key), $value);
            }
        }
    }

    /**
     * @param string $orderNumber
     * @return array
     */
    protected function output($orderNumber)
    {
        $getClass = new Vznl_RestApi_Orders_GetHawaii(array('order_number' => $orderNumber));
        return $getClass->process();
    }
}
