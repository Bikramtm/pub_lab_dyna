<?php

class Vznl_RestApi_Orders_Modify extends Vznl_RestApi_Orders_Abstract
{
    /**
     * @var Vznl_Checkout_Model_Sales_Order
     */
    protected $order;

    /**
     * @var int|string
     */
    protected $orderNumber;
    
    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct($request)
    {
        $routeParts = explode('/', $request->getQuery('route'));
        $this->request = $request;
        $this->orderNumber = $routeParts[2];
    }
    
    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        $response = array();

        if (!is_null($this->getOrder()->getId())) {
            // Update shipping address
            $this->setShippingAddress();
            
            if (count($this->getErrors()) === 0) {
                $response['success'] = true;
            } else {
                foreach ($this->getErrors() as $key => $error) {
                    $response['errors'] = $this->getErrors();
                }
            }
        } else {
            $response['errors'][] = array('item_key'=>'request', 'message'=>'Order not found');
        }

        return $response;
    }
    
    /**
     * @return Vznl_Checkout_Model_Sales_Order
     */
    public function getOrder()
    {
        if (is_null($this->order)) {
            $superOrder = Mage::getModel('superorder/superorder')->load((string) $this->orderNumber, 'order_number');
            $this->order = Mage::getModel('sales/order')->load($superOrder->getId(), 'superorder_id');
        }

        return $this->order;
    }
    
    /**
     * Set the shipping address.
     */
    protected function setShippingAddress()
    {
        $orderShippingAddress = $this->getOrder()->getShippingAddress()->getId();
        $orderShipping = Mage::getModel('sales/order_address')->load($orderShippingAddress);
        
        $data = [
            'street' => $orderShipping->getStreetFull(),
            'city' => $orderShipping->getCity(),
            'postcode' => $orderShipping->getPostcode(),
            'country_id' => $orderShipping->getCountryId()
        ];
        
        if (!is_null($this->getRequest()->getPost('delivery_dealer_code'))) {
            $dealer = Mage::getModel('agent/dealer')->load($this->getRequest()
                ->getPost('delivery_dealer_code'), 'vf_dealer_code');

            if (!$dealer->getId()) {
                $this->addError('delivery.dealer_code', 'Dealer not found');
            }

            $data = [
                'street' => $dealer->getStreet() . ' ' . $dealer->getHouseNr(),
                'city' => $dealer->getCty(),
                'postcode' => $dealer->getPostcode(),
                'country_id' => 'NL' // Dealer in NL
            ];
            
            $this->getOrder()->setDealerId($dealer->getId());
            $this->getOrder()->getPickupAtStore(true);
        } else {
            $this->getOrder()->setDealerId(null);
            $this->getOrder()->getPickupAtStore(false);
            $data = $this->setIfExists($data, 'street', $this->getRequest()->getPost('delivery_street'));
            $data = $this->setIfExists($data, 'city', $this->getRequest()->getPost('delivery_city'));
            $data = $this->setIfExists($data, 'postcode', $this->getRequest()->getPost('delivery_postcode'));
            $data = $this->setIfExists($data, 'country_id', $this->getRequest()->getPost('delivery_country'));
        }
        $orderShipping->addData($data);
        
        if (count($this->getErrors() === 0)) {
            try  {
                $this->getOrder()->save();
                $orderShipping->implodeStreetAddress()->save();
            } catch (Exception $e) {
                $this->addError('order.modify', $e->getMessage());
            }
        }
    }

    /**
     * @param array $data
     * @param string $key
     * @param mixed $value
     * @return array
     */
    protected function setIfExists($data, $key, $value)
    {
        if (!is_null($value)) {
            $data[$key] = $value;
        }

        return $data;
    }

    /**
     * @return Mage_Core_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->request;
    }
}