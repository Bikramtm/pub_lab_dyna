<?php

class Vznl_RestApi_Orders_Submit extends Vznl_RestApi_Orders_Abstract
{
    /** 
     * @var Vznl_Superorder_Model_Superorder
     */
    protected $superOrder;

    /** 
     * @var Mage_Sales_Model_Resource_Order_Collection
     */
    protected $orders;

    /**
     * @var Vznl_Customer_Model_Customer
     */
    protected $customer;

    /**
     * @param array|Mage_Core_Controller_Request_Http $request
     */
    public function __construct($request)
    {
        Mage::app()->getTranslator()->init(Mage_Core_Model_App_Area::AREA_FRONTEND);

        if (is_array($request)){
            $orderNumber = $request['order_number'];
        } else {
            $routeParts = explode('/', $request->getQuery('route'));
            $orderNumber = $routeParts[2];
        }

        $this->loadSuperOrder($orderNumber);

        // Check that the superorder has valid orders
        $this->orders = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('superorder_id', $this->superOrder->getId())
            ->addFieldToFilter('edited', array('neq' => 1));

        if (count($this->orders) < 1) {
            throw new Exception('Order not found');
        }

        $this->customer = Mage::getModel('customer/customer')->load($this->superOrder->getCustomerId());

        if (!$this->customer->getId()) {
            throw new Exception('Customer not found');
        }

    }

    /**
     * Process the HTTP request.
     * 
     * @return array
     * @todo Dependent on Orders/GetHawaii, test later
     */
    public function process()
    {
        /** @var Vznl_Service_Helper_Data $serviceHelper */
        $serviceHelper = Mage::helper('vznl_service');
        /** @var Vznl_Service_Model_Client_EbsOrchestrationClient $client */
        $client = $serviceHelper->getClient('ebs_orchestration');

        try {
            $this->initAgent($this->superOrder->getAgentId(), $this->superOrder->getDealerId());

            // Call esb
            $response = $client->processCustomerOrder($this->superOrder, null, null, null, null, null, null, null, null, $this->getAmountToPay()); //call esb

            if (!empty($response)) {
                //save order confirmation email in queue
                $payload = Mage::helper('communication/email')->getOrderConfirmationPayload($this->superOrder);
                Mage::getSingleton('communication/pool')->add($payload);

                /** @var Vznl_Communication_Helper_Sms $smsHelper */
                $smsHelper = Mage::helper('communication/sms');
                $smsHelper->createOrderConfirmationSms($this->superOrder);

                Mage::getSingleton('customer/session')->setCurrentSuperOrderId($this->superOrder->getId());
            } else {
                $this->addError('request', 'Error while processing order');
            }
        } catch (Exception $e) {
            $this->addError('request', $e->getMessage());
        }
        return $this->output($this->superOrder->getOrderNumber());
    }

    public function getOrder()
    {
        return $this->order;
    }

    protected function getAmountToPay()
    {
        $amountToPay = array();

        foreach ($this->superOrder->getOrders(true)->getItems() as $order) {
            if ($order->getPayment()->getMethod() !== 'adyen_hpp') {
                // Set total amount
                foreach ($order->getDeliveryOrderPackages() as $package) {
                    $totals = $package->getPackageTotals($order->getAllItems());
                    $amountToPay[$package->getPackageId()] += $totals['total'];
                }
            }
        }
        return $amountToPay;
    }

    protected function output($orderNumber)
    {
        $getClass = new Vznl_RestApi_Orders_GetHawaii(['order_number' => $orderNumber]);
        $getClass->setErrors($this->getErrors());
        return $getClass->process();
    }
}
