<?php

/**
 * Dependencies:
 *
 * Vznl_Communication_Helper_Log
 * Dyna_Customer_Model_Customer_Customer
 * Mage_Sales_Model_Resource_Order_Collection
 * Dyna_Superorder_Model_Superorder
 * Omnius_Superorder_Model_Mysql4_ChangeHistory_Collection
 * Omnius_Superorder_Model_StatusHistory
 * Vznl_Checkout_Helper_Pdf
 */
class Vznl_RestApi_Orders_Get extends Vznl_RestApi_Orders_Abstract
{
    /**
     * @var Vznl_Superorder_Model_Superorder
     */
    protected $superOrder;

    /**
     * @var Mage_Sales_Model_Resource_Order_Collection
     */
    protected $orders;

    /**
     * @var Vznl_Customer_Model_Customer_Customer
     */
    protected $customer;

    /**
     * @var bool
     */
    protected $includeDocuments = false;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct($request)
    {
        $routeParts = explode('/', $request->getQuery('route'));

        $this->superOrder = $this->findSuperOrder($routeParts[3]);
        $this->orders = $this->findSuperOrderOrders();
        $this->customer = $this->findCustomer();

        // Set flag if include documents or not
        $this->includeDocuments = (bool) $request->getQuery('documents');

        // Emulate the superorder code in order to read the product data for that store instead of default which may be different
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $storeId = Mage::getModel('core/website')->load($this->superOrder->getWebsiteId())->getDefaultStore()->getId();
        $appEmulation->startEnvironmentEmulation($storeId);
    }

    /**
     * @param  int $orderNumber
     * @return Vznl_Superorder_Model_Superorder
     */
    private function findSuperOrder($orderNumber)
    {
        return Mage::getModel('superorder/superorder')
                ->load($orderNumber, 'order_number');
    }

    /**
     * Find the orders of the super order.
     * 
     * @return Mage_Sales_Model_Resource_Order_Collection
     * @throws Exception
     */
    private function findSuperOrderOrders()
    {
        $orders = Mage::getModel('sales/order')
            ->getCollection()
            ->addFieldToFilter('superorder_id', $this->superOrder->getId())
            ->addFieldToFilter('edited', ['neq' => 1]);

        if ($orders->count() < 1) {
            throw new Exception('Order not found');
        }

        return $orders;
    }

    /**
     * Find the customer of a super order.
     * 
     * @return Vznl_Customer_Model_Customer_Customer
     * @throws Exception
     */
    private function findCustomer()
    {
        $customer = Mage::getModel('customer/customer')
            ->load($this->superOrder->getCustomerId());

        if (!$customer->getId()) {
            throw new Exception('Customer not found');
        }
        
        return $customer;
    }

    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        $data = [
            'order' => $this->getOrderData(),
            'customer' => $this->getCustomer(),
            'documents' => $this->getDocumentsData(),
            'email' => $this->getEmailHistoryData(),
            'identity' => $this->getIdentityData(),
            'payment' => $this->getPaymentData(),
            'delivery' => $this->getDeliveryData(),
        ];

        if (!is_null($this->customer->getIsBusiness())) {
            $data['business_details'] = $this->getBusinessData();
        }
        
        return $data;
    }

    /**
     * Get the order data.
     * @return array
     */
    protected function getOrderData()
    {
        $result = array();
        $website = Mage::app()->getWebsite($this->superOrder->getCreatedWebsiteId());
        $agent = Mage::getModel('agent/agent')->load($this->superOrder->getAgentId());

        $result = [
            'order_number' => $this->superOrder->getOrderNumber(),
            'created' => $this->superOrder->getCreatedAt(),
            'error_code' => $this->superOrder->getErrorCode(),
            'error_message' => $this->superOrder->getErrorDetail(),
            'status' => $this->getSuperOrderStatusHistory(),
            'channel' => $website->getName(),
            'dealercode' => $agent->getDealer()->getVfDealerCode(),
            'agent' => []
        ];

        foreach ($this->getSaleOrderTree($this->superOrder->getId()) as $treeRow) {
            $result['versions'][] = $treeRow;
        }

        /**
         * @var Omnius_Superorder_Model_Mysql4_ChangeHistory_Collection
         */
        $changeHistoryCollection = Mage::getModel('superorder/changeHistory')
            ->getCollection()
            ->addFieldToFilter('superorder_id', array('eq' => $this->superOrder->getId()))
            ->setOrder('entity_id', 'desc');
        
        foreach ($changeHistoryCollection as $history) {
            $website = Mage::app()->getWebsite($history->getWebsiteId());
            $agent = Mage::getModel('agent/agent')->load($history->getAgentId());
            if ($agent->getId()) {
                $agentData['username'] = $agent->getUsername();
                $agentData['firstname'] = $agent->getFirstName();
                $agentData['lastname'] = $agent->getLastName();
                $agentData['email'] = $agent->getEmail();
                $agentData['employee_number'] = $agent->getEmployeeNumber();
                $agentData['date'] = $history->getCurrentTime();
                $agentData['channel'] = Mage::getModel('core/website')->load($history->getWebsiteId())->getCode();
                $agentData['dealercode'] = $history->getDealerCode();
                $result['agent'][] = $agentData;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getEmailHistoryData()
    {
        $result = [];

        $collection = Mage::getModel('communication/log')
            ->getCollection()
            ->addFieldToFilter('superorder_id', array('eq' => $this->superOrder->getId()))
            ->addFieldToFilteR('type', ['eq' => Vznl_Communication_Helper_Log::SUBTYPE_GARANT])
            ->setOrder('created_at', 'desc');

        foreach ($collection as $log) {
            $result[] = [
                'recipient' => $log->getRecipient(),
                'type' => $log->getType(),
                'created_at' => $log->getCreatedAt(),
            ];
        }
        return $result;
    }

    /**
     * @return array
     */
    protected function getCustomer()
    {
        $dob = new DateTime($this->customer->getDob());
        $billingAddress = $this->customer->getDefaultBillingAddress() ?: Mage::getModel('customer/address');

        return [
            'ban' => $this->customer->getBan(),
            'type' => $this->customer->getIsBusiness() ? 'business' : 'consumer',
            'birthdate' => $dob->format('Y-m-d'),
            'email' => $this->customer->getCorrespondanceEmail(),
            'firstname' => $this->customer->getFirstname(),
            'middlename' => $this->customer->getMiddlename(),
            'lastname' => $this->customer->getLastname(),
            'phone' => $billingAddress->getTelephone() ? explode(';', $billingAddress->getTelephone()) : array(),
            'invoice_address' => [
                'city' => $billingAddress->getCity(),
                'country' => $billingAddress->getCountry(),
                'street' => implode(' ', $billingAddress->getStreet()),
                'postalcode' => $billingAddress->getPostcode(),
            ],
            'origin' => $this->customer->getCustomerLabel(),
        ];
    }

    /**
     * @return array
     */
    protected function getDocumentsData()
    {
        $result = [];
        
        if ($this->includeDocuments) {
            $pdfHelper = Mage::helper('vznl_checkout/pdf');
            // Contract
            foreach ($this->orders as $deliveryOrder) {
                if ($deliveryOrder->hasMobilePackage()) {
                    $data = [
                        'filename' => 'contract_' . $deliveryOrder->getIncrementId() . '.pdf',
                        'type' => 'Contract',
                        'binary' => Mage::helper('vznl_checkout/sales')->generateContract($deliveryOrder, true, true)
                    ];

                    $documents[] = $data;
                }
            }
            // Offer
            foreach ($this->orders as $deliveryOrder) {
                $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($deliveryOrder->getQuoteId());
                if (!is_null($quote) && $quote->getId() && $quote->getIsOffer()) {
                    $data = [
                        'filename' => 'offer_' . $quote->getId() . '_' . $quote->getCreatedAt() . '.pdf',
                        'type' => 'Offer',
                        ''
                    ];

                    $data = [];
                    $data['filename'] = 'offer_' . $quote->getId() . '_' . $quote->getCreatedAt() . '.pdf';
                    $data['type'] = 'Offer';
                    $data['binary'] = file_get_contents($pdfHelper->saveOffer($quote));
                    $documents[] = $data;
                }
            }
            // Garant
            $attachmentPaths = [];
            foreach ($this->orders as $deliveryOrder) {
                foreach ($deliveryOrder->getPackages() as $package){
                    list($packagesWithGarant, $garantProductOrders) = $this->superOrder->getGarantProductOrders();
                    $attachmentPaths[] = Mage::helper('vznl_esb')->getGarantAttachmentPath(
                        $packagesWithGarant,
                        $garantProductOrders,
                        $this->superOrder,
                        $package->getPackageId()
                    );
                }
            }
            if (count($attachmentPaths) > 0) {
                foreach ($attachmentPaths as $path) {
                    $filename = end(explode('/', $path[0]));
                    if (!empty($filename)) {
                        $data['filename'] = $filename;
                        $data['type'] = 'Warranty';
                        $data['binary'] = (isset($path[0]) ? file_get_contents($path[0]) : '');
                        $documents[] = $data;
                    }
                }
            }

            // Push all documents
            if (count($documents) > 0) {
                foreach ($documents as $data) {
                    $document = [];
                    $document['filename'] = $data['filename'];
                    $document['type'] = $data['type'];
                    $document['file'] = base64_encode($data['binary']);
                    $result[] = $document;
                }
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getIdentityData()
    {
        $date = new DateTime($this->customer->getValidUntil());

        return [
            'expiry_date' => $date->format('Y-m-d'),
            'number' => $this->customer->getIdNumber(),
            'type' => $this->customer->getIdType(),
            'nationality' => $this->customer->getIssuingCountry()
        ];
    }

    /**
     * @return array
     */
    public function getPaymentData()
    {
        return [
            'name' => $this->customer->getAccountHolder(),
            'number' => $this->customer->getAccountNo()
        ];
    }

    /**
     * @return array
     */
    public function getBusinessData()
    {
        $street = sprintf(
            '%s %s%s',
            $this->customer->getCompanyStreet(),
            $this->customer->getCompanyHouseNr(),
            $this->customer->getCompanyHouseNrAddition()
        );

        return [
            'name' => $this->customer->getCompanyName(),
            'address' => [
                'city' => $this->customer->getCompanyCity(),
                'country' => $this->customer->getCompanyCountryId(),
                'street' => trim($street),
                'postalcode' => $this->customer->getCompanyPostcode()
            ],
            'coc_number' => $this->customer->getCompanyCoc(),
            'establish_date' => $this->customer->getCompanyDate(),
            'expire_date' => $this->customer->getContractantValidUntil(),
            'legal_form' => $this->customer->getCompanyLegalForm(),
            'vat_number' => $this->customer->getCompanyVatId()
        ];
    }

    /**
     * @return array
     */
    protected function getDeliveryData()
    {
        $result = array();

        $deliveryOrderCollection = $this->superOrder->getOrders(true);

        foreach ($deliveryOrderCollection as $deliveryOrder) {
            $class = new Vznl_RestApi_Orders_GetDeliveryOrder(array('order_id' => $deliveryOrder->getId()));
            $deliveryData = $class->process();
            $result[] = $deliveryData;
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getSuperOrderStatusHistory()
    {
        return $this->getHistory(
            'superorder_id', 
            $this->superOrder, 
            Vznl_Superorder_Model_StatusHistory::ORDER_STATUS
        );
    }
}
