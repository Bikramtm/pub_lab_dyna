<?php

class Vznl_RestApi_Orders_Cancel
{
    const SO_REDIS_LOCK = 'superorder_redis_lock';

    /**
     * @var int
     */
    protected $orderNumber;

    /**
     * @var Vznl_Superorder_Model_Superorder|null
     */
    private $superOrder = null;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @throws Exception
     */
    public function __construct($request)
    {
        $routeParts = explode('/', $request->getQuery('route'));
        $orderNumber = $routeParts[2];

        /** @var Vznl_Superorder_Model_Superorder superorder */
        $this->superOrder = Mage::getModel('superorder/superorder')->load((string) $orderNumber, 'order_number');

        // Check that the super order exists
        if (!$this->superOrder || !$this->superOrder->getId()) {
            throw new Exception('Order not found');
        }
        
        // Check that the superorder has valid orders
        $this->orderCollection = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('superorder_id', $this->superOrder->getId())
            ->addFieldToFilter('edited', array('neq' => 1));

        if (count($this->orderCollection) < 1) {
            throw new Exception('Collection not found');
        }
    }
    
    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        $response = array();

        if ($this->canCancel()) {
            try {
                // Login the agent that cancels the order
                $agent = Mage::getModel('agent/agent')->load($this->superOrder->getAgentId());
                Mage::getSingleton('customer/session')->setAgent($agent);

                /** @var Mage_Core_Model_Cache $cacheModel */
                $cacheModel = Mage::getModel('core/cache');
                $key = 'cancel_superorder_lock_' . $this->superOrder->getId();

                if (unserialize($cacheModel->load($key))) {
                    $response['success'] = false;
                    $response['code'] = 1;
                    $response['errors'][] = array('item_key'=>'order_id', 'message' => 'Order cannot be cancelled, cancel in progress');
                } else {
                    $cacheModel->save(serialize(true), $key, [self::SO_REDIS_LOCK], 1800);
                    $this->superOrder->cancel();
                    $response['success'] = true;
                }
            } catch (Zend_Soap_Client_Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $response['success'] = false;
                $response['code'] = 2;
                $response['errors'][] = array('item_key' => 'request', 'message' => $e->getMesage());
                return;
            } catch (SoapFault $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $response['success'] = false;
                $response['code'] = 3;
                $response['errors'][] = array('item_key' => 'request', 'message' => 'Error while cancelling order, please review logs');
            }
        } else {
            $response['success'] = false;
            $response['code'] = 4;
            $response['errors'][] = array('item_key'=>'order_id', 'message' => 'Order cannot be cancelled');
        }
        return $response;
    }
    
    /**
     * Check if an order can be cancelled.
     * 
     * @return boolean
     */
    private function canCancel()
    {
        $statusAllowed = [
            Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_BACK_ORDER,
            Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PORTING_APPROVED,
            Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED,
            Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT,
            Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PORTING_PENDING,
            Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PORTING_REJECTED,
        ];

        foreach ($this->superOrder->getPackages()->getItems() as $package){
            if (!in_array($package->getStatus(), $statusAllowed)) {
                return false;
            }
        }
        return true;
    }
}