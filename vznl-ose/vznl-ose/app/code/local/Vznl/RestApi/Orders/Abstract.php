<?php

/**
 * Stuff that is copied from the checkout process. This needs to be refactored
 * to a more generic way of working since we have 2 codebases now that do the same thing
 */
abstract class Vznl_RestApi_Orders_Abstract extends Vznl_RestApi_Abstract
{
    const RETRY_TIME = 'vodafone_service/webshop/order_get_retry_time';
    const RETRY_ITERATIONS = 'vodafone_service/webshop/order_get_iterations';

    public function getOrderTree($id, &$tree = array(), $passedOrderIds = array())
    {
        $passedOrderIds[] = $id;
        $order = Mage::getModel('sales/order')->load($id);
        $tree[strtotime($order->getUpdatedAt())] = array(
            'id' => $order->getId(),
            'date' => $order->getUpdatedAt()
        );

        // Parents
        $orderParentCollection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('parent_id', $order->getId());
        foreach ($orderParentCollection as $orderParent) {
            if ($orderParent->getId() && !in_array($orderParent->getId(), $passedOrderIds)) {
                return $this->getOrderTree($orderParent->getId(), $tree, $passedOrderIds);
            }
        }
        // Children
        $childOrder = Mage::getModel('sales/order')->load($order->getParentId());
        if (!is_null($childOrder->getId()) && !in_array($childOrder->getParentId(), $passedOrderIds)) {
            return $this->getOrderTree($childOrder->getId(), $tree, $passedOrderIds);
        }
        arsort($tree);
        return $tree;
    }

    public function getSaleOrderTree($id, &$tree = array(), $passedOrderIds = array())
    {
        $passedOrderIds[] = $id;
        $superOrder = Mage::getModel('superorder/superorder')->load($id);
        $tree[strtotime($superOrder->getCreatedAt())] = array(
            'id' => $superOrder->getOrderNumber(),
            'date' => $superOrder->getCreatedAt()
        );

        // Parents
        $superOrderParentCollection = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('parent_id', $superOrder->getId());
        foreach ($superOrderParentCollection as $superOrderParent) {
            if ($superOrderParent->getId() && !in_array($superOrderParent->getParentId(), $passedOrderIds)) {
                return $this->getSaleOrderTree($superOrderParent->getParentId(), $tree, $passedOrderIds);
            }
        }
        // Children
        $childOrder = Mage::getModel('superorder/superorder')->load($superOrder->getParentId());
        if (!is_null($childOrder->getId()) && !in_array($childOrder->getParentId(), $passedOrderIds)) {
            return $this->getSaleOrderTree($childOrder->getId(), $tree, $passedOrderIds);
        }
        return $tree;
    }

    public function loadSuperOrder($orderNumber)
    {
        $iterations = 0;
        while (true) {
            try {
                $this->superOrder = Mage::getModel('superorder/superorder')->load((string) $orderNumber, 'order_number');
            } catch (Exception $e) {
                // just ignore the exception, nothing to do
                $this->superOrder = Mage::getModel('superorder/superorder');
            }
            // Check that the superorder exists
            if (!$this->superOrder || !$this->superOrder->getId()) {
                $quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('superorder_number', $orderNumber)->setPageSize(1, 1)->getLastItem();
                $exception = $quote->getHawaiiOrderException();
                if (!empty($exception)) {
                    throw new Exception($exception);
                }
            } else {
                return $this->superOrder;
            }

            if ($iterations > $this->getRetryIterations()) {
                throw new Exception('Order not found');
            }
            $iterations++;
            usleep($this->getRetryTime() * 1000000);
        }
    }

    public function renderPrices($basePrice, $product)
    {
        $taxHelper = Mage::helper('tax');
        $store = Mage::app()->getStore();

        $price = $taxHelper->getPrice($product, $basePrice, false);
        $priceInludingTax = $taxHelper->getPrice($product, $basePrice, true);
        return array('tax' => (float)($priceInludingTax - $price), 'subtotal' => (float)$price, 'total' => (float)$priceInludingTax);
    }

    /**
     * Get the current checkout session
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    protected function getCheckout()
    {
        return Mage::getSingleton('checkout/type_multishipping');
    }

    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    protected function getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    protected function getSaleType()
    {
        foreach ($this->getQuote()->getPackages() as $package) {
            return $package['sale_type'];
        }
        throw new Exception('No packages found');
    }

    public function initAgent($agentId, $dealerId)
    {
        $dealer = Mage::getModel('agent/dealer')->load($dealerId);
        $agent = Mage::getModel('agent/agent')->load($agentId);
        $agent->setDealer($dealer);
        $this->_getCustomerSession()->setAgent($agent);
    }

    /**
     * @return mixed
     */
    public function getRetryIterations()
    {
        return Mage::getStoreConfig(self::RETRY_ITERATIONS);
    }

    /**
     * @return mixed
     */
    public function getRetryTime()
    {
        return Mage::getStoreConfig(self::RETRY_TIME);
    }

    /**
     * Create the actual order
     * @return Vznl_Superorder_Model_Superorder
     */
    public function createOrder()
    {
        $checkout = $this->getCheckout();
        $checkout->setCollectRatesFlag(true);

        $quote = $this->getQuote();
        if ($quote->getId()) {
            $quote->setIsMultiShipping(true)->save();
        }

        $session = $this->_getCustomerSession();
        if ($quote->getCustomer()->getId() && !$quote->getCustomer()->getIsProspect()) {
            $customer = Mage::getModel('customer/customer')->load($quote->getCustomer()->getId());
            $session->setCustomer($customer);
            $session->unsBtwState();
        } else {
            $customer = $this->_addCustomer($quote);

            //if customer exists, load the entity to retrieve the campaigns, ctn records and other data
            $session->setCustomer($customer);
            $session->unsBtwState();

            // Retrieve the last quote the customer had and set it to inactive
            $customerQuote = Mage::getModel('sales/quote')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->loadByCustomer(Mage::getSingleton('customer/session')->getCustomerId());
            if ($customerQuote->getId()) {
                $customerQuote->setIsActive(0);
                $customerQuote->save();
            }

            // Set the current quote as active for the logged in customer
            $quote->setCustomer($customer);
            $quote->save();
        }

        $shipToInfo = $this->_buildDeliveryAddresses($customer->getId());
        $checkout->setShippingItemsInformation($shipToInfo);

        // TODO Since we need to hard-code at this moment, we manually set them
        $addresses = $quote->getAllShippingAddresses();
        foreach ($addresses as $address) {
            $address->setShippingMethod('flatrate_flatrate');
        }

        $this->_setDefaultPayment();

        /*********************************************************/
        try {
            if (!$quote->getBillingAddress()->getFirstname()) {
                $quote->setBillingAddress(Mage::getModel('sales/quote_address')->setData(
                    $customer->getDefaultBillingAddress() ? $customer->getDefaultBillingAddress()->getData() : [])
                );
            }
            $checkout->createOrders();
        } catch (Exception $e) {
            // Here is the thrown exception
            Mage::getSingleton('core/logger')->logException($e);
            $this->addError('order.create', $e->getMessage());
            return;
        }

        $orderPackages = array();
        $packagesTypes = $this->_checkIfDeliveryInShop();
        // TODO Refactor this when we have the call from ESB to set the telesale order to be saved with a certain status
        $orderIds = Mage::getSingleton('core/session')->getOrderIds();
        foreach ($orderIds as $orderIncrementId) {
            $order = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderIncrementId);

            //create new package entities to set their status
            foreach ($order->getAllItems() as $item) {
                $orderPackages[] = $item->getPackageId();
            }

            // Set the order delivery type
            if (isset($item)) {
                $order->getShippingAddress()->setDeliveryType($packagesTypes[$item->getPackageId()]['type']);
                if ($packagesTypes[$item->getPackageId()]['type'] == 'store') {
                    $order->getShippingAddress()->setDeliveryStoreId($packagesTypes[$item->getPackageId()]['store_id']);
                }
                $order->getShippingAddress()->save();
            }

            if (Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE) {
                foreach ($order->getAllItems() as $item) {
                    if ($packagesTypes[$item->getPackageId()]['type'] == 'store') {
                        $order->setEsbOrderStatus(Vznl_Checkout_Model_Sales_Order::ESB_VALIDATION);
                        $order->save();
                        break;
                    }
                }
            }
        }

        /**
         * If reservation failed, we need to keep reference to the superorder
         * no need to crete another one every time
         */
        if ($this->_getCustomerSession()->getFailedSuperOrderId()) {
            /** @var Vznl_Superorder_Model_Superorder $superOrder */
            $superOrder = Mage::getModel('superorder/superorder')->load($this->_getCustomerSession()->getFailedSuperOrderId());
            Mage::register('isSecureArea', true);
            foreach ($superOrder->getOrders() as $order) {
                /** @var Mage_Sales_Model_Order $order */
                $order->delete();
            }
            Mage::unregister('isSecureArea');
        }

        // Create a new superOrder
        $superOrder = Mage::getModel('superorder/superorder')->createNewSuperorder();

        $quotePackages = Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId());

        // If the superorder was successfully created, move the packages to the order
        /** @var Vznl_Package_Model_Package $quotePackage */
        foreach ($quotePackages as $quotePackage) {
            $quotePackage->setOrderId($superOrder->getId())->setQuoteId(null);
            $quotePackage->save();
        }
        unset($quotePackages);

        // Assign all the orders to the super order and custom increment id
        foreach ($orderIds as $orderIncrementId) {
            /** @var Vznl_Checkout_Model_Sales_Order $order */
            $order = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderIncrementId);
            $order->setSuperorderId($superOrder->getId());
            $order->setEdited(0);
            $order->save();
            if ($order->getPayment()->getMethod() === 'adyen_hpp') {
                $con = Mage::getSingleton('core/resource')->getConnection('core_write');
                $con->query(
                    sprintf(
                        'UPDATE superorder SET order_status="%s" WHERE entity_id="%s";',
                        Vznl_Superorder_Model_Superorder::SO_ADYEN_PAYMENT,
                        $superOrder->getId()
                    )
                );
                $con = Mage::getSingleton('core/resource')->getConnection('core_write');
                $con->query(
                    sprintf(
                        'INSERT INTO status_history (superorder_id, status, type, created_at) values(%d, "%s", "%s", "%s");',
                        $superOrder->getId(),
                        Vznl_Superorder_Model_Superorder::SO_ADYEN_PAYMENT,
                        'order_status',
                        Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s')
                    )
                );
            }
        }

        $quotePackages = Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('order_id', $superOrder->getId());
        // If the orders were successfully assigned to the superorder, update status
        /** @var Vznl_Package_Model_Package $quotePackage */
        foreach ($quotePackages as $quotePackage) {

            $quotePackage->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_INITIAL)
                ->setCreditcheckStatus(Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL)
                ->setCreditcheckStatusUpdated(now());

            if ($quotePackage->isNumberPorting()) {
                $quotePackage->setPortingStatus(Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL);
                $quotePackage->setPortingStatusUpdated(now());
            }
            $quotePackage->save();
        }
        unset($quotePackages);
        return $superOrder;
    }

    /**
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param array $bslJSON
     * @return array
     */
    public function create(Vznl_Checkout_Model_Sales_Quote $quote, $bslJSON = null)
    {
        if (!$quote->getId()) {
            throw new Exception(Mage::helper("vznl_checkout")->__('Invalid quote'));
        }

        /*$quote->setCustomStatus(Vznl_Checkout_Model_Sales_Quote::VALIDATION);
        $quote->save();*/

        $this->_getCheckoutSession()->replaceQuote($quote);

        // Create the order and place the ESB Order id on the response
        $superOrder = $this->createOrder();

        if ($superOrder) {
            // Check if we need to connect an ILS addon
            if (!is_null($bslJSON['eligibleComponents'])) {
                $normalizer = Mage::getSingleton('dyna_service/normalizer');
                $bslJSON = $normalizer->normalizeKeys(
                    $normalizer->objectToArray($bslJSON)
                );

                $ctnCode = $bslJSON['eligible_components']['component_identifier']['assigned_parent_id'];;
                $assignedProductId = $bslJSON['eligible_components']['component_identifier']['assigned_parent_id'];
                $billingOfferCode = $bslJSON['eligible_components']['component_identifier']['pricing_elements'][0]['catalog_pricing_code'];
                $componentCode = $bslJSON['eligible_components']['component_identifier']['catalog_code'];
                $assignedComponentId = $bslJSON['eligible_components']['component_identifier']['assigned_id'];
                $parentAssignedComponentId = $bslJSON['eligible_components']['component_identifier']['assigned_parent_id'];

                $eligibleAddonsArray = Mage::helper('vznl_configurator/inlife')->processAddonData($bslJSON);
                $this->setCurrentSessionAddons($eligibleAddonsArray, 'eligible', $ctnCode);

                $response = $this->callAddAddon(
                    $superOrder,
                    $assignedProductId,
                    $billingOfferCode,
                    $componentCode,
                    $assignedComponentId,
                    $parentAssignedComponentId,
                    $simulateOnly = true
                );
                if (!$response) {
                    throw new Exception('Error while adding addon');
                }
            }

            $result['orders'] = $superOrder->getOrderNumber();

            // Append the address to the response in order to have them in the credit check step
            $result['addresses'] = $this->_buildCreditCheckAddresses($quote);

            $quote->setCurrentStep('credit-check');
            $quote->save();
        }
        return $result;
    }

    private function _buildCreditCheckAddresses($quote)
    {
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());

        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['deliver']['address'];
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['pakket'][$packageId]['address'];
            }
        }

        return $addressArray;
    }

    protected function _addCustomer(Vznl_Checkout_Model_Sales_Quote $quote)
    {
        $billing = $quote->getBillingAddress();
        $shipping = $quote->isVirtual() ? null : $quote->getShippingAddress();

        //$customer = Mage::getModel('customer/customer');
        // Check if we just have a database mismatch
        $customer = Mage::getModel('customer/customer');

        /* @var $customer Mage_Customer_Model_Customer */
        $customerBilling = $billing->exportCustomerAddress();
        $customer->addAddress($customerBilling);
        $billing->setCustomerAddress($customerBilling);
        $customerBilling->setIsDefaultBilling(true);

        if ($shipping && !$shipping->getSameAsBilling()) {
            $customerShipping = $shipping->exportCustomerAddress();
            $customer->addAddress($customerShipping);
            $shipping->setCustomerAddress($customerShipping);
            $customerShipping->setIsDefaultShipping(true);
        } else {
            $customerBilling->setIsDefaultShipping(true);
        }

        Mage::helper('core')->copyFieldset('checkout_onepage_quote', 'to_customer', $quote, $customer);
        $customer->setPassword(md5(mt_rand()));
        $customer->setPasswordHash($customer->hashPassword($customer->getPassword()));
        $quote->setCustomer($customer)
            ->setCustomerId($customer->getId());
        $customer->save();


        return $customer;
    }

    protected function _buildDeliveryAddresses($customerId)
    {
        // Retrieve the previously saved data for addresses
        $quote = $this->getQuote();
        $data = Mage::helper('core')->jsonDecode($this->getQuote()->getShippingData());
        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            foreach ($this->getQuote()->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['deliver']['address'];
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($this->getQuote()->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['pakket'][$packageId]['address'];
            }
        }
        $addresses = array();
        foreach ($addressArray as $packageId => $address) {
            if (is_numeric($address)) {
                $address = Mage::getModel('customer/address')->load($address);
                if (!$address) {
                    throw new Exception(Mage::helper("vznl_checkout")->__('Invalid delivery address id'));
                }
                $addresses[$packageId] = $address->getId();
            } elseif (is_array($address)) {
                $address['postcode'] = isset($address['zipcode']) ? $address['zipcode'] : $address['postcode'];
                $addresses[$packageId] = $this->_addCustomerShippingAddress($customerId, $address);
            }
        }

        $result = array();
        $items = $quote->getAllItems();
        foreach ($items as $item) {
            $result[] = array(
                $item->getId() => array(
                    'address' => $addresses[$item->getPackageId()],
                    'qty' => $item->getQty()
                )
            );
        }
        return $result;
    }

    protected function _addCustomerShippingAddress($customerId, $addressData)
    {
        $shipAddress = $this->getQuote()->getShippingAddress();
        $addressData['firstname'] = $shipAddress->getFirstname();
        $addressData['company'] = $shipAddress->getCompany();

        // Delivery can only be in Netherlands!
        $addressData['country_id'] = Vznl_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;
        $addressData['fax'] = $shipAddress->getFax();
        $addressData['lastname'] = $shipAddress->getLastname();
        $addressData['middlename'] = $shipAddress->getMiddlename();
        $addressData['suffix'] = $shipAddress->getSuffix();
        $addressData['telephone'] = $shipAddress->getTelephone();

        // Check if the address was already added, in that case just return the id.
        $addressId = false;
        $customer = Mage::getModel('customer/customer')->load($customerId);

        foreach ($customer->getAddresses() as $tempAddress) {
            if ($addressData['street'] == array(
                    $tempAddress->getStreetName(),
                    $tempAddress->getHouseNo(),
                    $tempAddress->getHouseAdd(),
                ) &&
                $addressData['postcode'] == $tempAddress->getPostcode() &&
                $addressData['city'] == $tempAddress->getCity()
            ) {
                $addressId = $tempAddress->getId();
                break;
            }
        }

        if (!$addressId) {
            // address does not exist in the db , create it
            $address = Mage::getModel('customer/address');

            $addressForm = Mage::getModel('customer/form');
            $addressForm->setFormCode('customer_address_edit')
                ->setEntity($address);

            // Fix missing customer name for address validators
            if (empty($addressData['firstname']) && empty($addressData['lastname'])) {
                $addressData['firstname'] = $this->getQuote()->getCustomer()->getFirstname();
                $addressData['middlename'] = $this->getQuote()->getCustomer()->getMiddlename();
                $addressData['lastname'] = $this->getQuote()->getCustomer()->getLastname();
            }

            $addressErrors = $addressForm->validateData($addressData);
            if (true === $addressErrors) {
                $addressForm->compactData($addressData);
                $address->setCustomerId($customer->getId())
                    ->setIsDefaultBilling(false)
                    ->setIsDefaultShipping(false)
                    ->save();

                return $address->getId();
            } else {
                throw new Exception(Mage::helper('core')->jsonEncode($addressErrors));
            }
        } else {
            // just return the id
            return $addressId;
        }
    }

    protected function _setDefaultPayment()
    {
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($this->getQuote()->getShippingData());
        $paymentData = (isset($data['payment']) && $data['payment']) ? $data['payment'] : null;
        if (!$paymentData) {
            throw new Exception(Mage::helper("vznl_checkout")->__('No shipping method was set.'));
        }

        if (is_array($paymentData)) {
            $payment['method'] = reset($paymentData);
        } else {
            $payment['method'] = $paymentData;
        }
        $checkout = $this->getCheckout();
        $payment['checks'] = Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
            | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX;
        $checkout->setPaymentMethod($payment);
        $checkout->save();
    }

    protected function _checkIfDeliveryInShop()
    {
        $quote = $this->getQuote();
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());

        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId]['type'] = $data['deliver']['address']['address'];
                if ($addressArray[$packageId]['type'] == 'store') {
                    $addressArray[$packageId]['store_id'] = $data['deliver']['address']['store_id'];
                }
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId]['type'] = $data['pakket'][$packageId]['address']['address'];
                if ($addressArray[$packageId]['type'] == 'store') {
                    $addressArray[$packageId]['store_id'] = $data['pakket'][$packageId]['address']['store_id'];
                }
            }
        }

        return $addressArray;
    }

    /**
     * Execute the service call to add a new addon
     *
     * @param $billingOfferCode
     * @param $componentCode
     * @param $assignedComponentId
     * @param $parentAssignedComponentId
     * @param bool $simulateOnly
     * @param bool $returnEligibleAddons
     * @param array $componentSettings
     * @return mixed
     * @throws Exception
     */
    private function callAddAddon(
        $superOrder,
        $assignedProductId,
        $billingOfferCode,
        $componentCode,
        $assignedComponentId,
        $parentAssignedComponentId,
        $simulateOnly = true,
        $returnEligibleAddons = false,
        $isQuotationRequired = true,
        $componentSettings = array()
    )
    {
        // Validate input data
        if (!trim($billingOfferCode) || !trim($componentCode)) {
            throw new Exception('Invalid input data');
        }

        $rawAddon = $this->getCurrentSessionAddons('eligible', $billingOfferCode, $assignedProductId);

        $addons[] = array(
            'billing_offer_code' => $billingOfferCode,
            'component_code' => $componentCode,
            'assigned_component_id' => $assignedComponentId,
            'parent_assigned_component_id' => $parentAssignedComponentId,
            'component_settings' => $componentSettings,
            'context' => $rawAddon['context']
        );

        $isJob = true;
        $simulateOnly = false;
        $info = serialize(array(
            'dealer_code' => $this->_getCustomerSession()->getAgent()->getDealer()->getVfDealerCode(),
            'method' => 'addInlifeAddon',
            'arguments' => array($assignedProductId,
                $simulateOnly,
                $returnEligibleAddons,
                $isQuotationRequired,
                $addons,
                $isJob)
        ));
        $superOrder->setXmlToBeSent($info)
            ->setIsIls(true)
            ->save();
        return true;
    }

    /**
     * Get inlife service call client
     *
     * @return Vznl_Inlife_Model_Client_InlifeClient
     */
    private function getInlifeClient()
    {
        /** @var Vznl_Inlife_Helper_Data $h */
        $inlifeHelper = Mage::helper('vznl_inlife/data');
        return $inlifeHelper->getClient('inlife');
    }

    /**
     * Save the getAddons results to local cache
     *
     * @param array $addons
     * @param string $type
     */
    protected function setCurrentSessionAddons($addons, $type, $ctnCode)
    {
        $currentCustomer = $this->_getCustomerSession()->getCustomer();
        $key = sprintf('%s_%s_%s_%s', Mage::getSingleton('core/session')->getEncryptedSessionId(), $currentCustomer->getBan(), $ctnCode, 'inlife_' . $type . 'Addons');
        Mage::app()->getCache()->save(serialize($addons), $key, array(Vznl_Configurator_Model_Cache::CACHE_TAG));
    }

    /**
     * Retrieve addons from local cache
     *
     * @param string $type
     * @param null $code Addon code if only one specific addon is needed
     * @return mixed|null
     */
    public function getCurrentSessionAddons($type = 'removable', $code = null, $ctnCode = null)
    {
        $currentCustomer = $this->_getCustomerSession()->getCustomer();
        $key = sprintf('%s_%s_%s_%s', Mage::getSingleton('core/session')->getEncryptedSessionId(), $currentCustomer->getBan(), $ctnCode, 'inlife_' . $type . 'Addons');

        if ($eligibleAddons = Mage::app()->getCache()->load($key)) {
            if (!$code) {
                return unserialize($eligibleAddons);
            } else {
                $addons = unserialize($eligibleAddons);
                foreach ($addons as $addon) {
                    if ($addon['addon_options'][0]['catalog_pricing_code'] == $code) {
                        return $addon;
                    }
                }
            }
        }

        return null;
    }

    /**
     * @throws Exception
     */
    public function getBinary()
    {
        if (defined('PHP_BINARY') && strlen(constant('PHP_BINARY'))) {
            return constant('PHP_BINARY');
        } else {
            return $this->_getBinary('php');
        }
    }

    protected function _getBinary($exec)
    {
        if ($exec === 'curl') {
            return '/usr/bin/curl';
        } else {
            exec(sprintf('which %s', escapeshellarg($exec)), $output, $status);
            if (0 === $status) {
                return trim(join(PHP_EOL, $output));
            } else {
                return false;
            }
        }
    }

    /**
     * @param string $field
     * @param int $id
     * @param string $fieldType
     *
     * @return array
     */
    protected function getHistory($field, $entity, $fieldType)
    {
        /** @var Omnius_Superorder_Model_Mysql4_StatusHistory_Collection $historyItems */
        $historyItems = Mage::getModel('superorder/statusHistory')
            ->getCollection()
            ->addFieldToFilter($field, $entity->getId())
            ->addFieldToFilter('type', $fieldType)
            ->addOrder('created_at', 'DESC')
            ->addOrder('entity_id', 'DESC');

        $result = array();
        foreach ($historyItems as $item) {
            $data = array(
                'status' => $item->getStatus(),
                'date' => date('Y-m-d H:i:s', strtotime($item->getCreatedAt())),
                'message' => ($item->getStatus() === Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED ? $entity->getVfStatusDesc() : null),
            );
            if ($field === 'superorder_id' && $item->getAgentId() !== null) {
                $superOrder = Mage::getModel('superorder/superorder')->load($entity->getId());
                $data['agent'] = $superOrder->getCancelledBy();
            }
            $result[] = $data;
        }

        return $result;
    }
}
