<?php

class Vznl_RestApi_Orders_Overview extends Vznl_RestApi_Orders_Abstract
{
    const DEFAULT_DEALERCODE = '00803212';

    /**
     * @var Dyna_Customer_Model_Customer
     */
    private $customer;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct($request)
    {
        $this->request = $request;
        $routeParts = explode('/', $request->getQuery('route'));
        $this->customer = $this->findCustomer($routeParts[3]);
    }

    /**
     * @return Dyna_Customer_Model_Customer
     * @throws Exception
     */
    private function findCustomer($ban)
    {
        $customerExists = Mage::getModel('customer/customer')
            ->getCollection()
            ->addFieldToFilter('ban', $ban)
            ->setPageSize(1, 1)
            ->getLastItem();

        $customer = Mage::getModel('customer/customer')
            ->load($customerExists->getId());
        
        if (!$customer->getId()) {
            throw new Exception('Customer not found');
        }

        return $customer;
    }

    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        Mage::register('job_additional_information', array(
            'dealer_code' => self::DEFAULT_DEALERCODE,
        ));

        /**
         * @var Vznl_Inlife_Helper_Data
         */
        $inlifeHelper = Mage::helper('vznl_inlife/data');

        /** 
         * @var Vznl_Inlife_Model_Client_InlifeClient
         */
        $client = $inlifeHelper->getClient('inlife');
        
        $response = $client->searchInlifeOrder($this->customer->getBan());
        
        $result = array();
        $superOrderCollection = Mage::getModel('superorder/superorder')->getCollectionForCustomer($this->customer->getId());
        foreach ($superOrderCollection as $superOrder) {
            $total = 0;
            foreach ($superOrder->getOrders(true) as $order) {
                $orderTotals = $order->getTotals();
                $total += $orderTotals['total'];
            }

            $data = [];
            $data['order_number'] = $superOrder['order_number'];
            $data['order_status'] = $superOrder['order_status'];
            $data['unify_order_id'] = null;
            $data['amount'] = $total;
            $data['date'] = $this->getTimestampFromDate($superOrder['created_at']);
            $result[$data['date']] = $data;
        }

        if (is_array($response['orders_list'])) {
            foreach ($response['orders_list'] as $order) {
                $data = [];
                $data['order_number'] = null;
                $data['order_status'] = !empty($order['order_status']) ? $order['order_status'] : null;
                $data['unify_order_id'] = !empty($order['order_id']) ? $order['order_id'] : null;
                $data['amount'] = !empty($order['rc_amount_including_taxs']) ? $order['rc_amount_including_taxs'] : null;
                $data['date'] = !empty($order['created_date']) ? $order['created_date'] : null;
                $result[$data['date']] = $data;
            }
        }

        sort($result, SORT_NUMERIC);
        $indexedResult = array_values($result);

        return $indexedResult;
    }
}
