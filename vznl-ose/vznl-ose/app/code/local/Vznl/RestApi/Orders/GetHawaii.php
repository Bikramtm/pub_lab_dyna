<?php

class Vznl_RestApi_Orders_GetHawaii extends Vznl_RestApi_Orders_Abstract
{
    /**
     * @var Vznl_Superorder_Model_Superorder
     */
    protected $superOrder;

    /**
     * @var Mage_Sales_Model_Resource_Order_Collection
     */
    protected $orders;

    /**
     * @var Vznl_Customer_Model_Customer_Customer
     */
    protected $customer;

    /**
     * @var Vznl_Package_Model_Package[]
     */
    protected $packages;

    /**
     * @var Vznl_Checkout_Model_Sales_Quote|null
     */
    protected $quote;

    /**
     * @param array|Mage_Core_Controller_Request_Http $request
     * @throws Exception
     */
    public function __construct($request = null)
    {
        if (is_array($request)) {
            $orderNumber = $request['order_number'];
        } else {
            $routeParts = explode('/', $request->getQuery('route'));
            $orderNumber = $routeParts[2]; // $request['order_number'];
        }
        $this->superOrder = $this->loadSuperOrder($orderNumber);

        // Check that the superorder has valid orders
        $this->orders = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('superorder_id', $this->superOrder->getId())
            ->addFieldToFilter('edited', array('neq' => 1));

        if (count($this->orders) < 1) {
            throw new Exception('Order not found');
        }

        $this->customer = Mage::getModel('customer/customer')->load($this->superOrder->getCustomerId());

        if (!$this->customer->getId()) {
            throw new Exception('Customer not found');
        }
    }

    /**
     * Get the actual quote
     *
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    public function getQuote()
    {
        if (is_null($this->quote)) {
            $this->quote = Mage::getModel('sales/quote')->load($this->getOrder()->getQuoteId());
        }

        return $this->quote;
    }

    /**
     * Get all the order packages
     *
     * @return array
     */
    public function getPackages()
    {
        if (is_null($this->packages)) {
            $allPackages = array();
            $allDbPackages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('order_id', $this->superOrder->getId());
            $allDbOrders = Mage::getResourceModel('sales/order_collection')
                ->addFieldToFilter('superorder_id', $this->superOrder->getId())
                ->addFieldToFilter('edited', array('neq' => 1))
                ->load();
            /** @var $package Vznl_Package_Model_Package */
            foreach ($allDbPackages as &$package) {
                $id = $package->getPackageId();
                $thisPackage = [];
                $thisPackage['items'] = [];
                /** @var $order Vznl_Checkout_Model_Sales_Order */
                foreach ($allDbOrders as &$order) {
                    $allItems = $order->getAllItems();
                    foreach ($allItems as &$item) {
                        if ($item->getPackageId() == $id) {
                            $thisPackage['items'][] = $item;
                            $thisPackage['address'] = $order->getShippingAddress();
                            $thisPackage['payment']['method'] = $order->getPayment()->getMethod();
                            $thisPackage['payment']['paid'] = $package->isPayed() ? true : false;
                        }
                    }
                    unset($item);
                }
                $thisPackage['totals'] = $package->getPackageTotals($thisPackage['items']);
                $thisPackage['creditcheck_code'] = $package->getPackageEsbNumber();
                $thisPackage['creditcheck_status'] = $package->getCreditcheckStatus();
                $thisPackage['porting_status'] = $package->getPortingStatus();
                $thisPackage['status'] = $package->getStatus();
                $thisPackage['ctn'] = $package->getCtn();
                $thisPackage['coupon'] = $package->getCoupon();
                $thisPackage['sale_type'] = $package->getSaleType();
                $thisPackage['number_porting_validation_type'] = $package->getData('number_porting_validation_type');
                $thisPackage['number_porting_client_id'] = $package->getData('number_porting_client_id');
                $thisPackage['return_notes'] = $package->getData('return_notes');
                $thisPackage['current_number'] = $package->getData('current_number');
                $thisPackage['current_simcard_number'] = $package->getData('current_simcard_number');
                unset($order);
                $allPackages[$id] = $thisPackage;
            }
            unset($package);
            $this->packages = $allPackages;
        }

        return $this->packages;
    }

    /**
     * Get the first order.
     *
     * @return Vznl_Checkout_Model_Sales_Order
     */
    public function getOrder()
    {
        return $this->orders->setPageSize(1, 1)->getLastItem();
    }

    /**
     * Get the order cart totals.
     *
     * @return array
     */
    protected function getCartTotals()
    {
        $priceMonth = 0;
        $priceMonthIncludingVat = 0;
        $priceTotal = 0;
        $priceTotalIncludingVat = 0;
        foreach ($this->getOrder()->getAllItems() as $item) {
            $priceMonth += $item->getMaf();
            $priceMonthIncludingVat += $item->getMafInclTax();

            $priceTotal += $item->getPrice();
            $priceTotalIncludingVat += $item->getPriceInclTax();
        }

        return array(
            'monthly' => array(
                'price' => $priceMonth,
                'includingVat' => $priceMonthIncludingVat
            ),
            'total' => array(
                'price' => $priceTotal,
                'includingVat' => $priceTotalIncludingVat
            )
        );
    }

    /**
     * Get the order business details.
     *
     * @return array
     */
    public function getBusinessData()
    {
        $result = [
            'company' => [
                'address' => [
                    'city' => $this->customer->getCompanyCity(),
                    'country' => $this->getCountryFromCode($this->customer->getCompanyCountryId()),
                    'street' => $this->customer->getCompanyStreet(),
                    'house_number' => $this->customer->getCompanyHouseNr(),
                    'house_number_addition' => $this->customer->getCompanyHouseNrAddition(),
                    'postal_code' => $this->customer->getCompanyPostcode()
                ],
                'coc_number' => $this->customer->getCompanyCoc(),
            ],
            'contracting_party' => [
                'birthdate' => null,
                'email' => null,
                'correspondence_email' => null,
                'gender' => null,
                'initials' => null,
                'last_name' => null,
                'last_name_prefix' => null,
                'phone_1' => null,
                'phone_2' => null
            ]
        ];

        $result['company']['address']['city'] = $this->customer->getCompanyCity();
        $result['company']['address']['country'] = $this->getCountryFromCode($this->customer->getCompanyCountryId());
        $result['company']['address']['street'] = $this->customer->getCompanyStreet();
        $result['company']['address']['house_number'] = $this->customer->getCompanyHouseNr();
        $result['company']['address']['house_number_addition'] = $this->customer->getCompanyHouseNrAddition();
        $result['company']['address']['postal_code'] = $this->customer->getCompanyPostcode();
        $result['company']['coc_number'] = $this->customer->getCompanyCoc();
        $result['company']['establish_date'] = $this->getTimestampFromDate($this->customer->getCompanyDate());
        $result['company']['legal_form'] = $this->customer->getCompanyLegalForm();
        $result['company']['name'] = $this->customer->getCompanyName();
        $result['company']['vat_number'] = $this->customer->getCompanyVatId();

        $result['contracting_party']['birthdate'] = null;
        $result['contracting_party']['email'] = null;
        $result['contracting_party']['correspondence_email'] = null;
        $result['contracting_party']['gender'] = null;
        $result['contracting_party']['initials'] = null;
        $result['contracting_party']['last_name'] = null;
        $result['contracting_party']['last_name_prefix'] = null;
        $result['contracting_party']['phone_1'] = null;
        $result['contracting_party']['phone_2'] = null;

        if ($this->getQuote()->getCompanyCoc()) {
            $result['company']['billing_customer_id'] = $this->customer->getBan();
            $result['contracting_party']['birthdate'] = $this->getTimestampFromDate($this->getQuote()->getCustomerDob());
            $result['contracting_party']['email'] = $this->getQuote()->getAdditionalEmail();
            $result['contracting_party']['correspondence_email'] =  $this->getQuote()->getCorrespondenceEmail();
            $result['contracting_party']['gender'] = $this->getGender($this->getQuote()->getContractantGender());
            $result['contracting_party']['initials'] = $this->getQuote()->getContractantFirstname();
            $result['contracting_party']['last_name'] = $this->getQuote()->getContractantLastname();
            $result['contracting_party']['last_name_prefix'] = $this->getQuote()->getContractantMiddlename();
            $i = 1;
            $phonenumbers = explode(';', $this->getQuote()->getAdditionalTelephone());
            foreach ($phonenumbers as $phone) {
                $result['contracting_party']['phone_' . $i] = empty($phone) ? null : $phone;
                $i++;
            }
        }

        return $result;
    }

    /**
     * Get the order contract data
     * @return array
     */
    public function getContractantData()
    {
        $result = array();
        $isBusiness = $this->getQuote()->getCompanyCoc() ? true : false;
        if ($isBusiness) {
            $result['billing_customer_id'] = null;
            $result['birthdate'] = null;
            $result['email'] = null;
            $result['correspondence_email'] = null;
            $result['gender'] = null;
            $result['initials'] = null;
            $result['last_name'] = null;
            $result['last_name_prefix'] = null;
            $result['phone_1'] = null;
            $result['phone_2'] = null;

            $result['invoice_address']['city'] = null;
            $result['invoice_address']['country'] = null;
            $result['invoice_address']['street'] = null;
            $result['invoice_address']['house_number'] = null;
            $result['invoice_address']['house_number_addition'] = null;
            $result['invoice_address']['postal_code'] = null;
        } else {
            $result['billing_customer_id'] = $this->customer->getBan();
            $result['birthdate'] = $this->getTimestampFromDate($this->customer->getDob());
            $result['email'] = $this->customer->getAdditionalEmail();
            $result['correspondence_email'] = $this->getQuote()->getCorrespondenceEmail();
            $result['gender'] = $this->getGender($this->customer->getGender());
            $result['initials'] = $this->customer->getFirstname();
            $result['last_name'] = $this->customer->getLastname();
            $result['last_name_prefix'] = $this->customer->getMiddlename();

            $i = 1;
            if (!is_null($this->customer->getCustomerCtn())) {
                $result['phone_' . $i] = $this->customer->getData('customer_ctn');
                $i++;
            }
            foreach (explode(';', $this->customer->getAdditionalTelephone()) as $phone) {
                if (!empty($phone)) {
                    $result['phone_' . $i] = $phone;
                    $i++;
                }
            }
            $billingAddress = $this->customer->getDefaultBillingAddress() ?: Mage::getModel('customer/address');
            $result['invoice_address']['city'] = $billingAddress->getCity();
            $result['invoice_address']['country'] = $this->getCountryFromCode($billingAddress->getCountry());
            $result['invoice_address']['street'] = $billingAddress->getStreet1();
            $result['invoice_address']['house_number'] = $billingAddress->getStreet2();
            $result['invoice_address']['house_number_addition'] = $billingAddress->getStreet3();
            $result['invoice_address']['postal_code'] = $billingAddress->getPostcode();
        }

        return $result;
    }

    protected function getDeliveryOption()
    {
        return $this->getOrder()->getShippingAddress()->getDeliveryType() == 'store' ? 'pick_up_in_shop' : 'home_delivery';
    }

    /**
     * Get the order delivery data
     * @return array
     */
    public function getDeliveryData()
    {
        $prices = $this->getCartTotals();
        $shippingAddress = $this->getOrder()->getShippingAddress();
        $result = array();

        $result['selected_delivery_option'] = $this->getDeliveryOption();
        $result['home_delivery']['address']['city'] = null;
        $result['home_delivery']['address']['country'] = null;
        $result['home_delivery']['address']['street'] = null;
        $result['home_delivery']['address']['house_number'] = null;
        $result['home_delivery']['address']['house_number_addition'] = null;
        $result['home_delivery']['address']['postal_code'] = null;
        $result['home_delivery']['address_same_as_invoice'] = null;
        $result['home_delivery']['price'] = null;

        $result['pick_up_in_shop']['city'] = null;
        $result['pick_up_in_shop']['dealer_code'] = null;
        $result['pick_up_in_shop']['street'] = null;
        $result['pick_up_in_shop']['postal_code'] = null;
        $result['pick_up_in_shop']['price'] = null;

        if ($result['selected_delivery_option'] === 'home_delivery') {
            $result['home_delivery']['address']['city'] = $shippingAddress->getCity();
            $result['home_delivery']['address']['country'] = $this->getCountryFromCode($shippingAddress->getCountry());
            $result['home_delivery']['address']['street'] = $shippingAddress->getStreet1();
            $result['home_delivery']['address']['house_number'] = $shippingAddress->getStreet2();
            $result['home_delivery']['address']['house_number_addition'] = $shippingAddress->getStreet3();
            $result['home_delivery']['address']['postal_code'] = $shippingAddress->getPostcode();
            $result['home_delivery']['address_same_as_invoice'] = (bool) $shippingAddress->getSameAsBilling();
            $result['home_delivery']['price'] = 0;
        } else {
            $dealer = Mage::getModel('agent/dealer')->load($this->getOrder()->getDealerId());
            $result['pick_up_in_shop']['city'] = $shippingAddress->getCity();
            $result['pick_up_in_shop']['dealer_code'] = $dealer->getVfDealerCode();
            $result['pick_up_in_shop']['street'] = implode(' ', $shippingAddress->getStreet());
            $result['pick_up_in_shop']['postal_code'] = $shippingAddress->getPostcode();
            $result['pick_up_in_shop']['price'] = 0;
        }

        return $result;
    }

    /**
     * Get the order identity data
     * @return array
     */
    public function getIdentityData()
    {
        $result = array();
        $result['identity_expiry_date'] = $this->getTimestampFromDate($this->customer->getValidUntil());
        $result['identity_number'] = $this->customer->getIdNumber();
        $result['identity_type'] = $this->customer->getIdType();
        $result['nationality'] = $this->customer->getIssuingCountry();

        return $result;
    }

    /**
     * For the webshop they want to see all the products as number porting row
     * except the promotions / addons and accessoires
     * @return array
     */
    public function getNumberPortingData()
    {
        $result = array();
        $iteratorPackageId = null;
        $packageItems = '';
        foreach ($this->getPackages() as $packageId => $package) {
            if ($iteratorPackageId !== $packageId) {
                $packageItems = '';
                $iteratorPackageId = $packageId;
                foreach ($package['items'] as $quoteItem) {
                    $packageItems .= $quoteItem->getName() . ', ';
                }
            }
            $mobileNumberPorting = array();
            $mobileNumberPorting['customer_requests_porting'] = $package['current_number'] ? true : false;
            $mobileNumberPorting['package_id'] = $packageId;
            $mobileNumberPorting['package_is_portable'] = false;
            $mobileNumberPorting['fulfilment_status']['porting_date'] = $this->getTimestampFromDate($package['actual_porting_date']);
            $mobileNumberPorting['fulfilment_status']['porting_status'] = $package['porting_status'];
            $mobileNumberPorting['product_name'] = rtrim($packageItems, ', ');
            $mobileNumberPorting['porting_data']['contract_number'] = isset($package['number_porting_client_id']) ? $package['number_porting_client_id'] : null;
            $mobileNumberPorting['porting_data']['current_msisdn'] = $package['current_number'];
            $mobileNumberPorting['porting_data']['validation_type'] = $package['number_porting_validation_type'];

            foreach ($package['items'] as $quoteItem) {
                if ($quoteItem->getProduct()->isSubscription()) {
                    $mobileNumberPorting['package_is_portable'] = true;
                }
            }
            $result[] = $mobileNumberPorting;
        }

        return $result;
    }

    /**
     * Get the order fulfillment data
     * @return array
     */
    public function getOrderFulfilmentStatusData()
    {
        $deliveryOrder = $this->orders->setPageSize(1, 1)->getLastItem();
        if (!is_null($deliveryOrder->getPlannedDate())) {
            $deliveryDate = new DateTime($deliveryOrder->getPlannedDate());
        }
        $package = $this->getPackages()[1];

        $result = array();
        $result['decline_reason'] = !empty($package['return_notes']) ? $package['return_notes'] : null;
        $result['delivery_date'] = isset($deliveryDate) && !is_null($deliveryDate) ? $deliveryDate->format('Y-m-d') : null;
        $result['delivery_service'] = $deliveryOrder->getCarrierName();
        $result['delivery_time'] = isset($deliveryDate) && !is_null($deliveryDate) ? $deliveryDate->format('H:i:s') : null;
        $result['status'] = $this->superOrder->getOrderStatus();
        $result['track_n_trace_url'] = $deliveryOrder->getTrackAndTraceUrl();

        return $result;
    }

    /**
     * Get order payment data
     * @return array
     */
    public function getPaymentData()
    {
        $result = array();
        $result['account_name'] = $this->customer->getAccountHolder();
        $result['account_nr'] = $this->customer->getAccountNo();

        return $result;
    }

    /**
     * Get the order privacy data
     * @return array
     */
    public function getPrivacyData()
    {
        $result = array();
        $result['marketing'] = (bool) $this->customer->getPrivacyEmail();
        $result['terms_agreed'] = (bool) true;
        $result['number_info'] = (bool) $this->customer->getPrivacyNumberInformation();

        return $result;
    }

    /**
     * Get Thuiskopieheffing product skus
     * @return array
     */
    protected function getThuiskopieheffingProductSkus()
    {
        $data = array();
        $data[] = Mage::getStoreConfig(Vznl_PriceRules_Model_Observer::COPY_LEVY_PREFIX . 'standaard');

        return $data;
    }

    /**
     * Get all products
     * @return array
     */
    public function getProductData()
    {
        $result = array();
        $taxHelper = Mage::helper('tax');
        $store = Mage::app()->getStore();

        // Sort data per package
        foreach ($this->getPackages() as $package) {
            $packageData = array();
            $packageData['ctn'] = null;
            $packageData['salestype'] = null;
            $packageData['category'] = null;
            $packageData['device_category'] = null;
            $packageData['device_id'] = null;
            $packageData['device_name'] = null;
            $packageData['device_sku'] = null;
            $packageData['subscription_id'] = null;
            $packageData['subscription_name'] = null;
            $packageData['subscription_sku'] = null;
            $packageData['subscription_data'] = null;
            $packageData['subscription_duration'] = null;
            $packageData['subscription_minutes'] = null;
            $packageData['subscription_sms'] = null;
            $packageData['devicerc_id'] = null;
            $packageData['devicerc_name'] = null;
            $packageData['devicerc_sku'] = null;
            $packageData['sim_id'] = null;
            $packageData['sim_sku'] = null;
            $packageData['sim_name'] = null;
            $packageData['sim_number'] = null;
            $packageData['extras']['accessories'] = array();
            $packageData['extras']['addons'] = array();
            $packageData['voucher'] = $package['coupon'];

            $package['totals']['device_tax'] = 0;
            $package['totals']['device_subtotal'] = 0;
            $package['totals']['device_total'] = 0;
            $package['totals']['thuiskopieheffing_tax'] = 0;
            $package['totals']['thuiskopieheffing_subtotal'] = 0;
            $package['totals']['thuiskopieheffing_total'] = 0;
            $package['totals']['subscription_tax'] = 0;
            $package['totals']['subscription_subtotal'] = 0;
            $package['totals']['subscription_total'] = 0;
            $package['totals']['deviceprice_tax'] = 0;
            $package['totals']['deviceprice_subtotal'] = 0;
            $package['totals']['deviceprice_total'] = 0;
            $package['totals']['connection_tax'] = 0;
            $package['totals']['connection_subtotal'] = 0;
            $package['totals']['connection_total'] = 0;

            foreach ($package['items'] as $item) {
                $packageData['quantity'] = 1;
                $packageData['salestype'] = $this->getHawaiiSaleType($package['sale_type']);
                $packageData['ctn'] = $package['ctn'];
                $packageData['package_id'] = (int) $item->getPackageId();

                if (Mage::helper('vznl_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE), $item->getProduct())) {
                    $packageData['category'] = $item->getProduct()->getAttributeText('identifier_device_post_prepaid');
                    $packageData['device_category'] = 'phone';
                    $packageData['device_id'] = $item->getProduct()->getData('identifier_hawaii_id');
                    $packageData['device_name'] = $item->getProduct()->getName();
                    $packageData['device_sku'] = $item->getProduct()->getSku();

                    $package['totals']['device_tax'] += Mage::helper('core')->currency($item->getTaxAmount(), false, true);
                    $package['totals']['device_subtotal'] += Mage::helper('core')->currency($item->getPrice(), false, true);
                    $package['totals']['device_total'] += Mage::helper('core')->currency($item->getRowTotalInclTax(), false, true);
                } elseif (in_array($item->getProduct()->getSku(), $this->getThuiskopieheffingProductSkus())) {
                    $package['totals']['thuiskopieheffing_tax'] = Mage::helper('core')->currency($item->getTaxAmount(), false, true);
                    $package['totals']['thuiskopieheffing_subtotal'] = Mage::helper('core')->currency($item->getPrice(), false, true);
                    $package['totals']['thuiskopieheffing_total'] = Mage::helper('core')->currency($item->getPriceInclTax(), false, true);
                } elseif ($item->getProduct()->isSubscription()) {
                    $packageData['subscription_id'] = $item->getProduct()->getAttributeText('identifier_hawaii_id');
                    $packageData['subscription_name'] = $item->getProduct()->getName();
                    $packageData['subscription_sku'] = $item->getProduct()->getSku();
                    $packageData['subscription_data'] = $item->getProduct()->getProdspecsDataAantalMb();
                    $packageData['subscription_duration'] = $item->getProduct()->getAttributeText('identifier_commitment_months');
                    $packageData['subscription_minutes'] = $item->getProduct()->getProdspecsAantalBelminuten();
                    $packageData['subscription_sms'] = $item->getProduct()->getProdspecsSmsAmount();

                    $priceMaf = $item->getMafRowTotalInclTax() - $item->getMafDiscountAmount();
                    $priceStart = $store->roundPrice($store->convertPrice($priceMaf));
                    $price = $taxHelper->getPrice($item->getProduct(), $priceStart, false);
                    $priceInludingTax = $taxHelper->getPrice($item->getProduct(), $priceStart, true);
                    $package['totals']['subscription_tax'] += Mage::helper('core')->currency($priceInludingTax - $price, false, true);
                    $package['totals']['subscription_subtotal'] += Mage::helper('core')->currency($price, false, true);
                    $package['totals']['subscription_total'] += Mage::helper('core')->currency($priceInludingTax, false, true);

                    $prices = $this->renderPrices($item->getProduct()->getData('hawaii_subscr_device_price'), $item->getProduct());
                    $package['totals']['deviceprice_tax'] += $prices['tax'];
                    $package['totals']['deviceprice_subtotal'] += $prices['subtotal'];
                    $package['totals']['deviceprice_total'] += $prices['total'];

                    $prices = $this->renderPrices($item->getConnectionCost(), $item->getProduct());
                    $package['totals']['connection_tax'] += $prices['tax'];
                    $package['totals']['connection_subtotal'] += $prices['subtotal'];
                    $package['totals']['connection_total'] += $prices['total'];

                } elseif (Mage::helper('vznl_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD), $item->getProduct())) {
                    $packageData['sim_number'] = null; // @todo for ILS
                    $packageData['sim_id'] = $item->getProduct()->getData('identifier_hawaii_id');
                    $packageData['sim_name'] = $item->getProduct()->getName();
                    $packageData['sim_sku'] = $item->getProduct()->getSku();
                } elseif ($item->getProduct()->isDeviceSubscription()) {
                    $packageData['devicerc_id'] = $item->getProduct()->getAttributeText('identifier_hawaii_id');
                    $packageData['devicerc_name'] = $item->getProduct()->getName();
                    $packageData['devicerc_sku'] = $item->getProduct()->getSku();

                    $package['totals']['device_tax']        += Mage::helper('core')->currency($item->getTaxAmount(), false, true);
                    $package['totals']['device_subtotal']   += Mage::helper('core')->currency($item->getPrice(), false, true);
                    $package['totals']['device_total']      += Mage::helper('core')->currency($item->getRowTotalInclTax(), false, true);
                } elseif (Mage::helper('vznl_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY), $item->getProduct())) {
                    $accessory = array();
                    $accessory['id'] = $item->getProduct()->getAttributeText('identifier_hawaii_id');
                    $accessory['name'] = $item->getProduct()->getName();
                    $accessory['sku'] = $item->getProduct()->getSku();

                    $accessory['item_price']['normal_price']['amount_vat'] = Mage::helper('core')->currency($item->getTaxAmount(), false, true);
                    $accessory['item_price']['normal_price']['price_excl_vat'] = Mage::helper('core')->currency($item->getPrice(), false, true);
                    $accessory['item_price']['normal_price']['price_incl_vat'] = Mage::helper('core')->currency($item->getPriceInclTax(), false, true);
                    $accessory['item_price']['has_promo'] = false; // @todo
                    $packageData['extras']['accessories'][] = $accessory;
                } elseif (Mage::helper('vznl_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_ADDON), $item->getProduct())) {
                    $addon = array();
                    $addon['id'] = $item->getProduct()->getData('identifier_hawaii_id');
                    $addon['name'] = $item->getProduct()->getName();
                    $addon['sku'] = $item->getProduct()->getSku();

                    $addon['item_price']['normal_price']['amount_vat'] = Mage::helper('core')->currency($item->getMafTaxAmount(), false, true);
                    $addon['item_price']['normal_price']['price_excl_vat'] = Mage::helper('core')->currency($item->getMafRowTotal(), false, true);
                    $addon['item_price']['normal_price']['price_incl_vat'] = Mage::helper('core')->currency($item->getMafRowTotalInclTax(), false, true);

                    $addon['item_price']['has_promo'] = false; // @todo
                    $packageData['extras']['addons'][] = $addon;
                }
            }
            $packageData['package_price']['price_once']['device']['promo_price']['amount_vat'] = (float) $package['totals']['device_tax'];
            $packageData['package_price']['price_once']['device']['promo_price']['price_excl_vat'] = (float) $package['totals']['device_subtotal'];
            $packageData['package_price']['price_once']['device']['promo_price']['price_incl_vat'] = (float) $package['totals']['device_total'];
            $packageData['package_price']['price_once']['device']['thuisheffing']['amount_vat'] = (float) $package['totals']['thuiskopieheffing_tax'];
            $packageData['package_price']['price_once']['device']['thuisheffing']['price_excl_vat'] = (float) $package['totals']['thuiskopieheffing_subtotal'];
            $packageData['package_price']['price_once']['device']['thuisheffing']['price_incl_vat'] = (float) $package['totals']['thuiskopieheffing_total'];
            $packageData['package_price']['price_once']['subscription']['connection_price']['amount_vat'] = (float) $package['totals']['connection_tax'];
            $packageData['package_price']['price_once']['subscription']['connection_price']['price_excl_vat'] = (float) $package['totals']['connection_subtotal'];
            $packageData['package_price']['price_once']['subscription']['connection_price']['price_incl_vat'] = (float) $package['totals']['connection_total'];
            $packageData['package_price']['price_once']['total']['promo_price']['amount_vat'] = (float) $package['totals']['tax_price'];
            $packageData['package_price']['price_once']['total']['promo_price']['price_excl_vat'] = (float) $package['totals']['subtotal_price'];
            $packageData['package_price']['price_once']['total']['promo_price']['price_incl_vat'] = (float) $package['totals']['total'];

            $packageData['package_price']['price_recurring']['device']['promo_price']['amount_vat'] = (float) $package['totals']['deviceprice_tax'];
            $packageData['package_price']['price_recurring']['device']['promo_price']['price_excl_vat'] = (float) $package['totals']['deviceprice_subtotal'];
            $packageData['package_price']['price_recurring']['device']['promo_price']['price_incl_vat'] = (float) $package['totals']['deviceprice_total'];
            $packageData['package_price']['price_recurring']['subscription']['promo_price']['amount_vat'] = (float) $package['totals']['subscription_tax'] - $package['totals']['deviceprice_tax'];
            $packageData['package_price']['price_recurring']['subscription']['promo_price']['price_excl_vat'] = (float) $package['totals']['subscription_subtotal'] - $package['totals']['deviceprice_subtotal'];
            $packageData['package_price']['price_recurring']['subscription']['promo_price']['price_incl_vat'] = (float) $package['totals']['subscription_total'] - $package['totals']['deviceprice_total'];
            $packageData['package_price']['price_recurring']['total']['promo_price']['amount_vat'] = (float) $package['totals']['tax_maf'];
            $packageData['package_price']['price_recurring']['total']['promo_price']['price_excl_vat'] = (float) $package['totals']['subtotal_maf'];
            $packageData['package_price']['price_recurring']['total']['promo_price']['price_incl_vat'] = (float) $package['totals']['total_maf'];

            $result[] = $packageData;
        }

        return $result;
    }

    /**
     * Get the order total prices
     * @return array
     */
    public function getTotalPriceData()
    {
        $result = array();
        $totals = array();

        $totals['subtotal_price'] = 0;
        $totals['subtotal_maf'] = 0;
        $totals['tax_price'] = 0;
        $totals['total'] = 0;
        $totals['tax_maf'] = 0;
        $totals['total_maf'] = 0;
        $totals['connection_tax'] = 0;
        $totals['connection_subtotal'] = 0;
        $totals['connection_total'] = 0;

        foreach ($this->getPackages() as $package) {
            $package['totals']['connection_tax'] = 0;
            $package['totals']['connection_subtotal'] = 0;
            $package['totals']['connection_total'] = 0;

            foreach ($package['items'] as $item) {
                if ($item->getProduct()->isSubscription()) {
                    $prices = $this->renderPrices($item->getConnectionCost(), $item->getProduct());
                    $package['totals']['connection_tax'] += $prices['tax'];
                    $package['totals']['connection_subtotal'] += $prices['subtotal'];
                    $package['totals']['connection_total'] += $prices['total'];

                    $totals['connection_tax'] += $package['totals']['connection_tax'];
                    $totals['connection_subtotal'] += $package['totals']['connection_subtotal'];
                    $totals['connection_total'] += $package['totals']['connection_total'];
                }
            }
            $totals['subtotal_price'] += $package['totals']['subtotal_price'];
            $totals['subtotal_maf'] += $package['totals']['subtotal_maf'];
            $totals['tax_price'] = $totals['tax_maf'] + $package['totals']['tax_price'];
            $totals['total'] += $package['totals']['total'];
            $totals['tax_maf'] += $package['totals']['tax_maf'];
            $totals['total_maf'] += $package['totals']['total_maf'];
        }
        $result['price_once']['connection_price']['amount_vat'] = $totals['connection_tax'];
        $result['price_once']['connection_price']['price_excl_vat'] = $totals['connection_subtotal'];
        $result['price_once']['connection_price']['price_incl_vat'] = $totals['connection_total'];
        $result['price_once']['devices']['amount_vat'] = $totals['tax_price'];
        $result['price_once']['devices']['price_excl_vat'] = $totals['subtotal_price'];
        $result['price_once']['devices']['price_incl_vat'] = $totals['total'];

        $priceTax = 0;
        $priceSubtotal = 0;
        $priceTotal = 0;
        if ($this->getOrder()->getPayment()->getMethodInstance()->getCode() === 'cashondelivery' && $this->getDeliveryOption() === 'home_delivery') {
            $priceTax = $totals['tax_price'];
            $priceSubtotal = $totals['subtotal_price'];
            $priceTotal = $totals['total'];
        }
        $result['price_once']['delivery_costs']['amount_vat'] = $priceTax;
        $result['price_once']['delivery_costs']['price_excl_vat'] = $priceSubtotal;
        $result['price_once']['delivery_costs']['price_incl_vat'] = $priceTotal;

        $result['price_recurring']['amount_vat'] = $totals['tax_maf'];
        $result['price_recurring']['price_excl_vat'] = $totals['subtotal_maf'];
        $result['price_recurring']['price_incl_vat'] = $totals['total_maf'];

        return $result;
    }

    /**
     * Count the total number of products
     * @return int
     */
    public function getProductCount()
    {
        $productCount = 0;
        foreach ($this->getPackages() as $package) {
            foreach ($package['items'] as $item) {
                $possibleValues = array(
                    Vznl_Catalog_Model_Type::SUBTYPE_DEVICE,
                    Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                    Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY,
                    Vznl_Catalog_Model_Type::SUBTYPE_ADDON,
                    Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD
                );
                if (Mage::helper('vznl_catalog')->is($possibleValues, $item->getProduct())) {
                    $productCount++;
                }
            }
        }

        return (int) $productCount;
    }

    /**
     * Tell me if the order is paid or not
     * @return boolean
     */
    public function isPaid()
    {
        foreach ($this->getPackages() as $package) {
            return $package['payment']['paid'];
        }

        return false;
    }

    protected function getDealerCode()
    {
        $dealer = Mage::getModel('agent/dealer')->load($this->getOrder()->getDealerId());
        if ($dealer->getId()) {
            return $dealer->getVfDealerCode();
        }

        return null;
    }

    /**
     * Process the HTTP request.
     *
     * @return array
     * @todo Partly dependent on database migration, test again after database migration is finished.
     */
    public function process()
    {
        $response = array();

        $response['business_details'] = $this->getBusinessData();
        $response['contracting_party'] = $this->getContractantData();
        $response['dealer_code'] = $this->getDealerCode();
        $response['delivery_options'] = $this->getDeliveryData();
        $response['dynalean_order_id'] = $this->getOrder()->getId();
        $response['adyen_payment_id'] = $this->getOrder()->getIncrementId();
        $response['dynalean_quote_id'] = $this->getOrder()->getQuoteId();
        $response['identity'] = $this->getIdentityData();
        $response['is_business'] = (bool) $this->getOrder()->getCustomerIsBusiness();
        $response['mobile_number_porting'] = $this->getNumberPortingData();

        $response['order_fulfilment_status'] = $this->getOrderFulfilmentStatusData();
        $response['payment_details'] = $this->getPaymentData();
        $response['payment_method'] = $this->getOrder()->getPayment()->getMethodInstance()->getCode();
        $response['payment_result'] = $this->isPaid();
        $response['privacy'] = $this->getPrivacyData();
        $response['products'] = $this->getProductData();
        $response['nr_products'] = count($this->getPackages());
        $response['dynalean_order_id'] = $this->superOrder->getOrderNumber();
        $response['shopping_order_id'] = $this->getQuote()->getHawaiiShoppingOrderId();
        $response['total_price'] = $this->getTotalPriceData();
        $response['version_id'] = (int) $this->getQuote()->getHawaiiVersionId();

        // Errors
        foreach ($this->getErrors() as $error) {
            $key = str_replace('%INDEX%', $error['index'], $error['key']);
            $response['errors'][] = array("item_key" => $key, "message" => $error['message']);
        }

        return $response;
    }
}
