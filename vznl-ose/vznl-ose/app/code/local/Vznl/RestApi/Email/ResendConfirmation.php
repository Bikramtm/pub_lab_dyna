<?php

/**
 * Class Vznl_RestApi_Email_ResendConfirmation
 * Depedencies:
 * Vznl_Agent_Model_Website
 */
class Vznl_RestApi_Email_ResendConfirmation
{
    /**
     * @var string|int
     */
    protected $orderNumber;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var Dyna_Superorder_Model_Superorder
     */
    protected $superOrder;

    /**
     * @var Mage_Sales_Model_Resource_Order_Collection
     */
    protected $orderCollection;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @throws Exception
     */
    public function __construct($request)
    {
        $this->orderNumber = $request->getParam('order_number');
        $this->email = $request->getParam('email');

        try {
            $this->superOrder = Mage::getModel('superorder/superorder')->load((string) $this->orderNumber, 'order_number');
        } catch (Exception $e){
            throw new Exception('Order not found');
        }

        // Validate email address
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception('Email is not valid');
        }

        if (Mage::app()->getWebsite($this->superOrder->getCreatedWebsiteId())->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
            throw new Exception('Can\'t send email confirmation for indirect orders');
        }

        // Check that the superorder has valid orders
        $this->orderCollection = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('superorder_id', $this->superOrder->getId())
            ->addFieldToFilter('edited', array('neq' => 1));

        if (count($this->orderCollection) < 1) {
            throw new Exception('Collection not found');
        }
    }

    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        $response = array();
        try {
            $payload = Mage::helper('communication/email')->getOrderConfirmationPayload($this->superOrder);
            $payload['receiver'] = $this->email;
            Mage::getSingleton('communication/pool')->add($payload);

            /** @var Vznl_Communication_Helper_Sms $smsHelper */
            $smsHelper = Mage::helper('communication/sms');
            $smsHelper->createOrderConfirmationSms($this->superOrder);

            $response['success'] = true;
        } catch (Exception $e) {
            $response['success'] = false;
            $response['errors'][] = array('item_key' => 'request', 'message' => $e->getMesage());
        }

        return $response;
    }
}
