<?php

class Vznl_RestApi_Catalogue extends Vznl_RestApi_Abstract
{
    /**
     * @var array
     */
    public static $routes = array(
        array(
            'method' => 'POST',
            'path' => '/catalogue',
            'class' => 'Vznl_RestApi_Catalogue_Get',
            'log' => false,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/catalogue/new',
            'class' => 'Vznl_RestApi_Catalogue_GetNew',
            'log' => false,
            'active' => true,
        ),
        array(
            'method' => 'POST',
            'path' => '/catalogue/v6',
            'class' => 'Vznl_RestApi_Catalogue_Get6',
            'log' => false,
            'active' => true,
        ),
    );
}
