<?php

class Vznl_RestApi_Friendsfamily_Cancel extends Vznl_RestApi_Processor
{
    /**
     * Process the HTTP request.
     * 
     * @return array
     * @todo Test after database migration
     */
    public function process()
    {
        $params = $this->getBodyParams();

        /**
         * @var Vznl_FFHandler_Model_Request $requestModel
         */
        $requestModel = Mage::getModel("ffhandler/request");
        $requestModel->loadByConfirmationHash($params["hashCode"]);
        
        if (!$requestModel->getId() || $requestModel->getRequesterEmail() != $params["email"]) {
            return $this->error('01', 'The provided token does not exists');
        }

        if ($requestModel->getStatus() != Vznl_FFHandler_Model_Request::STATUS_WAITING) {
            return $this->error('02', 'The provided code cannot be cancelled.');
        }

        $requestModel->setStatus(Vznl_FFHandler_Model_Request::STATUS_CANCELED);
        $requestModel->save();

        return [
            'success' => true,
            'error' => []
        ];
    }

    /**
     * @param string $code
     * @param string $message
     * @return array
     */
    private function error($code, $message)
    {
        return [
            'success' => false,
            'error' => [
                [
                    'code' => $code,
                    'message' => $message
                ]
            ],
        ];
    }
}