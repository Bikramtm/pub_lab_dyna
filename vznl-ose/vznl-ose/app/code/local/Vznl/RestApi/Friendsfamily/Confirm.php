<?php

class Vznl_RestApi_Friendsfamily_Confirm extends Vznl_RestApi_Processor
{
    /**
     * Process the HTTP request.
     * 
     * @return array
     * @todo Test after database migration
     */
    public function process()
    {
        $params = $this->getBodyParams();

        $response['success'] = false;
        $response['error'] = array();

        /**
         * @var Vznl_FFHandler_Model_Request $requestModel
         * @var Vznl_FFManager_Model_Relation $relationModel
         */
        $requestModel = Mage::getModel("ffhandler/request");
        $requestModel->loadByConfirmationHash($params["token"]);

        if (!$requestModel->getId()){
            $response['error'][] = array('code' => '01', 'message' => 'The provided token does not exists');
            return $response;
        }

        if ($requestModel->getId() && $requestModel->getRequesterEmail() != $params["email"]) {
            $response['error'][] = array('code' => '02', 'message' => 'The provided email does not match with token.');
            return $response;
        }

        if ($requestModel->getStatus() == Vznl_FFHandler_Model_Request::STATUS_COMPLETED) {
            $response['error'][] = array('code' => '03', 'message' => 'The provided code was already confirmed.');
            return $response;
        }

        if ($requestModel->getStatus() != Vznl_FFHandler_Model_Request::STATUS_WAITING) {
            $response['error'][] = array('code' => '04', 'message' => 'The provided code cannot be confirmed.');
            return $response;
        }

        /**
         * get the relation and validate to be an active relation before confirming the request
         */
        $relationModel = Mage::getModel("ffmanager/relation");
        $relationModel->load($requestModel->getRelationId());
        if ($relationModel->getEnabled() != Vznl_FFManager_Model_Relation::IS_ENABLED) {
            $response['error'][] = array('code' => '05', 'message' => 'The relation is not active.');
            return $response;
        }

        $requestModel->complete();

        /**
         * @var Vznl_FFReport_Helper_Email $emailHelper
         */
        $emailHelper = Mage::helper("vznl_ffreport/email");
        $emailHelper->ffCodeGenerated($requestModel);

        return [
            'success' => true,
            'payload' => [
                'token' => $requestModel->getCode()
            ],
            'error' => [],
        ];
    }
}