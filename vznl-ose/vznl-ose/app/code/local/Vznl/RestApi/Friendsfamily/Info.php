<?php

class Vznl_RestApi_Friendsfamily_Info extends Vznl_RestApi_Processor
{
    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        $params = $this->getRouteParams();

        /**
         * @var Vznl_FFHandler_Model_Request $requestModel
         */
        $requestModel = Mage::getModel("ffhandler/request");
        $requestModel->loadByCode($params["token"]);

        if (!$requestModel->getId()) {
            return [
                'success' => false,
                'error' => [
                    [
                        'code' => '01',
                        'message' => 'The provided token does not exists'
                    ]
                ]
            ];
        }

        $requestData = $requestModel->getPublicData();

        return [
            'success' => true,
            'error' => [],
            'payload' => [
                'token' => $requestData['code'],
                'created_at' => $requestData['created_at'],
                'updated_at' => $requestData['updated_at'],
                'valid_from' => $requestData['valid_from'],
                'valid_to' => $requestData['valid_to']
            ],
        ];
    }

}