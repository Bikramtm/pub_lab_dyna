<?php

class Vznl_RestApi_Quotes_Get extends Vznl_RestApi_Quotes_Abstract
{
    protected $quote;
    protected $response = array();

    /**
     * @param object|array $request
     * @throws Exception
     * @todo Cannot test, awaiting database migration
     * @todo Throw exception when email address provided does not match quote
     */
    public function __construct($request = null)
    {
        if (!is_array($request)) {
            $routeParts = explode('/', $request->getQuery('route'));
            $quoteId = $routeParts[2];
            $email = $routeParts[3];
        } else {
            $quoteId = $request['quote_id'];
            $email = $request['email'];
        }

        /**
         * @var Vznl_Checkout_Model_Sales_Quote
         */
        $this->quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($quoteId);

        if (!$this->quote) {
            throw new Exception('Quote not found');
        }

        // Reset store ID
        if ($this->quote->getStoreId() != Mage::app()->getStore()->getStoreId()) {
            $this->quote->setStoreId(Mage::app()->getStore()->getStoreId());
            $this->quote->setTotalsCollectedFlag(true)->save();
        }

        $this->getCheckoutSession()->resetCheckout();
        $this->getCheckoutSession()->clear();
        $this->getCheckoutSession()->clearHelperData();
        $this->getCheckoutSession()->replaceQuote($this->quote);
    }

    /**
     * Get the actual quote.
     *
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        return $this->quote;
    }

    /**
     * Get the shipping address.
     *
     * @return object
     */
    protected function getShippingAddress()
    {
        $shippingData = Mage::helper('core')->jsonDecode($this->getQuote()->getShippingData());

        return $shippingData['deliver'];
    }

    /**
     * Get the billing address.
     *
     * @return object
     */
    protected function getBillingAddress()
    {
        return $this->getQuote()->getBillingAddress();
    }

    /**
     * @return array
     */
    protected function getThuiskopieheffingProductSkus()
    {
        $data = array();
        $data[] = Mage::getStoreConfig(Dyna_PriceRules_Model_Observer::COPY_LEVY_PREFIX . 'standaard');
        return $data;
    }

    /**
     *
     * @return typeGet the mapped business details of the quote
     */
    protected function getBusinessDetails()
    {
        $response = array();
        $response['company']['address']['city'] = $this->getQuote()->getCompanyCity();
        $response['company']['address']['country'] = $this->getCountryFromCode($this->getQuote()->getCompanyCountryId());
        $response['company']['address']['street'] = $this->getQuote()->getCompanyStreet();
        $response['company']['address']['house_number'] = $this->getQuote()->getCompanyHouseNr();
        $response['company']['address']['house_number_addition'] = $this->getQuote()->getCompanyHouseNrAddition();
        $response['company']['address']['postal_code'] = $this->getQuote()->getCompanyPostcode();
        $response['company']['billing_customer_id'] = $this->getQuote()->getCompanyCoc() ? $this->getQuote()->getCustomer()->getBan() : null;
        $response['company']['coc_number'] = $this->getQuote()->getCompanyCoc();
        $response['company']['establish_date'] = $this->getTimestampFromDate($this->getQuote()->getCompanyDate());
        $response['company']['legal_form'] = $this->getCompanyLegalForm($this->getQuote()->getCompanyLegalForm());
        $response['company']['name'] = $this->getQuote()->getCompanyName();
        $response['company']['vat_number'] = $this->getQuote()->getCompanyVatId();

        $response['contracting_party']['birthdate'] = null;
        $response['contracting_party']['email'] = null;
        $response['contracting_party']['correspondence_email'] = null;
        $response['contracting_party']['gender'] = null;
        $response['contracting_party']['initials'] = null;
        $response['contracting_party']['last_name'] = null;
        $response['contracting_party']['last_name_prefix'] = null;
        $response['contracting_party']['phone_1'] = null;
        $response['contracting_party']['phone_2'] = null;

        if ($this->getQuote()->getCompanyCoc()) {
            $response['contracting_party']['birthdate'] = $this->getTimestampFromDate($this->getQuote()->getCustomerDob());
            $emailStripped = explode(';', $this->getQuote()->getAdditionalEmail());
            $email = count($emailStripped) > 0 ? trim($emailStripped[0]) : null;
            $response['contracting_party']['email'] = $email;
            $response['contracting_party']['correspondence_email'] = $this->getQuote()->getCorrespondenceEmail();
            $response['contracting_party']['gender'] = $this->getGender($this->getQuote()->getContractantGender());
            $response['contracting_party']['initials'] = $this->getQuote()->getContractantFirstname();
            $response['contracting_party']['last_name'] = $this->getQuote()->getContractantLastname();
            $response['contracting_party']['last_name_prefix'] = $this->getQuote()->getContractantMiddlename();
            $i = 1;
            $phonenumbers = explode(';', $this->getQuote()->getAdditionalTelephone());
            foreach ($phonenumbers as $phone) {
                $response['contracting_party']['phone_' . $i] = empty($phone) ? null : $phone;
                $i++;
            }
        }

        return $response;
    }

    /**
     * Get all the mapped customer details of the quote
     */
    protected function getCustomerDetails()
    {
        $response = array();
        $billingAddress = $this->getBillingAddress();
        $isBusiness = $this->getQuote()->getCompanyCoc() ? true : false;

        if ($isBusiness) {
            $response['billing_customer_id'] = null;
            $response['birthdate'] = null;
            $response['email'] = null;
            $response['correspondence_email'] = null;
            $response['gender'] = null;
            $response['initials'] = null;
            $response['last_name'] = null;
            $response['last_name_prefix'] = null;
            $response['phone_1'] = null;
            $response['phone_2'] = null;

            $response['invoice_address']['city'] = null;
            $response['invoice_address']['country'] = null;
            $response['invoice_address']['street'] = null;
            $response['invoice_address']['house_number'] = null;
            $response['invoice_address']['house_number_addition'] = null;
            $response['invoice_address']['postal_code'] = null;
        } else {
            $response['billing_customer_id'] = $this->getQuote()->getCustomer()->getBan();
            $response['birthdate'] = $this->getTimestampFromDate($this->getQuote()->getCustomerDob());
            $emailStripped = explode(';', $this->getQuote()->getAdditionalEmail());
            $email = count($emailStripped) > 0 ? trim($emailStripped[0]) : null;
            $response['email'] = $email;
            $response['correspondence_email'] = $this->getQuote()->getCorrespondenceEmail();
            $response['gender'] = $this->getGender($this->getQuote()->getCustomerGender());
            $response['initials'] = $this->getQuote()->getCustomerFirstname();
            $response['last_name'] = $this->getQuote()->getCustomerLastname();
            $response['last_name_prefix'] = $this->getQuote()->getCustomerMiddlename();
            $i = 1;
            $phonenumbers = explode(';', $this->getQuote()->getAdditionalTelephone());
            foreach ($phonenumbers as $phone) {
                $response['phone_' . $i] = empty($phone) ? null : $phone;
                $i++;
            }
            $response['invoice_address']['city'] = $billingAddress->getCity();
            $response['invoice_address']['country'] = $this->getCountryFromCode($billingAddress->getCountry());
            $response['invoice_address']['street'] = $billingAddress->getStreet1();
            $response['invoice_address']['house_number'] = $billingAddress->getStreet2();
            $response['invoice_address']['house_number_addition'] = $billingAddress->getStreet3();
            $response['invoice_address']['postal_code'] = $billingAddress->getPostcode();
        }

        return $response;
    }

    /**
     * Get all the mapped identity details of the Quote
     *
     * @return array
     */
    protected function getIdentityDetails()
    {
        $response = array();

        if ($this->getQuote()->getCompanyCoc()) {
            $response['identity_expiry_date'] = $this->getTimestampFromDate($this->getQuote()->getContractantValidUntil());
            $response['identity_number'] = $this->getQuote()->getContractantIdNumber();
            $response['identity_type'] = $this->getIdType($this->getQuote()->getContractantIdType());
            $response['nationality'] = $this->getQuote()->getContractantIssuingCountry();
        } else {
            $response['identity_expiry_date'] = $this->getTimestampFromDate($this->getQuote()->getCustomerValidUntil());
            $response['identity_number'] = $this->getQuote()->getCustomerIdNumber();
            $response['identity_type'] = $this->getIdType($this->getQuote()->getCustomerIdType());
            $response['nationality'] = $this->getQuote()->getCustomerIssuingCountry();
        }

        return $response;
    }

    /**
     * Get the mapped Privacy details of the quote
     *
     * @return array
     */
    protected function getPrivacyDetails()
    {
        return [
            'marketing' => (bool) $this->getQuote()->getPrivacyEmail(),
            'terms_agreed' => (bool) $this->getQuote()->getContractAccepted(),
            'number_info' => (bool) $this->getQuote()->getPrivacyNumberInformation()
        ];
    }

    /**
     * Get the mapped payment details
     *
     * @return array
     */
    protected function getPaymentDetails()
    {
        return [
            'account_name' => $this->getQuote()->getCustomerAccountHolder(),
            'account_nr' => $this->getQuote()->getCustomerAccountNumber()
        ];
    }

    /**
     * Get the quote number porting details
     *
     * @return array
     */
    protected function getNumberPortingDetails()
    {
        $response = array();
        $iteratorPackageId = null;
        $packageItems = '';
        $packageCollection = $this->getQuote()->getPackages();
        foreach ($packageCollection as $packageId => $package) {
            if ($iteratorPackageId !== $packageId) {
                $packageItems = '';
                $iteratorPackageId = $packageId;
                foreach ($package['items'] as $quoteItem) {
                    $packageItems .= $quoteItem->getName() . ', ';
                }
            }
            $mobileNumberPorting = array();
            $mobileNumberPorting['customer_requests_porting'] = (bool) ($package['current_number']);
            $mobileNumberPorting['package_id'] = $packageId;
            $mobileNumberPorting['package_is_portable'] = false;
            $mobileNumberPorting['fulfilment_status']['porting_date'] = $package['actual_porting_date'];
            $mobileNumberPorting['fulfilment_status']['porting_status'] = $package['porting_status'];
            $mobileNumberPorting['product_name'] = rtrim($packageItems, ', ');
            $mobileNumberPorting['porting_data']['contract_number'] = isset($package['number_porting_client_id']) ? $package['number_porting_client_id'] : null;
            $mobileNumberPorting['porting_data']['current_msisdn'] = $package['current_number'];
            $mobileNumberPorting['porting_data']['validation_type'] = $package['number_porting_validation_type'];

            foreach ($package['items'] as $quoteItem) {
                if ($quoteItem->getProduct()->isSubscription()) {
                    $mobileNumberPorting['package_is_portable'] = true;
                }
            }
            $response[] = $mobileNumberPorting;
        }
        return $response;
    }

    /**
     * Quote fullfilment details are always empty on quote level
     *
     * @return array
     */
    protected function getFullfilmentDetails()
    {
        return [
            'decline_reason' => null,
            'delivery_date' => null,
            'delivery_service' => null,
            'delivery_time' => null,
            'status' => null,
            'track_n_trace_url' => null
        ];
    }

    /**
     * Get the quote package details
     *
     * @return array
     * @todo Cannot be tested, waiting for database migration
     */
    protected function getPackageDetails()
    {
        $response = array();
        $taxHelper = Mage::helper('tax');
        $store = Mage::app()->getStore();
        $packageCollection = $this->getQuote()->getPackages();

        foreach ($packageCollection as $packageId => $package) {
            $data = array();
            // init empty vars
            $data['salestype'] = $this->getHawaiiSaleType($package['sale_type']);
            $data['ctn'] = null;
            $data['package_id'] = $packageId;
            $data['quantity'] = 1;
            $data['category'] = $package['type'];
            $data['device_id'] = null;
            $data['device_name'] = null;
            $data['device_sku'] = null;
            $data['subscription_id'] = null;
            $data['subscription_name'] = null;
            $data['subscription_sku'] = null;
            $data['subscription_data'] = null;
            $data['subscription_duration'] = null;
            $data['subscription_minutes'] = null;
            $data['subscription_sms'] = null;
            $data['devicerc_id'] = null;
            $data['devicerc_name'] = null;
            $data['devicerc_sku'] = null;
            $data['sim_id'] = null;
            $data['sim_name'] = null;
            $data['sim_sku'] = null;
            $data['sim_number'] = null;
            $data['extras']['accessories'] = array();
            $data['extras']['addons'] = array();
            $data['voucher'] = $package['coupon'];

            // Empty nodes initial
            $data['package_price']['price_once']['subscription']['promo_price']['amount_vat'] = 0;
            $data['package_price']['price_once']['subscription']['promo_price']['price_excl_vat'] = 0;
            $data['package_price']['price_once']['subscription']['promo_price']['price_incl_vat'] = 0;
            $data['package_price']['price_once']['device']['promo_price']['amount_vat'] = 0;
            $data['package_price']['price_once']['device']['promo_price']['price_excl_vat'] = 0;
            $data['package_price']['price_once']['device']['promo_price']['price_incl_vat'] = 0;
            $data['package_price']['price_once']['device']['thuisheffing']['amount_vat'] = 0;
            $data['package_price']['price_once']['device']['thuisheffing']['price_excl_vat'] = 0;
            $data['package_price']['price_once']['device']['thuisheffing']['price_incl_vat'] = 0;
            $data['package_price']['price_once']['total']['promo_price']['amount_vat'] = 0;
            $data['package_price']['price_once']['total']['promo_price']['price_excl_vat'] = 0;
            $data['package_price']['price_once']['total']['promo_price']['price_incl_vat'] = 0;

            $data['package_price']['price_recurring']['subscription']['promo_price']['amount_vat'] = 0;
            $data['package_price']['price_recurring']['subscription']['promo_price']['price_excl_vat'] = 0;
            $data['package_price']['price_recurring']['subscription']['promo_price']['price_incl_vat'] = 0;
            $data['package_price']['price_recurring']['device']['promo_price']['amount_vat'] = 0;
            $data['package_price']['price_recurring']['device']['promo_price']['price_excl_vat'] = 0;
            $data['package_price']['price_recurring']['device']['promo_price']['price_incl_vat'] = 0;
            $data['package_price']['price_recurring']['total']['promo_price']['amount_vat'] = 0;
            $data['package_price']['price_recurring']['total']['promo_price']['price_excl_vat'] = 0;
            $data['package_price']['price_recurring']['total']['promo_price']['price_incl_vat'] = 0;

            $quoteItemCollection = Mage::getModel('sales/quote_item')->getCollection()->addFieldToFilter('package_id', $packageId)->setQuote($this->getQuote());
            foreach ($quoteItemCollection as $quoteItem) {
                $promoProductCollection = [];
                foreach (explode(',',$quoteItem->getAppliedRuleIds()) as $ruleId) {
                	$ruleModel = Mage::getModel('salesrule/rule');
                	$rule = Mage::helper('vznl_utility')->loadModelById($ruleModel,$ruleId);
                   // $rule = Mage::getModel('salesrule/rule')->load($ruleId);
                    $promoProduct = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId());
                    $prdId = $promoProduct->getIdBySku($rule->getPromoSku());
                    $promoProduct = Mage::helper('vznl_utility')->loadModelById($promoProduct,$prdId);
                 //   $promoProduct = $promoProduct->load($promoProduct->getIdBySku($rule->getPromoSku()));
                    $promoProductCollection[] = $promoProduct;
                }
                $product = Mage::getModel('catalog/product')->load((int) $quoteItem->getProductId());
                $totals = $quoteItem->calculatePricesForDiscount();

                $data['quantity'] = 1;
                $data['ctn'] = $package['ctn'];

                // Device
                if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE), $product)) {
                    $data['category'] = $product->getAttributeText('identifier_device_post_prepaid');
                    $data['device_id'] = $product->getData('identifier_hawaii_id');
                    $data['device_name'] = $product->getName();
                    $data['device_sku'] = $product->getSku();

                    $data['package_price']['price_once']['device']['promo_price']['amount_vat'] += $totals['price_after_discount'] - $taxHelper->getPrice($quoteItem->getProduct(), $totals['price_after_discount'], false);
                    $data['package_price']['price_once']['device']['promo_price']['price_excl_vat'] += $taxHelper->getPrice($quoteItem->getProduct(), $totals['price_after_discount'], false);
                    $data['package_price']['price_once']['device']['promo_price']['price_incl_vat'] += $totals['price_after_discount'];
                }

                // Thuiskopieheffing
                if (in_array($quoteItem->getProduct()->getSku(), $this->getThuiskopieheffingProductSkus())){
                    $data['package_price']['price_once']['device']['thuisheffing']['amount_vat'] += Mage::helper('core')->currency($quoteItem->getTaxAmount(), false, true);
                    $data['package_price']['price_once']['device']['thuisheffing']['price_excl_vat'] += Mage::helper('core')->currency($quoteItem->getPrice(), false, true);
                    $data['package_price']['price_once']['device']['thuisheffing']['price_incl_vat'] += Mage::helper('core')->currency($quoteItem->getPriceInclTax(), false, true);
                }

                // Subscription
                if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $product)) {
                    $data['subscription_id'] = $product->getData('identifier_hawaii_id');
                    $data['subscription_name'] = $product->getName();
                    $data['subscription_sku'] = $product->getSku();
                    $data['subscription_data'] = $product->getProdspecsDataAantalMb();
                    $data['subscription_duration'] = $product->getAttributeText('identifier_commitment_months') ?? null;
                    $data['subscription_minutes'] = $product->getProdspecsAantalBelminuten();
                    $data['subscription_sms'] = $product->getProdspecsSmsAmount();

                    $priceMaf = $quoteItem->getMafInclTax() - $quoteItem->getMafDiscountAmount();
                    $priceStart = $store->roundPrice($store->convertPrice($priceMaf));
                    $price = $taxHelper->getPrice($product, $priceStart, false);
                    $priceInludingTax = $taxHelper->getPrice($product, $priceStart, true);
                    $data['package_price']['price_recurring']['subscription']['promo_price']['amount_vat'] += ($priceInludingTax - $price);
                    $data['package_price']['price_recurring']['subscription']['promo_price']['price_excl_vat'] += $price;
                    $data['package_price']['price_recurring']['subscription']['promo_price']['price_incl_vat'] += $priceInludingTax;

                    $priceStart = $store->roundPrice($store->convertPrice($quoteItem->getConnectionCost()));
                    $price = $taxHelper->getPrice($product, $priceStart, false);
                    $priceInludingTax = $taxHelper->getPrice($product, $priceStart, true);
                    $data['package_price']['price_once']['subscription']['connection_price']['amount_vat'] += $priceInludingTax - $price;
                    $data['package_price']['price_once']['subscription']['connection_price']['price_excl_vat'] += $price;
                    $data['package_price']['price_once']['subscription']['connection_price']['price_incl_vat'] += $priceInludingTax;
                }
                // SIM
                if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD), $product)) {
                    $data['sim_id'] = $product->getData('identifier_hawaii_id');
                    $data['sim_name'] = $product->getName();
                    $data['sim_sku'] = $product->getSku();
                    $data['sim_number'] = null; // @todo - Fill sim with current simcard number for ILS

                    $priceStart = $store->roundPrice($store->convertPrice($quoteItem->getFinalPrice()));
                    $price = $taxHelper->getPrice($product, $priceStart, false);
                    $priceInludingTax = $taxHelper->getPrice($product, $priceStart, true);
                    $data['package_price']['price_once']['device']['promo_price']['amount_vat'] += $priceInludingTax - $price;
                    $data['package_price']['price_once']['device']['promo_price']['price_excl_vat'] += $price;
                    $data['package_price']['price_once']['device']['promo_price']['price_incl_vat'] += $priceInludingTax;
                }

                // Device RC
                if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION), $product)) {
                    $data['devicerc_id'] = $product->getData('identifier_hawaii_id');
                    $data['devicerc_name'] = $product->getName();
                    $data['devicerc_sku'] = $product->getSku();

                    $data['package_price']['price_recurring']['device']['promo_price']['amount_vat'] += $totals['maf_after_discount'] - $totals['maf_after_discount_excl_tax'];
                    $data['package_price']['price_recurring']['device']['promo_price']['price_excl_vat'] += $totals['maf_after_discount_excl_tax'];
                    $data['package_price']['price_recurring']['device']['promo_price']['price_incl_vat'] += $totals['maf_after_discount'];
                    $data['package_price']['price_recurring']['total']['promo_price']['amount_vat'] = $data['package_price']['price_recurring']['subscription']['promo_price']['amount_vat'] + $data['package_price']['price_recurring']['device']['promo_price']['amount_vat'];
                    $data['package_price']['price_recurring']['total']['promo_price']['price_excl_vat'] = $data['package_price']['price_recurring']['subscription']['promo_price']['price_excl_vat'] + $data['package_price']['price_recurring']['device']['promo_price']['price_excl_vat'];
                    $data['package_price']['price_recurring']['total']['promo_price']['price_incl_vat'] = $data['package_price']['price_recurring']['subscription']['promo_price']['price_incl_vat'] + $data['package_price']['price_recurring']['device']['promo_price']['price_incl_vat'];
                }
                // Addon
                if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_ADDON), $product)) {
                    $addon['id'] = $product->getIdentifierHawaiiId() ?? 'None';
                    $addon['sku'] = $product->getSku();
                    $addon['name'] = $product->getName();
                    $addon['item_price']['has_promo'] = false;
                    if ($addon['item_price']['has_promo']) {
                        $addon['item_price']['discount']['price_excl_vat'] = 0;
                        $addon['item_price']['discount']['amount_vat'] = 0;
                        $addon['item_price']['discount']['price_incl_vat'] = 0;
                        $addon['item_price']['promo_price']['price_excl_vat'] = 0;
                        $addon['item_price']['promo_price']['amount_vat'] = 0;
                        $addon['item_price']['promo_price']['price_incl_vat'] = 0;
                    }
                    $priceStart = $store->roundPrice($store->convertPrice($product->getMaf()));
                    $price = $taxHelper->getPrice($product, $priceStart, false);
                    $priceInludingTax = $taxHelper->getPrice($product, $priceStart, true);
                    $addon['item_price']['normal_price']['amount_vat'] = $priceInludingTax - $price;
                    $addon['item_price']['normal_price']['price_excl_vat'] = $price;
                    $addon['item_price']['normal_price']['price_incl_vat'] = $priceInludingTax;
                    $data['extras']['addons'][] = $addon;
                }
                // Accessory
                if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY), $product)) {
                    // OMNVFNL-9437: dont send if attribute_set = service_item and package_subtype_id = Accessory
                    if ($product->isServiceItem()) continue;

                    $accessory['id'] = $product->getIdentifierHawaiiId();
                    $accessory['sku'] = $product->getSku();
                    $accessory['name'] = $product->getName();

                    $priceStart = $store->roundPrice($store->convertPrice($product->getPrice()));
                    $price = $taxHelper->getPrice($product, $priceStart, false);
                    $priceInludingTax = $taxHelper->getPrice($product, $priceStart, true);
                    $accessory['item_price']['normal_price']['amount_vat'] = $priceInludingTax - $price;
                    $accessory['item_price']['normal_price']['price_excl_vat'] = $price;
                    $accessory['item_price']['normal_price']['price_incl_vat'] = $priceInludingTax;

                    $accessory['item_price']['has_promo'] = $product->getSpecialPrice() ? true : false;
                    if ($accessory['item_price']['has_promo']) {
                        $priceStart = $store->roundPrice($store->convertPrice($product->getSpecialPrice()));
                        $price = $taxHelper->getPrice($product, $priceStart, false);
                        $priceInludingTax = $taxHelper->getPrice($product, $priceStart, true);
                        $accessory['item_price']['promo_price']['price_excl_vat'] = $price;
                        $accessory['item_price']['promo_price']['amount_vat'] = $priceInludingTax - $price;
                        $accessory['item_price']['promo_price']['price_incl_vat'] = $priceInludingTax;

                        $accessory['item_price']['discount']['price_excl_vat'] = $accessory['item_price']['normal_price']['price_excl_vat'] - $accessory['item_price']['promo_price']['price_excl_vat'];
                        $accessory['item_price']['discount']['amount_vat'] = $accessory['item_price']['normal_price']['amount_vat'] - $accessory['item_price']['promo_price']['amount_vat'];
                        $accessory['item_price']['discount']['price_incl_vat'] = $accessory['item_price']['normal_price']['price_incl_vat'] - $accessory['item_price']['promo_price']['price_incl_vat'];
                    }
                    $data['extras']['accessories'][] = $accessory;
                }
            }

            // Package total prices
            $packageTotals = Mage::helper('dyna_configurator')->getActivePackageTotals($packageId, true);
            $packageTotalsEx = Mage::helper('dyna_configurator')->getActivePackageTotals($packageId, false);
            $data['package_price']['price_once']['total']['promo_price']['amount_vat'] = $packageTotals['totalprice'] - $packageTotalsEx['totalprice'];
            $data['package_price']['price_once']['total']['promo_price']['price_excl_vat'] = $packageTotalsEx['totalprice'];
            $data['package_price']['price_once']['total']['promo_price']['price_incl_vat'] = $packageTotals['totalprice'];
            $data['package_price']['price_recurring']['total']['promo_price']['amount_vat'] = $data['package_price']['price_recurring']['subscription']['promo_price']['amount_vat'] + $data['package_price']['price_recurring']['device']['promo_price']['amount_vat'];
            $data['package_price']['price_recurring']['total']['promo_price']['price_excl_vat'] = $data['package_price']['price_recurring']['subscription']['promo_price']['price_excl_vat'] + $data['package_price']['price_recurring']['device']['promo_price']['price_excl_vat'];
            $data['package_price']['price_recurring']['total']['promo_price']['price_incl_vat'] = $data['package_price']['price_recurring']['subscription']['promo_price']['price_incl_vat'] + $data['package_price']['price_recurring']['device']['promo_price']['price_incl_vat'];
            $response[] = $data;
        }
        return $response;
    }

    /**
     * Get the quote total prices
     *
     * @return array
     */
    protected function getTotalPriceDetails()
    {
        $response = array();
        $taxHelper = Mage::helper('tax');
        $store = Mage::app()->getStore();
        $packageCollection = $this->getQuote()->getPackages();

        $totals = array();
        $totalsEx = array();
        $totals['totalprice'] = $totals['totalmaf'] = $totals['totalconnection'] = 0;
        $totalsEx['totalprice'] = $totalsEx['totalmaf'] = $totalsEx['totalconnection'] = 0;

        foreach ($packageCollection as $packageId => $package) {
            foreach ($package['items'] as $quoteItem) {
                // Needed since aansluitenkosten are not part of the quote prices
                $totals['totalconnection'] += $quoteItem->getConnectionCost();
                $totalsEx['totalconnection'] += $taxHelper->getPrice($quoteItem->getProduct(), $quoteItem->getConnectionCost(), false);
            }
            $data = Mage::helper('dyna_configurator')->getActivePackageTotals($packageId, true);
            $totals['totalmaf'] = $totals['totalmaf'] + $data['totalmaf'];
            $totals['totalprice'] = $totals['totalprice'] + $data['totalprice'];

            $data = Mage::helper('dyna_configurator')->getActivePackageTotals($packageId, false);
            $totalsEx['totalmaf'] = $totalsEx['totalmaf'] + $data['totalmaf'];
            $totalsEx['totalprice'] = $totalsEx['totalprice'] + $data['totalprice'];
        }
        $price = $store->roundPrice($store->convertPrice($totalsEx['totalconnection']));
        $priceInludingTax = $store->roundPrice($store->convertPrice($totals['totalconnection']));
        $response['price_once']['connection_price']['amount_vat'] = $priceInludingTax - $price;
        $response['price_once']['connection_price']['price_excl_vat'] = $price;
        $response['price_once']['connection_price']['price_incl_vat'] = $priceInludingTax;

        $price = $store->roundPrice($store->convertPrice($totalsEx['totalprice']));
        $priceInludingTax = $store->roundPrice($store->convertPrice($totals['totalprice']));
        $response['price_once']['devices']['amount_vat'] = $priceInludingTax - $price;
        $response['price_once']['devices']['price_excl_vat'] = $price;
        $response['price_once']['devices']['price_incl_vat'] = $priceInludingTax;

        $price = 0;
        $priceInludingTax = 0;
        if ($this->response['payment_method'] === 'cashondelivery' && $this->response['delivery_options']['selected_delivery_option'] === 'home_delivery') {
            $price = $store->roundPrice($store->convertPrice($totalsEx['totalprice']));
            $priceInludingTax = $store->roundPrice($store->convertPrice($totals['totalprice']));
        }
        $response['price_once']['delivery_costs']['amount_vat'] = $priceInludingTax - $price;
        $response['price_once']['delivery_costs']['price_excl_vat'] = $price;
        $response['price_once']['delivery_costs']['price_incl_vat'] = $priceInludingTax;

        $price = $store->roundPrice($store->convertPrice($totalsEx['totalmaf']));
        $priceInludingTax = $store->roundPrice($store->convertPrice($totals['totalmaf']));
        $response['price_recurring']['amount_vat'] = $priceInludingTax - $price;
        $response['price_recurring']['price_excl_vat'] = $price;
        $response['price_recurring']['price_incl_vat'] = $priceInludingTax;
        return $response;
    }

    /**
     * Get the delivery details
     *
     * @return array
     */
    protected function getDeliveryDetails()
    {
        $response = array();
        $deliveryType = $this->_checkIfDeliveryInShop();
        $deliveryTypeData = array_shift($deliveryType);
        $shippingAddress = $this->getShippingAddress();
        if ($deliveryTypeData['type'] == 'store'){
            $dealer = Mage::getModel('agent/dealer')->load($this->getQuote()->getDealerId());
            $response['selected_delivery_option'] = 'pick_up_in_shop';
            $response['home_delivery']['address']['city'] = null;
            $response['home_delivery']['address']['country'] = null;
            $response['home_delivery']['address']['street'] = null;
            $response['home_delivery']['address']['house_number'] = null;
            $response['home_delivery']['address']['house_number_addition'] = null;
            $response['home_delivery']['address']['postal_code'] = null;
            $response['home_delivery']['address_same_as_invoice'] = false;
            $response['home_delivery']['price'] = null;
            $response['pick_up_in_shop']['city'] = $shippingAddress['address']['city'];
            $response['pick_up_in_shop']['dealer_code'] = $dealer->getVfDealerCode();
            $response['pick_up_in_shop']['street'] = implode(' ', $shippingAddress['address']['street']);
            $response['pick_up_in_shop']['postal_code'] = $shippingAddress['address']['postcode'];
            $response['pick_up_in_shop']['price'] = 0;
        } else {
            $response['selected_delivery_option'] = 'home_delivery';
            $response['home_delivery']['address']['city'] = $shippingAddress['address']['city'];
            $response['home_delivery']['address']['country'] = 'NLD'; // Homedelivery is only in NL
            $response['home_delivery']['address']['street'] = $shippingAddress['address']['street'][0];
            $response['home_delivery']['address']['house_number'] = $shippingAddress['address']['street'][1];
            $response['home_delivery']['address']['house_number_addition'] = $shippingAddress['address']['street'][2];
            $response['home_delivery']['address']['postal_code'] = $shippingAddress['address']['postcode'];
            $response['home_delivery']['address_same_as_invoice'] = $shippingAddress['address']['address'] === 'other_address' ? false : true;
            $response['home_delivery']['price'] = 0;
            $response['pick_up_in_shop']['city'] = null;
            $response['pick_up_in_shop']['dealer_code'] = null;
            $response['pick_up_in_shop']['street'] = null;
            $response['pick_up_in_shop']['postal_code'] = null;
            $response['pick_up_in_shop']['price'] = null;
        }
        return $response;
    }

    protected function getDealerCode()
    {
        $dealer = Mage::getModel('agent/dealer')->load($this->getQuote()->getDealerId());

        if ($dealer->getId()) {
            return $dealer->getVfDealerCode();
        }

        return null;
    }

    /**
     * Process the HTTP request.
     *
     * @return array
     */
    public function process()
    {
        $this->response['errors'] = array();

        if (!is_null($this->getQuote()->getId())) {
            $this->response['business_details'] = $this->getBusinessDetails();
            $this->response['chosen_order_type_is_business'] = $this->getQuote()->getCompanyCoc() ? true : false;
            $this->response['is_business'] = (bool) $this->getQuote()->getCustomerIsBusiness();
            $this->response['contracting_party'] = $this->getCustomerDetails();
            $this->response['identity'] = $this->getIdentityDetails();
            $this->response['privacy'] = $this->getPrivacyDetails();
            $this->response['payment_details'] = $this->getPaymentDetails();

            $data = Mage::helper('core')->jsonDecode($this->getQuote()->getShippingData());
            $this->response['payment_method'] = $data['payment'];

            $this->response['payment_result'] = false; // quote is always false since no payment is made yet
            $this->response['dealer_code'] = $this->getDealerCode();
            $this->response['delivery_options'] = $this->getDeliveryDetails();
            $this->response['mobile_number_porting'] = $this->getNumberPortingDetails();
            $this->response['products'] = $this->getPackageDetails();
            $this->response['nr_products'] = count($this->response['products']);
            $this->response['total_price'] = $this->getTotalPriceDetails();
            $this->response['order_fulfilment_status'] = $this->getFullfilmentDetails();

            $this->response['dynalean_order_id'] = null;
            $this->response['adyen_payment_id'] = null;
            $this->response['dynalean_quote_id'] = (int) $this->getQuote()->getId();
            $this->response['shopping_order_id'] = $this->getQuote()->getHawaiiShoppingOrderId();
            $this->response['version_id'] = (int) $this->getQuote()->getHawaiiVersionId();

            // Errors
            foreach ($this->getErrors() as $error) {
                $key = str_replace('%INDEX%', $error['index'], $error['key']);
                $this->response['errors'][] = array("item_key" => $key, "message" => $error['message']);
            }
        } else {
            $this->response['errors'][] = array('item_key' => 'request', 'message' => 'Quote not found');
        }
        return $this->response;
    }
}
