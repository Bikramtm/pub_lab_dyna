<?php

class Vznl_RestApi_Quotes_Shops
{
    /**
     * @var int
     */
    protected $quoteId;

    /**
     * @var Vznl_Checkout_Model_Sales_Quote
     */
    protected $quote;
    
    /**
     * @param Mage_Core_Controller_Request_Http|null $request
     */
    public function __construct($request = null)
    {
        if (!is_null($request)) {
            $routeParts = explode('/', $request->getQuery('route'));
            $this->quoteId = $routeParts[2];
        }
    }
        
    /**
     * Process the HTTP request.
     * 
     * @return array
     * @todo Check why shops is just returning null for everything.
     */
    public function process()
    {
        if (!$this->quoteWasFound()) {
            return [
                'errors' => [
                    [
                        'item_key' => 'request',
                        'message' => 'Quote not found',
                    ]
                ]
            ];
        }

        // Return empty shops array when quote does not have any products linked to it
        if (!$this->getProducts()) {
            return [
                'shops' => [],
            ];
        }

        return $this->getShops();
    }

    /**
     * Check whether the quote exists.
     * 
     * @return bool
     */
    private function quoteWasFound()
    {
        return (bool) $this->getQuote()->getId();
    }

    /**
     * @return array
     */
    private function getShops()
    {
        $productCollection = $this->getProducts();
        $stockCollection = $this->getStock($productCollection);
        $availableStores = $this->getAvailableStores($stockCollection);
        $shops = array();

        // Set shops
        foreach ($availableStores as $store) {
            $dealer = Mage::getModel('agent/dealer')
                ->load($store->store, 'axi_store_code');

            // Map only when dealer was found
            if ($dealer->getId()) {
                $shops[] = $this->mapStoreData($dealer, $store);
            }
        }

        return [
            'shops' => $shops,
        ];
    }

    /**
     * Get the products.
     * 
     * @return array
     */
    private function getProducts()
    {
        $products = [];

        foreach ($this->getQuote()->getAllItems() as $quoteItem) {
            /**
             * @var Vznl_Catalog_Model_Product
             */
            $product = $quoteItem->getProduct();

            if ($product->getType() !== 'subscription') {
                $products[] = $product;
            }
        }

        return $products;
    }

    /**
     * Get the live stock information for products.
     * 
     * @param  array[Vznl_Catalog_Model_Product] $products
     * @return mixed
     */
    private function getStock($products)
    {
        return Mage::getModel('stock/stock')
            ->getLiveStockForItem($products, 500, false);
    }

    /**
     * @param  array $stockCollection
     * @return array
     */
    private function getAvailableStores($stockCollection)
    {
        $availableStores = array();

        foreach ($stockCollection as $stock) {
            $storeCollection = $stock->getData('other_store_list');

            foreach ($storeCollection as $store) {
                $storeId = $store['store'];

                if ($store['stock'] > 0) {
                    $data = new stdClass();
                    $data->store = $storeId;
                    $data->store_name = $store['store_name'];
                    $availableStores[$storeId] = $data;
                } elseif (isset($availableStores[$storeId])) {
                    unset($availableStores[$storeId]);
                }
            }
        }
        
        return $availableStores;
    }

    /**
     * Map dealer and available store data into store data array.
     *
     * @param Vznl_Agent_Model_Dealer $dealer
     * @param stdClass $store
     * @return array
     */
    private function mapStoreData($dealer, $store)
    {
        return [
            'street' => $dealer->getData('street'),
            'postcode' => $dealer->getData('postcode'),
            'city' => $dealer->getData('city'),
            'house_nr' => $dealer->getData('house_nr'),
            'dealercode' => $dealer->getData('vf_dealer_code'),
            'telephone' => $dealer->getData('telephone'),
            'name' => $store->store_name
        ];
    }

    /**
     * @return int
     */
    public function getQuoteId()
    {
        return (int) $this->quoteId;
    }
    
    /**
     * Get the quote model.
     * 
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        if (is_null($this->quote)) {
            $this->quote = Mage::getModel('sales/quote')->load($this->getQuoteId());
        }

        return $this->quote;
    }
}