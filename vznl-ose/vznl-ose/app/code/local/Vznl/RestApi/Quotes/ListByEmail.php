<?php

class Vznl_RestApi_Quotes_ListByEmail extends Vznl_RestApi_Abstract
{
    /**
     * @var Mage_Sales_Model_Resource_Quote_Collection
     */
    private $quoteCollection;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct($request = null)
    {
        $routeParts = explode('/', $request->getQuery('route'));
        $email = trim($routeParts[2]);
        $this->quoteCollection = $this->getQuoteCollection($email);
    }

    /**
     * @param  string $email
     * @return Mage_Sales_Model_Resource_Quote_Collection
     */
    private function getQuoteCollection($email)
    {
        return Mage::getModel('sales/quote')
            ->getCollection()
            ->addFieldToFilter('customer_email', $email);
    }
    
    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        $response = array();

        foreach ($this->quoteCollection as $quote) {
            $response[] = [
                'dynalean_quote_id' => $quote->getId(),
                'created_at' => $this->getTimestampFromDate($quote->getCreatedAt()),
                'updated_at' => $this->getTimestampFromDate($quote->getUpdatedAt())
            ];
        }

        return $response;
    }
}