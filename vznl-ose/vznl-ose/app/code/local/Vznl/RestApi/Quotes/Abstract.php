<?php
/**
 * Stuff that is copied from the checkout process. This needs to be refactored
 * to a more generic way of working since we have 2 codebases now that do the same
 */
abstract class Vznl_RestApi_Quotes_Abstract extends Vznl_RestApi_Abstract
{
    const DEFAULT_EMAIL_SUFFIX = '@ssfe.vodafone.com';

    /**
     * @var bool
     */
    protected $hasSubscription = false;

    /**
     * Get the current cart.
     *
     * @return vznl_Checkout_Model_Cart
     */
    protected function getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * Get the current checkout session.
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Render all the privacy settings.
     *
     * @param array $data
     * @return array
     */
    protected function convertPrivacy($data)
    {
        $result = array();

        foreach ($data as $item => $value) {
            $result['privacy_' . $item] = $value;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function _getCustomerDataFields()
    {
        return array(
            'firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'middlename' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'gender' => array(
                'required' => true,
                'validation' => array(
                    'validateIsNumber'
                )
            ),
            'dob' => array(
                'required' => true,
                'validation' => array(
                    'validateMaxAge',
                    'validateMinAge',
                    'validateIsDate'
                )
            ),
            'id_type' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array()
            ),
            'id_number' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array()
            ),
            'valid_until' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateFutureDate',
                    'validateIsDate'
                )
            ),
            'issuing_country' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateCountryCode',
                )
            ),
        );
    }

    /**
     * @return array
     */
    public function _getBusinessDataFields()
    {
        return array(
            'company_vat_id' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'company_coc' => array(
                'required' => true,
                'validation' => array(
                    'validateName',
                    'vaiidateCoc',
                )
            ),
            'company_name' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'company_date' => array(
                'required' => true,
                'validation' => array(
                    'validatePastDate',
                    'validateIsDate'
                )
            ),
            'company_legal_form' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),

            'contractant_gender' => array(
                'required' => true,
                'validation' => array(
                    'validateIsNumber'
                )
            ),
            'contractant_firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_middlename' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_dob' => array(
                'required' => true,
                'validation' => array(
                    'validateMaxAge',
                    'validateMinAge',
                    'validateIsDate'
                )
            ),
            'contractant_id_type' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateName',
                )
            ),
            'contractant_id_number' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array()
            ),
            'contractant_valid_until' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateFutureDate',
                    'validateIsDate'
                )
            ),
            'contractant_issuing_country' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateCountryCode',
                )
            ),
        );
    }

    /**
     * @param array $data
     * @param Vznl_Customer_Model_Customer_Customer $customer
     * @return bool
     */
    public function saveCustomer(array $data, Vznl_Customer_Model_Customer_Customer $customer)
    {
        $checkoutHelper = Mage::helper('vznl_checkout');

        // Check if customer is already logged in
        if (!$customer->getId()) {
            $customerData = $data['customer'];
            $additionalData = $data['additional'];

            //set prefix based on gender
            if (isset($customerData['gender'])){
                switch($customerData['gender']){
                    case 2: $customerData['prefix'] = 'Mrs.';
                        break;
                    case 1: $customerData['prefix'] = 'Mr.';
                        break;
                    default: $customerData['prefix'] = '';
                        break;
                }
            }

            $addressData = $data['address'];

            // Convert from frontend data to magento format
            $customerData['is_business'] = (int) isset($customerData['type']) && $customerData['type'] == Vznl_Customer_Model_Customer_Customer::TYPE_BUSINESS;

            // Check if the credit failed button was clicked based on type of customer
            if ($customerData['is_business']) {
                if (isset($customerData['business_suspect_fraud'])) {
                    $customerData['suspect_fraud'] = $customerData['business_suspect_fraud'];
                    unset($customerData['business_suspect_fraud']);
                }
            }

            // Default country code is netherlands, unless a foreign address is chosen
            $customerData['country_id'] = Vznl_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;
            if ($customerData['is_business']) {
                // Clear the private fields if business
                // foreach ($this->_getCustomerDataFields() as $field => $data) {
                //     $customerData[$field] = null;
                // }
                $customerData['company'] = $customerData['company_name'];
                $customerData['vat_id'] = $customerData['company_vat_id'];
                $customerData['firstname'] = $customerData['contractant_firstname'];
                $customerData['lastname'] = $customerData['contractant_lastname'];
                $customerData['middlename'] = $customerData['contractant_middlename'];
                $customerData['gender'] = $customerData['contractant_gender'];
                $customerData['dob'] = $customerData['contractant_dob'];
            } else {
                // Clear the business fields if private
                foreach ($this->_getBusinessDataFields() as $field => $data) {
                    $customerData[$field] = null;
                }

                $customerData['company_address'] = null;
                $customerData['foreignAddress'] = null;
                $customerData['otherAddress'] = null;
            }

            $customerData['additional_email'] = join(';', array_filter($additionalData['email'], 'strlen'));
            $customerData['additional_fax'] = join(';', array_filter($additionalData['fax'], 'strlen'));
            $customerData['additional_telephone'] = join(';', array_filter($additionalData['telephone'], 'strlen'));
            $customerData['ziggo_telephone'] = isset($additionalData['ziggo_telephone']) ? $additionalData['ziggo_telephone'] : '';
            $customerData['ziggo_email'] = isset($additionalData['ziggo_email']) ? $additionalData['ziggo_email'] : '';

            // By default set the shipping address same as billing
            $customerData['use_for_shipping'] = 1;

            $errors = $this->_validateCustomerAdditionaldata($additionalData);
            $errors += $this->_validateCustomerData($customerData);
            $errors += $this->_validateBillingAddressData($addressData + $customerData);
            if ($errors) {
                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
            }
            if ($customerData['is_business']) {
                $customerData += $this->_getBusinessAddressByType($customerData);
            }
            $addressData += $this->_getAddressByType($addressData);
            $checkCustomer = 'invalid';
            while ($checkCustomer != null){
                $email = microtime(true).uniqid(true) . self::DEFAULT_EMAIL_SUFFIX;
                $checkCustomer = Mage::getModel('customer/customer')->loadByEmail($email)->getId();
            }
            // Customer email address is an uniqid now
            $addressData['email'] = $email;

            $addressData['street'] = is_array($addressData['street']) ? $addressData['street'] : array($addressData['street']);
            array_push($addressData['street'], $addressData['houseno']);
            array_push($addressData['street'], $addressData['addition']);

            if (isset($addressData['extra_street'])) {
                foreach ($addressData['extra_street'] as $streetLine) {
                    array_push($addressData['street'], $streetLine);
                }
            }

            // Update basic customer data that validators will throw errors if missing
            $this->updateCustomerMissingValues($customerData);

            // Convert the IBAN in case it is in an older form
            // TODO : Not anymore until the external calls are available
            //$addressData['account_no'] = Mage::helper('vznl_validators/data')->validateBicIban($addressData['account_no']);
            // Save all customer and billing address information to the quote
            try {
                $result = $this->getOnepage()->saveBilling($addressData + $customerData, null);
            } catch (Exception $e) {
                 throw new Exception(Mage::helper('core')->jsonEncode($e->getMessage()));
            }

            // Suppress errors in case of error-type -1
            if (isset($result['error']) && $result['error'] === -1) {
                $result = [];
            }

            if ($result) {
                throw new Exception(Mage::helper('core')->jsonEncode($result));
            }
        } else {
//            return true;
//            // Map fields here
//            $customerData = array(
//                'additional_email' => join(';', array_filter($customer->getCorrespondanceEmail(), 'strlen')),
//                'is_business' => $customer->getIsBusiness(),
//                'type' => $customer->getIsBusiness(),
//                'firstname' => $customer->getFirstname(),
//                'middlename' => $customer->getMiddlename(),
//                'lastname' => $customer->getLastname(),
//                'company_name' => $customer->getCompanyName(),
//                'company_coc' => $customer->getCompanyCoc(),
//                'company_address' => 'other_address',
//                'foreignAddress' => array(),
//                'otherAddress' => array(),
//                'contractant_firstname' => $customer->getFirstname(),
//                'contractant_middlename' => $customer->getMiddlename(),
//                'contractant_lastname' => $customer->getLastname(),
//            );
//            if ($customer->getIsBusiness()){
//                $customerData['otherAddress'] = array(
//                    'company_street' => $checkoutHelper->getTextValue($billingAddress->getStreet()[0]),
//                    'company_house_nr' => $checkoutHelper->getTextValue($billingAddress->getStreet()[1]),
//                    'company_city' => $billingAddress->getCity(),
//                );
//            }
//
//            // Additional info data
//            $addressData = array(
//                'address'=>'other_address',
//                'otherAddress' => array(
//                    'street' => $checkoutHelper->getTextValue($billingAddress->getStreet()[0]),
//                    'houseno' => $checkoutHelper->getTextValue($billingAddress->getStreet()[1]),
//                    'city' => $billingAddress->getCity(),
//                ),
//            );
//
//            // Perform validation
//            $errors = [];
//            $errors += $this->_validateCustomerDataExistingCustomer($customerData);
//            $errors += $this->_validateBillingAddressDataExistingCustomer($addressData + $customerData);
//
//            if ($errors) {
//                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
//            }
            return true;
        }
    }

    private function _validateCustomerAdditionaldata($data) {
        $errors = array();

        // Validate additional email data
        $emails = array_filter($data['email'], 'strlen');
        foreach ($emails as $id => $email) {
            if (!Mage::helper('vznl_validators')->validateEmailSyntax($email)
            || !Mage::helper('vznl_validators')->validateEmailDns($email)) {
                $errors += array('additional[email][' . $id . ']' => 'Invalid e-mail address');
            }
        }
        // Validate additional corresponde_email data
        $email = $data['correspondence_email'];
        if (!Mage::helper('vznl_validators')->validateEmailSyntax($email)
            || !Mage::helper('vznl_validators')->validateEmailDns($email)) {
            $errors += array('additional[correspondence_email][' . $id . ']' => 'Invalid correspondence e-mail address');
        }
        //TODO Validate additional fax
        //TODO Validate additional phone

        return $errors;
    }

    protected function _validateCustomerData($data)
    {
        $errors = array();

        $inputFieldsPrivate = array(
            'firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'middlename' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'gender' => array(
                'required' => true,
                'validation' => array(
                    'validateIsNumber'
                )
            ),
            'dob' => array(
                'required' => true,
                'validation' => array(
                    'validateMaxAge',
                    'validateMinAge',
                )
            ),
            'id_type' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateName',
                )
            ),
            'id_number' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array()
            ),
            'valid_until' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateFutureDate',
                )
            ),
            'issuing_country' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateCountryCode',
                )
            ),
        );
        $inputFieldsBusiness = array(
            'company_vat_id' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'company_coc' => array(
                'required' => true,
                'validation' => array(
                    'validateName',
                    'validateCoc',
                )
            ),
            'company_name' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'company_date' => array(
                'required' => true,
                'validation' => array(
                    'validatePastDate'
                )
            ),
            'company_legal_form' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),

            'contractant_gender' => array(
                'required' => true,
                'validation' => array(
                    'validateIsNumber'
                )
            ),
            'contractant_firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_middlename' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_dob' => array(
                'required' => true,
                'validation' => array(
                    'validateMaxAge',
                    'validateMinAge',
                )
            ),
            'contractant_id_type' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateName',
                )
            ),
            'contractant_id_number' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array()
            ),
            'contractant_valid_until' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateFutureDate',
                )
            ),
            'contractant_issuing_country' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateCountryCode',
                )
            ),
        );

        if ($data['is_business']) {
            $validations = $inputFieldsBusiness;
            switch($data['company_address']) {
                case 'other_address' :
                    $validationsBusiness = array(
                        'company_street' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'company_postcode' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'company_city' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                    );
                    $errors += $this->performValidate($data['otherAddress'], $validationsBusiness, 'customer[otherAddress][', ']');
                    break;
                case 'foreign_address' :
                    $validationsBusiness = array(
                        'company_street' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'company_house_nr' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                        'company_house_nr_addition' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'extra' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'company_city' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                        'company_region' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'company_postcode' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                        'company_country_id' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                    );
                    $errors += $this->performValidate($data['foreignAddress'], $validationsBusiness, 'customer[foreignAddress][', ']');
                    break;
                default :
                    throw new Exception('Missing company address data');
            }
        }
        else
        {
            $validations = $inputFieldsPrivate;
        }
        $errors += $this->performValidate($data, $validations, 'customer[', ']');

        return $errors;
    }

    protected function _validateCustomerDataExistingCustomer($data)
    {
        $errors = array();

        $inputFieldsPrivate = array(
            'firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'middlename' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
        );
        $inputFieldsBusiness = array(
            'company_coc' => array(
                'required' => true,
                'validation' => array(
                    'validateName',
                    'validateCoc',
                )
            ),
            'contractant_firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_middlename' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
        );

        if ($data['is_business']) {
            $validations = $inputFieldsBusiness;
            switch($data['company_address']) {
                case 'other_address' :
                    $validationsBusiness = array(
                        'company_street' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'company_city' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                    );
                    $errors += $this->performValidate($data['otherAddress'], $validationsBusiness, 'customer[otherAddress][', ']');
                    break;
                case 'foreign_address' :
                    $validationsBusiness = array(
                        'company_street' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'company_house_nr' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                        'company_city' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                    );
                    $errors += $this->performValidate($data['foreignAddress'], $validationsBusiness, 'customer[foreignAddress][', ']');
                    break;
                default :
                    throw new Exception('Missing company address data');
            }
        }
        else
        {
            $validations = $inputFieldsPrivate;
        }
        $errors += $this->performValidate($data, $validations, 'customer[', ']');

        return $errors;
    }

    private function _validateBillingAddressData($data)
    {
        $errors = array();
        $validations = $this->hasSubscription ? array(
            'account_no' => array(
                'required' => true,
                'validation' => array(
                    'validateBicIban'
                )
            ),
            'account_holder' => array(
                'required' => true,
                'validation' => array(
                    'validateName',
                )
            ),
            'fax' => array(
                'required' => false,
                'validation' => array(
                    'validateName',
                )
            ),
        )
        : array();

        switch($data['address']) {
            case 'other_address' :
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                            'validateName'
                        )
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNLPostcode'
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->_performValidate($data['otherAddress'], $addressValidations, 'address[otherAddress][', ']');
                break;
            case 'foreign_address' :
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'extra' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'region' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'country_id' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->_performValidate($data['foreignAddress'], $addressValidations, 'address[foreignAddress][', ']');
                break;
            default :
                throw new Exception('Missing company address data');
        }

        $errors += $this->_performValidate($data, $validations, 'address[', ']');

        return $errors;
    }

    private function _validateBillingAddressDataExistingCustomer($data)
    {
        $errors = array();
        $validations = array();

        switch($data['address']) {
            case 'other_address' :
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                            'validateName'
                        )
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->_performValidate($data['otherAddress'], $addressValidations, 'address[otherAddress][', ']');
                break;
            case 'foreign_address' :
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->_performValidate($data['foreignAddress'], $addressValidations, 'address[foreignAddress][', ']');
                break;
            default :
                throw new Exception('Missing company address data');
        }

        $errors += $this->_performValidate($data, $validations, 'address[', ']');

        return $errors;
    }

    /**
     * @param array $data
     * @param array $validations
     * @param null /string $prefix
     * @param null /string $suffix
     * @return array
     * @throws Mage_Customer_Exception
     */
    protected function performValidate($data, $validations, $prefix = null, $suffix = null)
    {
        $errors = array();
        foreach ($validations as $key => $values) {
            if ($values['required'] && ((!isset($data[$key]) || empty($data[$key])) && $data[$key] !== '0')) {
                $errors[$prefix . $key . $suffix] = 'This is a required field.';
                continue;
            }

            if (!$values['required'] && (!isset($data[$key]) || empty($data[$key]))) {
                continue;
            }

            if (isset($values['validation']) && is_array($values['validation'])) {
                foreach ($values['validation'] as $rule) {
                    //if another error is already set, don't validate
                    if (isset($errors[$prefix . $key . $suffix])) continue;
                    if (!Mage::helper('vznl_validators/data')->{$rule}($data[$key])) {
                        if (isset($values['message'])
                            && isset($values['message'][$rule])
                            && !empty($values['message'][$rule])) {
                            $errors[$prefix . $key . $suffix] = $values['message'][$rule];
                        } else {
                            $errors[$prefix . $key . $suffix] = 'Field has invalid data';
                        }
                        continue;
                    }
                }
            }
        }

        return $errors;
    }

    protected function getAddressByType($address)
    {
        switch ($address['address']) {
            case 'store' :
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                $addressData = Mage::getModel('agent/dealer')->getCollection()
                    ->addFieldToFilter('dealer_id', $address['store_id']);
                if (Mage::app()->getWebsite()->getCode() != Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE) {
                    $addressData->addFieldToFilter('store_id', Mage::app()->getWebsite()->getId());
                }
                $addressData->fetchItem();

                if ($addressData) {
                    $addressData = $addressData->getFirstItem();
                    $result = array(
                        'street' => join(' ', array($addressData['street'], $addressData['house_nr'])),
                        'postcode' => $addressData['postcode'],
                        'city' => $addressData['city'],
                    );
                }
                break;
            case 'direct' :
                $storeId = $this->_getCustomerSession()->getAgent()->getDealer()->getId();
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                $addressData = Mage::getModel('agent/dealer')->getCollection()
                    ->addFieldToFilter('dealer_id', $storeId)
                    ->fetchItem();
                if ($addressData) {
                    $addressData = $addressData->getData();
                    $address['address'] = 'store';
                    $result = array(
                        'street' => join(' ', array($addressData['street'], $addressData['house_nr'])),
                        'postcode' => $addressData['postcode'],
                        'city' => $addressData['city'],
                    );
                }
                break;
            case 'billing_address' :
                if (!isset($address['billingAddress'])) {
                    throw new Exception('Incomplete data');
                }

                $result = $address['billingAddress'];
                break;
            case 'foreign_address' :
                if (!isset($address['foreignAddress'])) {
                    throw new Exception('Incomplete data');
                }
                $temp = $address['foreignAddress'];
                $result = $temp;
                $result['street'] = array();

                // Build street data from input data
                $streetData = trim(join(' ', array(
                    !empty($temp['street']) ? $temp['street'] : null,
                    !empty($temp['houseno']) ? $temp['houseno'] : null,
                    !empty($temp['addition']) ? $temp['addition'] : null,
                )));
                if ($streetData) {
                    $result['street'][] = $streetData;
                }

                if (isset($temp['extra'],$temp['extra'][0]) && !empty($temp['extra'][0])) {
                    foreach ($temp['extra'] as $extraLine) {
                        $result['street'][] = $extraLine;
                    }
                }

                $regionData = isset($temp['region']) && !empty($temp['region']) ? $temp['region'] : array();
                if ($regionData) {
                    $result['street'][] = $regionData;
                }
                break;
            case 'other_address' :
                if (!isset($address['otherAddress'])) {
                    throw new Exception('Incomplete data');
                }

                $result = $address['otherAddress'];
                break;
            default:
                throw new Exception('Incomplete data');
        }
        $result['address'] = $address['address'];
        return $result;
    }

    protected function getBusinessAddressByType($address)
    {
        switch ($address['company_address']) {
            case 'foreign_address' :
                if (!isset($address['foreignAddress'])) {
                    throw new Exception('Incomplete data');
                }
                $temp = $address['foreignAddress'];
                $result = $temp;
                $result['company_street'] = array();

                // Build street data from input data
                $streetData = trim(join(' ', array(
                    !empty($temp['company_street']) ? $temp['company_street'] : null,
                    !empty($temp['company_house_nr']) ? $temp['company_house_nr'] : null,
                    !empty($temp['company_house_nr_addition']) ? $temp['company_house_nr_addition'] : null,
                )));

                if ($streetData) {
                    $result['company_street'][] = $streetData;
                }

                if (isset($temp['extra'],$temp['extra'][0]) && !empty($temp['extra'][0])) {
                    foreach ($temp['extra'] as $extraLine) {
                        $result['street'][] = $extraLine;
                    }
                }

                $regionData = isset($temp['company_region']) && !empty($temp['company_region']) ? array($temp['company_region']) : array();
                if ($regionData) {
                    $result['company_street'][] = $regionData;
                }
                break;
            case 'other_address' :
                if (!isset($address['otherAddress'])) {
                    throw new Exception('Incomplete data');
                }

                $result = $address['otherAddress'];
                break;
            default:
                throw new Exception('Incomplete data');
        }
        return $result;
    }

    public function _validatePortabilityData($data)
    {
        $isBusiness = $this->getCart()->getQuote()->getCustomerIsBusiness();
        $validations = array(
            'mobile_number' => array(
                'required' => true,
                'validation' => array(
                    'validateIsNumber',
                )
            ),
            'sim' => array(
                'required' => true,
            ),
            'contract' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'current_provider' => array(
                'required' => true,
                'validation' => array(
                    'validateNetworkProvider'
                )
            ),
            'current_operator' => array(
                'required' => true,
                'validation' => array(
                    'validateNetworkOperator'
                )
            ),
            'type' => array(
                'required' => true,
                'validation' => array(
                    'validateName',
                )
            ),
            'end_date_contract' => array(
                'required' => true,
                'validation' => array(
                    'validateIsDate',
                    $isBusiness ? 'validateMaxPortingDaysB' : 'validateMaxPortingDaysC',
                    'validateMinPortingDays',
                    'validateHolidayDate',
                    'validateWorkingDay',
                ),
                'message' => array(
                    'validateIsDate'            => 'The value is not a valid date format.',
                    'validateWorkingDay'        => 'The date is in a weekend day.',
                    $isBusiness ? 'validateMaxPortingDaysB' : 'validateMaxPortingDaysC'    => sprintf('The date can be up to %s days in the future.', $isBusiness ? '120' : '60'),
                    'validateMinPortingDays'    => 'The date must be at least 5 working days in the future.',
                    'validateHolidayDate'       => 'The date is in a holiday.',
                )
            ),
        );


        $errors = array();

        foreach ($data as $packageId => $packageData) {
            if (isset($packageData['check']) && $packageData['check']) {
                $tmpValidations = $validations;
                if ($packageData['number_porting_type'] == 0) {
                    unset($tmpValidations['contract']);
                }
                if ($packageData['number_porting_type'] == 1) {
                    unset($tmpValidations['sim']);
                }
                // Validate fields
                $errors += $this->_performValidate($packageData, $tmpValidations, 'portability[' . $packageId . '][', ']');

                // Validate sim data is consistent with operator data
                if (($packageData['number_porting_type'] == 0) && !Mage::helper('vznl_validators/data')->validateSimByProviderAndOperator($packageData['sim'], $packageData['current_operator'], $packageData['current_provider'])) {
                    $errors['portability[' . $packageId . '][sim]'] = Mage::helper('vznl_checkout')->__('Field has invalid data');
                }
            }
        }

        return $errors;
    }

    /**
     * Map the given number porting to the SSFE values
     *
     * @param string $currentProvider
     * @return array
     */
    public function getNumberPortingInfo($currentProvider, $sim=null)
    {
        $result = array('provider' => $currentProvider, 'operator' => null);
        $operatorCollection = Mage::getModel('operator/operator')->getOperatorsWithProviders();
        $validator = Mage::helper('vznl_validators');
        foreach ($operatorCollection as $code => $data) {
            foreach ($data['providers'] as $provider) {
                if (($provider['code'] == $currentProvider) && ($validator->validateSimByProviderAndOperator($sim, $code, $provider['code']))) {
                    $result['provider'] = $provider['code'];
                    $result['operator'] = $code;
                    break;
                }
            }
        }

        return $result;
    }

    public function _performValidate($data, $validations, $prefix = null, $suffix = null)
    {
        $errors = array();
        foreach ($validations as $key => $values) {
            if ($values['required'] && (!isset($data[$key]) || empty($data[$key]))) {
                $errors[$prefix . $key . $suffix] = Mage::helper('vznl_checkout')->__('This is a required field.');
                continue;
            }

            if (!$values['required'] && (!isset($data[$key]) || empty($data[$key]))) {
                continue;
            }

            if (!empty($values['validation'])) {
                foreach ($values['validation'] as $rule) {
                    if (!Mage::helper('vznl_validators/data')->{$rule}($data[$key])) {
                        if (isset($values['message'])
                            && isset($values['message'][$rule])
                            && !empty($values['message'][$rule])){
                            $errors[$prefix . $key . $suffix] = Mage::helper('vznl_checkout')->__($values['message'][$rule]);
                        } else {
                            $errors[$prefix . $key . $suffix] = Mage::helper('vznl_checkout')->__('Field has invalid data');
                        }
                        continue;
                    }
                }
            }
        }

        return $errors;
    }

    private function _getAddressByType($address)
    {
        switch ($address['address']) {
            case 'store' :
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                $addressData = Mage::getModel('agent/dealer')->getCollection()
                    ->addFieldToFilter('dealer_id', $address['store_id']);
                if (Mage::app()->getWebsite()->getCode() != Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE) {
                    $addressData->addFieldToFilter('store_id', Mage::app()->getWebsite()->getId());
                }
                $addressData->fetchItem();

                if ($addressData) {
                    $addressData = $addressData->getFirstItem();
                    $result = array(
                        'street' => array($addressData['street'], $addressData['house_nr'], ''),
                        'postcode' => $addressData['postcode'],
                        'city' => $addressData['city'],
                    );
                    $result['store_id'] = $address['store_id'];
                }
                break;
            case 'direct' :
                $storeId = $this->_getCustomerSession()->getAgent()->getDealer()->getId();
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                $addressData = Mage::getModel('agent/dealer')->getCollection()
                    ->addFieldToFilter('dealer_id', $storeId)
                    ->fetchItem();
                if ($addressData) {
                    $addressData = $addressData->getData();
                    $address['address'] = 'store';
                    $result = array(
                        'street' => array($addressData['street'], $addressData['house_nr'], ''),
                        'postcode' => $addressData['postcode'],
                        'city' => $addressData['city'],
                    );
                    $result['store_id'] = $storeId;
                }
                break;
            case 'billing_address' :
                if (!isset($address['billingAddress'])) {
                    throw new Exception('Incomplete data');
                }

                $result = array(
                    'street' => array($address['billingAddress']['street'], $address['billingAddress']['houseno'], $address['billingAddress']['addition']),
                    'postcode' => $address['billingAddress']['postcode'],
                    'city' => $address['billingAddress']['city'],
                );
                break;
            case 'foreign_address' :
                if (!isset($address['foreignAddress'])) {
                    throw new Exception('Incomplete data');
                }
                $temp = $address['foreignAddress'];
                $result = $temp;
                // Build street data from input data
                $result['street'] = array($temp['street']);

                $result['extra_street'] = array();
                if (isset($temp['extra']) && is_array($temp['extra'])) {
                    foreach ($temp['extra'] as $extraLine) {
                        if (!empty($extraLine)) {
                            $result['extra_street'][] = $extraLine;
                        }
                    }
                }
                break;
            case 'other_address' :
                if (!isset($address['otherAddress'])) {
                    throw new Exception('Incomplete data');
                }
                $result = array(
                    'street' => array($address['otherAddress']['street'], $address['otherAddress']['houseno'], $address['otherAddress']['addition']),
                    'postcode' => $address['otherAddress']['postcode'],
                    'city' => $address['otherAddress']['city'],
                );
                break;
            default:
                throw new Exception('Incomplete data');
        }
        $result['address'] = $address['address'];
        return $result;
    }

    private function _getBusinessAddressByType($address)
    {
        switch ($address['company_address']) {
            case 'foreign_address' :
                if (!isset($address['foreignAddress'])) {
                    throw new Exception('Incomplete data');
                }
                $temp = $address['foreignAddress'];
                $result = $temp;

                if (isset($temp['extra'],$temp['extra'][0]) && !empty($temp['extra'][0])) {
                    $result['company_extra_lines'] = join(';', array_filter($temp['extra'], 'strlen'));
                }

                break;
            case 'other_address' :
                if (!isset($address['otherAddress'])) {
                    throw new Exception('Incomplete data');
                }

                $result = $address['otherAddress'];

                // Default country is NL
                $result['company_country_id'] = vznl_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;
                break;
            default:
                throw new Exception('Incomplete data');
        }
        return $result;
    }

    public function getOnePage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    public function _checkIfDeliveryInShop()
    {
        $quote = $this->getQuote();
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());

        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId]['type'] = $data['deliver']['address']['address'];
                if ($addressArray[$packageId]['type'] == 'store') {
                    $addressArray[$packageId]['store_id'] = $data['deliver']['address']['store_id'];
                }
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId]['type'] = $data['pakket'][$packageId]['address']['address'];
                if ($addressArray[$packageId]['type'] == 'store') {
                    $addressArray[$packageId]['store_id'] = $data['pakket'][$packageId]['address']['store_id'];
                }
            }
        }

        return $addressArray;
    }

    /**
     * Update customer data
     * @parameter array $customerData
     * @return void
     */
    private function updateCustomerMissingValues($customerData) {
        $customer = $this->getCart()->getQuote()->getCustomer();
        $exclude = ['dob'];
        foreach ($customerData as $key => $value) {
            if (in_array($key, $exclude)) continue;
            $customer->setData($key, $value);
        }
        $customer->setData('dob', date('Y-m-d H:i:s', strtotime($customerData['dob'])));
    }

}
