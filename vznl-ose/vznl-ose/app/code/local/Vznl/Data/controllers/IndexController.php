<?php

/**
 * Data Import default front controller
 */
class Vznl_Data_IndexController extends Mage_Core_Controller_Front_Action
{
    /** @var Dyna_Core_Helper_Data|null */
    private $_dynaCoreHelper = null;

    /**
     * @return Dyna_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('dyna_core');
        }

        return $this->_dynaCoreHelper;
    }

    public function updateSectionDataAction()
    {
        $section = $this->getRequest()->getParam('section');
        // RFC-140821 - Perform prioritize service calls for the selected orders
        $ids = $this->getRequest()->getParam('ids', []);
        if ($section && count($ids)) {
            if ($section === 'number-porting' || $section === 'open-orders') {
                $method = 'triggerPorting';
                $field1 = 'porting_status';
                $field2 = 'porting_status_updated';
                $allowedStatuses = [
                    Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING,
                    Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED
                ];

                $this->updateSectionForOrders($ids, $method, $field1, $field2, $allowedStatuses);
            }
            if ($section == 'credit-checks' || $section === 'open-orders') {
                $method = 'triggerOrderValidation';
                $field1 = 'creditcheck_status';
                $field2 = 'creditcheck_status_updated';
                $allowedStatuses = [
                    Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED,
                    Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING,
                    Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED
                ];

                $this->updateSectionForOrders($ids, $method, $field1, $field2, $allowedStatuses);
            }
            $this->jsonResponse([
                'error' => false
            ]);
        } else {
            $this->jsonResponse([
                'error' => true,
                'message' => $this->__('Missing data')
            ]);
        }
    }

    private function updateSectionForOrders($orderIds, $method, $field1, $field2, $allowedStatuses)
    {
        $client = Mage::helper('vznl_service')->getClient('prioritize', ['options' => []]);
        $flippedAllowedStatuses = array_flip($allowedStatuses);

        foreach ($orderIds as $ordernumber) {
            try {
                /** @var Vznl_Superorder_Model_Superorder $so */
                $so = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($ordernumber);
                if ($so && $so->getId()) {

                    foreach ($so->getPackages() as $package) {
                        $dataField1 = $package->getData($field1);
                        if (isset($flippedAllowedStatuses[$dataField1])) {
                            $combined = $ordernumber . '.' . $package->getPackageId();
                            $result = $client->{$method}($combined);
                            if ($result['succeeded'] != 'true' && $result['succeeded'] !== true) {
                                continue;
                            }
                            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                            $updateData = array(
                            	$field1 => $result['result_message'],
                            	$field2 => now(),
                            	'updated_at' => now()
                            );
                            $write-update(
                            	'catalog_package',
                            	$updateData,
                            	array('entity_id = ?' => $package->getId())
                            );
                        }
                    }
                }
            } catch (Exception $e) {
                // If the call failed just continue
                continue;
            }
        }
    }

    public function loadLinkedAccountAction()
    {
        $response['error'] = false;
        $session = Mage::getSingleton('customer/session');
        $response = $session->getData('customerSearchData');
        $response['linked_accounts'] = array_reverse($response['linked_accounts']);
        $this->jsonResponse([
            'error' => $response['error'],
            'body' => $response,
        ]);
    }

    /**
     * Load customer baskers, including offers and risk orders
     */
    public function loadMyBasketsAction() {
        try {
            $carts = Mage::getModel('customer/customer')->mapShoppingCarts();
            $this->jsonResponse([
                'error' => false,
                'body' => $carts,
            ]);
        } catch (Exception $e) {
            $this->jsonResponse([
                'error' => true,
                'message'=> $this->__('There was a problem with loading customer carts')
            ]);
        }
    }

    /**
     * Get customer orders action - merged all orders and ufix orders into one call
     */
    public function searchByCustomerAllAction() {
        $section = $this->getRequest()->getParam('section');
        $countQuery = $this->getRequest()->getParam('countQuery');
        $myOrders = $this->getRequest()->getParam('myOrders');

        $allOrders = $this->loadCustomerOrders($section, $countQuery, '', $myOrders);
        $ufixOrders = $this->loadCustomerOrders($section, $countQuery, 'oo-inlife', $myOrders);

        $this->jsonResponse([
            'error' => [
                'all-orders' => $allOrders['error'],
                'ufix-orders' => $ufixOrders['error']
            ],
            'body' => [
                'all-orders' => $allOrders['body'],
                'ufix-orders' => $ufixOrders['body']
            ]
        ]);
    }

    /**
     * Get customer orders action
     */
    public function searchByCustomerAction()
    {
        $section = $this->getRequest()->getParam('section');
        $countQuery = $this->getRequest()->getParam('countQuery');

        /** @var string all|error|ready|check|other $cluster */
        $cluster = $this->getRequest()->getParam('cluster', '');
        $myOrders = $this->getRequest()->getParam('myOrders');

        $response = $this->loadCustomerOrders($section, $countQuery, $cluster, $myOrders);

        $this->jsonResponse([
            'error' => $response['error'],
            'body' => $response['body'],
        ]);
    }

    /**
     * Load orders from all buckets
     */
    public function searchByCustomerAllClustersAction() {
        $section = $this->getRequest()->getParam('section');
        $countQuery = $this->getRequest()->getParam('countQuery');
        /** @var string all|error|ready|check|other $cluster */
        $clusters = $this->getRequest()->getParam('clusters');
        $myOrders = $this->getRequest()->getParam('myOrders');

        $allClusers = explode(',', $clusters);
        $response = [];
        foreach ($allClusers as $cluster) {
            $result = $this->loadCustomerOrders($section, $countQuery, $cluster, $myOrders);
            $response['body']['buckets'][$cluster] = $result['body']['buckets'][$cluster];
            $response['error'] = $response['error'] ?? false || $result['error'];
        }

        $this->jsonResponse([
            'error' => $response['error'],
            'body' => $response['body'],
        ]);
    }

    /**
     * Load orders for customer
     */
    private function loadCustomerOrders($section, $countQuery, $cluster, $myOrders) {
        /** @var Vznl_Data_Helper_Data $dataHelper */
        $dataHelper = Mage::helper('vznl_data/data');

        $customerIdRequest = $this->getRequest()->getParam('customerId');

        /** @var Dyna_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');
        if ($myOrders) {
            $customer_id = $session->getCustomer()->getId();
        } else if(!empty($customerIdRequest) && is_numeric($customerIdRequest)){
            $customer_id = $customerIdRequest;
        }

        $customerScreen = false;
        $customer = null;
        if (isset($customer_id)) {
            /** @var Dyna_Customer_Model_Customer $customer */
            $customer = Mage::getModel('customer/customer')->load($customer_id);
            $customerScreen = !$customer->getId() ? false : $this->getRequest()->getParam('customerScreen');
        }

        /** @var Vznl_Agent_Model_Agent $agent */
        $agent = $session->getSuperAgent() ?: $session->getAgent(true) ;

        // Retrieve GET data
        $clusterData = [];
        if ($countQuery !== 'true') {
            $clusterOrderField = $this->getRequest()->getParam('orderbyfield', 'id');
            $clusterOrderType = $this->getRequest()->getParam('ordertype', 'desc');
            $clusterPage = (int)$this->getRequest()->getParam('page', 1);
            $clusterResultsPerPage = $countQuery === 'true' ? null : (int)$this->getRequest()->getParam('resultsOnPage');
            if (!trim($clusterResultsPerPage)) {
                $clusterResultsPerPage = Mage::getStoreConfig('agent_configuration/open_orders_limits/default_oo_results');
            }
            $clusterData = [
                'orderField' => $clusterOrderField,
                'orderType' => $clusterOrderType,
                'page' => $clusterPage,
                'resultsPerPage' => $clusterResultsPerPage
            ];
        }

        // Getting the section title, buckets and subject
        $sectionSkeleton = $dataHelper->getSectionSkeleton($section, $customerScreen);
        $subject = $sectionSkeleton['subject'];
        $sectionHeaderTitle = $sectionSkeleton['sectionHeaderTitle'];
        $sectionHeaderTotalText = $sectionSkeleton['sectionHeaderTotalText'];
        $buckets = $sectionSkeleton['buckets'];

        // Init empty bootstrap values
        $agentId = 0;
        $dealerId = 0;
        $response['error'] = false;
        $customerId = $customer && $customer->getId() > 0 ? $customer->getId() : null;
        $websiteId = $subject == 'FAILED_VALIDATIONS' ? $agent->getPermittedValidationWebsites() : $agent->getPermittedSearchWebsites([], $subject);

        if ($agent->isGranted('SEARCH_' . $subject . '_OF_AGENT')) {
            $agentId = $agent->getId();
        }
        if ($agent->isGranted('SEARCH_' . $subject . '_OF_DEALER')) {
            $dealerId = $agent->getDealer()->getDealerId();
        }
        if ($agent->isGranted('SEARCH_' . $subject . '_OF_GROUP')) {
            $agentId = $agent->getAgentsInSameGroup();
        }
        if ($agent->isGranted('SEARCH_' . $subject . '_OF_ALL')) {
            $agentId = 0;
        }

        if ($cluster !== '') {
            try {
                $response['body'] = $dataHelper->getBucketBody($cluster, $buckets, $agentId, $customerId, $dealerId, $section, $websiteId, $customerScreen, $countQuery, $clusterData);
            } catch (Exception $e) {
                $response['httpstatus'] = 500;
                $response['error'] = true;
                $response['body'] = $e->getMessage();
            }
        } else {
            // Init empty buckets
            $orderNumbers = array();
            foreach ($buckets as $bucket) {
                try {
                    $response['body']['buckets'][$bucket] = [];
                    if ($bucket != 'oo-inlife') {
                        $tmp = $dataHelper->getBucketBody($bucket, $buckets, $agentId, $customerId, $dealerId, $section, $websiteId, $customerScreen, $countQuery, $clusterData);
                        if (isset($tmp['buckets'][$bucket]['data'])) {
                            foreach ($tmp['buckets'][$bucket]['data'] as $k=>$d) {
                                $number = $d['number'];
                                if(isset($orderNumbers[$number])) {
                                    array_splice($tmp['buckets'][$bucket]['data'], $k, 1);
                                }
                                $orderNumbers[$d['number']] = $d['number'];
                            }
                        }
                        $response['body']['buckets'][$bucket] = $tmp['buckets'][$bucket];
                    }
                } catch (Exception $e) {
                    $response['httpstatus'] = 500;
                    $response['error'] = true;
                    $response['body'] = [
                        'error_message' => $e->getMessage(),
                    ];
                }
            }
        }

        // Set section data
        if (!isset($response['body']) || !is_array($response['body'])) {
            $response = [
                'body' => [],
                'error' => $response['error']
            ];
        }
        if (!isset($response['body']['section']) || empty($response['body']['section'])) {
            $response['body']['section'] = [];
        }
        $response['body']['section']['title'] = $sectionHeaderTitle;
        $response['body']['section']['totaltext'] = $sectionHeaderTotalText;
        $response['body']['section']['name'] = $section;
        $response['body']['section']['cluster'] = $cluster;

        // Set selected customer data
        $response['body']['customer'] = [];
        if ($customerId) {
            $response['body']['customer']['id'] = $customer->getId();
            $response['body']['customer']['name'] = $customer->getName();
            $response['body']['customer']['ban'] = $customer->getBan();
        }

        return $response;
    }


    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        /** @var array $response */
        $response = $this->getDynaCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);
    }

    /**
     * Get the available stub options
     * @throws Exception
     */
    public function getStubOptionsAction()
    {
    	if (Mage::getStoreConfig(Vznl_Utility_Helper_Stubs::XML_PATH_PHP_STUB_ENABLE, Mage::app()->getStore())) {
    		$command =$this->getRequest()->getParam('command');
    		$helper = Mage::helper('vznl_utility/stubs');
    		echo nl2br($helper->usageHelp());
    		return;
    	}

    	throw new Exception('Unauthorized');
    }

    /**
     * Run the selected stub action
     * @throws Exception
     */
    public function runServiceStubAction()
    {
    	if (Mage::getStoreConfig(Vznl_Utility_Helper_Stubs::XML_PATH_PHP_STUB_ENABLE, Mage::app()->getStore())) {
    		if ($this->getRequest()->isPost()) {
    			$helper = Mage::helper('vznl_utility/stubs');
    			$helpArray = array('-h', '-help');
    			$command = $this->getRequest()->getParam('command');

    			if (empty($command) || in_array(strtolower($command), $helpArray)) {
    				echo nl2br($helper->usageHelp());
    				return;
    			}

    			$requestData = $helper->parseWebCommand($command);
    			$response = $helper->execute($requestData);
    			echo nl2br($response);
    			return;
    		}
    	}

    	throw new Exception('Unauthorized');
    }
}
