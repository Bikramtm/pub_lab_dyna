<?php
// Add missing indexes for catalog_packages/superorder/sales_flat_order tables in order to improve the performance for order view queries
$setup = Mage::getSingleton('core/resource')->getConnection('core_read');
$indexes = [
    'superorder' => [
        'IDX_SUPERORDER_CREATED_WEBSITE_ID' => 'created_website_id',
        'IDX_SUPERORDER_IS_VF_ONLY' => 'is_vf_only',
        'IDX_SUPERORDER_CUSTOMER_ID' => 'customer_id',
        'IDX_SUPERORDER_CREATED_AT' => 'created_at',
        'IDX_SUPERORDER_CREATED_AGENT_ID' => 'created_agent_id',
    ],
    'catalog_package' => [
        'IDX_CATALOG_PACKAGE_PORTING_STATUS_UPDATED' => 'porting_status_updated',
        'IDX_CATALOG_PACKAGE_CREDITCHECK_STATUS_UPDATED' => 'creditcheck_status_updated',
        'IDX_CATALOG_PACKAGE_CREDITCHECK_STATUS' => 'creditcheck_status',
        'IDX_CATALOG_PACKAGE_PORTING_STATUS' => 'porting_status',
    ],

    'sales_flat_order' => [
        'IDX_SALES_FLAT_ORDER_EDITED' => 'edited',
    ],
];

$this->startSetup();

// Make sure the same index is not added twice
foreach ($indexes as $tableName => &$index) {
    foreach ($setup->fetchAll(sprintf('SHOW INDEX FROM %s', $tableName)) as $indexData) {
        if (isset($index[$indexData['Key_name']])) {
            unset($index[$indexData['Key_name']]);
        }
    }
}
unset($index);
unset($tableName);
foreach ($indexes as $tableName => $index) {
    foreach ($index as $indexName => $indexColumn) {
        $this->getConnection()->addIndex(
            $tableName,
            $indexName,
            [$indexColumn],
            Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
        );
    }
}
unset($index);
unset($tableName);
$this->endSetup();
