<?php

class Vznl_Guru_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Retrieve GURU url for customer
     * @param Dyna_Customer_Model_Customer|null $customer
     * @param string $customerCtn
     * @return string
     */
    public function getUrl(
        ?Dyna_Customer_Model_Customer $customer,
        string $customerCtn
    ):string {
        $url = '';
        if (!empty($customer->getData('ban'))) {
            $customerId = $customer->getData('ban');

            $agentHelper = Mage::helper('agent');
            $agent = $agentHelper->getAgent();

            $agentUsername = ($agent) ? $agent->getUsername() : '';
            $agentDealerCode = $agentHelper->getDaDealerCode();

            $tokenGenerator = Mage::getSingleton('guru/token');
            $token = $tokenGenerator->getToken(
                $agentUsername,
                $customerId,
                "fVF_customernr"
            );

            $urlGenerator = Mage::getSingleton('guru/url');
            $url = $urlGenerator->getUrl(
                $token,
                $customerId,
                $customerCtn,
                $agentUsername,
                $agentDealerCode
            );
        }

        return $url;
    }
}
