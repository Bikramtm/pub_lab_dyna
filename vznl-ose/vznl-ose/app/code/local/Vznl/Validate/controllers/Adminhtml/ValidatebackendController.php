<?php

class Vznl_Validate_Adminhtml_ValidatebackendController extends Mage_Adminhtml_Controller_Action
{
    /** @var Dyna_Core_Helper_Data|null */
    private $_dynaCoreHelper = null;
    private $_attributeSetCollection = null;
    private $_websites = null;

    protected $validateChecks = [
        'validate_hawai_xml_catalogue' => 'Validate Hawaii Catalog XML with XSD',
        'validate_hawai_xml_stock' => 'Validate Hawaii Stock XML with XSD',
        'validate_hawai_xml_mixmatch' => 'Validate Hawaii Mixmatch XML with XSD',
        'validateHawaiiXmlCatalogueV3' => 'Validate Hawaii Catalog Step 3 XML with XSD',
        'validateHawaiiXmlStockV3' => 'Validate Hawaii Stock Step 3 XML with XSD',
        'validateHawaiiXmlMixmatchV3' => 'Validate Hawaii Mixmatch Step 3 XML with XSD',
        'products_with_missing_data' => 'Products with missing data',
        'validate_axi_xsd' => 'Validate AXI with XSD',
        'products_incorrect_vat' => 'Validate products without 21% VAT',
        'stockableitems_without_sap_ean' => 'All active stockable products without a SAP or EAN code',
        'stockableitems_are_stockable' => 'All stockable products that are not set as stockable',
        'stockableitems_are_not_stockable' => 'All non stockable products that are set as stockable',
        'products_with_wrong_ean_length' => 'All products with an incorrect EAN code length',
        'products_with_wrong_sap_length' => 'All products with an incorrect SAP code length',
        'subscription_without_maf_connection_costs' => 'All subscriptions without MAF or connection price',
        'missing_hawaii_data' => 'Missing Hawaii data',
        'mixmatch_sku_active_magento' => 'Check if Mixmatch products are active in Magento',
        'multimapper_sku_active_magento' => 'Check if Multimapper products are active in Magento',
        'trailing_spaces_multimapper' => 'Multimapper with trailing spaces',
        'products_without_channel' => 'Active addons, voice_subscriptions, data_subscriptions and promotions without a channel',
        'duplicate_ean_or_sap' => 'Check for double EAN & SAP',
        'check_missing_mixmatch' => 'Check missing mixmatch',
    ];

    /**
     * @return boolean
     */
    public function verifyHost()
    {
        return false;
    }

    /**
     * @return Dyna_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('dyna_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * Perform the validations based on selected type
     * @throws Exception
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest())
        {
            $method = $this->getRequest()->getParam('type');
            try
            {
                $this->$method();
            }
            catch(Exception $e)
            {
                throw new Exception($e->getMessage());
            }
        }
        else
        {
            $this->loadLayout();
            $this->_title($this->__("Validate"));

            $layout = $this->getLayout();
            $block = $layout->getBlock('validatebackend');
            $block->setData('validatecollection', $this->validateChecks);

            $this->renderLayout();
        }
    }

    /**
     * Download the validation result based on provided type
     * @throws Zend_Controller_Response_Exception
     */
    public function downloadAction()
    {
        $type = $this->getRequest()->getParam('type');
        $xml = $this->$type(false);
        $this->getResponse ()
            ->setHttpResponseCode ( 200 )
            ->setHeader ( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true )
            ->setHeader ( 'Pragma', 'public', true )
            ->setHeader ( 'Content-type', 'application/force-download' )
            ->setHeader ('Content-Disposition', 'attachment' . '; filename='.$type.'.xml');
        $this->getResponse ()->clearBody ();
        $this->getResponse ()->sendHeaders ();
        echo $xml;
        exit;
    }

    /**
     * Checks if there are any products that are not associated to any website/channel
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function products_without_channel()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('attribute_set_id', $this->getAttributeSetIdsByName(['accessoire','voice_subscription','data_subscription', 'promotion']))
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addAttributeToFilter([
                ['attribute' => 'is_deleted', 'null' => true],
                ['attribute' => 'is_deleted', 'neq' => '1'],
            ]);
        foreach($collection as $product)
        {
            if(empty($product->getWebsiteIds()))
            {
                $data['data'][] = array('message '=>' Product: "'.$product->getSku().'" is not connected to a sales channel');
            }
        }
        $data['error'] = count($data['data']) > 0 ? true : false;
        return $this->jsonResponse($data);
    }

    /**
     * Checks if there are any addons which contain space character which could cause issues during retrieval/mapping
     */
    protected function trailing_spaces_multimapper()
    {
        $data=array();
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $queryResult = $write->query("select *
            from dyna_multi_mapper
            where
            (
                length(addon1_name) <> length(trim(addon1_name))
                or length(addon1_desc)  <> length(trim(addon1_desc))
                or length(addon1_nature) <> length(trim(addon1_nature))
                or length(addon1_soc) <> length(trim(addon1_soc))
            ) OR
            (length(addon2_name) <> length(trim(addon2_name))

            or length(addon2_desc)  <> length(trim(addon2_desc))

            or length(addon2_nature) <> length(trim(addon2_nature))

            or length(addon2_soc) <> length(trim(addon2_soc))
                ) OR
            (length(addon3_name) <> length(trim(addon3_name))

            or length(addon3_desc)  <> length(trim(addon3_desc))

            or length(addon3_nature) <> length(trim(addon3_nature))

            or length(addon3_soc) <> length(trim(addon3_soc))
                ) OR
            (length(addon4_name) <> length(trim(addon4_name))

            or length(addon4_desc)  <> length(trim(addon4_desc))

            or length(addon4_nature) <> length(trim(addon4_nature))

            or length(addon4_soc) <> length(trim(addon4_soc))
                ) OR
            (length(addon5_name) <> length(trim(addon5_name))

            or length(addon5_desc)  <> length(trim(addon5_desc))

            or length(addon5_nature) <> length(trim(addon5_nature))

            or length(addon5_soc) <> length(trim(addon5_soc))
                ) OR
            (length(addon6_name) <> length(trim(addon6_name))

            or length(addon6_desc)  <> length(trim(addon6_desc))

            or length(addon6_nature) <> length(trim(addon6_nature))

            or length(addon6_soc) <> length(trim(addon6_soc))
                ) OR
            (length(addon7_name) <> length(trim(addon7_name))

            or length(addon7_desc)  <> length(trim(addon7_desc))

            or length(addon7_nature) <> length(trim(addon7_nature))

            or length(addon7_soc) <> length(trim(addon7_soc))
                ) OR
            (length(addon8_name) <> length(trim(addon8_name))

            or length(addon8_desc)  <> length(trim(addon8_desc))

            or length(addon8_nature) <> length(trim(addon8_nature))

            or length(addon8_soc) <> length(trim(addon8_soc))
                ) OR
            (length(addon9_name) <> length(trim(addon9_name))

            or length(addon9_desc)  <> length(trim(addon9_desc))

            or length(addon9_nature) <> length(trim(addon9_nature))

            or length(addon9_soc) <> length(trim(addon9_soc))
                ) OR
            (length(addon10_name) <> length(trim(addon10_name))

            or length(addon10_desc)  <> length(trim(addon10_desc))

            or length(addon10_nature) <> length(trim(addon10_nature))

            or length(addon10_soc) <> length(trim(addon10_soc))
                )");
        while ($row = $queryResult->fetch() )
        {
            $error = array();
            $error['message'] = 'Trailing spaces at "'.$row['entity_id'].'" priceplan "'.$row['pp_sku'].'" with promo "'.$row['promo_sku'].'"';
            $data['data'][] = $error;
        }
        return $this->jsonResponse($data);
    }

    /**
     * @param string $endpointUrl
     * @param string $xsdPath
     * @param array $fields
     * @param bool $output
     *
     * @return mixed
     */
    protected function validateHawaiiXml(string $endpointUrl, string $xsdPath, array $fields, bool $output = true)
    {
        // Set response
        $data['error'] = false;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpointUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        curl_setopt($ch, CURLOPT_POSTREDIR, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $this->verifyHost());
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verifyHost());
        $data['xml'] = curl_exec($ch);
        curl_close($ch);

        if (!$output) {
            return $data['xml'];
        }

        // Validate against XSD
        libxml_use_internal_errors(true);
        $domDocument = new DOMDocument();
        $domDocument->loadXml($data['xml']);

        if (!$domDocument->schemaValidate($xsdPath)) {
            $data['error'] = true;
            $errors = libxml_get_errors();
            foreach ($errors as $error) {
                $data['data'][] = [
                    'code' => $error->code,
                    'line' => $error->line,
                    'column' => $error->column,
                    'message' => $error->message
                ];
            }
        }

        return $this->jsonResponse($data);
    }

    /**
     * Validate hawaii xml catalog against the xsd
     * @param bool $output
     * @return mixed
     */
    protected function validate_hawai_xml_catalogue($output = true)
    {
        $endpointUrl = 'http://' . Mage::app()->getFrontController()->getRequest()->getHttpHost() . '/rest/catalogue';
        $xsdPath = Mage::getModuleDir('etc',  'Vznl_Validate') . DIRECTORY_SEPARATOR . 'xsd' . DIRECTORY_SEPARATOR . 'hawaii.catalog.xsd';
        $fields = [
            'products' => 'true',
            'compatibility' => 'true',
            'promotion' => 'true',
            'refreshcache' => 'true',
        ];

        return $this->validateHawaiiXml($endpointUrl, $xsdPath, $fields, $output);
    }

    /**
     * Validate hawaii step3 XML catalog against the XSD
     * @param bool $output
     * @return mixed
     */
    protected function validateHawaiiXmlCatalogueV3(bool $output = true)
    {
        $endpointUrl = 'http://' . Mage::app()->getFrontController()->getRequest()->getHttpHost() . '/rest/catalogue/new';
        $xsdPath = Mage::getModuleDir('etc',  'Vznl_Validate') . DIRECTORY_SEPARATOR . 'xsd' . DIRECTORY_SEPARATOR . 'hawaii.catalog.v3.xsd';
        $fields = [
            'products' => 'true',
            'compatibility' => 'true',
            'promotion' => 'true',
            'refreshcache' => 'true',
        ];

        return $this->validateHawaiiXml($endpointUrl, $xsdPath, $fields, $output);
    }

    /**
     * Check if there are any products with incorrect vat amount
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function products_incorrect_vat()
    {
        $store = Mage::app()->getStore('default');
        $request = Mage::getSingleton('tax/calculation')->getRateRequest(null, null, null, $store);

        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('attribute_set_id', array('neq' => current($this->getAttributeSetIdsByName(['accessoire']))))
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addStoreFilter()
            ->addAttributeToFilter([
                ['attribute' => 'is_deleted', 'null' => true],
                ['attribute' => 'is_deleted', 'neq' => '1'],
            ]);
        foreach($collection as $product)
        {
            $percent = Mage::getSingleton('tax/calculation')->getRate($request->setProductClassId($product->getData('tax_class_id')));
            if($percent != $product->getTaxRate())
            {
                $data['data'][] = array('message' => 'Product: "'.$product->getSku().'" does not the correct VAT amount ('.$percent.'%)');
            }
        }
        $data['error'] = count($data['data']) > 0 ? true : false;
        return $this->jsonResponse($data);
    }

    /**
     * Validate stock xml returned by rest api call against the xsd
     * @param bool $output
     * @return false|string|Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function validate_hawai_xml_stock($output = true)
    {
        $endpointUrl = 'http://' . Mage::app()->getFrontController()->getRequest()->getHttpHost() . '/rest/catalogue';
        $xsdPath = Mage::getModuleDir('etc', 'Vznl_Validate') . DIRECTORY_SEPARATOR . 'xsd' . DIRECTORY_SEPARATOR . 'hawaii.catalog.stock.xsd';
        $fields = [
            'stock' => 'true',
        ];

        return $this->validateHawaiiXml($endpointUrl, $xsdPath, $fields, $output);
    }

    /**
     * Validate step3 stock XML returned by Rest API call against the XSD
     * @param bool $output
     * @return mixed
     */
    protected function validateHawaiiXmlStockV3(bool $output = true)
    {
        $endpointUrl = 'http://' . Mage::app()->getFrontController()->getRequest()->getHttpHost() . '/rest/catalogue/new';
        $xsdPath = Mage::getModuleDir('etc', 'Vznl_Validate') . DIRECTORY_SEPARATOR . 'xsd' . DIRECTORY_SEPARATOR . 'hawaii.catalog.stock.v3.xsd';
        $fields = [
            'stock' => 'true',
        ];

        return $this->validateHawaiiXml($endpointUrl, $xsdPath, $fields, $output);
    }

    /**
     * check if there are any invalid mixmatch combinations
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function mixmatch_sku_active_magento()
    {
        $data=array();
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $statusAttributeId = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'status')->getId();
        $queryResult = $write->query("select *
            from catalog_mixmatch cm
            left join catalog_product_entity cpe_dev on cm.target_sku = cpe_dev.sku
            left join catalog_product_entity cpe_sub on cm.target_sku = cpe_sub.sku
            left join catalog_product_entity_int cpei_dev on cpei_dev.entity_id = cpe_dev.entity_id and cpei_dev.attribute_id = $statusAttributeId 
            left join catalog_product_entity_int cpei_sub on cpei_sub.entity_id = cpe_sub.entity_id and cpei_sub.attribute_id = $statusAttributeId
            where cpe_dev.entity_id is null
            or cpe_sub.entity_id is null
            or cpei_dev.value <> 1
            or cpei_sub.value <> 1");
        while ($row = $queryResult->fetch() )
        {
            $error = array();
            $error['message'] = 'Device '.$row['device_sku'].' with subscription '.$row['subscription_sku'].' is not valid';
            $data['data'][] = $error;
        }
        return $this->jsonResponse($data);
    }

    protected function multimapper_sku_active_magento()
    {
        $data=array();
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $statusAttributeId = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'status')->getId();
        $queryResult = $write->query("select pp_sku, promo_sku, cpei_promo.value as promoActive, cpei_priceplan.value as priceplanActive
            from dyna_multi_mapper dmm
            left join catalog_product_entity cpe_priceplan on dmm.pp_sku = cpe_priceplan.sku
            left join catalog_product_entity cpe_promo on dmm.promo_sku = cpe_promo.sku
            left join catalog_product_entity_int cpei_priceplan on cpei_priceplan.entity_id = cpe_priceplan.entity_id and cpei_priceplan.attribute_id = $statusAttributeId
            left join catalog_product_entity_int cpei_promo on cpei_promo.entity_id = cpe_promo.entity_id and cpei_promo.attribute_id = $statusAttributeId and dmm.promo_sku <> ''
            where cpe_priceplan.entity_id is null
            or cpei_priceplan.value <> 1
            or (dmm.promo_sku <> '' and cpe_promo.entity_id is null)
            or (dmm.promo_sku <> '' and cpei_promo.value <> 1)
            order by pp_sku");
        while ($row = $queryResult->fetch() )
        {
            $error = array();
            $error['message'] = 'Priceplan '.$row['pp_sku'].' ('.($row['priceplanActive'] != 1 ? 'active' : 'inactive').') with promo '.$row['promo_sku'].' ('.($row['promoActive'] != 1 ? 'active' : 'inactive').') is not valid';
            $data['data'][] = $error;
        }
        return $this->jsonResponse($data);
    }

    /**
     * Validate mixmatch xml returned by rest api against xsd
     * @param bool $output
     * @return false|string|Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function validate_hawai_xml_mixmatch($output = true)
    {
        $endpointUrl = 'http://' . Mage::app()->getFrontController()->getRequest()->getHttpHost() . '/rest/catalogue';
        $xsdPath = Mage::getModuleDir('etc', 'Vznl_Validate') . DIRECTORY_SEPARATOR . 'xsd' . DIRECTORY_SEPARATOR . 'hawaii.catalog.mixmatch.xsd';
        $fields = [
            'mixmatch' => 'true',
        ];

        return $this->validateHawaiiXml($endpointUrl, $xsdPath, $fields, $output);
    }

    /**
     * Validate step3 mixmatch XML returned by Rest API against XSD
     * @param bool $output
     * @return mixed
     */
    protected function validateHawaiiXmlMixmatchV3(bool $output = true)
    {
        $endpointUrl = 'http://' . Mage::app()->getFrontController()->getRequest()->getHttpHost() . '/rest/catalogue/new';
        $xsdPath = Mage::getModuleDir('etc', 'Vznl_Validate') . DIRECTORY_SEPARATOR . 'xsd' . DIRECTORY_SEPARATOR . 'hawaii.catalog.mixmatch.v3.xsd';
        $fields = [
            'mixmatch' => 'true',
        ];

        return $this->validateHawaiiXml($endpointUrl, $xsdPath, $fields, $output);
    }

    /**
     * Validate xml returned by export api script against xsd
     * @param bool $output
     * @return string|Vznl_Validate_Adminhtml_ValidatebackendController|null
     */
    protected function validate_axi_xsd($output=true)
    {
        //try to determine phpPath
        $phpPath = shell_exec('which php') ? : '/usr/local/zend/bin/php';
        $data['xml'] = shell_exec(trim($phpPath) . ' ' . Mage::getBaseDir().DIRECTORY_SEPARATOR.'shell'.DIRECTORY_SEPARATOR.'export_axi.php');

        if(!$output)
        {
            return $data['xml'];
        }

        // Validate against XSD
        libxml_use_internal_errors(true);
        $domDocument = new DOMDocument();
        $domDocument->loadXml($data['xml']);

        $xsdPath = Mage::getModuleDir('etc', 'Vznl_Validate').DIRECTORY_SEPARATOR.'xsd'.DIRECTORY_SEPARATOR.'axi.SalesProductList.xsd';
        if(!$domDocument->schemaValidate($xsdPath)){
            $errors = libxml_get_errors();

            foreach($errors as $domError)
            {
                $error = array();
                $error['line'] = $domError->line;
                $error['message'] = $domError->message;
                $data['data'][] = $error;
            }
            $data['error'] = true;
            return $this->jsonResponse($data);
        }
        $data['error'] = false;
        return $this->jsonResponse($data);
    }

    /**
     * Check for products which are missing values for mandatory attributes
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function products_with_missing_data()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addStoreFilter()
            ->addAttributeToFilter([
                ['attribute' => 'is_deleted', 'null' => true],
                ['attribute' => 'is_deleted', 'neq' => '1'],
            ]);

        $allAttributes = [];
        $attributeSetIds = $this->getAttributeSetIdsByName();
        foreach($attributeSetIds as $attributeSetId){
            if(!isset($allAttributes[$attributeSetId])){
                $allAttributes[$attributeSetId] = Mage::getModel('catalog/product')->getResource()
                    ->loadAllAttributes()
                    ->getSortedAttributes($attributeSetId);
            }
        }

        foreach($collection as $product)
        {
            $productAttributeKeys = array_keys($product->getData());
            $attributes = $allAttributes[$product->getAttributeSetId()];
            foreach($attributes as $attribute)
            {
                if($attribute->getIsRequired() && in_array($attribute->getAttributeCode(), $productAttributeKeys))
                {
                    if(is_null($attribute->getFrontend()->getValue($product)))
                    {
                        $error['message'] = 'Product: <b>'.$product->getId().' ('.$product->getSku().')</b>: "'.$attribute->getAttributeCode().'" is empty or has invalid data';
                        $data['data'][] = $error;
                    }
                }
            }
        }
        $data['error'] = count($data['data']) > 0 ? true : false;
        return $this->jsonResponse($data);
    }

    /**
     * check for missing hawaii data
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function missing_hawaii_data()
    {
        $storeId = $this->getStoreIdByCode('vodafone_webshop');
        $currentWebsiteId = $this->getWebsiteIdByCode('vodafone_webshop');
        $needConfigurable = $this->getAttributeSetIdsByName(['data_subscription','data_device','simcard','prepaid_simonly','voice_device','voice_subscription']);
        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addStoreFilter($storeId);
        foreach($collection as $product)
        {
            foreach($product->getData() as $key => $value)
            {
                if(preg_match('/.*hawaii.*/', $key))
                {
                    if(is_null($value) || $value == '')
                    {
                        $data['data'][] = array('message' => $product->getSku().' has an empty Hawaii field "'.$key.'"');
                    }
                }
            }
            if($product->getData('type_id') == 'simple')
            {
                if(in_array($product->getAttributeSetId(), $needConfigurable))
                {
                    $configurable = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
                    if(!$configurable)
                    {
                        $data['data'][] = array('message' => $product->getSku().' does not belong to a configurable product');
                    }
                    else
                    {
                        $product = Mage::getModel('catalog/product')->load($configurable[0]);
                        $skip=false;
                        foreach($product->getWebsiteIds() as $websiteId)
                        {
                            if($websiteId == $currentWebsiteId)
                            {
                                $skip=true;
                            }
                        }
                        if(!$skip)
                        {
                            $data['data'][] = array('message' => $product->getSku().' does not have the correct channel for all its children');
                        }
                    }
                }
            }
        }
        $data['error'] = count($data['data']) > 0 ? true : false;
        return $this->jsonResponse($data);
    }

    /**
     * Check for enabled products which are missing sap or ean code
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function stockableitems_without_sap_ean()
    {
        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', array('in' => $this->getAttributeSetIdsByName(['voice_device','data_device', 'accessoire','simcard'])))
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addAttributeToFilter([
                ['attribute' => 'is_deleted', 'null' => true],
                ['attribute' => 'is_deleted', 'neq' => '1'],
            ]);
        foreach($collection as $product)
        {
            if(is_null($product->getIdentifierSapCodes()) || is_null($product->getIdentifierEanOrUpcCode()))
            {
                $data['data'][] = array('message' => $product->getSku().' with SAP ('.$product->getIdentifierSapCodes().') and EAN ('.$product->getIdentifierEanOrUpcCode().')');
            }
        }
        $data['error'] = count($data['data']) > 0 ? true : false;
        return $this->jsonResponse($data);
    }

    /**
     * Check for subscription products which are missing the maf or aansluikosten price
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function subscription_without_maf_connection_costs()
    {
        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', array('in' => $this->getAttributeSetIdsByName(['voice_subscription','data_subscription'])))
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addAttributeToFilter([
                ['attribute' => 'is_deleted', 'null' => true],
                ['attribute' => 'is_deleted', 'neq' => '1'],
            ]);
        foreach($collection as $product)
        {
            if(is_null($product->getMaf()) || is_null($product->getPrijsAansluitkosten()))
            {
                $data['data'][] = array('message' => $product->getSku().' does not have a valid MAF and/or connection price');
            }
        }
        $data['error'] = count($data['data']) > 0 ? true : false;
        return $this->jsonResponse($data);
    }

    /**
     * Check for stockable products which have no stock info
     * @param bool $checkStockable
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function stockableitems_are_stockable($checkStockable = true)
    {
        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', array($checkStockable ? 'in' : 'nin' => $this->getAttributeSetIdsByName(['accessoire', 'data_device','dummy_device','prepaid_simonly','simcard','voice_device'])))
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addAttributeToFilter([
                ['attribute' => 'is_deleted', 'null' => true],
                ['attribute' => 'is_deleted', 'neq' => '1'],
            ]);
        $valueToCheck = $checkStockable ? 1 : 0;
        foreach($collection as $product)
        {
            if($product->getData('identifier_axi_stockable_prod') != $valueToCheck)
            {
                $data['data'][] = array('message' => $product->getSku().' is ' . ($checkStockable ? 'not' : '') . ' set to AXI stockable');
            }
        }
        $data['error'] = count($data['data']) > 0 ? true : false;
        return $this->jsonResponse($data);
    }

    /**
     * Check for non-stockable products which have stock info in the database
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function stockableitems_are_not_stockable()
    {
        return $this->stockableitems_are_stockable(false);
    }

    /**
     * Check for products that have ean code but with incorrect format/value
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function products_with_wrong_ean_length()
    {
        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', array('nin' => $this->getAttributeSetIdsByName(['accessoire', 'data_device', 'prepaid_simonly', 'simcard', 'voice_device'])))
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addAttributeToFilter([
                ['attribute' => 'is_deleted', 'null' => true],
                ['attribute' => 'is_deleted', 'neq' => '1'],
            ]);
        foreach($collection as $product)
        {
            $parts = explode(',',$product->getIdentifierEanOrUpcCode());
            foreach($parts as $ean)
            {
                $ean = trim($ean);
                $length = strlen($ean);
                if($length != 0 && ($length < 12 || $length > 13))
                {
                    $data['data'][] = array('message' => $product->getSku().' has incorrect EAN length ('.$length.').');
                }
            }
        }
        $data['error'] = count($data['data']) > 0 ? true : false;
        return $this->jsonResponse($data);
    }

    /**
     * Check for products that have sap code but with incorrect format/value
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function products_with_wrong_sap_length()
    {
        $ids = $this->getAttributeSetIdsByName(['accessoire', 'data_device', 'dummy_device', 'prepaid_simonly','service_item', 'simcard', 'voice_device']);

        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', array('nin' => $ids))
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addAttributeToFilter([
                ['attribute' => 'is_deleted', 'null' => true],
                ['attribute' => 'is_deleted', 'neq' => '1'],
            ]);
        foreach($collection as $product)
        {
            $parts = explode(',',$product->getIdentifierSapCodes());
            foreach($parts as $sap)
            {
                $sap = trim($sap);
                $length = strlen($sap);
                if($length != 0 && $length != 9)
                {
                    $data['data'][] = array('message' => $product->getSku().' has incorrect SAP length ('.$length.').');
                }
            }
        }
        $data['error'] = count($data['data']) > 0 ? true : false;
        return $this->jsonResponse($data);
    }

    /**
     * Check for duplicate ean/sap codes (same ean/sap codes which are set for multiple different products)
     * @return Vznl_Validate_Adminhtml_ValidatebackendController
     */
    protected function duplicate_ean_or_sap()
    {
        $eanAttibuteCode = Mage::getModel('eav/config')->getAttribute('catalog_product', 'identifier_ean_or_upc_code')->getId();
        $sapAttibuteCode = Mage::getModel('eav/config')->getAttribute('catalog_product', 'identifier_sap_codes')->getId();
        $statusAttributeCode = Mage::getModel('eav/config')->getAttribute('catalog_product', 'status')->getId();

        /** @var Dyna_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')
            ->addStoreFilter()
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addAttributeToFilter([
                ['attribute' => 'identifier_ean_or_upc_code', 'neq' => ""],
                ['attribute' => 'identifier_sap_codes', 'neq' => ""],
            ])
            ->addAttributeToFilter([
                ['attribute' => 'is_deleted', 'null' => true],
                ['attribute' => 'is_deleted', 'neq' => '1'],
            ])
        ;

        $processed = [];
        $sapProcessed = [];
        $sql = "SELECT v.entity_id FROM catalog_product_entity_varchar v
        INNER JOIN catalog_product_entity_int i ON i.entity_id = v.entity_id
        WHERE (i.attribute_id = :status_id AND i.value = :status_value) AND
        (v.attribute_id = :id AND (FIND_IN_SET(:space_value, v.value) <> 0 OR FIND_IN_SET(:value, v.value) <> 0));";
        $skuSql = "SELECT sku FROM catalog_product_entity WHERE entity_id = :id";
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');

        foreach ($collection as $product) {

            $parts = explode(',', $product->getIdentifierEanOrUpcCode());
            foreach ($parts as $ean) {
                $ean = trim($ean);
                $length = strlen($ean);
                if ($length == 0) {
                    continue;
                }
                if  (! isset($processed[$ean])) {
                    $processed[$ean] = 1;
                    $queryResult = $read->query($sql, [
                        ':id' => $eanAttibuteCode,
                        ':space_value' => ' ' . $ean,
                        ':value' => $ean,
                        ':status_id' => $statusAttributeCode,
                        ':status_value' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
                    ]);
                    if ($queryResult->rowCount() > 1) {
                        $codes = '';
                        while ($row = $queryResult->fetch()) {
                            if (! isset($productSku[$row['entity_id']])) {
                                $sku = $read->fetchOne($skuSql, [":id" => $row['entity_id']]);
                                $productSku[$row['entity_id']] = $sku;
                            } else {
                                $sku = $productSku[$row['entity_id']];
                            }
                            $codes .= $sku ? $sku . ', ' : $row['entity_id'] . ', ';
                        }
                        $error = array();
                        $error['message'] = sprintf('EAN: "%s" is seen in CID %s', $ean, rtrim($codes, ', '));
                        $data['data'][] = $error;
                    }
                }
            }

            $sapCodes = explode(',', $product->getIdentifierSapCodes());
            foreach ($sapCodes as $sap) {
                $sap = trim($sap);
                $length = strlen($sap);
                if ($length == 0) {
                    continue;
                }
                if  (! isset($sapProcessed[$sap])) {
                    $sapProcessed[$sap] = 1;
                    $queryResult = $read->query($sql, [
                        ':id' => $sapAttibuteCode,
                        ':space_value' => ' ' . $sap,
                        ':value' => $sap,
                        ':status_id' => $statusAttributeCode,
                        ':status_value' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
                    ]);
                    if ($queryResult->rowCount() > 1) {
                        $codes = '';
                        while ($row = $queryResult->fetch()) {
                            if (! isset($productSku[$row['entity_id']])) {
                                $sku = $read->fetchOne($skuSql, [":id" => $row['entity_id']]);
                                $productSku[$row['entity_id']] = $sap;
                            } else {
                                $sku = $productSku[$row['entity_id']];
                            }
                            $codes .= $sku ? $sku . ', ' : $row['entity_id'] . ', ';
                        }
                        $error = array();
                        $error['message'] = sprintf('SAP: "%s" is seen in CID %s', $sap, rtrim($codes, ', '));
                        $data['data'][] = $error;
                    }
                }
            }
        }
        $data['error'] = count($data['data']) > 0 ? true : false;

        return $this->jsonResponse($data);
    }

    /**
     * Check for missing mixmatch rules for postpaid devices and subscriptions
     */
    protected function check_missing_mixmatch()
    {
        $deviceSubscriptionIds = $this->getAttributeSetIdsByName(['voice_device','data_device','voice_subscription','data_subscription']);

        $data = ['error' => false, 'data' => []];
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');

        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
        $type = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, Dyna_Catalog_Model_Type::TYPE_MOBILE);

        $deviceAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'identifier_device_post_prepaid');
        $deviceType = Mage::helper('dyna_catalog')->getAttributeAdminLabel($deviceAttribute, null, 'postpaid');


        $deviceCollection =  Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('')
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addFieldToFilter('attribute_set_id', ['in' => $deviceSubscriptionIds])
            ->addAttributeToFilter('identifier_device_post_prepaid', $deviceType)
            ->addAttributeToFilter([
                ['attribute' => 'is_deleted', 'null' => true],
                ['attribute' => 'is_deleted', 'neq' => '1'],
            ]);
        $deviceCollection->getSelect()->join('catalog_product_entity_varchar', sprintf('catalog_product_entity_varchar.entity_id = e.entity_id AND catalog_product_entity_varchar.attribute_id = %s AND FIND_IN_SET(%s,catalog_product_entity_varchar.value) AND catalog_product_entity_varchar.store_id != 0', $attribute->getAttributeId(), $type), '')
            ->join('core_store', 'catalog_product_entity_varchar.store_id = core_store.store_id', '')
            ->join('core_website', 'core_store.website_id = core_website.website_id', 'name')
            ->joinLeft('catalog_mixmatch', '(catalog_mixmatch.target_sku = e.sku or catalog_mixmatch.source_sku = e.sku ) and catalog_mixmatch.website_id = core_store.website_id', '')
            ->where('catalog_mixmatch.website_id IS NULL');
        $queryResult = $read->query($deviceCollection->getSelectSql(true))->fetchAll();
        foreach ($queryResult as $product) {
            $data['data'][] = ['message' => sprintf('CID: "%s" missing mixmatch in website "%s"', $product['sku'], $product['name'])];
        }

        $data['error'] = count($data['data']) > 0 ? true : false;

        return $this->jsonResponse($data);
    }

    /**
     * Get attribute set ids by name. If name is not provided, return all ids
     * @param null $attributeSetNames
     */
    protected function getAttributeSetIdsByName($attributeSetNames = null)
    {
        $ids = [];
        if(!isset($this->_attributeSetCollection)){
            $this->_attributeSetCollection = Mage::getModel("eav/entity_attribute_set")->getCollection();
        }
        foreach($this->_attributeSetCollection as $attributeSet){
            if(!$attributeSetNames || in_array($attributeSet->getAttributeSetName(), $attributeSetNames)){
                $ids[] = $attributeSet->getId();
            }
        }
        return $ids;
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = $this->getDynaCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);

        return $this;
    }

    /**
     * retrieve
     * @param $websiteCode
     * @return bool
     */
    private function getWebsiteIdByCode($websiteCode)
    {
        if (is_null($this->_websites)) {
            $this->_websites = Mage::app()->getWebsites(true, true);
        }
        if (isset($this->_websites[$websiteCode])) {
            return $this->_websites[$websiteCode]->getId();
        }
        return false;
    }

    /**
     * Retrieve store object by code
     *
     * @param string $store
     * @return Mage_Core_Model_Store
     */
    public function getStoreIdByCode($store)
    {
        if (is_null($this->_stores)) {
            $this->_stores = Mage::app()->getStores(true, true);
        }
        if (isset($this->_stores[$store])) {
            return $this->_stores[$store]->getId();
        }
        return false;
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }
}
