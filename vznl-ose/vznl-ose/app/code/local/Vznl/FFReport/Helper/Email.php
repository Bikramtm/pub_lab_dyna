<?php

class Vznl_FFReport_Helper_Email extends Mage_Core_Helper_Abstract
{
    const EMAIL_EVENT_REQUEST_GENERATED = "received_request_to_generate_ff_code";
    const EMAIL_EVENT_REQUESTER_BLOCKED = "ff_requester_blocked";
    const EMAIL_EVENT_NO_RELATION_FOUND = "ff_requester_unknown";
    const EMAIL_EVENT_FF_CODE_GENERATED = "ff_code_generated";
    const EMAIL_EVENT_FF_CODE_REMINDER = "send_ff_code_use_reminder";
    const EMAIL_EVENT_FAILED_REQUESTS_REPORT = "send_failed_requests_to_sales_manager";
    const EMAIL_EVENT_FF_CODES_USAGE_REPORT = "send_ff_usage_to_sales_manager";
    const EMAIL_EVENT_FF_CODE_USED = "ff_code_used";
    const EMAIL_EVENT_FF_USAGE_TO_REQUESTER = "send_ff_usage_to_requester";

    public function send($event, array $data)
    {
        Mage::dispatchEvent($event, $data);
    }

    /**
     * Trigger event to send email when a request was generated
     *
     * @param Vznl_FFHandler_Model_Request $request
     * @param bool $resend
     */
    public function requestSuccessfullyGenerated(Vznl_FFHandler_Model_Request $request, $resend = false)
    {
        $this->send(self::EMAIL_EVENT_REQUEST_GENERATED, array("request" => $request, "resend" => $resend));
    }

    /**
     * Trigger event to send email when a requester is blocked
     *
     * @param $email
     * @param $adminEmail
     */
    public function emailBlocked($email, $adminEmail = null)
    {
        // Currently disabled
        return;

        // $this->send(self::EMAIL_EVENT_REQUESTER_BLOCKED, array("params" => array("requesterEmail" => $email, "adminEmail" => $adminEmail)));
    }

    /**
     * Trigger event to send email when no relation was found for request
     *
     * @param $requestModel
     * @param $params
     */
    public function noRelationFound($requestModel, $params)
    {
        $this->send(self::EMAIL_EVENT_NO_RELATION_FOUND, array(
            'request_params' => $params,
            "request" => $requestModel
        ));
    }

    /**
     * Trigger event to send email when the request was completed (generated)
     *
     * @param Vznl_FFHandler_Model_Request $request
     */
    public function ffCodeGenerated(Vznl_FFHandler_Model_Request $request)
    {
        $this->send(self::EMAIL_EVENT_FF_CODE_GENERATED, array(
            "request" => $request
        ));
    }

    /**
     * Trigger event to send email to remind the requester and end-user that the ff code should be used
     *
     * @param Vznl_FFHandler_Model_Request $request
     */
    public function ffCodeUseReminder(Vznl_FFHandler_Model_Request $request)
    {
        $this->send(self::EMAIL_EVENT_FF_CODE_REMINDER, array(
            "request" => $request
        ));
    }

    /**
     * Trigger event to send email to remind the requester and end-user that the ff code should be used
     *
     * @param array $requestersFFCodes
     */
    public function dispatchFFCodeUsageOverview($requestersFFCodes)
    {
        $this->send(self::EMAIL_EVENT_FF_USAGE_TO_REQUESTER, array(
            "requester_codes" => $requestersFFCodes
        ));
    }

    /**
     * Trigger event to send email when an order is place in Friends & Family website
     *
     * @param Dyna_Superorder_Model_Superorder $superorder
     * @param Vznl_FFHandler_Model_Request $request
     */
    public function ffCodeUsed(Dyna_Superorder_Model_Superorder $superorder, Vznl_FFHandler_Model_Request $request)
    {
        $this->send(self::EMAIL_EVENT_FF_CODE_USED, array(
            "order" => $superorder,
            "request" => $request
        ));
    }
}
