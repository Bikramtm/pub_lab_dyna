<?php
/* delete duplicate comp rule for fixed */
$installer = $this;
$installer->startSetup();
$collection = Mage::getModel("package/packageType")->getCollection()->addFieldToSelect('entity_id');
$packageId = array_column($collection->getData(), 'entity_id');
$blankCollection = Mage::getModel('productmatchrule/rule')->getCollection()
    ->addFieldToSelect('product_match_rule_id')
    ->addFieldToSelect('rule_title')
    ->addFieldToSelect('package_type')
    ->addFieldToFilter('package_type', array('nin' => $packageId));

foreach ($blankCollection->getData() as $blank) {
    if (is_object($blank)) {
        $blank->delete();
    } else if ($blank['product_match_rule_id']) {
        $compRule = Mage::getModel('productmatchrule/rule')->load($blank['product_match_rule_id']);
        $compRule->delete();
    }
}
$installer->endSetup();