<?php

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

if ($connection->isTableExists('product_match_whitelist_index') && $connection->tableColumnExists('product_match_whitelist_index', 'package_type')) {
    // change package_type from tinyint to int
    $sql =
        <<<SQL
ALTER TABLE `product_match_whitelist_index` MODIFY `package_type` INT;
SQL;

    $connection->query($sql);
}

$installer->endSetup();