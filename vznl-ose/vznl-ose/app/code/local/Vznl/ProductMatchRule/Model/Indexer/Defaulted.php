<?php

/**
 * Class Vznl_ProductMatchRule_Model_Indexer_Defaulted
 */
class Vznl_ProductMatchRule_Model_Indexer_Defaulted extends Dyna_ProductMatchRule_Model_Indexer_Defaulted
{
    /**
     * @param $rule Dyna_ProductMatchRule_Model_Rule
     */
    protected function processItem($rule)
    {
        $websiteIds = $rule->getWebsiteIds();

        switch ($rule['operation_type']) {
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allProductIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    $this->addDynaConditionalCategoryOpTypeP2P($rule, $websiteId);
                }
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (!in_array($rule['right_id'], $this->_allProductIds)
                    || !in_array($rule['left_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    $this->addDynaConditionalCategoryOpTypeC2P($rule, $websiteId);
                }
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) { break; }
                foreach ($websiteIds as $websiteId) {
                    $this->addDynaConditionalCategoryOpTypeP2C($rule, $websiteId);
                }
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (!in_array($rule['left_id'], $this->_allCategoryIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) { break; }
                foreach ($websiteIds as $websiteId) {
                    $this->addDynaConditionalCategoryOpTypeC2C($rule, $websiteId);
                }
                break;
            default:
                break;
        }
    }

    /**
     * @param $ruleId
     * @param $packageId
     * @param $left
     * @param $right
     * @param $websiteId
     */
    protected function _addDynaConditionalCategory($ruleId, $packageId, $left, $right, $websiteId)
    {
        $this->_matches[] = array(self::ADD_MANDATORY, array($websiteId, $ruleId, $packageId, $left, $right));
        $this->_assertMemory();
    }

    protected function _insertMultipleCategories()
    {
        $values = '';

        $add = array();

        foreach ($this->_prev as $_key => &$_row) {
            /**
             * $_row [0] = add_action
             * $_row [1][0] = website_id
             * $_row [1][1] = rule_id
             * $_row [1][2] = package_id
             * $_row [1][3] = product_id
             * $_row [1][4] = category_id
             * $_row [1][5] = operation
             * $_row [1][6] = process_context_ids
             *
             */
            if(isset($_row[1][6])) {
                $row6 = $_row[1][6];
            }
            else {
                $row6 = null;
            }

            $add[$_row[1][0]][] = array($_row[1][3], $_row[1][4], $_row[1][1], $row6);
            unset($this->_prev[$_key]);
        }
        unset($_row);

        foreach ($add as $websiteId => &$combinations) {

            foreach ($combinations as $key => &$combination) {
                $values .= sprintf('("%s","%s","%s","%s"),', $websiteId, $combination[0], $combination[1], $combination[3]);

                if (strlen($values) >= $this->_maxPacketsLength) {
                    $sql = sprintf(
                        'INSERT INTO `%s` (`website_id`,`product_id`,`category_id`, `process_context_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                        $this->_mandatoryTable,
                        trim($values, ',')
                    );

                    $this->_query = $this->getConnection()->query($sql);

                    $this->_inserted += $this->_query->rowCount();
                    $this->closeConnection();

                    unset($sql);
                    $values = '';
                }
                unset($combinations[$key]);
            }
            unset($combination);
            unset($add[$websiteId]);
        }
        unset($combinations);
        unset($add);

        if ($values) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`product_id`,`category_id`, `process_context_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                $this->_mandatoryTable,
                trim($values, ',')
            );

            unset($values);
            $this->_query = $this->getConnection()->query($sql);

            $this->_inserted += $this->_query->rowCount();
            $this->closeConnection();
            unset($sql);
        }
    }
}