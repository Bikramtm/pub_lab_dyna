<?php

/**
 * Class Vznl_ProductMatchRule_Helper_Data
 */
class Vznl_ProductMatchRule_Helper_Data extends Dyna_ProductMatchRule_Helper_Data
{
    /**
     * Get removal not allowed product ids by evaluating service to product, service to category and service to service rules
     * @param $selectedProductsIds
     * @param $processContextId
     * @param $packageType
     * @return array
     * @internal param $selectedProducts
     */
    public function getServiceNotRemovableIds($selectedProductsIds, $processContextId, $packageType)
    {
        $mandatory = array();
        $websiteId = (int)Mage::app()->getWebsite()->getId();

        /** @var Dyna_Configurator_Model_Expression_Cart $cartModel */
        $cartModel = Mage::getModel('dyna_configurator/expression_cart');
        $cartModel->appendCurrentProducts($selectedProductsIds);

        $inputParams = [
            'customer' => Mage::getModel('dyna_configurator/expression_customer'),
            'cart' => $cartModel,
            'availability' => Mage::getModel('dyna_configurator/expression_availability'),
            'serviceability' => Mage::getModel('vznl_configurator/expression_serviceability'),
            'agent' => Mage::getModel('dyna_configurator/expression_agent'),
            'installBase' => Mage::getModel('dyna_configurator/expression_installBase'),
        ];

        /** @var Dyna_Configurator_Model_Expression_Catalog $catalogObject */
        $catalogObject = Mage::getModel('dyna_configurator/expression_catalog', $packageType);

        /** @var $dynaCoreHelper Dyna_Core_Helper_Data */
        $dynaCoreHelper = Mage::helper('dyna_core');

        /** @var Dyna_ProductMatchRule_Model_Resource_Rule_Collection $ruleCollection */
        $ruleCollection = Mage::getModel('dyna_productmatchrule/rule')
            ->getCollection();
        $ruleCollection
            // Make sure the rules are filtered by the requested process context
            ->join(array('productMatchRuleProcessContext' => 'dyna_productmatchrule/productMatchRuleProcessContext'), 'main_table.product_match_rule_id=productMatchRuleProcessContext.rule_id', null)
            // Make sure rules are of type service (legacy Omnius rules should be already evaluated at this point)
            ->addFieldToFilter('source_type', array('eq' => 1))
            ->addFieldToFilter('operation_type', array(
                    'in' => array(
                        Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P,
                        Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C,
                        Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2S
                    )
                )
            )
            // Make sure rules have a service expression that can be evaluated
            ->addFieldToFilter('operation', array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_REMOVAL_NOTALLOWED))
            ->addFieldToFilter('service_source_expression', array('notnull' => true))
            ->addFieldToFilter('productMatchRuleProcessContext.process_context_id', array('eq' => $processContextId))
            ->addFieldToFilter('effective_date', array(
                array('to' => date("Y-m-d"), 'date' => true),
                array('null' => true)
            ))
            ->addFieldToFilter('expiration_date', array(
                array('from' => date("Y-m-d"), 'date' => true),
                array('null' => true)
            ))
            ->distinct(true);

        if ($packageType) {
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getSingleton("dyna_package/packageType")
                ->loadByCode($packageType);
            if ($packageTypeModel->getId()) {
                $ruleCollection->addFieldToFilter("main_table.package_type", array('eq' => (int)$packageTypeModel->getId()));
            } else {
                Mage::throwException("Got package type value but cannot find resource in Allowed Service Rules call.");
            }
        } else {
            Mage::throwException("Empty package type in Allowed Service Rules call");
        }

        $ruleCollection->getSelect()
            ->join(array('rule_website' => 'product_match_rule_website'), 'rule_website.rule_id = main_table.product_match_rule_id', null)
            ->where('rule_website.website_id = ?', $websiteId)
            ->order('main_table.priority ASC');

        $errors = array();

        /** @var Dyna_ProductMatchRule_Model_Rule $rule */
        foreach ($ruleCollection as $rule) {
            try {
                if ($dynaCoreHelper->evaluateExpressionLanguage($rule->getServiceSourceExpression(), $inputParams)) {
                    if ($rule->getOperationType() == $rule::OP_TYPE_S2S) {
                        $targetedProductsExpression = trim($rule->getServiceTargetExpression());
                        $products = $dynaCoreHelper->evaluateExpressionLanguage($targetedProductsExpression, array(
                            // Let the evaluator know what kind of operation is evaluating
                            'catalog' => $catalogObject,
                            'cart' => $cartModel,
                        ));
                        $products = array_values($products);
                    } else {
                        $products = $this->getIndexedRemovalNotAllowed($rule->getId(), $websiteId, $selectedProductsIds);
                    }
                    // Intersect with selected product ids as service to service might return unselected product ids
                    $mandatory = array_merge($mandatory, array_intersect($products, $selectedProductsIds));
                }
            } catch (Exception $e) {
                // Error
                $errors[] = '[Rule ID = ' . $rule->getId() . '] ' . $e->getMessage();
            }
        }

        if (count($errors) > 0) {
            $rule->log('[ERROR] Found ' . count($errors) . ' errors within the rules');
        }

        $messages = '';
        foreach ($errors as $key => $value) {
            $messages = sprintf("%s [RULE] \n Expression: %s \n Message: %s \n", $messages, $key, $value);
        }

        if (strlen($messages) > 0) {
            $rule->log($messages);
        }

        return $mandatory;
    }

    /**
     * @return array|bool
     */
    public function getServiceCompRulesObjects()
    {
        if ($this->serviceCompRulesObjects === false) {
            $this->serviceCompRulesObjects = array(
                'customer' => Mage::getModel('vznl_configurator/expression_customer'),
                'availability' => Mage::getModel('dyna_configurator/expression_availability'),
                'agent' => Mage::getModel('dyna_configurator/expression_agent'),
                'installBase' => Mage::getModel('vznl_configurator/expression_installBase'),
                'activeInstallBase' => Mage::getModel('dyna_configurator/expression_activeInstallBase'),
                'serviceability' => Mage::getModel('vznl_configurator/expression_serviceability'),
            );
        }

        return $this->serviceCompRulesObjects;
    }
}