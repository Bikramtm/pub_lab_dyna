<?php

/**
 * Class Vznl_ProductMatchRule_Helper_ArrayRuleCache
 */
class Vznl_ProductMatchRule_Helper_ArrayRuleCache extends Dyna_ProductMatchRule_Helper_ArrayRuleCache
{
    public function __construct()
    {
        $this->maxWebSiteLength = 2 ** $this->websiteBits - 1;
    }

    public function setWebsiteBits($bits)
    {
        $this->websiteBits = $bits;
        $this->maxWebSiteLength = 2 ** $this->websiteBits - 1;
    }
}