<?php

use Psr\Log\LoggerInterface;
use Zend\Soap\Client;

/**
 * Class Vznl_Coc_Adapter_Stub_AdapterFactory
 */
class Vznl_Coc_Adapter_Stub_AdapterFactory
{
    /**
     * Create an adapter with the given variables
     * @param array $options An array with options
     * @return Vznl_Coc_Adapter_Default_DefaultAdapter The adapter
     */
    public static function create(array $options=[])
    {
        return new Vznl_Coc_Adapter_Stub_Adapter($options);
    }
}
