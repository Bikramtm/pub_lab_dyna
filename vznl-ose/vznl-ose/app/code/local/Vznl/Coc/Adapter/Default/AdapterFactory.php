<?php

use Psr\Log\LoggerInterface;
use Zend\Soap\Client;

/**
 * Class Vznl_Coc_Adapter_Default_AdapterFactory
 */
class Vznl_Coc_Adapter_Default_AdapterFactory
{
    /**
     * Create an adapter with the given variables
     * @param array $options An array with options
     * @return Vznl_Coc_Adapter_Default_DefaultAdapter The adapter
     */
    public static function create(array $options=[])
    {
        $options = array_merge(self::getDefaultOptions(), $options);
        return new Vznl_Coc_Adapter_Default_Adapter($options['wsdl'], $options);
    }

    /**
     * @return array
     */
    protected static function getDefaultOptions()
    {
        $options = Mage::helper('omnius_service')->getZendSoapClientOptions('coc') ? : [];

        /** @var Vznl_Coc_Helper_Data $cocHelper */
        $cocHelper = Mage::helper('coc');
        $options['wsdl'] = $cocHelper->getCocConfig('wsdl');
        $options['endpoint'] = $cocHelper->getCocConfig('usage_url');
        $options['soap_version'] = SOAP_1_2;
        $options['trace'] = true;
        $options['connection_timeout'] = $cocHelper->getCocConfig('timeout');
        $options['keep_alive'] = false;

        return $options;
    }
}
