<?php

/**
 * Class Vznl_Coc_Adapter_Coc_AdapterFactory
 */
class Vznl_Coc_Adapter_Coc_AdapterFactory
{
    /**
     * Create an adapter with the given variables
     * @param array $options An array with options
     * @return Vznl_Coc_Adapter_Default_DefaultAdapter The adapter
     */
    public static function create(array $options=[])
    {
        $options = array_merge(self::getDefaultOptions(), $options);
        return new Vznl_Coc_Adapter_Coc_Adapter($options);
    }

    /**
     * @return array
     */
    protected static function getDefaultOptions()
    {
        /** @var Vznl_Coc_Helper_Data $cocHelper */
        $cocHelper = Mage::helper('coc');

        $clientOptions = [
            'verify' => ($cocHelper->getCocConfig('ssl_peer') == 1),
        ];
        if ($cocHelper->getCocConfig('proxy_usage') == 1) {
            $proxyUrl = $cocHelper->getCocConfig('proxy_url') ? : '';
            $clientOptions['proxy'] = $proxyUrl;
        }

        $serviceUrl = rtrim($cocHelper->getCocConfig('usage_url'), '/');
        return array(
            'service_url' => $serviceUrl,
            'client_options' => $clientOptions
        );
    }
}
