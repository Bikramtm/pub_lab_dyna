<?php

/**
 * Class Vznl_Warmer_Model_Builder
 */
class Vznl_Warmer_Model_Builder
{
    /** @var Dyna_Core_Helper_Data|null */
    private $_dynaCoreHelper = null;
    /** @var Vznl_Warmer_Helper_Data|null */
    private $_warmerHelper = null;

    /** @var Varien_Simplexml_Element */
    protected $_config;

    /** @var array */
    protected $_warmers;

    /**
     * @return Dyna_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('dyna_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * @return Dyna_Core_Helper_Data
     */
    protected function getWarmerHelper()
    {
        if ($this->_warmerHelper === null) {
            $this->_warmerHelper = Mage::helper('warmer');
        }

        return $this->_warmerHelper;
    }

    /**
     * @param bool $verbose
     * @param bool $includeHeavy
     */
    public function buildCache($verbose = false, $includeHeavy = false)
    {
        Mage::app()->getCache()->clean();
        Mage::getConfig()->reinit();
        $time = microtime(true);
        $this->updateFrontendCachePrefix();

        // Log process start
        $this->getWarmerHelper()->write(sprintf(
            '--- Started at %s. Current keys in cache: %s ---',
            date('Y-m-d H:i:s'),
            count(Mage::app()->getCache()->getIds())
        ), $verbose);

        Mage::getSingleton('core/translate')->setLocale('nl_NL')->init('frontend', true);
        /** @var Vznl_Warmer_Model_Warmer_BaseWarmer $warmer */
        foreach ($this->_gatherWarmers() as $warmer) {
            try {
                $warmer->warmCache($verbose, $includeHeavy);
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
            }
        }

        // Log process end
        $this->getWarmerHelper()->write(sprintf(
            '--- Ended at %s. Current keys in cache: %s. Total process took %ss ---%s',
            date('Y-m-d H:i:s'),
            count(Mage::app()->getCache()->getIds()),
            number_format(microtime(true) - $time, 3),
            PHP_EOL
        ), $verbose);
    }

    /**
     * @return $this
     */
    protected function updateFrontendCachePrefix()
    {
        Mage::getConfig()->saveConfig(Dyna_Cache_Model_Cache::ENVIRONMENT_FRONTEND_CACHE_PREFIX, $prefix = md5(time() . rand(0, 1000)));
        return $this;
    }

    /**
     * @return array
     */
    protected function _gatherWarmers()
    {
        if (is_null($this->_warmers)) {
            $warmers = array();
            foreach ($this->getDynaCoreHelper()->toArray($this->_getXmlConfig()->xpath('warmers/warmer')) as $warmer) {
                if ($warmerInst = Mage::getModel(sprintf('warmer/warmer_%s', strtolower($warmer['class'])))) {
                    $warmers[] = array(
                        'instance' => $warmerInst,
                        'priority' => isset($warmer['priority']) ? $warmer['priority'] : 100,
                    );
                }
            }

            usort($warmers, array($this, 'sortWarmers'));

            $this->_warmers = array_map(
                function($el)
                {
                    return $el['instance'];
                },
                $warmers
            );
        }
        return $this->_warmers;
    }

    /**
     * @param $a
     * @param $b
     * @return int
     */
    public function sortWarmers($a, $b)
    {
        if ($a['priority'] == $b['priority']) {
            return 0;
        }
        return ($a['priority'] < $b['priority']) ? -1 : 1;
    }

    /**
     * @return Varien_Simplexml_Element
     */
    protected function _getXmlConfig()
    {
        if ( ! $this->_config) {
            return $this->_config = new Varien_Simplexml_Element(file_get_contents(Mage::getModuleDir('etc', 'Vznl_Warmer') . DS . 'warmers.xml'));
        }
        return $this->_config;
    }
} 
