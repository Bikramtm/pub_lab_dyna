<?php

/**
 * Class Dyna_Warmer_Model_Warmer_Rules
 */
class Vznl_Warmer_Model_Warmer_Rules extends Vznl_Warmer_Model_Warmer_BaseWarmer
{
    /**
     * @param bool $verbose
     * @param bool $includeHeavy
     * @param Dyna_Sandbox_Model_Release $release
     * @return mixed|void
     */
    public function warmCache($verbose = false, $includeHeavy = false, Dyna_Sandbox_Model_Release $release = null)
    {
        $params = array();
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $rulesHelper = Mage::helper('dyna_configurator/rules');
        $warmerStartTime = microtime(true);

        $this->getDynaWarmerHelper()->write('--- Starting rules warmer ---', $verbose);
        if ($this->_debugMode) {
            $this->logDebug('>>> ################################ <<<');
            $this->logDebug('>>> ##  Rules cache warmer stats (includeHeavy=' . (int)$includeHeavy . ') and release ' . ($release ? $release->getId() : 'not set') . '  ## <<<');
            $this->logDebug('>>> ################################ <<<');
        }

        $storeIds = Mage::app()->getStores();
        $packageTypes = array_map('strtolower', array_keys(Mage::helper('dyna_package')->getPackageTypes()));

        foreach ($storeIds as $storeId => $store) { /** @var Mage_Core_Model_Store $store */
            $websiteId = $store->getWebsiteId();
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

            foreach ($packageTypes as $packageType) {
                $time = microtime(true);
                $rulesHelper->getPreconditionRules([], $packageType, $websiteId);
                $duration = microtime(true) - $time;

                $outputParams = array();
                $outputParams['Store name'] = $store->getName();
                $outputParams['Package type'] = strtoupper($packageType);
                $outputParams['duration'] = number_format($duration, 3);

                $this->getDynaWarmerHelper()->write(sprintf(
                    '> Store %s - caching rules for package type: %s, took: %ss',
                    ...array_values($outputParams)
                ), $verbose);

                if ($this->_debugMode) {
                    $this->collectDebugParams($outputParams);
                }
            }

            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }

        $this->getDynaWarmerHelper()->write(sprintf(
            '--- Finished rules warmer. Duration: %ss ---',
            number_format(microtime(true) - $warmerStartTime, 3)
        ), $verbose);

        if ($this->_debugMode) {
            $this->logDebugParams();
        }
    }
}
