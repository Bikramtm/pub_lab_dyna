<?php

/**
 * Class Vznl_Sales_Helper_Data
 */
class Vznl_Sales_Helper_Data extends Dyna_Sales_Helper_Data
{
    /**
     * @param array $options
     * @return mixed
     */
    public function additionalValidation(array $options = [])
    {
        $this->handleBasketId();
        return Mage::helper('vznl_validatecart')->validate();
    }

    /**
     * @return null|array
     */
    protected function handleBasketId(): ?array
    {
        try {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $customerPan = $quote->getCustomer()->getData('pan');
            $basketId = $quote->getBasketId();

            // Check if there is some basket already assigned to the customer
            if (!empty($customerPan) && empty($basketId)) {
                $context = Mage::helper('dyna_catalog/processContext')->getProcessContextCode()
                    ?? Dyna_Catalog_Model_ProcessContext::ILSBLU;
                switch ($context) {
                    case Vznl_Catalog_Model_ProcessContext::MOVE:
                        $orderType = Vznl_Configurator_Model_PackageType::ORDER_TYPE_MOVE;
                        break;
                    case Vznl_Catalog_Model_ProcessContext::ILSBLU:
                    default:
                        $orderType = Vznl_Configurator_Model_PackageType::ORDER_TYPE_PRODUCT;
                        break;
                }
                $quote->setOrderType($orderType);
                $quote->save();
                $serviceAddressId = Mage::helper('vznl_configurator/inlife')->getServiceAddressId($quote->getId());
                $crossFootPrint = Mage::helper('vznl_configurator/inlife')->isCustomerMovingToDifferentAddress($orderType);

                $data = Mage::helper('vznl_getbasket')->getBasket(
                    $customerPan,
                    $serviceAddressId,
                    $crossFootPrint,
                    $orderType
                );
                if (!empty($data['error'])) {
                    $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode(
                        $data['code'],
                        $data['message'],
                        Vznl_GetBasket_Adapter_Peal_Adapter::name
                    );
                    return [
                        'error' => true,
                        'message' => $pealErrorCode['translation'],
                    ];
                }

                if (!empty($data['basketId'])) {
                    Mage::helper('vznl_validatecart')->setBasketId($data['basketId']);
                }
            }
        } catch (\Throwable $exception) {
            return null;
        }

        return null;
    }
}
