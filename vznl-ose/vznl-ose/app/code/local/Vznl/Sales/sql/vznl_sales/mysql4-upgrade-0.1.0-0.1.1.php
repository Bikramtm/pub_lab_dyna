<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->dropIndex(
    $installer->getTable('sales_flat_order'),
    'IDX_SALES_FLAT_ORDER_EDITED'
);

$installer->endSetup();
