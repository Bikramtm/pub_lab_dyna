<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ProductInfo_Model_Mysql4_Info extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("productinfo/info", "id");
    }
}
