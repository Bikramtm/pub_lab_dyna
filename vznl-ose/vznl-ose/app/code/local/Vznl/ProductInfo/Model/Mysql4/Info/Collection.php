<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ProductInfo_Model_Mysql4_Info_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Override constructor
     */
    public function _construct()
    {
        $this->_init("productinfo/info");
    }
}
