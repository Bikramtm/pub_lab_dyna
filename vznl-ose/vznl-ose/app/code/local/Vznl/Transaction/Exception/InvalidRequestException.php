<?php
class Vznl_Transaction_Exception_InvalidRequestException extends \Exception
{
    public function __construct($message = "", $code = 501, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}