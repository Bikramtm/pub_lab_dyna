<?php

class Vznl_Transaction_Model_Transaction extends Mage_Core_Model_Abstract implements JsonSerializable
{
    protected function _construct()
    {
        $this->_init("transaction/transaction");
    }

    /**
     * JsonSerialize method
     * @return array|mixed
     * @throws Varien_Exception
     */
    public function jsonSerialize()
    {
        return [
            'type' => $this->getType(),
            'httpcode' => $this->getHttpcode(),
            'error' => $this->getError(),
//            'agent' => $this->getAgent()->getUsername(),
//            'order' => $this->getOrder()->getIncrementId(),
            'created_at' => new DateTime($this->getCreatedAt()),
        ];
    }
}