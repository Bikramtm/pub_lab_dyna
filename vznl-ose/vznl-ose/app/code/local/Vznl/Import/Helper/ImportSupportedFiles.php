<?php

class Vznl_Import_Helper_ImportSupportedFiles
{
    protected $supportedFiles = [
        'Fixed_Package_Type.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_PACKAGE_TYPES
        ],
        'Fixed_PackageCreation.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_PACKAGE_CREATION_TYPES
        ],
        'Fixed_Bundle_Products.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_FIXED_PRODUCT
        ],
        'Fixed_Gen_Products.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_FIXED_PRODUCT
        ],
        'Fixed_Int_Products.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_FIXED_PRODUCT
        ],
        'Fixed_Tel_Products.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_FIXED_PRODUCT
        ],
        'Fixed_TV_Products.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_FIXED_PRODUCT
        ],
        'Fixed_Promotions.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_FIXED_PRODUCT
        ],
        'Fixed_CategoryTree.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_CATEGORIES_TREE
        ],
        'Fixed_Categories.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_CATEGORIES_TREE_PRODUCTS
        ],
        'Fixed_Comp_Rules.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_PRODUCT_MATCH_RULES
        ],
        'Fixed_Sales_Rules.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_FIXED_SALES_RULES
        ],
        'Fixed_Version.xml' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_LOGFILE
        ],
        'Fixed_Multimapper_peal.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MULTIMAPPER
        ],
        'Fixed_ProductFamily.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_PRODUCT_FAMILY
        ],
        'Fixed_ProductVersion.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_PRODUCT_VERSION
        ],
        'Mobile_Package_Type.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_PACKAGE_TYPES
        ],
        'Mobile_PackageCreation.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_PACKAGE_CREATION_TYPES
        ],
        'Mobile_Accessory.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MOBILE_PRODUCT
        ],
        'Mobile_Addon.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MOBILE_PRODUCT
        ],
        'Mobile_Devices.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MOBILE_PRODUCT
        ],
        'Mobile_DeviceRC.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MOBILE_PRODUCT
        ],
        'Mobile_Promotions.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MOBILE_PRODUCT
        ],
        'Mobile_Subscription.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MOBILE_PRODUCT
        ],
        'Mobile_CategoryTree.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MOBILE_PRODUCT
        ],
        'Mobile_Categories.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_CATEGORIES_TREE_PRODUCTS
        ],
        'Mobile_Comp_Rules.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_PRODUCT_MATCH_RULES
        ],
        'Mobile_Sales_Rules.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MOBILE_SALES_RULES
        ],
        'Mobile_Mixmatch.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MIXMATCH
        ],
        'Mobile_Multimapper_BSL.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_MULTIMAPPER
        ],
        'Mobile_Version.xml' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_LOGFILE
        ],
        'Mobile_ProductVersion.csv' => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_PRODUCT_VERSION
        ],
        'Mobile_ProductFamily.csv'  => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_MOBILE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_PRODUCT_FAMILY
        ],
        'Campaigns.xml'  => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_CAMPAIGN_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_CAMPAIGN
        ],
        'Campaign_Offers.xml'  => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_CAMPAIGN_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_CAMPAIGN_OFFER
        ],
        'Campaign_Version.xml'  => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_CAMPAIGN_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_LOGFILE
        ],
        'Bundles.xml'  => [
            Vznl_Import_Helper_Data::STACK_KEY => Vznl_Sandbox_Helper_Data::FILENAME_BUNDLE_KEYWORD,
            Vznl_Import_Helper_Data::IMPORT_TYPE_KEY => Vznl_Import_Helper_Data::TYPE_BUNDLE
        ]
    ];

    protected $patternFileNames = [
        'provis_to_omnius_<yyyymmdd>_<hh24miss>.csv' => '/^provis_to_omnius_\d\d\d\d(0[0-9]|1[0-2])(0[0-9]|1[0-9]|2[0-9]|3[0-1])_(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])([0-5][0-9])\.csv$/'
    ];

    public function isValid($file, $returnFileName = false)
    {
        $isValid = array_key_exists($file, $this->supportedFiles);
        if (!$isValid) {
            $isValid = $this->validatePatternFileName($file) !== false;
            $file = $this->validatePatternFileName($file);
        }

        return $returnFileName ? $file : $isValid;
    }

    /**
     * Validates provis file name with the following format: provis_to_omnius_<yyyymmdd>_<hh24miss>.csv
     *
     * @param $file
     * @return bool
     */
    public function validatePatternFileName($file)
    {
        foreach ($this->patternFileNames as $fileNamePattern => $regex) {
            if (preg_match($regex, $file) === 1) {
                return $fileNamePattern;
            }
        }

        return false;
    }

    public function getSupportedFiles(): array
    {
        return $this->supportedFiles;
    }

    public function getSupportedFilesFormatted(): string
    {
        return PHP_EOL.implode(PHP_EOL, array_keys($this->supportedFiles)).PHP_EOL;
    }

}
