<?php

/**
 * Class Vznl_Import_Helper_Data
 */
class Vznl_Import_Helper_Data extends Dyna_Import_Helper_Data
{
    const CATALOG_MNPFREEDAYS_IMPORT_LOG_FILE = 'mnpfreedays_import';

    //bundles
    const BUNDLE = 'Bundle';

    //campagain
    const CAMPAIGN = 'Campaigns';
    const CAMPAIGN_OFFERS = 'Campaign_Offers';
    const CAMPAIGN_VERSION = 'Campaign_Version';
    // Fixed
    const FIXED_PACKAGE_TYPE_CATEGORY = 'Fixed_Package_Type';
    const FIXED_PACKAGE_CREATION_CATEGORY = 'Fixed_PackageCreation';
    const FIXED_BUNDLE_PRODUCTS_CATEGORY = 'Fixed_Bundle_Products';
    const FIXED_GEN_PRODUCTS_CATEGORY = 'Fixed_Gen_Products';
    const FIXED_INT_PRODUCTS_CATEGORY = 'Fixed_Int_Products';
    const FIXED_TEL_PRODUCTS_CATEGORY = 'Fixed_Tel_Products';
    const FIXED_TV_PRODUCTS_CATEGORY = 'Fixed_TV_Products';
    const FIXED_PROMOTIONS_CATEGORY = 'Fixed_Promotions';
    const FIXED_CATEGORY_TREE = 'Fixed_CategoryTree';
    const FIXED_CATEGORY = 'Fixed_Categories';
    const FIXED_SALES_RULES = 'Fixed_Sales_Rules';
    const FIXED_MULTIMAPPER_PEAL = 'Fixed_Multimapper_peal';
    const FIXED_COMP_RULES = 'Fixed_Comp_Rules';
    const FIXED_VERSION = 'Fixed_Version';
    const FIXED_PRODUCT_FAMILY = 'Fixed_ProductFamily';
    const FIXED_PRODUCT_VERSION = 'Fixed_ProductVersion';
    // Mobile
    const MOBILE_PACKAGE_TYPE_CATEGORY = 'Mobile_Package_Type';
    const MOBILE_PACKAGE_CREATION_CATEGORY = 'Mobile_PackageCreation';
    const MOBILE_ACCESSORY_CATEGORY = 'Mobile_Accessory';
    const MOBILE_ADDON_CATEGORY = 'Mobile_Addon';
    const MOBILE_DEVICE_CATEGORY = 'Mobile_Devices';
    const MOBILE_DEVICERC_CATEGORY = 'Mobile_DeviceRC';
    const MOBILE_PROMOTION_CATEGORY = 'Mobile_Promotions';
    const MOBILE_SUBSCRIPTION_CATEGORY = 'Mobile_Subscription';
    const MOBILE_PRODUCT_FAMILY = 'Mobile_ProductFamily';
    const MOBILE_PRODUCT_VERSION = 'Mobile_ProductVersion';
    const MOBILE_CATEGORY_TREE = 'Mobile_CategoryTree';
    const MOBILE_CATEGORY = 'Mobile_Categories';
    const MOBILE_COMP_RULES = 'Mobile_Comp_Rules';
    const MOBILE_VERSION = 'Mobile_Version';
    const MOBILE_SALES_RULES = 'Mobile_Sales_Rules';
    const MOBILE_MULTIMAPPER_BSL = 'Mobile_Multimapper_BSL';
    const MOBILE_MIXMATCH = 'Mobile_Mixmatch';

    /* Log files */
    // Campaign
    const CAMPAIGN_LOG_FILE = 'Campaigns';
    const CAMPAIGN_OFFERS_LOG_FILE = 'Campaign_Offers';
    const CAMPAIGN_VERSION_LOG_FILE = 'Campaign_Version';
    // Fixed
    const FIXED_PACKAGE_TYPE_LOG_FILE = 'Fixed_Package_Type';
    const FIXED_PACKAGE_CREATION_LOG_FILE = 'Fixed_PackageCreation'; // naming it this way due to Fixed_PackageCreation.csv
    const FIXED_BUNDLE_PRODUCTS_LOG_FILE = 'Fixed_Bundle_Products';
    const FIXED_GEN_PRODUCTS_LOG_FILE = 'Fixed_Gen_Products';
    const FIXED_INT_PRODUCTS_LOG_FILE = 'Fixed_Int_Products';
    const FIXED_TEL_PRODUCTS_LOG_FILE = 'Fixed_Tel_Products';
    const FIXED_TV_PRODUCTS_LOG_FILE = 'Fixed_TV_Products';
    const FIXED_PROMOTIONS_LOG_FILE = 'Fixed_Promotions';
    const FIXED_CATEGORY_LOG_FILE = 'Fixed_Categories';
    const FIXED_CATEGORY_TREE_LOG_FILE = 'Fixed_CategoryTree'; // naming it this way due to Fixed_CategoryTree.csv
    const FIXED_SALES_RULES_LOG_FILE = 'Fixed_Sales_Rules';
    const FIXED_MULTIMAPPER_PEAL_LOG_FILE = 'Fixed_Multimapper_peal';
    const FIXED_COMP_RULES_LOG_FILE = 'Fixed_Comp_Rules';
    const FIXED_VERSION_LOG_FILE = 'fixed_version_import';
    const FIXED_PRODUCT_FAMILY_LOG_FILE = 'Fixed_ProductFamily'; // naming it this way due to Fixed_ProductFamily.csv
    const FIXED_PRODUCT_VERSION_LOG_FILE = 'Fixed_ProductVersion'; // naming it this way due to Fixed_ProductVersion.csv
    // Mobile
    const MOBILE_PACKAGE_TYPE_LOG_FILE = 'Mobile_Package_Type';
    const MOBILE_PACKAGE_CREATION_LOG_FILE = 'Mobile_PackageCreation'; // naming it this way due to Mobile_PackageCreation.csv
    const MOBILE_ACCESSORY_LOG_FILE = 'Mobile_Accessory';
    const MOBILE_ADDON_LOG_FILE = 'Mobile_Addon';
    const MOBILE_DEVICE_LOG_FILE = 'Mobile_Devices';
    const MOBILE_DEVICERC_LOG_FILE = 'Mobile_DeviceRC';
    const MOBILE_PROMOTION_LOG_FILE = 'Mobile_Promotions';
    const MOBILE_SUBSCRIPTION_LOG_FILE = 'Mobile_Subscription';
    const MOBILE_PRODUCT_FAMILY_LOG_FILE = 'Mobile_ProductFamily'; // naming it this way due to Mobile_ProductFamily.csv
    const MOBILE_PRODUCT_VERSION_LOG_FILE = 'Mobile_ProductVersion'; // naming it this way due to Mobile_ProductVersion.csv
    const MOBILE_MIXMTACH_LOG_FILE = 'Mobile_Mixmatch';
    const MOBILE_CATEGORY_TREE_LOG_FILE = 'Mobile_CategoryTree';
    const MOBILE_CATEGORY_LOG_FILE = 'Mobile_Categories';
    const MOBILE_COMP_RULES_LOG_FILE = 'Mobile_Comp_Rules';
    const MOBILE_VERSION_LOG_FILE = 'Mobile_Version';
    const MOBILE_SALES_RULES_LOG_FILE = 'Mobile_Sales_Rules';
    const MOBILE_MULTIMAPPER_BSL_LOG_FILE = 'Mobile_Multimapper_BSL';
    //Bundle
    const BUNDLE_LOG_FILE = 'Bundle';

    // Import types
    const TYPE_FIXED_PRODUCT = 'fixedProduct';
    const TYPE_FIXED_SALES_RULES = 'fixedSalesRules';
    const TYPE_MOBILE_PRODUCT = 'mobileProduct';
    const TYPE_MOBILE_SALES_RULES = 'mobileSalesRules';
    const TYPE_PACKAGE_TYPES = 'packageTypes';
    const TYPE_PACKAGE_CREATION_TYPES = 'packageCreationTypes';
    const TYPE_CATEGORIES_TREE = 'categoriesTree';
    const TYPE_CATEGORIES_TREE_PRODUCTS = 'categoriesTreeProducts';
    const TYPE_PRODUCT_MATCH_RULES = 'productMatchRules';
    const TYPE_LOGFILE = 'logfile';
    const TYPE_MULTIMAPPER = 'multimapper';
    const TYPE_PRODUCT_FAMILY = 'productFamily';
    const TYPE_PRODUCT_VERSION = 'productVersion';
    const TYPE_MIXMATCH = 'mixmatch';
    const TYPE_CAMPAIGN = 'campaign';
    const TYPE_CAMPAIGN_OFFER = 'campaignOffer';
    const TYPE_BUNDLE = 'bundles';

    const STACK_KEY = 'stack';
    const IMPORT_TYPE_KEY = 'import_type';

    const COMMON_FIXED = 'Fixed-stack_import';
    const COMMON_FIXED_LOG = 'Fixed-stack_import';
    const COMMON_MOBILE = 'Mobile-stack_import';
    const COMMON_MOBILE_LOG = 'Mobile-stack_import';
    const COMMON_CAMPAIGN = 'Campaign-stack_import';
    const COMMON_CAMPAIGN_LOG = 'Campaign-stack_import';

    /**
     * @return array
     */
    public function getCategoriesToFiles()
    {
        return [
            self::FIXED_TV_PRODUCTS_CATEGORY => self::FIXED_TV_PRODUCTS_LOG_FILE,
            self::FIXED_TEL_PRODUCTS_CATEGORY => self::FIXED_TEL_PRODUCTS_LOG_FILE,
            self::FIXED_GEN_PRODUCTS_CATEGORY => self::FIXED_GEN_PRODUCTS_LOG_FILE,
            self::FIXED_BUNDLE_PRODUCTS_CATEGORY => self::FIXED_BUNDLE_PRODUCTS_LOG_FILE,
            self::FIXED_INT_PRODUCTS_CATEGORY => self::FIXED_INT_PRODUCTS_LOG_FILE,
            self::FIXED_PROMOTIONS_CATEGORY => self::FIXED_PROMOTIONS_LOG_FILE,
            self::FIXED_PACKAGE_TYPE_CATEGORY => self::FIXED_PACKAGE_TYPE_LOG_FILE,
            self::FIXED_PACKAGE_CREATION_CATEGORY => self::FIXED_PACKAGE_CREATION_LOG_FILE,
            self::FIXED_SALES_RULES => self::FIXED_SALES_RULES_LOG_FILE,
            self::FIXED_MULTIMAPPER_PEAL => self::FIXED_MULTIMAPPER_PEAL_LOG_FILE,
            self::FIXED_COMP_RULES => self::FIXED_COMP_RULES_LOG_FILE,
            self::FIXED_CATEGORY => self::FIXED_CATEGORY_LOG_FILE,
            self::FIXED_CATEGORY_TREE => self::FIXED_CATEGORY_TREE_LOG_FILE,
            self::FIXED_VERSION => self::FIXED_VERSION_LOG_FILE,
            self::FIXED_PRODUCT_FAMILY => self::FIXED_PRODUCT_FAMILY_LOG_FILE,
            self::FIXED_PRODUCT_VERSION => self::FIXED_PRODUCT_VERSION_LOG_FILE,
            self::MOBILE_PACKAGE_TYPE_CATEGORY => self::MOBILE_PACKAGE_TYPE_LOG_FILE,
            self::MOBILE_PACKAGE_CREATION_CATEGORY => self::MOBILE_PACKAGE_CREATION_LOG_FILE,
            self::MOBILE_ACCESSORY_CATEGORY => self::MOBILE_ACCESSORY_LOG_FILE,
            self::MOBILE_ADDON_CATEGORY => self::MOBILE_ADDON_LOG_FILE,
            self::MOBILE_DEVICE_CATEGORY => self::MOBILE_DEVICE_LOG_FILE,
            self::MOBILE_DEVICERC_CATEGORY => self::MOBILE_DEVICERC_LOG_FILE,
            self::MOBILE_PROMOTION_CATEGORY => self::MOBILE_PROMOTION_LOG_FILE,
            self::MOBILE_SUBSCRIPTION_CATEGORY => self::MOBILE_SUBSCRIPTION_LOG_FILE,
            self::MOBILE_PRODUCT_FAMILY => self::MOBILE_PRODUCT_FAMILY_LOG_FILE,
            self::MOBILE_PRODUCT_VERSION => self::MOBILE_PRODUCT_VERSION_LOG_FILE,
            self::MOBILE_COMP_RULES => self::MOBILE_COMP_RULES_LOG_FILE,
            self::MOBILE_VERSION => self::MOBILE_VERSION_LOG_FILE,
            self::MOBILE_MULTIMAPPER_BSL => self::MOBILE_MULTIMAPPER_BSL_LOG_FILE,
            self::MOBILE_MIXMATCH => self::MOBILE_MIXMTACH_LOG_FILE,
            self::MOBILE_SALES_RULES => self::MOBILE_SALES_RULES_LOG_FILE,
            self::MOBILE_CATEGORY => self::MOBILE_CATEGORY_LOG_FILE,
            self::MOBILE_CATEGORY_TREE => self::MOBILE_CATEGORY_TREE_LOG_FILE,
            self::CAMPAIGN => self::CAMPAIGN_LOG_FILE,
            self::CAMPAIGN_OFFERS => self::CAMPAIGN_OFFERS_LOG_FILE,
            self::CAMPAIGN_VERSION => self::CAMPAIGN_VERSION_LOG_FILE,
            self::COMMON_FIXED => self::COMMON_FIXED_LOG,
            self::COMMON_MOBILE => self::COMMON_MOBILE_LOG,
            self::COMMON_CAMPAIGN => self::COMMON_CAMPAIGN_LOG,
            self::BUNDLE => self::BUNDLE_LOG_FILE
        ];
    }

    /**
     * Set product visibility (change from default visibility if dealer groups are provided)
     * @param $product
     * @param $importData
     */
    public function setProductVisibilityStrategy(&$product, $importData)
    {
        if (!empty($importData['group_ids'])) {
            $product->setProductVisibilityStrategy(
                json_encode(array(
                    'strategy' => in_array($importData['strategy'], [
                        Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL,
                        Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE,
                        Vznl_ProductFilter_Model_Filter::STRATEGY_EXCLUDE
                    ]) ? $importData['strategy'] : Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL,
                    'groups' => $importData['group_ids'],
                ))
            );
        }
    }

    /**
     * Get all websites codes as a string
     * Eg: retail,indirect,telesales
     * @return string
     */
    public function getAllWebsitesCodes()
    {
        $websitesCodes = '';
        $websites = Mage::app()->getWebsites();
        foreach ($websites as $website) {
            $websitesCodes .= $website->getCode() . ',';
        }
        return rtrim($websitesCodes, ',');
    }

    /**
     * Get header for type
     * @param string $type
     * @return mixed
     */
    public function getHeader($type)
    {        
        $headers = [
            //products (mobile, fixed, cable)
            'product_template' => [
                'required' => [
                    'sku',
                    'name',
                    'package_type_id',
                    'package_subtype_id',
                    'vat',
                    'status',
                    'product_visibility',                    
                    'attribute_set',
                ],
                'optional' => [
                    'description',
                    'price',
                    'maf',
                    'sorting',
                    'effective_date',
                    'expiration_date',
                    'product_type',
                    'prijs_maf_new_amount',
                    'promo_new_price',
                    'price_discount',
                    'maf_discount',
                    'discount_period_amount',
                    'discount_period',
                    'checkout_product',
                    'confirmation_text',
                    'ask_confirmation_first',
                    'initial_selectable',
                    'hierarchy_value',
                    'contract_value',
                    'lifecycle_status',
                    'display_name_inventory',
                    'display_name_configurator',
                    'display_name_cart',
                    'display_name_communication',
                    'product_version_id',
                    'product_family_id',
                    'product_segment',
                    'type_other_addons',
                    'template_360',
                    'bundle_hint',
                    'has_internet',
                    'int_bundle_type',
                    'download_speed',
                    'upload_speed',
                    'has_dtv',
                    'dtv_bundle_level',
                    'dtv_bundle_type',
                    'tv_product',
                    'tv_content',
                    'tv_nr_channels',
                    'tv_box_type',
                    'tv_box',
                    'has_fixed_tel',
                    'voice_product',
                    'voice_plan',
                    'voice_plan_applies_over',
                    'bundle_identifier',
                    'identifier_required_footprint',
                    'int_security_licenses',
                    'int_security_licenses_tag',
                    'is_fixed_ip_tier',
                    'is_ccr_benefit_eligible_part',
                    'is_ccr_benefit_product',
                    'price_increase_maf',
                    'price_increase_scope',
                    'requires_serviceability',
                    'required_serviceability_items',
                    'channel',
                    'additional_text',
                    'serial_number_type',
                    'capture_ean',
                    'identifier_sap_codes',

                    //product_groups_visibility
                    'strategy',
                    'group_ids',

                    //Mobile subscription specific attributes
                    'axi_los_verkoopbaar',
                    'hawaii_subscr_device_price',
                    'hawaii_subscr_family',
                    'hawaii_subscr_kind',
                    'hide_original_price',
                    'identifier_axi_stockable_prod',
                    'identifier_hawaii_id',
                    'identifier_hawaii_subscr_orig',
                    'identifier_subsc_postp_simonly',
                    'identifier_subscr_brand',
                    'identifier_subscr_category',
                    'identifier_subscr_type',
                    'maf_discount_percent',
                    'new_price',
                    'new_price_date',
                    'prijs_aansluitkosten',
                    'prijs_belminuut_buiten_bundel',
                    'prijs_mb_buiten_bundel',
                    'prijs_sms_buiten_bundel',
                    'prodspecs_4g_lte',
                    'prodspecs_aantal_belminuten',
                    'prodspecs_brand',
                    'prodspecs_data_aantal_mb',
                    'prodspecs_hawaii_channel',
                    'prodspecs_hawaii_def_phone',
                    'prodspecs_hawaii_usp1',
                    'prodspecs_hawaii_usp2',
                    'prodspecs_hawaii_usp3',
                    'prodspecs_network',
                    'prodspecs_sms_amount',
                    'prodspecs_soccode',
                    'prodspecs_wms_codes',
                    'special_from_date',
                    'stack',//should be removed, the value is determined from import file name
                    'tags',
                    'tax_class_id',//should be removed, the value is determined based on vat header value
                    'use_service_value',
                    'visibility',
                    'identifier_simcard_allowed',
                    'prijs_aansluitkosten_ret',

                    //Mobile device specific attributes
                    'axi_safety_stock',
                    'axi_threshold',
                    'hawaii_def_subscr_cbu',
                    'hawaii_def_subscr_ebu',
                    'identifier_ean_or_upc_code',
                    'identifier_hawaii_type',
                    'identifier_hawaii_type_name',
                    'prijs_thuiskopieheffing_type',
                    'prodspecs_2e_camera',
                    'prodspecs_3g',
                    'prodspecs_accu_verwisselb',
                    'prodspecs_afmetingen_breedte',
                    'prodspecs_afmetingen_dikte',
                    'prodspecs_afmetingen_lengte',
                    'prodspecs_afmetingen_unit',
                    'prodspecs_audio_out',
                    'prodspecs_batterij',
                    'prodspecs_belmin_sms_config',
                    'prodspecs_besturingssysteem',
                    'prodspecs_bluetooth',
                    'prodspecs_bluetooth_version',
                    'prodspecs_camera',
                    'prodspecs_data_mb_config',
                    'prodspecs_display_features',
                    'prodspecs_filmen_in_hd',
                    'prodspecs_flitser',
                    'prodspecs_gprs_edge',
                    'prodspecs_gps',
                    'prodspecs_hawaii_htmlkleurcode',
                    'prodspecs_hawaii_os_versie',
                    'prodspecs_hawaii_preorder',
                    'prodspecs_hawaii_speed_max',
                    'prodspecs_hdmi',
                    'prodspecs_kleur',
                    'prodspecs_megapixels_1e_cam',
                    'prodspecs_nfc',
                    'prodspecs_opslag_gb',
                    'prodspecs_opslag_uitbreidbaar',
                    'prodspecs_prepaid_simlock',
                    'prodspecs_processor',
                    'prodspecs_schermdiagonaal_inch',
                    'prodspecs_schermresolutie',
                    'prodspecs_spreektijd',
                    'prodspecs_standby_tijd',
                    'prodspecs_touchscreen',
                    'prodspecs_vfwallet',
                    'prodspecs_weight_gram',
                    'prodspecs_werkgeheugen_mb',
                    'prodspecs_wifi',
                    'prodspecs_wifi_frequenties',
                    'sap_code',
                    'special_price',
                    'special_to_date',
                    'stock_status',

                    //Mobile deviceRC specific attributes
                    'loan_indicator',
                    'identifier_commitment_months',

                    //Mobile addon specific attributes
                    'addon_type',
                    'is_deleted',
                    'sku_type',
                    'thumbnail_label',
                    'updated_at',
                    'url_path',


                    //Mobile accessories specific attrbutes
                    'meta_description',
                    'promo_description',
                    'short_description',

                    //Mobile promotion specific attributes
                    'percentagepromotion',
                    'price_discount_percent',
                    'prijs_aansluit_promo_bedrag',
                    'prijs_aansluit_promo_procent',
                    'prijs_promo_duur_maanden',
                    'aikido',
                    'identifier_device_post_prepaid',
                    'identifier_simcard_type',
                    'identifier_simcard_formfactor',
                    'identifier_simcard_selector',
                    'product_specifications',
                    'configurable_id',
                    'configurable_attributes',
                    'configurable_sku',
                    
                    // Fixed product listed in optional
                    'portfolioidentifier',
                    'belongs_to_family',
                    'fixed_int_part',
                    'fixed_tv_part',
                    'fixed_tel_part',
                    'product_status_online'
                ],
                'mapping' => [
                    'package_type_id' => 'package_type',
                    'package_subtype_id' => 'package_subtype',
                    'product_family_id' => 'product_family',
                    'product_version_id' => 'product_version',
                    'sap_code' => 'identifier_sap_codes',
                    'addon_type' => 'identifier_addon_type',
                ]
            ],
            'mixMatch' => [
                'required'=>[
                    'source_sku',
                    'target_sku',
                    'source_category',
                    'target_category',
                    'effective_date',
                    'subscriber_segment',
                    'price',
                    'maf',
                    'channel',
                ],
                'optional' => [
                    'expiration_date',
                    'priority',
                    'stream',
                    'agent_id',
                    'device_subscription_sku',
                ]
            ],
            'categories' => [
                'required' => [
                    'sku',
                    'is_family',
                    'is_anchor',
                    'mutually_exclusive',
                    'category_tree',
                ],
                'optional' =>[
                    'family_category_sorting',
                    'description',
                    'hide_not_selected_products'
                ],
                'mapping' => [
                    'family_category_sorting' => 'family_position'
                ]
            ],
            'categories_tree' => [
                'required' => [
                    'external_category_id',
                    'category_name',
                    'external_parent_category_id',
                    'family_display_name'
                ],
                'optional' =>[
                    'is_family',
                    'is_anchor',
                    'mutually_exclusive',
                    'family_category_sorting',
                    'hide_not_selected_products',
                    'description'
                ]
            ],
            'categories_products' => [
                'required' => [
                    'sku',
                    'external_category_id'
                ],
                'optional' =>[
                    'position'
                ]
            ],
            'salesRules' => [
                'required' => [
                    'rule_title',
                    'rule_description',
                    'priority',
                    'is_promo',
                    'is_active',
                    'action',
                    'action_scope',
                    'action_scope_category',
                    'based_on_product',
                    'based_on_category',
                    'channel',
                    'based_on_process_context',
                    'sr_uniqueid'
                ],
                'optional' =>[
                    'effective_date',
                    'expiration_date',
                    'maf_discount',
                    'maf_discount_percent',
                    'discount_period_amount',
                    'discount_period',
                    'price_discount',
                    'price_discount_percent',
                    'usage_discount',
                    'usage_discount_percent',
                    'service_source_expression',
                    'show_hint_once',
                    'message_text',
                    'message_title',
                    'rule_type',
                    'coupon_type',
                    'package_type_id',
                    'promotion_label',
                    'discount_code',
                    'uses_per_coupon',
                    'uses_per_customer',
                    'publish_in_rss_feed',
                    'coupon_discount_type',
                    'coupon_discount_size',
                    'maximum_qty_discount',
                    'discount_shelf_quantity',
                    'apply_to_shipping_amount',
                    'free_shipping',
                    'stop_further_rules_processing'
                ],
                'mapping' => [
                    'based_on_process_context' => 'process_context',
                    'sr_uniqueid' => 'sr_unique_id'
                ]
            ],
            'multimapper' => [
                'required' => [
                    'sku',
                    'process_context',
                ],
                'optional' =>[
                    'service_expression',
                    'priority',
                    'stop_execution',
                    'technicalid1',
                    'naturecode1',
                    'technicalid2',
                    'naturecode2',
                    'technicalid3',
                    'naturecode3',
                    'backend',
                    'default_backend',
                    'component_type',
                    'direction',
                    'bom_id'
                ],
                'mapping' => [
                    "technicalid1" => "technical_id",
                    "naturecode1" => "nature_code",
                    "technicalid2" => "technical_id_2",
                    "naturecode2" => "nature_code_2",
                    "technicalid3" => "technical_id_3",
                    "naturecode3" => "nature_code_3",
                ]
            ],
            'mobile_multimapper' => [
                'required' => [
                    'pp_sku',
                    'process_context',
                ],
                'optional' =>[
                    'pp_sku_description',
                    'oms_bo_code',
                    'oms_bo_type',
                    'promo_sku',
                    'promo_sku_description',
                    'oms_pbo_code',
                    'oms_pbo_type',
                    'oms_product_offering_code',
                    'oms_product_offering_id',
                    'oms_product_code',
                    'oms_mc_code',
                    'oms_comp_code_path',
                    'oms_comp_id',
                    'oms_component_attr_1',
                    'oms_attr_code_1',
                    'oms_attr_value_1',
                    'oms_component_attr_2',
                    'oms_attr_value_2',
                    'oms_attr_code_2',
                    'oms_component_attr_3',
                    'oms_attr_code_3',
                    'oms_attr_value_3',
                    'oms_component_attr_4',
                    'oms_attr_code_4',
                    'oms_attr_value_4',
                    'oms_component_attr_5',
                    'oms_attr_code_5',
                    'oms_attr_value_5',
                    'oms_component_attr_6',
                    'oms_attr_code_6',
                    'oms_attr_value_6',
                    'oms_component_attr_7',
                    'oms_attr_code_7',
                    'oms_attr_value_7',
                    'oms_component_attr_8',
                    'oms_attr_code_8',
                    'oms_attr_value_8',
                    'oms_component_attr_9',
                    'oms_attr_code_9',
                    'oms_attr_value_9',
                    'oms_component_attr_10',
                    'oms_attr_code_10',
                    'oms_attr_value_10',
                    'service_expression',
                    'priority',
                    'direction',
                ]
            ],
            'importPackage' => [
                'required' => [
                    'package_type_id',
                    'package_subtype_id',
                    'package_type_name',
                    'package_subtype_name',
                    'package_subtype_gui_sorting',
                    'default_cardinality',
                    'package_subtype_gui_sorting',
                ],
                'optional'=>[
                    'package_subtype_visibility',
                    'ignored_by_compatibility_rules',
                    'acq_cardinality',
                    'ilsblu_cardinality',
                    'move_cardinality',
                    'lifecycle_status',
                    'manual_override_allowed',
                    'package_subtype_lifecycle_status',
                    'retblu_cardinality',
                ],
                'mapping' => [
                    "package_type_id" => "package_type_code",
                    "package_subtype_id" => "package_subtype_code",
                    "package_subtype_gui_sorting" => "package_subtype_position",
                    "default_cardinality" => "cardinality",
                ]
            ],
            'packageCreationTypes' => [
                'required' => [
                    'package_creation_type_id',
                    'package_type_id',
                    'filter_attributes',
                    'package_creation_name',
                ],
                'optional' => [
                    'sorting',
                    'package_creation_grouping',
                    'package_creation_grouping_sorting',
                    'package_subtype_filter_attributes',
                ]
            ],
            'productMatchRules' => [
                'required' => [
                    'rule_title',
                    'source_type',
                    'target_type',
                    'source',
                    'source_collection',
                    'service_source_expression',
                    'service_target_expression',
                    'target',
                    'operation',
                    'priority',
                    'channel',
                    'rule_origin',
                    'process_context',
                    'operation_type',
                    'effective_date',
                    'package_type_id'
                ],
                'optional' => [
                    'expiration_date',
                    'rule_description',
                    'override_initial_selectable',
                    'ask_confirmation_first',
                    'confirmation_text'
                ],
                'mapping' => [
                    'package_type_id'=>'package_type',
                    'source' => 'left_id',
                    'operation' => 'operation',
                    'target' => 'right_id',
                    'operation_type' => 'operation_type',
                    'priority' => 'priority',
                    'channel' => 'website_id',
                    'rule_title' => 'rule_title',
                    'rule_description' => 'rule_description',
                    'rule_origin' => 'rule_origin',
                    'process_context' => 'process_context',
                    'service_source_expression' => 'service_source_expression',
                    'service_target_expression' => 'service_target_expression',
                    'source_collection' => 'source_collection',
                    'source_type' => 'source_type',
                    'target_type' => 'target_type',
                    'effective_date' => 'effective_date',
                    'expiration_date' => 'expiration_date',
                    'override_initial_selectable' => 'override_initial_selectable'
                ]
            ],
            'dealerCampaign' =>[
                'required' => [
                    'void',
                    'allowed/notallowedfor',
                    'campaign_id'
                ],
                'mapping' => [
                    'void' => 'vf_dealer_code',
                    'allowed/notallowedfor' => 'allowed',
                    'campaign_id' => 'name'
                ]
            ],
            'serviceProviders' => [
                'required' => [
                    "telcompany",
                    "osf gui name",
                    "cableprovider",
                ],
                'optional' => [
                    'haftungsfreistellung_vorh'
                ],
                'mapping' => [
                    'telcompany' => 'code',
                    'osf gui name' => 'name',
                    'cableprovider' => 'cable_provider',
                    'haftungsfreistellung_vorh' => 'fixed_line',
                ]
            ],
            //(not used yet)
            'pre_conf_packages_template' => [
                'package_id',
                'package_type_id',
                'package_name',
                'sku',
                'website',
                'optional_campaign_code'
            ],
            'agent' => [
                'required' => [
                    'username',
                    'password',
                    'is_active',
                    'role_id',
                    'dealer_code',
                    'store_id',
                ],
                'optional' => [
                    'first_name',
                    'last_name',
                    'email',
                    'phone',
                    'employee_number',
                ],
            ],
            'checkoutProducts' => [
                'required' => [
                    'sku',
                    'attribute_set',
                    'package_type_id',
                    'package_subtype_id',
                    'name',
                    'option_type',
                    'display_label_checkout',
                    'option_ranking',
                    'price',
                    'product_visibility'
                ]
            ],
            'productFamily' =>[
                'required'=>[
                    'product_family_id',
                ],
                'optional'=>[
                    'lifecycle_status',
                    'product_family_name',
                    'product_version_id',
                    'package_type_id',
                    'package_subtype_id'
                ]
            ],
            'productVersion' =>[
                'required'=>[
                    'product_version_id',
                ],
                'optional'=>[
                    'lifecycle_status',
                    'product_family_id',
                    'product_version_name',
                    'package_type_id',
                ]
            ],
            'optionTypes' =>[
                'required'=>[
                    'option_type'
                ],
                'optional'=>[
                    'display_label_checkout'
                ]
            ],
            'communicationVariables' =>[
                'required'=>[
                    'communication_variable'
                ],
                'optional'=>[
                    'message'
                ]
            ]
        ];        
        return $headers[$type];
    }

    /**
     * Prevent extra columns for being imported
     *
     * @param $data
     */
    public function preventExtraColumns(&$data)
    {
        if (isset($this->_logColumns['extra']) && !empty($this->_logColumns['extra'])) {
            foreach ($this->_logColumns['extra'] as $column) {
                unset($data[$column]);
            }
        }
    }
}