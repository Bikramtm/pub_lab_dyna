<?php

class Vznl_GetOrderStatus_Helper_Data extends Vznl_Core_Helper_Data
{
    /*
    * @return String
    * */
    public function getAdapterChoice()
    {
        return Mage::getStoreConfig('vodafone_service/get_order_status/adapter_choice');
    }

    /*
    * @return string peal endpoint url
    * */
    public function getEndPoint()
    {
        return Mage::getStoreConfig('vodafone_service/get_order_status/end_point');
    }

    /*
    * @return string peal username
    * */
    public function getLogin()
    {
        return Mage::getStoreConfig('vodafone_service/get_order_status/login');
    }

    /*
    * @return string peal password
    * */
    public function getPassword()
    {
        return Mage::getStoreConfig('vodafone_service/get_order_status/password');
    }

    /*
    * @return string peal cty
    * */
    public function getCountry()
    {
        return Mage::getStoreConfig('vodafone_service/get_order_status/country');
    }

    /*
    * @return string peal channel
    * */
    public function getChannel()
    {
        $agentHelper = Mage::helper('agent');
        $salesChannel = $agentHelper->getDealerSalesChannel();
        if ($salesChannel) {
            return trim($salesChannel);
        } else {
            return trim(Mage::getStoreConfig('vodafone_service/get_order_status/channel'));
        }
    }

    /*
    * @return object omnius_service/client_stubClient
    * */
    public function getStubClient()
    {
        return Mage::getModel('omnius_service/client_stubClient');
    }

    /*
    * @return boolean peal verify
    * */
    public function getVerify()
    {
        return Mage::getStoreConfigFlag('vodafone_service/get_order_status/verify');
    }

    /*
    * @return boolean peal proxy
    * */
    public function getProxy()
    {
        return Mage::getStoreConfigFlag('vodafone_service/get_order_status/proxy');
    }

    /*
     * @return object Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /*
    * @param $orderNumber
    * @return array peal/stub response based on adpter choice
    * */
    public function getOrderStatus($orderNumber)
    {
        $factoryName = $this->getAdapterChoice();
        $validAdapters = Mage::getSingleton('getOrderStatus/system_config_source_getOrderStatus_adapter')->toArray();
        if (!in_array($factoryName, $validAdapters)) {
            return 'Either no adapter is chosen or the chosen adapter is no longer/not supported!';
        }
        $adapter = $factoryName::create();
        return $adapter->call($orderNumber);
    }
}
