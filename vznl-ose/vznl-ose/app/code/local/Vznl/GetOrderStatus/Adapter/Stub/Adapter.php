<?php

/**
 * Class Vznl_GetOrderStatus_Adapter_Stub_Adapter
 *
 */
class Vznl_GetOrderStatus_Adapter_Stub_Adapter
{
    CONST NAME = 'GetOrderStatus';
    /**
     * @param $orderNumber
     * @return $responseData
     */
    public function call($orderNumber, $arguments = array())
    {
        if (is_null($orderNumber)) {
            return 'No order number provided to adapter';
        }
        $stubClient = Mage::helper('vznl_getorderstatus')->getStubClient();
        $stubClient->setNamespace('Vznl_GetOrderStatus');
        $responseData = $stubClient->call(self::NAME, $arguments);
        Mage::helper('vznl_getorderstatus')->transferLog($arguments, $responseData, self::NAME);
        return $responseData;
    }
}