<?php

use GuzzleHttp\Client;

class Vznl_GetOrderStatus_Adapter_Peal_Factory
{
    public static function create()
    {
        $client = new Client();
        $helper = Mage::helper('vznl_getorderstatus');
        return new Vznl_GetOrderStatus_Adapter_Peal_Adapter(
            $client,
            $helper->getEndPoint(),
            $helper->getChannel(),
            $helper->getCountry()
        );
    }
}