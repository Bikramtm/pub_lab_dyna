<?php

class Vznl_GetOrderStatus_Adapter_Stub_Factory
{
    public static function create()
    {
        return new Vznl_GetOrderStatus_Adapter_Stub_Adapter();
    }
}