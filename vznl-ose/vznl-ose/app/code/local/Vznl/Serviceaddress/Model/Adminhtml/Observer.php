<?php
class Vznl_Serviceaddress_Model_Adminhtml_Observer
{
    public function checkCustomerServiceAddress($observer)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getCustomer();
        /** @var Mage_Core_Controller_Request_Http $request */
        $request = $observer->getRequest();
        $data = $request->getPost();
        if (isset($data['account']['default_serviceaddress'])) {
            $customer->setData('default_service_address_id', $data['account']['default_serviceaddress']);
        }
    }
}