<?php
/**
 * Class Vznl_GetTelephoneNumbers_Adapter_Stub_Adapter
 *
 */
class Vznl_GetTelephoneNumbers_Adapter_Stub_Adapter
{
    /**
     * @param $basketId
     * @return $responseData
     */
    public function call($validateAddress,$arguments = array())
    {
        if (is_null($validateAddress)) {
            return 'Address information not provided to adapter';
        }
        $name = 'GetTelephoneNumbers';
        $requestData =  $arguments;
        $stubClient = Mage::helper('vznl_getTelephoneNumbers')->getStubClient();
        $stubClient->setNamespace('Vznl_GetTelephoneNumbers');
        $responseData = $stubClient->call($name, $arguments);
        Mage::helper('vznl_getTelephoneNumbers')->transferLog($arguments, $responseData, $name);
        return $responseData;
    }
}