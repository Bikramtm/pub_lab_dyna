<?php

/**
 * Used in admin panel adapter selector for GetTelephoneNumbers service
 *
 */
class Vznl_GetTelephoneNumbers_Model_System_Config_Source_ConsecutiveNum_Adapter
{
    const SEQUENTIAL  = '1';
    const RANDOM = '0';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::SEQUENTIAL, 'label'=>Mage::helper('adminhtml')->__('Sequential')),
            array('value' => self::RANDOM, 'label'=>Mage::helper('adminhtml')->__('Random')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $optionArray = $this->toOptionArray();
        $array = [];
        foreach($optionArray as $option){
            $array[] = $option['value'];
        }
        return $array;
    }

}
