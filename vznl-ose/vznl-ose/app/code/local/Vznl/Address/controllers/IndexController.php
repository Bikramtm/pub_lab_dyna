<?php

/**
 * Class Vznl_Address_IndexController
 */

require_once Mage::getModuleDir('controllers', 'Dyna_Address') . DS . 'IndexController.php';

class Vznl_Address_IndexController extends Dyna_Address_IndexController
{
    public function clearServicesAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $confirmation = $this->getRequest()->getParam("confirmation");
        /** @var Vznl_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('vznl_package');
        // Confirmation needed for renewing service address because it will delete all packages
        if (!$confirmation && $packageHelper->hasServiceAbilityRequiredPackages()) {
            $response = [
                'error' => true,
                'requireConfirmation' => true,
                'message' => $this->__("Please note that by continuing packages that require serviceability check will be removed from cart."),
            ];
            $this->jsonResponse($response);
        } else {
            /** @var Vznl_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $triggerRefresh = false;
            foreach($quote->getCartPackages() as $package){
                if (($package->getServiceAddressId() || $package->getServiceAddressData()) && (!$package->getEditingDisabled())) {
                    Mage::helper('vznl_package')->removePackage(null, null, $package->getId());
                    // if at least one package has been deleted, send a flag to frontend to trigger page reload
                    $triggerRefresh = true;
                }
            }

            // collect totals
            Mage::getSingleton('checkout/cart')->save();
            Mage::helper('vznl_checkout')->createNewQuote();
            // Go further with renewing service address
            $this->getStorage()->unsServiceAbility();
            $this->getStorage()->unsAddress();
            $this->getStorage()->unsetData('addressData');
            $response = [
                'error' => false,
                'message' => $this->__("Ready for new serviceability check"),
                'triggerRefresh' => $triggerRefresh,
            ];

            $this->jsonResponse($response);
        }
    }

    public function getPealErrorTranslationAction() 
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $message = $this->getRequest()->getParam("message");
        $code = $this->getRequest()->getParam("code");
        $service = $this->getRequest()->getParam("service");
        $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($code, $message, $service);
        $response = [
                'error' => true,
                'message' => $pealErrorCode['translation']
            ];

        $this->jsonResponse($response);
    }
}
