<?php
// Create a new role Support if it doesn't exist
/** @var Mage_Admin_Model_Role $role */
$role = Mage::getModel('admin/role')->getCollection()->addFieldToFilter('role_name', "Support")->getFirstItem();
if (!$role->getId()) {
    $role->setParentId(0)
        ->setTreeLevel(1)
        ->setRoleType('G')
        ->setSortOrder(0)
        ->setRoleName("Support")
        ->save();
    $resources = explode(',', '__root__,admin/sales,admin/sales/order');
    $rules = Mage::getModel('admin/rules')->setRoleId($role->getId())->setResources($resources);
    Mage::getModel('admin/resource_rules')->saveRel($rules);
}
