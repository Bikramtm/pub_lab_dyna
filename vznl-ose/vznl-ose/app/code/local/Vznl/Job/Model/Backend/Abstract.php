<?php

/**
 * Class Vznl_Job_Model_Backend_Abstract
 */
abstract class Vznl_Job_Model_Backend_Abstract
{
    /**
     * Saves the item to the queue
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    abstract public function push(Vznl_Job_Model_Job_Abstract $job);

    /**
     * Saves the item to the queue after the given index (pivot)
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @param $pivot
     * @return mixed
     */
    abstract public function pushAfter(Vznl_Job_Model_Job_Abstract $job, $pivot);

    /**
     * Saves the item to the queue before the given index (pivot)
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @param $pivot
     * @return mixed
     */
    abstract public function pushBefore(Vznl_Job_Model_Job_Abstract $job, $pivot);

    /**
     * Retrieves the next item from the queue
     *
     * @return Vznl_Job_Model_Job_Abstract|null
     */
    abstract public function pop();

    /**
     * Retrieves the next X items from the queue
     *
     * @param int $count
     * @return mixed
     */
    abstract public function getNext($count = 1);

    /**
     * Returns the size of the queue
     *
     * @return mixed
     */
    abstract public function size();

    /**
     * Fetches the result
     *
     * @param string $uid
     * @return bool|string
     */
    abstract public function get($uid);

    /**
     * Saves the processed job
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    abstract public function set(Vznl_Job_Model_Job_Abstract $job);

    /**
     * Deletes the given job
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    abstract public function del(Vznl_Job_Model_Job_Abstract $job);

    /**
     * Deletes the job at the given index
     *
     * @param $index
     * @return mixed
     */
    abstract public function delAtPos($index);

    /**
     * Returns the list of keys present in the
     * current db
     *
     * @return mixed
     */
    abstract public function keys();

    /**
     * @param $list
     * @return mixed
     */
    abstract public function count($list);

    /**
     * @return mixed
     */
    abstract public function getConnection();

    /**
     * Return time since key was added
     *
     * @param string $uid
     * @return int
     */
    public function getKeyAge($uid)
    {
        return 0;
    }
}
