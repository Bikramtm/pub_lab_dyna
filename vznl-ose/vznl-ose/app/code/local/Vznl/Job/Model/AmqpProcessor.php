<?php

/**
 * Class Vznl_Job_Model_AmqpProcessor
 */
class Vznl_Job_Model_AmqpProcessor extends Vznl_Job_Model_Processor
{
    /**
     * Processes jobs present in the queue
     */
    public function process()
    {
        $this->_processed = 0;
        /** @var Vznl_Job_Model_Queue $queue */
        $queue = Mage::helper('job')->getQueue('amqp');
        $this->processQueue($queue);
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return bool
     */
    protected function shouldDelayJob($job)
    {
        /** Should not check whether the job should be delayed. */
        return false;
    }
}
