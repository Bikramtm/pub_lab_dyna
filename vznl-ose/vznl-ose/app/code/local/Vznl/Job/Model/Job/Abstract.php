<?php

/**
 * Class Vznl_Job_Model_Job_Abstract
 *
 * @method string getLastUpdated()
 * @method Vznl_Job_Model_Job_Abstract setException(Exception $e)
 * @method Vznl_Job_Model_Job_Abstract unsResult()
 * @method Vznl_Job_Model_Job_Abstract unsException()
 */
abstract class Vznl_Job_Model_Job_Abstract extends Varien_Object
{
    const PENDING = -1;
    const FAILED = 0;
    const SUCCESS = 1;

    public $hasPriority = false;
    /**
     * Init job
     */
    public function __construct()
    {
        parent::__construct();

        if ( ! $this->hasData('status')) {
            $this->setData('status', self::PENDING);
        }

        if ( ! $this->hasData('tries')) {
            $this->setData('tries', 0);
        }
    }

    /**
     * @param $result
     * @return $this
     */
    public function setResult($result)
    {
        $this->setData('result', $result);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->getData('result');
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->getData('status');
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $allowedStatuses = array(self::SUCCESS, self::FAILED, self::PENDING);
        if ( ! in_array($status, $allowedStatuses)) {
            throw new InvalidArgumentException(sprintf('Expected one of %s, got %s', join(', ', $allowedStatuses), (is_object($status) ? get_class($status) : $status)));
        }
        $this->setData('status', $status);
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->getData('status') == self::SUCCESS;
    }

    /**
     * @return bool
     */
    public function isProcessed()
    {
        return $this->getData('status') != self::PENDING;
    }

    /**
     * @return int
     */
    public function getTries()
    {
        return $this->getData('tries');
    }

    /**
     * @return $this
     */
    public function incrementTries()
    {
        $this->setData('tries', 1 + $this->getData('tries'));
        return $this;
    }

    /**
     * @return $this
     */
    public function resetTries()
    {
        $this->setData('tries', 0);
        return $this;
    }

    /**
     * @return $this
     */
    public function setLastUpdated()
    {
        $this->setData('last_updated', now());
        return $this;
    }

    /**
     * @return Exception|mixed
     */
    public function getException()
    {
        if ( ! $this->hasData('exception')) {
            return new Exception('Unknown reason');
        }
        return $this->getData('exception');
    }

    /**
     * @param bool $original
     * @return mixed|string
     */
    public function getNamespace($original = false)
    {
        if ($original) {
            return $this->getData('namespace') ?: 'default';
        }
        return strtolower($this->hasData('namespace')
            ? sprintf('%s_%s', Vznl_Job_Model_Queue::QUEUE_KEY_PREFIX, $this->getData('namespace'))
            : sprintf('%s_%s', Vznl_Job_Model_Queue::QUEUE_KEY_PREFIX, 'default'));
    }

    /**
     * @param $ns
     */
    public function setNamespace($ns)
    {
        $this->setData('namespace', (string) $ns);
    }

    /**
     * @param $key
     * @param null $default
     * @return null
     */
    public function getAdditionalInfo($key, $default = null)
    {
        if ($data = $this->getData('additional_info')) {
            return isset($data[$key]) ? $data[$key] : $default;
        }
        return $default;
    }

    /**
     * @return bool
     */
    public function shouldBeDelayed()
    {
        //should return true if the job needs to be delayed
        return false;
    }

    /**
     * Always run this after the job is processed
     *
     * @return mixed
     */
    public function runAfterProcess()
    {
    }

    /**
     * Always run this before the job is processed
     */
    public function runBeforeProcess()
    {
    }

    /**
     * Always executed after job is pushed to queue
     */
    public function afterPush()
    {
    }

    /**
     * Return the age of a job since first inserted
     * @return int
     */
    public function getAge()
    {
        return 0;
    }
}
