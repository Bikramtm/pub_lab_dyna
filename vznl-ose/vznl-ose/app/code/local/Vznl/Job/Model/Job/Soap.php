<?php

/**
 * Class Vznl_Job_Model_Job_Soap
 */
class Vznl_Job_Model_Job_Soap extends Vznl_Job_Model_Job_Abstract
{
    /**
     * Code to run when a process job is complete
     *
     * @return bool|mixed
     * @throws Exception
     */
    public function runAfterProcess()
    {
        if ($data = $this->getData('additional_info')) {
            if (isset($data['run_superorder_history']) && $data['run_superorder_history'] == true) {
                // Create the order history when the job is complete to ensure history even if the agent closes the window
                $superOrderId = $data['superorder_id'];
                $superOrderParentId = isset($data['superorder_parent_id']) ? $data['superorder_parent_id'] : null;
                $newOrder = isset($data['new_order']) ? $data['new_order'] : false;
                Mage::helper('superorder/history')->saveSuperorderHistory($this->getId(), $superOrderId, $superOrderParentId, $newOrder);
            }

            if (isset($data['run_release_lock']) && $data['run_release_lock'] == true) {
                // Release super order lock
                $superOrderId = (isset($data['superorder_parent_id']) && $data['superorder_parent_id']) ? $data['superorder_parent_id'] : $data['superorder_id'];
                Mage::helper('vznl_checkout')->releaseLockOrder($superOrderId, null, false);
            }

            // Remove processed job ID from job_ids field
            if (isset($data['superorder_id']) && ($superOrderId = $data['superorder_id'])) {
                /** @var Vznl_Superorder_Model_Superorder $superOrder */
                $superOrder = Mage::getModel('superorder/superorder')->getCollection()
                    ->addFieldToFilter('entity_id', $superOrderId)
                    ->addFieldToSelect('entity_id')
                    ->addFieldToSelect('job_ids')
                    ->setPageSize(1, 1)
                    ->getLastItem();

                $jobIds = array_map('trim', explode(',',$superOrder->getJobIds() ? : ''));
                $firstInLine = reset($jobIds);
                if ($firstInLine != $this->getId()) {
                    $message = sprintf('Expecting job ID "%s" but got "%s"', $this->getId(), $firstInLine);
                    Vznl_Job_Model_Processor::log($message);
                    throw new Exception($message);
                }
                //if same job is found multiple times, remove all occurrences
                while(($key = array_search($this->getId(), $jobIds)) !== false) {
                    unset($jobIds[$key]);
                }

                $superOrder->setJobIds(implode(',', $jobIds))
                    ->save();
            }

            //Vznl_Job_Model_Processor::log("Updated superorder and removed job ".$this->getId());
            // Update the packages that were approved
            if(isset($data['do_approve_package']) && isset($data['superorder_id']) && $data['do_approve_package'] !== false && $data['superorder_id']) {
                //Vznl_Job_Model_Processor::log("Updating packages for approval for job". $this->getId());
                foreach($data['do_approve_package'] as $packageId) {
                    /** @var Vznl_Package_Model_Package $package */
                    $package = Mage::getModel('package/package')->getCollection()
                        ->addFieldToFilter('order_id', $data['superorder_id'])
                        ->addFieldToFilter('package_id', $packageId)
                        ->addFieldToSelect('entity_id')
                        ->addFieldToSelect('approve_order')
                        ->addFieldToSelect('approve_order_job_id')
                        ->addFieldToSelect('updated_at')
                        ->setPageSize(1, 1)
                        ->getLastItem();

                    $package->setApproveOrder(1)
                        ->setApproveOrderJobId(null)
                        ->setUpdatedAt(now())
                        ->save();
                }
            }

            if (isset($data['superorder_id']) && ($superOrderId = $data['superorder_id']) && isset($data['initial_state']) && $data['initial_state']) {
                /** @var Vznl_Superorder_Model_Superorder $superOrder */
                $superOrder = Mage::getModel('superorder/superorder')->getCollection()
                    ->addFieldToFilter('entity_id', $superOrderId)
                    ->addFieldToFilter('order_status', Vznl_Superorder_Model_Superorder::SO_INITIAL)
                    ->addFieldToSelect('entity_id')
                    ->addFieldToSelect('order_status')
                    ->addFieldToSelect('order_guid')
                    ->setPageSize(1, 1)
                    ->getLastItem();

                $superOrder->setOrderStatus(Vznl_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI)
                    ->setOrderGuid(null)
                    ->save();

                if($superOrder->getId()){
                    /** @var Vznl_Superorder_Model_StatusHistory $superOrderStatusHistory */
                    $superOrderStatusHistory = Mage::getModel('superorder/statusHistory');

                    $superOrderStatusHistory->setSuperorderId($superOrderId)
                        ->setStatus(Vznl_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI)
                        ->setType(Vznl_Superorder_Model_StatusHistory::SUPERORDER_STATUS)
                        ->setCreatedAt(now())
                        ->save();
                }
                //Vznl_Job_Model_Processor::log(sprintf("Successfully updated initial query for job %s.\n", $this->getId()));
            } else {
                Vznl_Job_Model_Processor::log(sprintf("No initial state found for job %s.\n", $this->getId()));
            }
        }

        return true;
    }

    /**
     * Always executed after job is pushed to queue
     */
    public function afterPush()
    {
        if (($data = $this->getData('additional_info')) && isset($data['superorder_id']) && ($superOrderId = $data['superorder_id'])) {
            /** @var Vznl_Superorder_Model_Superorder $job */
            $job = Mage::getModel('superorder/superorder')->getCollection()
                ->addFieldToFilter('entity_id', $superOrderId)
                ->addFieldToSelect('entity_id')
                ->addFieldToSelect('job_ids')
                ->setPageSize(1, 1)
                ->getLastItem();

            $jobIds = $job->getJobIds() ? : '';
            $jobIds = array_filter(array_map('trim', explode(',', $jobIds)));
            if($this->hasPriority && (empty($jobIds) || !Mage::helper('vznl_job')->getQueue()->jobIsInQueue($jobIds[0]))) {
                //Vznl_Job_Model_Processor::log("Prioritizing job " . $this->getId());
                array_unshift($jobIds, $this->getId());
            } else {
                array_push($jobIds, $this->getId());
            }
            $job->setJobIds(implode(',', $jobIds))
                ->save();
        }
    }

    /**
     * Checks if the current job needs to be delayed
     *
     * @return bool
     */
    public function shouldBeDelayed()
    {
        try{
            //Vznl_Job_Model_Processor::log(sprintf("Checking delay conditions for job %s.\n", $this->getId()));
            //Vznl_Job_Model_Processor::log(sprintf("Got connection for delay check on job %s.\n", $this->getId()));
            if ($superOrderId = $this->getAdditionalInfo('superorder_id')) {
                $superOrder = Mage::getModel('superorder/superorder')->getCollection()
                    ->addFieldToFilter('entity_id', $superOrderId)
                    ->addFieldToSelect('job_ids')
                    ->setPageSize(1, 1)
                    ->getLastItem();
                $superOrderJobs = $superOrder->getJobIds();
                if (strlen(trim($superOrderJobs)) && 0 !== strpos(trim($superOrderJobs), sprintf('%s', $this->getId()))) { //check that it is not the first item in the list
                    return true;
                }
                return false;
            } else {
                Vznl_Job_Model_Processor::log(sprintf("No superorder id found for job %s.\n", $this->getId()));
            }
        }
        catch (Exception $e) {
            Vznl_Job_Model_Processor::log(sprintf("Exception occured for job %s Message: %s.\n", $this->getId(), $e->getMessage()));
        }

        return parent::shouldBeDelayed();
    }

    public function getAge()
    {
        if (empty($this->getData('createdAt'))) {
            return 0;
        }
        return time() - $this->getData('createdAt');
    }
}
