<?php
/**
 * Class Amqp
 */
class Vznl_Job_Model_Backend_Amqp extends Vznl_Job_Model_Backend_Abstract
{
    /** @var Zend_Queue_Adapter_Activemq */
    private $_client;

    /** @var Zend_Queue */
    private $_queue;

    /** @var array */
    private $_options;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->_options = new Varien_Object($options);
    }

    /**
     * Saves the item to the queue
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    public function push(Vznl_Job_Model_Job_Abstract $job)
    {
        $this->_getClient()->send($job->getData('body'), $this->_queue);
    }

    /**
     * Saves the item to the queue after the given index (pivot)
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @param $pivot
     * @return mixed
     * @throws Exception
     */
    public function pushAfter(Vznl_Job_Model_Job_Abstract $job, $pivot)
    {
        throw new Exception('Not implemented');
    }

    /**
     * Saves the item to the queue before the given index (pivot)
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @param $pivot
     * @return mixed
     * @throws Exception
     */
    public function pushBefore(Vznl_Job_Model_Job_Abstract $job, $pivot)
    {
        throw new Exception('Not implemented');
    }

    /**
     * Retrieves the next item from the queue
     *
     * @return Vznl_Job_Model_Job_Abstract|null
     */
    public function pop()
    {
        $iterator = $this->_getClient()->receive(1, null, $this->_queue);
        if ($message = $iterator->current()) {
            $this->_getClient()->deleteMessage($message);
            return $this->_toAmqpJob($message);
        }
        return null;
    }

    /**
     * Retrieves the next X items from the queue
     *
     * @param int $count
     * @return mixed
     */
    public function getNext($count = 1)
    {
        $messages = array();
        $iterator = $this->_getClient()->receive($count, null, $this->_queue);
        foreach ($iterator as $message) {
            $messages[] = $this->_toAmqpJob($message);
        }
        return $messages;
    }

    /**
     * Returns the size of the queue
     *
     * @return mixed
     */
    public function size()
    {
        return $this->_getClient()->count($this->_queue);
    }

    /**
     * Fetches the result
     *
     * @param string $id
     * @return bool|string
     * @throws Exception
     */
    public function get($id)
    {
        throw new Exception('Not implemented');
    }

    /**
     * Saves the processed job
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    public function set(Vznl_Job_Model_Job_Abstract $job)
    {
        //no-op
    }

    /**
     * Deletes the given job
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     * @throws Exception
     */
    public function del(Vznl_Job_Model_Job_Abstract $job)
    {
        if ( ! $job->getData('handle')) {
            throw new Exception('Job has missing handle');
        }
        $message = new Zend_Queue_Message(array('data' => $job->getData()));
        $this->_getClient()->deleteMessage($message);
    }

    /**
     * Deletes the job at the given index
     *
     * @param $index
     * @return mixed
     * @throws Exception
     */
    public function delAtPos($index)
    {
        throw new Exception('Not implemented');
    }

    /**
     * Returns the list of keys present in the
     * current db
     * @return mixed
     */
    public function keys()
    {
        return array();
    }

    /**
     * @param $list
     * @return mixed
     * @throws Exception
     */
    public function count($list)
    {
        throw new Exception('Not implemented');
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return $this->_getClient();
    }

    protected function _getClient()
    {
        if ( ! $this->_client) {
            $_conf = Mage::helper('job/util')->pluck($this->_options->toArray());
            $driverOptions = array(
                'driverOptions' => (isset($_conf['driverOptions']) ? $_conf['driverOptions'] : array())
            );
            $queueOptions = array(
                'name' => isset($_conf['queue_name']) ? $_conf['queue_name'] : 'default',
                'persistent' => isset($_conf['persistent']) && $_conf['persistent'] ? true : false,
            );
            $this->_client = new Zend_Queue_Adapter_Activemq($driverOptions);
            $this->_queue = new Zend_Queue($this->_client, $queueOptions);
        }
        return $this->_client;
    }

    /**
     * @param $obj
     * @return Vznl_Job_Model_Job_Amqp
     */
    protected function _toAmqpJob($obj)
    {
        if ( ! $obj instanceof Zend_Queue_Message) {
            throw new LogicException('Object must be instance of Zend_Queue_Message');
        }
        $job = new Vznl_Job_Model_Job_Amqp();
        return $job->setData($obj->toArray());
    }
}
