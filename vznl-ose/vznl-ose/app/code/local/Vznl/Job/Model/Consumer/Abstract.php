<?php

/**
 * Class Vznl_Job_Model_Consumer_Abstract
 */
abstract class Vznl_Job_Model_Consumer_Abstract
{
    /** @var int */
    private $_priority = 10;

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->_priority;
    }

    /**
     * Returns true if the given job can be processed
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    abstract public function supports(Vznl_Job_Model_Job_Abstract $job = null);

    /**
     * Handles the processing of individual jobs
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    abstract public function processJob(Vznl_Job_Model_Job_Abstract $job);
}
