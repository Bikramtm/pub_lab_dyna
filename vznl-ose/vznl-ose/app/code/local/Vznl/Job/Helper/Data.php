<?php

/**
 * Class Vznl_Job_Helper_Data
 */
class Vznl_Job_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Returns queue
     * @param null $queueId
     * @return Vznl_Job_Model_Queue
     */
    public function getQueue($queueId = null)
    {
        return Vznl_Job_Model_Queue::create($queueId);
    }
}