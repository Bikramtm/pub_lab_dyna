<?php

/**
 * Class Vznl_Job_Helper_Util
 */
class Vznl_Job_Helper_Util
{
    /**
     * @param array $data
     * @return array
     */
    public function pluck(array $data)
    {
        $_ = array();
        foreach ($data as $key => $val) {
            if (is_array($val)) {
                $_[$key] = $this->pluck($val);
            } else {
                if ($val) {
                    $_[$key] = $val;
                }
            }
        }

        return $_;
    }

    /**
     * Read a key from backend config skipping the cache
     * 
     * @param $key
     */
    public function readConfig($key, $scopeId = 0)
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $conn->select()
            ->from('core_config_data', array('value'))
            ->where('path = ?', $key)
            ->where('scope_id = ?', $scopeId);

        return $conn->fetchOne($select);
    }
}