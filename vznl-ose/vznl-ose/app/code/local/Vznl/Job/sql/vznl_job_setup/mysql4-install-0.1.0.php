<?php

$jobTableName = 'vznl_job_runs';

$this->startSetup();
// Only execute when the table doesn't exist yet
if (!$this->tableExists($jobTableName)) {
    $this->getConnection()->query(sprintf('DROP TABLE IF EXISTS %s;', $jobTableName));

    // Create the table
    $table = $this->getConnection()
        ->newTable($this->getTable($jobTableName))
        ->addColumn('run_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'auto_increment' => true,
                'nullable' => false
            ))
        ->addColumn('job_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => false
            ))
        ->addColumn('start_date', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => false
            ))
        ->addColumn('end_date', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => false
            ))
        ->addIndex(
            $this->getTable($jobTableName),
                'end_date'
            );
    // Executing table creation object
    $this->getConnection()->createTable($table);
}
$this->endSetup();
