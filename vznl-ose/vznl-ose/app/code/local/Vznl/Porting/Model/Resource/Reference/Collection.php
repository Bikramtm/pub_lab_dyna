<?php

class Vznl_Porting_Model_Resource_Reference_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('vznl_porting/reference');
    }
}
