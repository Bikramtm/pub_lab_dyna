<?php

/**
 * @method string getCode()
 * Class Vznl_Porting_Model_Reference
 */
class Vznl_Porting_Model_Reference extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("vznl_porting/reference");
    }

    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }

    public function delete()
    {
        $this->setDeleted(1)->setDeletedAt(now())->save();

        return $this;
    }

    /**
     * Return the string of the reference based on locale and code
     */
    public function getText()
    {
        return (Mage::app()->getLocale()->getLocaleCode() == 'nl_NL') ? $this->getData('translation_nl') : $this->getData('description');
    }
}
