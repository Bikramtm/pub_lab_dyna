<?php

class Vznl_Porting_Block_Adminhtml_Reference extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Vznl_Porting_Block_Adminhtml_Reference constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_reference';
        $this->_blockGroup = 'vznl_porting';
        $this->_headerText = Mage::helper('vznl_porting')->__('Manage porting references');
        $this->_addButtonLabel = Mage::helper('vznl_porting')->__('Add new porting reference');
        parent::__construct();
    }
}
