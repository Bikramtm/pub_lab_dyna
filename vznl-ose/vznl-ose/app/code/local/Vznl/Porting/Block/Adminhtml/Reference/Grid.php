<?php

class Vznl_Porting_Block_Adminhtml_Reference_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Vznl_Porting_Block_Adminhtml_Reference_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('portingReferenceGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('vznl_porting/reference_collection')->addFieldToFilter('deleted', ['null' => true]);
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Create the admin images list
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('vznl_porting')->__('ID'),
            'align' => 'right',
            'width' => '30px',
            'index' => 'entity_id',
        ));

        $this->addColumn('code', array(
            'header' => Mage::helper('vznl_porting')->__('Code'),
            'align' => 'left',
            'index' => 'code'
        ));

        $this->addColumn('description', array(
            'header' => Mage::helper('vznl_porting')->__('Description'),
            'align' => 'left',
            'index' => 'description',
        ));

        $this->addColumn('translation_nl', array(
            'header' => Mage::helper('vznl_porting')->__('Dutch Translation'),
            'align' => 'left',
            'index' => 'translation_nl',
        ));

        return parent::_prepareColumns();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $row->getEntityId()));
    }
}
