<?php

class Vznl_Porting_Block_Adminhtml_Reference_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Vznl_Porting_Block_Adminhtml_Reference_Edit constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'vznl_porting';
        $this->_controller = 'adminhtml_reference';

        $this->_updateButton('save', 'label', Mage::helper('vznl_porting')->__('Save reference'));

        $objId = $this->getRequest()->getParam($this->_objectId);

        if (!empty($objId)) {
            $this->_addButton('delete', array(
                'label' => Mage::helper('vznl_porting')->__('Delete'),
                'class' => 'delete',
                'onclick' => 'deleteConfirm(\''
                    . Mage::helper('core')->jsQuoteEscape(
                        Mage::helper('vznl_porting')->__('Are you sure you want to do this?')
                    )
                    . '\', \''
                    . $this->getDeleteUrl()
                    . '\')',
            ));
        }
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('porting_reference') && Mage::registry('porting_reference')->getEntityId()) {
            return Mage::helper('vznl_porting')->__("Edit porting reference with id '%s'", $this->escapeHtml(Mage::registry('porting_reference')->getEntityId()));
        } else {
            return Mage::helper('vznl_porting')->__('Add new porting reference');
        }
    }
}
