<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Configurator_IndexController
 */
class Vznl_Configurator_IndexController extends Mage_Core_Controller_Front_Action
{
    /** @var Omnius_Core_Helper_Data|null */
    private $_dynaCoreHelper = null;

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('omnius_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * ILS action
     */
    public function inlifeAction()
    {
        if ($this->getRequest()->isPost()) {
            $context = $this->getRequest()->getPost('context');
            $tariffSku = $this->getRequest()->getPost('tariff');
            $convertToMember = is_null($this->getRequest()->getPost('convert_to_member')) ? false : $this->getRequest()->getPost('convert_to_member');
            $contractStartDate = $this->getRequest()->getPost('contract_start_date');
            $contractEndDate = $this->getRequest()->getPost('contract_end_date');
            $contractPossibleCancellationDate = $this->getRequest()->getPost('contract_possible_cancellation_date');
            $bundleProductIds = $this->getRequest()->getPost('bundle_product_ids');
            $packageServiceLineId = $this->getRequest()->getPost('service_line_id');
            $customerId = $this->getRequest()->getPost('customer_id');
            $stack = $this->getRequest()->getPost('stack');
            $moveAddressId = $this->getRequest()->getPost('address_id');

            Mage::register('ils_process_initiated', $context);

            if($context == strtoupper(Dyna_Catalog_Model_ProcessContext::MOVE)) {
                Mage::register('move_process_initiated', $context);
            }

            /** @var Vznl_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();

            $valid = $this->validateInlifeRequest($quote);

            if ($valid['error']) {
                return $this->jsonResponse($valid);
            }

            try {
                /** @var Vznl_Checkout_Model_Cart $cart */
                $cart = Mage::getSingleton('checkout/cart');
                /** @var Vznl_Configurator_Helper_Cart $cartHelper */
                $cartHelper = Mage::helper('dyna_configurator/cart');
                $packageType = strtolower($this->getRequest()->getPost('package_type'));
                $packageCreationType = strtolower($this->getRequest()->getPost('package_creation_type'));
                $packageCTN = $this->getRequest()->getPost('ctn');
                $customerSession = Mage::getSingleton('dyna_customer/session');
                $customerSession->setData('process_context', $context);
                $productIds = [];
                $currentProducts = '';
                $products = $productsBySku = [];
                $packageId = null;
                $customMessages = [];
                $stringErrors = [];
                $tariffArray = [];
                $removedProductsIds = [];

                //on move set billing address = new service address
                if ($context == strtoupper(Dyna_Catalog_Model_ProcessContext::MOVE)) {
                    $quote->setBillingAddress(Mage::getModel('sales/quote_address')->setData($quote->getServiceAddress()))->save();
                }

                // Get customer ID (PAN)
                $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
                $customerPan = $customer->getPan();

                // Get context
                $orderType = Vznl_Configurator_Model_PackageType::ORDER_TYPE_PRODUCT;
                switch ($context) {
                    case Vznl_Catalog_Model_ProcessContext::ILSBLU:
                        $orderType = Vznl_Configurator_Model_PackageType::ORDER_TYPE_PRODUCT;
                        break;
                    case Vznl_Catalog_Model_ProcessContext::MOVE:
                        $orderType = Vznl_Configurator_Model_PackageType::ORDER_TYPE_MOVE;
                        break;
                }
                $quote->setOrderType($orderType);
                $quote->save();

                // Check if address is different
                $crossFootPrint = Mage::helper('vznl_configurator/inlife')->isCustomerMovingToDifferentAddress($orderType);

                // Get target service ID
                $serviceAddressId = Mage::helper('vznl_configurator/inlife')->getServiceAddressId($quote->getId());

                if ($context == strtoupper(Dyna_Catalog_Model_ProcessContext::MOVE)) {
                    if ($moveAddressId && $serviceAddressId != $moveAddressId) {
                        Mage::log(" Move address id from DB : ".$serviceAddressId. "", null, 'moveaddress.log');
                        Mage::log(" Move address id from Params : ".$moveAddressId. "", null, 'moveaddress.log');
                        $serviceAddressId = $moveAddressId;
                    }
                }

                // Get basket
                $data = Mage::helper('vznl_getbasket')->getBasket($customerPan, $serviceAddressId, $crossFootPrint, $orderType);
                if($data['error']) {
                    $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($data["code"],$data['message'],Vznl_GetBasket_Adapter_Peal_Adapter::name);
                    $data = array(
                        'error' => true,
                        'message' => $pealErrorCode['translation'],
                    );
                    $this->jsonResponse($data);
                    return;
                }

                // Read in basket ID
                $basketId = null;
                if ($data) {
                    if ($data["basketId"]) {
                        $basketId = $data["basketId"];
                        Mage::helper('vznl_validatecart')->setBasketId($basketId);
                    }
                }

                // Consume reset basket API to retrive the installed base
                $resetBasketResponse = Mage::helper('vznl_resetbasket')->resetBasket($basketId);
                if($resetBasketResponse['error']) {
                    $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($resetBasketResponse["code"],$resetBasketResponse['message'],Vznl_ResetBasket_Adapter_Peal_Adapter::name);
                    $resetBasketResponse = array(
                        'error' => true,
                        'message' => $pealErrorCode['translation'],
                    );
                    $this->jsonResponse($resetBasketResponse);
                    return;
                }
                $resultArray = array();
                $resultArray = Mage::helper('vznl_validatecart')->getResultArray($resetBasketResponse['itemsList'], $resultArray);
                $resultArray["basketId"] = $basketId;
                Mage::getSingleton('core/session')->setResetResultArray($resultArray);
                Mage::log(" BasketId : ".$basketId." : ". json_encode($resultArray) . "\n\n", null, 'resultilsarray.log');

                $oseItemList = array();
                $productSkus = array();
                $hardwareDetail = array();

                $technicalIds = Mage::helper('vznl_resetbasket')->getOseItemList($resetBasketResponse['itemsList'], $oseItemList);
                foreach ($technicalIds as $technicalId => $value) {

                    $commercialSkus = Mage::getModel('dyna_multimapper/mapper')->getSkusForSoc($value['offerId'], $productSkus);
                    $productModel = Mage::getModel('catalog/product');
                    $productSkus = array_merge($productSkus, $commercialSkus);
                    if (isset($commercialSkus[0])) {
                        foreach ($commercialSkus as $productSku) {
                            $productId = $productModel->getIdBySku($productSku);
                            $hardwareDetail[$productSku]=$value;
                            $hardwareDetail[$productSku]["productId"]=$productId;
                        }
                    }
                }

                Mage::register('hardware_details_ils', $hardwareDetail);
                Mage::getSingleton('customer/session')->setHardwareDetailsIls($hardwareDetail);

                $productModel = Mage::getModel('dyna_catalog/product');

                /** @var Dyna_Catalog_Model_Resource_Product_Collection $productsCollection */
                $productsCollection = $productModel->getCollection();

                if (count($productSkus)) {
                    $productSkus = $this->filterSkus($productSkus);
                    $productSkus = array_combine($productSkus, $productSkus);
                    $productsCollection = $productsCollection->addFieldToFilter('sku', ['in' => $productSkus]);
                    $products = $productsCollection->getItems();
                    foreach ($products as $product) {
                        $sku = $product->getSku();
                        if ($productSkus[$sku]) {
                            $productIds[$sku] = $product->getId();
                            $productsBySku[$sku] = $product;
                        }
                        if ($product->isSubscription()) {
                            $tariffId = $product->getId();
                            $tariffArray[$product->getSku()] = $product->getId();
                        }
                    }
                    $currentProducts = implode(',', $productIds);
                }



                // Sale type is now replaced by process_context
                $saleType = $context;
                $packageCreationTypeId = $cartHelper->getCreationTypeId($packageCreationType);

                if (!$packageCreationTypeId) {
                    $packageCreationTypeId = $cartHelper->getCreationTypeId(Vznl_Catalog_Model_Type::getCreationTypesMapping()[strtolower($packageType)]);
                }

                $packageExists = false;
                $packages = $quote->getCartPackages();
                if ($packages) {
                    foreach ($packages as $package) {
                        if ($packageCTN && $package->getCtn() == $packageCTN) {
                            $packageId = $package->getPackageId();
                            $packageExists = true;
                            break;
                        }
                    }
                }

                if (!$packageId) {

                    // Create new package with the products and add it to the quote

                    $packageId = $cartHelper->initNewPackage(
                        $packageType,
                        $saleType,
                        $packageCTN,
                        $currentProducts,
                        null,
                        null,
                        null,
                        null,
                        $packageCreationTypeId
                    );
                }


                /** @var Vznl_Package_Model_Package $packageModel */

                $packageModel = $quote->getCartPackage($packageId);

                // needed to execute postcondition rules for cable
                Mage::register('product_add_origin', 'addMulti');

                // add products from installed base package
                if (!$packageExists) {
                    $productsErrors = $this->addInlifeProducts($productIds, $tariffId, $productsBySku, $cart, $packageModel, $customMessages);

                    //update expression objects based on the added products
                    Mage::helper('dyna_configurator/expression')->updateInstance(Mage::getSingleton('checkout/cart')->getQuote());
                    $stringErrors = array_merge($stringErrors, $productsErrors);

                    if($context == strtoupper(Dyna_Catalog_Model_ProcessContext::MOVE)) {
                        $validSubscription = Mage::helper('vznl_checkout/cart')->validateSubscriptionProduct($tariffArray, $packageType,$tariffId);
                        $tariffId = $validSubscription ? $tariffId : null;
                    }

                    $tariffDefaultsArray = $this->getTariffDefaults($tariffId, $currentProducts, $packageModel);

                    $tariffDefaultProducts = $tariffDefaultsArray['tariffDefaultProducts'];
                    $tariffDefaultProductsIdsBySku = $tariffDefaultsArray['tariffDefaultProductsIdsBySku'];
                    $tariffDefaultProductsModelsBySku = $tariffDefaultsArray['tariffDefaultProductsModelsBySku'];

                    // existing products that need to be marked as defaulted for the tariff
                    $existingTariffDefaultedProducts = array_intersect($tariffDefaultProductsIdsBySku, $productIds);
                    if (count($existingTariffDefaultedProducts) > 0) {
                        $this->markDefaultedProducts(
                            $cart,
                            $existingTariffDefaultedProducts,
                            $tariffDefaultProductsModelsBySku,
                            $tariffDefaultProducts,
                            $contractStartDate
                        );
                    }

                    // new default products added for the tariff
                    $newTariffDefaultedProducts = array_diff($tariffDefaultProductsIdsBySku, $productIds);
                    if (count($newTariffDefaultedProducts) > 0) {
                        // add defaulted products of the tariff
                        $productsErrors = $this->addInlifeProducts(
                            $newTariffDefaultedProducts,
                            $tariffId,
                            $tariffDefaultProductsModelsBySku,
                            $cart,
                            $packageModel,
                            $customMessages,
                            $tariffDefaultProducts
                        );
                        $stringErrors = array_merge($stringErrors, $productsErrors);
                    }


                    // Get existing product models by skus
                    if (!empty($productIds)) {
                        $productsCollection = $productsCollection->addFieldToFilter('entity_id',
                            ['in' => $productIds]);
                        foreach ($productsCollection as $product) {
                            $existingProductModelsBySku[$product->getSku()] = $product;
                        }
                    }

                    // Set system actions on the quote for existing products in the installed base
                    $this->setSystemActions($cart, $productIds, $existingProductModelsBySku);

                    $cart->save();

                    if($context == strtoupper(Dyna_Catalog_Model_ProcessContext::MOVE)) {
                        $allItems = $quote->getAllItems();
                        $removedProductsIds = Mage::helper('vznl_checkout/cart')->validateMoveProducts($productIds, $allItems, $packageId, $packageType);
                        Mage::unregister('move_process_initiated');
                        $cart->getQuote()->setTotalsCollectedFlag(false)->collectTotals()->save();
                    }
                }
                /*code to handle the promotion link show/hide based on the count*/
                $promoRules = Mage::helper('pricerules')->findApplicablePromoRules(true, $packageId);
                $promoCounter = Mage::getSingleton('core/session')->getPromoCounter();
                $promoCounter[$packageId] = $promoRules;
                Mage::getSingleton('core/session')->setPromoCounter($promoCounter);

                $response = array(
                    'error' => false,
                    'message' => $cartHelper->__('Package created successfully'),
                    'rightBlock' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml(),
                    'totals' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                    'package_id' => $packageId
                );
                if(!empty($customMessages['preselect']['items'])){
                    $response['customMessage'] = $customMessages['preselect']['message'] . " " . implode(", ", $customMessages['preselect']['items']);
                }
                if($removedProductsIds) {
                    $response['moveRemovedProducts'] = $removedProductsIds;
                } else {
                    $response['moveRemovedProducts'] = false;
                }
                $this->jsonResponse($response);

            } catch (Exception $e) {
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $e->getMessage()
                ));
            }
        }
    }

    /**
     * Delete skus which are set in admin in general configuration page at ignoredSku field.
     */
    protected function filterSkus($productSkus){
        $configIgnoredSkus = Mage::getStoreConfig('omnius_general/cable_general_settings/ignored_sku');
        $configIgnoredSkus= explode("\n", $configIgnoredSkus);
        foreach ($configIgnoredSkus as $ignoredSku){
            if (in_array($ignoredSku,$productSkus)){
                $productSkus = array_diff($productSkus,array($ignoredSku));
            }
        }
        return $productSkus;
    }

    public function setSystemActions($cart, $existingProducts, $existingProductModelsBySku)
    {
        foreach ($existingProducts as $productSku => $productId) {
            /** @var Vznl_Catalog_Model_Product $product */
            $product = $existingProductModelsBySku[$productSku] ?? null;
            if ($product) {
                /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItem */
                $quoteItem = $cart->getQuote()->getItemByProduct($product);
                if ($quoteItem) {
                    $quoteItem->setSystemAction(Vznl_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING);
                }
            }
        }
    }

    /**
     * @param array $productIds Contains an associative array sku=>product_id for all the products that need to be added
     * @param int $tariffId The product ID of the tariff
     * @param array $productsBySku Associative array sku=>product model for all the products that need to be added
     * @param Vznl_Checkout_Model_Cart $cart
     * @param Dyna_Package_Model_Package $packageModel
     * @param $customMessages
     * @param array $tariffDefaultProducts
     * @return array|bool
     */
    protected function addInlifeProducts(
        $productIds,
        $tariffId,
        $productsBySku,
        $cart,
        $packageModel,
        &$customMessages,
        $tariffDefaultProducts = []
    ) {
        $contractStartDate = $this->getRequest()->getPost('contract_start_date');
        $contractEndDate = $this->getRequest()->getPost('contract_end_date');
        $subEndDates = $this->getRequest()->getPost('sub_end_date');
        $subTerminationDates = $this->getRequest()->getPost('sub_termination_date');
        $stack = $this->getRequest()->getPost('stack');
        $contractPossibleCancellationDate = $this->getRequest()->getPost('contract_possible_cancellation_date');
        $convertToMember = $this->getRequest()->getPost('convert_to_member');
        $bundleProductIds = $this->getRequest()->getPost('bundle_product_ids', []);
        $customerId = $this->getRequest()->getPost('customer_id');
        $isInMinimumDuration = $this->isInMinimumDuration($customerId);

        $bundleProductIds = explode(",", $bundleProductIds);

        $stringErrors = [];
        $context = $packageModel->getSaleType();

        // define function
        $arraySearchMultidim = function ($array, $column, $key) {
            return (array_search($key, array_column($array, $column)));
        };

        $contractEndDateSkus = $this->explodeSkusAndDate($subEndDates);
        $contractTerminationDateSkus = $this->explodeSkusAndDate($subTerminationDates);

        foreach ($productIds as $productSku => $productId) {
            /** @var Dyna_Catalog_Model_Product $product */
            $product = $productsBySku[$productSku] ?? null;
            if ($product) {
                $isMobileProduct = count(
                    array_intersect(
                        explode(',', $product->getPackageType()),
                        Vznl_Catalog_Model_Type::getMobilePackages()
                    )
                );
                $isRetentionOrIls = in_array($context,
                    array_merge(
                        Vznl_Catalog_Model_ProcessContext::getProlongationProcessContexts(),
                        Vznl_Catalog_Model_ProcessContext::ILSProcessContexts()
                    )
                );
                $preselect = true;
                $foundInDefaulted = false;
                if ($tariffDefaultProducts) {
                    // check if product is a defaulted product of the tariff and it is not already part of installed base
                    $foundInDefaulted = $arraySearchMultidim($tariffDefaultProducts, 'target_product_id', $productId);
                }

                // [VFDED1W3S-1108] For Mobile ILS/Retention check the if the product has the Preselect attribute value set to 'Yes', otherwise it will be skipped
                if ($isMobileProduct
                    && $isRetentionOrIls
                    && !$product->getPreselect()
                    && $foundInDefaulted === false
                ) {
                    if (!isset($customMessages['preselect'])) {
                        $customMessages['preselect']['message'] = $this->__("Please be aware that the following products will not be added in the cart due the preselect option attribute: ");
                    }
                    $customMessages['preselect']['items'][] = $product->getSku();
                    $preselect = false;
                }

                $endDate = $terminationDate = '';
                if ($stack == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                    if (isset($contractEndDateSkus[$productSku]) && $contractEndDateSkus[$productSku]) {
                        $endDate = $contractEndDateSkus[$productSku];
                    }

                    if (isset($contractTerminationDateSkus[$productSku]) && $contractTerminationDateSkus[$productSku]) {
                        $terminationDate = $contractTerminationDateSkus[$productSku];
                    }
                } else {
                    $endDate = $contractEndDate;
                }
                $isDefaulted = $foundInDefaulted !== false ? 1 : null;
                $systemAction = $foundInDefaulted !== false ? Vznl_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD : Vznl_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING;
                $productData = [
                    'qty' => 1,
                    'one_of_deal' => 0,
                    'preselect_product' => $preselect,
                    'is_contract' => $foundInDefaulted !== false ? 0 : 1,
                    'system_action' => $foundInDefaulted !== false ? Vznl_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD : Vznl_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING,
                    'contract_start_date' => $contractStartDate,
                    'contract_end_date' => $endDate,
                    'contract_possible_cancellation_date' => $productId == $tariffId ? $contractPossibleCancellationDate : null,
                    'is_defaulted' => $foundInDefaulted !== false ? 1 : null,
                    'is_service_defaulted' => $foundInDefaulted !== false ? (int)$tariffDefaultProducts[$foundInDefaulted]['is_service'] : null,
                    'is_obligated' => $foundInDefaulted !== false ? $tariffDefaultProducts[$foundInDefaulted]['obligated'] : null
                ];

                /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
                $item = $cart->addProduct($productId, $productData);
                $item->setIsDefaulted($isDefaulted);
                $item->setSystemAction($systemAction);
                $item->setPreselectProduct($systemAction == Vznl_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING);
                $item->setContractStartDate($contractStartDate);
                $item->setContractEndDate($endDate);
                if (!is_string($item)) {
                    $item->setInMinimumDuration((int)$isInMinimumDuration)
                        ->setPackageType($packageModel->getType());

                    if ($convertToMember) {
                        $item->setPackageId($packageModel->getPackageId());
                    }

                    //move item to alternate quote items if product has preselect attribute
                    if (!$preselect) {
                        $item->setSystemAction(Vznl_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_NOT_APPLICABLE);
                        $item->isDeleted(true);
                    }
                } else {
                    $stringErrors[] = $item;
                }
            }
        }

        return $stringErrors;
    }

    /**
     * Validate the inlife action
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @return mixed
     */
    protected function validateInlifeRequest($quote) {
        // TODO: implement validators for VZNL ILS
        $response = [
            "success" => [
                'error' => false
            ],
            "fail" => [
                'error' => true,
                'message' => $this->__("This action can not be performed as it is not allowed.")
            ]
        ];
        return $response['success'];
    }

    /**
     * Determine whether or not contract is in minimum duration
     * @param $contractId
     * @return bool
     */
    protected function isInMinimumDuration($contractId)
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return $customer->getIsInMinimumDuration($contractId);
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = $this->getDynaCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);

        if (!$this->getRequest()->isAjax()) {
            $body = $this->getResponse()->getBody();
            $this->getResponse()->setHeader('Content-Type', 'text/html; charset=utf-8');
            $this->getResponse()
                ->setBody($body);
        }
    }

    /**
     * @param $data
     * @return array
     */
    protected function explodeSkusAndDate($data)
    {
        $result = [];
        if ($data) {
            $subDates = explode(',', $data);
            foreach ($subDates as $subDate) {
                $subDateArray = explode(':', $subDate);
                $result[$subDateArray[0]] = $subDateArray[1];
            }
        }
        return $result;
    }


    /**
     * @param int $tariffId The id of the tariff of the installed base package
     * @param string $currentProducts Comma separated product ids
     * @param Vznl_Package_Model_Package $packageModel The newly created package model
     * @todo: determine if the defaulted should be searched for all the products or only for the tariff
     * @return array
     */
    protected function getTariffDefaults($tariffId, $currentProducts, $packageModel)
    {
        $serviceProducts = array();
        $tariffDefaultProductsIds = $tariffDefaultProductsIdsBySku = $tariffDefaultProductsModelsBySku = array();

        /** @var Dyna_Checkout_Helper_Cart $checkoutHelper */
        $checkoutHelper = Mage::helper('dyna_checkout/cart');

        /** @var Dyna_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel('dyna_catalog/product');
        $context = $packageModel->getSaleType();

        $tariffDefaultProducts = array();
        if ($tariffId || ($currentProducts == "" && $context == Dyna_Catalog_Model_ProcessContext::MIGCOC)) {

            if ($currentProducts == "" && $context == Dyna_Catalog_Model_ProcessContext::MIGCOC) {
                $tariffId = null;
            }

            $tariffDefaultProducts = $checkoutHelper->getDefaultedProducts(
                [$tariffId],
                $packageModel,
                $sourceCollectionId = Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_IB
            );

            if (isset($tariffDefaultProducts['service'])) {
                $serviceProducts = $tariffDefaultProducts['service'];
                unset($tariffDefaultProducts['service']);
            }

            $tariffDefaultProducts = array_filter(array_merge(array_shift($tariffDefaultProducts), $serviceProducts));

            if (count($tariffDefaultProducts)) {
                foreach ($tariffDefaultProducts as $defaultProduct) {
                    $tariffDefaultProductsIds[] = $defaultProduct['target_product_id'];
                }
            }

            if (!empty($tariffDefaultProductsIds)) {
                /** @var Dyna_Catalog_Model_Resource_Product_Collection $productsCollection */
                $productsCollection = $productModel->getCollection();
                $productsCollection = $productsCollection->addFieldToFilter('entity_id',
                    ['in' => $tariffDefaultProductsIds]);
                foreach ($productsCollection as $productDefault) {
                    $tariffDefaultProductsIdsBySku[$productDefault->getSku()] = $productDefault->getId();
                    $tariffDefaultProductsModelsBySku[$productDefault->getSku()] = $productDefault;
                }
            }
        }

        return array(
            'tariffDefaultProducts' => $tariffDefaultProducts,
            'tariffDefaultProductsIdsBySku' => $tariffDefaultProductsIdsBySku,
            'tariffDefaultProductsModelsBySku' => $tariffDefaultProductsModelsBySku,
        );
    }

    /**
     * Marks the existing package products as defaulted/service defaulted/obligated
     * if there are some defaulted rules with source collection IB that apply
     * @param Vznl_Checkout_Model_Cart $cart
     * @param $existingProducts
     * @param $productsBySku
     * @param $tariffDefaultProducts
     */
    protected function markDefaultedProducts(
        $cart,
        $existingProducts,
        $productsBySku,
        $tariffDefaultProducts,
        $contractStartDate
    ) {
        // define function
        $arraySearchMultidim = function ($array, $column, $key) {
            return (array_search($key, array_column($array, $column)));
        };
        foreach ($existingProducts as $productSku => $productId) {
            /** @var Dyna_Catalog_Model_Product $product */
            $product = $productsBySku[$productSku] ?? null;
            if ($product) {
                /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItem */

                $quoteItem = $cart->getQuote()->getItemByProduct($product);

                if ($quoteItem) {
                    // check if product is a defaulted product of the tariff and it is not already part of installed base
                    $foundInDefaulted = $arraySearchMultidim($tariffDefaultProducts, 'target_product_id', $productId);

                    $quoteItem
                        ->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING)
                        ->setIsDefaulted(1)
                        ->setContractStartDate($contractStartDate)
                        ->setIsServiceDefaulted($foundInDefaulted !== false ? (int)$tariffDefaultProducts[$foundInDefaulted]['is_service'] : null);
                        //->setIsObligated($foundInDefaulted !== false ? $tariffDefaultProducts[$foundInDefaulted]['obligated'] : null);
                }

            }

        }
    }

}

