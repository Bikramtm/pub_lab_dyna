<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

require_once Mage::getModuleDir('controllers', 'Dyna_Configurator').DS.'CartController.php';

/**
 * Class Vznl_Configurator_CartController
 */
class Vznl_Configurator_CartController extends Dyna_Configurator_CartController
{
    /**
     * Returns eligible bundles in frontend
     *
     * @return string
     */
    public function getEligibleBundlesAction()
    {
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        /** @var Dyna_Package_Model_Package $package */
        $cartPackages = $quote->getCartPackages();

        if ((count($cartPackages) == 1 && !$customerSession->isLoggedIn()) ||
            ($customerSession->isLoggedIn() && $customerSession->getCustomer()->getNoOrderingAllowed())
            ) {
            $this->jsonResponse(array(
                'activeBundles' => [],
                'eligibleBundles' => [],
                'bundleComponents' => [],
                'bundleHints' => [],
            ));

            return null;
        }

        /** @var Dyna_Bundles_Helper_Data $bundleHelper */
        $bundleHelper = Mage::helper('dyna_bundles');
        $tariffs = array_filter($this->getRequest()->get('tariffs') ?: array());

        // Return eligible bundles for current state
        $this->jsonResponse(array(
            'activeBundles' => $bundleHelper->getActiveBundles(),
            'eligibleBundles' => $bundleHelper->getFrontendEligibleBundles(false, true, $tariffs),
            'bundleComponents' => $bundleHelper->getBundleComponents(),
            'bundleHints' => $bundleHelper->getBundleHints(),
        ));

        foreach ($cartPackages as $package) {
                if (!empty($package->getBundleTypes())) {
                    Mage::helper('vznl_checkout/cart')->validateCampaignOffer($package->getPackageId());
                }
        }

        return null;
    }

    /**
     * Get list of products that are visible in frontend based on current rules
     */
    public function getRulesAction()
    {
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $productIds = array();
        $ilsProductIds = array();
        /** If there are current products in cart, forward them to rules processor */
        if ($products = Mage::app()->getRequest()->getParam('products', false)) {
            $productIds = explode(',', $products);
            sort($productIds);
        }

        /**Handle address data(contains json object {"serviceability":..., "validate" :...}*/
        $addressData = json_decode($this->getRequest()->get('addressData'), true);
        $storageAddressData = json_decode(Mage::getSingleton('dyna_address/storage')->getData('addressData'), true);

        if (!empty($storageAddressData) && !empty($this->array_diff_r($addressData, $storageAddressData))) {
            Mage::getSingleton('dyna_address/storage')->setData('addressData', json_encode($addressData));
        }

        //skip levy product if found
        $productIds = Vznl_Catalog_Helper_Data::skipLevyProduct($productIds);

        if ($ilsProducts = Mage::app()->getRequest()->getParam('ilsProducts', false)) {
            $ilsProductIds = explode(',', $ilsProducts);
            sort($ilsProductIds);
        }
        $type = $this->getRequest()->get('type');
        $packageId = $this->getRequest()->get('packageId');
        $websiteId = $this->getRequest()->get('websiteId');
        $customerType = (int)Mage::getSingleton('customer/session')->getCustomer()->getIsSoho();
        $customerNumber = $this->getRequest()->get('customer_number');
        $serviceAddress = $quote->getServiceAddress() ? unserialize($quote->getServiceAddress()) : null;
        $addressId = (!empty($serviceAddress['addressId'])) ? $serviceAddress['addressId'] : null;
        $hash = $this->getRequest()->get('hash');

        if (!empty($quote->getCartPackage($packageId))) {
            $packageCreationId = $quote->getCartPackage($packageId)->getPackageCreationTypeId();

        } else {
            $packageType = Mage::getModel('dyna_package/packageType')->loadByCode($type);
            $packageCreationId = Mage::getModel('dyna_package/packageCreationTypes')->getCollection()->addFieldToFilter('package_type_id', array('eq' => $packageType->getId()))->getFirstItem()->getId();
        }

        Mage::register('getRules__customer_type', $customerType);

        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");

        try {
            $doCache = !Mage::helper('dyna_productmatchrule')->isDebugMode();
            $cablePackages = Dyna_Catalog_Model_Type::getCablePackages();
            // Cable specific service calls
            if (in_array(strtolower($type), $cablePackages)) {
                $doCache = false;
            }
            $systemAction ='';
            foreach($quote->getAllItems() as $item){
                $systemAction .= $item->getProductId() . ',' . ($item->getSystemAction() ?: 'none');
            }
            $cacheKey = "getRules_" . md5(serialize([$type, $packageId, $websiteId, $customerType, $customerNumber, $addressId, $productIds, $ilsProducts, $hash, $processContextHelper->getProcessContextCode(), $packageCreationId, $systemAction]));

            if ($doCache && ($cachedRules = $this->getCache()->load($cacheKey))) {
                $rules = unserialize($cachedRules);
                $rules['rules'] = array_values($rules['rules']);
                $rules['cache'] = 1;
            } else {
                // the getRules method call is under this line
                $this->removePromosFromRulesCheck($productIds);
                $rules = $this->getRules($productIds, $type, $websiteId, $ilsProductIds);
                $rules['context'] = $processContextHelper->getProcessContextCode();
                $rules['cache'] = 0;
                // with cache but not cached
                if ($doCache) {
                    $this->getCache()->save(serialize($rules), $cacheKey, [Dyna_Cache_Model_Cache::PRODUCT_TAG]);
                }
            }

            $this->jsonResponse($rules);
        } catch (Exception $e) {

            Mage::logException($e);
            $this->jsonResponse([
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Do not pass promo products to rules evaluation
     * @param $productIds
     */
    protected function removePromosFromRulesCheck(&$productIds)
    {
        $promoProductsIds = $this->getPromoProductsIds();
        foreach ($productIds as $key => $productId) {
            if (in_array($productId, $promoProductsIds)) {
                unset($productIds[$key]);
            }
        }
    }

    /**
     * Get ids of promo products and cache them for further uses
     * @return array|mixed
     */
    protected function getPromoProductsIds()
    {
        $promoCacheKey = 'GET_PROMO_PRODUCTS';
        if (!$serializedResult = $this->getCache()->load($promoCacheKey)) {
            $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                ->load('promotion', 'attribute_set_name')
                ->getAttributeSetId();

            // get promo products
            $products = Mage::getModel('catalog/product')
                ->getCollection()
                ->addFieldToFilter('attribute_set_id', $attributeSetId);

            $promoProductsIds = array();
            foreach ($products as $product) {
                $promoProductsIds[] = $product->getId();
            }

            $this->getCache()->save(serialize($promoProductsIds), $promoCacheKey, [Dyna_Cache_Model_Cache::PRODUCT_TAG]);
        } else {
            $promoProductsIds = unserialize($serializedResult);
        }

        return $promoProductsIds;
    }

    /**
     * Removes the quote from session and creates another one.
     */
    public function emptyAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        Mage::helper('dyna_customer')->clearMigrationData();
        Mage::getSingleton('dyna_customer/session')->unsActiveBundleId();
        Mage::getSingleton('core/session')->unsPromoCounter();
        $this->unsetLinkedAccount();
        $this->clearSpecialOfferPromoProducts();
        parent::emptyAction();
    }

    /**
     * remove the session offer data for the removed packages
     */
    protected function clearSpecialOfferPromoProducts()
    {
        $specialOfferPromoProducts = Mage::getSingleton('core/session')->getSpecialOfferPromoProducts();
        foreach ($specialOfferPromoProducts ?? [] as $packageId => $specialOfferPromoProduct) {
            $specialOfferPromoProducts[$packageId] = [];
        }
        Mage::getSingleton('core/session')->setSpecialOfferPromoProducts($specialOfferPromoProducts);
    }

    /**
     * Deletes a quote specified in a post call.
     */
    public function deleteSavedQuoteAction()
    {
        if ($this->getRequest()->isPost()) {
            $param = $this->getRequest()->get('quote_id');
            if ($param) {
                try {
                    $quote = Mage::getModel('sales/quote')->getCollection()
                        ->addFieldToFilter('entity_id', $param)
                        ->setPageSize(1, 1)
                        ->getLastItem();

                    if ($quote->getIsOffer()) {
                        Mage::helper('pricerules')->decrementAll($quote);
                    }

                    $quote->setIsActive(false);
                    $quote->delete();
                    Mage::getSingleton('core/session')->setloadCustomer(true);
                    $this->jsonResponse(array(
                        'error' => false
                    ));
                } catch(Exception $e) {
                    $this->jsonResponse(array(
                        'error'     => true,
                        'message'   => $e->getMessage()
                    ));
                }
            } else {
                $this->jsonResponse(array(
                    'error'     => true,
                    'message'   => Mage::helper('dyna_checkout')->__('The quote id was not specified.')
                ));
            }
        }
    }

    /**
     * Retrieves the list of possible combinations for the current package without the deviceRc
     */
    public function getDeviceRulesAction()
    {
        if (!$this->_getQuote()->getId()) {
            return $this->jsonResponse([
                'error' => true,
                'message' => $this->__('Invalid quote'),
            ]);
        }

        try {
            $items = [];
            $packageId = $this->_getQuote()->getActivePackageId();
            foreach ($this->_getQuote()->getAllItems() as $item) {
                // Only compute items that are in the current package, and are not device subscription
                $product = $item->getProduct();
                if ($item->getPackageId() == $packageId
                    && Mage::helper('omnius_catalog')->is(
                        Mage::getSingleton('omnius_configurator/catalog')->getOptionsForPackageType(Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE),
                        $item->getProduct()
                    )
                ) {
                    $items[] = $product->getId();
                }
            }
            $result = Mage::helper('omnius_configurator/cart')->getCartRules(
                $items,
                [],
                Mage::helper('superorder')->shouldEmulateStore(Mage::app()->getWebsite()->getId()),
                false,
                $this->_getQuote()->getActivePackage()->getType()
            );
        } catch (Exception $e) {
            return $this->jsonResponse([
                'error' => true,
                'message' => $this->__($e->getMessage()),
            ]);
        }

        return $this->jsonResponse([
            'error' => false,
            'ids' => Mage::helper('core')->jsonEncode($result),
        ]);
    }

    /**
     * Generate min/max/eql and cardinality warnings
     */
    public function getConfiguratorWarningsAction() {
        $cartProductsHash = $this->getRequest()->get('cartProductsHash');
        /** @var Vznl_Configurator_Helper_Data $configuratorHelper */
        $configuratorHelper = Mage::helper('vznl_configurator');
        $hasKipWhileNotAllowed = $configuratorHelper->hasKIPWhileNotAllowed() ? array('There should be only 1 kip at a specific address') : [];
        $ilsPackageHasBeenModified = Mage::getModel('package/package')->isILSPackageModified() || count($this->_getQuote()->getPackages()) == 0 ? [] : ['ILS package has not been modified'];
        $cardinalityWarnings = [];
        if (Mage::getStoreConfig('omnius_general/package_validation/enabled')) {
            $cardinalityWarnings = $this->getConfiguratorCartHelper()->getCardinalityWarnings() ?? [];
        }
        $minMaxEqlWarnings = $this->getConfiguratorCartHelper()->getMinMaxEqlWarnings($cartProductsHash) ?? [];
        $mandatoryWarnings = $this->getConfiguratorCartHelper()->getMandatoryWarnings() ?? [];
        $prolongationWarnings = $this->getConfiguratorCartHelper()->getProlongationWarnings() ?? [];
        $completeOrderWarnings = Mage::helper('dyna_checkout')->getCheckPackageStatusWarnings();

        $warnings = array_replace_recursive(
            $cardinalityWarnings
            ,$minMaxEqlWarnings
            ,$mandatoryWarnings
            ,$prolongationWarnings
            ,$hasKipWhileNotAllowed
            ,$ilsPackageHasBeenModified
            ,$completeOrderWarnings
        );

        foreach (Mage::getSingleton('checkout/cart')->getQuote()->getCartPackages() as $packageModel)
        {
            if (!empty($warnings[$packageModel->getPackageId()])) {
                // Update status
                $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
            } else {
                // Update status
                $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED);
            }
            $packageModel->save();
        }

        $response['warnings'] = $warnings;
        $response['complete'] = (empty($warnings) && $cartProductsHash != md5(serialize([]))) ? true : false;

        $this->jsonResponse($response);
    }

    /**
     * Process partial termination for ILS and MOVE
     */
    public function processPartialTermination($validateCart)
    {
        $fixedSubscriptionName = '';
        $packageCreationGroupName = '';
        $packageCreationTypeId = null;
        $allProducts = [];
        $currentPackages = Mage::getSingleton('checkout/session')->getQuote()->getPackages();
        foreach ($currentPackages as $currentPackage) {
            if($currentPackage['type'] == Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_FIXED) {
                $packageCreationTypeId = $currentPackage['package_creation_type_id'];
            }
        }

        $cartHelper = Mage::helper('dyna_configurator/cart');
        $items  = Mage::getModel('checkout/session')->getQuote()->getAllItems();
        $items = $cartHelper->getSortedCartProducts($items);
        foreach ($items as $item) {
            $productId = $item->getProductId();
            $packageId = $item->getPackageId();
            $packageSubType = Mage::getModel('vznl_catalog/product')->getById($productId)->getAttributeText('package_subtype');

            if ($packageSubType == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE) {
                $fixedSubscriptionName = $item->getName();
            }
            if ($item->getPackageType() == Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_FIXED) {
                $allProducts[] = $item->getName();
            }

        }
        if ($packageCreationTypeId) {
            $packageTypeGuiName = Mage::getSingleton('dyna_package/packageCreationTypes')->getById($packageCreationTypeId)->getGuiName();
        }

        return array(
            'isItemCancelled' => true,
            'fixedSubscriptionName' => $fixedSubscriptionName,
            'packageTypeGuiName' => $packageTypeGuiName,
            'allPackageItems' => $allProducts,
            'itemCancelledArray' => $validateCart['itemCancelledArray']
        );
    }

    /**
     * perform peal basket call (add, remove, validate, reset, create)
     */
    public function getValidateCartAction()
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $callValidatecart = $quote->hasFixed();
        $Helper = Mage::helper('vznl_validatecart');
        $Helper->removePealAddedProduct();

        if ($callValidatecart) {
            $validateCart = $Helper->validate();
            if (isset($validateCart['error'])) {
                $this->jsonResponse($validateCart);
                return;
            }
            if ($quote->isFixedIlsMove() && isset($validateCart['isItemCancelled']) && $validateCart['isItemCancelled']) {
                $response = $this->processPartialTermination($validateCart);
                $this->jsonResponse($response);
                return;
            }
        }

        $response = $Helper->processValidateCart($validateCart, $callValidatecart);
        $this->jsonResponse($response);
    }

    /**
     * The action used for the removal of packages from the configurator.
     */
    public function removePackageAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $accepted = $this->getRequest()->getParam('accepted', '0');
        try {
            $packageId = $this->getRequest()->getParam('packageId');
            $expanded = $this->getRequest()->getParam('expanded');
            $btwToggle = $this->getRequest()->getParam('btwState');
            $initialPackageModelId = $this->_getQuote()->getCartPackage($this->getRequest()->getParam('packageId'))->getId();

            /** @var Dyna_Package_Helper_Data $packageHelper */
            $packageHelper = Mage::helper('dyna_package');
            $removePackageDetails = $packageHelper->removePackage($packageId,$accepted);
            // Determine the agent's (COPS / Telesales) VOID(Red Sales Id) for remaining packages
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = $this->_getCart()->getQuote();
            $redSalesId = Mage::helper('bundles')->getRedSalesId();
            $quote->setSalesId($redSalesId);

            $this->_getCart()->save();

            //remove the session offer data for the removed package
            $offerSession = Mage::getSingleton('core/session')->getSpecialOfferPromoProducts();
            $offerSession[$packageId] = array();
            Mage::getSingleton('core/session')->setSpecialOfferPromoProducts($offerSession);

            $rightBlock = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml();
            $mustRemoveBundleChoice= !Mage::helper('bundles')->isBundleChoiceFlow();

            /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
            $processContextHelper = Mage::helper("dyna_catalog/processContext");

            /** @var Dyna_Customer_Model_Session $session */
            $session = Mage::getSingleton('customer/session');
            $session->updateLinkedAccountNumber();

            /*code to handle the promotion link show/hide based on the count*/
            $promoCounter = Mage::getSingleton('core/session')->getPromoCounter();
            unset($promoCounter[$packageId]);
            Mage::getSingleton('core/session')->setPromoCounter($promoCounter);

            $currentPackageModel = $quote->getCartPackage();
            $this->jsonResponse(array(
                'error'             => false,
                'totals'            => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->setBtwState($btwToggle)->toHtml(),
                'packagesIndex'     => $removePackageDetails['packagesIndex'],
                'complete'          => Mage::helper('dyna_checkout')->hasAllPackagesCompleted(),
                'expanded'          => $expanded ? Mage::getSingleton('core/layout')->createBlock('dyna_configurator/rightsidebar')->setTemplate('customer/expanded_cart.phtml')->toHtml() : '',
                'removeRetainables' => $removePackageDetails['removeRetainables'] ? array_values($removePackageDetails['packageIds']) : false,
                'quoteHash'         => Mage::helper('dyna_checkout/cart')->getQuoteProductsHash(),
                'salesId'           => $quote->getSalesId(),
                'rightBlock'        => $rightBlock,
                'packageType'        => $removePackageDetails['remainingCablePackage'],
                'deletedPackageType' => $removePackageDetails['deletedPackageType'],
                'mustRemoveBundleChoice' => $mustRemoveBundleChoice,
                'hasFixed' => $quote->hasFixed(),
                'processContextCode' => $processContextHelper->getProcessContextCode(),
                'clearConfigurator' => !$currentPackageModel || ($currentPackageModel->getId() != $initialPackageModelId),
            ));

        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__($e->getMessage())
            ));
        }
    }

    /**
     * Method used for the duplication of packages in the configurator.
     */
    public function duplicatePackageAction()
    {
        $packageId = $this->getRequest()->getParam('packageId');
        $type = $this->getRequest()->getParam('type');
        if ($packageId) {
            try {
                $items = array();
                /** @var Dyna_Checkout_Model_Sales_Quote $quote */
                $quote = $this->_getQuote();

                /** @var Omnius_Package_Model_Package $oldPackageModel */
                $oldPackageModel = $quote->getCartPackage($packageId);
                if ($oldPackageModel->getSaleType() != Dyna_Catalog_Model_ProcessContext::ACQ) {
                    throw new Exception($this->__("Any package with process context different than ACQ should not be duplicated."));
                }

                $newPackageId = Omnius_Configurator_Model_CartObserver::getNewPackageId($quote);

                // gather all quote items for current duplicate package
                $productIds = [];
                /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
                foreach ($quote->getAllItems() as $item) {
                    if ($item->getPackageId() == $packageId && !$item->isPromo() && !$item->getProduct()->isServiceItem()) {
                        $productIds[] = $item->getProductId();
                        array_push($items, array($item->getProductId() => $item));
                    }
                }

                $quote->saveActivePackageId($newPackageId);

                // Create the new package for the quote
                /** @var Omnius_Package_Model_Package $packageModel */
                $newPackage = Mage::getModel('package/package')
                    ->setData($oldPackageModel->getData())
                    ->setCoupon(null)
                    ->setId(null)
                    ->setBundleId(null)
                    ->setPackageId($quote->getActivePackageId())
                    ->save();

                // todo: determine if this is needed anymore
                $serviceNotAllowed = array();

                // building an array of products rejected from duplicating due to incompatibility with current cart
                $skipped = [];
                $addedProductIds = [];
                foreach ($items as $value) {
                    $item = current($value);
                    if ($productIds && in_array(key($value), $serviceNotAllowed)) {
                        $skipped[key($value)] = $item;
                        continue;
                    }

                    /** @var Dyna_Checkout_Helper_Cart $checkoutCartHelper */
                    $checkoutCartHelper = Mage::helper('dyna_checkout/cart');

                    $newitem = $checkoutCartHelper->addProductToCart(
                        key($value),
                        $item->getData('is_defaulted'),
                        $item->getData('is_service_defaulted'),
                        $item->getData('is_obligated'),
                        $item->getData('sale_type') //this is process_context
                    );
                    $newitem->setRemoveRuleId(',');
                    $newitem->setAppliedRuleIds($item->getAppliedRuleIds());

                    $addedProductIds[] = key($value);
                }

                $warningMessage = array();
                foreach ($skipped as $item) {
                    $warningMessage[] = $item->getName();
                }
                $quote->setCouponsPerPackage(array($newPackage->getPackageId() => $oldPackageModel->getCoupon()));

                /** Initially used by FF modules */
                Mage::dispatchEvent('configurator_cart_duplicate_package', [
                    'quote' => $quote,
                    'old_package' => $oldPackageModel,
                    'new_package' => $newPackage
                ]);


                $allExclusiveFamilyCategoryIds = Mage::getModel('catalog/category')->getAllExclusiveFamilyCategoryIds();
                $exclusiveFamilyCategoryIds = [];
                /** @var Dyna_Package_Model_Package $newPackage `*/
                foreach ($newPackage->getItemsByProductId($addedProductIds) as $item) {
                    if ($categoriesExclusive = array_intersect($item->getProduct()->getCategoryIds(), $allExclusiveFamilyCategoryIds)) {
                        foreach ($categoriesExclusive as $categoryExclusive) {
                            $exclusiveFamilyCategoryIds[$item->getProductId()] = $categoryExclusive;
                        }
                    }
                }
                $checkoutCartHelper->parseServiceDefaultedProducts($addedProductIds, $newPackage, $addedProductIds, $allExclusiveFamilyCategoryIds, $exclusiveFamilyCategoryIds);

                $promoRules = Mage::helper('pricerules')->findApplicablePromoRules(true, $newPackageId);
                $promoCounter = Mage::getSingleton('core/session')->getPromoCounter();
                $promoCounter[$newPackageId] = $promoRules;
                Mage::getSingleton('core/session')->setPromoCounter($promoCounter);

                $this->_getCart()->save();

                // run collectTotals in order to update the cart applied sales rules
                // (in case any stackable promo has been selected)
                $quote->setTotalsCollectedFlag(false)
                    ->collectTotals()
                    ->save();

                $this->jsonResponse(array(
                    'error'         => false,
                    'warning'         => count($warningMessage) ? $this->__("The following products were skipped from duplicating due to incompatibility with current cart: ") . implode(",", $warningMessage) : "",
                    'packageId'     => $this->_getQuote()->getActivePackageId(),
                    'type'          => $type,
                    'rightBlock'    => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->setCurrentPackageId($this->_getQuote()->getActivePackageId())->toHtml(),
                    'totals'        => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                ));
            } catch (Exception $e) {
                Mage::logException($e);
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $e->getMessage(),
                ));
            }

        } else {
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'No package id provided',
            ));
        }
    }

    /**
     * Returns an array with the differences between $array1 and $array2
     *
     * @param $array1
     * @param $array2
     * @return array
     */
    protected function array_diff_r($array1, $array2)
    {
        $result = [];
        foreach ($array1 ?? [] as $key => $value) {
            if (!is_array($array2) || !array_key_exists($key, $array2)) {
                $result[$key] = $value;
                continue;
            }
            if (is_array($value)) {
                $recursiveArrayDiff = $this->array_diff_r($value, $array2[$key]);
                if (count($recursiveArrayDiff)) {
                    $result[$key] = $recursiveArrayDiff;
                }
                continue;
            }
            if ($value != $array2[$key]) {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * The action used for creating single or multiple packages in the configurator.
     */
    public function createPackagesAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        try {
            /** @var Dyna_Configurator_Helper_Cart $cartHelper */
            $cartHelper = $this->getCartHelper();

            $packages = $this->getRequest()->getParam('packages');
            $saleType = $this->getRequest()->getParam('saleType');
            $ctn = $this->getRequest()->getParam('ctn');
            $offerId = $this->getRequest()->getParam('offerId');
            $isOffer = $this->getRequest()->getParam('isOffer');

            foreach ($packages as $package) {
                $newPackageId = $cartHelper->initNewPackage(
                    strtolower($package['type']),
                    $saleType,
                    $ctn,
                    $package['currentProducts'],
                    null,
                    null,
                    null,
                    null,
                    $package['packageCreationTypeId'],
                    $offerId
                );

                $currentProductsIds = explode(',', $package['currentProducts']);
                foreach ($this->_getCart()->getQuote()->getAllAddresses() as $address) {
                    $address->unsetData('cached_items_nonnominal');
                }

                Mage::getSingleton('core/session')->setIsOffer($isOffer);

                Mage::helper('dyna_checkout/cart')->processAddMulti([
                    'type' => $package['type'],
                    'packageId' => $newPackageId,
                    'products' => $currentProductsIds,
                    'clickedItemSelected' => true,
                    'isOffer' => $isOffer
                ]);
            }

            $this->_getCart()->getQuote()->setTotalsCollectedFlag(false);
            $this->_getCart()->save();

            $response = array(
                'error' => false,
                'packageId' => $newPackageId
            );

            Mage::helper('dyna_configurator/UCT')->exitUctFlow();
            $this->jsonResponse($response);
        } catch (Exception $e) {
            Mage::logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__($e->getMessage())
            ));
        }
    }
}
