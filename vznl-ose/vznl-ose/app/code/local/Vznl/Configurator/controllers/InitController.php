<?php

require_once Mage::getModuleDir('controllers', 'Dyna_Configurator') . DS . '/InitController.php';

class Vznl_Configurator_InitController extends Dyna_Configurator_InitController
{
    const DEFAULT_IN_MAGAZINE = 10;
    const DEFAULT_IN_THRESHOLD = 100;

    /**
     * @param $section
     *
     * Get configuration for specific action
     */
    protected function getActionData($section)
    {
        $section = strtolower(str_replace('Action', '', $section));
        $config = [
            'endpoints' => [
                'products' => sprintf('configurator/%s/all', $section),
                'cart.getPopup' => 'configurator/init/getProductpopupinfo',
                'cart.addMulti' => 'configurator/cart/addMulti',
                'cart.delete' => 'configurator/cart/remove',
                'cart.list' => 'configurator/cart/list',
                'cart.empty' => 'configurator/cart/empty',
                'cart.getRules' => 'configurator/cart/getRules',
                'cart.getBundles' => 'configurator/cart/getEligibleBundles',
                'cart.getPrices' => 'configurator/init/getPrices',
                'cart.getdeviceRules' => 'configurator/cart/getDeviceRules',
                'cart.gettariffRules' => 'configurator/cart/getSubscriptionRules',
                'bundles.create' => 'bundles/index/create',
                'bundles.getHint' => 'bundles/index/hint',
                'bundles.updateChoice' => 'bundles/index/updateBundleChoice',
                'bundles.fetchMandatoryFamilies' => 'bundles/index/fetchMandatoryFamilies',
                'redplus.updatePackageParent' => 'configurator/cart/updatePackageParent'
            ],
            'container' => '#config-wrapper',
            'sections' => array_map('strtolower', array_diff(
                Mage::getSingleton('dyna_configurator/catalog')->getOptionsForPackageType($section),
                Mage::getSingleton('dyna_configurator/catalog')->getOrderRestrictions(Mage::app()->getWebsite()->getCode())
            )),
            'type' => $section,
            'spinner' => 'skin/frontend/omnius/default/images/spinner.gif',
            'spinnerId' => '#spinner',
            'cache' => false,
            'include_btw' => Mage::getSingleton('customer/session')->showPriceWithBtw(),
            'hardwareOnlySwap' => Mage::getSingleton('customer/session')->getOrderEditMode() && Omnius_Checkout_Helper_Data::HARDWARE_ONLY_SWAP,
        ];

        return $this->buildConfigurator($section, $config);
    }

    /**
     * Returns product information
     */
    public function getProductsAction()
    {
        try {
            $requestParams = array();
            $request = $this->getRequest();

            $requestParams['websiteId'] = filter_var( trim($request->get('website_id')), FILTER_SANITIZE_NUMBER_INT);
            $requestParams['processContext'] = filter_var( trim($request->get('process_context')), FILTER_SANITIZE_STRING);
            $requestParams['packageCreationTypeId'] = filter_var( trim($request->get('packageCreationTypeId')), FILTER_SANITIZE_NUMBER_INT);
            $requestParams['customerType'] = filter_var( trim($request->get('customer_type')), FILTER_SANITIZE_STRING);
            $requestParams['showPriceWithBtw'] = $request->get('show_price_with_btw');
            $requestParams['axiStoreCode'] = filter_var( trim($request->get('axi_store_code')), FILTER_SANITIZE_STRING);

            $response = Mage::getModel('dyna_catalog/product')->getCatalogProducts($requestParams);

            $date = new DateTime('+1 year');
            return $this->jsonResponse(
                $response,
                200, [
                'Pragma' => 'cache',
                'Cache-Control' => 'max-age=31536000, must-revalidate',
                'Expires' => $date->format('r'),
                'X-Cache-Override' => true,
            ]);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load products',
            ));
        }
    }

    /**
     * Retrieves the stock status for a product via AJAX.
     * @return jsonResponse
     */
    public function getStockAction()
    {
        try {
            $html = Mage::getSingleton('core/layout')->createBlock('core/template')
                ->setTemplate('configurator/widget/stock.phtml')->toHtml();
            $this->getResponse()->setBody($html);
        } catch (Exception $e) {
            Mage::logException($e);
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => true,
                            'message' => $e->getMessage()
                        )
                    )
                );
        }
    }

    /**
     * Retrieves the stock status for all stores for a product via AJAX.
     */
    public function getStoresStockFoundAction()
    {
        $id = filter_var( trim($this->getRequest()->getParam('product_id', 0) ), FILTER_SANITIZE_NUMBER_INT);
        $axiStoreId = filter_var( trim($this->getRequest()->getParam('axi_store_id', 0) ), FILTER_SANITIZE_STRING);

        if ($id == 0 || $axiStoreId == 0) {
            $this->getResponse()->setBody('');
            return $this->getResponse();
        }

        $isTelesales = Mage::helper('agent')->isTelesalesLine();
        if ($isTelesales) {
            $dealers = Mage::getModel('agent/dealer')->getCollection();
        } else {
            $dealers = Mage::helper('agent')->getDealers();
        }
        $dealers->setOrder('city', 'asc')
            ->setOrder('street', 'asc');

        $_product = Mage::getModel('catalog/product')->load($id);
        $maxStoreDistance = Mage::getStoreConfig('vodafone_service/stock_settings/store_max_distance');
        $_productStockCollection = Mage::getModel('stock/stock')->getLiveStockForItem($_product, $axiStoreId, false, $maxStoreDistance);

        /** @var Dyna_Stock_Model_Stock $_productStock */
        $_productStock = $_productStockCollection->setPageSize(1, 1)->getLastItem();
        $_stockStores = new Varien_Data_Collection();
        foreach ($_productStock->getOtherStoreList() as $stock) {
            $_stockStores->addItem(new Varien_Object($stock));
        }

        try {
            $html = Mage::getBlockSingleton('core/template')->setTemplate('configurator/widget/partials/stores_found.phtml')
                ->setData('stockStores', $_stockStores)
                ->setData('dealers', $dealers)
                ->toHtml();
            $this->getResponse()->setBody($html);
        } catch (Exception $e) {
            Mage::logException($e);
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => true,
                            'message' => $e->getMessage()
                        )
                    )
                );
        }
    }

    /**
     * Returns agent helper Data
     * @return Vznl_Agent_Helper_Data
     */
    protected function getAgentHelper()
    {
        return Mage::helper('vznl_agent/data');
    }

    /**
     * Returns stock helper Data
     * @return Vznl_Stock_Helper_Data
     */
    protected function getStockHelper()
    {
        return Mage::helper('stock');
    }

    /**
     * Returns dealers list
     * @param $isTelesales
     * @return mixed
     */
    protected function getDealersList($isTelesales)
    {
        if ($isTelesales) {
            $dealers = Mage::getModel('agent/dealer')->getCollection();
        } else {
            $dealers = Mage::helper('agent')->getDealers();
        }

        $dealers->setOrder('city', 'asc')->setOrder('street', 'asc');

        return $dealers->getData('dealers');
    }

    /**
     * Initializes the configurator based on the package type and sale type.
     * @param $type
     * @param array $config
     * @param string $saleType
     */
    protected function buildConfigurator($type, array $config = array(), $saleType = Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION)
    {
        $packageId = filter_var( trim($this->getRequest()->getParam('packageId')), FILTER_SANITIZE_NUMBER_INT);
        $currentProducts = $this->getRequest()->getParam('current_products');
        $migrationOrMoveOffnet = $this->getRequest()->getParam('migrationOrMoveOffnet');
        $migrationOrMoveOffnetCustomerNo = $this->getRequest()->getParam('migrationOrMoveOffnetCustomerNo');
        $ctn = filter_var($this->getRequest()->getParam('ctn'), FILTER_SANITIZE_NUMBER_INT);
        $offerId = filter_var( trim($this->getRequest()->getParam('offerIdCampaign')), FILTER_SANITIZE_NUMBER_INT);
        $isOffer = $this->getRequest()->getParam('isOffer');
        $hasParentPackage = $this->getRequest()->getParam('has_parent');
        $markBundleId = $this->getRequest()->getParam('bundleId');
        $relatedProductSku = filter_var( trim($this->getRequest()->getParam('relatedProductSku')), FILTER_SANITIZE_STRING);
        $filters = $this->getRequest()->getParam('filters');
        $packageCreationTypeId = filter_var( trim($this->getRequest()->getParam('packageCreationTypeId')), FILTER_SANITIZE_NUMBER_INT);
        $executeClearCart = (int)$this->getRequest()->getParam('executeClearCart');
        $contractEndDate = $this->getRequest()->getParam('contractEndDate');
        $contractStartDate = $this->getRequest()->getParam('contractStartDate');
        $executeClearCart && Mage::helper('dyna_package')->clearServicePackages();
        $groupBundleId = null;
        $linkedAccountSelected = null;
        sort($config['sections'], SORT_NUMERIC);

        // search for permissions
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $contextHelper = Mage::helper('dyna_catalog/processContext');
        $processContext = $contextHelper->getProcessContextCode();
        $agent = $customerSession->getAgent();
        $agentHasAllowCab = $agent->isGranted('ALLOW_CAB');
        $agentHasAllowDsl = $agent->isGranted('ALLOW_DSL');
        $agentHasAllowMob = $agent->isGranted('ALLOW_MOB');
        $agentHasDebitToCredit = $agent->isGranted('DEBIT_TO_CREDIT');
        $agentHasSendSavedOffer = $agent->isGranted('SEND_OFFER');

        // no permission error
        $permissionError = $this->jsonResponse(array(
            'error' => true,
            'message' => $this->__("You do not have the permission for this action")
        ));

        switch (strtoupper($type)) {
            case Dyna_Catalog_Model_Type::TYPE_FIXED_DSL:
            case Dyna_Catalog_Model_Type::TYPE_LTE:
                // disable dsl if general permissions are missing
                if (!$agentHasAllowDsl) {
                    return $permissionError;
                }

                // disable dsl if pre-post and permissions missing
                if ($agentHasDebitToCredit &&
                    !$agentHasSendSavedOffer &&
                    in_array($processContext, [Dyna_Catalog_Model_ProcessContext::MIGCOC])) {
                    return $permissionError;
                }
                break;

            case Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE:
            case Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT:
            case Dyna_Catalog_Model_Type::TYPE_CABLE_TV:
                // disable cable if general permissions are missing
                if (!$agentHasAllowCab) {
                    return $permissionError;
                }
                // disable cable if pre-post and permissions missing
                if ($agentHasDebitToCredit &&
                    !$agentHasSendSavedOffer &&
                    in_array($processContext, [Dyna_Catalog_Model_ProcessContext::MIGCOC])) {
                    return $permissionError;
                }
                break;

            case Dyna_Catalog_Model_Type::TYPE_MOBILE:
            case Dyna_Catalog_Model_Type::TYPE_PREPAID:
                // disable mobile if general permissions are missing
                if (!$agentHasAllowMob) {
                    return $permissionError;
                }
                break;

            default:
                // nothing to do
        }

        $disabledSections = [];

        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('dyna_customer/session');

        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Dyna_Package_Model_Package $packageModel */
        $packageModel = null;
        $firstSubscriptionCustomerNumber = null;
        if ($packageId) {
            $packageModel = $quote->getCartPackage($packageId);
        }
        if (Mage::getSingleton('dyna_customer/session')->getCustomer()->isCustomerLoggedIn()) {
            // Get Customer Number from Cart package (if exists)
            if ($packageModel) {
                $customerNumber = $packageModel->getParentAccountNumber();
            } elseif ($firstSubscriptionCustomerNumber) { // from linked Account Number
                $customerNumber = $firstSubscriptionCustomerNumber;
            } else { // from Session
                $customerNumber = $customerSession->getCustomer()->getCustomerNumber();
            }

            $hasOrders = $this->checkOpenOrders($customerNumber, $packageModel, $type);
            if (!is_bool($hasOrders)) {
                $disabledSections[] = $hasOrders;
            }
        }

        /** @var  $cartHelper  Vznl_Configurator_Helper_Cart */
        $cartHelper = Mage::helper('dyna_configurator/cart');
        $cartHelper->setNewPackagePermanentFilters($filters);
        $saleType = in_array($this->getRequest()->getParam('saleType'), Vznl_Checkout_Model_Sales_Quote_Item::SALETYPE_ARRAY) ? $this->getRequest()->getParam('saleType') : $saleType;
        $extraCTNPackages = array();

        $bundlesHelper = Mage::helper('dyna_bundles');
        try {
            $linkedAccountSelected = null;

            if (!$packageId) {
                $ctns = explode(',', $ctn);
                $descriptions = Mage::getModel('ctn/ctn')->getAllCTNDetails($ctns);
                $getKey = function (array &$array, $key) {
                    return isset($array[$key]) ? $array[$key] : '';
                };

                $packageCTN = array_pop($ctns);
                // check if one-of-deal is active
                $isOneOfDeal = $cartHelper->isOneOfDealActive();
                $oneOfDealOptions = $isOneOfDeal && $packageCTN ? array('retainable' => false) : array();

                if ($packageCTN) {
                    $len = count($ctns);
                    for ($i = 0; $i < $len; ++$i) {
                        $extraCTNPackages[$ctns[$i]] = array(
                            'id' => (int)$cartHelper->initNewPackage(
                                $type,
                                $saleType,
                                $ctns[$i],
                                $getKey($descriptions, $ctns[$i]),
                                $oneOfDealOptions
                            ),
                            'description' => $getKey($descriptions, $ctns[$i]),
                        );
                    }

                    // only store description for last package
                    $extraCTNPackages[$packageCTN] = array(
                        'description' => $getKey($descriptions, $packageCTN),
                    );
                }
                $parentPackageId = null;
                if ($hasParentPackage) {
                    $parentPackageId = $quote->getActivePackage()->getEntityId();
                }

                if (!($packageId = $this->_findExistingPackageIdInQuote($type, $saleType, $packageCTN))) {
                    // Use existing package ID if the package is already in the quote
                    $packageId = (int)$cartHelper->initNewPackage(
                        $type,
                        $saleType,
                        $packageCTN,
                        $currentProducts,
                        $oneOfDealOptions,
                        $parentPackageId,
                        $relatedProductSku,
                        $markBundleId,
                        $packageCreationTypeId,
                        $offerId,
                        $contractEndDate,
                        $contractStartDate,
                        $isOffer
                    );
                }

                //If migration or moveOffnet is in progress, set flag on customer model loaded to customer/session
                if ($migrationOrMoveOffnet) {
                    // the customer number for the migration/move offnet must be set on session for later use in the checkout: we consider it an unique identifier for the service product details
                    Mage::helper('dyna_customer')->setMigrationData($migrationOrMoveOffnet, $migrationOrMoveOffnetCustomerNo, $quote->getCartPackage($packageId)->getId());
                }
            }

            $quote->saveActivePackageId($packageId);

            $cartItems = $cartHelper->getCartItems($type, $packageId);
            $initCart = $cartItems['initCart'];
            $acqProductIds = $cartItems['acqProductIds'];
            $installedBaseCart = $cartItems['installedBaseCart'];
            $defaultedItems = $cartHelper->getDefaultedProductsFromCart($quote, $packageId);
            $defaultedObligatedItems = $cartHelper->getDefaultedObligatedProductsFromCart($quote, $packageId);

            if(isset($initCart['tariff']) && in_array($type, Vznl_Catalog_Model_Type::getMobilePackages())) {
                $subProductId = $initCart['tariff'];
                $allowedSimCards = Mage::helper('vznl_configurator/attribute')->getSimCardTypes($subProductId);
            }

            $config['packageId'] = (int)$packageId;
            $config['sale_type'] = $saleType;
            $config['identifier'] = (int)$quote->getId();
            $config['current_products'] = $currentProducts;
            Mage::register('freeze_models', true, true);
            /** @var Dyna_Package_Model_Package $packageModel */
            $packageModel = $quote->getCartPackage($packageId);
            $config['old_sim'] = !empty($packageModel) ? $packageModel->getId() && $packageModel->getOldSim() : '';

            /** @var Dyna_Catalog_Model_ProcessContext $processContextModel */
            $saleTypeCheck = isset($packageModel) ? $packageModel->getSaleType() : '';
            $processContextId = Mage::getModel('dyna_catalog/processContext')->getProcessContextIdByCode($saleTypeCheck);
            foreach ($config['sections'] as $section) {
                if (Mage::helper('dyna_configurator')->isSectionDisabled($type, $section, $processContextId)) {
                    $disabledSections[] = $section;
                }
            }
            $config['disabledSections'] = $disabledSections;

            // add bundle entry if init configurator will create a bundle
            if ($markBundleId) {
                $bundlesHelper->addBundlePackageEntry($markBundleId, $packageModel->getEntityId());
            }
            if ($groupBundleId) {
                $bundlesHelper->addBundlePackageEntry($groupBundleId, $packageModel->getEntityId());
            }

            if (isset($linkedAccountSelected) && $linkedAccountSelected) {
                $packageModel->setParentAccountNumber($linkedAccountSelected);
            }
            if(isset($packageModel)){
                $packageModel->updateBundleTypes()->save();
            }

            /* If we have a campaign, every quote must have set the data campaign*/
            /** @var Dyna_Customer_Model_Session $session */
            $session = Mage::getSingleton('dyna_customer/session');
            $uctParams = $session->getUctParams();

            if ($uctParams && !empty($uctParams['transactionId']) && !empty($uctParams['campaignId'])) {
                $quote->setTransactionId($uctParams["transactionId"])
                    ->setCampaignCode($uctParams["campaignId"])
                    ->save();
                if ($offerId) {
                    $uctParams['offerId'] = $offerId;
                    $session->setUctParams($uctParams);
                    $packageModel->setOfferId($offerId);
                }
                $packageModel->save();
            }

            // get linked accounts block in case of mobile
            $linkedAccountSelection = "";
            /** @var Dyna_Customer_Model_Customer $customer */
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            /** @var Dyna_Customer_Helper_Customer $customerHelper */
            $customerHelper = Mage::helper('dyna_customer/customer');
            $customerHelper->handleLinkedAccountPerPackage($type, $packageCreationTypeId, $customer, $linkedAccountSelection, $linkedAccountSelected);
            $rightBlock = Mage::getSingleton('core/layout')->createBlock('core/template')
                ->setTemplate('checkout/cart/partials/package_block.phtml')
                ->setData(array(
                    'guiSection' => Dyna_Catalog_Model_Product::GUI_CART
                ))
                ->toHtml();

            /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
            $processContextHelper = Mage::helper("dyna_catalog/processContext");

            $hintMessages = Mage::getSingleton('customer/session')->getHints() ?: array();
            $hintMessages = array_filter($hintMessages, function($hint) use ($packageId) {
                return $hint['packageId'] == $packageId;
            });

            $hardwareDetailsIls = Mage::getSingleton('customer/session')->getHardwareDetailsIls() ?: array();

            $responseArray = array(
                'error' => false,
                'initCart' => count($initCart) ? $initCart : new stdClass(),
                'acqCart' => count($acqProductIds) ? $acqProductIds : new stdClass(),
                'installedBaseCart' => count($installedBaseCart) ? $installedBaseCart : new stdClass(),
                'defaultedItems' => count($defaultedItems) ? $defaultedItems : new stdClass(),
                'defaultedObligatedItems' => count($defaultedObligatedItems) ? $defaultedObligatedItems : new stdClass(),
                'config' => $config,
                'packageId' => (int)$packageId,
                'promoRules' => Mage::helper('pricerules')->findApplicablePromoRules(true),
                'processContextCode' => $processContextHelper->getProcessContextCode(),
                'rightBlock' => mb_convert_encoding($rightBlock, "UTF-8"),
                'linkedAccountSelection' => !in_array($saleType, Dyna_Catalog_Model_ProcessContext::ILSProcessContexts()) ? $linkedAccountSelection : '',
                'salesIds' => [],
                'quoteHash' => Mage::helper('dyna_checkout/cart')->getQuoteProductsHash(),
                'salesId' => $quote->getSalesId(),
                'hintMessages' => $hintMessages,
                'packageEntityId' => !empty($quote->getCartPackage()) ? (int)$quote->getCartPackage()->getEntityId() : '',
                'drc_eligible' => $quote->canShowDeviceRC(),
                'hardwareDetailsIls' => count($hardwareDetailsIls) ? $hardwareDetailsIls : new stdClass(),
                'simCardName' => $allowedSimCards ?? [],
            );

            if (($incompleteSections = Mage::helper('omnius_checkout')->checkPackageStatus($packageId)) && is_array($incompleteSections)) {
                $responseArray['incomplete_sections'] = $incompleteSections;
            }

            if (!empty($extraCTNPackages)) {
                $responseArray['ctnPackages'] = $extraCTNPackages;
            }

            $responseArray['activeBundleId'] = Mage::getSingleton('dyna_customer/session')->getActiveBundleId();
            $specialProducts = !empty($packageModel) ? Mage::helper('dyna_configurator/services')->getSpecialPriceProducts() : [];
            $responseArray['specialPriceProducts'] = $specialProducts['valid'] ?? null;
            $responseArray['specialPriceProductsInvalid'] = $specialProducts['invalid'] ?? null;

            if ($bundlesHelper->isBundleChoiceFlow() && $bundlesHelper->choiceAppliesToActivePackage()) {
                $responseArray['optionsPanel'] = $this->getLayout()->createBlock('dyna_bundles/options')->setTemplate('bundles/options.phtml')->toHtml();
            }

            // Check whether install base bundles have been broken and append an information message to the response
            $customer = $customerSession->getCustomer();
            if (($customer && $packageModel) && ($serviceLineId = $packageModel->getServiceLineId()) && ($customerId = $packageModel->getParentAccountNumber())) {
                if ($customer->isIBProductPartOfBundle($customerId, $serviceLineId) && $packageModel->hasDroppedBundleSubscription()) {
                    $responseArray['infoMessage'] = array(
                        'id' => sprintf("%d", $packageModel->getId()),
                        'message' => $this->__("The install base bundle was broken."),
                        'isModal' => true,
                    );
                }
            }
            return $this->jsonResponse($responseArray);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            return $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load configurator',
            ));
        }
    }

    /**
     * Find existing package ID based on type/sale type/CTN
     * @param string $type
     * @param string $saleType
     * @param string $ctn
     * @return int
     */
    protected function _findExistingPackageIdInQuote($type, $saleType, $ctn)
    {
        // Only look for the package ID if a CTN is set
        if (!$ctn) {
            return false;
        }

        $packages = Mage::getSingleton('checkout/cart')->getQuote()->getPackages();
        foreach ($packages as $package) {
            if (
                $package['type'] === $type &&
                $package['sale_type'] === $saleType &&
                $package['ctn'] === $ctn
            ) {
                return (int)$package['package_id'];
            }
        }

        return false;
    }

    public function preDispatch()
    {
    	$value = Mage::getStoreConfig('agent/general_config/allowed_urls');

    	if ($value && stripos($value,'getPrices') !== false) {
    		$this->setFlag('getPrices', self::FLAG_NO_START_SESSION, true);
    	}

        return get_parent_class(get_parent_class($this))::preDispatch();
    }

    /**
     * Get the updated products prices based on current selection
     */
    public function getPricesAction()
    {
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        $websiteId = $this->getRequest()->getParam('website_id');
        $isRetention = $this->getRequest()->getParam('type');
        $packageType = $this->getRequest()->getParam('packageType');
        $salesId = $this->getRequest()->getParam('sales_id');
        $productIds = array();
        $cacheKey = 'configurator_get_prices_action_' . $websiteId . '_' . $isRetention . '_' . $packageType . "_" . $salesId;
        if ($products = $this->getRequest()->getParam('products', false)) {
            $productIds = array_filter(explode(',', $products), function ($id) {
                return (int)$id;
            });
            //skip levy product if found
            $productIds = Vznl_Catalog_Helper_Data::skipLevyProduct($productIds);
            sort($productIds);
            $cacheKey .= join('_', $productIds);
        }

        if ($cachedPrices = $cache->load($cacheKey)) {
            $prices = unserialize($cachedPrices);
        } else {
            $prices = Mage::helper('dyna_checkout')->setSalesId($salesId)->getUpdatedPrices($productIds, $websiteId, $isRetention, $packageType);
            $cache->save(serialize($prices), $cacheKey, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
        }

        return $this->jsonResponse(
            array(
                'prices' => $prices
            )
        );
    }

    /**
     * Action that parses the shopping cart and renders the extended shopping cart.
     */
    public function expandCartAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        /*Remove peal added product from the cart when returning from extended cart*/
        $Helper = Mage::helper('vznl_validatecart');
        $Helper->removePealAddedProduct();

        $cartPackages=Mage::getSingleton('checkout/cart')->getQuote()->getCartPackages();
        $process_contexts=[];

        if ($cartPackages) {
            foreach ($cartPackages as $cartPackage) {
                $cartPackage->updateUseCaseIndication($cartPackage);
                $process_contexts[$cartPackage->getPackageId()] = $cartPackage->getSaleType();
            }
        }

        $fixedSubscriptionName = '';
        $html=Mage::getSingleton('core/layout')->createBlock('dyna_configurator/rightsidebar')
            ->setTemplate('customer/expanded_cart.phtml')->toHtml();
        $cartProducts=[];

        foreach (Mage::getModel('checkout/session')->getQuote()->getAllItems() as $item) {
            $packageId = $item->getPackageId();
            $productId = $item->getProductId();
            $packageSubType = $this->getPackageSubType($productId);
            if ($packageSubType == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE) {
                $fixedSubscriptionName = $item->getName();
            }
            $cartProducts[$packageId]['products'][] = $item->getProductId();
            $cartProducts[$packageId]['type'] = $item->getPackageType();
            $cartProducts[$packageId]['process_context'] = $process_contexts[$packageId];
        }
        foreach ($cartProducts as $packageId=>$package) {
            sort($package['products']);
            $cartProducts[$packageId] = [
                'products' => $package['products'],
                'type' => $package['type'],
                'process_context' => $package['process_context'],
            ];
        }
        $response = Mage::helper('omnius_core')->jsonResponse(['html' => $html, 'cartProducts' => md5(serialize($cartProducts)), 'fixedSubscriptionName' => $fixedSubscriptionName]);
        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json');
    }

    /**
     * @param $quote
     * @return string
     */
    public function getCartEmailButton($quote)
    {
        return $quote->getCartStatus() != Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK ? '<div class="btn btn-primary" onclick="jQuery.post(\'' . Mage::getUrl('checkout/cart/emailQuote') . '?cartId=' . $quote->getId() . '\', function(response){ jQuery(\'#cart-show-details .message\').html(response.message);})">' . $this->__('Email ' . $quote->getTypeOfCartLiteral()) . '</div>' : '&nbsp;';
    }

    /**
     * @IsGranted({"permissions":["CREATE_PACKAGE"]})
     */
    public function int_tv_fixtelAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        /*Remove peal added products from cart and prevent direct checkout when peal product is removed*/
        $helper = Mage::helper('vznl_validatecart');
        $helper->removePealAddedProduct();
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packages = $quote->getCartPackages();
        if ($packages) {
            foreach ($packages as $package) {
                if ($package->getData('type') == 'int_tv_fixtel' && $package->getData('current_status') == 'completed') {
                    $fixedPacakge = $this->getPackage($package);
                    $fixedPacakge->setData('current_status', 'open');
                    $fixedPacakge->save();
                }
            }
        }
        $this->getActionData(__FUNCTION__);
    }

    /**
     * Retrieve product additional information for info popup
     */
    public function getProductpopupdataAction()
    {
        $request = $this->getRequest();
        if (!$request->getParam('product_id') || !$request->getParam('website_id')) {
            $this->jsonResponse(array('error' => true, 'message' => Mage::helper('omnius_checkout')->__('Incorrect parameters provided')));
        } else {
            $product = Mage::getModel('catalog/product')->load($request->getParam('product_id'));
            $productInformation = Mage::helper('dyna_configurator')->getProductInformation($product);
            $this->jsonResponse($productInformation);
        }
    }

    /**
     * Todo : getProductpopupdataAction() throwing 401 error in ITC3 env
     * Retrieve product additional information for info popup
     */
    public function getProductpopupinfoAction()
    {
        $request = $this->getRequest();
        if (!$request->getParam('product_id') || !$request->getParam('website_id')) {
            $this->jsonResponse(array('error' => true, 'message' => Mage::helper('omnius_checkout')->__('Incorrect parameters provided')));
        } else {
            $product = Mage::getModel('catalog/product')->load($request->getParam('product_id'));
            $productInformation = Mage::helper('dyna_configurator')->getProductInformation($product);
            $this->jsonResponse($productInformation);
        }
    }

    /**
     *
     * Show Details of shopping cart
     */

    public function showDetailsCartAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        if ($this->getRequest()->isXmlHttpRequest()) {
            $params = new Varien_Object($this->getRequest()->getParams());
            /** @var Mage_Sales_Model_Resource_Quote_Collection $collection */
            try {
                if ($params->hasData('cart_id')) {
                    /** @var Mage_Sales_Model_Resource_Quote_Collection $collection */
                    $collection = Mage::getModel('sales/quote')->getCollection()
                        ->addFieldToFilter('entity_id', $params['cart_id'])
                        ->load();

                    /** @var Mage_Sales_Model_Quote $quote */
                    if (!$collection->getItemById($params->getData('cart_id'))) {
                        $this->getResponse()
                            ->setHeader('Content-Type', 'application/json')
                            ->setBody(
                                Mage::helper('core')->jsonEncode(
                                    array('error' => true, 'message' => 'No quote with the provided quote_id found')
                                )
                            );
                        return;
                    }
                    $quote = $collection->getItemById($params->getData('cart_id'));
                    $cartType = $params->getData('cart_type');
                    $createdDate = $params->getData('created_date');

                    $html = Mage::getSingleton('core/layout')->createBlock('core/template')
                        ->assign('quote', $quote)
                        ->setData('carttype', $cartType)
                        ->setData('createddate', $createdDate)
                        ->setTemplate('customer/cart_details.phtml')->toHtml();
                    $this->getResponse()->setBody($html);
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(
                            Mage::helper('core')->jsonEncode(
                                array(
                                    'html' => $html,
                                )
                            )
                        );
                    return;
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $e->getMessage())));
            }
        }
    }

    public function expandPackagePromosDrawerAction()
    {
        $packageId = filter_var( trim($this->getRequest()->getParam('packageId')), FILTER_SANITIZE_NUMBER_INT);
        $package = Mage::getSingleton('checkout/cart')->getQuote()->getCartPackage($packageId);
        $promoRules = Mage::helper('vznl_pricerules')->findApplicablePromoRulesByTarget($packageId);
        $promoRules = Mage::helper('vznl_pricerules')->checkBundleAddedPromosExist(Mage::getSingleton('checkout/cart')->getQuote(), $promoRules);

        $html = $this->getLayout()->createBlock('core/template')
            ->setPackage($package)
            ->setPromoRules($promoRules)
            ->setTemplate('configurator/mobile/sections/partials/general/promo_rules.phtml')->toHtml();

        $response = Mage::helper('omnius_core')->jsonResponse(['html' => $html]);
        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json');
    }

    /**
     * Get html block containing all modals
     */
    public function modalsAction()
    {
        $partialTerminationReasonModel = Mage::getSingleton('validateBasket/partialterminationreason');
        $partialTerminationReasons = [];
        $partialTerminationReasonItems = $partialTerminationReasonModel
                                          ->getCollection()->getItems();
        foreach ($partialTerminationReasonItems as $partialTerminationReasonItem) {
            $partialTerminationReasons[$partialTerminationReasonItem->getCode()] = $partialTerminationReasonItem->getNlValue();
        }

        // Set partial termination sub reasons per main reason code
        $partialTerminationSubreasonModel = Mage::getSingleton('validateBasket/partialterminationsubreason');
        $partialTerminationSubreasons = [];
        $partialTerminationSubreasonItems = $partialTerminationSubreasonModel
                                          ->getCollection()->getItems();
        $competitorEnabledSubreasonCodes = [];
        foreach ($partialTerminationSubreasonItems as $partialTerminationSubreasonItem) {
            if ($partialTerminationSubreasonItem->getIncludeCompetitor() == 1) {
                $competitorEnabledSubreasonCodes[] = $partialTerminationSubreasonItem->getCode();
            }
            $partialTerminationReason = $this->getTerminationReasonModel($partialTerminationReasonModel, $partialTerminationSubreasonItem);
            $mainReasonCode = $partialTerminationReason['code'];
            $subReason = array(
                'code' => $partialTerminationSubreasonItem->getCode(),
                'value' => $partialTerminationSubreasonItem->getNlValue()
            );
            if (array_key_exists($mainReasonCode, $partialTerminationSubreasons)) {
                array_push($partialTerminationSubreasons[$mainReasonCode], $subReason);
            }
            else {
                $partialTerminationSubreasons[$mainReasonCode] = [$subReason];
            }
        }

        // Get fixed line providers
        $operatorServiceProviderModel = Mage::getSingleton('operator/serviceProvider');
        $partialTerminationCompetitors = [];
        $operatorServiceProviderItems = $operatorServiceProviderModel
                                          ->getCollection()
                                          ->addFieldToFilter('fixed_line', 1)
                                          ->getItems();
        foreach ($operatorServiceProviderItems as $operatorServiceProviderItem) {
            $partialTerminationCompetitors[$operatorServiceProviderItem->getCode()] = $operatorServiceProviderItem->getName();
        }

        $partialTerminationReasonModel->setData('partialTerminationReasons', $partialTerminationReasons);
        $partialTerminationSubreasonModel->setData('partialTerminationSubreasons', $partialTerminationSubreasons);
        $partialTerminationSubreasonModel->setData('competitorEnabledSubreasonCodes', $competitorEnabledSubreasonCodes);
        $operatorServiceProviderModel->setData('partialTerminationCompetitors', $partialTerminationCompetitors);
        $this->loadLayout();
        $this->getLayout()->removeOutputBlock('root')->addOutputBlock('content');
        $this->renderLayout();
    }

    protected function getPackageSubType($productId)
    {
        return Mage::getModel('vznl_catalog/product')->load($productId)->getAttributeText('package_subtype');
    }

    protected function getPackage($package)
    {
        return Mage::getModel("package/package")->load($package->getData('entity_id'));
    }

    protected function getTerminationReasonModel($partialTerminationReasonModel, $partialTerminationSubreasonItem)
    {
        return $partialTerminationReasonModel->load($partialTerminationSubreasonItem->getMainReasonId());
    }

    /**
     * Fetches the stocks for the accessories.
     */
    public function getAccessoriesStockAction()
    {
        /** @var Omnius_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel('catalog/product');
        $websiteId = $this->getRequest()->getParam('website_id');
        $axiStoreId = $this->getRequest()->getParam('axi_store_id', 0);
        $result = $productModel->getStockCall($websiteId, Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY, $axiStoreId);
        $this->jsonResponse($result);
    }

    /**
     * Returns JSON response
     */
    protected function jsonResponse($data, $statusCode = 200, $headers=[]) {
        parent::jsonResponse($data, $statusCode, $headers);
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=utf-8');
    }
}
