<?php

/**
 * Class Vznl_Configurator_Model_Expression_Serviceability
 */
class Vznl_Configurator_Model_Expression_Serviceability extends Varien_Object
{
    /**
     * @var string
     */
    protected $postcode = false;
    protected $serviceabilityData = null;
    protected $validateData = null;

    /**
     * Vznl_Configurator_Model_Expression_Serviceability constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $addressData = json_decode(Mage::getSingleton('dyna_address/storage')->getData('addressData'), true);

        if ($addressData !== null) {
            $this->serviceabilityData = $addressData['serviceability'];
        }

        // Get Validate address from current quote; set in checkout/index/saveServiceAddress/
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $this->validateData = $quote->getServiceAddress() ? unserialize($quote->getServiceAddress()) : null;
    }

    /**
     * This function should parse the validate data response and look after postalCode
     * @return null|string
     */
    public function getPostcode()
    {
        if (!$this->postcode && $this->validateData && isset($this->validateData['postalcode'])) {
            $this->postcode = trim($this->validateData['postalcode']);
        }

        return $this->postcode;
    }

    /**
     * Validates the provided list of postcodes against the serviceability postcode
     * @param $postcodesList - an array of postcodes with wildcards (*)
     * @return bool - true if any of the postcodes provided match the serviceability postcode
     */
    public function postcodeMatches(...$postcodesList): bool
    {
        if (!$this->getPostcode()) {
            return false;
        }

        foreach ($postcodesList as $postcode) {
            if ($this->validateOnePostcode($postcode)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Validates a specific
     * @param $postcode
     * @return bool
     */
    protected function validateOnePostcode($postcode)
    {
        if ($postcode === '*') {
            return true;
        }

        return fnmatch($postcode, $this->getPostcode(), FNM_NOESCAPE | FNM_CASEFOLD);
    }

    /**
     * Validates a specific item from the serviceability response
     * If the item exists, then return its value
     * @param $itemKey
     * @return bool
     */
    public function item($itemKey)
    {
        $updatedAddressData = json_decode(Mage::getSingleton('dyna_address/storage')->getData('addressData'), true);
        if($itemKey=='footprint'){
            return Mage::getSingleton('checkout/cart')->getQuote()->getFootprint();
        }

        // get serviceability response registered in getRulesAction
        $serviceabilityResponse = $updatedAddressData['serviceability'];

        // if response is incorrect, return false
        if (empty($serviceabilityResponse['data'])) {
            return false;
        }

        // parse response and look for items
        foreach ($serviceabilityResponse['data'] as $service) {
            // if items exist
            if (!empty($items = $service['attributes']['items'])) {
                foreach ($items as $item) {
                    // if key matches the one we're looking for
                    if (strtolower($item['key']) == strtolower($itemKey)) {
                        // return value to further use it in the service expression
                        return $item['value'];
                    }
                }
            }
        }
        // if no footprint item has been found, return false
        return false;
    }

    /**
     * Validates if the install base address footprint matches the service address footprint
     * @return bool
     */
    public function isCFM()
    {
        //retrieve customer address footprint
        /** @var Vznl_Customer_Model_Customer $currentCustomer */
        $customers = Mage::helper('vznl_configurator')->getCustomersForCurrentStack();

        if (count($customers) == 0) {
            return false;
        }


        $ILSFootprint = '';
        foreach ($customers as $ban => $currentCustomer) {
            foreach ($currentCustomer['contracts'] as $contract) {
                foreach ($contract['subscriptions'] as $subscriptionData) {
                    if (!array_key_exists('addresses', $subscriptionData)) {
                        break 2;
                    }

                    foreach ($subscriptionData['addresses'] as $address) {
                        if (isset($address['footprint'])) {
                            $ILSFootprint = $address['footprint'];
                            break 4;
                        }
                    }
                }
            }
        }

        $serviceabilityFootprint = Mage::getSingleton('checkout/cart')->getQuote()->getFootprint();
        if(isset($this->serviceabilityData['data'])) {
            foreach ($this->serviceabilityData['data'] as $serviceabilityArray) {
                if (!empty($serviceabilityFootprint)) {
                    break;
                }
                if (isset($serviceabilityArray['attributes']['items']) && isset($serviceabilityArray['attributes'])) {
                    foreach ($serviceabilityArray['attributes']['items'] as $item) {
                        if (strtolower($item['key']) == 'footprint') {
                            $serviceabilityFootprint = $item['value'];
                            break;
                        }
                    }
                }
            }
        }

        //ILS footprint do not match new installation address(from serviceability) footprint => is Cross Footprint Move case
        return strtolower($ILSFootprint) != strtolower($serviceabilityFootprint);
    }
}