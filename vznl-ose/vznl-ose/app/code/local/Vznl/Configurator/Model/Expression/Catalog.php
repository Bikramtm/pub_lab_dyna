<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 *
 * Class Vznl_Configurator_Model_Expression_Catalog
 */
class Vznl_Configurator_Model_Expression_Catalog extends Dyna_Configurator_Model_Expression_Catalog
{
    /** @var Dyna_Cache_Model_Cache $_cache */
    protected $_cache = null;
    protected $prodSpecGroupsValues = [];
    protected $serviceabilityData = null;
    protected $validateData = null;
    const SERVICABILITY_OR_DELIMITER = '|';

    /**
     * Vznl_Configurator_Model_Expression_Catalog constructor.
     * @param $packageType
     */
    public function __construct($packageType)
    {
        parent::__construct($packageType);

        $addressData = json_decode(Mage::getSingleton('dyna_address/storage')->getData('addressData'), true);

        if (!empty($addressData) && $addressData['serviceability']) {
            $this->serviceabilityData = $addressData['serviceability'];
        }

        // Get Validate address from current quote; set in checkout/index/saveServiceAddress/
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $this->validateData = $quote->getServiceAddress() ? unserialize($quote->getServiceAddress()) : null;
    }

    /**
     * Get prodSpecGroup values from address data
     * @return array
     */
    public function getProdSpecGroupValues()
    {
        if(empty($this->serviceabilityData)) {
            return [];
        }
        $prodSpecGroupsValues = [];
        foreach ($this->serviceabilityData['data'] as $serviceItem) {
            if ($serviceItem["id"] === 'cable-internet-phone') {
                foreach ($serviceItem['attributes']['items'] as $attribute){
                    if($attribute['value'] === false || strcasecmp($attribute['value'], 'false') === 0){
                        continue;
                    }
                    $prodSpecGroupsValues[strtolower($attribute['key'])] = $attribute['value'];
                }
            }
        }

        return $prodSpecGroupsValues;
    }

    /**
     * @param $prodSpecGroupsValues
     */
    public function setProdSpecGroupsValues($prodSpecGroupsValues)
    {
        $this->prodSpecGroupsValues = $prodSpecGroupsValues;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * Get all products that have the requires_serviceability flag set to true
     * @return bool|false|Mage_Core_Model_Abstract|mixed
     */
    public function getProductsThatRequireServiceability()
    {
        $cacheKey = "products_that_have_required_serviceability";

        if (($serviceabilityProducts = $this->getCache()->load($cacheKey)) && $serviceabilityProducts = unserialize($serviceabilityProducts)) {
            return $serviceabilityProducts;
        }

        //Products collection with requires_serviceability true
        $products = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('entity_id')
            ->addAttributeToFilter('requires_serviceability', array('eq' => 1));
        $serviceabilityProducts = $products->load()->getColumnValues('entity_id');
        //save it to cache
        $this->getCache()->save(serialize($serviceabilityProducts), $cacheKey, array($this->getCache()::PRODUCT_TAG));
        return $serviceabilityProducts;
    }

    /**
     * Get all products that have the requires_serviceability_items matching the response from servicability
     * @return array|bool|false|Mage_Core_Model_Abstract|mixed
     */
    public function getProductsWithAvailableServiceabilityValue()
    {
        $serviceabilityProducts = [];
        $prodSpecGroupValues = $this->getProdSpecGroupValues();
        $prodSpecGroupKeys = array_keys($prodSpecGroupValues);

        if (empty($prodSpecGroupValues)) {
            return $serviceabilityProducts;
        }

        $cacheKey = "products_that_have_required_serviceability_items" . Mage::app()->getWebsite()->getId();
        if ($cachedResults = $this->getCache()->load($cacheKey)) {
            $products = unserialize($cachedResults);
        }else{
            //Products collection
            $products = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(["entity_id", "required_serviceability_items"])
                ->addAttributeToFilter('required_serviceability_items', array('neq' => ''))
                ->addAttributeToFilter('required_serviceability_items', array('notnull' => true))
                ->load();

            $this->getCache()->save(serialize($products->toArray()), $cacheKey, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        $serviceabilityProducts = $this->getServiceableProducts($products, $prodSpecGroupValues);

        return $serviceabilityProducts;
    }

    /**
     * validate all products that have the requires_serviceability_items to match the response from servicability
     * @return array|bool|false|Mage_Core_Model_Abstract|mixed
     */
    public function getServiceableProducts($products, $prodSpecGroupValues = [])
    {
        $validServiceabilityProducts = [];

        if(!$prodSpecGroupValues) {
            $prodSpecGroupValues = $this->getProdSpecGroupValues();
        }
        $prodSpecGroupKeys = array_keys($prodSpecGroupValues);

        foreach ($products as $product) {
            $productValues = array_map("trim", explode(",", $product['required_serviceability_items']));

            //check if product's serviceability items are available in the AddressDetails->prodSpecGroups
            foreach($productValues as $productValue){
                $productValue = trim($productValue,'"');
                $productValue = strtolower($productValue);
                if(strpos($productValue, self::SERVICABILITY_OR_DELIMITER) !== false){
                    $extendedProductValues = explode(self::SERVICABILITY_OR_DELIMITER, $productValue);
                    if(empty(array_intersect($extendedProductValues, $prodSpecGroupKeys))){
                        continue 2;
                    }
                }
                elseif(strpos($productValue, '=') > 0){
                    list($productKey, $productValueCompare) = explode('=', $productValue);

                    if(!in_array($productKey, $prodSpecGroupKeys) || $productValueCompare != $prodSpecGroupValues[$productKey]){
                        continue 2;
                    }
                }else{
                    if(!in_array($productValue, $prodSpecGroupKeys)){
                        continue 2;
                    }
                }
            }

            //if all checks passed, add product to result
            $validServiceabilityProducts[] = $product['entity_id'];
        }

        return $validServiceabilityProducts;
    }

    /**
     * Get all products that have the requires_serviceability_items not matching the response from servicability
     * @return array
     * @throws Mage_Core_Exception
     */
    public function getProductsWithoutAvailableServiceabilityValue()
    {
        $serviceabilityProducts = [];
        $prodSpecGroupValues = $this->getProdSpecGroupValues();
        $prodSpecGroupKeys = array_keys($prodSpecGroupValues);

        if (empty($prodSpecGroupValues)) {
            return $serviceabilityProducts;
        }

        $cacheKey = "products_that_have_no_required_serviceability_items" . Mage::app()->getWebsite()->getId();
        if ($cachedResults = $this->getCache()->load($cacheKey)) {
            $products = unserialize($cachedResults);
        }else{
            //Products collection
            $products = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(['entity_id', 'required_serviceability_items', 'requires_serviceability'])
                ->addAttributeToFilter('requires_serviceability', array('eq' => 1))
                ->load();

            $this->getCache()->save(serialize($products->toArray()), $cacheKey, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        foreach ($products as $product) {
            $serviceabilityProducts[$product['entity_id']] = $product['entity_id'];
            $productValues = array_map("trim", explode(",", $product['required_serviceability_items']));

            //check if product's serviceability items are available in the AddressDetails->prodSpecGroups
            foreach($productValues as $productValue){
                $productValue = trim($productValue,'"');
                $productValue = strtolower($productValue);
                if(strpos($productValue, self::SERVICABILITY_OR_DELIMITER) !== false){
                    $extendedProductValues = explode(self::SERVICABILITY_OR_DELIMITER, $productValue);
                    if(empty(array_intersect($extendedProductValues, $prodSpecGroupKeys))){
                        continue 2;
                    }
                }
                elseif(strpos($productValue, '=') > 0){
                    list($productKey, $productValueCompare) = explode('=', $productValue);

                    if(!in_array($productKey, $prodSpecGroupKeys) || $productValueCompare != $prodSpecGroupValues[$productKey]){
                        continue 2;
                    }
                }else{
                    if(!in_array($productValue, $prodSpecGroupKeys)){
                        continue 2;
                    }
                }
            }

            //if all checks passed, add product to result
            unset($serviceabilityProducts[$product['entity_id']]);
        }

        return array_values($serviceabilityProducts);
    }

    /**
     * @param null $packageSubtype
     * @param $attributeId
     * @param $operator
     * @param null $attributeValue
     * @return array
     */
    public function getProductsWithAttributeValue($packageSubtype = null, $attributeId, $operator, $attributeValue = null)
    {
        if (($packageSubtype == null && (($attributeValue == null) || ($attributeId == null) || ($operator == null)))) {
            return [];
        }

        /** @var Vznl_Configurator_Helper_Expression $evaluatorHelper */
        $evaluatorHelper = Mage::helper('dyna_configurator/expression');
        $productsCollection = $evaluatorHelper->getProductsCollectionByAttr($packageSubtype, $attributeId, $operator, $attributeValue, []);

        return $productsCollection->load()->getColumnValues('entity_id');
    }

    /**
     * Return list of device RCs with higher herarchy than current device rc
     * @return array
     */
    public function getDeviceRCsWithHierarchy($operator)
    {
        $return = array();
        $currentHierarchyValue = 0;
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackage = $quote->getActivePackage();
        foreach($activePackage->getItems() as $item){
            if($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)){
                $currentHierarchyValue = $item->getProduct()->getHierarchyValue();
                break;
            }
        }

        foreach($this->iterationProducts as $product){
            if(strcasecmp(current($product['type']), Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION) === 0 ){
                $comparison = false;
                switch ($operator){
                    case '>':
                        $comparison = $product['hierarchy_value'] > $currentHierarchyValue;
                        break;
                    case '<':
                        $comparison = $product['hierarchy_value'] < $currentHierarchyValue;
                        break;
                    case '=':
                        $comparison = $product['hierarchy_value'] == $currentHierarchyValue;
                        break;
                    case '<=':
                        $comparison = $product['hierarchy_value'] <= $currentHierarchyValue;
                        break;
                    case '>=':
                        $comparison = $product['hierarchy_value'] >= $currentHierarchyValue;
                        break;
                }
                if($comparison) {
                    $return[] = $product['entity_id'];
                }
            }
        }

        return $return;
    }

    /**
     * Return category data based on a list of category ids
     * @param $categoryIds
     * @return array
     */
    protected function getCategoryData($categoryIds)
    {
        $categoryData = [];
        if (!$categoryIds) {
            return $categoryData;
        }

        foreach ($categoryIds as $id) {
            if (!isset($this->allCategories[$id])) {
                continue;
            }
            // gather unique ids
            if ($categoryUniqueId = $this->allCategories[$id]['unique_id'] ?? null) {
                $categoryData['unique_id'][$categoryUniqueId] = 1;
            }
            // gather category paths as lower case
            $categoryPathArray = explode("/", $this->allCategories[$id]['path']);
            // remove root and default category
            unset($categoryPathArray[0]);
            unset($categoryPathArray[1]);
            $categoryPath = implode(
                Mage::helper('dyna_configurator/expression')->getCategoryPathSeparator(),
                array_map(function ($categoryId) {
                    return strtolower($this->allCategories[$categoryId]['name']);
                }, $categoryPathArray)
            );

            $categoryData['path'][$categoryPath] = 1;
        }

        return $categoryData;
    }
}
