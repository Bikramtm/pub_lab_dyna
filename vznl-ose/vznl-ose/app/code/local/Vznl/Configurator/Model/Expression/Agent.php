<?php

/**
 * Class Vznl_Configurator_Model_Expression_Agent
 */
class Vznl_Configurator_Model_Expression_Agent extends Dyna_Configurator_Model_Expression_Agent
{
    /**
     * Check if the login agent has hasDealerGroup.
     * agent.hasDealerGroup('dealerGroup')
     * @param $dealerGroup
     * @return bool
     */
    public function hasDealerGroup($dealerGroup)
    {
        $dealerGroup  = array_map("trim", explode(',', trim($dealerGroup)));
        $currentAgentDealerGroup = Mage::helper('vznl_agent')->getCurrentDealerGroupsNames();
        if (array_intersect($dealerGroup, $currentAgentDealerGroup)) {
            return true;
        }
        return false;
    }

    /**
     * Check if the login agent has hasDealerCode.
     * agent.hasDealerCode('dealerCode')
     * @param $dealerCode
     * @return bool
     */
    public function hasDealerCode($dealerCode)
    {
        $dealerCode = array_map("trim", explode(',', trim($dealerCode)));
        $currentAgentDealerCode = Mage::helper('vznl_agent')->getDaDealerCode();
        if (in_array($currentAgentDealerCode, $dealerCode)) {
            return true;
        }
        return false;
    }

    /**
     * Check if the login agent has hasAgentId.
     * agent.hasAgentId('agentId')
     * @param $agentId
     * @return bool
     */
    public function hasAgentId($agentId)
    {
        $agentId = array_map("trim", explode(',', trim($agentId)));
        $currentAgentId = Mage::helper('vznl_agent')->getAgentId();
        if (in_array($currentAgentId, $agentId)) {
            return true;
        }
        return false;
    }
}