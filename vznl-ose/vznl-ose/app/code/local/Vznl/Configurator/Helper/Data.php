<?php

/**
 * Class Vznl_Configurator_Helper_Data
 */
class Vznl_Configurator_Helper_Data extends Dyna_Configurator_Helper_Data
{
    const PACKAGE_SUBTYPE_LIFECYCLE_STATUS_LIVE = 'live';
    const PACKAGE_SUBTYPE_LIFECYCLE_STATUS_FUTURE = 'future';
    public $customerResponseByStack = array();
    /**
     * Get generic product information
     *
     * @param Vznl_Catalog_Model_Product $product The product to get the information from/for.
     * @return array The item information.
     */
    public function getProductInformation($product)
    {
        if (!$product->getAdditionalText()) {
            //    If sim or not hardware no item information is required
            if ($product->isSim() || !$product->isOfHardwareType()) {
                return false;
            }
        }
        $sku = $product->getData('sku');
        $image = Mage::getBaseUrl().'media/product_image/No_Image_FallBack.png';
        foreach (['png', 'jpg'] as $extension) {
            $imageLocation = '/product_image/'.$sku.'.'.$extension;
            if (!file_exists(Mage::getBaseDir('media').$imageLocation)) {
                continue;
            }
            $image = Mage::getBaseUrl().'media'.$imageLocation;
            break;
        }

        $productInformation = [
            'entity_id' => $product->getEntityId(),
            'name' => $product->getName(),
            'additional_text' => $product->getAdditionalText(),
            'package_type' => $this->getPackageType($product),
            'package_sub_type' => strtolower($product->getAttributeText('package_subtype')),
            'image' => $image,
            'sections' => [
                'overview' => [
                    'section_name' => Mage::helper('dyna_configurator')->__('Overview'),
                    'image' => '',
                    'short_description' => $product->getData('short_description'),
                    'additional_text' => $product->getData('additional_text'),
                    'image' => $image,
                ],
            ]
        ];

        if ($product->isDevice()) {
            $productInformation = array_merge($productInformation, $this->getDeviceInformation($product, $productInformation));
        }

        if ($product->isAccessory()) {
            $productInformation = array_merge($productInformation, $this->getAccessoryInformation($product, $productInformation));
        }

        return $productInformation;
    }

    /**
     * getPackageType
     * @param Vznl_Catalog_Model_Product $product
     * * @return array
     */
    public function getPackageType($product)
    {
        $packageTypes = explode(',', $product->getResource()->getAttribute('package_type')->getFrontend()->getValue($product));
        $mappedPackageTypes = array_map('strtolower', $packageTypes);
        return $mappedPackageTypes;
    }

    /**
     * Get device information
     *
     * @param Vznl_Catalog_Model_Product $product The product to get the information from/for.
     * @param array $productInformation The current already filled array with product information.
     * @return array The product information.
     */
    protected function getDeviceInformation($product, $productInformation = [])
    {
        if (!isset($productInformation['sections'])) {
            $productInformation = ['sections' => []];
        }
        $productInformation['sections']['device'] = [
            'section_name' => Mage::helper('dyna_configurator')->__('Device'),
            'data' => [
                Mage::helper('dyna_configurator')->__('Operating system') => $this->translateProductAttribute($product,'prodspecs_besturingssysteem'),
                Mage::helper('dyna_configurator')->__('Size (hxwxt)') => implode(' x ',
                [
                    $product->getData('prodspecs_afmetingen_lengte'),
                    $product->getData('prodspecs_afmetingen_breedte'),
                    $product->getData('prodspecs_afmetingen_dikte')
                ]) . ' ' . $product->getData('prodspecs_afmetingen_unit'),
                Mage::helper('dyna_configurator')->__('Weight') => $this->translateProductAttribute($product,'prodspecs_weight_gram', '') . ' ' . Mage::helper('dyna_configurator')->__('gram'),
                Mage::helper('dyna_configurator')->__('Battery') => $this->translateProductAttribute($product,'prodspecs_batterij', '') . ' ' . Mage::helper('dyna_configurator')->__('mAh'),
                Mage::helper('dyna_configurator')->__('Screen') => $this->translateProductAttribute($product,'prodspecs_schermdiagonaal_inch', '') . '"',
                Mage::helper('dyna_configurator')->__('Screen resolution') => $this->translateProductAttribute($product,'prodspecs_schermresolutie', '') . ' ' . Mage::helper('dyna_configurator')->__('px'),
                Mage::helper('dyna_configurator')->__('Touchscreen') => $this->translateProductAttribute($product,'prodspecs_touchscreen'),
                Mage::helper('dyna_configurator')->__('Camera') => $this->translateProductAttribute($product,'prodspecs_camera'),
                Mage::helper('dyna_configurator')->__('Camera resolution') => $this->translateProductAttribute($product,'prodspecs_megapixels_1e_cam', '') . ' ' . Mage::helper('dyna_configurator')->__('Mpix'),
                Mage::helper('dyna_configurator')->__('Processor') => $this->translateProductAttribute($product,'prodspecs_processor'),
                Mage::helper('dyna_configurator')->__('Memory') => $this->translateProductAttribute($product,'prodspecs_werkgeheugen_mb', '') . ' ' . Mage::helper('dyna_configurator')->__('MB'),
                Mage::helper('dyna_configurator')->__('Standby time') => $this->translateProductAttribute($product,'prodspecs_standby_tijd'),
                Mage::helper('dyna_configurator')->__('Talk time') => $this->translateProductAttribute($product,'prodspecs_spreektijd'),
                Mage::helper('dyna_configurator')->__('Network Capabilities') => $this->translateProductAttribute($product,'prodspecs_network'),
            ],
        ];

        $productInformation['sections']['entertainment'] = [
            'section_name' => Mage::helper('dyna_configurator')->__('Entertainment'),
            'data' => [
                Mage::helper('dyna_configurator')->__('HD recording') => $this->translateProductAttribute($product,'prodspecs_filmen_in_hd'),
                Mage::helper('dyna_configurator')->__('Audio out') => $this->translateProductAttribute($product,'prodspecs_audio_out'),
                Mage::helper('dyna_configurator')->__('HDMI') => $this->translateProductAttribute($product,'prodspecs_hdmi'),
                Mage::helper('dyna_configurator')->__('2nd camera') => $this->translateProductAttribute($product,'prodspecs_2e_camera'),
            ],
        ];

        $productInformation['sections']['connection'] = [
            'section_name' => Mage::helper('dyna_configurator')->__('Connection'),
            'data' => [
                Mage::helper('dyna_configurator')->__('4g lte') => $this->translateProductAttribute($product,'prodspecs_4g_lte'),
                Mage::helper('dyna_configurator')->__('3g') => $this->translateProductAttribute($product,'prodspecs_3g'),
                Mage::helper('dyna_configurator')->__('GPRS edge') => $this->translateProductAttribute($product,'prodspecs_gprs_edge'),
                Mage::helper('dyna_configurator')->__('WiFi frequencies') => $this->translateProductAttribute($product,'prodspecs_wifi_frequenties'),
                Mage::helper('dyna_configurator')->__('Bluetooth') => $this->translateProductAttribute($product,'prodspecs_bluetooth'),
                Mage::helper('dyna_configurator')->__('Bluetooth version') => $this->translateProductAttribute($product,'prodspecs_bluetooth_version'),
                Mage::helper('dyna_configurator')->__('NFC') => $this->translateProductAttribute($product,'prodspecs_nfc'),
                Mage::helper('dyna_configurator')->__('GPS') => $this->translateProductAttribute($product,'prodspecs_gps'),
            ],
        ];

        $productInformation['sections']['otherspecs'] = [
            'section_name' => Mage::helper('dyna_configurator')->__('Other specs'),
            'data' => [
                //Mage::helper('dyna_configurator')->__('Light sensor') => $product->getData('prodspecs_lichtsensor'),
                Mage::helper('dyna_configurator')->__('Expandable storage') => $this->translateProductAttribute($product,'prodspecs_opslag_uitbreidbaar'),
                Mage::helper('dyna_configurator')->__('Light (flash)') => $this->translateProductAttribute($product,'prodspecs_flitser'),
                //Mage::helper('dyna_configurator')->__('Voice dialing') => $product->getData('prodspecs_voice_dialing'),
                //Mage::helper('dyna_configurator')->__('Voice commands') => $product->getData('prodspecs_spraakbesturing'),
                Mage::helper('dyna_configurator')->__('Removable battery pack') => $this->translateProductAttribute($product,'prodspecs_accu_verwisselb'),
            ],
        ];

        return $productInformation;
    }

    /**
     * @param Vznl_Catalog_Model_Product $product The product to get the information from/for.
     * @param string $attrCode
     * @param string $default
     *
     * @return null|string
     */
    private function translateProductAttribute($product, $attrCode, $default = '-')
    {
        //for a attribute option type
        $val = $product->getAttributeText($attrCode);
        //for any other attribute type
        if($val == false) {
            $val = $product->getData($attrCode);
        }

        return $val ?? $default;
    }

    /**
     * Get accessory information
     *
     * @param Vznl_Catalog_Model_Product $product The product to get the information from/for.
     * @param array $productInformation The current already filled array with product information.
     * @return array The product information.
     */
    protected function getAccessoryInformation($product, $productInformation = [])
    {
        if (!isset($productInformation['sections'])) {
            $productInformation = ['sections' => []];
        }
        $productInformation['sections']['specifications'] = [
            'section_name' => Mage::helper('dyna_configurator')->__('Specifications'),
            'data' => [
                Mage::helper('dyna_configurator')->__('Size (hxwxt)') => implode(' x ',
                [
                    $product->getData('prodspecs_afmetingen_lengte'),
                    $product->getData('prodspecs_afmetingen_breedte'),
                    $product->getData('prodspecs_afmetingen_dikte')
                ]) . ' ' . $product->getData('prodspecs_afmetingen_unit'),
                Mage::helper('dyna_configurator')->__('Weight') => $product->getData('prodspecs_weight_gram') . ' ' . Mage::helper('dyna_configurator')->__('gram'),
            ],
        ];

        return $productInformation;
    }

    /**
     * Get devices for Indirect
     *
     * @return array|mixed
     */
    public function getDevicesForIndirect()
    {
        $key = serialize(array(strtolower(__METHOD__)));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $attribute = Mage::getSingleton("eav/config")
                ->getAttribute('catalog_product', Vznl_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);

            $subTypes = [];
            foreach (Vznl_Catalog_Model_Product::DEVICE_TYPES as $deviceType) {
                $subTypes[] = Mage::helper('dyna_catalog')
                    ->getAttributeAdminLabel($attribute, null, $deviceType);
            }

            $attributeSetValues = [];
            foreach (['voice_device', ' data_device', 'Mobile_Device'] as $setName) {
                $attributeSetValue = Mage::getResourceModel('eav/entity_attribute_set_collection')
                    ->addFieldToFilter('attribute_set_name', $setName)->setPageSize(1, 1)->getLastItem();
                $attributeSetValues[] = $attributeSetValue->getId();
            }

            $websiteId = Mage::app()->getStore(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE)->getWebsiteId();
            /** @var Dyna_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addWebsiteFilter($websiteId)
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('prodspecs_brand')
                ->addAttributeToFilter('attribute_set_id', ['in' => $attributeSetValues])
                ->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)
                ->addAttributeToFilter(Vznl_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, ['in' => $subTypes])
                ->addAttributeToFilter('is_deleted', false)
                ->load();
            $result = [];
            foreach ($collection as $prod) {
                $result[] = $prod->getName();
            }
            ksort($result);
            $this->getCache()->save(serialize($result), $key, array(Vznl_Configurator_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }
        return $result;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @return array
     */
    public function getBlockRowData($item)
    {
        /** @var Vznl_Catalog_Model_Product $product */
        $product = $item->getProduct();
        $originalItem = $item;
        $result = array(
            'name' => $product->getDisplayNameCart() ?: $product->getName(),
            'hashed' => false,
        );

        if ($product->isPromo()) {
            $result = $this->getPromoProductBlock($item, $product, $originalItem, $result);
        } elseif (($item->isPromo() || $item->isBundlePromo()) && $item->getTargetId()) {
            // to actually get the correct discount amount, we need parent quote item
            $result['price'] = $result['priceTax'] = -1 * abs($item->getAppliedPromoAmount());
            $result['maf'] = $result['mafTax'] = -1 * abs($item->getAppliedMafPromoAmount());
            $result['itTargetsProduct'] = 1;
        } elseif ($item->isPromo()) {
            $result['price'] = Mage::helper('tax')->getPrice($product, $item->getRowTotal(), false);
            $result['priceTax'] = Mage::helper('tax')->getPrice($product, $item->getRowTotalInclTax(), false);
            $result['maf'] = Mage::helper('tax')->getPrice($product, $item->getMaf(), false);
            $result['mafTax'] = Mage::helper('tax')->getPrice($product, $item->getMafInclTax(), true);
        }

        if ($product->isPromo() || $item->isPromo() || $item->isBundlePromo()) {
            return $result;
        }

        $result['maf'] = (float)$item->getMaf();
        $result['mafTax'] = (float)$item->getMafInclTax();
        $result['price'] = (float)$item->getPrice();
        $result['priceTax'] = (float)$item->getPriceInclTax();

        if (!$product->isPromo()) {
            $result['price'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getSpecialPrice() ?: $product->getPrice()) : $item->getItemFinalPriceInclTax(),
                false
            );
            $result['priceTax'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getSpecialPrice() ?: $product->getPrice()) : $item->getItemFinalPriceInclTax(),
                true
            );
            $result['maf'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getMaf()) : $item->getHideOriginalPrice() ? $item->getItemFinalMafExclTax() : $item->getMaf(),
                true
            );
            $result['mafTax'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getMaf()) : $item->getHideOriginalPrice() ? $item->getItemFinalMafInclTax() : $item->getMafInclTax(),
                true
            );
        }

        if ($product->isAccessory()) {
            $result['discount'] = true;
        }

        $result['initial_period'] = ($product->getInitialPeriod()) ? $product->getInitialPeriod() : null;
        $result['minimum_contract_duration'] = ($product->getMinimumContractDuration()) ? $product->getMinimumContractDuration() : null;
        $result['contract_period'] = ($product->getContractPeriod()) ? $product->getContractPeriod() : null;
        $result['is_contract'] = $item->isContract();
        $result['preselect_product'] = $item->getPreselectProduct();
        $result['is_defaulted'] = $item->getIsDefaulted();

        return $result;
    }

    /**
     * @param $packageType
     * @param bool $forConfigurator
     * @param bool $forCart
     * @return array|mixed
     */
    public function getPackageSubtypes($packageType, $forConfigurator = true, $forCart = false)
    {
        // Now called directly from package content so we need to strip the dummy part from packageType
        $packageType = str_replace("template-", "", $packageType);
        $storeCode = Mage::app()->getWebsite()->getCode();
        // get cache key
        $key = sprintf('package_subtypes_%s_%s_%s_%s_%s', $packageType, date('Y-m'), $forConfigurator ? "yes" : "no", $forCart ? "yes" : "no", $storeCode);

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $data = array();
            /** @var Dyna_Package_Model_PackageType $packageModel */
            $packageModel = Mage::getModel("dyna_package/packageType")
                ->loadByCode($packageType);

            // Get package subtypes for current package
            $subtypesCollection = $packageModel
                ->getPackageSubTypes()
                ->addOrder('front_end_position', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC);
            // Check if these packages are needed for configurator
            if ($forConfigurator) {
                $subtypesCollection->addFieldToFilter('package_subtype_visibility',
                    array(
                        array('finset'=> array('configurator')),
                    )
                );
            }
            // Check if these packages are needed for displaying in cart section
            if ($forCart) {
                $subtypesCollection->addFieldToFilter('package_subtype_visibility',
                    array(
                        array('finset'=> array('shopping_cart')),
                    )
                );
            }

            /** @var Dyna_Package_Model_PackageSubtype $subtype */
            // Create an array with all subtypes
            foreach ($subtypesCollection as $subtype) {
                if ($subtype->getPackageSubtypeLifecycleStatus() != $this::PACKAGE_SUBTYPE_LIFECYCLE_STATUS_FUTURE) {
                    $data[] = $subtype->getPackageCode();
                }
            }
            //filter subtypes by store code
            $data = array_diff($data, Mage::getSingleton('dyna_configurator/catalog')->getOrderRestrictions($storeCode));

            // save cache
            $this->getCache()
                ->save(
                    serialize($data),
                    $key,
                    array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                    $this->getCache()->getTtl()
                );

            // return results
            return $data;
        }
    }


    /**
     * @param Vznl_Checkout_Model_Sales_Quote_Item[] $items
     * @return Vznl_Checkout_Model_Sales_Quote_Item|null
     */
    public function extractSubscriptionFromPackageItems($items)
    {
        foreach ($items as $item) {
            if (
            Mage::helper('omnius_catalog')->is(
                array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION),
                $item->getProduct()
            )
            ) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param $product
     * @param $originalItem
     * @param $result
     * @return mixed
     */
    protected function getPromoProductBlock(Omnius_Checkout_Model_Sales_Quote_Item $item, $product, $originalItem, $result)
    {
        // get applied promo amount for later use in displaying the minus discounted promo products
        $appliedPromoAmount = $item->getAppliedPromoAmount();
        $appliedMafPromoAmount = $item->getAppliedMafPromoAmount();

        // possible promo subtypes
        $subtypes = array(
            Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
            Omnius_Catalog_Model_Type::SUBTYPE_ADDON,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_ADDONS
        );

        // promo values
        $priceDiscount = $product->getPriceDiscount();
        $priceDiscountPercent = $product->getPriceDiscountPercent() ?: $product->getData('price_discount_percent ');
        $promoNewPrice = $product->getPromoNewPrice();
        $priceMafNewAmount = $product->getPrijsMafNewAmount();
        $mafDiscount = $product->getMafDiscount();
        // not clear which of these 2 attributes will be used, so will leave both
        $mafDiscountPercent = $product->getMafDiscountPercent() ?: $product->getPrijsMafDiscountPercent();

        $allItems = $item->getQuote()->getAllItems();
        /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
        foreach ($allItems as $item) {
            $catalogModelTypeCond = $item->getProduct()->is($subtypes);

            if ($catalogModelTypeCond && ($originalItem->getTargetId() == $item->getProductId())
                && ($originalItem->getPackageId() == $item->getPackageId())) {
                if ($priceMafNewAmount) { // maf new amount
                    // if new amount is smaller than the original price of the targeted product
                    if($priceMafNewAmount < $item->getMafInclTax()) {
                        // display the maf with tax as this new amount
                        $result['mafTax'] = $priceMafNewAmount;
                        $result['maf'] = $item->getItemFinalMafExclTax();
                        $result['price'] = $item->getItemFinalPriceExclTax();
                        $result['priceTax'] = $item->getItemFinalPriceInclTax();
                    }
                } elseif ($mafDiscount) { // maf value discount
                    // if the maf discount is smaller than the original price of the targeted product
                    if ($mafDiscount < $item->getMafInclTax()) {
                        // display the discounted maf with -
                        $result['maf'] =  $this->getDiscountMafPricesForDisplay($product, $appliedMafPromoAmount);
                        $result['mafTax'] = -1 * abs($appliedMafPromoAmount);
                    } else {
                        // else display the maf discount of the promo product if this
                        // is bigger that the original price of the targeted product
                        $result['maf'] = $this->getDiscountMafPricesForDisplay($product, $product->getMafDiscount());
                        $result['mafTax'] = -1 * abs($product->getMafDiscount());
                    }
                    $result['price'] = $result['priceTax'] = -1 * abs($appliedPromoAmount);
                    $result['itTargetsProduct'] = 1;
                } elseif ($mafDiscountPercent) { // maf percent discount
                    if(($mafDiscountPercent > 0) && ($mafDiscountPercent <= 100)) {
                        $result['price'] = $result['priceTax'] = -1 * abs($appliedPromoAmount);
                        $result['maf'] = $this->getDiscountMafPricesForDisplay($product, $appliedMafPromoAmount);
                        $result['mafTax'] = -1 * abs($appliedMafPromoAmount);
                        $result['itTargetsProduct'] = 1;
                    }
                } elseif ($priceDiscount) {
                    if($priceDiscount < round($item->getPriceInclTax(),2)) {
                        $result['price'] = $this->getDiscountMafPricesForDisplay($product, $appliedPromoAmount);
                        $result['priceTax'] = -1 * abs($appliedPromoAmount);
                    } else {
                        $result['price'] = $this->getDiscountMafPricesForDisplay($product, $product->getPriceDiscount());
                        $result['priceTax'] = -1 * abs($product->getPriceDiscount());
                    }
                    $result['maf'] = $result['mafTax'] = -1 * abs($appliedMafPromoAmount);
                    $result['itTargetsProduct'] = 1;
                 } elseif ($priceDiscountPercent) {
                    if(($priceDiscountPercent > 0) && ($priceDiscountPercent <= 100)) {
                        $result['price'] = $this->getDiscountMafPricesForDisplay($product, $appliedPromoAmount);
                        $result['priceTax'] = -1 * abs($appliedPromoAmount);
                        $result['maf'] = $result['mafTax'] = -1 * abs($appliedMafPromoAmount);
                        $result['itTargetsProduct'] = 1;
                    }
                } elseif ($promoNewPrice < round($item->getPriceInclTax(),2)) {
                    $result['price'] = $promoNewPrice;
                    $result['priceTax'] = $item->getItemFinalPriceInclTax();
                    $result['mafTax'] = $item->getItemFinalMafInclTax();
                    $result['maf'] = $item->getItemFinalMafExclTax();
                }

                break;
            }
        }
        return $result;
    }

    /**
     * @param $product
     * @param $discount
     * @return float
     */
    public function getDiscountMafPricesForDisplay($product, $discount)
    {
        $discountWithoutTax = Mage::helper('tax')->getPrice($product, abs($discount), false);
        return -1 * $discountWithoutTax;
    }

    /**
     * @return bool
     */
    public function hasKIPWhileNotAllowed() {
        return false;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item[] $packageItems
     * @param bool|true $withTax
     * @param bool|false $skipDiscounts
     * @return array
     */
    public function calculatePackageSectionTotals($packageItems, $withTax = true, $skipDiscounts = false)
    {
        $totalPrice = $totalMaf = 0;
        $totalMafSection = [];
        $totalPriceSection = [];

        foreach ($packageItems as $item) {
            if ($item->getAlternateQuoteId()) {
                continue;
            }
            if ($withTax) {
                $mafUnit = $skipDiscounts ? $item->getMafInclTax() : $item->getItemFinalMafInclTax();
                $priceUnit = $item->getItemFinalPriceInclTax();
            } else {
                $mafUnit = $skipDiscounts ? $item->getMaf() : $item->getItemFinalMafExclTax();
                $priceUnit = $item->getItemFinalPriceExclTax();
            }

            $weeeTax = 0;
            if ($withTax) {
                if ($item->getWeeeTaxAppliedRowAmount()) {
                    foreach ($item->getWeeeTaxAppliedUnserialized() as $attribute) {
                        $weeeTax += $attribute['row_amount_incl_tax'];
                    }
                }
            } else {
                if ($item->getWeeeTaxAppliedRowAmount()) {
                    $weeeTax = $item->getWeeeTaxAppliedRowAmount();
                }
            }

            $priceUnit = round($priceUnit + $weeeTax, 2);

            $index = strtolower(current($item->getProduct()->getType()));

            if (!isset($totalMafSection[$index])) {
                $totalMafSection[$index] = 0;
            }
            if (!isset($totalPriceSection[$index])) {
                $totalPriceSection[$index] = 0;
            }

            $mafUnitValue   = ($item->getQty() ? $item->getQty() : $item->getQtyOrdered()) * $mafUnit;
            $priceUnitValue = ($item->getQty() ? $item->getQty() : $item->getQtyOrdered()) * $priceUnit;

            if ($item->getProduct()->isServiceItem()) {
                $totalMafSection[strtolower(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)] += $mafUnitValue;
                $totalPriceSection[strtolower(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)] += $priceUnitValue;
            }

            if ($index == strtolower(Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY) && true == $item->getProduct()->isServiceItem()) {
                $mafUnitValue   = 0;
                $priceUnitValue = 0;
            }

            $totalMafSection[$index] += $mafUnitValue;
            $totalPriceSection[$index] += $priceUnitValue;
        }

        return array('sectionmaf' => $totalMafSection, 'sectionprice' => $totalPriceSection);
    }

    /**
     * Get customer response from session adapted to current stack
     */
    public function getCustomersForCurrentStack()
    {
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $serviceCustomers = Mage::getSingleton('dyna_customer/session')->getServiceCustomers();
        $activePackage = $quote->getActivePackage();

        $accountIdentifierLinkids = array();
        // map customers from session by stack of current active package
        if ($activePackage && strtolower($activePackage->getType()) == strtolower(Vznl_Catalog_Model_Type::TYPE_MOBILE)) {
            if (empty($this->customerResponseByStack[strtolower(Vznl_Catalog_Model_Type::TYPE_MOBILE)])) {
                $this->checkCustomersByStack($serviceCustomers, Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL, strtolower(Vznl_Catalog_Model_Type::TYPE_MOBILE), $accountIdentifierLinkids);
            }
            // also check if any customer is linked
            if (!empty($accountIdentifierLinkids)) {
                $this->checkAccountIdentifierLinkid($accountIdentifierLinkids, Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL, $serviceCustomers, strtolower(Vznl_Catalog_Model_Type::TYPE_MOBILE));
            }

            return $this->customerResponseByStack[strtolower(Vznl_Catalog_Model_Type::TYPE_MOBILE)];
        } elseif ($activePackage && strtolower($activePackage->getType()) == strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED)) {
            if (empty($this->customerResponseByStack[strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED)])) {
                $this->checkCustomersByStack($serviceCustomers, Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL, strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED), $accountIdentifierLinkids);
            }

            if (!empty($accountIdentifierLinkids)) {
                $this->checkAccountIdentifierLinkid($accountIdentifierLinkids, Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL, $serviceCustomers, strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED));
            }

            return $this->customerResponseByStack[strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED)];
        }

        return [];
    }

    /**
     * Check customers from session
     * @param $serviceCustomers
     * @param $type
     * @param $stackToSave
     */
    protected function checkCustomersByStack($serviceCustomers, $type, $stackToSave, &$accountIdentifierLinkids)
    {
        foreach ($serviceCustomers as $responseType => $customers) {
            if ($responseType == $type) {
                foreach ($customers as $customerBan => $customer) {
                    $customerModel = Mage::getModel('customer/customer')->getCollection()
                        ->addFieldToFilter('ban', $customer['customer_number'])
                        ->addFieldToFilter('store_id', Mage::app()->getStore()->getWebsiteId())
                        ->setPageSize(1, 1)
                        ->getLastItem();

                    if ($customerModel->getAccountIdentifierLinkid()) {
                        array_push($accountIdentifierLinkids, $customerModel->getAccountIdentifierLinkid());
                    }

                    $this->customerResponseByStack[$stackToSave][$customerBan] = $customer;
                }
            }
        }
    }

    /**
     * Check if customers from session are linked or not
     * @param $accountIdentifierLinkids
     * @param $type
     * @param $serviceCustomers
     * @param $stack
     */
    protected function checkAccountIdentifierLinkid($accountIdentifierLinkids, $type, $serviceCustomers, $stack)
    {
        foreach ($serviceCustomers as $responseType => $customers) {
            if ($responseType == $type) {
                foreach ($customers as $customerBan => $customer) {
                    if (in_array($customer['customer_number'], $accountIdentifierLinkids)) {
                        $this->customerResponseByStack[$stack][$customerBan] = $customer;
                    }
                }
            }
        }
    }

    /**
     * Returns the open orders collection from the database.
     *
     * @param $clusterType
     * @param $resultsPerPage
     * @param $currentPage
     * @return object
     */
    public function getOpenOrders($clusterType, $resultsPerPage, $currentPage)
    {
        $superOrders = Mage::getModel('superorder/superorder')->getCollection();

        $superOrdersList = Mage::getModel('superorder/superorder')->getCollection();

        $superOrders->getSelect()
            ->joinLeft(
                array('orders' => 'sales_flat_order'),
                'main_table.entity_id = orders.superorder_id',
                [
                    'orders.customer_firstname AS customer_firstname',
                    'orders.customer_lastname AS customer_lastname',
                    'orders.company_name AS company_name'
                ]
            )
            ->joinRight(
                array('packages' => 'catalog_package'),
                'main_table.entity_id = packages.order_id',
                [
                    'packages.status AS status',
                    'COUNT(distinct(packages.entity_id)) AS total_packages',
                    'MAX(packages.tel_number) AS phone_number',
                ]
            );

        $filterCompletedOrders = "main_table.order_status = '" . Dyna_Superorder_Model_Superorder::SO_COMPLETED_SUCCESS
            . "' or main_table.order_status = '" . Dyna_Superorder_Model_Superorder::SO_COMPLETED_FAILED
            . "'";

        $filterErrorOrders = "main_table.order_status = '" . Dyna_Superorder_Model_Superorder::SO_IN_PROGRESS_WITH_FAILURE . "'";

        /** @var Dyna_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');
        $agent = $session->getSuperAgent() ?: $session->getAgent(true);

        if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_ALL)) {
            // For all permissions granted, retrieve all orders.
        } else if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_TELESALES)) {
            $superOrdersList->getSelect()->where('main_table.created_website_id = ?', Mage::app()->getWebsite(Dyna_AgentDE_Model_Website::WEBSITE_TELESALES_CODE)->getId());
        } else if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_DEALER_GROUP)) {
            $dealerGroupId = $agent->getDealer()->getDealerGroups()->getAllIds()[0];

            /** @var Dyna_AgentDE_Model_Dealer $dealerModel */
            $dealerModel = Mage::getModel('agent/dealer');
            $dealers = $dealerModel->getDealersInGroup($dealerGroupId);

            if (sizeof($dealers) > 0) {
                $superOrdersList->getSelect()->where('main_table.created_dealer_id IN (' . implode(",", $dealers) . ') OR main_table.created_dealer_id = ?', $agent->getDealer()->getDealerId());
            } else {
                // If the dealer is not part of a group, fetch only its own orders
                $superOrdersList->getSelect()->where('main_table.created_dealer_id = ?', $agent->getDealer()->getDealerId());
            }
        } else if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_DEALER)) {
            $superOrdersList->getSelect()->where('main_table.created_dealer_id = ?', $agent->getDealer()->getDealerId());
        } else {
            // If no permissions are granted return an empty collection
            $superOrdersList->getSelect()->where('1=0');
        }

        $superOrdersList->getSelect()->order('main_table.created_at DESC');

        $superOrdersList->getSelect()->limit($resultsPerPage + 1, ($currentPage - 1) * $resultsPerPage);

        $superordersList = [];
        foreach($superOrdersList as $superOrder) {
            $superordersList[] = $superOrder->getEntityId();
        }

        $superOrders->getSelect()->where('main_table.entity_id IN (' . implode(',', $superordersList) . ')');

        switch ($clusterType) {
            case 'error':
                $superOrders->getSelect()->where($filterErrorOrders);
                break;
            case 'no-error':
                $superOrders->getSelect()->where("NOT (" . $filterErrorOrders . ")");
                break;
        }

        $superOrders->getSelect()->where("NOT (" . $filterCompletedOrders . ")");

        $superOrders->getSelect()->group('main_table.entity_id');

        $superOrders->getSelect()->order('main_table.created_at DESC');

        return $superOrders;
    }


}
