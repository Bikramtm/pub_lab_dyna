<?php


class Vznl_Configurator_Helper_Attribute extends Dyna_Configurator_Helper_Attribute
{
    /**
     * @param null $consumerType
     * @return string
     */
    public function getSubscriptionIdentifier($consumerType = null)
    {
        if (!$consumerType) {
            $consumerType = Vznl_Catalog_Model_Product::IDENTIFIER_CONSUMER;
        }

        $key = sprintf('identifier_consumer_%s', $consumerType);

        if ($result = $this->getCache()->load($key)) {
            return $result;
        } else {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Vznl_Catalog_Model_Product::CATALOG_CONSUMER_TYPE);
            $consValue = (string) Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, $consumerType);
            $this->getCache()->save($consValue, $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $consValue;
        }
    }


    /**
     * Retrieve all available sim card types defined in admin
     *
     * @return array
     */
    public function getSimCardTypes($productId)
    {
        $website_id = Mage::app()->getWebsite()->getId();
        $key = __METHOD__ . '_' . $website_id . '_' . $productId[0];
        
        if ($collection = unserialize($this->getCache()->load($key))) {
            return $collection;
        } else {
            $productData = Mage::getModel('catalog/product')->load($productId[0]);
            $options = $productData->getAttributeText('identifier_simcard_allowed');

            $items = array();
            foreach ($options as $option) {
                $simName = trim($option);
                if (strlen($simName)) {
                    // RFC-151057: Check if SIM product is available in website
                    $simProd = Mage::getModel('catalog/product')->loadByAttribute('name', $simName);

                    if ($simProd && $simProd->getId() && ($simProd->getStatus() == 1) &&  in_array($website_id, $simProd->getWebsiteIds())) {
                        $items[$simProd->getId()] = $option;
                    }
                }
            }

            $result =  !empty($items) ? $items : null;
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * Renders the available filters
     * @param Varien_Data_Collection $collection
     * @return array|mixed
     */
    public function getAvailableFilters(Varien_Data_Collection $collection)
    {
        $key = serialize($collection->getAllIds());
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $filters = array();
            /** @var Omnius_Catalog_Model_Product $item */
            foreach ($collection->getItems() as $item) {
                $filters = $this->array_merge_recursive($filters, $item->getAvailableFilters());
            }
            uasort(
                $filters,
                function ($a, $b) {
                    return $a['position'] > $b['position'] ? -1 : 1;
                }
            );

            foreach ($filters as $key => $props) {
                if (!$this->isAssoc($props['options'])) {
                    $filters[$key]['options'][""] = '';
                }
                if (count($props['options']) < 2) {
                    unset($filters[$key]);
                }
            }

            $this->getCache()->save(serialize($filters), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $filters;
        }
    }

    /**
     * Check if a certain simcard type is available for a product
     *
     * @param $product
     * @param $sim
     * @return bool
     */
    public function checkSimTypeAvailable($product, $sim)
    {
        $key = serialize(array(__METHOD__ , $product->getId() , $sim));
        if ($collection = unserialize($this->getCache()->load($key))) {
            return $collection;
        } else {
            $productModel = Mage::getModel('catalog/product');
            $attr = $productModel->getResource()->getAttribute(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE);
            if ($attr->usesSource()) {
                $simId = $attr->getSource()->getOptionId($sim);
            }

            $result = false;
            $productCollection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect(array('entity_id', 'name'))->addAttributeToFilter(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE, $simId)->load();
            $allowedSims = $product->load($product->getId())->getAttributeText(Omnius_Catalog_Model_Product::AVAILABLE_SIM_TYPES_ATTRIBUTE);
            $allowedSims = is_array($allowedSims) ? $allowedSims : array($allowedSims);

            $flippedAllowedSims = array_flip($allowedSims);
            foreach ($productCollection as $prod) {
                $productName = $prod->getName();
                if (isset($flippedAllowedSims[$productName])) {
                    $result = true;
                    break;
                }
            }

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $result;
        }
    }
}
