<?php

/**
 * Class Vznl_AdyenPayment_Model_Cronjob
 */
class Vznl_AdyenPayment_Model_Cronjob extends Adyen_Payment_Model_Cronjob
{
    /**
     * {@inheritDoc}
     */
    public function updateNotificationQueue()
    {
        Mage::app()->getTranslator()->init(Mage_Core_Model_App_Area::AREA_FRONTEND);
        parent::updateNotificationQueue();
    }
}