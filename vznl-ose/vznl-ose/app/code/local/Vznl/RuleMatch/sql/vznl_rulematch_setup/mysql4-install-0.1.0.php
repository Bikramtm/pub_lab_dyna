<?php
/**
 * Installer that adds package_type column to sales_rule_product_match
 */

/** @var Mage_Sales_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

$table = $installer->getTable('sales_rule_product_match');
$columnName = 'package_type';

// add package_type to sales_rule_product_match table
if (!$installer->getConnection()->tableColumnExists($table, $columnName)) {
    $installer->getConnection()->addColumn($table, $columnName, [
        "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
        "length" => 10,
        "comment" => 'package_type fk'
    ]);
}

$installer->endSetup();
