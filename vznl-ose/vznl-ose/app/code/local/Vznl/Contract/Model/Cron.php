<?php

/**
 * Copyright (c) 2019. Dynacommerce B.V.
 * Class Vznl_Contract_Model_Cron
 */
class Vznl_Contract_Model_Cron
{
    const BATCH_SIZE      = 50;
    const RETRY_LIMIT     = 3;
    const FTP_LOG_NAME    = 'document_ftp.log';
    const TIMEOUT         = 15;

    /**
     * Handles sending PDF files over SFTP
     */
    public function processSftpJobs():void
    {
        try {
            $sftp = new Varien_Io_Sftp();
            $sftp->open(array(
                'host' => Mage::getStoreConfig('vodafone_service/contract_sftp/hostname'),
                'username' => Mage::getStoreConfig('vodafone_service/contract_sftp/username'),
                'password' => Mage::getStoreConfig('vodafone_service/contract_sftp/password'),
                'timeout' => self::TIMEOUT,
            ));

            $remotePath = Mage::getStoreConfig('vodafone_service/contract_sftp/path');
            $batchSize = $this->_getBatchSize();

            $this->_logMessage(sprintf('Starting SFTP job with bach size of: %d, remote path is: %s', $batchSize, $remotePath));

            if (!$sftp->cd($remotePath)) {
                $this->_logMessage(sprintf('Cannot access remote path: %s', $remotePath));
                $sftp->mkdir($remotePath);
            }

            foreach ($this->_getJobs() as $job) {
                // Store on remote SFTP
                $jobFileArray = explode(';', $job->getFileName());
                $identifier = $jobFileArray[1];
                $localFilePath = $jobFileArray[0];
                if (is_string($identifier)) {
                    $this->_processJob($sftp, $identifier, $localFilePath, $job);
                }
                $batchSize--;
                if ($batchSize == 0) {
                    break;
                }
            }
        } catch(Exception $e) {
            $this->_logMessage(sprintf('There was a fatal error handling SFTP job: %s', $e->getMessage()));
            Mage::logException($e);
        }
    }

    /**
     * Retrieves list of available jobs to process, with configurable limit
     * @return Vznl_Contract_Model_Resource_Contractjob_Collection
     */
    private function _getJobs():Vznl_Contract_Model_Resource_Contractjob_Collection
    {
        $jobs = Mage::getModel('vznl_contract/contractjob')
            ->getCollection()
            ->addFieldToFilter('tries', array('lt' => $this->_getRetryLimit()))
            ->addFieldToFilter('locked', array('eq' => 0))
            ->setPageSize($this->_getBatchSize())
            ->setCurPage(1);
        return $jobs;
    }

    /**
     * Retrieve the limit for jobs to be processed per one cron run
     * @return int
     */
    private function _getBatchSize():int
    {
        $batchSize = Mage::getStoreConfig('vodafone_service/contract_sftp/batch_size');
        $batchSize = $batchSize ?? self::BATCH_SIZE;
        return (int) $batchSize;
    }

    /**
     * Retrieve the retry limit for jobs to be processed per one cron run
     * @return int
     */
    private function _getRetryLimit():int
    {
        $limit = Mage::getStoreConfig('vodafone_service/contract_sftp/retry_limit');
        $limit = $limit ? $limit : self::RETRY_LIMIT;
        return (int) $limit;
    }

    /**
     * Removes file locally and from the queue (DB)
     * @param string $identifier
     * @param string $localFilePath
     * @param Vznl_Contract_Model_Contractjob $job
     */
    private function _removeLocalFile(
        string $identifier,
        string $localFilePath,
        Vznl_Contract_Model_Contractjob $job
    ):void
    {
        try {
            Mage::getModel('vznl_contract/contractjob')->deleteByJobId($job->getJobId());
        } catch (Exception $e) {
            $this->_logMessage(sprintf('There was an error with job deletion: %s', $e->getMessage()));
        }

        if (is_file($localFilePath)) {
            $isFileDeleted = unlink($localFilePath);
            if (!$isFileDeleted) {
                $this->_logMessage(sprintf('Could not delete file \'%s\'', $identifier));
            }
        }
    }

    /**
     * Removes file locally and from the queue (DB)
     * @param Varien_Io_Sftp $sftp
     * @param string $identifier
     * @param string $localFilePath
     * @param Vznl_Contract_Model_Contractjob $job
     */
    private function _processJob(
        Varien_Io_Sftp $sftp,
        string $identifier,
        string $localFilePath,
        Vznl_Contract_Model_Contractjob $job
    ):void
    {
        $startExecution = microtime(true);
        $job->lock();
        $errorMessage = null;

        $written = $sftp->write($identifier, $job->getContractData());
        if ($written) {
            $executionTime = microtime(true) - $startExecution;
            $this->_logMessage(sprintf('File \'%s\' successfully put in FTP server in %s', $identifier, round($executionTime,3)));
            $this->_removeLocalFile($identifier, $localFilePath, $job);
        } else {
            $errorMessage = sprintf('There was an error uploading file: %s', $identifier);
        }

        if (!is_null($errorMessage)) {
            $this->_logMessage($errorMessage);
            $job->increaseTries();
            $job->unlock();
            $job->setErrorMessage($errorMessage);
        }
    }

    /**
     * Logs status message
     * @param string $msg
     */
    private function _logMessage(
        string $msg
    ):void
    {
        Mage::log(
            $msg,
            null,
            self::FTP_LOG_NAME
        );
    }

}
