<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();

$tableName = $this->getTable('multimapper/mapper');

if($connection->tableColumnExists($tableName, 'bom_id')){
    $connection->changeColumn($tableName, 'bom_id', 'bom_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'nullable'  => true,
        'default'   => null
    ));
}

if($connection->tableColumnExists($tableName, 'service_expression')){
    $connection->changeColumn($tableName, 'service_expression', 'service_expression', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'nullable'  => true,
        'default'   => null
    ));
}


$connection->dropIndex($tableName, "IDX_DYNA_MULTIMAPPER_SKU_DIRECTION");

$connection->addIndex($tableName, "IDX_DYNA_MULTIMAPPER_FIXED", ['sku', 'direction', 'bom_id', 'service_expression'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);

$this->endSetup();