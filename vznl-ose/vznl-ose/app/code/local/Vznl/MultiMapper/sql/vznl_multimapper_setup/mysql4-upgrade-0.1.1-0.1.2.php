<?php
$installer = $this;
$installer->startSetup();
$installer->run("DROP TABLE IF EXISTS `dyna_reasoncodes`;");
// TODO write proper setup code
$sql=<<<SQLTEXT
CREATE TABLE `dyna_reasoncodes` (
  `entity_id` INT AUTO_INCREMENT,
  `customer_value_option_id` INT NULL,
  `catalog_product_reasoncode_option_id` INT NULL,
  `reason_code` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`entity_id`));

SQLTEXT;

$installer->run($sql);

$sql=<<<SQLTEXT
ALTER TABLE `dyna_reasoncodes` 
ADD UNIQUE INDEX `IDX_DYNA_REASONCODES_CUSTOMER_VALUE_REASONCODE_TYPE` (`customer_value_option_id` ASC, `catalog_product_reasoncode_option_id` ASC);

SQLTEXT;

$installer->run($sql);

$sql=<<<SQLTEXT
ALTER TABLE `dyna_multi_mapper` 
ADD UNIQUE INDEX `IDX_DYNA_MULTIMAPPER_PRICEPLAN_ID_PROMO_ID` (`priceplan_id` ASC, `promo_id` ASC);

SQLTEXT;

$installer->run($sql);

$sql=<<<SQLTEXT
ALTER TABLE `dyna_multi_mapper` 
ADD INDEX `IDX_DYNA_MULTIMAPPER_PRICEPLAN_ID` (`priceplan_id` ASC);

SQLTEXT;

$installer->run($sql);

$installer->endSetup();
