<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('multimapper/mapper'), 'oms_bo_code',
[
'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
'comment'   => 'Oms bo code',
'length' => 80,
'required'  => false,
'after'     => 'promo_sku_description',
'nullable'  => true
]
);

if($connection->tableColumnExists($installer->getTable('multimapper/mapper'), 'oms_product_cod')){
    $connection->changeColumn($installer->getTable('multimapper/mapper'), 'oms_product_cod', 'oms_product_code', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'nullable'  => true,
        'default'   => null
    ));
}


$installer->endSetup();