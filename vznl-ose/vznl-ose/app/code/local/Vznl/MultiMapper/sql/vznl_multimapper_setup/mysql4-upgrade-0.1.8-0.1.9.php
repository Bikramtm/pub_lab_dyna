<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();

$tableName = $this->getTable('multimapper/mapper');
$indexName = "IDX_DYNA_MULTIMAPPER_PRICEPLAN_SKU_PROMO_SKU";

$connection->dropIndex($tableName, $indexName);
$connection->addIndex($tableName, $indexName, ['promo_sku', 'pp_sku', 'oms_attr_value_1'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);

$this->endSetup();