<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();

$connection->addColumn($this->getTable('multimapper/mapper'),
    'bom_id',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'BOM ID',
        'required' => false,
        'nullable' => true
    )
);

$installer->endSetup();
