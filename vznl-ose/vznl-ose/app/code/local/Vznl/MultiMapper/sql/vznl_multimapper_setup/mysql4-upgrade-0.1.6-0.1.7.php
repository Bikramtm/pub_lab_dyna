<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$connection = $installer->getConnection();

$addonsTable = 'vznl_multimapper/addon';

$connection->addColumn($this->getTable($addonsTable),
    'technical_id_2',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Technical ID 2',
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable($addonsTable),
    'nature_code_2',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Nature Code 2',
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable($addonsTable),
    'technical_id_3',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Technical ID 3',
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable($addonsTable),
    'nature_code_3',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Nature Code 3',
        'required' => false,
        'nullable' => true
    )
);

$installer->endSetup();
