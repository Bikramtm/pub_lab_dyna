<?php


class Vznl_MultiMapper_Block_Adminhtml_Mapper extends Dyna_MultiMapper_Block_Adminhtml_Mapper
{

    public function __construct()
    {

        $this->_controller = "adminhtml_mapper";
        $this->_blockGroup = "multimapper";
        $this->_headerText = Mage::helper("multimapper")->__("Mapper Manager");
        $this->_addButtonLabel = Mage::helper("multimapper")->__("Add New Item");
        parent::__construct();

        $this->_addButton('import', [
            'label' => Mage::helper('multimapper')->__('Upload MultiMapper'),
            'onclick' => 'javascript:new Popup(\'' . $this->getImportUrl() . '\', {title:\'' . Mage::helper('multimapper')->__('Upload new MultiMapper file') . '\', width: 600, height:220})',
            'class' => 'go',
        ]);
    }

    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }

}
