<?php

/**
 * Class Vznl_MultiMapper_Block_Adminhtml_Mapper_Grid
 */
class Vznl_MultiMapper_Block_Adminhtml_Mapper_Grid extends Dyna_MultiMapper_Block_Adminhtml_Mapper_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("mapperGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    /**
     * @param $collection
     * @param $column
     * @return $this
     */
    public function mapperGridCustomEntityFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        if (!empty($value['from'])) {
            $collection->getSelect()->where("main_table.entity_id >= ?", $value['from']);
        }
        if (!empty($value['to'])) {
            $collection->getSelect()->where("main_table.entity_id <= ?", $value['to']);
        }
    }

    /**
     * @param $collection
     * @param $column
     * @return $this
     */
    public function mapperGridCustomServiceExpressionFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $collection->getSelect()->where("main_table.service_expression like ?", "%" . $value . "%");
    }

    /**
     * @param $collection
     * @param $column
     * @return $this
     */
    public function mapperGridCustomDirectionFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $collection->getSelect()->where("main_table.direction like ?", "%" . $value . "%");
    }

    /**
     * @param $collection
     * @param $column
     * @return $this
     */
    public function mapperGridCustomOmdObCodeFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $collection->getSelect()->where("main_table.oms_bo_code like ?", "%" . $value . "%");
    }

    /**
     * @param $collection
     * @param $column
     * @return $this
     */
    public function mapperGridCustomStackFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $collection->getSelect()->where("main_table.stack like ?", "%" . $value . "%");
    }

    /**
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        if (!$this->_isExport) {
            $this->addColumn("entity_id", array(
                "header" => Mage::helper("multimapper")->__("ID"),
                "align" => "right",
                "width" => "50px",
                "type" => "number",
                "index" => "entity_id",
                'filter_condition_callback' => array($this, 'mapperGridCustomEntityFilter')
            ));
        }
        $this->addColumn("sku", array(
            "header" => Mage::helper("multimapper")->__("SKU"),
            "index" => "sku",
        ));
        $this->addColumn("service_expression", array(
            "header" => Mage::helper("multimapper")->__("Service expression"),
            "index" => "service_expression",
            'filter_condition_callback' => array($this, 'mapperGridCustomServiceExpressionFilter')
        ));
        $this->addColumn("direction", array(
            "header" => Mage::helper("multimapper")->__("Direction"),
            "index" => "direction",
            'filter_condition_callback' => array($this, 'mapperGridCustomDirectionFilter')
        ));
        $this->addColumn("oms_bo_code", array(
            "header" => Mage::helper("multimapper")->__("Oms Bo Code"),
            "index" => "oms_bo_code",
            'filter_condition_callback' => array($this, 'mapperGridCustomOmdObCodeFilter')
        ));
        $this->addColumn("stack", array(
            "header" => Mage::helper("multimapper")->__("Stack"),
            "index" => "stack",
            'filter_condition_callback' => array($this, 'mapperGridCustomStackFilter')
        ));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }

    protected function _prepareCollection()
    {
        /** @var Omnius_MultiMapper_Model_Mysql4_Mapper_Collection $collection */
        $collection = Mage::getModel("multimapper/mapper")->getCollection();

        $collection->getSelect()
            ->joinLeft(
                array('dyna_multi_mapper_addons'=>'dyna_multi_mapper_addons'),
                'dyna_multi_mapper_addons.mapper_id = main_table.entity_id',
                array('addonID' => 'dyna_multi_mapper_addons.entity_id')
            )
            ->joinLeft(
                array('dyna_multi_mapper_index_process_context'=>'dyna_multi_mapper_index_process_context'),
                'dyna_multi_mapper_index_process_context.addon_id = dyna_multi_mapper_addons.entity_id',
                array('processContextAddonID' => 'dyna_multi_mapper_index_process_context.addon_id','processContextID' => 'dyna_multi_mapper_index_process_context.process_context_id')
            )
            ->joinLeft(
                array('process_context'=>'process_context'),
                'process_context.entity_id = dyna_multi_mapper_index_process_context.process_context_id',
                array('name' => 'process_context.name')
            )
            ->group('main_table.entity_id');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_mapper', array(
            'label' => Mage::helper('multimapper')->__('Remove Mapper'),
            'url' => $this->getUrl('*/adminhtml_mapper/massRemove'),
            'confirm' => Mage::helper('multimapper')->__('Are you sure?')
        ));
        return $this;
    }

    /**
     * Generate the excel file using the PHPExcel library
     * @param $sheetName
     * @param $websiteId
     * @return array
     */
    public function getExcelFile($sheetName = '', $websiteId = null)
    {
        $objPHPExcel = new PHPExcel();

        $this->_buildWorksheet($objPHPExcel, 'multimapper');

        $path = Mage::getBaseDir('var') . DS . 'export' . DS;
        $name = md5(microtime());
        $file = $path . $name . '.xlsx';

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($file);

        return [
            'type' => 'filename',
            'value' => $file,
            'rm' => true // can delete file after use
        ];
    }

    /**
     * Parse db data and build the Excel Worksheet
     *
     * @param PHPExcel $objPHPExcel
     * @param $title
     * @param int $index
     * @return mixed
     */
    protected function _buildWorksheet($objPHPExcel, $title, $index = 0)
    {
        $columns = Mage::getModel('multimapper/mapper')->getColumns();
        $columnsR = Mage::getModel('multimapper/mapper')->getRecursiveColumns();

        if ($index == 0) {
            $ws = $objPHPExcel->getActiveSheet();
        } else {
            $ws = $objPHPExcel->createSheet();
        }

        $ws->setTitle($title);

        $data = Mage::getResourceModel('multimapper/mapper_collection')->load();

        // Always add the header in the excel file
        $columnIndex = 0;
        foreach ($columns as $column) {
            $ws->setCellValueByColumnAndRow($columnIndex, 1, $column);
            ++$columnIndex;
        }
        for ($i = 1; $i <= 10; $i++) {
            foreach ($columnsR as $column) {
                $ws->setCellValueByColumnAndRow($columnIndex, 1, $column . $i);
                ++$columnIndex;
            }
        }

        // Add database data to the stylesheet
        $rowIndex = 2;
        foreach ($data as $item) {
            $columnIndex = 0;
            foreach ($columns as $column) {
                $ws->setCellValueByColumnAndRow($columnIndex, $rowIndex, $item->getData($column));
                ++$columnIndex;
            }
            for ($i = 1; $i <= 10; $i++) {
                foreach ($columnsR as $column) {
                    $ws->setCellValueByColumnAndRow($columnIndex, $rowIndex, $item->getData($column . $i));
                    ++$columnIndex;
                }
            }
            ++$rowIndex;
        }

        return $ws;
    }
}
