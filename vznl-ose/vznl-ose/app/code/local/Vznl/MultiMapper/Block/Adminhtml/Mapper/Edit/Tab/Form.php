<?php

/**
 * Class Vznl_MultiMapper_Block_Adminhtml_Mapper_Edit_Tab_Form
 */
class Vznl_MultiMapper_Block_Adminhtml_Mapper_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $id = $this->getRequest()->get('id');
        $stack = "";
        if (isset($id)) {
            $mapper = Mage::getSingleton('dyna_multimapper/mapper')->load($id);
            $stack = strtolower(trim($mapper->getData('stack')));
        }
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $createStack = $this->getRequest()->get('createStack');
        if (!isset($id) && !isset($createStack)) {
            $fieldset = $form->addFieldset("multimapper_form", ["legend" => Mage::helper("multimapper")->__("New Multimapper")]);
            $fieldset->addField('createStack', 'select', array(
                'label' => Mage::helper('vznl_checkout')->__('Multimapper Type'),
                'name' => 'type',
                "class" => "required-entry",
                "required" => true,
                'values' => [
                    'fixed' => Mage::helper('vznl_checkout')->__('Fixed'),
                    'mobile' => Mage::helper('vznl_checkout')->__('Mobile'),
                ]
            ));
            $button = $fieldset->addField('continue', 'button', array(
                'value' => Mage::helper('core')->__('Continue'),
                'name' => 'continue',
                'class' => 'form-button',
                'onclick' => "setSettings('" . $this->getContinueUrl() . "')",
            ));
            $button->setAfterElementHtml('<script>
                //< ![CDATA
                function setSettings(urlTemplate)
                  {
                    var stack = document.getElementById(\'createStack\').value;
                    urlTemplate =urlTemplate.replace(\'{{createStack}}\', stack);
                    location.href = urlTemplate;
                  }
                //]]>
                </script>');

            return parent::_prepareForm();
        } else {
            $data = Mage::registry("mapper_data")->getData();
            $fieldset = $form->addFieldset("multimapper_form", ["legend" => Mage::helper("multimapper")->__("Item information")]);
            if (isset($createStack)) {
                $stack = $createStack;
            }
            if ($stack == "fixed") {
                //fields added for the Fixed import
                $fields = [
                    "service_expression" => "Service Expression",
                    "priority" => "Priority",
                    "stop_execution" => "Stop Execution",
                    "comment" => "Comment",
                    "sku" => "SKU",
                    "xpath_outgoing" => "Outgoing path",
                    "xpath_incoming" => "Incoming path",
                    "component_type" => "Component Type",
                    "direction" => "Direction",
                    "defaulted" => "Defaulted",
                    "ogw_component_name" => "OGW Component Name",
                    "source_component_id" => "Source Component ID",
                    "system_name" => "System Name",
                    "element_id" => "Element ID",
                    "technical_id" => "Technical ID",
                    "nature_code" => "Nature Code",
                    "backend" => "Backend",
                    "name" => "Process Context",
                    "technical_id_2" => "Technical ID 2",
                    "nature_code_2" => "Nature Code 2",
                    "technical_id_3" => "Technical ID 3",
                    "nature_code_3" => "Nature Code 3",
                ];
            } else {
                $fieldset->addField("pp_sku", "text", [
                    "label" => Mage::helper("multimapper")->__("U-Buy price plan SKU"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "pp_sku",
                    "value" => isset($data["pp_sku"]) ? $data["pp_sku"] : "",
                ]);
                $fields = [
                    "pp_sku_description" => "U-Buy price plan SKU description",
                    "promo_sku" => "U-Buy promotion SKU",
                    "promo_sku_description" => "U-Buy promotion SKU description",
                    "oms_bo_type" => "Base or Addon",
                    "oms_bo_code" => "OMS billing offer code",
                    "oms_pbo_type" => "Base promo or addon promo",
                    "oms_pbo_code" => "OMS promotional billing offer code",
                    "oms_product_offering_code" => "EPC product offering (A Product Offering is a complete sellable product/entity offered to the customer)",
                    "oms_product_offering_id" => "Offering id",
                    "oms_product_code" => "EPC product specification (A product specification is any service that a service provider provides to its customers. i.e Mobile ).",
                    "oms_mc_code" => "EPC Main Component (The Main Component is the top-level component representing the product specification. The relation between the product specification and the main component is a one-to-one relation. i.e. Mobile_Main)",
                    "oms_comp_code_path" => "EPC components path",
                    "oms_comp_id" => "Component id",
                ];
            }
            foreach ($fields as $key => $desc) {
                if ($key == "sku") {
                    $fieldset->addField($key, "text", [
                        "label" => Mage::helper("multimapper")->__($desc),
                        "name" => $key,
                        "class" => "required-entry",
                        "required" => true,
                        "value" => isset($data[$key]) ? $data[$key] : "",
                    ]);
                    continue;
                }
                if ($key == "name") {
                    $fieldset->addField($key, "multiselect", [
                        "label" => Mage::helper("multimapper")->__($desc),
                        "name" => $key,
                        "class" => "required-entry",
                        "required" => true,
                        "values" => $this->getProcessContextOption(),
                        "value" => isset($data['entity_id']) ? $this->getProcessContext($data['entity_id']) : "",
                    ]);
                    continue;
                }
                $fieldset->addField($key, "text", [
                    "label" => Mage::helper("multimapper")->__($desc),
                    "name" => $key,
                    "value" => isset($data[$key]) ? $data[$key] : "",
                ]);
            }
            if ($stack != "fixed") {
                for ($i = 1; $i <= 10; $i++) {
                    $fieldset->addField("oms_component_attr_" . $i, "text", [
                        "label" => sprintf("%s %d", Mage::helper("multimapper")->__("EPC components"), $i),
                        "name" => "oms_component_attr_" . $i,
                        "value" => isset($data["oms_component_attr_" . $i]) ? $data["oms_component_attr_" . $i] : "",
                    ]);
                    $fieldset->addField("oms_attr_code_" . $i, "text", [
                        "label" => sprintf("%s %d", Mage::helper("multimapper")->__("The attributes that belong to a certain component"), $i),
                        "name" => "oms_attr_code_" . $i,
                        "value" => isset($data["oms_component_attr_" . $i]) ? $data["oms_component_attr_" . $i] : "",
                    ]);
                    $fieldset->addField("oms_attr_value_" . $i, "text", [
                        "label" => sprintf("%s %d", Mage::helper("multimapper")->__("Attribute value"), $i),
                        "name" => "oms_attr_value_" . $i,
                        "value" => isset($data["oms_attr_value_" . $i]) ? $data["oms_attr_value_" . $i] : "",
                    ]);
                }
            }
            if ($stack == "fixed") {
                $fieldset->addField("bom_id", "text", [
                    "label" => Mage::helper("multimapper")->__("BOM ID"),
                    "name" => "bom_id",
                    "value" => isset($data["bom_id"]) ? $data["bom_id"] : "",
                ]);
                $fieldset->addField("stack", "text", [
                    "label" => Mage::helper("multimapper")->__("Stack"),
                    "name" => "stack",
                    "value" => $stack,
                    "disabled" => "disabled",
                ]);
            }
            return parent::_prepareForm();
        }
    }

    public function getContinueUrl()
    {
        return $this->getUrl('*/*/new', array(
            'createStack' => '{{createStack}}',
        ));
    }

    /**
     * Map bundle rules
     * @return array
     */
    protected function getProcessContextOption()
    {
        /**
         * @var $processContextModel Dyna_Catalog_Model_ProcessContext
         */
        $processContextModel = Mage::getModel('dyna_catalog/processContext');
        return $processContextModel->getAllProcessContextByNamesForForm();
    }

    /**
     * Get all the ProcessContext of current MultiMapper
     * @param $id
     * @return array
     */
    protected function getProcessContext($id)
    {
        $collection = Mage::getModel("multimapper/mapper")->getCollection()->addFieldToSelect('entity_id')->addFieldToFilter('main_table.entity_id', $id);

        $collection->getSelect()
            ->joinLeft(
                array('dyna_multi_mapper_addons'=>'dyna_multi_mapper_addons'),
                'dyna_multi_mapper_addons.mapper_id = main_table.entity_id',
                array('addonID' => 'dyna_multi_mapper_addons.entity_id')
            )
            ->joinLeft(
                array('dyna_multi_mapper_index_process_context'=>'dyna_multi_mapper_index_process_context'),
                'dyna_multi_mapper_index_process_context.addon_id = dyna_multi_mapper_addons.entity_id',
                array('')
            )
            ->joinLeft(
                array('process_context'=>'process_context'),
                'process_context.entity_id = dyna_multi_mapper_index_process_context.process_context_id',
                array('id' => 'dyna_multi_mapper_index_process_context.process_context_id')
            );

        $processContexts = array();

        foreach ($collection->getData() as $item) {
            if ($item['id']) {
                $processContexts[] = $item['id'];
            }
        }

        $processContextIds = array_values($processContexts);

        return $processContextIds;
    }
}

