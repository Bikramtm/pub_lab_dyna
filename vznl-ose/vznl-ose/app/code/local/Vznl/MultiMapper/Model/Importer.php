<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_MultiMapper_Model_Importer
 */
class Vznl_MultiMapper_Model_Importer extends Dyna_MultiMapper_Model_Importer
{
    const ALL_PROCESS_CONTEXTS = "*";

    private $defaultProcessContexts;
    private $currentRuleProcessContext;
    /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
    private $processContextHelper;

    protected $header;
    /**
     * @var string
     */
    protected $_csvDelimiter = ";";

    protected $mainFields = [
        'sku',
        'service_expression',
        'priority',
        'stop_execution',
        'comment',
        'xpath_outgoing',
        'xpath_incomming',
        'component_type',
        'direction',
        'process_context',
        'bom_id',
    ];

    /** @var Vznl_Import_Helper_Data $_helper */
    public $_helper;

    protected $mobileMainFields = [
        'pp_sku',
        'pp_sku_description',
        'promo_sku',
        'promo_sku_description',
        'oms_bo_code',
        'oms_bo_type',
        'oms_pbo_code',
        'oms_pbo_type',
        'oms_product_offering_code',
        'oms_product_offering_id',
        'oms_product_code',
        'oms_mc_code',
        'oms_comp_code_path',
        'oms_comp_id',
        'oms_component_attr_1',
        'oms_attr_code_1',
        'oms_attr_value_1',
        'oms_component_attr_2',
        'oms_attr_code_2',
        'oms_attr_value_2',
        'oms_component_attr_3',
        'oms_attr_code_3',
        'oms_attr_value_3',
        'oms_component_attr_4',
        'oms_attr_code_4',
        'oms_attr_value_4',
        'oms_component_attr_5',
        'oms_attr_code_5',
        'oms_attr_value_5',
        'oms_component_attr_6',
        'oms_attr_code_6',
        'oms_attr_value_6',
        'oms_component_attr_7',
        'oms_attr_code_7',
        'oms_attr_value_7',
        'oms_component_attr_8',
        'oms_attr_code_8',
        'oms_attr_value_8',
        'oms_component_attr_9',
        'oms_attr_code_9',
        'oms_attr_value_9',
        'oms_component_attr_10',
        'oms_attr_code_10',
        'oms_attr_value_10',
        'service_expression',
        'priority',
        'direction',
        'process_context'
    ];

    /** @var string $_logFileName */
    protected $_logFileName = "multimapper_peal_import";

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper('vznl_import');
    }

    /**
     * @param $path
     * @return bool
     * @throws Exception
     */
    public function import($path)
    {
        $this->_helper->setPathToImportFile($path);
        $file = fopen($path, 'r');
        $lc = 0;
        $success = true;

        // Variable for logging purposes
        $successCount = 0;
        $this->processContextHelper = Mage::helper("dyna_catalog/processContext");
        $this->setDefaultProcessContexts();

        while (($line = fgetcsv($file, null, $this->_csvDelimiter)) !== false) {
            $lc++;
            if ($lc == 1) {
                $line = array_map('strtolower', $line);
                //check if match our template
                if (strpos($path,"Mobile") !== false) {
                    $checkHeader = $this->getImportHelper()->checkHeader($line, 'mobile_multimapper');
                } elseif (strpos($path,"Fixed") !== false) {
                    $checkHeader = $this->getImportHelper()->checkHeader($line, 'multimapper');
                }

                //log messages
                $messages = $this->getImportHelper()->getMessages();
                $attachments = [];
                foreach ($messages as $key => $msg){
                    //write file log
                    $this->_log($msg);
                    if ($key !== 'success' && substr($this->_pathToFile, 0, 9) != '/vagrant/') {
                        $attachments[] = ['key' => $key, 'text' => $msg];
                    }
                }
                if (!empty($attachments)) {
                    $msg = 'While processing file ' . $this->_pathToFile . ' we encountered the following:';
                    $this->_helper->postToSlack($msg, $attachments);
                }
                //check if the headers are ok
                if (!$checkHeader) {
                    return false;
                }
                $this->prepareMultiMapperColumns($line);
                continue;
            }
            //used for logging purposes
            $this->_totalFileRows++;
            $this->currentRuleProcessContext = null;

            /** Parse addon like values */
            if (!$csvLineData = array_combine($this->header, $line)) {
                $this->_logError("[ERROR] Inconsistency in import file as the first row (columns headers) columns count differs from the current line (" . $lc . ") columns count. Skipping row.");
                continue;
            }

            $csvLineData = $this->parseMultiMapperData($csvLineData);
            if (strpos($path,"Mobile")) {
                $sku = trim($csvLineData['pp_sku'] ?: $csvLineData['PP_SKU'] ?: "");
            } else {
                $sku = trim($csvLineData['sku'] ?: $csvLineData['SKU'] ?: "");
            }
            $csvLineData['direction'] = strtolower($csvLineData['direction']);
            $direction = trim($csvLineData['direction'] ? $csvLineData['direction'] : "both");
            //skip the row if the direction is not in AND there is a bom_id value
            if ($direction != "in" && $csvLineData['bom_id']) {
                $csvLineData['bom_id'] = null;
                $this->_logError('[ERROR] Skipping row ' . var_export($line, true) . ' because for entries with bom_id the direction should be In.');
                continue;
            }

            $product = Mage::getModel("catalog/product")->getIdBySku($sku);
            if (!$product) {
                $success = false;
                $this->_logError('[ERROR] Skipping row '  . $lc . ' ' . var_export($line, true) . ' because the product could not be found using the SKU "' . $sku . '"');
                continue;
            }

            $uniqueRecord = [];
            $skipMapper = 0;
            /** @var Vznl_MultiMapper_Model_Mapper $mapperModel */
            $mapperModel = Mage::getModel('vznl_multimapper/mapper');
            $csvLineData['service_expression'] = $csvLineData['service_expression'] ?? null;

            if (strpos($path,"Mobile") !== false) {
                $uniqueRecord = array(
                    'pp_sku' => trim($csvLineData['pp_sku']),
                    'promo_sku' => trim($csvLineData['promo_sku']),
                    'oms_attr_value_1' => trim($csvLineData['oms_attr_value_1']),
                    'direction' => trim($csvLineData['direction']),
                    'oms_bo_code' => trim($csvLineData['oms_bo_code']),
                    'oms_comp_id' => trim($csvLineData['oms_comp_id']),
                    'oms_product_offering_id' => trim($csvLineData['oms_product_offering_id'])
                );
            } elseif (strpos($path,"Fixed") !== false) {
                $uniqueRecord = array(
                    'sku' => trim($csvLineData['sku']),
                    'direction' => trim($csvLineData['direction']),
                    'bom_id' => trim($csvLineData['bom_id']),
                    'service_expression' => trim($csvLineData['service_expression'])
                );
            }
            $mapper = $mapperModel->getMapperByUniqueIndex($uniqueRecord, $path);

            if ($mapper->getSize()) {
                if (strpos($path, "Fixed") !== false) {
                    $mapperId = $mapper->getFirstItem()->getData('entity_id');
                    $fixedAddon = Mage::getModel("dyna_multimapper/addon")->load($mapperId, 'mapper_id');
                    if ($fixedAddon->getData('technical_id') !== $csvLineData['technical_id']) {
                        $skipMapper = 1;
                        $this->_log("Multimapper entry found, saving only the Addon details");
                    }
                }
                if (!$skipMapper) {
                    $this->_logError('[ERROR] Skipping row no ' . $lc . ' since duplicate record found');
                    continue;
                }
            }

            if (!$skipMapper) {
                // determine the process context. if is a string it must be a valid string that can be mapped to an id, if is an id it must be a valid one, else -> error)
                // @todo remove hardcode * for process_context if not provided when VFDED1W3S-2478 is done
                if (!isset($csvLineData["process_context"])) {
                    $csvLineData["process_context"] = "*";
                }
                if (!$this->setProcessContextId($csvLineData["process_context"], $line)) {
                    $this->_logError('[ERROR] Skipping row no ' . $lc);
                    continue;
                }

                if (strpos($path,"Mobile") !== false) {
                    foreach ($this->mobileMainFields as $mainField) {
                        if (isset($csvLineData[$mainField])) {
                            $mapperModel->setData($mainField, trim($csvLineData[$mainField]));
                        }
                    }
                } else {
                    foreach ($this->mainFields as $mainField) {
                        if (isset($csvLineData[$mainField])) {
                            $mapperModel->setData($mainField, trim($csvLineData[$mainField]));
                        }
                    }
                }
                try {
                    $mapperModel->setStack($this->stack);
                    $mapperModel->save();
                }
                catch(Exception $exception){
                    $this->_logError('[ERROR] Skipping row no ' . $lc);
                    $this->_logError($exception->getMessage());
                    continue;
                }
            }
            /** @var Dyna_MultiMapper_Model_Addon $addon */
            $addon = Mage::getModel("dyna_multimapper/addon");
            $addon->setStack($this->stack);

            if ($skipMapper) {
                $addon->setData('mapper_id', $mapperId);
            } else {
                $addon->setData('mapper_id', $mapperModel->getId());
            }
            $addon->setData('backend', isset($csvLineData['backend']) ? $csvLineData['backend'] : (isset($csvLineData['default_backend']) ? $csvLineData['default_backend'] : null));

            if (isset($csvLineData['technical_id']) || isset($csvLineData['oms_bo_code'])) {

                foreach ($this->fields as $field) {
                    if (isset($this->mapping[$field])) {
                        // mapping for fields
                        $field = $this->mapping[$field];
                    }
                    $addon->setData($field, isset($csvLineData[$field]) ? trim($csvLineData[$field]) : null);
                }

                $addon->save();

                if ($this->currentRuleProcessContext) {
                    $addon->setProcessContextsIds($this->currentRuleProcessContext);
                }
            } else {
                $fields = array_diff($this->header, $this->mainFields, $this->mapping);
                $flippedFields = array_flip($fields);
                foreach ($fields as $field) {
                    if (strpos($field, 'Additional') !== false) {
                        $fieldReplaced = str_replace('Additional', '', $field);
                        if (!isset($flippedFields[$fieldReplaced])) {
                            $this->_logError(sprintf('Did not find field %s for %s', str_replace('Additional', '', $field), $field));
                            continue;
                        }
                        $newAddon = clone $addon;
                        $newAddon->setData('addon_name', trim(str_replace('Additional', '', $field)))
                            ->setData('addon_additional_value', trim($csvLineData[$field]))
                            ->setData('addon_value', trim($csvLineData[str_replace('Additional', '', $field)]))
                            ->save();
                        if ($this->currentRuleProcessContext) {
                            $newAddon->setProcessContextsIds($this->currentRuleProcessContext);
                        }
                    } else {
                        continue;
                    }
                }
            }

            $this->_log(sprintf('Successfully imported CSV line %d [product `%s` ] multimapper entry.', $lc, $sku), true);
            $successCount++;
        }
        //used for logging purposes
        $this->_skippedFileRows = $this->_totalFileRows - $successCount;

        return $success;
    }

    /**
     * @return Mage_Core_Helper_Abstract|null
     */
    protected function getImportHelper()
    {
        /** @var Dyna_Import_Helper_Data _helper */
        return $this->_helper == null ? Mage::helper("dyna_import/data") : $this->_helper;
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function parseMultiMapperData($data)
    {
        /** stop_execution */
        if (!empty($data['stop_execution'])) {
            if (strtolower($data['stop_execution']) == "no") {
                $data['stop_execution'] = 0;
            } else {
                $data['stop_execution'] = 1;
            }
        }

        /** direction - OMNVFDE-2582
         * in = Technical coming in = 360 View
         * out = Technical going out = Submitorder
         * both = all of the above
         */
        if (empty($data['direction']) || !in_array(strtolower($data['direction']), array('in', 'out', 'both'))) {
            $data['direction'] = 'both';
        }

        return $data;
    }

    /**
     * Return true or false if a string is empty
     * @param $value string
     * @return string
     */
    protected function cleanEmptyValues($value) : string
    {
        return str_replace("-", "", trim($value));
    }

    /**
     * Set the default process contexts [id->code]
     */
    private function setDefaultProcessContexts()
    {
        if (!$this->defaultProcessContexts) {
            $this->defaultProcessContexts = $this->processContextHelper->getProcessContextsByCodes();
        }
    }

    /**
     * Set the process context id
     * if it is a string it must be mapped to one of the possible strings for process context (it string must be contained in one of the default process context) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for process context
     * else -> skip row, error
     * @param string $processContextsForRule
     * @param array $line
     * @return bool
     */
    private function setProcessContextId($processContextsForRule, $line)
    {
        if (trim($processContextsForRule) == self::ALL_PROCESS_CONTEXTS) {
            $this->currentRuleProcessContext = $this->defaultProcessContexts;
            return true;
        }

        $processContextsForRuleArray = explode(',', strtoupper(trim($processContextsForRule)));
        foreach ($processContextsForRuleArray as $processRuleContextForRule) {
            if (isset($this->defaultProcessContexts[trim($processRuleContextForRule)])) {
                $this->currentRuleProcessContext[] = $this->defaultProcessContexts[trim($processRuleContextForRule)];
            } else {
                $this->_logError('[ERROR] the process_context ' . trim($processRuleContextForRule) . ' is not valid (rule line  ' . var_export($line, true) . '). skipping row');
                return false;
            }
        }

        return true;
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    /**
     * Log Error function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }

    public function getFileLogName()
    {
        return strtolower($this->stack) . '_' . $this->_logFileName . '.' . $this->_logFileExtension;
    }

    /**
     * @param $firstRow
     * @return Vznl_MultiMapper_Model_Importer
     */
    protected function prepareMultiMapperColumns($firstRow) : self
    {
        $firstRow = array_filter($firstRow, [$this, "cleanEmptyValues"]);
        if (in_array("pp_sku", array_values($firstRow))) {
            /** @var Vznl_Import_Helper_Data mapping */
            $this->fields = $this->getImportHelper()->getMapping('mobile_multimapper', ['required', 'optional']);
        } else {
            /** @var Vznl_Import_Helper_Data mapping */
            $this->mapping = $this->getImportHelper()->getMapping('multimapper', ['mapping']);
            $this->fields = $this->getImportHelper()->getMapping('multimapper', ['required', 'optional']);
        }
        foreach ($firstRow as $entry) {
            /** Checking whether or not this field is mapped to another one */
            if (array_key_exists($entry, $this->mapping)) {
                $this->header[] = $this->mapping[$entry];
            } else {
                /** Not a mapped field, then save it as it is;  */
                $this->header[] = $entry;
            }
        }

        return $this;
    }

    public function clearMultiMapperData($promoSku, $ppSku, $omsAttributeValue, $direction)
    {
        $coreConnection = Mage::getSingleton('core/resource');
        $connection = $coreConnection->getConnection('core_write');
        $resTableName = Mage::getResourceSingleton('multimapper/mapper');
        $tableName = $resTableName->getMainTable();
        if (($connection->delete($tableName,
                [
                    'promo_sku = ?' => $promoSku,
                    'pp_sku = ?' => $ppSku,
                    'oms_attr_value_1 = ?' => $omsAttributeValue,
                    'direction = ?' => $direction
                ])
            )
        )
        {
            return "Multimapper removed";
        }
    }
}
