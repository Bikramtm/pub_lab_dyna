<?php

class Vznl_MultiMapper_Model_ImporterXML extends Dyna_MultiMapper_Model_ImporterXML
{
    const ALL_PROCESS_CONTEXTS = "*";

    private $defaultProcessContexts;
    private $currentRuleProcessContext;
    /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
    private $processContextHelper;

    protected $lineNo = 0;
    protected $skippedComponent = [];

    /**
     * Dyna_MultiMapper_Model_ImporterXML constructor.
     * @param $args
     */
    public function __construct($args)
    {
        $this->logFile = "import_multimapper_rules.log";
        parent::__construct($args);
    }

    public function import()
    {
        // retrieve data
        $xmlComponents = $this->getXmlData(true, false);
        $this->processContextHelper = Mage::helper("dyna_catalog/processContext");
        $this->setDefaultProcessContexts();


        // parse components
        foreach ($xmlComponents->Component as $component) {
            foreach ($component->SimpleProduct as $simpleProduct) {
                // set current line
                $this->lineNo++;

                // get component data
                $systemName = (string) $component->SystemName;
                $OGWComponentName = (string) $component->OGWComponentName;
                $sourceComponentID = (string) $component->SourceComponentID;

                $sku = (string) $simpleProduct->SKU;
                $elementId = (string) $simpleProduct->ElementId;
                $OGWComponentIdReference = (string) $simpleProduct->OGWComponentIdReference;

                // is SKU is missing, skip and log row
                if (!$sku) {
                    $this->skippedComponent[$this->lineNo] = $this->lineNo;
                    $this->_logError('[ERROR] Skipping element with ElementId '.$elementId.' because SKU field is missing(SourceComponentID = '.$sourceComponentID.').');
                    continue;
                }

                // if invalid SKU, skip and log row
                $product = Mage::getModel("catalog/product")->getIdBySku($sku);
                if (!$product) {
                    $this->skippedComponent[$this->lineNo] = $this->lineNo;
                    $this->_logError('[ERROR] Skipping element with ElementId '.$elementId.' because the product could not be found using the SKU "' . $sku . '"');
                    continue;
                }

                // search current mapper
                /** @var Dyna_MultiMapper_Model_Mapper $mapperModel */
                $mapperModel = Mage::getModel('multimapper/mapper')
                    ->getCollection()
                    ->addFieldToFilter('sku', ['eq' => $sku])
                    ->getFirstItem();

                // create new mapper entry
                if (!$mapperModel || !$mapperModel->getId()) {
                    $mapperModel = Mage::getModel('multimapper/mapper');
                    $mapperModel->setServiceExpression(null)
                        ->setPriority(null)
                        ->setStopExecution(null)
                        ->setComment(null)
                        ->setSku($sku)
                        ->setXpathOutgoing(null)
                        ->setXpathIncomming(null)
                        ->setComponentType(null)
                        ->setDirection(null)
                        ->setDefaulted(null)
                        ->setOgwComponentName($OGWComponentName)
                        ->setSourceComponentId($sourceComponentID)
                        ->setSystemName($systemName)
                        ->setElementId($elementId)
                        ->setOgwComponentIdReference($OGWComponentIdReference);
                }
                $mapperModel->setStack($this->stack);
                $mapperModel->save();

                // is mapper cant be saved, skip this row
                if (!$mapperModel->getId()) {
                    $this->skippedComponent[$this->lineNo] = $this->lineNo;
                    $this->logLine('[ERROR] Skipping element with ElementID '.$elementId.' because mapper cant be saved.');
                    continue;
                }

                // parse each mapper addons(attributes)
                foreach ($simpleProduct->Attribute as $attribute) {
                    // get attribute data
                    $attributeCode = (string) $attribute->AttributeCode;
                    $attributeValue = (string) $attribute->AttributeValue;
                    $attributeAction = (string) $attribute->AttributeAction;
                    $attributeDirection = (string) $attribute->Direction;
                    $attributeBasketAction = (string) $attribute->BasketAction;
                    $attributeAdditionalValue = (string) $attribute->AttributeAdditionalValue;
                    $attributeComponentElementId = (string) $attribute->ComponentElementId;
                    $attributeProcessContext = $attribute->ProcessContexts;
                    $attributeProcessContexts = array();
                    if (isset($attributeProcessContext)) {
                        foreach($attributeProcessContext->ProcessContext as $pc){
                            $attributeProcessContexts[] = (string)$pc;
                        }
                    }
                    if(empty($attributeProcessContexts)){
                        $attributeProcessContexts = '*';
                    }
                    else{
                        $attributeProcessContexts = implode(',', $attributeProcessContexts);
                    }
                    if (!$this->setProcessContextId($attributeProcessContexts, $elementId)) {
                        $this->_logError('[ERROR] Skipping element with ElementID ' . $elementId);
                        continue;
                    }

                    // if addon is not found, create and save it
                    /** @var Dyna_MultiMapper_Model_Addon $addonModel */
                    $addonModel = Mage::getModel('dyna_multimapper/addon');

                    // try to determine source condition
                    $condition = false;
                    if (isset($attribute->SourceConditions)) {
                        try {
                            /** @var Dyna_Import_Model_ExpressionLanguage_Source_Conditions $condition */
                            $condition = Mage::getModel('dyna_import/expressionLanguage_source_conditions');
                            $condition->parseConditions($attribute->SourceConditions, 'Source');
                        } catch (Exception $exception) {
                            $condition = false;
                            $this->_logError('[ERROR] Source condition not created becasue: '.$exception->getMessage());
                        }
                    }

                    $addonModel->setMapperId($mapperModel->getId())
                        ->setAddonName($attributeCode)
                        ->setAddonAdditionalValue($attributeAdditionalValue)
                        ->setAddonValue($attributeValue)
                        ->setPromoId(null)
                        ->setTechnicalId(null)
                        ->setNatureCode(null)
                        ->setBackend(null)
                        ->setAttributeAction($attributeAction)
                        ->setServiceExpression(($condition) ? $condition->asString() : '')
                        ->setBasketAction($attributeBasketAction)
                        ->setComponentElementId($attributeComponentElementId)
                        ->setDirection($attributeDirection);

                    $addonModel->setStack($this->stack);
                    // save addon
                    $addonModel->save();

                    if($this->currentRuleProcessContext) {
                        $addonModel->setProcessContextsIds($this->currentRuleProcessContext);
                    }
                }
            }
            $this->_skippedFileRows = count($this->skippedComponent);
        }

        $this->_totalFileRows = $this->lineNo;
    }

    /**
     * Set the default process contexts [id->code]
     */
    private function setDefaultProcessContexts()
    {
        if (!$this->defaultProcessContexts) {
            $this->defaultProcessContexts = $this->processContextHelper->getProcessContextsByCodes();
        }
    }

    /**
     * Set the process context id
     * if it is a string it must be mapped to one of the possible strings for process context (it string must be contained in one of the default process context) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for process context
     * else -> skip row, error
     * @param string $processContextsForRule
     * @param string $elementID
     * @return bool
     */
    private function setProcessContextId($processContextsForRule, $elementID)
    {
        if (trim($processContextsForRule) == self::ALL_PROCESS_CONTEXTS) {
            $this->currentRuleProcessContext = $this->defaultProcessContexts;
            return true;
        }

        $processContextsForRuleArray = explode(',', strtoupper(trim($processContextsForRule)));
        $this->currentRuleProcessContext = array();
        foreach ($processContextsForRuleArray as $processRuleContextForRule) {
            if (isset($this->defaultProcessContexts[trim($processRuleContextForRule)])) {
                $this->currentRuleProcessContext[] = $this->defaultProcessContexts[trim($processRuleContextForRule)];
            } else {
                $this->_logError('[ERROR] the process_context ' . trim($processRuleContextForRule) . ' is not valid (element ID  ' . var_export($elementID, true) . '). skipping row');
                return false;
            }
        }

        return true;
    }
}
