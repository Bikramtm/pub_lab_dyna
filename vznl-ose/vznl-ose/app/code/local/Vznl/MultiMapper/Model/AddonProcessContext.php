<?php

/**
 * Class Vznl_MultiMapper_Model_AddonProcessContext
 */
class Vznl_MultiMapper_Model_addonProcessContext extends Dyna_MultiMapper_Model_addonProcessContext
{
    protected function _construct()
    {
        $this->_init("dyna_multimapper/addonProcessContext");
    }
}
