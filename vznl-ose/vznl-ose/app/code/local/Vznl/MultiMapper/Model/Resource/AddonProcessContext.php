<?php

/**
 * Class Vznl_MultiMapper_Model_Resource_AddonProcessContext
 */
class Vznl_MultiMapper_Model_Resource_addonProcessContext extends Dyna_MultiMapper_Model_Resource_addonProcessContext
{
    public function _construct()
    {
        $this->_init("dyna_multimapper/addonProcessContext", 'addon_id');
    }
}
