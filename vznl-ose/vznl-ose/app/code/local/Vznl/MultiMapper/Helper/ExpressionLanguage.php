<?php
/**
 * Wrapper over Symfony Expression Language used for gathering evaluation responses into a single response object used later for determining what sources
 * evaluate conditions to true
 * Class Dyna_Bundles_Helper_ExpressionLanguage
 */

class Vznl_MultiMapper_Helper_ExpressionLanguage extends Symfony\Component\ExpressionLanguage\ExpressionLanguage
{
    protected $response = array();
    /** @var Dyna_Checkout_Model_Sales_Quote */
    protected $quote = null;
    protected $simulation = false;

    /**
     * Evaluate condition against certain objects
     * Returned values are merged into $response property which can later be reset if needed
     * @param string|\Symfony\Component\ExpressionLanguage\Expression $expression
     * @param array $values
     * @return string
     */
    public function evaluate($expression, $values = array())
    {
        $expressionObject = array(
            'order' => Mage::getModel('vznl_configurator/expression_order'),
        );
        // forwarding not operator to expression language evaluation object as parameter (@see OVG-2301)
        $expression = preg_replace("/(![^(]+)\(([^)]+)\)/","$1($2, true)",$expression);
        /** @var Dyna_Core_Helper_Data $coreHelper */
        $coreHelper = Mage::helper('dyna_core');

        $result = $coreHelper->evaluateExpressionLanguage($expression, $expressionObject);

        // merge all results, if not empty array, into one big array
        // can be later accessed by keys ex: subscription => array(value1, value2, value3) etc
        if ($result && is_array($result)) {
            $this->response = Mage::helper('dyna_core')->arrayRecursiveUnique(array_merge_recursive($this->response, $result));
        } elseif ($result && is_bool($result)) {
            // try to get last response from registry (evaluating with "and" and "or" will result in boolean response)
            if (($newResult = Mage::registry('install_base_expression_result')) && is_array($newResult)) {
                $this->response = Mage::helper('dyna_core')->arrayRecursiveUnique(array_merge_recursive($this->response, $newResult));
            }
        }
        return $result;
    }
}