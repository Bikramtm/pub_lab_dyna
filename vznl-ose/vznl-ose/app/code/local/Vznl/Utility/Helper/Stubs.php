<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 * Class Vznl_Utility_Helper_Stubs
 */

class Vznl_Utility_Helper_Stubs extends Mage_Core_Helper_Abstract
{
    const XML_PATH_PHP_STUB_ENABLE = 'dev/debug/php_stub_enabled';
    
    const ESB_STATUS_CC_ILT_PARTIAL = 'Partial';
    const ESB_STATUS_CC_PENDING = 'Pending';
    const ESB_STATUS_CC_APPROVED = 'Approved';
    const ESB_STATUS_CC_REJECTED = 'Rejected';
    const ESB_STATUS_CC_ADDINFO = 'AdditionalInfoRequired';
    const ESB_STATUS_CC_REFERRED = 'Referred';
    
    const ESB_STATUS_NP_PENDING = 'Pending';
    const ESB_STATUS_NP_APPROVED = 'Approved';
    const ESB_STATUS_NP_REJECTED = 'Rejected';
    const ESB_STATUS_NP_REFERRED = 'Referred';
    const ESB_STATUS_NP_REJECTED_CANCELLED = 'Final Rejected';
    
    protected $_superOrder;
    protected $_stub;
    protected $_packageId;
    protected $_additionalData;
    protected $_packages;
    
    /** @var  Vznl_Esb_Helper_Data */
    protected $_esbHelper;
    
    protected $_stubOptions = [
    	'order-happyflow',
    	'order-axi-error',
    	'order-failed-validation',
    	'order-exception',
    	'order-readyforfulfillment',
    	'order-failed-delivery',
    	'order-cancel',
    	'creditcheck-rejected',
    	'creditcheck-additionalinforequired',
    	'creditcheck-accepted',
    	'creditcheck-referred',
    	'creditcheck-ilt-tohigh',
    	'creditcheck-ilt-missingdata',
    	'adyen-payment-success',
    	'porting-pending',
    	'porting-rejected',
    	'porting-approved',
    	'porting-final-rejected',
    	'handle-process-axi',
    	'handle-add-sim',
    	'handle-add-imei'
    ];
    
    /**
     * execute the stubs
     */
    public function execute($data)
    {
    	$this->_esbHelper = Mage::helper('vznl_esb');
    	try {
    		$this->_stub = $data['stub'];
    		$orderNumber = $data['orderNumber'];
    		$this->_packageId = $data['packageId'];
    		$this->_additionalData = $data['additionalData'];
    	
    		if (is_numeric($this->_stub)) {
    			$this->_stub = $this->_stubOptions[(intval($this->_stub) -1)];
    		}
    	    
    		if (!in_array($this->_stub, $this->_stubOptions)) {
    			throw new Exception('Stub not found, please provide a valid stub name');
    		}
    	
    		if (empty($orderNumber)) {
    			$this->_superOrder = Mage::getModel('superorder/superorder')->getCollection()->setOrder('created_at', 'desc')->setPageSize(1)->getLastItem();
    		} else {
    		    $this->_superOrder = Mage::getModel('superorder/superorder')->load((string) $orderNumber, 'order_number');
    		}
    	    
    		$this->_uri = empty($data['uri']) ? 'telesales-omnius-vf-nl.dev.ynad.local' : $data['uri'];
    	    $message = $this->_executeStub();
    		
    		return $message . $this->_stub . ' : OK' . PHP_EOL;
    	} catch (Exception $e) {
    		return $e->getMessage() . PHP_EOL;
    	}
    }
    
    /**
     * parse the incoming web command
     * @param string $command
     */
    public function parseWebCommand($command)
    {
        $inputArray = explode(' ', $command);
        
        // initialize the parsed array
        $parsedArray = array(
        	'stub' => '',
        	'orderNumber' => '',
        	'packageId' => '',
        	'uri' => '',
        	'additionalData' => ''	
        );
        
        while ($inputArray) {
        	$key = array_shift($inputArray);
        	
        	switch ($key) {
        		case '-s' : 
        			$parsedArray['stub'] = array_shift($inputArray);
        			break;
        			
        		case '-o' :
        			$parsedArray['orderNumber'] = array_shift($inputArray);
        			break;
        		
        		case '-p' :
        			$parsedArray['packageId'] = array_shift($inputArray);
        			break;
        			
        		case '-u' :
        			$parsedArray['uri'] = array_shift($inputArray);
        			break;
        			
        		case '-a' :
        			$parsedArray['additionalData'] = array_shift($inputArray);
        			break;
        	}
        }
        
        return $parsedArray;       
    }
        
    protected function _executeStub()
    {
    	$parts = explode('-', $this->_stub);
        $return = '';
        
    	switch ($parts[0]) {
    		case 'order':
    			$return = $this->_runOrderStub($this->_stub);
    			break;
    			
    		case 'adyen':
    			$return = $this->_runAdyenStub($this->_stub);
    			break;
    			
    		case 'creditcheck':
    			if ($parts[1] == 'ilt') {
    				$return = $this->_runIltStub($this->_stub);
    			} else {
    				$return = $this->_runCreditcheckStub($this->_stub);
    			}
    			break;
    			
    		case 'porting':
    			$return = $this->_runPortingStub($this->_stub);
    			break;
    			
    		case 'handle':
    			$return = $this->_handleRequiredAction($this->_stub);
    			break;
    			
    		default:
    			throw new Exception('Invalid stub part found');
    	}
    	
    	return $return;
    }
    
    
    protected function _runAdyenStub($stub)
    {
    	switch ($stub) {
    		case 'adyen-payment-success':
    			$deliveryOrder = $this->_superOrder->getDeliveryOrders()->getFirstItem();
    			$totals = $deliveryOrder->getTotals();
    
    			$config = array(
    				'adapter'      => 'Zend_Http_Client_Adapter_Socket',
    				'ssltransport' => 'tls'
    			);
    			$client = new Zend_Http_Client('http://'.$this->_uri.'/adyen/process/ins', $config);
    			$client->setAuth('U-Buy', 'sadatrAdRapaSt4WaxEvEv7jEPhe=peN');
    			$client->setParameterPost(array(
    				'merchantAccountCode'  => 'VodafoneDevelopment',
    				'amount'   => $totals['total'],
    				'live' => false,
    				'eventCode' => 'AUTHORISATION',
    				'currency' => 'EUR',
    				'merchantReference' => $deliveryOrder->getIncrementId(),
    				'paymentMethod' => 'ideal',
    				'success' => true,
    				'pspReference' => uniqid().rand(10000,99999),
    			));
    			
    			$message =  'Calling Adyen API:' . PHP_EOL;
    			$message .= 'Order: ' . $deliveryOrder->getIncrementId() . PHP_EOL;
    			$message .= 'Total: ' . $totals['total'] . PHP_EOL;
    			$response = $client->request('POST');
    			return $message . $response->getBody() . PHP_EOL;
    	}
    }
    
    protected function _setInitialSuperOrderStatuses($orderNumber)
    {
    	$this->_esbHelper->setSaleOrderStatus($orderNumber . ';' . Vznl_Superorder_Model_Superorder::SO_INITIAL);
    	$this->_esbHelper->setSaleOrderStatus($orderNumber . ';' . Vznl_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI);
    	$this->_esbHelper->setSaleOrderStatus($orderNumber . ';' . Vznl_Superorder_Model_Superorder::SO_WAITING_FOR_VALIDATION);
    }
    
    protected function _setInitialPackageStatuses($orderNumber, $packageId)
    {
    	$this->_esbHelper->setPackageStatus($orderNumber . ';' . $packageId .';' . Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION);
    	$this->_esbHelper->setCreditcheckStatus($orderNumber . ';' . $packageId .';' . self::ESB_STATUS_CC_PENDING);
    	$this->_esbHelper->setPackageStatus($orderNumber . ';' . $packageId .';' . self::ESB_STATUS_CC_PENDING);
    	$this->_esbHelper->setVfOrderNr($orderNumber . ';' . $packageId .';' . $this->_generateDaOrderNumber());
    }
    
    protected function _runIltStub($stub)
    {
    	switch($stub) {
    		case 'creditcheck-ilt-tohigh':
    			$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());
    			foreach ($this->_getPackages() as $package) {
    				$packageId = $package->getPackageId();
    				$this->_setInitialPackageStatuses($this->_superOrder->getOrderNumber(), $packageId);
    				$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId .';' . self::ESB_STATUS_CC_ILT_PARTIAL);
    				$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId .';' . self::ESB_STATUS_CC_ILT_PARTIAL . ';Lening niet toereikend.#BKR was niet bereikbaar om te toetsen');
    			}
    			$this->_esbHelper->setCustomerBan($this->_superOrder->getOrderNumber() . ';' . $this->_superOrder->getCustomerId() . ';' . $this->_generateBan() . ';' . Vznl_Customer_Model_Customer_Customer::LABEL_UNIFY);
    			$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . Vznl_Superorder_Model_Superorder::SO_VALIDATED_PARTIALLY);
    			break;
    			
    		case 'creditcheck-ilt-missingdata':
    			$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());
    			foreach ($this->_getPackages() as $package) {
    				$packageId = $package->getPackageId();
    				$this->_setInitialPackageStatuses($this->_superOrder->getOrderNumber(), $packageId);
    				$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . self::ESB_STATUS_CC_ILT_PARTIAL);
    				$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . self::ESB_STATUS_CC_ILT_PARTIAL . ';ILT data ontbreekt');
    			}
    			$this->_esbHelper->setCustomerBan($this->_superOrder->getOrderNumber() . ';' . $this->_superOrder->getCustomerId() . ';' . $this->_generateBan() . ';' . Vznl_Customer_Model_Customer_Customer::LABEL_UNIFY);
    			$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . Vznl_Superorder_Model_Superorder::SO_VALIDATED_PARTIALLY);
    			break;
    	}
    }
    
    protected function _runPortingStub($stub) 
    {
    	switch ($stub){
    		case 'porting-pending' :
    			foreach ($this->_getPackages() as $package) {
    				$package->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PORTING_PENDING);
    				Mage::helper('vznl_utility')->saveModel($package);
    				$this->_esbHelper->setNumberportingStatus($this->_superOrder->getOrderNumber() . ';' . $package->getPackageId() . ';' . 
    					self::ESB_STATUS_NP_PENDING);
    			}
    			break;
    			
    		case 'porting-rejected' :
    			$this->_runPortingStub('porting-pending');
    			foreach ($this->_getPackages() as $package) {
    				$package->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PORTING_REJECTED);
    				Mage::helper('vznl_utility')->saveModel($package);
    				$this->_esbHelper->setNumberportingStatus($this->_superOrder->getOrderNumber() . ';' . $package->getPackageId() . ';' . 
    					self::ESB_STATUS_NP_REJECTED . ';' . 99 );
    			}
    			break;
    			
    		case 'porting-approved' :
    			$this->_runPortingStub('porting-pending');
    			foreach($this->_getPackages() as $package) {
    				$package->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PORTING_APPROVED);
    				Mage::helper('vznl_utility')->saveModel($package);
    				$this->_esbHelper->setNumberportingStatus($this->_superOrder->getOrderNumber() . ';' . $package->getPackageId() . ';' . 
    					self::ESB_STATUS_NP_APPROVED);
    			}
    			break;
    			
    		case 'porting-final-rejected':
    			$this->_runPortingStub('porting-rejected');
    			foreach($this->_getPackages() as $package) {
    				$package->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED);
    				Mage::helper('vznl_utility')->saveModel($package);
    				$this->_esbHelper->setNumberportingStatus($this->_superOrder->getOrderNumber() . ';' . $package->getPackageId() . ';' . 
    					self::ESB_STATUS_NP_REJECTED_CANCELLED);
    			}
    
    			$this->_superOrder->cancel();
    			break;
    	}
    }
    
    protected function _runCreditcheckStub($stub)
    {
    	switch($stub){
    		case 'creditcheck-accepted':
    			$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());
    
    			foreach ($this->_getPackages() as $package) {
    				$packageId = $package->getPackageId();
    				$this->_setInitialPackageStatuses($this->_superOrder->getOrderNumber(), $packageId);    			    
    				$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    					self::ESB_STATUS_CC_APPROVED);
    				$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    					Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED);
    				$this->_esbHelper->setVfOrderNr($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    					$this->_generateDaOrderNumber());
    			}
    
    			$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    				Vznl_Superorder_Model_Superorder::SO_VALIDATED);
    			$this->_esbHelper->setCustomerBan($this->_superOrder->getOrderNumber() . ';' . $this->_superOrder->getCustomerId() . ';' . 
    				$this->_generateBan() . ';' . Vznl_Customer_Model_Customer_Customer::LABEL_UNIFY);
    			break;
    			
    		case 'creditcheck-rejected':
    			$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());
    
    			foreach($this->_getPackages() as $package) {
    				$packageId = $package->getPackageId();
    				$this->_setInitialPackageStatuses($this->_superOrder->getOrderNumber(), $packageId);
    				$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    					self::ESB_STATUS_CC_REJECTED);
    				$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    					Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED);
    			}
    			$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber(). ';' . 
    				Vznl_Superorder_Model_Superorder::SO_VALIDATION_FAILED);
    			$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber(). ';' . 
    				Vznl_Superorder_Model_Superorder::SO_STATUS_CANCELLED . ';ESB');
    			break;
    
    		case 'creditcheck-additionalinforequired':
    			$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());
    
    			foreach($this->_getPackages() as $package) {
    				$packageId = $package->getPackageId();
    				$this->_setInitialPackageStatuses($this->_superOrder->getOrderNumber(), $packageId);
    				$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    					self::ESB_STATUS_CC_ADDINFO);
    				$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    					self::ESB_STATUS_CC_ADDINFO . ';We needz ur income!');
    			}
    			break;
    			
    		case 'creditcheck-referred':
    			$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());
    			$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    				Vznl_Superorder_Model_Superorder::SO_STATUS_REFERRED);
    
    			foreach($this->_getPackages() as $package) {
    				$packageId = $package->getPackageId();
    				$this->_setInitialPackageStatuses($this->_superOrder->getOrderNumber(), $packageId);
    				$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    					self::ESB_STATUS_CC_REFERRED);
    			}
    			break;
    	}
    }
    
    protected function _setPackageStatus($status)
    {
    	foreach ($this->_getPackages() as $package) {
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . 
    			$package->getPackageId() . ';' . $status);
    	}
    }
    
    protected function _runOrderExceptionStub()
    {
    	$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());    	
    	foreach ($this->_getPackages() as $package) {
    		$packageId = $package->getPackageId();
    		$this->_setInitialPackageStatuses($this->_superOrder->getOrderNumber(), $packageId);
    		$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			self::ESB_STATUS_CC_APPROVED);
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED);
    	}
    	
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' .
    		Vznl_Superorder_Model_Superorder::SO_VALIDATED);
    	$this->_esbHelper->setCustomerBan($this->_superOrder->getOrderNumber() . ';' . $this->_superOrder->getCustomerId() . ';' .
    		$this->_generateBan() . ';' . Vznl_Customer_Model_Customer_Customer::LABEL_UNIFY);
    	
    	$this->_setPackageStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED);    		
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' .
    		Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS);
    	
    	$this->_setPackageStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT);    	 
    	$this->_esbHelper->setSaleOrderError($this->_superOrder->getOrderNumber() . 
    		';12345;Backend generated error: whatever you want here');    	 
    }
    
    protected function _runOrderFailedValidationStub()
    {
    	$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());
    	
    	foreach ($this->_getPackages() as $package) {
    		$packageId = $package->getPackageId();
    		$this->_setInitialPackageStatuses($this->_superOrder->getOrderNumber(), $packageId);
    		$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_FAILED);
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED);
    	}
    	
    	$this->_esbHelper->setSaleOrderError($this->_superOrder->getOrderNumber() . 
    		';12345;Backend generated error: whatever you want here');
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    		Vznl_Superorder_Model_Superorder::SO_VALIDATION_FAILED);
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    		Vznl_Superorder_Model_Superorder::SO_STATUS_CANCELLED . ';ESB');
    }
    
    protected function _runOrderHappyFlowStub()
    {
    	$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());
    	
    	foreach ($this->_getPackages() as $package) {
    		$packageId = $package->getPackageId();
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION);
    		$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			self::ESB_STATUS_CC_PENDING);
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			self::ESB_STATUS_CC_PENDING);
    		$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			self::ESB_STATUS_CC_APPROVED);
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED);
    		$this->_esbHelper->setVfOrderNr($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			$this->_generateDaOrderNumber());
    	}
    	
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' .
    		Vznl_Superorder_Model_Superorder::SO_VALIDATED);
    	$this->_esbHelper->setCustomerBan($this->_superOrder->getOrderNumber() . ';' . $this->_superOrder->getCustomerId() . ';' . 
    		$this->_generateBan() . ';' . Vznl_Customer_Model_Customer_Customer::LABEL_UNIFY);
    	
    	$this->_setPackageStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED);
    	
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    		Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS);
    	
    	$this->_setPackageStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT);
    	
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    		Vznl_Superorder_Model_Superorder::SO_FULFILLED);
    	
    	foreach ($this->_getPackages() as $package) {
    		$packageId = $package->getPackageId();
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_DELIVERED);
    		$this->_esbHelper->setOrderMsdisn($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			$package->getPackageEsbNumber() . ';' . $this->_generateMsisdn());
    	}
    	
    	$this->_setAxiProcessed();
    }
    
    protected function _runOrderFailedDeliveryStub()
    {
    	$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());
    	
    	foreach ($this->_getPackages() as $package) {
    		$packageId = $package->getPackageId();
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION);
    		$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			self::ESB_STATUS_CC_PENDING);
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			self::ESB_STATUS_CC_PENDING);
    		$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			self::ESB_STATUS_CC_APPROVED);
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED);
    		$this->_esbHelper->setVfOrderNr($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			$this->_generateDaOrderNumber());
    	}
    	
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    		Vznl_Superorder_Model_Superorder::SO_VALIDATED);
    	$this->_esbHelper->setCustomerBan($this->_superOrder->getOrderNumber() . ';' . $this->_superOrder->getCustomerId() . ';' . 
    		$this->_generateBan() . ';' . Vznl_Customer_Model_Customer_Customer::LABEL_UNIFY);
    	
    	$this->_setPackageStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED);
    		
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    		Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS);
    	
    	$this->_setPackageStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT);
    	
    	foreach ($this->_getPackages() as $package) {
    		$packageId = $package->getPackageId();
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_NOT_DELIVERED);
    		$this->_esbHelper->setOrderMsdisn($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			$package->getPackageEsbNumber() . ';' . $this->_generateMsisdn());
    	}
    	
    	$this->_setAxiProcessed();
    }
    
    protected function _runOrderReadyForFulfillmentStub()
    {
    	$this->_setInitialSuperOrderStatuses($this->_superOrder->getOrderNumber());
    	
    	foreach($this->_getPackages() as $package) {
    		$packageId = $package->getPackageId();
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION);
    		$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			self::ESB_STATUS_CC_PENDING);
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			self::ESB_STATUS_CC_PENDING);
    		$this->_esbHelper->setCreditcheckStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			self::ESB_STATUS_CC_APPROVED);
    		$this->_esbHelper->setPackageStatus($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED);
    		$this->_esbHelper->setVfOrderNr($this->_superOrder->getOrderNumber() . ';' . $packageId . ';' . 
    			$this->_generateDaOrderNumber());
    	}
    	
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' .
    			Vznl_Superorder_Model_Superorder::SO_VALIDATED);
    	$this->_esbHelper->setCustomerBan($this->_superOrder->getOrderNumber() . ';' .
    			$this->_superOrder->getCustomerId() . ';' . $this->_generateBan() . ';' . Vznl_Customer_Model_Customer_Customer::LABEL_UNIFY);
    	
    	$this->_setPackageStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED);
    	$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    		Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS);
    	
    	$this->_setPackageStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT);
    	$this->_setAxiProcessed();
    }
    
    protected function _runOrderStub($stub)
    {
    	switch($stub){
    		case 'order-axi-error':
    			$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    			    Vznl_Superorder_Model_Superorder::SO_INITIAL);
                $this->_setPackageStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED);    
    			
                $this->_esbHelper->setSaleOrderError($this->_superOrder->getOrderNumber() . ';1000;Random Axi Error');
    			$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber(). ';' . 
    					Vznl_Superorder_Model_Superorder::SO_FAILED);
    			break;
    			
    		case 'order-exception':
    			$this->_runOrderExceptionStub();
    			break;
    			
    		case 'order-failed-validation':
    			$this->_runOrderFailedValidationStub();    			
    			break;
    			
    		case 'order-happyflow':
    			$this->_runOrderHappyFlowStub();    			
    			break;
    			
    		case 'order-failed-delivery':
    			$this->_runOrderFailedDeliveryStub();    			
    			break;
    			
    		case 'order-readyforfulfillment':
    			$this->_runOrderReadyForFulfillmentStub();
    			break;
    			
    		case 'order-cancel':
    			$this->_esbHelper->setSaleOrderStatus($this->_superOrder->getOrderNumber() . ';' . 
    			    Vznl_Superorder_Model_Superorder::SO_CANCELLED);
    			break;
    	}
    }
    
    protected function _handleRequiredAction($stub)
    {
    	$write = Mage::getSingleton('core/resource')->getConnection('core_write');
    	switch ($stub) {
    		case 'handle-process-axi':
    			$this->_setAxiProcessed();
    			break;
    			
    		case 'handle-add-sim':
    			if (!$this->_additionalData) {
    				throw new Exception('Please give the sim number as `-a` parameter');
    			}
    			
    			if ($this->_superOrder->getEntityId()) {
    				$where = 'order_id = ' . $this->_superOrder->getEntityId();
    				if ($this->_packageId) {
    					$where .= ' AND package_id = ' . $this->_packageId;
    				}    				 
    				$write->update(
    					'catalog_package',
    					array('sim_number' => $this->_additionalData),
    					$where
    				);
    			}
    			break;
    		case 'handle-add-imei':
    			if (!$this->_additionalData) {
    				throw new Exception('Please give the imei number as `-a` parameter');
    			}
    			
    			if ($this->_superOrder->getEntityId()) {
    				$where = 'order_id = ' . $this->_superOrder->getEntityId();
    				if ($this->_packageId) {
    					$where .= ' AND package_id = ' . $this->_packageId;
    				}
    				$write->update(
    					'catalog_package',
    					array('imei' => $this->_additionalData),
    					$where
    				);
    			}
    			break;
    	}
    }
    
    protected function _setAxiProcessed()
    {
 	   	if ($this->_superOrder->getId()) {
    		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
    		$where = 'entity_id = ' . $this->_superOrder->getId();
    		$write->update(
    		    'superorder',
    			array('axi_processed' => 1),
    			$where
    		);
    	}    	
    }
    
    protected function _generateBan()
    {
    	return $this->_superOrder->getCustomer()->getBan() ?: rand(100000000,999999999);
    }
    
    protected function _generateMsisdn()
    {
    	return '316'.rand(10000000,99999999);
    }
    
    protected function _generateDaOrderNumber()
    {
    	return 'DA-UBUY-'.date('Ymd-His-'.rand(10,99));
    }
    
    protected function _getPackages()
    {
    	$superOrder = $this->_superOrder;
    	if ($this->_packages) {
    		return $this->_packages;
    	}
    	if (!$this->_packageId) {
    		$packages = $superOrder->getPackages();
    	} else {
    		$packages = [];
    		foreach ($superOrder->getPackages() as $package) {
    			if ($this->_packageId == $package->getPackageId()) {
    				$packages[] = $package;
    			}
    		}
    	}
    	$this->_packages = $packages;
    	return $this->_packages;
    }
    
    
    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
    	$usageString = 'Usage:
  -s <stub>                     Stub name / number
  -o <orderNumber>              Order number (leave empty to retrieve latest order)
  -p <packageNumber>            Package number
  -u <uri>                      Uri
  -a <additionalData>           Additional data (E.g. the imei to set)
  -help                         This help
    
Possible stubs:' . PHP_EOL;
    
    	$stubCount = count($this->_stubOptions);
    	for ($i = 0; $i < $stubCount; $i++) {
    		$usageString .= ($i + 1) . '. ' . $this->_stubOptions[$i] . PHP_EOL;
    	}
        
    	return $usageString;
    }
}
