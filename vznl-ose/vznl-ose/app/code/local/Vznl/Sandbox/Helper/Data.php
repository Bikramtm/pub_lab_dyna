<?php
/**
 * Class Vznl_Sandbox_Helper_Data
 */
class Vznl_Sandbox_Helper_Data extends Dyna_Sandbox_Helper_Data
{
    /**
     * @todo $supportedFilenames will be replaced by dynamically loaded data from xml
     * @todo replace dependencies on public var with helper
     */
    public $supportedFilenames;
    private $_infoExecution = null;
    private $_params = [];
    private $_shellFile = 'import_full.sh';
    private $_typesTemplate = [
        'mobile'    => '-m',
        'fixed'     => '-f',
        'campaign'  => '-c',
        'bundles'    => '-b'
    ];

    private $websites;

    CONST FILENAME_FIXED_KEYWORD = 'Fixed';
    CONST FILENAME_MOBILE_KEYWORD = 'Mobile';
    CONST FILENAME_CAMPAIGN_KEYWORD = 'Campaign';
    CONST FILENAME_BUNDLE_KEYWORD = 'Bundles';

    public $_keywordToLabel = [
        self::FILENAME_FIXED_KEYWORD => 'Fixed',
        self::FILENAME_MOBILE_KEYWORD => 'Mobile',
        self::FILENAME_CAMPAIGN_KEYWORD => 'Campaign',
        self::FILENAME_BUNDLE_KEYWORD => 'Bundles'
    ];

    public $_labelToKeyword = [
        'Fixed' => self::FILENAME_FIXED_KEYWORD,
        'Mobile' => self::FILENAME_MOBILE_KEYWORD,
        'Campaign' => self::FILENAME_CAMPAIGN_KEYWORD,
        'Bundle' => self::FILENAME_BUNDLE_KEYWORD
    ];

    public $_fileToStack = [
        self::FILENAME_FIXED_KEYWORD => [
            'Fixed_Multimapper_peal.csv',
            'Fixed_Package_Type.csv',
            'Fixed_PackageCreation.csv',
            'Fixed_Bundle_Products.csv',
            'Fixed_Gen_Products.csv',
            'Fixed_Int_Products.csv',
            'Fixed_Tel_Products.csv',
            'Fixed_TV_Products.csv',
            'Fixed_Promotions.csv',
            'Fixed_CategoryTree.csv',
            'Fixed_Categories.csv',
            'Fixed_Comp_Rules.csv',
            'Fixed_Sales_Rules.csv',
            'Fixed_Version.xml',
            'Fixed_ProductVersion.csv',
            'Fixed_ProductFamily.csv'
        ],
        self::FILENAME_MOBILE_KEYWORD => [
            'Mobile_Package_Type.csv',
            'Mobile_PackageCreation.csv',
            'Mobile_Accessory.csv',
            'Mobile_Addon.csv',
            'Mobile_Devices.csv',
            'Mobile_DeviceRC.csv',
            'Mobile_Promotions.csv',
            'Mobile_Subscription.csv',
            'Mobile_ProductFamily.csv',
            'Mobile_ProductVersion.csv',
            'Mobile_Version.xml',
            'Mobile_Sales_Rules.csv',
            'Mobile_Multimapper_BSL.csv',
            'Mobile_Mixmatch.csv',
            'Mobile_Comp_Rules.csv',
            'Mobile_CategoryTree.csv',
            'Mobile_Categories.csv'
        ],
        self::FILENAME_CAMPAIGN_KEYWORD => [
            'Campaigns.xml',
            'Campaign_Offers.xml',
            'Campaign_Version.xml'
        ],
        self::FILENAME_BUNDLE_KEYWORD => [
            'Bundle.xml'
        ]
    ];

    public function __construct()
    {
        $this->supportedFilenames = Mage::helper('vznl_import/importSupportedFiles')->getSupportedFiles();
    }

    /*
     * Perform import
     */
    public function performImport($information)
    {
        if(empty($information)){
            return;
        }
        $this->_infoExecution = $information ;
        if($this->validateParams()){
            $this->_executeImport();
        }
    }

    /**
     * Validate types params from execution row
     *
     * @return bool
     */
    private function validateParams(){
        if($types = $this->_infoExecution->getData('type')){
            $types = json_decode($types);
            $types = array_map('strtolower', $types);
            // @todo this should check against $supportedStacks in dyna_abstract and not typestemplate
            if(count(array_intersect($types, array_keys($this->_typesTemplate))) > 0){
                $this->_params = $types;
                return true;
            }
        }
        return false;
    }

    /**
     *
     */
    private function _executeImport()
    {
        Mage::register('sandbox_import_no_redis_logging', true);
        $finalCommand = 'nohup ' . $this->getFile() . ' -s false -i ' . $this->_infoExecution->getId(). ' -x false ' ;
        //add slack notification
        $finalCommand .= '-t ' . $this->getSlackNotification();
        foreach ($this->_typesTemplate as $type => $command){
            if(in_array($type,$this->_params)){
                $finalCommand .= $command . ' true ';
            }
        }

        //not waiting for the shell output
        $logFile = Mage::getBaseDir('base') . DS . 'var/log/' . $this->logfilePrefix . $this->logfileSuffix;
        $logFileError = Mage::getBaseDir('base') . DS . 'var/log/' . $this->logfilePrefix . '-error' . $this->logfileSuffix;
        $finalCommand .= '-l "' . $logFile . '" -e "' . $logFileError . '"';
        $finalCommand .= " >" . $logFile . " 2>" . $logFileError . " &";
        $importFile = Mage::getBaseDir('base') . DS .'shell/import_full.sh';
        chmod($importFile, 0755);
        @exec($finalCommand);
    }

    /**
     * Method to check whether to send slack notification on import
     * @return bool
     */
    public function getSlackNotification()
    {
         if(Mage::getStoreConfigFlag('sandbox_configuration/notification/slack'))
         {
             return 'true ';
         }
         return 'false ';
    }

    /**
     * Get path of file
     * @return bool|string
     */
    private function getFile()
    {
        $io = new Varien_Io_File();
        $shellDir = Mage::getBaseDir('base') .DS. 'shell';
        $filePath = $shellDir .DS. $this->_shellFile;
        return $io->fileExists($filePath) ? $filePath : false;
    }

    /**
     * Check running imports
     */
    public function checkRunningImports()
    {
        $messages = [];
        $periodTimestamp = date('Y-m-d h:i:s',strtotime('-12 hours'));
        /** @var Dyna_Sandbox_Model_ImportInformation $model */
        $model = Mage::getModel('dyna_sandbox/importInformation');
        $runningExecutions = $model->getCollection()
            ->addFieldToFilter('finished_at', array('null' => true))
            ->addFieldToFilter('started_at', array('gteq' => $periodTimestamp))
            ->addFieldToFilter('status', array('eq' => $model::STATUS_RUNNING));

        if(count($runningExecutions) > 0){
            $messages[] = "Following imports are already running:";
            foreach($runningExecutions as $importExection){
                $messages[] = 'A ' . implode(',',json_decode($importExection->getType())) . ' import [ID #' . $importExection->getId() . '] is already running since ' . $importExection->getStartedAt() . '. Current status: ' . $importExection->getStatus();
            }
        } else {
            $messages[] = "No imports are currently running";
        }

        return $messages;
    }

    public function exportProductsXml($path, $filename) {
        // set memory limit
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $this->connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        // required variables
        $productsXml = $path . $filename;
        $productsGz  = $productsXml.".tmp.gz";

        //filter only Mobile Package type products in the Dyna export.
        $package_type = Mage::getResourceModel('catalog/product')->getAttribute('package_type')->getSource()->getOptionId('Mobile');

        // get all products collection
        /** @var Mage_Catalog_Model_Resource_Product_Collection $productsCollection */
        $productsCollection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(array('entity_id', 'sku'))
            ->addAttributeToFilter('package_type', $package_type)
            ->setPageSize(100)
            ->load();

        // get no. of pages
        $totalRecords = $productsCollection->getSize();
        $totalPages   = ceil($totalRecords / 100);
        $productsXmlFile = fopen($productsXml, "a");
        fwrite($productsXmlFile, '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL .'<products>');

        // parse each page
        for ($currentPage = 1; $currentPage <= $totalPages; $currentPage++) {
            // get current products
            $productsCollection->setCurPage($currentPage);

            // parse products
            foreach ($productsCollection as $product) {
                // create output array
                $productData = array(
                    'attributes'       => $this->getProductAttributes($product),
                    'categories'       => $this->getProductCategories($product),
                    'allowedWith'      => $this->getProductAllowedWith($product),
                    'multi_mappers'    => $this->getProductMultiMappers($product),
                    'non_static_rules' => array(),
                );

                // save data to file
                $xml = Dyna_Sandbox_Helper_Array2XML::createXML('product', $productData);
                $xmlText = $xml->saveXML();
                fwrite($productsXmlFile, str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $xmlText));
            }
            $productsCollection->clear();
            usleep(100);
        }

        fwrite($productsXmlFile, '</products>');
        fclose($productsXmlFile);
        // compress file
        $fp = gzopen($productsGz, 'w9');
        gzwrite($fp, file_get_contents($productsXml));
        gzclose($fp);

        // delete xml file
        unlink($productsXml);

        return $productsGz;
    }

    /**
     * Get rules allowed with a specific product
     * @param $product
     * @return array
     */
    protected function getProductAllowedWith($product)
    {
        $allowData   = array();

        // get allowed with
        $allowSql = "
          SELECT `pmwi1`.`target_product_id`, `cpe1`.`sku`, `cpev1`.`value` 
          FROM `product_match_whitelist_index` AS pmwi1
          LEFT JOIN `catalog_product_entity` AS cpe1 ON `cpe1`.`entity_id` = `pmwi1`.`target_product_id`
          LEFT JOIN `catalog_product_entity_varchar` AS `cpev1` 
               ON (`cpev1`.`entity_id` = `pmwi1`.`target_product_id`) AND 
                  (`cpev1`.`attribute_id` = (SELECT attribute_id FROM `eav_attribute` ea LEFT JOIN `eav_entity_type` et ON ea.entity_type_id = et.entity_type_id  WHERE `ea`.`attribute_code` = 'name' AND et.entity_type_code = 'catalog_product')) 
            WHERE `pmwi1`.`source_product_id` = :entityId
        
          UNION 
          
          SELECT `pmwi2`.`source_product_id`, `cpe2`.`sku`, `cpev2`.`value` 
          FROM `product_match_whitelist_index` AS pmwi2
          LEFT JOIN `catalog_product_entity` AS cpe2 ON `cpe2`.`entity_id` = `pmwi2`.`source_product_id`
          LEFT JOIN `catalog_product_entity_varchar` AS `cpev2` 
               ON (`cpev2`.`entity_id` = `pmwi2`.`source_product_id`) AND 
                  (`cpev2`.`attribute_id` = (SELECT attribute_id FROM `eav_attribute` ea LEFT JOIN `eav_entity_type` et ON ea.entity_type_id = et.entity_type_id  WHERE `ea`.`attribute_code` = 'name' AND et.entity_type_code = 'catalog_product'))
            WHERE `pmwi2`.`target_product_id` = :entityId";


        $allowResults = $this->connection->fetchAll($allowSql, array(':entityId' => $product->getEntityId()));

        // get defaulted/obligated
        $otherSql = "
        SELECT `pmdi`.`target_product_id`, `pmdi`.`obligated`, `cpe1`.`sku`, `cpev1`.`value` 
        FROM `product_match_defaulted_index` AS pmdi
        LEFT JOIN `catalog_product_entity` AS cpe1 ON `cpe1`.`entity_id` = `pmdi`.`target_product_id`
        LEFT JOIN 
          `catalog_product_entity_varchar` AS `cpev1` 
               ON (`cpev1`.`entity_id` = `pmdi`.`target_product_id`) AND 
                  (`cpev1`.`attribute_id` = (SELECT attribute_id FROM `eav_attribute` ea LEFT JOIN `eav_entity_type` et ON ea.entity_type_id = et.entity_type_id  WHERE `ea`.`attribute_code` = 'name' AND et.entity_type_code = 'catalog_product'))
              WHERE `pmdi`.`source_product_id` = :entityId";
        $otherResults = $this->connection->fetchAll($otherSql, array(':entityId' => $product->getEntityId()));

        foreach ($allowResults as $allowResult) {
            $mixMatchSql     = "SELECT `price`, `subscriber_segment` FROM `dyna_mixmatch_flat` WHERE `source_sku` = :source AND `target_sku` = :target";
            $mixMatchData    = [];
            $mixMatchResults = $this->connection->fetchAll($mixMatchSql, array(':source' => $product->getSku(), ':target' => $allowResult['sku']));

            foreach ($mixMatchResults as $matchResult) {
                $mixMatchData['mix_match_price'][] = array(
                    '@value'      => "".$matchResult['price'],
                    '@attributes' => array('type' => $matchResult['subscriber_segment'])
                );
            }

            $allowData['product'][] = array(
                '@attributes'      => array('type' => 'allowed'),
                'name'             => $allowResult['value'],
                'sku'              => $allowResult['sku'],
                'mix_match_prices' => $mixMatchData,
            );
        }


        foreach ($otherResults as $otherResult) {
            $mixMatchSql     = "SELECT `price`, `subscriber_segment` FROM `dyna_mixmatch_flat` WHERE `source_sku` = :source AND `target_sku` = :target";
            $mixMatchData    = array();
            $mixMatchResults = $this->connection->fetchAll($mixMatchSql, array(':source' => $product->getSku(), ':target' => $otherResult['sku']));

            foreach ($mixMatchResults as $matchResult) {
                $mixMatchData['mix_match_price'][] = array(
                    '@value'      => "".$matchResult['price'],
                    '@attributes' => ($matchResult['subscriber_segment']) ? array('type' => $matchResult['subscriber_segment']) : []
                );
            }

            $allowData['product'][] = array(
                '@attributes'      => array('type' => (array_key_exists('obligated', $otherResult) && $otherResult['obligated'] == '1') ? 'obligated' : 'defaulted'),
                'name'             => $otherResult['value'],
                'sku'              => $otherResult['sku'],
                'mix_match_prices' => $mixMatchData,
            );
        }

        return $allowData;
    }

    /**
     * Get attributes for a specific product
     * @param Dyna_Catalog_Model_Product $product
     * @return array
     */
    protected function getProductAttributes($product)
    {
        $productAttributes = array();
        if (is_null($this->websites)) {
            $this->websites = Mage::app()->getWebsites();
        }
        $productWebsites = $product->getWebsiteIds();
        foreach ($this->websites as $website) {
            foreach ($productWebsites as $i => $id) {
                if ($website->getId() == $id) {
                    $productWebsites[$i] = strtolower($website->getName());
                }
            }
        }

        $productObj = Mage::getModel('catalog/product')->load( $product->getId());

        /** @var Mage_Eav_Model_Attribute $attribute */
        foreach ($product->getAttributes() as $attribute) {
            if (in_array($attribute->getAttributeCode(), array('category_ids', 'prijs_thuiskopieheffing_bedrag'))) {
                continue;
            }

            $attributeValue =  Mage::getResourceModel('catalog/product')->getAttributeRawValue(
                $product->getEntityId(),
                $attribute->getAttributeCode(),
                Mage::app()->getStore()->getStoreId()
            );
            $attributeValue = $this->_reValidateAttributeValue($attribute, $attributeValue);

            if (!$attribute->getIsGlobal()) {
                $productAttributes[$attribute->getAttributeCode()] = [
                    '@attributes' => array('channel' => implode(',', $productWebsites)),
                    '@value'      => $attributeValue
                ];
            } else {
                $productAttributes[trim($attribute->getAttributeCode())] = $attributeValue;
            }
        }

        return $productAttributes;
    }
    
    protected function _reValidateAttributeValue($attribute, $attributeValue)
    {
    	switch ($attribute->getAttributeCode()) {
    		case 'attribute_set_id':
    			$model = Mage::getModel("eav/entity_attribute_set")->load($attributeValue);
    			$attributeValue  = $model->getAttributeSetName();
    			break;
    		case 'entity_type_id':
    			$model = mage::getModel("eav/entity_type")->load($attributeValue);
    			$attributeValue = $model->getEntityTypeCode();
    			break;
    		case 'package_type':
    		case 'package_subtype':
    		case 'product_visibility':
    			$attributeValue = $this->_decodeAttributeValueV($attributeValue);
    			break;
    		case 'initial_selectable':
    			$attributeValue = $attributeValue == 1 ? 'Yes' : 'No';
    			break;
    		case 'status':
    		case 'visibility':
    			$options = Mage::getModel("catalog/product_" . $attribute->getAttributeCode())->getOptionArray();
    			$attributeValue  = $options[$attributeValue];
    			break;
    		case 'tax_class_id':
    			$attributeValue = Mage::getModel("tax/class")->load($attributeValue)->getClassName();
    			break;
    	}
    	
    	return $attributeValue;
    }
    
    protected function _decodeAttributeValueV($attributeValue) 
    {
    	foreach (explode(',', $attributeValue) as $attrVal) {
    		$arrValue = json_decode(Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, $attrVal));
    		$values[] = reset($arrValue);
    	}
    	$attributeValue = implode(',', $values);
    	return $attributeValue;
    }
}
