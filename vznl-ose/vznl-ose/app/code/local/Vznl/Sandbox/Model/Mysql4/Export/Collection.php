<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Model_Mysql4_Export_Collection
 */
class Vznl_Sandbox_Model_Mysql4_Export_Collection extends Omnius_Sandbox_Model_Mysql4_Export_Collection
{
    /**
     * Constructor override
     */
    public function _construct()
    {
        $this->_init('sandbox/export');
    }
}
