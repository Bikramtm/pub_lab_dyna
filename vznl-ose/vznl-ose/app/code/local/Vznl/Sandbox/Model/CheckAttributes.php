<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

class Vznl_Sandbox_Model_CheckAttributes extends Omnius_Sandbox_Model_CheckAttributes
{
    private $_config = null;

    /**
     * @return Omnius_Sandbox_Model_Config
     */
    private function _getConfig()
    {
        if ( ! $this->_config) {
            return $this->_config = Mage::getSingleton('sandbox/config');
        }
        return $this->_config;
    }

    /**
     * Performs various condition checks on the attributes
     * @param $observer
     * @throws Mage_Adminhtml_Exception
     */
    public function checkAttributes($observer)
    {
        // if a replica is active/configured
        if (Mage::registry('bypass_replication_on_import')) {
            // avoid replication on import process
            return false;
        }
        $attributesMirroring = true;

        /** @var Omnius_Sandbox_Model_Config $replicas */
        $replicas = Mage::getSingleton('sandbox/config')->getReplicas();
        /** @var Dyna_Catalog_Model_Product $product */
        $product = $observer->getProduct();
        /** @var Omnius_Sandbox_Helper_Remote $remote */
        $remote = Mage::helper('sandbox/remote');
        /** @var Varien_Db_Adapter_Pdo_Mysql $masterAdapter */
        $masterAdapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        /** @var Omnius_Sandbox_Model_Demux $demux */
        $demux = Mage::getSingleton('sandbox/demux');
        $slaveAdapters = $demux->getAdapters();
        $disabledSlaveAdapters = $demux->getDisabledAdapters();

        //Get attributes as array for current product
        $masterAttributes['attributes'] = [];
        $masterAttributes['attributeSets'] = [];
        $resultSet = $masterAdapter->fetchAll("select * from eav_attribute where entity_type_id='4' order by attribute_id asc");
        foreach ($resultSet as $result) {
            $masterAttributes['attributes'][$result['attribute_code']] = $result['attribute_id'];
        }
        $resultSet = $masterAdapter->fetchAll("select * from eav_attribute_set where entity_type_id='4' order by attribute_set_id asc");
        foreach ($resultSet as $result) {
            $masterAttributes['attributeSets'][$result['attribute_set_name']] = $result['attribute_set_id'];
        }

        $masterAdapterConfig = $masterAdapter->getConfig();
        $increment = (int) $masterAdapter
            ->fetchOne("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'catalog_product_entity' AND table_schema = ':name';",
                array(
                    ':name' => $masterAdapterConfig['dbname']
                )
            );

        $replicas = $this->_getConfig()->getReplicas(true);
        foreach ($disabledSlaveAdapters as $name => $adapter) {
            if (isset($replicas[$name]['can_sync_ai']) && $replicas[$name]['can_sync_ai']) {
                $this->checkAutoIncrement($product, $increment, $adapter, $name);
            }
        }

        foreach ($slaveAdapters as $replicaName => $slaveAdapter) {
            $this->checkAutoIncrement($product, $increment, $slaveAdapter, $replicaName);

            //Let's check attributes mirroring
            $replicaAttributes = $remote->getSlaveAttributes($slaveAdapter);
            if (
                array_diff_assoc($masterAttributes['attributes'], $replicaAttributes['attributes'])
                || array_diff_assoc($masterAttributes['attributeSets'], $replicaAttributes['attributeSets'])
            ) {
                $attributesMirroring = false;
                break;
            }
        }

        //Attributes id or attribute set ids differ from replicas. Saving cancelled
        if (!$attributesMirroring) {
            $product->cancelSave(Mage::helper('catalog/product')->__("Product cannot be saved due to replication attributes ids differences"));
        }
    }
}
