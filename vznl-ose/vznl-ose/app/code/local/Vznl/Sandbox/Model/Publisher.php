<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Model_Publisher
 */
class Vznl_Sandbox_Model_Publisher extends Dyna_Sandbox_Model_Publisher
{
    protected $replicaPHPPath;

    const VARNISH_PORT = '6081';

    public function publish(Omnius_Sandbox_Model_Release $release)
    {
        /**
         * Check if release has importCheck and the last import has status success (if at least one import was done)
         */
        if($release->getImportCheck())
        {
            /** @var Dyna_Sandbox_Model_ImportInformation $model */
            $model = Mage::getModel('dyna_sandbox/importInformation');
            $lastImport = $model->getCollection()->getLastItem();
            if($lastImport->getId() && $lastImport->getStatus() !== $model::STATUS_SUCCESS){
                return $release->addMessage('Exists an import with status '.$lastImport->getStatus(), $release::MESSAGE_ERROR);
            }
        }

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        if ($this->_lockRelease($release->getId())) {
            $release
                ->setStatus(Omnius_Sandbox_Model_Release::STATUS_RUNNING) //in order to not load the release again
                ->addMessage('Release locked')
                ->save();
            /** @var Omnius_Sandbox_Model_Replica $replica */
            $replica = Mage::getModel('sandbox/replica')->load($release->getReplica());
            if ( ! $replica || ($replica && !$replica->getId())) {
                return $release->addMessage(sprintf('Release references a non-existing replica (ID %s)', $replica->getId()), $release::MESSAGE_ERROR);
            }

            $release
                ->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                ->save();
            try {
                /**
                 * Run indexers before dumping tables
                 * @var Dyna_Warmer_Model_Warmer_Indexing $indexersWarmer
                 */
                if ($indexersWarmer = Mage::getModel('warmer/warmer_indexing')) {
                    $release->addMessage('Starting indexers')->save();
                    Mage::dispatchEvent('warmer_cache_warmCache', ['model' => $indexersWarmer, 'verbose' => false, 'includeHeavy' => $release->getIsHeavy(), 'release' => $release]);
                    $release->addMessage('Indexers finished')->save();
                }

                /**
                 * Migrate tables to the replica
                 */
                $release
                    ->addMessage('Database tables migration started')
                    ->save();
                if ($replica->isRemote()) {
                    $release = $this->externalPublish($release, $replica);
                } else {
                    $publishCommand = $this->_getDbHelper()
                        ->createDumpCommand($this->_getDatabaseConfig(), $release->getTables(), $replica->getMysqlConfig());
                    $release
                        ->addMessage(sprintf('Executed command "%s"', $this->censorCommand($publishCommand)), $release::MESSAGE_INFO);

                    $this->executeAdditionalCommandsBeforeDBDump($release, $replica);

                    $this->_getShell()->exec($publishCommand);

                    $this->executeAdditionalCommandsAfterDBDump($release, $replica);

                    $release
                        ->addMessage('Database tables migration ended')
                        ->save();
                }

                /**
                 * Migrate media files to the replica
                 */
                if ($release->getExportMedia()) {
                    if ($replica->isRemote()) {
                        $release
                            ->addMessage('Media files migration started')
                            ->save();
                        $release = $this->externalMediaSync($release, $replica);
                        $release->addMessage('Media files migration ended');
                    } else {
                        $from = DS . trim(Mage::getBaseDir('media'), DS) . DS . 'catalog';
                        $to = DS . trim($replica->getEnvironmentPath(), DS) . DS . join(DS, array('media', 'catalog'));
                        $release
                            ->addMessage('Media catalog files migration started')
                            ->save();
                        $this->_getFileMapper()->mapMedia($from, $to, null, false);
                        $release->addMessage('Media catalog files migration ended');

                        $from = DS . trim(Mage::getBaseDir('media'), DS) . DS . 'bundles';
                        $to = DS . trim($replica->getEnvironmentPath(), DS) . DS . join(DS, array('media', 'bundles'));
                        $release
                            ->addMessage('Media bundles files migration started')
                            ->save();
                        $this->_getFileMapper()->mapMedia($from, $to, null, false);
                        $release->addMessage('Media bundles files migration ended');

                        $from = DS . trim(Mage::getBaseDir('media'), DS) . DS . 'product_image';
                        $to = DS . trim($replica->getEnvironmentPath(), DS) . DS . join(DS, array('media', 'product_image'));
                        $release
                            ->addMessage('Media product_image files migration started')
                            ->save();
                        $this->_getFileMapper()->mapMedia($from, $to, null, false);
                        $release->addMessage('Media product_image files migration ended');
                    }
                } else {
                    $release
                        ->addMessage('Media files migration disabled for this release. Skipping.')
                        ->save();
                }

                /**
                 * Clear Varnish
                 */
                if ($servers = $replica->getVarnishServers()) {
                    foreach ($servers as $server) {
                        //$cmd = sprintf('curl --noproxy "*" -X BAN %s', $server);
                        $cmd = sprintf('curl --noproxy "*" -X BAN %s:' . Vznl_Sandbox_Model_Publisher::VARNISH_PORT, $server);
                        $release->addMessage(sprintf('Clearing Varnish server (%s) with command "%s"', $server, $cmd), Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);
                        if ($replica->isRemote()) {
                            $output = $this->_getSftp()->getConnection()->exec($cmd);
                        } else {
                            $output = $this->_getShell()->exec($cmd);
                        }
                        $output = strtolower($output);
                        if (strpos($output, 'successfully') !== false || strpos($output, '200') !== false) {
                            $release->addMessage(sprintf('Varnish cache successfully cleared for server %s !', $server), Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);
                        } else {
                            Mage::log('Varnish clearing command output:', null, 'external_release.log', true);
                            Mage::log($output, null, 'external_release.log', true);
                            $release->addMessage('Could not connect to Varnish Server. Please check external_release.log to see the error', Omnius_Sandbox_Model_Release::MESSAGE_ERROR);
                        }
                    }
                }
                $release->save();

                /**
                 * Clear Omnius cache as well
                 */
                $clearCacheCmd = sprintf(
                      "%s %s",
                      $replica->getPhpPath(),
                      join(DS, array(rtrim($replica->getEnvironmentPath(), DS), 'shell', 'clearCache.php'))
                );
                $release->addMessage('Clearing Omnius cache', Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);

                if ($replica->isRemote()) {
                    $this->_getSftp()->getConnection()->exec($clearCacheCmd);
                } else {
                    $this->_getShell()->exec($clearCacheCmd);
                }
                $release->addMessage('Successfully cleared Omnius cache', Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);
                $release->save();

              // release update should only be executed on the master environment
              if ($replica->isRemote()) {
                  $this->replicaPHPPath = $replica->getPhpPath();
                  Mage::log('Starting update release at: '.date("Y-m-d H:i:s"), null, 'external_release.log', true);
                  $output = $this->_getShell()->exec($this->_getReleaseUpdateCmd($release->getId(), $replica->getId()));
                  Mage::log(var_export($output, true), null, 'external_release.log', true);
              }

                      /**
                       * Assure the connection is close
                       */
                if ($replica->isRemote() && $this->_getSftp()->getConnection()) {
                    $this->_getSftp()->close();
                    $release
                        ->addMessage('SFTP connection closed')
                        ->save();
                }

                //remove releases will be finished by a shell script (see "/shell/update_release_status.php")
                if ( ! $replica->isRemote()) {
                    /**
                     * Return release after updating finish timestamp and status
                     */
                    return $release
                        ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                        ->setStatus(Omnius_Sandbox_Model_Release::STATUS_SUCCESS)
                        ->addMessage('Release successfully published', $release::MESSAGE_SUCCESS);
                }

                return $release;

            } catch (Exception $e) {
                return $release
                    ->addMessage($e->getMessage(), $release::MESSAGE_ERROR)
                    ->setStatus(Omnius_Sandbox_Model_Release::STATUS_ERROR)
                    ->incrementTries()
                    ->save();
            }
        }
        return $release->addMessage(
            sprintf(
                'Could not acquire lock for release (ID: %s). Already running or successfully finished.',
                $release->getId()
            ), $release::MESSAGE_ERROR
        );
    }

    private function iterateDirectory($iterator, $localBase, $remoteBase)
    {
        foreach ($iterator as $file) { /** @var SplFileInfo $file */
            $realPath = $file->getRealPath();
            if (false !== strpos($realPath, sprintf('%scache%s', DS, DS))
                || 'cache' === substr($realPath, strlen($realPath) - 5, 5)
            ) { //skip cache dir
                continue;
            }
            $dirPath = str_replace($localBase, $remoteBase, $file->getRealPath());
            if (!$file->isDir()) {
                $dirPath = dirname($dirPath);
            }
            if ( ! $this->_getSftp()->cd($dirPath)) {
                $this->_getSftp()->getConnection()->exec(sprintf('cd / && mkdir -p %s', DS . trim($dirPath, DS)));
                if ( ! $this->_getSftp()->cd($dirPath)) {
                    throw new Exception(sprintf('Could not create path "%s" on remote system', $dirPath));
                }
            }
            if ($file->isFile()) {
                $this->_getSftp()->write($file->getFilename(), $file->getRealPath(), NET_SFTP_LOCAL_FILE);
            }
        }
    }

    /**
     * @param Omnius_Sandbox_Model_Release $release
     * @param Omnius_Sandbox_Model_Replica $replica
     * @throws Exception
     * @return Omnius_Sandbox_Model_Release
     */
    public function externalMediaSync(Omnius_Sandbox_Model_Release $release, Omnius_Sandbox_Model_Replica $replica)
    {
        $localBase = rtrim(Mage::getBaseDir(), DS);
        $localCatalogPath = $localBase . DS . 'media' . DS . 'catalog';
        $localBundlesPath = $localBase . DS . 'media' . DS . 'bundles';
        $localProductImagePath = $localBase . DS . 'media' . DS . 'product_image';
        $remoteBase = rtrim($replica->getEnvironmentPath(), DS);

        if(is_dir($localCatalogPath)){
            $iteratorCatalog = new RecursiveDirectoryIterator($localCatalogPath, FilesystemIterator::SKIP_DOTS);
            $iteratorCatalog = new RecursiveIteratorIterator($iteratorCatalog, RecursiveIteratorIterator::SELF_FIRST);
        }
        if(is_dir($localBundlesPath)){
            $iteratorBundles = new RecursiveDirectoryIterator($localBundlesPath, FilesystemIterator::SKIP_DOTS);
            $iteratorBundles = new RecursiveIteratorIterator($iteratorBundles, RecursiveIteratorIterator::SELF_FIRST);
        }
        if(is_dir($localProductImagePath)){
            $iteratorProductImage = new RecursiveDirectoryIterator($localProductImagePath, FilesystemIterator::SKIP_DOTS);
            $iteratorProductImage = new RecursiveIteratorIterator($iteratorProductImage, RecursiveIteratorIterator::SELF_FIRST);
        }


        $this->_getSftp()->open($replica->getSshConfig());
        $release
            ->addMessage('SFTP connection made')
            ->save();

        if ( ! $this->_getSftp()->cd($remoteBase)) {
            throw new Exception(sprintf('Remote environment path (%s) not found', $remoteBase));
        }

        $release
            ->addMessage(sprintf('Started moving media files to %s', $remoteBase))
            ->save();

        if($this->isFileExists($localCatalogPath) && is_dir($localCatalogPath)) {
            $this->iterateDirectory($iteratorCatalog, $localBase, $remoteBase);
        }
        if($this->isFileExists($localBundlesPath) && is_dir($localBundlesPath)) {
            $this->iterateDirectory($iteratorBundles, $localBase, $remoteBase);
        }
        if($this->isFileExists($localProductImagePath) && is_dir($localProductImagePath)) {
            $this->iterateDirectory($iteratorProductImage, $localBase, $remoteBase);
        }

        return $release;
    }

    public function isFileExists($path) {
        return file_exists($path);
    }
}
