<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Model_Mysql4_Replica
 */
class Vznl_Sandbox_Model_Mysql4_Replica extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Constructor override
     */
    protected function _construct()
    {
        $this->_init('sandbox/replica', 'id');
    }

    /**
     * Load an object
     *
     * @param Mage_Core_Model_Abstract $object
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @return Mage_Core_Model_Resource_Db_Abstract
     */
    public function load(Mage_Core_Model_Abstract $object, $value, $field = null)
    {
        if (is_null($field)) {
            if (!is_null($value) && is_numeric($value)) {
                $field = $this->getIdFieldName();
            } else {
                $field = 'name';
            }

        }

        $read = $this->_getReadAdapter();
        if ($read && !is_null($value)) {
            $select = $this->_getLoadSelect($field, $value, $object);
            $data = $read->fetchRow($select);

            if ($data) {
                $object->setData($data);
            }
        }

        $this->unserializeFields($object);
        $this->_afterLoad($object);

        return $this;
    }
}
