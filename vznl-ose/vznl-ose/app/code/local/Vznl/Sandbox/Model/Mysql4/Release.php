<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mysql4_Release
 */
class Vznl_Sandbox_Model_Mysql4_Release extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Constructor override
     */
    protected function _construct()
    {
        $this->_init('sandbox/release', 'id');
    }
}
