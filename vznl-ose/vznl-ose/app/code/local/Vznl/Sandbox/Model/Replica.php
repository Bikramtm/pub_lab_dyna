<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Sandbox
 */
class Vznl_Sandbox_Model_Replica extends Omnius_Sandbox_Model_Replica
{
    /**
     * Constructor override
     */
    protected function _construct()
    {
        $this->_init("sandbox/replica");
    }
    /**
     * @return array
     */
    public function getVarnishServers()
    {
        return array_filter(array_map('trim', explode(',', $this->getData('varnish_servers'))), function($server) {
            $r = '/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(:[0-9]+)?$/';
            return preg_match($r, $server);
        });
    }

    /**
     * Override the save method to make sure created_at is not affected by environment timezone settings also bypass xml save
     * @return $this
     * @throws Exception
     */
    public function save()
    {
        if (!$this->hasData('created_at')) {
            $this->setCreatedAt(NULL);
        }

        return Mage_Core_Model_Abstract::save();
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    public function delete()
    {
       return Mage_Core_Model_Abstract::delete();
    }

    /**
     * @return array
     */
    public function getMysqlConfig()
    {
        return array(
            'host' => $this->getMysqlHost(),
            'username' => $this->getMysqlUser(),
            'password' => $this->getMysqlPassword(),
            'dbname' => $this->getMysqlDatabase(),
            'port' => $this->hasMysqlPort() ? (int) $this->getMysqlPort() : 3306,
        );
    }

    /**
     * @return array
     */
    public function getSshConfig()
    {
        return array_filter(array(
            'host' => $this->getData('ssh_host'),
            'username' => $this->getData('ssh_user'),
            'password' => base64_decode($this->getData('ssh_pass')),
            'port' => $this->getData('ssh_port'),
        ));
    }

    /**
     * @return string
     */
    public function getMysqlPassword(): string
    {
       return base64_decode(parent::getMysqlPassword());
    }

    /**
     * @return string
     */
    public function getSshPass(): string
    {
        return base64_decode(parent::getSshPass());
    }

}