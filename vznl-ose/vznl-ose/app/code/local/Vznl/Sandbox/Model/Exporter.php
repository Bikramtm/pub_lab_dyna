<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Model_Exporter
 */
class Vznl_Sandbox_Model_Exporter extends Omnius_Sandbox_Model_Exporter
{
    /** @var Vznl_Sandbox_Model_CatalogExporter */
    protected $_catalogExporter;

    /**
     * @param Vznl_Sandbox_Model_Export $export
     * @return Vznl_Sandbox_Model_Export
     */
    public function export(Vznl_Sandbox_Model_Export $export)
    {
        if ($this->_lockExport($export->getId())) {
            $export
                ->setStatus(Omnius_Sandbox_Model_Export::STATUS_RUNNING) //in order to not load the release again
                ->addMessage('Export locked')
                ->save();

            try {
                /** @var Vznl_Sandbox_Model_ExportStrategy_ExportStrategy $strategy */
                if ( ! ($strategy = Mage::getSingleton(sprintf('vznl_sandbox/exportStrategy_%sXmlCatalog', $export->getExportType())))) {
                    throw new Exception('Invalid strategy set on export.');
                }

                $export->addMessage(sprintf('Starting %s export', $export->getExportType()));

                $export
                    ->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                    ->save();

                $export->addMessage(sprintf('Cd to location %s', $export->getExportLocation()));

                if (Dyna_Sandbox_Model_Export::PRODUCTS_TYPE == $export->getExportType()) {
                    $remoteFilePath = $this->_addTimestamp($export->getExportFilename());
                    $command = sprintf(
                        'nohup nice php %s --exportProductsXML --exportId ' . $export->getId() . ' --sftp ' . base64_encode(serialize($export->getSftpArgs())) . ' --path ' . $export->getExportLocation() . DS . ' --filename ' . $remoteFilePath . ' > /dev/null 2>&1 &',
                        'shell/sandbox_ops.php'
                    );

                    @exec($command, $output, $exitCode);
                    $export->addMessage('Writing to export file');
                    $export->save();
                    return $export;

                } else if (Vznl_Sandbox_Model_Export::DYNA_TYPE == $export->getExportType()) {
                    /**
                     * The export for dyna catalog must generate two files, one for belcompany
                     * and one for the retail
                     */
                    $stores = Mage::app()->getStores();
                    $storeIds = [];
                    foreach ($stores as $storeItem) {
                        if ($storeItem->getCode() == 'default') {
                            $storeIds = array_filter(array_map(function(Mage_Core_Model_Store $store){
                                if (in_array($store->getCode(), array('default', 'belcompany'))) {
                                    return $store->getId();
                                }
                                return false;
                            }, Mage::app()->getStores()));
                        } elseif ($storeItem->getCode() == 'retail') {
                            $storeIds = array_filter(array_map(function(Mage_Core_Model_Store $store){
                                if (in_array($store->getCode(), array('retail', 'belcompany'))) {
                                    return $store->getId();
                                }
                                return false;
                            }, $stores));
                        }
                    }

                    //Do not include Fixed products in the Dyna export.
                    $packageType = Mage::getResourceModel('catalog/product')->getAttribute('package_type')->getSource()->getOptionId('INT_TV_FIXTEL');

                    $appEmulation = Mage::getSingleton('core/app_emulation');
                    foreach ($storeIds as $storeId) {
                        $exportXml = null;
                        $remoteFilePath = $this->_addTimestamp($export->getExportFilename());
                        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
                        $collection = Mage::getModel('catalog/product')
                            ->getCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('is_deleted', array('neq' => 1))
                            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
                            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                            ->addAttributeToFilter('package_type', array('nlike' => "%{$packageType}%"))
                            ->addStoreFilter();

                        $exportXml = $this->getExporter()->export($collection, $strategy, $exportXml);
                        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

                        // establish sftp connection
                        $sftpConnection = $this->getSftp();
                        // and then open it
                        $sftpConnection->open($export->getSftpArgs());
                        $export->addMessage('SFTP connection made');
                        // cd to replica's env path
                        if ($sftpConnection->cd($export->getExportLocation())) {
                            $export->addMessage(sprintf('Copying export file to %s', $export->getExportPath()));
                            $succeeded = $this->getSftp()->write(
                                $remoteFilePath,
                                str_replace(
                                    '<?xml version="1.0"?>',
                                    '<?xml version="1.0" encoding="UTF-8"?>',
                                    $exportXml->asPrettyXML()
                                )
                            );
                            if (!$succeeded) {
                                $this->getSftp()->close();
                                throw new Exception(sprintf('Could not copy export file ("%s").', $remoteFilePath));
                            } else {
                                $export->addMessage('Upload succeeded');
                            }
                        } else {
                            $sftpConnection->close();
                            throw new Exception(sprintf('Could not cd to export path ("%s")', $export->getExportLocation()));
                        }
                    }

                    $this->getSftp()->close();
                    $export->addMessage('Closed SFTP connection');

                    return $export
                        ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                        ->setStatus(Omnius_Sandbox_Model_Export::STATUS_SUCCESS)
                        ->addMessage(sprintf('Export successfully made at %s', $export->getExportPath()), Omnius_Sandbox_Model_Export::MESSAGE_SUCCESS);
                } else { // AXI export
                    $remoteFilePath = $this->_addTimestamp($export->getExportFilename());

                    $appEmulation = Mage::getSingleton('core/app_emulation');
                    $exportXml = null;
                    foreach ($this->_getStoreIds($export) as $storeId) {
                        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

                        $collection = Mage::getModel('catalog/product')
                            ->getCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('is_deleted', array('neq' => 1))
                            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
                            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                            ->addStoreFilter();

                        $exportXml = $this->getExporter()->export($collection, $strategy, $exportXml);

                        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
                    }

                    /* ---IMPORTANT!--- */
                    // @todo: upgrade somehow phpseclib to latest version from github main repository
                    // even though connect20/Lib_Phpseclib (1.9.3.6 currently) can be upgraded to 1.9.3.9 (as latest Magento_Core_Modules is also 1.9.3.9)
                    // many warnings will still be thrown as the transition from PHP 4 to PHP 7 has not been done (as it is on
                    // https://github.com/phpseclib/phpseclib)

                    // this is why we'll report just the errors, for now
                    error_reporting(E_ERROR);
                    /* ------ */

                    // establish sftp connection
                    $sftpConnection = $this->getSftp();
                    // and then open it
                    $sftpConnection->open($export->getSftpArgs());
                    $export->addMessage('SFTP connection made');
                    // cd to replica's env path
                    if ($sftpConnection->cd($export->getExportLocation())) {
                        $export->addMessage(sprintf('Copying export file to %s', $export->getExportPath()));
                        $succeeded = $sftpConnection->write(
                            $remoteFilePath,
                            str_replace(
                                '<?xml version="1.0"?>',
                                '<?xml version="1.0" encoding="UTF-8"?>',
                                $exportXml->asPrettyXML()
                            )
                        );

                        if ( ! $succeeded) {
                            $sftpConnection->close();
                            throw new Exception(sprintf('Could not copy export file ("%s").', $remoteFilePath));
                        } else {
                            $export->addMessage('Upload succeeded');
                        }

                        $sftpConnection->close();
                        $export->addMessage('Closed SFTP connection');
                    } else {
                        $sftpConnection->close();
                        throw new Exception(sprintf('Could not cd to export path ("%s")', $export->getExportLocation()));
                    }

                    return $export
                        ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                        ->setStatus(Omnius_Sandbox_Model_Export::STATUS_SUCCESS)
                        ->addMessage(sprintf('Export successfully made at %s', $export->getExportPath()), Omnius_Sandbox_Model_Export::MESSAGE_SUCCESS);
                }
            } catch (Exception $e) {
                return $export
                    ->addMessage($e->getMessage(), Omnius_Sandbox_Model_Export::MESSAGE_ERROR)
                    ->setStatus(Omnius_Sandbox_Model_Release::STATUS_ERROR)
                    ->incrementTries()
                    ->save();
            }
        }
        return $export->addMessage(sprintf(
            'Could not acquire lock for export (ID: %s). Already running or successfully finished.',
            $export->getId()
        ), Omnius_Sandbox_Model_Export::MESSAGE_ERROR);
    }

    /**
     * @return Vznl_Sandbox_Model_CatalogExporter
     */
    protected function getExporter()
    {
        if ( ! $this->_catalogExporter) {
            return $this->_catalogExporter = Mage::getSingleton('vznl_sandbox/catalogExporter');
        }
        return $this->_catalogExporter;
    }
}
