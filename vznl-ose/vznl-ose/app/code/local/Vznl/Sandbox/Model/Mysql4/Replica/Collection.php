<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Model_Mysql4_Replica_Collection
 */
class Vznl_Sandbox_Model_Mysql4_Replica_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Constructor override
     */
    public function _construct()
    {
        $this->_init('sandbox/replica');
    }
}
