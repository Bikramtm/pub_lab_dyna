<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Import
 */
class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Import extends Dyna_Sandbox_Block_Adminhtml_Release_Import
{
    protected $_objectId = 'id';
    protected $_formScripts = array();
    protected $_formInitScripts = array();
    protected $_mode = 'edit';
    protected $_blockGroup = 'import';

    protected $_controller = 'adminhtml_release';

    protected function _construct()
    {
        get_parent_class(parent::class)::_construct();

        if (!$this->hasData('template')) {
            $this->setTemplate('sandbox/import.phtml');
        }

        $buttonClass = 'save';
        if(count(Mage::helper("vznl_sandbox")->checkRunningImports()) > 1){
            $buttonClass .= ' disabled';
        }
        $this->_addButton('save', array(
            'label' => Mage::helper('multimapper')->__('Import'),
            'onclick' => 'importForm.submit();',
            'class' => $buttonClass,

        ), 1, 1,'footer');
    }

    /**
     * Preparing global layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $this->setChild('form', $this->getLayout()->createBlock('vznl_sandbox/adminhtml_importInformation_import_form'));

        return get_parent_class(parent::class)::_prepareLayout();
    }
}