<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_Replica_Grid
 */
class Vznl_Sandbox_Block_Adminhtml_Replica_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $sandboxList;

    /**
     * Overrride the constructor to customize the grid
     * Omnius_Sandbox_Block_Adminhtml_Replica_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('replicaGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);

        $this->sandboxList = Mage::getSingleton('sandbox/config')->getSandboxConnections();

        //Loading sandbox.xml to get replicas status
    }

    /**
     * Initialize collection
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('sandbox/replica_collection');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Override prepareColumns method to show custom columns
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header' => Mage::helper('sandbox')->__('Name'),
            'index' => 'name',
        ));

        $this->addColumn('environment_path', array(
            'header' => Mage::helper('sandbox')->__('Environment path'),
            'index' => 'environment_path',
        ));

        $this->addColumn('is_remote', array(
            'header' => Mage::helper('sandbox')->__('Location'),
            'index' => 'ssh_host',
            'frame_callback' => array($this, 'isRemote'),
            'filter' => false,
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('sandbox')->__('Status'),
            'index' => 'name',
            'frame_callback' => array($this, 'isEnabled'),
            'filter' => false,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    /**
     * Get edit url for a record
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * Checks if a replica is enabled or not
     * @param $data
     * @param Omnius_Sandbox_Model_Replica $replica
     * @return string
     */
    public function isEnabled ($data,  Omnius_Sandbox_Model_Replica $replica) {
        if(array_search($replica->getId(), $this->sandboxList) !== false) {
            return "Enabled";
        }
        else {
            return "Disabled";
        }
    }

    /**
     * Checks if a replica is remote or not
     * @param $data
     * @param Omnius_Sandbox_Model_Replica $replica
     * @return string
     */
    public function isRemote($data, Omnius_Sandbox_Model_Replica $replica)
    {
        return ($replica->getSshHost() && $replica->getSshUser())
            ? 'Remote'
            : 'Local';
    }

    /**
     * Customize the remove/process replica mass actions
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_replica', array(
            'label' => Mage::helper('sandbox')->__('Remove Replica'),
            'url' => $this->getUrl('*/adminhtml_replica/massRemove'),
            'confirm' => Mage::helper('sandbox')->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('enable_selected_replicas', array(
            'label' => Mage::helper('sandbox')->__('Enable selected replicas'),
            'url' => $this->getUrl('*/adminhtml_replica/enableSelected'),
            'confirm' => Mage::helper('sandbox')->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('disable_selected_replicas', array(
            'label' => Mage::helper('sandbox')->__('Disable selected replicas'),
            'url' => $this->getUrl('*/adminhtml_replica/disableSelected'),
            'confirm' => Mage::helper('sandbox')->__('Are you sure?')
        ));
        return $this;
    }
}
