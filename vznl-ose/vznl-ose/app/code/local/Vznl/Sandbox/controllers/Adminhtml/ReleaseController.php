<?php
require_once ('Dyna/Sandbox/controllers/Adminhtml/ReleaseController.php');

/**
 * Class Vznl_Sandbox_Adminhtml_ExportController
 */
class Vznl_Sandbox_Adminhtml_ReleaseController extends Dyna_Sandbox_Adminhtml_ReleaseController
{

    /**
     * Persist the data in the database
     */
    public function saveAction()
    {
        $dateError = false;
        $post_data = $this->getRequest()->getPost();
      
        if ($post_data) {
            $dateNl = new DateTime("now", new DateTimeZone('Europe/Amsterdam'));

            if (isset($post_data['deadline'])) {
                $a = new DateTime(Mage::getSingleton('core/date')->gmtDate());
                $b = new DateTime(Mage::getSingleton('core/date')->date());
                $diff = $a->diff($b);
                $deadlineStr = $post_data['deadline'];
                $deadline = new DateTime($deadlineStr);
                if ($diff->invert) {
                    $deadline->add($diff);
                } else {
                    $deadline->sub($diff);
                }
                $post_data['deadline'] = $deadline->format('Y-m-d H:i:s');
                if ($post_data['deadline'] < $dateNl->format('Y-m-d H:i:s')) {
                    $dateError = true;
                }
                if(isset($post_data['is_heavy'])){
                    $post_data['is_heavy'] = 1;
                }else {
                    $post_data['is_heavy'] = 0;
                }

                if(isset($post_data['export_media'])){
                    $post_data['export_media'] = 1;
                }else {
                    $post_data['export_media'] = 0;
                }

                if(isset($post_data['import_check'])){
                    $post_data['import_check'] = 1;
                }else {
                    $post_data['import_check'] = 0;
                }
            }

            if (isset($post_data['deadline']) && isset($post_data['one_time_date'])) {
                if ($post_data['one_time_date'] < $dateNl->format('Y-m-d H:i:s') || $post_data['one_time_date'] > $post_data['deadline']) {
                    $dateError = true;
                }
            }

            if (!isset($post_data['schedule_type']) || empty($post_data['schedule_type']) || !isset($post_data['one_time_date'])) {
                $post_data['one_time_date'] = '0000-00-00 00:00:00';
            }

            if (!isset($post_data['created_at'])) {
                $post_data['created_at'] = $dateNl->format('Y-m-d H:i:s');
            }

            if ($dateError) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please set correct deadline and execution dates'));
                Mage::getSingleton('adminhtml/session')->setReleaseData($this->getRequest()->getPost());
                if ($this->getRequest()->getParam('id')) {
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                } else {
                    $this->_redirect('*/*/new');
                }

                return;
            }

            try {
                $model = Mage::getModel('sandbox/release')
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam('id'))
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Release was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setReleaseData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setReleaseData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

        }
        $this->_redirect('*/*/');
    }

    /**
     * Trigger edit mode for a record
     */
    public function editAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Release'));
        $this->_title($this->__('Edit Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sandbox/release')->load($id);
        if ($model->getId()) {
            $model->setDeadline(Mage::getModel('core/date')->date('Y-m-d H:i:s', $model->getDeadline()));
            if ($model->getOneTimeDate() == '0000-00-00 00:00:00') {
                $model->setOneTimeDate(null);
            }
            Mage::register('release_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('sandbox/release');
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Release Manager'), Mage::helper('adminhtml')->__('Release Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Release Description'), Mage::helper('adminhtml')->__('Release Description'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('sandbox/adminhtml_release_edit'))->_addLeft($this->getLayout()->createBlock('sandbox/adminhtml_release_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sandbox')->__('Item does not exist.'));
            $this->_redirect('*/*/');
        }
    }
}
