<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$exportTableName = 'sandbox_replica';

/** @var Mage_Sales_Model_Mysql4_Setup $this */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

if ($connection->isTableExists($exportTableName)) {
    $connection->modifyColumn($exportTableName, 'environment_path',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 255,
            'nullable' => false,
            'comment' => 'Environment Path'
        ))
    ;
}

$installer->endSetup();