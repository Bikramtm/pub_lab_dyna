<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $this->getTable('dyna_bundles/campaign');
if ($connection->tableColumnExists($tableName, 'dealer_id') === false) {
    $connection->addColumn($tableName,
        'dealer_id',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'Dealer Id'
        )
    );
}

if ($connection->tableColumnExists($tableName, 'dealer_group') === false) {
    $connection->addColumn($tableName,
        'dealer_group',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'Dealer Group'
        )
    );
}

if ($connection->tableColumnExists($tableName, 'product_segment') === false) {
    $connection->addColumn($tableName,
        'product_segment',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'product_segment'
        )
    );
}

if ($connection->tableColumnExists($tableName, 'channel') === false) {
    $connection->addColumn($tableName,
        'channel',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'Channel'
        )
    );
}
$installer->endSetup();