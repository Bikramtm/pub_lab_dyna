<?php

/** @var Mage_Sales_Model_Entity_Setup $this */

$this->startSetup();
$connection = $this->getConnection();
$bundleRuleTable = 'bundle_rules';

if (!$connection->tableColumnExists($bundleRuleTable, 'effective_date')) {
    $connection->addColumn($bundleRuleTable, 'effective_date', array(
        'type' => Varien_Db_Ddl_Table::TYPE_DATE,
        'comment' => 'Rule start date',
        'nullable' => true,
        'default' => null,
    ));
}

if (!$connection->tableColumnExists($bundleRuleTable, 'expiration_date')) {
    $connection->addColumn($bundleRuleTable, 'expiration_date', array(
        'type' => Varien_Db_Ddl_Table::TYPE_DATE,
        'comment' => 'Rule end date',
        'nullable' => true,
        'default' => null,
    ));
}

$this->endSetup();
