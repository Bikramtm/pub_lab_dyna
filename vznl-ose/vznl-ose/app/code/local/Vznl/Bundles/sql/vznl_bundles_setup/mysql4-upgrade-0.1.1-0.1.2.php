<?php

/** @var Mage_Sales_Model_Entity_Setup $this */

$this->startSetup();
$connection = $this->getConnection();
$bundleRuleTable = 'bundle_rules';

if ($connection->tableColumnExists($bundleRuleTable, 'website_id')) {
    $connection->dropColumn($bundleRuleTable,"website_id");
    $this->getConnection()->addColumn($bundleRuleTable, "website_id", array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 100,
        'comment' => "Website Ids"
    ));
}
$this->endSetup();
