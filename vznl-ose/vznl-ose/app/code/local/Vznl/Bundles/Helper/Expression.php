<?php

class Vznl_Bundles_Helper_Expression extends Dyna_Bundles_Helper_Expression
{
    protected $simulateActive = false;

    /**
     * Update helper instance simulating as if a product was added to cart in order to anticipate which products are eligible for bundling
     * @param Dyna_Catalog_Model_Product $product
     * @return $this
     */
    public function simulateProduct(Dyna_Catalog_Model_Product $product)
    {
        $this->simulateActive = true;

        // If already present in quote, quote instance is up to date
        if (in_array($product->getSku(), $this->activePackageProducts)) {
            $this->packages[$this->activePackageId][$product->getSku()]['simulated'] = 1;
            return $this;
        }

        $this->cartEmpty = false;

        $this->simulatingProduct = $product->getId();

        // proceeding further with product in quote simulation
        $sku = $product->getSku();

        // add sku to quoteProducts array
        $this->quoteProducts[$sku] = $sku;
        $this->activePackageProducts[$sku] = $sku;
        // Add sku to quote list of products
        $this->packages[$this->activePackageId][$sku] = $sku;
        $this->packages[$this->activePackageId][$sku]['simulated'] = 1;
        // Build categories for current sku
        $this->packages[$this->activePackageId]['categories'] = array();
        foreach ($product->getCategoryIds() as $categoryId) {
            if (isset($this->allCategories[$categoryId])) {
                $this->packages[$this->activePackageId]['categories'][$categoryId] = $this->getCategoryPath($categoryId);
                // Add category id and name to the list of categories for current sku
                $this->quoteProductsCategories[$sku][$categoryId] = $this->getCategoryPath($categoryId);
                // Add category id and name to the list of categories for current sku
                $this->packages[$this->activePackageId]['categories'][$categoryId] = $this->activePackageProductsCategories[$sku][$categoryId] = $this->getCategoryPath($categoryId);
            }
        }

        // Build quote categories
        $this->quoteCategories = array_unique(array_reduce($this->quoteProductsCategories, "array_merge", array()));

        // Build active package categories
        $this->activePackageCategories = array_unique(array_reduce($this->activePackageProductsCategories, "array_merge", array()));

        return $this;
    }

    /**
     * Check if the simulate mode is on
     * @return bool
     */
    public function isSimulateActive()
    {
        return $this->simulateActive;
    }

    /**
     * Return the ID of the simulated product
     * @return int
     */
    public function getSimulatedProductId()
    {
        return $this->simulatingProduct;
    }

    /**
     * Determine whether or not active package in cart is of a certain type
     * @param $type
     * @return array
     */
    public function packageType($type)
    {
        $response = array();

        if (($activePackage = $this->quote->getCartPackage())
            && (strtolower($activePackage->getType()) == strtolower($type))
            && $this->canBeBundled($activePackage)) {

            $packageType = strtoupper($this->packageTypes[$activePackage->getPackageId()]);
            $tempResponse['package_types'][$packageType][] = (int)$activePackage->getPackageId();
            $tempResponse['package_types'][$packageType] = array_unique($tempResponse['package_types'][$packageType]);

            //intersect with other findings
            $configHelper = Mage::helper('dyna_configurator/expression');
            $response = $configHelper->updateBundleResult($tempResponse);
        }

        return $response;
    }

    /**
     * Checks if a specific product is found in current package/cart
     * @param $sku
     * @return bool
     */
    public function containsProduct($sku)
    {
        if ($this->cartEmpty) {
            return false;
        }
        $activePackage = $this->quote->getActivePackage();
        $configHelper = Mage::helper('dyna_configurator/expression');

        if (isset($this->quoteProducts[$sku])) {

            $tempResponse['package_types'] = [];

            // cart contains product in this category, let's get all packages that satisfy this condition
            foreach ($this->packages as $packageId => $packageData) {
                //if active package is mobile, skip the rest and only process this one
                if($configHelper->shouldSkipBundleProcessing($activePackage, $this->packageTypes[$packageId], $packageId)){
                    continue;
                }

                if (isset($this->packages[$packageId][$sku])) {

                    if($activePackage->getPackageId() == $packageId && $this->packages[$packageId][$sku]['simulated'] != 1 && Mage::registry('skipSimulation') == false){
                        continue;
                    }

                    $packageType = strtoupper($this->packageTypes[$packageId]);
                    $tempResponse['package_types'][$packageType][] = (int)$packageId;
                    $tempResponse['package_types'][$packageType] = array_unique($tempResponse['package_types'][$packageType]);
                }
            }

            //intersect with other findings
            $configHelper->updateBundleResult($tempResponse);

            return (isset($tempResponse['package_types']) && !empty($tempResponse['package_types']))?true:false;;
        }

        return false;
    }

    /**
     * Determine whether or not active package contains a certain product
     * @param $sku
     * @param bool $notExpression
     * @return bool
     */
    public function packageContainsProduct($sku, $notExpression = false)
    {
        if ($this->cartEmpty) {
            return false;
        }

        $configHelper = Mage::helper('dyna_configurator/expression');

        if ((!$notExpression && isset($this->activePackageProducts[$sku])) || ($notExpression && !isset($this->activePackageProducts[$sku]))) {

            if($this->packages[$this->activePackageId][$sku]['simulated'] != 1 && Mage::registry('skipSimulation') == false){
                return false;
            }

            $packageType = strtoupper($this->packageTypes[$this->activePackageId]);
            $tempResponse['package_types'][$packageType][] = (int)$this->activePackageId;
            $tempResponse['package_types'][$packageType] = array_unique($tempResponse['package_types'][$packageType]);

            //intersect with other findings
            $configHelper->updateBundleResult($tempResponse);

            return (isset($tempResponse['package_types']) && !empty($tempResponse['package_types']))?true:false;;
        }

        return false;
    }

    /**
     * Determines whether entire cart contains or not product in a certain category
     * @param $categoryName
     * @param bool $notExpression
     * @return array
     */
    public function containsProductInCategory($categoryName, $notExpression = false)
    {
        if ($this->cartEmpty) {
            return array();
        }

        $activePackage = $this->quote->getActivePackage();
        $configHelper = Mage::helper('dyna_configurator/expression');

        if (in_array($categoryName, $this->quoteCategories)) {
            $tempResponse['package_types'] = [];
            // cart contains product in this category, let's get all packages that satisfy this condition
            foreach ($this->packages as $packageId => $packageData) {
                //if active package is mobile, skip the rest and only process this one
                if($configHelper->shouldSkipBundleProcessing($activePackage, $this->packageTypes[$packageId], $packageId)){
                    continue;
                }

                if (!$notExpression && isset($this->packages[$packageId]['categories']) && in_array($categoryName, $this->packages[$packageId]['categories'])) {
                    $packageType = strtoupper($this->packageTypes[$packageId]);
                    $tempResponse['package_types'][$packageType][] = (int)$packageId;
                    $tempResponse['package_types'][$packageType] = array_unique($tempResponse['package_types'][$packageType]);
                }
            }
            //intersect with other findings
            $response = $configHelper->updateBundleResult($tempResponse);

            return $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT];
        }

        return array();
    }

    /**
     * Determines whether or not active package contains product in category
     * @param $categoryName
     * @param bool $notExpression
     * @param bool $certainPackageId
     * @return array
     */
    public function packageContainsProductInCategory($categoryName, $notExpression = false, $certainPackageId = false)
    {
        if ($this->cartEmpty) {
            return array();
        }

        $activePackage = $this->quote->getActivePackage();
        $configHelper = Mage::helper('dyna_configurator/expression');

        if (!$certainPackageId && in_array($categoryName, $this->quoteCategories) ||
            ($certainPackageId && in_array($categoryName, (!empty($this->packages[$certainPackageId]['categories']) ? $this->packages[$certainPackageId]['categories'] : array())))) {
            $tempResponse['package_types'] = [];
            // cart contains product in this category, let's get all packages that satisfy this condition
            foreach ($this->packages as $packageId => $packageData) {

                $packageId = $certainPackageId && $certainPackageId == $packageId ? $certainPackageId : $packageId;
                //if active package is mobile, skip the rest and only process this one
                if($configHelper->shouldSkipBundleProcessing($activePackage, $this->packageTypes[$packageId], $packageId) ||
                    $packageId != $activePackage->getPackageId()){
                    continue;
                }

                if (!$notExpression && isset($this->packages[$packageId]['categories']) && in_array($categoryName, $this->packages[$packageId]['categories'])) {
                    $packageType = strtoupper($this->packageTypes[$packageId]);
                    $tempResponse['package_types'][$packageType][] = (int)$packageId;
                    $tempResponse['package_types'][$packageType] = array_unique($tempResponse['package_types'][$packageType]);
                }
            }
            //intersect with other findings
            $response = $configHelper->updateBundleResult($tempResponse);

            return $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT];
        }

        return array();
    }

    /**
     * Determine whether or not current cart contains a certain package type
     * @param string $packageCode
     * @return array
     */
    public function containsPackageCreationType(string $packageCode)
    {
        $tempResponse = array();
        $packageCode = strtoupper($packageCode);
        $cartPackages = $this->_getCartPackages();
        $configHelper = Mage::helper('dyna_configurator/expression');

        if ($cartPackages) {
            foreach ($cartPackages as $package) {
                if ($package->getEditingDisabled()) {
                    continue;
                }

                if (!$this->canBeBundled($package)) {
                    continue;
                }

                if ((strtoupper($package->getType()) == $packageCode)) {
                    $packageType = strtoupper($this->packageTypes[$package->getPackageId()]);
                    $tempResponse['package_types'][$packageType][] = (int)$package->getPackageId();
                    $tempResponse['package_types'][$packageType] = array_unique($tempResponse['package_types'][$packageType]);
                }
            }
        }

        //intersect with other findings
        $response = $configHelper->updateBundleResult($tempResponse);

        return $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT];
    }
}
