<?php

class Vznl_Bundles_Model_Expression_QuotePackageScope extends Dyna_Bundles_Model_Expression_QuotePackageScope
{

    /**
     * Extend the dyna method to save the cart after promo is added
     *
     * @param $skus
     * @param $target
     * @return bool
     * @throws Mage_Core_Exception
     * @throws Mage_Core_Model_Store_Exception
     */
    public function addPromoProduct($skus, $target)
    {
        $websiteId = Mage::app()->getWebsite()->getId();
        if (($emulatedWebsiteId = Mage::helper('superorder')->shouldEmulateStore($websiteId)) != $websiteId) {
            // If a product from another shop should be added, emulate that store
            $storeId = Mage::app()->getWebsite($emulatedWebsiteId)->getDefaultStore()->getId();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }

        $aSkus = explode(',', $skus);
        foreach ($aSkus as $sku) {
            $quote = $this->getQuote();
            $cart = $this->getCart();
            if ($sku) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId($storeId);
                $product->load($product->getIdBySku($sku));
                if (!$product->getId()) {
                    Mage::throwException('Invalid product ID given');
                }
            } else {
                Mage::throwException('Invalid product ID given');
            }

            $numberCtns = count($this->getQuote()->getPackageCtns());
            $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

            $targetProductId = null;
            if (!$this->exists($product)) {
                $allItems = $this->package->getAllItems();
                foreach ($allItems as $targetItem) {
                    if ($targetItem->getSku() == $target) {
                        $targetProductId = $targetItem->getProductId();
                        break;
                    }
                }
                // adding a product to a target that doesn't exist should never happen as the action should have an existence condition for this
                // but still returning evaluation response to true to prevent logging
                if (!$targetProductId) {
                    return true;
                }

                //before adding product, remove other promos from same mutual excl category
                $removedPromoSkus = [];
                $removeRuleIds = [];
                $targetItems = [];
                $mutualProductCategories = Mage::helper('vznl_catalog')->getMutuallyExclusiveCategories();

                //if promo being added is part of any mutual excl category, check other promos to see if they conflict
                if(isset($mutualProductCategories[$sku])){
                    foreach($allItems as $item){

                        //if product is not part of any mutual excl category skip the check
                        if(!isset($mutualProductCategories[$item->getSku()])){
                            continue;
                        }

                        //only check promos, rest can be skiped
                        if(!$item->isPromo()){
                            continue;
                        }

                        $sameMutualCategory = array_intersect($mutualProductCategories[$item->getSku()], $mutualProductCategories[$sku]);
                        if(!empty($sameMutualCategory))
                            $removedPromoSkus[] = $item->getSku();
                            $targetItems[] = $item->getTargetItem();
                            $cart->removeItem($item->getId());
                    }
                }

                //if some promos were removed, try to identify which sale rule added them to remove them from applied rules
                if(!empty($removedPromoSkus)){
                    foreach(explode(',', $this->getQuote()->getAppliedRuleIds()) as $appliedRuleId){
                        $salesRule = Mage::getModel('salesrule/rule')->load($appliedRuleId);
                        foreach ($removedPromoSkus as $removedPromoSku) {
                            if ($removedPromoSku == $salesRule->getPromoSku() && $salesRule->getisPromo()) {
                                $removeRuleIds[] = $appliedRuleId;
                            }
                        }
                    }
                }

                //if we identified which rule needs to be removed, try to remove them from the applied rule ids
                if($removeRuleIds){
                    $quoteAppliedRuleIds = explode(',',$this->getQuote()->getAppliedRuleIds());
                    $quoteAppliedRuleIds = array_diff($quoteAppliedRuleIds, $removeRuleIds);
                    $this->getQuote()->setAppliedRuleIds(implode(',', $quoteAppliedRuleIds))->save();

                    foreach($targetItems as $targetItem){
                        $itemAppliedRuleIds = explode(',',$targetItem->getAppliedRuleIds());
                        $itemAppliedRuleIds = array_diff($itemAppliedRuleIds, $removeRuleIds);
                        $targetItem->setAppliedRuleIds(implode(',', $itemAppliedRuleIds))->save();
                    }
                }

                $result = $cart->addProduct($product, array(
                        'qty' => $numberCtns ?: 1,
                        'one_of_deal' => ($isOneOfDeal && $numberCtns) ? 1 : 0,
                    )
                );

                if (is_string($result)) {
                    Mage::throwException($result);
                } else {
                    // get the package id and package type of the product in the current quote and bundle
                    $packageInfo = $this->getCartPackageInfo();

                    $result
                        ->setPackageType($packageInfo['packageType'])
                        ->setBundleComponent($this->bundle->getId())
                        ->setTargetId($targetProductId)
                        ->setPackageId($packageInfo['packageId'])
                        ->setIsBundlePromo(1);
                }
                if ($quote->getTotalsCollectedFlag()) {
                    $quote->setTotalsCollectedFlag(false);
                }
                $cart->save();
            } else {
                $this->markProductAsBundleComponent($product);
            }

        }

        return isset($result) ? $result : true;
    }

    /**
     * @param $sku
     * @return Dyna_Checkout_Model_Sales_Quote_Item|Mage_Checkout_Model_Cart|Mage_Sales_Model_Quote_Item|Omnius_Checkout_Model_Sales_Quote_Item
     */
    protected function addSkuToCart($sku)
    {
        $websiteId = Mage::app()->getWebsite()->getId();
        if (($emulatedWebsiteId = Mage::helper('superorder')->shouldEmulateStore($websiteId)) != $websiteId) {
            // If a product from another shop should be added, emulate that store
            $storeId = Mage::app()->getWebsite($emulatedWebsiteId)->getDefaultStore()->getId();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }

        $cart = $this->getCart();
        if ($sku) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($storeId);
            $product->load($product->getIdBySku($sku));
            if (!$product->getId()) {
                Mage::throwException('Invalid product ID given');
            }
        } else {
            Mage::throwException('Invalid product ID given');
        }

        $numberCtns = count($this->getQuote()->getPackageCtns());
        $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

        $result = false;
        if (!$this->exists($product)) {
            $result = $cart->addProduct($product, array(
                    'qty' => $numberCtns ?: 1,
                    'one_of_deal' => ($isOneOfDeal && $numberCtns) ? 1 : 0,
                )
            );

            if (is_string($result)) {
                Mage::throwException($result);
            } else {
                // get the package id and package type of the product in the current quote and bundle
                $packageInfo = $this->getCartPackageInfo();

                $result
                    ->setPackageType($packageInfo['packageType'])
                    ->setBundleComponent($this->bundle->getId())
                    ->setPackageId($packageInfo['packageId']);
            }
            if ($this->getQuote()->getTotalsCollectedFlag()) {
                $this->getQuote()->setTotalsCollectedFlag(false);
            }
            $cart->save();
        } else {
            $this->markProductAsBundleComponent($product);
        }

        return $result;
    }

    /**
     * Add connection fee to a subscription in order to overwrite the aansluitkosten
     * @param $promoSku
     * @param $targetSku
     * @return bool
     */
    public function addConnectionCost($promoSku, $targetSku)
    {
        //add promo to the cart
        $promoItem = $this->addPromoProduct($promoSku, $targetSku);

        //loop through quote items to find tariff and update the connection cost
        foreach($this->getQuote()->getAllItems() as $item){

            //if tariff is found, check if promo connection cost is lower and overwrite the tariff connection cost
            if(strcasecmp($item->getSku(), $targetSku) === 0){
                $qty = $item->getTotalQty();
                $promProd = $promoItem->getProduct();
                $tariffProd = $item->getProduct();
                $tariffConnCost = $tariffProd->getData('prijs_aansluitkosten');

                if($promoConnCost = $promProd->getData('prijs_aansluit_promo_bedrag')) {
                    if($promoConnCost < $tariffConnCost) {
                        $quoteAmount        = $promoConnCost;
                        $connectionCost     = $qty * $quoteAmount;
                    }
                }
                elseif($promoPrecent = $promProd->getData('prijs_aansluit_promo_procent')) {
                    if($promoPrecent > 0 && $promoPrecent <= 100) {
                        $connectionCost    = ($qty * $tariffConnCost) * (float)$promoPrecent / 100;
                    }
                }

                $connectionCost     = min($connectionCost, $tariffConnCost * $qty);
                $item->setConnectionCost($connectionCost);
                break;
            }
        }
        return true;
    }
}