<?php

class Vznl_Bundles_Model_BundleRule extends Dyna_Bundles_Model_BundleRule
{
    CONST TYPE_MOBILEFIXED = 'MobileFixed';
    /**
     * @param null $customerNumber
     * @param null $productId
     * @return array
     */
    public function getSummaryBundleDetails($customerNumber = null, $productId = null, $selectedInstalledBaseProductEntityId = null, $interStackPackageId = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getCart()->getQuote();
        $bHelper = $this->getBundlesHelper();
        /** @var Dyna_Package_Model_Package $activePackage */
        $activePackage = $quote->getCartPackage();
        $dummyPackage = null;
        $bundleProductsSummaryTotals = [];
        $response = [];

        if ($customerNumber) {
            $customer = $this->getCustomerSession()->getCustomer();
            $installBaseProducts = $customer->getInstalledBaseProducts($customerNumber, $productId);
            /** @var Dyna_Package_Model_Package $dummyPackage */
            $dummyPackage = Mage::getModel("package/package");



            // Save install base package
            $dummyPackage
                ->setPackageCreationTypeId(Mage::getModel('dyna_package/packageCreationTypes')
                    ->getTypeByPackageCode(Vznl_Catalog_Model_Type::CREATION_TYPE_FIXED)->getId())
                ->setInstalledBaseProducts(implode(",", $installBaseProducts))
                ->setInitialInstalledBaseProducts(implode(",", $installBaseProducts))
                ->setEditingDisabled(true)
                ->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
                ->setCreatedAt(now())
                ->setBundleId($this->getId())
                ->setBundleName($this->getBundleRuleName())
                ->setQuoteId($quote->getId())
                ->setParentAccountNumber($customerNumber)
                ->setServiceLineId($productId);

        }

        Mage::unregister('bundleSummaryProducts');
        if ($interStackPackageId) {
            $secondPackage = $quote->getCartPackage($interStackPackageId);
            $bundlePackages = [
                $activePackage,
                $secondPackage
            ];

            foreach ($bundlePackages as $package) {
                $ruleParser = $bHelper->buildRuleParserFromBundleRuleForDummyScope($this, $package, ($package != $secondPackage ? $secondPackage : $activePackage), $interStackPackageId);
                $ruleParser->parse();
            }
        } else {
            $ruleParser = $bHelper->buildRuleParserFromBundleRuleForDummyScope($this, $activePackage, $dummyPackage, $interStackPackageId);
            $ruleParser->parse();
        }
        $response['products'] = Mage::registry('bundleProductsSummary');
        $response['totals'] = $bundleProductsSummaryTotals;

        return $response;
    }

    /**
     * Return the quote items that are required for bundling (targeted products in cart)
     * @return Dyna_Checkout_Model_Sales_Quote_Item[]
     */
    public function getBundleComponentItems()
    {
        $items = array();

        // get expression language instance
        /** @var Dyna_Bundles_Helper_ExpressionLanguage $expressionLanguage */
        $expressionLanguage = Mage::helper('dyna_bundles/expressionLanguage');
        $expressionLanguage->resetResponse();

        /** @var Dyna_Configurator_Helper_Expression $bundlesSimulatorHelper */
        $bundlesSimulatorHelper = Mage::helper('dyna_bundles/expression');
        $bundlesSimulatorHelper->updateInstance(Mage::getSingleton('checkout/cart')->getQuote());
        $bundlesSimulatorHelper->setCurrentBundle($this);

        $cartObject = Mage::getModel('dyna_configurator/expression_cart')->setBundlesHelper($bundlesSimulatorHelper);

        // get expression objects
        $expressionObjects = array(
            'customer' => Mage::getModel('dyna_configurator/expression_customer'),
            'cart' => $cartObject,
            'availability' => Mage::getModel('dyna_configurator/expression_availability'),
            'installBase' => Mage::getModel('dyna_configurator/expression_installBase'),
            'agent' => Mage::getModel('dyna_configurator/expression_agent'),
        );

        // throw exception if at least one fails, it should not be possible to access create action in this case
        foreach ($this->getConditions() as $bundleCondition) {
            if (!$expressionLanguage->evaluate($bundleCondition->getCondition(), $expressionObjects)) {
                Mage::throwException("You cannot create a bundle which evaluates to false");
            }
        }

        $evaluationResponse = $expressionLanguage->getResponse();
        $targetedProductIds = $evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] ?? array();
        $processedTargetedProductSkus = [];
        foreach ($targetedProductIds as $targetedProductId) {
            if ($targetedProductId['eligibleProductSku']) {
                $processedTargetedProductSkus[] = $targetedProductId['eligibleProductSku'];
            }
        }

        $activePackageId = $this->getCart()->getQuote()->getActivePackageId();
        foreach ($this->getCart()->getQuote()->getAllItems() as $quoteItem) {
            if ($quoteItem->getPackageId() !== $activePackageId && !in_array($quoteItem->getSku(), $processedTargetedProductSkus)) {
                continue;
            }

            if (in_array($quoteItem->getSku(), $processedTargetedProductSkus) && $quoteItem->getProduct()->isSubscription()) {
                $items[] = $quoteItem;
            }
        }

        return $items;
    }

    /**
     * Determines whether or not the bundle is of type MobileFixed
     * @return bool
     */
    public function isMobileFixed()
    {
        return $this->getType() === static::TYPE_MOBILEFIXED;
    }

    /**
     * Return current bundle type (currently only three special cases are treated: Red+, Suso, Kombi)
     * @return string
     */
    public function getType()
    {
        if(strcasecmp($this->getBundleType(), self::TYPE_MOBILEFIXED) === 0){
            return self::TYPE_MOBILEFIXED;
        }

        return parent::getType();
    }
}