<?php

/**
 * Class Vznl_Bundles_Model_Import_Bundle
 */
class Vznl_Bundles_Model_Import_Bundle extends Dyna_Bundles_Model_Import_Bundle
{
    protected $_logFileName = "Bundles";
    protected $_logFileExtension = "log";

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper('vznl_import');
    }

    /**
     * Import bundles
     *
     * @param array $data
     *
     * @return void
     */
    public function import($data)
    {
        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;

        $data = $this->_setDataMapping($data);

        $mandatoryAttributes = [
            'bundle_rule_name',
            'conditions',
            'website_id',
            'processContext',
        ];
        foreach ($mandatoryAttributes as $attribute) {
            if (!isset($data[$attribute])) {
                $this->_logError('Skipping line without node: ' . $attribute);
                $skip = true;
            }
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        $this->_log('Start importing ' . $data['bundle_rule_name'], false);

        try {
            /** @var Dyna_Bundles_Model_BundleRule $bundleRule */
            $bundleRule = Mage::getModel('dyna_bundles/bundleRule');
            $existingBundleRule = $bundleRule->load($data['bundle_rule_name'], 'bundle_rule_name');

            if ($existingBundleRule->getId()) {
                $bundleRule = $existingBundleRule;
                $this->_log('Loading existing bundle rule "' . $data['bundle_rule_name'] . '" with ID ' . $bundleRule->getId(), false);
            }

            $params = [
                'bundle_rule_name',
                'bundle_gui_name',
                'description',
                'bundle_type',
                'website_id',
                'expirationdate',
                'effectivedate',
            ];
            // all parameters are default null
            $defaultParams = array_combine($params, array_fill(0, count($params), null));
            $defaultParams[5] = 1; // default website_id: 1
            $this->_setDataWithDefaults($bundleRule, $data, $defaultParams);

            if (!$this->getDebug()) {
                $bundleRule->save();
            }

            if (!$this->getDebug()) {
                $this->_addBundleActions($bundleRule, $data['actions'] ?? []);
                $this->_addBundleConditions($bundleRule, $data['conditions']);
                $this->_addBundleHints($bundleRule, $data['hints']);
                $this->_addBundleProcessContexts($bundleRule, $data['processContext']);
            }

            unset($bundleRule);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['bundle_rule_name'], false);
        } catch (Exception $ex) {
            $this->_logError(print_r($ex,true));
            $this->_logEx($ex);
            fwrite(STDERR, $ex->getMessage());
        }
    }

    /**
     * Process custom data mapping for bundles
     *
     * @param array $data
     *
     * @return array
     */
    protected function _setDataMapping($data)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }
        $channel = explode(',', $data['channel']);
        $websites = Mage::app()->getWebsites(false, true);
        foreach($channel as $website)
        {
            $web[] = $websites[trim(strtolower($website))]->getId();
        }

        $data['website_id'] = implode(',', $web);
        unset($data['channel']);

        unset ($data['hint']);

        // set effective date
        if (isset($data['effectivedate'])) {
            $data['effective_date'] = $this->getXmlValue($data, "effectivedate");
            unset($data['effectivedate']);
        }

        // set expiration date
        if (isset($data['expirationdate'])) {
            $data['expiration_date'] = $this->getXmlValue($data, "expirationdate");
            unset($data['expirationdate']);
        }

        return $data;
    }

    public function getFileLogName()
    {
        return strtolower($this->stack) . '_' . $this->_logFileName . '.' . $this->_logFileExtension;
    }

    public function clearBundleProcessContextTable()
    {
        $resource = Mage::getSingleton('core/resource');
        $conn = $resource->getConnection('core_write');
        $bundleProcessContextTable = $resource->getTableName('dyna_bundles/bundle_processcontext');

        $conn->query(sprintf('TRUNCATE TABLE %s', $bundleProcessContextTable));

        return $this;
    }
}