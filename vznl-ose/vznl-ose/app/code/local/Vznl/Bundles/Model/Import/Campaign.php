<?php

/**
 * Class Vznl_Bundles_Model_Import_Campaign
 */
class Vznl_Bundles_Model_Import_Campaign extends Dyna_Bundles_Model_Import_Campaign
{
    /**
     * Import campaign
     *
     * @param array $data
     *
     * @return void
     */
    protected $_logFileName = "Campaigns";
    protected $_logFileExtension = "log";
    public $_skippedFileRows = 0;

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper('vznl_import');
    }

    public function import($data)
    {
        $skip = false;
        $msg = array();
        //for logging purposes
        $this->_totalFileRows++;
        $data = $this->_setDataMapping($data);
        $productsegments = $this->getProductsegment();
        $this->checkMandatoryColumn($data);
        /*check valid dealer id*/
        $dealerId = array_map("trim", explode(',', $data['dealer_id']));
        $invalidDealerId = $this->checkInvalidDealerId($dealerId);
        if (count($invalidDealerId)) {
            $msg[] = 'ERROR invalid Dealer Id(s) ' . implode(',', $invalidDealerId);
            $skip = true;
        }

        /*check valid dealer group*/
        $dealerGroup = array_map("trim", explode(',', $data['dealer_group']));
        $invalidedGroup = $this->checkInvalidGroup($dealerGroup);

        if (count($invalidedGroup)) {
            $msg[] = 'ERROR invalid Dealer code(s) ' . implode(',', $invalidedGroup);
            $skip = true;
        }

        /* check valid channel*/
        $websites = array_map("trim", explode(',', $data['channel']));
        $invalidChannel = $this->checkChannel($websites);


        if (count($invalidChannel)) {
            $msg[] = 'ERROR invalid channel(s) ' . implode(',', $invalidChannel);
            $skip = true;
        }

        /* check valid product segment*/
        if (!in_array($data['product_segment'], $productsegments)) {
            $msg[] = 'ERROR invalid Product Segments ' . $data['product_segment'];
            $skip = true;
        }
        if ($skip) {
            $this->importLogMsg($skip, $msg);
            return;
        }

        try {
            /** @var Dyna_Bundles_Model_Campaign $campaign */
            $campaign = Mage::getModel('dyna_bundles/campaign');
            $existingCampaign = $campaign->load($data['campaign_id'], 'campaign_id');

            if ($existingCampaign->getEntityId()) {
                $msg[] = 'ERROR Skipping the campaign ' . $data['campaign_id'] . ', since this is a duplicate entry and a Campaign with same ID exists in the import file';
                $this->importLogMsg(true, $msg);
                return;
            }

            $data = $this->_filterData($data);
            $data = $this->_promotionhints($data);
            $params = [
                'campaign_id',
                'campaign_title',
                'cluster',
                'cluster_category',
                'advertising_code',
                'campaign_code',
                'marketing_code',
                'campaign_hint',
                'offer_hint',
                'promotion_hint1',
                'promotion_hint2',
                'promotion_hint3',
                'valid_from',
                'valid_to',
                'dealer_id',
                'dealer_group',
                'channel',
                'default',
            ];
            // all parameters are default null
            $defaultParams = array_combine($params, array_fill(0, count($params), null));
            $this->_setDataWithDefaults($campaign, $data, $defaultParams);
            if (!$this->getDebug()) {
                $campaign->save();
            }
            if (!$this->getDebug()) {
                $this->_setOffers($campaign, $data);
            }
            unset($campaign);
            $this->_qties++;
            $this->_log('Finished importing ' . $data['campaign_id'], false);
        } catch (Exception $ex) {
            Mage::log($ex->getMessage());
            $this->_logEx($ex);
        }
    }

    public function getDealerGroup()
    {
        $agentData = Mage::getModel("agent/dealergroup")->getCollection()
            ->addFieldToSelect('group_id');
        return array_column($agentData->getData(), 'group_id');
    }

    public function getDealerId()
    {
        $agentData = Mage::getModel("agent/dealer")->getCollection()
            ->addFieldToSelect('vf_dealer_code')
            ->addFieldToFilter('vf_dealer_code', array('neq' => null));
        return array_column($agentData->getData(), 'vf_dealer_code');
    }

    public function getChannel()
    {
        $websiteCode = array();
        foreach (Mage::app()->getWebsites() as $website) {
            $websiteCode[] = $website->getCode();
        }
        return $websiteCode;
    }

    public function getProductsegment()
    {
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'product_segment'); //"product_segment" is the attribute_code
        $allOptions = $attribute->getSource()->getAllOptions(true, true);
        foreach ($allOptions as $instance) {
            $productSegment[] = $instance['label'];
        }
        return $productSegment;
    }

    public function getFileLogName()
    {
        return strtolower($this->stack) . '_' . $this->_logFileName . '.' . $this->_logFileExtension;
    }

    public function checkMandatoryColumn($data)
    {
        $mandatoryAttributes = ['campaign_id', 'dealer_id', 'dealer_group', 'channel', 'product_segment'];
        foreach ($mandatoryAttributes as $attribute) {
            if (!isset($data[$attribute])) {
                $this->_logError('Skipping line without ' . $attribute);
                $skip = true;
            }
        }
        $this->_log('Start importing ' . $data['campaign_id'], false);
    }

    public function checkInvalidDealerId($dealerId)
    {
        $invalidDealerId = array();
        $dealerIds = $this->getDealerId();
        foreach ($dealerId as $id) {
            if (!in_array($id, $dealerIds)) {
                $invalidDealerId[] = $id;
            }
        }
        return $invalidDealerId;
    }

    public function checkInvalidGroup($dealerGroup)
    {
        $invalidedGroup = array();
        $dealerGroups = $this->getDealerGroup();
        foreach ($dealerGroup as $group) {
            if (!in_array($group, $dealerGroups)) {
                $invalidedGroup[] = $group;
            }
        }
        return $invalidedGroup;
    }

    public function checkChannel($websites)
    {
        $invalidChannel = array();
        $channels = $this->getChannel();
        foreach ($websites as $website) {
            if (!in_array($website, $channels)) {
                $invalidChannel[] = $website;
            }
        }
    }

    public function importLogMsg($skip, $msg)
    {
        $this->_skippedFileRows++;
        $this->_logError('Skipping row ' . $this->_skippedFileRows . ' due to below error(s)');
        foreach ($msg as $log) {
            $this->_logError($log);
        }
    }

    /**
     * Add relation to offers
     *
     * @param Dyna_Bundles_Model_Campaign $campaign
     * @param array $data
     */
    protected function _setOffers($campaign, $data)
    {
        if (!empty($data['offer_ids'])) {
            $offerCount = 0;
            /** @var Dyna_Bundles_Model_CampaignOffer $offerModel */
            foreach (explode(',', $data['offer_ids']) as $offerId) {
                $offerCount++;
                if ($offerCount > 7) {
                    $this->_logError('Skipping the Offer ' . $offerId .
                        ' for the campaign ' .
                        $campaign->getCampaignId() . ', since only 7 offers are allowed per Campaign.');
                } else {
                    $offerModel = Mage::getModel('dyna_bundles/campaignOffer');
                    $offerId = trim($offerId);
                    $offer = $offerModel->load($offerId, 'offer_id');
                    if ($offer->getEntityId()) {
                        $offerModel = $offer;
                    } else {
                        $offerModel->setData([
                            'offer_id' => $offerId
                        ]);
                        $offerModel->save();
                    }

                    $query = "INSERT INTO `bundle_campaign_offer_relation` (`campaign_id`, `offer_id`) VALUES (" . $campaign->getEntityId() . ", " . $offerModel->getEntityId() . ") 
                ON DUPLICATE KEY UPDATE `campaign_id` = `campaign_id`";
                    Mage::getSingleton('core/resource')->getConnection('core_write')->exec($query);
                }
            }
        }
    }
}
