<?php
class Vznl_Bundles_Model_Expression_QuotePackageDummyScope extends Dyna_Bundles_Model_Expression_QuotePackageDummyScope
{
    /**
     * Method to register the bundle summary so the extended bundle drawer knows what to show
     * @param $promoSku
     * @param $targetSku
     * @return bool
     */
    public function addConnectionCost($promoSku, $targetSku)
    {
        return self::addPromoProduct($promoSku, $targetSku);
    }

    /**
     * Overwrite the Dyna method to show product in the extended drawer even if already exits in cart
     * @param $skus
     * @param $target
     * @return bool
     * @throws Mage_Core_Exception
     * @throws Mage_Core_Model_Store_Exception
     */
    public function addPromoProduct($skus, $target)
    {
        if (Mage::registry('bundleProductsSummary')) {
            $this->bundleProductsSummary = Mage::registry('bundleProductsSummary');
            Mage::unregister('bundleProductsSummary');
        }

        $aSkus = explode(',', $skus);
        foreach ($aSkus as $sku) {
            if ($sku) {
                $product = Mage::getModel("catalog/product")->loadByAttribute('sku', $sku);
                if (!$product) {
                    Mage::log(sprintf("Unable to find sku for add : %s", $sku), null, "bundleRules.log");
                } else {
                    $packageTypes = explode(',', $product->getPackageType());

                    //if product has associated multiple package types
                    if (isset($packageTypes[1])) {
                        foreach ($packageTypes as $type) {
                            $this->bundleProductsSummary[trim($type)][] = [
                                'name' => $product->getDisplayNameConfigurator() ?: $product->getName(),
                                'type' => 'add',
                                'productId' => (int)$product->getId(),
                                'promoDiscount' => $product->getMafDiscount() ?? 0,
                                'discountFormatted' => $product->getMafDiscount() ? "-" . Mage::app()->getStore()->formatPrice($product->getMafDiscount(), 1) : null,
                                'maf' => Mage::helper('tax')->getPrice($product, $product->getMaf(), false)
                            ];
                        }
                    } else {
                        $this->bundleProductsSummary[$product->getPackageType()][] = [
                            'name' => $product->getDisplayNameConfigurator() ?: $product->getName(),
                            'type' => 'add',
                            'productId' => (int)$product->getId(),
                            'promoDiscount' => $product->getMafDiscount() ?? 0,
                            'discountFormatted' => $product->getMafDiscount() ? "-" . Mage::app()->getStore()->formatPrice($product->getMafDiscount(), 1) : null,
                            'maf' => Mage::helper('tax')->getPrice($product, $product->getMaf(), false)
                        ];
                    }

                }
            }
        }
        Mage::register('bundleProductsSummary', $this->bundleProductsSummary);

        return true;
    }
}