<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Rules_Edit_Tab_Form
 */
class Vznl_Bundles_Block_Adminhtml_Rules_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setDestElementId('edit_form');
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldSet = $form->addFieldset("bundle_rules_form", array("legend" => Mage::helper("bundles")->__("Bundle Rules Information")));

        $fieldSet->addField("bundle_rule_name", "text", array(
            "label" => Mage::helper("bundles")->__("Bundle rule name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "bundle_rule_name",
        ));

        $fieldSet->addField("bundle_gui_name", "text", array(
            "label" => Mage::helper("bundles")->__("Bundle gui name"),
            "name" => "bundle_gui_name",
        ));

        $fieldSet->addField("description", "textarea", array(
            "label" => Mage::helper("bundles")->__("Description"),
            "name" => "description",
        ));

        $fieldSet->addField('website_id', 'multiselect', array(
            'label' =>  Mage::helper("bundles")->__('Website'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(),
            'name' => 'website_id',
            "class" => "required-entry",
            "required" => true
        ));

        if (Mage::getSingleton("adminhtml/session")->getRulesData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getRulesData());
            Mage::getSingleton("adminhtml/session")->getRulesData(null);
        } elseif (Mage::registry("bundle_rules_data")) {
            $form->setValues(Mage::registry("bundle_rules_data")->getData());
        }


        return parent::_prepareForm();
    }

    /**
     * Return the list of categories for dropdown;
     *
     * @return array -- [{id} => {name}, ....]
     */
    public function getCategoriesForDropdown()
    {
        /** @var Mage_Catalog_Model_Resource_Category_Collection $categoriesCollection */
        $categoriesCollection = Mage::getModel('catalog/category')->getCollection()
            ->addFieldToFilter('entity_id', array('nin' => [1, 2]))
            ->addAttributeToSort('name', Zend_Db_Select::SQL_ASC);
        $selectable_collection = array(-1 => 'Please select a category...');
        foreach ($categoriesCollection as $category) {
            $selectable_collection[$category->getId()] = $category->getPathAsString();
        }

        return $selectable_collection;
    }
}
