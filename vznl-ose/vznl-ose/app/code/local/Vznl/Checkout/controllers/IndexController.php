<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Dyna
 * @package     Vznl_Checkout
 */
class Vznl_Checkout_IndexController extends Mage_Core_Controller_Front_Action
{
    CONST PACKAGE_SUCCESS = 'success';
    CONST PACKAGE_WAITING = 'waiting';
    CONST PACKAGE_FAILED = 'failed';
    CONST PACKAGE_REFERRED = 'referred';
    CONST PACKAGE_REJECTED = 'rejected';
    CONST PEAL_CUSTOMER_PRIVATE = "RESIDENTIAL";
    CONST PEAL_CUSTOMER_BUSINESS = "BUSINESS";
    CONST CHECKOUT_DELIVERY_METHOD_CONTRACT = "SHOP";
    CONST FIXED_NUMBER_RESERVE = "Reserve";
    CONST DELIVERY_METHOD_SPLIT = "split";
    CONST DELIVERY_METHOD_DIRECT = "direct";
    CONST DELIVERY_METHOD_DELIVER = "deliver";
    CONST DELIVERY_METHOD_BILLING_ADDRESS = "billing_address";
    CONST DELIVERY_METHOD_OTHER_ADDRESS = "other_address";
    CONST DELIVERY_METHOD_OTHER_PICKUP = "pickup";
    CONST DELIVERY_ONE_DAY_TECH_INSTALL = "oneday_technician_install";

    /** @var Vznl_Checkout_Helper_Data $checkoutHelper */
    protected $checkoutHelper;

    /** @var Vznl_Checkout_Helper_Validation $checkoutValidationHelper */
    protected $checkoutValidationHelper;

    protected $_classActions = array(
        'saveContract',
        'saveContractPolling',
        'saveNewNetherlands',
        'saveNumberSelection',
        'saveDeviceSelection',
        'saveCreditCheck',
        'saveOverview',
        'saveOverviewPolling',
        'saveOverviewAfter',
        'saveCustomer',
        'saveNumberPorting',
        'saveIncomeTest',
        'saveDeliveryAddress',
        'savePaymentMethod',
        'saveFixedToMobile',
        'saveVodafoneHome',
        'updateCreditNote',
        'convertBic',
        'stockStores',
        'searchAddress',
        'searchCompany',
        'saveSuperOrder',
        'saveSuperOrderPolling',
        'cancelOrder',
        'cancelPendingOrder',
        'cancelOrderPolling',
        'retrieveNumberList',
        'saveFailedCreditCheck',
        'retryPolling',
        'redoCreditCheck',
        'redoCreditCheckPolling',
        'validateDate',
        'convertFirstname',
        'registerReturn',
        'registerReturnPolling',
        'createLongIban',
        'getLockInfo',
        'saveFixedNumber',
        'saveServiceAddress',
        'savePaymentAddress',
        'saveWrittenConsent',
        'saveFixedDeliveryAddress',
        'saveTechnicianDelivery',
        'saveFootprint',
        'showPealOrder',
        'saveChangeDeliveryAddress',
        'saveChangeIncomeTest',
    );
    private $_customer = null;

    private $dynaServiceHelper = null;
    private $dynaCoreServiceHelper = null;

    /**
     * @return Vznl_Core_Helper_Service
     */
    private function getCoreServiceHelper()
    {
        if ($this->dynaCoreServiceHelper === null) {
            $this->dynaCoreServiceHelper = Mage::helper('vznl_core/service');
        }
        return $this->dynaCoreServiceHelper;
    }

    /**
     * @return Vznl_Service_Helper_Data
     */
    private function getServiceHelper()
    {
        if ($this->dynaServiceHelper === null) {
            $this->dynaServiceHelper = Mage::helper('vznl_service');
        }
        return $this->dynaServiceHelper;
    }

    /**
     * Un Mask Data
     * @param array $data
     * @return array
     */
    private function unMaskData(
        array $data
    ):array
    {
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $map = [
            'id_number' => 'id_number',
            'contractant_id_number' => 'contractant_id_number',
            'account_no' => 'account_no',
            'account_no_mobile' => 'account_no',
            'account_no_fixed_different' => 'account_no'
        ];
        foreach ($map as $dataKey => $customerKey) {
            if (isset($data[$dataKey]) && strpos($data[$dataKey], Vznl_Customer_Model_Customer_Customer::MASK_CHAR) !== false) {
                $data[$dataKey] = $customerData->getData($customerKey);
            }
        }
        return $data;
    }

    /**
     * @return Mage_Core_Controller_Front_Action
     */
    public function preDispatch()
    {
        $this->checkoutHelper =  Mage::helper('vznl_checkout');
        $this->checkoutValidationHelper = Mage::helper('vznl_checkout/validation');

        return parent::preDispatch();
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Get current active quote instance
     *
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        if (Mage::getSingleton('customer/session')->getOrderEdit()) {
            $quote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
            if ($quote->getId()) {
                $this->_getCheckoutSession()->setLoadInactive()->setQuoteId($quote->getId());
            }
        }

        return $this->_getCheckoutSession()->getQuote();
    }

    /**
     * @param $action
     * @return bool
     */
    public function hasAction($action)
    {
        return in_array($action, $this->_classActions);
    }

    /**
     * Get one page checkout model
     *
     * @return Mage_Checkout_Model_Type_Multishipping
     */
    public function getCheckOut()
    {
        return Mage::getSingleton('checkout/type_multishipping');
    }

    /**
     * Get one page checkout model
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function getOnePage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    public function retrieveNumberListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                // Set successful response
                $result['error'] = false;
                $ctn = $request->getParam('current_ctn') ?? $request->getParam('last_ctn');
                $result['numbers'] = $this->checkoutHelper->getAllAvailablePhoneNumbers($ctn);
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    private function _getCustomerEmail($customer)
    {
        $emailAddressCsv = $customer->getData('additional_email');
        $emailAddressArr = explode(';', $emailAddressCsv);
        return $emailAddressArr[0];
    }

    public function registerReturnAction()
    {
        /** @var Vznl_Checkout_Helper_RegisterReturn $registerReturnHelper */
        $registerReturnHelper = Mage::helper('vznl_checkout/registerReturn');

        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $data = $request->getParam('returned', array());
                $triggerComplete = $request->getParam('trigger_complete', array());
                foreach ($data as $packageId => $packageData) {
                    $triggerCompleteBoolean = isset($triggerComplete[$packageId]) && $triggerComplete[$packageId];
                    $superOrderId = $this->getQuote()->getSuperOrderEditId();
                    $temporaryQueueItems = $this->getQuote()->getAllVisibleItems();

                    // Set the current agents name
                    $customerSession = Mage::getSingleton('customer/session');
                    $packageData['agent_name'] = $customerSession->getAgent(true)->getUsername();

                    $result = $registerReturnHelper->registerReturnForPackage($superOrderId, $packageId, $packageData, $triggerCompleteBoolean, $temporaryQueueItems);

                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );

                    return;
                }
            } catch (Exception $e) {
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                return;
            }
        }
        $result['error'] = true;
        $result['message'] = $this->__('Invalid package data');
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }

    public function registerReturnPollingAction()
    {
        $this->saveOverviewPollingAction();
    }

    public function convertFirstnameAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $input = $request->getParam('name');
                $result = array(
                    'error' => 'false',
                    'name' => $this->checkoutHelper->processInitials($input),
                );
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        } else {
            return;
        }
    }

    public function retryPollingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost() && Mage::helper('agent')->isTelesalesLine() === false) {
            try {
                $id = trim($request->getParam('polling_id'));
                if ($this->isJSON($id)) {
                    $pollingId = Mage::helper('core')->jsonDecode($id);
                } else {
                    $pollingId = $id;
                }

                /** @var Dyna_Job_Helper_Data $jobHelper */
                $jobHelper = Mage::helper('vznl_job');
                $queue = $jobHelper->getQueue();
                try {
                    $queue->retry($pollingId);
                    $result['pollingId'] = $pollingId;
                } catch (\Exception $e) {
                    Mage::getSingleton('core/logger')->logException($e);
                    $this->getServiceHelper()->returnServiceError($e);

                    return;
                }

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    public function saveContractPollingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $id = trim($this->getRequest()->getParam('id'));
                if (!$id || $id === 'undefined') {
                    $result = array(
                        'error' => false,
                        'performed' => true
                    );
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }
                /** @var Vznl_Job_Model_Pool $resultsPool */
                $resultsPool = Mage::getSingleton('vznl_job/pool');
                $response = $resultsPool->getResult($id);
                if ($response === null) {
                    $result = array(
                        'error' => false,
                        'performed' => false
                    );
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                } elseif ($response === false) {
                    $result = array(
                        'error' => true,
                        'message' => $this->__('There is no such job')
                    );
                    Mage::logException(new Exception('saveContractPollingAction: No such job for id' . $id));
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }

                if (!isset($response['process_result']['status']) || !in_array((string)$response['process_result']['status'], array('successfully completed', 'completed succesfully', 'completed with warnings', 'ProcessOrderCallSkipped'))) {
                    $error = isset($response['process_result']['description']) ? $response['process_result']['description'] : 'no error in response';
                    throw new Exception($error);
                }

                $result = array(
                    'error' => false,
                    'performed' => true
                );
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }

        }
    }

    /**
     * Checkout method saveContractAction()
     * @return void
     */
    public function saveContractAction():void
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                /** @var Vznl_Superorder_Model_Superorder $superOrder */
                $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($request->getParam('superorder_id'));

                $params = $request->getParam('contract');
                if (!$superOrder->getId()) {
                    Mage::throwException('No active superorder found.');
                }

                $serviceHelper = $this->getServiceHelper();
                /** @var Vznl_Service_Model_Client_ApprovalClient $approvalClient */
                $approvalClient = $serviceHelper->getClient('approval');

                /** @var Vznl_Service_Model_Client_LockManagerClient $lockClient */
                $lockClient = Mage::helper('omnius_service')->getClient('lock_manager');

                $deliveryOrders = $superOrder->getOrders(true)->getItems();

                try {
                    $lockInfo = $lockClient->doGetLockInfo($superOrder);
                    $lockUser = isset($lockInfo['lock']) ? ($lockInfo['lock']['user_i_d'] ?: null) : null;

                    $signed_contracts = $request->getParam('contracts');

                    $approveResult = false;
                    foreach ($deliveryOrders as $deliveryOrder) {
                        $incrementId = $deliveryOrder->getIncrementId();
                        if (isset($signed_contracts[$incrementId])) {
                            $approveResult = $approvalClient->approveOrder($superOrder, null, false, true, $lockUser, $incrementId);
                        }
                    }
                    if (!$approveResult) { // Approval not needed, then hand over the lock to ESB

                        $session = $this->_getCustomerSession();
                        $agent = $session->getAgent(true);
                        $agentId = $agent->getId();

                        if ($lockUser !=  Vznl_Service_Helper_Data::LOCK_USER_ESB) {
                            $lockClient->unLockSaleOrder($superOrder, $agentId);
                        }
                    }
                    $result['pollingId'] = $approveResult === false ? 'undefined' : $approveResult;
                } catch (\Exception $e) {
                    Mage::getSingleton('core/logger')->logException($e);
                    $this->getServiceHelper()->returnServiceError($e);
                    return;
                }

                /** @var Vznl_Checkout_Helper_Pdf $pdfHelper */
                $pdfHelper = Mage::helper('vznl_checkout/pdf');

                $sendContract = false;
                /** @var Vznl_Checkout_Model_Sales_Order $deliveryOrder */
                foreach ($deliveryOrders as $deliveryOrder) {
                    $incrementId = $deliveryOrder->getIncrementId();
                    if (isset($params['sendmail_'.$incrementId]) && ($params['sendmail_'.$incrementId] == 1)) {
                        $sendContract = true;
                    }
                    if (isset($params['signed_current_package_'.$incrementId])) {
                        if ($deliveryOrder->getId() && !$deliveryOrder->getContractSignDate()) {
                            $deliveryOrder->setContractSignDate(date("Y-m-d H:i:s"));
                            $deliveryOrder->save();
                        }
                    }
                }

                if ($sendContract) {
                    $this->sendContract($superOrder, $deliveryOrders, $pdfHelper);
                }

                Mage::getSingleton('customer/session')->setOrderEditMode(false);
                Mage::getSingleton('customer/session')->setLastSuperOrder(null); //clear it

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                return;
            }
        }
    }

    /**
     * Send contract
     *
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param array $deliveryOrders
     * @param Vznl_Checkout_Helper_Pdf $pdfHelper
     * @return void
     * @throws Exception
     */
    protected function sendContract(
        Vznl_Superorder_Model_Superorder $superOrder,
        array $deliveryOrders,
        Vznl_Checkout_Helper_Pdf $pdfHelper
    ):void
    {
        $dealerId = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $from = $dealer->getSenderDomain();
        try {
            $emailHelper = Mage::helper('communication/email');
            $customer = Mage::getModel('customer/customer')->load($superOrder->getCustomerId());
            $videoLink = $superOrder->hasRetention() ? Mage::getStoreConfig('email_configuration/email_configuration/order_confirmation_video_link_retention') : Mage::getStoreConfig('email_configuration/email_configuration/order_confirmation_video_link_acquisition');
            // Setup name
            $email = $superOrder->getCorrespondanceEmail();

            if ($email) {
                if (Mage::app()->getStore()->getCode() == Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE) {
                    $emailCode = $customer->getIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_BELCOMPANY_CONTRACT_BUSINESS : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_BELCOMPANY_CONTRACT;
                } elseif (Mage::app()->getStore()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                    $emailCode = $customer->getIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_INDIRECT_CONTRACT_BUSINESS : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_INDIRECT_CONTRACT;
                } else {
                    $emailCode = $customer->getIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCOPY_CONTRACT_BUSINESS : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCOPY_CONTRACT;
                }

                // Set the attachments
                $attachments = [];
                $agentHelper = Mage::helper('agent');
                $isIndirect =  $agentHelper->isIndirectStore();
                $addContractAttachment = false;
                // Contracts
                /** @var Vznl_Checkout_Model_Sales_Order $deliveryOrder */
                foreach ($deliveryOrders as $deliveryOrder) {
                    $addContractAttachment = false;
                    if ($deliveryOrder->hasFixed()) {
                        if (!$isIndirect) {
                            $addContractAttachment = true;
                        }
                        $contractOutput = Mage::helper('vznl_checkout/sales')->generateFixedContract($deliveryOrder, true, true);
                    } else {
                        $isMobile = true;
                        $addContractAttachment = true;
                        $contractOutput = Mage::helper('vznl_checkout/sales')->generateContract($deliveryOrder, true, true);
                    }
                    if ($addContractAttachment) {
                        $attachments[] = $pdfHelper->saveContract($contractOutput, $deliveryOrder->getIncrementId(), 'pdf', true);
                    }
                }

                // Terms
                if ($isMobile) {
                    $attachments[] = $pdfHelper->getTermsAndConditionsPath();
                }

                // Loan overview
                /** @var Vznl_Checkout_Model_Sales_Order $deliveryOrder */
                foreach ($deliveryOrders as $deliveryOrder) {
                    // If loan is required get the pdf
                    if ($deliveryOrder->isLoanRequired() && ($loanPdf = $pdfHelper->saveLoanOverview($deliveryOrder, true))) {
                        $loanOverviews[] = $loanPdf;
                    }
                }
                foreach ($loanOverviews as $loanOverview) {
                    $attachments[] = $loanOverview;
                }

                $contractParametes = [
                    'email_code' => $emailCode,
                    'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
                    'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),
                    'parameters' => [
                        'Link_OrderStatusPage' => Mage::helper('vznl_checkout/email')->generateOrderUrl($superOrder, $customer),
                        'Link_Video' => $videoLink,
                        'Dealer'     => $emailHelper->getCommunicationDealerData($superOrder),
                        'Customer'   => $emailHelper->getCommunicationCustomerData($superOrder),
                        'Order'      => $emailHelper->getCommunicationOrderData($superOrder),
                    ],
                    'receiver'     => $email,
                    'from'         => $from,
                    'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
                    'attachments'  => array_filter($attachments),
                ];

                Mage::getSingleton('communication/pool')->add($contractParametes);
                Mage::log($contractParametes, null, 'contractParametes.log');

            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            throw $e;
        }
    }

    public function searchAddressAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            try {
                if ($request->getPost('postcode') == "undefined"
                    || $request->getPost('houseno') == "undefined"
                    || $request->getPost('addition') == "undefined"
                    || ! is_numeric($request->getPost('houseno'))
                ) {
                    $result['error'] = true;
                    $result['message'] = $this->__("Invalid data");
                } else {
                    $result['error'] = false;

                    /** @var Vznl_Postcode_Helper_Data $postcodeHelper */
                    $postcodeHelper = Mage::helper('postcode');
                    $address = $postcodeHelper->getPostcodeAddressData((string)$request->getPost('postcode'), (int)$request->getPost('houseno'));
                    if ($address) {
                        $result['fields'] = array(
                            "street" => $address["street"],
                            "houseno" => $address["house_number"],
                            "postcode" => $address["postcode"],
                            "city" => $address["town"]
                        );
                    } else {
                        $result['error'] = true;
                    }
                }
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__('Combination of HouseNumber and ZipCode not found!');
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
            }

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );

            return;
        }
    }

    public function searchCompanyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            try {
                $result['error'] = false;
                $companyCoC = trim($request->getPost('company_coc'));
                if (!$companyCoC) {
                    throw new Exception($this->__('KVK number is required'));
                }
                try {
                    /** @var Vznl_Coc_Helper_Data $cocHelper */
                    $cocHelper = Mage::helper('coc');
                    $cocCompany = $cocHelper->getCoCData($companyCoC);
                } catch (\Exception $e) {
                    $this->getServiceHelper()->returnServiceError($e);
                    return;
                }

                // Checks if a company is returned
                if (!$cocCompany || empty($cocCompany)) {
                    $result['error'] = true;
                    $result['message'] = $this->__('COC number not known in COC database');
                } else {
                    $result['k_v_k_number'] = isset($cocCompany['k_v_k_number']) ? $cocCompany['k_v_k_number'] : '';
                    $result['company_name'] = isset($cocCompany['company_name']) ? $cocCompany['company_name'] : '';
                    $result['houseno'] = isset($cocCompany['houseno']) ? $cocCompany['houseno'] : '';
                    $result['postcode'] = isset($cocCompany['postcode']) ? $cocCompany['postcode'] : '';
                    $result['company_vat_id'] = isset($cocCompany['company_vat_id']) ? $cocCompany['company_vat_id'] : '';
                    $result['date'] = isset($cocCompany['date']) ? $cocCompany['date'] : '';
                }
            } catch (Exception $e) {
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );

            return;
        }
    }

    public function cancelOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $superOrderNo = trim($request->getPost('super_order'));
                if ($superOrderNo) {
                    /** @var Vznl_Superorder_Model_Superorder $superOrder */
                    $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo);
                    if (!$superOrder || !$superOrder->getId()) {
                        Mage::getSingleton('core/logger')->logException(new Exception('Invalid Order id during cancelOrder'));
                        throw new Exception($this->__('Invalid Order id'));
                    }
                    $superOrder->cancel();
                    $this->checkoutHelper->exitSuperOrderEdit();
                    // Set successful response
                    $result['error'] = false;
                    $result['polling_id'] = $superOrder->getData('polling_id');
                    $result['message'] = "Step data successfully saved";
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                } else {
                    Mage::getSingleton('core/logger')->logException(new Exception('Invalid Order id during cancelOrder'));
                    throw new Exception($this->__('Invalid Order id'));
                }
            } catch (Exception $e) {
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }
        }
    }

    /**
     * Cancels in DA an order in pending state
     */
    public function cancelPendingOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $superOrderNo = trim($request->getPost('super_order'));
                if ($superOrderNo) {
                    /** @var Vznl_Superorder_Model_Superorder $superOrder */
                    $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo);
                    if (!$superOrder || !$superOrder->getId() || !$superOrder->hasPendingPackages()) {
                        Mage::getSingleton('core/logger')->logException(new Exception('Invalid Order id during cancelOrder'));
                        throw new Exception($this->__('Invalid Order id'));
                    }
                    $superOrder->cancelPendingOrder();
                    $this->checkoutHelper->exitSuperOrderEdit();
                    // Set successful response
                    $result['error'] = false;
                    $result['message'] = "Step data successfully saved";
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );

                    return;
                } else {
                    Mage::getSingleton('core/logger')->logException(new Exception('Invalid Order id during cancelOrder'));
                    throw new Exception($this->__('Invalid Order id'));
                }
            } catch (Exception $e) {
                $this->getServiceHelper()->returnServiceError($e);

                return;
            }
        }
    }

    public function cancelOrderPollingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $id = trim($request->getPost('id'));
                /** @var Vznl_Job_Model_Pool $resultsPool */
                $resultsPool = Mage::getSingleton('vznl_job/pool');
                $response = $resultsPool->getResult($id);
                if ($response === null) {
                    $result = array(
                        'error' => false,
                        'performed' => false
                    );
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                } elseif ($response === false) {
                    $result = array(
                        'error' => true,
                        'message' => $this->__('There is no such job')
                    );
                    Mage::logException(new Exception('cancelOrderPollingAction: No such job for id' . $id));
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }

                /** @var Vznl_Superorder_Model_Superorder $superOrder */
                $sessionData =  $this->_getCustomerSession()->getData($id);
                $superOrder = $sessionData['superorder'];
                $superOrder->postProcessOrderCancel($id);
                $this->checkoutHelper->exitSuperOrderEdit();

                // Set successful response
                $result['error'] = false;
                $result['performed'] = true;
                $result['polling_id'] = $superOrder->getData('polling_id');
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                $this->_getCustomerSession()->unsData($id);
                return;
            } catch (Exception $e) {
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }
        }
    }

    // Hollands Nieuwe
    public function saveNewNetherlandsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $data = $request->getPost('new_netherlands');

                $errors = $this->checkoutValidationHelper->validateNewNetherlands($data);

                if ($errors) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
                }

                if ($data) {
                    $this->updateNewHollandPackage($data);
                }

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());

                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    /**
     * Update package with newHolland data
     *
     * @param $data
     */
    protected function updateNewHollandPackage($data)
    {
        $orders = Mage::getSingleton('core/session')->getOrderIds();

        foreach ($orders as $order) {
            $dbOrder = Mage::getModel('sales/order')->loadByAttribute('increment_id', $order);
            $updatedPackages = array();
            foreach ($dbOrder->getAllItems() as $item) {
                if (isset($data[$item->getPackageId()]) && !in_array($item->getPackageId(), $updatedPackages) && isset($data[$item->getPackageId()]['order_number'])) {
                    $packageModel = Mage::getModel('package/package')
                        ->getPackages($dbOrder->getSuperorderId(), null, $item->getPackageId())
                        ->getFirstItem();
                    if ($packageModel->getId()) {
                        $packageModel->setNewHolland($data[$item->getPackageId()]['order_number'])->save();
                    }
                    $updatedPackages[$item->getPackageId()] = $item->getPackageId();
                }
            }
        }
    }

    public function saveNumberSelectionAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                /** Save Mobile Number Selection Begins */
                $data = $request->getPost('numberselection');
                $superOrderNo = $request->getPost('order_edit');
                $errors = $this->checkoutValidationHelper->validateNumberSelectionData($data);

                if ($errors) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
                    // return;
                }

                if ($data && empty($superOrderNo)) {
                    $orders = array();
                    $orderIds = Mage::getSingleton('core/session')->getOrderIds();
                    $sessionOrderIds = !empty($orderIds) ? $orderIds : [];
                    foreach ($sessionOrderIds as $orderId) {
                        $orders[] = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderId);
                    }
                    $this->_updateNumberSelection($orders, $data);
                } elseif ($data && !empty($superOrderNo)) {
                    $superOrderId = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo)->getId();
                    $orders = Mage::getModel('sales/order')->getNonEditedOrderItems($superOrderId);
                    $this->_updateNumberSelection($orders, $data);
                }
                /**  Save Mobile Number Selection Ends */
                /**  Save Mobile Device Selection Begins */
                $deviceData = $request->getPost('deviceselection');
                $superOrderNo = $request->getPost('order_edit');

                if ($deviceData && empty($superOrderNo)) {
                    $orders = array();
                    $orderIds = Mage::getSingleton('core/session')->getOrderIds();

                    foreach (Mage::getSingleton('core/session')->getOrderIds() as $orderId) {
                        $orders[] = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderId);
                    }
                    $this->_updateDeviceSelection($orders, $deviceData);
                } elseif ($deviceData && !empty($superOrderNo)) {
                    $superOrderId = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo)->getId();
                    $orders = Mage::getModel('sales/order')->getNonEditedOrderItems($superOrderId);
                    $this->_updateDeviceSelection($orders, $deviceData);
                }
                /**  Save Mobile Device Selection Ends */
                /**  Save Fixed Number Selection Begins*/
                $FixedData = $request->getPost();
                $FixedErrors = $this->checkoutValidationHelper->validateFixedNumberSelectionData($FixedData);
                if ($FixedErrors) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($FixedErrors));
                    //return;
                }
                $fixedTelephoneNumber = $this->getRequest()->getParam('fixed_telephone_number');
                $fixedExtraTeleNumber = $this->getRequest()->getParam('fixed_extra_telephone_no');
                $hardwareName = $this->getRequest()->getParam('hardware_info');
                $captureEanNumber = $this->getRequest()->getParam('capture_ean_number');
                $serialNumbers = $this->getRequest()->getParam('serial_number');
                if (empty($superOrderNo)) {
                    $fixedOrders = array();
                    $orderIds = Mage::getSingleton('core/session')->getOrderIds();
                    //print_r($orderIds);
                    $sessionOrderIds = !empty($orderIds) ? $orderIds : [];

                    foreach ($sessionOrderIds as $orderId) {
                        $fixedOrders = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderId);
                        if (isset($fixedTelephoneNumber)) {
                            $fixedOrders->setFixedTelephoneNumber($fixedTelephoneNumber);
                            $fixedOrders->setFixedExtraTelephoneNo($fixedExtraTeleNumber);
                            $fixedOrders->save();
                        }
                    }
                } elseif (!empty($superOrderNo)) {
                    $superOrderId = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo)->getId();
                    $fixedOrders = Mage::getModel('sales/order')->getNonEditedOrderItems($superOrderId);
                    if (!empty($fixedTelephoneNumber)) {
                        $fixedOrders->setFixedTelephoneNumber($fixedTelephoneNumber);
                        $fixedOrders->setFixedExtraTelephoneNo($fixedExtraTeleNumber);
                        $fixedOrders->save();
                    }
                }

                $saveFixedHardware = isset($hardwareName) || isset($captureEanNumber) || isset($serialNumbers);
                if (isset($fixedOrders) && $saveFixedHardware) {
                    $orderItem = $fixedOrders->getAllItems();
                    foreach ($orderItem as $key => $fixedItem) {
                        $sku = $fixedItem->getSku();
                        if (isset($hardwareName[$sku])) {
                            $fixedItem->setHardwareName($hardwareName[$sku]);
                        }
                        if (isset($captureEanNumber[$sku])) {
                            $fixedItem->setEanNumber($captureEanNumber[$sku]);
                        }
                        if (isset($serialNumbers[$sku])) {
                            foreach ($serialNumbers[$sku] as $serialNumberType => $serialNumberValue){
                                switch ($serialNumberType) {
                                    case 'smartcard':
                                        $fixedItem->setSerialNumber($serialNumberValue);
                                        break;
                                    case 'box':
                                        $fixedItem->setBoxSerialNumber($serialNumberValue);
                                        break;
                                }
                            }
                        }
                        $fixedItem->save();
                    }
                    $fixedOrders->save();
                }
                /**  Save Fixed Number Selection Ends*/

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    public function saveDeviceSelectionAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $data = $request->getPost('deviceselection');
                $superOrderNo = $request->getPost('order_edit');

                if ($data && empty($superOrderNo)) {
                    $orders = array();
                    foreach (Mage::getSingleton('core/session')->getOrderIds() as $orderId) {
                        $orders[] = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderId);
                    }
                    $this->_updateDeviceSelection($orders, $data);
                } elseif ($data && !empty($superOrderNo)) {
                    $superOrderId = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo)->getId();
                    $orders = Mage::getModel('sales/order')->getNonEditedOrderItems($superOrderId);
                    $this->_updateDeviceSelection($orders, $data);
                }

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                //$result['contract'] = $this->getLayout()->createBlock('core/template')->setShowStep(true)->setTemplate('checkout/cart/steps/save_contract.phtml')->toHtml();
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    public function saveFailedCreditCheckAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $superOrderNo = trim($request->getPost('order_no'));
                $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo);
                $initialQuote = $superOrder->getOriginalQuote();

                if ($initialQuote->getId()) {
                    if ($this->_getCustomerSession()->getFailedSuperOrderId() != $superOrder->getId()) {
                        $initialQuote->setSaveAsCart(true);
                        $this->_createNewQuoteOnServiceCallFail($initialQuote, $superOrder->getId());
                    }
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
            }
        }
//        return;
    }

    public function redoCreditCheckPollingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $id = trim($request->getPost('id'));
                /** @var Vznl_Job_Model_Pool $resultsPool */
                $resultsPool = Mage::getSingleton('vznl_job/pool');
                $response = $resultsPool->getResult($id);
                if ($response === null) {
                    $result = array(
                        'error' => false,
                        'performed' => false
                    );
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                } elseif ($response === false) {
                    $result = array(
                        'error' => true,
                        'message' => $this->__('There is no such job')
                    );
                    Mage::logException(new Exception('redoCreditCheckPollingAction: No such job for id' . $id));
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }
                $this->checkoutHelper->handleProcessOrderResponse($response); //update package status
                $this->_getCustomerSession()->setFailedSuperOrderId(null);
                // Set successful response
                $result['error'] = false;
                $result['performed'] = true;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }
        }
    }

    public function redoCreditCheckAction()
    {
        $request = $this->getRequest();
        $result = array();

        if ($request->isPost()) {
            $superOrderNo = $request->getParam('superorder_id');
            $oldSuperOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo);

            $orders = $oldSuperOrder->getOrders(true);

            $superOrder = Mage::getModel('superorder/superorder')->createNewSuperorder();
            $superOrder->setParentId($oldSuperOrder->getParentId());
            $superOrder->setAgentId($oldSuperOrder->getAgentId());
            $superOrder->setDealerId($oldSuperOrder->getDealerId());
            $superOrder->save();

            //connect old packages with the new superOrder
            $packages = Mage::getModel('package/package')->getOrderPackages($oldSuperOrder->getId());
            $orderIds = array();

            foreach ($packages->getItems() as $package) {
                $package->setData('order_id', $superOrder->getId());
                $package->setData('creditcheck_status', Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL);
                $package->setData('status', Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_INITIAL);
                $package->save();
            }

            $orderIncrements = array();
            foreach ($orders as $order) {
                $order->setSuperorderId($superOrder->getId());
                $order->setIncrementId(null);
                $order->save();
                $orderIds[$order->getId()] = $order->getIncrementId();
                $orderIncrements[$order->getId()] = Mage::helper('vznl_checkout')->__('Order number') . ':' . $order->getIncrementId();
            }

            //cancel old super order
            $oldSuperOrder->setOrderStatus(Vznl_Superorder_Model_Superorder::SO_CANCELLED);
            $oldSuperOrder->save();

            $url = Mage::getUrl('checkout/cart/print', array('superorder_id' => $superOrder->getOrderNumber()));
            $result['contract_data'] = array(
                'contract_url' => $url,
                'increment_ids' => $orderIncrements
            );

            $result['error'] = false;
            $result['superorder_id'] = $superOrder->getId();
            $result['superorder_number'] = $superOrder->getOrderNumber();

            /**
             * Call CreateOrder
             */
            try {
                $amountToPay = $this->_getCustomerSession()->getToPayAmount();
                $serviceHelper = $this->getServiceHelper();
                /** @var Omnius_Service_Model_Client_EbsOrchestrationClient $client */
                $client = $serviceHelper->getClient('ebs_orchestration');

                $customer = $this->getQuote()->getCustomer();
                $pollingId = $client->processCustomerOrder($superOrder, null, null, null, null, null, null, null, null, $amountToPay, false, false, $customer); //call esb

                Mage::getSingleton('core/session')->setOrderIds($orderIds);
            } catch (LogicException $e) {
                $this->_createNewQuoteOnServiceCallFail($this->getQuote(), $superOrder->getId());
                $this->getServiceHelper()->returnServiceError($e);
                return;
            } catch (Exception $e) {
                $this->_createNewQuoteOnServiceCallFail($this->getQuote(), $superOrder->getId());
                Mage::getSingleton('core/logger')->logException($e);
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }
            $result['polling_id'] = $pollingId;
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        }

        $result['error'] = true;
        $result['message'] = "Improper request type";
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
        return $result;
    }

    public function saveCreditCheckAction()
    {
        $request = $this->getRequest();
        $orderNo = strtok($request->getPost('creditSuperOrderId'), '.');
        $allOrders = Mage::getModel('vznl_superorder/superorder')->getSuperOrderByOrderNumber($orderNo);
        $orders = Mage::getModel('sales/order')->getNonEditedOrderItems($allOrders->getId());
        foreach ($orders as $order) {
             $orderId[$order->getId()] = $order->getIncrementId();
        }
        if ($request->isPost()) {
            try {
                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                if (!Mage::helper('agent')->isTelesalesLine()) {
                    // Also return the Number Selection step data, because it was updated by the ESB
                    if (isset($orders)) {
                        $result['html'] = $this->getLayout()->createBlock('vznl_checkout/cart')->setShowStep(true)->setOrderIds($orderId)->setTemplate('checkout/cart/steps/save_number_selection.phtml')->toHtml();
                    } else {
                        $result['html'] = $this->getLayout()->createBlock('vznl_checkout/cart')->setShowStep(true)->setTemplate('checkout/cart/steps/save_number_selection.phtml')->toHtml();
                    }
                    /*if (Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                        $result['device_selection'] = $this->getLayout()->createBlock('vznl_checkout/cart')->setShowStep(true)->setTemplate('checkout/cart/steps/save_device_selection.phtml')->toHtml();
                    }*/
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    public function updateCreditNoteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $quote = $this->getQuote();

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }

                $data = $request->getPost();

                if (!$data['packageId']) {
                    throw new Exception($this->__('Invalid package id'));
                }

                if (!$data['creditNote']) {
                    throw new Exception($this->__('Invalid credit note'));
                }

                $packageModel = Mage::getModel('package/package')
                    ->getPackages(null, $quote->getId(), $data['packageId'])
                    ->getFirstItem();

                if (!$packageModel->getId()) {
                    throw new Exception($this->__('Invalid package id'));
                }

                $packageModel->setCreditNote($data['creditNote'])->save();

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    public function getLockInfoAction()
    {
        try {
            $superorderId = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit())->getSuperOrderEditId();

            $orderLockInfo = Mage::getSingleton('customer/session')->getOrderLockInfo() ?: [];
            $lockInfo = isset($orderLockInfo[$superorderId]) ? $orderLockInfo[$superorderId] : Mage::helper('vznl_checkout')->getLockInfo($superorderId);

            $orderLockInfo[$superorderId] = $lockInfo;
            Mage::getSingleton('customer/session')->setOrderLockInfo($orderLockInfo);
            $superOrder = Mage::getModel('superorder/superorder')->load($superorderId);
            $result = [
                'error' => false,
                'html' => $this->getLayout()->createBlock('core/template')->setTemplate('checkout/cart/partials/lock_info.phtml')->setSuperorder($superOrder)->toHtml()
            ];
        } catch (Exception $e) {
            Mage::logException($e);
            $result = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }

    /**
     * Parse input and sanitise any user inputted data
     *
     * @return Mage_Core_Controller_Request_Http
     */
    public function getRequest()
    {
        if (isset($this->_requestSanitised)) {
            return $this->_requestSanitised;
        }
        $request = parent::getRequest();
        $post = $request->getPost();
        array_walk_recursive($post, function (&$value) {
            $value = strip_tags($value);
        });
        $request->setPost($post);
        $this->_requestSanitised = $request;
        return $request;
    }

    public function saveCustomerAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                // Validate there is a quote
                $quote = $this->getQuote();
                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }

                // Check if customer is already logged in
                $customer = $this->_getCustomerSession()->getCustomer();
                if (!$customer->getId()) {
                    $acquisition = true;
                    $data = $this->_buildAndValidateCustomerData($request, $quote);
                    // Save all customer and billing address information to the quote
                    $result = $this->getOnepage()->saveBilling($data, null);

                    if ($result) {
                        throw new Exception(Mage::helper('core')->jsonEncode($result));
                    }
                } else {
                    // When customer is logged in check if it is hardware only for this customer
                    $acquisition = false;
                    $customerData = $request->getPost('customer', array());
                    $data = $this->_buildAndValidateCustomerData($request, $quote);
                    //unmask data
                    $data = $this->unMaskData($data);
                    if ($customer->getIsProspect() || isset($customerData['hardware_only']) && $customerData['hardware_only'] == $customer->getId()) {
                        // Validate input data
                        if ($customer->getIsProspect()) {
                            // Also save all customer and billing address information to the quote
                            $result = $this->getOnepage()->saveBilling($data, null);
                        }
                        foreach ($data as $key => $value) {
                            // Map data to the customer
                            $customer->setData($key, $value);
                        }
                        $customer->save();
                    }else{
                        $result = $this->getOnepage()->saveBilling($data, null);
                        foreach ($data as $key => $value) {
                            // Map data to the customer
                            $customer->setData($key, $value);
                        }
                        $customer->save();
                    }
                    $emailOverwrite = trim($request->getParam('email_overwrite'));
                    if ($emailOverwrite) {
                        if (Mage::helper('dyna_validators')->validateEmailSyntax($emailOverwrite)) {
                            $this->_getCustomerSession()->setNewEmail($emailOverwrite);

                            $emails = explode(';', $customer->getAdditionalEmail());
                            $emails[0] = $emailOverwrite;
                            $customer->setEmailOverwritten(1)->setAdditionalEmail(implode(';', $emails))->save();
                        } else {
                            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode(['email_overwrite' => $this->__('Please enter a valid email address. For example johndoe@domain.com.')]));
                        }
                    } else {
                        $this->_getCustomerSession()->setNewEmail(null);
                    }
                }

                // When store is Indirect, we simulate delivery to billing address and payment already paid
                if (Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                    if(is_array($data)) {
                        $data['deliver']['address'] = $this->checkoutHelper->getVirtualDeliveryAddress();
                        $data['payment'] = 'alreadypaid';
                        $quote->setMobileOneoffPayment($data['payment']);
                        $this->_saveDeliveryAddressData($data);
                    } else {
                        $shipData = array();
                        $shipData['deliver']['address'] = $this->checkoutHelper->getVirtualDeliveryAddress();
                        $shipData['payment'] = 'alreadypaid';
                        $quote->setMobileOneoffPayment($shipData['payment']);
                        $this->_saveDeliveryAddressData($shipData);
                    }
                }

                $isFixed = $quote->hasFixed();
                $isMobile = $quote->hasMobilePackage();
                if ($isFixed) {
                    $privacyCheckRequired = $request->getParam('privacy_check_required');
                    $quote->setData('privacy_check_required', $privacyCheckRequired);
                    $privacyCustomerData = $request->getParam('privacy_customerdata');
                    if ($privacyCustomerData) {
                        $quote->setData('privacy_customerdata', $request->getParam('privacy_customerdata'));
                    } elseif ($privacyCheckRequired) {
                        $quote->setData('privacy_customerdata', null);
                        $result['error'] = true;
                        $result['message'] = "please check privacy";
                        $result['fields']['privacy_customerdata'] = $this->__('please accept privacy check');
                        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                            Mage::helper('core')->jsonEncode($result)
                        );
                        return;
                    }
                }

                $quote->setData('additional_email', isset($data['additional_email']) ? $data['additional_email'] : '');
                $quote->setData('customer_id_type', isset($data['id_type']) ? $data['id_type'] : '');
                $quote->setData('customer_id_number', isset($data['id_number']) ? $data['id_number'] : '');
                $quote->setData('customer_valid_until', isset($data['valid_until']) ? $data['valid_until'] : '');
                if($isFixed && !$isMobile){
                    $quote->setData('customer_issuing_country','NL');
                }
                else{
                    $quote->setData('customer_issuing_country', isset($data['issuing_country']) ? $data['issuing_country'] : '');
                }
                $quote->setData('ziggo_telephone', isset($data['ziggo_telephone']) ? $data['ziggo_telephone'] : '');
                $quote->setData('ziggo_email', isset($data['ziggo_email']) ? $data['ziggo_email'] : '');

                if ($isFixed) {
                    $quote->setData('privacy_add_phone_book', isset($data['privacy_add_phone_book']) ? $data['privacy_add_phone_book'] : '');
                    $quote->setData('privacy_block_caller_id', isset($data['privacy_block_caller_id']) ? $data['privacy_block_caller_id'] : '');
                    $quote->setData('privacy_add_info', isset($data['privacy_add_info']) ? $data['privacy_add_info'] : '');
                    $quote->setData('privacy_hide_number', isset($data['privacy_hide_number']) ? $data['privacy_hide_number'] : '');
                }

                $quote->setCustomStatus(Vznl_Checkout_Model_Sales_Quote::VALIDATION);
                if ($request->getPost('current_step') && !$request->getParam('is_offer')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }

                if (!empty($data['otherAddress']['country_id']) && $data['otherAddress']['country_id'] != 'NL') {
                    $result['skipHomeDelivery'] = true;
                }

                $quote->save();

                // Check and save offer
                if ($request->getParam('is_offer') == 1) {
                    $offerEmail = $request->getParam('offer_email');
                    if (!Mage::helper('dyna_validators')->validateEmailSyntax($offerEmail)) {
                        throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode(['email_offer' => $this->__('Please enter a valid email address. For example johndoe@domain.com.')]));
                    }

                    if ($this->createOffer($offerEmail)) {
                        $result['is_offer'] = true;
                        // Unload customer session after sending the quotation - new business customer
                        if ($acquisition) {
                            $customerHelper = Mage::helper('vznl_customer');
                            $customerHelper->unloadCustomer();
                        }
                    }
                }

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }

        $this->_redirect('/');
    }

    /**
     * @param string $emailToSend
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    private function createOffer($emailToSend = null)
    {
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();
        // Get SOC codes for each package
        /** @var Vznl_Package_Model_Package[] $packages */
        $packages = Mage::getModel('package/package')
            ->getPackages(null, $quote->getId());
        foreach ($packages as $package) {
            // Get SOC codes only if package contains subscription
            if ($package->hasSubscription($quote)) {
                $socCodes = Mage::helper('multimapper')->validateSocCodesByPackageFromQuote($quote, $package->getPackageId());
                $package->setSocCodes(serialize($socCodes));
                $package->save();
            }
        }

        // Mark quote as offer
        $quote->setIsOffer(1)
            ->setChannel(Mage::app()->getWebsite()->getCode());

        //Unset the saved shopping cart.
        Mage::helper('vznl_checkout')->unsetSavedCart($quote->getQuoteParentId());

        $customer = null;
        if (!empty($quote->getCustomer())) {
            /** @var Omnius_Customer_Model_Customer_Customer $customer */
            $customer = $quote->getCustomer();
        }

        if (!$customer || !$customer->getId()) {
            // Create the customer in database if it doesn't exist
            $customer = $this->_addCustomer();
            $quote->setCustomer($customer)->setCustomerId($customer->getId());
            // API call to create a prospect mobile customer - elastic search.
            Mage::helper('vznl_customer/search')->createApiCustomer($customer, $quote->getBillingAddress(), $quote, 'SaveCheckoutFields');
        }

        // Send email when offer is created
        $emailToSend = $emailToSend ?? $customer->getCorrespondanceEmail();
        $check = Mage::helper('vznl_checkout/email')->sendConfirmShoppingCart($quote, $emailToSend);
        if (!$check) {
            Mage::getSingleton('core/session')->setGeneralError(
                $this->checkoutHelper->__('Failed to send email to the provided address') . '. ' .
                $this->checkoutHelper->__('The offer has been successfully saved')
            );
        } else {
            Mage::getSingleton('core/session')->setGeneralError(
                $this->checkoutHelper->__('The offer has been successfully saved')
            );
        }

        // Create new quote and remove the offer quote from the shopping cart
        $this->checkoutHelper->createNewQuote();
        $this->checkoutHelper->setItemPriceHistory($quote);

        return $quote;
    }

    public function saveNumberPortingAction()
    {
        /** @var Vznl_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('vznl_checkout');
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $data = $request->getPost();
                $errors = $this->checkoutValidationHelper->validateFixedNumberSelectionData($data);
                if ($errors) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
                    // return;
                }

                /** @var Vznl_Checkout_Model_Sales_Quote $quote */
                $quote = $this->getQuote();
                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }

                $changedPackages = [];
                $numberPortingData = $request->getPost('numberporting', array());

                foreach ($numberPortingData as $packageId => $data) {
                    /** @var Vznl_Package_Model_Package $package */
                    $package = $quote->getPackageById($packageId);

                    // Check if number porting is required, if not skip it
                    if (!isset($data['check']) || $data['check'] == '0') {
                        $checkoutHelper->clearNumberPortingData($package);
                        continue;
                    }

                    $hasChanges = $checkoutHelper->handleNumberPortingData($package, $data);
                    if ($hasChanges) {
                        $changedPackages[$packageId] = true;
                    }
                }

                if ($request->getPost('current_step')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }

                $quote->setTotalsCollectedFlag(true)->save();

                // Save fixed number porting details to quote
                $numberFixedPortingData = $request->getPost('numberportingfixed', array());

                foreach ($numberFixedPortingData as $packageId => $data) {
                    /** @var Vznl_Package_Model_Package $package */
                    $package = $quote->getPackageById($packageId);

                    $package->setFixedNumberPortingStatus(isset($data['check']) ? $data['check'] : '0')
                        ->setFixedFirstLinePhone(isset($data['first_line']) ? $data['first_line'] : null)
                        ->setFixedSecondLinePhone(isset($data['second_line']) ? $data['second_line'] : null)
                        ->save();
                }

                // Save fixed over stappen details to quote
                $fixedOverStappen = $request->getPost('switchprovider', array());

                foreach ($fixedOverStappen as $packageId => $data) {
                    /** @var Vznl_Package_Model_Package $package */
                    $package = $quote->getPackageById($packageId);

                    $package->setFixedOverstappenStatus(isset($data['check']) ? $data['check'] : '0')
                        ->setFixedOverstappenCurrentProvider(isset($data['current_provider']) ? $data['current_provider'] : null)
                        ->setFixedOverstappenDeliveryDate(isset($data['delivery_wish_date']) ? $data['delivery_wish_date'] : null)
                        ->setFixedOverstappenContractId(isset($data['contract_id']) ? $data['contract_id'] : null)
                        ->setFixedOverstappenTerminationFee(isset($data['termination_fee']) ? $data['termination_fee'] : '0')
                        ->save();
                }

                //Fixed mobile number update in quote
                $fixedTelephoneNumber = $this->getRequest()->getParam('fixed_telephone_number');
                $fixedExtraTeleNumber = $this->getRequest()->getParam('fixed_extra_telephone_no');
                if (isset($fixedTelephoneNumber)) {
                    $quote->setFixedTelephoneNumber($fixedTelephoneNumber);
                    $quote->setFixedExtraTelephoneNo($fixedExtraTeleNumber);
                    $quote->setData('fixed_extra_telephone_no', $fixedExtraTeleNumber);
                    $quote->save();
                }

                $session = $this->_getCustomerSession();
                /** @var Mage_Customer_Model_Customer $customer */
                $customer = $session->getCustomer();

                if ($session->getOrderEdit()) {
                    // When in order edit mode, create a quote for each package that has number porting changed
                    $quotes = Mage::helper('dyna_package')->getModifiedQuotes();
                    // But first remove packages that are already having something edited to prevent duplicates
                    $this->updateChangedPackages($quotes, $changedPackages);

                    /**
                     * Check if current order is not locked by another agent
                     */
                    $this->checkoutHelper->checkOrder($quote->getSuperOrderEditId());

                    // save packages in a new quote
                    $this->saveChangedPackages($changedPackages, $quote, $customer);
                }

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    /**
     * @param $quotes
     * @param $changedPackages
     */
    protected function updateChangedPackages($quotes, &$changedPackages)
    {
        foreach ($quotes as $modifiedQuote) {
            $items = $modifiedQuote->getAllItems();
            foreach ($items as $item) {
                if (isset($changedPackages[$item->getPackageId()])) {
                    unset($changedPackages[$item->getPackageId()]);
                    break;
                }
            }
        }
    }

    /**
     * @param $changedPackages
     * @param $quote
     * @param $customer
     */
    protected function saveChangedPackages($changedPackages, $quote, $customer)
    {
        foreach ($changedPackages as $packageId => $changedPackage) {
            $quoteItems = $quote->getPackageItems($packageId);
            /** @var Vznl_Checkout_Model_Sales_Quote $newQuote */
            $newQuote = Mage::getModel('sales/quote')
                ->setCustomer($customer)
                ->setBillingAddress(Mage::getModel('sales/quote_address')->setData($customer->getDefaultBillingAddress()
                    ? $customer->getDefaultBillingAddress()->getData() : array()))
                ->setShippingAddress(Mage::getModel('sales/quote_address')->setData($customer->getDefaultShippingAddress()
                    ? $customer->getDefaultShippingAddress()->getData() : array()))
                ->setSuperQuoteId($quote->getId())
                ->setSuperOrderEditId($quote->getSuperOrderEditId())
                ->setStoreId($quote->getStoreId());
            $newQuote->save();

            /** @var Mage_Sales_Model_Quote_Item $quoteItem */
            foreach ($quoteItems as &$quoteItem) {
                $newQuoteItem = Mage::getModel('sales/quote_item')
                    ->setData($quoteItem->getData())
                    ->unsetData('item_id')
                    ->setQuote($newQuote);
                $newQuoteItem->save();
            }
            unset($quoteItem);

            $packageModelOld = Mage::getModel('package/package')
                ->getPackages(null, $quote->getId(), $packageId)
                ->getFirstItem();

            Mage::getModel('package/package')
                ->setData($packageModelOld->getData())
                ->setEntityId(null)
                ->setQuoteId($newQuote->getId())
                ->setOrderId(null)
                ->save();
            $newQuote->setActivePackageId($packageId);
            $newQuote
                ->setTotalsCollectedFlag(false)
                ->collectTotals()
                ->save();
        }
    }

    public function saveIncomeTestAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return;
        }
        try {
            /** @var Vznl_Checkout_Model_Sales_Quote $quote */
            $quote = $this->getQuote();

            if (!$quote->getId()) {
                throw new Exception($this->__('Invalid quote'));
            }

            $data = $request->getPost('incometest', array());

            $errors = $this->checkoutValidationHelper->validateIncomeTestData($data);

            if ($errors) {
                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
            }

            Mage::getSingleton('checkout/session')->setIncomeTestData($data);

            if ($request->getPost('current_step')) {
                $quote->setCurrentStep($request->getPost('current_step'));
                $quote->save();
            }

            // Set successful response
            $result['error'] = false;
            $result['message'] = "Step data successfully saved";

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        } catch (Exception $e) {
            // Set fail response for other errors
            $result['error'] = true;
            $result['message'] = $this->__($e->getMessage());
            if ($this->getCoreServiceHelper()->isDev()) {
                $result['trace'] = $e->getTrace();
            }
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        }

    }

    public function saveChangeIncomeTestAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return;
        }
        try {
            /** @var Vznl_Checkout_Model_Sales_Quote $quote */
            $quote = $this->getQuote();

            if (!$quote->getId()) {
                throw new Exception($this->__('Invalid quote'));
            }

            $data = $request->getPost('incometest', array());

            $errors = $this->checkoutValidationHelper->validateIncomeTestData($data);

            if ($errors) {
                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
            }

            Mage::getSingleton('checkout/session')->setIncomeTestData($data);

            // Set successful response
            $result['error'] = false;
            $result['message'] = "ILT data successfully saved";
            Mage::getSingleton('checkout/session')->setIltChanged(true);

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        } catch (Exception $e) {
            // Set fail response for other errors
            $result['error'] = true;
            $result['message'] = $this->__($e->getMessage());
            if ($this->getCoreServiceHelper()->isDev()) {
                $result['trace'] = $e->getTrace();
            }
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        }

    }

    /**
     * Save income data
     * @param array $data
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @return bool
     * @throws Exception
     */
    private function _saveIncomeTestData(
        array $data,
        Vznl_Superorder_Model_Superorder $superOrder
    ):bool
    {
        $orderNumber = $superOrder->getOrderNumber();

        switch ($data['choice']) {
            case 'fill-in-now':
                $valuesToStore = [
                    'family_type',
                    'income',
                    'housing_costs',
                    'birth_middlename',
                    'birth_lastname'
                ];
                $dataToStore = [];
                foreach ($valuesToStore as $key) {
                    $dataToStore[$key] = $data[$key];
                }
                if (!Mage::helper('ilt')->storeIltData($superOrder, $dataToStore)) {
                    throw new Exception("Store ILT failed!");
                }
                $superOrder->setTimesStoredIlt($superOrder->getTimesStoredIlt() + 1);
                break;

            case 'fill-in-later':
                Mage::helper('vznl_checkout/email')->sendFillInILTLaterEmail(
                    $data['email'],
                    Mage::getSingleton('customer/session')->getCustomer(),
                    $orderNumber,
                    $superOrder
                );
                break;
        }

        $superOrder->setIltSelection($data['choice']);
        if (!$superOrder->getIltSelectionInitial()) {
            $superOrder->setIltSelectionInitial($data['choice']);
        }

        // We already save superorder later on
        // $superOrder->save();

        return true;
    }

    // Vodafone Thuis
    public function saveVodafoneHomeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $quote = $this->getQuote();

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }
                $data = $request->getPost('vodafone_home', array());

                $this->checkoutValidationHelper->validateVodafoneHomeData($data);

                $this->_saveVodafoneHomeData($data);

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    // VastOpMobiel
    public function saveFixedToMobileAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $quote = $this->getQuote();

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }
                $data = $request->getPost('fixed', array());

                $this->checkoutValidationHelper->validateFixedToMobileData($data);

                $this->_saveFixedToMobileData($data);

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    /**
     * Save payment details action
     * @return void
     * @throws Exception
     */
    public function savePaymentMethodAction():void
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return;
        }
        try {
            /** @var Vznl_Checkout_Model_Sales_Quote $quote */
            $quote = $this->getQuote();

            if (!$quote->getId()) {
                throw new Exception($this->__('Invalid quote'));
            }
            $paymentData = $request->getPost('payment', array());
            $paymentData = $this->unMaskData($paymentData);
            if (!isset($paymentData['monthly_method'])) {
                throw new Exception('Missing payment method');
            }
            /************************* START: billing address section ********************************/
            $errors = array();
            $customer = $this->_getCustomerSession()->getCustomer();
            $addressData = $request->getPost('billAddress', array());
            $address_type = $addressData['address_type'];
            //isFixedIlsMove
            if ($request->getPost('isFixedIlsMove') == "1" && $address_type == 'same_as_service_address') {
                $addressData = $request->getPost('billingAdd', array());
                $addressData['address_type'] = 'diffrent_address';
            }
            $serviceAddress = unserialize($quote->getServiceAddress());
            $serviceAddrId = isset($serviceAddress["addressId"]) ? $serviceAddress["addressId"] : '';
            $serviceStreet = isset($serviceAddress["street"]) ? $serviceAddress["street"] : '';
            $serviceHouseNumber = isset($serviceAddress["housenumber"]) ? $serviceAddress["housenumber"] : '';
            $serviceHouseNumberAddition = isset($serviceAddress["housenumberaddition"]) ? $serviceAddress["housenumberaddition"] : '';
            $servicePostalCode = isset($serviceAddress["postalcode"]) ? $serviceAddress["postalcode"] : '';
            $serviceCity = isset($serviceAddress["city"]) ? $serviceAddress["city"] : '';
            $street = array($serviceStreet, $serviceHouseNumber, $serviceHouseNumberAddition, $serviceAddrId);

            $addressStreet = (!empty($addressData['street'])) ? $addressData['street'] : '';
            $addressHouseno = (!empty($addressData['houseno'])) ? $addressData['houseno'] : '';
            $addressAddition = (!empty($addressData['addition'])) ? $addressData['addition'] : '';
            $addressAddressId = (isset($addressData['addressId'])) ? $addressData['addressId'] : '';

            $fixedStreet = array($addressStreet, $addressHouseno, $addressAddition, $addressAddressId);
            if (is_array($fixedStreet)) {
                $fixedStreetStr = (implode("\n", $fixedStreet));
            }
            $quoteBillingAddress = $quote->getBillingAddress()->getData();

            $billingAddress = Mage::getModel('sales/quote_address')->setData($quoteBillingAddress);

            if ($quote->hasMobilePackage()) {
                if ($addressData['address_type'] == 'same_as_service_address') {
                    $defaultAddress = $quote->getCustomer()->getDefaultBillingAddress();
                    if ($defaultAddress) {
                        $billingAddress->setStreet($defaultAddress->getStreet());
                        $billingAddress->setData('city', $defaultAddress->getCity());
                        $billingAddress->setData('postcode', $defaultAddress->getPostcode());
                    }
                } else if ($addressData['address_type'] == 'diffrent_address') {
                    $billingAddress->setStreet(array($addressStreet, $addressHouseno, $addressAddition));
                    $billingAddress->setData('city', $addressData['city']);
                    $billingAddress->setData('postcode', $addressData['postcode']);
                }
            }

            if ($quote->hasFixed()) {
                if ((isset($addressData['address_type_fixed']) && $addressData['address_type_fixed'] == 'same_as_service_address_fixed') || $addressData['address_type'] == 'same_as_service_address') {
                    if (is_array($street)) {
                        $streetStr = (implode("\n", $street));
                    }
                    $billingAddress->setData('fixed_street', $streetStr);
                    $billingAddress->setData('fixed_city', $serviceCity);
                    $billingAddress->setData('fixed_postcode', $servicePostalCode);
                } elseif ((isset($addressData['address_type_fixed']) && $addressData['address_type_fixed'] == 'diffrent_address') || $addressData['address_type'] == 'diffrent_address') {
                    $billingAddress->setData('fixed_street', $fixedStreetStr);
                    $billingAddress->setData('fixed_city', $addressData['city']);
                    $billingAddress->setData('fixed_postcode', $addressData['postcode']);
                    $data['street'] =  $fixedStreet;
                    $data['postcode'] =  $addressData['postcode'];
                    $data['city'] =  $addressData['city'];
                    $data['address'] = 'billing_address';
                    if (!empty($data)) {
                        $errors += $this->checkoutValidationHelper->validateSplitBillingAddressData($data);
                    }
                }
            }

            if ($quote->hasMobilePackage() && $quote->hasFixed()) {
                $billingAddress->setStreet($street);
                $billingAddress->setData('city', $serviceCity);
                $billingAddress->setData('postcode', $servicePostalCode);
            }

            $quote->setBillingAddress($billingAddress);
            /************************* END: billing address section ********************************/

            $Accountnomobile = (!empty($paymentData['account_no_mobile'])) ? $paymentData['account_no_mobile'] : '';
            $Accountholdermobile = (!empty($paymentData['account_holder_mobile'])) ? $paymentData['account_holder_mobile'] : '';

            if ($quote->hasMobilePackage()) {
                $payment['account_no'] = ($Accountnomobile) ?: $paymentData['account_no'];
                $payment['account_holder'] = ($Accountholdermobile) ?: $paymentData['account_holder'];
                if (Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                    $payment['mobile_oneoff_payment'] = $quote->getMobileOneoffPayment();
                } else {
                    $payment['mobile_oneoff_payment'] = $paymentData['oneoff_method'];
                }
                // $quote->setData('payment_method', 'direct_debit');
                $quote->setData('customer_account_number', $payment['account_no']);
                $quote->setData('customer_account_holder', $payment['account_holder']);
                $quote->setData('mobile_oneoff_payment', $payment['mobile_oneoff_payment']);

                $errors = $this->checkoutValidationHelper->validatePaymentData($paymentData, $customer);
            }
            if ($quote->hasFixed()) {
                $payment['bill_distribution_method'] = $paymentData['bill_distribution_method'];

                $payment['fixed_payment_method'] =  (!isset($paymentData['fixed_method']) || $paymentData['fixed_method'] == 'direct_debit_fixed' || $paymentData['fixed_method'] == 'direct_debit_fixed_different') ? "DIRECT_DEBIT" : $paymentData['fixed_method'];
                if ($paymentData['monthly_method'] == 'direct_debit') {
                    $payment['fixed_account_no'] = $paymentData['account_no'] ;
                    $payment['fixed_account_holder'] = $paymentData['account_holder'];
                    $payment['fixed_payment_method'] = strtoupper($paymentData['monthly_method']);
                } elseif ($paymentData['monthly_method'] == 'multiple_payment') {
                    if ($paymentData['fixed_method'] == "direct_debit_fixed") {
                        $payment['fixed_account_no'] = $Accountnomobile ; //same as mobile package
                        $payment['fixed_account_holder'] = $Accountholdermobile; //same as mobile package
                    } elseif ($paymentData['fixed_method'] == "direct_debit_fixed_different") {
                        $payment['fixed_account_no'] = $paymentData['account_no_fixed_different'] ;
                        $payment['fixed_account_holder'] = $paymentData['account_holder_fixed_different'];
                    }
                } elseif ($paymentData['monthly_method'] == 'ACCEPTGIRO') {
                    $payment['fixed_payment_method'] = $paymentData['monthly_method'];
                }
                $errors = $this->checkoutValidationHelper->validatePaymentData($paymentData, $customer);

                $quote->setData('fixed_payment_method_monthly_charges', $payment['fixed_payment_method']);
                $quote->setData('fixed_account_number', $payment['fixed_account_no']);
                $quote->setData('fixed_account_holder', $payment['fixed_account_holder']);
                $quote->setData('fixed_payment_method_one_time_charge', 'checkmo');
                $quote->setData('fixed_bill_distribution_method', $payment['bill_distribution_method']);
                if (!$quote->hasMobilePackage()) {
                    $quote->setData('customer_account_number', $payment['fixed_account_no']);
                    $quote->setData('customer_account_holder', $payment['fixed_account_holder']);
                }
            }

            $customer = $quote->getCustomer();
            if (!$customer->getId() ||
                (
                    $customer->getId() &&
                    empty($customer->getData('account_no'))
                )
                ||
                (
                    $customer->getId() &&
                    (!empty($customer->getData('account_no')) && (strpos($paymentData['account_no'],'*') === false))
                )
            ) {
                $customer->setData('account_no', $payment['account_no']);
            }

            if (!$customer->getId() || ($customer->getId() && empty($customer->getData('account_holder'))) || ($customer->getId() && !empty($customer->getData('account_holder'))))
            {
                $customer->setData('account_holder', $payment['account_holder']);
            }

            $quote->setCurrentStep("savePaymentMethod");
            if ($errors) {
                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
            }
            $quote->save();
            // Set successful response
            $result['error'] = false;
            $result['message'] = "Step data successfully saved";

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        } catch (Mage_Customer_Exception $e) {
            // Set fail response for validations
            $result['error'] = true;
            $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
            $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        } catch (Exception $e) {
            // Set fail response for other errors
            $result['error'] = true;
            $result['message'] = $this->__($e->getMessage());
            if ($this->getCoreServiceHelper()->isDev()) {
                $result['trace'] = $e->getTrace();
            }
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        }
    }

    public function saveDeliveryAddressAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            try {
                $quote = $this->getQuote();
                $helper = Mage::helper('checkout/data');

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }
                $isFixed = $this->getQuote()->hasFixed();
                $isMobile = $this->getQuote()->hasMobilePackage();
                $delivery = $request->getPost('delivery', array());
                $data = array();
                if (!isset($delivery['method'])) {
                    throw new Exception($this->__('Missing shipment method'));
                }

                $deliveryMethod = $delivery['method'];
                if($isFixed && $isMobile) {
                    $delivery['method'] = Vznl_Checkout_IndexController::DELIVERY_METHOD_SPLIT;
                }

                $errors = array();
                $showDeliverySteps = (Mage::helper('agent')->isRetailStore()) ? false : true;

                switch ($delivery['method']) {
                    case Vznl_Checkout_IndexController::DELIVERY_METHOD_DIRECT:
                        if ($isFixed) {
                            $fixedDeliveryMethodData = $this->getRequest()->getParam('fixed_delivery_method');
                            $fixedDeliveryMethod = $helper->getFixedmethod($fixedDeliveryMethodData);
                            $fixedDeliveryWishDate = $helper->getFixedSelectedDeliveryWishDate($fixedDeliveryMethodData, $this);
                            if ($fixedDeliveryMethodData == 'oneday_technician_install') {
                                $fixedDeliveryWishDate = $this->getRequest()->getParam('delivery_wish_date_soho');
                            }
                            $quote->setFixedDeliveryMethod($fixedDeliveryMethod);
                            $basketId = $quote->getData('basket_id');
                            $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);
                            /*$customerType = ($quote->getData('customer_id_type')) ? Dyna_Customer_Helper_Services::PARTY_TYPE_PRIVATE : Dyna_Customer_Helper_Services::PARTY_TYPE_SOHO;
                            foreach (Mage::getModel('checkout/session')->getQuote()->getCartPackages() as $package) {
                                if ($package->isFixed()) {
                                    $isOverstappen = $package->getFixedOverstappenStatus();
                                }
                            }
                            $availableDeliveryDates = Mage::helper('vznl_deliverywishdates')->getDeliveryWishDates($basketId, $customerType, $fixedDeliveryMethod, $isOverstappen, $quote->getQuoteNewHardware());
                            foreach ($availableDeliveryDates['availableDateDetails'] as $date) {
                                $availability[] = date('d-m-Y', strtotime($date['availableStartDate']));
                            }

                            if (in_array($fixedDeliveryWishDate, $availability)) {
                                $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);
                            } else {
                                $quote->setData('delivery_wish_date', null);
                                $quote->setFixedDeliveryWishDate(null);
                                $result['error'] = true;
                                $result['message'] = $this->__('Please select a valid date');
                                $result['fields']['delivery_wish_date'] = $this->__('Please select a valid date');
                                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                                    Mage::helper('core')->jsonEncode($result)
                                );
                                return;
                            }*/
                        }
                        $addressData = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();

                        $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType([
                            'address' => 'direct',
                            'store_id' => $addressData,
                        ]);

                        if ($this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address'])) {
                            $errors += ['delivery[method][text]' => $this->__('Select a store')];
                        }
                        $showDeliverySteps = true;
                        break;
                    case Vznl_Checkout_IndexController::DELIVERY_METHOD_OTHER_PICKUP:
                        /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                        if (!isset($delivery['pickup']['store_id']) || empty($delivery['pickup']['store_id'])) {
                            $errors += ['delivery[pickup][store]' => $this->__('Select a store')];
                        } else {
                            $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType([
                                'address' => 'store',
                                'store_id' => $delivery['pickup']['store_id'],
                                'dealer_column' => 'dealer_id',
                            ]);
                            if ($this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address'])) {
                                $errors += ['delivery[pickup][store]' => $this->__('Select a store')];
                            }
                        }
                        break;
                    case Vznl_Checkout_IndexController::DELIVERY_METHOD_DELIVER:
                        $delivery['deliver']['address'] = Vznl_Checkout_IndexController::DELIVERY_METHOD_BILLING_ADDRESS;
                        if (!isset($delivery['deliver']['address'])) {
                            throw new Exception($this->__('Missing shipping address info'));
                        }
                        $address = $request->getPost('billingAddress', []);
                        if ($isFixed) {
                            $fixedDeliveryMethodData = $this->getRequest()->getParam('fixed_delivery_method');
                            $fixedDeliveryMethod = $helper->getFixedmethod($fixedDeliveryMethodData);
                            $fixedDeliveryWishDate = $helper->getFixedSelectedDeliveryWishDate($fixedDeliveryMethodData, $this);
                            $quote->setFixedDeliveryMethod($fixedDeliveryMethod);
                            $packageArray = array();
                            $items = array();
                            $packages = array();
                            $isOverstappen = false;
                            foreach ($quote->getAllItems() as $item) {
                                $items[] = $item;
                                $packageArray[$item->getPackageId()][] = $item->getName();
                                if (!isset($packages[$item->getPackageId()])) {
                                    $currentPackage = Mage::getModel('package/package')
                                        ->getPackages($quote->getEntityId(), null, $item->getPackageId())
                                        ->getFirstItem();
                                    $packages[$item->getPackageId()] = $currentPackage;
                                }
                            }
                            $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);

                            /* $basketId = $quote->getData('basket_id');
                            $customerType = ($quote->getData('customer_id_type')) ? Dyna_Customer_Helper_Services::PARTY_TYPE_PRIVATE : Dyna_Customer_Helper_Services::PARTY_TYPE_SOHO;
                            foreach ($items as $item) {
                                $package = $item->getPackage();
                                $isOverstappen = $package->getFixedOverstappenStatus();
                            }
                            $availableDeliveryDates = Mage::helper('vznl_deliverywishdates')->getDeliveryWishDates($basketId, $customerType, $fixedDeliveryMethod, $isOverstappen, $quote->getQuoteNewHardware());
                            foreach ($availableDeliveryDates['availableDateDetails'] as $date) {
                                $availability[] = $date['availableStartDate'];
                            }
                            if (in_array($fixedDeliveryWishDate, $availability)) {
                                $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);
                            } else {
                                $quote->setData('delivery_wish_date', null);
                                $result['error'] = true;
                                $result['message'] = "Please select a valid date";
                                $result['fields']['delivery_wish_date'] = $this->__('Please select a valid date');
                                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                                    Mage::helper('core')->jsonEncode($result)
                                );
                                return;
                            }*/
                            $data['deliver']['address'] = array(
                                'address' => $delivery['deliver']['address'],
                                'street' => array($address['street'], $address['houseno'], $address['addition']),
                                'postcode' => $address['postcode'],
                                'city' => $address['city']
                            );
                        } else {
                            $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                                'address' => $delivery['deliver']['address'],
                                'billingAddress' => $request->getPost('billingAddress', array()),
                            ));
                        }
                        $errors += $this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address']);
                        break;
                    case Vznl_Checkout_IndexController::DELIVERY_METHOD_OTHER_ADDRESS:
                        if ($isFixed) {
                            $otherAddress = $this->getRequest()->getParam('otherAddress', array());
                            $fixedOtherAddressId = $otherAddress['addressid'];
                            $fixedDeliveryMethodData = $this->getRequest()->getParam('fixed_delivery_method');
                            $fixedDeliveryMethod = $helper->getFixedmethod($fixedDeliveryMethodData);
                            $fixedDeliveryWishDate = $helper->getFixedSelectedDeliveryWishDate($fixedDeliveryMethodData, $this);
                            $quote->setFixedDeliveryMethod($fixedDeliveryMethod);

                            $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);
                            /* $basketId = $quote->getData('basket_id');
                            foreach (Mage::getModel('checkout/session')->getQuote()->getCartPackages() as $package) {
                                if ($package->isFixed()) {
                                    $isOverstappen = $package->getFixedOverstappenStatus();
                                }
                            }
                            $customerType = ($quote->getData('customer_id_type')) ? Dyna_Customer_Helper_Services::PARTY_TYPE_PRIVATE : Dyna_Customer_Helper_Services::PARTY_TYPE_SOHO;
                            $availableDeliveryDates = Mage::helper('vznl_deliverywishdates')->getDeliveryWishDates($basketId, $customerType, $fixedDeliveryMethod, $isOverstappen, $quote->getQuoteNewHardware());
                            foreach ($availableDeliveryDates['availableDateDetails'] as $date) {
                                $availability[] = $date['availableStartDate'];
                            }
                            if (in_array($fixedDeliveryWishDate, $availability)) {
                                $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);
                            } else {
                                $quote->setData('delivery_wish_date', null);
                                $result['error'] = true;
                                $result['message'] = "Please select a valid date";
                                $result['fields']['delivery_wish_date'] = $this->__('Please select a valid date');
                                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                                    Mage::helper('core')->jsonEncode($result)
                                );
                                return;
                            }*/
                        }
                        $delivery['deliver']['address'] = Vznl_Checkout_IndexController::DELIVERY_METHOD_OTHER_ADDRESS;
                        if (!isset($delivery['deliver']['address'])) {
                            throw new Exception($this->__('Missing shipping address info'));
                        }

                        $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                            'address' => $delivery['deliver']['address'],
                            'otherAddress' => $request->getPost('otherAddress', array()),
                        ));

                        if (isset($fixedOtherAddressId)) {
                            $quote->setPealDifferentAddressId($fixedOtherAddressId);
                        }

                        $errors += $this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address']);
                        break;
                    case Vznl_Checkout_IndexController::DELIVERY_METHOD_SPLIT:
                        $splitData = $request->getPost('delivery', array());
                        $splitData = isset($splitData['pakket']) ? $splitData['pakket'] : array();
                        if (!$splitData) {
                            throw new Exception($this->__('Incomplete data'));
                        }

                        $cancelledPackages = Mage::helper('dyna_package')->getCancelledPackages();
                        $cancelledPackages = !empty($cancelledPackages) ? $cancelledPackages : [];
                        $cancelledPackagesNow = Mage::getSingleton('customer/session')->getCancelledPackagesNow();
                        $cancelledPackagesNow = !empty($cancelledPackagesNow) ? $cancelledPackagesNow : [];
                        $flippedCancelledPackages = array_flip($cancelledPackages);
                        $flippedCancelledPackagesNowQuote = array_flip($cancelledPackagesNow[$quote->getId()]);

                        $mobileAddress = [];
                        $count = 0;
                        $flippedMobilePackages = array_flip(Vznl_Catalog_Model_Type::getMobilePackages());
                        foreach ($splitData as $packageId => $address) {

                            if ($deliveryMethod != $delivery['method']) {
                                if ($deliveryMethod == Vznl_Checkout_IndexController::DELIVERY_METHOD_DELIVER) {
                                    $deliveryMethod = Vznl_Checkout_IndexController::DELIVERY_METHOD_BILLING_ADDRESS;
                                }
                                $address['address'] = $deliveryMethod;
                                $otherAddress = $request->getPost('otherAddress', array());
                                $address['otherAddress']['postcode'] = $otherAddress['postcode'];
                                $address['otherAddress']['houseno'] = $otherAddress['houseno'];
                                $address['otherAddress']['addition'] = $otherAddress['addition'];
                                $address['otherAddress']['street'] = $otherAddress['street'];
                                $address['otherAddress']['city'] = $otherAddress['city'];
                                $address['otherAddress']['addressid'] = $otherAddress['addressid'];
                            }
                            if (isset($flippedCancelledPackages[$packageId]) || (!empty($cancelledPackagesNow[$quote->getId()]) && isset($flippedCancelledPackagesNowQuote[$packageId]))) {
                                continue;
                            }
                            if ($isFixed) {
                                $fixedDeliveryMethodData = $this->getRequest()->getParam('fixed_delivery_method');
                                $fixedDeliveryMethod = $helper->getFixedmethod($fixedDeliveryMethodData);
                                $fixedDeliveryWishDate = $helper->getFixedSelectedDeliveryWishDate($fixedDeliveryMethodData, $this);
                                $quote->setFixedDeliveryMethod($fixedDeliveryMethod);
                                $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);

                                /*$basketId = $quote->getData('basket_id');
                                foreach (Mage::getModel('checkout/session')->getQuote()->getCartPackages() as $package) {
                                    if ($package->isFixed()) {
                                        $isOverstappen = $package->getFixedOverstappenStatus();
                                    }
                                }
                                $customerType = ($quote->getData('customer_id_type')) ? Dyna_Customer_Helper_Services::PARTY_TYPE_PRIVATE : Dyna_Customer_Helper_Services::PARTY_TYPE_SOHO;
                                $availableDeliveryDates = Mage::helper('vznl_deliverywishdates')->getDeliveryWishDates($basketId, $customerType, $fixedDeliveryMethod, $isOverstappen, $quote->getQuoteNewHardware());
                                foreach ($availableDeliveryDates['availableDateDetails'] as $date) {
                                    $availability[] = $date['availableStartDate'];
                                }
                                if (in_array($fixedDeliveryWishDate, $availability) && $fixedDeliveryMethod != 'SHOP') {
                                    $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);
                                } else {
                                    $quote->setData('delivery_wish_date', null);
                                    $quote->setFixedDeliveryWishDate(null);

                                    if ($fixedDeliveryMethod != 'SHOP') {
                                        $result['error'] = true;
                                        $result['message'] = "Please select a valid date";
                                        $result['fields']['delivery_wish_date'] = $this->__('Please select a valid date');
                                        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                                            Mage::helper('core')->jsonEncode($result)
                                        );
                                        return;
                                    }
                                }*/

                                $addressType = $address['type'];
                                if(isset($flippedMobilePackages[$addressType])) {
                                    $address['dealer_column'] = 'dealer_id';
                                }
                                if (isset($address['address'])) {
                                    if (isset($flippedMobilePackages[$addressType]) && $count < 1) {
                                        $data['pakket'][$packageId]['address'] = $this->checkoutHelper->_getAddressByType($address);
                                    } elseif ($address['type'] == Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_FIXED) {
                                        $data['pakket'][$packageId]['address'] = $this->checkoutHelper->_getAddressByType($address);
                                    }
                                }

                                if (isset($flippedMobilePackages[$addressType])) {
                                    if($count < 1) {
                                        $mobileAddress = $data['pakket'][$packageId]['address'];
                                    }
                                    $count++;
                                }

                                if ($count > 1 && isset($flippedMobilePackages[$addressType])) {
                                    $data['pakket'][$packageId]['address'] = $mobileAddress;
                                }

                                $data['pakket'][$packageId]['type'] = $address['type'];

                                $fixedOtherAddressId = isset($address['otherAddress']['addressid']) ? $address['otherAddress']['addressid'] : '';
                                if ($fixedOtherAddressId) {
                                    $quote->setPealDifferentAddressId($fixedOtherAddressId);
                                }
                                if ($this->getRequest()->getParam('fixed_delivery_method') == 'SHOP' && !isset($data['pakket'][$packageId]['type'])) {
                                    $addressData = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();
                                    $data['pakket'][$packageId]['address'] = $this->checkoutHelper->_getAddressByType([
                                        'address' => 'direct',
                                        'store_id' => $addressData,
                                    ]);
                                    $data['pakket'][$packageId]['type'] = strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED);
                                }

                                if ($data['pakket'][$packageId]['type'] == strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED)) {
                                    $data['pakket'][$packageId]['address']['street'][0] = $data['pakket'][$packageId]['address']['street'][0].' ';
                                }
                            } else {

                                if (isset($address['address'])) {
                                    $data['pakket'][$packageId]['address'] = $this->checkoutHelper->_getAddressByType($address);
                                }
                                $addressType = $address['type'];
                                if (isset($flippedMobilePackages[$addressType])) {
                                    if($count < 1) {
                                        $mobileAddress = $data['pakket'][$packageId]['address'];
                                    }
                                    $count++;
                                }
                                if ($count > 1 && isset($flippedMobilePackages[$addressType])) {
                                    $data['pakket'][$packageId]['address'] = $mobileAddress;
                                }

                                $data['pakket'][$packageId]['type'] = $address['type'];
                            }

                            if ($data['pakket'][$packageId]['address']['address'] == 'store' &&
                                $data['pakket'][$packageId]['address']['store_id'] ==  Mage::getSingleton('customer/session')->getAgent(true)->getDealer()->getId()) {
                                $showDeliverySteps = true;
                            }
                        }

                        //saving fixed address in shipping data on converged + fixed ils or No fixed delivery type
                        if ($isFixed && $isMobile && ($quote->isFixedIls() || !$quote->getQuoteNewHardware())) {
                            $serviceAddress = unserialize($quote->getServiceAddress());
                            $data['pakket'][$quote->getFixedPackageId()] = [
                                'type' => Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_FIXED,
                                'address' => [
                                    'street' => [
                                        $serviceAddress['street'].' ' ?? "",
                                        $serviceAddress['housenumber'] ?? "",
                                        $serviceAddress['housenumberaddition'] ?? "",
                                    ],
                                    'postcode' => $serviceAddress['postalcode'] ?? "",
                                    'city' => $serviceAddress['city'] ?? "",
                                    'address' => 'billing_address'
                                ]
                            ];
                        }
                        foreach ($splitData as $packageId => $address) {
                            if (isset($data['pakket'][$packageId]['address']) && !isset($data['pakket'][$packageId]['address']['houseno'])) {
                                $data['pakket'][$packageId]['address']['houseno'] = $data['pakket'][$packageId]['address']['street'][1];
                            }
                            $errors += $this->checkoutValidationHelper->validateSplitDeliveryAddressData(
                                $data['pakket'][$packageId]['address'], $packageId
                            );
                        }
                        break;
                    default:
                        $errors += array('delivery[method]' => $this->__('Invalid delivery method'));
                }

                if ($errors) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
                }

                $this->_saveDeliveryAddressData($data);

                if ($request->getPost('current_step')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }
                $quote->save();

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                if (Mage::helper('agent')->isRetailStore()) {
                    $result['delivery_steps'] = $showDeliverySteps;
                }

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    public function saveOverviewPollingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $id = trim($this->getRequest()->getParam('id'));
                if (!$id || 'undefined' === $id) {
                    $result = array(
                        'error' => false,
                        'performed' => true
                    );
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }

                /** @var Vznl_Job_Model_Pool $resultsPool */
                $resultsPool = Mage::getSingleton('vznl_job/pool');
                $response = $resultsPool->getResult($id);
                if ($response === null) {
                    $result = array(
                        'error' => false,
                        'performed' => false
                    );
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                } elseif ($response === false) {
                    $result = array(
                        'error' => true,
                        'message' => $this->__('There is no such job')
                    );
                    Mage::logException(new Exception('saveOverviewPollingAction: No such job for id' . $id));
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }
                $this->checkoutHelper->handleProcessOrderResponse($response); //update package status
                if (isset($response['process_result']['success']) && $response['process_result']['success'] == 0 && $response['process_result']['success'] != 'true') {
                    throw new Exception($response['process_result']['description']);
                }
                $result = array(
                    'error' => false,
                    'performed' => true
                );
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                $superOrderId = $this->_getCustomerSession()->getData($id);
                if ($this->_getCustomerSession()->getFailedSuperOrderId() != $superOrderId['superorder_id']) {
                    /** @var Vznl_Superorder_Model_Superorder $superOrder */
                    $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId['superorder_id']);
                    $quote = $superOrder->getOriginalQuote();
                    $quote->setSaveAsCart(true);
                    $this->_createNewQuoteOnServiceCallFail($quote, $superOrder->getId());
                }
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }
        }
    }

    public function saveOverviewAfterAction()
    {
        $result = array();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $quote = $this->getQuote();
            $id = filter_var( trim($request->getPost('polling_id')), FILTER_SANITIZE_NUMBER_INT);
            $isRefund = trim($request->getPost('refund'));
            if ($id && 'undefined' != $id) {
                try {
                    /** @var Vznl_Superorder_Model_Superorder $superOrder */
                    $sessionData = $this->_getCustomerSession()->getData($id);
                    $superOrder = Mage::getModel('superorder/superorder')->load($sessionData['superorder_id']);

                    if ($this->_getCustomerSession()->getFailedSuperOrderId() &&
                        $this->_getCustomerSession()->getFailedSuperOrderId() == $superOrder->getId()
                    ) {
                        $this->_getCheckoutSession()->setLoadInactive(false);
                    }

                    if ($superOrder->getErrorCode()) {
                        if (! $isRefund) {
                            $quote->setSaveAsCart(true);
                            $this->_createNewQuoteOnServiceCallFail($quote, $superOrder->getId());
                        }
                        throw new Exception($superOrder->getErrorCode() . ' ' . $superOrder->getErrorDetail());
                    }

                    Mage::getSingleton('customer/session')->setCurrentSuperOrderId($superOrder->getId());
                    $this->_getCustomerSession()->setFailedSuperOrderId(null);

                    if (! $isRefund) {
                        //initialize a new quote
                        $this->checkoutHelper->createNewQuote();
                    }

                    $parentSuperorderId = null;
                    if ($isRefund) {
                        $customerSession = $this->_getCustomerSession();
                        $customerSession->setOrderEdit(null);
                        $customerSession->setOrderEditMode(false);
                        $customerSession->setPackageDifferences(null);
                        /** @var Vznl_Checkout_Model_Cart $cart */
                        $cart = Mage::getSingleton('checkout/cart');
                        $cart->exitEditMode();
                    }

                } catch (Exception $e) {
                    // Set fail response for other errors
                    $result['error'] = true;
                    $result['message'] = $this->__($e->getMessage());
                    if ($this->getCoreServiceHelper()->isDev()) {
                        $result['trace'] = $e->getTrace();
                    }
                    Mage::getSingleton('core/logger')->logException($e);
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );

                    return;
                }
            }
            $result['error'] = false;
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        }
    }

    /**
     * Create and submit order
     * @return void
     * @throws Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function saveOverviewAction():void
    {
        $request = $this->getRequest();
        $result = array();

        /** @var Vznl_Agent_Helper_Data $agentHelper */
        $agentHelper = Mage::helper('agent');
        $dealerData = $agentHelper->getDealerData();

        // Skip order creation for async handling of the ILT errors
        $skipStoreIlt = $request->get('skip_store_ilt') ? $request->get('skip_store_ilt') : 0;

        /** @var Vznl_Customer_Model_Customer $customer */
        $customer = $this->_getCustomerSession()->getCustomer();

        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();

        // Do not perform collectTotals again on quote, as it was previosly performed
        $quote->setTotalsCollectedFlag(true);
        $isMobile = $quote->hasMobilePackage();
        $isRetention = $quote->hasRetention();
        $isIlsFixed = $quote->getPackageSaleType() == Dyna_Catalog_Model_ProcessContext::ILSBLU ? true : false;
        $isMoveProcess = $quote->getPackageSaleType() == Vznl_Catalog_Model_ProcessContext::MOVE ? true : false;

        if (!$quote->getId()) {
            throw new Exception($this->__('Invalid quote'));
        }

        $result['addresses'] = $this->_buildCreditCheckAddresses($quote);
        $serviceAddress = unserialize($quote->getServiceAddress());

        if ($request->isPost()) {
            try {
                $isFixed = $quote->hasFixed();
                if ($isFixed) {
                    $campaignId =  filter_var( trim($request->getParam('campaign_id')), FILTER_SANITIZE_NUMBER_INT);
                    if ($campaignId && ((!is_numeric($campaignId)) || (strlen((string) $campaignId) != 6))) {
                        $result['error'] = true;
                        $result['message'] = "Campaign id should be 6 digit code";
                        $result['fields']['campaign_id'] = $this->__('Campaign id should be 6 digit code');
                        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                            Mage::helper('core')->jsonEncode($result)
                        );
                        return;
                    }

                    $quote->setData('campaign_id', $request->getParam('campaign_id'));
                    $quote->setData('sales_log', $request->getParam('sales_log'));

                    // update written_consent
                    if ($agentHelper->isTelesalesChannel() &&
                        (
                            $dealerData->getData('call_center_activity') == 'multiskill' ||
                            $dealerData->getData('call_center_activity') == 'outbound'
                        ) &&
                        !$this->_getCustomerSession()->getCustomer()->getIsSoho()
                    ) {
                        if ($dealerData->getData('call_center_activity') == 'multiskill') {
                            $writtenConsent = $this->getRequest()->getParam('written_consent');
                        } else {
                            $writtenConsent = 1;
                        }

                        $quote->setFixedWrittenConsent($writtenConsent);
                    }

                    $quote->save();
                }

                // Overwrite customer email
                $emailOverwrite = trim($request->getParam('email_overwrite'));
                $emails = explode(';', $customer->getAdditionalEmail());
                if ($emailOverwrite && !in_array($emailOverwrite, $emails)) {
                    if (Mage::helper('dyna_validators')->validateEmailSyntax($emailOverwrite)) {
                        $this->_getCustomerSession()->setNewEmail($emailOverwrite);
                        array_push($emails, $emailOverwrite);
                        $customer->setEmailOverwritten(1)->setCorrespondenceEmail(implode(';', $emails))->save();
                    } else {
                        throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode(['email_overwrite' => $this->__('Please enter a valid email address. For example johndoe@domain.com.')]));
                    }
                } else {
                    $this->_getCustomerSession()->setNewEmail(null);
                }

                if ($skipStoreIlt == 0) {
                    $this->checkoutHelper->processNotesAndPickup($request);
                    $quote->setCustomStatus(Vznl_Checkout_Model_Sales_Quote::VALIDATION);

                    // Validate coupon
                    $couponHelper = Mage::helper('pricerules/external_coupon_validator');
                    foreach ($quote->getPackages() as $packageData) {
                        if (false === empty($packageData['coupon'])) {
                            $package = Mage::getModel('package/package')->load($packageData['entity_id']);
                            if (!$couponHelper->isValid($package->getCoupon(), $quote, $package)) {
                                throw new Exception(sprintf($this->__('Coupon "%s" is no longer valid'),
                                    $package->getCoupon()));
                            }
                        }
                    }

                    // Create the order and place the ESB Order id on the response
                    try {
                        /** @var Vznl_Superorder_Model_Superorder $superOrder */
                        $superOrder = $this->createOrder();

                    } catch (LogicException $e) {
                        /** @var Vznl_Service_Model_Client_PortalInternalClient $client */
                        if (!Mage::helper('vznl_service')->useStubs()) {
                            $client = Mage::helper('vznl_service')->getClient('portal_internal');
                            $client->riskShoppingCart(
                                $this->_getCustomerSession()->getCustomer(),
                                $quote,
                                $this->_getCustomerSession()->getAgent(true)
                            );
                        }
                        $result['error'] = false;
                        $result['message'] = "Step data successfully saved";
                        $result['polling_id'] = 'undefined';
                        $result['orders'] = $this->checkoutHelper->__('Risk incident recorded, order was not created');
                        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                            Mage::helper('core')->jsonEncode($result)
                        );
                        return;
                    }

                    $hasHn = $superOrder->hasHNItems();
                    if ($hasHn) {
                        $superOrder->setIsVfOnly(0);
                        $superOrder->save();
                    }
                }

                // Retrieve superorder from redo session if not there already
                if (!isset($superOrder)) {
                    $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($this->_getCheckoutSession()->getIltStoreOrderNumber());
                    $this->_getCheckoutSession()->setIltStoreOrderNumber(null);
                }

                // @todo: _retreiveSoRedoSession does not seem to exist, will throw error
                // $superOrder = !isset($superOrder) ? $this->_retreiveSoRedoSession() : $superOrder;

                $result['orders'] = $superOrder->getOrderNumber();
                $result['order_id'] = $superOrder->getId();

                /**
                 * Trigger the StoreILT
                 */
                $this->triggerILT($superOrder, $quote, $result);
                if (isset($result['error']) && !empty($result['error'])) {
                    $result['error'] = true;
                    $result['modalError'] = $this->__('There was an error with processing order');
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }

                /**
                 * Call CreateOrder
                 */
                try {
                    $serviceHelper = $this->getServiceHelper();

                    /** @var Vznl_Service_Model_Client_EbsOrchestrationClient $client */
                    $client = $serviceHelper->getClient('ebs_orchestration');
                    $pollingId = $client->processCustomerOrder(
                        $superOrder,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        $this->_getCustomerSession()->getToPayAmount(),
                        false,
                        false,
                        $quote->getCustomer()
                    ); //call esb
                } catch (LogicException $e) {
                    $result['error'] = true;
                    $result['modalError'] = $e->getMessage();
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                } catch (\Exception $e) {
                    // When processCustomerOrder fails, save the quote as shopping cart
                    $quote->setSaveAsCart(true);
                    $this->_createNewQuoteOnServiceCallFail($quote, $superOrder->getId());
                    Mage::getSingleton('core/logger')->logException($e);
                    $this->getServiceHelper()->returnServiceError($e);
                    return;
                }

                // Append the address to the response in order to have them in the credit check step
                $result['polling_id'] = $pollingId;

                if ($request->getPost('current_step')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }
                $ilsProductHasVoice = false;
                $ilsProductHasInternet = false;
                $cart = Mage::getModel('checkout/cart')->getQuote();
                foreach ($cart->getAllItems() as $item) {
                    $items[] = $item;
                    $packageArray[$item->getPackageId()][] = $item->getName();
                    if (!isset($packages[$item->getPackageId()])) {
                        $currentPackage = Mage::getModel('package/package')
                            ->getPackages($cart->getEntityId(), null, $item->getPackageId())
                            ->getFirstItem();
                        $packages[$item->getPackageId()] = $currentPackage;
                    }
                }
                foreach ($items as &$item) {
                    $newProductExcludeInstalled = "";
                    $installedBaseProducts = $item->getData('preselect_product');
                    if($installedBaseProducts) {
                        $installedBaseProductsItems = $item->getProduct();
                        if ($installedBaseProductsItems->getAttributeText('voice_product')) {
                            $installProductHasVoice = true;
                        }
                    }
                    if(!$installedBaseProducts) {
                        $newProductExcludeInstalled = $item->getProduct();
                    }
                    if($newProductExcludeInstalled) {
                        if($newProductExcludeInstalled->getHasInternet()) {
                            $ilsProductHasInternet = true;
                        }
                        if($newProductExcludeInstalled->getAttributeText('voice_product')) {
                            $ilsProductHasVoice = true;
                        }
                        if($newProductExcludeInstalled->getAttributeText('serial_number_type')) {
                            $ilsProductHasHardware = true;
                        }
                        if($newProductExcludeInstalled->getAttributeText('capture_ean') && ($newProductExcludeInstalled->getCaptureEan() != 0)) {
                            $ilsProductHasHardware = true;
                        }
                    }
                }
                $cartBlock = $this->getLayout()->createBlock('checkout/cart');
                $hasFixedVoiceProduct = $cartBlock->hasFixedVoiceProduct();
                $hasHardwareProduct = $cartBlock->hasHardwareProducts();
                $cartBlock = Mage::app()->getLayout()->createBlock('checkout/cart');
                $retainCheckStatus = Mage::getSingleton('customer/session')->getRetainTelephoneNumberStatus();

                if (($isFixed && $ilsProductHasVoice && $ilsProductHasHardware && !$isMobile && !$retainCheckStatus) || ($isFixed && !$installProductHasVoice && $ilsProductHasHardware && $isIlsFixed) || ($retainCheckStatus && $isMoveProcess)) {
                    $result['overview_step'] = 'show_numberselection';
                } elseif (($isFixed && !$ilsProductHasVoice && !$ilsProductHasHardware && !$isMobile && !$isIlsFixed) || ($isFixed && $installProductHasVoice && $isIlsFixed && !$isMobile)) {
                    $result['overview_step'] = 'show_contract';
                } elseif (($isRetention && $agentHelper->isIndirectStore() && !$cartBlock->shouldContainDeviceSelectionSection() && !$isFixed ) || ($isMobile && $isFixed && $quote->getFixedDeliveryMethod() == "LSP" && $isRetention && $agentHelper->isIndirectStore() && !$cartBlock->shouldContainDeviceSelectionSection())) {
                    $result['overview_step'] = 'show_credit_contract';
                }

                $this->_esbHelper = Mage::helper('vznl_esb');
                $this->_esbHelper->setSaleOrderStatus($superOrder->getOrderNumber() . ';' . Vznl_Superorder_Model_Superorder::SO_PRE_IN_INITIAL . ';' . true);
                if (!$isMobile && $isFixed && $quote->getFixedDeliveryMethod() == "LSP") {
                    $result['lsp_delivery'] = true;
                }

                $quote->save();
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";

                // Mark the quote as not a cart
                if ($quote->getQuoteParentId()) {
                    $parentQuote = Mage::getModel('sales/quote')->load($quote->getQuoteParentId());
                    if ($parentQuote->getId()) {
                        $parentQuote->setCartStatus(null)
                            ->save();
                    }
                }

                if ($agentHelper->isAssistedChannel()) {
                    //indirect || retail & home delivery hide contract & number selection
                    if ($quote->getFixedDeliveryMethod() == "LSP" && !$isMobile) {
                        $result['skip_number_selection_contract'] = true;
                    }
                    $orderedItems = $superOrder->getOrderedItems(true, false);
                    if (!$isMobile && $isFixed && !$hasFixedVoiceProduct && $quote->getFixedDeliveryMethod() == "LSP") {
                        $result['skip_number_selection_contract'] = true;
                    }
                    if ($this->checkoutHelper->canProcessDeliverySteps($orderedItems) || $isFixed) {
                        $result['contract_page'] = $this->getLayout()
                            ->createBlock('vznl_checkout/cart')
                            ->setShowStep(true)
                            ->setTemplate('checkout/cart/steps/save_contract.phtml')
                            ->toHtml();
                    }
                    if (!Mage::helper('vznl_checkout')->checkInThisStore()) {
                        $result['skip_number_selection'] = true;
                    }
                }

                $this->_getCustomerSession()->setData(
                    $pollingId,
                    array(
                        'superorder_id' => $superOrder->getId()
                    )
                );

                if ($serviceAddress) {
                    $this->saveCustomerServiceAddress($serviceAddress);
                }

                /** @var Dyna_FFHandler_Helper_Request $requestHelper */
                //@todo fix later on
                //$requestHelper = Mage::helper("ffhandler/request");
                //$requestHelper->updateUsageToSuperorder($quote->getId(), $superOrder->getId());

                $request->setPost('superorderNumber', $superOrder->getOrderNumber());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                Mage::getSingleton('core/logger')->logException($e);
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    public function convertBicAction()
    {
        $accountNumber = trim($this->getRequest()->getParam('account_number'));
        $valid = null;
        try {
            $valid = Mage::helper('dyna_validators')->validateNLIBAN($accountNumber);
        } catch (Exception $ex) {
            $result = array(
                'errors' => true,
                'to_fill' => $ex->getMessage()
            );
        }
        if ($valid) {
            $result = array(
                'errors' => false,
                'to_fill' => $valid,
            );
        } else {
            $result = array(
                'errors' => true,
                'message' => $this->checkoutHelper->__('Incorrect bank account (11-test)')
            );
        }

        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(Mage::helper('core')->jsonEncode($result));
//        return;
    }

    public function createLongIbanAction()
    {
        try {
            $quote = $this->getQuote();
            $customer = $quote->getCustomer();
            $accountNumber = $quote->getCustomerAccountNumber();
            $accountNumberLong = Mage::helper('dyna_validators')->validateNLIBAN($accountNumber);
            if ($customer->getAccountNoLong() != $accountNumberLong) {
                $customer->setAccountNoLong($accountNumberLong)->save();
                $quote->updateFields(['customer_account_number_long' => $accountNumberLong]);
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
        }
    }

    public function stockStoresAction()
    {
        $productIds = array();
        $productSkus = array();
        $packageId = trim($this->getRequest()->getParam('package_id'));

        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($this->getQuote()->getAllItems() as $item) {
            if ($packageId && ($item->getPackageId() != $packageId)) {
                continue;
            }
            // TODO Quickfix, move to model
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            if ($product->getIdentifierAxiStockableProd()) {
                array_push($productIds, $item->getProductId());
                array_push($productSkus, $item->getSku());
            }
        }

        try {
            $stockCollection = new Varien_Data_Collection();
            $storeCode = Mage::helper('agent')->getAxiStore();

            foreach ($productIds as $prodId) {
                try {
                    $stocks = Mage::getModel('stock/stock')->getLiveStockForItem(Mage::getModel('catalog/product')->load($prodId), $storeCode);
                } catch (\Exception $e) {
                    Mage::getSingleton('core/logger')->logException($e);
                    $this->getServiceHelper()->returnServiceError($e);
                    return;
                }
                foreach ($stocks as $stock) {
                    $stockCollection->addItem($stock);
                }
            }

            $list = $this->getLayout()->createBlock('core/template')
                ->setTemplate('checkout/cart/store_list.phtml')
                ->setStock($stockCollection)
                ->toHtml();

            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array(
                    'error' => false,
                    'list' => Mage::helper('dyna_configurator')->toArray($list),
                )));
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array(
                    'error' => true,
                    'message' => $this->__('Failed to retrieve stock information.'),
                    'exception' => $e->getMessage(),
                )));
        }
    }

    /**
     * Loads the ILT adapters and saves the ILT data
     *
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param array $result
     * @param bool $unset
     * @return void
     * @throws Exception
     */
    public function triggerILT(
        Vznl_Superorder_Model_Superorder $superOrder,
        Vznl_Checkout_Model_Sales_Quote $quote,
        &$result,
        $unset = true
    ):void
    {
        $request = $this->getRequest();
        $session = $this->_getCustomerSession();
        $agent = $session->getAgent(true);
        $skipStoreIlt = $request->get('skip_store_ilt') ? $request->get('skip_store_ilt') : 0;

        if ($quote->isIltRequired() && $skipStoreIlt != 2) {
            try {
                $iltRepository = new Vznl_Ilt_Repository_SuperorderIlt();
                $loggerWriter = new Aleron75_Magemonolog_Model_Logwriter('services/Ilt/'.\Ramsey\Uuid\Uuid::uuid4().'.log');
                if (Mage::getStoreConfig('vodafone_service/ilt/use_stubs')) {
                    $iltAdapter = Omnius\InkomensLastenToets\ExternalAdapter\Stub\Factory::create($iltRepository, $loggerWriter->getLogger());
                } else {
                    $iltAdapter = Omnius\InkomensLastenToets\ExternalAdapter\Bsl\Factory::create(Mage::getStoreConfig('vodafone_service/ilt/wsdl'), $iltRepository, $loggerWriter->getLogger(),  [
                        'username' => Mage::getStoreConfig('vodafone_service/ilt/bsl_username'),
                        'password' => Mage::getStoreConfig('vodafone_service/ilt/bsl_password'),
                        'agent' => $agent->getUsername(),
                        'dealercode' => $agent->getDealerCode(),
                        'service_endpoint'=> Mage::getStoreConfig('vodafone_service/ilt/usage_url'),
                        'client_options' => Mage::helper('vznl_core/service')->getZendSoapClientOptions('ilt') ? : []
                    ]);
                }

                $incomeTestData = Mage::getSingleton('checkout/session')->getIncomeTestData();

                if (!empty($incomeTestData['choice'])) {
                    $this->_saveIncomeTestData(
                        $incomeTestData,
                        $superOrder
                    );
                } else {
                    /** @var Vznl_Ilt_Model_Ilt $iltData */
                    $iltData = Mage::getModel('ilt/ilt')->load($superOrder->getId(), 'superorder_id');
                    if ($iltData->getEntityId()) {
                        $incomeTestData['choice'] = $superOrder->getIltSelection();
                    }
                }

                if ($incomeTestData['choice'] === 'fill-in-now') {
                    $data = $iltRepository->get($superOrder->getOrderNumber());

                    if (!$iltAdapter->send($data) && Mage::helper('agent')->isRetailStore()) {
                        $this->_getCheckoutSession()->setIltStoreOrderNumber($superOrder->getOrderNumber());
                        Mage::getSingleton('customer/session')->setOrderEdit($quote->getId());
                        $result['error'] = true;
                        $result['ilt_failed'] = true;
                        $result['message'] = $this->checkoutHelper->__("ILT data was not stored");
                        $result['orders'] = $this->checkoutHelper->__('ILT data was not stored');
                        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                            Mage::helper('core')->jsonEncode($result)
                        );
                        return;
                    }

                    // Remove data from database when send is done correctly
                    $iltRepository->delete($data);
                }

                // Remove data from checkout sessions
                Mage::getSingleton('checkout/session')->unsIncomeTestData();

            } catch (\Omnius\InkomensLastenToets\ExternalAdapter\Bsl\Exception\BslException $e) {
                if (Mage::helper('agent')->isRetailStore()) {
                    $this->_getCheckoutSession()->setIltStoreOrderNumber($superOrder->getOrderNumber());
                    Mage::getSingleton('customer/session')->setOrderEdit($quote->getId());
                    $result['error'] = true;
                    $result['ilt_failed'] = true;
                    $result['message'] = $this->checkoutHelper->__("ILT data was not stored");
                    $result['orders'] = $this->checkoutHelper->__('ILT data was not stored');
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }
            } catch (Exception $e) {
                Mage::logException($e);
                $result['error'] = true;
                $result['message'] = $e->getMessage();
                $result['orders'] = $this->checkoutHelper->__('ILT data was not stored');
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        } else {
            if ($unset) {
                Mage::getSingleton('customer/session')->setOrderEdit(null);
            }
            Mage::getSingleton('checkout/session')->unsIncomeTestData();
        }

        /**
         * Store ILT and Loan data on superOrder
         */
        $superOrder->setHasLoan((int)$superOrder->isLoanRequired());
        $superOrder->setHasIlt((int)$superOrder->isIltRequired());
        $superOrder->save();
    }

    public function saveSuperOrderAction()
    {
        Mage::register('is_override_promos', true);
        /** @var Omnius_Audit_Model_History $history */
        $history = Mage::getSingleton('audit/history');

        // Skip order creation for async handling of the ILT errors
        $skipStoreIlt = $this->_request->get('skip_store_ilt') ? $this->_request->get('skip_store_ilt') : 0;

        $checkoutSession = Mage::getSingleton('checkout/session');
        $customerSession = Mage::getSingleton('customer/session');
        if (!$customerSession->getOrderEditMode()) {
            // Set fail response for other errors
            $result['error'] = true;
            $result['message'] = $this->__('Order is locked!');

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        }
        $newSuperOrder = null;

        $orderIdsArray = array();

        $superQuoteId = $customerSession->getOrderEdit();
        /** @var Vznl_Checkout_Model_Sales_Quote $superQuote */
        $superQuote = Mage::getModel('sales/quote')->load($superQuoteId);
        $superOrderId = $superQuote->getSuperOrderEditId();

        /**
         * Check if current order is not locked by another agent
         * It redirects to 'checkout/cart' if is locked
         */
        $this->checkoutHelper->checkOrder($superOrderId);

        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);

        /** @var Vznl_Checkout_Helper_Sales $salesConverter */
        $salesConverter = Mage::helper('vznl_checkout/sales');
        $shippingData = Mage::helper('core')->jsonDecode($superQuote->getData('shipping_data'));
        $changedDeliveredPackages = Mage::helper('dyna_package')->isDeliveredPackageModified();
        $cancelledPackages = Mage::helper('dyna_package')->getCancelledPackagesNow();
        $revertPackageStatus = [];
        $allModifiedPackages = $cancelledPackages;

        $superOrder->setNewCustomer(0)->save();

        /**
         * Snapshot current superorder state
         */
        $history->snapshot($superOrder);

        $quotes = Mage::helper('dyna_package')->getModifiedQuotes();
        $packageQuotes = array();
        /**
         * Gather all ids of edited packages
         * and create an array containing the quotes
         * per package for later use
         */
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        foreach ($quotes->getItems() as $quote) {
            $quote->setIsSuperMode(true);
            $allModifiedPackages[] = $quote->getActivePackageId();
            $packageQuotes[$quote->getActivePackageId()] = $quote;
        }

        /**
         * Gather all ids of packages with changed address
         * We compare the shipping address values
         * with the ones from the super quote "shipping_data" field
         */
        $editedPackagesIds = implode(",",$allModifiedPackages);
        $packagesWithChangedAddress = array();
        $packageShippingData = array();
        $packagePaymentMethod = array();
        $allOrders = Mage::getModel('sales/order')->getNonEditedOrderItems($superOrder->getId());
        $onlyPaymentChanged = true;
        Mage::getSingleton('checkout/session')->setDeliveryChanged(false);
        Mage::getSingleton('checkout/session')->setIltChanged(false);
        /** @var Mage_Sales_Model_Order $order */
        foreach ($allOrders as $order) {
            $orderPackages = $this->extractOrderPackages($order);
            $orderPackageIds = array_keys($orderPackages);

            foreach ($orderPackageIds as $packageId) {
                if (isset($shippingData['pakket']) && isset($shippingData['pakket'][$packageId]) && isset($shippingData['pakket'][$packageId]['address'])) {
                    $initialAddress = $shippingData['pakket'][$packageId]['address'];
                    $packageShippingData[$packageId] = $shippingData['pakket'][$packageId];
                    $packagePaymentMethod[$packageId] = array(
                        'method' => $shippingData['payment'][$packageId],
                        'check' => Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_MULTISHIPPING
                            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                            | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX,
                    );
                } elseif (isset($shippingData['deliver'])) { //this means all packages have the same address (no order split)
                    $initialAddress = $shippingData['deliver']['address'];
                    $packageShippingData[$packageId] = $shippingData['deliver'];
                    $packagePaymentMethod[$packageId] = array(
                        'method' => $shippingData['payment'] ?? $order->getPayment()->getMethod(),
                        'check' => Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_MULTISHIPPING
                            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                            | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX,
                    );
                } else { //BC as some past orders may exist with no shipping_data
                    Mage::log(sprintf('Backward compatibility: Super quote (id:%s) does not have "shipping_data". Skipping address change check.', $superQuote->getId()), Zend_Log::EMERG);
                    continue;
                }

                //also add delivery_type in the retrieval key and change the key from delivery_type to address
                //$currentAddress = $order->getShippingAddress()->toArray(array_merge(array_keys($initialAddress),array('delivery_type','delivery_store_id'))); //extract only the keys present in $initialAddress

                $tmpCurrentAddress = $order->getShippingAddress();

                $currentAddress = array(
                    'street' => array(
                        0 => $tmpCurrentAddress->getStreet(1),
                        1 => $tmpCurrentAddress->getStreet(2),
                        2 => $tmpCurrentAddress->getStreet(3),
                    ),
                    'postcode' => $tmpCurrentAddress->getPostcode(),
                    'city' => $tmpCurrentAddress->getCity(),
                    'address' => $tmpCurrentAddress['delivery_type'],
                    'store_id' => $tmpCurrentAddress['delivery_store_id']
                );

                unset($tmpCurrentAddress);

                $compareArrays = function ($array1, $array2) use (&$compareArrays) {
                    foreach ($array1 as $key => $value) {
                        if (is_array($value)) {
                            if ((!isset($array2[$key])) || (!is_array($array2[$key]))) {
                                $difference[$key] = $value;
                            } else {
                                $new_diff = $compareArrays($value, $array2[$key]);
                                if ($new_diff !== 0) {
                                    $difference[$key] = $new_diff;
                                }
                            }
                        } elseif (!array_key_exists($key, $array2) || $array2[$key] != $value) {
                            $difference[$key] = $value;
                        }
                    }
                    return !isset($difference) ? 0 : $difference;
                };

                if (!isset($initialAddress['store_id'])) {
                    $initialAddress['store_id'] = null;
                }

                if (! isset($initialAddress['street'][2])) {
                    $initialAddress['street'][2] = '';
                }

                /**
                 * If array_diff returns anything, the address has been changed
                 * or payment is different
                 */
                if (($compareArrays($initialAddress, $currentAddress) || $compareArrays($currentAddress, $initialAddress) || $packagePaymentMethod[$packageId]['method'] != $order->getPayment()->getMethod())
                    && !array_key_exists($packageId, $packagesWithChangedAddress) //no duplicates, if there's any chance for that
                ) {

                    if ($compareArrays($initialAddress, $currentAddress) || $compareArrays($currentAddress, $initialAddress)) {
                        $onlyPaymentChanged = false;
                    }
                    $changedAddress = Mage::getModel('sales/order_address');
                    foreach ($order->getShippingAddress()->getData() as $key => $value) {
                        if ($key != 'entity_id' && $key != 'parent_id') {
                            $changedAddress[$key] = $value;
                        }
                    }
                    foreach ($initialAddress as $key => $value) {
                        if ($key == 'street') {
                            $value = implode("\n", $value);
                        }
                        $changedAddress->setData($key, $value);
                    }
                    $changedAddress->setDeliveryType($initialAddress['address']);
                    if ($initialAddress['address'] == 'store') {
                        $changedAddress->setDeliveryStoreId($initialAddress['store_id']);
                    } else {
                        $changedAddress->unsDeliveryStoreId();
                    }
                    $packagesWithChangedAddress[$packageId] = $changedAddress; //save changed address for later use
                }
            }
        }

        /**
         * Closure that returns the quote containing
         * the package with the provided ID
         */
        $getQuoteWithPackage = function ($packageId) use ($packageQuotes) {
            if (isset($packageQuotes[$packageId])) {
                return $packageQuotes[$packageId];
            }
            return false;
        };

        //create a superorder if delivered packages were changed
        if ($changedDeliveredPackages) {
            /** @var Vznl_Superorder_Model_Superorder $newSuperOrder */
            if ($skipStoreIlt == 0) {
                $newSuperOrder = Mage::getModel('superorder/superorder')->createNewSuperorder($superOrder);
            } else {
                $newSuperOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($this->_getCheckoutSession()->getIltStoreOrderNumber());
                $this->_getCheckoutSession()->setIltStoreOrderNumber(null);
            }
        }

        $esbChangedPackageIds = array();
        $esbEditedOrders = array();
        $esbNewOrders = array();
        $esbOrdersWithAddressChanged = array();

        /** @var Vznl_FFHandler_Helper_Request $requestHelper */
        $requestHelper = Mage::helper("ffhandler/request");

        /** @var Vznl_Checkout_Model_Sales_Order $order */
        foreach ($allOrders as $order) {
            $orderPackages = $this->extractOrderPackages($order);
            $orderPackageIds = array_unique(array_keys($orderPackages));
            $orderPackageIdCount = count($orderPackageIds) ;
            $hasEdited = false;
            $hasAddressChange = false;

            foreach ($orderPackageIds as $packageId) {
                /** @var Vznl_Checkout_Model_Sales_Quote $quote */
                if ($quote = $getQuoteWithPackage($packageId)) { //the package was edited if we find a quote for it
                    if (in_array($packageId, array_keys($packagesWithChangedAddress))) { //the package has a changed address as well
                        $hasEdited = true;
                        $hasAddressChange = true;
                    } else { //package was just edited, still has the same address
                        $hasEdited = true;
                    }
                    $esbChangedPackageIds[] = $packageId;
                    $esbEditedOrders[$order->getId()] = $order;

                    /** @var Vznl_Package_Model_Package $oldPackage */
                    $oldPackage = Mage::getModel('package/package')->getCollection()
                        ->addFieldToFilter('order_id', $superOrder->getId())
                        ->addFieldToFilter('package_id', $packageId)
                        ->getLastItem();

                    /** @var Vznl_Package_Model_Package $newPackage */
                    $newPackage = Mage::getModel('package/package')->getCollection()
                        ->addFieldToFilter('quote_id', $quote->getId())
                        ->addFieldToFilter('package_id', $packageId)
                        ->getLastItem();

                    if ($oldPackage->getCoupon() && !$newPackage->getCoupon()) {
                        $requestHelper->removeCodeUsageByPackage($oldPackage);
                    }

                } elseif (in_array($packageId, array_keys($packagesWithChangedAddress))) { //the package was not edited but it has a changed address
                    $hasAddressChange = true;
                    $esbEditedOrders[$order->getId()] = $order;
                }
            }

            if ($hasAddressChange === true) {
                // set a flag to the ESB that the order has the address changed
                $esbOrdersWithAddressChanged[$order->getId()] = $order->getId();
            }

            if ($skipStoreIlt == 0) {
                /**
                 * Decide what actions are needed
                 */
                if ($hasEdited === true && $hasAddressChange === false) { //has only edited packages
                    $onlyPaymentChanged = false;
                    /** @var Vznl_Checkout_Model_Sales_Quote $newQuote */
                    $newQuote = $salesConverter->orderToQuote($order);
                    // Save quote to assure it has an id in db
                    $newQuote->setTotalsCollectedFlag(true)->save();
                    /**
                     * Remove all items contained in the edited packages or if package is delivered or canceled
                     * All packages will be removed if we have changed delivery packages
                     */
                    $editedPackagesArray = array();
                    foreach ($newQuote->getAllItems() as $item) {
                        if ($getQuoteWithPackage($item->getPackageId())
                            || ($changedDeliveredPackages && !in_array($item->getPackageId(), $cancelledPackages))
                            || (!$changedDeliveredPackages && in_array($item->getPackageId(), $cancelledPackages))
                        ) { //if item belongs to an edited package, edited delivered package or canceled package we remove it
                            foreach ($item->getOptions() as $option) {
                                $item->removeOption($option->getCode());
                            }
                            $editedPackagesArray[] = $item->getPackageId();
                            $item->isDeleted(true);
                        }
                    }

                    Mage::getModel('package/package')->deletePackages($newQuote->getId(), $editedPackagesArray);

                    /**
                     * If we have changed packages that were delivered,
                     * we must create a new order for the old super order
                     * containing only the not edited packages
                     * For this we iterate over the quote packages and
                     * remove all packages that were edited
                     */
                    if ($changedDeliveredPackages && $orderPackageIdCount > 1) { //no need for this if the order has only one package
                        /** @var Vznl_Checkout_Model_Sales_Quote $oldQuote */
                        $oldQuote = $salesConverter->orderToQuote($order);
                        $editedPackagesArray = array();
                        $cancelledPackageAsEdited = array();
                        foreach ($oldQuote->getAllItems() as $item) {
                            if ($getQuoteWithPackage($item->getPackageId())) {
                                foreach ($item->getOptions() as $option) {
                                    $item->removeOption($option->getCode());
                                }
                                $editedPackagesArray[] = $item->getPackageId();
                                $item->isDeleted(true);
                            } elseif (in_array($item->getPackageId(), $cancelledPackages)) {
                                $cancelledPackageAsEdited[$item->getPackageId()] = $item->getPackageId();
                            }
                        }

                        Mage::getModel('package/package')->deletePackages($oldQuote->getId(), $editedPackagesArray);

                        $cancelledPackageAsEdited = array_unique($cancelledPackageAsEdited);
                        if ($cancelledPackageAsEdited) {
                            foreach ($cancelledPackageAsEdited as $cancelledPackageId) {

                                $quotePackage = Mage::getModel('package/package')
                                    ->getPackages(null, $newQuote->getId(), $cancelledPackageId)
                                    ->getFirstItem();
                                $revertPackageStatus[] = [
                                    'model' => $quotePackage,
                                    'status' => $quotePackage->getStatus()
                                ];
                                $quotePackage->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED)->save();
                            }
                        }

                        /**
                         * Save new order with all edited packages
                         */
                        $orderData = $order->getShippingAddress()->getData();
                        unset($orderData['entity_id']);
                        unset($orderData['parent_id']);

                        /** @var Vznl_Checkout_Model_Sales_Order $oldOrder */
                        $oldOrder = Mage::getModel('sales/order');
                        $orderPayment = Mage::getModel('sales/order_payment')
                            ->setStoreId($order->getStoreId())
                            ->setMethod($order->getPayment()->getMethod());
                        $oldOrder->setPayment($orderPayment);
                        $oldOrder->addStatusHistoryComment($order->getStatus(), '[Notice] - Order edited (package changed)');
                        $oldOrder->setData($order->getData());
                        $oldOrder->unsetData('entity_id');
                        $oldOrder->unsetData('increment_id');
                        $oldOrder->setSuperorderId($superOrder->getId()); //it will belong to the initial super order
                        $oldQuote->setTotalsCollectedFlag(false)
                            ->collectTotals()
                            ->save();

                        $oldOrder = $salesConverter->quoteToOrder($oldQuote, $oldOrder);

                        /** @var Mage_Sales_Model_Order_Address $address */
                        $address = Mage::getModel('sales/order_address');
                        $address->setData($newQuote->getShippingAddress()->getData());
                        $address->unsetData('entity_id');
                        $address->unsetData('parent_id');
                        $address->setStreet($address->getStreet());
                        $address
                            ->setData('delivery_type', $orderData['delivery_type'])
                            ->setData('payment_type', $orderData['payment_type'])
                            ->setData('delivery_store_id', $orderData['delivery_store_id']);
                        $oldOrder->setShippingAddress($address);

                        $oldOrder->setParentId($order->getId())->setEdited(1)->save();
                    }

                    /**
                     * Merge all edited packages in $newQuote
                     * All unedited packages are already present in $newQuote
                     */
                    foreach ($orderPackageIds as $packageId) {
                        if ($quote = $getQuoteWithPackage($packageId)) {
                            $quote->setIsSuperMode(true);
                            $newQuote->merge($quote);
                        }
                    }

                    /**
                     * Save new order with all edited packages
                     */
                    $orderData = $order->getShippingAddress()->getData();
                    unset($orderData['entity_id']);
                    unset($orderData['parent_id']);
                    /** @var Mage_Sales_Model_Quote_Address $address */
                    $address = Mage::getModel('sales/quote_address')->setData($orderData);
                    $newQuote->setIsSuperMode(true);
                    $newQuote
                        ->setShippingAddress($address->setCollectShippingRates(true))//set the changed address
                        ->setTotalsCollectedFlag(false)//assure totals collect is possible
                        ->collectTotals()//collect totals/update item qty
                        ->save();
                    /** @var Vznl_Checkout_Model_Sales_Order $newOrder */
                    $newOrder = Mage::getModel('sales/order');
                    $orderPayment = Mage::getModel('sales/order_payment')
                        ->setStoreId($order->getStoreId())
                        ->setMethod($order->getPayment()->getMethod());
                    $newOrder->setPayment($orderPayment);
                    $newOrder->addStatusHistoryComment($order->getStatus(), '[Notice] - Order edited (package changed)');
                    $newOrder->setData($order->getData());
                    $newOrder->unsetData('parent_id');
                    $newOrder->unsetData('entity_id');
                    $newOrder->unsetData('increment_id');
                    $newOrder->unsetData('contract_sign_date');
                    if (!$changedDeliveredPackages) {
                        $newOrder->setParentId($order->getId());
                    } else {
                        $newOrder->clearFields();
                    }
                    $newOrder->setSuperorderId(isset($newSuperOrder) ? $newSuperOrder->getId() : $superOrder->getId());

                    $newOrder = $salesConverter->quoteToOrder($newQuote, $newOrder);

                    /** @var Mage_Sales_Model_Order_Address $address */
                    $address = Mage::getModel('sales/order_address');
                    $address->setData($newQuote->getShippingAddress()->getData());
                    $address->unsetData('entity_id');
                    $address->unsetData('parent_id');
                    $address->setStreet($address->getStreet());
                    $address
                        ->setData('delivery_type', $orderData['delivery_type'])
                        ->setData('payment_type', $orderData['payment_type'])
                        ->setData('delivery_store_id', $orderData['delivery_store_id']);
                    $newOrder->setEditedPackagesIds($editedPackagesIds);
                    $newOrder->setShippingAddress($address);
                    $newOrder->setEdited(0);
                    $newOrder->save();
                    if (isset($esbEditedOrders[$order->getId()])) {
                        $esbNewOrders[$order->getId()][$newOrder->getId()] = $newOrder;
                    }

                    $orderIdsArray[$newOrder->getId()] = $newOrder->getIncrementId();

                    $agentHelper = Mage::helper('vznl_agent');
                    $dealerData = $agentHelper->getDealerData();
                    if ($order->getShippingAddress()->getData('delivery_type') != "store" && Mage::app()->getStore()->getCode() != Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                        $result['skip_number_selection'] = true;
                    } else if ($orderData['delivery_type'] == "store" && $orderData['delivery_store_id'] != $dealerData->getId()) {
                        $result['skip_number_selection'] = true;
                    }

                    if (!$changedDeliveredPackages) {
                        $order->setEditedOrder(1);
                    }

                } elseif ($hasEdited === true && $hasAddressChange === true) { //has both edited and with address changed packages
                    $onlyPaymentChanged = false;
                    /** @var Vznl_Checkout_Model_Sales_Quote $newQuote */
                    $newQuote = $salesConverter->orderToQuote($order);
                    // Save quote to assure it has an id in db
                    $newQuote->setTotalsCollectedFlag(true)->save();

                    $editedPackagesArray = array();
                    /**
                     * Remove all items contained in the edited packages or if delivered
                     */
                    foreach ($newQuote->getAllItems() as $item) {
                        //if item belongs to an edited package or has changed address or was cancelled, remove it
                        if ($getQuoteWithPackage($item->getPackageId())
                            || in_array($item->getPackageId(), array_keys($packagesWithChangedAddress))
                            || ($changedDeliveredPackages && !in_array($item->getPackageId(), $cancelledPackages))
                            || (!$changedDeliveredPackages && in_array($item->getPackageId(), $cancelledPackages))
                        ) {
                            foreach ($item->getOptions() as $option) {
                                $item->removeOption($option->getCode());
                            }
                            $editedPackagesArray[] = $item->getPackageId();
                            $item->isDeleted(true);
                        }
                    }

                    Mage::getModel('package/package')->deletePackages($newQuote->getId(), $editedPackagesArray);
                    /**
                     * Merge all edited packages in $newQuote
                     * All unedited packages are already present in $newQuote
                     */
                    $handledPackages = [];
                    $cancelledPackageAsEdited = [];
                    foreach ($orderPackageIds as $packageId) {
                        if (($quote = $getQuoteWithPackage($packageId)) && (!in_array($packageId, array_keys($packagesWithChangedAddress)))) { //if package was edit BUT still has the same address  || $changedDeliveredPackages
                            $newQuote->merge($quote);
                            array_push($handledPackages, $packageId);
                        }
                        if (in_array($packageId, $cancelledPackages) && $changedDeliveredPackages) {
                            array_push($handledPackages, $packageId);
                            array_push($cancelledPackageAsEdited, $packageId);
                        }
                    }
                    if ($cancelledPackageAsEdited) {
                        foreach ($cancelledPackageAsEdited as $cancelledPackageId) {
                            $quotePackage = Mage::getModel('package/package')
                                ->getPackages(null, $newQuote->getId(), $cancelledPackageId)
                                ->getFirstItem();
                            $revertPackageStatus[] = [
                                'model' => $quotePackage,
                                'status' => $quotePackage->getStatus()
                            ];
                            $quotePackage->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED)->save();
                        }
                    }
                    /**
                     * Special case when all order packages where edited,
                     * their address changed and are all delivered
                     * Then we should not have any items in $newQuote
                     */
                    if (!empty($newQuote->getAllItems())) {
                        /**
                         * Regenerate shipping_data for new quote
                         * As this quote has packages with same address,
                         * we only the address of any package from the quote
                         */
                        $firstPackage = reset($handledPackages);
                        $newShippingData = array();
                        if ($firstPackage) {
                            $newShippingData = array(
                                'deliver' => $packageShippingData[$firstPackage],
                            );
                        }

                        /**
                         * Save quote containing all edited and unedited packages
                         * all having the same address as before
                         */
                        /** @var Vznl_Checkout_Model_Sales_Order $newOrder */
                        $newOrder = Mage::getModel('sales/order');

                        $orderPayment = Mage::getModel('sales/order_payment')
                            ->setStoreId($order->getStoreId())
                            ->setMethod($order->getPayment()->getMethod());
                        $newOrder->setPayment($orderPayment);

                        $newOrder->addStatusHistoryComment($order->getStatus(), '[Notice] - Order edited (package edited)');
                        $newOrder->setData($order->getData());
                        $newOrder->unsetData('parent_id');
                        $newOrder->unsetData('entity_id');
                        $newOrder->unsetData('increment_id');
                        $newOrder->unsetData('contract_sign_date');
                        if (!$changedDeliveredPackages) {
                            $newOrder->setParentId($order->getId());
                        } else {
                            $newOrder->clearFields();
                        }
                        $newOrder->setSuperorderId(isset($newSuperOrder) ? $newSuperOrder->getId() : $superOrder->getId());
                        $newQuote
                            ->getShippingAddress()->setCollectShippingRates(true);
                        $newQuote->setIsSuperMode(true);
                        $newQuote
                            ->setTotalsCollectedFlag(false)
                            ->collectTotals()
                            ->setShippingData(Mage::helper('core')->jsonEncode($newShippingData))
                            ->save();
                        $newOrder = $salesConverter->quoteToOrder($newQuote, $newOrder);

                        $address = Mage::getModel('sales/order_address');
                        $address->setData($newQuote->getShippingAddress()->getData());
                        $address->unsetData('entity_id');
                        $address->unsetData('parent_id');
                        $address->setStreet($address->getStreet());
                        $address
                            ->setData('delivery_type', $order->getShippingAddress()->getData('delivery_type'))
                            ->setData('payment_type', $order->getShippingAddress()->getData('payment_type'))
                            ->setData('delivery_store_id', $order->getShippingAddress()->getData('delivery_store_id'));
                        $newOrder->setShippingAddress($address);
                        $newOrder->setEditedPackagesIds($editedPackagesIds);
                        $newOrder->setEdited(0);
                        $newOrder->save();
                        $orderIdsArray[$newOrder->getId()] = $newOrder->getIncrementId();

                        if (!$changedDeliveredPackages) {
                            $order->setEditedOrder(1);
                        }
                    }

                    /**
                     * We must keep reference to all created quotes and package IDs for the packages with changed addresses
                     * as one package even if it has a changed address, it can still have the same address
                     * as another package, meaning that both packages must reside in the same quote
                     */
                    $quotesWithChangedAddresses = array();
                    if ($unhandledPackages = array_diff($orderPackageIds, $handledPackages)) { //has packages with changed addresses
                        foreach ($unhandledPackages as $packageId) {
                            if (isset($packagesWithChangedAddress[$packageId])
                                && ($changedAddress = $packagesWithChangedAddress[$packageId])
                                && ($paymentMethod = $packagePaymentMethod[$packageId])
                            ) { //package has different address
                                /**
                                 * Check if another package has the same address
                                 */
                                $addressHash = md5(serialize($changedAddress->toArray()) . serialize($paymentMethod));
                                if (isset($quotesWithChangedAddresses[$addressHash])) {
                                    $previousQuote = $quotesWithChangedAddresses[$addressHash];
                                } else {
                                    $previousQuote = null;
                                }
                                $newQuote = $salesConverter->orderToQuote($order); //create quote from order
                                // Save quote to assure it has an id in db
                                $newQuote->setTotalsCollectedFlag(true)->save();
                                $editedPackagesArray = array();
                                if ($editedQuote = $getQuoteWithPackage($packageId)) { //is also edited
                                    /**
                                     * Remove all packages as we need an empty quote
                                     */
                                    foreach ($newQuote->getAllItems() as $item) {
                                        foreach ($item->getOptions() as $option) {
                                            $item->removeOption($option->getCode());
                                        }
                                        $editedPackagesArray[] = $item->getPackageId();
                                        $item->isDeleted(true);
                                    }
                                    Mage::getModel('package/package')->deletePackages($newQuote->getId(), $editedPackagesArray);
                                    $newQuote->merge($editedQuote); //add edited package to new quote
                                } else { //only the address changed
                                    /**
                                     * Remove all packages with different package ID
                                     * as we need to keep only the package items from the package
                                     * with a different address
                                     */
                                    foreach ($newQuote->getAllItems() as $item) {
                                        if ($item->getPackageId() != $packageId) {
                                            foreach ($item->getOptions() as $option) {
                                                $item->removeOption($option->getCode());
                                            }
                                            $editedPackagesArray[] = $item->getPackageId();
                                            $item->isDeleted(true);
                                        }
                                    }
                                }

                                /**
                                 * If previous with the same address exists
                                 * merge it with current and throw it away
                                 */
                                if ($previousQuote) {
                                    $previousQuote->setIsSuperMode(true);
                                    $newQuote->merge($previousQuote); //move all packages from previous to current quote as they all have the same address
                                }

                                $address = Mage::getModel('sales/quote_address')->setData($changedAddress->getData());
                                $newQuote
                                    ->setShippingAddress($address->setCollectShippingRates(true))//set the changed address
                                    ->setTotalsCollectedFlag(false)//assure totals collect is possible
                                    ->collectTotals(); //collect totals/update item qty
                                $newQuote->removePayment();
                                $newQuote->save();
                                $quotesWithChangedAddresses[$addressHash] = $newQuote;

                                unset($unhandledPackages[$packageId]); //remove package ID from unhandledPackages as now it is handled
                            }
                        }
                    }
                    /**
                     * Save all newly created quotes that contain
                     * packages with changed address, if there are any
                     */
                    foreach ($quotesWithChangedAddresses as $newQuote) {
                        /**
                         * Regenerate shipping_data for new quote
                         */
                        $packageId = null;
                        foreach ($newQuote->getAllItems() as $item) {
                            $packageId = $item->getPackageId();
                            break;
                        }

                        //Add package to the handled packages to avoid keeping it in the old super order.
                        $handledPackages[$packageId] = $packageId;
                        $newShippingData = array();
                        if ($packageId) {
                            $newShippingData = array(
                                'deliver' => $packageShippingData[$packageId],
                            );
                        }

                        /** @var Vznl_Checkout_Model_Sales_Order $newOrder */
                        $newOrder = Mage::getModel('sales/order');

                        $orderPayment = Mage::getModel('sales/order_payment')
                            ->setStoreId($order->getStoreId())
                            ->setMethod($packagePaymentMethod[$packageId]['method']);
                        $newOrder->setPayment($orderPayment);

                        $newOrder->addStatusHistoryComment($order->getStatus(), '[Notice] - Order edited (address changed)');
                        $newOrder->setData($order->getData());
                        $newOrder->unsetData('parent_id');
                        $newOrder->unsetData('entity_id');
                        $newOrder->unsetData('increment_id');
                        $newOrder->unsetData('contract_sign_date');
                        if (!$changedDeliveredPackages) {
                            $newOrder->setParentId($order->getId());
                        } else {
                            $newOrder->clearFields();
                        }
                        $newOrder->setSuperorderId(isset($newSuperOrder) ? $newSuperOrder->getId() : $superOrder->getId());

                        $newQuote->getShippingAddress()->setCollectShippingRates(true)
                            ->setTotalsCollectedFlag(false)
                            ->collectTotals()
                            ->setShippingData(Mage::helper('core')->jsonEncode($newShippingData))
                            ->save();
                        $newOrder = $salesConverter->quoteToOrder($newQuote, $newOrder);

                        $address = Mage::getModel('sales/order_address');
                        $address->setData($newQuote->getShippingAddress()->getData());
                        $address->unsetData('entity_id');
                        $address->unsetData('parent_id');
                        $address->setStreet($address->getStreet());
                        $address
                            ->setData('delivery_type', $packagesWithChangedAddress[$packageId]->getData('delivery_type'))
                            ->setData('payment_type', $packagesWithChangedAddress[$packageId]->getData('payment_type'))
                            ->setData('delivery_store_id', $packagesWithChangedAddress[$packageId]->getData('delivery_store_id'));

                        $newOrder->setEditedPackagesIds($editedPackagesIds);
                        $newOrder->setEdited(0);
                        $newOrder->setShippingAddress($address);
                        $newOrder->save();

                        $agentHelper = Mage::helper('vznl_agent');
                        $dealerData = $agentHelper->getDealerData();
                        if ($packagesWithChangedAddress[$packageId]->getData('delivery_type') != "store" && Mage::app()->getStore()->getCode() != Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                            $result['skip_number_selection'] = true;
                        } else if ($packagesWithChangedAddress[$packageId]->getData('delivery_type') == "store" && $packagesWithChangedAddress[$packageId]->getData('delivery_store_id') != $dealerData->getId()) {
                            $result['skip_number_selection'] = true;
                        }

                        if (isset($esbEditedOrders[$order->getId()])) {
                            $esbNewOrders[$order->getId()][$newOrder->getId()] = $newOrder;
                        }

                        $orderIdsArray[$newOrder->getId()] = $newOrder->getIncrementId();

                        if (!$changedDeliveredPackages) {
                            $order->setEditedOrder(1);
                        }
                    }

                    $unhandledPackages = array_diff($orderPackageIds, $handledPackages);
                    $unhandledPackageCount = !empty($unhandledPackages);
                    /**
                     * Special case when order has delivered packages
                     * and we are left with unhandled packages for which
                     * we need to create an new order and set is as belonging
                     * to the old super order
                     */
                    if ($changedDeliveredPackages && $unhandledPackageCount) {
                        /** @var Vznl_Checkout_Model_Sales_Quote $oldQuote */
                        $oldQuote = $salesConverter->orderToQuote($order);
                        $editedPackagesArray = array();
                        foreach ($oldQuote->getAllItems() as $item) {
                            if (!in_array($item->getPackageId(), $unhandledPackages)) { //remove already handled items
                                foreach ($item->getOptions() as $option) {
                                    $item->removeOption($option->getCode());
                                }
                                $editedPackagesArray[] = $item->getPackageId();
                                $item->isDeleted(true);
                            }
                        }

                        Mage::getModel('package/package')->deletePackages($oldQuote->getId(), $editedPackagesArray);

                        /**
                         * Save new order with all edited packages
                         */
                        $orderData = $order->getShippingAddress()->getData();
                        unset($orderData['entity_id']);
                        unset($orderData['parent_id']);
                        /** @var Mage_Sales_Model_Quote_Address $address */
                        $quoteAddress = Mage::getModel('sales/quote_address')->setData($orderData);

                        /** @var Vznl_Checkout_Model_Sales_Order $oldOrder */
                        $oldOrder = Mage::getModel('sales/order');
                        $orderPayment = Mage::getModel('sales/order_payment')
                            ->setStoreId($order->getStoreId())
                            ->setMethod($order->getPayment()->getMethod());
                        $oldOrder->setPayment($orderPayment);
                        $oldOrder->addStatusHistoryComment($order->getStatus(), '[Notice] - Order edited (package changed)');
                        $oldOrder->setData($order->getData());
                        $oldOrder->unsetData('entity_id');
                        $oldOrder->unsetData('increment_id');
                        if (!$changedDeliveredPackages) {
                            $oldOrder->setParentId($order->getId());
                        } else {
                            $oldOrder->clearFields();
                        }
                        $oldOrder->setSuperorderId($superOrder->getId()); //it will belong to the initial super order

                        $oldQuote
                            ->getShippingAddress()->setCollectShippingRates(true)
                            ->setTotalsCollectedFlag(false)
                            ->collectTotals();
                        $oldQuote->save();

                        $oldOrder = $salesConverter->quoteToOrder($oldQuote, $oldOrder);

                        /** @var Mage_Sales_Model_Order_Address $address */
                        $address = Mage::getModel('sales/order_address');
                        $address->setData($quoteAddress->getData());
                        $address->unsetData('entity_id');
                        $address->unsetData('parent_id');
                        $address->setStreet($quoteAddress->getStreet());
                        $address
                            ->setData('delivery_type', $orderData['delivery_type'])
                            ->setData('payment_type', $orderData['payment_type'])
                            ->setData('delivery_store_id', $orderData['delivery_store_id']);
                        $oldOrder->setShippingAddress($address);

                        $oldOrder->setParentId($order->getId())->setEdited(true)->save();
                    }
                } elseif ($hasEdited === false && $hasAddressChange === true) { //has only address changed packages
                    /**
                     * We must keep reference to all created quotes and package IDs as one package
                     * even if it has a changed address, it can still have the same address
                     * as another package, meaning that both packages must reside in the same quote
                     */
                    $quotesWithChangedAddresses = array();
                    $handledPackages = array();
                    foreach ($orderPackageIds as $packageId) {
                        if (isset($packagesWithChangedAddress[$packageId])
                            && ($changedAddress = $packagesWithChangedAddress[$packageId])
                            && ($paymentMethod = $packagePaymentMethod[$packageId])
                        ) { //package has different address

                            /**
                             * Check if another package has the same address
                             */
                            $addressHash = md5(serialize($changedAddress->toArray()) . serialize($paymentMethod));
                            if (isset($quotesWithChangedAddresses[$addressHash])) {
                                $previousQuote = $quotesWithChangedAddresses[$addressHash];
                            } else {
                                $previousQuote = null;
                            }

                            $newQuote = $salesConverter->orderToQuote($order);

                            $editedPackagesArray = array();

                            /**
                             * Remove all packages with different package Id
                             */
                            foreach ($newQuote->getAllItems() as $item) {
                                if ($item->getPackageId() != $packageId || in_array($item->getPackageId(), $cancelledPackages)) {
                                    foreach ($item->getOptions() as $option) {
                                        $item->removeOption($option->getCode());
                                    }
                                    $editedPackagesArray[] = $item->getPackageId();
                                    $item->isDeleted(true);
                                }
                            }

                            Mage::getModel('package/package')->deletePackages($newQuote->getId(), $editedPackagesArray);

                            /**
                             * If previous with the same address exists
                             * merge it with current and throw it away
                             */
                            if ($previousQuote) {
                                $newQuote->merge($previousQuote); //move all packages from previous to current quote as they all have the same address
                            }

                            $address = clone $newQuote->getShippingAddress();
                            $address->setData('entity_id', null);
                            $address->setData('parent_id', null);
                            $address = $address->setData($changedAddress->getData());

                            $newQuote->setShippingAddress($address->setCollectShippingRates(true));

                            $quotesWithChangedAddresses[$addressHash] = $newQuote;

                            array_push($handledPackages, $packageId); //save handled package ID reference
                        }
                    }

                    /**
                     * Save all newly created quotes
                     */
                    foreach ($quotesWithChangedAddresses as $newQuote) {
                        // Ignore empty quotes
                        if (!$newQuote->getAllItems()) {
                            continue;
                        }

                        $newQuote->setTotalsCollectedFlag(false)//assure totals collect is possible
                        ->collectTotals()
                            ->save(); //collect totals/update item qty

                        /**
                         * Regenerate shipping_data for new quote
                         */
                        $packageId = null;
                        foreach ($newQuote->getAllItems() as $item) {
                            $packageId = $item->getPackageId();
                            break;
                        }
                        $newShippingData = array();
                        if ($packageId) {
                            $newShippingData = array(
                                'deliver' => $packageShippingData[$packageId],
                            );
                        }

                        /** @var Vznl_Checkout_Model_Sales_Order $newOrder */
                        $newOrder = Mage::getModel('sales/order');

                        $newOrder->addStatusHistoryComment($order->getStatus(), '[Notice] - Order edited (address changed)');
                        $newOrder->setData($order->getData());
                        $newOrder->unsetData('parent_id');
                        $newOrder->unsetData('entity_id');
                        $newOrder->unsetData('increment_id');
                        $newOrder->unsetData('contract_sign_date');
                        if (!$changedDeliveredPackages) {
                            $newOrder->setParentId($order->getId());
                        } else {
                            $newOrder->clearFields();
                        }
                        $newOrder->setSuperorderId(isset($newSuperOrder) ? $newSuperOrder->getId() : $superOrder->getId());

                        $newQuote->setShippingData(Mage::helper('core')->jsonEncode($newShippingData))->save();

                        $newOrder = $salesConverter->quoteToOrder($newQuote, $newOrder);

                        $address = Mage::getModel('sales/order_address');
                        $address->setData($newQuote->getShippingAddress()->getData());
                        $address->unsetData('entity_id');
                        $address->unsetData('parent_id');
                        $address->setStreet($address->getStreet());
                        if (isset($packagesWithChangedAddress[$packageId])) {
                            $address->setDeliveryType($packagesWithChangedAddress[$packageId]->getDeliveryType());
                            $address->setDeliveryStoreId($packagesWithChangedAddress[$packageId]->getDeliveryStoreId());
                        }
                        $newOrder->setShippingAddress($address);
                        $orderPayment = Mage::getModel('sales/order_payment')
                            ->setStoreId(Mage::app()->getStore()->getId())
                            ->setCustomerPaymentId(0)
                            ->setMethod($packagePaymentMethod[$packageId]['method']);
                        $newOrder->setEditedPackagesIds($editedPackagesIds);
                        $newOrder->setPayment($orderPayment);
                        $newOrder->setEdited(0);
                        $newOrder->save();

                        if (isset($esbEditedOrders[$order->getId()])) {
                            $esbNewOrders[$order->getId()][$newOrder->getId()] = $newOrder;
                        }

                        $orderIdsArray[$newOrder->getId()] = $newOrder->getIncrementId();

                        if (!$changedDeliveredPackages) {
                            $order->setEditedOrder(1);
                        }
                    }

                    /**
                     * If we have any "untouched" packages, we must create
                     * a new order containing them
                     */
                    if ($unhandledPackages = array_diff($orderPackageIds, $handledPackages)) {
                        /** @var Vznl_Checkout_Model_Sales_Quote $newQuote */
                        $newQuote = $salesConverter->orderToQuote($order);

                        $editedPackagesArray = array();
                        /**
                         * Remove all packages that were already handled
                         */
                        foreach ($newQuote->getAllItems() as $item) {
                            if (!in_array($item->getPackageId(), $unhandledPackages) || in_array($item->getPackageId(), $cancelledPackages)) {
                                foreach ($item->getOptions() as $option) {
                                    $item->removeOption($option->getCode());
                                }
                                $editedPackagesArray[] = $item->getPackageId();
                                $item->isDeleted(true);
                            }
                        }

                        Mage::getModel('package/package')->deletePackages($newQuote->getId(), $editedPackagesArray);

                        /**
                         * Regenerate shipping_data for new quote
                         * As this quote has packages with same address,
                         * we only the address of any package from the quote
                         */
                        $firstPackage = reset($unhandledPackages);
                        $newShippingData = array();
                        if ($firstPackage) {
                            $newShippingData = array(
                                'deliver' => $packageShippingData[$firstPackage],
                            );
                        }

                        $newQuote
                            ->setTotalsCollectedFlag(false)//assure totals collect is possible
                            ->collectTotals()//collect totals/update item qty
                            ->save();

                        $newOrder = Mage::getModel('sales/order');
                        $newOrder = $salesConverter->quoteToOrder($newQuote, $newOrder, true);
                        $newOrder->getShippingAddress()->setData('delivery_type', $order->getShippingAddress()->getData('delivery_type'))
                            ->setData('delivery_store_id', $order->getShippingAddress()->getData('delivery_store_id'));

                        $newOrder->addStatusHistoryComment($order->getStatus(), '[Notice] - Order edited (order split due to address change)');

                        $newOrder->setData($order->getData());
                        $newOrder->unsetData('parent_id');
                        $newOrder->unsetData('entity_id');
                        $newOrder->unsetData('increment_id');
                        $newOrder->unsetData('contract_sign_date');
                        $newOrder->setEdited(0);
                        if (!$changedDeliveredPackages) {
                            $newOrder->setParentId($order->getId());
                        }
                        $newOrder->setSuperorderId(isset($newSuperOrder) ? $newSuperOrder->getId() : $superOrder->getId());

                        $newQuote->setShippingData(Mage::helper('core')->jsonEncode($newShippingData))->setIsActive(0)->save();
                        $orderPayment = Mage::getModel('sales/order_payment')
                            ->setStoreId(Mage::app()->getStore()->getId())
                            ->setCustomerPaymentId(0)
                            ->setMethod($order->getPayment()->getMethod());
                        $newOrder->setEditedPackagesIds($editedPackagesIds);
                        $newOrder->setPayment($orderPayment);
                        $newOrder->save();

                        $orderIdsArray[$newOrder->getId()] = $newOrder->getIncrementId();

                        if (isset($esbEditedOrders[$order->getId()])) {
                            $esbNewOrders[$order->getId()][$newOrder->getId()] = $newOrder;
                        }

                        if (!$changedDeliveredPackages) {
                            $order->setEditedOrder(1);
                        }
                    }
                }
            }
        }

        $iltSuperOrder = $changedDeliveredPackages ? $newSuperOrder : $superOrder;
        /**
         * Trigger storeILT
         */
        $this->triggerILT($iltSuperOrder, $superQuote, $result, false);
        if ($result['error']) {
            // add to checkout session because it is needed on a retry
            if ($skipStoreIlt == 0) {
                foreach ($esbNewOrders as $existingOrderId => $newOrders) {
                    foreach ($newOrders as $newOrderId => $newOrder) {
                        $esbNewOrders[$existingOrderId][$newOrderId] = null;
                    }
                }

                $this->_getCheckoutSession()->setData('ilt_order_ids_array', $orderIdsArray);
                $this->_getCheckoutSession()->setData('ilt_new_orders', $esbNewOrders);
            }
            return;
        } else if ($skipStoreIlt > 0) {
            // On a retry the following data is still missing so get it from the session
            $orderIdsArray = $this->_getCheckoutSession()->getData('ilt_order_ids_array');
            $esbNewOrders = $this->_getCheckoutSession()->getData('ilt_new_orders');
            foreach ($esbNewOrders as $existingOrderId => $newOrders) {
                foreach ($newOrders as $newOrderId => $newOrder) {
                    $esbNewOrders[$existingOrderId][$newOrderId] = Mage::getModel('sales/order')->load($newOrderId);
                }
            }
        }

        $refundAmounts = ['payments' => [], 'refundAmount' => []];

        if ($cancelledPackages) {
            try {
                //handle removed packages
                /** @var $helper Vznl_Checkout_Helper_Sales */
                $helper = Mage::helper('vznl_checkout/sales');

                // Get refund reason from session
                // setCancelRefundReason() is called in CartController.php method: beforeEditSuperorderPackageAction
                $refundReason = $checkoutSession->getCancelRefundReason();
                $returnChannel = $checkoutSession->getReturnChannel();

                $refundAmounts = $helper->handleCancelledPackages(null, $this->getRequest()->getPost('refund_method'), $refundReason, $returnChannel);
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }

            $checkoutSession->unsCancelRefundReason(); // clear session variable
        }

        //Add the refund amounts for the changed packages to the cancelled packages amounts
        $refundAmountsChanged = Mage::helper('superorder')->calculatePackageDifference();
        $refundAmounts['payments'] = $refundAmounts['payments'] + $refundAmountsChanged;
        $refundAmounts['refundAmount'] = $refundAmounts['refundAmount'] + $refundAmountsChanged;

        // Append the address to the response in order to have them in the credit check step
        //$result['addresses'] = $this->_buildCreditCheckAddresses($superQuote);
        // Get the new super order numbers if there are any new created

        if ($changedDeliveredPackages) {
            $esbOldSuperOrder = null;
            $esbSuperOrder = $newSuperOrder;
            $result['orders'] = $newSuperOrder->getOrderNumber();
            $oldSuperOrder = Mage::getModel('superorder/superorder')->load($newSuperOrder->getParentId()); /** @var Vznl_Superorder_Model_Superorder $oldSuperOrder */
            if ($oldSuperOrder->getId()) {
                $esbOldSuperOrder = $oldSuperOrder;
                $result['orders'] = $oldSuperOrder->getOrderNumber();
                $result['new_order'] = $newSuperOrder->getOrderNumber();
                $result['new_order_id'] = $newSuperOrder->getId();

                /**
                 * Set hardware_change flag
                 */
                $packagesData = array();
                $packagesData['old'] = array();
                $packagesData['new'] = array();
                $oldPackages = $oldSuperOrder->getPackages();
                foreach ($newSuperOrder->getOrders(true) as $order) { /** @var Vznl_Checkout_Model_Sales_Order $order */
                    foreach ($order->getAllVisibleItems() as $item) {/** @var Vznl_Checkout_Model_Sales_Order_Item $item */
                        $packagesData['new'][$item->getPackageId()][$item->getProductId()] = $item;
                    }
                }
                foreach ($oldSuperOrder->getOrders(true) as $order) { /** @var Vznl_Checkout_Model_Sales_Order $order */
                    foreach ($order->getAllVisibleItems() as $item) {/** @var Vznl_Checkout_Model_Sales_Order_Item $item */
                        if (in_array($item->getPackageId(), $esbChangedPackageIds)) {
                            $packagesData['old'][$item->getPackageId()][$item->getProductId()] = $item;
                        }
                    }
                }
                foreach ($packagesData['old'] as $packageId => $items) {
                    foreach ($items as $item) { /** @var Vznl_Checkout_Model_Sales_Order_Item $item */
                        if (($item->getProduct()->isRequiredTobeReturned()) &&
                            (((isset($packagesData['new'][$packageId][$item->getProductId()])) && $packagesData['new'][$packageId][$item->getProductId()]->getItemDoa())
                                || !isset($packagesData['new'][$packageId][$item->getProductId()]))) {
                            $package = $oldPackages->getItemByColumnValue('package_id', $packageId);
                            if ($package && $package->getId()) {
                                $package
                                    ->setData('hardware_change', true)
                                    ->save();
                            }
                        }
                    }
                }
                unset($packagesData);
            }
            Mage::getSingleton('customer/session')->setLastSuperOrder($newSuperOrder->getId());
            $newSuperOrder->sortPackages();
        } else {
            $esbOldSuperOrder = null;
            $esbSuperOrder = $superOrder;
            $result['orders'] = $superOrder->getOrderNumber();
            Mage::getSingleton('customer/session')->setLastSuperOrder($superOrder->getId());
        }

        // Set updated at timestamp
        $superOrder->setUpdatedAt(now())->save();

        // From this point on cache all models for faster processing, since they won't be changed
        if (!Mage::registry('freeze_models')) {
            Mage::unregister('freeze_models');
            Mage::register('freeze_models', true);
        }

        $result['orders'] = $superOrder->getOrderNumber();
        $result['order_id'] = $superOrder->getId();
        $result['addresses'] = $this->_buildCreditCheckAddresses($superQuote);

        // If no changes on items then no delivery order is created so this order has to be unlocked so the esb can work with it.
        if (empty($packageQuotes)) {
            /** @var Dyna_Agent_Helper_Data $agentHelper */
            $agentHelper = Mage::helper('agent');
            $client = Mage::helper('omnius_service')->getClient('lock_manager');
            $lockInfo = $client->doGetLockInfo($superOrder);
            $lockUser = isset($lockInfo['lock']) ? ($lockInfo['lock']['user_i_d'] ?: null) : null;
            if ($lockUser != 'ESB' && !is_null($lockUser)) {
                $client->doHandOverLock($superOrder->getOrderNumber(), $lockUser, 'ESB');
            } else if($lockUser != 'ESB'){
                $client->doHandOverLock($superOrder->getOrderNumber(), $agentHelper->getAgentId(), 'ESB');
            }
        }

        Mage::unregister('is_override_promos');
        /** @var Omnius_Service_Model_Client_EbsOrchestrationClient $client */
        try {
            $serviceHelper = $this->getServiceHelper();
            $toBePaidAmount = Mage::getSingleton('customer/session')->getToPayAmount();
            $client = $serviceHelper->getClient('ebs_orchestration');

            $returnOnlyXml = false;

            $esbSuperOrder->setOnlyPaymentChanged($onlyPaymentChanged);
            $editedOrders = (!empty($esbEditedOrders) || !empty($esbNewOrders)) ? array($esbEditedOrders, $esbNewOrders) : null;
            $poolId = $client->processCustomerOrder(
                $esbSuperOrder,
                $esbOldSuperOrder,
                $editedOrders,
                $esbChangedPackageIds,
                $esbOrdersWithAddressChanged,
                $cancelledPackages,
                $refundAmounts,
                $this->getRequest()->getPost('refund_method'),
                null,
                $toBePaidAmount,
                $returnOnlyXml
            ); //call esb
            $revertPackageStatus = [];

            if ($returnOnlyXml) {
                $esbSuperOrder->setXmlToBeSent($poolId)->save();
                $poolId = null;
            }

            $checkoutSession->setData('super_quote_id', $superQuote->getId());
            $checkoutSession->setData('superorder_id', $superOrderId);
            $checkoutSession->setData('process_order_job_id', $poolId);

            if ($cancelledPackages) {
                $sessionQuoteId = $customerSession->getOrderEdit();
                $quote = Mage::getModel('sales/quote')->load($sessionQuoteId);
                $orderId = null;
                foreach ($cancelledPackages as $packageId) {
                    foreach ($quote->getAllItems() as $item) {
                        if ($item->getPackageId() == $packageId) {
                            $orderId = $item->getEditOrderId();
                            break;
                        }
                    }

                    if ($orderId) {
                        $order = Mage::getModel('sales/order')->load($orderId);

                        /** @var Vznl_Package_Model_Package $packageModel */
                        $packageModel = Mage::getModel('package/package')
                            ->getPackages($order->getSuperorderId(), null, $packageId)
                            ->getFirstItem();

                        if ($packageModel->getId()) {
                            // Delete CTN from customer_ctn table if it was not a retention package
                            if (!$packageModel->isRetention()) {
                                $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                                $write->exec("DELETE FROM customer_ctn WHERE ctn = '" . $packageModel->getTelNumber() . "' AND customer_id = '" . $order->getCustomerId() . "'");
                            }
                            // Set package status to cancelled only if there is no new superorder created
                            if ($esbOldSuperOrder === null) {
                                $packageModel
                                    ->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED)
                                    ->save();
                            }
                        }
                    }
                }
            }

            $data = array(
                'esb_changed_package_ids' =>  $esbChangedPackageIds,
                'order_ids_array' =>  $orderIdsArray,
                'all_modified_packages' =>  $allModifiedPackages,
                'esb_old_super_order' =>  $esbOldSuperOrder ? $esbOldSuperOrder->getId() : null,
                'esb_super_order' =>  $esbSuperOrder->getId(),
            );
            $checkoutSession->setData('save_superorder_without_canceled', $data);

            $result['error'] = false;
            $result['pool_id'] = array($poolId);

            // variable set in: handleCancelledPackages
            $orderHasOtherPackages = Mage::registry('order_has_other_packages');
            $approveOrderTriggered = Mage::registry('approve_order_already_triggered');
            if (! $changedDeliveredPackages && ($esbChangedPackageIds || $esbOrdersWithAddressChanged || $orderHasOtherPackages) && !$approveOrderTriggered) {
                $serviceHelper = $this->getServiceHelper();
                /** @var $approvalClient Omnius_Service_Model_Client_ApprovalClient */
                $approvalClient = $serviceHelper->getClient('approval');
                $approvalClient->approveOrder($esbSuperOrder);
            }
            if (!Mage::helper('agent')->isTelesalesLine()) {
                $orderedItems = $esbSuperOrder->getOrderedItems(true, false, $orderIdsArray, $allModifiedPackages);
                $orderId = array();
                foreach ($allOrders as $order) {
                    $orderId[$order->getId()] = $order->getIncrementId();
                }
                /*$result['creditcheck_page'] = $this->getLayout()
                    ->createBlock('core/template')
                    ->setSuperOrderId($iltSuperOrder->getId())
                    ->setTemplate('checkout/cart/steps/save_credit_check.phtml')
                    ->toHtml();*/

                $result['device_selection'] = $this->getLayout()
                    ->createBlock('vznl_checkout/cart')
                    ->setShowStep(true)
                    ->setOrderIds($orderId)
                    ->setTemplate('checkout/cart/steps/save_number_selection.phtml')
                    ->toHtml();

                if ($this->checkoutHelper->canProcessDeliverySteps($orderedItems)) {
                    $result['contract_page'] = $this->getLayout()
                        ->createBlock('vznl_checkout/cart')
                        ->setShowStep(true)
                        ->setOrderIds($orderIdsArray)
                        ->setTemplate('checkout/cart/steps/save_contract.phtml')
                        ->toHtml();
                }

                if ($this->checkoutHelper->canProcessDeviceSelectionStep($orderedItems)) {
                    Mage::unregister('order');
                    Mage::register('order', $esbSuperOrder);
                    $result['device_selection'] = $this->getLayout()->createBlock('vznl_checkout/cart')
                        ->setShowStep(true)
                        ->setTemplate('checkout/cart/steps/save_device_selection.phtml')
                        ->toHtml();
                }
            } else {
                // RFC-150665 - Unannounced return should show different message
                if (!empty($oldSuperOrder)) {
                    if (!empty($cancelledPackages) && (count($cancelledPackages) == count($allModifiedPackages))) {
                        $result['oldOrderMessage'] = $this->__('The goods return is successfully registered under the following number.');
                    } else {
                        $result['oldOrderMessage'] = $this->__('Your original order has changed. You can find the original order with the next number.');
                        $oldOrderPackages = $oldSuperOrder->getPackages();
                        $isAnnouncedReturn = true;
                        foreach ($oldOrderPackages as $oldOrderPackage) {
                            if ($oldOrderPackage->getReturnDate()) {
                                $isAnnouncedReturn = false;
                                break;
                            }
                        }
                        if ($isAnnouncedReturn) {
                            $result['newOrderMessage'] = $this->__('The order has been successfully registered. You can find the new order by the modified packets with the next number. To place the order the return goods must first be registered.');
                        }
                    }
                }
            }

            $this->getRequest()->setPost('superorderNumber', $esbSuperOrder->getOrderNumber());
            return $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

        } catch (Exception $e) {
            foreach ($revertPackageStatus as $package) {
                $package['model']->setStatus($package['status'])->save();
            }
            // Set fail response for other errors
            $result['error'] = true;
            $result['message'] = $this->__($e->getMessage());
            if ($this->getCoreServiceHelper()->isDev()) {
                $result['trace'] = $e->getTrace();
            }

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        }

    }

    protected function _finishSuperOrderSave($orderIdsArray, $superQuote, $superOrderId, $esbSuperOrder = null, $allModifiedPackages = null, $esbOldSuperOrderId = null)
    {
        // send order confirmation email if not all packages are cancelled
        $allActivePackages = Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('status', array('nin' => Vznl_Package_Model_Package::getCancelledTypePackages()))
            ->addFieldToFilter('order_id', $superOrderId);

        if ((! $esbOldSuperOrderId)
            && count($allActivePackages)
            && (Mage::helper('agent')->isTelesalesLine(Mage::app()->getWebsite($esbSuperOrder->getWebsiteId())->getCode()))
        ) {
            //save order confirmation email in queue, only in edit before delivery
            $payload = Mage::helper('communication/email')->getOrderConfirmationPayload($esbSuperOrder);
            Mage::getSingleton('communication/pool')->add($payload);

            /** @var Vznl_Communication_Helper_Sms $smsHelper */
            $smsHelper = Mage::helper('communication/sms');
            $smsHelper->createOrderConfirmationSms($esbSuperOrder);
        }

        // After processorder
        Mage::getSingleton('core/session')->setOrderIds($orderIdsArray);
        Mage::getSingleton('customer/session')->setOrderEdit(null);
        Mage::getSingleton('customer/session')->setCancelledPackages(false);

        $superQuote->setIsActive(false);
        $superQuote->save();

        $this->checkoutHelper->exitSuperOrderEdit($superOrderId); //exit super order edit mode
        $this->checkoutHelper->createNewQuote(); // create a new quote on the cart

        $result['error'] = false;
        $result['performed'] = true;
        $result['message'] = "Super order successfully saved";
        if (!Mage::helper('agent')->isTelesalesLine() && $esbSuperOrder && $allModifiedPackages) {
            $orderedItems = $esbSuperOrder->getOrderedItems(true, false, $orderIdsArray, $allModifiedPackages);
            if ($this->checkoutHelper->canProcessDeliverySteps($orderedItems)) {
                $result['contract_page'] = $this->getLayout()
                    ->createBlock('vznl_checkout/cart')
                    ->setShowStep(true)
                    ->setTemplate('checkout/cart/steps/save_contract.phtml')
                    ->toHtml();

                $result['device_selection'] = $this->getLayout()
                    ->createBlock('vznl_checkout/cart')
                    ->setShowStep(true)
                    ->setTemplate('checkout/cart/steps/save_number_selection.phtml')
                    ->toHtml();
            }
        }

        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }

    public function saveSuperOrderPollingAction()
    {
        /** @var Vznl_Job_Model_Queue $queue */
        $queue = Mage::helper('vznl_job')->getQueue();

        $checkoutSession = Mage::getSingleton('checkout/session');

        $data = $checkoutSession->getData('save_superorder_without_canceled');
        $poolId = $checkoutSession->getData('process_order_job_id');

        if (! is_array($data)) {
            Mage::logException(new Exception('saveSuperorderPollingAction: No save_superorder_without_cancelled session data available'));
            return $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => true,
                            'message' => $this->__('Job not present'),
                        )
                    )
                );
        }

        /** @var Vznl_Job_Model_Job_Abstract $job */
        $job = $queue->get($poolId);
        if ($poolId && $job && $job->isProcessed()) {

            if (! $job->isSuccessful()) {
                $e = $job->getException();
                Mage::getSingleton('core/logger')->logException($e);
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }

            $orderIdsArray = $data['order_ids_array'];
            $allModifiedPackages = $data['all_modified_packages'];
            $esbOldSuperOrder = Mage::getModel('superorder/superorder')->load($data['esb_old_super_order']);
            $esbSuperOrder = Mage::getModel('superorder/superorder')->load($data['esb_super_order']);

            if (in_array(strtolower($esbSuperOrder->getOrderStatus()), [strtolower(Vznl_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI), strtolower(Vznl_Superorder_Model_Superorder::SO_INITIAL), strtolower(Vznl_Superorder_Model_Superorder::SO_PRE_INITIAL)])) {
                return $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(
                            array(
                                'error' => false,
                                'performed' => false,
                            )
                        )
                    );
            }

            try {
                // Edited packages
                if ($esbOldSuperOrder->getId()) {
                    // Check old superorder for HN items and update status
                    $hasHn = $esbOldSuperOrder->hasHNItems();
                    if ($hasHn) {
                        $esbOldSuperOrder->setIsVfOnly(0);
                    } else {
                        $esbOldSuperOrder->setIsVfOnly(1);
                    }
                    $esbOldSuperOrder->save();
                }

                // Check current/new superorder for HN Items and update status
                $hasHn = $esbSuperOrder->hasHNItems();
                if ($hasHn) {
                    $esbSuperOrder->setIsVfOnly(0);
                } else {
                    $esbSuperOrder->setIsVfOnly(1);
                }
                $esbSuperOrder->save();
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if (Mage::helper('omnius_service')->isDev()) {
                    $result['trace'] = $e->getTrace();
                }

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }

            $checkoutSession->unsetData('super_quote_id');
            $checkoutSession->unsetData('save_superorder_without_canceled');
            $checkoutSession->unsetData('process_order_job_id');
            $checkoutSession->unsetData('order_ids_array');

            $superQuote = Mage::getModel('sales/quote')->load($checkoutSession->getData('super_quote_id'));
            $this->_finishSuperOrderSave($orderIdsArray, $superQuote, $checkoutSession->getData('superorder_id'), $esbSuperOrder, $allModifiedPackages, $data['esb_old_super_order']);
        } elseif ($poolId && ! $job) {
            Mage::logException(new Exception('saveSuperorderPollingAction: No job present for id ' . $poolId));
            return $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => true,
                            'message' => $this->__('Job is not present'),
                        )
                    )
                );
            //no job or still processing
        } elseif (! $poolId) {

            $superOrderId = ($data['esb_old_super_order']) ?:$data['esb_super_order'];

            $checkoutSession->unsetData('super_quote_id');
            $checkoutSession->unsetData('save_superorder_without_canceled');
            $checkoutSession->unsetData('process_order_job_id');
            $checkoutSession->unsetData('order_ids_array');

            $this->checkoutHelper->exitSuperOrderEdit($superOrderId); //exit super order edit mode
            $this->checkoutHelper->createNewQuote(); // create a new quote on the cart

            return $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => false,
                            'performed' => true,
                        )
                    )
                );
        } else {
            return $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => false,
                            'performed' => false,
                        )
                    )
                );
        }
    }


    protected function isJSON($string)
    {
        return is_string($string) && is_object(json_decode($string)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

    private function extractOrderPackages(Mage_Sales_Model_Order $order)
    {
        $packages = array();
        /** @var Mage_Sales_Model_Order_Item $orderItem */
        foreach ($order->getAllItems() as $orderItem) {
            $packages[$orderItem->getPackageId()][] = $orderItem->toArray();
        }
        return $packages;
    }

    private function _convertPrivacy($data)
    {
        $result = array(
            'privacy_sms' => 0,
            'privacy_email' => 0,
            'privacy_mailing' => 0,
            'privacy_telemarketing' => 0,
            'privacy_number_information' => 0,
            'privacy_phone_books' => 0,
            'privacy_disclosure_commercial' => 0,
            'privacy_market_research' => 0,
            'privacy_sales_activities' => 0,
        );

        foreach ($data as $item => $value) {
            $result['privacy_' . $item] = $value;
        }

        return $result;
    }

    private function _saveVodafoneHomeData($data)
    {
        $packages = Mage::getModel('package/package')
            ->getPackages(null, $this->getQuote()->getId());
        foreach ($packages as $package) {
            if (isset($data[$package->getPackageId()]['order_number'])) {
                $package->setVodafoneHome($data[$package->getPackageId()]['order_number'])->save();
            }
        }
    }

    private function _saveFixedToMobileData($data)
    {
        $packages = Mage::getModel('package/package')
            ->getPackages(null, $this->getQuote()->getId());
        foreach ($packages as $package) {
            if (isset($data[$package->getPackageId()]['mobile_number'])) {
                $package->setFixedToMobile($data[$package->getPackageId()]['mobile_number'])->save();
            }
        }
    }

    private function _saveDeliveryAddressData($data)
    {
        $quote = $this->getQuote();
        $quote->setShippingData(Mage::helper('core')->jsonEncode($data));
        $quote->save();
        return true;
    }

    /**
     * Create super order for the quote
     * @return Vznl_Superorder_Model_Superorder
     * @throws Mage_Core_Exception
     * @throws Varien_Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function createOrder():Vznl_Superorder_Model_Superorder
    {
        $checkout = $this->getCheckout();
        $checkout->setCollectRatesFlag(true);
        $quote = $this->getQuote();
        $customer = $quote->getCustomer();
        $isMobile = $quote->hasMobilePackage();

        if ($customer->getId() && !$customer->getIsProspect()) {
            $customer = Mage::getModel('customer/customer')->load($customer->getId());
            if ($this->_getCustomerSession()->getNewEmail()) {
                // When the email was overwritten, also set it on customer when the order is created
                $emails = explode(';', $customer->getAdditionalEmail());
                array_unshift($emails, $this->_getCustomerSession()->getNewEmail());
                $customer->setEmailOverwritten(1)->setCorrespondenceEmail(implode(';', $emails))->save();
                $this->_getCustomerSession()->setNewEmail(null);

                // Also setting the correspondance email
                $quote->setCorrespondenceEmail($emails[0]);
            }
        } else {
            $customer = $this->_addCustomer();

            // If customer exists, load the entity to retrieve the campaigns, ctn records and other data
            $session = $this->_getCustomerSession();
            $session->setCustomer($customer);
            $session->unsBtwState();

            $quote->save();
        }

        Mage::getSingleton('customer/session')->setCustomerInfoSync(true);
        $quote->setIsMultiShipping(true);
        $shipToInfo = $this->_buildDeliveryAddresses($customer->getId(), $quote);
        $checkout->setShippingItemsInformation($shipToInfo);

        // TODO Since we need to hard-code at this moment, we manually set them
        $addresses = $quote->getAllShippingAddresses();
        foreach ($addresses as $address) {
            $address->setShippingMethod('flatrate_flatrate');
        }

        // Check if the quote is a risk, and if it is just skip to next step
        if ($quote->checkRisk()) {
            $packages = Mage::getModel('package/package')
                ->getPackages(null, $this->getQuote()->getId());
            foreach ($packages as $package) {
                // Get SOC codes only if package contains subscription
                if ($package->hasSubscription($quote)) {
                    $socCodes = Mage::helper('multimapper')->validateSocCodesByPackageFromQuote($quote, $package->getPackageId());
                    $package->setSocCodes(serialize($socCodes));
                    $package->save();
                }
            }

            // Mark quote as Offer and as Risk
            $quote
                ->setCartStatus(Vznl_Checkout_Model_Sales_Quote::CART_STATUS_RISK)
                ->setIsOffer(1)
                ->setChannel(Mage::app()->getWebsite()->getCode())
                ->save();

            //Unset the saved shopping cart.
            Mage::helper('vznl_checkout')->unsetSavedCart($quote->getQuoteParentId());

            // API call to create prospect customer if not already existing Mobile / Fixed.
            $customer = Mage::getModel('customer/customer')->load($customer->getId());
            $this->createApiCustomer($customer, '', $quote, 'SaveExistingFields');
            throw new LogicException('Agent does not have access to submit this order');
        }

        $this->_setDefaultPayment($quote);
        /*********************************************************/
        $checkout->createOrders();

        // From this point on cache all models for faster processing, since they won't be changed
        if (!Mage::registry('freeze_models')) {
            Mage::unregister('freeze_models');
            Mage::register('freeze_models', true);
        }

        $orderPackages = array();
        $packagesTypes = $this->_checkIfDeliveryInShop($quote);

        // TODO Refactor this when we have the call from ESB to set the telesale order to be saved with a certain status
        $orderIds = Mage::getSingleton('core/session')->getOrderIds();

        foreach ($orderIds as $orderIncrementId) {
            $order = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderIncrementId);

            //create new package entities to set their status
            foreach ($order->getAllItems() as $item) {
                $orderPackages[] = $item->getPackageId();
            }

            // Set the order delivery type
            if (isset($item)) {
                $order->getShippingAddress()->setDeliveryType($packagesTypes[$item->getPackageId()]['type']);
                if ($packagesTypes[$item->getPackageId()]['type'] == 'store') {
                    $order->getShippingAddress()->setDeliveryStoreId($packagesTypes[$item->getPackageId()]['store_id']);
                }
                $order->getShippingAddress()->save();
            }

            if (Mage::helper('agent')->isTelesalesLine()) {
                foreach ($order->getAllItems() as $item) {
                    if ($packagesTypes[$item->getPackageId()]['type'] == 'store') {
                        $order->setEsbOrderStatus(Vznl_Checkout_Model_Sales_Order::ESB_VALIDATION);
                        $order->save();
                        break;
                    }
                }
            }
        }

        // Create a new superOrder
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->createNewSuperorder();
        $quotePackages = Mage::getModel('package/package')->getPackages(null, $quote->getId());

        if (!$superOrder->getCustomerId()){
            $superOrder->setCustomerId($customer->getId());
            $superOrder->save();
        }

        // If the superorder was successfully created, move the packages to the order
        $isOneOff = false;
        /** @var Vznl_Package_Model_Package $quotePackage */
        foreach ($quotePackages as $quotePackage) {
            $quotePackage->setOrderId($superOrder->getId())
                ->setQuoteId(null)
                ->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_INITIAL)
                ->setCreditcheckStatus(Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL)
                ->setCreditcheckStatusUpdated(now());

            if ($quotePackage->getOneOfDeal()) {
                $isOneOff = true;
            }

            if ($quotePackage->isNumberPorting()) {
                $quotePackage->setPortingStatus(Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL)
                    ->setPortingStatusUpdated(now());
            }

            $quotePackage->save();
        }
        unset($quotePackages);

        // Assign all the orders to the super order and custom increment id
        foreach ($orderIds as $orderIncrementId) {
            /** @var Vznl_Checkout_Model_Sales_Order $order */
            $order = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderIncrementId);
            $order->setSuperorderQuery($superOrder->getId())
                ->setEditedOrder(0)
                ->oneOff($isOneOff)
                ->processPackageChecksum();
        }

        // We save bucket details for worklists only in case of mobile
        if ($isMobile) {
            /** @var Vznl_Customer_Helper_Data $customerHelper */
            $customerHelper = Mage::helper('vznl_customer');
            $customerHelper->setOoImeiInOrder($superOrder);
        }

        /** @var Vznl_Customer_Helper_Search $searchHelper */
        $searchHelper = Mage::helper('vznl_customer/search');
        $searchHelper->updateOrderNumberApiCall($superOrder, $superOrder->getCustomer());

        return $superOrder;
    }

    private function _validateCustomerAdditionaldata($data)
    {
        $errors = array();

        // Validate additional email data
        $emails = array_filter($data['email'] ?: [], 'strlen');
        foreach ($emails as $id => $email) {
            if (!Mage::helper('dyna_validators')->validateEmailSyntax($email)
                || !Mage::helper('dyna_validators')->validateEmailDns($email)) {
                $errors += array('additional[email][' . $id . ']' => $this->__('Invalid e-mail address'));
            }
        }
        //TODO Validate additional fax
        //TODO Validate additional phone

        return $errors;
    }

    private function _validateCustomerData($data)
    {
        $errors = array();

        $inputFieldsPrivate = $this->_getCustomerDataFields();
        $inputFieldsBusiness = $this->_getBusinessDataFields($data);
        if ($data['is_business']) {
            $validations = $inputFieldsBusiness;
            switch ($data['company_address']) {
                case 'other_address':
                    $validationsBusiness = array(
                        'company_street' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'company_house_nr' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'company_house_nr_addition' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'company_postcode' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNLPostcode'
                            )
                        ),
                        'company_city' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                    );
                    $errors += $this->checkoutHelper->_performValidate($data['otherAddress'], $validationsBusiness, 'customer[otherAddress][', ']');
                    break;
                case 'foreign_address':
                    $validationsBusiness = array(
                        'company_street' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'company_house_nr' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'company_house_nr_addition' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'extra' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'company_city' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                        'company_region' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'company_postcode' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                        'company_country_id' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                    );
                    $errors += $this->checkoutHelper->_performValidate($data['foreignAddress'], $validationsBusiness, 'customer[foreignAddress][', ']');
                    break;
                default:
                    throw new Exception($this->__('Missing company address data'));
            }
        } else {
            $validations = $inputFieldsPrivate;
        }

        if ($this->hasSubscription) {
            $errors += $this->validateIdNumberByType($data);
        }

        return $errors + $this->checkoutHelper->_performValidate($data, $validations, 'customer[', ']');
    }

    private function _validateReuseCustomerData($data)
    {
        if ($data['is_business']) {
            $validations = [
                'contractant_reuse_known_data' => [
                    'required' => true,
                    'validation' => [
                        'validateIsNotEmpty'
                    ]
                ],
                'contractant_dob' => array(
                    'required' => ($this->isMobile && !$this->isBoth) ? true : false,
                    'validation' => array(
                        'validateMaxAge',
                        'validateMinAge',
                        'validateIsNLDate'
                    )
                ),
                'contractant_id_type' => [
                    'required' => $this->hasSubscription ? true : false,
                    'validation' => [
                        'validateName',
                    ]
                ],
                'contractant_id_number' => [
                    'required' => $this->hasSubscription ? true : false,
                    'validation' => []
                ],
                'contractant_issue_date' => [
                    'required' => ($this->hasSubscription && !($this->isMobile && !$this->isBoth)) ? true : false,
                    'validation' => [
                        'validateIsNLDate'
                    ]
                ],
                'contractant_valid_until' => [
                    'required' => $this->hasSubscription ? true : false,
                    'validation' => [
                        'validateFutureDate',
                        'validateIsNLDate'
                    ]
                ],
                'contractant_issuing_country' => [
                    'required' => $this->hasSubscription ? true : false,
                    'validation' => [
                        'validateCountryCode',
                    ]
                ],
            ];
        } else {
            $validations = [
                'reuse_known_data' => [
                    'required' => true,
                    'validation' => [
                        'validateIsNotEmpty'
                    ]
                ],
                'dob' => [
                    'required' => ($this->isMobile && !$this->isBoth) ? true : false,
                    'validation' =>[
                        'validateMaxAge',
                        'validateMinAge',
                        'validateIsNLDate'
                    ]
                ],
                'id_type' => [
                    'required' => $this->hasSubscription ? true : false,
                    'validation' => []
                ],
                'id_number' => [
                    'required' => $this->hasSubscription ? true : false,
                    'validation' => []
                ],
                'valid_until' => [
                    'required' => $this->hasSubscription ? true : false,
                    'validation' => [
                        'validateFutureDate',
                        'validateIsNLDate'
                    ]
                ],
                'issue_date' => [
                    'required' => ($this->hasSubscription && !($this->isMobile && !$this->isBoth)) ? true : false,
                    'validation' => [
                        'validateIsNLDate',
                    ]
                ],
                'issuing_country' => [
                    'required' => $this->hasSubscription ? true : false,
                    'validation' => [
                        'validateCountryCode',
                    ]
                ],
            ];
        }

        return $this->checkoutHelper->_performValidate($data, $validations, 'customer[', ']');
    }

    private function _validateVodafoneHomeData($data)
    {
        //TODO validate the input fields
        return true;
    }

    private function _validateFixedToMobileData($data)
    {
        //TODO validate the input fields
        return true;
    }

    private function _validateSplitDeliveryAddressData($data, $packageId)
    {
        $errors = array();
        switch ($data['address']) {
            case 'store':
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNLPostcode'
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                if ($this->checkoutHelper->_performValidate($data, $addressValidations, 'delivery[pakket][' . $packageId . ']store[', ']')) {
                    $errors += array('delivery[pakket][' . $packageId . '][address]' => $this->__('Invalid store chosen'));
                }
                break;
            case 'billing_address':
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNLPostcode'
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->checkoutHelper->_performValidate($data, $addressValidations, 'delivery[pakket][' . $packageId . '][billingaddress][', ']');
                break;
            case 'other_address':
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNLPostcode'
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->checkoutHelper->_performValidate($data, $addressValidations, 'delivery[pakket][' . $packageId . '][otherAddress][', ']');
                break;
            case 'foreign_address':
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'extra' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'region' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'country_id' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->checkoutHelper->_performValidate($data, $addressValidations, 'delivery[pakket][' . $packageId . '][foreignAddress][', ']');
                break;
            default:
                throw new Exception($this->__('Missing delivery address data'));
        }

        return $errors;
    }

    private function _validateBillingAddressData($data)
    {
        $errors = array();
        $validations = $this->hasSubscription ? array(
            //commenting this section as these fiels are moved from customer section.
            //need to remove this block on confirmation.
            /*'account_no' => array(
                'required' => $required,
                'validation' => array(
                    'validateBicIban'
                )
            ),
            'account_holder' => array(
                'required' => $required,
                'validation' => array(
                    'validateName',
                )
            ),*/
            'fax' => array(
                'required' => false,
                'validation' => array(
                    'validateName',
                )
            ),
        )
            : array();

        switch ($data['address']) {
            case 'other_address':
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'houseno' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNLPostcode'
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->checkoutHelper->_performValidate($data['otherAddress'], $addressValidations, 'address[otherAddress][', ']');
                break;
            case 'foreign_address':
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'houseno' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'extra' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'region' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'country_id' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->checkoutHelper->_performValidate($data['foreignAddress'], $addressValidations, 'address[foreignAddress][', ']');
                break;
            default:
                throw new Exception($this->__('Missing company address data'));
        }

        $errors += $this->checkoutHelper->_performValidate($data, $validations, 'address[', ']');

        return $errors;
    }

    private function _buildDeliveryAddresses(
        $customerId,
        $quote = null
    )
    {
        if (!$quote) {
            $quote = $this->getQuote();
        }
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());

        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['deliver']['address'];
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['pakket'][$packageId]['address'];
            }
        }

        $addresses = array();
        foreach ($addressArray as $packageId => $address) {
            if (is_numeric($address)) {
                $address = Mage::getModel('customer/address')->load($address);
                if (!$address) {
                    throw new Exception($this->__('Invalid delivery address id'));
                }
                $addresses[$packageId] = $address->getId();
            } elseif (is_array($address)) {
                $addresses[$packageId] = $this->_addCustomerShippingAddress($customerId, $address, $quote);
            }
        }

        $result = array();
        $items = $quote->getAllItems();

        /** @var Vznl_Checkout_Model_Sales_Order_Item $item */
        foreach ($items as $item) {
            $address = isset($addresses[$item->getPackageId()]) ? $addresses[$item->getPackageId()] : [];
            $result[] = array(
                $item->getId() => array(
                    'address' => $address,
                    'qty' => $item->getQty()
                )
            );
        }

        return $result;
    }

    private function _checkIfDeliveryInShop($quote = null)
    {
        if (!$quote) {
            $quote = $this->getQuote();
        }
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());

        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId]['type'] = $data['deliver']['address']['address'];
                if ($addressArray[$packageId]['type'] == 'store') {
                    $addressArray[$packageId]['store_id'] = $data['deliver']['address']['store_id'];
                }
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId]['type'] = $data['pakket'][$packageId]['address']['address'];
                if ($addressArray[$packageId]['type'] == 'store') {
                    $addressArray[$packageId]['store_id'] = $data['pakket'][$packageId]['address']['store_id'];
                }
            }
        }

        return $addressArray;
    }

    private function _addCustomerShippingAddress($customerId, $addressData, $quote = null)
    {
        if (!$quote) {
             $quote = $this->getQuote();
           }
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $shipAddress = $quote->getShippingAddress();
        $addressData['firstname'] = (trim($shipAddress->getFirstname()) != '') ? $shipAddress->getFirstname() : $customer->getFirstname();
        $addressData['company'] = $shipAddress->getCompany();
        // Delivery can only be in Netherlands!
        $addressData['country_id'] = Vznl_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;
        $addressData['fax'] = $shipAddress->getFax();
        $addressData['lastname'] = (trim($shipAddress->getLastname()) != '') ? $shipAddress->getLastname() : $customer->getLastname();
        $addressData['middlename'] = (trim($shipAddress->getMiddlename()) != '') ? $shipAddress->getMiddlename() : $customer->getMiddlename();
        $addressData['suffix'] = (trim($shipAddress->getSuffix()) != '') ? $shipAddress->getSuffix() : $customer->getSuffix();
        $addressData['prefix'] = (trim($shipAddress->getPrefix()) != '') ? $shipAddress->getPrefix() : $customer->getPrefix();
        $addressData['telephone'] = $shipAddress->getTelephone();
        // Check if the address was already added, in that case just return the id.
        $addressId = false;

        foreach ($customer->getAddresses() as $tempAddress) {
            if ($addressData['street'] == array(
                    $tempAddress->getStreet(1),
                    $tempAddress->getHouseNo(),
                    $tempAddress->getHouseAdd(),
                ) &&
                $addressData['postcode'] == $tempAddress->getPostcode() &&
                $addressData['city'] == $tempAddress->getCity()
            ) {
                $addressId = $tempAddress->getId();
                break;
            }
        }
        if (!$addressId) {
            // address does not exist in the db , create it
            $address = Mage::getModel('customer/address');

            $addressForm = Mage::getModel('customer/form');
            $addressForm->setFormCode('customer_address_edit')
                ->setEntity($address);

            $addressErrors = $addressForm->validateData($addressData);
            if (true === $addressErrors) {
                $addressForm->compactData($addressData);
                $address->setCustomerId($customer->getId())
                    ->setIsDefaultBilling(false)
                    ->setIsDefaultShipping(false)
                    ->save();

                return $address->getId();
            } else {
                throw new Exception(Mage::helper('core')->jsonEncode($addressErrors));
            }

        } else {
            // just return the id
            return $addressId;
        }
    }

    private function _addCustomer()
    {
        $quote = $this->getQuote();
        $billing = $quote->getBillingAddress();
        $shipping = $quote->isVirtual() ? null : $quote->getShippingAddress();

        //$customer = Mage::getModel('customer/customer');
        /* @var $customer Mage_Customer_Model_Customer */
        $customer = $quote->getCustomer();
        $customerBilling = $billing->exportCustomerAddress();
        $customer->addAddress($customerBilling);
        $billing->setCustomerAddress($customerBilling);
        $customerBilling->setIsDefaultBilling(true);
        if ($shipping && !$shipping->getSameAsBilling()) {
            $customerShipping = $shipping->exportCustomerAddress();
            $customer->addAddress($customerShipping);
            $shipping->setCustomerAddress($customerShipping);
            $customerShipping->setIsDefaultShipping(true);
        } else {
            $customerBilling->setIsDefaultShipping(true);
        }

        Mage::helper('core')->copyFieldset('checkout_onepage_quote', 'to_customer', $quote, $customer);
        $customer->setPassword(md5(mt_rand()));
        $customer->setPasswordHash($customer->hashPassword($customer->getPassword()));
        $customer->setIsProspect(false);
        $quote->setCustomer($customer)
            ->setCustomerId($customer->getId());
        // RFC 160064 - When a certain email address is used, generate an unique dummy one for safety
        $allEmails = explode(";", $customer->getData('additional_email'));

        if (false === empty($quote->getDummyEmail())) {
            $allEmails[0] = Mage::helper('vznl_customer')->generateDummyEmail();
            $customer->setData('email', implode(";", $allEmails));
            $quote->setData('email', implode(";", $allEmails));
            $customer->setData('additional_email', implode(";", $allEmails));
            $quote->setData('additional_email', implode(";", $allEmails));
        }

        $quote->setData('ziggo_telephone', $customer->getData('ziggo_telephone'));
        $quote->setData('ziggo_email', $customer->getData('ziggo_email'));

        $customer->save();
        // API call to create a prospect customer - elastic search.
        $this->createApiCustomer($customer, $customerBilling, $quote, 'SaveCheckoutFields');

        return $customer;
    }

    private function _getBusinessAddressByType($address)
    {
        switch ($address['company_address']) {
            case 'foreign_address':
                if (!isset($address['foreignAddress'])) {
                    throw new Exception($this->__('Incomplete data'));
                }
                $temp = $address['foreignAddress'];
                $result = $temp;

                if (isset($temp['extra'],$temp['extra'][0]) && !empty($temp['extra'][0])) {
                    $result['company_extra_lines'] = join(';', array_filter($temp['extra'], 'strlen'));
                }

                break;
            case 'other_address':
                if (!isset($address['otherAddress'])) {
                    throw new Exception($this->__('Incomplete data'));
                }

                $result = $address['otherAddress'];

                // Default country is NL
                $result['company_country_id'] = Vznl_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;
                break;
            default:
                throw new Exception($this->__('Incomplete data'));
        }
        return $result;
    }

    private function _setDefaultPayment($quote = null)
    {
        if (!$quote) {
            $quote = $this->getQuote();
        }
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());
        $isMobile = $quote->hasMobilePackage();
        if ($isMobile) {
            $paymentMethodData = $quote->getMobileOneoffPayment();
        } else {
            $paymentMethodData = $quote->getFixedPaymentMethodOneTimeCharge();
        }
        $paymentData = (isset($data['payment']) && $data['payment']) ? $data['payment'] : $paymentMethodData;
        if (!$paymentData) {
            throw new Exception($this->__('No shipping method was set.'));
        }

        if (is_array($paymentData)) {
            $payment['method'] = reset($paymentData);
        } else {
            $payment['method'] = $paymentData;
        }
        $checkout = $this->getCheckout();
        $payment['checks'] = Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_MULTISHIPPING
            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
            | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX;
        $checkout->setPaymentMethod($payment);
    }

    private function _validateSplitPaymentAndDelivery($data)
    {
        $errors = array();

        if (isset($data['pakket']) && is_array($data['payment'])) {
            foreach ($data['pakket'] as $packetId => $packetData) {
                if (empty($data['payment'][$packetId])) {
                    $errors += array('payment[pakket][' . $packetId . '][type]' => $this->__('Please select a payment method'));
                    continue;
                }
                if (Mage::getSingleton('customer/session')->getOrderEdit()) {
                    $quote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
                    foreach ($quote->getAllItems() as $item) {
                        if ($item->getPackageId() == $packetId) {
                            $packetData['order_id'] = $item->getEditOrderId();
                            break;
                        }
                        continue;
                    }
                    $temp[$packetId] = Mage::helper('core')->jsonEncode($packetData);
                } else {
                    $temp[$packetId] = Mage::helper('core')->jsonEncode($packetData);
                }

                foreach ($temp as $id => $newtemp) {
                    if ($id !== $packetId && $newtemp == $temp[$packetId]) {
                        if ($data['payment'][$id] !== $data['payment'][$packetId]) {
                            $errors += array('payment[pakket][' . $packetId . '][type]' => $this->__('Payment methods should be the same for same address'));
                        }
                    }
                }
            }
        }

        return $errors;
    }

    private function _validateFixedNumberSelectionData($data)
    {
        $result = [];
        if (empty($data) || !is_array($data)) {
            return $result;
        }

        if (isset($data['fixed_telephone_number']) && isset($data['fixed_extra_telephone_no'])) {
            if ($data['fixed_telephone_number'] != "") {
                if ($data['fixed_telephone_number'] == $data['fixed_extra_telephone_no']) {
                    $result += array('fixed_extra_telephone_no' => $this->__('Number is already selected'));
                }
            }
        }

        return $result;
    }

    private function _validateNumberSelectionData($data)
    {
        $result = [];
        if (empty($data) || !is_array($data)) {
            return $result;
        }

        $usedNumbers = array();
        foreach ($data as $id => $number) {
            if ($id && !empty($number['mobile_number'])) {
                // Validate number is unique
                if (in_array($number['mobile_number'], $usedNumbers)) {
                    $result += array('numberselection[' . $id . '][mobile_number]' => $this->__('Number is already selected'));
                } else {
                    array_push($usedNumbers, $number['mobile_number']);
                }
                // Validate sim is correct format
                if (!empty($number['new_sim_number']) && !Mage::helper('dyna_validators/data')->validateSim($number['new_sim_number'])) {
                    $result += array('numberselection[' . $id . '][new_sim_number]' => $this->__('Invalid simcard number'));
                }
            }
        }
        unset($usedNumbers);

        return $result;
    }

    private function _validateNewNetherlands($data)
    {
        $result = array();
        foreach ($data as $id => $number) {
            if (!isset($number['order_number'])) {
                continue;
            }
            $number['order_number'] = trim($number['order_number']);
            if ($id && (!isset($number['order_number']) || empty($number['order_number']))) {
                $result += array('new_netherlands[' . $id . '][order_number]' => $this->__('This is a required field.'));
            }
        }

        return $result;
    }

    private function _buildCreditCheckAddresses($quote)
    {

        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());

        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $package_type = $quote->getPackageType($packageId);
                $addressArray[$packageId] = $data['deliver']['address'];
                $addressArray[$packageId]['package_type']= $package_type;
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $package_type = $quote->getPackageType($packageId);
                $addressArray[$packageId] = $data['pakket'][$packageId]['address'];
                $addressArray[$packageId]['package_type']= $package_type;
            }
        }

        return $addressArray;
    }

    /**
     * @param $orders
     * @param $data
     */
    private function _updateNumberSelection($orders, $data)
    {
        foreach ($orders as $order) {
            $packages = $order->getPackages();
            foreach ($packages as $packageModel) {
                $packageId = $packageModel->getPackageId();
                if (isset($data[$packageId])) {
                    if (isset($data[$packageId]['mobile_number'])) {
                        $defaultedMsisdn = $packageModel->getTelNumber();
                        $customerId = $order->getCustomerId();
                        $packageModel->setTelNumber($data[$packageId]['mobile_number']);
                        if (!Mage::helper('omnius_service')->isOverride()) {
                            // Only update the ctn when not in override mode
                            if ($defaultedMsisdn && $customerId) {
                                $currentCtn = Mage::getModel('ctn/ctn')->loadCtn($defaultedMsisdn, $customerId);
                                if($currentCtn->getCode()) {
                                    try {
                                        $currentCtn
                                            ->setCtn($data[$packageId]['mobile_number'])
                                            ->save();
                                    } catch (Exception $e) {
                                        Mage::log($e->getMessage());
                                    }
                                }
                            }
                        }
                        // Call vNext search API to add customer CTN
                        $searchHelper = Mage::helper('vznl_customer/search');
                        $searchHelper->addCustomerCtnApiCall(
                            $order->getCustomer(),
                            $data[$packageId]['mobile_number'],
                            Vznl_Customer_Helper_Search::API_CUSTOMER_CTN_TYPE_MOBILE
                        );
                    }
                    if (isset($data[$packageId]['new_sim_number'])) {
                        $packageModel->setSimNumber($data[$packageId]['new_sim_number']);
                    }
                    if (isset($data[$packageId]['imei_number'])) {
                        $packageModel->setImei($data[$packageId]['imei_number']);
                    }
                    $packageModel->save();

                    unset($data[$packageId]);
                }
            }
        }
    }

    private function _updateDeviceSelection($orders, $data)
    {
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');
        foreach ($orders as $order) {
            $packages = $order->getPackages();
            foreach ($packages as $packageModel) {
                $packageId = $packageModel->getPackageId();
                if (isset($data[$packageId])) {
                    if (Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE && isset($data[$packageId]['device_name'])) {
                        $prices = $packageHelper->processPrices($data[$packageId], $packageId, $order);
                        if ($prices) {
                            $packageModel
                                ->setDeviceName($prices['device_name'])
                                ->setImei($prices['imei_number'])
                                ->setMixmatch($prices['mixmatch'])
                                ->setBaseMixmatch($prices['base_mixmatch'])
                                ->setCatalogPrice($prices['catalog_price'])
                                ->setCatalogPriceInclTax($prices['catalog_price_incl_tax'])
                                ->setRecurringCumulated($prices['recurring_cumulated'])
                                ->save();

                            unset($data[$packageId]);
                        }
                    }
                }
            }
        }
    }

    private function _getCustomerDataFields()
    {
        return array(
            'firstname' => [
                'required' => true,
                'validation' => [
                    'validateName'
                ]
            ],
            'middlename' => [
                'required' => false,
                'validation' => [
                    'validateName'
                ]
            ],
            'lastname' => [
                'required' => true,
                'validation' => [
                    'validateName'
                ]
            ],
            'gender' => [
                'required' => true,
                'validation' => [
                    'validateIsNumber'
                ]
            ],
            'dob' => [
                'required' => true,
                'validation' =>[
                    'validateMaxAge',
                    'validateMinAge',
                    'validateIsNLDate'
                ]
            ],
            'id_type' => [
                'required' => $this->hasSubscription ? true : false,
                'validation' => []
            ],
            'id_number' => [
                'required' => $this->hasSubscription ? true : false,
                'validation' => []
            ],
            'valid_until' => [
                'required' => $this->hasSubscription ? true : false,
                'validation' => [
                    'validateFutureDate',
                    'validateIsNLDate'
                ]
            ],
            'issue_date' => [
                'required' => ($this->hasSubscription && !($this->isMobile && !$this->isBoth)) ? true : false,
                'validation' => [
                    'validateIsNLDate',
                ]
            ],
            'issuing_country' => [
                'required' => $this->hasSubscription ? true : false,
                'validation' => [
                    'validateCountryCode',
                ]
            ],
        );
    }

    private function _getBusinessDataFields($data = [])
    {
        return array(
            'company_vat_id' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'company_coc' => array(
                'required' => true,
                'validation' => $this->checkoutHelper->getKvKValidation($data)
            ),
            'company_name' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'company_date' => array(
                'required' => true,
                'validation' => array(
                    'validatePastDate',
                    'validateIsNLDate'
                )
            ),
            'company_legal_form' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_gender' => array(
                'required' => true,
                'validation' => array(
                    'validateIsNumber'
                )
            ),
            'contractant_firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_middlename' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_dob' => array(
                'required' => true,
                'validation' => array(
                    'validateMaxAge',
                    'validateMinAge',
                    'validateIsNLDate'
                )
            ),
            'contractant_id_type' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateName',
                )
            ),
            'contractant_id_number' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array()
            ),
            'contractant_issue_date' => array(
                'required' => ($this->hasSubscription && !($this->isMobile && !$this->isBoth)) ? true : false,
                'validation' => array(
                    'validateIsNLDate'
                )
            ),
            'contractant_valid_until' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateFutureDate',
                    'validateIsNLDate'
                )
            ),
            'contractant_issuing_country' => array(
                'required' => $this->hasSubscription ? true : false,
                'validation' => array(
                    'validateCountryCode',
                )
            ),
        );
    }

    private function _buildAndValidateCustomerData($request, $quote)
    {
        $showError = true;
        $customerData =
            $request->getPost('customer', array()) +
            $this->_convertPrivacy($request->getPost('privacy', array()));
        $addressData = $request->getPost('address', array());
        $customer = $this->_getCustomerSession()->getCustomer();

        //set prefix based on gender
        if (isset($customerData['gender'])) {
            $customerData['prefix'] = $this->checkoutHelper->getGenderPrefix($customerData['gender']);
        }

        $this->hasSubscription = $request->getPost('hasSubscription');
        $this->isMobile = $quote->hasMobilePackage();
        $this->isFixed = $quote->hasFixed();
        $this->isBoth = $this->isMobile && $this->isFixed;

        // Convert from frontend data to magento format
        $customerData['is_business'] = (int) isset($customerData['type']) && $customerData['type'] == Omnius_Customer_Model_Customer_Customer::TYPE_BUSINESS;

        if (isset($customerData['reuse_known_data']) || isset($customerData['contractant_reuse_known_data'])) {
            $errorsRuse = $this->checkoutValidationHelper->validateReuseCustomerData(
                $customerData,
                $this->hasSubscription,
                $this->isMobile,
                $this->isBoth
            );
            if ($errorsRuse) {
                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errorsRuse));
            }
            $showError = false;
        }

        // Check if the credit failed button was clicked based on type of customer
        if ($customerData['is_business']) {
            if (isset($customerData['business_suspect_fraud'])) {
                $customerData['suspect_fraud'] = $customerData['business_suspect_fraud'];
                unset($customerData['business_suspect_fraud']);
            }

            if (isset($customerData['contractant_gender'])) {
                $prefix = $this->checkoutHelper->getGenderPrefix($customerData['contractant_gender']);
            } else {
                $prefix = '';
            }

            $customerData['contractant_prefix'] = $prefix;
        }

        $checkoutCart = Mage::getSingleton('checkout/cart');
        if (false === empty($customerData['suspect_fraud'])) {
            $checkoutCart->getQuote()->updateFields(array('suspect_fraud' => 1));
        } else {
            $checkoutCart->getQuote()->updateFields(array('suspect_fraud' => 0));
        }

        // Default country code is netherlands, unless a foreign address is chosen
        $customerData['country_id'] = Vznl_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;
        if ($customerData['is_business']) {
            // Clear the private fields if business
            foreach ($this->checkoutValidationHelper->getCustomerDataFields(
                $this->hasSubscription,
                $this->isMobile,
                $this->isBoth
            ) as $field => $data) {
                $customerData[$field] = null;
            }
            $customerData['company'] = $customerData['company_name'];
            $customerData['vat_id'] = $customerData['company_vat_id'];
            $customerData['firstname'] = $customerData['contractant_firstname'];
            $customerData['lastname'] = $customerData['contractant_lastname'];
            $customerData['middlename'] = $customerData['contractant_middlename'];
            $customerData['gender'] = isset($customerData['contractant_gender']) ? $customerData['contractant_gender'] : null;
            $customerData['dob'] = $customerData['contractant_dob'];
            $addressData['address'] = $customerData['company_address'];
            if ($customerData['otherAddress']) {
                $addressData['otherAddress'] = [
                    'postcode' => $customerData['otherAddress']['company_postcode'],
                    'houseno'  => $customerData['otherAddress']['company_house_nr'],
                    'addition' => $customerData['otherAddress']['company_house_nr_addition'],
                    'country_id'=> $addressData['otherAddress']['country_id'],
                    'street'   => $customerData['otherAddress']['company_street'],
                    'city'     => $customerData['otherAddress']['company_city'],
                ];
            }
        } else {
            // Clear the business fields if private
            foreach ($this->checkoutValidationHelper->getBusinessDataFields(
                [],
                $this->hasSubscription,
                $this->isMobile,
                $this->isBoth
            ) as $field => $data) {
                $customerData[$field] = null;
            }

            $customerData['company_address'] = null;
            $customerData['foreignAddress'] = null;
            $customerData['otherAddress'] = null;
        }

        if ($addressData['address'] == "foreign_address") {
            $addressData['foreignAddress'] = [
                'postcode' => $addressData['otherAddress']['postcode'],
                'houseno'  => $addressData['otherAddress']['houseno'],
                'addition' => $addressData['otherAddress']['addition'],
                'country_id'=> $addressData['otherAddress']['country_id'],
                'street'   => $addressData['otherAddress']['street'],
                'city'     => $addressData['otherAddress']['city'],
            ];
        }
        $additionalData = $request->getPost('additional');
        $customerData['additional_email'] = $additionalData['email'][0];
        $customerData['additional_fax'] = isset($additionalData['fax']) ? join(';', array_filter($additionalData['fax'], 'strlen')) : '';
        $customerData['additional_telephone'] = isset($additionalData['telephone']) ? join(';', array_filter($additionalData['telephone'], 'strlen')) : '';
        if(!empty($additionalData['ziggo_telephone'])) {
            $customerData['ziggo_telephone'] = $additionalData['ziggo_telephone'];
        }
        if(!empty($additionalData['ziggo_email'])) {
            $customerData['ziggo_email'] = $additionalData['ziggo_email'];
        }

        if (false === empty($customerData['dummy_email'])) {
            $checkoutCart->getQuote()->updateFields(array('dummy_email' => 1));
            $allEmails[0] = Mage::helper('vznl_customer')->generateDummyEmail();
            $customerData['email'] = implode(";", $allEmails);
            $customerData['additional_email'] = implode(";", $allEmails);
            $customerData['ziggo_email'] = Vznl_Customer_Model_Customer::DUMMY_EMAIL_ADDRESS;
        } else {
            $checkoutCart->getQuote()->updateFields(array('dummy_email' => 0));
            $customerData['email'] = $customerData['additional_email'];
        }
        $customerData['use_for_shipping'] = 0;


        $errors = $this->checkoutValidationHelper->validateCustomerAdditionalData($additionalData);
        if ($customer->getIsProspect() || !$customer->getId()) {
            $errors += $this->checkoutValidationHelper->validateCustomerData(
                $customerData,
                $this->hasSubscription,
                $this->isMobile,
                $this->isBoth
            );
        }
        $errors += $this->checkoutValidationHelper->validateBillingAddressData(
            $addressData + $customerData,
            $this->hasSubscription,
            $this->isMobile
        );
        if ($errors && $showError) {
            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
        }
        if ($customerData['is_business']) {
            $customerData += $this->_getBusinessAddressByType($customerData);
        }
        $addressData += $this->checkoutHelper->_getAddressByType($addressData);

        $quoteShippingAddress = $quote->getShippingAddress()->getData();
        $quoteBillingAddress = $quote->getBillingAddress()->getData();

        // We dont want the quote address to be overwritten in case the step was submitted after shipping or payment addresses are already set
        if (empty($quoteShippingAddress['postcode']) && empty($quoteBillingAddress['postcode'])) {
            // By default set the shipping address same as billing
            $customerData['use_for_shipping'] = 1;
            $errors = $this->checkoutValidationHelper->validateCustomerAdditionalData($additionalData);
            $errors += $this->checkoutValidationHelper->validateCustomerData(
                $customerData,
                $this->hasSubscription,
                $this->isMobile,
                $this->isBoth
            );
            $errors += $this->checkoutValidationHelper->validateBillingAddressData(
                $addressData + $customerData,
                $this->hasSubscription,
                $this->isMobile
            );
            if ($errors && $showError) {
                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
            }
            if ($customerData['is_business']) {
                $customerData += $this->_getBusinessAddressByType($customerData);
            }
            $addressData += $this->checkoutHelper->_getAddressByType($addressData);

            $addressData['street'] = is_array($addressData['street']) ? $addressData['street'] : array($addressData['street']);

            if (isset($addressData['houseno'])) {
                array_push($addressData['street'], $addressData['houseno']);
            }
            if (isset($addressData['addition'])) {
                array_push($addressData['street'], $addressData['addition']);
            }

            if (isset($addressData['extra_street'])) {
                foreach ($addressData['extra_street'] as $streetLine) {
                    array_push($addressData['street'], $streetLine);
                }
            }
        }
        return $addressData + $customerData;
    }

    /**
     * @throws Mage_Customer_Exception
     *
     * Validate porting contract end date
     */
    public function validateDateAction()
    {
        $result = array();
        try {
            $date = $this->getRequest()->getParam('date');
            $type = $this->getRequest()->getParam('type');
            $customerType = $this->getRequest()->getParam('customerType');
            $currentOperator = $this->getRequest()->getParam('current_operator');
            $currentProvider = $this->getRequest()->getParam('current_provider');
            $packageId = $this->getRequest()->getParam('packageId');
            $errors = $this->_validatePortabilityData(array(), $date, $type, $packageId, $currentOperator, $currentProvider, $customerType);

            //only return the date error
            foreach ($errors as $key => $error) {
                if (strpos($key, 'end_date_contract') === false) {
                    unset($errors[$key]);
                } else {
                    $result['field'] = $this->getRequest()->getParam('field');
                    $result['field_error'] = $error;
                }
            }

            if ($errors) {
                throw new Mage_Customer_Exception(reset($errors));
            }
        } catch (Exception $e) {
            // Set fail response for validations
            $result['error'] = true;
            $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
            $result['field_error'] = $e->getMessage();
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        }
    }

    /**
     * @param $quote
     * @param $superOrderId
     * @return bool
     */
    private function _createNewQuoteOnServiceCallFail($quote, $superOrderId)
    {
        // Check if there is already a saved shopping cart saved for this
        $savedCarts = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('quote_parent_id', $quote->getId())
            ->addFieldToFilter('cart_status', Vznl_Checkout_Model_Sales_Quote::CART_STATUS_SAVED);
        if (count($savedCarts) == 0) {
            $newQuote = Mage::helper('vznl_checkout')->createNewCartQuote($quote, $superOrderId);
        } else {
            $newQuote = false;
        }
        if (!$newQuote) {
            return false;
        }
        Mage::getSingleton('checkout/cart')
            ->setQuote($newQuote)
            ->save();
        $this->_getCheckoutSession()->setQuoteId($newQuote->getId());
        $this->_getCustomerSession()->setFailedSuperOrderId($superOrderId);
    }

    /*
     * Saved Fixed Telephone Number to the Quote table
     */
    public function saveFixedNumberAction()
    {
        try {
            $fixedTelephoneNumber = $this->getRequest()->getParam('fixed_telephone_number');
            $hardwareName = $this->getRequest()->getParam('hardware_info');
            $serialNumber = $this->getRequest()->getParam('hardware_serial_number');
            $quote = $this->getQuote();
            $quoteItem = $quote->getAllItems();

            foreach ($quoteItem as $key => $item) {
                $sku = $item->getSku();
                if (isset($hardwareName[$sku])) {
                    $quote->setHardwareName($hardwareName[$sku]);
                }
                if (isset($serialNumber[$sku])) {
                    $quote->setSerialNumber($serialNumber[$sku]);
                }
            }

            $serviceAddress = unserialize($quote->getServiceAddress());
            $serviceHouseNumber = isset($serviceAddress["housenumber"]) ? $serviceAddress["housenumber"] : '';
            $serviceHouseNumberAddition = isset($serviceAddress["housenumberaddition"]) ? $serviceAddress["housenumberaddition"] : '';
            $servicePostalCode = isset($serviceAddress["postalcode"]) ? $serviceAddress["postalcode"] : '';
            $quote->setFixedTelephoneNumber($fixedTelephoneNumber);
            $quote->setCurrentStep('saveFixedNumber');
            $quote->save();
            //$customerType value is sent to the peal and the value is as per the peal specifications.
            $customerType = ($quote->getData('customer_id_type')) ? Vznl_Checkout_IndexController::PEAL_CUSTOMER_PRIVATE : Vznl_Checkout_IndexController::PEAL_CUSTOMER_BUSINESS;
            //$requestType action type to release or reserve
            $requestType = Vznl_Checkout_IndexController::FIXED_NUMBER_RESERVE;
            //Reserve Telephone Number to Peal
            $reserveHelper = Mage::helper('vznl_reserveTelephoneNumber');
            $reserveResponse = $reserveHelper->reserveTelephoneNumber($requestType, $serviceHouseNumber, $servicePostalCode, $fixedTelephoneNumber, $customerType, $serviceHouseNumberAddition);
            if ($reserveResponse['error']) {
                $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($reserveResponse['code'],$reserveResponse['message'],Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter::name);
                $reserveResponse = array(
                    'error' => true,
                    'message' => $pealErrorCode['translation'],
                );
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($reserveResponse)
                );
                return;
            }
            if ($reserveResponse) {
                $response = $reserveResponse;
                if ($response['statusCode'] != "200") {
                    $result['error'] = false;
                    $result['message'] = "Telephone Number Reserve Failed";
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }
            } else {
                $result['error'] = true;
                $result['message'] = "Telephone Number Reserve Failed";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }

            // Call vNext search API to add customer CTN
            $searchHelper = Mage::helper('vznl_customer/search');
            $searchHelper->addCustomerCtnApiCall(
                $quote->getCustomer(),
                $fixedTelephoneNumber,
                Vznl_Customer_Helper_Search::API_CUSTOMER_CTN_TYPE_FIXED
            );

            // Set successful response
            $result['error'] = false;
            $result['message'] = "Step data successfully saved";
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        } catch (Exception $e) {
            // Set fail response for other errors
            $result['error'] = true;
            $result['message'] = $this->__($e->getMessage());
            if ($this->getCoreServiceHelper()->isDev()) {
                $result['trace'] = $e->getTrace();
            }
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        }
        //return;
    }

    /*
     * Save selected service address
     */
    public function saveServiceAddressAction()
    {
        $address = $this->getRequest()->getParam('serviceAddress');

        if (!empty($address)) {
            $addressData = $this->getRequest()->getParam('addressData');
            $addressDataDecoded = json_decode($addressData,true);
            $serviceAddress = serialize($address);

            /** @var Dyna_Address_Model_Storage $sessionAddress */
            $sessionAddress = Mage::getSingleton('dyna_address/storage');
            $sessionAddress->setData('address', $address);

            if (!empty($addressDataDecoded['serviceability'])) {
                $sessionAddress->setData('addressData', $addressData);
            }

            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $quote->setServiceAddress($serviceAddress);
            $quote->save();
        }

        return true;
    }

    /*
     * Save payment informations
     */
    public function savePaymentAddressAction()
    {
        try {
            $data = $this->getRequest()->getParams();
            $quote = $this->getQuote();
            $serviceAddress = unserialize($quote->getServiceAddress());
            $serviceStreet = isset($serviceAddress["street"]) ? $serviceAddress["street"] : '';
            $serviceHouseNumber = isset($serviceAddress["housenumber"]) ? $serviceAddress["housenumber"] : '';
            $serviceHouseNumberAddition = isset($serviceAddress["housenumberaddition"]) ? $serviceAddress["housenumberaddition"] : '';
            $servicePostalCode = isset($serviceAddress["postalcode"]) ? $serviceAddress["postalcode"] : '';
            $serviceCity = isset($serviceAddress["city"]) ? $serviceAddress["city"] : '';

            $quoteBillingAddress = $quote->getBillingAddress()->getData();

            $billingAddress = Mage::getModel('sales/quote_address')->setData($quoteBillingAddress);

            if ($data['address_type'] == 'same_as_service_address') {
                $billingAddress->setStreet(array($serviceStreet, $serviceHouseNumber, $serviceHouseNumberAddition));
                $billingAddress->setData('city', $serviceCity);
                $billingAddress->setData('postcode', str_replace(' ', '', $servicePostalCode));
            } else {
                $billingAddress->setData('region_id', $data['otherFixedAddress']['addressid']);
                $billingAddress->setStreet(array($data['otherAddress']['street'], $data['otherAddress']['houseno'], $data['otherAddress']['addition']));
                $billingAddress->setData('city', $data['otherAddress']['city']);
                $billingAddress->setData('postcode', str_replace(' ', '', $data['otherAddress']['postcode']));
            }
            $billingAddress->save();
            $customer = $quote->getCustomer();
            if (!$customer) {
                $customer->setData('account_no', $data['account_number']);
                $customer->setData('account_holder', $data['account_holder_name']);
            }
            $quote->setData('customer_account_number', $data['account_number']);
            $quote->setData('customer_account_holder', $data['account_holder_name']);
            $quote->setData('fixed_payment_method_monthly_charges', $data['fixed_payment_method_monthly_charges']);
            $quote->setData('fixed_bill_distribution_method', $data['fixed_bill_distribution_method']);
            $quote->setData('fixed_payment_method_one_time_charge', $data['fixed_payment_method_one_time_charge']);
            $quote->setBillingAddress($billingAddress);
            $quote->setCurrentStep('savePaymentAddress');
            $quote->save();
            // set success response
            $result['error'] = false;
            $result['message'] = "Step data successfully saved";
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        } catch (Exception $e) {
            // Set fail response for other errors
            $result['error'] = true;
            $result['message'] = $this->__($e->getMessage());
            if ($this->getCoreServiceHelper()->isDev()) {
                $result['trace'] = $e->getTrace();
            }
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        }
    }

    /*
     * Saved Fixed Written Consent to the Quote table
     */
    public function saveWrittenConsentAction()
    {
        try {
            $writtenConsent = $this->getRequest()->getParam('written_consent');
            $quote = $this->getQuote();
            $quote->setFixedWrittenConsent($writtenConsent);
            $quote->setCurrentStep('saveWrittenConsent');
            $quote->save();
            // Set successful response
            $result['error'] = false;
            $result['message'] = "Step data successfully saved";
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        } catch (Exception $e) {
            // Set fail response for other errors
            $result['error'] = true;
            $result['message'] = $this->__($e->getMessage());
            if ($this->getCoreServiceHelper()->isDev()) {
                $result['trace'] = $e->getTrace();
            }
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        }

    }
    public function saveTechnicianDeliveryAction()
    {
        $request = $this->getRequest()->getParam('value');
        $sku = $this->getRequest()->getParam('sku');
        $packageId = $this->getRequest()->getParam('packageId');
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        $isCheckout = true;
        $rightBlock = $this->getLayout()->createBlock('dyna_configurator/rightsidebar')->setCheckout($isCheckout)->setTemplate('customer/right_sidebar.phtml');
        if ($product) {
            $productId = $product->getId();
            $productmodel = Mage::getModel('catalog/product')->load($productId);
            if ($request == "remove") {
                $quote = $this->getQuote();
                $quoteItem = $quote->getAllItems();
                foreach ($quoteItem as $item){
                    $catalogSku = $item->getData('sku');
                    if ($catalogSku == $sku) {
                        $quote->removeItem($item->getData('item_id'));
                        $quote->save();
                    }
                }
                $quote->setActivePackageId($packageId)->collectTotals()->save();
                $Helper = Mage::helper('vznl_validatecart')->validate();
                $result['error'] = false;
                $result['message'] = "item is removed successfully";
                $result['totals'] = $this->getLayout()->createBlock('checkout/cart_totals')->setCheckoutButton($isCheckout)->setTemplate('checkout/cart/totals.phtml')->toHtml();
                $result['rightBlock'] = $rightBlock->setCurrentPackageId($packageId)->toHtml();
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } else {

                $quote = $this->getQuote();
                $quoteItem = $quote->getAllItems();
                foreach ($quoteItem as $item) {
                    $catalogSku = $item->getData('sku');
                    if ($catalogSku == $sku) {
                        return true;
                    }
                }

                $cart = Mage::getSingleton('checkout/cart');
                $cart->addProduct($productmodel, array('qty'=>'1'));
                $cart->save();
                $quote->setActivePackageId($packageId)->collectTotals()->save();
                $Helper = Mage::helper('vznl_validatecart')->validate();
                $dynahelper = Mage::helper('dyna_checkout');
                $productprice = $dynahelper->showNewFormattedPriced($product->getPrice());

                //$productprice = number_format($product->getPrice(), 2',','.');

                $result['price'] = $productprice;
                $result['error'] = false;
                $result['message'] = "item is added successfully";
                $result['totals'] = $this->getLayout()->createBlock('checkout/cart_totals')->setCheckoutButton($isCheckout)->setTemplate('checkout/cart/totals.phtml')->toHtml();
                $result['rightBlock'] = $rightBlock->setCurrentPackageId($packageId)->toHtml();
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }
        } else {
            $result['error'] = true;
            $result['message'] = "their is no such product";
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );

        }
    }

    /*
     * Save delivery address & date information
     */
    public function saveFixedDeliveryAddressAction()
    {
        $request = $this->getRequest();
        $helper = Mage::helper('checkout/data');
        if ($request->isPost()) {
            try {
                $quote = $this->getQuote();
                $fixedDeliveryMethod = $this->getRequest()->getParam('fixed_delivery_method');
                $fixedDeliveryMethodData = $this->getRequest()->getParam('fixed_delivery_method');
                $fixedDeliveryWishDate = $helper->getFixedSelectedDeliveryWishDate($fixedDeliveryMethodData, $this);
                $quote->setFixedDeliveryMethod($fixedDeliveryMethod);
                $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);
                $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);

                /*$basketId = $quote->getData('basket_id');
                $customerType = ($quote->getData('customer_id_type')) ? Dyna_Customer_Helper_Services::PARTY_TYPE_PRIVATE : Dyna_Customer_Helper_Services::PARTY_TYPE_SOHO;
                foreach (Mage::getModel('checkout/session')->getQuote()->getCartPackages() as $package) {
                    if ($package->isFixed()) {
                        $isOverstappen = $package->getFixedOverstappenStatus();
                    }
                }
                $availableDeliveryDates = Mage::helper('vznl_deliverywishdates')->getDeliveryWishDates($basketId, $customerType, $fixedDeliveryMethod, $isOverstappen, $quote->getQuoteNewHardware());

                foreach ($availableDeliveryDates['availableDateDetails'] as $date) {
                    $availability[] = $date['availableStartDate'];
                }

                if (in_array($fixedDeliveryWishDate, $availability)) {
                    $quote->setFixedDeliveryWishDate($fixedDeliveryWishDate);
                } else {
                    $quote->setData('delivery_wish_date', null);
                    $result['error'] = true;
                    $result['message'] = "Please select a valid date";
                    $result['fields']['delivery_wish_date'] = $this->__('Please select a valid date');
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }*/

                $address = $request->getPost('otherFixedAddress', array());

                if (array_filter($address)) {
                    $addressId = ($address['addressid'])?$address['addressid']:"";
                    $data['deliver']['address'] = array(
                        'address_id'=> $addressId,
                        'street' => array($address['street'], $address['houseno'], $address['addition']),
                        'postcode' => $address['postcode'],
                        'city' => $address['city']
                    );
                    $data['deliver']['address']['address'] = 'other_address';
                } elseif ($request->getPost('serviceAddress', array())) {
                    $address = $request->getPost('serviceAddress', array());
                    if ($fixedDeliveryMethod == "SHOP") {
                        $addressData = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();

                        $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                            'address' => 'store',
                            'store_id' => $addressData,
                        ));
                    } else {
                        $data['deliver']['address'] = array(
                            'street' => array($address['street'], $address['houseno'], $address['addition']),
                            'postcode' => $address['postcode'],
                            'city' => $address['city']
                        );
                    }
                    $data['deliver']['address']['address'] = 'service_address';
                }
                $data['payment'] = Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT;

                $this->_saveDeliveryAddressData($data);

                if ($request->getPost('current_step')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }

                // START Update quote shipping address
                $shippingAddress = $quote->getShippingAddress();
                if (isset($data['deliver']['address']['street']) && is_array($data['deliver']['address']['street'])) {
                    $shippingAddress->setStreet($data['deliver']['address']['street']);
                }
                if (isset($data['deliver']['address']['postcode'])) {
                    $shippingAddress->setData('postcode', $data['deliver']['address']['postcode']);
                }
                if (isset($data['deliver']['address']['city'])) {
                    $shippingAddress->setData('city', $data['deliver']['address']['city']);
                }
                if (isset($data['deliver']['address']['address_id'])) {
                    $shippingAddress->setData('region_id', $data['deliver']['address']['address_id']);
                }
                $shippingAddress->save();
                // END Update quote shipping address

                $quote->save();

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }
        }
    }

    /*
     * Show peal order number
     */
    public function showPealOrderAction()
    {
        $orderNo = $this->getRequest()->getParam('orderNo');
        $superOrder = Mage::getModel('superorder/superorder')->load((string) $orderNo, 'order_number');
        $pealOrder = $superOrder->getPealOrderId() ? $superOrder->getPealOrderId() : '';

        $result['peal_order_id'] = $pealOrder;
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }

    /*
     * Save customer service address
     * @param array|null $serviceAddress
     * @return void
     */
    protected function saveCustomerServiceAddress(
        ?array $serviceAddress
    ):void
    {
        $customerSession = $this->_getCustomerSession()->getCustomer();
        if ($customerSession->getId()) {
            $customer = Mage::getModel('customer/customer')->load($customerSession->getId());
            $service_addressId = $customer->getData('default_service_address_id');
            $address = Mage::getModel('customer/address')->load($service_addressId);
            $hasChanges = false;

            if ($serviceAddress) {
                $StreetName = isset($serviceAddress["street"]) ? $serviceAddress["street"] : '';
                $buildingNumber = isset($serviceAddress["housenumber"]) ? $serviceAddress["housenumber"] : '';
                $postalZone = isset($serviceAddress["postalcode"]) ? $serviceAddress["postalcode"] : '';
                $cityName = isset($serviceAddress["city"]) ? $serviceAddress["city"] : '';
                $addressId = isset($serviceAddress["addressId"]) ? $serviceAddress["addressId"] : '';
                $country = 'NL';
                $buildingNumberAddition = isset($serviceAddress["housenumberaddition"]) ? $serviceAddress["housenumberaddition"] : '';

                $addressData = array(
                    'is_active' => 1,
                    'prefix' => null,
                    'firstname' => $customer->getFirstName(),
                    'middlename' => null,
                    'lastname'=> $customer->getLastName(),
                    'suffix'=> null,
                    'city' => $cityName,
                    'postcode' => $postalZone,
                    'country_id' => $country,
                    'telephone' =>null,
                    'parent_id' => $customer->getId(),
                );

                foreach ($addressData as $key => $value) {
                    if ($address->getData($key) != $value) {
                        $address->setData($key, $value);
                        $hasChanges = true;
                    }
                }

                $street = array(
                    '0' => $StreetName,
                    '1' => $buildingNumber,
                    '2' => $buildingNumberAddition,
                    '3' => $addressId
                );


                if ($address->getStreet() != $street) {
                    $address->setStreet($street);
                    $hasChanges = true;
                }

                if ($hasChanges) {
                    $address->save();
                }
                $customer->setDefaultServiceAddressId($address->getId())
                    ->save();
            }
        }
    }

    /*
     * Save footprint
     */
    public function saveFootprintAction()
    {
        $footprint = $this->getRequest()->getParam('footprint');
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quote->setFootprint($footprint);
        $quote->save();

        return true;
    }

    public function createApiCustomer($customer, $customerBilling, $quote, $type)
    {
        Mage::helper('vznl_customer/search')->createApiCustomer($customer, $customerBilling, $quote, $type);
    }

    public function saveChangeDeliveryAddressAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $quote = $this->getQuote();
                $helper = Mage::helper('checkout/data');

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }
                $delivery = $request->getPost('delivery', array());
                $data = array();
                $errors = array();
                if (!isset($delivery['method'])) {
                    throw new Exception($this->__('Missing shipment method'));
                }
                switch ($delivery['method']) {
                    case Vznl_Checkout_IndexController::DELIVERY_METHOD_DELIVER :
                        $delivery['deliver']['address'] = Vznl_Checkout_IndexController::DELIVERY_METHOD_BILLING_ADDRESS;
                        if (!isset($delivery['deliver']['address'])) {
                            throw new Exception($this->__('Missing shipping address info'));
                        }
                        $address = $request->getPost('billingAddress', array());
                        $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                            'address' => $delivery['deliver']['address'],
                            'billingAddress' => $request->getPost('billingAddress', array()),
                        ));
                        $errors += $this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address']);
                        break;
                    case Vznl_Checkout_IndexController::DELIVERY_METHOD_OTHER_ADDRESS :
                        $delivery['deliver']['address'] = Vznl_Checkout_IndexController::DELIVERY_METHOD_OTHER_ADDRESS;
                        if (!isset($delivery['deliver']['address'])) {
                            throw new Exception($this->__('Missing shipping address info'));
                        }

                        $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                            'address' => $delivery['deliver']['address'],
                            'otherAddress' => $request->getPost('otherAddress', array()),
                        ));

                        $errors += $this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address']);
                        break;
                    default:
                        $errors += array('delivery[method]' => $this->__('Invalid delivery method'));
                }
                if ($errors) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
                }

                $this->_saveDeliveryAddressData($data);
                $quote->save();

                // Set successful response
                Mage::getSingleton('checkout/session')->setDeliveryChanged(true);
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getCoreServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }
}
