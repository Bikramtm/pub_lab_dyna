<?php

/**
 * One page checkout status
 *
 * @category   Dyna
 * @package    Vznl_Checkout
 */
class Vznl_Checkout_Block_Pdf_Contract extends Mage_Page_Block_Html
{
    /**
     * @var Vznl_Checkout_Model_Sales_Order $order
     */
    private $order;

    /**
     * @var bool $logo
     */
    private $logo = true;

    /**
     * @var Vznl_Package_Model_Package $package
     */
    private $package;

    /**
     * @var string $websiteCode
     */
    private $websiteCode;

    private $orderDeviceRCAmount = null;

    /**
     * @param Vznl_Checkout_Model_Sales_Order $order
     * @param bool $logo
     * @return $this
     */
    public function init(Vznl_Checkout_Model_Sales_Order $order)
    {
        $this->order = $order;
        $this->websiteCode = $this->order->getWebsiteCode();
        return $this;
    }

    /**
     * @return bool
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param bool $logo
     * @return $this
     */
    public function setLogo(bool $logo)
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencySymbol()
    {
        return Mage::app()->
        getLocale()->
        currency(Mage::app()->getStore()->getCurrentCurrencyCode())->
        getSymbol();
    }

    /**
     * @return bool
     */
    public function isNewCustomer() {
        return ($this->getOrder()->getSuperOrder()->getNewCustomer() == 1);
    }

    /**
     * Get the Order Number
     * @return string
     */
    public function getOrderNumber()
    {
        $superOrder = $this->getOrder()->getSuperOrder();

        if($superOrder) {
            $superOrderNumber = $superOrder->getOrderNumber();
        }
        return $superOrderNumber;
    }

    /**
     * Get the fullname of customer
     * @return string
     */
    public function getCustomerName()
    {
        return $this->getCustomerIsBusiness()
            ? $this->order->getCustomer()->getData('company_name')
            : join(' ', array_filter(array(
                $this->__($this->order->getCustomer()->getData('prefix')),
                $this->order->getCustomer()->getData('firstname'),
                $this->order->getCustomer()->getData('middlename'),
                $this->order->getCustomer()->getData('lastname')
            )));
    }

    /**
     * Get the Birth Name of customer
     * @return string
     */
    public function getCustomerBirthName()
    {
        return $this->order->getCustomerMiddlename();
    }

    /**
     * @return bool
     */
    public function getCustomerIsBusiness()
    {
        return $this->order->getCustomer()->getIsBusiness();
    }

    /**
     * Get the Coc number
     * @return mixed
     */
    public function getKvkNumber()
    {
        return $this->order->getCustomer()->getData('company_coc');
    }

    /**
     * Get the VAT number
     * @return mixed
     */
    public function getVATNumber()
    {
        return $this->order->getCompanyVatId();
    }

    /**
     * Get customer date of birth in format d-m-Y
     * @return string
     */
    public function getCustomerDob()
    {
        $dob = $this->getCustomerIsBusiness()
            ? $this->order->getCustomer()->getData('contractant_dob')
            : $this->order->getCustomer()->getData('dob');
        $date = new DateTime($dob);
        return $date->format('d-m-Y');
    }

    /**
     * @return string
     */
    public function getCustomerBankAccount()
    {
        return $this->order->getCustomerAccountNumberLong() ?: $this->order->getCustomer()->getData('account_no');
    }

    /**
     * @return string
     */
    public function getCustomerBankAccountMask()
    {
        return Vznl_Customer_Model_Customer_Customer::MASK_CHAR . substr($this->getCustomerBankAccount(), Vznl_Customer_Model_Customer_Customer::UNMASK_CHAR_LENGTH);
    }

    /**
     * @return string
     */
    public function getCustomerBankAccountHolder()
    {
        return $this->order->getCustomer()->getData('account_holder');
    }

    /**
     * @return mixed
     */
    public function getCustomerEmail()
    {
        $emails = explode(';', $this->order->getCustomer()->getData('additional_email'));
        return !is_null($this->order->getCorrespondanceEmail()) ? $this->order->getCorrespondanceEmail() : $emails[0];
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->getCustomerIsBusiness()
            ? join(' ', array_filter(array(
                $this->__($this->order->getContractantPrefix()),
                $this->order->getCustomer()->getData('contractant_firstname'),
                $this->order->getContractantMiddlename(),
                $this->order->getCustomer()->getData('contractant_lastname')
            )))
            : join(' ', array_filter(array(
                $this->__($this->order->getCustomerPrefix()),
                $this->order->getCustomer()->getData('firstname'),
                $this->order->getCustomerMiddlename(),
                $this->order->getCustomer()->getData('lastname')
            )));
    }

    /**
     * @return mixed
     */
    public function getCustomerBan()
    {
        return $this->order->getCustomer()->getBan();
    }

    /**
     * @return string (Peal Account Number)
     */
    public function getCustomerPan()
    {
        return $this->order->getCustomer()->getPan();
    }

    /**
     * @return string
     */
    public function getBillingAddressStreet()
    {
        $address = $this->order->getBillingAddress();
        return $address->getStreet1() . ' ' . $address->getStreet2(). ' ' . $address->getStreet3();
    }

    public function getBillingAddressPostalcode()
    {
        $address = $this->order->getBillingAddress();
        return $address->getPostcode();
    }

    public function getBillingAddressCity()
    {
        $address = $this->order->getBillingAddress();
        return $address->getCity();
    }

    public function getBillingAddressCountry()
    {
        $address = $this->order->getBillingAddress();
        return Mage::app()->getLocale()->getCountryTranslation($address->getCountry());
    }

    public function getBillingHouseNumberAddition()
    {
        $address = $this->order->getData('service_address');
        return unserialize($address);
    }

    /**
     * Check if this contract has porting
     * @return bool
     */
    public function hasNumberporting(): bool
    {
        foreach ($this->getOrder()->getPackages() as $package) {
            if ($package->isNumberPorting()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return Vznl_Checkout_Model_Sales_Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getIDType()
    {
        $idType = $this->getCustomerIsBusiness()
            ? $this->order->getCustomer()->getData('contractant_id_type')
            : $this->order->getCustomer()->getData('id_type');

        $identificationTypes = [
            'N' => 'Dutch ID card',
            'P' => 'Passport',
            'R' => 'Drivers license',
            '0' => 'Type EU',
            '1' => 'Type I',
            '2' => 'Type II',
            '3' => 'Type III',
            '4' => 'Type IV',
        ];
        return $identificationTypes[$idType];
    }

    public function getIDNumber()
    {
        return $this->getCustomerIsBusiness()
            ? $this->order->getCustomer()->getData('contractant_id_number')
            : $this->order->getCustomer()->getData('id_number');
    }

    /**
     * @return string
     */
    public function getIDNumberMask()
    {
        return Vznl_Customer_Model_Customer_Customer::MASK_CHAR . substr($this->getIDNumber(), Vznl_Customer_Model_Customer_Customer::UNMASK_CHAR_LENGTH);
    }

    public function getIDExpireDate()
    {
        $date = new DateTime(($this->getCustomerIsBusiness() ? $this->order->getCustomer()->getContractantValidUntil() : $this->order->getCustomerValidUntil()));
        return $date->format('d-m-Y');
    }

    public function getPaymentMethod()
    {
        return Mage::helper('vznl_checkout')->getPaymentOptionTextForName($this->order->getPayment()->getMethod());
    }

    public function getOrderDate()
    {
        $date = new DateTime($this->order->getCreatedAt());
        return $date->format('d-m-Y');
    }

    public function getPriceplan()
    {
        foreach ($this->getOrder()->getAllItems() as $item) {
            if ($item->getPackageId() == $this->getPackage()->getPackageId()) {
                $flippedProductType = array_flip($item->getProduct()->getType());
                if (isset($flippedProductType[Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION])) {
                    return $item->getProduct();
                }
            }
        }
        return Mage::getModel('catalog/product');
    }

    /**
     * @return Vznl_Package_Model_Package
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param Vznl_Package_Model_Package $package
     * @return $this
     */
    public function setPackage(Vznl_Package_Model_Package $package)
    {
        $this->package = $package;
        return $this;
    }

    public function getSubscriptionStartDate()
    {
        return $this->package->getSubscriptionStartDate()->format('d-m-Y');
    }

    public function getSigningDate()
    {
        if (!$this->order->getContractSignDate()) {
            return date('Y-m-d');
        }

        return $this->order->getContractSignDate();
    }

    public function getDevice()
    {
        if ($this->websiteCode == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
            /** If the shop is indirect, fake the product */

            if (!is_null($this->getPackage())) {
                // Return null if simonly product and no device name got set
                if(is_null($this->getPackage()->getDeviceName())){
                    return null;
                }
                $indirectDevice = Mage::getModel('sales/order_item');
                $indirectProduct = Mage::getModel('catalog/product');

                $indirectProduct->setPrice($this->getPackage()->getCatalogPriceInclTax());
                $indirectProduct->setTaxClassId(2);

                $indirectDevice->setProduct($indirectProduct);

                $indirectDevice->setName($this->getPackage()->getDeviceName());

                $subscriptionDuration = ($this->getDeviceRC()) ? intval($this->getDeviceRC()->getProduct()->getAttributeText('identifier_commitment_months')) : 0;
                $subscriptionCosts = ($this->getDeviceRC()) ? $this->getDeviceRC()->getItemFinalMafInclTax() : 0;

                // One time device costs
                $indirectDevice->setRowTotalInclTax($this->getPackage()->getMixmatch()); // ItemFinalPriceInclTax
                $indirectDevice->setRowTotal(Mage::helper('tax')->getPrice($indirectProduct, $this->getPackage()->getMixmatch(), false)); // ItemFinalPriceExclTax

                // Device price i.c.w. subscription
                $oneTimePayment = $indirectDevice->getItemFinalPriceInclTax();
                $totalDeviceRc = ($subscriptionDuration * $subscriptionCosts);
                $totalCostIncl = $totalDeviceRc + $oneTimePayment;
                $totalTax = $totalCostIncl/1.21*.21;

                $indirectDevice->setMixmatchSubtotal($totalCostIncl-$totalTax);
                $indirectDevice->setMixmatchTax($totalTax);
                return $indirectDevice;
            }
        }

        foreach ($this->getOrder()->getAllItems() as $item) {
            if (!$this->package || $item->getPackageId() == $this->getPackage()->getPackageId()) {
                $flippedProductType = array_flip($item->getProduct()->getType());
                if (isset($flippedProductType[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE])) {
                    return $item;
                }
            }
        }
        return null;
    }

    public function isSimOnly()
    {
        $deviceRc = $this->getDeviceRC();
        $isIndirect = $this->websiteCode == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE;
        return !$deviceRc
            || ($isIndirect && $deviceRc->getSku() == Mage::helper('vznl_checkout/aikido')->getIndirectNoHardwareSku())
            || !$this->getDevice();
    }

    /**
     * @return null|Vznl_Checkout_Model_Sales_Order_Item
     */
    public function getDeviceRC()
    {
        foreach ($this->getOrder()->getAllItems() as $item) {
            if (!$this->package || $item->getPackageId() == $this->getPackage()->getPackageId()) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                if($product->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                    return $item;
                }
            }
        }
        return null;
    }

    /**
     * @return bool
     */
    public function hasDeviceRC()
    {
        if (!$this->getDeviceRC() || $this->getDeviceRC()->getMaf() == 0) {
            return false;
        }

        return true;
    }

    /**
     * Loops through all order items and returns true if the total DeviceRC is greater than zero
     * @return bool
     */
    public function orderHasDeviceRC()
    {
        if (is_null($this->orderDeviceRCAmount)){
            foreach ($this->getOrder()->getAllItems() as $item) {
                $flippedProductType = array_flip($item->getProduct()->getType());
                if (isset($flippedProductType[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION])) {
                    $this->orderDeviceRCAmount +=  $item->getMaf();
                }
            }
        }

        return $this->orderDeviceRCAmount > 0;
    }

    /**
     * @return string
     */
    public function getBarcodeImage()
    {
        /** @var Vznl_Barcode_Model_Barcode $barcodeGen */
        $barcodeGen = Mage::getModel('barcode/barcode');

        $code = (string)$this->order->getSuperorder()->getOrderNumber();
        while (strlen($code) < 21) {
            $code = '0' . $code;
        }
        $data = $barcodeGen->generate('ORD-' . $code);
        return base64_encode($data);
    }

    /**
     * Get outlet string
     *
     * @return string
     */
    public function getOutlet()
    {
        $dealerCode = $this->getDealer()->getVfDealerCode();

        switch ($this->websiteCode) {
            case Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE:
            case Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE:
                return $dealerCode . ", " . $this->getDealer()->getAddress();
            case Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE:
                return $dealerCode . ", " . "Vodafone Telesales";
            case Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE:
                return $dealerCode . ", " . "Vodafone Online Shop";
            default:
                return $dealerCode;
        }
    }

    /**
     * Check if the channel is either Telesales/Eshop and not Direct/Indirect
     * @return bool
     */
    public function isTelesalesOrEshop()
    {
        /** @var Vznl_Agent_Helper_Data $agentHelper */
        $agentHelper = Mage::helper('agent');
        if ($agentHelper->checkIsTelesalesOrWebshop()
            && !($agentHelper->isRetailStore()
            || $agentHelper->isIndirectStore())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return Dyna_Agent_Model_Dealer
     */
    public function getDealer()
    {
        return $this->order->getDealer();
    }

    /**
     * Return CTN assosiated with the package
     * @param Vznl_Package_Model_Package $package
     * @return string
     */
    public function getCtn($package)
    {
        return $package->getCtn()
            ?: $package->getCurrentNumber()
                ?: $package->getTelNumber();
    }

    /**
     * Return current Agent name
     * @return string
     */
    public function getAgentName()
    {
        $agent = Mage::helper('agent/data')->getAgent();
        return $agent ? $agent->getName() : '';
    }

    /**
     * Return current Agent sales code
     * @return string
     */
    public function getAgentSalesCode()
    {
        $agent = Mage::helper('agent/data')->getAgent();
        return $agent->getDealerCode();
    }

    /**
     * Return current customer Id
     * @return string
     */
    public function getCustomerId()
    {
        return Mage::getSingleton('customer/session')->getCustomerId();
    }

    /**
     * Return current customer's telephone number
     * @return string
     */
    public function getCustomerTelephone()
    {
        return $this->order->getBillingAddress()->getTelephone();
    }

    /**
     * Return PSTN selected
     * @return string
     */
    public function getPstn()
    {
        return $this->order->getData('fixed_telephone_number');
    }

    /**
     * Return Extra PSTN selected
     * @return string
     */
    public function getExtraPstn()
    {
        return $this->order->getData('fixed_extra_telephone_no');
    }


    /**
     * Return price_increase_scope
     * @return bool
     */
    public function getPriceIncreaseScope()
    {
        foreach ($this->getPackage()->getPackageItems() as $item) {
            $ProductId = $item->getProductId();
            $StoreId = $item->getStoreId();
            $product = Mage::getResourceModel('catalog/product');
            $priceIncreaseScope = $product->getAttributeRawValue($ProductId, 'price_increase_scope', $StoreId);
            if ($priceIncreaseScope) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return fixed_payment_method_monthly_charges selected
     * @return string
     */
    public function getFixedPaymentMethodMonthlyCharges()
    {
        $paymentMethod = $this->order->getData('fixed_payment_method_monthly_charges');
        if(strtolower($paymentMethod) == 'direct_debit') {
            $paymentType = 'Direct debit';
        } elseif (strtolower($paymentMethod) == 'acceptgiro') {
            $paymentType = 'By Invoice';
        }
        return $paymentType;
    }

    /**
     * Return fixed_bill_distribution_method selected
     * @return string
     */
    public function getFixedBillDistributionMethod()
    {
        return $this->order->getData('fixed_bill_distribution_method');
    }

    public function getOrderStatus()
    {
        $superOrder = $this->getOrder()->getSuperOrder();

        if($superOrder) {
            $superOrderStatus = $superOrder->getOrderStatus();
        }
        return $superOrderStatus;

    }

    public function getOrderStatusNotCancelledOrClosed()
    {
        $orderStatus = $this->getOrderStatus();
        if (($orderStatus == Vznl_Superorder_Model_Superorder::SO_CANCELLED) || ($orderStatus == Vznl_Superorder_Model_Superorder::SO_CLOSED)) {
            return true;
        }

    }

    public function isCustomer()
    {
        return ($this->getCustomerBan() || $this->getCustomerPan()) ? true : false;
    }

}