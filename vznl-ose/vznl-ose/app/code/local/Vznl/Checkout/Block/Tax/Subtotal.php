<?php
/**
 * @category    Dyna
 * @package     Vznl_Checkout
 */

/**
 * Subtotal Total Row Renderer
 */

class Vznl_Checkout_Block_Tax_Subtotal extends Mage_Tax_Block_Checkout_Subtotal
{
    /**
     * Get subtotal excluding tax
     *
     * @return float
     */
    public function getMafSubtotalExclTax()
    {
        $excl = $this->getMafTotal()->getAddress()->getMafGrandTotal() - $this->getMafTotal()->getAddress()->getMafTaxAmount();
        $excl = max($excl, 0);
        return $excl;
    }

    /**
     * Get subtotal excluding tax
     *
     * @return float
     */
    public function getDeviceTotal()
    {
        $excl = $this->getTotal()->getAddress()->getInitialMixmatchSubtotal() != null ? $this->getTotal()->getAddress()->getInitialMixmatchSubtotal() : $this->getTotal()->getAddress()->getGrandTotal() - $this->getTotal()->getAddress()->getTaxAmount();
        $excl = max($excl, 0);
        return $excl;

    }
}
