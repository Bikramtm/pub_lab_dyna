<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Dyna
 * @package     Vznl_Checkout
 */

/**
 * One page checkout status
 *
 * @category   Dyna
 * @package    Vznl_Checkout
 */
class Vznl_Checkout_Block_Pdf_Loanoverview extends Mage_Page_Block_Html
{
    /** @var Vznl_Package_Model_Package null  */
    protected $_package = null;

    /** @var Vznl_Checkout_Model_Sales_Order_Item */
    protected $_deviceRc = null;
    /** @var Vznl_Checkout_Model_Sales_Order_Item */
    protected $_device = null;

    /**
     * @param Vznl_Package_Model_Package $package
     *
     * @return Vznl_Checkout_Block_Pdf_Loanoverview
     */
    public function init($package)
    {
        $this->_package = $package;

        $items = $package->getPackageItems();
        /** @var Vznl_Checkout_Model_Sales_Order_Item $item */
        foreach ($items as $item) {
            if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION),
                $item->getProduct())) {
                $this->_deviceRc = $item;
            } else if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE),
                $item->getProduct())) {
                $this->_device = $item;
            }
        }
        return $this;
    }

    public function getTotalCost()
    {
        return $this->_deviceRc->getTotalPaidByMaf(true);
    }

    public function getTotalLoan()
    {
        return $this->_deviceRc->getTotalPaidByMaf(true);
    }

    public function getContractLength()
    {
        return $this->_deviceRc->getProduct()->getAttributeText(Vznl_Catalog_Model_Product::SUBSCRIPTION_DURATION);
    }

    public function getDevicePrice()
    {
        if ($this->isIndirect()) {
            return round($this->_package->getCatalogPriceInclTax(),0);
        }
        return round($this->_device->getProduct()->getPrice(true),0);
    }

    public function getDeviceName()
    {
        if ($this->isIndirect()) {
           return $this->_package->getDeviceName();
        }
        return $this->_device->getProduct()->getName();
    }

    public function isIndirect()
    {
        $websiteId = $this->_package->getSuperOrder()->getWebsiteId();
        $websiteCode = Mage::app()->getWebsite($websiteId)->getCode();
        return $websiteCode == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE;
    }

    public function getCreditMediator()
    {
        $dealer = $this->_package->getSuperOrder()->getDealer();
        $paidCode = $dealer ? $dealer->getPaidCode() : null;

        // If not indirect or dealer not exists use this or has no paid code
        if (!$this->isIndirect() || !$paidCode) {
            return [
                'name' => 'Vodafone Libertel B.V.',
                'address' => [
                    'street' => 'Avenue Ceramique 300',
                    'zipcode' => '6221 KX',
                    'city' => 'Maastricht'
                ],
                'url' => 'www.vodafone.nl',
                'phone' => '0800-0094'
            ];
        }

        // get dealer with paid code as vf dealer code
        $dealer = Mage::getModel('agent/dealer')->load($paidCode, 'vf_dealer_code');
        $name = ''; // OMNVFNL-1488 still need a way to retrieve a name here
        $name = ($dealer->getName())?(string)$dealer->getName():'';
        return [
            'name' => $name,
            'address' => [
                'street' => $dealer->getStreet(),
                'zipcode' => $dealer->getPostcode(),
                'city' => $dealer->getCity()
            ],
            'url' => 'www.vodafone.nl',
            'phone' => $dealer->getTelephone()
        ];
    }
}
