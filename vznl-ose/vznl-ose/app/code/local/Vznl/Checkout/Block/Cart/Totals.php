<?php
/**
 * @category    Dyna
 * @package     Vznl_Checkout
 */

class Vznl_Checkout_Block_Cart_Totals extends Mage_Checkout_Block_Cart_Totals
{
    const TOTALS_TEMPLATE_CHECKOUT = 1;
    const TOTALS_TEMPLATE_SHOPPING_BAG = 2;
    
    protected $_mafTotals = null;

    protected $_renderers = array();

    public function getMafTotals()
    {
        if (is_null($this->_mafTotals)) {
            return $this->getMafTotalsCache();
        }
        return $this->_mafTotals;
    }

    public function getMafTotalsCache()
    {
        if (empty($this->_mafTotals)) {
            $this->_mafTotals = $this->getQuote()->getMafTotals();
        }
        return $this->_mafTotals;
    }

    public function setMafTotals($value)
    {
        $this->_mafTotals = $value;
        return $this;
    }

    /**
     * Render totals html for specific totals area (footer, body)
     *
     * @param   null|string $area
     * @param   int $colspan
     * @return  string
     */
    public function renderTotals($area = null, $colspan = 1, $checkout = 0)
    {
        $this->_renderers = array();
        $html = '';
        foreach($this->getTotals() as $total) {
            if ($total->getArea() != $area && $area != -1) {
                continue;
            }
            $code = $total->getCode();
            if ($total->getAs()) {
                $code = $total->getAs();
            }
            if(!isset($this->_renderers[$code])) {
                $this->_renderers[$code] = $this->_getTotalRenderer($code)->setTotal($total);
            }
            else {
                $this->_renderers[$code]->setTotal($total);
            }
        }

        foreach($this->getMafTotals() as $total) {
            if ($total->getArea() != $area && $area != -1) {
                continue;
            }
            $code = $total->getCode();
            if ($total->getAs()) {
                $code = $total->getAs();
            }
            $key = (strpos($code, 'maf_') === 0) ? substr($code, 4) : $code;
            if(!isset($this->_renderers[$key])) {
                $this->_renderers[$key] = $this->_getTotalRenderer($code)->setMafTotal($total);
            }
            else {
                $this->_renderers[$key]->setMafTotal($total);
            }
        }

        foreach($this->_renderers as $key => $renderer) {
            if ($key == 'discount'){
                continue;
            }
            $html .= $renderer->setColspan($colspan)
                ->setRenderingArea(is_null($area) ? -1 : $area)
                ->setData('checkout', $checkout)
                ->toHtml();
        }
        return $html;
    }
}
