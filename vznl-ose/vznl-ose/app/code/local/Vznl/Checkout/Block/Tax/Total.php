<?php
/**
 * @category    Dyna
 * @package     Vznl_Checkout
 */

/**
 * Ttotal Total Row Renderer
 */

class Vznl_Checkout_Block_Tax_Total extends Mage_Checkout_Block_Total_Default
{
    protected $_template = 'tax/checkout/total.phtml';

    /**
     * Get subtotal excluding tax
     *
     * @return float
     */
    public function getMafSubtotalExclTax()
    {
        $excl = $this->getTotal()->getAddress()->getMafGrandTotal();
        $excl = max($excl, 0);
        return $excl;
    }

    /**
     * Get subtotal excluding tax
     *
     * @return float
     */
    public function getDeviceTotal()
    {
        $excl = $this->getTotal()->getAddress()->getInitialMixmatchTotal() != null ? $this->getTotal()->getAddress()->getInitialMixmatchTotal() : $this->getTotal()->getAddress()->getGrandTotal() ;
        $excl = max($excl, 0);
        return $excl;

    }
}
