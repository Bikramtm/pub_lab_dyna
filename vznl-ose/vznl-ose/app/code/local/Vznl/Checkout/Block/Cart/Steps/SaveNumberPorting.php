<?php
/**  */
class Vznl_Checkout_Block_Cart_Steps_SaveNumberPorting extends Vznl_Checkout_Block_Cart
{
    protected $_editedPackagesIds = array();
    protected $_disabledPackages = array();
    protected $_orderEditId = null;
    protected $_superorder = null;
    protected $_isCrossStore = null;

    public function __construct()
    {
        if ($this->_isOrderEdit()) {
            // TODO: get this values from parent block instead of registry
            $this->_editedPackagesIds = Mage::registry('editedPackages');
            $this->_disabledPackages = Mage::registry('disabledPackages');
        }

        parent::__construct();
    }

    /**
     * Verify if order edit is active
     *
     * @return int|false (Returns ID of the superOrder if order edit is active)
     */
    protected function _isOrderEdit()
    {
        if (is_null($this->_orderEditId)) {
            $this->_orderEditId = $this->getQuote() && $this->getQuote()->getSuperOrderEditId() ? $this->getQuote()->getSuperOrderEditId() : false;
        }
        return $this->_orderEditId;
    }

    /**
     * Override the default cartPackages with additional info needed for the number porting template
     *
     * @return array|null
     */
    public function getCartPackages()
    {
        $cartPackages = array();
        $packages = $this->getPackages();
        $cartHelper = Mage::helper('dyna_configurator/cart');

        if ($packages) {
            foreach ($packages as $packageId => $package) {
                if (Mage::helper('dyna_catalog')->packageContainsVfDaSubscription($package)) {
                    $packageItems = '';
                    $ctns = $this->getQuote()->getPackageCtns($packageId);
                    $hasDataSubscription = false;
                    if($package['items']) {
                        $package['items'] = $cartHelper->getSortedCartProducts($package['items']);
                    }
                    foreach ($package['items'] as $item) {
                        $packageItems .= $item->getName() . ', ';
                        $product = $item->getProduct();
                        if($product->isSubscription()) {
                            $imageUrl = $item->getProduct()->getImageUrl();
                            $subscription_name = $item->getName();
                        }
                        if($product->isDataSubscription()){
                            $hasDataSubscription = true;
                        }
                    }

                    // NumberPorting not applicable if package already contains CTNs!
                    // Neither if subscription is a data subscription
                    if (!empty($ctns) || $hasDataSubscription) {
                        continue;
                    }

                    $cartPackages[$packageId] = array(
                        'name' => Mage::helper('dyna_configurator')->getPackageTitle($ctns, $package['type']),
                        'sale_type' => $package['sale_type'],
                        'subscription_name' => $subscription_name,
                        'items' => rtrim($packageItems, ', '),
                        'current_number' => $package['current_number'],
                        'connection_type' => $package['connection_type'],
                        'actual_porting_date' => $package['actual_porting_date'],
                        'number_porting_type' => $package['number_porting_type'],
                        'number_porting_validation_type' => isset($package['number_porting_validation_type']) ? $package['number_porting_validation_type'] : null,
                        'number_porting_client_id' => isset($package['number_porting_client_id']) ? $package['number_porting_client_id'] : null,
                        'image_url' => $imageUrl
                    );
                }
            } #endforeach
        } #endif

        ksort($cartPackages);

        return $cartPackages;
    }

    /**
     * Determine if number porting can apply for a specific package
     *
     * @return bool
     */
    public function isPackagePortingDisabled()
    {
        if ($this->_isOrderEdit()) {
            if (!$this->canAgentEdit()) {
                return true;
            }

            // Number porting edit is disabled in all scenarios
            $portingDisabled = true;
        } else {
            $portingDisabled = false;
        }

        return $portingDisabled;
    }

    /**
     * Retrieve superorder, only on order edit!
     *
     * @return null
     */
    protected function getSuperorder()
    {
        if ($this->_superorder) {
            return $this->_superorder;
        }

        if ($this->_isOrderEdit()) {
            $this->_superorder = Mage::getModel('superorder/superorder')->load($this->getQuote()->getSuperOrderEditId());
        }

        return $this->_superorder;
    }

    protected function getIsCrossStore()
    {
        if (!isset($this->_isCrossStore)) {
            $this->_isCrossStore = $this->getCustomerSession()->getOrderEdit() && ($this->getSuperOrder()->getCreatedWebsiteId() != Mage::app()->getWebsite()->getId());
        }

        return $this->_isCrossStore;
    }
}
