<?php

/**
 * Class Vznl_Checkout_Block_Tax_Tax
 */
class Vznl_Checkout_Block_Tax_Tax extends Mage_Tax_Block_Checkout_Tax
{
    public function getDeviceTax()
    {
        return $this->getTotal()->getAddress()->getInitialMixmatchTax() != null ? $this->getTotal()->getAddress()->getInitialMixmatchTax() : $this->getTotal()->getAddress()->getTaxAmount();
    }
}
