<?php

class Vznl_Checkout_Block_Adminhtml_Activationreason_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('activation_reason');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('operator')->__('Item Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('vznl_checkout')->__('Item Information'),
            'title' => Mage::helper('vznl_checkout')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('vznl_checkout/adminhtml_activationreason_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
