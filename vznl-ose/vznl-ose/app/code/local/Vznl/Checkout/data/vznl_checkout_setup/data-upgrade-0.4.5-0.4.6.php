<?php
/**
 * Installer that add peal_outbond_flag
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

// Add peal_outbond_flag on sales quote resource
$this->getConnection()->addColumn($this->getTable("sales/quote"), "peal_outbond_flag", [
    "type" => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    "comment" => "Peal Outbond Flag",
]);

// Add peal_outbond_flag on sales order resource
$this->getConnection()->addColumn($this->getTable("sales/order"), "peal_outbond_flag", [
    "type" => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    "comment" => "Peal Outbond Flag",
]);

$this->endSetup();