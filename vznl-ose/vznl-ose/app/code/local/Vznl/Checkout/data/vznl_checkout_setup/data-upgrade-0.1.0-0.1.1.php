<?php
/* @var $installer Mage_Checkout_Model_Resource_Setup */
$installer = new Mage_Checkout_Model_Resource_Setup('core_setup');
$installer->startSetup();

$installer->run("UPDATE core_config_data SET value = 0 WHERE (path like '%/handling_fee' and value is NULL) or (path like '%/handling_fee' and value = '')");

$installer->endSetup();
