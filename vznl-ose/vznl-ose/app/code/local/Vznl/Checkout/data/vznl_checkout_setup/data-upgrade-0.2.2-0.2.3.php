<?php
/* @var $installer Mage_Checkout_Model_Resource_Setup */
$installer = new Mage_Checkout_Model_Resource_Setup('core_setup');
$installer->startSetup();

$installer->run("INSERT INTO templateversion (path, type, start_date) VALUES ('checkout/cart/pdf/warranty/warranty_5.phtml','garant', now())");

$installer->endSetup();
