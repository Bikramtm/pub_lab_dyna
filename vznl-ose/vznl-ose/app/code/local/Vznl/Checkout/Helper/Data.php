<?php

/**
 * Class Data
 */
class Vznl_Checkout_Helper_Data extends Dyna_Checkout_Helper_Data
{
    const XML_PATH_EMAIL_ADMIN_QUOTE_NOTIFICATION = 'customer/quote_email/exist_user_quote_template';
    const PHONE_NUMBER_CONTRACT_PATH_BUSINESS = 'general/store_information/contract_phone';
    const PHONE_NUMBER_CONTRACT_PATH_CONSUMER = 'general/store_information/contract_phone_consumer';
    const XML_PATH_SALES_GENERAL_PATH = 'sales/general';
    const SHOW_ONLY_ONE_PACKAGE = false; // Limitation for alpha shops
    const HARDWARE_ONLY_SWAP = false;// Limitation for alpha shops
    const BILLING_ADDRESS_OPTION = [
        'same_as_service_address' => 'Same as service address',
        'another_address' => 'Another address'
    ];
    const DELIVERY_METHOD_DIRECT = "direct";
    const SALES_HINT = 'sales_hint';
    const FIXED_METHOD_LSP = 'LSP';
    /** @var Vznl_Configurator_Model_Cache */
    protected $_cache;

    /**
     * Returns true if quote has at least one completed mobile subscription
     *
     * @return bool
     */
    public function checkOneCompletedPackage()
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $packages = $conn->fetchAll(
            Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
                ->addFieldToFilter('current_status',
                    Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
                ->getSelect()
        );
        $allPackages = $conn->fetchAll(
            Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id',
                    $this->_getQuote()->getId())
                ->getSelect()
        );
        $firstPackage = current($allPackages);

        return $firstPackage['one_of_deal']
            ? count($allPackages) == count($packages)
            : (bool)count($packages);
    }

    /**
     * Returns maximum price for order from config
     * @return int
     */
    public function getMaxPriceForOrder()
    {
        $key = md5(__METHOD__);
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = (float)Mage::getStoreConfig(sprintf('%s/max_price',
                self::XML_PATH_SALES_GENERAL_PATH));
            $this->getCache()->save(serialize($result),
                $key,
                array(Vznl_Configurator_Model_Cache::CACHE_TAG),
                $this->getCache()->getTtl());
            return $result;
        }
    }

    /**
     * Return Super Order Offset
     * @return int
     */
    public function getSuperOrderOffset()
    {
        $key = md5(__METHOD__);
        if (false !== ($result = $this->getCache()->load($key))) {
            return unserialize($result);
        } else {
            $result = (int)Mage::getStoreConfig(sprintf('%s/superorder_offset',
                self::XML_PATH_SALES_GENERAL_PATH));
            $this->getCache()->save(serialize($result),
                $key,
                array(Vznl_Configurator_Model_Cache::CACHE_TAG),
                $this->getCache()->getTtl());
            return $result;
        }
    }

    /**
     * Converts a long iban to short iban as provided in omnitracker DF-003734
     *
     * @param $iban
     * @return string
     */
    public function longIbanToShort($iban)
    {
        return str_pad(ltrim(substr($iban, -10),
            '0'),
            7,
            '0',
            STR_PAD_LEFT);
    }

    /**
     * Checks if an order change can be submitted.
     * Currently it checks the following:
     * - If an order is fulfilled
     * - If at least one package has changes
     *
     * <NOTE:> If no super order is found, it will return true
     *
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param int $packageId The package id to check, if <null>  check all packages.
     * @return bool
     */
    public function canSubmitChange(Vznl_Checkout_Model_Sales_Quote $quote, $packageId = null): bool
    {
        /** @var Vznl_Checkout_Model_Sales_Quote $superQuote */
        if ($quote->getSuperQuoteId()) {
            $superQuote = Mage::getModel('sales/quote')->load($quote->getSuperQuoteId());
        } else {
            $superQuote = $quote;
        }

        $superOrderId = $superQuote->getSuperOrderEditId();
        // If change
        if ($superOrderId) {
            /** @var Vznl_Superorder_Model_Superorder $superOrder */
            $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId, 'entity_id');
            // If status is not Validation Partially Approved the order can only be changed if it contains changes
            if (!in_array($superOrder->getOrderStatus(),
                [Vznl_Superorder_Model_Superorder::SO_VALIDATED_PARTIALLY])) {
                foreach ($superQuote->getListOfModifiedQuotes($superOrderId) as $quoteToHandle) {
                    if (Mage::helper('vznl_package')->containsChanges($quoteToHandle,
                        $superOrder,
                        $packageId)) {
                        return true;
                    }
                }
                return Mage::helper('vznl_package')->containsChanges($quote,
                    $superOrder,
                    $packageId);
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function canCompleteOrder()
    {
        $currentQuote = $this->_getQuote();
        $quoteData = $currentQuote->getData();

        $maxPrice = $this->getMaxPriceForOrder();
        if (!isset($quoteData['grand_total'])) $quoteData['grand_total'] = 0;
        $cartTotal = (float)$quoteData['grand_total'];

        return ($this->checkOneCompletedPackage()) && ($cartTotal <= $maxPrice);
    }

    public function sendNotificationEmail($to, $vars = array(),
                                          $templateConfigPath = self::XML_PATH_EMAIL_ADMIN_QUOTE_NOTIFICATION)
    {
        if (!$to) {
            return;
        }

        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);
        $mailTemplate = Mage::getModel('core/email_template');
        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $template = Mage::getStoreConfig($templateConfigPath,
            Mage::app()->getStore()->getId());
        $mailTemplate->sendTransactional(
            $template,
            Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_IDENTITY, Mage::app()->getStore()->getId()),
            $to['email'],
            $to['name'],
            $vars
        );
        $translate->setTranslateInline(true);
        return $this;
    }

    /**
     * @param null $productsIds
     * @param null $websiteId
     * @param null $packageType
     * @return array
     */
    public function getUpdatedPrices($productsIds = null,
                                     $websiteId = null,
                                     $salesType = null,
                                     $packageType = null)
    {
        /** @var Dyna_Checkout_Helper_Tax $_taxHelper */
        $_taxHelper = Mage::helper('tax');
        $_store = Mage::app()->getStore();

        //if website is provided, get the store of the website
        if ($websiteId) {
            $_store = Mage::app()->getWebsite($websiteId)->getDefaultStore();
        }

        if ($packageType == null) {
            $packageType = 0;
        }

        $types = array();
        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product',
            Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
        $types[] = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute,
            null,
            Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION);
        $types[] = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute,
            null,
            Vznl_Catalog_Model_Type::SUBTYPE_DEVICE);
        $types[] = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute,
            null,
            Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION);
        $products = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR,
                array('in' => array($types)))
            ->addWebsiteFilter($websiteId)
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('maf')
            ->addAttributeToSelect('regular_maf')
            ->addTaxPercents()
            ->addAttributeToSelect('tax_class_id')
            ->load();

        /**
         * We must extract the subscription from the package
         * with the provided package ID
         */
        $subscription = null;
        $deviceRCSku = null;
        $device = null;

        // check selected products
        $selectedProducts = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('tax_class_id')
            ->addAttributeToFilter('entity_id', array('in' => $productsIds))
            ->load();
        foreach ($selectedProducts as $product) {
            if (!$subscription &&
                Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION),
                    $product)
            ) {
                $subscription = $product;
            } elseif (!$device &&
                Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE),
                    $product)
            ) {
                $device = $product;
            } elseif (!$deviceRCSku &&
                Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION),
                    $product)
            ) {
                $deviceRCSku = $product->getSku();
            }

            if ($subscription && $device && $deviceRCSku) {
                break;
            }
        }

        $mixMatches = $this->getMixMatchPrices(
            $subscription ? $subscription->getSku() : null,
            $device ? $device->getSku() : null,
            $deviceRCSku ? $deviceRCSku : null,
            $websiteId,
            $salesType
        );

        $prices = array();
        /** @var Mage_Catalog_Model_Product $product */
        foreach ($products->getItems() as $product) {
            // check if product is device
            $is_device = Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE),
                $product);

            $priceStart = $_store->roundPrice($_store->convertPrice($product->getPrice()));
            $price = $_taxHelper->getPrice($product, $priceStart, false);
            $priceWithTax = $_taxHelper->getPrice($product, $priceStart, true);

            $mafStart = $product->getData('maf') != null ? $product->getData('maf') : 0;
            $mafStart = $_store->roundPrice($_store->convertPrice($mafStart));
            $maf = $_taxHelper->getPrice($product, $mafStart, false);
            $mafWithTax = $_taxHelper->getPrice($product, $mafStart, true);

            if (isset($mixMatches[$product->getSku()])) {
                $priceStart = $_store->roundPrice($_store->convertPrice($mixMatches[$product->getSku()]));
                $priceWithTax = $_taxHelper->getPrice($product, $priceStart, true);
                if ($product->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                    $price = $_taxHelper->getPrice($device, $priceStart, false);
                } else {
                    $price = $_taxHelper->getPrice($product, $priceStart, false);
                }

            } elseif ($is_device) {
                //if change, also change in the _afterSave event of the sales order as same formula is applied
                if ($subscription && $subscription->hasSac() && $product->hasPurchasePrice()) {
                    $priceStart = $_store->roundPrice($_store->convertPrice($this->getPricing()
                        ->calculatePrice($product->getPurchasePrice(),
                            $subscription->getSac(),
                            $product->getTaxRate())));
                    $price = $_taxHelper->getPrice($product, $priceStart, false);
                    $priceWithTax = $_taxHelper->getPrice($product, $priceStart, true);
                }
            } else {
                $price = null;
                $priceWithTax = null;
            }

            $prices[$product->getId()]['rel_id'] = null;
            if ($is_device && $subscription) {
                $prices[$product->getId()]['rel_id'] = (int)$subscription->getId();
            } elseif (!$is_device && $device) {
                $prices[$product->getId()]['rel_id'] = (int)$device->getId();
            }

            $prices[$product->getId()]['price'] = $price;
            $prices[$product->getId()]['price_with_tax'] = $priceWithTax;

            if (!$is_device) {
                $prices[$product->getId()]['maf'] = $maf;
                $prices[$product->getId()]['regular_maf'] = $maf;
                $prices[$product->getId()]['maf_with_tax'] = $mafWithTax;
            }
        }

        return $prices;
    }

    // Check if the quote has a business or a personal contract
    public function checkIsBusinessCustomer($quote = null)
    {
        if (!$quote) {
            $quote = $this->_getQuote();
        }
        if ($quote->getCustomer()->getId()) {
            $customer = $quote->getCustomer();
            return (bool)$customer->getIsBusiness();
        } else {
            $customer = null;
            return (bool)$quote->getCustomerIsBusiness();
        }
    }

    // Check if the quote is retention only
    public function checkIsRetentionOnly($quote = null)
    {
        return $this->checkIsSalesTypeOnly(Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE,
            $quote);
    }

    // Check if the quote is acquisition only
    public function checkIsAcquisitionOnly($quote = null)
    {
        return $this->checkIsSalesTypeOnly(Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE,
            $quote);
    }

    /**
     * @param string $salesType
     * @param Mage_Sales_Model_Quote|null $quote
     * @return bool
     */
    private function checkIsSalesTypeOnly($salesType, $quote = null)
    {
        if (!$quote) {
            $quote = $this->_getQuote();
        }
        $isSalesType = true;
        foreach ($quote->getPackagesInfo() as $package) {
            // Check if all packages are the given type
            if ($package['sale_type'] != $salesType) {
                $isSalesType = false;
            }
        }

        return $isSalesType;
    }

    /**
     * @param array $packages
     * @return array
     */
    public function containsNewNetherlands($packages)
    {
        $return = array();
        foreach ($packages as $packageId => $package) {
            foreach ($package['items'] as $item) {
                if (isset($return[$item->getPackageId()])) {
                    continue;
                }
                /** @var Dyna_Catalog_Model_Product $product */
                $product = $item->getProduct();
                if ($product->isHollandsNieuweNetworkProduct()) {
                    $return[$item->getPackageId()] = true;
                }
            }
        }

        return $return;
    }

    // Check if the quote (or package) has mobile only items
    public function checkHasMobileOnly($quote = null, $packageId = null)
    {
        return $this->checkHasTypeOnly(Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE,
            $quote,
            $packageId);
    }

    // Check if the quote (or package) has hardware only items
    public function checkHasHardwareOnly($quote = null, $packageId = null)
    {
        return $this->checkHasTypeOnly(Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE,
            $quote,
            $packageId);
    }

    // Check if the quote (or package) has prepaid only items
    public function checkHasPrepaidOnly($quote = null, $packageId = null)
    {
        return $this->checkHasTypeOnly(Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID,
            $quote,
            $packageId);
    }

    /**
     * @param string $type One of the Omnius_Configurator_Model_PackageType package types.
     * @param null $quote The quote or null.
     * @param null $packageId The package id to search for or null.
     * @return array The array with package ids with the given type.
     */
    private function checkHasTypeOnly($type, $quote = null, $packageId = null)
    {
        if (!$quote) {
            $quote = $this->_getQuote();
        }
        $return = array();
        foreach ($quote->getPackagesInfo() as $package) {
            if ($packageId) {
                if ($packageId != $package['package_id']) {
                    continue;
                }
            }

            // Check if any of the packages are prepaid
            if ($package['type'] == $type) {
                $return[$package['package_id']] = true;
            }
        }

        return $return;
    }

    /**
     * Call the dealer adapter and retrieve a list of all available numbers
     *
     * @param string $lastCtn
     * @throws Exception
     * @return array
     */
    public function getAllAvailablePhoneNumbers($lastCtn = null)
    {
        $result = array();

        /** @var Omnius_Service_Helper_Data $h */
        $h = Mage::helper('omnius_service');

        /** @var Omnius_Service_Model_Client_DealerAdapterClient $client */
        $client = $h->getClient('dealer_adapter');
        // In override mode, do not make the DealerAdapter call, return data from stub files
        $client->setUseStubs(Mage::helper('omnius_service')->isOverride());

        try {
            $customer = $this->_getCustomerSession()->getCustomer();

            $ret = $client->getAvailableResource('MSISDN',
                $lastCtn,
                null,
                $customer->getCustomerLabel());
            foreach ($ret['party_end_user']['logical_resource'] as $number) {
                $result[] = $number['logical_res_phone_number'];
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $client->setUseStubs(false);

            throw $e;
        }

        $client->setUseStubs(false);

        return $result;
    }

    public function formatCtn($ctn, $useSpaces = true)
    {
        $ctn = trim($ctn);
        $ctn = str_replace(' ', '', $ctn);
        $ctn = preg_replace('/^31(\d*)/', '0$1', $ctn);

        $rootIndent = 2;
        if (substr($ctn, 0, 3) === '097') {
            $rootIndent = 3;
        }

        $reversed = strrev($ctn);
        $reversedLen = strlen($reversed);
        $formatted = '';
        for ($i = 0; $i < $reversedLen ; $i++) {
            if ($useSpaces
                && (($reversedLen - $i >= $rootIndent)
                    && ($i % 2 === 0 && $i > 0))) {
                $formatted .= ' ';
            }
            $formatted .= $reversed[$i];
        }
        return strrev($formatted);
    }

    /**
     * @param null $packageId
     * @return string
     */
    public function checkPackageStatus($packageId = null)
    {
        $packageId = $packageId ?: $this->_getQuote()->getActivePackageId();
        $packageItems = $this->_getQuote()->getPackageItems($packageId);
        $sections = [];
        $productsArray = [];
        foreach ($packageItems as $pI) {
            // Only account for the actual products, not simcards or promos
            $possibleTypes = [
                Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY,
                Vznl_Catalog_Model_Type::SUBTYPE_DEVICE,
                Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION,
                Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                Vznl_Catalog_Model_Type::SUBTYPE_ADDON,
            ];
            if (array_intersect($possibleTypes, $pI->getProduct()->getType())) {
                $productsArray[] = $pI->getProductId();
            }
        }

        $mandatoryFamilies = Mage::helper('dyna_configurator/cart')->getMandatory($productsArray,
            null,
            Vznl_Catalog_Model_Type::TYPE_MOBILE);

        if (!Mage::getSingleton('customer/session')->getOrderEditMode()
            && !empty($mandatoryFamilies)) {
            $addons = [];
            foreach ($packageItems as $item) {
                if ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_ADDON)) {
                    $addons[] = $item->getProductId();
                }
            }

            if (empty($addons)) {
                $sections['addon'] = $this->__('The package must contain one of the mandatory addons.');
            }

            foreach ($mandatoryFamilies as $family) {
                $collectionIds = Mage::getModel('catalog/category')->getProductIds($family);
                if (!empty($collectionIds) && empty(array_intersect($addons, $collectionIds))) {
                    $sections['addon'] = $this->__('The package must contain one of the mandatory addons.');
                }
            }
        }
        /** @var Vznl_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getPackages(null,
            $this->_getQuote()->getId(),
            $packageId)->getFirstItem();

        /**
         * Check whether the package contains a deleted item that was set as DOA.
         */
        /** @var Mage_Checkout_Model_Session $checkoutSession */
        $checkoutSession = Mage::getSingleton('checkout/session');
        $doaItems = $checkoutSession->getCancelDoaItems() ?: [];
        $flippedDoaItems = array_flip($doaItems);
        if (isset($doaItems[$packageId])) {
            $doaItems = unserialize($doaItems[$packageId]);
            /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
            foreach ($packageItems as $item) {
                $tempSku = explode('|', $item->getSku());
                if (!empty($doaItems) && isset($flippedDoaItems[$tempSku[0]]) && $item->getProduct()->isEndOfLife()) {
                    $sections[strtolower($item->getProduct()->getAttributeText('identifier_package_subtype'))] = $this->__('The package cannot contain an item that was set as DOA and was deleted.');
                }
            }
        }

        $packageType = $packageModel->getType();

        if (($packageType != Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_FIXED)
            && (Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE)) {
            // on indirect, check if at least a subscription exists
            // check if at least one subscription is found
            $subscription = array_filter($packageItems,
                function (Mage_Sales_Model_Quote_Item $item) {
                if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                    $item->getProduct()->getType())) {
                    return $item;
                }

                return false;
            });

            $subscription = count($subscription) ? reset($subscription) : null;

            //if no subscription is found, return false
            if (!$subscription) {
                $sections['tariff'] = $this->__('A mobile package that does not contain a SIM Only subscription must have one regular subscription with a device attached.');
            }

            if ($subscription && $subscription->getProduct()->getAikido()) {
                $deviceSubscription = array_filter($packageItems, function (Mage_Sales_Model_Quote_Item $item) {
                    if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION,
                        $item->getProduct()->getType())) {
                        return $item;
                    }

                    return false;
                });

                if (!count($deviceSubscription)) {
                    $sections['devicerc'] = $this->__('You need to select a device subscription to continue to the checkout section.');
                }
            }
        } else {
            $device = array_filter($packageItems, function (Mage_Sales_Model_Quote_Item $item) {
                if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE,
                    $item->getProduct()->getType())) {
                    return $item;
                }

                return false;
            });
            $device = count($device)
                ? reset($device)
                : null;

            $accessory = array_filter($packageItems, function (Mage_Sales_Model_Quote_Item $item) {
                if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY,
                    $item->getProduct()->getType())) {
                    return $item;
                }

                return false;
            });
            $accessory = count($accessory) ? reset($accessory) : null;

            if ($packageType == Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE) {
                $productStatus = false;

                $subscription = array_filter($packageItems, function (Mage_Sales_Model_Quote_Item $item) {
                    if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                        $item->getProduct()->getType())) {
                        return $item;
                    }

                    return false;
                });
                $subscription = count($subscription) ? reset($subscription) : null;

                if ($subscription) {
                    $subscriptionProduct = Mage::getModel('catalog/product')->load($subscription->getProduct()->getId());
                    $isAikido = $subscriptionProduct->getAikido();
                    $productStatus = $subscriptionProduct->getSimOnly() || $isAikido;

                    $deviceSubscription = array_filter($packageItems, function (Mage_Sales_Model_Quote_Item $item) {
                        if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION,
                            $item->getProduct()->getType())) {
                            return $item;
                        }

                        return false;
                    });
                    $deviceSubscription = count($deviceSubscription) ? reset($deviceSubscription) : null;

                    if ($subscription && $device && $isAikido && !$deviceSubscription) {
                        $sections['devicerc'] = $this->__('You need to select a device subscription to continue to the checkout section.');
                    }
                }
                if (!$subscription) {
                    $sections['tariff'] = $this->__('A mobile package that does not contain a SIM Only subscription must have one regular subscription with a device attached.');
                }

                // If the subscription is Aikido or simonly
                if ($subscription && isset($subscriptionProduct) && $productStatus) {
                    // There must be a device or a sim
                    $sim = array_filter($packageItems, function (Mage_Sales_Model_Quote_Item $item) {
                        if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD, $item->getProduct()->getType())) {
                            return $item;
                        }

                        return false;
                    });
                    $sim = (count($sim) || $packageModel->getOldSim());

                    if (!$device && !$sim) {
                        $sections['sim-select'] = $this->__('A mobile package contains a SIM Only or Aikido subscription without sim');
                    }
                } elseif ($subscription && !$device) {
                    $sections['device'] = $this->__('A mobile package that does not contain a SIM Only subscription must have one regular subscription with a device attached.');
                }
            } elseif ($packageType == Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE) {
                if (!$device && !$accessory) {
                    $sections['device'] = $this->__('A hardware package must contain a device or an accessory.');
                    $sections['accessoire'] = $this->__('A hardware package must contain a device or an accessory.');
                }
            } elseif ($packageType == Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID) {
                if (!$device) {
                    $sections['device'] = $this->__('A prepaid package must contain a device.');
                }
            }
        }

        return $sections ?: Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED;
    }

    /**
     * Return warnings array if packages are not complete
     *
     * @return array
     */
    public function getCheckPackageStatusWarnings()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('checkout/session')->getQuote();
        $packages = $quote->getCartPackages(true);
        $warnings = [];
        foreach ($packages as $packageModel) {
            $packageId = $packageModel->getPackageId();
            if (!empty($packageId)) {
                $status = $this->checkPackageStatus($packageId);
                if (is_array($status)) {
                    $warnings[$packageId] = $status;
                }
            }
        }
        return $warnings;
    }

    /**
     * @param null $subscriptionSku
     * @param null $deviceSku
     * @param null $deviceRC
     * @param null $websiteId
     * @param null $salesType
     * @return array|mixed
     */
    public function getMixMatchPrices(
        $subscriptionSku = null,
        $deviceSku = null,
        $deviceRC = null,
        $websiteId = null,
        $salesType = null
    )
    {
        $key = serialize(array(__METHOD__,
            $subscriptionSku,
            $deviceSku,
            $deviceRC,
            $websiteId,
            $salesType));

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
            $result = array();
            $aikidoHelper = Mage::helper('vznl_checkout/aikido');
            $backupRC = $salesType == Dyna_Catalog_Model_ProcessContext::ACQ
                ? $aikidoHelper->getDefaultAcquisitionSku()
                : $aikidoHelper->getDefaultRetentionSku();

            $subscriptionModel = Mage::getModel('catalog/product')->loadByAttribute('sku', $subscriptionSku);
            $isAikido = $subscriptionModel && !$subscriptionModel->getAikido() ? false : true;

            if ($subscriptionSku) {
                /** @var Dyna_MixMatch_Model_Resource_PriceIndex_Collection $mixMatches */
                $mixMatches = Mage::getResourceModel('dyna_mixmatch/priceIndex_collection')
                    ->addFieldToSelect(array('source_sku', 'target_sku', 'device_subscription_sku', 'price'))
                    ->addFieldToFilter('website_id', $websiteId ? $websiteId : Mage::app()->getWebsite()->getId())
                    ->addFieldToFilter('source_sku', $subscriptionSku);

                $mixMatches = $adapter->fetchAll($mixMatches->getSelect());

                foreach ($mixMatches as $mixMatch) {
                    if (
                        ($deviceRC != null && $mixMatch['device_subscription_sku'] == $deviceRC)
                        || ($deviceRC == null && $mixMatch['device_subscription_sku'] == $backupRC)
                        || !$isAikido
                    ) {
                        $result[$mixMatch['target_sku']] = $mixMatch['price'];
                        if ($deviceSku != null && $mixMatch['target_sku'] == $deviceSku) {
                            $result[$mixMatch['device_subscription_sku']] = $mixMatch['price'];
                        }
                    }
                }
            }

            if ($deviceSku) {
                /** @var Dyna_MixMatch_Model_Resource_PriceIndex_Collection $mixMatches */
                $deviceMixMatches = Mage::getResourceModel('dyna_mixmatch/priceIndex_collection')
                    ->addFieldToSelect(array('source_sku', 'device_subscription_sku', 'price'))
                    ->addFieldToFilter('website_id', $websiteId ? $websiteId : Mage::app()->getWebsite()->getId())
                    ->addFieldToFilter('target_sku', $deviceSku);

                $deviceMixMatches = $adapter->fetchAll($deviceMixMatches->getSelect());
                foreach ($deviceMixMatches as $deviceMixMatch) {
                    if (
                        ($deviceRC != null && $deviceMixMatch['device_subscription_sku'] == $deviceRC)
                        || ($deviceRC == null && $deviceMixMatch['device_subscription_sku'] == $backupRC)
                        || !$isAikido
                    ) {
                        $result[$deviceMixMatch['source_sku']] = $deviceMixMatch['price'];
                        if (($subscriptionSku != null && $deviceMixMatch['source_sku'] == $subscriptionSku)) {
                            $result[$deviceMixMatch['device_subscription_sku']] = $deviceMixMatch['price'];
                        }
                    }
                }
            }

            if ($subscriptionSku && $deviceSku && $isAikido) {
                /** @var Dyna_MixMatch_Model_Resource_PriceIndex_Collection $mixMatches */
                $deviceRcMixMatches = Mage::getResourceModel('dyna_mixmatch/priceIndex_collection')
                    ->addFieldToSelect(array('source_sku', 'device_subscription_sku', 'price'))
                    ->addFieldToFilter('website_id', $websiteId ? $websiteId : Mage::app()->getWebsite()->getId())
                    ->addFieldToFilter('target_sku', $deviceSku)
                    ->addFieldToFilter('source_sku', $subscriptionSku);

                $deviceRcMixMatches = $adapter->fetchAll($deviceRcMixMatches->getSelect());
                foreach ($deviceRcMixMatches as $deviceRcMixMatch) {
                    $result[$deviceRcMixMatch['device_subscription_sku']] = $deviceRcMixMatch['price'];
                }
            }

            $this->getCache()->save(serialize($result),
                $key,
                array(Vznl_Configurator_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @return Dyna_MixMatch_Model_Pricing
     */
    protected function getPricing()
    {
        return Mage::getSingleton('dyna_mixmatch/pricing');
    }

    /**
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    protected function _getQuote()
    {
        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    /**
     * @return Vznl_Configurator_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('vznl_configurator/cache');
        }
        return $this->_cache;
    }

    public function getQuoteIncrementPrefix()
    {
        return Mage::getStoreConfig('sales/general/quote_increment_prefix')
            ? Mage::getStoreConfig('sales/general/quote_increment_prefix')
            : '';
    }

    public function getOfferLifetime()
    {
        return Mage::getStoreConfig('sales/general/offer_lifetime')
            ? Mage::getStoreConfig('sales/general/offer_lifetime')
            : 0;
    }

    public function createNewQuote($actualQuote = null)
    {
        $cart = Mage::getSingleton('checkout/cart');
        if (!$actualQuote) {
            $actualQuote = $cart->getQuote();
        }

        if ($actualQuote->getId()) {
            $actualQuote->setIsActive(false)->save();
        }
        $quote = Mage::getModel('sales/quote');
        $this->_getCustomerSession()->unsNewEmail();
        $customer = $this->_getCustomerSession()->getCustomer();

        Mage::getSingleton('checkout/session')->clear();

        $quote->setIsActive(true)
            ->setStoreId(Mage::app()->getStore(true)->getId())
            ->setCustomer($customer)
            ->save();

        $cart->setQuote($quote);

        Mage::getSingleton('checkout/session')->replaceQuote($quote);
    }

    /**
     * Clone the given quote without keeping any customer related data (promos/ctns/addresses)
     *
     * @param Vznl_Checkout_Model_Sales_Quote $oldQuote
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    public function cloneQuoteWithoutCustomerData($oldQuote)
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $newQuote = Mage::getModel('sales/quote');
        $newQuote->setCustomer($this->_getCustomerSession()->getCustomer());
        $newQuote->save();

        $newPackages = array();
        //Check if there are any retention packages and remove them
        $packagesModelOld = Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('quote_id', $oldQuote->getId());
        $packagesModelOld = $conn->fetchAssoc($packagesModelOld->getSelect());
        foreach ($packagesModelOld as $packageModelOld) {
            if ($packageModelOld['sale_type'] != Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE
                && !$packageModelOld['ctn']
                && $packageModelOld['sale_type'] != Vznl_Checkout_Model_Sales_Quote_Item::INLIFE
            ) {
                $newPackages[] = $packageModelOld['package_id'];
                $packageModelNew = Mage::getModel('package/package');
                $packageModelNew
                    ->setData($packageModelOld)
                    ->setData('entity_id', null)
                    ->setQuoteId($newQuote->getId())
                    ->setOrderId(null)
                    ->save();
            }
        }
        unset($packagesModelOld);
        $flippedNewPackages = array_flip($newPackages);

        foreach ($oldQuote->getAllItems() as $oldItem) {
            $oldItemPackageId = $oldItem->getPackageId();
            if (isset($flippedNewPackages[$oldItemPackageId]) && $oldItem->getPackageType()) {
                $newItem = clone $oldItem;
                $newQuote = $newQuote->addItem($newItem);
            }
        }
        $oldQuote->setIsActive(false);
        $oldQuote->save();

        // Copy shipping data
        $newQuote->setShippingData($oldQuote->getShippingData());
        $cart = Mage::getSingleton('checkout/cart');
        $newQuote->setIsActive(true)
            ->setStoreId(Mage::app()->getStore(true)->getId())
            ->setCartStatusStep(Vznl_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CHECKOUT);

        $cart->setQuote($newQuote);
        Mage::getSingleton('checkout/session')->setQuoteId($cart->getQuote()->getId());

        return $newQuote;
    }

    public function cloneQuote($quoteId)
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $oldQuote = Mage::getModel('sales/quote')->load($quoteId);

        /** @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getResourceModel('core/transaction');

        $newQuote = Mage::getModel('sales/quote');
        $newQuote->setCustomer($oldQuote->getCustomer());

        foreach ($oldQuote->getAllItems() as $oldItem) {
            $newItem = clone $oldItem;
            $newItem->setItemId(null);
            $newQuote->addItem($newItem);
        }

        $oldQuote->setIsActive(false);
        $oldQuote->save();

        $newQuote->setIsActive(true)
            ->setDealerId($oldQuote->getDealerId())
            ->setAgentId($oldQuote->getAgentId())
            ->setStoreId($oldQuote->getStoreId())
            ->setCartStatusStep(Vznl_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CHECKOUT)
            ->setQuoteParentId($oldQuote->getId())
            ->save();

        $packagesModelOld = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $oldQuote->getId());
        $packagesModelOld = $conn->fetchAssoc($packagesModelOld->getSelect());
        foreach ($packagesModelOld as $packageModelOld) {
            $packageModelNew = Mage::getModel('package/package');
            $packageModelNew
                ->setData($packageModelOld)
                ->setQuoteId($newQuote->getId())
                ->setOrderId(null)
                ->save();
        }
        unset($packagesModelOld);

        // clone shipping address
        /**@var Vznl_Checkout_Model_Sales_Quote_Address $address */
        foreach ($oldQuote->getAllAddresses() as $address) {
            $newAddress = Mage::getModel('sales/quote_address');
            $newAddress->setData($address->getData());
            $newAddress->unsetData('address_id');
            $newAddress->setQuoteId($newQuote->getId());
            $transaction->addObject($newAddress);
        }

        // Copy shipping data
        $newQuote->setShippingData($oldQuote->getShippingData());

        try {
            $transaction->save();
        } catch (Exception $e) {
            $newQuote->delete();
            throw new \RuntimeException('Cannot clone quote');
        }

        $newQuote->setTotalsCollectedFlag(false)
            ->save();

        return $newQuote;
    }

    /**
     * @param $request Mage_Core_Controller_Request_Http
     * @throws Exception
     */
    public function processNotesAndPickup($request)
    {
        $manualPickup = $request->get('manual_pickup');
        $manualPickupRemarks = $request->get('manual_pickup_remarks');
        $creditNotes = $request->get('credit_note');
        $manualActivations = $request->get('manual_activation_reason');
        $manualActivationSimcards = $request->get('manual_activation_simcard');
        $manualActivationRemarks = $request->get('manual_activation_remarks');
        /** @var Dyna_Package_Model_Mysql4_Package $packageCollection */
        $packageCollection = Mage::getModel('package/package')->getPackages(null,
            Mage::getSingleton('checkout/session')->getQuote()->getId());

        /** @var Vznl_Package_Model_Package $package */
        foreach ($packageCollection as $package) {
            $packageId = $package->getPackageId();
            $package->setManualPickup(false);
            $package->setManualPickupRemarks(null);
            if (isset($manualPickup[$packageId])) {
                $package->setManualPickup(true);
                $package->setManualPickupRemarks($manualPickupRemarks[$packageId]);
            }

            $package->setData('manual_activation_reason', null);
            $package->setData('manual_activation_user_input', null);
            $package->setData('sim_number', null);
            if (!empty($manualActivations[$packageId])) {
                $package->setData('manual_activation_reason', $manualActivations[$packageId]);
                $package->setData('sim_number', $manualActivationSimcards[$packageId]);
                if (!empty($manualActivationRemarks[$packageId])) {
                    $package->setData('manual_activation_user_input', $manualActivationRemarks[$packageId]);
                }
            }

            $package->setCreditNote(null);
            if (isset($creditNotes[$packageId]) && !empty($creditNotes[$packageId])) {
                $package->setCreditNote($creditNotes[$packageId]);
            }

            $this->saveModelVz($package);
        }
    }

    /**
     * wrapper method to save a model to avoid code sniffer warning in FOR loops
     * TODO Move this method to a common utility class
     */
    public function saveModelVz(&$model)
    {
    	$model->save();
    }

    /**
     * Splits the quote by removing the not completed packages
     */
    public function splitQuote()
    {
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = $this->_getQuote();
        $notCompleted = array();

        /** @var Vznl_Package_Model_Package $packageModel */
        $packageModels = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId());

        // Check for incomplete packages
        foreach ($packageModels as $packageModel) {
            if ($packageModel->getCurrentStatus() != Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED) {
                $packageModel->delete();
                array_push($notCompleted, $packageModel->getPackageId());
            }
        }

        // Remove the incomplete packages items
        $flippedNotCompleted = array_flip($notCompleted);
        foreach ($quote->getAllItems() as $item) {
            $itemPackageId = $item->getPackageId();
            if (isset($flippedNotCompleted[$itemPackageId])) {
                $quote->removeItem($item->getId());
            }
        }

        // If items were removed recalculate totals
        if (count($notCompleted)) {
            $quote
                ->setTotalsCollectedFlag(false)
                ->collectTotals()
                ->save();

            Mage::helper('dyna_configurator/cart')->reindexPackagesIds($this->_getQuote());
        }
    }


    public function exitSuperOrderEdit($superOrderEditId = null)
    {
        Mage::getSingleton('customer/session')->setOrderLockInfo(null);
        /** @var Mage_Customer_Model_Session $customerSession */
        $customerSession = $this->_getCustomerSession();
        if ($customerSession->getOrderEdit()) {

            /**
             * Decrement sales rules that were not applied on the order.
             * @var Vznl_Checkout_Helper_Data $checkoutHelper
             */
            $checkoutHelper = Mage::helper('vznl_checkout');
            $checkoutHelper->decrementQuotesOnUnload();
            if (!$superOrderEditId) {
                /** @var Vznl_Checkout_Model_Sales_Quote $currentQuote */
                $currentQuote = Mage::getModel('sales/quote')->load($customerSession->getOrderEdit());
            } else {
                $currentQuote = Mage::getModel('sales/quote')->getListOfModifiedQuotes($superOrderEditId,
                    null)->getLastItem();
            }

            $modifiedQuotes = Mage::getModel('sales/quote')->getCollection()
                ->addFieldToFilter('super_quote_id', $currentQuote->getId());
            $currentQuote->setTotalsCollectedFlag(true)->setIsActive(0)->save();
            if ($modifiedQuotes) {
                foreach ($modifiedQuotes as $modifiedQuote) {
                    $modifiedQuote->setTotalsCollectedFlag(true)->setIsActive(0)->save();
                }
            }
        }
        /** @var Vznl_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        $customerSession->setOrderEdit(null);
        $customerSession->setOrderEditMode(false);
        $customerSession->setPackageDifferences(null);
        $customerSession->unsNewEmail();

        Mage::getSingleton('checkout/session')->setDeliveryChanged(false);
        Mage::getSingleton('checkout/session')->setIltChanged(false);
        Mage::getSingleton('checkout/session')->unsIncomeTestData();
        Mage::getSingleton('checkout/session')->unsCancelDoaItems();
        Mage::getSingleton('checkout/session')->unsDoaItems();
        Mage::getSingleton('checkout/session')->unsCancelRefundReason();

        $cart->exitEditMode();
    }

    /** @var Omnius_Service_Model_Client_AxiClient */
    protected static $client;

    protected static function getAxiClient()
    {
        if (!static::$client) {
            static::$client = Mage::helper('omnius_service')->getClient('axi');
        }
        return static::$client;
    }

    protected static function getLockManagerClient()
    {
        if (!static::$client) {
            static::$client = Mage::helper('vznl_service')->getClient('lock_manager');
        }
        return static::$client;
    }

    /**
     * Locks given super order to current logged in agent
     *
     * @param $superOrderId
     * @param null $agentId
     * @return bool
     */
    public function lockOrder($superOrderId, $agentId = null, $reason = 'Test Reason')
    {
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);

        $session = $this->_getCustomerSession();
        if (!$agentId) {
            $agent = $session->getAgent(true);
            if ($agent) {
                $agentId = $agent->getId();
            }
        }

        static::getLockManagerClient()->lockSaleOrder($superOrder, $agentId, $reason);

        // Lock order to current agent
        $lockInfo = Mage::getSingleton('customer/session')->getOrderLockInfo();
        if (isset($lockInfo[$superOrder->getId()])) {
            $lockInfo[$superOrder->getId()]['lock']['lock_state'] = "Locked";
            $lockInfo[$superOrder->getId()]['lock']['user_i_d'] = $session->getAgent(true)->getId();
            Mage::getSingleton('customer/session')->setOrderLockInfo($lockInfo);
        }

        return true;
    }

    /**
     * Releases order lock
     *
     * @param $superOrderId
     * @param null $agentId
     * @param bool $sendToAxi
     * @throws Exception
     */
    public function releaseLockOrder($superOrderId, $agentId = null, $sendToAxi = true)
    {
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);

        if ($sendToAxi && !$agentId) {
            $session = $this->_getCustomerSession();
            $agent = $session->getAgent(true);
            $agentId = $agent->getId();
        }

        if ($sendToAxi) {
            try {
                static::getLockManagerClient()->unLockSaleOrder($superOrder, $agentId);
            } catch (Exception $ex) {
                // Ignore the error, and log to figure out how it became out of sync??
                Mage::log($ex->getMessage(), null, 'debug_failed_unlocks.log');
            }
        }

        $lockInfo = Mage::getSingleton('customer/session')->getOrderLockInfo();
        if ($lockInfo && isset($lockInfo[$superOrder->getId()])) {
            $lockInfo[$superOrder->getId()]['lock']['lock_state'] = "Unlocked";
            $lockInfo[$superOrder->getId()]['lock']['user_i_d'] = '';
            Mage::getSingleton('customer/session')->setOrderLockInfo($lockInfo);
        }

    }

    /**
     * Checks if super order is locked or not
     *
     * @param $superOrderId
     * @param bool $redirect
     * @return mixed
     */
    public function checkOrder($superOrderId, $redirect = true)
    {
        if (Mage::helper('omnius_service')->isOverride()) {
            return true;
        }
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);
        $lockData = $orderLockInfo = Mage::getSingleton('customer/session')->getOrderLockInfo() ?: array();
        $lockData = isset($lockData[$superOrderId]) ? $lockData[$superOrderId] : null;

        $lockAgentId = isset($lockData['lock']['user_i_d']) && is_numeric($lockData['lock']['user_i_d']) ? $lockData['lock']['user_i_d'] : -1;
        $lockAgent = Mage::getModel('agent/agent')->load($lockAgentId);

        if ($lockAgentId = $lockAgent->getId()) {
            $session = $this->_getCustomerSession();
            $agent = $session->getAgent(true);
            if ($agent->getId() != $lockAgentId) {
                if ($redirect) {
                    $lockAgent = Mage::getModel('agent/agent')->load($lockAgentId);
                    $agentName = trim($lockAgent->getFirstName() . " " . $lockAgent->getLastName());
                    Mage::getSingleton('core/session')->addError(sprintf($this->__('The order is currently locked by: %s'), $agentName));
                    Mage::app()->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));

                    return false;
                }
        } else {
                return $lockAgentId;
            }
        }

        //if superorder was done on Indirect channel and we are now on a different website, don't allow edit
        if (Mage::app()->getWebsite()->getId() != $superOrder->getWebsiteId() &&
            Mage::app()->getWebsite($superOrder->getWebsiteId())->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE
        ) {
            if ($redirect) {
                Mage::getSingleton('core/session')->addError($this->__('Changing orders created in the indirect channel is not permitted from other channels'));
                Mage::app()->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));

               // exit;
            } else {
                return 'crossChannel';
            }
        }
        return false;
    }

    /**
     * @param $string
     * @param bool $capitalizeFirstCharacter
     * @return mixed
     */
    public function underscoreToCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace(' ',
            '',
            ucwords(str_replace('_', ' ', $string)));
        return $capitalizeFirstCharacter ? $str : lcfirst($str);
    }

    public function getTextValue($item = null)
    {
        return trim($item);
    }

    public function formatAddressAsHtml($address, $text = false)
    {
        $html = (isset($address['street'][0]))
            ? $this->getTextValue($address['street'][0])
            : '';
        $html .= (isset($address['street'][1]))
            ? ' ' . $this->getTextValue($address['street'][1])
            : '';
        $html .= (isset($address['street'][2]))
            ? ' ' . (strlen($this->getTextValue($address['street'][2])) > 3 ? "" : $this->getTextValue($address['street'][2]))
            : '';
        $html .= ($text) ? ', ' : '<br />';
        $html .= $this->getTextValue($address['postcode'])
            . ', ' . $this->getTextValue($address['city']);

        $html = trim($html);
        if (strip_tags($html) == ',') return '';

        return $html;
    }

    /**
     * @param $items
     * @return array
     *
     * Get a list of order items or quote item and return a list of product ids
     */
    public static function getOrderQuoteItemIds($items)
    {
        $return = [];
        foreach ($items as $item) {
            $return[$item->getProductId()] = $item->getItemDoa();
        }

        return $return;
    }

    /**
     * Saves the history of item prices to make sure we remember what changes were performed
     *
     * @param Vznl_Checkout_Model_Sales_Order|Vznl_Checkout_Model_Sales_Quote $orderOrQuote
     * @param bool $onlyItemId
     * @return string
     */
    public function setItemPriceHistory($orderOrQuote, $onlyItemId = false)
    {
        $alreadySet = true;
        foreach ($orderOrQuote->getAllItems() as $tmpItem) {
            $alreadySet = $alreadySet && (strlen(trim($tmpItem->getOrderItemPrices()) > 0));
        }
        if ($alreadySet) return;

        //gather all packages
        $packages = array();
        $packagePrices = array();
        foreach ($orderOrQuote->getAllItems() as $item) {
            $packages[$item->getPackageId()][] = $item;
            $packagePrices[$item->getId()] = array();
        }

        //loop through the items to get the subscription and device in order to find possible mixmatch
        foreach ($packages as $packageItems) {
            foreach ($packageItems as $item) {
                if ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)) {
                    $subscription = $item;
                } elseif ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)) {
                    /** @var Vznl_Checkout_Model_Sales_Order_Item $device */
                    $device = $item;
                } elseif ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                    /** @var Vznl_Checkout_Model_Sales_Order_Item $deviceRC */
                    $deviceRC = $item;
                }
            }

            if (isset($device) && isset($subscription)) {
                $mixMatchPrice = false;
                if (!$subscription->getProduct()->getAikido()) {
                    /** @var Dyna_MixMatch_Model_Resource_Price_Collection $mixMatches */
                    $mixMatches = $this->getMixMatches($device->getSku(),
                        $subscription->getSku(),
                        null,
                        true /* true return as array */,
                        $orderOrQuote->getStore()->getWebsiteId());
                    if (!empty($mixMatches)) {
                        $mixMatchPrice = $mixMatches[0]['price'];
                    }
                } else {
                    if (isset($deviceRC)) {
                        $mixMatchPrice = $device->getRowTotalInclTax() + $deviceRC->getTotalPaidByMaf();
                    } else {
                        Mage::log('Error: Impossible scenario.', null, 'device_rc_errors.log');
                    }
                }

                if ($mixMatchPrice !== false) {
                    $packagePrices[$device->getId()]['mixmatch'] = $mixMatchPrice;
                    $packagePrices[$device->getId()]['mixmatch_excl_tax'] = Mage::helper('tax')->getPrice($device->getProduct(), $mixMatchPrice, false);
                } else {
                    if ($subscription->getProduct()->hasSac() && $device->getProduct()->hasPurchasePrice()) {
                        //calculation based on the getUpdatedPrices from the Vznl_Checkout_Helper_Data helper
                        $priceStart = Mage::app()->getStore()->roundPrice(
                            Mage::app()->getStore()->convertPrice(
                                Mage::getSingleton('dyna_mixmatch/pricing')->calculatePrice(
                                    $device->getProduct()->getPurchasePrice(), $subscription->getSac(), $device->getProduct()->getTaxRate()
                                )
                            )
                        );
                        $priceWithTax = Mage::helper('tax')->getPrice($device->getProduct(), $priceStart, true);
                        $packagePrices[$device->getId()]['mixmatch'] = $priceWithTax;
                    }
                }
            }
            unset($device);
            unset($subscription);
            unset($deviceRC);

            $productPriceColumns = array(
                'price',
                'regular_maf',
                'tax_class_id',
                'special_price',
                'prijs_aansluit_promo_bedrag',
                'prijs_aansluit_promo_procent',
                'prijs_maf_discount_percent',
                'prijs_maf_new_amount'
            );
            $productIds = array_map(function ($item) {
                return $item->getProductId();
            }, $packageItems);
            $productCollection = Mage::getModel('catalog/product')->getCollection()
                ->addFieldToFilter('entity_id', array('in' => $productIds))
                ->addStoreFilter($orderOrQuote->getStoreId())
                ->addAttributeToSelect($productPriceColumns);

            /** @var Mage_Sales_Model_Order_Item $item */
            foreach ($packageItems as $item) {
                foreach (array(
                             'discount_amount',
                             'discount_percent',
                             'discount_percent',
                             'discount_invoiced',
                             'base_discount_invoiced',
                             'discount_refunded',
                             'tax_before_discount',
                             'amount_refunded',
                             'price',
                             'row_total',
                             'row_total_incl_tax',
                             'weee_tax_applied',
                             'weee_tax_applied_row_amount',
                             'weee_tax_applied_amount',
                             'base_weee_tax_applied_row_amount',
                             'base_weee_tax_applied_amount',
                             'tax_amount',
                             'tax_percent',
                             'tax_before_discount',
                             'no_discount',
                             'maf_discount_amount',
                             'maf_tax_before_discount',
                             'hidden_maf_tax_amount',
                             'applied_rule_ids',
                             'mixmatch_subtotal',
                             'mixmatch_tax',
                         ) as $priceColumn
                ) {
                    $packagePrices[$item->getId()][$priceColumn] = $item->getData($priceColumn);
                }

                $tmpProd = $productCollection->getItemByColumnValue('entity_id', $item->getProductId());
                if ($tmpProd === null) {
                    $tmpProd = new Varien_Object();
                }

                foreach ($productPriceColumns as $productPriceColumn) {
                    $packagePrices[$item->getId()]['product'][$productPriceColumn] = $tmpProd->getData($productPriceColumn);
                }
                $packagePrices[$item->getId()]['product']['price_excl'] = Mage::helper('tax')->getPrice($tmpProd, $tmpProd->getPrice(), false);

                if (!$onlyItemId || $onlyItemId == $item->getId()) {
                    $item->setData('order_item_prices', Zend_Json::encode($packagePrices[$item->getId()]))->save();
                }
                if ($onlyItemId) {
                    return Zend_Json::encode($packagePrices[$item->getId()]);
                }
            }
        }
    }

    /**
     * @param $deviceSku
     * @param $subscriptionSku
     * @param $deviceRCSku
     * @param bool $asArray
     * @param null $websiteId
     * @return mixed|Varien_Data_Collection
     */
    protected function getMixMatches($deviceSku,
                                     $subscriptionSku,
                                     $deviceRCSku = null,
                                     $asArray = false,
                                     $websiteId = null)
    {
        $key = md5(sprintf(
            '%s|%s|%s|%s|%s',
            $deviceSku,
            $subscriptionSku,
            $deviceRCSku,
            $asArray ? 1 : 0,
            is_object($websiteId)
                ? $websiteId->getId()
                : ($websiteId
                ? $websiteId
                : Mage::app()->getWebsite()->getId()
            )
        ));

        if ($result = (unserialize($this->getCache()->load($key)))) {
            return $result;
        } else {
            /** @var Dyna_MixMatch_Model_Resource_Price_Collection $mixMatches */
            $mixMatches = Mage::getResourceModel('omnius_mixmatch/price_collection')
                ->addFieldToFilter('website_id', $websiteId)
                ->addFieldToFilter('target_sku', $deviceSku)
                ->addFieldToFilter('source_sku', $subscriptionSku);

            if ($deviceRCSku != null) {
                $mixMatches->addFieldToFilter('device_subscription_sku', $deviceRCSku);
            }

            if ($asArray) {
                $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
                $result = $conn->fetchAll($mixMatches->getSelect());
            } else {
                $result = new Varien_Data_Collection();
                foreach ($mixMatches->getItems() as $item) {
                    $result->addItem($item);
                }
            }

            $this->getCache()->save(serialize($result),
                $key,
                array(Vznl_Configurator_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $packageId
     * @param bool $items
     * @param array $itemsGroupType
     * @param $isBusiness
     * @return int
     *
     * Calculates the difference that a client (or Vodafone) has to pay based on the products that he changed in a package
     */
    public function getEditPackagePriceDifference($packageId,
                                                  $items = FALSE,
                                                  $itemsGroupType = array(),
                                                  $isBusiness = '')
    {
        $diffPrice = 0;

        $cart = Mage::getSingleton('checkout/cart');
        if (!$cart->isChanged()) return $diffPrice;

        if (!isset($isBusiness)) {
            $isBusiness = $this->_getCustomerSession()->getCustomer()
                && $this->_getCustomerSession()->getCustomer()->getIsBusiness();
        }

        $previousPackages = Mage::getSingleton('checkout/session')->getIsEditMode()
            ? Mage::getSingleton('checkout/session')->getPreviousPackages()
            : null;
        $productIdsInPackage = array();
        $previousIdsInPackage = array();

        //if items are not provided, get them manually
        if ($items === FALSE) {
            $items = array();
            foreach (Mage::helper('dyna_package')->getModifiedQuotes() as $quote) {
                /** @var Vznl_Checkout_Model_Sales_Quote $quote */
                if ($quote->getPackageById($packageId)->isPayed()) {
                    //calculate price diff only if package is already payed
                    foreach ($quote->getAllItems() as $quoteItem) {
                        if ($quoteItem->getPackageId() == $packageId) {
                            $items[] = $quoteItem;
                        }
                    }
                }
            }
        }

        foreach ($items as $item) {
            /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
            if ($item->getQuote()->getPackageById($packageId)->isPayed()) {
                //calculate price diff only if package is already payed
                $productIdsInPackage[] = $item->getProductId();
            }
        }

        $flippedItemsGroupType = array_flip($itemsGroupType);
        foreach ($previousPackages[$packageId]['items'] as $groupType => $groupItems) {
            $groupTypeModified = ucfirst($groupType);
            foreach ($groupItems as $previousItem) {
                $quote = $this->getQuoteWithId($previousItem['quote_id']);
                if ($quote->getPackageById($packageId)->isPayed()) {
                    //calculate price diff only if package is already payed
                    if (empty($itemsGroupType)
                        || isset($flippedItemsGroupType[$groupTypeModified])) {
                        $previousIdsInPackage[] = $previousItem['product_id'];
                        if ($groupType == Omnius_Configurator_Model_AttributeGroup::ATTR_GROUP_PROMOTIONS) {
                            $diffPrice -= $previousItem['applied_promo_amount'];
                        } else {
                            $totalFpt = 0;
                            if ($previousItem['weee_tax_applied']) {
                                $taxesUnserialized = unserialize($previousItem['weee_tax_applied']);
                                foreach ($taxesUnserialized as $tax) {
                                    $totalFpt += $isBusiness
                                        ? $tax['amount']
                                        : $tax['amount_incl_tax'];
                                }
                            }

                            $rowTotal = $previousItem['row_total']
                                - ($previousItem['discount_amount']
                                    - $previousItem['hidden_tax_amount']);
                            $rowTotalIncl = $previousItem['row_total_incl_tax']
                                - $previousItem['discount_amount'];
                            $itemPrice = $isBusiness ? $rowTotal : $rowTotalIncl;
                            $diffPrice -= $totalFpt + $itemPrice;
                        }
                    }
                }
            }
        }
        foreach ($items as $item) {
            if ($item->getQuote()->getPackageById($packageId)->isPayed()) {
                //calculate price diff only if package is already payed
                $diffPrice = round($diffPrice, 2);
                $rowTotalNew = $item->getItemFinalPriceExclTax();
                $rowTotalInclNew = $item->getItemFinalPriceInclTax();
                $finalPrice = $isBusiness ? $rowTotalNew : $rowTotalInclNew;
                $fpt = Mage::helper('tax')->getPrice($item->getProduct(),
                    $item->getFixedProductTaxInclVat(),
                    !$isBusiness);
                $diffPrice += $finalPrice + $fpt;
            }
        }

        return $diffPrice;
    }

    /**
     * @return array
     *
     * Return total differences in packages
     */
    public function getTotalPackagePriceDifference()
    {
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');
        $modifiedQuotes = $packageHelper->getModifiedQuotes();
        $diffTotalPrice = count($modifiedQuotes) ? 0 : false;

        foreach ($modifiedQuotes as $editeQuote) {
            $diffPrice = $packageHelper->getPackageDifferences($editeQuote->getActivePackageId());
            $diffTotalPrice += $diffPrice;
        }

        return $diffTotalPrice;
    }

    /**
     * @return bool
     *
     * Checks if a user is on the checkout section
     */
    public static function onCheckout()
    {
        return Mage::app()->getRequest()->getModuleName() == 'checkout' && Mage::app()->getRequest()->getControllerName() == 'cart';
    }

    /**
     * @return array
     * @param $packageDatas
     *
     * Totals for order edits need to be calculate manually
     * as they need to be computed from the original order, temporary quotes,
     * cancelled packages, etc
     */
    public function getEditTotals($packageDatas)
    {
        $total_maf_excl = 0;
        $total_price_excl = 0;
        $total_maf_tax = 0;
        $total_price_tax = 0;
        $total_maf = 0;
        $total_price = 0;
        $additional_subtotal_price = 0;
        $additional_total_price = 0;
        $additional_tax_price = 0;

        /**
         * Loop through each package.
         * If package is cancelled or is currently being cancelled,
         * ignore its prices
         */

        $flippedCancelledPackagesNow = array_flip(Mage::helper('dyna_package')->getCancelledPackagesNow());
        foreach ($packageDatas as $packageData) {
            $packageModel = $packageData[0];
            $packageItems = $packageData[1];
            $packageId = $packageModel->getPackageId();
            if ($packageModel->getStatus() != Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED
                && $packageModel->getStatus() != Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PORTING_CANCELLED
                && (!isset($flippedCancelledPackagesNow[$packageId]))
                && ($packageModel->getStatus() != Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_RETURNED)
            ) {
                $packageTotals = $packageModel->getPackageTotals($packageItems);
                $total_maf_excl += $packageTotals['subtotal_maf'];
                $total_price_excl += $packageTotals['subtotal_price'];
                $total_maf_tax += $packageTotals['tax_maf'];
                $total_price_tax += $packageTotals['tax_price'];
                $total_maf += $packageTotals['total_maf'];
                $total_price += $packageTotals['total'];
                $additional_subtotal_price += $packageTotals['additional_subtotal'];
                $additional_total_price += $packageTotals['additional_total'];
                $additional_tax_price += $packageTotals['additional_tax'];
            }
        }

        return array(
            'total_maf_excl' => $total_maf_excl,
            'total_price_excl' => $total_price_excl,
            'total_maf_tax' => $total_maf_tax,
            'total_price_tax' => $total_price_tax,
            'total_maf' => $total_maf,
            'total_price' => $total_price,
            'additional_total' => $additional_total_price,
            'additional_subtotal' => $additional_subtotal_price,
            'additional_tax' => $additional_tax_price
        );
    }

    public function handleProcessOrderResponse($response)
    {
        if (Mage::helper('omnius_service')->useStubs()) {
            return;
        }
        //make sure the superorder exists and update it's status
        $superOrder = Mage::getModel('superorder/superorder')
            ->getCollection()
            ->addFieldToFilter('order_number', $response['order_result']['sales_ordernumber'])
            ->getLastItem();
        if (!$superOrder->getId())
            throw new Exception('No superorder found with the provided sales order number');

        // retrieve all packages and update their status based on the response
        $packageCollections = Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('order_id', $superOrder->getId());
        foreach ($packageCollections as $package) {
            $packages[$package->getPackageId()] = $package;
        }

        foreach ($response['order_result']['packages'] as $package) {
            if (isset($package['package_i_d'])
                && isset($package['package_status'])) {
                $package = array($package);
            }

            if (is_array($package)) {
                foreach ($package as $pack) {
                    $packageId = $pack['package_i_d'];
                    $status = $pack['package_status'];
                    if (isset($packages[$packageId])) {
                        $packageModel = $packages[$packageId];
                        if ($packageModel && $packageModel->getId()) {
                            $packageModel->setStockStatus($status)->save();
                        }
                    }
                }
            }
        }
    }

    public function getContractPackages($superOrderId = null)
    {
        $packages = array();
        if ($superOrderId) {
            $orders = Mage::getModel('sales/order')
                ->getNonEditedOrderItems($superOrderId);
            foreach ($orders as $order) {
                foreach ($order->getAllItems() as $item) {
                    $packageId = $item->getPackageId();
                    $packages[$packageId]['items'][$item->getId()] = $item;
                }
            }
        } elseif ($this->_getCustomerSession()->getOrderEdit()) {

            $quotes = Mage::helper('dyna_package')->getModifiedQuotes();
            foreach ($quotes as $quote) {
                $packages += $quote->getPackages();
            }
        } else {
            $session = Mage::getSingleton('checkout/session');
            /** @var Vznl_Checkout_Model_Sales_Quote $quote */
            $quote = $session->getQuote();
            $packages = $quote->getPackages();
        }

        return $packages;
    }

    /**
     * Based on the type, retrieve all countries, or only a specific country
     *
     * @param $type
     */
    public function getCountriesForDropdown($type)
    {
        switch ($type) {

            case 'N' :
            case 'R' :
                $result = [['value' => 'NL', 'label' => $this->__('Netherlands')]];
                break;
            case 'P' :
            case '0' :
            case '1' :
            case '2' :
            case '3' :
            case '4' :
                $result = $this->getAllCountries();
                break;
            default:
                $result = $this->getAllCountries();
        }

        return $result;
    }

    public function getEuCountries()
    {
        $cache = $this->getCache();
        if ($result = $cache->load('cached_eu_countries')) {
            return unserialize($result);
        }

        $euCountries = explode(',',
            Mage::getStoreConfig(Mage_Core_Helper_Data::XML_PATH_EU_COUNTRIES_LIST));

        $all = Mage::getResourceModel('directory/country_collection')
            ->loadData()
            ->toOptionArray(false);
        $flippedEuCountries = array_flip($euCountries);
        foreach ($all as $key => $node) {
            $value = $node['value'];
            if (!isset($flippedEuCountries[$value])) {
                unset($all[$key]);
            }
        }

        $cache->save(serialize($all), 'cached_eu_countries');
        return $all;
    }

    public function getAllCountries()
    {
        $cache = $this->getCache();
        // If cached retrieve from cache
        if ($result = $cache->load('cached_all_countries')) {
            return unserialize($result);
        }

        // If not cached
        $allowedCountryCollection = array();
        $countryCollection = Mage::getResourceModel('directory/country_collection')
            ->loadData()
            ->toOptionArray(false);
        $allowedString = Mage::getStoreConfig('general/country/allow');
        $allowedCollection = explode(',', $allowedString);
        $flippedAllowedCollection = array_flip($allowedCollection);
        foreach ($countryCollection as $country) {
            $value = $country['value'];
            if (isset($flippedAllowedCollection[$value])) {
                array_push($allowedCountryCollection, $country);
            }
        }

        //Save the array in cache
        $cache->save(serialize($allowedCountryCollection), 'cached_all_countries');
        return $allowedCountryCollection;
    }

    /**
     * Check if the package with the provided id has delivery set to this store from the quote
     *
     * @param $packageId
     * @return bool
     */
    public function checkDeliveryInThisStore($quote, $packageId = null)
    {
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());
        $storeId = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();
        $allPackages = false;

        if (!$packageId) {
            $allPackages = true;
            $packageId = 1;
        }
        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            $addressArray[$packageId] = $data['deliver']['address'];
        } elseif (isset($data['pakket'])) { // Split delivery
            if (!$allPackages) {
                $addressArray[$packageId] = $data['pakket'][$packageId]['address'];
            } else {
                foreach ($data['pakket'] as $address) {
                    if ($address['address']['address'] == 'store'
                        && $address['address']['store_id'] == $storeId) {
                        return true;
                    } else {
                        $addressArray[$packageId] = $address['address'];
                    }
                }
            }
        }

        if (!empty($addressArray[$packageId])
            && $addressArray[$packageId]['address'] == 'store'
            && $addressArray[$packageId]['store_id'] == $storeId) {
            return true;
        }

        return false;
    }

    /**
     * By default, shipping costs should be hidden (FogBugz 3341).
     * However, based on FogBugz 2029 these will be displayed on phase 4
     */
    public function showShippingCosts()
    {
        return false;
    }

    /**
     * Get the edited quotes in order edit mode
     *
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @return array
     */
    public function getModifiedPackages($quote = null)
    {
        if (!$quote) {
            $quote = Mage::getModel('sales/quote')->load($this->_getCustomerSession()->getOrderEdit());
        }

        $modifiedQuotes = $quote->getListOfModifiedQuotes($quote->getSuperOrderEditId());

        $packageIds = array();
        /** @var Vznl_Checkout_Model_Sales_Quote $modifiedQuote */
        foreach ($modifiedQuotes as $modifiedQuote) {
            $packageIds = array_merge($packageIds, array_keys($modifiedQuote->getPackages()));
        }
        return array_unique($packageIds);
    }

    /**
     * Get specific packages info
     *
     * @param array $packages
     * @return array
     */
    public function getCartPackages($packages)
    {
        $cartPackages = array();
        $cartHelper = Mage::helper('dyna_configurator/cart');
        if ($packages) {
            foreach ($packages as $packageId => $package) {
                $packageName = $subscriptionName = '';
                if($package['items']) {
                    $package['items'] = $cartHelper->getSortedCartProducts($package['items']);
                }
                foreach ($package['items'] as &$item) {
                    $packageName .= $item->getName() . ', ';
                    if($item->getProduct()->isSubscription())
                    {
                        $subscriptionName = $item->getName();
                    }
                    if($package['type'] == 'prepaid' || $package['type'] == 'hardware') {
                        if($item->getProduct()->isDevice()) {
                            $subscriptionName = $item->getName();
                        }
                    }
                }
                unset($item);
                $cartPackages[$packageId] = array(
                    'type' => $package['type'],
                    'name' => rtrim($packageName, ', '),
                    'current_number' => $package['current_number'],
                    'sale_type' => $package['sale_type'],
                    'tel_number' => $package['tel_number'],
                    'one_of_deal' => $package['one_of_deal'],
                    'subscriptionName' => $subscriptionName,
                    'packageTitle' => Mage::helper('vznl_package')->getTypeAndSubtypeTitle($package, null)['packageTitle']
                );
            }
        }

        return $cartPackages;
    }

    /**
     * Retrieve the initial product ids of the package
     *
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param $packageId
     * @return array
     */
    public function getOldPackageProducts($quote, $packageId)
    {
        $oldItems = array();
        $found = false;
        $modifiedQuotes = $quote->getListOfModifiedQuotes($quote->getSuperOrderEditId());
        foreach ($modifiedQuotes as $modifiedQuote) {
            /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
            foreach ($modifiedQuote->getAllVisibleItems() as $item) {
                if ($item->getPackageId() == $packageId) {
                    if ($item->getProduct()->isOfHardwareType()) {
                        $oldItems[] = $item->getProductId();
                    }
                    $found = true;
                } else {
                    // Since the quote can only have one packageId,
                    // break after first item, because it will not be found
                    break;
                }
            }
            // Since there can only be one quote for that packageId,
            // stop after it was found
            if ($found) {
                break;
            }
        }

        return array_unique($oldItems);
    }

    /**
     * Check what items have changed in the current package
     * (ignore subscription changes)
     */
    public function getPackagesDifferences($quote, $added = false)
    {
        $changes = array();
        $current = array();
        $old = array();
        $productNames = array();
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $modifiedQuotes = $quote->getListOfModifiedQuotes($quote->getSuperOrderEditId());
        /** @var Vznl_Checkout_Model_Sales_Quote $modifiedQuote */
        foreach ($modifiedQuotes as $modifiedQuote) {
            /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
            foreach ($modifiedQuote->getAllVisibleItems() as $item) {
                if (!isset($current[$item->getPackageId()])) {
                    $current[$item->getPackageId()] = array();
                }

                $current[$item->getPackageId()][] = $item->getProductId();
                $productNames[$item->getProductId()] = $item->getProduct()->getName();
                $currentDoa[$item->getPackageId()][$item->getProductId()] = (bool)$item->getItemDoa();
            }
        }
        foreach ($quote->getAllVisibleItems() as $item) {
            if (!isset($current[$item->getPackageId()])) {
                $old[$item->getPackageId()] = array();
            }

            $old[$item->getPackageId()][] = $item->getProductId();
            $productNames[$item->getProductId()] = $item->getProduct()->getName();
            $oldDoa[$item->getPackageId()][$item->getProductId()] = (bool)$item->getItemDoa();
        }

        // needs to check for items that are removed
        $itemsToCheck = $old;
        $itemsToCheckAgainst = $current;
        $doaToCheck = $oldDoa;
        $doaToCheckAgainst = $currentDoa;
        if ($added) {
            // needs to check for items that are added
            $itemsToCheck = $current;
            $itemsToCheckAgainst = $old;
            $doaToCheck = $currentDoa;
            $doaToCheckAgainst = $oldDoa;
        }

        foreach ($itemsToCheck as $packageId => $items) {
            $flippedItemsToChangeAgainst = array_flip($itemsToCheckAgainst[$packageId]);
            foreach ($items as $item) {
                if (isset($itemsToCheckAgainst[$packageId])
                    &&
                    (!isset($flippedItemsToChangeAgainst[$item])
                        || (isset($doaToCheckAgainst[$packageId][$item])
                            && isset($doaToCheck[$packageId][$item])
                            && $doaToCheckAgainst[$packageId][$item] != $doaToCheck[$packageId][$item]))
                ) {
                    $changes[$packageId][] = $productNames[$item];
                }
            }
        }
        return $changes;
    }

    public function getRefundAmounts($quote, $packages, $editMode = false)
    {
        $totalRefund = 0;
        $packagesRefund = array();

        if (!empty($packages)) {
            foreach ($packages as $packageId) {
                if ($editMode) {
                    $packageDiffPrice = Mage::helper('dyna_package')->getPackageDifferences($packageId);
                    $totalRefund += $packageDiffPrice;
                    $packagesRefund[$packageId] = $packageDiffPrice;
                } else {
                    $totals = Mage::helper('dyna_configurator')->getActivePackageTotals(
                        $packageId,
                        !($this->_getCustomerSession()->getCustomer()
                            && $this->_getCustomerSession()->getCustomer()->getIsBusiness()),
                        $quote->getId()
                    );

                    $totalRefund += $totals['totalprice'];
                    $packagesRefund[$packageId] = $totals['totalprice'];
                }
            }
        }
        return array('packagesRefund' => $packagesRefund,
            'totalRefund' => $totalRefund);
    }

    /**
     * @return bool
     */
    public function showSuspectFraudCheck()
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        return ( ( !Mage::helper('vznl_agent')->isTelesalesLine() ) && ( ( $quote->hasFixed() && $quote->hasMobilePackage() ) || ( $quote->hasMobilePackage()) ) );
    }

    /**
     * Returns the default value of a field from DB
     *
     * @param $field
     * @param null $section
     * @param null $storeId
     * @return null
     */
    public function getDefaultValueForField($field, $section = null, $storeId = null)
    {
        // If no section provided, return null
        if (!$section) {
            return null;
        }

        $storeId = $storeId
            ? is_object($storeId)
                ? $storeId->getId()
                : $storeId
            : Mage::app()->getStore()->getId();

        $key = sprintf('default_values_for_%s_%s_%s', $field, $section, $storeId);
        if (false !== ($result = $this->getCache()->load($key))) {
            return unserialize($result);
        } else {
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            if (!is_numeric($section)) {
                $section = Mage::getResourceModel('field/section_collection')
                    ->addFieldToFilter('name', $section);
                if (false === ($section = $conn->fetchOne($section->getSelect()))) {
                    $this->getCache()->save('N;',
                        $key, array(Vznl_Configurator_Model_Cache::CACHE_TAG),
                        $this->getCache()->getTtl());
                    return null;
                }
            }
            $sectionId = $section;

            $value = Mage::getResourceModel('field/field_collection')
                ->addFieldToSelect('default_value')
                ->addFieldToFilter('store_id', $storeId)
                ->addFieldToFilter('section_id', $sectionId)
                ->addFieldToFilter('name', $field);

            if (false === ($value = $conn->fetchOne($value->getSelect()))) {
                $this->getCache()->save('N;',
                    $key,
                    array(Vznl_Configurator_Model_Cache::CACHE_TAG),
                    $this->getCache()->getTtl());
                return null;
            }

            $this->getCache()->save(serialize($value),
                $key,
                array(Vznl_Configurator_Model_Cache::CACHE_TAG),
                $this->getCache()->getTtl());
            return $value;
        }
    }

    /**
     * Check if the delivery steps (number selection and contract) can be displayed
     *
     * @param $orderedItems
     * @return bool
     */
    public function canProcessDeliverySteps($orderedItems)
    {
        $blockPackageDelivery = array();

        if (count($orderedItems) > 0) {
            foreach ($orderedItems as $packageId => $packageItems) {
                $hasHn = Mage::helper('dyna_catalog')
                    ->packageContainsHollandsNieuweSubscription(array('items' => $packageItems));
                if (!$hasHn) {
                    if (!Mage::helper('dyna_catalog')
                        ->packageContainsVfDaSubscription(array('items' => $packageItems))) {
                        $blockPackageDelivery[$packageId] = true;
                    }
                } else {
                    $blockPackageDelivery[$packageId] = true;
                }
            }

            if (count($blockPackageDelivery) != count($orderedItems)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Validate the ordered items contain a device subscription that is not simonly for indirect
     *
     * @param array $orderedItems
     * @return bool
     */
    public function canProcessDeviceSelectionStep($orderedItems)
    {
        if (Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
            $noHardware = Mage::helper('vznl_checkout/aikido')->getIndirectNoHardwareSku();
            foreach ($orderedItems as $packageId => $items) {
                $hasVodafone = false;
                $hasDeviceSubscription = false;
                foreach ($items as $item) {
                    $product = $item->getProduct();
                    if (Mage::helper('dyna_catalog')
                        ->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION),
                            $product)) {
                        $hasVodafone = true;
                    }
                    if ($product->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)
                        && ($product->getSku() != $noHardware)) {
                        $hasDeviceSubscription = true;
                    }
                }
                if ($hasDeviceSubscription && $hasVodafone) {
                    return true;
                }
            }
        };

        return false;
    }

    public function decorateExclIncl($price, $item, $showPriceWithoutBtw)
    {
        $product = $item->getProduct();
        $showPriceWithoutBtw = ($showPriceWithoutBtw == false)
            ? false : !$this->_getCustomerSession()->showPriceWithBtw();
        $excl = Mage::helper('tax')->getPrice($product, $price, false);
        $incl = Mage::helper('tax')->getPrice($product, $price, true);
        // TAX Display
        $priceStyle = $showPriceWithoutBtw ? '' : 'display: none;';
        $priceTaxStyle = !$showPriceWithoutBtw ? '' : 'display: none;';
        return
            '<div class="excl-vat-tax" style="' . $priceStyle . '">' .
            Mage::helper('core')->currency($excl) .
            '</div>
            <div class="incl-vat-tax" style="' . $priceTaxStyle . '">' .
            Mage::helper('core')->currency($incl) .
            '</div>';
    }

    /**
     * Creates the refund methods options structure
     * @param bool $asOptions Return as formatted string to be used in select
     * @param bool $firstKey Return only the first option
     * @param float $refundAmount
     * @return bool|string|array
     */
    public function getRefundMethods($asOptions = false, $firstKey = false, $refundAmount = null)
    {
        // TODO: maybe get this options from backend config
        $optionsArr = array(
            'cash' => 'Refund via cashier',
            'dss' => 'DSS refund form',
        );

        if ($firstKey) {
            return key($optionsArr);
        }

        if ($asOptions) {
            $html = '';
            foreach ($optionsArr as $k => $v) {
                if ($k == 'cash'
                    && !($refundAmount <= 150
                        && Mage::helper('agent')->isRetailStore())) {
                    continue;
                }
                $html .= '<option value="' . $k . '">' . $this->__($v) . '</option>';
            }
            return $html;
        }

        return $optionsArr;
    }

    /**
     * Creates the refund reasons options structure for change
     * @param bool $asOptions Return as formatted string to be used in select
     * @param bool $firstKey Return only the first option
     * @return bool|string|array
     */
    public function getRefundReasonsChange($asOptions = false, $firstKey = false)
    {
        if (!$optionsArr = Mage::helper('dyna_customer/data')->getRefundReasonsChangeOptions()) {
            return false;
        }

        if ($firstKey) {
            return key($optionsArr);
        }

        if ($asOptions) {
            $html = '';
            foreach ($optionsArr as $k => $v) {
                $html .= '<option value="'
                    . $k
                    . '">'
                    . $v
                    . '</option>';
            }

            return $html;
        }

        return $optionsArr;
    }

    /**
     * Creates the refund reasons options structure for cancel
     * @param bool $asOptions Return as formatted string to be used in select
     * @param bool $firstKey Return only the first option
     * @return bool|string|array
     */
    public function getRefundReasonsCancel($asOptions = false, $firstKey = false)
    {
        if (!$optionsArr = Mage::helper('dyna_customer/data')
            ->getRefundReasonsCancelOptions()) {
            return false;
        }

        if ($firstKey) {
            return key($optionsArr);
        }

        if ($asOptions) {
            $html = '';
            foreach ($optionsArr as $k => $v) {
                $html .= '<option value="' . $k . '">' . $v . '</option>';
            }

            return $html;
        }

        return $optionsArr;
    }

    /**
     * Format the date based on locale settings
     * @param $date
     * @param string $format
     * @return string
     */
    public function formatDateLocale($date, $format = 'medium')
    {
        return Mage::app()
            ->getLocale()
            ->date(Mage::getModel('core/date')->timestamp(strtotime($date)),
                null,
                null,
                false)
            ->toString(Mage::app()->getLocale()->getDateTimeFormat($format));
    }

    /**
     * Create an array with all the packages that are disabled
     * (cannot apply any change) in this session
     *
     * @param array $packages
     * @param int $superOrderEditId
     * @return array
     */
    public function checkForDisabledPackages($packages, $superOrderEditId)
    {
        $disabledPackages = array();
        $packageHelper = Mage::helper('dyna_package');
        $packageModel = Mage::getModel('package/package');

        // Edited packages of the SuperOrder
        $cancelledPackages = $packageHelper->getCancelledPackages();
        // Edited packages in this session
        $cancelledPackagesNow = $packageHelper->getCancelledPackagesNow();
        $allCancelledPackages = array_merge($cancelledPackages, $cancelledPackagesNow);

        // Edited packages in this session.
        $editedPackagesIds = Mage::registry('editedPackages');
        if (empty($editedPackagesIds)) {
            $editedPackagesIds = array();
        }

        $countEditedPackages = count($editedPackagesIds);
        // SuperOrder is locked or not
        $isLocked = $this->checkOrder($superOrderEditId, false) === true;
        $childOrders = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('parent_id', $superOrderEditId);
        if ($countEditedPackages > 0) {
            //get the status of the edited package
            $editedPackageStatus = $packageModel->getPackages($superOrderEditId, null, $editedPackagesIds[0])->getFirstItem()->getStatus();
        } else {
            $editedPackageStatus = null;
        }

        $flippedAllCancelledPackages = array_flip($allCancelledPackages);
        $flippedEditedPackagesIds = array_flip($editedPackagesIds);
        foreach ($packages as $packageId => $quotePackage) {
            foreach ($childOrders as $childOrder) {
                $childPackage = $this->getChildPackage($childOrder, $packageId);
                if ($childPackage->getId()) {
                    break;
                }
            }
            $package = $packageModel->getPackages($superOrderEditId, null, $packageId)->getFirstItem();
            $isCanceled = isset($flippedAllCancelledPackages[$packageId]);
            $isModified = isset($flippedEditedPackagesIds[$packageId]);
            $disabledPackages[$packageId] = (!$package->getSuperOrder()->isEditable())
                || ($countEditedPackages
                    && (($editedPackageStatus && ($package->getStatus() !== $editedPackageStatus))
                        || $isLocked
                        || ($isModified !== false)))
                || ($isCanceled !== false)
                || (isset($childPackage) && $childPackage->getId());
        }

        return $disabledPackages;

    }

    /**
     * Strip all characters from string, and return upper case letters followed by a dot
     *
     * @param $input
     * @return string
     */
    public function processInitials($input)
    {
        $input = preg_replace('/[0987654321\s\.\~\`\!\@\#\$\%\^\&\*\(\)\_\+\=\-\[\]\{\}\;\'\:\|\"\<\>\,\/\?\\"]/', '', $input);

        if (empty($input)) {
            return '';
        }

        $text = array();
        $length = mb_strlen($input);
        for ($i = 0; $i < $length; ++$i) {
            $text[] = mb_strtoupper(mb_substr($input, $i, 1));
        }
        return implode('.', $text) . '.';
    }

    public function _getAddressByType($address)
    {
        $dealer_column = 'dealer_id';
        if (isset($address['dealer_column'])) {
            $dealer_column = $address['dealer_column'];
        }

        switch ($address['address']) {
            case 'store' :
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                $addressData = Mage::getModel('agent/dealer')->getCollection()
                    ->addFieldToFilter($dealer_column, $address['store_id']);
                if (!Mage::helper('agent')->isTelesalesLine()) {
                    $addressData->addFieldToFilter('store_id', Mage::app()->getStore()->getId());
                }
                $addressData->fetchItem();

                if ($addressData) {
                    $addressData = $addressData->getFirstItem();
                    $result = array(
                        'street' => array(trim($addressData['street']), trim($addressData['house_nr']), ''),
                        'postcode' => str_replace(' ', '', $addressData['postcode']),
                        'city' => trim($addressData['city']),
                    );
                    $result['store_id'] = $address['store_id'];
                }
                break;
            case 'direct' :
                $storeId = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                $addressData = Mage::getModel('agent/dealer')->getCollection()
                    ->addFieldToFilter('dealer_id', $storeId)
                    ->fetchItem();
                if ($addressData) {
                    $addressData = $addressData->getData();
                    $address['address'] = 'store';
                    $result = array(
                        'street' => array(trim($addressData['street']), trim($addressData['house_nr']), ''),
                        'postcode' => str_replace(' ', '', $addressData['postcode']),
                        'city' => trim($addressData['city']),
                    );
                    $result['store_id'] = $storeId;
                }
                break;
            case 'billing_address' :
                if (!isset($address['billingAddress'])) {
                    throw new Exception($this->__('Incomplete data'));
                }

                $result = array(
                    'street' => array($address['billingAddress']['street'], $address['billingAddress']['houseno'], $address['billingAddress']['addition']),
                    'postcode' => str_replace(' ', '', $address['billingAddress']['postcode']),
                    'city' => $address['billingAddress']['city'],
                );
                break;
            case 'foreign_address' :
                if (!isset($address['foreignAddress'])) {
                    throw new Exception($this->__('Incomplete data'));
                }
                $temp = $address['foreignAddress'];
                $result = $temp;
                // Build street data from input data
                $result['street'] = array($temp['street']);

                $result['extra_street'] = array();
                if (isset($temp['extra']) && is_array($temp['extra'])) {
                    foreach ($temp['extra'] as $extraLine) {
                        if (!empty($extraLine)) {
                            $result['extra_street'][] = $extraLine;
                        }
                    }
                }
                break;
            case 'other_address' :

                if (!isset($address['otherAddress'])) {
                    throw new Exception($this->__('Incomplete data'));
                }
                $result = array(
                    'street' => array($address['otherAddress']['street'], $address['otherAddress']['houseno'], $address['otherAddress']['addition']),
                    'postcode' => str_replace(' ', '', $address['otherAddress']['postcode']),
                    'city' => $address['otherAddress']['city'],
                );
                break;
            default:
                throw new Exception($this->__('Incomplete data'));
        }
        $result['address'] = $address['address'];
        return $result;
    }

    public function _validateDeliveryAddressData($data, $prefix = '')
    {
        $errors = array();
        switch ($data['address']) {
            case 'store':
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNLPostcode'
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                );
                $errors += $this->_performValidate($data, $addressValidations, strlen($prefix) ? $prefix : 'store[', ']');
                break;
            case 'billing_address':
                $addressValidations = array(
                        'street' => array(
                            'required' => true,
                            'validation' => array(
                                'validateStreet'
                            ),
                            'message' => ['validateStreet' => $this->__('Street name and number are mandatory')]
                        ),
                        'houseno' => array(
                            'required' => false,
                            'validation' => array()
                        ),
                        'addition' => array(
                            'required' => false,
                            'validation' => array()
                        ),
                        'postcode' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNLPostcode'
                            )
                        ),
                        'city' => array(
                            'required' => true,
                            'validation' => array()
                        ),
                    );
                $errors += $this->_performValidate($data, $addressValidations, strlen($prefix) ? $prefix : 'billingAddress[', ']');
                break;
            case 'other_address' :
            case 'shipping_address' :
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                            'validateStreet'
                        ),
                        'message' => ['validateStreet' => $this->__('Street name and number are mandatory')]
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array()
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array()
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                            'validateNLPostcode'
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                );
                $errors += $this->_performValidate($data, $addressValidations, strlen($prefix) ? $prefix : 'otherAddress[', ']');
                break;
            case 'foreign_address' :
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array()
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array()
                    ),
                    'extra' => array(
                        'required' => false,
                        'validation' => array()
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'region' => array(
                        'required' => false,
                        'validation' => array()
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'country_id' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                );
                $errors += $this->_performValidate($data, $addressValidations, strlen($prefix) ? $prefix : 'foreignAddress[', ']');
                break;
            default :
                throw new Exception($this->__('Missing delivery address data'));
        }
        return $errors;
    }

    /**
     * @param array $data
     * @param array $validations
     * @param null /string $prefix
     * @param null /string $suffix
     * @return array
     * @throws Mage_Customer_Exception
     */
    public function _performValidate($data, $validations, $prefix = null, $suffix = null)
    {
        $errors = array();
        foreach ($validations as $key => $values) {
            if ($values['required'] && ((!isset($data[$key])) || (empty($data[$key]) && strlen($data[$key]) == 0))) {
                $errors[$prefix . $key . $suffix] = $this->__('This is a required field.');
                continue;
            }

            if (!$values['required'] && (!isset($data[$key]) || empty($data[$key]))) {
                continue;
            }

            if (isset($values['validation']) && is_array($values['validation'])) {
                foreach ($values['validation'] as $rule) {
                    //if another error is already set, don't validate
                    if (isset($errors[$prefix . $key . $suffix])) {
                        continue;
                    }
                    if (!empty($rule) && !Mage::helper('dyna_validators/data')->{$rule}($data[$key])) {
                        if (!empty($values['message'][$rule])
                        ) {
                            $errors[$prefix . $key . $suffix] = $this->__($values['message'][$rule]);
                        } else {
                            $errors[$prefix . $key . $suffix] = $this->__('Field has invalid data');
                        }
                        continue;
                    }
                }
            }
        }

        return $errors;
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * @param $packId
     * @param $esbSuperOrderId
     *
     * Call work item for delivered packages which are changed or cancelled, have homeDelivery and no goods were returned
     */
    public function callWorkItem($packId, $esbSuperOrderId)
    {
        $client = Mage::helper('omnius_service')->getClient('work_item');
        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $esbSuperOrderId)
            ->addFieldToFilter('package_id', $packId)
            ->getLastItem();

        //if found and has no return date (meaning goods were not yet returned)
        if ($package->getId() && $package->isDelivered()) {
            //check if homedelivery
            $saleOrders = Mage::getModel('sales/order')->getNonEditedOrderItems($esbSuperOrderId, true, false);
            foreach ($saleOrders as $saleOrder) {
                foreach ($saleOrder->getAllItems() as $item) {
                    if ($item->getPackageId() == $packId) {
                        if ($saleOrder->isHomeDelivery()) {
                            $superOrder = $this->getSuperOrder($esbSuperOrderId);
                            $client->executeWorkItem($superOrder->getOrderNumber(), ($packId - 1));
                        }
                        break;
                    }
                }
            }
        }
    }

    public function createNewCartQuote($quote, $superOrderId)
    {
        if (!$quote->getId()) {
            return false;
        }

        // Check if superorder has been edited
        $count = Mage::getModel('sales/order')->getCollection()
            ->addFieldToSelect('entity_id')
            ->addFieldToFilter('superorder_id', $superOrderId)
            ->addFieldToFilter('edited', 1)
            ->count();

        if ($count) {
            return false;
        }

        // Create a new quote and set it active
        $newQuote = $this->cloneQuote($quote->getId());

        if ($quote->getSaveAsCart()) {
            // Mark the new quote as saved shopping cart
            $newQuote->setCartStatus(Vznl_Checkout_Model_Sales_Quote::CART_STATUS_SAVED)
                ->setActivePackageId(1)
                ->save();
        }

        // Create the packages for the new quote
        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $superOrderId);

        foreach ($packages as $package) {
            Mage::getModel('package/package')
                ->setData($package->getData())
                ->setEntityId(null)
                ->setStatus(null)
                ->setCreditcheckStatus(null)
                ->setCreditcheckStatusUpdated(null)
                ->setVfStatusCode(null)
                ->setVfStatusDesc(null)
                ->setPortingStatus(null)
                ->setPortingStatusUpdated(null)
                ->setQuoteId($newQuote->getId())
                ->setOrderId(null)
                ->save();
        }

        if ($quote->getQuoteParentId()) {
            $parentQuote = Mage::getModel('sales/quote')->load($quote->getQuoteParentId());
            if ($parentQuote->getId()) {
                $parentQuote->setCartStatus(null)
                    ->save();
            }
        }

        return $newQuote;
    }

    /**
     * Returns a virtual delivery address used in Indirect channel
     *
     * @return array
     */
    public function getVirtualDeliveryAddress()
    {
        $dealer = Mage::getSingleton('customer/session')->getAgent(true)->getDealer();
        return array(
            'street' => array($dealer->getStreet(), $dealer->getHouseNr(), ''),
            'postcode' => str_replace(' ', '', $dealer->getPostcode()),
            'city' => $dealer->getCity(),
            'address' => 'store',
            'store_id' => $dealer->getId(),
        );
    }

    /**
     * Transform names to the format needed in EBS
     *
     * @param string $name
     * @param int $limit
     * @param bool $first
     * @return string
     */
    public function formatNameForEbsOrchestration($name, $limit)
    {
        return substr($name, 0, $limit); // Limit last name to x characters
    }

    /**
     * Checks if there are delivery orders in this store.
     * @return bool
     */
    public function checkInThisStore()
    {
        if (Mage::app()->getStore()->getCode() != Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
            if (Mage::registry('skipToDeliverSteps')) {
                $orders = Mage::registry('orders');
                foreach ($orders as $order) {
                    if ($this->isOrderInThisStore($order)) {
                        return true;
                    }
                }
            } else {
                foreach (Mage::getSingleton('core/session')->getOrderIds() as $orderId) {
                    $order = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderId);
                    if ($this->isOrderInThisStore($order)) {
                        return true;
                    }
                }
            }
        } else {
            return true;
        }

        return false;
    }

    /**
     * Checks if the delivery order has the delivery method in this store.
     * @param $order Vznl_Checkout_Model_Sales_Order
     * @return bool
     */
    public function isOrderInThisStore($order)
    {
        $storeId = Mage::getSingleton('customer/session')->getAgent()->getDealer()->getId();
        $orderAddress = $order->getShippingAddress();
        return $orderAddress->getDeliveryType() == 'store' && $orderAddress->getDeliveryStoreId() == $storeId;
    }

    /**
     * Retrieve lock details to be able to display lock info for this order
     */
    public function getLockInfo($superorderId)
    {
        $currentAgent = $this->_getCustomerSession()->getAgent(true);
        $superorder = Mage::getModel('superorder/superorder')->load($superorderId);
        if (!$superorder->getId()) {
            return '';
        }

        $lockInfo = Mage::helper('omnius_service')->getClient('lock_manager')->doGetLockInfo($superorder);

        $this->_getCustomerSession()->setOrderEditMode(
            isset($lockInfo['lock']['user_i_d'])
            && $currentAgent->getId() == $lockInfo['lock']['user_i_d']
        );
        $orderEditMode = Mage::getSingleton('customer/session')->getOrderEditMode();

        $lockInfo['orderEditMode'] = $orderEditMode;
        $lockInfo['currentAgent'] = $currentAgent->getId();

        $lockInfo['locked_by'] = '';
        if (isset($lockInfo['lock']['user_i_d'])) {
            $lockInfo['locked_by'] = $lockInfo['lock']['user_i_d'];
            if (is_numeric($lockInfo['locked_by'])) {
                $lockedByAgent = Mage::getModel('agent/agent')->load($lockInfo['locked_by']);
                if ($lockedByAgent->getId()) {
                    $lockInfo['locked_by'] = trim($lockedByAgent->getFirstName() . " " . $lockedByAgent->getLastName());
                    $lockInfo['employee_number'] = $lockedByAgent->getEmployeeNumber();
                }
            }
        }

        $allowedSteal = false;
        if (isset($lockInfo['lock']['from_date_time']) && isset($lockInfo['lock']['lock_type']['allow_steal_after_locked_time_in_minutes'])) {
            $t1 = strtotime($lockInfo['lock']['from_date_time']);
            $allowedSteal = ($t1 + $lockInfo['lock']['lock_type']['allow_steal_after_locked_time_in_minutes'] * 60) < strtotime(Mage::getSingleton('core/date')->gmtDate());
        }
        if (isset($lockInfo['lock']['lock_type']['user_i_ds_locks_cannot_be_stolen_from'])) {
            $preventStealFrom = explode('|', $lockInfo['lock']['lock_type']['user_i_ds_locks_cannot_be_stolen_from']);
            if (isset($lockInfo['lock']['user_i_d']) && in_array($lockInfo['lock']['user_i_d'], $preventStealFrom)) {
                $allowedSteal = false;
            }
        }
        $lockInfo['allow_steal'] = isset($lockInfo['lock']) && $lockInfo['lock']['lock_type']['allow_steal'] && $allowedSteal;

        return $lockInfo;
    }

    /**
     * Get Contract phone settings from backend
     *
     * @param $customer Omnius_Customer_Model_Customer_Customer
     * @return mixed
     */
    public function getPhoneForContract($customer)
    {
        if ($customer->getIsBusiness()) {
            return $template = Mage::getStoreConfig(self::PHONE_NUMBER_CONTRACT_PATH_BUSINESS, Mage::app()->getStore()->getId());
        } else {
            return $template = Mage::getStoreConfig(self::PHONE_NUMBER_CONTRACT_PATH_CONSUMER, Mage::app()->getStore()->getId());
        }
    }

    /**
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param int $packageId
     * @param $target
     * @return array
     */
    public function updateMixMatches($quote, $packageId, $target)
    {
        $packageId = '';
        $subscription = $deviceRC = $deviceSku = null;
        /** @var Dyna_Checkout_Helper_Tax $taxHelper */
        $taxHelper = Mage::helper('tax');
        /** @var Dyna_MixMatch_Model_Pricing $mm */
        $mm = Mage::getModel('dyna_mixmatch/pricing');

        //levy product
        $levyAddonId = Mage::getResourceSingleton('catalog/product')->getIdBySku(Mage::getStoreConfig(Dyna_PriceRules_Model_Observer::COPY_LEVY_PREFIX . 'standaard'));
        foreach ($quote->getAllItems() as $packageItem) {
            if ($packageItem->getProductId() == $levyAddonId) {
                continue;
            }
            if ($packageItem->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) && $target->getPackageId() == $packageItem->getPackageId()) {
                $subscription = $packageItem;
                if ($deviceSku == null) {
                    $package = Mage::getModel('package/package')
                        ->getPackages(null, $packageItem->getQuoteId(), $packageItem->getPackageId())
                        ->getFirstItem();
                    $deviceSku = $package->isRetention()
                        ? Mage::helper('vznl_checkout/aikido')->getDefaultRetentionSku()
                        : Mage::helper('vznl_checkout/aikido')->getDefaultAcquisitionSku();
                }
            }

            if ($packageItem->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION) && $target->getPackageId() == $packageItem->getPackageId()) {
                $deviceRC = $packageItem;
            }
        }

        if ($subscription && $subscription->getProduct()->getAikido() && $deviceRC !== null) {
            /** @var Dyna_MixMatch_Model_Pricing $mm */
            $mm = Mage::getModel('dyna_mixmatch/pricing');
            $mixMatch = $mm->getMixMatchCollection($target->getSku(), $subscription->getSku(), $deviceRC->getSku());
            if (count($mixMatch) > 0) {
                $baseMixMatch = $mixMatch->getFirstItem()->getPrice() + (($deviceRC->getItemFinalMafInclTax()) * $subscription->getProduct()->getSubscriptionDuration());
                $subtotal = Mage::helper('tax')->getPrice($target->getProduct(), $baseMixMatch, false);
                $tax = $baseMixMatch - $subtotal;
            }

            return isset($subtotal) && isset($tax) ? ['subtotal' => $subtotal, 'tax' => $tax] : ['subtotal' => null, 'tax' => null];

        } else if ($subscription) {
            $mixMatch = $mm->getMixMatchCollection($target->getSku(), $subscription->getSku(), $deviceSku);
            if ($mixMatch && count($mixMatch) > 0) {
                // Calculating item mixmatch price if not null
                if ($mixMatch->getFirstItem()->getPrice() !== null && !$target->isContract()) {
                    // Just to be sure that price is positive for mixmatch
                    $baseMixMatch = abs($mixMatch->getFirstItem()->getPrice());
                    $subtotal = $taxHelper->getPrice($target->getProduct(), $baseMixMatch, false);
                    $tax = $baseMixMatch - $subtotal;
                    if ($target->getProduct()->isPromotion()) {
                        $subtotal = -1 * $subtotal;
                        $tax = -1 * $tax;
                    }
                }

                // Calculating item mixmatch maf if not null
                if ($mixMatch->getFirstItem()->getMaf() !== null) {
                    // Just to be sure that price is positive
                    $baseMafMixMatch = abs($mixMatch->getFirstItem()->getMaf());
                    $mafSubtotal = $taxHelper->getPrice($target->getProduct(), $baseMafMixMatch, false);
                    $mafTax = $baseMafMixMatch - $mafSubtotal;
                    if ($target->getProduct()->isPromotion()) {
                        // Making subtotal and tax negative so that they will decrease price
                        // Discount mixmatch maf
                        $mafSubtotal = -1 * $mafSubtotal;
                        $mafTax = -1 * $mafTax;
                    }
                }

                return array(
                    'subtotal' => isset($subtotal) ? $subtotal : null,
                    'tax' => isset($tax) ? $tax : null,
                    'maf_subtotal' => isset($mafSubtotal) ? $mafSubtotal : null,
                    'maf_tax' => isset($mafTax) ? $mafTax : null,
                );
            }
        }

        return [];
    }

    /**
     * Checks whether the current session contains any DOA changes.
     * @return bool
     */
    public function sessionHasDoa()
    {
        $cancelDoaItems = Mage::getSingleton('checkout/session')->getCancelDoaItems();
        $isDoa = false;
        if (is_array($cancelDoaItems)) {
            foreach ($cancelDoaItems as $packageId => $doaItems) {
                if (!empty(unserialize($doaItems))) {
                    $isDoa = true;
                    break;
                }
            }
        }

        return $isDoa;
    }

    /**
     * @param int $id
     */
    public function unsetSavedCart($id)
    {
        $removeSavedCart = Mage::getModel('sales/quote')->load($id);
        if ($removeSavedCart->getId()) {
            $removeSavedCart->setCartStatus(null)->save();
        }
    }

    /**
     * Decrement sales rules on customer unload.
     */
    public function decrementQuotesOnUnload()
    {
        /** @var Vznl_Checkout_Model_Sales_Quote[] $modifiedQuotes */
        $modifiedQuotes = Mage::helper('dyna_package')->getModifiedQuotes();
        foreach ($modifiedQuotes as $modifiedQuote) {
            Mage::helper('pricerules')->decrementAll($modifiedQuote);
        }

        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('checkout/cart')->getQuote();

        if (
            !$quote->getIsOffer()
            && (
                !$quote->getSuperOrderEditId() || (
                    $quote->getSuperOrderEditId()
                    && $quote->getSuperQuoteId()
                )
            )
        ) {
            Mage::helper('pricerules')->decrementAll($quote);
        }
    }

    /**
     * Based on received data, set NL KvK Validation or Foreign KvK Validation
     *
     * @param $data
     * @return array
     */
    public function getKvKValidation($data)
    {
        $res = [
            'validateName'
        ];

        if (isset($data['company_address']) && $data['company_address'] == 'foreign_address') {
            $res[] = 'validateCocForeign';
        } else {
            $res[] = 'validateCoc';
        }

        return $res;
    }

    /**
     * Gets the available delivery options for the current store.
     * @return array
     */
    public function getDeliveryOptions()
    {
        /** @var Dyna_Agent_Helper_Data $agentHelper */
        $agentHelper = Mage::helper('agent');

        $deliveryOptions = [
            'deliver'
        ];

        // Check if current order has more than one package
        if ($this->hasMoreThanOnePackage()) {
            $deliveryOptions[] = 'split';
        }

        // Pickup is possible for everything that is not online.
        if (!$agentHelper->checkIsWebshop()) {
            $deliveryOptions[] = 'pickup';
        }

        // Only retail stores can deliver on their own store.
        if ($agentHelper->isRetailStore() == true) {
            $deliveryOptions[] = 'direct';
        }
        return $deliveryOptions;
    }

    /**
     * Gets the available billing address options for fixed.
     * @return array
     */
    public function getBillingAddressOptions()
    {
        return self::BILLING_ADDRESS_OPTION;
    }

    /**
     * Gets the available payment options for the given deliveryMethod, taken into account the current store, the package
     * etc
     * @return array
     */
    public function getPaymentOptionsForDeliveryMethod($deliveryMethod)
    {
        $paymentOptions = [];
        // Add all payment options, but as false so it won't be used
        foreach ($this->getAllPaymentOptions() as $paymentOption) {
            $paymentOptions[$paymentOption] = false;
        }

        /** @var Dyna_Agent_Helper_Data $agentHelper */
        $agentHelper = Mage::helper('agent');

        // Different platforms
        $isTelesales = $agentHelper->isTelesalesLine();
        $isWebshop = $agentHelper->checkIsWebshop();

        // Different product types
        $isRetention = $this->checkIsRetentionOnly();

        $isExistingCustomer = $this->getCustomer() && $this->getCustomer()->getId();

        // Check if delivery method is pickup in (other) store.
        if (($deliveryMethod == 'direct' || $deliveryMethod == 'pickup') && !$isWebshop) {
            // Retention can only have cash register when the customer exists
            if (!$isRetention || $isExistingCustomer) {
                $paymentOptions['payinstore'] = true;
            }
        } // Check if delivery method is home delivery
        elseif ($deliveryMethod == 'deliver') {
            // Retention can only have cash on delivery when the customer exists
            if (!$isRetention || $isExistingCustomer) {
                $paymentOptions['cashondelivery'] = true;
                // Webshop can also do adyen
                if ($isWebshop) {
                    $paymentOptions['adyen_hpp'] = true;
                }
            }

            // Check if telesales and existing customer. But not a not business customer with a acquisition.
            if ($isTelesales && $isExistingCustomer) {
                $paymentOptions['checkmo'] = true;
            }
        } elseif ($deliveryMethod == 'split') {
            $paymentOptions['split-payment'] = true;
        }

        return $paymentOptions;
    }

    /**
     * Get alle possible delivery options and the possible payment methods.
     * @return array
     */
    public function getPaymentOptions()
    {
        $paymentOptions = [];
        $deliveryOptions = $this->getDeliveryOptions();
        foreach ($deliveryOptions as $deliveryMethod) {
            $paymentOptions[$deliveryMethod] = $this->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        }

        return $paymentOptions;
    }

    /**
     * Get all possible payment options.
     * @return array The array with payment options.
     */
    public function getAllPaymentOptions()
    {
        $paymentOptions = [
            'cashondelivery',
            'checkmo',
            'adyen_hpp',
            'payinstore',
            'alreadypaid',
            'split-payment'
        ];
        return $paymentOptions;
    }

    /**
     * Get the text belong to the payment option name.
     * @param $name The name to find the text for.
     * @return string The translated text.
     */
    public function getPaymentOptionTextForName($name)
    {
        $text = null;
        switch ($name) {
            case 'cashondelivery':
                $text = 'Reimbursement';
                break;
            case 'checkmo':
                $text = 'Invoice';
                break;
            case 'adyen_hpp':
                $text = 'Online (Adyen)';
                break;
            case 'payinstore':
                $text = 'Pay in store';
                break;
            case 'alreadypaid':
                $text = 'Already paid';
                break;
            case 'split-payment':
                $text = 'Split payment';
                break;
            case 'direct_debit':
                $text = "Direct debit";
                break;
        }
        return $this->__($text);
    }

    /**
     * Get the text belong to the delivery option name.
     * @param $name The name to find the text for.
     * @return string The translated text.
     */
    public function getDeliveryOptionTextForName($name)
    {
        $text = null;
        switch ($name) {
            case 'direct':
                $text = 'Take directly';
                break;
            case 'pickup':
                if (Mage::helper('agent')->isRetailStore()) {
                    $text = 'Pick up in another store';
                } else {
                    $text = 'Pick up in a store';
                }
                break;
            case 'deliver':
                $text = 'Deliver';
                break;
            case 'split':
                $text = 'Split delivery';
                break;
            case 'service_address':
                $text = 'Deliver';
                break;
            case 'other_address':
                $text = 'Deliver at other Address';
                break;
            case 'technician':
                $text = 'Technician at Service Address';
                break;
            case 'technician_asap':
                $text = 'Technician at Service Address ASAP';
                break;
        }
        return $this->__($text);
    }

    /**
     * Gets the customer.
     *
     * @return Omnius_Customer_Model_Customer_Customer The customer
     */
    protected function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    protected function hasMoreThanOnePackage($quote = null)
    {
        if (!$quote) {
            if ($this->_getCustomerSession()->getOrderEdit()) {
                $quote = Mage::getSingleton('checkout/cart')->getQuote(true);
            } else {
                $quote = $this->_getQuote();
            }
        }
        return count($quote->getPackages()) > 1;
    }

    /**
     * Checks whether the contract can be printed.
     * @param $orderIncrementId The order increment id to check for.
     * @return bool <true> if can be printed, <false> if not.
     */
    public function canPrintContract($orderIncrementId): bool
    {
        $order = Mage::getModel('sales/order')->getFirstNonEditedOrder($orderIncrementId);
        return $order->getId() && count(Mage::getModel('superorder/superorder')->belongsToCustomer($order->getSuperorderId())) > 0;
    }

    /**
     * Reset the number porting data for the given package
     * @param Vznl_Package_Model_Package $package
     * @return bool <true> if successful
     */
    public function clearNumberPortingData($package)
    {
        // Set mobile number
        $package->setCurrentNumber(null)
            ->setNumberPortingType(null)
            ->setConnectionType(null)
            ->setNumberPortingValidationType(null)
            ->setNumberPortingClientId(null)
            ->save();
        return true;
    }

    /**
     * Handle number porting with validation by SMS-code.
     * @param Vznl_Package_Model_Package $package
     * @param array $portingData The porting data
     * @return bool <true> if successful, <false> if not.
     * @throws Exception An exception is thrown when an invalid porting choice was chosen.
     */
    public function handleNumberPortingData($package, $portingData)
    {
        // Check if the current number has changed
        $mobileNumber = isset($portingData['mobile_number']) ? $portingData['mobile_number'] : null;
        // Get the validation choice
        $choice = isset($portingData['choice']) ? $portingData['choice'] : '0';
        $validationType = $this->getValidationChoice($choice);
        if ($validationType == null) {
            throw new Exception($this->__('Please choose a valid way to validate numberporting') . '.');
        }

        if (!$mobileNumber || empty($mobileNumber)) {
            throw new Exception($this->__('No mobile number given') . '.');
        }

        // Check if data is the same, number and validation type should have changed
        if ($mobileNumber && $mobileNumber == $package->getCurrentNumber() && $validationType == $package->getNumberPortingValidationType()) {
            return false;
        }

        if ($validationType == Vznl_Package_Model_Package::NP_VALIDATION_TYPE_CLIENT_ID) {
            $clientId = isset($portingData['client_id']) ? $portingData['client_id'] : null;
            if (!$clientId) {
                throw new Exception($this->__('A client id should be provided when chosing for validation on client id') . '.');
            }
            $package->setNumberPortingClientId($clientId);
        }
        // Set mobile number
        $package->setCurrentNumber($mobileNumber)
            ->setNumberPortingType(isset($portingData['customer']) ? ($portingData['customer'] == '0' ? 0 : 1) : null)
            ->setConnectionType(isset($portingData['connection']) ? ($portingData['connection'] == '0' ? 0 : 1) : null)
            ->setNumberPortingValidationType($validationType)
            ->save();

        return true;
    }

    /**
     * Gets the validation choice and checks if it it valid
     * @param string|integer $choice When string, will check if it is a valid value. When the right value will be returned if it exists.
     * @return string|null The validation choice.
     */
    protected function getValidationChoice($choice)
    {
        $validationChoices = [
            Vznl_Package_Model_Package::NP_VALIDATION_TYPE_SMART_VALIDATION,
            Vznl_Package_Model_Package::NP_VALIDATION_TYPE_CLIENT_ID
        ];

        // Check if the choice is already know, if so return it
        if (in_array($choice, $validationChoices)) {
            return $choice;
        }
        return isset($validationChoices[$choice]) ? $validationChoices[$choice] : null;
    }

    /**
     * Get the possible options with real index value
     * @return array
     */
    public function getFixedDeliveryOptionsIndexValueArray()
    {
        return [
            '' => 'service_address',
            'LSP' => 'other_address',
            'TECHNICIAN DELIVERY' => 'technician',
            'TECHNICIAN DELIVERY ASAP' => 'technician_asap',
            'SHOP' => 'direct',
        ];
    }

    /**
     * Gets the available delivery options for fixed.
     * @return array
     */
    public function getFixedDeliveryOptions()
    {
        $deliveryOptions = [];
        $currentQuote = $this->_getQuote();
        $deliveryMethod = explode(",", $currentQuote->getFixedDeliveryCheck());
        $isSeparateDeliveryAllowed = $currentQuote->getFixedSeparateDeliveryAllowed();
        if (in_array('LSP', $deliveryMethod) && $isSeparateDeliveryAllowed == "TRUE") {
            $deliveryOptions[] = 'LSP';
        }
        if (in_array('TECHNICIAN DELIVERY', $deliveryMethod)) {
            $deliveryOptions[] = 'technician';
        }
        if (in_array('TECHNICIAN DELIVERY ASAP', $deliveryMethod)) {
            $deliveryOptions[] = 'technician_asap';
        }

        return $deliveryOptions;
    }

    /**
     * Get Ziggo Donor Parties
     *
     * @return array
     */
    public function getZiggoDonorParties()
    {
        $donor_parties = Mage::getConfig()->getNode('default/checkout/ziggosettings/donnerlist');
        return explode(PHP_EOL,$donor_parties);
    }

    /**
     * Get gender prefix
     *
     * @param $genderValue
     *
     * @return string
     */
    public function getGenderPrefix($genderValue)
    {
        switch ($genderValue) {
            case 3:
                $prefix = 'Mr./Mrs.';
                break;
            case 2:
                $prefix = 'Mrs.';
                break;
            case 1:
                $prefix = 'Mr.';
                break;
            default:
                $prefix = '';
                break;
        }

        return $prefix;
    }
     public function getFixedmethod($method) {
        switch ($method) {
            case 'SHOP' :
                $fixedDeliveryText = "SHOP";
                break;
            case 'LSP' :
                $fixedDeliveryText = "LSP";
                break;
            case 'technician' :
                $fixedDeliveryText = "TECHNICIAN DELIVERY";
                break;
            case 'technician_asap' :
                $fixedDeliveryText = "TECHNICIAN DELIVERY ASAP";
                break;
            case 'oneday_technician_install' :
                $fixedDeliveryText = "TECHNICIAN INSTALL";
                break;
            default :
                $fixedDeliveryText = "";
        }
        return $fixedDeliveryText;

     }

     public function getFixedSelectedDeliveryWishDate($fixedDeliveryMethodData, $data) {
        if($fixedDeliveryMethodData == Vznl_Checkout_IndexController::CHECKOUT_DELIVERY_METHOD_CONTRACT) {
            $fixedDeliveryWishDate = $data->getRequest()->getParam('delivery_wish_date_LSP');
        } else if($fixedDeliveryMethodData == Vznl_Checkout_IndexController::DELIVERY_ONE_DAY_TECH_INSTALL){
            $fixedDeliveryWishDate = $data->getRequest()->getParam('delivery_wish_date_soho');
        } else {
            $fixedDeliveryWishDate = $data->getRequest()->getParam('delivery_wish_date_' . $fixedDeliveryMethodData);
        }
        return $fixedDeliveryWishDate;
     }

    /**
     * Return price_increase_scope
     * @return bool
     */
    public function getPriceIncreaseScope($items)
    {
        foreach ($items as $item) {
            $ProductId = $item->getProductId();
            $StoreId = $item->getStoreId();
            $product = Mage::getResourceModel('catalog/product');
            $priceIncreaseScope = $product->getAttributeRawValue($ProductId, 'price_increase_scope', $StoreId);
            if ($priceIncreaseScope) {
                return true;
            }
        }
        return false;
    }

    /*
     * Verifies if package contains SimOnly product
     * @return boolean
     */
    public function hasSimOnly($package)
    {
        $simOnlyCategory = Mage::getModel('catalog/category')->getCategoryByNamePath( 'Mobile/ALL_SIMONLY_TO', '/' );

        if ($simOnlyCategory) {
            foreach ($package->getAllItems() as $item) {
                $catalogProduct = $this->getCatalogProduct($item);
                $productCategories = $catalogProduct->getCategoryIds();

                if (in_array($simOnlyCategory->getId(), $productCategories)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string $createdAt
     * @return bool
     */
    public function isCartReadonly(string $createdAt)
    {
        $now = new \DateTime();
        $now->setTimestamp(Mage::getModel('core/date')->timestamp());

        $creationDate = new \DateTime();
        $creationDate->setTimestamp(Mage::getModel('core/date')->timestamp($createdAt));

        $daysSinceCreation = (int)$creationDate->diff($now)->format("%r%a");

        return ($this->getSavedOfferValidityPeriod() + $this->getSavedCartReadonlyPeriod()) < $daysSinceCreation;
    }


    /**
     * @param string $createdAt
     * @return bool
     */
    public function isOfferReadonly(string $createdAt)
    {
        $now = new \DateTime();
        $now->setTimestamp(Mage::getModel('core/date')->timestamp());

        $creationDate = new \DateTime();
        $creationDate->setTimestamp(Mage::getModel('core/date')->timestamp($createdAt));

        $daysSinceCreation = (int)$creationDate->diff($now)->format("%r%a");

        return ($this->getSavedOfferValidityPeriod() + $this->getSavedOfferReadonlyPeriod()) < $daysSinceCreation;
    }

    protected function getCatalogProduct($item)
    {
        return Mage::getModel('catalog/product')->load($item->getProduct()->getId());
    }

    protected function getSuperOrder($esbSuperOrderId)
    {
        return Mage::getModel('superorder/superorder')->load($esbSuperOrderId);
    }

    protected function getChildPackage($childOrder, $packageId)
    {
        return Mage::getModel('package/package')->getPackages($childOrder->getId(), null, $packageId)->getFirstItem();
    }

    protected function getQuoteWithId($quoteId)
    {
        return Mage::getModel('sales/quote')->load($quoteId);
    }

    public function setTelephoneNumber($extraRequired) {
        $quote = $this->_getQuote();
        $customerSession = Mage::getSingleton('customer/session');
        $customerData = $customerSession->getCustomerMode2();
        $customer = $customerData['Customer'];
        $result = [];
        if (!isset($customer[Vznl_Customer_Adapter_Search_Adapter::ZIGGO_CUSTOMER_STACK])) {
             $customer[Vznl_Customer_Adapter_Search_Adapter::ZIGGO_CUSTOMER_STACK] = $customer;
        }
        foreach ($customer as $key => $data) {
            $customerProduct = $data['Contract']['Subscription']['Product'];
            foreach ($customerProduct as $options => $value ) {
                $featureCatagories = $value['FeatureCategories'];
                $result = $this->getTelephoneFromResponse($featureCatagories);

                if($result['telephoneNo']) {
                    $quote->setFixedTelephoneNumber($result['telephoneNo']);
                }
                if($result['extraTelephoneNo'] && $extraRequired) {
                    $quote->setFixedExtraTelephoneNo($result['extraTelephoneNo']);
                }
            }
        }
        $quote->save();
    }

    public function getTelephoneFromResponse($featureCatagories) {
        foreach ($featureCatagories as $key) {
            if($key['Features']['Key'] == 'Tel_main_number') {
                $CustomerTelephoneNo = $key['Features']['FeatureLabelValue'];
            }
            if($key['Features']['Key'] == 'Tel_extra_number') {
                $CustomerExtraTelephoneNo = $key['Features']['FeatureLabelValue'];
            }
        }
        $result = array(
            'telephoneNo' => $CustomerTelephoneNo,
            'extraTelephoneNo' => $CustomerExtraTelephoneNo
        );

        return $result;
    }

    /**
     * Checks whether there is a Message Hint is there or not
     * @param array $rules
     * @return array
     */
    public function getMessageHint($rules)
    {
        $message_hint = array();
        $salesRules = Mage::getModel('salesrule/rule')->getCollection()
                      ->addFieldToSelect(array('rule_id', 'simple_action', 'sales_hint_message', 'sales_hint_title'))
                      ->addFieldToFilter('rule_id', array('in' => $rules))
                      ->addFieldToFilter('simple_action', array('eq' => self::SALES_HINT));

        foreach ($salesRules as $salesRule) {
            $message_hint['message_hint'] = array("id" => $salesRule->getData('rule_id'), "message" => $salesRule->getData('sales_hint_message'), "hint_title" => $salesRule->getData('sales_hint_title'));
        }

        return $message_hint;
    }

    public function showNewFormattedPriced($price = 0, $currencyInFront = true)
    {
        return str_replace(' ', '&thinsp;', parent::showNewFormattedPriced($price, $currencyInFront));
    }

    /**
     * Get Business Legal key from value
     * @param string $code
     * @return int
     */
    public function getBusinessLegalFormsKey($code)
    {
        $allOptions = $this->_prepareOptions(Mage::getStoreConfig('vodafone_customer/company_types/legal_forms'));
        foreach ($allOptions as $_code => $label) {
            if ($code == $label) {
                return $_code;
            }
        }
        return false;
    }
}
