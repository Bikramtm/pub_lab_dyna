<?php

/**
 * Class Pdf
 */
class Vznl_Checkout_Helper_Pdf extends Mage_Core_Helper_Abstract
{
    const DOCUMENT_LOG_NAME = 'document_saving.log';
    const PDF_EMAIL_PATH = '/var/contract_email';

    /**
     * Saves contract PDF locally and into SFTP job queue
     * @param string|null $contractOutput
     * @param string $fileIdentifier
     * @param string $ext
     * @param bool $isEmail
     * @return string
     * @throws Exception
     */
    public function saveContract(
        ?string $contractOutput,
        string $fileIdentifier,
        string $ext = 'pdf',
        bool $isEmail = false
    ):string
    {
        if (empty($contractOutput)) {
            $this->_logMessage(sprintf('[WARNING] Contract file is empty for order %s', $fileIdentifier));
        }

        $filename = $this->getPath($fileIdentifier, $ext, $isEmail);
        $dest = 'contract_'.$fileIdentifier.'.'.$ext;
        $this->saveDocument($filename, $contractOutput);

        if (!$isEmail) {
            $model = Mage::getModel('vznl_contract/contractjob');
            $model->addSftpJob($filename, $dest, $contractOutput);
        }

        return $filename;
    }

    /**
     * Saves offer PDF locally
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @return string
     * @throws Exception
     */
    public function saveOffer(
        Vznl_Checkout_Model_Sales_Quote $quote
    ):string
    {
        $printOutput = Mage::app()->getLayout()
            ->createBlock('vznl_checkout/pdf_offer')
            ->setQuote($quote)
            ->setTemplate('checkout/cart/pdf/offer.phtml')
            ->toHtml();

        $pdfInstance = new OmniusPdf;
        $pdfInstance->loadHtml($printOutput);
        $pdfInstance->render();
        $pdfOutput = $pdfInstance->output();

        $filename = $this->getOfferPath($quote->getId());
        $this->saveDocument($filename, $pdfOutput);

        return $filename;
    }

    /**
     * Gets the warranty file paths.
     * @param $order Vznl_Checkout_Model_Sales_Order The delivery order.
     * @param $packageItems Vznl_Checkout_Model_Sales_Order_Item[][] An array holding arrays with items sorted by package id as key.
     * @return array The warranty pdf file paths.
     * @throws Exception
     */
    public function saveWarranty(
        Vznl_Checkout_Model_Sales_Order $order,
        array $packages
    ):array
    {
        $filenames = array();

        foreach ($packages as $pid => $packageItems) {
            $warrantyOutput = $this->getWarrantyOutput($order, $packageItems, $pid);
            $warranties = $warrantyOutput->getWarrantyProducts();
            foreach ($warranties as $warranty) {
                /**
                 * Generate the PDF version of the HTML file
                 * */
                $pdfInstance = new OmniusPdf;

                $pdfInstance->loadHtml($warrantyOutput->setWarrantyProduct($warranty)->toHtml());
                $pdfInstance->render();
                $pdfOutput = $pdfInstance->output();

                $identifier = $order->getIncrementId() . '_' . $pid . '_' . $warranty->getId();
                $filename = $this->getWarrantyPath($identifier, 'pdf');
                if ($this->saveDocument($filename, $pdfOutput)) {
                    $filenames[] = $filename;
                }
            }
        }

        return $filenames;
    }

    /**
     * Gets the filepath of the loan overview for the given order(also creates the pdf there).
     * @param $order Vznl_Checkout_Model_Sales_Order The delivery order.
     * @param bool $isEmail
     * @param string $ext
     * @return string The loan overview file path.
     * @throws Exception
     */
    public function saveLoanOverview(
        Vznl_Checkout_Model_Sales_Order $order,
        bool $isEmail = false,
        string $ext = 'pdf'
    ):string
    {
        $pdfOutput = $this->getLoanPdfOutput($order);
        $identifier = $order->getIncrementId();
        $filename = $this->getLoanOverviewPath($identifier, 'pdf', $isEmail);
        $dest = 'loan_'.$identifier.'.'.$ext;
        $this->saveDocument($filename, $pdfOutput);
        if (!$isEmail) {
            $model = Mage::getModel('vznl_contract/contractjob');
            $model->addSftpJob($filename, $dest, $pdfOutput);
        }
        return $filename;
    }

    /**
     * Gets the warranty pdf output which can be used to return as body.
     * @param $order Vznl_Checkout_Model_Sales_Order The delivery order.
     * @param $packageItems Vznl_Checkout_Model_Sales_Order_Item[][] An array holding arrays with items sorted by package id as key.
     * @return string The warranty pdf output.
     */
    public function getWarrantyPdfOutput(
        Vznl_Checkout_Model_Sales_Order $order,
        array $packageItems
    ):string
    {
        /**
         * Generate the PDF version of the HTML file
         * */
        $pdfInstance = new OmniusPdf;
        $pdfHtml = '';
        foreach ($packageItems as $pid => $items) {
            $warrantyOutput = $this->getWarrantyOutput($order, $items, $pid);

            $warranties = $warrantyOutput->getWarrantyProducts();
            foreach ($warranties as $warranty) {
                if (!$pdfHtml == '') {
                    $pdfHtml .= '<div style="page-break-after:always">&nbsp;</div>';
                }
                $pdfHtml .= $warrantyOutput->setWarrantyProduct($warranty)->toHtml();
            }
        }

        $pdfInstance->loadHtml($pdfHtml);
        $pdfInstance->render();
        $pdfOutput = $pdfInstance->output();

        return $pdfOutput;
    }

    /**
     * Gets the loan overview pdf output which can be used to return as body.
     * @param $order Vznl_Checkout_Model_Sales_Order The delivery order.
     * @return string The loan overview pdf output.
     */
    public function getLoanPdfOutput(
        Vznl_Checkout_Model_Sales_Order $order
    ):string
    {
        /**
         * Generate the PDF version of the HTML file
         * */
        $pdfInstance = new OmniusPdf;
        $pdfHtml = '';

        /** @var Vznl_Package_Model_Package $package */
        foreach ($order->getDeliveryOrderPackages() as $package) {
            $hasDeviceRc = false;
            $items = $package->getPackageItems();
            /** @var Vznl_Checkout_Model_Sales_Order_Item $item */
            foreach ($items as $item) {
                $product = $item->getProduct();
                // Check if the product is of type device subscription and the loan option is set to either Loan or Loan + ILT
                $isDeviceRc = Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION), $product);
                if ($isDeviceRc && $product->isLoanRequired()) {
                    $hasDeviceRc = $item;
                }
            }

            // If not has device rc, skip
            if (!$hasDeviceRc) {
                continue;
            }

            $loanoverviewOutput = $this->getLoanOverviewOutput($package);
            if (!$pdfHtml == '') {
                $pdfHtml .= '<div style="page-break-after:always">&nbsp;</div>';
            }
            $pdfHtml .= $loanoverviewOutput->toHtml();
        }

        $pdfInstance->loadHtml($pdfHtml);
        $pdfInstance->render();
        $pdfOutput = $pdfInstance->output();

        return $pdfOutput;
    }

    /**
     * @param string $filename
     * @return string
     */
    private function _getTranslatedFilename(
        string $filename
    ): string
    {
        return (string)Mage::helper('vznl_checkout')->__($filename);
    }

    /**
     * @param $identifier
     * @param string $ext
     * @param $isEmail
     * @return string
     * @throws Exception
     */
    public function getPath(
        $identifier,
        $ext = 'pdf',
        $isEmail = false
    ):string
    {
        return sprintf('%s/contract_%s.%s', $this->assureDirExistence($isEmail), $identifier, $ext);
    }

    /**
     * @param $identifier
     * @param string $ext
     * @param bool $isEmail
     * @return string
     * @throws Exception
     */
    public function getLoanOverviewPath(
        string $identifier,
        string $ext = 'pdf',
        bool $isEmail = false
    ):string
    {
        return sprintf('%s/loan_%s.%s', $this->assureDirExistence($isEmail), $identifier, $ext);
    }

    /**
     * @param $identifier
     * @param string $ext
     * @return string
     * @throws Exception
     */
    public function getWarrantyPath(
        string $identifier,
        string $ext = 'pdf'
    ):string
    {
        return sprintf('%s/warranty_%s.%s', $this->assureDirExistence(true), $identifier, $ext);
    }

    /**
     * @param $identifier
     * @param string $ext
     * @return string
     * @throws Exception
     */
    public function getOfferPath(
        string $identifier,
        string $ext = 'pdf'
    ):string
    {
        $filename = sprintf('%s/'.$this->__('Offer').'_%s.%s', $this->assureDirExistence(true), $identifier, $ext);
        return $filename;
    }

    /**
     * Save the given output on the given filename.
     * @param string $filename The filename to save the output on.
     * @param string|null $output The output to save.
     * @return bool <true> if succeeded, <false> if the file isn't created or doesn't exist.
     * @throws Exception
     */
    protected function saveDocument(
        string $filename,
        ?string $output
    ):bool
    {
        $this->_logMessage(sprintf('Saving a document on \'%s\'', $filename));

        // try to put the file contents
        $io = new Varien_Io_File();
        $io->cd(dirname($filename));
        if (!$io->streamOpen($filename)) {
            $this->_logMessage(sprintf('[WARNING] Failed to open document data at path %s', $filename));
            return false;
        }
        if (!$io->streamWrite($output)) {
            $this->_logMessage(sprintf('[WARNING] Failed to save document data at path %s', $filename));
            return false;
        }
        $io->streamClose();
        // Set rights so both deamons and webserver can write
        $io->chmod($filename, 0777);

        $io = new Varien_Io_File();
        // Check if file exists
        if (!$io->fileExists($filename)) {
            $this->_logMessage(sprintf('[WARNING] Could not retrieve the document from path: \'%s\'', $filename));
            return false;
        }
        return true;
    }

    /**
     * @param $isEmail
     * @return string
     * @throws Exception
     */
    protected function assureDirExistence(
        bool $isEmail
    ):string
    {
        if (!$isEmail) {
            $path = Mage::getBaseDir() . '/var/contract';
        } else {
            $path = Mage::getBaseDir() . self::PDF_EMAIL_PATH;
        }
        $io = new Varien_Io_File();
        if (!realpath($path) && !$io->mkdir($path, 0777, true)) {
            Mage::throwException(sprintf('Failed to create contracts directory at path %s', $path));
        }
        return $path;
    }

    /**
     * @param $order Vznl_Checkout_Model_Sales_Order The delivery order.
     * @param $packageItems Vznl_Checkout_Model_Sales_Order_Item[][] An array holding arrays with items sorted by package
     * @param $pid string The package id.
     * @return Vznl_Checkout_Block_Pdf_Warranty|null The pdf warranty block or null.
     */
    private function getWarrantyOutput(
        Vznl_Checkout_Model_Sales_Order $order,
        ?array $packageItems,
        string $pid
    ):?Vznl_Checkout_Block_Pdf_Warranty
    {
        Mage::helper('vznl_checkout/email')->setCurrentPackageTheme();

        $date = $order->getCreatedAt();
        /** @var Vznl_Checkout_Model_TemplateVersion $templateVersion */
        $templateVersion = $this->getTemplateVersion(Vznl_Checkout_Model_TemplateVersion::TEMPLATETYPE_GARANT, $date);
        if (is_null($templateVersion)) {
            return null;
        }
        /**
         * Save warranty to disk
         */
        /** @var $warrantyOutput Vznl_Checkout_Block_Pdf_Warranty */
        $warrantyOutput = Mage::getSingleton('core/layout')
            ->createBlock('vznl_checkout/pdf_warranty')
            ->setTemplate($templateVersion->getPath())
            ->init($order, $packageItems, $pid);
        return $warrantyOutput;
    }

    /**
     * @param Vznl_Package_Model_Package $package The package to use.
     * @return Vznl_Checkout_Block_Pdf_Loanoverview The pdf loan overview
     */
    private function getLoanOverviewOutput(
        Vznl_Package_Model_Package $package
    ):?Vznl_Checkout_Block_Pdf_Loanoverview
    {
        /** @var $printOutput Vznl_Checkout_Block_Pdf_Loanoverview */
        $printOutput = Mage::getSingleton('core/layout')
            ->createBlock('vznl_checkout/pdf_loanoverview')
            ->setTemplate('checkout/cart/pdf/LoanOverview.phtml')
            ->init($package);
        return $printOutput;
    }

    /**
     * Gets the template version to use with the given type and date.
     *
     * @param $type string The type of template.
     * @param $date string The date where the template is used on.
     * @return null|Vznl_Checkout_Model_TemplateVersion The template version for the given type and date.
     */
    private function getTemplateVersion(
        string $type,
        string $date
    ):?Vznl_Checkout_Model_TemplateVersion
    {
        if (empty($type) || empty($date)) {
            return null;
        }

        $template = Mage::getModel('vznl_checkout/templateVersion');
        /** @var Vznl_Checkout_Model_Mysql4_TemplateVersion_Collection $collection */
        $collection = $template->getCollection();

        $collection->addFieldToSelect('*')
            ->addFieldToFilter('type', array('eq' => $type))
            ->addFieldToFilter('start_date', array('lteq' => $date))
            ->setOrder('start_date');

        foreach ($collection as $object) {
            return $object;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getTermsAndConditionsPath():string
    {
        return Mage::getBaseDir() . '/media/20181114_AV_ConsumentEnZakelijk_AV_Lening_Consument.pdf';
    }

    /**
     * Get the path for the garant terms PDF.
     *
     * @param null|Dyna_Superorder_Model_Superorder $order The delivery order.
     * @return null|string
     */
    public function getGarantTermsPath(
        Dyna_Superorder_Model_Superorder $order = null
    ):?string
    {
        return $this->getAttachmentPath(
            Vznl_Checkout_Model_TemplateVersion::TEMPLATETYPE_GARANT_TERMS,
            $order,
            '/media/Voorwaarden_Vodafone_Garant.pdf'
        );
    }

    /**
     * Get the path for the assurant IPID PDF.
     *
     * @param null|Dyna_Superorder_Model_Superorder $order The delivery order.
     * @return null|string
     */
    public function getAssurantIpidPath(
        Dyna_Superorder_Model_Superorder $order = null
    ):?string
    {

        return $this->getAttachmentPath(
            Vznl_Checkout_Model_TemplateVersion::TEMPLATETYPE_GARANT_IPID,
            $order
        );
    }

    /**
     * @param string $templateType
     * @param null|Dyna_Superorder_Model_Superorder $order The delivery order.
     * @param null|string $defaultPath
     * @return null|string
     */
    private function getAttachmentPath(
        string $templateType,
        ?Dyna_Superorder_Model_Superorder $order = null,
        ?string $defaultPath = null
    ):?string
    {
        $date = $order ? $order->getCreatedAt() : now();

        /** @var Vznl_Checkout_Model_TemplateVersion $templateVersion */
        $templateVersion = $this->getTemplateVersion($templateType, $date);
        $path = !is_null($templateVersion) ? $templateVersion->getPath() : $defaultPath;

        return $path ? Mage::getBaseDir() . $path : null;
    }
    
    /**
     * @param string $templateType
     * @param null|Dyna_Superorder_Model_Superorder $order The delivery order.
     * @param null|string $defaultPath
     * @return null|string
     */
    private function getAttachmentPathWithoutBaseDirectory(
        string $templateType,
        ?Dyna_Superorder_Model_Superorder $order = null,
        ?string $defaultPath = null
    ):?string
    {
        $date = $order ? $order->getCreatedAt() : now();

        /** @var Vznl_Checkout_Model_TemplateVersion $templateVersion */
        $templateVersion = $this->getTemplateVersion($templateType, $date);
        $path = !is_null($templateVersion) ? $templateVersion->getPath() : $defaultPath;

        return $path ? $path : null;
    }

    
    /**
     * @param $message $message
     * @return void
     */
    private function _logMessage(
        string $message
    ):void {
        Mage::log($message, null, self::DOCUMENT_LOG_NAME);
    }
    
        
    /**
     * Get the path for the IPID PDF link path without base directory.
     *
     * @param null|Dyna_Superorder_Model_Superorder $order The delivery order.
     * @return null|string
     */
    public function getAssurantIpidPLinkPath(
        Dyna_Superorder_Model_Superorder $order = null
    ):?string
    {

        return $this->getAttachmentPathWithoutBaseDirectory(
            Vznl_Checkout_Model_TemplateVersion::TEMPLATETYPE_GARANT_IPID,
            $order
        );
    }

}
