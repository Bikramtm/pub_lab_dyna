<?php

/**
 * Class Vznl_Checkout_Helper_Email
 */
class Vznl_Checkout_Helper_Email extends Mage_Core_Helper_Abstract
{
    const HOME_COPY_CHARGE_HIGH = 6.05;
    const HOME_COPY_CHARGE_LOW = 3.03;

    const ORDER_EMAIL_URL_PREFIX = 'vodafone_service/emails/order_url_%s';

    /**
     * Send the shopping cart confirmation to a given email
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param string $to
     * @return boolean
     */
    public function sendConfirmShoppingCart(Vznl_Checkout_Model_Sales_Quote $quote, $to)
    {
        if($quote->getId()) {
            $this->setCurrentPackageTheme();
        }
        $dealerId = Mage::helper('agent')->getDaDealerCode($quote->getAgent());
        $dealerChannel = Mage::helper('agent')->getWebsiteCode($quote->getWebsiteId());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();

        $from = $dealer->getSenderDomain();

        try
        {
            // Get template
            if($quote->getIsOffer())
            {
                $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_OFFER_MAIL;

                /** @var Vznl_Checkout_Helper_Pdf $pdfHelper */
                $pdfHelper = Mage::helper('vznl_checkout/pdf');
                $offerPdf = $pdfHelper->saveOffer($quote);
            }
            elseif($quote->getStoreId() == 5)
            {
                $emailCode = $quote->getCustomerIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SHOPPING_CART_ONLINE_BUSINESS : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SHOPPING_CART_ONLINE;
            }
            else
            {
                $emailCode = $quote->getCustomerIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SHOPPING_CART_BUSINESS  : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SHOPPING_CART;
            }

            $payload = array(
                'email_code' => $emailCode,
                'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
                'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),
                'parameters' => Mage::helper('communication/email')->getCommunicationParametersForQuote($quote),
                'receiver' => $to,
                'from' => $from,
                'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
            );

            if(isset($offerPdf)) {
                $payload['attachments'] = array_filter(array($offerPdf));
            }

            // Create job
            Mage::getSingleton('communication/pool')->add($payload);

            return true;

        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            return false;
        }
    }

    public function sendFillInILTLaterEmail($email, $customer, $orderNumber, $superOrder) {
        $dealerId = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $from = $dealer->getSenderDomain();

        Mage::getSingleton('communication/pool')->add(
            array(
                'email_code' => Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ILT_FILL_IN_LATER,
                'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
                'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),
                'receiver' => $email,
                'from' => $from,
                'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
                'parameters' => array(
                    array('Contact_Gender' => Mage::helper('vznl_checkout')->__(($customer->getGender() == 2) ? 'madam' : 'mister')),
                    array('Contact_LastName' => $customer->getMiddlename() ? $customer->getMiddlename() . ' ' . $customer->getLastname() : $customer->getLastname()),
                    array('SSFE_SaleOrderNumber' => $orderNumber),
                    array('Dealer' => $this->getDealerPayload($superOrder)),
                ),
            )
        );
    }

    public function sendOrderConfirmationEmail($superorderId = NULL, $quoteId = NULL)
    {
        $html = '';

        if ($superorderId) {
            $superorderModel = Mage::getModel('superorder/superorder')->load($superorderId);
            $isMobile = $superorderModel->hasMobilePackage(); // is Vodafone
            $isFixed = $superorderModel->hasFixed(); // is Ziggo
            $isConverged = $isMobile && $isFixed;
            $orders = $superorderModel->getOrders(true);
            $deliveryCosts = $superorderModel->getDeliveryCosts();
            $orderCreatedAt = $superorderModel->getCreatedAt();

            $parentSuperorderId = $superorderModel->getParentId();
            $parentOrderPackages = null;
            if ($parentSuperorderId) {
                $parentSuperorder =  Mage::getModel('superorder/superorder')->load($parentSuperorderId);
                $parentOrders = $parentSuperorder->getOrders(true);
                foreach ($parentOrders as $parentOrder) {
                    $parentOrderPackages[$parentOrder->getId()] = $parentOrder->getPackageIds();
                }
            }

            /** @var Vznl_Checkout_Model_Sales_Order $order */
            foreach ($orders as $order) {
                $hasRetention = $hasNumberPorting = false;
                $orderPackages = $order->getPackageIds();
                $packages = $order->getPackages(array('in' =>$orderPackages));
                $foundParentOrder = false;
                $difference = null;
                // On change before the payment method is needed from the order itself and not the parent
                $parentPaymentMethod = $order->getPayment() ? $order->getPayment()->getMethod() : null;
                foreach ($packages as $package) {

                    if ($package->isRetention()) {
                        $hasRetention = true;
                    }
                    if ($package->isNumberPorting()) {
                        $hasNumberPorting = true;
                    }
                    if ( ! $foundParentOrder) {
                        if(isset($parentOrderPackages) && is_array($parentOrderPackages)) {
                            foreach ($parentOrderPackages as $parentOrderId => $parentPackages) {
                                if (in_array($package->getPackageId(), $parentPackages)) {
                                    $parentOrder = Mage::getModel('sales/order')->load($parentOrderId);
                                    $parentOrderIsPaid = $parentOrder->isPaid();
                                    $parentPaymentMethod = $parentOrder->getPayment()->getMethod();
                                    $foundParentOrder = true;
                                }
                            }
                        }
                    }
                    if (Mage::getSingleton('customer/session')->getOrderEdit() && $package->isDelivered()) {
                        $packageDifference = Mage::helper('dyna_package')->getPackageDifferences($package->getPackageId());
                        if ( ! is_array($packageDifference) && ! empty($packageDifference)) {
                            if ($difference === null) {
                                $difference = 0;
                            }
                            $difference += Mage::helper('dyna_package')->getPackageDifferences($package->getPackageId());
                        }
                    }

                }

                $shippingAddress = $order->getShippingAddress();
                $shippingAddress->setData('street', $shippingAddress->getStreet());
                $deliveryAdd = Mage::helper("vznl_checkout")->formatAddressAsHtml($shippingAddress, true);

                $billingAddress = $order->getBillingAddress();
                $billingAddress->setData('street', $billingAddress->getStreet());
                $invoiceAdd = Mage::helper("vznl_checkout")->formatAddressAsHtml($billingAddress, true);


                $customer = Mage::getModel('customer/customer')->load($superorderModel->getCustomerId());

                if($invoiceAdd == $deliveryAdd && $customer->getIsBusiness()) {
                    $showCompanyName = $customer->getCompanyName();
                }

                $paymentMethod = $order->getPayment()->getMethod();
                $shippingAddress = $order->getShippingAddress();
                $shippingAddress->setData('street', $shippingAddress->getStreet()); // Set street as array
                $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
                $this->setCurrentPackageTheme();
                $html .= Mage::getSingleton('core/layout')
                    ->createBlock('core/template')
                    ->setIsIls((bool)$superorderModel->getIsIls())
                    ->setPackages($packages)
                    ->setDifference($difference)
                    ->setCustomer($customer)
                    ->setIsBusiness($order->isAikido() ? false : $customer->getIsBusiness())
                    ->setOrderEdit(Mage::getSingleton('customer/session')->getOrderEdit())
                    ->setHasRetention($hasRetention)
                    ->setIsPaid($order->isPaid())
                    ->setSigningDate($this->getSigningDate($order))
                    ->setShowCustomerCompany(isset($showCompanyName) ? $showCompanyName : false)
                    ->setHasNumberPorting($hasNumberPorting)
                    ->setDeliveryAddress($shippingAddress)
                    ->setDeliveryCosts($deliveryCosts)
                    ->setIsPaidParent(isset($parentOrderIsPaid) ? $parentOrderIsPaid : null)
                    ->setOrderCreateDate($orderCreatedAt)
                    ->setPaymentMethod($paymentMethod)
                    ->setParentPaymentMethod(isset($parentPaymentMethod) ? $parentPaymentMethod : null)
                    ->setIsConverged($isConverged)
                    ->setTemplate('order/partials/packages_list.phtml')
                    ->toHtml();
            }
        }

        if ($quoteId) {
            /** @var $quoteModel Vznl_Checkout_Model_Sales_Quote */
            $quoteModel = Mage::getModel('sales/quote')->loadByIdWithoutStore($quoteId);
            if ( ! $quoteModel) {
                throw new Exception("Quote not found");
            }
            $packages = Mage::getModel('package/package')->getPackages(null, $quoteId);
            $deliveryCosts = NULL;
            $orderCreatedAt = NULL;
            $deliveryAddress = NULL;

            $this->setCurrentPackageTheme();
            $html .= Mage::getSingleton('core/layout')
                ->createBlock('core/template')
                ->setPackages($packages)
                ->setIsIls(false)
                ->setIsFixed($isFixed)
                ->setIsBusiness($quoteModel->isAikido() ? false : ($quoteModel->getCustomer()->getId() ? $quoteModel->getCustomer()->getIsBusiness() : $quoteModel->getCustomerIsBusiness()))
                ->setQuote($quoteModel)
                ->setTemplate('order/partials/packages_list.phtml')
                ->toHtml();
        }

        return $html;
    }

    public function getPackageItemsLines($packageItems, $coupon = null, $isIls = false, $isBusiness = false, $isRetention = false)
    {
        $itemHtml = array();
        // money format handler
        $coreHelper = Mage::helper('core');
        $money = function($price) use($coreHelper) { return $coreHelper->currency($price, true, false); };
        $productGroupCollection = Vznl_Package_Model_Package::getEmailProductOrder($isIls);
        $deviceRC = null;
        $subscription = null;
        $isAikido = false;
        /** @var vznl_checkout/aikido_Helper_Data $aikidoHelper */
        $aikidoHelper = Mage::helper('vznl_checkout/aikido');
        /** @var Vznl_Checkout_Model_Sales_Order_Item|Vznl_Checkout_Model_Sales_Quote_Item $additional */
        foreach ($packageItems as $additional) {
            if ($additional->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                $deviceRC = $additional;
            }

            if ($additional->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)) {
                $subscription = $additional;
                $isAikido = (bool) $subscription->getProduct()->getAikido();
            }
        }
        $priceRulesHelper = Mage::helper('pricerules');

        foreach($productGroupCollection as $productType) {
            /** @var Vznl_Checkout_Model_Sales_Quote_Item|Vznl_Checkout_Model_Sales_Order_Item $item */
            foreach ($packageItems as $item) {

                if(!in_array($productType, $item->getProduct()->getType())) {
                    continue;
                }
                // isServiceItem() disabled
                if($item->getProduct()->isServiceItem()) {
                    continue;
                }
                $product = $item->getProduct();
                if ($product->is(Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD) && !$isIls) {
                    continue;
                }
                $showMafZeroPrices = ($product->is(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) ||
                    $product->is(Vznl_Catalog_Model_Type::SUBTYPE_ADDON)
                    || $product->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)
                ); // showMaf Price if $0 only for subscription & addons (requested change to also show devices)
                $showOneTimeZeroPrices = ($product->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE) ||
                    $product->is(Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY) ||
                    $product->is(Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD)); // show onetime prices if $0 for device & accessory

                if(!in_array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE, $product->getType()) && $product->isPromo()) continue;

                $itemHtml[$productType][$item->getId()]['name'] = $item->getName();
                $itemHtml[$productType][$item->getId()]['maf'] = $item->getMaf() && ($item->getMaf() != 0 || $showMafZeroPrices) ? $money($item->getMaf()): '';
                $itemHtml[$productType][$item->getId()]['mafTax'] = $item->getMafInclTax() && ($item->getMafInclTax() != 0 ||$showMafZeroPrices) ? $money($item->getMafInclTax()): '';
                $itemHtml[$productType][$item->getId()]['price'] = $item->getRowTotal() && ($item->getRowTotal() != 0 || $showOneTimeZeroPrices ) ? $money($item->getRowTotal()): '';
                $itemHtml[$productType][$item->getId()]['priceTax'] = $item->getRowTotalInclTax() && ($item->getRowTotalInclTax() != 0 || $showOneTimeZeroPrices) ? $money($item->getRowTotalInclTax()): '';

                if ($product->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE) && $isAikido && $deviceRC != null) {
                    $itemHtml[$productType][$item->getId()]['maf'] =
                        $deviceRC->getMaf() && ($deviceRC->getMaf() != 0 || $showMafZeroPrices)
                            ? $money($deviceRC->getItemFinalMafInclTax())
                            : '';
                    $itemHtml[$productType][$item->getId()]['mafTax'] =
                        $deviceRC->getMafInclTax() && ($deviceRC->getMafInclTax() != 0 || $showMafZeroPrices)
                            ? $money($deviceRC->getItemFinalMafInclTax())
                            : '';

                    $itemHtml[$productType][$item->getId()]['subgroup'][] = array(
                        'name' => '<i style="font-style: italic;">'
                            . $this->__('from ') . $money(Mage::helper('tax')->getPrice($product, $product->getFinalPrice(), !$isBusiness )) . ' '
                            . $this->__('to ') . $money(Mage::helper('tax')->getPrice($product, $item->getMixmatchSubtotal() + $item->getMixmatchTax(), !$isBusiness))
                            .'</i>'
                    );

                    if ($deviceRC->getSku() != $aikidoHelper->getDefaultRetentionSku() && $deviceRC->getSku() != $aikidoHelper->getDefaultAcquisitionSku()) {
                        $itemHtml[$productType][$item->getId()]['subgroup'][] = array(
                            'name' => ''
                        );

                        $itemHtml[$productType][$item->getId()]['subgroup'][] = array(
                            'name' =>
                                '<table style="width: 100%">
<tbody>
<tr>
<td>
<i style="font-style: italic; FONT-SIZE: 13px; FONT-FAMILY: Arial,Verdana, sans-serif; COLOR: #7D7D7D; PADDING-BOTTOM: 0; PADDING-TOP: 0; PADDING-LEFT: 0; PADDING-RIGHT: 0; VERTICAL-ALIGN: top; ">
' . $this->__('Hardware price') .'
</i>
</td>
<td>
<i style="font-style: italic; FONT-SIZE: 13px; FONT-FAMILY: Arial,Verdana, sans-serif; COLOR: #7D7D7D; PADDING-BOTTOM: 0; PADDING-TOP: 0; PADDING-LEFT: 0; PADDING-RIGHT: 0; VERTICAL-ALIGN: top; ">
&euro;
</i>
</td>
<td style="text-align: right;">
<i style="font-style: italic; FONT-SIZE: 13px; FONT-FAMILY: Arial,Verdana, sans-serif; COLOR: #7D7D7D; PADDING-BOTTOM: 0; PADDING-TOP: 0; PADDING-LEFT: 0; PADDING-RIGHT: 0; VERTICAL-ALIGN: top; ">
'. $priceRulesHelper->customPriceFormat($item->getRowTotalInclTax() && ($item->getRowTotalInclTax() != 0 || $showOneTimeZeroPrices) ? $item->getRowTotalInclTax(): 0) .'
</i>
</td>
</tr>

<tr>
<td>
<i style="font-style: italic; FONT-SIZE: 13px; FONT-FAMILY: Arial,Verdana, sans-serif; COLOR: #7D7D7D; PADDING-BOTTOM: 0; PADDING-TOP: 0; PADDING-LEFT: 0; PADDING-RIGHT: 0; VERTICAL-ALIGN: top; ">
' . sprintf($this->__('%d months %s'), (int) $subscription->getProduct()->getSubscriptionDuration(), !$isBusiness ? $itemHtml[$productType][$item->getId()]['mafTax'] : $itemHtml[$productType][$item->getId()]['maf'])  .'
</i>
</td>
<td>
<i style="font-style: italic; FONT-SIZE: 13px; FONT-FAMILY: Arial,Verdana, sans-serif; COLOR: #7D7D7D; PADDING-BOTTOM: 0; PADDING-TOP: 0; PADDING-LEFT: 0; PADDING-RIGHT: 0; VERTICAL-ALIGN: top; ">
&euro;
</i>
</td>
<td style="text-align: right;">
<i style="font-style: italic; FONT-SIZE: 13px; FONT-FAMILY: Arial,Verdana, sans-serif; COLOR: #7D7D7D; PADDING-BOTTOM: 0; PADDING-TOP: 0; PADDING-LEFT: 0; PADDING-RIGHT: 0; VERTICAL-ALIGN: top; ">
'. $priceRulesHelper->customPriceFormat($deviceRC->getTotalPaidByMaf()) .'
</i>
</td>
</tr>
<tr>
<td style="
font-size: 1px;
padding: 0;
margin: 0;
">
&nbsp;
</td>
<td colspan="2" style="
margin: 0;
font-size: 1px;
    padding: 0;
    border-bottom: 1px solid #7D7D7D;
">
&nbsp;
</td>
</tr>
<tr>
<td>
<i style="font-style: italic; FONT-SIZE: 13px; FONT-FAMILY: Arial,Verdana, sans-serif; COLOR: #7D7D7D; PADDING-BOTTOM: 0; PADDING-TOP: 0; PADDING-LEFT: 0; PADDING-RIGHT: 0; VERTICAL-ALIGN: top; ">
' . $this->__('Base price ') .'
</i>
</td>
<td>
<i style="font-style: italic; FONT-SIZE: 13px; FONT-FAMILY: Arial,Verdana, sans-serif; COLOR: #7D7D7D; PADDING-BOTTOM: 0; PADDING-TOP: 0; PADDING-LEFT: 0; PADDING-RIGHT: 0; VERTICAL-ALIGN: top; ">
&euro;
</i>
</td>
<td style="text-align: right;">
<i style="font-style: italic; FONT-SIZE: 13px; FONT-FAMILY: Arial,Verdana, sans-serif; COLOR: #7D7D7D; PADDING-BOTTOM: 0; PADDING-TOP: 0; PADDING-LEFT: 0; PADDING-RIGHT: 0; VERTICAL-ALIGN: top; ">
'. $priceRulesHelper->customPriceFormat(Mage::helper('tax')->getPrice($product, $item->getMixmatchSubtotal() + $item->getMixmatchTax(), !$isBusiness)) .'
</i>
</td>
</tr>
</tbody>
</table>'
                        );
                    }

                    $itemHtml[$productType][$item->getId()]['subgroup'][] = array(
                        'name' => '&nbsp;'
                    );
                }

                if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $product->getType())
                ||
                    in_array(Vznl_Catalog_Model_Type::SUBTYPE_ADDON, $product->getType())
                ) {
                    if ($item->getMafDiscountAmount() > 0) {
                        $discountNames = [];
                        /** @var Vznl_Checkout_Model_Sales_Order_Item $itemPromo */
                        foreach ($packageItems as $itemPromo) {
                            if (
                                $itemPromo->isPromo()
                                && $item->getProductId() == $itemPromo->getTargetId()
                                && ($itemPromo->getProduct()->getPrijsMafNewAmount() || $itemPromo->getProduct()->getPrijsMafDiscountPercent())
                            ) {
                                $discountNames[] = $itemPromo->getName();
                            }
                        }
                        $itemHtml[$productType][$item->getId()]['subgroup'][] = ['name' => '<i style="font-style: italic; ">' . implode(', ', $discountNames) . '</i>'];

                        $itemHtml[$productType][$item->getId()]['promoPrice'] = $item->getItemFinalMafExclTax();
                        $itemHtml[$productType][$item->getId()]['promoPriceTax'] = $item->getItemFinalMafInclTax();
                    }
                }

                if(in_array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $product->getType())) {
                    $connectionCost = $item->getConnectionCost();
                    if(!empty($connectionCost) && $connectionCost > 0) {
                        if (!$isRetention) {
                            $connectionCostExcl = Mage::helper('tax')->getPrice($item->getProduct(), $connectionCost, false);
                            $itemHtml[$productType][$item->getId()]['connection_fee']['name'] = $this->__('Connection Fee');
                            $itemHtml[$productType][$item->getId()]['connection_fee']['price'] = $connectionCostExcl;
                            $itemHtml[$productType][$item->getId()]['connection_fee']['priceTax'] = $connectionCost;
                        } else {
                            $itemHtml[$productType][$item->getId()]['connection_fee']['name'] = $this->__('No Connection Fee');
                            $itemHtml[$productType][$item->getId()]['connection_fee']['price'] = 0;
                            $itemHtml[$productType][$item->getId()]['connection_fee']['priceTax'] = 0;
                        }
                    }
                    if ($isRetention) {
                        $itemHtml[$productType][$item->getId()]['connection_fee']['name'] = $this->__('No Connection Fee');
                        $itemHtml[$productType][$item->getId()]['connection_fee']['price'] = 0;
                        $itemHtml[$productType][$item->getId()]['connection_fee']['priceTax'] = 0;
                    }
                }

                if(in_array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE, $product->getType())) {
                    $imageSrc = Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize(70);
                    $imageSrc = str_replace('restapi', 'telesales', $imageSrc);
                    $itemHtml[$productType][$item->getId()]['image']  = '<img src="' . $imageSrc . '" alt="Image" />';
                    $weeeTaxApplied = $item->getWeeeTaxAppliedUnserialized();
                    if(!empty($weeeTaxApplied)) {
                        foreach($weeeTaxApplied as $taxApplied) {
                            $connCost = Mage::helper('tax')->getPrice(
                                $item->getProduct(),
                                $taxApplied['base_amount_incl_tax'],
                                true
                            );
                            $connCostExcl = Mage::helper('tax')->getPrice(
                                $item->getProduct(),
                                $taxApplied['base_amount_incl_tax'],
                                false
                            );
                            $connCostFormatted = Mage::helper('core')->currency($connCost, true, false);
                            $connCostExclFormatted = Mage::helper('core')->currency($connCostExcl, true, false);

                            $taxName = 'Home copy charge';

                            $itemHtml[$productType][$item->getId()]['subgroup'][] = array(
                                'name' => Mage::helper("vznl_checkout")->__($taxName),
                                'price' => $connCostExclFormatted,
                                'priceTax' => $connCostFormatted,
                            );
                        }
                    }
                    $tmpPackageItems = $packageItems;
                    /** @var Vznl_Checkout_Model_Sales_Quote_Item $tmpItem */
                    foreach ($tmpPackageItems as $tmpItem) {
                        $tmpProduct = $tmpItem->getProduct();
                        if ($tmpProduct->is(Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD)) {
                            $itemHtml[$productType][$item->getId()]['subgroup'][] = array(
                                'name' => $tmpItem->getName(),
                                'price' => $money($tmpItem->getRowTotal()),
                                'priceTax' => $money($tmpItem->getRowTotalInclTax()),
                            );
                        }

                        if ($tmpProduct->isServiceItem()) {
                            $itemHtml[$productType][$item->getId()]['subgroup'][] = array(
                                'name' => $tmpItem->getName(),
                                'price' => $money($tmpItem->getRowTotal()),
                                'priceTax' => $money($tmpItem->getRowTotalInclTax()),
                            );
                        }

                        if(Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_NONE), $tmpProduct) && $tmpProduct->isServiceItem()) {
                            $itemHtml[$productType][$item->getId()]['subgroup'][] = array(
                                'name' => $tmpItem->getName(),
                                'price' => $money($tmpItem->getRowTotal()),
                                'priceTax' => $money($tmpItem->getRowTotalInclTax()),
                            );
                        }
                    }

                    if($coupon) {
                        $coupon = Mage::getModel('salesrule/coupon')->load($coupon, 'code');
                        $rule = Mage::getModel('salesrule/rule')->load($coupon->getRuleId());

                        $itemHtml[$productType][$item->getId()]['subgroup'][] = array(
                            'name' => $this->__('Extra hardware discount') . '<br />' . PHP_EOL . '<i style="font-size: 11px">' . $rule->getName() . '</i>',
                            'price' => $money(-($item->getDiscountAmount() - $item->getHiddenTaxAmount())),
                            'priceTax' => $money(-$item->getDiscountAmount()),
                        );
                    }
                }
            }
        }

        return $itemHtml;
    }

    /**
     * If helper is called from a shell script, we need to make sure the current package and theme are set accordingly
     */
    public function setCurrentPackageTheme()
    {
        if(Mage::getSingleton('core/design_package')->getPackageName() == Mage_Core_Model_Design_Package::DEFAULT_PACKAGE) {
            $website = Mage::getModel('core/website')->load(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE);
            Mage::getDesign()->setArea('frontend')
                ->setPackageName(Mage::getStoreConfig('design/package/name', $website->getDefaultStore()->getId()))
                ->setTheme(Mage::getStoreConfig('design/theme/template', $website->getDefaultStore()->getId()));
        }
    }

    /**
     * @param $customer
     * @return mixed
     */
    private function getOrderUrl($customer) {
        return Mage::getStoreConfig(sprintf(self::ORDER_EMAIL_URL_PREFIX, ($customer->getIsBusiness() ? 'business' : 'customer')));
    }

    /**
     * @param Vznl_Superorder_Model_Superorder $superorder
     * @param Omnius_Customer_Model_Customer_Customer $customer
     * @return string
     */
    public function generateOrderUrl(Vznl_Superorder_Model_Superorder $superorder, Omnius_Customer_Model_Customer_Customer $customer) {
        $orderUrl = Mage::getStoreConfig(sprintf(self::ORDER_EMAIL_URL_PREFIX, ($customer->getIsBusiness() ? 'business' : 'customer')));
        $sharedKey = Mage::getStoreConfig('vodafone_service/webshop/shoppingcart_key');
        $hash = hash('sha512', $sharedKey . ';' . $customer->getCorrespondanceEmail() . ';' . $superorder->getOrderNumber());
        $orderUrl .= htmlentities('?d_id='.$superorder->getOrderNumber().'&email_address='.urlencode($customer->getCorrespondanceEmail()).'&signature='.$hash);
        return $orderUrl;
    }

    /**
     * @param string $date
     * @return null|string
     */
    protected function stripTimeFromDate($date)
    {
        if (!$date) {
            return null;
        }
        $date = new DateTime($date);
        return $date->format('d-m-Y');
    }

    /**
     * @param Vznl_Checkout_Model_Sales_Order $order
     * @return null|string
     */
    protected function getSigningDate($order) {
        if(!$order->getContractSignDate()) {
            return $this->stripTimeFromDate(date("Y-m-d"));
        }

        return $this->stripTimeFromDate($order->getContractSignDate());
    }

    protected function getDealerPayload($quote) {
        $dealerId = Mage::helper('agent')->getDaDealerCode($quote->getAgent());
        $dealerChannel = Mage::helper('agent')->getWebsiteCode($quote->getWebsiteId());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();

        return array(
            'DealerGroup' => $dealerId,
            'DealerGroupChannel' => $dealerChannel,
            'PrimaryBrand' => $primaryBrand,
        );
    }
}
