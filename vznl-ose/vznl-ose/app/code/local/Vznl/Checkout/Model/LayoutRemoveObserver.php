<?php

/**
 * Class LayoutRemoveObserver
 */
class Vznl_Checkout_Model_LayoutRemoveObserver extends Mage_Core_Model_Abstract
{
    const ROOTNODE = 'root';

    public function unremoveUpdate($observer)
    {
        $update = $observer->getLayout()->getUpdate();
        $originalUpdates = $update->asArray();
        $update->resetUpdates();

        $toUnremove = $this->_getUnremoveNames($this->_getSimplexmlFromFragment(implode('', $originalUpdates)));

        foreach ($originalUpdates as $sXmlUpdate) {
            $update->addUpdate($this->_processUnremoveNodes($sXmlUpdate, $toUnremove));
        }
    }

    protected function _processUnremoveNodes($string, $toUnremove)
    {
        $oXmlUpdate = $this->_getSimplexmlFromFragment($string);
        $nodes = $oXmlUpdate->xpath('//remove');
        foreach ($nodes as $node) {
            if (in_array($node['name'], $toUnremove)) {
                unset($node['name']);
            }
        }

        $sXml = '';
        foreach ($oXmlUpdate->children() as $node) {
            $sXml .= $node->asXml();
        }
        return $sXml;

    }

    protected function _getUnremoveNames($xml)
    {
        $nodes = $xml->xpath('//unremove');
        $unRemove = array();
        foreach ($nodes as $node) {
            $unRemove[] = (string)$node['name'];
        }
        return $unRemove;
    }

    protected function _getSimplexmlFromFragment($fragment)
    {
        return simplexml_load_string('<' . self::ROOTNODE . '>' . $fragment . '</' . self::ROOTNODE . '>');
    }
}
