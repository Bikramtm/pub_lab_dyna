<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/**
 * Portability model
 *
 * @category    Vznl
 * @package     Vznl_Checkout
 */
class Vznl_Checkout_Model_Portability extends Omnius_Checkout_Model_Portability
{
    /**
     * @param array $info
     * @throws Exception
     */
    public function insertPortabilityInfo($info)
    {
        if (!empty($info)) {
            foreach ($info as $data) {
                if (!empty($data['validation_type'])) {
                    switch (strtoupper($data['validation_type'])) {
                        case 'CUSTOMER_NUMBER':
                            $data['validation_type'] = 'CustomerID';
                            break;
                        case 'SMART_VAL':
                            $data['validation_type'] = 'SmartVal';
                            break;
                    }
                }
                $this->setData($data);
                $this->save();
            }
        }
    }

    /**
     * @param string $quoteId
     * @param string $packageId
     * @return array
     */
    public function retrievePortingInfo($quoteId, $packageId = null)
    {
        $portabilityInfo = parent::retrievePortingInfo($quoteId, $packageId);

        if (!empty($portabilityInfo)) {
            foreach ($portabilityInfo as $key => $data) {
                if (!empty($data['validation_type'])) {
                    switch ($data['validation_type']) {
                        case 'CustomerID':
                            $portabilityInfo[$key]['validation_type'] = 'CUSTOMER_NUMBER';
                            break;
                        case 'SmartVal':
                            $portabilityInfo[$key]['validation_type'] = 'SMART_VAL';
                            break;
                    }
                }
            }
        }
        return $portabilityInfo;
    }
}
