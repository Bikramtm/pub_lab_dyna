<?php
/**
 * Tax totals calculation model
 */
class Vznl_Checkout_Model_Tax_Sales_Total_Quote_Tax extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    /**
     * Tax module helper
     *
     * @var Mage_Tax_Helper_Data
     */
    protected $_helper;

    /**
     * Tax calculation model
     *
     * @var Mage_Tax_Model_Calculation
     */
    protected $_calculator;

    /**
     * Tax configuration object
     *
     * @var Mage_Tax_Model_Config
     */
    protected $_config;

    /**
     * Flag which is initialized when collect method is start.
     * Is used for checking if store tax and customer tax requests are similar
     *
     * @var bool
     */
    protected $_areTaxRequestsSimilar = false;

    /**
     * Array for the rounding deltas
     *
     * @var array
     */
    protected $_roundingDeltas = array();

    /**
     * Array for the base rounding deltas
     *
     * @var array
     */
    protected $_baseRoundingDeltas = array();

    /**
     * @var Mage_Core_Model_Store
     */
    protected $_store;

    /**
     * Hidden taxes array
     *
     * @var array
     */
    protected $_hiddenTaxes = array();


    /**
     * Weee helper class
     *
     * @var Mage_Weee_Helper_Data
     */
    protected $_weeeHelper;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->setCode('maf_tax');
        $this->_helper = Mage::helper('tax');
        $this->_calculator = Mage::getSingleton('tax/calculation');
        $this->_config = Mage::getSingleton('tax/config');
        $this->_weeeHelper = Mage::helper('weee');
    }

    /**
     * Round the total amounts in address
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Mage_Tax_Model_Sales_Total_Quote_Tax
     */
    protected function _roundTotals(Mage_Sales_Model_Quote_Address $address)
    {
        // initialize the delta to a small number to avoid non-deterministic behavior with rounding of 0.5
        $totalDelta = 0.000001;
        $baseTotalDelta = 0.000001;
        /*
         * The order of rounding is import here.
         * Tax is rounded first, to be consistent with unit based calculation.
         * Hidden tax and shipping_hidden_tax are rounded next, which are really part of tax.
         * Shipping is rounded before subtotal to minimize the chance that subtotal is
         * rounded differently because of the delta.
         * Here is an example: 19.2% tax rate, subtotal = 49.95, shipping = 9.99, discount = 20%
         * subtotalExclTax = 41.90436, tax = 7.7238, hidden_tax = 1.609128, shippingPriceExclTax = 8.38087
         * shipping_hidden_tax = 0.321826, discount = -11.988
         * The grand total is 47.952 ~= 47.95
         * The rounded values are:
         * tax = 7.72, hidden_tax = 1.61, shipping_hidden_tax = 0.32,
         * shipping = 8.39 (instead of 8.38 from simple rounding), subtotal = 41.9, discount = -11.99
         * The grand total calculated from the rounded value is 47.95
         * If we simply round each value and add them up, the result is 47.94, which is one penny off
         */
        $totalCodes = array('maf_tax', 'maf_subtotal', 'maf_discount');
        foreach ($totalCodes as $totalCode) {
            $exactAmount = $address->getMafTotalAmount($totalCode);
            $baseExactAmount = $address->getBaseMafTotalAmount($totalCode);
            if (!$exactAmount && !$baseExactAmount) {
                continue;
            }
            $roundedAmount = $this->_calculator->round($exactAmount + $totalDelta);
            $baseRoundedAmount = $this->_calculator->round($baseExactAmount + $baseTotalDelta);
            $address->setMafTotalAmount($totalCode, $roundedAmount);
            $address->setBaseMafTotalAmount($totalCode, $baseRoundedAmount);
            $totalDelta = $exactAmount + $totalDelta - $roundedAmount;
            $baseTotalDelta = $baseExactAmount + $baseTotalDelta - $baseRoundedAmount;
        }
        return $this;
    }

    /**
     * Collect tax totals for quote address
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Tax_Model_Sales_Total_Quote_Tax
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $this->_setAddress($address);
        /**
         * Reset amounts
         */
        $this->_setMafAmount(0);
        $this->_setBaseMafAmount(0);

        $this->_roundingDeltas = array();
        $this->_baseRoundingDeltas = array();
        $this->_hiddenTaxes = array();

        $this->_store = $address->getQuote()->getStore();
        $customer = $address->getQuote()->getCustomer();
        if ($customer) {
            $this->_calculator->setCustomer($customer);
        }

        if (!$address->getAppliedTaxesReset()) {
            $address->setAppliedMafTaxes(array());
        }

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }
        $request = $this->_calculator->getRateRequest(
            $address,
            $address->getQuote()->getBillingAddress(),
            $address->getQuote()->getCustomerTaxClassId(),
            $this->_store
        );

        if ($this->_config->priceIncludesTax($this->_store)) {
            $this->_areTaxRequestsSimilar = $this->_calculator->compareRequests(
                $this->_calculator->getRateOriginRequest($this->_store),
                $request
            );
        }

        switch ($this->_config->getAlgorithm($this->_store)) {
            case Mage_Tax_Model_Calculation::CALC_UNIT_BASE:
                $this->_unitBaseCalculation($address, $request);
                break;
            case Mage_Tax_Model_Calculation::CALC_ROW_BASE:
                $this->_rowBaseCalculation($address, $request);
                break;
            case Mage_Tax_Model_Calculation::CALC_TOTAL_BASE:
                $this->_totalBaseCalculation($address, $request);
                break;
            default:
                break;
        }

        $this->_addMafAmount($address->getExtraTaxAmount());
        $this->_addBaseMafAmount($address->getBaseExtraTaxAmount());

        $this->_processHiddenTaxes();

        //round total amounts in address
        $this->_roundTotals($address);
        return $this;
    }

    /**
     * Process hidden taxes for items and shippings (in accordance with hidden tax type)
     *
     * @return void
     */
    protected function _processHiddenTaxes()
    {
        $this->_getAddress()->setMafTotalAmount('hidden_maf_tax', 0);
        $this->_getAddress()->setBaseMafTotalAmount('hidden_maf_tax', 0);
        foreach ($this->_hiddenTaxes as $taxInfoItem) {
            if (isset($taxInfoItem['item'])) {
                // Item hidden taxes
                $item = $taxInfoItem['item'];
                $hiddenTax = $taxInfoItem['value'];
                $baseHiddenTax = $taxInfoItem['base_value'];
                $qty = $taxInfoItem['qty'];

                $hiddenTax = $this->_calculator->round($hiddenTax);
                $baseHiddenTax = $this->_calculator->round($baseHiddenTax);
                $item->setHiddenMafTaxAmount(max(0, $qty * $hiddenTax));
                $item->setBaseHiddenMafTaxAmount(max(0, $qty * $baseHiddenTax));
                $this->_getAddress()->addMafTotalAmount('hidden_maf_tax', $item->getHiddenMafTaxAmount());
                $this->_getAddress()->addBaseMafTotalAmount('hidden_maf_tax', $item->getBaseHiddenMafTaxAmount());
            }
        }
    }

    /**
     * Check if price include tax should be used for calculations.
     * We are using price include tax just in case when catalog prices are including tax
     * and customer tax request is same as store tax request
     *
     * @param $store
     * @return bool
     */
    protected function _usePriceIncludeTax($store)
    {
        if ($this->_config->priceIncludesTax($store) || $this->_config->getNeedUsePriceExcludeTax()) {
            return $this->_areTaxRequestsSimilar;
        }
        return false;
    }

    /**
     * Calculate address tax amount based on one unit price and tax amount
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Tax_Model_Sales_Total_Quote_Tax
     */
    protected function _unitBaseCalculation(Mage_Sales_Model_Quote_Address $address, $taxRateRequest)
    {
        $items = $this->_getAddressItems($address);
        $itemTaxGroups = array();
        $store = $address->getQuote()->getStore();
        $catalogPriceInclTax = $this->_config->priceIncludesTax($store);

        /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
        foreach ($items as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $this->_unitBaseProcessItemTax(
                        $address, $child, $taxRateRequest, $itemTaxGroups, $catalogPriceInclTax);
                }
                $this->_recalculateParent($item);
            } else {
                $this->_unitBaseProcessItemTax(
                    $address, $item, $taxRateRequest, $itemTaxGroups, $catalogPriceInclTax);
            }

        }

        if ($address->getQuote()->getMafTaxesForItems()) {
            $itemTaxGroups += $address->getQuote()->getMafTaxesForItems();
        }
        $address->getQuote()->setMafTaxesForItems($itemTaxGroups);
        return $this;
    }

    /**
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param Varien_Object $taxRateRequest
     * @param array $itemTaxGroups
     * @param boolean $catalogPriceInclTax
     */
    protected function _unitBaseProcessItemTax(
        $address, $item, $taxRateRequest, &$itemTaxGroups, $catalogPriceInclTax
    )
    {
        $taxRateRequest->setProductClassId($item->getProduct()->getTaxClassId());
        $rate = $this->_calculator->getRate($taxRateRequest);

        $item->setMafTaxAmount(0);
        $item->setBaseMafTaxAmount(0);
        $item->setHiddenMafTaxAmount(0);
        $item->setBaseHiddenTaxAmount(0);
        $item->setBaseHiddenMafTaxAmount(0);
        //$item->setTaxPercent($rate);
        $item->setMafDiscountTaxCompensation(0);
        $rowTotalInclTax = $item->getMafRowTotalInclTax();
        $recalculateRowTotalInclTax = false;
        if (!isset($rowTotalInclTax)) {
            $qty = $item->getTotalQty();
            $item->setMafRowTotalInclTax($this->_store->roundPrice($item->getMafTaxableAmount() * $qty));
            $item->setBaseMafRowTotalInclTax(
                $this->_store->roundPrice($item->getBaseMafTaxableAmount() * $qty));
            $recalculateRowTotalInclTax = true;
        }

        $appliedRates = $this->_calculator->getAppliedRates($taxRateRequest);
        $item->setTaxRates($appliedRates);
        if ($catalogPriceInclTax) {
            $this->_calcUnitTaxAmount($item, $rate);
            $this->_saveAppliedTaxes(
                $address, $appliedRates, $item->getMafTaxAmount(), $item->getBaseMafTaxAmount(), $rate);
        } else {
            //need to calculate each tax separately
            $taxGroups = array();
            foreach ($appliedRates as $appliedTax) {
                $taxId = $appliedTax['id'];
                $taxRate = $appliedTax['percent'];
                $this->_calcUnitTaxAmount($item, $taxRate, $taxGroups, $taxId, $recalculateRowTotalInclTax);
                $this->_saveAppliedTaxes(
                    $address, array($appliedTax), $taxGroups[$taxId]['tax'], $taxGroups[$taxId]['base_tax'], $taxRate);
            }

        }
        if ($rate > 0) {
            $itemTaxGroups[$item->getId()] = $appliedRates;
        }
        $this->_addMafAmount($item->getMafTaxAmount());
        $this->_addBaseMafAmount($item->getBaseMafTaxAmount());
        return $this;
    }

    /**
     * Calculate unit tax anount based on unit price
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @param   float $rate
     * @param   array $taxGroups
     * @param   string $taxId
     * @param   boolean $recalculateRowTotalInclTax
     * @return  Mage_Tax_Model_Sales_Total_Quote
     */
    protected function _calcUnitTaxAmount(
        $item, $rate, &$taxGroups = null, $taxId = null, $recalculateRowTotalInclTax = false
    )
    {
        $qty = $item->getTotalQty();
        $inclTax = $item->getIsPriceInclTax();
        $price = $item->getMafTaxableAmount();
        $basePrice = $item->getBaseMafTaxableAmount();
        $rateKey = ($taxId == null) ? (string)$rate : $taxId;

        $hiddenTax = null;
        $baseHiddenTax = null;
        $unitTaxBeforeDiscount = null;
        $weeeTaxBeforeDiscount = null;
        $baseUnitTaxBeforeDiscount = null;
        $baseWeeeTaxBeforeDiscount = null;

        switch ($this->_config->getCalculationSequence($this->_store)) {
            case Mage_Tax_Model_Calculation::CALC_TAX_BEFORE_DISCOUNT_ON_EXCL:
            case Mage_Tax_Model_Calculation::CALC_TAX_BEFORE_DISCOUNT_ON_INCL:
                $unitTaxBeforeDiscount = $this->_calculator->calcTaxAmount($price, $rate, $inclTax, false);
                $baseUnitTaxBeforeDiscount = $this->_calculator->calcTaxAmount($basePrice, $rate, $inclTax, false);

                $unitTaxBeforeDiscount = $unitTax = $this->_calculator->round($unitTaxBeforeDiscount);
                $baseUnitTaxBeforeDiscount = $baseUnitTax = $this->_calculator->round($baseUnitTaxBeforeDiscount);
                break;
            case Mage_Tax_Model_Calculation::CALC_TAX_AFTER_DISCOUNT_ON_EXCL:
            case Mage_Tax_Model_Calculation::CALC_TAX_AFTER_DISCOUNT_ON_INCL:
                $discountAmount = $item->getMafDiscountAmount() / $qty;
                $baseDiscountAmount = $item->getBaseMafDiscountAmount() / $qty;

                $unitTaxBeforeDiscount = $this->_calculator->calcTaxAmount($price, $rate, $inclTax, false);
                $unitTaxDiscount = $this->_calculator->calcTaxAmount($discountAmount, $rate, $inclTax, false);
                $unitTax = $this->_calculator->round(max($unitTaxBeforeDiscount - $unitTaxDiscount, 0));

                $baseUnitTaxBeforeDiscount = $this->_calculator->calcTaxAmount($basePrice, $rate, $inclTax, false);
                $baseUnitTaxDiscount = $this->_calculator->calcTaxAmount($baseDiscountAmount, $rate, $inclTax, false);
                $baseUnitTax = $this->_calculator->round(max($baseUnitTaxBeforeDiscount - $baseUnitTaxDiscount, 0));

                $unitTax = $this->_calculator->round($unitTax);
                $baseUnitTax = $this->_calculator->round($baseUnitTax);

                $unitTaxBeforeDiscount = max(0, $this->_calculator->round($unitTaxBeforeDiscount));
                $baseUnitTaxBeforeDiscount = max(0, $this->_calculator->round($baseUnitTaxBeforeDiscount));

                if ($inclTax && $discountAmount > 0) {
                    $hiddenTax = $unitTaxBeforeDiscount - $unitTax;
                    $baseHiddenTax = $baseUnitTaxBeforeDiscount - $baseUnitTax;
                    $this->_hiddenTaxes[] = array(
                        'rate_key' => $rateKey,
                        'qty' => $qty,
                        'item' => $item,
                        'value' => $hiddenTax,
                        'base_value' => $baseHiddenTax,
                        'incl_tax' => $inclTax,
                    );
                } elseif ($discountAmount > $price) { // case with 100% discount on price incl. tax
                    $hiddenTax = $discountAmount - $price;
                    $baseHiddenTax = $baseDiscountAmount - $basePrice;
                    $this->_hiddenTaxes[] = array(
                        'rate_key' => $rateKey,
                        'qty' => $qty,
                        'item' => $item,
                        'value' => $hiddenTax,
                        'base_value' => $baseHiddenTax,
                        'incl_tax' => $inclTax,
                    );
                }

                break;
        }

        $rowTax = $this->_store->roundPrice(max(0, $qty * $unitTax));
        $baseRowTax = $this->_store->roundPrice(max(0, $qty * $baseUnitTax));
        $item->setMafTaxAmount($item->getMafTaxAmount() + $rowTax);
        $item->setBaseMafTaxAmount($item->getBaseMafTaxAmount() + $baseRowTax);
        if (is_array($taxGroups)) {
            $taxGroups[$rateKey]['maf_tax'] = max(0, $rowTax);
            $taxGroups[$rateKey]['base_maf_tax'] = max(0, $baseRowTax);
        }

        $rowTotalInclTax = $item->getMafRowTotalInclTax();
        if (!isset($rowTotalInclTax) || $recalculateRowTotalInclTax) {
            if ($this->_config->priceIncludesTax($this->_store)) {
                $item->setMafRowTotalInclTax($price * $qty);
                $item->setBaseMafRowTotalInclTax($basePrice * $qty);
            } else {
                $item->setMafRowTotalInclTax(
                    $item->getMafRowTotalInclTax() + $unitTaxBeforeDiscount * $qty);
                $item->setBaseMafRowTotalInclTax(
                    $item->getBaseMafRowTotalInclTax() +
                    $baseUnitTaxBeforeDiscount * $qty);
            }
        }

        return $this;
    }

    /**
     * Calculate address total tax based on row total
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @param   Varien_Object $taxRateRequest
     * @return  Mage_Tax_Model_Sales_Total_Quote
     */
    protected function _rowBaseCalculation(Mage_Sales_Model_Quote_Address $address, $taxRateRequest)
    {
        $items = $this->_getAddressItems($address);
        $itemTaxGroups = array();
        $store = $address->getQuote()->getStore();
        $catalogPriceInclTax = $this->_config->priceIncludesTax($store);

        foreach ($items as $item) {
            if ($item->getParentItem()) {
                continue;
            }
            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $this->_rowBaseProcessItemTax(
                        $address, $child, $taxRateRequest, $itemTaxGroups, $catalogPriceInclTax);
                }
                $this->_recalculateParent($item);
            } else {
                $this->_rowBaseProcessItemTax(
                    $address, $item, $taxRateRequest, $itemTaxGroups, $catalogPriceInclTax);
            }
        }

        if ($address->getQuote()->getMafTaxesForItems()) {
            $itemTaxGroups += $address->getQuote()->getMafTaxesForItems();
        }
        $address->getQuote()->setMafTaxesForItems($itemTaxGroups);
        return $this;
    }

    /**
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param Varien_Object $taxRateRequest
     * @param array $itemTaxGroups
     * @param boolean $catalogPriceInclTax
     * @return Mage_Tax_Model_Sales_Total_Quote_Tax
     */
    protected function _rowBaseProcessItemTax($address, $item, $taxRateRequest, &$itemTaxGroups, $catalogPriceInclTax)
    {
        $taxRateRequest->setProductClassId($item->getProduct()->getTaxClassId());
        $rate = $this->_calculator->getRate($taxRateRequest);

        $item->setMafTaxAmount(0);
        $item->setBaseMafTaxAmount(0);
        $item->setHiddenMafTaxAmount(0);
        $item->setBaseHiddenMafTaxAmount(0);
        $item->setTaxPercent($rate);
        $item->setMafDiscountTaxCompensation(0);
        $rowTotalInclTax = $item->getMafRowTotalInclTax();
        $recalculateRowTotalInclTax = false;
        if (!isset($rowTotalInclTax)) {
            $item->setMafRowTotalInclTax($item->getMafTaxableAmount());
            $item->setBaseMafRowTotalInclTax($item->getBaseMafTaxableAmount());
            $recalculateRowTotalInclTax = true;
        }

        $appliedRates = $this->_calculator->getAppliedRates($taxRateRequest);
        $item->setTaxRates($appliedRates);
        if ($catalogPriceInclTax) {
            $this->_calcRowTaxAmount($item, $rate);
            $this->_saveAppliedTaxes(
                $address, $appliedRates, $item->getMafTaxAmount(), $item->getBaseMafTaxAmount(), $rate);
        } else {
            //need to calculate each tax separately
            $taxGroups = array();
            foreach ($appliedRates as $appliedTax) {
                $taxId = $appliedTax['id'];
                $taxRate = $appliedTax['percent'];
                $this->_calcRowTaxAmount($item, $taxRate, $taxGroups, $taxId, $recalculateRowTotalInclTax);
                $this->_saveAppliedTaxes(
                    $address, array($appliedTax), $taxGroups[$taxId]['maf_tax'], $taxGroups[$taxId]['base_maf_tax'], $taxRate);
            }
        }
        if ($rate > 0) {
            $itemTaxGroups[$item->getId()] = $appliedRates;
        }
        $this->_addMafAmount($item->getMafTaxAmount());
        $this->_addBaseMafAmount($item->getBaseMafTaxAmount());
        return $this;
    }

    /**
     * Calculate item tax amount based on row total
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @param   float $rate
     * @param   array $taxGroups
     * @param   string $taxId
     * @param   boolean $recalculateRowTotalInclTax
     * @return  Mage_Tax_Model_Sales_Total_Quote_Tax
     */
    protected function _calcRowTaxAmount(
        $item, $rate, &$taxGroups = null, $taxId = null, $recalculateRowTotalInclTax = false
    )
    {
        $inclTax = $item->getIsPriceInclTax();
        $subtotal = $taxSubtotal = $item->getMafTaxableAmount();
        $baseSubtotal = $baseTaxSubtotal = $item->getBaseMafTaxableAmount();
        $rateKey = ($taxId == null) ? (string)$rate : $taxId;

        $hiddenTax = null;
        $baseHiddenTax = null;
        $rowTaxBeforeDiscount = null;
        $baseRowTaxBeforeDiscount = null;

        switch ($this->_helper->getCalculationSequence($this->_store)) {
            case Mage_Tax_Model_Calculation::CALC_TAX_BEFORE_DISCOUNT_ON_EXCL:
            case Mage_Tax_Model_Calculation::CALC_TAX_BEFORE_DISCOUNT_ON_INCL:
                $rowTaxBeforeDiscount = $this->_calculator->calcTaxAmount($subtotal, $rate, $inclTax, false);
                $baseRowTaxBeforeDiscount = $this->_calculator->calcTaxAmount($baseSubtotal, $rate, $inclTax, false);

                $rowTaxBeforeDiscount = $rowTax = $this->_calculator->round($rowTaxBeforeDiscount);
                $baseRowTaxBeforeDiscount = $baseRowTax = $this->_calculator->round($baseRowTaxBeforeDiscount);
                break;
            case Mage_Tax_Model_Calculation::CALC_TAX_AFTER_DISCOUNT_ON_EXCL:
            case Mage_Tax_Model_Calculation::CALC_TAX_AFTER_DISCOUNT_ON_INCL:
                $discountAmount = $item->getMafDiscountAmount();
                $baseDiscountAmount = $item->getBaseMafDiscountAmount();

                $rowTax = $this->_calculator->calcTaxAmount(
                    max($subtotal - $discountAmount, 0),
                    $rate,
                    $inclTax
                );
                $baseRowTax = $this->_calculator->calcTaxAmount(
                    max($baseSubtotal - $baseDiscountAmount, 0),
                    $rate,
                    $inclTax
                );

                $rowTax = $this->_calculator->round($rowTax);
                $baseRowTax = $this->_calculator->round($baseRowTax);

                //Calculate the Row Tax before discount
                $rowTaxBeforeDiscount = $this->_calculator->calcTaxAmount(
                    $subtotal,
                    $rate,
                    $inclTax,
                    false
                );
                $baseRowTaxBeforeDiscount = $this->_calculator->calcTaxAmount(
                    $baseSubtotal,
                    $rate,
                    $inclTax,
                    false
                );

                $rowTaxBeforeDiscount = max(0, $this->_calculator->round($rowTaxBeforeDiscount));
                $baseRowTaxBeforeDiscount = max(0, $this->_calculator->round($baseRowTaxBeforeDiscount));

                if ($inclTax && $discountAmount > 0) {
                    $hiddenTax = $rowTaxBeforeDiscount - $rowTax;
                    $baseHiddenTax = $baseRowTaxBeforeDiscount - $baseRowTax;
                    $this->_hiddenTaxes[] = array(
                        'rate_key' => $rateKey,
                        'qty' => 1,
                        'item' => $item,
                        'value' => $hiddenTax,
                        'base_value' => $baseHiddenTax,
                        'incl_tax' => $inclTax,
                    );
                } elseif ($discountAmount > $subtotal) { // case with 100% discount on price incl. tax
                    $hiddenTax = $discountAmount - $subtotal;
                    $baseHiddenTax = $baseDiscountAmount - $baseSubtotal;
                    $this->_hiddenTaxes[] = array(
                        'rate_key' => $rateKey,
                        'qty' => 1,
                        'item' => $item,
                        'value' => $hiddenTax,
                        'base_value' => $baseHiddenTax,
                        'incl_tax' => $inclTax,
                    );
                }
                break;
        }
        $item->setMafTaxAmount($item->getMafTaxAmount() + max(0, $rowTax));
        $item->setBaseMafTaxAmount($item->getBaseMafTaxAmount() + max(0, $baseRowTax));
        if (is_array($taxGroups)) {
            $taxGroups[$rateKey]['maf_tax'] = max(0, $rowTax);
            $taxGroups[$rateKey]['base_maf_tax'] = max(0, $baseRowTax);
        }

        $rowTotalInclTax = $item->getMafRowTotalInclTax();
        if (!isset($rowTotalInclTax) || $recalculateRowTotalInclTax) {
            if ($this->_config->priceIncludesTax($this->_store)) {
                $item->setMafRowTotalInclTax($subtotal);
                $item->setBaseMafRowTotalInclTax($baseSubtotal);
            } else {
                $item->setMafRowTotalInclTax(
                    $item->getMafRowTotalInclTax() + $rowTaxBeforeDiscount);
                $item->setBaseMafRowTotalInclTax($item->getBaseMafRowTotalInclTax() +
                $baseRowTaxBeforeDiscount);
            }
        }
        return $this;
    }

    /**
     * Calculate address total tax based on address subtotal
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @param   Varien_Object $taxRateRequest
     * @return  Mage_Tax_Model_Sales_Total_Quote_Tax
     */
    protected function _totalBaseCalculation(Mage_Sales_Model_Quote_Address $address, $taxRateRequest)
    {
        $items = $this->_getAddressItems($address);
        $store = $address->getQuote()->getStore();
        $taxGroups = array();
        $itemTaxGroups = array();
        $catalogPriceInclTax = $this->_config->priceIncludesTax($store);

        foreach ($items as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $this->_totalBaseProcessItemTax(
                        $child, $taxRateRequest, $taxGroups, $itemTaxGroups, $catalogPriceInclTax);
                }
                $this->_recalculateParent($item);
            } else {
                $this->_totalBaseProcessItemTax(
                    $item, $taxRateRequest, $taxGroups, $itemTaxGroups, $catalogPriceInclTax);
            }
        }

        if ($address->getQuote()->getMafTaxesForItems()) {
            $itemTaxGroups += $address->getQuote()->getMafTaxesForItems();
        }
        $address->getQuote()->setMafTaxesForItems($itemTaxGroups);

        foreach ($taxGroups as $taxId => $data) {
            if ($catalogPriceInclTax) {
                $rate = (float)$taxId;
            } else {
                $rate = $data['applied_rates'][0]['percent'];
            }

            $totalTax = array_sum($data['maf_tax']);
            $baseTotalTax = array_sum($data['base_maf_tax']);
            $this->_addMafAmount($totalTax);
            $this->_addBaseMafAmount($baseTotalTax);
            $totalTaxRounded = $this->_calculator->round($totalTax);
            $baseTotalTaxRounded = $this->_calculator->round($totalTaxRounded);
            $this->_saveAppliedTaxes($address, $data['applied_rates'], $totalTaxRounded, $baseTotalTaxRounded, $rate);
        }
        return $this;
    }

    /**
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param Varien_Object $taxRateRequest
     * @param array $taxGroups
     * @param array $itemTaxGroups
     * @param boolean $catalogPriceInclTax
     * @return Mage_Tax_Model_Sales_Total_Quote_Tax
     */
    protected function _totalBaseProcessItemTax(
        $item, $taxRateRequest, &$taxGroups, &$itemTaxGroups, $catalogPriceInclTax
    )
    {
        $taxRateRequest->setProductClassId($item->getProduct()->getTaxClassId());
        $rate = $this->_calculator->getRate($taxRateRequest);

        $item->setMafTaxAmount(0);
        $item->setBaseMafTaxAmount(0);
        $item->setHiddenMafTaxAmount(0);
        $item->setBaseHiddenMafTaxAmount(0);
        $item->setTaxPercent($rate);
        $item->setMafDiscountTaxCompensation(0);
        $rowTotalInclTax = $item->getMafRowTotalInclTax();
        $recalculateRowTotalInclTax = false;
        if (!isset($rowTotalInclTax)) {
            $item->setMafRowTotalInclTax($item->getMafTaxableAmount());
            $item->setBaseMafRowTotalInclTax($item->getBaseMafTaxableAmount());
            $recalculateRowTotalInclTax = true;
        }

        $appliedRates = $this->_calculator->getAppliedRates($taxRateRequest);
        if ($catalogPriceInclTax) {
            $taxGroups[(string)$rate]['applied_rates'] = $appliedRates;
            $taxGroups[(string)$rate]['incl_tax'] = $item->getIsPriceInclTax();
            $this->_aggregateTaxPerRate($item, $rate, $taxGroups);
        } else {
            //need to calculate each tax separately
            foreach ($appliedRates as $appliedTax) {
                $taxId = $appliedTax['id'];
                $taxRate = $appliedTax['percent'];
                $taxGroups[$taxId]['applied_rates'] = array($appliedTax);
                $taxGroups[$taxId]['incl_tax'] = $item->getIsPriceInclTax();
                $this->_aggregateTaxPerRate($item, $taxRate, $taxGroups, $taxId, $recalculateRowTotalInclTax);
            }
        }
        if ($rate > 0) {
            $itemTaxGroups[$item->getId()] = $appliedRates;
        }
        return $this;
    }

    /**
     * Aggregate row totals per tax rate in array
     *
     * @param   Vznl_Checkout_Model_Sales_Quote_Item $item
     * @param   float $rate
     * @param   array $taxGroups
     * @param   int|null $taxId
     * @param   bool $recalculateRowTotalInclTax
     * @return  Vznl_Checkout_Model_Tax_Sales_Total_Quote_Tax
     */
    protected function _aggregateTaxPerRate(
        $item, $rate, &$taxGroups, $taxId = null, $recalculateRowTotalInclTax = false
    )
    {
        $inclTax = $item->getIsPriceInclTax();
        $rateKey = ($taxId == null) ? (string)$rate : $taxId;
        $taxSubtotal = $subtotal = $item->getMafTaxableAmount();
        $baseTaxSubtotal = $baseSubtotal = $item->getBaseMafTaxableAmount();

        if (!isset($taxGroups[$rateKey]['maf_totals'])) {
            $taxGroups[$rateKey]['maf_totals'] = array();
            $taxGroups[$rateKey]['base_maf_totals'] = array();
        }

        $hiddenTax = null;
        $baseHiddenTax = null;

        switch ($this->_helper->getCalculationSequence($this->_store)) {
            case Mage_Tax_Model_Calculation::CALC_TAX_BEFORE_DISCOUNT_ON_EXCL:
            case Mage_Tax_Model_Calculation::CALC_TAX_BEFORE_DISCOUNT_ON_INCL:
                $rowTaxBeforeDiscount = $this->_calculator->calcTaxAmount($subtotal, $rate, $inclTax, false);
                $baseRowTaxBeforeDiscount = $this->_calculator->calcTaxAmount($baseSubtotal, $rate, $inclTax, false);

                $taxBeforeDiscountRounded = $rowTax = $this->_deltaRound($rowTaxBeforeDiscount, $rateKey, $inclTax);
                $baseTaxBeforeDiscountRounded = $baseRowTax = $this->_deltaRound($baseRowTaxBeforeDiscount,
                    $rateKey, $inclTax, 'base');
                $item->setMafTaxAmount($item->getMafTaxAmount() + max(0, $rowTax));
                $item->setBaseMafTaxAmount($item->getBaseMafTaxAmount() + max(0, $baseRowTax));
                break;
            case Mage_Tax_Model_Calculation::CALC_TAX_AFTER_DISCOUNT_ON_EXCL:
            case Mage_Tax_Model_Calculation::CALC_TAX_AFTER_DISCOUNT_ON_INCL:
                $discount = $item->getMafDiscountAmount();
                $baseDiscount = $item->getBaseMafDiscountAmount();

                $taxSubtotal = max($subtotal - $discount, 0);
                $baseTaxSubtotal = max($baseSubtotal - $baseDiscount, 0);

                $rowTax = $this->_calculator->calcTaxAmount($taxSubtotal, $rate, $inclTax, false);
                $baseRowTax = $this->_calculator->calcTaxAmount($baseTaxSubtotal, $rate, $inclTax, false);

                $rowTax = $this->_deltaRound($rowTax, $rateKey, $inclTax);
                $baseRowTax = $this->_deltaRound($baseRowTax, $rateKey, $inclTax, 'base');

                $item->setMafTaxAmount($item->getMafTaxAmount() + max(0, $rowTax));
                $item->setBaseMafTaxAmount($item->getBaseMafTaxAmount() + max(0, $baseRowTax));

                //Calculate the Row taxes before discount
                $rowTaxBeforeDiscount = $this->_calculator->calcTaxAmount(
                    $subtotal,
                    $rate,
                    $inclTax,
                    false
                );
                $baseRowTaxBeforeDiscount = $this->_calculator->calcTaxAmount(
                    $baseSubtotal,
                    $rate,
                    $inclTax,
                    false
                );


                $taxBeforeDiscountRounded = max(
                    0,
                    $this->_deltaRound($rowTaxBeforeDiscount, $rateKey, $inclTax, 'tax_before_discount')
                );
                $baseTaxBeforeDiscountRounded = max(
                    0,
                    $this->_deltaRound($baseRowTaxBeforeDiscount, $rateKey, $inclTax, 'tax_before_discount_base')
                );

                if ($inclTax && $discount > 0) {
                    $roundedHiddenTax = $taxBeforeDiscountRounded - max(0, $rowTax);
                    $baseRoundedHiddenTax = $baseTaxBeforeDiscountRounded - max(0, $baseRowTax);
                    $this->_hiddenTaxes[] = array(
                        'rate_key' => $rateKey,
                        'qty' => 1,
                        'item' => $item,
                        'value' => $roundedHiddenTax,
                        'base_value' => $baseRoundedHiddenTax,
                        'incl_tax' => $inclTax,
                    );
                }
                break;
        }

        $rowTotalInclTax = $item->getRowTotalInclTax();
        if (!isset($rowTotalInclTax) || $recalculateRowTotalInclTax) {
            if ($this->_config->priceIncludesTax($this->_store)) {
                $item->setMafRowTotalInclTax($subtotal);
                $item->setBaseMafRowTotalInclTax($baseSubtotal);
            } else {
                $item->setMafRowTotalInclTax(
                    $item->getMafRowTotalInclTax() + $taxBeforeDiscountRounded);
                $item->setBaseMafRowTotalInclTax(
                    $item->getBaseMafRowTotalInclTax()
                    + $baseTaxBeforeDiscountRounded);
            }
        }

        $taxGroups[$rateKey]['maf_totals'][] = max(0, $taxSubtotal);
        $taxGroups[$rateKey]['base_maf_totals'][] = max(0, $baseTaxSubtotal);
        $taxGroups[$rateKey]['maf_tax'][] = max(0, $rowTax);
        $taxGroups[$rateKey]['base_maf_tax'][] = max(0, $baseRowTax);
        return $this;
    }

    /**
     * Round price based on previous rounding operation delta
     *
     * @param float $price
     * @param string $rate
     * @param bool $direction price including or excluding tax
     * @param string $type
     * @return float
     */
    protected function _deltaRound($price, $rate, $direction, $type = 'regular')
    {
        if ($price) {
            $rate = (string)$rate;
            $type = $type . $direction;
            // initialize the delta to a small number to avoid non-deterministic behavior with rounding of 0.5
            $delta = isset($this->_roundingDeltas[$type][$rate]) ? $this->_roundingDeltas[$type][$rate] : 0.000001;
            $price += $delta;
            $this->_roundingDeltas[$type][$rate] = $price - $this->_calculator->round($price);
            $price = $this->_calculator->round($price);
        }
        return $price;
    }

    /**
     * Recalculate parent item amounts base on children data
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @return  Mage_Tax_Model_Sales_Total_Quote_Tax
     */
    protected function _recalculateParent(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $rowTaxAmount = 0;
        $baseRowTaxAmount = 0;
        foreach ($item->getChildren() as $child) {
            $rowTaxAmount += $child->getMafTaxAmount();
            $baseRowTaxAmount += $child->getBaseMafTaxAmount();
        }
        $item->setMafTaxAmount($rowTaxAmount);
        $item->setBaseMafTaxAmount($baseRowTaxAmount);
        return $this;
    }

    /**
     * Collect applied tax rates information on address level
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @param   array $applied
     * @param   float $amount
     * @param   float $baseAmount
     * @param   float $rate
     */
    protected function _saveAppliedTaxes(Mage_Sales_Model_Quote_Address $address,
                                         $applied, $amount, $baseAmount, $rate)
    {
        $previouslyAppliedTaxes = $address->getAppliedTaxes();
        $process = count($previouslyAppliedTaxes);

        foreach ($applied as $row) {
            if ($row['percent'] == 0) {
                continue;
            }
            if (!isset($previouslyAppliedTaxes[$row['id']])) {
                $row['process'] = $process;
                $row['maf_amount'] = 0;
                $row['base_maf_amount'] = 0;
                $previouslyAppliedTaxes[$row['id']] = $row;
            }

            if (!is_null($row['percent'])) {
                $row['percent'] = $row['percent'] ? $row['percent'] : 1;
                $rate = $rate ? $rate : 1;

                $appliedAmount = $amount / $rate * $row['percent'];
                $baseAppliedAmount = $baseAmount / $rate * $row['percent'];
            } else {
                $appliedAmount = 0;
                $baseAppliedAmount = 0;
                foreach ($row['rates'] as $rate) {
                    $appliedAmount += $rate['maf_amount'];
                    $baseAppliedAmount += $rate['base_maf_amount'];
                }
            }


            if ($appliedAmount || (isset($previouslyAppliedTaxes[$row['id']]['maf_amount']) && $previouslyAppliedTaxes[$row['id']]['maf_amount'])) {
                if(isset($previouslyAppliedTaxes[$row['id']]['maf_amount'])){
                    $previouslyAppliedTaxes[$row['id']]['maf_amount'] += $appliedAmount;
                }
                if(isset($previouslyAppliedTaxes[$row['id']]['base_maf_amount'])){
                    $previouslyAppliedTaxes[$row['id']]['base_maf_amount'] += $baseAppliedAmount;
                }
            } else {
                unset($previouslyAppliedTaxes[$row['id']]);
            }
        }
        $address->setAppliedMafTaxes($previouslyAppliedTaxes);
    }

    /**
     * Add tax totals information to address object
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Tax_Model_Sales_Total_Quote_Tax
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $applied = $address->getAppliedMafTaxes();
        $store = $address->getQuote()->getStore();
        $amount = $address->getMafTaxAmount();

        $items = $this->_getAddressItems($address);
        $discountTaxCompensation = 0;
        foreach ($items as $item) {
            $discountTaxCompensation += $item->getMafDiscountTaxCompensation();
        }
        $taxAmount = $amount + $discountTaxCompensation;

        $area = null;
        if ($this->_config->displayCartTaxWithGrandTotal($store) && $address->getMafGrandTotal()) {
            $area = 'taxes';
        }

        if (($amount != 0) || ($this->_config->displayCartZeroTax($store))) {
            $address->addMafTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('tax')->__('Maf Tax'),
                'full_info' => $applied ? $applied : array(),
                'value' => $amount,
                'area' => $area
            ));
        }

        $store = $address->getQuote()->getStore();
        /**
         * Modify subtotal
         */
        if ($this->_config->displayCartSubtotalBoth($store) || $this->_config->displayCartSubtotalInclTax($store)) {
            if ($address->getMafSubtotalInclTax() > 0) {
                $subtotalInclTax = $address->getMafSubtotalInclTax();
            } else {
                $subtotalInclTax = $address->getMafSubtotal() + $taxAmount;
            }

            $address->addMafTotal(array(
                'code' => 'maf_subtotal',
                'title' => Mage::helper('sales')->__('Maf Subtotal'),
                'value' => $subtotalInclTax,
                'value_incl_tax' => $subtotalInclTax,
                'value_excl_tax' => $address->getMafSubtotal(),
            ));
        }

        return $this;
    }

    /**
     * Add total model amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _addMafAmount($amount)
    {
        if ($this->_canAddAmountToAddress) {
            $this->_getAddress()->addMafTotalAmount($this->getCode(),$amount);
        }
        return $this;
    }

    /**
     * Add total model base amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _addBaseMafAmount($baseAmount)
    {
        if ($this->_canAddAmountToAddress) {
            $this->_getAddress()->addBaseMafTotalAmount($this->getCode(), $baseAmount);
        }
        return $this;
    }

    /**
     * Process model configuration array.
     * This method can be used for changing totals collect sort order
     *
     * @param   array $config
     * @param   store $store
     * @return  array
     */
    public function processConfigArray($config, $store)
    {
        $calculationSequence = $this->_helper->getCalculationSequence($store);
        switch ($calculationSequence) {
            case Mage_Tax_Model_Calculation::CALC_TAX_BEFORE_DISCOUNT_ON_INCL:
                $config['before'][] = 'discount';
                break;
            default:
                $config['after'][] = 'discount';
                break;
        }
        return $config;
    }

    /**
     * Get Tax label
     *
     * @return string
     */
    public function getLabel()
    {
        return Mage::helper('tax')->__('Maf Tax');
    }

    /**
     * Set total model amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _setMafAmount($amount)
    {
        if ($this->_canSetAddressAmount) {
            $this->_getAddress()->setMafTotalAmount($this->getCode(), $amount);
        }
        return $this;
    }

    /**
     * Set total model base amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _setBaseMafAmount($baseAmount)
    {
        if ($this->_canSetAddressAmount) {
            $this->_getAddress()->setBaseMafTotalAmount($this->getCode(), $baseAmount);
        }
        return $this;
    }
}
