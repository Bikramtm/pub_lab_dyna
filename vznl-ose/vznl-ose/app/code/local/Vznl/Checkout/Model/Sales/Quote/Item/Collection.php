<?php

/**
 * Class Vznl_Checkout_Model_Sales_Quote_Item_Collection
 */
class Vznl_Checkout_Model_Sales_Quote_Item_Collection extends Mage_Sales_Model_Resource_Quote_Item_Collection
{
    protected function _assignProducts()
    {
        foreach($this as $item) {
            $item->setQty($item->getQty());
        }

        return $this;
    }

    /**
     * Overwrite default behavior to prevent skipping adding items with same id
     * @param Varien_Object $item
     * @return $this|Varien_Data_Collection
     */
    public function addItem(Varien_Object $item)
    {
        $itemId = $this->_getItemId($item);

        if (!is_null($itemId)) {
            if (isset($this->_items[$itemId])) {
                //if item with same id was found, check if sku is the same
                $tempItem = $this->_items[$itemId];
                if($tempItem->getSku() == $item->getSku()){
                    return $this; //same sku, skip it, don't add it again
                }

                //if it's a different sku, move the new product to the back of the list
                $this->_items[$itemId]=$item;
                $this->_items[]=$tempItem;
            }
            $this->_items[$itemId] = $item;
        } else {
            $this->_addItem($item);
        }
        return $this;
    }
}
