<?php

/**
 * Class Order
 */
class Vznl_Checkout_Model_Sales_Order extends Mage_Sales_Model_Order
{
    const ESB_VALIDATION = 'telesale-to-retail-test';
    const DEFAULT_COUNTRY = 'NL';
    const INCREMENT_ID_PREFIX = 'DY1';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_NOT_DELIVERED = 'not_delivered';
    const STATUS_PICKED = 'picked';
    const STATUS_RDY4SHIP = 'rdy4ship';
    const STATUS_INPROGRESS = 'inprogress';
    const STATUS_RETPENDING = 'retpending';
    const DA_MODE_MULTI_CTN      = 'MultiCTN';
    const DA_MODE_SINGLE_CTN     = 'SingleCTN';
    const REASON_CODE_APPOINTMENT = 'APPOINTMENT';
    public static $reasonCodeLabel = array(
        'APPOINTMENT' => 'Failed Appointment'
    );

    /**
     * @var Dyna_Agent_Model_Dealer
     */
    private $dealer;

    /**
     * @var array
     */
    private $_myCached = array();
    private $statusTypes = array(
        'order_status'
    );

    /**
     * Remove the IMEI from the package if the device was changed
     *
     * @param $packageModel
     * @param $devices
     * @param $packageId
     */
    private function removePackageImei($packageModel, $devices, $packageId)
    {
        if ($packageModel->getChecksum() && isset($devices[$packageId]) && !in_array($devices[$packageId], explode(',', $packageModel->getChecksum()))) {
            $packageModel->setImei(null);
        }
    }

    /**
     * Remove the simcard from the package if the simcard type was changed
     *
     * @param $packageModel
     * @param $sims
     * @param $packageId
     */
    private function removePackageSimcard($packageModel, $sims, $packageId)
    {
        if ($packageModel->getChecksum() && isset($sims[$packageId]) && !in_array($sims[$packageId], explode(',', $packageModel->getChecksum()))) {
            $packageModel->setSimNumber(null);
        }
    }

    /**
     * Include coupons because they also alter the price
     *
     * @param $packageModel
     * @param $checksums
     * @param $packageId
     */
    private function addPackageCouponToChecksum($packageModel, &$checksums, $packageId)
    {
        $coupon = $packageModel->getCoupon();
        if (!empty($coupon)) {
            $checksums[$packageId][] = $coupon;
        }
    }

    /**
     * Remove or add the old_sim flag if a sim is selected and package is retention
     *
     * @param $packageModel
     * @param $sims
     * @param $packageId
     */
    private function setOldSimFlag($packageModel, $sims, $packageId)
    {
        if ($packageModel->isRetention()) {
            if (isset($sims[$packageId])) {
                $packageModel->setOldSim(false);
            } else {
                $packageModel->setOldSim(true);
            }
        }
    }

    /**
     * Set IMEI number, SIM number and Telephone number on the package
     * if set on the parent order's package
     *
     * @param $packageModel
     */
    private function setPackageData($packageModel)
    {
        $imei = trim($packageModel->getImei());
        $tel_number = trim($packageModel->getTelNumber());
        $sim_number = trim($packageModel->getSimNumber());

        if (empty($imei) || empty($tel_number) || empty($sim_number)) {
            $so = Mage::getModel('superorder/superorder')->load($this->getSuperorderId());
            if ($so->getParentId()) {

                $oldPackage = Mage::getSingleton('package/package')->getOrderPackages($so->getParentId(), $packageId);

                if (empty($imei) && $oldPackage->getData('imei')) {
                    $packageModel->setImei($oldPackage->getData('imei'));
                }
                if (empty($tel_number) && $oldPackage->getData('tel_number')) {
                    $packageModel->setTelNumber($oldPackage->getData('tel_number'));
                }
                if (empty($sim_number) && $oldPackage->getData('sim_number')) {
                    $packageModel->setSimNumber($oldPackage->getData('sim_number'));
                }
            }
        }
    }

    protected function _beforeSave()
    {
        if (!$this->getPayment()) {
            Mage::log(
                'OrderId: ' . $this->getId() .
                ', current website: ' . Mage::app()->getWebsite()->getName() .
                ', url: ' . Mage::helper('core/url')->getCurrentUrl() .
                ', Agent: ' . Mage::helper('agent')->getAgentId() .
                ', Quoteid: ' . $this->getQuoteId() .
                ' Origin: ' . php_sapi_name() .
                ' Trace: ' . mageDebugBacktrace(true, false, true), null, 'Orders_no_payment.log', true
            );
        }
        if (!$this->getId()) {
            $this->setCreatedAt(now());
        }
        parent::_beforeSave();
    }

    /**
     * Save price info for all order items
     *
     * @return Mage_Sales_Model_Order
     */
    protected function _afterSave()
    {
        Mage::helper('superorder')->checkStatusesChanged($this->getOrigData(), $this->getData(), $this->statusTypes, $this);
        parent::_afterSave();

        // update the checksum of the package
        $this->processPackageChecksum();

        $oldQuote = Mage::getModel('sales/quote')->load($this->getQuoteId());
        $tempQuote = Mage::getSingleton('customer/session')->getOfferteData();
        if ($tempQuote && $oldQuote->getId() == $tempQuote->getId()) {
            foreach ($this->getAllItems() as $orderItem) {
                foreach ($tempQuote->getItemsCollection() as $item) {
                    if ($orderItem->getProductId() == $item->getProductId() && $orderItem->getPackageId() == $item->getPackageId()) {
                        $orderItem->setOrderItemPrices($item->getOrderItemPrices())->save();
                        break;
                    }
                }
            }
        } else {
            Mage::helper('vznl_checkout')->setItemPriceHistory($this);
        }

        //Saving current product type, so we cand view this order in case this product is deleted
        foreach ($this->getAllItems() as $orderItem) {
            if ($orderItem->getProduct() && $orderItem->getProduct()->getType()) {
                $packType = $orderItem->getProduct()->getType();
                if (!empty($packType)) {
                    $productType=current($packType);
                    if ($productType) {
                        $orderItem->setPackProductType($productType);
                    }
                }
                //Applied rules for this order Item
                $appliedRulesNames="";
                if ($orderItem->getAppliedRuleIds()) {
                    $appliedRulesIds=explode(",", $orderItem->getAppliedRuleIds());
                    array_walk($appliedRulesIds, 'trim');
                    if (!empty($appliedRulesIds)) {
                        foreach ($appliedRulesIds as $appliedRule) {
                            $newAppliedRuleCollection = Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('rule_id', $appliedRule)->load();
                            foreach ($newAppliedRuleCollection as $newAppliedRule) {
                                $ruleName = $newAppliedRule->getName();
                                $appliedRulesNames = $appliedRulesNames ? $appliedRulesNames . ", " . $ruleName : $ruleName;
                            }
                        }
                    }
                }
                if ($appliedRulesNames) {
                    $orderItem->setAppliedRuleNames($appliedRulesNames);
                }
                $orderItem->save();
            }
        }
    }

    /**
     * @param null $packageId
     * @return mixed
     */
    public function getPackages($packageId = null)
    {
        $collection = Mage::getResourceModel('package/package_collection')
            ->addFieldToFilter('order_id', $this->getSuperorderId());
        if ($packageId != null) {
            if (is_scalar($packageId)) {
                return $collection->addFieldToFilter('package_id', $packageId)->getFirstItem();
            } else if (is_array($packageId)) {
                return $collection->addFieldToFilter('package_id', $packageId);
            }
        } else {
            return $collection->load();
        }
    }

    /**
     * Check if the deliveryOrder has a mobile package or not to generate contract
     * @return bool
     */
    public function hasMobilePackage() : bool
    {
        foreach ($this->getDeliveryOrderPackages() as $package) {
            if ($package->isMobile()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the website code where the order has been created
     * @return mixed
     */
    public function getWebsiteCode()
    {
        $websiteId = Mage::getModel('core/store')->load($this->getStoreId())->getWebsiteId();
        return Mage::getModel('core/website')->load($websiteId)->getCode();
    }

    /**
     * @return bool
     */
    public function needsContractSigning()
    {
        if (!$this->isHomeDelivery()) {
            return false; // These are signed in store, don't need backend systems handling the signing
        }

        $packages = $this->getPackageIds();
        $needsSigning = false; // Packages that need approval also have contract
        foreach ($packages as $packageId) {
            $package = $this->getPackages($packageId);
            if ($package->needsCreditCheck()) { // Seems to be same condition for credit check
                $needsSigning = true;
                break;
            }
        }
        return $needsSigning;
    }

    /**
     * Check if there delivery is done in a store
     * @return bool
     */
    public function isStoreDelivery()
    {
        return ($this->getShippingAddress()->getDeliveryStoreId() > 0);
    }

    /**
     * Check if the delivery is done at home
     * @return bool
     */
    public function isHomeDelivery()
    {
        return ($this->getShippingAddress()->getDeliveryStoreId() == false);
    }

    /**
     * Check if the delivery is in another store then the order was placed
     * @return bool
     */
    public function isOtherStoreDelivery()
    {
        if ($this->getShippingAddress()->getDeliveryStoreId() === null) {
            return false;
        } elseif ($this->isTelesalesOrder() || $this->getShippingAddress()->getDeliveryStoreId() != $this->getDealerId()) {
            return true;
        }
        return false;
    }

    /**
     * Check if the delivery is virtual and no actual delivery of goods is needed
     * @return bool
     */
    public function isVirtualDelivery()
    {
        if ($this->getWebsiteCode() === Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
            return true; // Indirect orders always virtual
        }

        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($this->getSuperorderId());
        if ($superOrder->getParentId()) {
            $superOrder = Mage::getModel('superorder/superorder')->load($superOrder->getParentId());
            $oldOrderPackages = $superOrder->getPackages();
            $mapping = [];
            // Map the packages to the new ids
            foreach ($oldOrderPackages as $oldPackage) {
                $mapping[$oldPackage->getPackageId()] = $oldPackage->getNewPackageId();
            }
            // Compare changed items
            $parentOrderItems = $superOrder->getOrderedItems(true);
            $currentOrderItems = Mage::getResourceModel('sales/order_item_collection')
                ->setOrderFilter($this->getId());
            $currentHardwareItems = [];
            // Find all the hardware products from the current magento order
            foreach ($currentOrderItems as $item) {
                $currentPackages[$item->getPackageId()] = $item->getPackageId();
                if ($item->getProduct()->isOfHardwareType()) {
                    $currentHardwareItems[$item->getPackageId()][$item->getProductId()] = ['product_id' => $item->getProductId(), 'doa' => $item->getItemDoa()];
                }
            }

            // Remove the hardware products that are in the current order (unchanged)
            foreach ($parentOrderItems as $items) {
                foreach ($items as $item) {
                    if ($item->getProduct()->isOfHardwareType()) {
                        $packageId = $mapping[$item->getPackageId()];
                        if (isset($currentHardwareItems[$packageId][$item->getProductId()]) && $currentHardwareItems[$packageId][$item->getProductId()]['product_id'] == $item->getProductId() && !$currentHardwareItems[$packageId][$item->getProductId()]['doa']) {
                            unset($currentHardwareItems[$packageId][$item->getProductId()]);
                        }
                    }
                }
            }

            // If there are still items return false;
            if (count(array_filter($currentHardwareItems))) {
                return false;
            }
        } else {
            // Check if all packages are virtual (sim only, retention, no SIM)
            $packages = $this->getPackageIds();
            foreach ($packages as $packageId) {
                $package = $this->getPackages($packageId);
                $productIds = array_map(function($item) { return $item->getProductId(); }, $package->getPackageItems());
                $productCollection = Mage::getModel('catalog/product')->getCollection()
                    ->addFieldToFilter('entity_id', $productIds);

                foreach ($package->getPackageItems() as $item) {
                    $product = $productCollection->getItemByColumnValue('entity_id', $item->getProductId());
                    if ($product && $product->isOfHardwareType()) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getPackageIds()
    {
        $packageIds = array();
        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($this->getAllVisibleItems() as $item) {
            if (in_array($item->getPackageId(), $packageIds)) {
                continue;
            }
            array_push($packageIds, $item->getPackageId());
        }
        return array_unique($packageIds);
    }

    public function getDeliveryOrderPackages()
    {
        return $this->getPackages(array('in' => $this->getPackageIds()));
    }

    public function getContractantIdNumber($customFormat = false)
    {
        if (!$customFormat) {
            return parent::getContractantIdNumber();
        }

        return  Mage::helper('dyna_validators')->stripNLD(parent::getContractantIdNumber());
    }

    public function getCustomerIdNumber($customFormat = false)
    {
        if (!$customFormat) {
            return parent::getCustomerIdNumber();
        }

        return Mage::helper('dyna_validators')->stripNLD(parent::getCustomerIdNumber());
    }

    public function getCustomer()
    {
        if ($this->getCustomerId()) {
            return Mage::getModel('customer/customer')->load($this->getCustomerId());
        }
        return null;
    }

    public function getAllItems($sorted = false)
    {
        $key = __METHOD__ . ' ' . $this->getId() . ' ' . (int) $sorted;
        if (Mage::registry('freeze_models') === true) {
            if (isset($this->_myCached[$key])) {
                return $this->_myCached[$key];
            }
        }

        $orderItems = parent::getAllItems();
        if ($sorted === false) {
            $this->_myCached[$key] = $orderItems;

            return $orderItems;
        }

        $packagesAll = array();
        $result = array();

        foreach ($orderItems as $item) {
            if ($item->isPromo()) {
                $packagesAll[$item->getPackageId()][Vznl_Catalog_Model_Type::SUBTYPE_PROMOTION][$item->getId()] = $item;
            } else {
                $packagesAll[$item->getPackageId()][current($item->getProduct()->getType())][$item->getId()] = $item;
            }
        }

        foreach ($packagesAll as $tempPackage) {
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_ADDON]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_ADDON] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_GENERAL]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_GENERAL] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_PROMOTION]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_PROMOTION] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_NONE]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_NONE] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_ADDONS]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_ADDONS] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_GOODIES]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_GOODIES] : array();
            $result += isset($tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_REPLACEMENT_MATERIALS]) ? $tempPackage[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_REPLACEMENT_MATERIALS] : array();
        }
        unset($packagesAll);
        $this->_myCached[$key] = $result;

        return $result;
    }

    /**
     * Processes package checksum save
     * @return void
     * @throws Mage_Core_Exception
     */
    public function processPackageChecksum():void
    {
        // If the order is not yet assigned to an superorder, don't create/update packages
        if (!$this->getSuperorderId() || $this->getEdited()) {
            return;
        }
        $items = $this->getAllVisibleItems();
        $packageIds = array();
        $checksums = array();
        $devices = array();
        $sims = array();
        foreach ($items as &$item) {
            $packageIds[] = $item->getPackageId();
            if (!isset($checksums[$item->getPackageId()])) {
                $checksums[$item->getPackageId()] = array();
            }

            $checksums[$item->getPackageId()][] = $item->getProductId();

            if ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)) {
                $devices[$item->getPackageId()] = $item->getProductId();
            }

            if ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD)) {
                $sims[$item->getPackageId()] = $item->getProductId();
            }
        }
        unset($item);

        $packageIds = array_unique($packageIds);

        /** @var Mage_Eav_Model_Entity_Collection_Abstract $packages */
        $packages = Mage::getSingleton('package/package')->getPackages($this->getSuperorderId(), $this->getQuoteId());
        foreach ($packageIds as $packageId) {
            /** @var Vznl_Package_Model_Package $packageModel */
            $packageModel = null;
            foreach ($packages->getItems() as $item) {
                if ($item->getOrderId() == $this->getSuperorderId() && $item->getPackageId() == $packageId) {
                    $packageModelOrder = $item;
                    break;
                }
            }

            foreach ($packages->getItems() as $item) {
                if ($item->getQuoteId() == $this->getQuoteId() && $item->getPackageId() == $packageId) {
                    $packageModelQuote = $item;
                    break;
                }
            }

            if (isset($packageModelOrder) && isset($packageModelQuote)) {
                $packageModelOrder->setCoupon($packageModelQuote->getCoupon())->save();
                $packageModel = $packageModelOrder;
            } elseif (!isset($packageModelOrder) && isset($packageModelQuote)) {
                $packageModel = $packageModelQuote;

                $this->setPackageData($packageModel);

                $packageModel->setOrderId($this->getSuperorderId())
                    ->setQuoteId(null);

                if (!in_array($packageModel->getStatus(), Vznl_Package_Model_Package::getCancelledTypePackages())) {
                    $packageModel->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED)// mark package as validated in edit after delivery
                        ->setCompleteReturn(null)
                        ->setNotDamagedItems(null)
                        ->setReturnNotes(null)
                        ->setReturnedBy(null)
                        ->setReturnDate(null)
                        ->setHardwareChange(null)
                        ->setRefundMethod(null)
                        ->setApproveOrderJobId(null);
                }
            } elseif (isset($packageModelOrder) && !isset($packageModelQuote)) {
                $packageModel = $packageModelOrder;
            } elseif (!isset($packageModelQuote) && !isset($packageModelOrder)) {
                $packageModel = Mage::getModel('package/package')
                    ->setPackageId($packageId)
                    ->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_INITIAL)
                    ->setOrderId($this->getSuperorderId())
                    ->setQuoteId(null);
            }

            $this->removePackageImei($packageModel, $devices, $packageId);
            $this->removePackageSimcard($packageModel, $sims, $packageId);
            $this->setOldSimFlag($packageModel, $sims, $packageId);

            // Also add order id to the checksum to ensure similar packages in same SO with different addresses are not coupled for telesales
            $websiteCode = Mage::app()->getWebsite(Mage::getModel('core/store')->load($this->getStoreId())->getWebsiteId())->getCode();
            if (Mage::helper('agent')->isTelesalesLine($websiteCode)) {
                $checksums[$packageId][] = $this->getId();
            }

            $this->addPackageCouponToChecksum($packageModel, $checksums, $packageId);

            $products = $checksums[$packageId];
            sort($products);
            $packageModel->setChecksum(join(',', $products));
            $packageModel->save();
        }
    }

    /**
     * @param $superorderId
     * @param $onlyFinal
     * @return mixed
     */
    public function getNonEditedOrderItems($superorderId, $onlyFinal = true, $returnItems = true)
    {
        if (!$superorderId) {
            return null;
        }

        $collection = $this->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('superorder_id', $superorderId);
        if ($onlyFinal) {
            $collection->addFieldToFilter('edited', array('neq' => 1));
        }

        $val = $returnItems ? $collection->getItems() : $collection;

        return $val;
    }

    /**
     * @param $incrementId
     * @return mixed
     * @internal param $customIncrementId
     */
    public function getFirstNonEditedOrder($incrementId)
    {
        return $this->getCollection()
            ->addFieldToFilter('increment_id', $incrementId)
            ->addFieldToFilter('edited', array('neq' => 1))
            ->getFirstItem();
    }

    /**
     * @param null $packageId
     * @return null
     */
    public function getDeliveredParentId($packageId = null)
    {
        if (! $packageId) {
            $items = $this->getAllItems();
            $item = reset($items);
            $packageId = $item->getPackageId();
        }

        $superOrder = Mage::getModel('superorder/superorder')->load($this->getSuperorderId());
        $packageObj = $this->getPackages($packageId);
        if ($packageObj->getOldPackageId()) {
            $packageId = $packageObj->getOldPackageId();
        }
        if ($superOrder->getParentId()) {
            $orders = $this->getCollection()
                ->addAttributeToFilter('superorder_id', $superOrder->getParentId())
                ->addAttributeToFilter('edited', 0);

            foreach ($orders as $order) {
                foreach ($order->getAllVisibleItems() as $item) {
                    if ($item->getPackageId() == $packageId) {
                        return $order->getId();
                    }
                }
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function getTotals()
    {
        // Initialise array to avoid missing key warnings
        $totals = [
            'subtotal_price' => 0,
            'subtotal_maf' => 0,
            'tax_price' => 0,
            'total_maf' => 0,
            'tax_maf' => 0,
            'total' => 0,
        ];

        foreach ($this->getPackages() as $package) {
            $itemCollection = array();
            foreach ($this->getAllItems() as $item) {
                if ($item->getPackageId() == $package->getPackageId()) {
                    $itemCollection[] = $item;
                }
            }
            $packageTotals = $package->getPackageTotals($itemCollection, false);
            $totals['subtotal_price']   += isset($packageTotals['subtotal_price']) ? $packageTotals['subtotal_price'] : 0;
            $totals['subtotal_maf']     += isset($packageTotals['subtotal_maf']) ? $packageTotals['subtotal_maf'] : 0;
            $totals['tax_price']        += isset($packageTotals['tax_price']) ? $packageTotals['tax_price'] : 0;
            $totals['total_maf']        += isset($packageTotals['total_maf']) ? $packageTotals['total_maf'] : 0;
            $totals['tax_maf']          += isset($packageTotals['tax_maf']) ? $packageTotals['tax_maf'] : 0;
            $totals['total']            += isset($packageTotals['total']) ? round($packageTotals['total'], 2) : 0;
        }

        return $totals;
    }

    /**
     * Check if the delivery order is paid
     * @return boolean
     */
    public function isPaid()
    {
        foreach ($this->getPackages() as $package) {
            if (!$package->isPayed()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Add the items of multiple orders to an order for contract purposes
     *
     * @param Mage_Sales_Model_Order $order
     */
    public function merge(Mage_Sales_Model_Order $order)
    {
        foreach ($order->getAllVisibleItems() as $item) {
            $newItem = clone $item;
            $newItem->setId(null);
            $this->addItem($newItem);
        }
        $this->setIncrementId($this->getIncrementId() . ', ' . $order->getIncrementId());
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return $this->getChildren()->count() > 0;
    }

    /**
     * @return bool
     */
    public function hasGrandChildren()
    {
        return $this->getChildren()->count() > 1;
    }

    /**
     * @return bool
     */
    public function hasParents()
    {
        return $this->getParents()->count() > 0;
    }

    /**
     * @return bool
     */
    public function hasGrandParents()
    {
        return $this->getParents()->count() > 1;
    }

    /**
     * @return Varien_Data_Collection
     * @throws Exception
     */
    public function getChildren()
    {
        $children = new Varien_Data_Collection();
        $sql = sprintf('SELECT * FROM `%s` WHERE parent_id="%%s";', $this->getResource()->getMainTable());
        $connection = $this->getResource()->getReadConnection();
        $resourceName = $this->getResourceName();
        $eId = $this->getId();
        $this->_findChildren($eId, $children, $connection, $sql, $resourceName);
        return $children;
    }

    /**
     * @param $eId
     * @param $children
     * @param $connection
     * @param $sql
     * @param $resourceName
     */
    protected function _findChildren($eId, $children, $connection, $sql, $resourceName)
    {
        $childs = $connection->fetchAll(sprintf($sql, $eId));
        foreach ($childs as &$child) {
            $children->addItem(Mage::getModel($resourceName)->addData($child));
            $this->_findChildren($child['entity_id'], $children, $connection, $sql, $resourceName);
        }
    }

    /**
     * @return Varien_Data_Collection
     * @throws Exception
     */
    public function getParents()
    {
        $parents = new Varien_Data_Collection();
        if ( ! $this->getParentId()) {
            return $parents;
        }
        $_parents = array();
        $sql = sprintf('SELECT * FROM `%s` WHERE entity_id="%%s";', $this->getResource()->getMainTable());
        $connection = $this->getResource()->getReadConnection();
        $resourceName = $this->getResourceName();
        $pId = $this->getParentId();
        while ($parent = $connection->fetchRow(sprintf($sql, $pId))) {
            $_parents[] = Mage::getModel($resourceName)->addData($parent);
            if ( ! isset($parent['parent_id'])) {
                unset($parent);
                break;
            }
            $pId = $parent['parent_id'];
            unset($parent);
        }
        usort($_parents, function($a, $b) {
            return $a->getId() > $b->getId();
        });
        foreach ($_parents as $p) {
            $parents->addItem($p);
        }
        return $parents;
    }

    /**
     * Set edited order with raw queries.
     * @param $flag
     * @return $this
     */
    public function setEditedOrder($flag)
    {
        $this->setEdited($flag);
        $this->runRawQuery('edited', $flag);
        return $this;
    }

    /**
     * Set superorder id with raw queries
     * @param $superorderId
     * @return $this
     */
    public function setSuperorderQuery($superorderId)
    {
        $this->setSuperorderId($superorderId);
        $this->runRawQuery('superorder_id', $superorderId);
        return $this;
    }

    protected function runRawQuery($field, $data)
    {
        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('core_write');
        $write->update(
            'sales_flat_order',
            array($field => $data),
            array('entity_id = ?' => $this->getId())
        );
    }

    /**
     * Set the dealer adapter mode from raw query
     * @param $param
     * @return $this
     */
    public function oneOff($param) {
        $this->runRawQuery('dealer_adapter_mode',$param ? self::DA_MODE_MULTI_CTN : self::DA_MODE_SINGLE_CTN );
        return $this;
    }

    public function doesNotContainsHardware()
    {
        if ($this->getWebsiteCode() === Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
            return true; // Indirect orders always virtual
        }
        foreach ($this->getAllItems() as $item) {
            if ($item->getProduct()->isOfHardwareType()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks whether the order contains at least one Aikido subscription.
     * @return bool
     */
    public function isAikido()
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getProduct()->getAikido()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a order has a product that requires a Loan
     * @return bool
     */
    public function isLoanRequired()
    {
        if (is_null($this->getData('customer_is_business')) || !$this->getData('customer_is_business')) {
            foreach ($this->getAllItems() as $item) {
                if ($item->getProduct()->isLoanRequired()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check if a order has a product that requires ILT
     * @return bool
     */
    public function isIltRequired()
    {
        if (is_null($this->getData('customer_is_business')) || !$this->getData('customer_is_business')) {
            foreach ($this->getAllItems() as $item) {
                if ($item->getProduct()->isIltRequired()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Retrieve the dealer that created the superorder
     *
     * @return Dyna_Agent_Model_Dealer
     */
    public function getDealer()
    {
        if ($this->dealer === null) {
            $this->dealer = Mage::getModel('agent/dealer')->load($this->getDealerId());
        }

        return $this->dealer;
    }

    public function clearFields()
    {
        /** @var Mage_Core_Model_Config_Element $fields */
        $fields = Mage::getConfig()->getFieldset('delivered_order_ignored_fields','global');
        foreach ($fields as $field=>$data) {
            $this->setData($field, null);
        }

        return $this;
    }

    public function isOrderedInStore()
    {
        if ($this->getWebsiteCode() === Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE) {
            return true;
        }
        return false;
    }

    public function isOrderedInIndirect()
    {
        return ($this->getWebsiteCode() === Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE);
    }

    public function getSuperOrder()
    {
        if (!$this->superorder) {
            $this->superorder = Mage::getModel('superorder/superorder')->load($this->getSuperorderId());
        }
        return $this->superorder;
    }

    public function hasDevice() : bool
    {
        foreach ($this->getAllVisibleItems() as $visibleItem) {
            if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE, $visibleItem->getProduct()->getType())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the delivery method
     * @return string The delivery method
     */
    public function getDeliveryMethod()
    {
        $deliveryMethod = '';
        if ($this->getShippingAddress() && ($type = $this->getShippingAddress()->getDeliveryType())) {
            if ($type == 'billing_address' || $type == 'other_address') {
                $deliveryMethod = Mage::helper('vznl_checkout')->__('home delivery');
            } else {
                if ($this->isTelesalesOrder() || !$this->isOtherStoreDelivery()) {
                    $deliveryMethod = Mage::helper('vznl_checkout')->__('order store');
                } else {
                    $deliveryMethod = Mage::helper('vznl_checkout')->__('other store');
                }
            }
        }
        return $deliveryMethod;
    }

    /**
     * Checks whether this order was ordered in telesales (or bc webshop/traders)
     * @return bool <true> if order in one of these, <false> if not.
     */
    public function isTelesalesOrder()
    {
        return in_array(
            $this->getWebsiteCode(),
            array(
                Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE,
                Vznl_Agent_Model_Website::WEBSITE_BC_WEBSHOP_CODE,
                Vznl_Agent_Model_Website::WEBSITE_TRADERS_CODE
            )
        );
    }

    /**
     * Checks whether the order has a package with type fixed.
     * @return bool <true> if has a fixed package, <false> if not.
     */
    public function hasFixed()
    {
        foreach ($this->getDeliveryOrderPackages() as $package) {
            if ($package->isFixed()) {
                return true;
            }
        }
        return false;
    }
}
