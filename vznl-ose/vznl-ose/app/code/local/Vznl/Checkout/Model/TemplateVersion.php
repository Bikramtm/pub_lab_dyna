<?php

class Vznl_Checkout_Model_TemplateVersion extends Mage_Core_Model_Abstract
{
    const TEMPLATETYPE_CONTRACT = 'contract';
    const TEMPLATETYPE_GARANT = 'garant';
    const TEMPLATETYPE_GARANT_TERMS = 'garant_terms';
    const TEMPLATETYPE_GARANT_IPID = 'garant_ipid';

    protected function _construct()
    {
        $this->_init("vznl_checkout/templateVersion");
    }

    /**
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->getData('entity_id');
    }

    /**
     * @param mixed $entityId
     * @return Vznl_Checkout_Model_Pdf_TemplateVersion
     */
    public function setEntityId($entityId)
    {
        $this->setData('entity_id', $entityId);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->getData('path');
    }

    /**
     * @param mixed $path
     * @return Vznl_Checkout_Model_Pdf_TemplateVersion
     */
    public function setPath($path)
    {
        $this->setData('path', $path);
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param DateTime $startDate
     * @return Vznl_Checkout_Model_Pdf_TemplateVersion
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Vznl_Checkout_Model_Pdf_TemplateVersion
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


}
