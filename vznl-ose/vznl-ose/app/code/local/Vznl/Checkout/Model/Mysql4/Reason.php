<?php

class Vznl_Checkout_Model_Mysql4_Reason extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("vznl_checkout/reason", "entity_id");
    }
}
