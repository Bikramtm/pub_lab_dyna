<?php

class Vznl_Checkout_Model_Mysql4_Reason_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("vznl_checkout/reason");
    }
}
