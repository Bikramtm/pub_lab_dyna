<?php
/**
 * Installer that adds the preselect_product columns
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();


$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "preselect_product", [
    "type" => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    "after" => "new_hardware",
    "comment" => "Preselect value from product attribute"
]);


$installer->endSetup();

