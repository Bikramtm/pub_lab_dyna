<?php
/**
 * Installer that adds the fixed_written_consent
 * column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "privacy_add_phone_book", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "serial_number",
    "comment" => 'privacy mention the number in phone book',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "privacy_block_caller_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "privacy_add_phone_book",
    "comment" => 'privacy block the caller identification',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "privacy_add_info", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "privacy_block_caller_id",
    "comment" => 'privacy mention number at information services',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "privacy_hide_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "privacy_add_info",
    "comment" => 'hide number from invoices',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "privacy_add_phone_book", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "serial_number",
    "comment" => 'privacy mention the number in phone book',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "privacy_block_caller_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "privacy_add_phone_book",
    "comment" => 'privacy block the caller identification',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "privacy_add_info", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "privacy_block_caller_id",
    "comment" => 'privacy mention number at information services',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "privacy_hide_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "privacy_add_info",
    "comment" => 'hide number from invoices',
]);


/**
 * Installer that adds the fixed_written_consent
 * column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */
// Add peal_order_sub_status to superorder resource
$installer->getConnection()->addColumn($this->getTable("superorder"), "peal_order_sub_status", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255,
    "comment" => "PEAL order sub-status received from oil",
]);

$installer->endSetup();