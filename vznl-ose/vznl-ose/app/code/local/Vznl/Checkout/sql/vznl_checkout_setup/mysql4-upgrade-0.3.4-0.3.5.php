<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->delete($this->getTable("core_config_data"), "path='checkout/cart/refund_reason_change'");
$installer->getConnection()->delete($this->getTable("core_config_data"), "path='checkout/cart/refund_reason_cancel'");

$row = array(
    'scope' => 'default',
    'scope_id' => 0,
    'path' => 'checkout/cart/refund_reason_change',
    'value' => 'BESCHADIGD:Damaged
INCOMPLEET:Incomplete
DOA:Defect - Redelivery
DOA/SWAP:Defect - Change
ONTEVREDEN:Doesn\'t meet expectations');

$installer->getConnection()->insertForce($installer->getTable('core/config_data'), $row);

$row = array(
    'scope' => 'default',
    'scope_id' => 0,
    'path' => 'checkout/cart/refund_reason_cancel',
    'value'=> 'BESCHADIGD:Damaged
INCOMPLEET:Incomplete
DEFECT:Defect
ONTEVREDEN:Doesn\'t meet expectations');

$installer->getConnection()->insertForce($installer->getTable('core/config_data'), $row);

$installer->endSetup();
