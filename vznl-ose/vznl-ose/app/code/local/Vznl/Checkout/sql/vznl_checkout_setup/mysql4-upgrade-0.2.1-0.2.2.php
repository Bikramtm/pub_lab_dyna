<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "privacy_check_required", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "privacy_customerdata",
    "comment" => 'privacy check required or not',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "privacy_check_required", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "privacy_customerdata",
    "comment" => 'privacy check required or not',
]);

$installer->endSetup();
