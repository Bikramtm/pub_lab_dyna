<?php
/**
 * Installer that adds the fixed_written_consent
 * column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

// Add fixed_written_consent on sales quote resource
$this->getConnection()->addColumn($this->getTable("sales/quote"), "fixed_written_consent", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "customer_number",
    "comment" => "Written Consent",
]);

// Add fixed_written_consent on sales order resource
$this->getConnection()->addColumn($this->getTable("sales/order"), "fixed_written_consent", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "customer_number",
    "comment" => "Written Consent",
]);