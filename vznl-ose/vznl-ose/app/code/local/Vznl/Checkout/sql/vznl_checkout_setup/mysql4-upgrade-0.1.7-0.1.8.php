<?php
/**
 * Installer that adds the fixed_written_consent
 * column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "offer_type", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "comment" => "offer type data from peal validate basket",
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "offer_type", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "comment" => "offer type data from peal validate basket",
]);

$installer->endSetup();
