<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$table = $this->getTable('sales/order');
$index = 'IDX_SALES_FLAT_ORDER_SUPERORDER_ID_EDITED';

$connection->addIndex($table, $index, array('superorder_id', 'edited'));

$installer->endSetup();
