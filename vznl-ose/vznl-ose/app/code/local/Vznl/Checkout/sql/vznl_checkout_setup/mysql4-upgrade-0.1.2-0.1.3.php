<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "campaign_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'campaign_id from overview',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "sales_log", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'sales log from overview',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "privacy_customerdata", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "comment" => 'save privacy check to share data from ziggo to vodafone',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "consolidate", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "default" => 0,
    "comment" => 'save privacy check to share data from ziggo to vodafone',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "basket_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'save privacy check to share data from ziggo to vodafone',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "campaign_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'campaign_id from overview',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "sales_log", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'sales log from overview',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "privacy_customerdata", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "comment" => 'save privacy check to share data from ziggo to vodafone',
]);

$installer->endSetup();
