<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
/** @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();

$table = $this->getTable('sales_flat_quote_item');
$columnsForAlternate = [
    'mixmatch_maf_subtotal',
    'mixmatch_maf_tax',
    'custom_maf',
    'original_custom_maf'
];

foreach ($columnsForAlternate as $column){
    if ($connection->tableColumnExists($table, $column)) {
        $connection->modifyColumn($table, $column, 'DECIMAL(12,4) NULL DEFAULT NULL');
    }
}
