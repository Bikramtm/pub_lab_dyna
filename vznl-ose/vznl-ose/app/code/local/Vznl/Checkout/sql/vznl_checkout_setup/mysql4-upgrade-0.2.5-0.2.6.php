<?php
/**
 * Installer that adds the fixed_written_consent
 * column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "fixed_account_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "privacy_hide_number",
    "comment" => 'fixed account number',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "fixed_account_holder", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_account_number",
    "comment" => 'fixed account holder name',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "mobile_oneoff_payment", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_account_holder",
    "comment" => 'fixed account holder name',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "fixed_account_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "privacy_hide_number",
    "comment" => 'fixed account number',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "fixed_account_holder", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_account_number",
    "comment" => 'fixed account holder name',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "mobile_oneoff_payment", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_account_holder",
    "comment" => 'fixed account holder name',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_address"), "fixed_street", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "initial_mixmatch_maf_total",
    "comment" => 'billing address type',
]); 

$installer->getConnection()->addColumn($installer->getTable("sales/quote_address"), "fixed_city", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_street",
    "comment" => 'billing address type',
]); 

$installer->getConnection()->addColumn($installer->getTable("sales/quote_address"), "fixed_postcode", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_street",
    "comment" => 'billing address type',
]);   

$installer->getConnection()->addColumn($installer->getTable("sales/order_address"), "fixed_street", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "delivery_store_id",
    "comment" => 'billing address type',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_address"), "fixed_city", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_street",
    "comment" => 'billing address type',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_address"), "fixed_postcode", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_city",
    "comment" => 'billing address type',
]);

/**
 * Installer that adds the fixed_extra_telephone_no
 * column after fixed_telephone_number
 */

// Add fixed_extra_telephone_no on sales quote resource
$installer->getConnection()->addColumn($this->getTable("sales/quote"), "fixed_extra_telephone_no", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "fixed_telephone_number",
    "comment" => "External fixed Extra telephone number to which the superorder maps to",
]);

$installer->getConnection()
    ->addIndex(
        $this->getTable("sales/quote"),
        $this->getIdxName($this->getTable("sales/quote"), "fixed_extra_telephone_no"),
        "fixed_extra_telephone_no");

// Add fixed_extra_telephone_no on sales order resource
$installer->getConnection()->addColumn($this->getTable("sales/order"), "fixed_extra_telephone_no", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "fixed_telephone_number",
    "comment" => "External fixed Extra telephone number to which the superorder maps to",
]);

$installer->getConnection()
    ->addIndex(
        $this->getTable("sales/order"),
        $this->getIdxName($this->getTable("sales/order"), "fixed_extra_telephone_no"),
        "fixed_extra_telephone_no");

$installer->endSetup();

/* @var $installer Mage_Checkout_Model_Resource_Setup */
$installer = new Mage_Checkout_Model_Resource_Setup('core_setup');
$installer->startSetup();

$installer->run("
DROP VIEW IF EXISTS `vw_order_details`;
CREATE VIEW `vw_order_details` AS
SELECT
    `s`.`created_at` AS `created_at`,
    `s`.`updated_at` AS `updated_at`,
    `w`.`code` AS `code`,
    `d`.`vf_dealer_code` AS `vf_dealer_code`,
    `p`.`sale_type` AS `sale_type`,
    `p`.`type` AS `package_type`,
    `s`.`order_number` AS `order_number`,
    `p`.`package_id` AS `package_id`,
    `s`.`order_status` AS `saleorder_status`,
    `p`.`status` AS `package_status`,
    `o`.`status` AS `deliverorder_status`,
    `p`.`package_esb_number` AS `package_esb_number`,
    `p`.`vf_status_code` AS `package_error_code`,
    `p`.`vf_status_desc` AS `package_error_message`,
    `s`.`error_detail` AS `saleorder_error_message`,
    `s`.`error_code` AS `saleorder_error_code`,
    `p`.`creditcheck_status` AS `creditcheck_status`,
    `p`.`porting_status` AS `porting_status`,
    `p`.`contract_end_date` AS `contract_end_date`,
    `o`.`contract_sign_date` AS `contract_sign_date`,
    `o`.`increment_id` AS `deliveryorder_number`,
    `p`.`approve_order_job_id` AS `approve_order_job_id`,
    `s`.`job_ids` AS `job_ids`,
    `o`.`customer_firstname` AS `customer_firstname`,
    `o`.`customer_middlename` AS `customer_middlename`,
    `o`.`customer_lastname` AS `customer_lastname`,
    `oa`.`telephone` AS `tel_number`,
    `o`.`additional_email` AS `customer_email`,
    COALESCE(`p`.`tel_number`, `p`.`ctn`) AS `ctn`,
    `p`.`current_number` AS `current_number`,
    `p`.`current_simcard_number` AS `current_simcard_number`,
    `p`.`sim_number` AS `sim_number`,
    `p`.`network_provider` AS `network_provider`,
    `p`.`network_operator` AS `network_operator`,
    `p`.`contract_end_date` AS `wishdate`,
    `p`.`imei` AS `imei`,
    `s`.`ilt_selection` AS `ilt_selection`,
    `s`.`times_processed` AS `times_processed`,
    `el`.`created_at` AS `contract_email_date`,
    `ac`.`username` AS `initial_agent`,
    `ae`.`username` AS `last_change_agent`,
    CASE 1
    WHEN `s`.`has_loan` = 1 AND `s`.`has_ilt` = 1 THEN 'Loan+ILT'
    WHEN `s`.`has_loan` = 1 AND `s`.`has_ilt` = 0 THEN 'Loan' 
    WHEN `s`.`has_loan` = 0 AND `s`.`has_ilt` = 1 THEN 'Loan+ILT'
    ELSE NULL
    END AS `ilt_flag`,
    `dc`.`count` AS `validation_count` 
FROM
    `superorder` `s`
    LEFT JOIN `catalog_package` `p` ON `p`.`order_id` = `s`.`entity_id`
    JOIN `sales_flat_order` `o` ON `o`.`superorder_id` = `s`.`entity_id`
    JOIN `core_website` `w` ON `w`.`website_id` = `s`.`website_id`
    JOIN `dealer` `d` ON `d`.`dealer_id` = `s`.`dealer_id`
    JOIN `sales_flat_order_address` `oa` ON `oa`.`entity_id` = `o`.`billing_address_id`
    LEFT JOIN `dyna_communication_log` `el` ON `el`.`superorder_id` = `s`.`entity_id` 
    JOIN `agent` `ac` ON `s`.`created_agent_id` = `ac`.`agent_id`
    LEFT JOIN `agent` `ae` ON `s`.`agent_id` = `ae`.`agent_id`
    INNER JOIN `vw_order_details_deliverycount` `dc` ON `dc`.`order_number` = `s`.`order_number` 
    
WHERE
    (`s`.`created_at` > (NOW() - INTERVAL 4 MONTH ))
");

$installer->endSetup();

