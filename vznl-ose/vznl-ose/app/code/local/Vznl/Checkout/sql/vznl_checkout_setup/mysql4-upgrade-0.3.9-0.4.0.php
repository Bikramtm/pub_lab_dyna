<?php
/**
 * Installer that adds the preselect_product columns
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;
$installer->startSetup();

$reasons = [
    "Error in previous order",
    "Error, workaround is active",
    "Custom arrangements with Credit Assessment (Override creditcheck)",
    "Error in priceplan:",
    "Ban/Ben construction",
    "Other:",
];

foreach ($reasons as $key => $value) {
    $installer->getConnection()->update(
        $installer->getTable('manual_activation_reasons'),
        array('name' => $value),
        $installer->getConnection()->quoteInto('entity_id = ?', intval($key+1))
    );
}

$installer->endSetup();
