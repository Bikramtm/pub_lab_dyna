<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$salesruleTable = $this->getTable('salesrule/rule');

if (!$this->getConnection()->tableColumnExists($salesruleTable, 'promotion_label')) {
    $this->getConnection()
        ->addColumn(
            $salesruleTable,
            'promotion_label',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Promotion label used in stackable promotions display',
                'nullable' => true,
                'default' => null
            )
        );
}

$installer->endSetup();

