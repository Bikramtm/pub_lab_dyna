<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_PriceRules_Model_Observer
 */
class Vznl_PriceRules_Model_Observer extends Dyna_PriceRules_Model_Observer
{

    /**
     * @param $sku
     * @return bool|Mage_Catalog_Model_Product
     */
    public function _loadProduct($sku)
    {
        if (empty($this->_cache[$sku])) {
            $productAttributes = Mage::getSingleton('sales/quote_config')->getProductAttributes();
            // also add hide_original_price
            $productAttributes = array_merge($productAttributes, array('hide_original_price'));

            $productCollection = Mage::getModel('catalog/product')->getCollection()
                ->addFieldToFilter('sku', array('eq' => $sku))
                ->addAttributeToSelect($productAttributes);

            if ($productCollection->getSize() == 0) {
                return $this->_cache[$sku] = false;
            }

            $product = $productCollection->getFirstItem();
            $product = $product->load($product->getId());

            if (Mage_Catalog_Model_Product_Status::STATUS_ENABLED != $product->getStatus()) {
                return $this->_cache[$sku] = false;
            }
            $this->_cache[$sku] = $product;
        }

        return $this->_cache[$sku];
    }

    /**
     * Process sales rule form creation
     * @param   Varien_Event_Observer $observer
     * @return  Vznl_PriceRules_Model_Observer
     */
    public function handleFormCreation($observer)
    {
        $actionsSelect = $observer->getForm()->getElement('simple_action');
        if ($actionsSelect) {
            $vals = $actionsSelect->getValues();

            $vals[] = array(
                'value' => 'process_add_product',
                'label' => Mage::helper('ampromo')->__('Auto add products'),
            );

            $vals[] = array(
                'value' => 'process_remove_product',
                'label' => Mage::helper('ampromo')->__('Auto remove products'),

            );

            $vals[] = array(
                'value' => 'process_replace_product',
                'label' => Mage::helper('ampromo')->__('Auto replace products'),

            );
            /** Amasty fields  */
            $vals[] = array(
                'value' => 'ampromo_items',
                'label' => Mage::helper('ampromo')->__('Auto add promo items with products'),

            );

            $vals[] = array(
                'value' => 'ampromo_remove_items',
                'label' => Mage::helper('ampromo')->__('Auto remove promo items'),

            );

            $vals[] = array(
                'value' => 'ampromo_replace_items',
                'label' => Mage::helper('ampromo')->__('Auto replace promo items'),

            );

            $vals[] = array(
                'value' => 'sales_hint',
                'label' => Mage::helper('ampromo')->__('Sales hint'),

            );
            /** Amasty fields*/

            $actionsSelect->setValues($vals);
            $actionsSelect->setOnchange('promo_check()');

            $fldSet = $observer->getForm()->getElement('action_fieldset');

            $fldSet->addField(
                'promo_sku_replace',
                'text',
                array(
                    'name' => 'promo_sku_replace',
                    'label' => Mage::helper('ampromo')->__('Remove Products'),
                    'note' => Mage::helper('ampromo')->__('Comma separated list of the SKUs'),
                ),
                'promo_sku'
            );

            $fldSet->addField(
                'promo_sku',
                'text',
                array(
                    'name' => 'promo_sku',
                    'label' => Mage::helper('ampromo')->__('Products'),
                    'note' => Mage::helper('ampromo')->__('Comma separated list of the SKUs'),
                ),
                'simple_action'
            );

            $fldSet->addField(
                'sales_hint_title',
                'text',
                array(
                    'name' => 'sales_hint_title',
                    'label' => Mage::helper('ampromo')->__('Title'),
                    'note' => Mage::helper('ampromo')->__('Sales hint title'),
                    'class' => 'validate-length maximum-length-65',
                ),
                'simple_action'
            );

            $fldSet->addField(
                'sales_hint_message',
                'textarea',
                array(
                    'name' => 'sales_hint_message',
                    'label' => Mage::helper('ampromo')->__('Message'),
                    'note' => Mage::helper('ampromo')->__('Sales hint message'),
                    'class' => 'validate-length maximum-length-100',
                ),
                'sales_hint_message'
            );
        }

        return $this;
    }

    /**
     * Display sales hint message
     *
     * @param $observer
     * @return bool
     */
    public function salesHint($observer)
    {
        $rule = $observer->getRule();
        $item = $observer->getItem();
        $itemPackageId = $item->getPackageId();

        /** @var Vznl_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');

        // register these messages on customer session,
        // onKey param should not be set, so that the sales hints are displayed based on priority order
        $customerSession->addHint(array(
            'packageId' => $itemPackageId,
            'title' => $rule->getSalesHintTitle(),
            'message' => $rule->getSalesHintMessage(),
            // make sure it arrives int in response
            'ruleId' => (int)$rule->getId(),
            'hintOnce' => $rule->getShowHint() == Dyna_PriceRules_Model_Validator::HINT_ONCE,
        ), false);

        return true;
    }

    /**
     * @param $observer
     * @return mixed
     */
    public function addConditionToSalesRule($observer)
    {
        $additional = $observer->getAdditional();
        $conditions = (array) $additional->getConditions();

        $priceRulesHelper = Mage::helper('pricerules');
        $conditions = array_merge_recursive($conditions, array(
            array('label' => $priceRulesHelper->__('Channel'), 'value' => 'pricerules/condition_channel'),
            array('label' => $priceRulesHelper->__('Dealer'), 'value' => 'pricerules/condition_dealer'),
            array('label' => $priceRulesHelper->__('Dealer Group'), 'value' => 'pricerules/condition_dealergroup'),
            array('label' => $priceRulesHelper->__('Value'), 'value' => 'pricerules/condition_value'),
            array('label' => $priceRulesHelper->__('Campaign'), 'value' => 'pricerules/condition_campaign'),
            array('label' => $priceRulesHelper->__('Customer segment'), 'value' => 'pricerules/condition_customersegment'),
            array('label' => $priceRulesHelper->__('Paidcode'), 'value' => 'pricerules/condition_paidcode'),
            array('label' => $priceRulesHelper->__('Lifecycle'), 'value' => 'pricerules/condition_lifecycle'),
            array('label' => $priceRulesHelper->__('Lifecycle detail'), 'value' => 'dyna_pricerules/condition_lifecycledetail'),
            array('label' => $priceRulesHelper->__('Brand'), 'value' => 'dyna_pricerules/condition_brand'),
            array('label' => $priceRulesHelper->__('Dealer allowed campaigns'), 'value' => 'dyna_pricerules/condition_dealerCampaign'),
            array('label' => $priceRulesHelper->__('Process context'), 'value' => 'dyna_pricerules/condition_processContext'),
            array('label' => $priceRulesHelper->__('Red Sales Id'), 'value' => 'dyna_pricerules/condition_redSalesId'),
            array('label' => $priceRulesHelper->__('Package type'), 'value' => 'vznl_pricerules/condition_packageType'),
        ));

        $additional->setConditions($conditions);
        $observer->setAdditional($additional);

        return $observer;
    }
    /**
     * Add new products to quote
     *
     * @param Mage_Sales_Model_Resource_Quote_Item_Collection $items
     * @param array $skus
     * @param Mage_Sales_Model_Resource_Quote_Item $parentItem
     */
    protected function add($items, $skus, $parentItem)
    {
        // Existing SKU in given package
        $existing = [];
        // Parse item and build existing array
        foreach ($items as &$i) {
            $existing[$i->getPackageId()][$i->getSku()][] = $i->getTargetId();
        }
        unset($i);

        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        // Parse SKUs given for add
        foreach ($skus as $sku) {
            // Check if this SKU exists already in this package
            if (isset($existing[$parentItem->getPackageId()][$sku])) {
                continue;
            }
            // Get product ID by SKU
            $id = Mage::getModel('catalog/product')->getIdBySku($sku);
            $item = $cart->addProduct($id);
            if (!is_string($item)) {
                $item->setSystemAction(Vznl_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
                $item->save();
                if (!$item->getId()) {
                    $cart->getQuote()->addItem($item);
                }
            }
        }
        // Save quote after remove items
    }

    /**
     * @param $observer
     * @return bool
     */
    public function addPromo($observer)
    {
        $rule = $observer->getRule();

        $promoSku = $rule->getPromoSku();

        if (!$promoSku) {
            return false;
        }

        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $item = $observer->getItem();
        $address = $observer->getAddress();
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $address->getQuote();

        $allItems = $quote->getAllItems();

        $existing = array();
        foreach ($allItems as $i) {
            $existing[$i->getPackageId()][$i->getSku()][] = $i->getTargetId();
        }
        unset($i);

        $promoSku = explode(',', $promoSku);


        $itemProductId = $item->getProductId();
        foreach ($promoSku as $sku) {
            $sku = trim($sku);
            if (!$sku) {
                continue;
            }

            $alreadyExisting = false;
            if (isset($existing[$item->getPackageId()][$sku]) && is_array($existing[$item->getPackageId()][$sku])) {
                $alreadyExisting = true;
                $flippedExistingPackageIdSku = array_flip($existing[$item->getPackageId()][$sku]);
            }

            if ($alreadyExisting && isset($flippedExistingPackageIdSku[$itemProductId])) {
                continue;
            }

            $product = $this->_loadProduct($sku);
            if (!$product || Mage_Catalog_Model_Product_Status::STATUS_ENABLED != $product->getStatus()) {
                continue;
            }

            if (Mage_Catalog_Model_Product_Status::STATUS_ENABLED != $product->getStatus()) {
                continue;
            }

            /** @var Dyna_Checkout_Model_Sales_Quote_Item $promoItem */
            if (!$alreadyExisting) {
                $alternateQuoteItem = $quote->getItemBySku($sku, $item->getPackageId(), true);

                if ($alternateQuoteItem && $alternateQuoteItem->isContract() && $alternateQuoteItem->isPromo()) {
                    $promoItem = $alternateQuoteItem;
                    $promoItem->isDeleted(false);
                } else {
                    // check if promo is eligible for the possible mutually exclusive products from cart
                    $promoIsExclusiveEligible = (!$rule->getIsPromo()) ? Mage::helper('vznl_pricerules')->defaultedPromoIsExclusiveEligible($rule->getId(), $sku) : true;

                    //before adding the product, check if there is no bundle product in there
                    $mutualExclCategories = Mage::helper('vznl_catalog')->getMutuallyExclusiveProducts();
                    foreach($quote->getAllItems() as $quoteItem){
                        if($item->getPackageId() != $quoteItem->getPackageId()){
                            continue;
                        }
                        //if a bundle promo is found, check if it's part of a mutual excl category as the promo product that is about to be added
                        if($quoteItem->getIsBundlePromo()){
                            foreach($mutualExclCategories as $categoryId => $mutualProducts){
                                if(in_array($quoteItem->getSku(), $mutualProducts) && in_array($sku, $mutualProducts)){
                                    $promoIsExclusiveEligible = false;
                                    break;
                                }
                            }
                        }
                    }

                    if ($promoIsExclusiveEligible) {
                        $promoItem = $quote->addProduct($product, 1);
                        $promoItem->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
                    }
                }

                if (isset($promoItem) && !is_string($promoItem)) {
                    // set active package id to the promo product
                    $promoItem->setTargetId($item->getProductId());
                    $promoItem->setIsPromo(true);
                    $promoItem->setPackageId($item->getPackageId());
                    $promoItem->setPackageType($item->getPackageType());
                    $promoItem->setEditOrderId($item->getEditOrderId());
                    $promoItem->setCustomPrice(0);
                    $promoItem->setOriginalCustomPrice(0);

                    // remove rule id from $ruleIdsToBeRemoved if promo item is still in cart
                    $ruleIdsToBeRemoved = Mage::registry('remove_applied_rule_ids');
                    if($ruleIdsToBeRemoved && array_intersect([$rule->getId()], $ruleIdsToBeRemoved)) {
                        $ruleIdsToBeRemoved = array_diff($ruleIdsToBeRemoved, $rule->getId());
                        Mage::unregister('remove_applied_rule_ids');
                        Mage::register('remove_applied_rule_ids', $ruleIdsToBeRemoved);
                    }
                }
            } else {
                $promoItem = $quote->getItemBySku($sku, $item->getPackageId());
                // set active package id to the promo product
                $promoItem->setTargetId($item->getProductId());
                $promoItem->setIsPromo(true);
            }
        }

        $address->unsetData('cached_items_nonnominal');
        $address->unsetData('cached_items_all');
        $address->unsetData('cached_items_nominal');

        return true;
    }

    /**
     * @param $observer
     * @return bool
     */
    public function removePromo($observer)
    {
        $rule = $observer->getRule();
        $promoSku = $rule->getPromoSku();

        if (!$promoSku) {
            return false;
        }

        $quote = $observer->getQuote();
        $allItems = $quote->getAllVisibleItems();

        if($promoSku = trim(array_shift(explode(',', $promoSku)))){
            foreach ($allItems as $item){
                if($item->getSku() == $promoSku){
                    if(!$item->getId()){
                        $item->isDeleted(true);
                    }
                    else{
                        $quote->removeItem($item->getId());
                    }
                }
            }
        }
        return true;
    }
}
