<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_PriceRules_Block_Adminhtml_Promo_Quote_Grid
 */
class Vznl_PriceRules_Block_Adminhtml_Promo_Quote_Grid extends Omnius_PriceRules_Block_Adminhtml_Promo_Quote_Grid
{
    /**
     * Delimiter for csv export
     * @var string
     */
    protected $_delimiterCSV = ";";

    /**
     * Retrieve Grid data as CSV
     *
     * @return string
     */
    public function getCsv()
    {
        $csv = '';
        $this->_isExport = true;
        $this->_prepareGrid();
        $this->getCollection()->getSelect()->limit();
        $this->getCollection()->setPageSize(0);
        $this->getCollection()->load();
        $this->_afterLoadCollection();

        $data = array();
        foreach ($this->_columns as $column) {
            if (!$column->getIsSystem()) {
                $data[] = '"' . $column->getExportHeader() . '"';
            }
        }
        $csv .= implode($this->_delimiterCSV, $data) . "\n";

        foreach ($this->getCollection() as $item) {
            $data = array();
            foreach ($this->_columns as $column) {
                if (!$column->getIsSystem()) {
                    $data[] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'),
                            $column->getRowFieldExport($item)) . '"';
                }
            }
            $csv .= implode($this->_delimiterCSV, $data) . "\n";
        }

        if ($this->getCountTotals()) {
            $data = array();
            foreach ($this->_columns as $column) {
                if (!$column->getIsSystem()) {
                    $data[] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'),
                            $column->getRowFieldExport($this->getTotals())) . '"';
                }
            }
            $csv .= implode($this->_delimiterCSV, $data) . "\n";
        }

        return $csv;
    }
}