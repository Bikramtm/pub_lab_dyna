<?php

/**
 * @category   Vznl
 * @package    Vznl_MixMatch
 */
class Vznl_MixMatch_Block_Adminhtml_Rules extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_rules';
        $this->_blockGroup = 'vznl_mixmatch';

        $this->_headerText = Mage::helper('dyna_mixmatch')->__('Manage MixMatch Rules');
        $this->_addButtonLabel = Mage::helper('dyna_mixmatch')->__('Add New Item');
        parent::__construct();

        $this->_addButton('lastexport', array(
            'label' => Mage::helper('dyna_mixmatch')->__('Download last uploaded Mixmatch'),
            'onclick' => 'setLocation(\'' . $this->getLastExportUrl() . '\')',
            'class' => 'go',
        ));

        $this->_addButton('export', array(
            'label' => Mage::helper('dyna_mixmatch')->__('Download Mixmatch'),
            'onclick' => 'setLocation(\'' . $this->getExportUrl() . '\')',
            'class' => 'go',
        ));

        $this->_addButton('import', array(
            'label' => Mage::helper('dyna_mixmatch')->__('Upload Mixmatch'),
            'onclick' => 'javascript:new Popup(\'' . $this->getImportUrl() . '\', {title:\'' . Mage::helper('dyna_mixmatch')->__('Upload new MixMatch file') . '\', width: 600, height:400})',
            'class' => 'go',
        ));
    }

    public function getExportUrl()
    {
        $params = array();
        $filtersName = 'mixmatch_rules_filter';
        if ($filtersValue = $this->getRequest()->getParam($filtersName)) {
            $params = array($filtersName => $filtersValue);
        }

        return $this->getUrl('*/*/export', $params);
    }

    public function getLastExportUrl()
    {
        $params = array();
        $filtersName = 'mixmatch_rules_filter';
        if ($filtersValue = $this->getRequest()->getParam($filtersName)) {
            $params = array($filtersName => $filtersValue);
        }

        return $this->getUrl('*/*/lastexport', $params);
    }

    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }
}
