<?php

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $this->getTable('dyna_mixmatch_flat');

// Only execute if the table exists
if ($installer->tableExists($tableName)) {
    $connection->addColumn($tableName, 'device_subscription_sku', "varchar(64) NULL AFTER target_sku");
}

$installer->endSetup();