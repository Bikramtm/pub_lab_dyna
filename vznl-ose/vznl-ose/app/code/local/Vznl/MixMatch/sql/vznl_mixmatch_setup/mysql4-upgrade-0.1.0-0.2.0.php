<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $this->getTable('omnius_mixmatch/mixmatch');
$fields1 = array('device_sku', 'subscription_sku', 'device_subscription_sku', 'website_id');
$fields2 = array('website_id', 'device_sku', 'subscription_sku', 'device_subscription_sku');

$index1 = $installer->getIdxName($tableName, $fields1, Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
$index2 = $installer->getIdxName($tableName, $fields2, Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);

//$connection->dropIndex($tableName, $index1);
//$connection->dropIndex($tableName, $index2);
$indexName = 'IDX_DYNA_MIXMATCH_SOURCE_TARGET_DEVICE_SUBS_WEB_ID';
$connection->addIndex($tableName, $indexName, ['source_sku','target_sku','device_subscription_sku','website_id'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
$installer->endSetup();

