<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $this->getTable('omnius_mixmatch/mixmatch');
$indexName = 'IDX_DYNA_MIXMATCH_SOURCE_TARGET_CATEGORY_DEVICE_SUBS_WEB_ID';

$connection->addIndex($tableName, $indexName, ['device_subscription_sku','website_id','source_category','target_category'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
$installer->endSetup();