<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

$table = $this->getTable('dyna_mixmatch/priceIndex');
$index = 'IDX_DYNA_MIXMATCH_FLAT_TGT_SKU_DEVICE_SUBS_SKU_WEB_ID';

$connection->addIndex($table, $index, array('device_subscription_sku','website_id','target_sku'));

$installer->endSetup();