<?php
/**
 * Class Observer
 */
class Vznl_MixMatch_Model_Observer extends Dyna_MixMatch_Model_Observer
{
    /**
     * @param $observer
     */
    public function calculateItemPrice($observer)
    {
        /** @var Mage_Sales_Model_Quote_Item $item */
        $item = $observer->getQuoteItem();

        // Ensure we have the parent item, if it has one
        $item = ( $item->getParentItem() ? $item->getParentItem() : $item );

        // if item is device, search for subscription and update price
        if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE), $item->getProduct())) {
            $this->_updateDevicePrice($item);
        }

        // if item is a subscription, search for devices and update prices
        if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $item->getProduct())) {
            $this->_updateDevicesPrice($item);
        }

        if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION), $item->getProduct())) {
            $this->_updateDevicesPrice($item);
        }
    }

    /**
     * Recalculates device prices when a subscription is removed from the cart
     *
     * @param $observer
     */
    public function recalculatePrices($observer)
    {
        /** @var Mage_Sales_Model_Quote_Item $item */
        $item = $observer->getQuoteItem();

        // if item is a subscription, search for devices and update prices
        if (Mage::helper('dyna_catalog')->is(array(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $item->getProduct())) {
            $this->_updateDevicesPrice($item);
        }

        // if item is a subscription, search for devices and update prices
        if (Mage::helper('dyna_catalog')->is(array(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION), $item->getProduct())) {
            $this->_updateDevicesPrice($item);
        }
    }

    /**
     * Overwrite the price of the added device (item) if there is a subscription in the cart
     * @param $item
     * @return bool
     */
    protected function _updateDevicePrice($item)
    {
        $this->_updatePrice($item, $this->getPricing()->getDevicePrice($item));
    }

    /**
     * Overwrite the prices of all devices in cart based on the added subscription (item)
     * @param $item
     * @return bool
     */
    protected function _updateDevicesPrice($item)
    {
        $devices = $item->getQuote()->getPackageItems($item->getPackageId());
        foreach($devices as $device) {
            // check if it is a device
            if ( ! Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE), $device->getProduct())) {
                continue; //skip items that are not devices
            }

            $this->_updatePrice($device, $this->getPricing()->getDevicePrice($device));
        }
    }

    /**
     * @param $item
     * @param $price
     */
    protected function _updatePrice($item, $price)
    {
        if($item instanceof Mage_Sales_Model_Quote_Item) {
            $item->getProduct()->setIsSuperMode(true); //Enable super mode on the product.
            $item->setCustomPrice($price) //Set the custom price
                ->setOriginalCustomPrice($price)
                ->save();
        }
    }

    /**
     * @return Dyna_MixMatch_Model_Pricing
     */
    protected function getPricing()
    {
        return Mage::getSingleton('vznl_mixmatch/pricing');
    }
}
