<?php
/**
 * @category    Vznl
 * @package     Vznl_MixMatch
 */
class Vznl_MixMatch_Model_Price extends Omnius_MixMatch_Model_Price
{
    protected $_cacheTag = 'mixmatch_price';

    /** @var Dyna_Configurator_Model_Cache */
    protected $_cache;

    /**
     * Retrive product id by sku
     *
     * @param   string $sku
     * @return  integer
     */
    public function getIdBySku($sku)
    {
        return $this->_getResource()->getIdBySku($sku);
    }

    /**
     * Processing object after save data
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        /**
         * Do not clean cache (heavy I/O) after each save
         */
        return $this;
    }

    public function checkMixMatchAvailable($previousResults, $products, $deviceCollection, $subsCollection, $deviceSubsCollection)
    {
        $device = false;
        $aikidoSubscription = false;
        $deviceSubscription = false;

        foreach ($products as $prod) {
            if ($prod->is(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE)) {
                $device = $prod;
            }

            if ($prod->is(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) && $prod->getAikido()) {
                $aikidoSubscription = $prod;
            }

            if ($prod->is(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                $deviceSubscription = $prod;
            }
        }

        $finalResult = $previousResults;
        if($device && $aikidoSubscription)
        {
            $mixMatchResult = $this->getForColumn($aikidoSubscription, $device, false);
            $flippedMixMatchResults = array_flip($mixMatchResult);
            $flippedDeviceSubsCollection = array_flip($deviceSubsCollection);
            $newResult = [];
            foreach ($finalResult as $item) {
                if ((isset($flippedDeviceSubsCollection[$item]) && isset($flippedMixMatchResults[$item])) || !isset($flippedDeviceSubsCollection[$item])) {
                    $newResult[] = $item;
                }
            }
            $finalResult = $newResult;
        }

        if($device && $deviceSubscription)
        {
            $mixMatchResult = $this->getForColumn(false, $device, $deviceSubscription);
            $flippedMixMatchResults = array_flip($mixMatchResult);
            $flippedSubsCollection = array_flip($subsCollection);
            $newResult = [];
            foreach ($finalResult as $item) {
                if ((isset($flippedSubsCollection[$item]) && isset($flippedMixMatchResults[$item])) || !isset($flippedSubsCollection[$item])) {
                    $newResult[] = $item;
                }
            }
            $finalResult = $newResult;
        }

        if($aikidoSubscription && $deviceSubscription)
        {
            $mixMatchResult = $this->getForColumn($aikidoSubscription, false, $deviceSubscription);
            $flippedMixMatchResults = array_flip($mixMatchResult);
            $flippedDeviceCollection = array_flip($deviceCollection);
            $newResult = [];
            foreach ($finalResult as $item) {
                if ((isset($flippedDeviceCollection[$item]) && isset($flippedMixMatchResults[$item])) || !isset($flippedDeviceCollection[$item])) {
                    $newResult[] = $item;
                }
            }
            $finalResult = $newResult;
        }

        return array_values(array_unique($finalResult));
    }


    /**
     * @param Dyna_Catalog_Model_Product|bool $aikidoSubscription
     * @param Dyna_Catalog_Model_Product|bool $device
     * @param Dyna_Catalog_Model_Product|bool $deviceSubscription
     * @return array
     */
    public function getForColumn($aikidoSubscription = false, $device = false, $deviceSubscription = false)
    {
        /** @var Dyna_Core_Helper_Data $coreHelper */
        $coreHelper = Mage::helper('dyna_core');
        $websiteId = Mage::app()->getStore()->getWebsiteId();

        // Trim ids from SKUs in case the product has been deleted.
        $aikidoSubscriptionSku = $aikidoSubscription ? $coreHelper->trimSku($aikidoSubscription->getSku()) : 'false';
        $deviceSku = $device ? $coreHelper->trimSku($device->getSku()) : 'false';
        $deviceSubscriptionSku = $deviceSubscription ? $coreHelper->trimSku($deviceSubscription->getSku()) : 'false';
        $key = serialize(array(
            __METHOD__,
            $aikidoSubscriptionSku,
            $deviceSku,
            $deviceSubscriptionSku,
            $websiteId
        ));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {

            $mixmatchCollection = Mage::getModel('vznl_mixmatch/price')
                ->getCollection()
                ->addFieldToFilter('website_id', $websiteId);

            if ($deviceSubscription) {

                $mixmatchCollection->addFieldToFilter('device_subscription_sku', $deviceSubscriptionSku);
            } else {
                $columnName = 'device_subscription_sku';
            }

            if ($aikidoSubscription) {
                $mixmatchCollection->addFieldToFilter('source_sku', $aikidoSubscriptionSku);
            } else {
                $columnName = 'source_sku';
            }

            if ($device) {
                $mixmatchCollection->addFieldToFilter('target_sku', $deviceSku);
            } else {
                $columnName = 'target_sku';
            }

            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $mixmatchCollection->getSelect()
                ->joinLeft(array("wl" => Mage::getSingleton('core/resource')->getTableName('catalog/product')),
                    "main_table." . $columnName . " = wl.sku")
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('wl.entity_id')
                ->distinct(true);

            $result = array_unique($connection->fetchCol($mixmatchCollection->getSelect()));
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl());
            return $result;
        }
    }

    /**
     * @return Dyna_Configurator_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_configurator/cache');
        }
        return $this->_cache;
    }
}
