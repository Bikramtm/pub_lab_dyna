<?php

interface Vznl_ValidateCartInterface_Helper_ValidateCartInterface
{
    /**
     * getCartProduct from the cart
     *
     * @return item object
     */
    public function getCartProduct();

    /**
     * createBasket will create the basket
     *
     * @return id
     */
    public function createBasket();

    /**
     * getBasketId will get the basket id from quote
     *
     * @return id
     */
    public function getBasketId();

    /**
     * setBasketId will set the basket id to the quote
     *
     * @return boolean
     */
    public function setBasketId();

    /**
     * addProduct will add product to the basket
     *
     * @return array
     */
    public function addProduct();

    /**
     * removeProduct will remove product from the basket
     *
     * @return boolean
     */
    public function removeProduct();

    /**
     * clearCart will clear the basket
     *
     * @return boolean
     */
    public function clearBasket();

    /**
     * getMultiMappers will get the multi mapper add on
     *
     * @return array
     */
    public function getMultiMappers();

    /**
     * validateBasket will validate the basket
     *
     * @return boolean
     */
    public function validateBasket();

    /**
     * validate will communicate with all other services
     *
     * @return boolean
     */
    public function validate();
}
