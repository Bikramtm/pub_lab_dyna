<?php

class Vznl_Catalog_Adminhtml_ImageuploadController extends Mage_Adminhtml_Controller_Action
{
    const BASE_FOLDER = 'product_image';
    const TMP_FOLDER  = 'imageTmp';
    const THUMBNAIL_FOLDER = 'thumbnail';

    protected $_acceptedExtensions = ['jpg', 'jpeg', 'png'];
    protected $_mimeTypeExtensions = [IMAGETYPE_JPEG => 'jpg', IMAGETYPE_PNG => 'png'];
    protected $_basePath;
    protected $_tmpPath;
    protected $_thumbnailPath;
    protected $_currentMimeType = false;

    public function indexAction()
    {
        $this->_title($this->__('Product image upload'));
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('vznl_catalog/adminhtml_imageupload_edit'));
        $this->renderLayout();
        Mage::getSingleton('adminhtml/session')->addWarning(
            $this->__('The allowed maximum file size is '.ini_get('upload_max_filesize').'. To upload a bigger file size make changes to your server setting')
        );
    }

    protected function _setAndCheckPaths()
    {
        $this->_basePath = Mage::getBaseDir('media') . '/'.self::BASE_FOLDER.'/';
        $this->_tmpPath = $this->_basePath.self::TMP_FOLDER;
        $this->_thumbnailPath = $this->_basePath.self::THUMBNAIL_FOLDER;
        $file = new Varien_Io_File();
        foreach ([$this->_basePath, $this->_tmpPath, $this->_thumbnailPath] as $checkDirectory) {
            $file->checkAndCreateFolder($checkDirectory);
        }
    }

    protected function _processFile(array $imageFile): bool
    {
        $extension = strtolower($imageFile['filetype']);
        if (!in_array($extension, $this->_acceptedExtensions)) {
            return false;
        }
        $this->_currentMimeType = false;
        for ($size = 1; $size <= 3; $size++) {
            if ($this->_createThumbnail($imageFile, $size) === false) {
                return false;
            }
        }
        $fileSystem = new Varien_Io_File();
        $fileSystem->mv($this->_tmpPath . '/' . $imageFile['text'], $this->_basePath . $imageFile['new']);
        @chmod($this->_basePath . $imageFile['text'], 0777);
        $fileSystem->rm($this->_tmpPath . '/' . $imageFile['text']);

        return true;
    }

    protected function _createThumbnail(array &$file, int $size): bool
    {
        $fileName = $file['text'];
        $pathInfo = pathinfo($fileName);
        $imageSize = 30 * $size;
        $imageObj = new Varien_Image($this->_tmpPath.'/'.$fileName);
        if ($this->_currentMimeType === false) {
            $this->_currentMimeType = $imageObj->getMimeType();
            if (!isset($this->_mimeTypeExtensions[$this->_currentMimeType])) {
                return false;
            }
            $file['new'] = $pathInfo['filename'].'.'.$this->_mimeTypeExtensions[$this->_currentMimeType];
        }
        $imageObj->constrainOnly(true);
        $imageObj->keepAspectRatio(true);
        $imageObj->keepTransparency(true);
        $imageObj->resize($imageSize, $imageSize);
        $imageObj->quality($this->_currentMimeType === IMAGETYPE_JPEG ? 95 : 100);
        $imageObj->save($this->_thumbnailPath.($size > 1 ? $size.'x' : '').'/'.$file['new']);
        @chmod($this->_thumbnailPath.($size > 1 ? $size.'x' : '').'/'.$file['new'], 0777);

        return true;
    }

    protected function _processUpload(): bool
    {
        $this->_setAndCheckPaths();

        $uploader = Mage::getModel('core/file_uploader', 'file');
        $uploader->setAllowedExtensions(['zip']);
        $uploader->save($this->_basePath);

        $file = new Varien_Io_File();
        if ($uploadFile = $uploader->getUploadedFileName()) {
            $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
            $file->mv($this->_basePath . $uploadFile, $this->_basePath.$newFilename);
            $zip = new ZipArchive();
            $zip->open($this->_basePath . $newFilename);
            $zip->extractTo($this->_tmpPath);
            $zip->close();
            $file->rm($this->_basePath.$newFilename);
        }

        $filesProcessed = 0;
        foreach ($this->_getFiles($this->_tmpPath) as $file) {
            if ($this->_processFile($file) === false) {
                continue;
            }
            $filesProcessed++;
        }

        return $filesProcessed > 0;
    }

    protected function _getFiles($path): array
    {
        $file = new Varien_Io_File();
        $file->open(['path' => $path]);

        return $file->ls(Varien_Io_File::GREP_FILES);
    }

    public function uploadAction()
    {
        try {
            if ($this->getRequest()->getPost()) {
                if ($this->_processUpload()) {
                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        $this->__('Files are uploaded and resized successfully')
                    );
                    $this->_redirect('*/*');
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
                );
                $this->_redirect('*/*');
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirect('*/*');
        }
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }
}
