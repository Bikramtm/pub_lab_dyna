<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Dyna_Catalog_Model_Resource_Product
 */
class Vznl_Catalog_Model_Resource_Product extends Dyna_Catalog_Model_Resource_Product
{
    /** @var array */
    protected static $allPackageTypes = false;
    /** @var array */
    protected static $packageTypesIds = false;

    public function getPackageTypesById($id)
    {
        self::loadPackageTypesIds();
        self::loadPackageTypeIdsPairs();
        return static::$allPackageTypes[$id] ?? null;
    }

    /**
     * Get all products (id as key) along with their package_type options (as array)
     */
    public static function loadPackageTypeIdsPairs()
    {
        if (static::$allPackageTypes === false) {
            $cacheKey = "ALL_PACKAGE_TYPES_TO_PRODUCT_IDS_CACHE";
            /** @var Dyna_Cache_Model_Cache $cacheModel */
            $cacheModel = Mage::getModel('dyna_cache/cache');
            if (!$serializedResult = $cacheModel->load($cacheKey)) {
                $resource = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                $query = "SELECT catalog_product_entity_varchar.entity_id, catalog_product_entity_varchar.value FROM catalog_product_entity_varchar 
                          LEFT JOIN eav_attribute ON eav_attribute.attribute_id = catalog_product_entity_varchar.attribute_id 
                          WHERE eav_attribute.attribute_code = 'package_type'";
                self::$allPackageTypes = $readConnection->fetchPairs($query);
                foreach (self::$allPackageTypes as $productId => $packageTypes) {
                    if ($packageTypes) {
                        $packageTypesArray = explode(',', $packageTypes);
                        foreach ($packageTypesArray as $packageType) {
                            $packageTypesIds[] = self::$packageTypesIds[$packageType];
                        }
                        self::$allPackageTypes[$productId] = $packageTypesIds;
                        unset($packageTypesArray, $packageTypesIds);
                    }
                }
                $cacheModel->save(serialize(self::$allPackageTypes), $cacheKey, array($cacheModel::PRODUCT_TAG), $cacheModel->getTtl());
            } else {
                self::$allPackageTypes = unserialize($serializedResult);
            }
        }
    }

    /**
     * Get all package_type attribute options and its corresponding entity_id from catalog_package_types
     * (cached)
     */
    public static function loadPackageTypesIds()
    {
        if (static::$packageTypesIds === false) {
            $cacheKey = "PACKAGE_TYPE_IDS_CACHE";
            /** @var Dyna_Cache_Model_Cache $cacheModel */
            $cacheModel = Mage::getModel('dyna_cache/cache');
            if (!$serializedResult = $cacheModel->load($cacheKey)) {
                $resource = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                $query = "SELECT eav_attribute_option_value.option_id, catalog_package_types.`entity_id` FROM catalog_package_types
                          LEFT JOIN eav_attribute_option_value ON eav_attribute_option_value.value = catalog_package_types.package_code
                          LEFT JOIN eav_attribute_option ON eav_attribute_option.option_id = eav_attribute_option_value.option_id
                          LEFT JOIN eav_attribute ON eav_attribute.attribute_id = eav_attribute_option.attribute_id
                          WHERE eav_attribute.attribute_code = 'package_type'";
                self::$packageTypesIds = $readConnection->fetchPairs($query);
                $cacheModel->save(serialize(self::$packageTypesIds), $cacheKey, array($cacheModel::PRODUCT_TAG), $cacheModel->getTtl());
            } else {
                self::$packageTypesIds = unserialize($serializedResult);
            }
        }
    }
}