<?php

/**
 * Class Vznl_Catalog_Model_Product_Import_CheckoutProduct
 */
class Vznl_Catalog_Model_Product_Import_CheckoutProduct extends Dyna_Catalog_Model_Product_Import_CheckoutProduct
{
    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        // This will format itself trough observer
        $product->setUrlKey($data['name']);
        $product->setProductVisibilityStrategy(
            json_encode(array(
                'strategy' => Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL,
                'groups' => null,
            ))
        );
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setIsDeleted('0');
    }
}
