<?php
class Vznl_Catalog_Model_Resource_Product_Collection extends Dyna_Catalog_Model_Resource_Product_Collection
{
    /**
     * Return all product ids for a certain package type
     * @param string $forPackageType
     * @return int[]
     */
    public function getPackageTypeProductIds(string $forPackageType, $websiteId = null)
    {
        if (!$websiteId) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }

        // try loading ids from cache instance
        if (empty(self::$sectionProductIds[$forPackageType][$websiteId])) {
            // if not cached at instance level, try loading these from redis
            $key = "package_type_product_ids_" . $forPackageType . "_" . $websiteId;
            /** @var Dyna_Cache_Model_Cache $cache */
            $cache = Mage::getSingleton('dyna_cache/cache');
            if (!(self::$sectionProductIds[$forPackageType] = unserialize($cache->load($key)))) {
                // Adding package type attribute to filtering
                $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
                $typeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $forPackageType);
                // Adding product visibility attribute to filtering
                $visibilityAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);
                $visibilityTypeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($visibilityAttribute, null, Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CONFIGURATOR);

                self::$sectionProductIds[$forPackageType][$websiteId] = $this
                    ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, array("finset" => $typeValue))
                    ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR, array("finset" => $visibilityTypeValue))
                    ->addWebsiteFilter($websiteId)
                    ->getSelect()
                    ->reset(Varien_Db_Select::COLUMNS)
                    ->columns("entity_id")
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_COLUMN);

            }

            $cache->save(serialize(self::$sectionProductIds[$forPackageType][$websiteId]), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
        }

        return self::$sectionProductIds[$forPackageType][$websiteId];
    }
}