<?php

class Vznl_Catalog_Model_Category extends Dyna_Catalog_Model_Category
{
    protected static $categoryCache = array();

    /**
     * Retrieve category path by names path
     *
     * @param string $path
     * @param string $separator
     *
     * @return array|bool
     */
    public function namePathToIdPath($path, $separator = "/")
    {
        $names = explode($separator, $path);
        // all categories have PRODUCTS category as parent
        $ids = [1, 2];
        foreach ($names as $key => $name) {
            $index = $key + 2;
            $parentId = isset($ids[$index - 1]) ? $ids[$index - 1] : 0;

            if (!isset(self::$categoryCache[$parentId])) {
                self::$categoryCache[$parentId] = Mage::getResourceModel('catalog/category_collection')
                    ->addAttributeToFilter('parent_id', $parentId)
                    ->addAttributeToSelect(['entity_id', 'name', 'unique_id']);
            }

            $category = self::$categoryCache[$parentId]->getItemByColumnValue('unique_id', $name);
            if (!$category || !$category->getId()) {

                //if not found by unique id, also try by case insensitive name check
                foreach(self::$categoryCache[$parentId] as $catTemp)
                {
                    if(strcasecmp($catTemp->getName(), $name) == 0){
                        $category = $catTemp;
                    }
                }

                if (!$category || !$category->getId()) {
                    return false;
                }
            }
            $ids[$index] = (int) $category->getEntityId();
        }
        return implode($separator, $ids);
    }

    /**
     * Get a list of all categories that are family and mutually exclusive (if the current model has an id, that id is not returned)
     *
     * @return array
     */
    public function getAllExclusiveFamilyCategoryIds()
    {
        $key = md5(serialize([strtolower(__METHOD__), $this->getId()]));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $result = [];
            /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
            $collection = $this->getCollection()
                          ->addAttributeToFilter('is_family', 1)
                          ->addAttributeToFilter('allow_mutual_products', 1)
                          ->addAttributeToFilter('entity_id', array('neq' => $this->getId()))
                          ->addAttributeToSelect('entity_id')
                          ->load();
            $result = array_keys($collection->getItems());
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }
}
