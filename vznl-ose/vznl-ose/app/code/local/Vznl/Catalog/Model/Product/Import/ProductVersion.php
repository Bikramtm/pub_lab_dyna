<?php

/**
 * Class Dyna_Catalog_Model_Product_Import_ProductVersion
 */
class Vznl_Catalog_Model_Product_Import_ProductVersion extends Dyna_Catalog_Model_Product_Import_ProductVersion
{
    protected $_logFileName = "productversion_import";

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper('vznl_import');
    }

    public function process($rawData)
    {
        if (isset($rawData['file']) && isset($rawData['executionId'])) {
            $this->_logFileName = "";
            $this->_helper->setImportLogFile($rawData['file'] . '_execution_' . $rawData['executionId'] . '.log');
            unset($rawData['file']);
            unset($rawData['executionId']);
        }
        $this->_rowNumber++;
        $rawData = array_combine($this->_header, $rawData);
        if (!array_filter($rawData)) {
            return false;
        }

        try {
            //for future modifications
            $this->_data['product_version_id'] = $rawData['product_version_id'];
            $this->_data['stack'] = $this->stack;
            if (isset($rawData['lifecycle_status']) && in_array($rawData['lifecycle_status'], Dyna_Catalog_Model_Lifecycle::getLifecycleTypes())) {
                $this->_data['lifecycle_status'] = $rawData['lifecycle_status'];
            } elseif (isset($rawData['lifecycle_status'])) {
                $this->_log("[Info] '" . $rawData['lifecycle_status'] . "' is not a valid type for lifecycle status, the supported types are: " . implode('; ', Dyna_Catalog_Model_Lifecycle::getLifecycleTypes()) . '. Lifecycle for this entry will be set to null.');
            }

            if (isset($rawData['product_family_id'])) {
                $this->_data['product_family_id'] = $rawData['product_family_id'];
            }

            if (isset($rawData['product_version_name'])) {
                $this->_data['product_version_name'] = $rawData['product_version_name'];
            }

            if (!empty($this->packageTypes) && $rawData['package_type_id'] !== '') {
                if (in_array($rawData['package_type_id'], array_keys($this->packageTypes))) {
                    $this->_data['package_type_id'] = array_search($this->packageTypes[$rawData['package_type_id']], $this->packageTypesId);
                } else {
                    $this->_log("[Info] '" . $rawData['package_type_id'] . "' is not a valid value for package type, the supported types are: " . implode('; ', $this->packageTypes) . '. package_type_id for this entry will be set to null.');
                }
            }

            /** save product version record */
            $model = Mage::getModel('dyna_catalog/productVersion');

            $model->addData($this->_data);
            $model->save();
            if ($this->getDebug()) {
                $this->_log("Successfully imported CSV line " . $this->_rowNumber . " : " . implode('|', $this->_data));
            }

            $this->_data = [];
            $this->_totalFileRows++;
        } catch (Exception $e) {
            $this->_skippedFileRows++;
            $this->_logError("[ERROR] Skipped row because :" . $e->getMessage());
            //fwrite(STDERR, $e->getMessage());
        }
    }

    public function getFileLogName()
    {
        return strtolower($this->stack) . '_' . $this->_logFileName . '.' . $this->_logFileExtension;
    }
}