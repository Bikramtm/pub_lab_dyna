<?php

/**
 * Class Vznl_Catalog_Model_Product_Import_ProductFamily
 */
class Vznl_Catalog_Model_Product_Import_ProductFamily extends Dyna_Catalog_Model_Product_Import_ProductFamily
{
    protected $_logFileName = "productfamily_import";

    /**
     * Dyna_Catalog_Model_Product_Import_ProductFamily constructor.
     */
    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper('vznl_import');
    }

    /**
     * @param $header
     * @return bool
     */
    public function setHeader($header)
    {
        $this->_header = $header;
        return true;
    }

    public function process($rawData)
    {
        $this->_rowNumber++;
        $rawData = array_combine($this->_header, $rawData);
        if (!array_filter($rawData)) {
            return false;
        }

        try {
            //for future modifications
            $this->_data['product_family_id'] = $rawData['product_family_id'];
            $this->_data['stack'] = $this->stack;

            if (isset($rawData['lifecycle_status']) && in_array($rawData['lifecycle_status'], Dyna_Catalog_Model_Lifecycle::getLifecycleTypes())) {
                $this->_data['lifecycle_status'] = $rawData['lifecycle_status'];
            } elseif (isset($rawData['lifecycle_status'])) {
                $this->_log("[Info] '" . $rawData['lifecycle_status'] . "' is not a valid type for lifecycle status, the supported types are: " . implode('; ', Dyna_Catalog_Model_Lifecycle::getLifecycleTypes()) . '. Lifecycle for this entry will be set to null.');
            }

            $this->_data['product_family_name'] = $rawData['product_family_name'];
            $this->_data['product_version_id'] = $rawData['product_version_id'] ?? '';
            $rawData['package_type_id'] = trim($rawData['package_type_id']);
            $rawData['package_subtype_id'] = trim($rawData['package_subtype_id']);

            if (!empty($this->packageTypes) && $rawData['package_type_id'] !== '') {
                if (in_array($rawData['package_type_id'], array_keys($this->packageTypes))) {
                    $this->_data['package_type_id'] = array_search($this->packageTypes[$rawData['package_type_id']], $this->packageTypesId);
                } else {
                    $this->_log("[Info] '" . $rawData['package_type_id'] . "' is not a valid value for package type, the supported types are: " . implode('; ', $this->packageTypes) . '. package_type_id for this entry will be set to null.');
                }
            }

            if ($rawData['package_subtype_id'] !== '' && isset($this->_data['package_type_id']) && $this->_data['package_type_id'] != null) {
                if ($subtype = $this->packageHelper->getPackageSubtype($rawData['package_type_id'], $rawData['package_subtype_id'], false, [])) {
                    $this->_data['package_subtype_id'] = $subtype['entity_id'];
                } else {
                    $this->_logError("[ERROR] '" . $rawData['package_subtype_id'] . "' is not a valid value for package subtype. package_subtype_id for this entry will be set to null.");
                }
            }

            /** save product family record */
            $model = Mage::getModel('dyna_catalog/productFamily');

            $model->addData($this->_data);
            $model->save();
            if ($this->getDebug()) {
                $this->_log("Successfully imported CSV line " . $this->_rowNumber . " : " . implode('|', $this->_data));
            }

            $this->_totalFileRows++;
        } catch (Exception $e) {
            $this->_skippedFileRows++;
            $this->_logError("[ERROR] Skipped row because :" . $e->getMessage());
        }
    }

    public function getLogFileName()
    {
        return strtolower($this->stack) . '_' .  $this->_logFileName . '.' . $this->_logFileExtension;
    }
}
