<?php

/**
 * Class Vznl_Catalog_Model_Lifecycle
 */
class Vznl_Catalog_Model_Lifecycle extends Dyna_Catalog_Model_Lifecycle
{
    const TYPE_LIVE = "live";
    const TYPE_LEGACY = "legacy";
    const TYPE_DISCONTINUED = "discontinued";
    const TYPE_FUTURE = "future";

    /**
     * Return available product lifecycle statuses
     * But with keys == values in order to not change the import behaviour
     * @return array
     */
    public static function getReversedLifecycleTypes()
    {
        return array(
            static::TYPE_LIVE => static::TYPE_LIVE,
            static::TYPE_LEGACY => static::TYPE_LEGACY,
            static::TYPE_DISCONTINUED => static::TYPE_DISCONTINUED,
            static::TYPE_FUTURE => static::TYPE_FUTURE,
        );
    }
}
