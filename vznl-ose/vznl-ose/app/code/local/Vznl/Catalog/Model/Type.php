<?php

/**
 * Class Vznl_Catalog_Model_Type
 */
class Vznl_Catalog_Model_Type extends Dyna_Catalog_Model_Type
{
    // mobile package subtypes
    const SUBTYPE_DEVICE_SUBSCRIPTION = 'DeviceRC';
    const SUBTYPE_DEVICE = 'Device';
    const SUBTYPE_ADDON = 'AddOn';
    // fixed package subtypes
    const SUBTYPE_FIXED_PACKAGE = 'PAKKET';
    const SUBTYPE_FIXED_PACKAGE_DETAILS = 'PAKKET_DETAILS';
    const SUBTYPE_FIXED_PACKAGE_ADDONS = 'PAKKET_ADDONS';
    const SUBTYPE_FIXED_HARDWARE_MATERIALS = 'HARDWARE_MATERIALS';
    const SUBTYPE_FIXED_GOODIES = 'GOODIES';
    const SUBTYPE_FIXED_REPLACEMENT_MATERIALS = 'REPLACEMENTMATERIALS';
    // fixed package type
    const TYPE_FIXED = 'INT_TV_FIXTEL';
    const TYPE_FIXED_PACKAGE = 'fixed';
    const FIXED_SUBTYPE_SUBSCRIPTION = 'PAKKET';
    const FIXED_SUBTYPE_ADDON = 'PAKKET_ADDONS';
    const SUBTYPE_SIMCARD = 'Simcard';
    const SUBTYPE_NONE = 'None';
    // Package creation types
    const CREATION_TYPE_FIXED = 'ZIG';

    //fixed product name
    const FIXED_TELEFONIE_PRODUCT = 'TELEFONIE';

    /**
     * Config array which holds the hardware package type
     * @return array
     */
    public static function getHardwarePackages()
    {
        return [
            strtolower(self::TYPE_HARDWARE)
        ];
    }

    /**
     * Config array which holds the fixed package type
     * @return array
     */
    public static function getFixedPackages()
    {
        return [
            strtolower(self::TYPE_FIXED)
        ];
    }

    public static $hasMafPrices = [
        self::MOBILE_SUBTYPE_ADDON,
        self::DSL_SUBTYPE_ADDON,
        self::SUBTYPE_ADDON,
        self::SUBTYPE_GOODY,
        self::MOBILE_SUBTYPE_PROMOTION,
        self::MOBILE_SUBTYPE_SUBSCRIPTION,
        self::MOBILE_PREPAID_SUBSCRIPTION,
        self::SUBTYPE_DEVICE_SUBSCRIPTION,
        self::SUBTYPE_SUBSCRIPTION,
        self::SUBTYPE_FIXED_PACKAGE,
        self::SUBTYPE_FIXED_PACKAGE_DETAILS,
        self::SUBTYPE_FIXED_PACKAGE_ADDONS,
        self::SUBTYPE_FIXED_HARDWARE_MATERIALS,
        self::SUBTYPE_FIXED_GOODIES,
        self::SUBTYPE_FIXED_REPLACEMENT_MATERIALS,
        self::SUBTYPE_FEE,
    ];

    /**
     * Config array that holds the allowed packages for the indirect channel
     * @return array
     */
    public static function packagesAvailableForIndirectStore()
    {
        return [
            strtolower(self::TYPE_MOBILE),
            strtolower(self::TYPE_FIXED)
        ];
    }

    /**
     * @return array
     */
    public static function getPackagesThatNeedServiceability()
    {
        return static::getFixedPackages();
    }

    /**
     * Retrieve all the subtypes of Fixed Package
     * @return array
     */
    public static function getFixedSubtypes()
    {
        return [
            self::SUBTYPE_FIXED_PACKAGE => self::SUBTYPE_FIXED_PACKAGE,
            self::SUBTYPE_FIXED_PACKAGE_DETAILS => self::SUBTYPE_FIXED_PACKAGE_DETAILS,
            self::SUBTYPE_FIXED_PACKAGE_ADDONS => self::SUBTYPE_FIXED_PACKAGE_ADDONS,
            self::SUBTYPE_FIXED_GOODIES => self::SUBTYPE_FIXED_GOODIES,
            self::SUBTYPE_FIXED_REPLACEMENT_MATERIALS => self::SUBTYPE_FIXED_REPLACEMENT_MATERIALS,
            self::SUBTYPE_FIXED_HARDWARE_MATERIALS => self::SUBTYPE_FIXED_HARDWARE_MATERIALS,
        ];
    }

    public static function getMobilePackages()
    {
        return [
            strtolower(self::TYPE_MOBILE),
            strtolower(self::TYPE_PREPAID),
            strtolower(self::TYPE_REDPLUS),
            strtolower(self::TYPE_HARDWARE)
        ];
    }

    public static $subscriptions = [
        self::MOBILE_SUBTYPE_SUBSCRIPTION,
        self::SUBTYPE_SUBSCRIPTION,
        self::MOBILE_PREPAID_SUBSCRIPTION,
        self::FIXED_SUBTYPE_SUBSCRIPTION
    ];

    public static $options = [
        self::MOBILE_SUBTYPE_ADDON,
        self::FIXED_SUBTYPE_ADDON,
        self::SUBTYPE_ADDON,
        self::SUBTYPE_FIXED_HARDWARE_MATERIALS,
    ];

    /**
     * @return array
     */
    public static function getCreationTypesMapping()
    {
        $creationTypesMapping = [
            strtolower(self::TYPE_MOBILE) => self::CREATION_TYPE_MOBILE,
            strtolower(self::TYPE_PREPAID) => self::CREATION_TYPE_PREPAID,
            strtolower(self::TYPE_CABLE_INDEPENDENT) => self::CREATION_TYPE_CABLE_INDEPENDENT,
            strtolower(self::TYPE_CABLE_INTERNET_PHONE) => self::CREATION_TYPE_CABLE_INTERNET_PHONE,
            strtolower(self::TYPE_CABLE_TV) => self::CREATION_TYPE_CABLE_TV,
            strtolower(self::TYPE_FIXED_DSL) => self::CREATION_TYPE_DSL,
            strtolower(self::TYPE_LTE) => self::CREATION_TYPE_LTE,
            strtolower(self::TYPE_FIXED) => self::CREATION_TYPE_FIXED,
            strtolower(self::TYPE_HARDWARE) => self::CREATION_TYPE_HARDWARE
        ];

        return array_map('strtolower', $creationTypesMapping);
    }
}
