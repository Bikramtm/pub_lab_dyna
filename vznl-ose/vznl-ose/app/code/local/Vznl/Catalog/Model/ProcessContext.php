<?php
/**
 * Class Dyna_Catalog_Model_ProcessContext
 */
class Vznl_Catalog_Model_ProcessContext extends Dyna_Catalog_Model_ProcessContext
{
    // Move address service check
    const SERVICECHECK = "SERVICECHECK";

    protected $matchRulePCTable;

    protected function _construct()
    {
        $this->_init("dyna_catalog/processContext");
        $this->matchRulePCTable = (string) Mage::getSingleton('core/resource')->getTableName('dyna_productmatchrule/matchRulePc');
    }

    /**
     * Get set of all process contexts
     * @param $processContextString
     * @return string
     */
    public function getProcessContextSetId($processContextString)
    {
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $select = $write->select();

        $select->from($this->matchRulePCTable, 'entity_id')
            ->where('process_context_ids = :process_context_ids');

        $processContextSetId = $write->fetchOne($select, array(':process_context_ids' => $processContextString));

        if ($processContextSetId) {
            return $processContextSetId;
        } else {
            // if not existing, create it
            $row = array(
                'process_context_ids' => $processContextString
            );

            $write->insert($this->matchRulePCTable,$row);
            return $write->lastInsertId();
        }
    }

    /**
     * Move process contexts
     * @return array
     */
    public static function MoveProcessContexts()
    {
        return [self::MOVE];
    }

    /**
     * ILS and Move process contexts
     * @return array
     */
    public static function ILSAndMoveProcessContexts()
    {
        return [self::ILSBLU, self::ILSNOBLU, self::MOVE];
    }
}