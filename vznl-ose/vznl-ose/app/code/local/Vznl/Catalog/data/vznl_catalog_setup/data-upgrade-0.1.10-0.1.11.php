<?php

/**
 * Add Simcard subpackage to package_subtype attribute
 */

$installer = $this;
/* @var $installer Dyna_Mobile_Model_Resource_Setup */

$installer->startSetup();

/** @var Mage_Eav_Model_Attribute $attribute */
$attributeModel = Mage::getModel('eav/entity_attribute');
/** @var Mage_Eav_Model_Entity_Attribute_Source_Table $attributeOptionModel */
$attributeOptionModel = Mage::getModel('eav/entity_attribute_source_table') ;

// Add package_type option
$attributeId = $installer->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'package_subtype', 'attribute_id');

/** @var Mage_Eav_Model_Attribute $attribute */
$attribute = $attributeModel->load($attributeId);
$attributeOptionModel->setAttribute($attribute);
$options = $attributeOptionModel->getAllOptions(false);

$newPackageSubtype = ['Simcard'];

$addNew = true;
foreach ($options as $option) {
    if (in_array($option['label'], $newPackageSubtype)) {
        $addNew = false;
    }
}
if ($addNew) {
    $newOption = [
        'attribute_id' => $attributeId,
        'values' => $newPackageSubtype,
    ];

    $installer->addAttributeOption($newOption);
}

$installer->endSetup();