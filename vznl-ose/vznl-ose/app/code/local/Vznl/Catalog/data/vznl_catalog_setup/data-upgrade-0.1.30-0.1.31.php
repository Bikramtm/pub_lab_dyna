<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->defaultGroupIdAssociations = null;

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeSets = [
    'voice_subscription' => [
        'General' => [
            'sort' => 1,
            'attributes' => [
                'vat',
                'effective_date',
                'maf',
                'checkout_product',
                'prijs_belminuut_buiten_bundel',
                'prijs_sms_buiten_bundel',
                'prodspecs_brand',
                'prodspecs_sms_amount',
                'prodspecs_wms_codes',
                'additional_text',
                'product_version',
                'product_family',
                'product_type',
                'ask_confirmation_first',
                'confirmation_text',
                'display_name_cart',
                'display_name_communication',
                'display_name_configurator',
                'display_name_inventory',
                'hierarchy_value',
                'contract_value',
                'price_increase_maf',
                'price_increase_scope',
                'lifecycle_status',
            ],
            'attributesToRemove' => [
                'regular_maf'
            ],
        ]
    ],
    'data_subscription' => [
        'General' => [
            'sort' => 1,
            'attributes' => [
                'vat',
                'effective_date',
                'maf',
                'checkout_product',
                'prijs_belminuut_buiten_bundel',
                'prijs_sms_buiten_bundel',
                'prodspecs_brand',
                'prodspecs_sms_amount',
                'prodspecs_wms_codes',
                'prodspecs_aantal_belminuten',
                'additional_text',
                'product_version',
                'product_family',
                'product_type',
                'ask_confirmation_first',
                'confirmation_text',
                'display_name_cart',
                'display_name_communication',
                'display_name_configurator',
                'display_name_inventory',
                'hierarchy_value',
                'contract_value',
                'price_increase_maf',
                'price_increase_scope',
                'identifier_subscr_type',
                'lifecycle_status',
            ],
            'attributesToRemove' => [
                'regular_maf',
                'identifier_subscr_type_data'
            ],
        ]
    ]
];

// add additional_text
$attr = Mage::getResourceModel('catalog/eav_attribute')->loadByCode($entityTypeId, 'additional_text');
if (!$attr->getId()) {
    $installer->addAttribute(
        $entityTypeId,
        'additional_text',
        [
            'label' => 'Additional text',
            'input' => 'textarea',
            'type' => 'text',
            'required' => false,
        ]
    );
}

foreach ($attributeSets as $attributeSetName => $attributeSetGroups) {
    // load current attribute set
    $attributeSet = $installer->getAttributeSet($entityTypeId, $attributeSetName);

    foreach ($attributeSetGroups as $groupName => $groupData) {
        /** @var Mage_Eav_Model_Entity_Setup $currentGroup */
        $currentGroup = $installer->getAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName);

        if (!$currentGroup) {
            $installer->addAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName, $groupData['sort']);

            $currentGroup = $installer->getAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName);
        }

        //get a list of all attribute codes of the current group
        $currentGroupAttributes = Mage::getResourceModel('catalog/product_attribute_collection')
            ->setAttributeGroupFilter($currentGroup['attribute_group_id']);
        $currentGroupAttributes->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $currentGroupAttributes->addFieldToSelect('attribute_code');
        $currentGroupAttributes = $connection->fetchCol($currentGroupAttributes->getSelect());

        foreach ($groupData['attributes'] as $attributeCode) {
            //verify if attributeCode is in current group
            if (!in_array($attributeCode, $currentGroupAttributes)) {
                $attributeId = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $attributeCode)->getId();
                //assign attribute to current group
                $installer->addAttributeToGroup($entityTypeId, $attributeSet['attribute_set_id'], $currentGroup['attribute_group_id'], $attributeId);
            }
        }

        // removed unused attributes from attribute set group
        if (isset($groupData['attributesToRemove'])) {
            foreach ($groupData['attributesToRemove'] as $attributeCode) {
                if (in_array($attributeCode, $currentGroupAttributes)) {
                    $attributeId = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $attributeCode)->getId();
                    $installer->deleteTableRow(
                        'eav/entity_attribute',
                        'attribute_id',
                        $attributeId,
                        'attribute_set_id',
                        $attributeSet['attribute_set_id']
                    );
                }
            }
        }
    }
}


$installer->startSetup();
$installer->getConnection();
$installer->updateAttribute('catalog_product', 'product_segment', 'is_filterable', 1);
$installer->updateAttribute('catalog_product', 'int_bundle_type', 'is_filterable', 1);
$installer->updateAttribute('catalog_product', 'dtv_bundle_type', 'is_filterable', 1);
$installer->updateAttribute('catalog_product', 'bundle_identifier', 'is_filterable', 1);
$this->endSetup();

