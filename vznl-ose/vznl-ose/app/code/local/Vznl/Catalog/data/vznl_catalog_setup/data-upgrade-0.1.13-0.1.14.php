<?php
/**
 * Add the new fixed subtypes to the package_subtype
 */
$installer = $this;
$installer->startSetup();
$attributeCode = 'package_subtype';
$attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);

if ($attribute->getId() && $attribute->getFrontendInput() == 'select') {
    /** Fixed custom subtypes */
    $newOptions = Vznl_Catalog_Model_Type::getFixedSubtypes();
    $exitOptions = array();
    $options = Mage::getModel('eav/entity_attribute_source_table')
        ->setAttribute($attribute)
        ->getAllOptions(false);
    foreach ($options as $option) {
        //make sure that the script founds if any fixed subtype was added manually
        if (in_array(strtolower($option['label']), array_map("strtolower", $newOptions))) {
            $exitOptions['value'][$option['value']] = true;
            $exitOptions['delete'][$option['value']] = true;
        }
    }

    if (!empty($exitOptions)) {
        $exitOptions['attribute_id'] = $attribute->getId();
        //remove old attributes
        $installer->addAttributeOption($exitOptions);
    }

    if (!empty($newOptions)) {
        $add['attribute_id'] = $attribute->getId();
        $add['values'] = array_values($newOptions);
        //add fixed subtypes
        $installer->addAttributeOption($add);
    }
}