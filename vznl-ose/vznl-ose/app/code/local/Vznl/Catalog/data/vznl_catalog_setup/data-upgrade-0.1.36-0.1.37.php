<?php
/* @var $installer Mage_Catalog_Model_Resource_Setup */

$installer = $this;

$installer->startSetup();

/*** Update produt catalog attribute*/
$installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'sku_type', 'frontend_input', 'text');

$installer->endSetup();