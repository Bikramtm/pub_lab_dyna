<?php

/* @var $installer Mage_Catalog_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

//remove the existing attributes since it was a required field
$groupName = 'General';
$attributeCodes = array('fixed_int_part', 'fixed_tv_part', 'fixed_tel_part', 'portfolioidentifier', 'belongs_to_family', 'product_status_online');
foreach ($attributeCodes as $attributeCode) {
    $attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
    if ($attributeId) {
        $installer->removeAttribute($entityTypeId, $attributeCode);
    }
}

// add the attributes with required set to false
$attributes=[
    'fixed_int_part' => [
        'group' => '',
        'label' => 'Fixed Int Part',
        'input' => 'text',
        'type' => 'varchar',
        'visible_on_front' => true,
        'filterable' => false,
        'searchable' => false,
        'apply_To' => 'simple',
        'user_defined' => true,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required' => false,
        'backend' => ''
    ],
    'fixed_tv_part' => [
        'group' => '',
        'label' => 'Fixed TV Part',
        'input' => 'text',
        'type' => 'varchar',
        'visible_on_front' => true,
        'filterable' => false,
        'searchable' => false,
        'apply_To' => 'simple',
        'user_defined' => true,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required' => false,
        'backend' => ''
    ],
    'fixed_tel_part' => [
        'group' => '',
        'label' => 'Fixed TEL Part',
        'input' => 'text',
        'type' => 'varchar',
        'visible_on_front' => true,
        'filterable' => false,
        'searchable' => false,
        'apply_To' => 'simple',
        'user_defined' => true,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required' => false,
        'backend' => ''
    ],
    'portfolioidentifier' => [
        'group' => '',
        'label' => 'Portfolio Identifier',
        'input' => 'text',
        'type' => 'varchar',
        'visible_on_front' => true,
        'filterable' => false,
        'searchable' => false,
        'apply_To' => 'simple',
        'user_defined' => true,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required' => false,
        'backend' => ''
    ],
    'belongs_to_family' => [
        'group' => '',
        'label' => 'Belongs To Family',
        'input' => 'text',
        'type' => 'varchar',
        'visible_on_front' => true,
        'filterable' => false,
        'searchable' => false,
        'apply_To' => 'simple',
        'user_defined' => true,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required' => false,
        'backend' => ''
    ],
    'product_status_online' => [
        'group' => '',
        'label' => 'Product Status Online',
        'input' => 'text',
        'type' => 'varchar',
        'visible_on_front' => true,
        'filterable' => false,
        'searchable' => false,
        'apply_To' => 'simple',
        'user_defined' => true,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required' => false,
        'backend' => ''
    ]
];

//add attributes
foreach ($attributes as $attributeCode => $options) {
    $attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
    if (!$attributeId) {
        $installer->addAttribute($entityTypeId, $attributeCode, $options);
    }
}

/*
    Attribute Sets :'key' = Attribute code (i.e name of attribute) and
    'value' = Attribute set names where the attribute is assigned.
*/
$attributeSets=[
    'fixed_int_part' => [
        'Fixed_Bundle_Product'
    ],
    'fixed_tv_part' => [
        'Fixed_Bundle_Product'
    ],
    'fixed_tel_part' => [
        'Fixed_Bundle_Product'
    ],
    'portfolioidentifier' => [
        'Fixed_Bundle_Product',
        'Fixed_Internet_Product',
        'Fixed_Generic_Product',
        'Fixed_Promotions',
        'Fixed_Tel_Product',
        'Fixed_TV_product'
    ],
    'belongs_to_family' => [
        'Fixed_Internet_Product',
        'Fixed_Generic_Product',
        'Fixed_Promotions',
        'Fixed_Tel_Product',
        'Fixed_TV_product'
    ],
    'product_status_online' => [
        'Fixed_Bundle_Product',
        'Fixed_Internet_Product',
        'Fixed_Generic_Product',
        'Fixed_Promotions',
        'Fixed_Tel_Product',
        'Fixed_TV_product'
    ]
];

// Assigning the attribute set to the attributes
foreach ($attributes as $attributeName => $attributeDetails) {
    foreach ($attributeSets[$attributeName] as $attributeSetName) {
        $attributeSetId = $installer->getAttributeSetId($entityTypeId, $attributeSetName);
        if($attributeSetId){
            $installer->addAttributeToSet($entityTypeId, $attributeSetId, 'General', $attributeName);
        }
    }
}
$installer->endSetup();