<?php
/**
 * Change the cable packageCreationType from Prepaid to Cable_internet_phone
 */

$packageCreationTypes = Mage::getModel('dyna_package/packageCreationTypes');
$cablePackageType = $packageCreationTypes->getCollection()
    ->addFieldToFilter('name', Vznl_Catalog_Model_Type::CREATION_TYPE_CABLE_INTERNET_PHONE)
    ->getFirstItem();

if ($cablePackageType->getId()) {
    $cablePackageType->setPackageTypeCode(Vznl_Catalog_Model_Type::CREATION_TYPE_CABLE_INTERNET_PHONE)->save();
}