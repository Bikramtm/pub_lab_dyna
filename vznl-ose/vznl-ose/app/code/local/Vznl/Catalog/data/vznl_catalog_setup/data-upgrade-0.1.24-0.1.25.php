<?php
/**
 * Add attributes to all the fixed attribute sets except promotion attribute set
 */
/* @var $installer Mage_Sales_Model_Entity_Setup */

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeSets = array(
    "Fixed_Bundle_Product" => [
        "Fixed Attributes" => [
            "price_increase_maf",
            "price_increase_scope"
        ]
    ],
    "Fixed_TV_Product" => [
        "Fixed Attributes" => [
            "price_increase_maf",
            "price_increase_scope"
        ]
    ],
    "Fixed_Internet_Product" => [
        "Fixed Attributes" => [
            "price_increase_maf",
            "price_increase_scope"
        ]
    ],
    "Fixed_Tel_Product" => [
        "Fixed Attributes" => [
            "price_increase_maf",
            "price_increase_scope"
        ]
    ],
    "Fixed_Generic_Product" => [
        "Fixed Attributes" => [
            "price_increase_maf",
            "price_increase_scope"
        ]
    ]
);

foreach ($attributeSets as $setCode => $attributes) {
    foreach ($attributes as $groupName => $attribute) {
        foreach ($attribute as $attributeMap) {
            $attributeSetId = $installer->getAttributeSet($entityTypeId, $setCode, "attribute_set_id");
            $installer->addAttributeToSet($entityTypeId, $attributeSetId, $groupName, $attributeMap);
        }
    }
}
$installer->endSetup();