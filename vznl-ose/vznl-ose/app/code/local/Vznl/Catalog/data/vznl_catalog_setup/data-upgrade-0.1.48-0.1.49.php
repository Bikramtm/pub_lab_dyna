<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

// update custom attribute "price_discount_percent " to "price_discount_percent"
$installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'price_discount_percent ', 
	array('attribute_code' => 'price_discount_percent'));

$installer->endSetup();
