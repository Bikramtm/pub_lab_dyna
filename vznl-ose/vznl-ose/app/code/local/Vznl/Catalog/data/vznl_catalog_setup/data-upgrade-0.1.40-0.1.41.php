<?php
$installer = $this;
$installer->startSetup();

$attributeCode = 'voice_product';
$attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);

if ($attribute->getId() && $attribute->getFrontendInput() == 'select') {
    $newOptions = array('First', 'Second', 'Both', 'NULL');
    $exitOptions = array();
    $options = Mage::getModel('eav/entity_attribute_source_table')
        ->setAttribute($attribute)
        ->getAllOptions(false);
    $exitOptions = array_column($options, label);

    $insertOptions = array_diff($newOptions, $exitOptions);
    $option['attribute_id'] = $attribute->getId();
    $arrayCount = sizeof($insertOptions);
    for($iCount=0;$iCount<$arrayCount;$iCount++){
        $option['value']['option'.$iCount][0] = $insertOptions[$iCount];
    }
    $installer->addAttributeOption($option);

}
$installer->endSetup();
