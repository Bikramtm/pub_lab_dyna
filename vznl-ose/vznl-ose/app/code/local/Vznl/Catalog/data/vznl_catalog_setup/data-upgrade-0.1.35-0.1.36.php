<?php
/**
 * Add attribute and attribute set for fixed products. These attributes are created for fixed products import, and will be
 * available in the configurator for creating fixed rules
 */
/* @var $installer Mage_Sales_Model_Entity_Setup */

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Create attributes
$attributes = [
    'identifier_cons_bus' => [
        'input' => 'select',
        'type' => 'varchar',
        'label' => 'Identifier Cons Bus',
        'visible' => true,
        'required' => true,
        'visible_on_front' => false,
        'unique' => false,
        'default' => 'Both',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => 'eav/entity_attribute_backend_array',
        'option' => [
            'values' => [
                'Business',
                'Private',
                'Both',
            ]
        ],
    ],
];

foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $code, $options);
    }
}
unset($attributes);

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Assign attribute to attribute set with group label
$attributeSets = array(
    "prepaid_simonly" => [
        "General" => [
            "identifier_cons_bus", "identifier_simcard_selector", "promo_description", "service_item_type"
        ]
    ],
    "data_device" => [
        "General" => [
            "channel", "product_family_id", "product_version_id", "identifier_hawaii_prep_orig_id", "loan_indicator"
        ]
    ],
    "dummy_device" => [
        "General" => [
                "vat", "maf",  "prodspecs_soccode",  "prodspecs_4g_lte",  "prodspecs_aantal_belminuten",  "prodspecs_brand",  "prodspecs_data_aantal_mb",  "prodspecs_hawaii_channel",  "prodspecs_hawaii_usp1",  "prodspecs_hawaii_usp2",  "prodspecs_hawaii_usp3",  "prodspecs_sms_amount",  "effective_date",  "ask_confirmation_first",  "identifier_axi_stockable_prod",  "channel",  "checkout_product",  "confirmation_text",  "contract_value",  "display_name_cart",  "display_name_communication",  "display_name_configurator",  "display_name_inventory",  "hierarchy_value",  "identifier_hawaii_id", "lifecycle_status", "price_increase_maf",  "price_increase_scope",  "prodspecs_wms_codes",  "product_family_id",  "product_type",  "product_version_id", "visibility",  "axi_safety_stock",  "axi_threshold",  "hawaii_def_subscr_cbu",  "hawaii_def_subscr_ebu", "identifier_hawaii_type",  "identifier_hawaii_type_name",  "prijs_thuiskopieheffing_type",  "prodspecs_2e_camera",  "prodspecs_3g",  "prodspecs_accu_verwisselb",  "prodspecs_afmetingen_breedte",  "prodspecs_afmetingen_dikte",  "prodspecs_afmetingen_lengte",  "prodspecs_afmetingen_unit",  "prodspecs_audio_out",  "prodspecs_batterij",  "prodspecs_belmin_sms_config",  "prodspecs_besturingssysteem",  "prodspecs_bluetooth",  "prodspecs_bluetooth_version",  "prodspecs_camera",  "prodspecs_data_mb_config",  "prodspecs_display_features",  "prodspecs_filmen_in_hd",  "prodspecs_flitser",  "prodspecs_gprs_edge",  "prodspecs_gps",  "prodspecs_hawaii_htmlkleurcode",  "prodspecs_hawaii_os_versie",  "prodspecs_hawaii_preorder",  "prodspecs_hawaii_speed_max",  "prodspecs_hdmi",  "prodspecs_kleur",  "prodspecs_megapixels_1e_cam",  "prodspecs_nfc",  "prodspecs_opslag_gb",  "prodspecs_opslag_uitbreidbaar",  "prodspecs_prepaid_simlock",  "prodspecs_processor",  "prodspecs_schermdiagonaal_inch",  "prodspecs_schermresolutie",  "prodspecs_spreektijd",  "prodspecs_standby_tijd",  "prodspecs_touchscreen",  "prodspecs_vfwallet",  "prodspecs_weight_gram",  "prodspecs_werkgeheugen_mb",  "prodspecs_wifi",  "prodspecs_wifi_frequenties",  "stock_status", "loan_indicator", "identifier_device_post_prepaid", "prijs_thuiskopieheffing_bedrag", "prijs_beltegoed_bedrag", "identifier_simcard_formfactor", "prodspecs_ecoscore", "identifier_hawaii_prep_orig_id", "terms_and_conditions"
        ]
    ],
    "service_item" => [
        "General" => [
            "identifier_cons_bus", "identifier_simcard_selector", "prijs_beltegoed_bedrag", "promo_description", "identifier_hawaii_type", "identifier_hawaii_type_name", "identifier_simcard_type"
        ]
    ],
    "simcard" => [
        "General" => [
            "cost", "identifier_cons_bus", "prijs_beltegoed_bedrag", "promo_description", "service_item_type"
        ]
    ],
    "voice_device" => [
        "General" => [
            "product_family_id", "product_version_id"
        ]
    ]
);

foreach ($attributeSets as $setCode => $attributes) {
    foreach ($attributes as $groupName => $attribute) {
        foreach ($attribute as $attributeMap) {
            $attributeSetId = $installer->getAttributeSet($entityTypeId, $setCode, "attribute_set_id");
            $installer->addAttributeToSet($entityTypeId, $attributeSetId, $groupName, $attributeMap);
        }
    }
}

$installer->endSetup();
