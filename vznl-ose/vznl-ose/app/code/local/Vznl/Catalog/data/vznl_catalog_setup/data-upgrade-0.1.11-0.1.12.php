<?php
/**
 * Add attribute and attribute set for fixed products. These attributes are created for fixed products import, and will be
 * available in the configurator for creating fixed rules
 */
/* @var $installer Mage_Sales_Model_Entity_Setup */

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Create attributes
$attributes = [
    'vat' => [
        'group' => 'Fixed Attributes',
        'label' => 'vat',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'class' => 'validate-number',
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'maf' => [
        'group' => 'Fixed Attributes',
        'label' => 'maf',
        'input' => 'price',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'effective_date' => [
        'group' => 'Fixed Attributes',
        'label' => 'Effective Date',
        'input' => 'datetime',
        'type' => 'datetime',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'hierarchy_value' => [
        'group' => 'Fixed Attributes',
        'label' => 'Hierarchy Value',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'contract_value' => [
        'group' => 'Fixed Attributes',
        'label' => 'Contract Value',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'lifecycle_status' => [
        'group' => 'Fixed Attributes',
        'label' => 'Lifecycle Status',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'default' => 'live',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'live',
                        1 => 'legacy',
                        2 => 'discontinued',
                        3 => 'future'
                    )
            )
    ],
    'display_name_inventory' => [
        'group' => 'Fixed Attributes',
        'label' => 'Display Name Inventory',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'display_name_configurator' => [
        'group' => 'Fixed Attributes',
        'label' => 'Display Name Configurator',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'display_name_cart' => [
        'group' => 'Fixed Attributes',
        'label' => 'Display Name Cart',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'display_name_communication' => [
        'group' => 'Fixed Attributes',
        'label' => 'Display Name Communication',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'product_version_id' => [
        'group' => 'Fixed Attributes',
        'label' => 'Product Version Id',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'product_family_id' => [
        'group' => 'Fixed Attributes',
        'label' => 'Product Family Id',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'template_360' => [
        'group' => 'Fixed Attributes',
        'label' => 'Template 360',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'bundle_hint' => [
        'group' => 'Fixed Attributes',
        'label' => 'Bundle Hint',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'has_internet' => [
        'group' => 'Fixed Attributes',
        'label' => 'has_internet',
        'input' => 'boolean',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'default' => '0',
    ],
    'int_bundle_type' => [
        'group' => 'Fixed Attributes',
        'label' => 'int_bundle_type',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'Start',
                        1 => 'Complete',
                        2 => 'Max',
                        3 => 'None'
                    ),
            ),
        'default' => 'None',
    ],
    'download_speed' => [
        'group' => 'Fixed Attributes',
        'label' => 'Download Speed',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'upload_speed' => [
        'group' => 'Fixed Attributes',
        'label' => 'Upload Speed',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'int_security_licenses' => [
        'group' => 'Fixed Attributes',
        'label' => 'Int Security Licenses',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'int_security_licenses_tag' => [
        'group' => 'Fixed Attributes',
        'label' => 'Int Security Licenses Tag',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'Free',
                        1 => 'Paid'
                    ),
            ),
        'default' => ''
    ],
    'is_fixed_ip_tier' => [
        'group' => 'Fixed Attributes',
        'label' => 'Is Fixed Ip Tier',
        'input' => 'boolean',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'has_dtv' => [
        'group' => 'Fixed Attributes',
        'label' => 'Has DTV',
        'input' => 'boolean',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'default' => '0',
    ],
    'dtv_bundle_level' => [
        'group' => 'Fixed Attributes',
        'label' => 'DTV Bundle Level',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => '0',
                        1 => '1',
                        2 => '2',
                        3 => '3',
                        4 => '4',
                        5 => '5'
                    ),
            ),
        'default' => '0',
    ],
    'dtv_bundle_type' => [
        'group' => 'Fixed Attributes',
        'label' => 'DTV Bundle Type',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'Start',
                        1 => 'Complete',
                        2 => 'Max',
                        3 => 'Online TV'
                    ),
            ),
        'default' => '',
    ],
    'tv_product' => [
        'group' => 'Fixed Attributes',
        'label' => 'TV Product',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'CATV',
                        1 => 'DTV'
                    ),
            ),
        'default' => '',
    ],
    'tv_content' => [
        'group' => 'Fixed Attributes',
        'label' => 'TV Content',
        'input' => 'multiselect',
        'type' => 'text',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => 'eav/entity_attribute_backend_array',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'Film1',
                        1 => 'MS',
                        2 => 'MSL',
                        3 => 'MSXL',
                        4 => 'FOXEDL',
                        5 => 'FOXINT',
                        6 => 'ZST',
                        7 => 'Hindi',
                        8 => 'Erotiek',
                        9 => 'Videoland',
                        10 => 'Turks'
                    ),
            ),
        'default' => '',
    ],
    'tv_nr_channels' => [
        'group' => 'Fixed Attributes',
        'label' => 'TV NR channels',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'tv_box_type' => [
        'group' => 'Fixed Attributes',
        'label' => 'TV Box Type',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'Main',
                        1 => 'Secondary',
                        2 => 'none'
                    ),
            ),
        'default' => '',
    ],
    'tv_box' => [
        'group' => 'Fixed Attributes',
        'label' => 'TV Box',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'CI+',
                        1 => 'MB',
                        2 => 'MBXL',
                        3 => 'EOS',
                        4 => 'B2OSC'
                    ),
            ),
        'default' => '',
    ],
    'has_fixed_tel' => [
        'group' => 'Fixed Attributes',
        'label' => 'Has Fixed Tel',
        'input' => 'boolean',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'default' => '0',
    ],
    'voice_product' => [
        'group' => 'Fixed Attributes',
        'label' => 'Voice Product',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'firstline',
                        1 => 'Secondline'
                    ),
            ),
        'default' => '',
    ],
    'voice_plan' => [
        'group' => 'Fixed Attributes',
        'label' => 'Voice Plan',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'volop bellen',
                        1 => 'volop bellen internationaal'
                    ),
            ),
        'default' => '',
    ],
    'voice_plan_applies_over' => [
        'group' => 'Fixed Attributes',
        'label' => 'voice_plan_applies_over',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'first line',
                        1 => 'second line',
                        2 => 'both'
                    ),
            ),
        'default' => '',
    ],
    'bundle_identifier' => [
        'group' => 'Fixed Attributes',
        'label' => 'bundle_identifier',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'Alles-in-1',
                        1 => 'Internet & DTV',
                        2 => 'Internet en Online TV',
                        3 => 'TV only',
                        4 => 'Internet & Bellen',
                        5 => 'Bellen en Online TV'
                    ),
            ),
        'default' => '',
    ],
    'identifier_required_footprint' => [
        'group' => 'Fixed Attributes',
        'label' => 'identifier_required_footprint',
        'input' => 'multiselect',
        'type' => 'text',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => 'eav/entity_attribute_backend_array',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'UPC',
                        1 => 'Ziggo'
                    ),
            ),
        'default' => '',
    ],
    'is_ccr_benefit_eligible_part' => [
        'group' => 'Fixed Attributes',
        'label' => 'is_ccr_benefit_eligible_part',
        'input' => 'boolean',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'default' => '0',
    ],
    'is_ccr_benefit_product' => [
        'group' => 'Fixed Attributes',
        'label' => 'is_ccr_benefit_product',
        'input' => 'boolean',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'default' => '0',
    ],
    'price_increase_maf' => [
        'group' => 'Fixed Attributes',
        'label' => 'price_increase_maf',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'price_increase_scope' => [
        'group' => 'Fixed Attributes',
        'label' => 'price_increase_scope',
        'input' => 'boolean',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'default' => '0',
    ],
    'requires_servicability_item' => [
        'group' => 'Fixed Attributes',
        'label' => 'requires_servicability_item',
        'input' => 'multiselect',
        'type' => 'text',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => 'eav/entity_attribute_backend_array',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'tbd'
                    ),
            ),
        'default' => '',
    ],
    'required_servicability' => [
        'group' => 'Fixed Attributes',
        'label' => 'required_servicability',
        'input' => 'boolean',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'default' => '0',
    ],
    'type_other_addons' => [
        'group' => 'Fixed Attributes',
        'label' => 'type_other_addons',
        'input' => 'select',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'none',
                        1 => 'replacementmaterials',
                        2 => 'activationCharge'
                    ),
            ),
        'default' => '',
    ],
    'product_subline' => [
        'group' => 'Fixed Attributes',
        'label' => 'product_subline',
        'input' => 'textarea',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'delivery_comment' => [
        'group' => 'Fixed Attributes',
        'label' => 'delivery_comment',
        'input' => 'textarea',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],

];

foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $code, $options);
    }
}
unset($attributes);

// creating attribute set
$attributeSetCode = array(
    "Fixed_Bundle_Product",
    "Fixed_TV_Product",
    "Fixed_Internet_Product",
    "Fixed_Tel_Product",
    "Fixed_Generic_Product"
);
$attributeGroupNames = [
    1 => 'Fixed Attributes'
];
$attributeSortOrder = 10;
$tableName = $installer->getTable('eav_attribute_group');
foreach ($attributeSetCode as $code) {
    $installer->addAttributeSet($entityTypeId, $code, $attributeSortOrder++);
    $attributeSetId = $installer->getAttributeSet($entityTypeId, $code, "attribute_set_id");
    foreach ($attributeGroupNames as $sortOrder => $attributeGroupName) {
        $installer->run('INSERT INTO ' . $tableName . ' (`attribute_set_id`, `attribute_group_name`, `sort_order`) VALUES ("' . $attributeSetId . '","' . $attributeGroupName . '","' . $sortOrder . '")');
    }
}

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Assign attribute to attribute set with group label
$attributeSets = array(
    "Fixed_Bundle_Product" => [
        "General" => [
            "sku", "name", "description", "package_type", "package_subtype", "status", "product_visibility", "price"
        ],
        "Fixed Attributes" => [
            "vat", "maf", "sorting", "effective_date", "expiration_date", "initial_selectable", "hierarchy_value", "contract_value", "lifecycle_status", "display_name_inventory", "display_name_configurator", "display_name_cart", "display_name_communication", "product_version_id", "product_family_id", "product_segment", "template_360", "bundle_hint", "has_internet", "int_bundle_type", "download_speed", "upload_speed", "has_dtv", "dtv_bundle_level", "dtv_bundle_type", "tv_nr_channels", "tv_box_type", "tv_box", "has_fixed_tel", "voice_product", "voice_plan", "bundle_identifier", "identifier_required_footprint", "is_ccr_benefit_eligible_part", "price_increase_maf", "price_increase_scope", "requires_servicability_item", "required_servicability"
        ]
    ],

    "Fixed_TV_Product" => [
        "General" => [
            "sku", "name", "description", "package_type", "package_subtype", "status", "product_visibility", "price"
        ],
        "Fixed Attributes" => [
            "vat", "maf", "sorting", "effective_date", "expiration_date", "initial_selectable", "hierarchy_value", "contract_value", "lifecycle_status", "display_name_inventory", "display_name_configurator", "display_name_cart", "display_name_communication", "product_version_id", "product_family_id", "product_segment", "template_360", "tv_product", "tv_content", "tv_nr_channels", "tv_box_type", "tv_box", "is_ccr_benefit_product", "identifier_required_footprint", "requires_servicability_item", "required_servicability"
        ]
    ],

    "Fixed_Internet_Product" => [
        "General" => [
            "sku", "name", "description", "package_type", "package_subtype", "status", "product_visibility", "price"
        ],
        "Fixed Attributes" => [
            "vat", "maf", "sorting", "effective_date", "expiration_date", "initial_selectable", "hierarchy_value", "contract_value", "lifecycle_status", "display_name_inventory", "display_name_configurator", "display_name_cart", "display_name_communication", "product_version_id", "product_family_id", "product_segment", "template_360", "download_speed", "upload_speed", "int_security_licenses", "int_security_licenses_tag", "is_fixed_ip_tier", "is_ccr_benefit_product", "identifier_required_footprint", "requires_servicability_item", "required_servicability"
        ]
    ],

    "Fixed_Tel_Product" => [
        "General" => [
            "sku", "name", "description", "package_type", "package_subtype", "status", "product_visibility", "price"
        ],
        "Fixed Attributes" => [
            "vat", "maf", "sorting", "effective_date", "expiration_date", "initial_selectable", "hierarchy_value", "contract_value", "lifecycle_status", "display_name_inventory", "display_name_configurator", "display_name_cart", "display_name_communication", "product_version_id", "product_family_id", "product_segment", "template_360", "voice_product", "voice_plan", "voice_plan_applies_over", "identifier_required_footprint", "requires_servicability_item", "required_servicability"
        ]
    ],

    "Fixed_Generic_Product" => [
        "General" => [
            "sku", "name", "description", "package_type", "package_subtype", "status", "product_visibility", "price"
        ],
        "Fixed Attributes" => [
            "vat", "maf", "sorting", "effective_date", "expiration_date", "initial_selectable", "hierarchy_value", "contract_value", "lifecycle_status", "display_name_inventory", "display_name_configurator", "display_name_cart", "display_name_communication", "product_version_id", "product_family_id", "product_segment", "type_other_addons", "template_360"
        ]
    ]
);

foreach ($attributeSets as $setCode => $attributes) {
    foreach ($attributes as $groupName => $attribute) {
        foreach ($attribute as $attributeMap) {
            $attributeSetId = $installer->getAttributeSet($entityTypeId, $setCode, "attribute_set_id");
            $installer->addAttributeToSet($entityTypeId, $attributeSetId, $groupName, $attributeMap);
        }
    }
}
$installer->endSetup();
