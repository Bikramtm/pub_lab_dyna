<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();
//clear category product mapping
$collection = Mage::getModel('catalog/category')
    ->getCollection();

$collection->getSelect()
    ->reset(Zend_Db_Select::COLUMNS)
    ->columns(array('entity_id'));

$categoryIds = '('.implode(',',array_column($collection->getdata(),'entity_id')) .')';

$tableName = 'catalog_category_product';
if (($installer->getConnection()->delete($tableName,
    [
        'category_id NOT IN '.$categoryIds
    ])
)
)
{
    Mage::log("category product Data removed");
}

//clear stock status and item table

$collection = Mage::getModel('catalog/product')
    ->getCollection();

$collection->getSelect()
    ->reset(Zend_Db_Select::COLUMNS)
    ->columns(array('entity_id'));

$productIds = '('.implode(',',array_column($collection->getdata(),'entity_id')) .')';

$tableName = 'cataloginventory_stock_item';
if (($installer->getConnection()->delete($tableName,
    [
        'product_id NOT IN '.$productIds
    ])
)
)
{
    Mage::log("ccataloginventory_stock_item Data removed");
}

$tableName = 'cataloginventory_stock_status';
if (($installer->getConnection()->delete($tableName,
    [
        'product_id NOT IN '.$productIds
    ])
)
)
{
    Mage::log("cataloginventory_stock_status Data removed");
}
$installer->endSetup();