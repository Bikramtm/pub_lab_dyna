<?php
/**
 * Remove attributes from attributes sets.
 */
$installer = $this;
$installer->startSetup();
$attributesToRemove = ['requires_serviceability', 'required_serviceability_items'];
$attrSets = ['accessoire', 'addon', 'addon_hybride', 'data_device', 'data_subscription', 'default', 'deviceRC', 'dummy_device', 'prepaid_simonly', 'promotion', 'service_item', 'simcard', 'voice_device', 'voice_subscription'];

foreach ($attrSets as $attrSet) {
    foreach ($attributesToRemove as $attributeToRemove) {
        $installer->deleteTableRow(
            'eav/entity_attribute',
            'attribute_id',
            $installer->getAttributeId('catalog_product', $attributeToRemove),
            'attribute_set_id',
            $installer->getAttributeSetId('catalog_product', $attrSet)
        );
    }
}

$installer->endSetup();