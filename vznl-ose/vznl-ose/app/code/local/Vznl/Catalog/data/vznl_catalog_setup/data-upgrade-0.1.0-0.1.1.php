<?php

/**
 * Compatibility rules for 0 MAF deviceRC products in order to handle the ACQ and RET process context
 */

/** @var Dyna_ProductMatchRule_Model_Rule $ruleModel */
$ruleModel = Mage::getModel('dyna_productmatchrule/rule');
/** @var Vznl_Catalog_Model_Product $productModel */
$productModel = Mage::getModel('catalog/product');
$acqOneTimeSKU = "4700100";
$retOneTimeSKU = "4700102";
/** @var Dyna_Catalog_Model_ProcessContext $processContextModel */
$processContextModel = Mage::getModel('dyna_catalog/processContext');
$processContextModel->getProcessContextIdByCode(Dyna_Catalog_Model_ProcessContext::ACQ);
$acqProcessContext = $processContextModel->getProcessContextIdByCode(Dyna_Catalog_Model_ProcessContext::ACQ);
$websites = Mage::app()->getWebsites();
$websitesIds = [];
foreach ($websites as $website) {
    $websitesIds[] = $website->getId();
}
$rules = [
    [
        "rule_data" => [
            "service_source_expression" => "true",
            "website_id" => NULL,
            "operation_type" => Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P,
            "operation" => Dyna_ProductMatchRule_Model_Rule::OP_NOTALLOWED,
            "priority" => 88,
            "left_id" => NULL,
            "right_id" => $productModel->getResource()->getIdBySku($retOneTimeSKU),//get id of product by sku,
            "rule_title" => "Disable " . $retOneTimeSKU . " on ACQ flow",
            "package_type" => 1,
            "source_type" => 1,
            "source_collection" => 0,
            "target_type" => 0,
            "override_initial_selectable" => 0
        ],
        "websitesIds" => $websitesIds,
        "processContextIds" => [
            $processContextModel->getProcessContextIdByCode(Dyna_Catalog_Model_ProcessContext::ACQ)
        ]
    ],
    [
        "rule_data" => [
            "service_source_expression" => "true",
            "website_id" => NULL,
            "operation_type" => Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P,
            "operation" => Dyna_ProductMatchRule_Model_Rule::OP_NOTALLOWED,
            "priority" => 88,
            "left_id" => NULL,
            "right_id" => $productModel->getResource()->getIdBySku($acqOneTimeSKU),//get id of product by sku,
            "rule_title" => "Disable " . $acqOneTimeSKU . " on Retention Flow",
            "package_type" => 1,
            "source_type" => 1,
            "source_collection" => 0,
            "target_type" => 0,
            "override_initial_selectable" => 0
        ],
        "websitesIds" => $websitesIds,
        "processContextIds" => [
            $processContextModel->getProcessContextIdByCode(Dyna_Catalog_Model_ProcessContext::REO),
            $processContextModel->getProcessContextIdByCode(Dyna_Catalog_Model_ProcessContext::RETBLU),
            $processContextModel->getProcessContextIdByCode(Dyna_Catalog_Model_ProcessContext::RETNOBLU),
            $processContextModel->getProcessContextIdByCode(Dyna_Catalog_Model_ProcessContext::RETTER)
        ]
    ]
];

foreach ($rules as $rule) {

    $ruleModel->setData($rule['rule_data'])->save();
    $ruleModel->setWebsiteIds($rule['websitesIds']);
    $ruleModel->setProcessContextsIds($rule['processContextIds']);
}
