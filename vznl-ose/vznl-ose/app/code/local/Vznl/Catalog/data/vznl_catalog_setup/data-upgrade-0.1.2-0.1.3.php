<?php
// Add include_all as product strategy for all products, as default
$products = Mage::getModel('catalog/product')->getCollection();
foreach ($products as $product) {
    $product = Mage::getModel('catalog/product')->load($product->getId());
    $product->setProductVisibilityStrategy(
        json_encode(array(
            'strategy' => Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL,
            'groups' => null,
        ))
    );
    Mage::getModel('productfilter/filter')->createRule($product->getId(), Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL, null);
}