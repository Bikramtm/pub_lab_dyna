<?php
$installer = $this;

$installer->startSetup();
$installer->getConnection();
$entityTypeId = Mage_Catalog_Model_Category::ENTITY;
$installer->removeAttribute($entityTypeId, 'is_mandatory');
$this->endSetup();