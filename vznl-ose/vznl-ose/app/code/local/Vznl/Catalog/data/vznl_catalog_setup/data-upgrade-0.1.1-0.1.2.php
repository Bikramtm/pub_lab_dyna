<?php
/** @var $installer Mage_Tax_Model_Resource_Setup */
$installer = $this;

/** Install tax classes */

/**
 * TAX_19
 */
$lastInsertedClassId = intval($installer->getConnection()->fetchOne('SELECT class_id FROM tax_class ORDER BY class_id DESC LIMIT 1'));
$newClassId = $lastInsertedClassId + 1;
$data = array(
    array(
        'class_id'     => $newClassId,
        'class_name'   => 'TAX_19',
        'class_type'   => Mage_Tax_Model_Class::TAX_CLASS_TYPE_PRODUCT
    )
);
foreach ($data as $row) {
    $installer->getConnection()->insertForce($installer->getTable('tax/tax_class'), $row);
}

/**
 * create rule for tax_19
 */
$lastInsertedRuleId = intval($installer->getConnection()->fetchOne('SELECT tax_calculation_rule_id FROM tax_calculation_rule ORDER BY tax_calculation_rule_id DESC LIMIT 1'));
$newRuleId = $lastInsertedRuleId + 1;
$data = array(
    'tax_calculation_rule_id'   => $newRuleId,
    'code'                      => 'Retail Customer-Taxable Goods-Rate 19',
    'priority'                  => 1,
    'position'                  => 1
);
$installer->getConnection()->insertForce($installer->getTable('tax/tax_calculation_rule'), $data);

/**
 * install tax calculation rates
 */
$lastInsertedRateId = intval($installer->getConnection()->fetchOne('SELECT tax_calculation_rate_id FROM tax_calculation_rate ORDER BY tax_calculation_rate_id DESC LIMIT 1'));
$newRateId = $lastInsertedRateId + 1;
$data = array(
    array(
        'tax_calculation_rate_id'   => $newRateId,
        'tax_country_id'            => 'DE',
        'tax_region_id'             => '*',
        'tax_postcode'              => '*',
        'code'                      => 'TAX_19',
        'rate'                      => '19.0000'
    )
);
foreach ($data as $row) {
    $installer->getConnection()->insertForce($installer->getTable('tax/tax_calculation_rate'), $row);
}

/**
 * install tax calculation
 */
$data = array(
    array(
        'tax_calculation_rate_id'   => $newRateId,//3,
        'tax_calculation_rule_id'   => $newRuleId,//2,
        'customer_tax_class_id'     => 3,
        'product_tax_class_id'      => $newClassId
    ),
    array(
        'tax_calculation_rate_id'   => $newRateId,//3,
        'tax_calculation_rule_id'   => $newRuleId,//2,
        'customer_tax_class_id'     => 3,
        'product_tax_class_id'      => 2
    )
);
$installer->getConnection()->insertMultiple($installer->getTable('tax/tax_calculation'), $data);

/**
 * TAX_6
 */
$lastInsertedClassId = intval($installer->getConnection()->fetchOne('SELECT class_id FROM tax_class ORDER BY class_id DESC LIMIT 1'));
$newClassId = $lastInsertedClassId + 1;
$data = array(
    array(
        'class_id'     => $newClassId,
        'class_name'   => 'TAX_6',
        'class_type'   => Mage_Tax_Model_Class::TAX_CLASS_TYPE_PRODUCT
    )
);
foreach ($data as $row) {
    $installer->getConnection()->insertForce($installer->getTable('tax/tax_class'), $row);
}

/**
 * create rule for tax_6
 */
$lastInsertedRuleId = intval($installer->getConnection()->fetchOne('SELECT tax_calculation_rule_id FROM tax_calculation_rule ORDER BY tax_calculation_rule_id DESC LIMIT 1'));
$newRuleId = $lastInsertedRuleId + 1;
$data = array(
    'tax_calculation_rule_id'   => $newRuleId,
    'code'                      => 'Retail Customer-Taxable Goods-Rate 6',
    'priority'                  => 1,
    'position'                  => 1
);
$installer->getConnection()->insertForce($installer->getTable('tax/tax_calculation_rule'), $data);

/**
 * install tax calculation rates
 */
$lastInsertedRateId = intval($installer->getConnection()->fetchOne('SELECT tax_calculation_rate_id FROM tax_calculation_rate ORDER BY tax_calculation_rate_id DESC LIMIT 1'));
$newRateId = $lastInsertedRateId + 1;
$data = array(
    array(
        'tax_calculation_rate_id'   => $newRateId,
        'tax_country_id'            => 'DE',
        'tax_region_id'             => '*',
        'tax_postcode'              => '*',
        'code'                      => 'TAX_6',
        'rate'                      => '6.0000'
    )
);
foreach ($data as $row) {
    $installer->getConnection()->insertForce($installer->getTable('tax/tax_calculation_rate'), $row);
}

/**
 * install tax calculation
 */
$data = array(
    array(
        'tax_calculation_rate_id'   => $newRateId,
        'tax_calculation_rule_id'   => $newRuleId,
        'customer_tax_class_id'     => 3,
        'product_tax_class_id'      => $newClassId
    ),
    array(
        'tax_calculation_rate_id'   => $newRateId,
        'tax_calculation_rule_id'   => $newRuleId,
        'customer_tax_class_id'     => 3,
        'product_tax_class_id'      => 2
    )
);
$installer->getConnection()->insertMultiple($installer->getTable('tax/tax_calculation'), $data);

/**
 * TAX_0
 */
$lastInsertedClassId = intval($installer->getConnection()->fetchOne('SELECT class_id FROM tax_class ORDER BY class_id DESC LIMIT 1'));
$newClassId = $lastInsertedClassId + 1;
$data = array(
    array(
        'class_id'     => $newClassId,
        'class_name'   => 'TAX_0',
        'class_type'   => Mage_Tax_Model_Class::TAX_CLASS_TYPE_PRODUCT
    )
);
foreach ($data as $row) {
    $installer->getConnection()->insertForce($installer->getTable('tax/tax_class'), $row);
}

/**
 * create rule for tax_0
 */
$lastInsertedRuleId = intval($installer->getConnection()->fetchOne('SELECT tax_calculation_rule_id FROM tax_calculation_rule ORDER BY tax_calculation_rule_id DESC LIMIT 1'));
$newRuleId = $lastInsertedRuleId + 1;
$data = array(
    'tax_calculation_rule_id'   => $newRuleId,
    'code'                      => 'Retail Customer-Taxable Goods-Rate 0',
    'priority'                  => 1,
    'position'                  => 1
);
$installer->getConnection()->insertForce($installer->getTable('tax/tax_calculation_rule'), $data);

/**
 * install tax calculation rates
 */
$lastInsertedRateId = intval($installer->getConnection()->fetchOne('SELECT tax_calculation_rate_id FROM tax_calculation_rate ORDER BY tax_calculation_rate_id DESC LIMIT 1'));
$newRateId = $lastInsertedRateId + 1;
$data = array(
    array(
        'tax_calculation_rate_id'   => $newRateId,
        'tax_country_id'            => 'DE',
        'tax_region_id'             => '*',
        'tax_postcode'              => '*',
        'code'                      => 'TAX_0',
        'rate'                      => '0.0000'
    )
);
foreach ($data as $row) {
    $installer->getConnection()->insertForce($installer->getTable('tax/tax_calculation_rate'), $row);
}

/**
 * install tax calculation
 */
$data = array(
    array(
        'tax_calculation_rate_id'   => $newRateId,
        'tax_calculation_rule_id'   => $newRuleId,
        'customer_tax_class_id'     => 3,
        'product_tax_class_id'      => $newClassId
    ),
    array(
        'tax_calculation_rate_id'   => $newRateId,
        'tax_calculation_rule_id'   => $newRuleId,
        'customer_tax_class_id'     => 3,
        'product_tax_class_id'      => 2
    )
);
$installer->getConnection()->insertMultiple($installer->getTable('tax/tax_calculation'), $data);