<?php

$installer = $this;

$installer->startSetup();

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
$attributesToUpdate = ['expiration_date', 'effective_date'];

foreach($attributesToUpdate as $attributeToUpdate){
    $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attributeToUpdate);
    if($attributeModel->getId()){
        $updateAttributeQuery = "UPDATE eav_attribute SET backend_model='eav/entity_attribute_backend_datetime', backend_type='datetime', frontend_input = 'datetime', source_model=NULL WHERE attribute_id=" . $attributeModel->getId();
        $conn->exec($updateAttributeQuery);
    }
}



$installer->endSetup();