<?php
$this->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

/** Get package_type attribute id */
$packageTypeAttribute = $connection->fetchRow("SELECT * FROM `eav_attribute` WHERE `attribute_code` = 'package_type'");

/** Get all options of package_type attribute */
$selectSql = "SELECT * FROM `eav_attribute_option` 
              INNER JOIN `eav_attribute_option_value` ON `eav_attribute_option_value`.`option_id` = `eav_attribute_option`.`option_id` 
              WHERE `eav_attribute_option`.`attribute_id` = " . $packageTypeAttribute['attribute_id'];
$packageTypesOptions = $connection->fetchAll($selectSql);

foreach ($packageTypesOptions as $packageTypesOption) {
    /** Remove TV, Internet & Phone option */
    if ($packageTypesOption['value'] == 'TV, Internet & Phone') {
        $connection->query("DELETE FROM `eav_attribute_option` WHERE `option_id` = " . $packageTypesOption['option_id']);
        $connection->query("DELETE FROM `eav_attribute_option_value` WHERE `value_id` = " . $packageTypesOption['value_id']);
    }
    /** Update int_tv_fixtel option to uppercase */
    if ($packageTypesOption['value'] == 'int_tv_fixtel') {
        $connection->query("UPDATE `eav_attribute_option_value` SET `value` = 'INT_TV_FIXTEL' WHERE `value_id` = " . $packageTypesOption['value_id']);
    }
}

$this->endSetup();