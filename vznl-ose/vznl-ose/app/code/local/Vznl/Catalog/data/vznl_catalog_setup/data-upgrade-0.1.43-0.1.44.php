<?php

$installer = $this;

$installer->startSetup();

$installer->updateAttribute(
        Mage_Catalog_Model_Product::ENTITY,
        'serial_number_type',
    array('backend_model' => 'eav/entity_attribute_backend_array')
);

$installer->endSetup();