<?php

$installer = $this;

$installer->startSetup();

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');

$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'regular_maf');
if($attributeModel->getId()){
    $removeRegularMafsQuery = 'delete from catalog_product_entity_decimal where attribute_id=' . $attributeModel->getId();
    $conn->exec($removeRegularMafsQuery);
}

$installer->endSetup();