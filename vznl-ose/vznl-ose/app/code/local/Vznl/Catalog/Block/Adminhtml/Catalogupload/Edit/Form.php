<?php

class Vznl_Catalog_Block_Adminhtml_Catalogupload_Edit_Form extends Dyna_Catalog_Block_Adminhtml_Catalogupload_Edit_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                "id" => "upload_form",
                "action" => $this->getUrl("*/*/upload"),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );

        $fieldset = $form->addFieldset('base_fieldset', array('legend' => $this->__('Please upload a valid zip archive')));
        //removed the read-only attribute since issue was seen in IE
        $fieldset->addField('file', 'file', array(
            'label' => $this->__('File'),
            'value'  => '',
            'class' => 'required-entry',
            'required' => true,
            'disabled' => false,
            'name' => 'file',
        ));

        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');

        $this->setForm($form);

        return Mage_Adminhtml_Block_Widget_Form::_prepareForm();
    }
}
