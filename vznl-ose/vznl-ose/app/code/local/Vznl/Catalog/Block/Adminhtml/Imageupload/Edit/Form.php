<?php

class Vznl_Catalog_Block_Adminhtml_Imageupload_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                "id" => "upload_form",
                "action" => $this->getUrl("*/*/upload"),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );

        $fieldset = $form->addFieldset('base_fieldset', array('legend' => $this->__('Please upload a valid zip archive')));
        $fieldset->addField('file', 'file', array(
            'label' => $this->__('File'),
            'value'  => '',
            'class' => 'required-entry',
            'required' => true,
            'disabled' => false,
            'readonly' => true,
            'name' => 'file',
        ));

        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');

        $this->setForm($form);

        return parent::_prepareForm();
    }
}
