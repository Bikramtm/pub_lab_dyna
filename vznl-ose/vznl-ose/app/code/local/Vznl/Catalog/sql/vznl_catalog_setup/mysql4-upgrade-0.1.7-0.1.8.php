<?php

$installer = $this;

$installer->startSetup();

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');

$conn->update('catalog_package_subtypes', array('package_code' => 'AddOn'), 'package_code = "Addon"');
$conn->update('eav_attribute_option_value', array('value' => 'AddOn'), 'value = "Addon"');

$installer->endSetup();