<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->removeAttribute('catalog_product', 'requires_serviceability');

$installer->startSetup();
// Empty installer
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$newAttributes = array(
    "requires_serviceability" => array(
        'group' => 'General',
        'label' => 'Requires Serviceability',
        'input' => 'boolean',
        'type' => 'int',
        'required' => 0,
        'source' => 'eav/entity_attribute_source_boolean',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'user_defined' => 1
    ),
    "required_serviceability_items" => array(
        'group' => 'General',
        'label' => 'Required Serviceability Items',
        'input' => 'text',
        'type' => 'text',
        'required' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => 'list values by "," operator',
        'user_defined' => 1
    )
);

foreach ($newAttributes as $newAttribute => $dataAttribute) {
    $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, $newAttribute, $dataAttribute);
    $attribute = $setup->getAttribute(Mage_Catalog_Model_Product::ENTITY, $newAttribute);
    $_attributeSetId = $setup->getAttributeSetId(Mage_Catalog_Model_Product::ENTITY, 'Default');//default
    $_attributeGroup = $setup->getAttributeGroup(Mage_Catalog_Model_Product::ENTITY, $_attributeSetId, 'General'); //General
    $setup->addAttributeToSet(
        Mage_Catalog_Model_Product::ENTITY,
        $_attributeSetId,
        $_attributeGroup['attribute_group_id'],
        $attribute['attribute_id']
    );
}

$installer->endSetup();
