<?php

class Vznl_AddItemToBasket_Helper_Data extends Vznl_Core_Helper_Data
{
    /*
    * @return String
    * */
    public function getAdapterChoice()
    {
        return trim(Mage::getStoreConfig('vodafone_service/additemtobasket/adapter_choice'));
    }

    /*
    * @return string peal endpoint url
    * */
    public function getEndPoint()
    {
        return trim(Mage::getStoreConfig('vodafone_service/additemtobasket/end_point'));
    }

    /*
    * @return string peal username
    * */
    public function getLogin()
    {
        return trim(Mage::getStoreConfig('vodafone_service/additemtobasket/login'));
    }

    /*
    * @return string peal password
    * */
    public function getPassword()
    {
        return trim(Mage::getStoreConfig('vodafone_service/additemtobasket/password'));
    }

    /*
    * @return string peal cty
    * */
    public function getCountry()
    {
        return trim(Mage::getStoreConfig('vodafone_service/additemtobasket/country'));
    }

    /*
    * @return string peal channel
    * */
    public function getChannel()
    {
        $agentHelper = Mage::helper('agent');
        $salesChannel = $agentHelper->getDealerSalesChannel();
        if ($salesChannel) {
            return trim($salesChannel);
        } else {
            return trim(Mage::getStoreConfig('vodafone_service/additemtobasket/channel'));
        }

    }

    /*
    * @return object omnius_service/client_stubClient
    * */
    public function getStubClient()
    {
        return Mage::getModel('omnius_service/client_stubClient');
    }

    /*
    * @return boolean peal verify
    * */
    public function getVerify()
    {
        return Mage::getStoreConfigFlag('vodafone_service/additemtobasket/verify');
    }

    /*
    * @return boolean peal proxy
    * */
    public function getProxy()
    {
        return Mage::getStoreConfigFlag('vodafone_service/additemtobasket/proxy');
    }

    /*
     * @return object Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /*
    * @param $basketId
    * @param $technicalId
    * @return array peal/stub response based on adpter choice
    * */
    public function addItemToBasket($basketId, $technicalId = false)
    {
        $factoryName = $this->getAdapterChoice();
        $validAdapters = Mage::getSingleton('addItemToBasket/system_config_source_addItem_adapter')->toArray();
        if (!in_array($factoryName, $validAdapters)) {
            return 'Either no adapter is chosen or the chosen adapter is no longer/not supported!';
        }
        $adapter = $factoryName::create();
        return $adapter->call($basketId, $technicalId);
    }

}
