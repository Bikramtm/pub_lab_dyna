<?php

class Vznl_StaticCacheInvalidate_Block_Page_Html_Head extends Mage_Page_Block_Html_Head {

    /**
     * Prepare static elements by adding version string to them
     * @param string $format - HTML element format for sprintf('<element src="%s"%s />', $src, $params)
     * @param array $staticItems - array of relative names of static items to be grabbed from js/ folder
     * @param array $skinItems - array of relative names of skin items to be found in skins according to design config
     * @param callback $mergeCallback
     * @return string
     * @return Vznl_StaticCacheInvalidate_Block_Page_Html_Head
     */
    protected function &_prepareStaticAndSkinElements(
        $format,
        array $staticItems,
        array $skinItems,
        $mergeCallback = null
    ):string {
        $version = Mage::helper('dyna_cache')->getAppVersion();

        // Strip out unwanted characters
        $version = str_replace(['Software version',' '], '', $version);

        $format = sprintf($format, "%s?v{$version}", "%s");
        return parent::_prepareStaticAndSkinElements($format, $staticItems, $skinItems, $mergeCallback);
    }

    /**
     * Remove element by Type
     * @param string $type
     * @return Vznl_StaticCacheInvalidate_Block_Page_Html_Head
     */
    public function removeByType(
        string $type
    ):Vznl_StaticCacheInvalidate_Block_Page_Html_Head {
        foreach ($this->_data as $key => $value) {
            if (strstr($key,$type.'/')) {
                unset($this->_data[$key]);
            }
        }
        return $this;
    }
}
