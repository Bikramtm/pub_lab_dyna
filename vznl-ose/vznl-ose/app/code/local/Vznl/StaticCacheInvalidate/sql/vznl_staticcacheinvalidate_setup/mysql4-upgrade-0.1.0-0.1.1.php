<?php

/** @var Mage_Sales_Model_Mysql4_Setup $this */
$installer = $this;
$installer->startSetup();

$setup = new Mage_Core_Model_Config();

$setup->saveConfig('dev/css/merge_css_files', '1');
$setup->saveConfig('dev/js/merge_files', '1');

$installer->endSetup();
