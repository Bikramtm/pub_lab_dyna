<?php

class Vznl_ReserveTelephoneNumber_Adapter_Stub_Factory
{
    public static function create()
    {
        return new Vznl_ReserveTelephoneNumber_Adapter_Stub_Adapter();
    }
}