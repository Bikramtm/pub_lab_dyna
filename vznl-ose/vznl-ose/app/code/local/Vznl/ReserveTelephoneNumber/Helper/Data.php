<?php

class Vznl_ReserveTelephoneNumber_Helper_Data extends Vznl_Core_Helper_Data
{
    /*
    * @return string
    * */
    public function getAdapterChoice()
    {
        return Mage::getStoreConfig('vodafone_service/reserveTelephoneNumber/adapter_choice');
    }

    /*
    * @return string peal endpoint url
    * */
    public function getEndPoint()
    {
        return trim(Mage::getStoreConfig('vodafone_service/reserveTelephoneNumber/end_point'));
    }

    /*
    * @return string peal username
    * */
    public function getLogin()
    {
        return trim(Mage::getStoreConfig('vodafone_service/reserveTelephoneNumber/login'));
    }

    /*
    * @return string peal password
    * */
    public function getPassword()
    {
        return trim(Mage::getStoreConfig('vodafone_service/reserveTelephoneNumber/password'));
    }

    /*
    * @return string peal cty
    * */
    public function getCountry()
    {
        return trim(Mage::getStoreConfig('vodafone_service/reserveTelephoneNumber/country'));
    }

    /*
    * @return string peal channel
    * */
    public function getChannel()
    {
        $agentHelper = Mage::helper('agent');
        $salesChannel = $agentHelper->getDealerSalesChannel();
        if ($salesChannel) {
            return trim($salesChannel);
        } else {
            return trim(Mage::getStoreConfig('vodafone_service/reserveTelephoneNumber/channel'));
        }
    }

    /*
    * @return string peal requesttype
    * */
    public function getRequestType()
    {
        return trim(Mage::getStoreConfig('vodafone_service/reserveTelephoneNumber/requesttype'));
    }

    /*
    * @return object omnius_service/client_stubClient
    * */
    public function getStubClient()
    {
        return Mage::getModel('omnius_service/client_stubClient');
    }

    /*
    * @return boolean peal verify
    * */
    public function getVerify()
    {
        return Mage::getStoreConfigFlag('vodafone_service/reserveTelephoneNumber/verify');
    }

    /*
    * @return boolean peal proxy
    * */
    public function getProxy()
    {
        return Mage::getStoreConfigFlag('vodafone_service/reserveTelephoneNumber/proxy');
    }

    /*
    * @return string foot print
    * */
    public function getFootPrint()
    {
        return trim(Mage::getStoreConfig('vodafone_service/reserveTelephoneNumber/footprintziggo'));
    }

    /*
     * @return object Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /**
     * @param $requestType
     * @param $houseFlatNumber
     * @param $postCode
     * @param $directoryNumber
     * @param $customerType
     * @param $houseFlatExt
     * @return array|string
     * */
    public function reserveTelephoneNumber($requestType, $houseFlatNumber, $postCode, $directoryNumber, $customerType, $houseFlatExt)
    {
        $factoryName = $this->getAdapterChoice();
        $validAdapters = Mage::getSingleton('reserveTelephoneNumber/system_config_source_adapterChoice_adapter')->toArray();
        if (!in_array($factoryName, $validAdapters)) {
            return 'Either no adapter is chosen or the chosen adapter is no longer/not supported!';
        }
        $adapter = $factoryName::create();
        $arguments = array(
            'requestType'   => $requestType,
            'houseFlatNumber' => $houseFlatNumber,
            'postCode' => $postCode,
            'directoryNumber' => $directoryNumber,
            'customerType' => $customerType,
            'houseFlatExt' => $houseFlatExt
        );
        return $adapter->call($arguments);
    }
}
