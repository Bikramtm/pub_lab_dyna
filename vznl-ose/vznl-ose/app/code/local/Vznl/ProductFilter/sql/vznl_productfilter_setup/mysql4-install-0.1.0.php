<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/* Add catalog_product_agent_visibility table */

// Defining table structure via object
if (!$installer->getConnection()->isTableExists('catalog_product_agent_visibility')) {
    $table = $this->getConnection()
        ->newTable($this->getTable('catalog_product_agent_visibility'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'auto_increment' => true,
                'nullable' => false
            ))
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ))
        ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ))
        ->addForeignKey(
            $this->getFkName('catalog_product_agent_visibility_index_product', 'product_id', 'catalog/product', 'entity_id'),
            'product_id', $this->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $this->getFkName('catalog_product_agent_visibility_index_agent_group', 'group_id', 'agent/dealergroup', 'group_id'),
            'group_id', $this->getTable('agent/dealergroup'), 'group_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        );
    // Executing table creation object
    $this->getConnection()->createTable($table);
}

/* Add product_visibility_strategy attribute */

$attrGroupName = 'General';
$attrCode = 'product_visibility_strategy';
$attrLabel = 'Product Visibility Group';

$objCatalogEavSetup = Mage::getResourceModel('catalog/eav_mysql4_setup', 'core_setup');
$attrIdTest = $objCatalogEavSetup->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $attrCode);

if ($attrIdTest === false) {
    $objCatalogEavSetup->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attrCode, array(
        'group' => $attrGroupName,
        'sort_order' => 1000,
        'type' => 'varchar',
        'backend' => '',
        'frontend' => '',
        'label' => $attrLabel,
        'input' => 'text',
        'class' => '',
        'source' => '',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'visible_on_front' => false,
        'unique' => false,
        'is_configurable' => false,
        'used_for_promo_rules' => true
    ));
}

$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');

$query = "ALTER TABLE catalog_product_agent_visibility DROP PRIMARY KEY, ADD PRIMARY KEY(entity_id);";
$writeConnection->query($query);
$query = "ALTER TABLE catalog_product_agent_visibility CHANGE group_id group_id INT(10) UNSIGNED NULL COMMENT 'Group_id';";
$writeConnection->query($query);


$query = "UPDATE catalog_eav_attribute SET is_filterable=1 WHERE attribute_id= (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'stock_status')";
$writeConnection->query($query);

$installer->endSetup();