<?php

/**
 * Class Vznl_ProductFilter_Model_Mysql4_Filter_Collection
 */
class Vznl_ProductFilter_Model_Mysql4_Filter_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('productfilter/filter');
    }

    public function delete()
    {
        foreach ($this->getItems() as $item) {
            $item->delete();
            unset($this->_items[$item->getId()]);
        }
        return $this;
    }
}
