<?php

/**
 * Class Vznl_ProductFilter_Model_SaveProductObserver
 */
class Vznl_ProductFilter_Model_SaveProductObserver extends Varien_Object
{
    /**
     * Flag to stop observer executing more than once
     *
     * @var static bool
     */
    static protected $_singletonFlag = false;

    /**
     * This method will run when the product is saved from the Magento Admin
     * Use this function to update the product model, process the
     * data or anything you like
     *
     * @param Varien_Event_Observer $observer
     */
    public function saveProductTabData(Varien_Event_Observer $observer)
    {
        if ( ! self::$_singletonFlag) {
            self::$_singletonFlag = true;

            /**
             * Let observer handle event only on admin
             */
            if (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml') {

                $product = $observer->getEvent()->getProduct();

                try {
                    $visibilityDomain = $this->_getRequest()->getPost('visibility_domain');
                    if (strtolower($visibilityDomain) === Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL) {
                        $groups = null;
                        $strategy = Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL;
                    } else {
                        $strategy = strtolower($visibilityDomain) == Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE ? Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE : Vznl_ProductFilter_Model_Filter::STRATEGY_EXCLUDE;
                        $groups = $this->_getRequest()->getPost('visible_to_groups');
                        $groups = $groups[$visibilityDomain];
                    }

                    /**
                     * Create the filter rules
                     */
                    Mage::getModel('productfilter/filter')->createRule($product->getId(), $strategy, $groups);

                    //load product before save to prevent duplicate images or other data
                    $product = Mage::getModel('catalog/product')->load($product->getId());
                    $product->setProductVisibilityStrategy(
                            json_encode(array(
                                'strategy' => $visibilityDomain,
                                'groups' => $groups,
                            ))
                        );
                    $product->getResource()->save($product);
                }
                catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
        }
    }

    /**
     * Retrieve the product model
     *
     * @return Mage_Catalog_Model_Product $product
     */
    public function getProduct()
    {
        return Mage::registry('product');
    }

    /**
     * Shortcut to getRequest
     *
     */
    protected function _getRequest()
    {
        return Mage::app()->getRequest();
    }
} 