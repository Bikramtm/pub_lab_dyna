<?php
class Vznl_InfluxLogs_Model_Observer extends Mage_Core_Model_Abstract {

    /**
     * Log format of influxDB measurements inside log files
     * @const string
     */
    const LOG_FORMAT = "[Measurements]\nName: %s\nTimestamp: %u\nExternalRequestTime: %.4f\nTotalExecutionTime: %.4f\nInternalProcessingTime: %.4f\nExceptionThrown: %u\nCounter: %u\nHost: %s\nURL: %s\nChannel: %s\nExternalId: %s\n[/Measurements]";

    /**
     * List of supported influxDB tags
     * @const array
     */
    const INFLUX_TAGS = [
        'Name',
        'Host',
        'URL',
        'Channel',
        'ExternalId'
    ];

    /**
     * List of supported influxDB fields
     * @const array
     */
    const INFLUX_FIELDS = [
        'ExternalRequestTime',
        'InternalProcessingTime',
        'TotalExecutionTime',
        'ExceptionThrown',
        'Counter'
    ];

    /**
     * Location of all logs that will checked for influxDB data
     * @var string
     */
    protected $influxLogsLocation = 'var/log/services';

    /**
     * InfluxDB logs locatin
     * @const string
     */
    const LOG_FILE = 'influxdb.log';

    /**
     * Initialize log rotation update for InfluxDB
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function initAllActiveLogsFetch():void {
        try {
            $logsList = $this->getAvailableLogsLocations();
            foreach ($logsList as $logPath) {
                $this->fetchLogContents($logPath);
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, Vznl_InfluxLogs_Model_Observer::LOG_FILE);
        }
    }

    /**
     * Get available log locations
     * @return array
     * @throws Exception
     */
    private function getAvailableLogsLocations():array {
        $paths = [];
        try {
            $logLocation = new Varien_Io_File();
            $logLocation->open(['path' => Mage::getBaseDir().DS.$this->influxLogsLocation]);
            foreach ($logLocation->ls() as $ref) {
                if (!$ref['leaf']) {
                    $paths[] = $ref['id'];
                }
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, Vznl_InfluxLogs_Model_Observer::LOG_FILE);
        }
        return $paths;
    }

    /**
     * Fetch local logs for InfluxDB
     * @param string $logPath
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function fetchLogContents(
        $logPath
    ):void {
        $logsDirectory = $logPath . DS;
        try {
            $directoryIterator = new DirectoryIterator($logsDirectory);
            $logId = md5($logsDirectory);
            $lastTimestampSent = $this->getLastTimestampSent($logId);
            foreach ($directoryIterator as $file) {
                if (in_array($file->getBasename(), ['.','..'])) continue;
                if ($lastTimestampSent >= $file->getMTime()) continue;
                $influxDetails = $this->getInfluxDetails($file->getPathname());
                $this->saveInfluxLogs($influxDetails, $file->getPathname());
                $this->saveLastTimestampSent($file->getMTime(), $logId);
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, Vznl_InfluxLogs_Model_Observer::LOG_FILE);
        }
    }

    /**
     * Load file and red influx logs
     * @param string $filePath
     * @return array
     */
    private function getInfluxDetails($filePath) {
        try {
            $ioAdapter = new Varien_Io_File();
            $ioAdapter->open(['path' => $ioAdapter->dirname($filePath)]);
            $contents = $ioAdapter->read($filePath);
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, Vznl_InfluxLogs_Model_Observer::LOG_FILE);
        }
        return $this->extractInfluxData($contents ?? '');
    }

    /**
     * Extract influx data from log file
     * @param string $contents
     * @return array
     */
    private function extractInfluxData(
        string $contents
    ):array
    {
        $data = [];
        preg_match('/\[Measurements\](.*?)\[\/Measurements\]/s', $contents, $matches);
        if (isset($matches[1]) && !empty($matches[1])) {
            $explodeFunction = function($item) use (&$data) {
                list($key, $val) = explode(": ", $item);
                $data[$key] = $val;
            };
            array_map($explodeFunction, explode("\n", trim($matches[1])));
        }
        return $data;
    }

    /**
     * Save data to InfluxDB
     * @param array $data
     * @param string $filePath
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function saveInfluxLogs(
        array $data,
        string $filePath
    ) {
        if (empty($data)) {
            Mage::log('InfluxDB Data not found in file: ' . $filePath, null, Vznl_InfluxLogs_Model_Observer::LOG_FILE);
            return false;
        }
        $adapter = Vznl_InfluxLogs_Adapter_InfluxDB_Factory::create();
        // Save data in InfluxDB
        return $adapter->call(
            $this->formatMeasurementName($data['Name']),
            $data['Timestamp'],
            $this->extractInfluxTags($data),
            $this->extractInfluxField($data)
        );
    }

    /**
     * Save data to InfluxDB
     * @param string $measurementFullName
     * @return string
     */
    private function formatMeasurementName(
        string $measurementFullName
    ):string {
        list ($measurement) = explode("_", $measurementFullName);
        return $measurement;
    }

    /**
     * Get last sent timestamp for logs ID
     * @param string $logId
     * @return int
     * @throws Exception
     */
    private function getLastTimestampSent(
        string $logId
    ):int {
        $lastTimestampValue = 0;
        $lastTimestampFile = $this->getLastTimestampFilePath($logId);
        $io = new Varien_Io_File();
        if ($io->fileExists($lastTimestampFile)) {
            $io->open(['path' => $io->dirname($lastTimestampFile)]);
            $io->streamOpen($lastTimestampFile, 'r');
            $lastTimestampValue = $io->streamRead();
            $io->streamClose();
        }
        return $lastTimestampValue;
    }

    /**
     * Save last sent timestamp for logs ID
     * @param int $lastModifiedTime
     * @param string $logId
     * @return void
     * @throws Exception
     */
    private function saveLastTimestampSent(
        int $lastModifiedTime,
        string $logId
    ):void {
        $lastTimestampFile = $this->getLastTimestampFilePath($logId);
        $io = new Varien_Io_File();
        $io->open(['path' => $io->dirname($lastTimestampFile)]);
        $io->streamOpen($lastTimestampFile, 'w');
        $io->streamWrite($lastModifiedTime);
        $io->streamClose();
    }

    /**
     * Get last sent timestamp (container) file path
     * @param string $logId
     * @return string
     */
    private function getLastTimestampFilePath(
        string $logId
    ):string {
        return DS.'tmp'.DS.'influx_logs_timestamp_'.$logId;
    }

    /**
     * Extract data that to be sent as tag for influxDB
     * @param array $data
     * @return array
     */
    private function extractInfluxTags(
        array $data
    ):array {
        $tags = [];
        foreach ($data as $key => $value) {
            if(in_array($key, self::INFLUX_TAGS)) {
                $tags[$key] = $value;
            }
        }
        return $tags;
    }

    /**
     * Extract data that to be sent as field for influxDB
     * @param array $data
     * @return array
     */
    private function extractInfluxField(
        array $data
    ):array {
        $fields = [];
        foreach ($data as $key => $value) {
            if(in_array($key, self::INFLUX_FIELDS)) {
                $fields[$key] = $value;
            }
        }
        return $fields;
    }
}
