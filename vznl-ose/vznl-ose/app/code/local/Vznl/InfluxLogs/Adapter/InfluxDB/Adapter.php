<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Vznl_InfluxLogs_Adapter_InfluxDB_Adapter
 */
class Vznl_InfluxLogs_Adapter_InfluxDB_Adapter
{

    /**
     * @var string
     */
    private $url;

    /**
     * @var Client
     */
    private $db;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * Adapter constructor.
     *
     * @param Client $client
     * @param string $url
     * @param string $db
     * @param string $username
     * @param string $password
     */
    public function __construct(
        Client $client,
        string $url,
        string $db,
        string $username = '',
        string $password = ''
    ) {
        $this->url = $url;
        $this->db = $db;
        $this->username = $username;
        $this->password = $password;
        $this->client = $client;
    }

    /**
     * @param string $measurement
     * @param int $timestamp
     * @param array $tags
     * @param array $data
     * @return bool
     * @throws GuzzleException
     */
    public function call(
        string $measurement,
        int $timestamp,
        array $tags,
        array $data
    ):bool {
        try {
            $body = $measurement . ',' . http_build_query($tags,'',',') . ' ' . http_build_query($data,'',',') . ' ' . $timestamp;
            $this->client->request(
                'POST',
                $this->url . '/write?db=' . $this->db,
                [
                    'body' => $body,
                    'auth' => [$this->username, $this->password]
                ]
            );
        } catch (ClientException|RequestException $exception) {
            Mage::log($exception->getMessage(), null, Vznl_InfluxLogs_Model_Observer::LOG_FILE);
        }
        return true;
    }
}
