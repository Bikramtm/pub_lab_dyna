<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Vznl_DeliveryWishDates_Adapter_Peal_Adapter
 */
class Vznl_DeliveryWishDates_Adapter_Peal_Adapter
{
    CONST name = "DeliveryWishDates";
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $cty;

    /**
     * @var string
     */
    private $executionTimeId;

    /**
     * @var string
     */
    private $requestTimeId;

    /**
     * Adapter constructor.
     *
     * @param Client $client
     * @param string $endpoint
     * @param string $channel
     * @param string $cty
     */
    public function __construct(Client $client, string $endpoint, string $channel, string $cty)
    {
        $this->endpoint = $endpoint;
        $this->channel = $channel;
        $this->cty = $cty;
        $this->client = $client;
    }

    /**
     * @param $basketId
     * @param $customerType
     * @param $deliveryMethod
     * @return string|array
     * @throws Exception
     * @throws GuzzleException
     */
    public function call($basketId, $customerType, $deliveryMethod, $isOverstappen, $newHardware)
    {
        $this->executionTimeId = $this->getHelper()->initTimeMeasurement();

        if (is_null($basketId)) {
            return 'No basketId provided to adapter';
        }

        if (is_null($customerType)) {
            return 'No customerType provided to adapter';
        }

        $userName = $this->getHelper()->getLogin();
        $password = $this->getHelper()->getPassword();
        $verify = $this->getHelper()->getVerify();
        $proxy = $this->getHelper()->getProxy();
        $appointmentDetailView['basketId'] = $basketId;
        $appointmentDetailView['customerType'] = $customerType;
        if ($isOverstappen) {
            $appointmentDetailView['isOverstappen'] = true;
        } elseif ($newHardware) {
            $appointmentDetailView['deliveryMethod']=$deliveryMethod;
        }
        $requestLog['url'] = $this->getUrl();
        $requestLog['body'] = $appointmentDetailView;
        try {
            $this->requestTimeId = $this->getHelper()->initTimeMeasurement();

            if ($userName != '' && $password != '') {
                $response = $this->client->request('POST', $this->getUrl(),
                    [
                        'auth' => [$userName, $password],
                        'body' => json_encode($appointmentDetailView),
                        'verify' => $verify,
                        'proxy' => $proxy,
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ]
                    ]
                );
            } else {
                $response = $this->client->request('POST', $this->getUrl(),
                    [
                        'body' => json_encode($appointmentDetailView),
                        'verify' => $verify,
                        'proxy' => $proxy,
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ]
                    ]
                );
            }

            $this->getHelper()->transferLog(
                json_encode($requestLog),
                json_encode(json_decode($response->getBody())),
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $source = json_decode($response->getBody(), true);
        } catch (ClientException|RequestException $exception) {
            $this->getHelper()->transferLog(
                $exception->getRequest(),
                ['msg' => $exception->getMessage(), 'code' => $exception->getCode()],
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $result['error'] = true;
            $result['message'] = $exception->getMessage();
            $result['code'] = $exception->getCode();
            return $result;
        }
        return $source;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->endpoint .
            "?cty=" . $this->cty .
            "&chl=" . $this->channel;
    }

    /**
     * @return object Vznl_DeliveryWishDates_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('vznl_deliverywishdates');
    }
}
