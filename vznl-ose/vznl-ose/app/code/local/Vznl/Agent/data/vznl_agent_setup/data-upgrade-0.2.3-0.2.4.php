<?php

$installer = $this;
$installer->startSetup();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$write->insert(
    "role_permission",
    array(
        "name" => Vznl_Agent_Model_Agent::SELL_FIXED_TO_BLACKLISTED,
        'description'=> "Agent is able to sell for fixed blocked customers"
    )
);

$installer->endSetup();