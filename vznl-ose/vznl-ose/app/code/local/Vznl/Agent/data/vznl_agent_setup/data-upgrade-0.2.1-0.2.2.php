<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();
$allowed_urls = Mage::getStoreConfig('agent/general_config/allowed_urls') . "\n" . 'configurator_init_getDeviceStock'."\n".'configurator_init_getAccessoriesStock';
$installer->setConfigData('agent/general_config/allowed_urls', $allowed_urls);
$installer->endSetup();
