<?php

$installer = $this;

$installer->startSetup();

$sql = <<<SQL
INSERT INTO role_permission
(name)
VALUES 
("SEARCH_ORDER_OF_AGENT")
SQL;

$installer->getConnection()->query($sql);

$installer->endSetup();