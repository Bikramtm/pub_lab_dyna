<?php

$installer = $this;
$installer->startSetup();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$write->insert(
    "role_permission",
    array(
        "name" => Vznl_Agent_Model_Agent::VIEW_SAVED_SHOPPING_CART,
        'description'=> "Agent is able to view a saved shopping cart from the 360 view"
    )
);
$write->insert(
    "role_permission",
    array(
    	"name" => Vznl_Agent_Model_Agent::VIEW_SAVED_OFFER,
        'description'=> "Agent is able to view a saved offer/quote from the 360 view"
    )
);
$write->insert(
    "role_permission",
    array(
    	"name" => Vznl_Agent_Model_Agent::DELETE_SAVED_SHOPPING_CART,
        'description'=> "Agent is able to delete a customer saved shopping cart"
    )
);
$write->insert(
    "role_permission",
    array(
    	"name" => Vznl_Agent_Model_Agent::DELETE_SAVED_OFFER,
        'description'=> "Agent can delete any saved offer/quote of a customer"
    )
);
$write->insert(
    "role_permission",
    array(
    	"name" => Vznl_Agent_Model_Agent::CHANGE_SAVED_SHOPPING_CART,
        'description'=> "Agent can open a saved shopping cart and make changes"
    )
);
$write->insert(
    "role_permission",
    array(
    	"name" => Vznl_Agent_Model_Agent::CHANGE_SAVED_OFFER,
        'description'=> "Agent can open a saved offer/quote and make changes"
    )
);

$installer->endSetup();