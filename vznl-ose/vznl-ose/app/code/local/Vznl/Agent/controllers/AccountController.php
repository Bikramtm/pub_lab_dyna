<?php

require_once Mage::getModuleDir('controllers', 'Dyna_AgentDE') . DS . 'AccountController.php';

class Vznl_Agent_AccountController extends Dyna_AgentDE_AccountController
{

    /**
     * Action predispatch
     *
     * Check agent authentication for some actions
     */
    public function preDispatch()
    {
        Mage_Core_Controller_Front_Action::preDispatch();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        $action = $this->getRequest()->getActionName();
        $openActions = array(
            'create',
            'login',
            'logout',
            'dealer',
            'dealerPost',
            'password',
            'forgotCredentials',
            'passwordReset',
            'temporaryPassword',
            'adtLogin'
        );
        $pattern = '/^(' . implode('|', $openActions) . ')/i';
        if ( ! preg_match($pattern, $action)) {
            if ( ! $this->_getSession()->getAgent()) {
                $this->setFlag('', 'no-dispatch', true);
            }
        } else {
            $this->_getSession()->setNoReferer(true);
        }
    }

    /**
     * Login as Another Agent Action
     */
    public function loginAsAgentAction()
    {
        $session = $this->_getSession();
        $superAgent = $session->getSuperAgent();
        $agent = $session->getAgent();
        if(!($superAgent || $agent->getIsSuperAgent())) {
            $session->addError($this->__('Super agent rights are required.'));
            return ;
        } elseif ($this->isAjax()) {
            $login = $this->getRequest()->getPost();

            try {
                $actualQuote = Mage::getSingleton('checkout/cart')->getQuote();
                $agent->authenticate($login['username'], null, true);
                $username = $this->_getSession()->getAgent()->getUsername();
                $dealerId = $this->_getSession()->getAgent()->getDealer() ? $this->_getSession()->getAgent()->getDealer()->getVfDealerCode() : '';
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode(array("error" => false, "username" => $username, "dealer" => $dealerId)));
                // clear cart and create new quote
                Mage::helper('dyna_checkout')->createNewQuote($actualQuote);
            } catch (Mage_Core_Exception $e) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $e->getMessage())));
                $this->getResponse()->setHttpResponseCode(200);
            } catch (Exception $e) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setHttpResponseCode(200);
                $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $this->__('Server Error'))));
                // Mage::getSingleton('core/logger')->logException($e); // PA DSS violation: this exception log can disclose customer password
            }
            $session->setData('store_code', Mage::app()->getWebsite()->getCode());
            return ;
        } elseif ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username'])) {
                try {
                    $actualQuote = Mage::getSingleton('checkout/cart')->getQuote();
                    $agent->authenticate($login['username'], null, true);
                    // clear cart and create new quote
                    Mage::helper('dyna_checkout')->createNewQuote($actualQuote);
                } catch (Mage_Core_Exception $e) {
                    $session->addError($e->getMessage());
                } catch (Exception $e) {
                    $session->addError($this->__('Server Error'));
                    // Mage::getSingleton('core/logger')->logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } else {
                $session->addError($this->__('Login is required.'));
            }
            $session->setData('store_code', Mage::app()->getWebsite()->getCode());
            return ;
        }
        $this->_loginPostRedirect();
    }

    /**
     * ADT login
     *
     * Login with ADT token
     */

    public function adtLoginAction()
    {
        $file = new Varien_Io_File();
        $token = $this->getRequest()->getParam('token');
        $session = $this->_getSession();
        if($token) {
            $key_path = Mage::getStoreConfig("vodafone_service/rsa_keys/rsa_key_path");
            if($key_path && $file->fileExists($key_path)) {
                try {
                    $file->open();
                    $key = $file->read($key_path);
                    $rsa = new \phpseclib\Crypt\RSA();
                    $rsa->loadKey($key);
                    $rsa->setEncryptionMode(2);
                    $token = base64_decode(str_replace(' ', '+', $token));
                    parse_str($rsa->decrypt($token),$data);

                    $startDateTime = substr($data["iatTimeStamp"], 0, 10);
                    $endDateTime = substr($data["expTimeStamp"], 0, 10);
                    $currentDateTime = substr(time(), 0, 10);

                    if (!($currentDateTime > $startDateTime && $currentDateTime < $endDateTime)) {
                        $session->addError($this->__('Token has expired!'));
                        $this->loginPostAction();
                        return;
                    }

                    $this->getRequest()->setPost('login',[
                        'username' => $data['username'],
                        'password' => $data['password']
                    ]);
                    $_SERVER['REQUEST_METHOD'] = 'POST';
                }
                catch(Exception $e) {
                    $session->addError($this->__('Unable to decrypt token!'));
                }
            } else {
                $session->addError($this->__('Invalid ADT key path!'));
            }
        } else {
            $session->addError($this->__('No Token received!'));
        }
        $this->loginPostAction();
    }

    /**
     * Keep alive action to make sure the agent session is retained.
     */
    public function keepAliveAction()
    {
        Mage::helper('agent')->updateLastActiveTime();
    }

        /**
     * Login post action
     */
    public function loginPostAction()
    {
        if ($this->_getSession()->getAgent()) {
            $this->_redirectUrl(Mage::getUrl());
            return;
        }
        $session = $this->_getSession();

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                $agent = Mage::getModel('agent/agent')->getCollection()->addFieldToFilter('username', $login['username'])->getFirstItem();
                try {
                    $agent->authenticate($login['username'], $login['password']);
                } catch (Mage_Core_Exception $e) {
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $agent->incrementLoginAttempts();
                            break;
                        case Dyna_Agent_Model_Agent::LOCKED_AGENT:
                            $this->_getSession()->setAgent(null);
                            $this->_getSession()->setSuperAgent(null);
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $message = $e->getMessage();
                            break;
                        case Dyna_Agent_Model_Agent::TEMPORARY_PASSWORD:
                            $this->_getSession()->setAgent(null);
                            $this->_getSession()->setSuperAgent(null);
                            if (Mage::getSingleton('customer/session')->getTemporaryAgent()) {
                                return $this->_redirect('*/*/temporaryPassword');
                            }
                            break;
                        default:
                            $message = $e->getMessage();
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $agent->incrementLoginAttempts();
                    }
                    $session->addError($message);
                    $session->setUsername($login['username']);
                    return $this->_redirect('*/*/login');
                } catch (Exception $e) {
                    // Mage::getSingleton('core/logger')->logException($e); // PA DSS violation: this exception log can disclose customer password
                }
                $session->setData('store_code', Mage::app()->getWebsite()->getCode());
            } else {
                $session->addError($this->__('Login and password are required.'));
            }
        }

        Mage::getSingleton('customer/session')->unsTemporaryAgent();
        $agent = $session->getAgent();
        if ($agent && ! $agent->getEverlastingPass()) {
            $passwordDeadline = new DateTime($agent->getPasswordChanged());
            $passwordDeadline->add(new DateInterval(sprintf('P%sD', abs((int) Mage::getStoreConfig(Dyna_Agent_Model_Agent::PASSWORD_EXPIRY_DURATION)))));
            if ($passwordDeadline->getTimestamp() <= Mage::getModel('core/date')->timestamp(time())) {
                Mage::getSingleton('customer/session')->setTemporaryAgent($agent);
                $this->_getSession()->setAgent(null);
                $this->_getSession()->setSuperAgent(null);
                return $this->_redirect('*/*/temporaryPassword');
            }
        }
        if ($session->getAgent()
            && ! empty($dealerForm['address'])
            && ($dealer = $this->validateAddress($dealerForm['address']))
            && $dealer->getStoreId() == Mage::app()->getStore()->getId()
        ) {
            $session->getAgent()->setDealer($dealer);
        }

        $this->_getSession()->renewSession();
        Mage::dispatchEvent("dyna_agent_login");

        $this->_loginPostRedirect();
    }

    /**
     * Send the password or username forgot email
     */
    public function forgotCredentialsAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('forgot');
            if (!empty($data['username'])) {
                try {
                    $agent = Mage::getModel('agent/agent')->load($data['username'], 'username');
                    if ($agent->getId()) {
                        $agent->forgotPassword();
                    }
                    // For security reasons there will be no error thrown in case the username isn't found.

                    $result['error'] = false;
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                } catch (Exception $e) {
                    $result['error'] = true;
                    $result['message'] = $e->getMessage();
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }
            } else {
                $result['error'] = true;
                $result['message'] = $this->__('Missing input data');
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        } else {
            return $this->_redirect('*/*/login');
        }
    }
}
