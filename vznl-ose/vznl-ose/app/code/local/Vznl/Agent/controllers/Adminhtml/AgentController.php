<?php
require_once Mage::getModuleDir('controllers', 'Dyna_Agent') . '/Adminhtml/AgentController.php';
/**
 * Class Vznl_Agent_Adminhtml_AgentController
 */
class Vznl_Agent_Adminhtml_AgentController extends Dyna_Agent_Adminhtml_AgentController
{
    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            try {
                $currentAgent = Mage::getModel("agent/agent")->load($this->getRequest()->getParam("id"));
                $toSaveData = $this->getDataToSave($currentAgent, $postData);
                foreach (Mage::app()->getStores() as $store) {
                    $this->updateAgentForStore($currentAgent, $store, $toSaveData, $postData);
                }

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Agent was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setAgentData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $currentAgent->getId()));
                    return;
                }
            } catch (Exception $e) {
                $agentData = $this->getRequest()->getPost();
                if ($e->getCode() == Dyna_Agent_Helper_Data::AGENT_FAILED_PASSWORD) {
                    $agentData['password'] = '';
                    $agentData['change_password'] = '';
                }
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setAgentData($agentData);
                if ($this->getRequest()->getParam("id")) {
                    $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                }else {
                    $this->_redirect("*/*/new");
                }
                return;
            }
        }
        $this->_redirect("*/*/");
    }

    /**
     * Gets the data to save.
     * @param Dyna_AgentDE_Model_Agent|null $currentAgent The agent to use data from or null.
     * @param $postData The post request data.
     * @return array The data to save.
     */
    protected function getDataToSave($currentAgent, $postData)
    {
        $toSaveData = array();
        if ($currentAgent->getAgentId()) {
            $toSaveData = $currentAgent->getData();
            unset($toSaveData['agent_id']);
        }

        foreach ($postData as $k=>$val) {
            $toSaveData[$k] = trim($val);
            if ($k == 'password' || ($k == 'change_password' && strlen($val) > 0)) {
                Mage::helper('agent')->validatePassword($currentAgent, $val);
                $toSaveData['password'] = Mage::getModel('customer/customer')->hashPassword($val);
                Mage::register('agent_password_changed', true);
            }
        }

        $resetAttempts = isset($toSaveData['reset_attempts']) && ($toSaveData['reset_attempts'] !== null);
        $agentLocked = isset($toSaveData['locked']) && ($currentAgent->getLocked() != $toSaveData['locked']) && !$toSaveData['locked'];
        if ($resetAttempts || $agentLocked) {
            $toSaveData['login_attempts'] = 0;
        }

        return $toSaveData;
    }

    /**
     * Updates all agents with the same username as the given agent for all storeviews
     * @param yna_AgentDE_Model_Agent|null $currentAgent The current agent.
     * @param $store the given store.
     * @param $toSaveData The data to save.
     * @param $postData The post request data.
     */
    protected function updateAgentForStore($currentAgent, $store, $toSaveData, $postData)
    {
        $agent = Mage::getModel('agent/agent')
            ->getCollection()
            ->addFieldToFilter('username', $currentAgent->getUsername())
            ->addFieldToFilter('store_id', $store->getStoreId())
            ->setPageSize(1, 1)
            ->getLastItem();

        if (isset($postData['store_id_'.$store->getStoreId()])) {
            $data = $toSaveData;
            $data['store_id'] = $store->getStoreId();
            $data['dealer_id'] = $data['dealer_code_' . $store->getStoreId()];
            $data['dealer_code'] = (isset($data['agent_code_' . $store->getStoreId()]) ? $data['agent_code_' . $store->getStoreId()] : null);
            $data['role_id'] = $data['role_' . $store->getStoreId()];
            if (isset($toSaveData['group_id'])) {
                $agent->setData('group_id', $toSaveData['group_id'])->save();
            }

            // If agent has no agent id yet, agent doesn't exist yet
            if(!$agent->getAgentId()){
                $agent->setCreatedAt((new DateTime())->format('Y-m-d H:i:s'));
            }

            $agent->addData($data)
                ->save();

            if ($currentAgent->getAgentId()) {
                $agent->setTemporaryPassword($currentAgent->getTemporaryPassword())
                    ->save();
            }
        } else {
            $agent->delete();
        }
    }
}
