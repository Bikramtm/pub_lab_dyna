<?php

$installer = $this;
$installer->startSetup();

// Add line_of_business (VF/Ziggo/Combined) in agent table
$this->getConnection()->addColumn($this->getTable("agent/dealer"), "line_of_business", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "comment" => "line_of_business",
    "default" => "Combined"
]);

$this->endSetup();