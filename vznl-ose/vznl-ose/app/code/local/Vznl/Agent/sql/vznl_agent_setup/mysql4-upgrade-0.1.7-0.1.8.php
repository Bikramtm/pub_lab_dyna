<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;

$installer->startSetup();

$dealerSql = <<<SQL
ALTER TABLE `dealer`
  ADD COLUMN `sales_code` VARCHAR(45) NULL AFTER `primary_brand`,
  ADD COLUMN `sales_channel` VARCHAR(45) NULL AFTER `sales_code`,
  ADD COLUMN `sales_location` VARCHAR(45) NULL AFTER `sales_channel`;
SQL;

$agentSql = <<<SQL
ALTER TABLE `agent`
  ADD COLUMN `sales_code` VARCHAR(45) NULL AFTER `other_red_sales_ids`;
SQL;

$installer->getConnection()->query($agentSql);
$installer->getConnection()->query($dealerSql);

$installer->endSetup();
