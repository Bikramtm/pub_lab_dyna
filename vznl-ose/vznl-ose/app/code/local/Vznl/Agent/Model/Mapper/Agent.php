<?php

/**
 * Class Dyna_Agent_Model_Mapper_Agent
 */
class Vznl_Agent_Model_Mapper_Agent extends Vznl_Agent_Model_Mapper_AbstractMapper
{
    /** @var Dyna_Agent_Model_Mysql4_Agent_Collection */
    protected $_collection;

    /** @var Dyna_Agent_Model_Mysql4_Agentrole_Collection */
    protected $_roleCollection;

    /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection */
    protected $_dealerCollection;

    /** @var Dyna_Agent_Model_Mysql4_Dealergroup_Collection */
    protected $_groupCollection;

    /**
     * @param array $data
     * @return Dyna_Agent_Model_Agent
     */
    public function map(array $data)
    {
        $existingAgent = null;
        if (isset($data['agent_id']) && $data['agent_id'] && isset($data['username']) && $data['username']) {
            $existingAgent = $this->_getCollection()->getItemByColumnValue('agent_id', $data['agent_id']);
            if ($existingAgent && ($existingAgent->getUsername() !== htmlspecialchars_decode($data['username']))) {
                $existingAgent = null;
            }
        }
        if (isset($data['agent_id'])) {
            unset($data['agent_id']);
        }
        if ($existingAgent != null) {
            $agent = $existingAgent->load($existingAgent->getId());
            $agent->setIsNew(false);
        } else {
            $agent = Mage::getModel('agent/agent');
            if (!isset($data['password']) || (isset($data['password']) && !strlen(trim($data['password'])))) {
                throw new RuntimeException(Mage::helper('agent')->__('Please make sure all new agents have a password defined in the the import file.'));
            }
            $agent->setIsNew(true);
        }

        foreach ($data as $property => $value) {
            try {
                $agent->setData($property, $this->mapProperty($property, $value, $agent));
            } catch (UnexpectedValueException $e) {
                $e->getMessage();
                //no-op as this ex will be thrown for old agents
                $e->getMessage();
            }
        }

        if (!$agent->getIsNew()) {
            $agent->unsetData('password');
        }

        return $agent;
    }

    /**
     * @param $password
     * @param $agent
     * @return string
     */
    protected function mapPassword($password, $agent)
    {
        if ($agent->getIsNew()) {
            Mage::helper('agent')->validatePassword($agent, $password);
            return Mage::getModel('customer/customer')->hashPassword($password);
        } else {
            throw new UnexpectedValueException('Password will not be updated for already existing agents');
        }
    }

    /**
     * @param $roleId
     * @return mixed
     */
    protected function mapRoleId($roleId)
    {
        if ($role = $this->_getRoleCollection()->getItemByColumnValue('role', $roleId)) {
            return $role->getRoleId();
        } elseif ($roleRoleId = $this->_getRoleCollection()->getItemByColumnValue('role_id', $roleId)) {
            return $roleRoleId->getRoleId();
        }
        return $roleId;
    }

    /**
     * @param $dealerId
     * @return mixed
     */
    protected function mapDealerId($dealerId)
    {
        if (($dealer = $this->_getDealerCollection()->getItemByColumnValue('vf_dealer_code', $dealerId))
            || (!$this->_getDealerCollection()->getItemByColumnValue('vf_dealer_code', $dealerId) && ($dealer = $this->_getDealerCollection()->getItemByColumnValue('dealer_id', $dealerId)))
        ) {
            return $dealer->getId();
        }

        return $dealerId;
    }

    /**
     * @param $dealerCode
     * @return mixed
     */
    protected function mapDealerCode($dealerCode)
    {
        if (($dealer = $this->_getDealerCollection()->getItemByColumnValue('vf_dealer_code', $dealerCode))
            || (!$this->_getDealerCollection()->getItemByColumnValue('vf_dealer_code', $dealerCode) && ($dealer = $this->_getDealerCollection()->getItemByColumnValue('dealer_id', $dealerCode)))
        ) {
            return $dealer->getId();
        }

        return $dealerCode;
    }

    /**
     * @param $groupId
     * @return mixed
     */
    protected function mapGroupId($groupId)
    {
        if (($group = $this->_getGroupCollection()->getItemByColumnValue('vf_dealer_code', $groupId))
            || (!$this->_getGroupCollection()->getItemByColumnValue('vf_dealer_code', $groupId) && ($group = $this->_getGroupCollection()->getItemByColumnValue('dealer_id', $groupId)))
        ) {
            return $group->getId();
        }

        return $groupId;
    }

    /**
     * @param $storeId
     * @return mixed
     */
    protected function mapStoreId($storeId)
    {
        $storeId = trim($storeId);
        if (is_numeric($storeId)) {
            return $storeId;
        }

        foreach (Mage::app()->getStores() as $store) { /** @var Mage_Core_Model_Store $store */
            if ($storeId === $store->getName() || $storeId === $store->getFrontendName()) {
                return $store->getId();
            }
        }
        return $storeId;
    }

    /**
     * @param $isActive
     * @return int|string
     */
    protected function mapIsActive($isActive)
    {
        if (is_numeric($isActive)) {
            $result = (int) $isActive;
        } else {
            $result = $isActive = strtolower(trim(__($isActive)));
            $yes = strtolower(__('Yes'));
            $no = strtolower(__('No'));
            if ($yes === $isActive || 'ja' === $isActive) {
                $result = 1;
            } elseif ($no === $isActive || 'nee' === $isActive) {
                $result = 0;
            }
        }

        return $result;
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Agent_Collection
     */
    protected function _getCollection()
    {
        if ( ! $this->_collection) {
            return $this->_collection = Mage::getResourceModel('agent/agent_collection');
        }
        return $this->_collection;
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Agentrole_Collection
     */
    protected function _getRoleCollection()
    {
        if ( ! $this->_roleCollection) {
            return $this->_roleCollection = Mage::getResourceModel('agent/agentrole_collection');
        }
        return $this->_roleCollection;
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Dealer_Collection
     */
    protected function _getDealerCollection()
    {
        if ( ! $this->_dealerCollection) {
            return $this->_dealerCollection = Mage::getResourceModel('agent/dealer_collection');
        }
        return $this->_dealerCollection;
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Dealergroup_Collection
     */
    protected function _getGroupCollection()
    {
        if ( ! $this->_groupCollection) {
            return $this->_groupCollection = Mage::getResourceModel('agent/dealergroup_collection');
        }
        return $this->_groupCollection;
    }
}
