<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_Agent_Model_Mapper_Dealer
 */
class Vznl_Agent_Model_Mapper_Dealer extends Dyna_Agent_Model_Mapper_Dealer
{
    /**
     * @param $groupIds
     * @return array
     */
    protected function mapGroupId($groupIds)
    {
        if (!is_array($groupIds)) {
            $groupIds = explode(';', $groupIds);
        }
        return array_map('intval', array_filter(array_map('trim', $groupIds)));
    }
}