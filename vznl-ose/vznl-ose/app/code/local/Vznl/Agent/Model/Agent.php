<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Agent_Model_Agent
 */
class Vznl_Agent_Model_Agent extends Dyna_AgentDE_Model_Agent
{
    const AGENT_SEARCH_PACKAGE = 'SEARCH';
    const LIST_VIEW_PERMISSIONS = ['GROUP', 'ALL', 'BRAND', 'AGENT', 'DEALER'];
    const ALLOW_ZIGGO = 'ALLOW_ZIGGO';
    const SELL_FIXED_TO_BLACKLISTED = 'SELL_FIXED_TO_BLACKLISTED';
    const VIEW_SAVED_SHOPPING_CART = 'VIEW_SAVED_SHOPPING_CART';
    const VIEW_SAVED_OFFER = 'VIEW_SAVED_OFFER';
    const DELETE_SAVED_SHOPPING_CART = 'DELETE_SAVED_SHOPPING_CART';
    const DELETE_SAVED_OFFER = 'DELETE_SAVED_OFFER';
    const CHANGE_SAVED_SHOPPING_CART = 'CHANGE_SAVED_SHOPPING_CART';
    const CHANGE_SAVED_OFFER = 'CHANGE_SAVED_OFFER';
    const ALLOW_MOBILE = 'ALLOW_MOB';
    /**
     * @var Dyna_Cache_Model_Cache
     */
    protected $cache = null;

    /**
     * Update password_changed field with current datetime when password is altered.
     * Conditions:
     * - Password was altered (i.e. it's not a new password)
     * - Agent does not have a temporary password (this would break the change temporary pw functionality)
     *
     * @return Mage_Core_Model_Abstract|void
     */
    public function _beforeSave()
    {
        $hasNewPassword = (bool) $this->getChangePassword();
        $temporaryPassword = (bool) $this->getTemporaryPassword();

        if (Mage::registry('agent_password_changed') && $hasNewPassword && !$temporaryPassword) {
            $now = new DateTime();

            $this->setPasswordChanged(
                $now->format('Y-m-d H:i:s')
            );
        }
        if($this->getIsActive() != '1') {
            $key = 'agent_' . md5(serialize([$this->getId(), null]));
            Mage::objects()->save($this, $key);
        }

        return parent::_beforeSave();
    }

    /**
     * Overridden to call event which triggers password change for this agent in other stores.
     *
     * @param string $password
     * @param bool $updateInAllStores
     * @return Vznl_Agent_Model_Agent
     */
    public function setPassword($password, $updateInAllStores = true)
    {
        Mage::unregister('agent_password_changed');

        $agent = parent::setPassword($password);

        // OMNVFNL-5369 Start
        if ($updateInAllStores) {
            Mage::helper('vznl_agent/password')->changePasswordInAllStores($agent);
        }
        // OMNVFNL-5369 End

        return $agent;
    }

    /**
     * Send a password reset email towards this agent
     * @param $body The password reset link, or when $username is <true> the `username` of the agent.
     * @param bool $username <true> means that the $body parameters will contain an username.
     */
    protected function sendCredentialsMail($body, $username = true)
    {
        /** @var Vznl_Agent_Helper_Data  $agent */
        $agent = Mage::helper('agent');
        $dealerId = $agent->getDaDealerCode();
        $dealerChannel = $agent->getWebsiteCode();
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();
        if (Mage::helper('core')->isModuleEnabled('Vznl_Communication')) {
            $nameLists[] = [
                'pwdresetlink' => $body,
                'Contact_FirstName' => $this->getFirstName(),
                'Contact_LastName' => $this->getLastName(),
                'Dealer' => array(
                    'DealerGroup' => $dealerId,
                    'DealerGroupChannel' => $dealerChannel,
                    'PrimaryBrand' => $primaryBrand,
                ),
            ];
            try {
                Mage::getSingleton('communication/pool')->add(
                    [
                        'email_code' => Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_PASS_AGENT_RESET,
                        'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),
                        'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),
                        'receiver' => $this->getEmail(),
                        'from' => $from,
                        'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
                        'parameters' => $nameLists,
                    ]
                );
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
            }
        } else {
            // TODO: Create recover password email template and assign it in Backend config
            $vars = ['username' => $username ? $body : $this->getName()];
            if (!$username) {
                $vars['password_reset_link'] = $body;
            }

            try {
                $templateId = Mage::getStoreConfig(self::CREDENTIALS_EMAIL_TEMPLATE);
                $receiverEmail = $this->getEmail();
                $emailTemplate = Mage::getModel('core/email_template')->load($templateId);

                $storeId = Mage::app()->getStore()->getStoreId();

                //If you'd like to view the processed template before sending the email, you print the following function:
                $emailTemplate->getProcessedTemplate($vars);
                //Otherwise, the next steps would be to set the email sender information. In this example we are pulling in our sending info from the Magento store email address configuration under the general contact tab:
                $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email', $storeId));
                $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name', $storeId));

                //And finally, we send out the email:
                $emailTemplate->send($receiverEmail, $vars['username'], $vars);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
    }

    /**
     * Loop through the permissions and check for which websites the agent is allowed to search
     *
     * @param array $excludedWebsiteCodes List of the codes that should be excluded
     * @param string $type
     *
     * @return mixed
     */
    public function getPermittedSearchWebsites($excludedWebsiteCodes = [], $type = 'ORDER')
    {
        $key = md5(__METHOD__ . $this->getAgentId() . join('|', $excludedWebsiteCodes) . '|' . $type);

        if (!($websiteIds = unserialize($this->getCache()->load($key)))) {
            $websiteIds = [];

            // If agent can search all websites, just return true
            if ($this->isGranted(self::AGENT_SEARCH_PACKAGE . '_' . $type . '_OF_ALL')) {
                return true;
            } else {
                $flippedExcludedWebsiteCodes = array_flip($excludedWebsiteCodes);
                // Loop through the websites, and check for which one he does have permissions
                /** @var Mage_Core_Model_Website $website */
                foreach (Mage::app()->getWebsites() as $website) {
                    $websiteCode = $website->getCode();
                    if (!isset($flippedExcludedWebsiteCodes[$websiteCode]) && $this->isGranted(self::AGENT_SEARCH_PACKAGE . '_' . $type . '_OF_' . mb_strtoupper($websiteCode))) {
                        $websiteIds[] = $websiteCode;
                    }
                    if (!isset($flippedExcludedWebsiteCodes[$websiteCode]) && $this->isGranted('SEARCH_' . $type . '_OF_BRAND')) {
                        $websiteIds[] = $websiteCode;
                    }
                }
            }

            $websiteIds = array_unique($websiteIds);
            $this->getCache()->save(serialize($websiteIds), $key, [Dyna_Cache_Model_Cache::AGENT_TAG], $this->getCache()->getTtl());
        }

        return $websiteIds;
    }

    /**
     * Loop through the permissions and check for which websites the agent is allowed to search
     *
     * @param array $excludedWebsiteCodes List of the codes that should be excluded
     *
     * @return mixed
     */
    public function getPermittedValidationWebsites($excludedWebsiteCodes = [])
    {
        $key = md5(__METHOD__ . $this->getAgentId() . join('|', $excludedWebsiteCodes));

        if (!($websiteIds = unserialize($this->getCache()->load($key)))) {
            $websiteIds = [];

            // If agent can search all websites, just return true
            if ($this->isGranted('SEARCH_FAILED_VALIDATIONS_OF_ALL')) {
                return true;
            } else {
                /** @var Vznl_Agent_Helper_Data $agentHelper */
                $agentHelper = Mage::helper('agent');

                $flippedExcludedWebsiteCodes = array_flip($excludedWebsiteCodes);
                // Loop through the websites, and check for which one he does have permissions
                /** @var Mage_Core_Model_Website $website */
                foreach (Mage::app()->getWebsites() as $website) {
                    $websiteCode = $website->getCode();
                    if (!isset($flippedExcludedWebsiteCodes[$websiteCode]) && $agentHelper->checkIsTelesalesOrWebshop($websiteCode) && $this->isGranted('SEARCH_FAILED_VALIDATIONS_OF_' . mb_strtoupper($websiteCode))) {
                        $websiteIds[] = $website->getId();
                    }
                }

                if ($this->isGranted('SEARCH_FAILED_VALIDATIONS_OF_BRAND') && $agentHelper->checkIsTelesalesOrWebshop()) {
                    $websiteIds[] = Mage::app()->getWebsite()->getId();
                }
            }

            $websiteIds = array_unique($websiteIds);
            $this->getCache()->save(serialize($websiteIds), $key, [Dyna_Cache_Model_Cache::AGENT_TAG], $this->getCache()->getTtl());
        }

        return $websiteIds;
    }

    /**
     * @param $action string
     * @param Vznl_Superorder_Model_Superorder $superorder
     *
     * @return bool
     * @throws Exception
     */
    public function canDoActionOnSuperorderPackage($action, Vznl_Superorder_Model_Superorder $superorder)
    {
        if (!($action == self::AGENT_CANCEL_PACKAGE || $action == self::AGENT_CHANGE_PACKAGE)) {
            throw new Exception('Invalid action to perform on order package.');
        }

        $key = $action . '_' . $superorder->getId() . '_' . $this->getId();
        $result = Mage::registry($key);
        if ($result !== null) {
            return $result;
        } else {
            $result = $this->isGranted($action . '_PACKAGE_OF_ALL');
            $permissions = ['Group', 'Brand', 'Dealer', 'Agent', 'Channel'];
            foreach ($permissions as $permission) {
                $result = $result || $this->{"checkPermissionPackageBy" . $permission}($action, $superorder);
            }
            Mage::register($key, $result);

            return $result;
        }
    }

    /**
     * @param $permission
     * @param Vznl_Superorder_Model_Superorder $superorder
     *
     * @return bool
     */
    public function checkPermissionPackageByBrand($permission, Vznl_Superorder_Model_Superorder $superorder)
    {
        if ($this->isGranted([$permission . '_PACKAGE_OF_BRAND'])) {
            $belcompanyWebsiteIds = [3, 7];
            if (
                (in_array($superorder->getCreatedWebsiteId(), $belcompanyWebsiteIds) && in_array($this->getWebsiteId(), $belcompanyWebsiteIds)) ||
                !in_array($superorder->getCreatedWebsiteId(), $belcompanyWebsiteIds) && !in_array($this->getWebsiteId(), $belcompanyWebsiteIds)
            ) {
                return true;

            }
        }
        return false;
    }

    /**
     * @param $permission
     * @param Vznl_Superorder_Model_Superorder $superorder
     *
     * @return bool
     */
    public function checkPermissionPackageByGroup($permission, Vznl_Superorder_Model_Superorder $superorder)
    {
        if ($this->isGranted([$permission . '_PACKAGE_OF_GROUP'])) {
            $superorderGroups = $superorder->getAgent()->getDealer()->getData('group_id');
            if (array_intersect($superorderGroups, $this->getDealer()->getData('group_id'))) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $permission
     * @param Vznl_Superorder_Model_Superorder $superorder
     *
     * @return bool
     */
    public function checkPermissionPackageByDealer($permission, Vznl_Superorder_Model_Superorder $superorder)
    {
        if ($this->isGranted([$permission . '_PACKAGE_OF_DEALER']) && $superorder->getDealer()->getId() == $this->getDealer()->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @param $permission
     * @param Vznl_Superorder_Model_Superorder $superorder
     *
     * @return bool
     */
    public function checkPermissionPackageByAgent($permission, Vznl_Superorder_Model_Superorder $superorder)
    {
        if ($this->isGranted([$permission . '_PACKAGE_OF_AGENT']) && $superorder->getAgent()->getId() == $this->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @param $permission
     * @param Vznl_Superorder_Model_Superorder $superorder
     *
     * @return bool
     */
    public function checkPermissionPackageByChannel($permission, Vznl_Superorder_Model_Superorder $superorder)
    {
        return $this->isGranted([$permission . '_PACKAGE_OF_' . mb_strtoupper(Mage::app()->getWebsite($superorder->getCreatedWebsiteId())->getCode())]);
    }

    /**
     * @return mixed
     */
    public function getDealerCode()
    {
        if ($this->getId()) {
            if ($this->getData('dealer_code') !== null && Mage::helper('agent')->isTelesalesLine()) {
                return $this->getData('dealer_code');
            } else {
                return $this->getDealer()->getVfDealerCode();
            }
        } else {
            return $this->getData('dealer_code');
        }
    }

    /**
     * Check if the agent can manually activate an order
     *
     * @return bool
     */
    public function canManuallyActivate()
    {
        return $this->isGranted('CREATE_MANUAL_CONNECT_ORDER');
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->cache) {
            $this->cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->cache;
    }

    /**
     * Check if the agent can search failed orders in the telesales Validations tab
     *
     * @return bool
     */
    public function canSearchFailedOrders()
    {
        return $this->canViewListOfType('FAILED_VALIDATIONS');
    }

    /**
     * Check if the agent can search orders within the open orders tab
     *
     * @return bool
     */
    public function canViewWorklist()
    {
        return $this->canViewListOfType('WORKLIST');
    }

    /**
     * Check if the agent can search orders within the credit check order tab
     *
     * @return bool
     */
    public function canViewCClist()
    {
        return $this->canViewListOfType('CREDITCHECK');
    }

    /**
     * Check if the agent can search orders within the number porting order tab
     *
     * @return bool
     */
    public function canViewNPlist()
    {
        return $this->canViewListOfType('NUMBERPORTING');
    }

    /**
     * Check for permissions based on the provided type
     *
     * @param $type
     * @return bool
     */
    public function canViewListOfType($type)
    {
        $possiblePermissions = [];
        foreach (Mage::app()->getWebsites() as $website) {
            $possiblePermissions[] = 'SEARCH_' . $type . '_OF_' . mb_strtoupper($website->getCode());
        }
        foreach (static::LIST_VIEW_PERMISSIONS as $perm) {
            $possiblePermissions[] = 'SEARCH_' . $type . '_OF_' . $perm;
        }
        foreach ($possiblePermissions as $permission) {
            if ($this->isGranted($permission)) {
                return true;
            }
        }

        return false;
    }

    public function getIsSuperAgent()
    {
        $currentWebsiteCode = Mage::app()->getWebsite()->getCode();
        $session = Mage::getSingleton('customer/session');

        if ($session->getSuperAgent())
            return true;

        $role = $session->getAgent(true)->getRoleId();
        switch ($currentWebsiteCode) {
            case Vznl_Agent_Model_Website::WEBSITE_BC_WEBSHOP_CODE:
            case Vznl_Agent_Model_Website::WEBSITE_TRADERS_CODE:
            case Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE:
                $result = $role == Dyna_Agent_Model_Agentrole::SUPERAGENT_ROLE;
                break;
            case Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE :
            case Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE :
                $result = $role != Dyna_Agent_Model_Agentrole::BUSINESS_SPECIALIST;
                break;
            default:
                $result = false;
        }

        return $result;
    }
    /**
     * @param array $data
     * @return Vznl_Agent_Model_Agent
     */
    public function map(array $data)
    {
        $existingAgent = null;
        if (isset($data['agent_id']) && $data['agent_id'] && isset($data['username']) && $data['username']) {
            $existingAgent = $this->_getCollection()->getItemByColumnValue('agent_id', $data['agent_id']);
            if ($existingAgent && ($existingAgent->getUsername() !== htmlspecialchars_decode($data['username']))) {
                $existingAgent = null;
            }
        }
        if (isset($data['agent_id'])) {
            unset($data['agent_id']);
        }
        if ($existingAgent != null) {
            $agent = $existingAgent->load($existingAgent->getId());
            $agent->setIsNew(false);
        } else {
            $agent = Mage::getModel('agent/agent');
            if (!isset($data['password']) || (isset($data['password']) && !strlen(trim($data['password'])))) {
                throw new RuntimeException(Mage::helper('agent')->__('Please make sure all new agents have a password defined in the the import file.'));
            }
            $agent->setIsNew(true);
        }

        foreach ($data as $property => $value) {
            try {
                $agent->setData($property, $this->mapProperty($property, $value, $agent));
            }
            catch (UnexpectedValueException $e) {
                //no-op as this ex will be thrown for old agents
                $e->getMessage();
            }
        }

        if (!$agent->getIsNew()) {
            $agent->unsetData('password');
        }

        return $agent;
    }

    /**
     * Get the JWT scopes of Dealer
     * @return array
     */
    public function getJwtDealerScopes()
    {
        $scope = array(
            'VF' => array('customer:search:bsl','customer:search:quick'),
            'Ziggo' => array('customer:search:peal','customer:search:quick'),
            'Combined' => array('customer:search:bsl','customer:search:peal','customer:search:quick')
        );
        $scopes = [];
        if ($this->isGranted('SEARCH_CUSTOMER')) {
            $scopes = $scope[$this->getDealer()->getLineOfBusiness()];
        }
        return $scopes;
    }

    /**
     * To check is the order created by same dealer
     * @param $orderNumber
     * @return boolean
     */
    public function isCurrentDealerOrder($orderNumber)
    {
        $order = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
        if ($order->getId()) {
            if ($this->getDealer()->getId() == $order->getDealerId())
                return true;
            else
                return false;
        }
        return true;
    }
}
