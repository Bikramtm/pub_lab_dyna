<?php

class Vznl_Agent_Helper_Password extends Mage_Core_Helper_Abstract
{
    /**
     * Change passwords in all stores (VZNL logic)
     * 
     * @param Vznl_Agent_Model_Agent $agent
     * @return void
     */
    public function changePasswordInAllStores(Vznl_Agent_Model_Agent $agent)
    {
        $newPassword = $agent->getPassword();

        $agents = Mage::getModel('agent/agent')
            ->getCollection()
            ->addFieldToFilter('username', $agent->getUsername())
            ->addFieldToFilter('agent_id', ['neq' => $agent->getId()]);

        // OMNVFNL-5369 Change password in all stores
        foreach ($agents as $currentAgent) {
            $isTemporary = $agent->getTemporaryPassword() !== $currentAgent->getTemporaryPassword();

            $currentAgent
                ->setData('password', $newPassword) // Do not call setPassword as it will hash the password again
                ->setPasswordChanged($agent->getPasswordChanged())
                ->setTokensGenerated(null)
                ->save();

            if ($isTemporary) {
                $currentAgent->setTemporaryPassword(0);
                Mage::getSingleton('customer/session')->unsTemporaryAgent();
            }

            $currentAgent
                ->setPasswordResetToken(null)
                ->setPasswordResetTokenExpiryDate(null)
                ->save();
        }
    }
}
