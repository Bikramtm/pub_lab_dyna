<?php
class Vznl_RetainTelephoneNumbers_Helper_Data extends Vznl_Core_Helper_Data
{
    /*
    * @return String
    * */
    public function getAdapterChoice()
    {
        return Mage::getStoreConfig('vodafone_service/retaintelephonenumber/adapter_choice');
    }

    /*
    * @return string peal endpoint url
    * */
    public function getEndPoint()
    {
        return trim(Mage::getStoreConfig('vodafone_service/retaintelephonenumber/end_point'));
    }

    /*
    * @return string peal username
    * */
    public function getLogin()
    {
        return trim(Mage::getStoreConfig('vodafone_service/retaintelephonenumber/login'));
    }

    /*
    * @return string peal password
    * */
    public function getPassword()
    {
        return trim(Mage::getStoreConfig('vodafone_service/retaintelephonenumber/password'));
    }

    /*
    * @return string peal cty
    * */
    public function getCountry()
    {
        return trim(Mage::getStoreConfig('vodafone_service/retaintelephonenumber/country'));
    }

    /*
    * @return string peal channel
    * */
    public function getChannel()
    {
        $agentHelper = Mage::helper('agent');
        $salesChannel = $agentHelper->getDealerData()->getData('sales_channel');
        if ($salesChannel) {
            return trim($salesChannel);
        } else {
            return trim(Mage::getStoreConfig('vodafone_service/retaintelephonenumber/channel'));
        }
    }

    /*
    * @return object omnius_service/client_stubClient
    * */
    public function getStubClient()
    {
        return Mage::getModel('omnius_service/client_stubClient');
    }

    /*
    * @return boolean peal verify
    * */
    public function getVerify()
    {
        return Mage::getStoreConfigFlag('vodafone_service/retaintelephonenumber/verify');
    }

    /*
    * @return boolean peal proxy
    * */
    public function getProxy()
    {
        return Mage::getStoreConfigFlag('vodafone_service/retaintelephonenumber/proxy');
    }

    /*
     * @return object Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /*
    * @param $validateAddress
    * @param $arguments
    * @return array peal/stub response
    * */
    public function retainTelephoneNumbers($validateAddress)
    {
        $factoryName = $this->getAdapterChoice();
        $validAdapters = Mage::getSingleton('retainTelephoneNumbers/system_config_source_adapterChoice_adapter')->toArray();
        if (!in_array($factoryName, $validAdapters)) {
            return 'Either no adapter is chosen or the chosen adapter is no longer/not supported!';
        }
        $adapter = $factoryName::create();
        return $adapter->call($validateAddress);
    }
}
