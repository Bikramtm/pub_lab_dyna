<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_Cache_CacheController
 */
require_once Mage::getModuleDir('controllers', 'Dyna_Cache') . '/CacheController.php';

class Vznl_Cache_CacheController extends Dyna_Cache_CacheController
{
    /**
     * Flush all caches (Also varnish)
     */
    public function flushAllCachesAction()
    {
        Mage::dispatchEvent('adminhtml_cache_flush_all_caches');
        try {
            // Flush varnish
            Mage::helper('dyna_cache')->clearVarnish();

            // Flush system
            Mage::app()->cleanCache();
            Mage::app()->getCacheInstance()->getFrontend()->clean();

            // Flush js/css
            Mage::getModel('core/design_package')->cleanMergedJsCss();

            $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__("All caches have been flushed."));
        } catch(Exception $exception) {
            $this->_getSession()->addWarning(Mage::helper('adminhtml')->__("Not all caches were flushed."));
        }

        $this->_redirect('*/*');
    }
}
