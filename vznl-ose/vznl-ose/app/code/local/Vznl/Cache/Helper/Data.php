<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_Cache_Helper_Data
 */
class Vznl_Cache_Helper_Data extends Dyna_Cache_Helper_Data
{
    /**
     * @return bool|string
     */
    public function getAppVersion()
    {
        $version = '';
        if (realpath($softwareFile = Mage::getConfig()->getOptions()->getEtcDir() . DS . parent::SOFTWARE_FILE)) {
            $version .= trim(include $softwareFile);
        }

        if (realpath($versionFile = Mage::getBaseDir('media') . DS . parent::VERSION_FILE)) {
            $version .= ' ' . trim(include $versionFile);
        } else {
            $version .= ' seed';
        }

        $version = trim($version);

        if (!empty($version)) {
            return $version;
        }

        return false;
    }

    /**
     * Execute the clear varnish command
     *
     * @return bool
     * @throws Exception
     */
    public function clearVarnish()
    {
        $ipList = $this->getVarnishIps();

        if ($ipList) {
            $cmd = join(
                    '; ',
                    array_map(function ($server) {return sprintf('(curl --noproxy "*" -X BAN %s > /dev/null 2>&1 &)', escapeshellarg($server));}, $ipList)
                ) . ';';

            if (strlen($output = exec($cmd))) {
                throw new Exception(sprintf('Could not clear Varnish cache. Output: %s', $output));
            }

        }

        return true;
    }

    /**
     * Get varnish IP addresses from VANISH_IP_PATH config path
     *
     * @return array
     */
    protected function getVarnishIps()
    {
        return array_filter(array_map('trim', explode(PHP_EOL, Mage::getStoreConfig(Dyna_Cache_Model_Cache::VANISH_IP_PATH))), function ($serverIp) {
            // Strip port from IP address before validation
            if (strpos($serverIp, ":") !== false) {
                list ($serverIp) = explode( ":", $serverIp );
            }
            return filter_var($serverIp, FILTER_VALIDATE_IP);
        });
    }
}
