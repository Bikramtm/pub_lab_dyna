<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_Cache_Block_Cache
 */
class Vznl_Cache_Block_Cache extends Dyna_Cache_Block_Cache
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();

        $message = Mage::helper('core')->__('Flushing all caches will strain the system. Are you sure that you want flush it?');
        $this->_addButton('flush_all', array(
            'label' => Mage::helper('core')->__('Flush all Caches'),
            'onclick' => 'confirmSetLocation(\'' . $message . '\', \'' . $this->getFlushAllCachesUrl() . '\')',
            'class' => 'delete-all',
        ));
    }

    /**
     * Get url for cleaning all caches
     */
    public function getFlushAllCachesUrl()
    {
        return $this->getUrl('*/*/flushAllCaches');
    }

    public function getHeaderWidth(){
        return '20%';
    }
}
