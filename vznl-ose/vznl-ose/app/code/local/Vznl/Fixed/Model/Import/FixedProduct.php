<?php

/**
 * Class Vznl_Fixed_Model_Import_FixedProduct
 */
class Vznl_Fixed_Model_Import_FixedProduct extends Dyna_Fixed_Model_Import_FixedProduct
{
    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper('vznl_import');
    }

    /**
     * Import cable product
     *
     * @param array $data
     * @return void
     */
    public function importProduct($data)
    {
        $skip = false;
        $this->_totalFileRows++;

        $data = $this->_setDataMapping($data);

        if (!$this->_helper->hasValidPackageDefined($data)) {
            $this->_skippedFileRows++;
            return;
        }

        if (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (!array_key_exists('name', $data) || empty($data['name'])) {
            $this->_logError('Skipping line without name');
            $skip = true;
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        $data['stack'] = $this->stack;
        $data['sku'] = trim($data['sku']);

        $this->_currentProductSku = $data['sku'];
        $this->_log('Start importing ' . $data['name'], true);
        $data = $this->checkPrice($data);

        try {
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product');
            $id = $product->getIdBySku($data['sku']);
            $new = ($id == 0);
            if (!$new) {
                $this->_log('Loading existing product"' . $data['sku'] . '" with ID ' . $id, true);
                $product->load($id);
            }

            // check vat value and tax
            $vatValue = $data['vat'];
            $vatClass = Mage::helper('vznl_catalog')->getVatTaxClass($vatValue);

            $skip = false;
            if (!$this->_setAttributeSetByCode($product, $data)) {
                $this->_logError('Unknown attribute set "' . $data['attribute_set'] . '" for product ' . $data['sku']);
                $skip = true;
            }
            if (!$this->_setWebsitesByCsvString($product, $data)) {
                $this->_logError('Unknown website "' . $data['_product_websites'] . '" for product ' . $data['sku']);
                $skip = true;
            }
            /*Reset product Id to original one*/
            $productInfoModel = Mage::getModel('productinfo/info')->loadBySku(trim($data['sku']));
            if($productInfoModel->getData()[0]) {
                $product->setEntityId($productInfoModel->getData()[0]['entity_id']);
            }

            if ($new) {

                try {
                    if ($productInfoModel->getData()[0]['entity_id']) {
                        $this->clearStockData('cataloginventory/stock_item', $productInfoModel->getData()[0]['entity_id']);
                        $this->clearStockData('cataloginventory/stock_status', $productInfoModel->getData()[0]['entity_id']);
                    }
                    $this->_createStockItem($product);

                } catch (Exception $exception) {
                    $this->_logError('[ERROR] Skipping stock import data with sku ' . $data['sku']);
                    $this->_logError($exception->getMessage());
                    $this->_skippedFileRows++;
                    return;
                }
            }

            if (!$this->setTaxClassIdByName($product, $vatClass)) {
                $this->_logError((empty($vatClass) ? 'Empty vat value' : 'Unknown vat value "' . $vatClass . '"') . ' for product ' . $data['sku']);
                $skip = true;
            }

            if ($skip) {
                // mark record as failed in logs
                $this->_skippedFileRows++;
                return;
            }

            $this->_setDefaults($product, $data);
            $this->_setStatus($product, $data);

            $this->_helper->setProductVisibilityStrategy($product, $data);
            if (isset($data['group_ids'])) {
                unset($data['group_ids']);
            }
            if (isset($data['strategy'])) {
                unset($data['strategy']);
            }

            $this->_setProductData($product, $data);

            // set product categories
            if (isset($data['category']) && is_array($data['category']) && count($data['category'])) {
                $product->setCategoryIds($data['category']);
            }

            // Adding product versions to product
            if (!empty($data['product_version'])) {
                $versionIds = array();
                foreach (array_filter(explode(",", $data['product_version'])) as $versionCode) {
                    if ($productVersionId = $this->productVersionModel::getProductVersionIdByCode($versionCode)) {
                        $versionIds[] = $productVersionId;
                    } else {
                        $this->_logError('Unknown version id "' . $data['product_version'] . '" for product ' . $data['sku']);
                    }
                }
                $product->setData("product_version_id", implode(",", $versionIds));
            }
            // Adding product family to product
            if (!empty($data['product_family'])) {
                $familyIds = array();
                foreach (array_filter(explode(",", $data['product_family'])) as $familyCode) {
                    if ($familyId = $this->productFamilyModel::getProductFamilyIdByCode($familyCode)) {
                        $familyIds[] = $familyId;
                    } else {
                        $this->_logError('Unknown family id "' . $data['product_family'] . '" for product ' . $data['sku']);
                    }
                }
                $product->setData("product_family", implode(",", $familyIds));
            }

            if (!$this->getDebug()) {
                $product->save();
                /*Save the new product in info table for latter use*/
                if (!$productInfoModel->getData()[0]) {
                    $productInfo = Mage::getModel('productinfo/info');
                    $productInfo->setData('entity_id', $product->getId());
                    $productInfo->setData('sku', trim($data['sku']));
                    $productInfo->save();
                }
            }

            if (!$this->getDebug()) {
                $productVisibilityInfo = @json_decode($product->getProductVisibilityStrategy(), true);
                if ($productVisibilityInfo) {
                    $groups = $productVisibilityInfo['strategy'] ? explode(',', $productVisibilityInfo['groups']) : null;
                    Mage::getModel('productfilter/filter')->createRule($product->getId(), $productVisibilityInfo['strategy'], $groups);
                }
            }

            unset($product);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['name'], true);
            $this->_currentProductSku = null;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
            $this->_currentProductSku = null;
            $this->_skippedFileRows++;
        }
    }

    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        // This will format itself trough observer
        $product->setUrlKey($data['name']);
        $product->setProductVisibilityStrategy(
            json_encode(array(
                'strategy' => Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL,
                'groups' => null,
            ))
        );
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setIsDeleted('0');
    }

    /**
     * Process custom data mapping for fixed products
     *
     * @param array $data
     * @return array
     */
    protected function _setDataMapping($data)
    {
        // Map websites if set or mark telesales as default
        if (!empty($data['channel']) && $data['channel'] == '*') {
            $data['_product_websites'] = Mage::helper('vznl_import')->getAllWebsitesCodes();
        } else {
            $data['_product_websites'] = !empty($data['channel']) ? strtolower(str_replace(' ','',$data['channel'])) : 'telesales';
        }

        $header_mappings = array_flip($this->_helper->getMapping('product_template'));

        // new name => old name
        $renamedFields = [
        ];

        $renamedFields = array_merge($renamedFields, $header_mappings);

        foreach ($renamedFields as $new => $old) {
            if (empty($data[$new]) && !empty($data[$old])) {
                $data[$new] = $data[$old];
                unset($data[$old]);
            }
        }

        Mage::helper("dyna_import/data")->preventExtraColumns($data);

        if (!empty($data['maf'])) {
            $data['maf'] = $this->getFormattedPrice($data['maf']);
        }

        if (empty($data['attribute_set'])) {
            $data['attribute_set'] = $this->_getAttributeSet($data);
        }

        if (!empty($data['price'])) {
            $data['price'] = $this->getFormattedPrice($data['price']);
        }

        if (!empty($data['prijs_maf_new_amount'])) {
            $data['prijs_maf_new_amount'] = $this->getFormattedPrice($data['prijs_maf_new_amount']);
        }

        if (!empty($data['promo_new_price'])) {
            $data['promo_new_price'] = $this->getFormattedPrice($data['promo_new_price']);
        }

        if (!empty($data['maf_discount'])) {
            $data['maf_discount'] = $this->getFormattedPrice($data['maf_discount']);
        }

        if (!empty($data['price_discount'])) {
            $data['price_discount'] = $this->getFormattedPrice($data['price_discount']);
        }

        if (isset($data['product_visibility'])) {
            $data['product_visibility'] = $this->parseProductVisibility($data['product_visibility']);
        }

        if (isset($data['serial_number_type'])) {
            $data['serial_number_type'] = $this->parseSerialNumberType($data['serial_number_type']);
        }

        /** Text retrieved for additional_text attribute needs to be parsed as BB code */
        $bbParser = new Dyna_Mobile_Model_Import_BBParser();
        if (!empty($data['additional_text'])) {
            $bbParser->setText($data['additional_text']);
            $data['additional_text'] = $bbParser->parseText();
        }
        
        /** Get value for checkout product attribute */
        $data['checkout_product'] = $this->getCheckoutProductValue($data);

        return $data;
    }

    /**
     * @param $price
     * @return string
     */
    protected function getFormattedPrice($price)
    {
        $price = str_replace(',', '.', $price);

        return number_format($price, 4, '.', null);
    }

    public function getFileLogName($productFileName)
    {
        return strtolower($productFileName) . '_import.' . $this->_logFileExtension;
    }

    /**
     * Determine checkout product attribute value
     *
     * @param array $data
     * @return null|string
     */
    protected function getCheckoutProductValue($data)
    {
        if(isset($data['checkout_product']) && $data['checkout_product']) {
            return ('yes' == strtolower($data['checkout_product'])) ? 'Yes' : 'No';
        }
        return 'No';
    }

    /**
     * Set product status
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setStatus($product, $data)
    {
        if (array_key_exists('status', $data) && $data['status']) {
            switch (strtolower($data['status'])) {
                case 'enabled':
                case 'ingeschakeld':
                    $product->setStatus(1);
                    break;
                case 'disabled':
                case 'uitgeschakeld':
                default:
                    $product->setStatus(2);
                    break;
            }
        } else {
            $product->setStatus(2);
        }
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }

        if ($this->_currentProductSku) {
            $msg = '[' . $this->_currentProductSku . '] ' . $msg;
        }

        $this->_helper->logMsg($msg, false);
    }

    /**
     * Log function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        if ($this->_currentProductSku) {
            $msg = '[' . $this->_currentProductSku . '] ' . $msg;
        }

        $this->_helper->logMsg($msg);
    }

    /**
     * Method to parse the value of the product Serial Number Types
     * @param $values
     * @return string
     * @internal param $value
     */
    protected function parseSerialNumberType($values)
    {
        $markedOptions = [];
        $markedOptionIds = [];
        $options = [];

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, Vznl_Catalog_Model_Product::SERIAL_NUMBER_TYPE_ATTR);
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

            $values = explode(',', $values);
            foreach($values as $value){
                $markedOptions[] = strtolower(str_replace(' ', '', $value));
            }
            foreach ($options as $option) {
                if(in_array(strtolower(str_replace(' ', '', $option['label'])), $markedOptions)){
                    $markedOptionIds[] = $option['label'];
                }
            }

        return implode(',', $markedOptionIds);
    }

    public function clearStockData($model, $productId)
    {
        $coreConnection = Mage::getSingleton('core/resource');
        $connection = $coreConnection->getConnection('core_write');
        $resTableName = Mage::getResourceSingleton($model);
        $tableName = $resTableName->getMainTable();
        if (($connection->delete($tableName,
            [
                'product_id = ?' => $productId,
            ])
        )
        )
        {
            return "Data removed";
        }
    }
}
