#!/usr/bin/env bash

##
## USAGE: __PROG__
##
## This __PROG__ script can be used to package the code as rpm or deb
##
## __PROG__ has a dependency on FPM. See README.md for more information.
##
## __PROG__ <buildnumber> <resourcetype>
##
## resourcetype:
## - rpm
## - deb
## - zpk
##
## Examples:
##   __PROG__ 1 rpm
##   __PROG__ 12 deb
##   __PROG__ 14 zpk

#------------------------------------------------------------------------------
# Init variables
#------------------------------------------------------------------------------
SCRIPTPATH=''
PROJECTROOT=''
me=`basename "$0"`
APP_NAME=''
APP_VERSION=''
PACKAGE_NAME=''
BUILD_NO=$1
PACKAGE_TYPE=$2
VENDOR="Dynacommerce"

#------------------------------------------------------------------------------
# Methods
#------------------------------------------------------------------------------
function usage () {
    grep '^##' "$0" | sed -e 's/^##//' -e "s/__PROG__/$me/" 1>&2
}
function setScriptAndProjectPath() {
    case "$(uname -s)" in
        Linux*)
            SCRIPTPATH=$(dirname "$(readlink -f "$0")")
            PROJECTROOT=$SCRIPTPATH/..
        ;;
        Darwin*)
            SCRIPTPATH=$(dirname "$(greadlink -f "$0")")
            PROJECTROOT=$SCRIPTPATH/..
        ;;
        *)
            echo "Could not determine script path"
            exit 1
        ;;
    esac
}

#------------------------------------------------------------------------------
# Init variables
#------------------------------------------------------------------------------
APP_NAME=''
APP_VERSION=''
PACKAGE_NAME=''
BUILD_NO=$1
PACKAGE_TYPE=$2
ARCHITECTURE="all"
VENDOR="Dynacommerce"
source $(dirname $0)/package_zpk.sh

#------------------------------------------------------------------------------
# Methods
#------------------------------------------------------------------------------
function initPackageName() {
    echo "Setting up package naming"
    # Name
    APP_NAME=$(composer config name)
    if [ -z "$APP_NAME" ]; then
        echo "- No app name found in the 'composer.json'! Exiting..."
        exit 1
    else
        echo "- Init package with name: $APP_NAME"
    fi

    # Version
    APP_VERSION=$(composer config version)
    if [ -z "$APP_VERSION" ]; then
        echo "- No version found in the 'composer.json'! Exiting..."
        exit 1
    else
        echo "- Init package with version: $APP_VERSION"
    fi

    # Set iteration number
    echo "- Init package for build: "$BUILD_NO""
    if [ $BUILD_NO != "SNAPSHOT" ]; then
        BUILD_NO=1
    fi

    echo "- Init package for build: "$BUILD_NO""
    PACKAGE_NAME=${APP_NAME}-${APP_VERSION}
}

function packageDeb() {
    echo "Generating package for type: $PACKAGE_TYPE"
    local DEPENDENCIES=( \
        libxml2-utils \
        zip \
        unzip \
        php7.1-cli \
        php7.1-fpm \
        php7.1-xml \
        php7.1-gd \
        php7.1-curl \
        php7.1-intl \
        php7.1-mysql \
        php7.1-mbstring \
        php7.1-soap \
        php7.1-zip \
        php7.1-bcmath \
        php7.1-mcrypt
    )
    local fpm_ignore=`for p in ${IGNORE_PATTERNS[@]} ; do echo "-x $p" ; done`
    local dependencies=`for p in ${DEPENDENCIES[@]} ; do echo "-d $p" ; done`
    mkdir -p $PROJECTROOT/build/$PACKAGE_TYPE
    /usr/local/bin/fpm -s dir \
        -t $PACKAGE_TYPE \
        -n $APP_NAME \
        -p $PROJECTROOT/build/$PACKAGE_TYPE \
        -v $APP_VERSION \
        -a $ARCHITECTURE \
        --iteration $BUILD_NO \
        --vendor $VENDOR \
        --before-install $PROJECTROOT/package/deb/pre_install.sh \
        --after-install $PROJECTROOT/package/deb/post_install.sh \
        --before-upgrade $PROJECTROOT/package/deb/pre_upgrade.sh \
        --after-upgrade $PROJECTROOT/package/deb/post_upgrade.sh \
        --before-remove $PROJECTROOT/package/deb/pre_uninstall.sh \
        --after-remove $PROJECTROOT/package/deb/post_uninstall.sh \
        $fpm_ignore \
        $dependencies \
        -f $PROJECTROOT/build/package/=/
}

function packageRpm() {
    echo "Generating package for type: $PACKAGE_TYPE"
    local DEPENDENCIES=( \
        zip \
        unzip \
        php-cli \
        php-fpm \
        php-xml \
        php-gd \
        php-curl \
        php-intl \
        php-mysqlnd \
        php-mbstring \
        php-soap \
        php-zip \
        php-bcmath \
        php-mcrypt \
    )
    local fpm_ignore=`for p in ${IGNORE_PATTERNS[@]} ; do echo "-x $p" ; done`
    local dependencies=`for p in ${DEPENDENCIES[@]} ; do echo "-d $p" ; done`
    mkdir -p $PROJECTROOT/build/$PACKAGE_TYPE
    /usr/local/bin/fpm -s dir \
        -t $PACKAGE_TYPE \
        -n $APP_NAME \
        -p $PROJECTROOT/build/$PACKAGE_TYPE \
        -v $APP_VERSION \
        -a $ARCHITECTURE \
        --iteration $BUILD_NO \
        --vendor $VENDOR \
        --before-install $PROJECTROOT/package/rpm/pre_install.sh \
        --after-install $PROJECTROOT/package/rpm/post_install.sh \
        --before-upgrade $PROJECTROOT/package/rpm/pre_upgrade.sh \
        --after-upgrade $PROJECTROOT/package/rpm/post_upgrade.sh \
        --before-remove $PROJECTROOT/package/rpm/pre_uninstall.sh \
        --after-remove $PROJECTROOT/package/rpm/post_uninstall.sh \
        $fpm_ignore \
        $dependencies \
        -f $PROJECTROOT/build/package/=/
}

function package() {
    case $PACKAGE_TYPE in
      deb)
        packageDeb
      ;;
      rpm)
        packageRpm
      ;;
      zpk)
        packageZpk $PROJECTROOT $PACKAGE_NAME $BUILD_NO $APP_VERSION $PACKAGE_TYPE
      ;;
    esac
}

function validateParameters() {
    # Check if FPM is installed
    if [ ! -f "/usr/local/bin/fpm" ]; then
        echo "Package FPM is required. Please see section FPM in the README.md file."
        exit 1
    fi

    # Check if build number is set
    if [ -z "$BUILD_NO" ]; then
        echo "No build number given! Exiting..."
        exit 1
    fi

    if [ "$PACKAGE_TYPE" == "zpk" ] && [ ! -f "/usr/local/bin/zs-client" ]; then
        echo 'Zend-server SDK not found in "./usr/bin/zs-client". See https://github.com/zend-patterns/ZendServerSDK for more information' >&2
        exit 1
    fi

    # Check for valid package types
    validparams=(deb rpm zpk)
    if [[ ! ${validparams[*]} =~ "$PACKAGE_TYPE" ]]; then
        echo "Invalid package type given! Exiting..."
        exit 1
    fi
}

#------------------------------------------------------------------------------
# Execution
#------------------------------------------------------------------------------
# Set script as working directory
setScriptAndProjectPath
initPackageName
validateParameters
package
