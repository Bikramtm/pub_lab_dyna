#!/usr/bin/env bash

# Verify PHP Version
PHPVERSION=$(php --version | head -n 1 | cut -d " " -f 2 | cut -c 1,3)
if [[ $PHPVERSION != "71" ]]; then
    echo "- Error: Invalid PHP Version installed"
    exit 1
fi

# Check for redis-cli
if ! [ -x "$(command -v redis-cli)" ]; then
    echo '- Error: redis-cli is not installed.' >&2
    exit 1
fi

# check for mysql-client
if ! [ -x "$(command -v mysql)" ]; then
    echo '- Error: mysql-client is not installed.' >&2
    exit 1
fi

# Create application user
echo "- Creating application user "
if [ `id -u "omnius-ose" 2>/dev/null || echo -1` -ge 0 ]; then
    # Skipped, already exists
    echo "- User already exists"
else
    sudo useradd -r -s /bin/false -d /opt/omnius/ose omnius-ose
fi
