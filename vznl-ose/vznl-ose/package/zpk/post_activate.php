<?php
/* The script post_activate.php allow users to "clean up" after a new version of the application
 * is activated, for example by removing a "Down for Maintenance" message
 * set in Pre-activate.
 * The following environment variables are accessable to the script:
 *
 * - ZS_RUN_ONCE_NODE - a Boolean flag stating whether the current node is
 *   flagged to handle "Run Once" actions. In a cluster, this flag will only be set when
 *   the script is executed on once cluster member, which will allow users to write
 *   code that is only executed once per cluster for all different hook scripts. One example
 *   for such code is setting up the database schema or modifying it. In a
 *   single-server setup, this flag will always be set.
 * - ZS_WEBSERVER_TYPE - will contain a code representing the web server type
 *   ("IIS" or "APACHE")
 * - ZS_WEBSERVER_VERSION - will contain the web server version
 * - ZS_WEBSERVER_UID - will contain the web server user id
 * - ZS_WEBSERVER_GID - will contain the web server user group id
 * - ZS_PHP_VERSION - will contain the PHP version Zend Server uses
 * - ZS_APPLICATION_BASE_DIR - will contain the directory to which the deployed
 *   application is staged.
 * - ZS_CURRENT_APP_VERSION - will contain the version number of the application
 *   being installed, as it is specified in the package descriptor file
 * - ZS_PREVIOUS_APP_VERSION - will contain the previous version of the application
 *   being updated, if any. If this is a new installation, this variable will be
 *   empty. This is useful to detect update scenarios and handle upgrades / downgrades
 *   in hook scripts
 */

require_once 'helpers.php';
zendLog("Starting post_activate");

try
{
    $share = getenv('ZS_SHARE');
    $appLocation = getenv('ZS_APPLICATION_BASE_DIR');

    $shareDirs = [
        '/shell',
    ];

    $structureDirs = [
        '/shell',
        '/app/code/community/Omnius/Proxy',
        '/app/code/community/Omnius/Proxy/blocks',
        '/app/code/community/Omnius/Proxy/controllers',
        '/app/code/community/Dyna/Proxy',
        '/app/code/community/Dyna/Proxy/blocks',
        '/app/code/community/Dyna/Proxy/controllers',
    ];

    $errorMessage = "";

    // Ensure log directory exists
    shell_exec('mkdir -p '.getenv('ZS_APPLICATION_BASE_DIR').'/var/log');

    // Create default folder structure if not exist
    foreach ($structureDirs as $dir)
    {
        $directory = getenv('ZS_APPLICATION_BASE_DIR') . "$dir/";
        if (!file_exists($directory))
        {
            zendLog("Creating structured folder '$directory'");
            if (!mkdir($directory, 0777, true)) {
                $errorMessage .= "Failed to create structured folder '$directory', please create this folder manually with 0777 permissions\n";
            }
            zendLog("Structured folder '$directory' created");
        }
        else
        {
            zendLog("Structured folder for '$directory' already exists, setting permissions to 0777");

            $output = array();
            $return_var = -1;
            exec("chmod 777 -R $directory", $output, $return_var);

            if ($return_var != 0)
            {
                $errorMessage .= "Failed to set permissions for existing structured folder '$directory', please set 0777 permissions for this folder manually\n";
            }
        }
    }

    // Permissions to composer references
    shell_exec('chmod 755 '.getenv('ZS_APPLICATION_BASE_DIR').'/vendor/bin/*');

    // Flush caches
    // Redis
    shell_exec('redis-cli -n '.getenv('ZS_REDIS_DB').' -h '.getenv('ZS_REDIS_HOST').' -p '.getenv('ZS_REDIS_PORT').' flushdb');
    shell_exec('redis-cli -n '.getenv('ZS_BACKEND_DB').' -h '.getenv('ZS_BACKEND_HOST').' -p '.getenv('ZS_BACKEND_PORT').' flushdb');
    // Varnish
    if(getenv('ZS_BACKEND_VARNISH_HOSTS') != ''){
        $hosts = explode(',', getenv('ZS_BACKEND_VARNISH_HOSTS'));
        foreach($hosts as $host){
            shell_exec('curl --noproxy "*" -X BAN '.$host);
        }
    }

    if (strlen($errorMessage) > 0) {
        throw new OSEException($errorMessage);
    }

    if (strlen($share) > 0)
    {
        if (!file_exists($share)) {
            zendLog("Creating share folder '$share'");
            if (!mkdir($share, 0777, true)) {
                $message = "Failed to create shared folder '$share', please create this folder manually with 0777 permissions";
                throw new OSEException($message);
            }
            zendLog("Shared folder '$share' created");
        } else {
            zendLog("Setting share folder to: '$share'");
        }

        //for first deploy
        foreach ($shareDirs as $dir) {
            if (is_dir("$share$dir")) {
                continue;
            }

            zendLog("Creating directory $share$dir");
            exec("mkdir -p $share$dir");

            zendLog("Moving contents from '" . getenv('ZS_APPLICATION_BASE_DIR') . $dir . "/*" . "' to directory '$share$dir'");
            exec("mv " . getenv('ZS_APPLICATION_BASE_DIR') . "$dir/* $share$dir/");
        }
        zendLog("Shared folders have been set");

        //remove current folder and create symlink to that folder
        foreach ($shareDirs as $dir)
        {
            exec("rm -rf " . getenv('ZS_APPLICATION_BASE_DIR') . "$dir/");
            exec("ln -s $share$dir " . getenv('ZS_APPLICATION_BASE_DIR') . "$dir");

            zendLog("ln -s $share$dir " . getenv('ZS_APPLICATION_BASE_DIR') . "$dir");
        }
    }
    else
    {
        zendLog("Shared folder is not configured for " . implode("|",$shareDirs));
    }

    #setup permissions
    if (debugMode())
    {
        zendLog("Setting up permissions for all directories (as we are running in debug mode)");
        setDirectoryPermission($appLocation);
    }
    else
    {
        zendLog("Setting up permissions for structured directories");
        foreach($structureDirs as $dir)
        {
            setDirectoryPermission($appLocation . $dir);
        }
    }

}catch (Exception $e) {
    throw new OSEException("Exception occurred during post_activate: " .$e->getMessage());
}

zendLog("Stopping post_activate");
