<?php
/* The script pre_activate.php should contain code that should make the changes in the server
 * environment so that the application is fully functional. For example, this may include
 * changing symbolic links to "data" directories from previous to current versions,
 * upgrading an existing DB schema, or setting up a "Down for Maintenance"
 * message on the live version of the application
 * The following environment variables are accessable to the script:
 *
 * - ZS_RUN_ONCE_NODE - a Boolean flag stating whether the current node is
 *   flagged to handle "Run Once" actions. In a cluster, this flag will only be set when
 *   the script is executed on once cluster member, which will allow users to write
 *   code that is only executed once per cluster for all different hook scripts. One example
 *   for such code is setting up the database schema or modifying it. In a
 *   single-server setup, this flag will always be set.
 * - ZS_WEBSERVER_TYPE - will contain a code representing the web server type
 *   ("IIS" or "APACHE")
 * - ZS_WEBSERVER_VERSION - will contain the web server version
 * - ZS_WEBSERVER_UID - will contain the web server user id
 * - ZS_WEBSERVER_GID - will contain the web server user group id
 * - ZS_PHP_VERSION - will contain the PHP version Zend Server uses
 * - ZS_APPLICATION_BASE_DIR - will contain the directory to which the deployed
 *   application is staged.
 * - ZS_CURRENT_APP_VERSION - will contain the version number of the application
 *   being installed, as it is specified in the package descriptor file
 * - ZS_PREVIOUS_APP_VERSION - will contain the previous version of the application
 *   being updated, if any. If this is a new installation, this variable will be
 *   empty. This is useful to detect update scenarios and handle upgrades / downgrades
 *   in hook scripts
 */
require_once 'helpers.php';
zendLog("Starting pre_activate");

try {
    ini_set('max_execution_time', 1200);

    if (debugMode())
    {
        zendLog("OSE Package is running in debug mode!", "WARNING");
    }

    changePhpIniValue('memory_limit', getenv('ZS_PHP_MEMORY_LIMIT'));
    changePhpIniValue('post_max_size', '20M');
    changePhpIniValue('upload_max_filesize', '20M');
    changePhpIniValue('max_file_uploads', '50');
    changePhpIniValue('max_input_vars', '2000');
    set_time_limit(1200);

    removeOldZendPackages();

    if (!file_exists(__DIR__ . '/mysql') && !file_exists(__DIR__ . '/mysqldump')) {
        zendLog("Extracting MySQL binaries");
        $zippath = sprintf('unzip -o %s', __DIR__ . "/mysqlbinaries.zip");
        zendLog("Extracting MySQL binaries with command: '" . $zippath . "'");
        exec($zippath);
        zendLog("MySQL binaries extracted");
    }

    $appLocation = getenv('ZS_APPLICATION_BASE_DIR');
    $catalogVersionInPackage = "|02-24|05-16";

    $localFile = $appLocation . '/app/etc/local.xml';
    $originLocalFile = __DIR__ . '/local.xml';
    $jobsFile = $appLocation . '/app/etc/jobs.xml';
    $originJobsFile = __DIR__ . '/jobs.xml';
    $mysqlFile = __DIR__ . '/mysql';
    $seedFile = $appLocation . '/var/backup.sql.gz';

    $arr = array(
        'CRYPT' => getenv('ZS_CRYPT'),
        'COMPOSER_PATH' => getenv('ZS_APPLICATION_BASE_DIR'),
        'DISABLE_LOCAL_MODULES' => getenv('ZS_DISABLE_LOCAL_MODULES'),
        'DB_TABLE_PREFIX' => getenv('ZS_DB_TABLE_PREFIX'),

        'DB_HOST' => getenv('ZS_DB_HOST'),
        'DB_USERNAME' => getenv('ZS_DB_USERNAME'),
        'DB_PASSWORD' => getenv('ZS_DB_PASSWORD'),
        'DB_NAME' => getenv('ZS_DB_NAME'),
        'DB_INITSTATEMENTS' => getenv('ZS_DB_INITSTATEMENTS'),
        'DB_MODEL' => getenv('ZS_DB_MODEL'),
        'DB_TYPE' => getenv('ZS_DB_TYPE'),
        'DB_PDOTYPE' => getenv('ZS_DB_PDOTYPE'),
        'DB_ACTIVE' => getenv('ZS_DB_ACTIVE'),

        'SESSION_SAVE' => getenv('ZS_SESSION_SAVE'),

        'REDIS_HOST' => getenv('ZS_REDIS_HOST'),
        'REDIS_PORT' => getenv('ZS_REDIS_PORT'),
        'REDIS_TIMEOUT' => getenv('ZS_REDIS_TIMEOUT'),
        'REDIS_DB' => getenv('ZS_REDIS_DB'),
        'REDIS_COMPRESSION_THRESHOLD' => getenv('ZS_REDIS_COMPRESSION_THRESHOLD'),
        'REDIS_COMPRESSION_LIB' => getenv('ZS_REDIS_COMPRESSION_LIB'),
        'REDIS_LOG_LEVEL' => getenv('ZS_REDIS_LOG_LEVEL'),
        'REDIS_MAX_CONCURRENCY' => getenv('ZS_REDIS_MAX_CONCURRENCY'),
        'REDIS_BREAK_AFTER_FRONTEND' => getenv('ZS_REDIS_BREAK_AFTER_FRONTEND'),
        'REDIS_BREAK_AFTER_ADMINHTML' => getenv('ZS_REDIS_BREAK_AFTER_ADMINHTML'),
        'REDIS_BOT_LIFETIME' => getenv('ZS_REDIS_BOT_LIFETIME'),
        'REDIS_DISABLE_LOCKING' => getenv('ZS_REDIS_DISABLE_LOCKING'),

        'BACKEND_SERVER' => getenv('ZS_BACKEND_SERVER'),
        'BACKEND_PORT' => getenv('ZS_BACKEND_PORT'),
        'BACKEND_DB' => getenv('ZS_BACKEND_DB'),
        'BACKEND_FORCE_STANDALONE' => getenv('ZS_BACKEND_FORCE_STANDALONE'),
        'BACKEND_CONNECT_RETRIES' => getenv('ZS_BACKEND_CONNECT_RETRIES'),
        'BACKEND_READ_TIMEOUT' => getenv('ZS_BACKEND_READ_TIMEOUT'),
        'BACKEND_AUTOMATIC_CLEANING_FACTOR' => getenv('ZS_BACKEND_AUTOMATIC_CLEANING_FACTOR'),
        'BACKEND_COMPRESS_DATA' => getenv('ZS_BACKEND_COMPRESS_DATA'),
        'BACKEND_COMPRESS_TAGS' => getenv('ZS_BACKEND_COMPRESS_TAGS'),
        'BACKEND_COMPRESS_THRESHOLD' => getenv('ZS_BACKEND_COMPRESS_THRESHOLD'),
        'BACKEND_COMPRESSION_LIB' => getenv('ZS_BACKEND_COMPRESSION_LIB'),
        'BACKEND_USE_LUA' => getenv('ZS_BACKEND_USE_LUA'),
        'BACKEND_NOMATCHINGTAGS' => getenv('ZS_BACKEND_NOMATCHINGTAGS'),

        'REDISKIBANA_HOST' => getenv('ZS_REDISKIBANA_HOST'),
        'REDISKIBANA_PORT' => getenv('ZS_REDISKIBANA_PORT'),
        'REDISKIBANA_DATABASE' => getenv('ZS_REDISKIBANA_DATABASE'),
        'REDISKIBANA_KEYPREFIX' => getenv('ZS_REDISKIBANA_KEYPREFIX'),
        'REDISKIBANA_LOGGING_ENABLED' => getenv('ZS_REDISKIBANA_LOGGING_ENABLED'),

        'PHP_BINARY_PATH' => getenv('ZS_PHP_BINARY_PATH'),
        'JOBPROCESSOR_SERVER' => getenv('ZS_JOBPROCESSOR_SERVER'),
        'JOBPROCESSOR_PORT' => getenv('ZS_JOBPROCESSOR_PORT'),
        'JOBPROCESSOR_DB' => getenv('ZS_JOBPROCESSOR_DB'),
        'JOBPROCESSOR_CLEARTIMEOUT' => getenv('ZS_JOBPROCESSOR_CLEARTIMEOUT'),
        'JOBPROCESSOR_MIN_CONSUMER_OVERHEAD' => getenv('ZS_JOBPROCESSOR_MIN_CONSUMER_OVERHEAD'),

        'SANDBOX_REPLICAS_FILE' => getenv('ZS_SANDBOX_REPLICAS_FILE'),
        'SANDBOX_SANDBOX_FILE' => getenv('ZS_SANDBOX_SANDBOX_FILE'),
        'SYSTEM_PATH_MEDIA' => getenv('ZS_SYSTEM_PATH_MEDIA'),
        'SYSTEM_PATH_CONTRACT_EMAIL' => getenv('ZS_SYSTEM_PATH_CONTRACT_EMAIL'),
        'SYSTEM_PATH_CONTRACT' => getenv('ZS_SYSTEM_PATH_CONTRACT'),
        'SYSTEM_PATH_VAR' => getenv('ZS_SYSTEM_PATH_VAR'),
    );

    if (file_exists($originLocalFile)) {
        $localXml = file_get_contents($originLocalFile);
        if (isset($localXml)) {
            zendLog("Updating local.xml");
            $localXml = str_replace(array_keys($arr), $arr, $localXml);
            file_put_contents($localFile, $localXml);
            zendLog("Updated local.xml");
        }
    }

    // Replicas configuration
    zendLog("Updating replicas from ".getenv('ZS_SANDBOX_REPLICAS_FILE'));
    if(getenv('ZS_SANDBOX_REPLICAS_FILE') != ''){
        zendLog("Using configured file");
        $replicasXml = file_get_contents(getenv('ZS_SANDBOX_REPLICAS_FILE'));
        zendLog($replicasXml);
    } else {
        zendLog("Using default");
        $replicasXml = file_get_contents(__DIR__ . '/replicas.xml');
    }
    $replicasFile = $appLocation . '/app/etc/replicas.xml';
    file_put_contents($replicasFile, $replicasXml);
    zendLog("Updated");

    // Sandbox configuration
    zendLog("Updating replicas from ".getenv('ZS_SANDBOX_REPLICAS_FILE'));
    if(getenv('ZS_SANDBOX_SANDBOX_FILE') != ''){
        zendLog("Using configured file");
        $sandboxXml = file_get_contents(getenv('ZS_SANDBOX_SANDBOX_FILE'));
    } else{
        zendLog("Using default");
        $sandboxXml = file_get_contents(__DIR__ . '/sandbox.xml');
    }
    $sandboxFile = $appLocation . '/app/etc/sandbox.xml';
    file_put_contents($sandboxFile, $sandboxXml);
    zendLog("Updated");

    // React Config
    file_put_contents($appLocation.'/config/config.json', file_get_contents(__DIR__ . '/config.json'));

    // Jobs configuration
    if (file_exists($originJobsFile)) {
        $jobsXml = file_get_contents($originJobsFile);
        if (isset($jobsXml)) {
            zendLog("Updating jobs.xml");
            $jobsXml = str_replace(array_keys($arr), $arr, $jobsXml);
            $jobsFile = $appLocation . '/app/etc/jobs.xml';
            file_put_contents($jobsFile, $jobsXml);
            zendLog("Updated jobs.xml");
        }
    }

    // Set media folder
    shell_exec('mv '.$appLocation.'/media '.$appLocation.'/media.bak');
    shell_exec('ln -s '.getenv('ZS_SYSTEM_PATH_MEDIA').' '.$appLocation.'/media');
    shell_exec('cp -Rf '.$appLocation.'/media.bak/* '.$appLocation.'/media');
    shell_exec('rm -rf '.$appLocation.'/media.bak');

    // Set var folder
    shell_exec('mv '.$appLocation.'/var '.$appLocation.'/var.bak');
    shell_exec('ln -s '.getenv('ZS_SYSTEM_PATH_VAR').' '.$appLocation.'/var');
    shell_exec('cp -Rf '.$appLocation.'/var.bak/* '.$appLocation.'/var');
    shell_exec('rm -rf '.$appLocation.'/var.bak');

    # Adding script execution script
    $scriptContents = "<?php ";
    $scriptContents .= "var_dump(shell_exec('rm -f /tmp/wsdl-*'); \n";

    if (isset($_SERVER["HTTP_HOST"])) {
        $scriptContents .= "echo \"Executed on: " . $_SERVER['HTTP_HOST'] . "; \n";
    } else {
        $scriptContents .= "echo \"Executed on: " . exec('php -r "echo gethostname();"') . "; \n";
    }

    $filename = $appLocation . '/runscript.php';
    file_put_contents($filename, $scriptContents);
    chmod($filename, 0777);

    if (file_exists(__DIR__ . '/import_full.sh'))
    {
        zendLog("Updating sandbox script");
        $scriptLocation = $appLocation . '/shell/import_full.sh';
        $scriptFile = __DIR__ . '/import_full.sh';
        $data = file_get_contents($scriptFile);
        file_put_contents($scriptLocation, $data);
        chmod($scriptLocation, 0755);
        zendLog("Updated sandbox script");
    };

    // Copy Cron
    if(getenv('ZS_CRON_NAME') != '') {
        $cronContents = file_get_contents(__DIR__ . '/../cron.dist');
        str_replace('{CRON_USER}', getenv('ZS_CRON_USER'), $cronContents);
        str_replace('{PHP_BINARY_PATH}', getenv('ZS_PHP_BINARY_PATH'), $cronContents);
        str_replace('{CRON_DOCROOT}', getenv('ZS_CRON_DOCROOT'), $cronContents);
        file_put_contents('/etc/cron.d/' + getenv('ZS_CRON_NAME'), $cronContents);
    }


//mysql chmod
    chmod($mysqlFile, 0777);

// Create connection
    $conn = new mysqli(getenv('ZS_DB_HOST'), getenv('ZS_DB_USERNAME'), getenv('ZS_DB_PASSWORD'));

// Check connection
    if ($conn->connect_error) {
        throw new OSEException(sprintf('Connection failed: %s', $conn->connect_error));
    }

// Check if seed file is readable
//    if (!is_readable($seedFile)) {
//        throw new OSEException(sprintf('Backup file %s IS NOT READABLE or DOES NOT EXIST', $seedFile));
//    }

// Check if database exist
    if (!$conn->select_db(getenv('ZS_DB_NAME'))) {
        throw new OSEException(sprintf('Database %s not EXIST, create one', getenv('ZS_DB_NAME')));
    }


//Check if database don't contains any tables
//    $query = 'SHOW tables FROM ' . getenv('ZS_DB_NAME');
//    $result = $conn->query($query);
//    $row = $result->fetch_object();
//
//    zendLog("Checking if database don't contains any tables");
//
//    if (!$row) {
//
//        //Import database
//        zendLog("Restoring database from backup file => $seedFile");
//        zendLog('zcat ' . $seedFile . ' | ' . ' MYSQL ' . ' -h ' . getenv('ZS_DB_HOST') . ' -u ' . getenv('ZS_DB_USERNAME') . ' -p' . "*********" . ' ' . getenv('ZS_DB_NAME'));
//        exec('zcat ' . $seedFile . ' | ' . $mysqlFile . ' -h ' . getenv('ZS_DB_HOST') . ' -u ' . getenv('ZS_DB_USERNAME') . ' -p' . getenv('ZS_DB_PASSWORD') . ' ' . getenv('ZS_DB_NAME'));
//        zendLog("Database restored");
//
//        #Setting catalogversion according to zend package database contents
//        $scriptContents = "<?php ";
//        $scriptContents .= "return '$catalogVersionInPackage'; \n";
//
//        if (!is_dir($appLocation . '/media')) {
//            // dir doesn't exist, create it
//            mkdir($appLocation . '/media');
//            zendLog("media directory created because it did not exist yet");
//        }
//
//        $filename = $appLocation . '/media/APP_VERSION.php';
//        file_put_contents($filename, $scriptContents);
//        chmod($filename, 0777);
//        zendLog("Catalog version '$catalogVersionInPackage' was set according to the database in the package");
//
//    } else {
//        zendLog("Skipped database restoration");
//
//        $appLocation = getenv('ZS_APPLICATION_BASE_DIR');
//        zendLog("Copying previous catalog file to the new location");
//        $previousLocation = str_replace(getenv('ZS_CURRENT_APP_VERSION'), getenv('ZS_PREVIOUS_APP_VERSION'), $appLocation);
//        $source = $previousLocation . '/media/APP_VERSION.php';
//
//        if (file_exists($source) === true) {
//            $destination = $appLocation . '/media/APP_VERSION.php';
//            exec("cp -r $source $destination");
//            zendLog("Previous catalog file copied to the new location");
//        } else {
//            zendLog("Previous catalog version file not found, so no catalog version file was copied", "WARNING");
//        }
//    }

    $conn->close();
}catch (Exception $e) {
    throw new OSEException("Exception occurred during pre_activate: " .$e->getMessage());
}
zendLog("Stopping pre_activate");
