<?php
/* The script post_stage.php will be executed after the staging process ends. This will allow
 * users to perform some actions on the source tree or server before an attempt to
 * activate the app is made. For example, this will allow creating a new DB schema
 * and modifying some file or directory permissions on staged source files
 * The following environment variables are accessable to the script:
 * 
 * - ZS_RUN_ONCE_NODE - a Boolean flag stating whether the current node is
 *   flagged to handle "Run Once" actions. In a cluster, this flag will only be set when
 *   the script is executed on once cluster member, which will allow users to write
 *   code that is only executed once per cluster for all different hook scripts. One example
 *   for such code is setting up the database schema or modifying it. In a
 *   single-server setup, this flag will always be set.
 * - ZS_WEBSERVER_TYPE - will contain a code representing the web server type
 *   ("IIS" or "APACHE")
 * - ZS_WEBSERVER_VERSION - will contain the web server version
 * - ZS_WEBSERVER_UID - will contain the web server user id
 * - ZS_WEBSERVER_GID - will contain the web server user group id
 * - ZS_PHP_VERSION - will contain the PHP version Zend Server uses
 * - ZS_APPLICATION_BASE_DIR - will contain the directory to which the deployed
 *   application is staged.
 * - ZS_CURRENT_APP_VERSION - will contain the version number of the application
 *   being installed, as it is specified in the package descriptor file
 * - ZS_PREVIOUS_APP_VERSION - will contain the previous version of the application
 *   being updated, if any. If this is a new installation, this variable will be
 *   empty. This is useful to detect update scenarios and handle upgrades / downgrades
 *   in hook scripts
 */
require_once 'helpers.php';
zendLog("Starting post_stage");

try
{
//    if (!file_exists(__DIR__ . '/mysql') && !file_exists(__DIR__ . '/mysqldump')) {
//        $zippath = sprintf('unzip -o %s', __DIR__ . "/mysqlbinaries.zip");
//        zendLog("Extracting MySQL binaries with command: '" . $zippath . "'");
//        exec($zippath);
//        zendLog("MySQL binaries extracted");
//    }
//
//    $mysqlDumpFile = __DIR__ . '/mysqldump';
//    chmod($mysqlDumpFile, 0777);
//
//    $appLocation = getenv('ZS_APPLICATION_BASE_DIR');
//
//    if (file_exists(OSETEMPLOCATION)) {
//        // First remove the old backup files (keep last 5 files)
//        zendLog("Removing old backups");
//        exec("cd " . OSETEMPLOCATION . " && rm `ls " . OSETEMPLOCATION . "  -t | grep OSE_BACKUP_" . getenv('ZS_DB_NAME') . " | awk 'NR>5'`");
//    }
//
//    // Second create a database backup
//    $backupName = "OSE_BACKUP_" . getenv('ZS_DB_NAME') . "_" . date('Y-m-d') . "_FROM_" . getenv('ZS_PREVIOUS_APP_VERSION') . "_TO_" . getenv('ZS_CURRENT_APP_VERSION') . '.gz';
//    $previousLocation = str_replace(getenv('ZS_CURRENT_APP_VERSION'), getenv('ZS_PREVIOUS_APP_VERSION'), $appLocation);
//    $command = "cd $previousLocation/shell/" . " && " . "/usr/local/zend/bin/php createdatabasebackup.php --destination " . OSETEMPLOCATION . " --name $backupName";
//
//    if (file_exists("$previousLocation/shell/")) {
//        zendLog("Creating database backup using the createbackupfromdatabase.php script with command '$command'");
//        exec($command);
//        zendLog("Backup created at " . OSETEMPLOCATION . "$backupName");
//    }
//    else
//    {
//        zendLog("Backup could not be created at " . OSETEMPLOCATION . "$backupName" . " because the directory did not exist", "ERROR");
//    }
//
//    // Create connection
//    $conn = new mysqli(getenv('ZS_DB_HOST'), getenv('ZS_DB_USERNAME'), getenv('ZS_DB_PASSWORD'), getenv('ZS_DB_NAME'));
//
//    // Check connection
//    if ($conn->connect_error)
//    {
//        throw new OSEException(sprintf('Connection failed: %s', $conn->connect_error));
//    }
//
//    zendLog("Updating some database variables to be used as a default");
//
//    $query = [];
//    # Set webroot
//    $query[] = "UPDATE `" . getenv('ZS_DB_NAME') . "`.`core_config_data` SET `value` = '" . getenv('ZS_BASE_URL_SECURE') . "' WHERE `path` = 'web/secure/base_url'";
//    $query[] = "UPDATE `" . getenv('ZS_DB_NAME') . "`.`core_config_data` SET `value` = '" . getenv('ZS_BASE_URL_UNSECURE') . "' WHERE `path` = 'web/unsecure/base_url'";
//    # set the product visibility enabled
//    $query[] = "UPDATE `" . getenv('ZS_DB_NAME') . "`.`core_config_data` SET `value` = 1 WHERE `path` = 'catalog/frontend/product_visibility_enabled'";
//    #DISABLE STUB USAGE
//    $query[] = "UPDATE `" . getenv('ZS_DB_NAME') . "`.`core_config_data` SET `value` = 0 WHERE `path` = 'omnius_service/general/use_stubs'";
//    #SET OGW AS PARTYNAME
//    $query[] = "UPDATE `" . getenv('ZS_DB_NAME') . "`.`core_config_data` SET `value` = 'OGW' WHERE `path` = 'omnius_service/service_settings_header/party_name'";
//    #change password
//    $query[] = "UPDATE `" . getenv('ZS_DB_NAME') . "`.`admin_user` SET `password` = 'c71fba4ed840df3e6c19d5e0b67335a3:BawaVeKuXHrdth1qY1sabUofS72A3vmY' WHERE `username` = 'admin'";
//
//    foreach ($query as $q) {
//        $conn->query($q);
//    }
//
//    zendLog("Database variables updated");
//
//    $conn->close();
}catch (Exception $e) {
    throw new OSEException("Exception occurred during post_stage: " .$e->getMessage());
}
zendLog("Stopping post_stage");
