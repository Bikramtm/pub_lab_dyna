'use strict';

(function ($) {
  window.CustomerDe = function () {
    this.initialize.apply(this, arguments);
    this.customerData = null; // the loaded customer details data
    this.customerSearchResultsJson = null; // the entire list found on a search of a customer
    this.templateSourceCustomersList = '#Handlebars-Template-Search-Customer';
    this.templateSourceCustomersPotentialLink = '#Handlebars-Template-Customer-Potential-Link';
    this.customerInfoContainer = $('#customer-search-results');
    this.breadcrumb = $('#breadcrumb-custom');
    this.breadcrumbValue = [];
    this.leftCustomerInfoContainer = $('.col-left').find('.sticker');
    this.customerShoppingCartData = {};
    this.customerProducts = {};
    this.templateShoppingCartViewModal = '#Handlebars-Template-Shopping-Cart-View-Modal';
    this.templateShoppingCartDeleteModal = '#Handlebars-Template-Shopping-Cart-Delete-Modal';
    this.templateShoppingCartNotesModal = '#Handlebars-Template-Shopping-Cart-Notes-Modal';
    this.customersToLink = [];
    this.typePrivate = null;
    this.typeSoho = null;
    this.searchedCustomerData = null;
    this.localCustomersTemplate = $('#Local-Customer-Search-Results');
    this.dunningContainerModal = $('#dunningStatusModal');
    this.isBusiness = false;
    this.customerNumber = null;
    this.customerObject = null;
    this.topAddressSearch = '#topaddressCheckForm';
    this.offerId = null;
    this.targetPanelId = null;
    this.isAjax = false;
  };
  window.CustomerDe.prototype = $.extend(VEngine.prototype, {
    // Used only from leave campaign confirmation modal
    selectProduct: function (element) {
      // Remove campaigns spans from configurator
      jQuery('#config-wrapper').find('.item-row').find('.campaign-identifier').find('span').addClass('hidden').html('');
      var productId = jQuery(element).attr('data-product-id');
      jQuery('#config-wrapper').find('.item-row[data-id=\'' + productId + '\']').trigger('click');
    },
    setBusinessState: function () {
      //  If customer loaded, customer type cannot be changed
      if (this.customerData) {
        return;
      }
      var toggleClass = '.soho-toggle';
      if (!window.configurator && $('.cart_packages').children('.side-cart:visible').length == 0) {
        ToggleBullet.onSwitchChange(toggleClass, 'soho');
        /** customer switch has been updated */
        if (this.isBusiness = ToggleBullet.state(toggleClass)) {
          /** OMNVFDE-301: For SOHO, nationality and DOB not displayed, **/
          $('[data-id="customer-details-dob"]').hide();
          $('[data-target=' + CABLE_TV_PACKAGE + ']').addClass('disabled');
        } else {
          $('[data-id="customer-details-dob"]').show();
          if (address.addressId) {
            $('[data-target=' + CABLE_TV_PACKAGE + ']').removeClass('disabled');
          }
        }
      }
      else {
        ToggleBullet.switchPending = true;
        showCancelCartModal();
      }
    },
    hasLegacyType: function (container, type) {
      for (var i = 0; i < container.length; i++) {
        if (container[i].name == 'legacy_type' && container[i].value == type) {
          return true;
        }
      }

      return false;
    },
    validateExactAddress: function (data) {
      var el = $(data).parents('form');
      var self = this;
      var formData = el.serializeObject();
      var exactModal = $('#validateExactAddressMultipleResultsModal');
      var exactAddresses = exactModal.find('.ajax-results-addresses:first');

      // set form settings
      formData['type'] = 'validate_exact_address';
      formData['loader'] = false; // disable loader

      // create the callback
      var callback = function (results) {
        $('#spinner').modal('hide');
        if (results.error || (!results.addresses.length)) {
          results.message = !results.addresses || !results.addresses.length ? results.message : Translator.translate('Invalid address');
          showModalError(results.message);
        } else {
          var template = Handlebars.compile($('#HT-validateExactAddressMultipleResults').html());
          var responseHTML = template(results);

          exactAddresses.html(responseHTML);
          exactModal.modal('show');
        }
      };

      // post data
      this.ajax('POST', 'address.validate', formData, callback);
    },
    searchExactAddress: function (data) {
      $('#customer_search_form').find('#street').val($(data).data('street'));
      $('#customer_search_form').find('#zipcode').val($(data).data('postalcode'));
      $('#customer_search_form').find('#city').val($(data).data('city-name'));
      $('#customer_search_form').find('#no').val($(data).data('building-nr'));
      $('#customer_search_form').find('#perform-exact-address-search').val('1');
      $('#customer_search_form').find('#submit-search').trigger('click');
      $('#customer_search_form').find('#perform-exact-address-search').val('0');

      $('#validateExactAddressMultipleResultsModal').modal('hide');
    },
    validateCustomerSearchFields: function () {
      var valid = false;

      $('#customer_search_form input[type=text]').each(function () {
        if (($(this).val() != '') && !$(this).closest('div').hasClass('hide')) {
          valid = true;
          return false;
        }
      });

      return valid;
    },
    searchCustomer: function (data, sortAlphabetical) {
      var self = this,
        el = $(data).parents('form');
      var $_loadingGif = $(data).children('.loading-search'),
        $_searchFilters = $('.search-fields');

      var validationResults = this.validateCustomerSearchFields();

      $_loadingGif.removeClass('invisible');
      $_searchFilters.addClass('disabled-section');

      if (this.helper.isObject(el)) {
        data = $.map($(el).serializeArray(el), function (obj) {
          return obj;
        });
      }

      var advancedSearchRadio = $('#advanced-search');
      if (advancedSearchRadio.val() == 0) {
        for (var i = 0; i < data.length; i++) {
          if (data[i].name == 'email') {
            data.splice(i, 1);
            i--;
          }
          if (data[i].name == 'device_id' && !self.hasLegacyType(data, 'cable')) {
            data.splice(i, 1);
            i--;
          }
          if (data[i].name == 'sim_imei' && !self.hasLegacyType(data, 'mobile')) {
            data.splice(i, 1);
            i--;
          }
        }
      }

      var legacySearchRadio = $('#legacy-search');
      if (legacySearchRadio.val() == 0) {
        for (var i = 0; i < data.length; i++) {
          if (data[i].name == 'legacy_type') {
            data.splice(i, 1);
            i--;
          }
        }
      }

      var callback = function (data, status) {
        self.customerSearchResultsJson = data;
        // According to OMNVFDE-1002 event though there is just 1 customer found he needs to be authenticated first
        /*if (!data.hasOwnProperty('one_result')) {
                 self.showCustomerList();
                 } else {
                 // if is a single result => get potential links call
                 if (self.customerSearchResultsJson.hasOwnProperty('serviceCustomers') &&
                 self.customerSearchResultsJson.serviceCustomers.hasOwnProperty('customers') &&
                 self.customerSearchResultsJson.serviceCustomers.hasOwnProperty('error') &&
                 self.customerSearchResultsJson.serviceCustomers.error == false) {

                 if (self.customerSearchResultsJson.serviceCustomers.customers[0]['Contact']['Temporary'] == 'false') {
                 console.log(data);
                 self.showLinkPageDetails(self.customerSearchResultsJson.serviceCustomers.customers[0]['Contact']['ID']);
                 } else {
                 self.showCustomerList(); // Show the list with only one result, which is the TMP one, which will not be clickable
                 }
                 }
                 else {
                 self.showCustomerList();
                 }
                 }*/

        // on an UCT flow, if the call number search returns only one result, the customer should be automatically loaded
        if (window.address.customerUctParams == true) {
          if (data.hasOwnProperty('one_result') && data.one_result) {
            var customerData = data.serviceCustomers.customers[0];

            self.linkCustomers(customerData.Contact.ID, customerData.ParentAccountNumber.ID, customerData.linkedAccounts.length, false);
            return true;
          }
        }

        address.clearFormData = false;
        /**
                 * Fill the serviceability top bar with the address fields from customer search and enable the check button
                 */
        self.fillServiceAddressForm();
        address.clearFormData = true;


        self.showCustomerList(sortAlphabetical);
        $_loadingGif.addClass('invisible');
        $_searchFilters.removeClass('disabled-section');
        // Initialize tooltips after ajax content
        $('[data-toggle="tooltip"]').tooltip();
      };
      var customersEndpoint = 'customer.search';
      if (el.find('.legacy-search-switch .toggle-button').hasClass('on-state')) {
        customersEndpoint = 'customer.searchLegacy';
      } else if (el.find('.exact-search-switch .toggle-button').hasClass('on-state')) {
        customersEndpoint = 'customer.searchExact';
      }
      //save searched data
      self.searchedCustomerData = data;

      if (validationResults) {
        this.post(customersEndpoint, {data: data, loader: false}, callback, function (err, status, xhr) {
          /**@var {XMLHttpRequest} xhr */
          if (xhr && xhr.status == 414) {
            err = 'Request too long.';
          }
          if (xhr && xhr.status != 0) {
            showModalError(err.message);
          }
          $_loadingGif.addClass('invisible');
          $_searchFilters.removeClass('disabled-section');
        });
      } else {
        var templateData = {
          error: 1,
          message: Translator.translate('At least one search criterion must be passed')
        };
        callback(templateData, [])
      }
    },
    fillServiceAddressForm: function () {
      var formData = {
        'postCode': $('[id="zipcode"]').val(),
        'city': $('[id="city"]').val(),
        'street': $('[id="street"]').val(),
        'houseNo': $('[id="no"]').val(),
        'addition': $('[id="addNo"]').val()
      };
      address.fillServiceabilityFormFields(formData);

      for (var field in formData) {
        if (formData[field] !== null && formData[field] !== '') {
          address.enableCheckServiceabilityButton();
          break;
        }
        address.disableCheckServiceabilityButton();
      }
    },

    prepareAndLoadCustomer: function (parentAccountNumberId, contactId, isProspect) {
      var self = this;
      var packageAddedInCart = $('.cart_packages .side-cart').is('[data-package-id]');
      if (packageAddedInCart) {
        $.post(MAIN_URL + 'configurator/cart/empty', function () {
          $('#package-types')
            .find('.show-clear-cart-warning')
            .removeClass('show-clear-cart-warning');
          self.loadCustomerData(parentAccountNumberId, contactId, isProspect, true);
        });
      } else {
        self.loadCustomerData(parentAccountNumberId, contactId, isProspect, false);
      }
    },
    loadCustomerData: function (parentAccountNumberId, contactId, isProspect, cartWasCleared) {
      var self = this;

      var callbackMode1 = function (data) {

        if (data.error) {
          showModalError(data.message);
          setPageLoadingState(false);
          return;
        }

        if (data.warning) {
          showNotificationBar(data.message, 'error');
          // Set the proceed/link and proceed btns to show the warning in clicked
          $('.proceed-button input, .link-proceed-button input').attr('onclick', 'showNotificationBar(\'' + data.message + '\', \'error\')');
          setTimeout(function () {
            hideNotificationBar();
          }, 10000);
          return;
        }

        options.mode = 2;

        $.ajax({
          url: window.customerDe.settings.endpoints['customer.load'],
          type: 'post',
          data: options,
          loader: false,
          success: callbackMode2,
          error: function (err, status, xhr) {
            if (xhr && xhr.status != 0) {
              showModalError(xhr.message);
            }
          }
        });

        // OMNVFDE-3166: Clear service address when a customer is loaded
        address.resetServices();
        self.customerNumber = data.customerData.customer_number;




        // Preserve customer data
        self.customerData = data.customerData;
        if (data.customerData) {
          var leftDetailsSticker = $('#Handlebars-Template-Details-Customer'),
            leftDetailsStickerTemplate = (leftDetailsSticker.html()) ? leftDetailsSticker.html() : '';
          // Parse the json with customer results in the handlebars template
          var template = Handlebars.compile(leftDetailsStickerTemplate),
            customersHTML = template(data.customerData);
          self.leftCustomerInfoContainer.html(customersHTML);

          // Check customer type, set switch accordingly and disable switch
          self.handleCustomerState();

          // Check if element is expanded and hide it for customer campaigns content to be visible
          if ($('.enlargeSearch').hasClass('expanded')) {
            LeftSidebar.toggleLeftSideBar(false);
            self.customerInfoContainer.empty();
          }

          //disable check serviceability form until mode2 call has completed
          address.disableTopAddressCheckForm();

          //disable links that depend on the next calls
          LeftSidebar.setCustomerMode2ButtonsState(false);
          LeftSidebar.setCustomerHouseHoldButtonsState(false);

          /**
                     * if the cart was previously empties => update the cart, update totals, destroy configurator
                     */
          if (cartWasCleared != undefined && cartWasCleared == true) {
            if (window['configurator']) {
              window['configurator'].destroy();
            }
            var totals = $('.sidebar #cart_totals');
            totals.replaceWith(data.totals);
            $('.side-cart[data-package-id]').remove();
            $('#package-types').removeClass('hide');
            $('#package-types ').find('.btn').each(function (index, el) {
              $(el).removeClass('hide')
            });
            window.sidebar.handlePriceDisplayState();
            window.configurator && window.configurator.closeBundleDetailsModal();
          }

          /**
                     * if the same address as the customer address is already loaded -> do not reload the address
                     */
          if (self.customerAddressIsLoadedAddress(data)) {
            return;
          }
        }
        if (data.saveCartModal) {
          $('#save-cart-modal').replaceWith(data.saveCartModal);
        }
        if (data.createWorkItemModal) {
          $('#workitem_service_create_modal').replaceWith(data.createWorkItemModal);
        }
        self.checkDunningStatus();
        self.checkHasExcludedAccount();

        $('.left-customer-info').children('.loading-search').removeClass('invisible');


      };

      var callbackMode2 = function (data) {
        if (data.error) {
          showModalError(data.message);
          setPageLoadingState(false);
          return;
        }

        LeftSidebar.setCustomerMode2ButtonsState(true);
        address.enableTopAddressCheckForm();

        if (!data.customerData.isProspect) {
          sessionStorage.setItem('permissionShowKias', data.customerData.permissionMessages.show_kias);
          sessionStorage.setItem('permissionShowFn', data.customerData.permissionMessages.show_fn);
          sessionStorage.setItem('permissionShowKd', data.customerData.permissionMessages.show_kd);

          self.post('customer.household', {loader: true}, callbackHousehold, function (err, status, xhr) {
            if (xhr && xhr.status != 0) {
              showModalError(xhr.message);
            }
          });

          $.each(data['has_open_orders'], function (packageType, hasOpenOrder) {
            $('#package-types')
              .find('.btn-block.cursor[data-target="' + packageType + '"]')
              .attr('data-has-open-order', String(hasOpenOrder));
          });

          if ($('#package-types').find('.btn-block.cursor[data-has-open-order!="false"]').length > 0) {
            showNotificationBar('<strong>Open orders in progress.</strong> Some functions will not be available', 'warning');
          }
        }

        $('.left-customer-info').children('.loading-search').addClass('invisible');

        // Preserve customer data
        if (data.customerData) {
          $.extend(self.customerData, data.customerData);

          var leftCustomerInfo = $('#ctn-tabs');
          if (data.customerData.products_count > 0) {
            leftCustomerInfo.find('a[data-toggle="products-content"]')
              .parent().children('.number-of-circle:first')
              .html(data.customerData.products_count)
              .removeClass('hidden');
          }
          if (data.customerData.orders_count > 0) {
            leftCustomerInfo.find('a[data-toggle="orders-content"]')
              .parent().children('.number-of-circle:first')
              .html(data.customerData.orders_count)
              .removeClass('hidden');
          }

          // Check customer type, set switch accordingly and disable switch
          self.handleCustomerState();

          // Check if element is expanded and hide it for customer campaigns content to be visible
          if ($('.enlargeSearch').hasClass('expanded')) {
            LeftSidebar.toggleLeftSideBar(false);
          }

          /**
                     * if the same address as the customer address is already loaded -> do not reload the address
                     */
          if (self.customerAddressIsLoadedAddress(data)) {
            return;
          }
        }
        if (self.customerData.isProspect == undefined || !self.customerData.isProspect) {
          var elem;
          var topAddressSearch = $(self.topAddressSearch);

          if (self.customerData.service_address !== undefined && self.customerData.service_address && self.customerData.service_address.street && self.customerData.service_address.no) {
            $('.address-check span.loading-search').removeClass('invisible');

            if (self.customerData.service_address.id) {
              // Build new complete customer address element used by checkService function
              elem = jQuery('<div/>', {
                'data-id': self.customerData.service_address.id,
                'data-street': self.customerData.service_address.street,
                'data-building-nr': self.customerData.service_address.no,
                'data-postalcode': self.customerData.service_address.postal_code,
                'data-city-name': self.customerData.service_address.city,
                'data-house-addition': self.customerData.service_address.house_addition,
                'data-is-customer': true
              });

              // Perform serviceAvailability call
              address.checkService(elem);
              // Filter available campaigns
              phone.filterCustomerCampaigns(self.customerData);
            } else {
              // No service address id received, triggering validate address (see OMNVFDE-2444)
              topAddressSearch.find('input[name="postcode"]').val(self.customerData.service_address.postal_code);
              topAddressSearch.find('input[name="city"]').val(self.customerData.service_address.city);
              topAddressSearch.find('input[name="street"]').val(self.customerData.service_address.street);
              topAddressSearch.find('input[name="houseNo"]').val(self.customerData.service_address.no);
              topAddressSearch.find('input[name="addition"]').val(self.customerData.service_address.house_addition);
              address.checkServiceabilityHeader(self.topAddressSearch);
            }
          } else if (self.customerData.legal_address_id !== undefined) {
            if (self.customerData.legal_address_id) {
              $('.address-check span.loading-search').removeClass('invisible');

              elem = jQuery('<div/>', {
                'data-id': self.customerData.legal_address_id,
                'data-street': self.customerData.street,
                'data-building-nr': self.customerData.no,
                'data-postalcode': self.customerData.postal_code,
                'data-city-name': self.customerData.city,
                'data-house-addition': self.customerData.house_addition,
                'data-is-customer': true
              });

              // Perform serviceAvailability call
              address.checkService(elem);
              // Filter available campaigns
              phone.filterCustomerCampaigns(self.customerData);
            } else {
              // No service address id received, triggering validate address (see OMNVFDE-2444)
              topAddressSearch.find('input[name="postcode"]').val(self.customerData.postal_code);
              topAddressSearch.find('input[name="city"]').val(self.customerData.city);
              topAddressSearch.find('input[name="street"]').val(self.customerData.street);
              topAddressSearch.find('input[name="houseNo"]').val(self.customerData.no);
              topAddressSearch.find('input[name="addition"]').val(self.customerData.house_addition);
              address.checkServiceabilityHeader(self.topAddressSearch);
            }
          } else {
            console.error('No service address or legal address received for current customer. Serviceability/Validate address not triggered.');
          }
        }
        if (data.saveCartModal) {
          $('#save-cart-modal').replaceWith(data.saveCartModal);
        }
        if (data.createWorkItemModal) {
          $('#workitem_service_create_modal').replaceWith(data.createWorkItemModal);
        }
        if (data.eligibleBundles) {
          self.updateRedPlusCartButton(data.eligibleBundles);
        }

        setPageLoadingState(false);
      };

      var callbackHousehold = function (data) {
        if (data.error || !data.hasOwnProperty('household')) {
          if (data.hasOwnProperty('message')) {
            showModalError(data.message, 'Error', null);
          } else {
            showModalError(data, 'Error', null);
          }

          return;
        }

        LeftSidebar.setCustomerHouseHoldButtonsState(true);

        $('#household_badge').html(data.household.members_count);
      };

      var options = {
        contactId: contactId,
        parentAccountNumberId: parentAccountNumberId,
        isProspect: isProspect == undefined ? 0 : isProspect,
        cartWasCleared: cartWasCleared == undefined ? false : cartWasCleared,
        mode: 1
      };
      $.ajax({
        url: window.customerDe.settings.endpoints['customer.load'],
        type: 'post',
        data: options,
        loader: true,
        success: callbackMode1,
        error: function (err, status, xhr) {
          if (xhr && xhr.status != 0) {
            showModalError(xhr.message);
          }
        }
      });
    },

    updateRedPlusCartButton: function (bundles) {
      if (bundles) {
        bundles.forEach(function (bundle) {
          if (bundle.redPlusBundle && (bundle.targetedSubscriptions.length > 0 || bundle.targetedPackagesInCart.length > 0) && bundle.creationTypeId) {
            $('div#package-types').find('.btn[data-package-creation-type-id=\'' + REDPLUS_CREATION_TYPE_ID + '\']:first').removeClass('disabled');
          }
        });

      }
    },

    /**
         * If there is already an address loaded
         * check if this existing address is equals to the customer address that needs to be loaded
         * @returns {boolean}
         */
    customerAddressIsLoadedAddress: function (data) {
      if (address == undefined || !address.hasOwnProperty('services') || !address.services || !address.services.hasOwnProperty('address_id')) {
        return false;
      }
      var self = this;
      var topAddressSearch = $(self.topAddressSearch);
      if (data.customerData.service_address !== undefined && data.customerData.service_address) {
        if (data.customerData.service_address.id && address.services.address_id == data.customerData.service_address.id) {
          return true;

        } else if (data.customerData.service_address.postal_code == topAddressSearch.find('input[name="postcode"]').val() &&
                    data.customerData.service_address.city == topAddressSearch.find('input[name="city"]').val() &&
                    data.customerData.service_address.street == topAddressSearch.find('input[name="street"]').val() &&
                    data.customerData.service_address.no == topAddressSearch.find('input[name="houseNo"]').val() &&
                    data.customerData.service_address.house_addition == topAddressSearch.find('input[name="addition"]').val()) {
          return true;
        }
      }
      else if (data.customerData.legal_address_id !== undefined) {
        if (data.customerData.legal_address_id && address.services.address_id == data.customerData.legal_address_id) {
          return true;
        }
        else if (data.customerData.postal_code == topAddressSearch.find('input[name="postcode"]').val() &&
                    data.customerData.city == topAddressSearch.find('input[name="city"]').val() &&
                    data.customerData.street == topAddressSearch.find('input[name="street"]').val() &&
                    data.customerData.no == topAddressSearch.find('input[name="houseNo"]').val() &&
                    data.customerData.house_addition == topAddressSearch.find('input[name="addition"]').val()) {
          return true;
        }
      }

      return false;
    },

    handleCustomerState: function () {
      var toggleClass = '.soho-toggle';

      if (this.customerData['isSoho'] || this.customerData['PartyType'] == this.typeSoho) {
        ToggleBullet.switchOn(toggleClass);
        this.isBusiness = true;
      } else {
        this.isBusiness = false;
        ToggleBullet.switchOff(toggleClass);
      }
      ToggleBullet.disable(toggleClass);
    },

    toggleCollapseLinkedCustomer: function (button, event, classPrefix) {
      event.stopPropagation();
      var target = $(button).attr('data-target');
      var targetSelected = $('.' + target);
      if (targetSelected.hasClass('out')) {
        targetSelected.removeClass('out');
        $(button).removeClass('collapse-button-' + classPrefix);
        $(button).addClass('contract-button-' + classPrefix);
      } else {
        targetSelected.addClass('out');
        $(button).addClass('collapse-button-' + classPrefix);
        $(button).removeClass('contract-button-' + classPrefix);
      }
    },
    getAuthDetails: function (contactId, parentAccountNumberId, event, button) {
      event.stopPropagation();
      var $_modalTarget = $('#authModal');
      $_modalTarget.find('.modal-body > p').addClass('hidden');

      var offsetTop = $(button).offset().top;
      var modalTop = offsetTop - 100; // For arranging correct position of model in different pages
      var offsetLeft = $(button).offset().left;
      var modalLeft = offsetLeft + 30;

      var modalData = {};

      // Get service call data
      $.ajax({
        url: '/customerde/details/authenticateCustomer/',
        type: 'post',
        data: ({
          contactId: contactId,
          parentAccountNumberId: parentAccountNumberId
        }),
        dataType: 'json',
        success: function (results) {
          if (results.error == true) {
            showModalError(results.message);
          } else {
            // parse the json with customer results in the handlebars template
            $.extend(modalData, results.data);

            $.each(modalData, function (index, value) {
              var $_elem = $_modalTarget.find('.' + index);
              value = !value ? 'N/A' : value;
              $_elem.text(value);
              // show the label even if there is no data
              // if (value) {
              $_elem.parent().removeClass('hidden');
              // }
            });
            $('.modal.authModal').css('top', modalTop + 'px');
            $('.modal.authModal').css('left', modalLeft + 'px');

            $_modalTarget.appendTo('body').modal({
              backdrop: false
            });
          }
        }
      });
    },
    /* Authentication for household members */
    getHouseholdAuthDetails: function (contactId, parentAccountNumberId, event, button) {
      var self = this;
      event.stopPropagation();
      var $_modalTarget = $('#householdAuthModal');
      $_modalTarget.find('.auth-data-parent').addClass('hidden');
      $(button).parent().parent().addClass('authselected');

      var offsetTop = $(button).offset().top;
      var height = $('.authModal .modal-content').height();
      var modalTop = offsetTop;
      var offsetLeft = $(button).offset().left;
      var modalLeft = offsetLeft - 77 - 10; // Modal already have 77 px from left, so removed that and moved 10px left
      var modalData = {};

      // Removing the data of stack as data are added dynamically
      $('.auth-data-parent .stack-div').remove();

      // Prevent multiple ajax call, check if ajax call is already made
      if (self.isAjax)
        return;
      self.isAjax = true;

      // Get service call data
      $.ajax({
        url: '/customerde/details/authenticateHouseholdCustomer/',
        type: 'post',
        data: ({
          contactId: contactId,
          parentAccountNumberId: parentAccountNumberId
        }),
        dataType: 'json',
        success: function (results) {

          // Enable ajax call to happen
          self.isAjax = false;

          if (results.error == true) {
            showModalError(results.message);
          } else {
            $.extend(modalData, results);
            $.each(modalData.data, function (indexArr, valueArr) {
              $.each(valueArr, function (index, value) {
                var $_elem = $_modalTarget.find('.' + index + '-div');
                value = !value ? 'N/A' : value;
                var category_type = '';
                var category_value = valueArr['account_category'];

                if (category_value === 'KIAS' || category_value === 'KS')
                  category_type = 'stack-mobile-selected';
                else if (category_value === 'KD' || category_value === 'KIP')
                  category_type = 'stack-cable-selected';
                else if (category_value === 'FN' || category_value === 'DSL')
                  category_type = 'stack-fixed-selected';

                var html = '<div class="stack-div">';
                if (category_type)
                  html += '<span class="' + category_type + ' stack-icon"></span>';
                html += '<span class="' + index + ' auth-data">' + value + '</span>';

                html += '</div>';
                $_elem.append(html);
                $_elem.removeClass('hidden');
              });

            });

            $(button).parent().parent().removeClass('authselected');

            $('.modal.authModal').css('top', modalTop + 'px');
            $('.modal.authModal').css('left', modalLeft + 'px');

            $_modalTarget.appendTo('body').modal({
              backdrop: false
            });
          }
        }
      });
    },
    getTotalBillingDetails: function (event, button, customerNumber) {
      event.stopPropagation();
      var $_modalTarget = $(customerNumber);

      var offsetTop = $(button).offset().top;
      var modalContent = $_modalTarget.find('.modal-content');
      var modalTop = offsetTop - modalContent.height() / 2 - 14;
      var offsetLeft = $(button).offset().left;
      var modalLeft = offsetLeft + 45;

      $_modalTarget.attr('aria-hidden', 'true')
        .css('width', modalContent.width() + 80 + 'px')
        .css('top', modalTop + 'px')
        .css('left', modalLeft + 'px');

      $_modalTarget.modal({
        backdrop: false,
        show: true
      });
      // Weird behavior with bootstrap mobile: second modal, even though has backdrop set to false will trigger backdrop
      $('.modal-backdrop').remove();

    },
    closeModal: function () {
      $('.modal').not('.bundleModal').modal('hide');
    },
    showLinkPageDetails: function (contactId) {
      var self = this;
      setPageLoadingState(true);
      // Get service call data
      $.ajax({
        url: '/customerde/index/getCustomerPotentialLinks/',
        type: 'post',
        data: ({contactId: contactId}),
        dataType: 'json',
        success: function (response) {
          // if there is a problem with the customer id do not proceed
          if (response.hasOwnProperty('validationError') && response.validationError == true) {
            var errorMessage = '';
            for (var p in response.message) {
              errorMessage += response.message[p] + ' ';
            }
            showModalError(errorMessage);
          } else {
            self.showLinkPage(response);
          }
          setPageLoadingState(false);
        }
      });
    },
    showCustomerList: function (sortAlphabetical) {
      var self = this;
      if (sortAlphabetical
                && self.customerSearchResultsJson.serviceCustomers
                && !self.customerSearchResultsJson.serviceCustomers.error) {
        self.customerSearchResultsJson.serviceCustomers.customers.sort(function (a, b) {
          var name1 = a.Role.Person.FamilyName.toLowerCase();
          var name2 = b.Role.Person.FamilyName.toLowerCase();

          if (name1 < name2) return -1;
          if (name1 > name2) return 1;
          return 0;
        });
      }
      // parse the json with customer results in the handlebars template
      var template = Handlebars.compile($(self.templateSourceCustomersList).html()),
        customersHTML = template(self.customerSearchResultsJson);
      self.customerInfoContainer.html(customersHTML);
      // after the search expand the left side column
      LeftSidebar.toggleLeftSideBar(true);
      this.customerInfoContainer.show();
    },
    markLinkCustomers: function (parentAccountNumberId, contactId, element) {
      var self = this;
      var elemIsLinkedSelector = '.icon-already-linked.linked-id-' + parentAccountNumberId;
      var elemIsNotLinkedSelector = '.icon-not-linked.linked-id-' + parentAccountNumberId;
      if ($(elemIsLinkedSelector).hasClass('hidden')) {
        $(elemIsLinkedSelector).removeClass('hidden');
        $(elemIsNotLinkedSelector).addClass('hidden');
        self.customersToLink.push(contactId);
      }
      else {
        $(elemIsNotLinkedSelector).removeClass('hidden');
        $(elemIsLinkedSelector).addClass('hidden');
        var indexToRemove = jQuery.inArray(contactId, self.customersToLink);
        if (indexToRemove !== (-1)) {
          // remove from array by value
          self.customersToLink.splice(indexToRemove, 1);
        }
      }
      self.removeLinkCustomersBtn(self.customersToLink);
    },
    removeLinkCustomersBtn: function(customersToLink) {
      // depending if a potential linked customer is checked, show or hide the buttons to "proceed and link"/"proceed"
      if (customersToLink.length) {
        $('.link-proceed-button').show();
        $('.proceed-button').hide();
      } else {
        $('.link-proceed-button').hide();
        $('.proceed-button').show();
      }
    },
    preventDoubleClick: function (elem) {
      var $el = $(elem);
      if ($el.data('clicked')) {
        // Previously clicked, stop actions

        return false;
      } else {
        // Mark to ignore next click
        $el.data('clicked', true);
        // Unmark after 1 second
        window.setTimeout(function () {
          $el.removeData('clicked');
        }, 1000)
      }
      return true;
    },
    linkCustomers: function (contactId, parentAccountNumberId, alreadyLinked, event) {

      var self = this;
      // Avoid executing multiple ajax calls if button is pressed again (see OMNVFDE-3396)
      if (event && !self.preventDoubleClick(event.target)) {
        return;
      }

      // if there are potential links marked => create the links
      if (self.customersToLink.length) {
        // The target customer is already a linked customer and we re-use the existing Link ID
        if (alreadyLinked <= 1) {
          self.customersToLink.push(contactId);
          contactId = null;
        }

        $.ajax({
          url: '/customerde/index/createPotentialLink/',
          type: 'post',
          data: {
            targetCustomerLinkedId: contactId,
            potentialCustomersLinks: self.customersToLink
          },
          dataType: 'json',
          success: function (response) {
            if (response.error) {
              showModalError(response.message, false, function() {
                self.customersToLink = [];
                self.removeLinkCustomersBtn(self.customersToLink);
                jQuery('.icon-already-linked').each(function() {
                  if (!jQuery(this).hasClass('hidden')) {
                    jQuery(this).addClass('hidden');
                    jQuery(this).prev().removeClass('hidden');
                  }
                });
              });
            } else {
              self.prepareAndLoadCustomer(parentAccountNumberId, response.data.PartyIdentification.ID, false);
            }
          },
          error: function (response) {
            console.log(response);
          }
        });
      }
      // else load the customer
      else {
        self.prepareAndLoadCustomer(parentAccountNumberId, contactId, false);
      }
    },
    sortArrayByField: function dynamicSort(property) {
      var sortOrder = 1;
      if (property[0] === '-') {
        sortOrder = -1;
        property = property.substr(1);
      }
      return function (a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
      }
    },
    showLinkPage: function (templateData) {

      //sort linked customer by priority [Cable, DSL, Mobile]
      if (typeof templateData != 'undefined' && typeof templateData.customer != 'undefined' && typeof templateData.customer.linkedAccounts != 'undefined') {
        var linkedArray = $.map(templateData.customer.linkedAccounts, function (value, index) {
          return [value];
        });
        linkedArray.sort(this.sortArrayByField('AccountCategory'));
        templateData.customer.linkedAccounts = linkedArray;
      }

      var self = this;
      templateData.customerLogged = self.customerData;
      $('#customer_link_details').find('.back-button').show();
      // parse the json with customer potential links in the handlebars template
      var template = Handlebars.compile($j(self.templateSourceCustomersPotentialLink).html()),
        customersHTML = template(templateData);
      this.customerInfoContainer.html(customersHTML);

      if (templateData.hasOwnProperty('customer') && templateData.customer.hasOwnProperty('linkedAccounts')) {
        $('input[type=checkbox]').each(function () {
          this.checked = true;
          self.markLinkCustomers(this.value, this);
        });
      }
      // after the search expand the left side column
      LeftSidebar.toggleLeftSideBar(true);
      // Initialize tooltips after ajax content
      $('[data-toggle="tooltip"]').tooltip();
    },
    /* Get household products for individual household members */
    householdProducts: function (contactId, parentAccountNumberId) {
      setPageLoadingState(true);
      var self = this;
      var options = {
        contactId: contactId,
        parentAccountNumberId: parentAccountNumberId,
        mode: 1
      };
      $.ajax({
        url: window.customerDe.settings.endpoints['customer.householdproducts'],
        type: 'post',
        data: options,
        loader: false,
        success: function (response) {
          setPageLoadingState(false);
          if (response.error == true) {
            showModalError(response.message);
          } else {
            // Adding breadcrumbs
            if (response.breadcrumb != undefined) {
              self.breadcrumbValue.push({
                text: response.breadcrumb,
                link: 'product_info_left',
                type: 'click'
              });
              // create breadcrumb
              self.createBreadcrumb();
            }

            var template = Handlebars.compile($('#left-sidebar-products-content').html()),
              customersHTML = template(response);
            self.customerInfoContainer.html(customersHTML);
            //display the billing info button if we have postpaid
            if ($('.products-order-number-kias-show').length > 0) {
              $.each($('.products-order-number-kias-show'), function (key, element) {
                var customerNumber = jQuery(element).attr('data-customer-number');
                jQuery('#products-order-number-kias-' + customerNumber).find('span').removeClass('hidden');
                jQuery('#products-order-number-kias-' + customerNumber).attr('data-toggle', 'popover');
              });
            }
          }
        },
        error: function (err, status, xhr) {
          if (xhr && xhr.status != 0) {
            showModalError(xhr.message);
          }
        }
      });

    },

    loadCustomerInfo: function (element) {
      if ( $(element).hasClass('disabled') ) {
        return;
      }

      var self = this;
      element = $(element);
      if (!element.parent().hasClass('active')) {
        var targetId = element.attr('data-toggle');
        self.targetPanelId = targetId;

        setPageLoadingState(true);
        $.post(MAIN_URL + 'customerde/details/showCustomerPanel', {'section': targetId}, function (response) {
          setPageLoadingState(false);
          if (response.error == true) {
            showModalError(response.message);
          } else {

            // set the shopping cart data on a customer variable
            switch (targetId) {
            case 'carts-content':
              self.customerShoppingCartData = response;
              break;
            case 'products-content':
              self.customerProducts = response;
              break;
            }

            // Adding breadcrumbs
            self.breadcrumb.addClass('hidden');
            if (response.breadcrumb != undefined) {
              // Empty breadcrumbs value as first will be parent here, if we want to add child then we'll not empty existing breadcrumb value
              self.breadcrumbValue = [];
              self.breadcrumbValue.push({
                text: response.breadcrumb,
                link: element.attr('id'),
                type: 'click'
              });

              self.breadcrumb.removeClass('hidden');
              self.createBreadcrumb();
            }

            /** templates ids need to be defined as left-sidebar- + section **/
            var template = Handlebars.compile($('#left-sidebar-' + targetId).html()),
              customersHTML = template(response);
            self.customerInfoContainer.html(customersHTML);
            //display the billing info button if we have pospaid
            if ($('.products-order-number-kias-show').length > 0) {
              $.each($('.products-order-number-kias-show'), function (key, element) {
                var customerNumber = jQuery(element).attr('data-customer-number');
                jQuery('#products-order-number-kias-' + customerNumber).find('span').removeClass('hidden');
                jQuery('#products-order-number-kias-' + customerNumber).attr('data-toggle', 'popover');
              });
            }

            /** Clear previously active element **/
            element.parent().siblings().removeClass('active');

            /** Mark selected element as active **/
            element.parent().addClass('active');

            /** Apply bootstrap dropDown (if needed) for newly parsed content **/
            $('.dropdown-toggle').dropdown();

            switch (targetId) {
            case 'products-content':
              customerSection.checkServiceabilityIsLoaded();
              customerSection.checkKipAddressForMoveOffnetAlreadyLoaded();
              break;
            }
            LeftSidebar.toggleLeftSideBar(true);

            // init popovers
            $('[data-toggle="popover"]').popover({
              html: true,
              title: function () {
                return $('#totalBillingDetailsModal' + $(this).attr('data-popover-type') + '-' + $(this).attr('data-customer-number') + ' .popover-header').html();
              },
              content: function () {
                return $('#totalBillingDetailsModal' + $(this).attr('data-popover-type') + '-' + $(this).attr('data-customer-number') + ' > .popover-content').html();
              }
            });
            // hide popover when clicking outside
            $('html').on('click', function (e) {
              if (typeof $(e.target).data('original-title') == 'undefined' &&
                                typeof $(e.target).parent().data('original-title') == 'undefined' &&
                                !$(e.target).parents().is('.popover.in')) {
                $('[data-original-title]').popover('hide');
              }
            });

            // Initialize tooltips after ajax content
            $('[data-toggle="tooltip"]').tooltip();

            var dropdownToggle = $('.dropdown[data-icon-collapse="true"]');
            dropdownToggle.on('shown.bs.dropdown', function () {
              $(this).children()
                .find('span.dropdown-changeable-icon')
                .removeClass($(this).data('expand-class'))
                .addClass($(this).data('collapse-class'));
            });

            dropdownToggle.on('hidden.bs.dropdown', function () {
              $(this).children()
                .find('span.dropdown-changeable-icon')
                .removeClass($(this).data('collapse-class'))
                .addClass($(this).data('expand-class'));
            });

            $('html').on('click', function (event) {
              if (dropdownToggle !== event.target && !dropdownToggle.has(event.target).length) {
                dropdownToggle.each(function () {
                  $(this).children()
                    .find('span.dropdown-changeable-icon')
                    .removeClass($(this).data('collapse-class'))
                    .addClass($(this).data('expand-class'));
                });
              }
            });

            // modals enable
            $('[data-toggle="modal"]').click(function () {
              var modalId = $(this).data('target');
              $(modalId).modal({
                'backdrop': false
              });
            });

            // show permission messages
            if (sessionStorage.getItem('permissionShowKias') == 'true') {
              jQuery('.kias-no-permission-wrapper').removeClass('hidden');
            }

            if (sessionStorage.getItem('permissionShowKd') == 'true') {
              jQuery('.kd-no-permission-wrapper').removeClass('hidden');
            }

            if (sessionStorage.getItem('permissionShowFn') == 'true') {
              jQuery('.fn-no-permission-wrapper').removeClass('hidden');
            }

          }
        });

        // hide the slide button form the left container
        $('.col-left .reduce').hide();
        // hide the shadow
        $('.col-left').css('box-shadow', 'none');
      } else {
        // remove active class of siblings
        element.parent().siblings().each(function () {
          $(this).removeClass('active');
        });
      }
    },
    // It will create breadcrumb html based on the data entered in breadcrumbValue
    // Implemented for click event on leftsidebar when clicked on breadcrumb link to go back
    // Change click to ajax for ajax call and href for calling href links
    createBreadcrumb: function () {
      var self = this;
      if (self.breadcrumbValue.length) {
        var html = '<span class="back"><img src="../../skin/frontend/vodafone/default/images/de/keyboard-arrow-left.png"/></span>';
        var count = self.breadcrumbValue.length;
        for (var i = 0; i < count; i++) {
          if (i == 0 && count == 1) {
            html += '<span class="active">' + self.breadcrumbValue[i].text.toUpperCase() + '</span>'
          } else if (i == 0 && count > 1) {
            if (self.breadcrumbValue[i].type == 'click') {
              html += '<span class="inactive" onclick="customerDe.callPanel(\'' + self.breadcrumbValue[i].link + '\')">' + self.breadcrumbValue[i].text.toUpperCase() + '</span>'
            } else if (self.breadcrumbValue[i].type == 'url') {
              html += '<span class="inactive"><a href="' + self.breadcrumbValue[i].link + '">' + self.breadcrumbValue[i].text.toUpperCase() + '</a></span>'
            } else {
              html += '<span class="inactive">' + self.breadcrumbValue[i].text.toUpperCase() + '</span>'
            }
          } else if (i > 0 && i != (count - 1)) {
            if (self.breadcrumbValue[i].type == 'url') {
              html += '<span class="divide">/</span><span class="inactive"><a href="' + self.breadcrumbValue[i].link + '">' + self.breadcrumbValue[i].text.toUpperCase() + '</a></span>'
            } else {
              html += '<span class="divide">/</span><span class="inactive">' + self.breadcrumbValue[i].text.toUpperCase() + '</span>'
            }
          } else {
            html += '<span class="divide">/</span><span class="active">' + self.breadcrumbValue[i].text.toUpperCase() + '</span>'
          }
        }
        self.breadcrumb.html(html);
        self.breadcrumb.show();
      }
    },
    // Copying selected element to clipboard
    addToClipboard: function(containerId) {
      if( $(containerId).length > 0 ) {
        $(containerId).select();
        document.execCommand('copy');
      }
    },
    loadOrderByIdFromContainer: function(containerId) {
      var orderId = $(containerId).val();
      window.location='/checkout/cart/view/order_number/' + orderId;
    },
    // It will call loadCustomerInfo function when clicked on the breadcrumb
    callPanel: function (targetId) {
      $('#' + targetId).parent().removeClass('active');
      $('#' + targetId).trigger('click');
    },
    unloadCustomer: function (element) {
      var self = this;
      self.customerNumber = null;
      // Avoid executing multiple ajax calls if button is pressed again
      $(element).attr('onclick','');
      // Unload customer
      $.ajax({
        url: '/customerde/details/unloadCustomer/',
        type: 'post',
        cache: false,
        data: ({customer: self.customerData != null  ? self.customerData.id : ''}),
        dataType: 'json',
        success: function (response) {
          setPageLoadingState(true);
          self.customerData = null;
          if (response.saveCartModal) {
            $('#save-cart-modal').replaceWith(response.saveCartModal);
          }
          window.location = '/';
          // remove cart warnings from session
          sessionStorage.removeItem('warning');
          sessionStorage.removeItem('warningMessage');
          sessionStorage.removeItem('warningMessageCardinality');

          sessionStorage.removeItem('permissionShowKias');
          sessionStorage.removeItem('permissionShowKd');
          sessionStorage.removeItem('permissionShowFn');

          // remove linked accounts & red+ panel states
          store.remove('redPlusPanelState');
          store.remove('linkedAccountPanelState');
          // disable Proceed button from cart
          $('#configurator_checkout_btn').addClass('disabled');
        }
      });
    },
    /**
         *
         * Sort a html table content by column
         *
         * @param element
         * @param container string | the container that holds the values
         * @param rowsContainer string | the type of element that holds row entries (tr|ul)
         * @param cellContainer string | the type of element that holds the value (td|li)
         */
    sortRows: function (element, container, rowsContainer, cellContainer) {
      element = $(element);
      /** If custom compare needed, use orderBy **/
      var orderBy = element.attr('order-by');
      var columnIndex = element.index();
      /** Remove all sorting references for element siblings */
      if (element.hasClass('sort-desc')) {
        element.siblings().removeClass('sort-asc').removeClass('sort-desc');
        element.removeClass('sort-desc');
        element.addClass('sort-asc');
      } else {
        element.siblings().removeClass('sort-asc').removeClass('sort-desc');
        element.removeClass('sort-asc');
        element.addClass('sort-desc');
      }

      var elems = jQuery.makeArray($(container).find(rowsContainer));
      elems.sort(function (a, b) {
        var valueA = $(a).find(cellContainer).eq(columnIndex).html().trim();
        var valueB = $(b).find(cellContainer).eq(columnIndex).html().trim();
        var result;

        /** Numeric comparison for default sorting ascending **/
        if (((valueA - 0) == valueA && ('' + valueA).trim().length > 0) && ((valueB - 0) == valueB && ('' + valueB).trim().length > 0)) {
          var nrValueA = parseInt(valueA);
          var nrValueB = parseInt(valueB);

          result = nrValueA > nrValueB ? 1 : -1;
        } else {
          /** strange behavior for Date.parse direct comparison so values will be converted to timestamps and compared as string **/
          if (orderBy == 'date') {
            var splitDateA = valueA.split('-');
            var splitDateB = valueB.split('-');

            valueA = splitDateA[2] + splitDateA[1] + splitDateA[0];
            valueB = splitDateB[2] + splitDateB[1] + splitDateB[0];
          }

          result = valueA.localeCompare(valueB);
        }

        if (element.hasClass('sort-asc')) {
          return result;
        } else {
          return result >= 0 ? -1 : 1;
        }
      });

      $(container).html(elems);

      /** Reindex drop down for bootstrap **/
      $('.dropdown-toggle').dropdown();
    },
    showShoppingCart: function (event, quoteId) {
      var self = this,
        shoppingCartDetails = {};
      event.stopPropagation();

      $.each(self.customerShoppingCartData.shoppingCarts, function (key, quoteObj) {
        //to do show a specific shopping cart details when the quoteId will be send as a valid one
        if (key == quoteId) {
          shoppingCartDetails = quoteObj;
        }
      });

      var template = Handlebars.compile($j(self.templateShoppingCartViewModal).html()),
        cartsHTML = template(shoppingCartDetails);
      $('#shoppingCartModal').html(cartsHTML);
      $('#shoppingCartModal').appendTo('body').modal();
    },
    deleteShoppingCart: function (event, quoteId) {
      var self = this,
        deleteTemplateData = {'quoteId': quoteId};
      event.stopPropagation();

      var template = Handlebars.compile($j(self.templateShoppingCartDeleteModal).html()),
        cartsHTML = template(deleteTemplateData);

      $('#shoppingCartModalDelete').html(cartsHTML);
      $('#shoppingCartModalDelete').appendTo('body').modal();
    },
    showShoppingCartNotes: function (event, notes) {
      var self = this,
        shoppingCartNotes = jQuery.parseJSON(notes);
      event.stopPropagation();

      var template = Handlebars.compile($j(self.templateShoppingCartNotesModal).html()),
        cartsHTML = template(shoppingCartNotes);

      $('#shoppingCartModalNotes').html(cartsHTML);
      $('#shoppingCartModalNotes').appendTo('body').modal();
    },
    expandRightSidebar: function () {
      var bundleModal = jQuery('.modal.bundleModal.bundleDetails');
      var direction = '';
      var enlargeButton = $('.enlarge');
      if ($(enlargeButton).hasClass('left-direction')) {
        direction = 'left';
        var leftSide = $('.col-left').outerWidth();
        $(enlargeButton).removeClass('left-direction');
        $(enlargeButton).addClass('right-direction');
        bundleModal.removeClass('hidden').addClass('hidden');
      }
      else {
        direction = 'right';
        var rightSide = $('.col-left').outerWidth();
        $(enlargeButton).removeClass('right-direction');
        $(enlargeButton).addClass('left-direction');
        bundleModal.removeClass('hidden');
      }
      var rightDirection = $(enlargeButton).hasClass('right-direction');
      var panelDefaultWidth = 315;

      if ($(enlargeButton).hasClass('expanded')) {
        var self = $(enlargeButton);
        $(enlargeButton).parent().find('.panel-content').fadeOut({
          queue: false,
          duration: 500,
          complete: function () {
            self
              .siblings('.sticker')
              .fadeIn(500);
            window.configurator && window.configurator.assureElementsHeight();
            if (direction == 'left') {
              $('.col-right').find('#right-panel-cart').remove();
            }
          }
        });
        $(enlargeButton).parent().animate({
          width: panelDefaultWidth
        }, 500);
        $('.col-main').fadeIn({queue: true, duration: 500});

        $(enlargeButton).removeClass('expanded').addClass('contracted');

      } else {

        if (direction == 'left') {
          expandTheRightSidebar();
        }
        //if the other panel is already expanded, contract it back
        if ($('button.expanded').length > 0) {
          $('button.expanded').first().trigger('click');
        }

        $('.col-main').fadeOut({queue: true, duration: 500});

        $(enlargeButton).parent().find('.sticker').fadeOut({
          queue: false,
          duration: 500,
          complete: function () {
            $(this)
              .siblings('.panel-content')
              .fadeIn(500);

            /** Triggering resize for cart content to expand **/
            $(window).trigger('resize');
          }
        });

        $(enlargeButton).removeClass('contracted').addClass('expanded');
      }
    },
    switchSavedCartMenu: function (event, self) {
      event.stopPropagation();
      self = jQuery(self);

      if (!self.hasClass('open')) {
        jQuery('.col-left .block-container').find('div').removeClass('open');
        jQuery('.checkout-package-details .saved-carts-menu').find('div').removeClass('open');
      }
      self.toggleClass('open');

      self.siblings('.menu-expand').first().toggleClass('open');

      if (!self.siblings('.menu-expand').first().hasClass('top-menu')) {
        jQuery('.menu-expand.top-menu').removeClass('open');
        jQuery('.drop-menu.top-menu').removeClass('open');
      }

      jQuery('.menu-expand.open').not(self.siblings('.menu-expand').first()).each(function () {
        jQuery(this).toggleClass('open');
      });

      if (jQuery('.saved-carts-menu-expand').last()[0] == self.siblings('.saved-carts-menu-expand')[0]) {
        jQuery('.table-responsive').height('+=40');
      }
    },
    toggleCustomerList: function (elem) {
      if ($(elem).hasClass('active')) {
        return;
      }
      $('.customer-list-tab').each(function () {
        $(this).toggleClass('active');
      });
      $('.customers-list').each(function () {
        $(this).toggleClass('hidden');
      });
    },

    loadLocalCustomersList: function () {
      var self = this;
      var container = $('.customers-local-list');
      container.html(' ');
      // Get local customers list
      $.ajax({
        url: '/customerde/search/getLocalCustomers',
        type: 'post',
        data: {data: self.searchedCustomerData, loader: false},
        dataType: 'json',
        success: function (response) {
          // parse the json with customer results in the handlebars template
          var template = Handlebars.compile(self.localCustomersTemplate.html()),
            customersHTML = template(response);
          container.html(customersHTML);
        }
      });
    },

    checkDunningStatus: function () {
      var self = this;
      if (self.customerData['in_dunning'] == 'IN_DUNNING') {
        var jElement = jQuery(self.dunningContainerModal);
        // Show it
        jElement.modal('show');
      }
    },
    closeDunningStatusModal: function () {
      this.dunningContainerModal.modal('hide');
    },
    checkHasExcludedAccount: function () {
      var self = this;
      if (self.customerData['has_excluded_cable_account'] && self.customerData['has_excluded_mobile_account']) {
        showNotificationBar(Translator.translate('Customer has excluded cable and mobile accounts. Only Active accounts are loaded.'), 'warning');
        setTimeout(function () {
          hideNotificationBar();
        }, 10000);
      } else if (self.customerData['has_excluded_cable_account']) {
        showNotificationBar(Translator.translate('Customer has excluded cable accounts. Only Active accounts are loaded.'), 'warning');
        setTimeout(function () {
          hideNotificationBar();
        }, 10000);
      } else if (self.customerData['has_excluded_mobile_account']) {
        showNotificationBar(Translator.translate('Customer has excluded mobile accounts. Only Active accounts are loaded.'), 'warning');
        setTimeout(function () {
          hideNotificationBar();
        }, 10000);
      }
    },

    submitWorkItem: function () {
      var createWorkitemTicketForm = new VarienForm('createWorkitemTicket');
      // check form validation
      if (createWorkitemTicketForm.validator.validate()) {
        $.ajax({
          url: '/agent/account/callCreateWorkItem',
          type: 'post',
          data: $('#createWorkitemTicket').serialize(),
          dataType: 'json',
          success: function (response) {
            if (response.error == false) {
              var referenceId = response.data.ReferenceID ? response.data.ReferenceID : '';
              $('#workitem_service_create_modal').modal('toggle');
              var successModal = jQuery('#success-workitem');
              jQuery('#created-ticket-id').text(referenceId);
              successModal.modal();
            } else {
              showModalError(response.message, 'Error', function () {
                $('#workitem_service_create_modal').modal('toggle');
              });
            }
          }
        });
      }
    },

    displayTemporaryCustomerError: function (customerNumber, $message) {
      event.stopPropagation();
      showNotificationBar(customerNumber + ': ' + $message, 'error');
      setTimeout(function () {
        hideNotificationBar();
      }, 10000);
    },

    loadOrderInfo: function (element) {
      var orderNumber = jQuery(element).attr('id');
      var orderStatus = jQuery(element).parents('.div-table-row').find('#osf-order-status').text();

      $.ajax({
        url: '/checkout/cart/displayOrder',
        type: 'post',
        data: {'orderNumber': orderNumber, 'orderStatus': orderStatus},
        dataType: 'json',
        success: function (response) {
          if (response.error == false) {
            jQuery('#customer-search-results div.customer-results-table').addClass('orderview-fix').empty().html(response.data);
            jQuery('#cart-left ul.bs-sidenav li:last-child').prev().addClass('active');

            // Scroll to the product overview step
            if (jQuery('#cart-content').length) {
              jQuery('#cart-content').animate({
                scrollTop: jQuery('p#order_overview').offset().top - jQuery('#cart-content').offset().top + jQuery('#cart-content').scrollTop()
              });
            }
          }
        }
      });
    },

    closeDunningAlert: function (element) {
      jQuery(element).parent().closest('.products-sub-package').fadeOut(function () {
        jQuery(this).remove();
      });
    },

    expandProducts: function (element) {
      var containerId = '#' + jQuery(element).attr('data-container-products-id');
      if (jQuery(element).hasClass('not-expanded')) {
        jQuery(element).removeClass('not-expanded').addClass('expanded');
        jQuery(containerId).fadeIn('100', function () {
          jQuery(this).removeClass('hide');
        });
      } else if (jQuery(element).hasClass('expanded')) {
        jQuery(element).addClass('not-expanded').removeClass('expanded');
        jQuery(containerId).fadeOut('100', function () {
          jQuery(this).addClass('hide');
        });
      }
    },

    // Note on package functionality

    getNoteOnPackage: function (element, noteId) {
      var packageId = $(element).attr('id');
      var packageNotesModal = $('#packageNotesModal');

      packageNotesModal.find('button.pull-right').each(function () {
        if (!$(this).hasClass('hidden')) {
          $(this).addClass('hidden');
        }
      });

      $.post(MAIN_URL + 'checkout/cart/getNotesOfPackage', {
        packageId: packageId,
        noteId: noteId
      }).done(function (response) {
        packageNotesModal.find('.modal-title').text('');
        packageNotesModal.find('.modal-body .modal-block').empty();

        var editedNoteId = false;
        packageNotesModal.find('.modal-title').text(Translator.translate('Note on') + ' ' + response['packageTitle']);
        if (response['notes']) {
          response['notes'].forEach(function (note) {
            editedNoteId = note['entity_id'];
            var noteDisplayed = (noteId !== 0) ? '' : '<div class="note-body">' + note['note'] + '</div>';
            packageNotesModal.find('.modal-body .modal-block').append('<div class="note-description"><div class="note-date">' + note['package_agent_status'] + '<span>' + note['agent_name'] + '</span>' + ' - ' + note['last_update_date'] + '</div>' + noteDisplayed + '</div><hr>');
            packageNotesModal.find('.modal-body .modal-block .note-date span').attr({
              'onmouseover': 'jQuery(this).tooltip("show")',
              'onmouseleave': 'jQuery(this).tooltip("hide")',
              'data-trigger': 'manual',
              'data-placement': 'bottom',
              'data-html': 'true',
              'title': '<p class="note-agent-name">' + note['agent_name'] + '</p>' +
                            '<p class="note-agent-email">' + note['agent_email'] + '</p>' +
                            '<p class="note-agent-phone">Phonenumber: ' + note['agent_phone'] + '</p>'
            });
          });
        }

        if (!noteId) {
          packageNotesModal.find('#addNoteOnPackage').addClass('hidden');
          $('.edit-note-btn').attr({'id': packageId, 'data-note-id': editedNoteId}).removeClass('hidden');
          if ($(element).hasClass('package-notes-checkout')) {
            $('.edit-note-btn').attr('onclick', 'window.customerDe.getNoteOnPackage(this,' + editedNoteId + ')');
          }
        } else {
          var noteForm = packageNotesModal.find('#addNoteOnPackage');
          var noteTextarea = packageNotesModal.find('textarea');
          packageNotesModal.find('hr').remove();
          noteForm.append('<hr>').removeClass('hidden');
          noteTextarea.val(response['notes'][0]['note']);
          noteTextarea.focus();
          packageNotesModal.find('#addNoteOnPackage .characters-left').text((150 - noteTextarea.val().length) + ' chars. remaining');
          $('.save-note-btn').removeClass('hidden');
        }

        packageNotesModal.find('.modal-body .modal-block').removeClass('hidden');
        packageNotesModal.modal('show');
      });
    },

    displayNoteAddForm: function (element) {
      var packageNotesModal = $('#packageNotesModal');
      packageNotesModal.find('textarea').val('');
      packageNotesModal.find('#addNoteOnPackage .characters-left').text('150 chars. remaining');
      packageNotesModal.find('button.pull-right').each(function () {
        if (!$(this).hasClass('hidden')) {
          $(this).addClass('hidden');
        }
      });
      $('.post-note-btn').removeClass('hidden');

      var packageId = $(element).attr('id');
      packageNotesModal.find('.post-note-btn')
        .attr('id', packageId);

      packageNotesModal.find('.modal-body .modal-block').addClass('hidden');
      packageNotesModal.find('#addNoteOnPackage').removeClass('hidden');
      packageNotesModal.modal('show');
    },

    postNoteOnPackage: function (element) {
      var packageId = $(element).attr('id');
      var note = $('#addNoteOnPackage').find('textarea').val();
      if (note.length) {
        $.post(MAIN_URL + 'checkout/cart/setNoteOnPackage', {
          packageId: packageId,
          note: note
        }).done(function (response) {
          $('#addNoteOnPackage').find('textarea').val('');

          $('.edit-note-btn').attr({'id': packageId, 'data-note-id': response['noteId']});

          var checkoutNoteBtn = $('.checkout-section-container.review-order').find('#' + packageId + '.package-notes-checkout');
          if (checkoutNoteBtn.length) {
            checkoutNoteBtn.removeClass('hidden');
          } else {
            var noteMarkerPackage = $('#' + packageId + '.package-notes-extended');
            if (noteMarkerPackage.hasClass('hidden')) {
              noteMarkerPackage.removeClass('hidden');
            }
            var noteMarkerPackageNotes = $('#' + packageId + '.package-notes');
            if (noteMarkerPackageNotes.hasClass('hidden')) {
              noteMarkerPackageNotes.removeClass('hidden');
            }
          }
          $('.mtm-' + packageId).addClass('hidden');
          $('.note-field-btn').removeClass('hidden');
          $('.post-note-btn').addClass('hidden');
        });
      } else {
        showModalError('Please enter your note!', 'Error', function () {
          $('#packageNotesModal').modal('show');
        });
      }

    },

    checkNoteCharactersLeft: function (element, limit, e) {
      var left = limit - ($(element).val().length);
      if (left <= 0) {
        $('#addNoteOnPackage').find('.characters-left').text((0) + ' chars. remaining');
        e.preventDefault();
      }
      if (left > 0) {
        $('#addNoteOnPackage').find('.characters-left').text((left) + ' chars. remaining');
      }
    },

    editNoteOnPackage: function () {
      var packageNotesModal = $('#packageNotesModal');
      var noteId = packageNotesModal.find('.edit-note-btn').attr('data-note-id');
      var updatedNote = packageNotesModal.find('textarea').val();

      $.post(MAIN_URL + 'checkout/cart/editNoteOnPackage', {
        noteId: noteId,
        note: updatedNote
      }).done(function (response) {
        if (response['total'] == 0) {
          var packageId = packageNotesModal.find('.edit-note-btn').attr('id');
          var noteMarkerPackageNotes = $('#' + packageId + '.package-notes');
          if (!noteMarkerPackageNotes.hasClass('hidden')) {
            noteMarkerPackageNotes.addClass('hidden');
          }
          var noteExtendedMarkerPackage = $('#' + packageId + '.package-notes-extended');
          if (!noteExtendedMarkerPackage.hasClass('hidden')) {
            noteExtendedMarkerPackage.addClass('hidden');
          }
          var noteCheckoutMarkerPackage = $('#' + packageId + '.package-notes-checkout');
          if (!noteCheckoutMarkerPackage.hasClass('hidden')) {
            noteCheckoutMarkerPackage.addClass('hidden');
          }

          // Showing 3 dots to add notes for the package if notes are removed completely
          $('.mtm-' + packageId).removeClass('hidden');
        }
        packageNotesModal.find('textarea').val('');
      });
    }

    // END note on package functionality
  });
})(jQuery);
