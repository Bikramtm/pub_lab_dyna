jQuery(document).ready(function () {
  /* @todo proper fix this for IE
         var select = jQuery('select')
         select.select2();
         select.on("select2:open", function (e) {
         var wrapper = jQuery(this).closest('.input-group-custom');
         if (wrapper.hasClass('validation-failed')) {
         wrapper.find('.validation-advice').fadeOut(100, function () {
         wrapper.removeClass('validation-failed');
         });
         }
         });
     */
  jQuery('#checkout-wrapper').checkoutBeautifier();
});


(function (jQuery) {
  jQuery.fn.checkoutBeautifier = function (options) {
    var self = this;
    var settings = jQuery.extend({}, options);

    function inputLabels(obj) {
      (jQuery(obj).val() ? jQuery(obj).addClass('input-dirty').parent().addClass('input-dirty') : jQuery(obj).removeClass('input-dirty').parent().removeClass('input-dirty'));
    }

    function accordion(obj) {
      jQuery('.accordion-head').on('click', obj, function (evt) {
        evt.stopImmediatePropagation();
        var item = jQuery(this).parent(),
          radio = jQuery(this).find('input[type="radio"]'),
          checkbox = jQuery(this).find('input[type="checkbox"]');
        if (radio.length > 0) {
          if (radio.prop('disabled')) {
            return false;
          } else if (radio.prop('checked')) {
            if( !item.hasClass('disabled') ) {
              // You should not be able to uncheck radio
              //item.removeClass('active');
              //radio.prop('checked', false).trigger("change");
            }
          } else {
            item.addClass('active').siblings().removeClass('active');
            radio.prop('checked', true).trigger('change');
          }
        }
        else if (checkbox.length > 0) {
          if (checkbox.prop('disabled')) {
            return false;
          } else if (checkbox.prop('checked')) {
            item.removeClass('active');
            checkbox.prop('checked', false).trigger('change');
          } else {
            item.addClass('active');
            checkbox.prop('checked', true).trigger('change');
          }
        }
        hideNestedAccordionsOfUnselectedChoices(this);
        return false;
      });

      jQuery('.checkbox-wrapper').on('click', obj, function(){
        initHeaderClick(this,'checkbox');
      });

      jQuery('.radio-wrapper').on('click', obj, function(evt){
        evt.stopImmediatePropagation();
        initHeaderClick(this,'radio');
      });

      jQuery('.radio-wrapper-fake').on('click', obj, function(evt){
        evt.stopImmediatePropagation();
        initHeaderClick(this,'checkbox', true);
        var that = this;
        var checkStatus = jQuery(obj).find('input[type="checkbox"]').prop('checked');
        jQuery('.radio-wrapper-fake').each(function() {
          if(jQuery(that).find('input[type="checkbox"]').attr('id') !== jQuery(this).find('input[type="checkbox"]').attr('id')) {
            initHeaderClickSynchronized(this,'checkbox', checkStatus);
          }
        });
      });
    }
    function hideNestedAccordionsOfUnselectedChoices(element) {
      jQuery(element).siblings('.accordion-body').find('.accordion').find('.form-control').removeAttr('disabled');
      jQuery(element).parent().siblings('.accordion-item').find('.accordion').find('.form-control').attr('disabled', true);
    }

    //emulates radio button behavior for checkbox inputs
    function initHeaderClickSynchronized(element, button, checkStatus) {
      var input = jQuery(element).find('input[type="'+button+'"]');
      if(checkStatus && input.prop('checked')) {
        input.prop('checked', false).trigger('change');
      }
    }
    function initHeaderClick(element, button, activate) {
      activate = typeof activate !== 'undefined' ? activate : false;
      var input = jQuery(element).find('input[type="'+button+'"]');
      if (input.prop('checked')) {
        input.prop('checked', false).trigger('change');
        if(activate) {
          jQuery(element).parent().removeClass('active');
        }
      } else {
        input.prop('checked', true).trigger('change');
        jQuery(element).parent().siblings().removeClass('active');
        if(activate) {
          jQuery(element).parent().addClass('active');
        }
      }
    }
    function init() {
      jQuery('input,textarea,select', self).each(function () {
        inputLabels(this);
      });
      jQuery('.accordion', self).each(function () {
        accordion(this);
        setupInitState(this);
      });
      attachEvents();

    }
    // Setup accordions to proper values on checkout init
    function setupInitState( obj ) {
      var inputs = jQuery(obj).find('.accordion-head input[type="radio"], .accordion-head input[type="checkbox"]');
      jQuery.each(inputs, function (key, item) {
        if( jQuery(item).prop('checked') ) {
          if( !jQuery(item).closest('.accordion-item').hasClass('active') ) {
            jQuery(item).closest('.accordion-item').addClass('active');
          }
        }
      })
    }
    function attachEvents() {
      jQuery('input, textarea, select', self).on('change', function () {
        inputLabels(this);
      });
      jQuery('input, textarea, select', self).on('focus', function () {
        var elem = jQuery(this);
        var wrapper = elem.closest('.input-group-custom');
        if( wrapper.hasClass('validation-failed') ) {
          var advice = wrapper.find('.validation-advice').get(0);
          if( advice ) {
            Validation.hideAdvice(this,advice);
          }
          //elem.removeClass('validation-failed');
          //wrapper.removeClass('validation-failed');
        }
      });
      jQuery('input, textarea, select', self).on('errorlabels:remove', function () {
        var elem = jQuery(this);
        var wrapper = elem.closest('.input-group-custom');
        elem.removeClass('validation-failed');
        wrapper.removeClass('validation-failed');
      });
    }

    init();
    return this;
  };
}(jQuery)
);
jQuery('body').on('DOMNodeInserted', '.validation-advice', function () {
  jQuery(this).closest('.input-group-custom').addClass('validation-failed');
});

jQuery(document).ready(function() {

  /// VFDE CUSTOM MODEL BINDING
  jQuery('[vfde-model]').on('change', function(){
    vfdeDirectiveSynchronize(jQuery(this));
  });
  jQuery('[vfde-model]').on('vfde-init', function(){
    vfdeDirectiveSynchronize(jQuery(this));
  });

  // SET INITIAL MODEL VALUES
  jQuery('[vfde-model]').each(function() {
    jQuery(this).trigger('vfde-init');
  });

});

function vfdeDirectiveSynchronize(element) {
  var modelName = element.attr('vfde-model');
  var value = element.val();
  if(element.attr('type') == 'checkbox' || element.attr('type') == 'radio') {
    value = element.is(':checked') ? element.val() : null;
  }
  jQuery('[vfde-show~="'+modelName+'"]').each(function() {

    var vfde_show = jQuery(this).attr('vfde-show');

    if(vfde_show.search('&&') != -1) {
      var splitValues =  vfde_show.split('&&');
      value = parseVfdeComparisonStatement(splitValues[0].trim());
      var secondValue = parseVfdeComparisonStatement(splitValues[1].trim());
      if(value && secondValue) {
        return jQuery(this).show();
      } else {
        return jQuery(this).hide();
      }
    }
    if(vfde_show.search('==') != -1) {
      var splitValues =  vfde_show.split('==');
      value = parseVfdeComparisonStatement(splitValues[0].trim());
      var valueComparedTo = parseVfdeComparisonStatement(splitValues[1].trim());
      if(value == valueComparedTo) {
        return jQuery(this).show();
      } else {
        return jQuery(this).hide();
      }
    }
    if(vfde_show.search('!=') != -1) {
      var splitValues =  vfde_show.split('!=');
      value = parseVfdeComparisonStatement(splitValues[0].trim());
      var valueComparedTo = parseVfdeComparisonStatement(splitValues[1].trim());
      if(value != valueComparedTo) {
        return jQuery(this).show();
      } else {
        return jQuery(this).hide();
      }
    }
    if(vfde_show == modelName) {
      if(value) {
        return jQuery(this).show();
      } else {
        return jQuery(this).hide();
      }
    }
    if(vfde_show == '!'+modelName) {
      if(!value) {
        return jQuery(this).show();
      } else {
        return jQuery(this).hide();
      }
    }
  });
}

function parseVfdeComparisonStatement (value) {
  if(value.search('!=') != -1) {
    var splitValues = value.split('!=');
    var secondValue = retriveVfdeModelValue(splitValues[0].trim());
    var thirdValue = retriveVfdeModelValue(splitValues[1].trim());
    return secondValue != thirdValue;
  }
  if(value.search('==') != -1) {
    var splitValues = value.split('==');
    var secondValue = retriveVfdeModelValue(splitValues[0].trim());
    var thirdValue = retriveVfdeModelValue(splitValues[1].trim());
    return secondValue == thirdValue;
  }
  return vfdeBooleanParsing(retriveVfdeModelValue(value));
}

function retriveVfdeModelValue(value) {
  if(jQuery('[vfde-model="'+value+'"]').attr('type') == 'checkbox') {
    if(jQuery('[vfde-model="'+value+'"]').is(':checked')) {
      return jQuery('[vfde-model="'+value+'"]').val()
    } else {
      return false;
    }
  }
  if(value.substring(0,1) == '!') {
    value = value.substring(1);
    if(jQuery('[vfde-model="'+value+'"]').length) {
      return !jQuery('[vfde-model="'+value+'"]').val();
    } else {
      return vfdeBooleanParsing(value);
    }
  }
  if(jQuery('[vfde-model="'+value+'"]').length) {
    return jQuery('[vfde-model="'+value+'"]').val();
  } else {
    return vfdeBooleanParsing(value);
  }
}

function vfdeBooleanParsing(value) {
  if(['true', 'false'].indexOf(value) != -1) {
    return Boolean(value);
  }
  return value;
}

// Hiding validation error on focus for divs which are hidden
jQuery('body').on('focusin', 'input, textarea, select', function(event){
  var elem = jQuery(this);
  var wrapper = elem.closest('.input-group-custom');
  if (wrapper.hasClass('validation-failed')) {
    var advice = wrapper.find('.validation-advice').get(0);
    if (advice) {
      Validation.hideAdvice(this, advice);
    }
    elem.removeClass('validation-failed');
    wrapper.removeClass('validation-failed');
  }
});

Vue.config.devtools = true;
Vue.config.debug = true;
Vue.config.silent = false;

// Vue.use(VueMaterial);

Vue.use(VTooltip.VTooltip);

Vue.component('v-select', VueSelect.VueSelect);
Vue.directive('my-tooltip', VTooltip.VTooltip);

var vueConfig = {
  items: [
    {text: 'name1'},
    {text: 'name2'},
    {text: 'name3'}
  ],
  billing: [
    {text: 'name1'},
    {text: 'name2'},
    {text: 'name3'}
  ],
  boxes: [{show: false}, {show: true}, {show: false}],
  radio1: false,
  radio2: false,
  radio3: false,
  radio4: false,
  radio5: false,
  e1: null,
  other_identity: false,
  contact_person: false,
  status_sms: false,
  status_email: false,
  billing_current_address: false,
  billing_other_address: false,
  billing_po_box: false,
  billing_email: false,
  billing_invoice: false,
  hidden1: false,
  country: null,
  msg: 'This is a button.'
}

//if( typeof vueCheckoutContainers != "undefined" ) {
//    jQueryj.extend( vueConfig, vueCheckoutContainers );
//}

//console.log( vueCheckoutContainers );
//console.log( vueConfig );

new Vue({
  el: '#checkout-section-first',
  data: vueConfig
});

if (typeof vueCheckoutContainers !== "undefined") {
  new Vue({
    el: '#checkout-section-second',
    data: vueCheckoutContainers
  });

  new Vue({
    el: '#checkout-section-third',
    data: vueCheckoutContainers
  });

  new Vue({
    el: '#checkout-section-fourth',
    data: vueCheckoutContainers
  });

  new Vue({
    el: '#checkout-section-connection',
    data: vueCheckoutContainers
  });
}



