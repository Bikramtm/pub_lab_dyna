'use strict';

(function ($) {
  window.Bundle = function () {};

  window.Bundle.prototype = {
    // stores current bundle id
    bundleId: null,
    /** @var {Boolean} **/
    susoBundle: false,
    /** @var {Boolean} **/
    redPlusBundle: false,
    // stores package ids that are linked into a bundle
    packageIds: [],

    /**
         * Adds bundle data to this bundle
         * @param bundleData {JSON}
         * @returns {Bundle}
         */
    addBundleData: function (bundleData) {
      $.extend(this, bundleData);

      return this;
    },
    /**
         * Returns whether or not current bundle is Red+
         * @returns {Boolean}
         */
    isRedPlus: function ()
    {
      return this.susoBundle;
    },
    /**
         * Returns whether or not current bundle is SurfSohort
         * @returns {Boolean}
         */
    isSuso: function()
    {
      return this.susoBundle;
    }
  };
})(jQuery);
