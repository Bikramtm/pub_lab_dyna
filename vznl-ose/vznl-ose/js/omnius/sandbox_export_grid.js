/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

jQuery(document).ready(function () {
  setInterval(function () {
    jQuery.ajax({
      type: 'POST',
      url: checkExportsUrl,
      data: {form_key: form_key_checkExports}
    })
      .done(function (data) {
        var grid = jQuery(data).find('.grid').find('#exportGrid_table').find('tbody').html();
        jQuery('[id="page:main-container"]').find('#exportGrid').find('.grid').find('#exportGrid_table').find('tbody').html(grid);
        jQuery('form#exportGrid_massaction-form').prepend('<input type="hidden" name="form_key" value="' + FORM_KEY + '"/>');
        exportGridJsObject.initGridAjax();
      });
  }, 5000);
});
