'use strict';

(function ($) {
    window.Address = function () {
        this.initialize.apply(this, arguments);
    }; 
    window.Address.prototype = $.extend(VEngine.prototype, {
        serviceAbilityContainer: '#serviceability-content',
        serviceAddressModalTrigger: '#address-check-container',
        serviceAbilityMoreInfoContainer: '#serviceability-more-info',
        defaultCampaign: '#default-campaign',
        serviceAbilityUnknownModal: '#serviceabilityUnknownModal',
        serviceAbilityMultipleResultsModal: '#serviceabilityMultipleResultsModal',
        addressCheckModal: '#addressCheckModal',
        moveServiceAddressCheckModal: '#moveServiceAddressCheckModal',
        customerAddressSearchContainer: '#customer-address-search',
        genericConfirmationModal: '#generic-confirmation-modal',
        customerUctParams: false,
        removeLoaders: [],
        removeshowmore: [],
        /** Callback used for init configurator - can be used in other scopes */
        callBack: null,
        callBackParam: null,
        callBackObject: null,
        /** Services object **/
        services: null,
        waiting: false,
        fullAddress: null,
        addressSearchKeywords: {},
        addressSearchResults: {},
        addressId: null,
        restrictedCountries : null,
        hasGoogleAutocomplete: null,
        /** Flag used to determine whether the checkserviceability form should be cleared based on certain events */
        clearFormData: false,
        showCheckModal: function (callBack, callBackParam, callBackObject) {
            var self = this;
            var addressCheckModal = this.addressCheckModal;
            /** Register callBack call */
            this.callBack = callBack;
            this.callBackParam = callBackParam;
            this.callBackObject = callBackObject;

            if (callBackParam['moveOffnetScenario']) {
                addressCheckModal = this.moveServiceAddressCheckModal;
            }
            /** Clear callBack params on window close */
            $(addressCheckModal).on('hidden.bs.modal', function () {
                self.callBack = null;
                self.callBackParam = null;
                self.callBackObject = null;
                $('#cart-spinner').removeClass('hide').addClass('hide');
            });
            $(addressCheckModal).on('show.bs.modal', function () {
                if (HAS_GOOGLE_AUTOCOMPLETE && !window.serviceabilityGoogleModal){
                    window.serviceabilityGoogleModal = $(addressCheckModal).googleSearchAddress({
                        streetInputName: "street",
                        cityInputName: "city",
                        houseNumberInputName: "houseNo",
                        houseNumberAddition: "addition",
                        zipCodeInputName: "postcode"
                    });
                }
            });

            if (HAS_GOOGLE_AUTOCOMPLETE) {
                $(addressCheckModal).modal({backdrop: 'static'});
            } else {
                $(addressCheckModal).modal("show");
            }
        },
        showServiceabilityUnknownModal : function () {
            $(this.serviceAbilityUnknownModal).modal();
        },
        removeTopAddressFailedValidation: function() {
            jQuery('.top-address-validations').each(function (i, el) {
                jQuery(el).removeClass('validation-failed');
                jQuery(el).next('p.message-invalid-address').remove();
            });
        },
        addTopAddressFailedValidation: function() {
            var self = this;
            self.removeTopAddressFailedValidation();
            jQuery('.top-address-validations').each(function (i, el) {
                jQuery(el).addClass("validation-failed")
                    .after('<p class="message-invalid-address">{0}</p>'.format(Translator.translate('Invalid address')));
            });
        },
        checkServiceabilityHeader: function(formId) {
            $('.address-check span.loading-search').removeClass('invisible');
            var self = this;
            var formData = $(formId).serializeObject();
            formData['type'] = 'service_address';
            var addressStreetNode = jQuery('[id="topBar.addressCheck.street"]');
            var serviceabilityModal = $('#serviceabilityMultipleResultsModal');
            var serviceabilityAddresses = serviceabilityModal.find('.ajax-results-addresses:first');
            var addressCheckForm = new VarienForm('topaddressCheckForm');

            if (addressCheckForm.validator.validate() == false) {
                return;
            }

            var callback = function (results) {
                if (results.error) {
                    if(results.message == Translator.translate('Invalid address')){
                        self.addTopAddressFailedValidation();
                        showModalError(results.message);
                    } else {
                        showModalError(results.message);
                        // remove loader and clear form
                        $('.address-check span.loading-search').addClass('invisible');
                        $(formId).find('input').each(function(){
                            $(this).val('');
                        });
                    }
                } else {
                    if (results.addresses) {
                        if (results.addresses.length == 1) {
                            var singleAddress = $('<div></div>').attr('data-id',results.addresses[0].ID)
                                .attr('data-street',results.addresses[0].StreetName)
                                .attr('data-building-nr',results.addresses[0].BuildingNumber)
                                .attr('data-postalcode',results.addresses[0].PostalZone)
                                .attr('data-house-addition',results.addresses[0].HouseNumberAddition)
                                .attr('data-city-name',results.addresses[0].CityName);
                            self.checkService(singleAddress);
                        } else {
                            var template = Handlebars.compile($("#HT-serviceabilityMultipleResults").html()),
                                responseHTML = template(results);

                            serviceabilityAddresses.html(responseHTML);
                            serviceabilityModal.modal('show');
                        }
                    }
                }
            };

            if(addressStreetNode.hasClass('validation-failed')){
                addressStreetNode.removeClass('validation-failed');
            }

            formData['loader'] = false; // disable loader
            this.ajax('POST', 'address.validate', formData, callback);
        },
        validate: function (formId, button) {
            var self = this;
            var $_container = $(button).parents('.ajax-container:first');
            var $_message = $_container.find('.ajax-results-message:first');
            var $_addresses = $_container.find('.ajax-results-addresses:first');

            this.clearMoveServiceAddressCheckModalResults();

            var formData = $(formId).serializeObject();
            var addressCheckForm = new VarienForm(formId.replace('#',''));
            if (addressCheckForm.validator.validate() == false) {
                return;
            }

            $(button).addClass('active');

            var callback = function (results) {
                // Make address check inputs enabled when request is done
                if ($(button).closest('.modal').attr('id') == self.moveServiceAddressCheckModal.replace('#','')) {
                    $(self.moveServiceAddressCheckModal).find('.address-check-input').removeClass('disabled').prop('disabled', false);
                }
                $(button).removeClass('active');
                if (results.error) {
                    showModalError(results.message);
                } else {
                    $_message.text(results.message);

                    if (results.addresses) {
                        if (results.addresses.hasOwnProperty('BuildingNumber')) {
                            results.addresses = [results.addresses];
                        }
                        var template = Handlebars.compile($("#HT-Check-Address").html()),
                            responseHTML = template(results);
                        $_addresses.html(responseHTML);
                    }
                }
            };

            formData['loader'] = false; // disable loader

            // Make address check inputs disabled while request is executed
            if ($(button).closest('.modal').attr('id') == this.moveServiceAddressCheckModal.replace('#','')) {
                $(this.moveServiceAddressCheckModal).find('.address-check-input').addClass('disabled').prop('disabled', true);
            }
            this.ajax('POST', 'address.validate', formData, callback);
        },
        /** Start serviceability check for current address id */
        checkService: function (elem) {
            elem = $(elem);
            var self = this;
            var $_message = $(elem).closest('.modal').find('.ajax-results-message:first');
            var $_addresses = $(elem).closest('.modal').find('.ajax-results-addresses:first');
            // Callback is required when starting a serviceability needed package before a serviceability check
            // These are set from this.showCheckModal
            var callBack = self.callBack;
            var callBackParam = self.callBackParam;
            var callBackObject = self.callBackObject;
            // Hiding modal window. Note that there is an event attached to hide that unsets callBack, callBackParam and callBackObject
            if ($(elem).closest('.modal').attr('id') == this.addressCheckModal.replace('#','')) {
                $(elem).closest('.modal').modal('hide');
            }

            $(self.serviceAbilityMultipleResultsModal).modal("hide");

            var confirmation = Number(elem.attr('data-clear-cart-confirmed') != undefined);
            // Setting request params
            var requestData = {
                'addressId':  elem.attr('data-id'),
                'street': elem.attr('data-street'),
                'buildingNr': elem.attr('data-building-nr'),
                'postalcode': elem.attr('data-postalcode'),
                'city': elem.attr('data-city-name'),
                'houseAddition': elem.attr('data-house-addition'),
                'isCustomer': elem.attr('data-is-customer'),
                'myProductsCheck':elem.attr('data-my-products-check'),
                'customerNumber':elem.attr('data-customer-number'),
                'component-ctn':elem.attr('data-component-ctn'),
                'tasi':elem.attr('data-tasi'),
                'clearCartConfirmed': confirmation,
                'sourcePackageCreationType':elem.attr('data-package-creation-type')
            };
            self.waiting = true;
            $(document).trigger("serviceability-check-started", []);
            $('.address-check span.loading-search').removeClass('invisible');
            $.ajax({
                url: '/address/index/checkServices/',
                type: "post",
                data: requestData,
                dataType: "json",
                loader: false,
                cache: false,
                success: function (response) {
                    if (response.error == false) {
                        if ($(elem).closest('.modal').attr('id') == self.moveServiceAddressCheckModal.replace('#','')) {
                            $(self.moveServiceAddressCheckModal)
                                .find('#suggestionContainer .suggestion-street')
                                .html(elem.attr('data-street'));
                            $(self.moveServiceAddressCheckModal)
                                .find('#suggestionContainer .suggestion-houseno')
                                .html(elem.attr('data-building-nr'));
                            $(self.moveServiceAddressCheckModal)
                                .find('#suggestionContainer .suggestion-addition')
                                .html(elem.attr('data-house-addition'));
                            $(self.moveServiceAddressCheckModal)
                                .find('#suggestionContainer .suggestion-postalcode')
                                .html(elem.attr('data-postalcode'));
                            $(self.moveServiceAddressCheckModal)
                                .find('#suggestionContainer .suggestion-city')
                                .html(elem.attr('data-city-name'));
                            $(this.moveServiceAddressCheckModal)
                                .find('#suggestionContainer .package-type-bandwidth span')
                                .val($('#serviceability-content [data-technology="(V)DSL"]').data('downstream'));

                            self.resetMoveServiceAddressCheckModal();
                        }

                        if (response.requireConfirmation) {
                            $(self.genericConfirmationModal).find('.confirmation-message').html(response.message);
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-id", elem.attr('data-id'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-street", elem.attr('data-street'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-building-nr", elem.attr('data-building-nr'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-postalcode", elem.attr('data-postalcode'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-city-name", elem.attr('data-city-name'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-house-addition", elem.attr('data-house-addition'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-is-customer", elem.attr('data-is-customer'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-my-products-check", elem.attr('data-my-products-check'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-customer-number", elem.attr('data-customer-number'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-component-ctn", elem.attr('data-component-ctn'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-tasi", elem.attr('data-tasi'));
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-clear-cart-confirmed", true);
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("data-dismiss", "modal");
                            $(self.genericConfirmationModal).find('.confirm-action-button').attr("onclick", "address.checkService(this);");
                            $(self.genericConfirmationModal).modal("show");
                        }
                        else {
                            if(response.clearCart) {
                                showModalError(response.clearCartMessage, 'Warning');
                                if (response.clearCartMessage && response.clearCart && customerSection.migrationOrMoveStarted){
                                    customerSection.migrationOrMoveStarted = false;
                                }

                                var packagesAdded = $('.cart_packages .side-cart[data-package-id]');
                                var packageId = false;

                                //$('#show-packages').addClass('hide');
                                $('.text-center.no-package-zone').removeClass('hide');
                                if(packagesAdded.length) {
                                    $.each(packagesAdded, function(key, pack) {
                                        var packageId = parseInt($(pack).data('package-id'));
                                        if (response.removedPackageIds && response.removedPackageIds.indexOf(packageId) != -1) {
                                            $(pack).remove();
                                        }
                                    });

                                    $('#package-types')
                                        .find(".btn-block[data-package-creation-type-id!='" + REDPLUS_CREATION_TYPE_ID + "']")
                                        .removeClass('show-clear-cart-warning');

                                }
                            }

                            self.services = response;
                            self.city = elem.attr('data-city-name');
                            self.postcode = elem.attr('data-postalcode');
                            self.fullAddress = elem.attr('data-street')+ ' '+ elem.attr('data-building-nr')+ ' '+ elem.attr('data-house-addition') +
                                elem.attr('data-postalcode') + ' '+ elem.attr('data-postalcode')+ ' '+  elem.attr('data-city-name');
                            if (self.customerUctParams == true) {
                                self.getCallerCampaignData(false, 1);
                            }

                            if(response.hasOwnProperty('cartTotals') && response.cartTotals){
                                /* update side cart totals */
                                var totals = $('.sidebar #cart_totals');
                                totals.replaceWith(response.cartTotals);
                                if(typeof window.configurator !== 'undefined'){
                                    window.configurator.destroy();
                                    window.configurator.closeBundleDetailsModal();
                                }
                                if(typeof window.sidebar !== 'undefined'){
                                    //toggle Gesamt section
                                    sidebar.updatePrices();
                                    sidebar.updatePrices();
                                }
                            }
                            if(response.hasOwnProperty('rightBlock') && response.rightBlock){
                                jQuery('.cart_packages').replaceWith(response.rightBlock);
                            }
                        }
                    } else {
                        showModalError(response.message);
                    }
                },
                complete: function(response) {
                    if(response && response.hasOwnProperty('responseJSON') && response.responseJSON.hasOwnProperty('requireConfirmation') &&  response.responseJSON.requireConfirmation) {
                        setPageLoadingState(false);
                    }
                    else {
                        self.notifyServiceAbilityChanged();
                        if (callBack) {
                            callBack.apply(callBackObject, [callBackParam]);
                        }
                        self.showServiceBar();
                    }

                    $('.address-check span.loading-search').addClass('invisible');
                }
            });
        },
        handleMoveServiceAddressCheckButton: function() {
            var enableButton = true;
            $(this.moveServiceAddressCheckModal).find('.address-check-input.required-entry').each(function(){
                if (!$(this).val().replace(/\s/g, '').length) {
                    enableButton = false;
                    return false;
                }
            });
            if (enableButton) {
                $(this.moveServiceAddressCheckModal).find('.modal-footer .check-button').removeClass('disabled').prop('disabled', false);
            } else {
                $(this.moveServiceAddressCheckModal).find('.modal-footer .check-button').addClass('disabled').prop('disabled', true);
            }
        },
        resetMoveServiceAddressCheckModal: function() {
            this.clearMoveServiceAddressCheckModalResults();
            $(this.moveServiceAddressCheckModal).find('#suggestionContainer').addClass('hidden');
            $(this.moveServiceAddressCheckModal).find('#notificationContainer').addClass('hidden');
            $(this.moveServiceAddressCheckModal).find('.address-check-input').val('');

            if (jQuery('.btn-move-service.selected').hasClass('selected')) {
                customerSection.disableCableToFixedMoveOffnet(jQuery('.btn-move-service.selected'));
            }

            if (customerSection.migrationOrMoveStarted) {
                customerSection.cancelMoveOffnet();
            }

            this.handleMoveServiceAddressCheckButton();
        },
        clearMoveServiceAddressCheckModalResults: function() {
            $(this.moveServiceAddressCheckModal).find('.ajax-results-message:first').empty().removeClass('error');
            $(this.moveServiceAddressCheckModal).find('.ajax-results-addresses:first').empty();
        },
        getCallerCampaignData: function(param, page) {
            var self = this;
            setPageLoadingState(true);
            var requestData = {
                'page': page
            };
            $.ajax({
                type: 'POST',
                data: requestData,
                dataType: "json",
                url: '/customerde/details/getCallerCampaignData',
                success: function(response) {
                    setPageLoadingState(false);
                    if (response.success) {
                        if (!param && "has_campaign" in response && response.has_campaign == true) {
                            reloadQuote(response.quote_id,0);
                        } else if ("callerData" in response && "campaignData" in response) {
                            if(response.salesIds){
                                var salesIds = {};
                                salesIds['data'] = response.salesIds;
                                if (response.salesIds.error) {
                                    salesIds = response.salesIds;
                                }
                                window.getVoids(salesIds);
                            }
                            if ("campaignData" in response.campaignData && "campaign_id" in response.campaignData.campaignData) {
                                $('#titleCampaignId').html(response.campaignData.campaignData.campaign_id);
                            }
                            response.campaignData.campaigns.each(function(campaign){
                                if(campaign.identifier) {
                                    self.removeshowmore.push(campaign.identifier);
                                }
                                if (!campaign) {
                                    return true;
                                } else if (response.campaignData.serviceability != 1) {
                                    self.removeLoaders.push(campaign.identifier);
                                    return true;
                                }
                                var initConfiguratorData = campaign.initConfiguratorData;
                                var getRules = {
                                    'products': initConfiguratorData.currentProducts,
                                    'type': initConfiguratorData.type,
                                    'offer': campaign.identifier
                                };
                                if (initConfiguratorData.hasInvalidProducts == 0) {
                                    $.ajax({
                                        type: 'POST',
                                        data: getRules,
                                        dataType: "json",
                                        identifier: campaign.identifier,
                                        url: 'configurator/cart/getCompatibility',
                                        success: function(compResponse) {
                                            var element = $('.campaign-row.col-xs-12').find('[data-campaign=' + this.identifier + ']');
                                            if (compResponse.compatible) {
                                                var campaignButton = element.find('.buy-campaign-button');
                                                if (!campaignButton.hasClass('status-disabled') && !campaignButton.hasClass('invalid-products-disabled')
                                                    && !campaignButton.hasClass('disabled-by-permission') && !campaignButton.hasClass('disabled-by-open-order')) {
                                                    element.find('.content-block').removeClass('disabled');
                                                    campaignButton.removeClass('disabled');
                                                    element.find('.buy-campaign-button .campaign-buy-btn').attr('disabled', false);
                                                }
                                            }
                                            element.find('.loading-compatibility').hide();
                                        }
                                    });
                                } else {
                                    var element = $('.campaign-row.col-xs-12').find('[data-campaign=' + campaign.identifier + ']');
                                    element.find('.loading-compatibility').hide();
                                }
                            });
                            var callerContainer = $j("#homepage-wrapper .caller-data");
                            var template = Handlebars.compile($j("#Handlebars-Template-Customer-Caller-Data").html()),
                                callerHTML = template(response.campaignData);
                            callerContainer.html(callerHTML);

                            var campaignsContainer = $j("#homepage-wrapper #campaigns");
                            var template = Handlebars.compile($j("#Handlebars-Template-Customer-Campaigns").html()),
                                campaignsHTML = template(response.campaignData);
                            campaignsContainer.html(campaignsHTML);
                        }
                    } else {
                       showModalError(response.message);
                    }
                },
                complete: function() {
                    self.removeLoaders.each(function (i, el) {
                        $('.campaign-row.col-xs-12').find('[data-campaign=' + i + ']').find('.loading-compatibility').hide();
                    });
                    self.removeLoaders = [];

                    self.removeshowmore.each(function (i, el) {
                        var uspsmdiv='#uspsmdiv_'+i;
                        var campaignusp = $('.campaign-row.col-xs-12').find('[data-campaign=' + i + ']').find('.campofferblock');

                        if(campaignusp['0'].scrollHeight>170) {
                            $(uspsmdiv).css({"display":"block"});
                        } else {
                            $(uspsmdiv).css({"display":"none"});
                        }
                    });
                    self.removeshowmore = [];
                }
            });
        },


        searchCampaign: function(data) {
            var self = this;
            jQuery.ajax({
                url: '/customerde/details/getDefaultCampaignData/',
                type: "post",
                data: {
                    search: data
                },
                dataType: "json",
                success: function (response) {
                    self.parseCampaignResults(response);
                    setPageLoadingState(false);
                }
            });
        },

        parseCampaignResults: function(data) {
            var searchList = jQuery('ul.search-campaign-list');
            var li_entry = null;
            searchList.html('');

            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    var toolTipText='';
                    toolTipText += '<p class="campaign-tooltipheader">' + Translator.translate("Campaign Id") +':</p>' +
                        '<p class="campaign-tooltiptext">' + data[i]["campaign_id"] + '</p>' +
                        '<p class="campaign-tooltipheader">' + Translator.translate("Cluster") + ':</p>' +
                        '<p class="campaign-tooltiptext">' + data[i]["cluster"] + '</p>' +
                        '<p class="campaign-tooltipheader">' + Translator.translate("Campaign Sapn") + ' :</p>' +
                        '<p class="campaign-tooltiptext">' + data[i]["timeFrom"] + " - " + data[i]["timeTo"] + '</p>';
                    toolTipText += '<p class="campaign-tooltipheader">' + Translator.translate("Campaign Promotion") + ':</p>';

                    if (data[i]["promotion_hint1"]) {
                        toolTipText += '<p class="campaign-tooltiptext">' + data[i]["promotion_hint1"] + '</p>';
                    }
                    if (data[i]["promotion_hint2"]) {
                        toolTipText += '<p class="campaign-tooltiptext">' + data[i]["promotion_hint2"] + '</p>';
                    }
                    if (data[i]["promotion_hint3"]) {
                        toolTipText += '<p class="campaign-tooltiptext">' + data[i]["promotion_hint3"] + '</p>';
                    }

                    toolTipText += '<p class="campaign-tooltipheader">' + Translator.translate("Offers") + ':</p>';

                    li_entry = "<li class='campli'>" +
                        "<div class='defaultdiv1'>" +
                            "<div class='campdefaulttitle'>" +
                                data[i]['campaign_title'] +
                                "<span class='defaultcampaign-info' data-toggle='tooltip' data-html='true' data-container='body' data-placement='right service-description' data-title='" + toolTipText + "' >&nbsp;</span>" +
                            "</div>" +
                            "<p class='campdefaultsubtitle'>" + data[i]['campaign_hint'] + "</p>" +
                        "</div>" +
                        "<div class='defaultdiv2'>";

                    if (data[i]['campaign_id'] == data[i]['selectedCampaignId']) {
                        li_entry += "<input type='button' value='Selected' class='campaign-buy-btn btn campbtn-infoselected col-sm-10' >";
                    } else {
                        li_entry += "<input type='button' value='Select' class='campaign-buy-btn btn campbtn-infoselect col-sm-10' onclick='address.changeCampaignId(\""+data[i]["campaign_id"]+"\")'>" ;
                    }
                    li_entry += "</div>" +
                        "<div class='clearboth ptm'></div>" +
                        "<div class='defcampline'></div>" +
                        "<div class='clearboth ptm'></div>" +
                        "</li>";
                    searchList.append(li_entry);
                }
            } else {
                li_entry =
                    "<li class='campli'>" +
                        "<div class='campdefaulttitle'>" + Translator.translate("No Default Campaign Found") +
                    "</div></div></li>";
                searchList.append(li_entry);
            }
        },

        changeCampaignId: function(campaign_id) {
            var self = this;
            jQuery.ajax({
                url: '/customerde/details/setDefaultCampaignData/',
                type: "post",
                data: {
                    campaign_id: campaign_id
                },
                dataType: "json",
                success: function (response) {
                    self.parseCampaignResults(response);
                    setPageLoadingState(false);
                    self.reloadCampaignPage();
                }
            });
        },

        reloadCampaignPage: function() {
            window.address.closeCampaignModal();
            window.address.getCallerCampaignData(true, 1);
            setPageLoadingState(false);
            return false;
        },
        /**
        * Function to check the default campaign are there and returns the data list
        */
        checkDefaultCampaign: function () {
            jQuery(this.defaultCampaign).modal();
            var searchData = jQuery('#search_campaign').val();
            var self = this;
                jQuery.ajax({
                    url: '/customerde/details/getDefaultCampaignData/',
                    type: "post",
                    data: {
                        search: searchData
                    },
                    dataType: "json",
                    success: function (response) {
                        self.parseCampaignResults(response);
                        setPageLoadingState(false);
                    }
                });
        },

        closeCampaignModal: function () {
            $(this.defaultCampaign).modal("hide");
        },

        campaignUspShow: function (identifier, show) {
            var usid = 'uspvalue_' + identifier;
            var uspshowmorediv = 'uspshowmore_' + identifier;
            var uspshowlessdiv = 'uspshowless_' + identifier;
            var uspmaindiv = 'uspblock_' + identifier;
            var usptext = document.getElementById(usid).value;

            if (show == 'SM') {
                document.getElementById(uspmaindiv).classList.add('campofferblockov');
                document.getElementById(uspmaindiv).classList.remove('campofferblock');

                document.getElementById(usid).value = 'SL';
                document.getElementById(uspshowmorediv).style.display = "none";
                document.getElementById(uspshowmorediv).style.display = "none";
                document.getElementById(uspshowlessdiv).style.display = "block";
            }
            else {
                document.getElementById(uspmaindiv).classList.remove('campofferblockov');
                document.getElementById(uspmaindiv).classList.add('campofferblock');

                document.getElementById(usid).value = 'SM';
                document.getElementById(uspshowmorediv).style.display = "block";
                document.getElementById(uspshowlessdiv).style.display = "none";
            }
        },

        customerAddressSearch: function (element)  {
            window.checkout.searchButtonId = element.attr('id');
            var idPrefix = window.checkout.searchButtonId.replace('[search]', '');

            var addressSearchData = {
                city: $('[id="' + idPrefix + '[city]"]').val(),
                postcode: $('[id="' + idPrefix + '[postcode]"]').val(),
                street: $('[id="' + idPrefix + '[street]"]').val()
            };

            this.addressSearchRequest(addressSearchData);
        },
        addressSearchRequest: function (data) {
            var self = this;

            $.ajax({
                url: '/address/index/addressSearch/',
                type: "post",
                data: data,
                dataType: "json",
                cache: false,
                success: function (response) {
                    if (response.error == false) {
                        self.addressSearchResults = response;
                        self.showCustomerAddressSearch();
                        self.executeCustomerAddressSearch();
                    } else {
                        showModalError(response.message);
                    }
                },
            });
        },
        showServiceBar: function() {
            var template = Handlebars.compile($("#Address-ServiceAbility-Short").html()), responseHTML = template(this.services);
            var templateMore = Handlebars.compile($("#Address-ServiceAbility-More-Info").html()), responseMoreHTML = templateMore(this.services);
            $(this.serviceAbilityContainer).find('#topaddressCheckForm').hide();
            $(this.serviceAbilityContainer).html("").hide();
            $('#topaddressCheckForm').hide();
            $(this.serviceAbilityContainer).append(responseHTML).show();
            $(this.serviceAbilityMoreInfoContainer).find('.modal-body').html(responseMoreHTML);

            // Hide address check trigger (clear address button has been added under the additional info button from serviceability bar which will re-show the trigger)
            $(this.serviceAddressModalTrigger).hide();

            // Initially hide all components and open first one to start
            $('.service-additional-info').hide();
            $('.service-additional-info:first').show();

            // Actions to additional info tabs
            $('.service-additional-title').click(function () {
                $('.service-additional-title').next('.service-additional-info').hide();
                $(this).next('.service-additional-info').show();
            });

            // Reindex document tooltips
            jQuery('[data-toggle="tooltip"]').tooltip();
        },
        checkServiceMoreInfo: function () {
            $(this.serviceAbilityMoreInfoContainer).modal();
        },
        showCustomerAddressSearch: function () {
            var template = Handlebars.compile($('#Customer-Address-Search-Template').html());
            var responseHTML = template(this.addressSearchResults);

            $(this.customerAddressSearchContainer).find('.modal-body').html(responseHTML);
        },
        executeCustomerAddressSearch: function () {
            $(this.customerAddressSearchContainer).modal();
        },
        resetServices: function() {
            var self = this;

            self.addressId = null;
            self.services = null;
            self.city = null;
            self.postcode = null;
            self.fullAddress = null;
            $(self.serviceAbilityContainer).html("").hide();

            // Hide confirmation modal - if any
            $(self.genericConfirmationModal).modal("hide");

            // Show new address search trigger
            $(self.serviceAddressModalTrigger).show();

            //Show the address check form
            self.showTopAddressCheckForm();
        },
        clearServices: function (confirmation) {
            var self = this;
            confirmation = Number(confirmation != undefined);
            self.waiting = true;
            $(document).trigger("serviceability-check-started", []);
            $.ajax({
                url: '/address/index/clearServices/',
                type: "post",
                data: ({
                    'address_id': '',
                    'confirmation': confirmation
                }),
                dataType: "json",
                cache: false,
                success: function (response) {
                    if (response.error == false) {
                        self.resetServices();

                        setPageLoadingState(true);
                        if(response['packages']){
                            // Remove packages which need new service address
                            response['packages'].each(function(packageDetails){
                                self.handleRemovedPackage(packageDetails);
                            });
                        }
                    } else if (response.requireConfirmation) {
                        $(self.genericConfirmationModal).find('.confirmation-message').html(response.message);
                        $(self.genericConfirmationModal).modal("show");
                        $(self.genericConfirmationModal).find('.confirm-action-button').attr("onclick", "address.clearServices(true)");
                    } else {
                        showModalError(response.message);
                    }
                },
                complete: function(response) {
                    setPageLoadingState(false);
                    if (self.customerUctParams == true) {
                        self.getCallerCampaignData(false, 1);
                    }
                    self.notifyServiceAbilityChanged();
                    if(response.responseJSON.error == false && response.responseJSON.triggerRefresh === true){
                        setTimeout(function () {
                            window.location.reload();
                        }, 500);
                    }
                    if ($('.enlargeSearch').hasClass('left-direction')) {
                        $('.enlargeSearch').trigger('click');
                    }
                }
            });
        },
        handleRemovedPackage: function (data) {
            var sideBlock = $(".side-cart[data-package-id='"+data['packageId']+"']");
            var checkoutButton = $('#configurator_checkout_btn');
            var classList = checkoutButton.attr('class');
            var hasWarningClass = classList.indexOf("btn-warning");
            var oldDisabled = checkoutButton.prop('disabled');
            var bundleId = sideBlock.data('bundle-id');
            var aDeleteLink = $(sideBlock).find('.icons.delete a');
            aDeleteLink.removeAttr('onclick');

            if(data.mustRemoveBundleChoice) {
                $('#bundle-options').remove()
            }

            if(data.complete) {
                $('#configurator_checkout_btn').show();
            } else {
                if ($(aDeleteLink).parents('.side-cart').length == 0) {
                    $('#configurator_checkout_btn').hide();
                }
            }

            if (data.hasOwnProperty('showOneOfDealPopup') && data.showOneOfDealPopup) {
                $('#one-of-deal-modal').data('packageId', blockPackageId).modal();
                return;
            }
            if (sideBlock.hasClass('active') && window['configurator']) {
                window['configurator'].destroy();
            }

            sideBlock.remove();
            // Remove bundle hints
            if(typeof configurator != 'undefined') {
                configurator.closeBundleDetailsModal();
            }

            if (data) {
                var totals = $('.sidebar #cart_totals');
                if (data.totals && !oldDisabled) {
                    data.totals = data.totals.replace('disabled="disabled" ', '');
                }
                if (data.totals && hasWarningClass && hasWarningClass != -1) {
                    data.totals = data.totals.replace('btn-info ', 'btn-warning ');
                }
                if(data.hasOwnProperty('totals')) totals.replaceWith(data.totals);
            }

            // Remove any dummy bundle packages if exist
            var packages = $('.col-right .cart_packages .side-cart.block');
            $.each(packages, function(index, value) {
                // If removed package belongs to a bundle id, remove all bundle packages
                if (bundleId && bundleId === $(value).data('bundle-id')) {
                    $(value).remove();
                }
            });

            // Reset packages indexes in right sidebar
            if (data.hasOwnProperty('packagesIndex') &&
                data['packagesIndex'] != null &&
                !$.isEmptyObject(data['packagesIndex'])
            ) {
                // Reassigning packages variables because it might be altered by dummy bundle package removal
                packages = $('.col-right .cart_packages .side-cart.block');

                $.each(packages, function(index, value){
                    var initialPackageId = $(value).data('package-id');
                    var newPackageId = data['packagesIndex'][initialPackageId];
                    $(value).attr('data-package-id', newPackageId);
                    $(value).data('package-id', newPackageId);

                    if (sessionStorage.warning && sessionStorage.warningMessage && initialPackageId) {
                        var arr = JSON.parse(sessionStorage.warningMessage).split('|');
                        var newWarnings = [];
                        for (var i = 0; i < arr.length; i++) {
                            var sessionPackageId = arr[i].substring(arr[i].lastIndexOf("package") + 8, arr[i].lastIndexOf("."));
                            if (initialPackageId == sessionPackageId) {
                                var newWarning = arr[i].substring(0,12) + newPackageId + arr[i].substring(arr[i].lastIndexOf("."), arr[i].length);
                                newWarnings.push(newWarning);
                            } else {
                                newWarnings.push(arr[i]);
                            }
                        }
                        var messageToSession = newWarnings.join('|');
                        if (messageToSession && messageToSession.length > 40) {
                            sessionStorage.setItem('warningMessage', JSON.stringify(messageToSession));
                        } else {
                            sessionStorage.removeItem('warning');
                            sessionStorage.removeItem('warningMessage');
                            data.complete = true;
                            delete data['warning'];
                            delete data['warningMessage'];
                        }
                    }
                    if (sessionStorage.warningMessageCardinality) {
                        var warningMsg = [];
                        var sessionMessageCardinality = JSON.parse(sessionStorage.getItem('warningMessageCardinality')).split('|');
                        $.each(sessionMessageCardinality, function (id,message){
                            var packageId = message.substring(message.lastIndexOf('[') + 1, message.lastIndexOf(']'));
                            if (warningMsg[packageId] === undefined) {
                                warningMsg[packageId] = [];
                            }
                            if (initialPackageId == packageId) {
                                var newWarningCardinality = message.substring(0,message.lastIndexOf('[')+1) + newPackageId + message.substring(message.lastIndexOf(']'), message.length);
                                warningMsg[packageId].push(newWarningCardinality);
                            } else {
                                warningMsg[packageId].push(message);
                            }
                        });
                        if (warningMsg.length != 0) {
                            sessionStorage.setItem('warningMessageCardinality',JSON.stringify(warningMsg.filter(function (item) { return item != undefined }).join("|")));
                        } else {
                            sessionStorage.removeItem('warningMessageCardinality');
                        }
                    }
                });

                if (typeof window.configurator != 'undefined') {
                    configurator['packageId'] = data['packagesIndex'][configurator['packageId']] ;
                    if (data.rules) {
                        // Refresh Cable rules
                        configurator['cableRules'] = {'rules' : data.rules};
                        configurator.reapplyRules();
                    }

                    if (data.rightBlock) {
                        if (data.packageType) {
                            var rightBlock = $('div[data-package-type="' + data.packageType + '"]:visible').first();
                            rightBlock.replaceWith(data.rightBlock);
                            if (!rightBlock.hasClass('active')) {
                                rightBlock.find('.block-container').trigger('click');
                            }
                        } else {
                            var rightBlock = $('div[data-package-type].active:visible').first();
                            rightBlock.replaceWith(data.rightBlock);
                        }
                    }
                }

            } else {
                $('#package-types').removeClass('hide');
                $('#show-packages').addClass('hide');
            }

            if (!sessionStorage.warning || !sessionStorage.warningMessage) {
                if (!data.complete) {
                    $('#configurator_checkout_btn').prop('disabled', !data.complete);
                }
            }

            if (window.address.customerUctParams == true) {
                window.address.getCallerCampaignData(true, 1);
            }

            updateCartPackageLine();
        },
        enableOrDisableAddressCheckBtn: function() {
            var requiredFieldsAreFilled = true;
            $('[id="topaddressCheckForm"] .required-entry').each(function() {
                var value = $(this).val();
                if (value === '' || value === null || $(this).hasClass('validation-failed')) {
                    requiredFieldsAreFilled = false;
                }
            });

            if (requiredFieldsAreFilled === true) {
                $('[id="topaddressCheckForm"] button').prop('disabled', false);
            } else {
                $('[id="topaddressCheckForm"] button').prop('disabled', true);
            }
        },
        showTopAddressCheckForm: function() {
            var topAddressForm = $('#topaddressCheckForm');
            topAddressForm.find(':input').val('').removeClass('validation-passed');
            this.enableOrDisableAddressCheckBtn();
            topAddressForm.show();
        },
        disableTopAddressCheckForm: function () {
            var topAddressForm = $('#topaddressCheckForm');
            topAddressForm.find(':input').prop('disabled', true);
            topAddressForm.find('button').prop('disabled', true);
        },
        enableTopAddressCheckForm: function () {
            var topAddressForm = $('#topaddressCheckForm');
            topAddressForm.find(':input').prop('disabled', false);
            topAddressForm.find('button').prop('disabled', false);
        },
        notifyServiceAbilityChanged: function () {
            self.waiting = false;
            /** This event will be caught in sidebar.js and other areas that depend on serviceability */
            $(document).trigger("serviceability-check-completed", []);
        },
        validateAddresses: function (prefilled) {
            var self = this;
            if(prefilled == false) {
                $('input[data-address-type]').on('blur', function () {
                    self.validateAddress(self, $(this).data('address-type'));
                });
            } else {
                self.validateAddress(self,prefilled);
            }

        },
        validateAddress: function (self, addressType) {
            var $valid = true;
            var $fields = $('input[data-address-type="'+addressType+'"]');
            //first, make sure all required address fields are mandatory
            $fields.each(function (i, el) {
                if(jQuery(el).hasClass('disallow-generic-address-validation')) {
                    $valid = false;
                    return false;
                }
                if(jQuery(el).hasClass('required-entry') && jQuery.trim(jQuery(el).val()) == ''){
                    $valid = false;
                    return false;
                }
            });
            //if not all required fields are filled in, don't do anything yet
            if(!$valid){
                return;
            }
            // if this address was an an invalid one last time, remove .invalid-address class in order to check it again
            $fields.each(function (i, el) {
                jQuery(el).removeClass("invalid-address");
                jQuery(el).parent().find('.validation-advice').remove();
            });

            // validate each field of address
            $fields.each(function (i, el) {
                $valid &= Validation.validate($fields[i])
            });
            if ($valid) {
                var $data = {};
                $fields.each(function(i, el) {
                    $data[$(this).data("form")] = $(this).val();
                });

                for(var key in $data){
                    if(!$data[key] && jQuery('input[data-form="' + key + '"]').hasClass('required-entry')){
                        return;
                    }
                }

                $data["type"] = addressType;
                self.post('address/index/validate/', $data, function(response) {
                    if(response.error) {
                        showModalError(response.message);
                        self.markAddressAsInvalid($fields);
                    }
                    if (response.error == false &&
                        (response.hasOwnProperty("valid") && !response.valid ||
                            response.hasOwnProperty("valid") && response.valid.hasOwnProperty("validity") && !response.valid.validity)) {
                        self.markAddressAsInvalid($fields,true);
                    }
                    if (response.error == false && response.hasOwnProperty("valid") && response.valid) {
                        // Remove validation
                        $fields.each(function(i, el) {
                            var wrapper = jQuery( el ).closest('.input-group-custom');
                            var advice = wrapper.find('.validation-advice').get(0);
                            if( advice ) {
                                Validation.hideAdvice(this,advice);
                            }
                        });
                    }
                    if (response.error == false && response.valid) {
                        if (response.valid.result.ValidateAddress.length == 1) {
                            var addressDetails = response.valid.result.ValidateAddress[0];

                            $fields.filter("[data-form*='postcode']").val(addressDetails['PostalZone']);
                            $fields.filter("[data-form*='city']").val(addressDetails['CityName']);
                            $fields.filter("[data-form*='street']").val(addressDetails['StreetName']);
                            $fields.filter("[data-form*='houseno']").val(addressDetails['BuildingNumber']);
                            $fields.filter("[data-form*='addition']").val(addressDetails['HouseNumberAddition']);
                        }
                    }
                }, function(error) {
                    self.markAddressAsInvalid($fields,true);
                });
            }
        },
        markAddressAsInvalid: function(address,validate) {
            address.each(function (i, el) {
                jQuery(el).addClass("invalid-address");
                if(validate) {
                    Validation.validate(el);
                }
            });
        },
        enableCheckServiceabilityButton: function() {
            jQuery('#topaddressCheckForm').find('button.check-button').prop('disabled', false);
        },
        disableCheckServiceabilityButton: function() {
            jQuery('#topaddressCheckForm').find('button.check-button').prop('disabled', true);
        },
        getServiceabilityFormFields: function() {
            var form = jQuery('#topaddressCheckForm');

            return {
                'postCode': form.find('[name="postcode"]'),
                'city':     form.find('[name="city"]'),
                'street':   form.find('[name="street"]'),
                'houseNo':  form.find('[name="houseNo"]'),
                'addition': form.find('[name="addition"]')
            };
        },
        fillServiceabilityFormFields: function(formData) {
            var form = this.getServiceabilityFormFields();

            form.postCode   .val(formData.postCode);
            form.city       .val(formData.city);
            form.street     .val(formData.street);
            form.houseNo    .val(formData.houseNo);
            form.addition   .val(formData.addition);
        },
        emptyServiceabilityFromFields: function() {
            jQuery('#topaddressCheckForm')[0].reset();
        },
        handleServiceabilityForm: function() {
            if (this.clearFormData === true) {
                this.emptyServiceabilityFromFields();
                this.disableCheckServiceabilityButton();
            }
        },
        disableInCheckout: function(selector) {
            if(/checkout/.test(window.location.pathname)) {
                $('[hide-in-checkout]').each(function(){
                    $(this).addClass('hidden');
                });
            }
        }
    });

    // Init address object on document ready
    window.address = new Address({
        baseUrl: MAIN_URL,
        spinner: MAIN_URL + 'skin/frontend/omnius/default/images/spinner.gif'
    });

    $.each({
        'address.validate': 'address/index/validateAddress'
    }, function (key, url) {
        window.address.addEndpoint(key, url);
    });
})(jQuery);