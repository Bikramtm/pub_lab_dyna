/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

(function ($) {

    var root = window;
    root.Configurator = {};

    Base = Configurator.Base = function (options) {
        this.options = $.extend(this.options, options || {});
        this.initialize.apply(this, this.options);
    };

    var automatically_refresh_selection = false;
    $.extend(Base.prototype, {
        initialized: false,
        ltIE9: false,
        options: {
            endpoints: {
                'templates': 'configurator/template/all',
                'products': 'configurator/mobile/all'
            }
            , packageId: null
            , container: '#config-wrapper'
            , sections: []
            , disabledSections: []
            , type: 'mobile'
            , spinner: 'skin/frontend/omnius/default/images/spinner.gif'
            , spinnerId: '#spinner'
            , cache: false
            , customerPanel: '#customer_details_panel'
            , enlargeLeft: '.enlarge.right-direction'
            , elementsToShowOnClose: 20
            , include_btw: true
            , hardwareOnlySwap: false
            , hiddenSections: ['subscription', 'addon']
            , isHollandsNieuwe: false
        },

        allowedSaleType: 'RETBLU',
        allowedDefaultSim: 'ACQ',
        fromSimFooter: false,
        baseAlternatives: '',
        cart: {},
        acqCart: [],
        ilsCart: [],
        mandatory: [],
        removalNotAllowed: [],
        selectableNotAllowedIdsFromContract: [],
        waiting: false,
        lastSection: null,
        filterStack: {},
        filterValues: {},
        permanentFilterStack: {},
        permanentFilterValues: {},
        packageTotals: {'maf': 0, 'price': 0},
        ajaxQueueSize: 0,
        shelve: {},
        lastPrices: null,
        lastUpdatedPrices: null,
        invalidProducts: {},
        KIP: null,
        DSL: null,
        identifier: 0,
        eligibleBundles: [],
        activeBundles: [],
        bundleComponents: [],
        bundleHints: [],
        showRedPlusModal: true,
        redPlusRelatedItem: null,
        bundleComponentItem: null,
        showSusoModal: true,
        showBundleBreakingModal: true,
        susoRelatedItem: null,
        showReservedMemberConfirmation: true,
        installBaseOwnerSubscription: null,
        activePackageEntityId: null,

        bundleChoice: function() {
            var configurator = this;
            return {
                fetchMandatoryFamilies: function(callback) {
                    configurator.http.post('bundles.fetchMandatoryFamilies', {}, callback, null);
                },
                validateRequiredFamilies: function(data) {
                    if (data) {
                        this._showValidationWarnings();
                        this._showWarnings(data);
                    }
                },
                _showValidationWarnings: function(){
                    if ($('#bundle-options').length) {
                        var $products = new Validation('bundle-options');
                        $products.validate();
                    }
                },
                _showWarnings: function(familiesPerPackage) {

                    if (familiesPerPackage && familiesPerPackage.length > 0) {
                        var $familyNames = [];
                        $.each(familiesPerPackage, function(key1, families) {
                            $.each(families, function(key2, family){
                                $familyNames.push(family['familyName']);

                            });
                        });

                        showNotificationBar(Translator.translate('Please select a product from the mandatory families: ' + $familyNames.join() ), 'error');
                    }


                }

            }
        },

        $$: function (selector, forceRefresh) {
            forceRefresh = forceRefresh || false;

            if (!forceRefresh && this.shelve.hasOwnProperty(selector)) {
                return this.shelve[selector];
            }
            var el = $(selector);
            this.shelve[selector] = el;
            return el;
        },

        log: function (method) {
            // return;
            console.log('configurator.{0}'.format(method));
        },

        debug: function () {
            console.info(Date.now());
            console.debug(arguments);
        },

        _collectTemplates: function (globalTemplates) {
            var self = this;

            /** Gather the section templates */
            var templates = {};
            $.each(self.options.sections, function (key, val) {
                templates[val] = globalTemplates[self.options.type][val] || '';
            });
            this.setTemplates(templates);

            /** Gather the partials */
            $.each(self.options.sections, function (key, val) {
                var partialsTemp = globalTemplates['partials'][self.options.type][val];
                $.each(partialsTemp, function (partialId, html) {
                    Handlebars.registerPartial(val + '_' + partialId, Handlebars.compile(html, {noEscape: true}));
                });
            });

            // @todo remove the generals
            // $.each(globalTemplates['partials'][self.options.type]['general'], function (partialId, html) {
            //     Handlebars.registerPartial('general_' + partialId, Handlebars.compile(html, {noEscape: true}));
            // });
        },

        initialize: function (options) {
            this.ltIE9 = this.$$('#isLTIE9').val() == '1';
            this.websiteId = this.$$('#current_website_id').val();
            this.$$('#config-wrapper').show();
            /* hide the wrapper; */
            var self = this;
            this.cart = {};
            this.subscriptionIdentifier = "";
            this.deviceIdentifier = "";
            this.options = $.extend(this.options, options);
            this.options.sections.forEach(function (element) {
                if (SUBSCRIPTION_IDENTIFIERS.indexOf(element) !== -1) {
                    self.subscriptionIdentifier = element;
                }
                if (DEVICES_IDENTIFIERS.indexOf(element) !== -1) {
                    self.deviceIdentifier = element;
                }
            });
            this.packageId = this.options.packageId;
            this.saleType = this.options.sale_type;
            this.cache = this.options.cache || false;
            this.http.cache = new Cache(this, this.options.type);
            this.http.endpoints = $.extend(this.http.endpoints, this.options.endpoints);
            this.http.spinnerId = this.options.spinnerId;
            this.isHollandsNieuwe = this.options.isHollandsNieuwe;
            this.permanentFilterStack = root.permanentFilters;
            //assure that sections are array (indirect bug)
            var getType = {};
            if (self.options.sections && getType.toString.call(self.options.sections) === '[object Object]') {
                var sections = [];
                for (var i in self.options.sections) {
                    if (self.options.sections.hasOwnProperty(i)) {
                        sections.push(self.options.sections[i]);
                    }
                }
                self.options.sections = sections;
            }

            /** Reset filters */
            this.filterStack = {};
            this.filterValues = {};

            /* Close left side bar if expanded */
            if ($(this.options.enlargeLeft).hasClass('expanded')) {
                $(this.options.enlargeLeft).trigger('click');
            }

            /*  Close customer panel if it is opened */
            if ($(this.options.customerPanel).is(':visible')) {
                $(this.options.customerPanel + ' .close:first').trigger('click');
            }

            /** Temporary prevent all clicks in document */
            $(document).click(function (e) {
                e.stopPropagation();
            });

            window.addResizeEvent(function () {
                self.assureElementsHeight();
            });

            this.initSpinner();

            this._collectTemplates(window.templates);

            this.setFilters(root.filters, function () {
                self.setProducts(root.products, root.families, function () {
                    self.startConfigurator(function () {
                        self.prepareCart(root.initCart, function () {
                            self.setupAcqProducts();
                            $(root)
                                .unbind('resize orientationchange')
                                .bind('resize orientationchange', function () {
                                    var el = $('{0}'.format(self.options.container));
                                    var header = self.$$('#header');
                                    if (el.length) {
                                        var height = $(window).outerHeight() - 2;
                                        if (header.length) {
                                            height -= header.outerHeight();
                                        }
                                        el.css({
                                            /*'overflow-y': 'auto'*/
                                            /*'height': height*/
                                        });
                                    }
                                })
                                .trigger('resize');
                            var packageBlock = $('[data-package-id="{0}"]'.format(self.packageId));
                            if (packageBlock.length) {
                                packageBlock.addClass('active');

                                var scrollTo = packageBlock.offset().top - packageBlock.parent().offset().top + packageBlock.parent().scrollTop();
                                if (scrollTo != 0) {
                                    packageBlock.parent().animate({
                                        scrollTop: scrollTo
                                    }, 1000);
                                }
                            }
                            if (self.options.hardwareOnlySwap) {
                                $.each(self.options.hiddenSections, function (index, elem) {
                                    $('#'+elem+'-block').hide();
                                });
                            }

                            //Open first selection when configurator starts
                            var diff = $(self.options.sections).not(self.options.disabledSections).get();
                            self.toggleSection(diff[0]);

                            self.initialized = true;
                        });
                    });
                });
            });

            //update prices will be executed by the toggleSection function
            self.updatePrices(root.prices);

            /** Release click lock on document */
            $(document).unbind('click');
            if (typeof self.simType == 'undefined' && (self.saleType == self.allowedSaleType)) {
                $('.radio.sim-old input[type="radio"]').prop('checked', true);
            }
            if (typeof self.simType != 'undefined') {
                var simCardName = '.sim-new ul li a[data-type="' + self.simType + '"]';
                $(simCardName).addClass('selected-sim');
            }

            self.reapplyRules(0, function() {
                if (self.products.tariff) {
                    self.getEligibleBundles();
                }
            }.bind(self));
            $(document).trigger('configurator.initialized');
        },

        getPrices: function () {
            var self = this;
            var allProducts = self.acqCart.concat(self.ilsCart);
            var cartProducts = allProducts.sort();
            var theType = self.saleType;

            var data = {
                'website_id': self.websiteId,
                'type': theType,
                'packageType' : self.options.type,
                'sales_id': root.SALES_ID
            };
            if (cartProducts.length > 0) {
                data.products = cartProducts.join(',');
            }

            this.http.ajax('GET', 'cart.getPrices', data, true).done(function (data) {
                var prices = data['prices'];
                root['prices'] = prices;

                if (Object.keys(prices).length) {
                    self.lastPrices = new Date().getTime();
                    self.updatePrices(prices);
                }
            });
        },

        /**
         * Get eligible and active bundles from backend
         * @returns {Base}
         */
        getEligibleBundles: function () {
            var self = this;

            // get bundles after rules to forward the available tarrifs instead of simulating all cart tariffs
            var tariffIdentifier;
            for (var section in self.products) {
                if (self.products.hasOwnProperty(section) && TYPE_TARIFFS.indexOf(section) !== -1) {
                    tariffIdentifier = section;
                    break;
                }
            }
            var allowedTariffs = self.products.tariff.map(function (tariff) {
                return tariff.entity_id;
            }).filter(function(productId) {
                return self.allowedProducts.indexOf(productId) > -1;
            });
            this.http.ajax('POST', 'cart.getBundles', {tariffs: allowedTariffs}, false).done(function (data) {self.updateBundleData(data);});

            return this;
        },

        /**
         * Update bundle data on current instance
         * @param data
         * @returns {Base}
         */
        updateBundleData: function (data)
        {
            var self = this;

            // setting eligible bundles
            if (data.eligibleBundles.length) {
                // set eligible bundles on configurator
                self.eligibleBundles = data.eligibleBundles;
            } else {
                // clear eligible bundles from configurator
                self.eligibleBundles = [];
            }

            // resetting bundle data on configurator
            self.activeBundles = [];

            // update bundles on this instance
            if (data.activeBundles.length) {
                data.activeBundles.forEach(function (bundleData) {
                    var bundle = new Bundle().addBundleData(bundleData);
                    self.activeBundles.push(bundle);
                });
            }

            self.bundleComponents = data.bundleComponents;
            self.bundleHints = data.bundleHints;

            // let UI update configurator
            $(document).trigger('omnius.configurator.rule.updated');

            return this;
        },

        applyNonStandardRates: function() {
            var self = this;

            window.setPageLoadingState(true);
            $.ajax({
                'type': 'POST',
                'url': 'configurator/cart/applyNonStandardRates',
                'data': {
                    'packageType': self.options.type,
                    'availableProducts': self.allowedProducts
                },
                'success': function(response) {
                    if (response.error) {
                        showModalError(response.message);
                    } else {
                        if (Object.keys(response.nonStandardRates).length) {
                            Object.keys(window.prices).each(function(key, index) {
                                if (response.nonStandardRates[key]) {
                                    window.prices[key] = response.nonStandardRates[key];
                                }
                            });

                            self.lastPrices = new Date().getTime();
                            self.updatePrices();
                        }
                        if (response.hasOwnProperty('rightBlock')) {
                            jQuery('.cart_packages').replaceWith(response.rightBlock);
                            togglePackage(jQuery('.cart_packages').find('div[data-package-id="' + response.activePackageId + '"]').find('.side-block-activate'));
                        }
                    }
                },
                'complete': function (response) {
                    window.setPageLoadingState(false);
                }
            })
        },

        reapplyRules: function (disableCache, callBack) {
            if (typeof disableCache === 'undefined') { disableCache = 0; }
            var self = this;
            // get ordered products ids
            var cartProducts = self.getPackageProductIds();
            var rulesCallback = function (data) {
                setPageLoadingState(false);
                if (data.error) {
                    showModalError(data.message, 'Compatibility Rules error');
                    return;
                }

                // todo: check if data['rules'] needs to be active to all packages
                if ($.inArray(self.options.type, window.allPackages) > -1) {
                    self.allowedProducts = data['rules'];
                } else {
                    self.allowedProducts = self.getPackageProductIds().length ? data['rules'] : root.allowedProducts;
                }
                if ($('#is_order_edit_mode').val() == 1) {
                    self.mandatory = [];
                } else {
                    self.mandatory = self.getPackageProductIds().length ? data['mandatory'] : [];
                }

                self.removalNotAllowed = data.hasOwnProperty('removalNotAllowed') ? data.removalNotAllowed : [];
                self.selectableNotAllowedIdsFromContract = data.hasOwnProperty('selectableNotAllowedIdsFromContract') ? data.selectableNotAllowedIdsFromContract : [];

                // Demo only
                if (data['disabled'] != undefined) {
                    $('.product-table .item-row').removeClass('disabled-row');
                    $.each(data.disabled, function (index, pid) {
                        $('.product-table [data-id="{0}"]'.format(pid)).addClass('disabled-row');
                    });
                }

                if (data.hasOwnProperty('invalid')) {
                    self.invalidProducts = data.invalid;
                }

                self.applyMandatory();
                self.applyInvalid();
                $.each(self.products, function (section, products) {
                    self.filterProducts(products, section);
                });
                if ($.inArray(self.options.type, window.CABLE_PACKAGES) > -1) {
                    self.applyNonStandardRates();
                }
                if (typeof callBack === "function") {
                    callBack();
                }
            };

            self.KIP = $('#serviceability-content [data-technology="KIP"]').data('downstream');
            self.DSL = $('#serviceability-content [data-technology="(V)DSL"]').data('downstream');
            var postData = {
                'websiteId': self.websiteId,
                'type': self.options.type,
                'kip': self.KIP,
                'dsl': self.DSL,
                'identifier': self.options.identifier,
                'packageId': self.options.packageId,
                'customer_type': customerDe.isBusiness,
                'customer_number': customerDe.customerNumber,
                'hash': root.quoteHash ? root.quoteHash : QUOTE_HASH
            };
            if(disableCache) {
                postData.bypassCache = Math.random()
            }
            if (cartProducts.length > 0) {
                postData.products = cartProducts.join(',');
            }
            if (self.ilsCart.length > 0) {
                postData.ilsProducts = self.ilsCart.join(',');
            }

            this.http.post('cart.getRules', postData, rulesCallback);
        },

        applyMandatory: function () {
            var self = this;
            $('.conf-block .family .family-status .optional').addClass('hidden');
            $('.conf-block .family .family-status .mandatory').removeClass('hidden');
            if (self.mandatory != undefined) {
                $.each(self.mandatory, function (index, value) {
                    var status = $('.conf-block .family[data-family-id="' + value + '"] .family-status');
                    status.find('.optional').addClass('hidden');
                    status.find('.mandatory').removeClass('hidden');
                });
            }
        },

        applyInvalid: function() {
            var self = this;
            $.each(self.invalidProducts, function (sku, pid) {
                $('.product-table [data-id="{0}"]'.format(pid)).addClass('disabled-row');
            });
        },

        //All products from the family category shouldn't be shown if at least one is displayed(selected/mandatory/exclusive)
        hideNotSelectedProducts: function() {
            var self = this;
            var families = $('.product-table .family');
            //parse existing families
            $.each(families, function (id, el) {
                if($(el).data('hide-not-selected') == 1 ){
                    var mainRows = $(el).find('.row.selected, .row.mandatory-row');
                    if(mainRows.length != 0 ){
                        var rows = $(el).find('.row[style$="display: block;"]:not(.selected):not(.mandatory-row)');
                        $.each(rows, function (id, el) {
                            $(el).addClass('hide_not_selected_products').css("display", 'none');
                        });
                    }
                }
            });
        },

        destroy: function () {
            $(this.options.container).find('.content')[0].innerHTML = '';
            $('#homepage-wrapper').show();
            /* show favorites and deals; */
            $('#config-wrapper').hide();
            /* hide the wrapper;*/
        },

        initSpinner: function () {
            if (!$(this.options.spinnerId).length) {
                this.$$('body').before('<div id="' + this.options.spinnerId.replace(/\.|#/g, '') +
                    '" class="spinner"><p><img src="#"/></p></div>'.replace(/#/g, this.options.spinner));
            }
        },

        prepareCart: function (data, callback) {
            var self = this;
            $.each(data, function (type, ids) {
                self.cart[type] = ids.unique();
            });

            if (root.sim) {
                this.sim = root.sim;
            }

            if (self.cart['simcard']) {
                self.selectedSim = true;
                self.cart['sim'] = self.cart['simcard'];
            }

            if (self.cart['sim']) {
                if ((typeof self.sim[self.cart['sim'][0]]) != 'undefined') {
                    var sim = self.sim[self.cart['sim'][0]];
                    var simType = sim['name'];
                    if (simType) {
                        this.simType = simType;
                        self.addSimOption(simType);
                    }
                }
            }

            $.each(self.cart, function (section, products) {
                var block = $('#'+section+'-block');
                if (block.length) {
                    $.each(products, function (key, id) {
                        var row = block.find('#item-product-'+id);
                        if (row.length) {
                            row.addClass('selected');
                            if (ILS_PROCESS_CONTEXTS.indexOf(root.processContextCode.toLowerCase()) !== -1) {
                                if (root.defaultedItems.length && root.defaultedItems.indexOf(id.toString()) !== -1) {
                                    row.find('span.item-label').removeClass('hidden').removeClass('contract').addClass('given').text(Translator.translate('GIVEN'));
                                }
                            }
                            if (root.defaultedObligatedItems.length && root.defaultedObligatedItems.indexOf(id.toString()) !== -1) {
                                row.addClass('mandatory-row');
                                if (ILS_PROCESS_CONTEXTS.indexOf(root.processContextCode.toLowerCase()) !== -1) {
                                    row.find('span.item-label').removeClass('hidden').removeClass('given').addClass('mandatory').text(Translator.translate('MANDATORY'));
                                }
                            }
                            var familyId = row.parents('.family').data('family-id');
                            var family = false;
                            familyId && window.families.every(function (el) {
                                familyId = familyId.toString();
                                if (el.entity_id === familyId) {
                                    family = el;
                                    return false;
                                }
                                return true;
                            });
                            if (family && (family.hasOwnProperty('allow_mutual_products') && family.allow_mutual_products.toString() === "1")) {
                                row.siblings().addClass('exclusive-row');
                            }
                        }
                    });
                }
                self.addSelections(section, false);
            });

            // mark multiple selected products in cart
            self.markMultiOptions();

            if(typeof self.products != 'undefined') {
                $.each(self.products, function (section, products) {
                    self.filterProducts(products, section);
                });
            }

            // apply updated prices, if we have any
            if (root['prices']) {
                self.updatePrices(root['prices']);
            }

            if (root.getType(callback) === 'function') {
                callback();
            }
        },

        updatePrices: function () {
            // If prices have not been updated, do nothing
            if (!this.initialized || (this.lastPrices && this.lastUpdatedPrices === this.lastPrices)) {
                return;
            }

            // let UI handle frontend updates
            this.lastUpdatedPrices = this.lastPrices;

            $(document).trigger('omnius.configurator.prices.updated');
        },

        /**
         * Iterating through each section products to set products state
         * Products can be cached in local storage so inconsistencies might appear between init configurator response and cached products
         * @param state bool
         */
        updateBtwState: function (state) {
            for (var category in this.products) {
                if (this.products.hasOwnProperty(category)) {
                    var len = this.products[category].length;
                    for (var i = 0; i < len; ++i) {
                        this.products[category][i]['include_btw'] = state;
                    }
                }
            }
        },

        setPackageId: function (packageId) {
            this.packageId = packageId;
        },

        setupAcqProducts: function () {
            this.acqCart = [];
            this.ilsCart = [];
            // get all products from cart on the current package
            for (var confSection in initCart) {
                if (initCart.hasOwnProperty(confSection)) {
                    var len = this.cart[confSection].length;
                    for (var i = 0; i < len; ++i) {
                        var productId = this.cart[confSection][i];
                        if (!root.installedBaseCart.hasOwnProperty(productId)) {
                            this.acqCart.push(productId);
                        } else {
                            this.ilsCart.push(productId);
                        }
                    }
                }
            }
        },

        getPackageProductIds: function () {
            var self = this;
            var cartProducts = self.acqCart;
            cartProducts.sort();
            return cartProducts;
        },

        increaseQueueSize: function () {
            ++this.ajaxQueueSize;
            $('#cart-spinner').removeClass('hide');
            $('.col-right.sidebar').find('> button.enlarge').hide();
            $('.cart_packages')
                .css('overflow', 'hidden')
                .scrollTop(0);
        },

        decreaseQueueSize: function () {
            --this.ajaxQueueSize;
            if (this.ajaxQueueSize <= 0) {
                this.ajaxQueueSize = 0;
                $('#cart-spinner').addClass('hide');
                $('.col-right.sidebar > button.enlarge').show();
                var cartPackages = $('.cart_packages');

                var activePackageBlock = cartPackages.find('[data-package-id={0}]'.format(this.packageId));
                // var scrollTo = activePackageBlock.offset().top - cartPackages.offset().top + cartPackages.scrollTop();
                //cartPackages.scrollTop(scrollTo);
                //cartPackages.css('overflow', 'auto');
            }
        },

        addMultiToCart: function (products, section, callback, async, refreshConfigurator) {
            startPiwikCall(MAIN_URL + "configurator/cart/addMulti");
            var self = this;

            if (!refreshConfigurator) {
                // show spinner in cart separate
                window.setPageLoadingState && window.setPageLoadingState(true);
                window.setPageOverrideLoading(true);
                // increase size of Queue on click
                self.increaseQueueSize();
            }

            // Disable proceed button during spinner
            var nextButton = $('#cart_totals input[type="button"]');
            // Preserving initial button state
            var proceedButtonDisabled = nextButton.attr('disabled');
            nextButton.attr('disabled', true);

            // addMulti call
            self.http.ajaxQueue('POST', 'cart.addMulti', {
                    'products': products,
                    'type': self.options.type,
                    'section': section,
                    'simType': self.simType,
                    'oldSim': self.options.old_sim,
                    'packageId': self.packageId,
                    'clickedItemId': self.lastAddedItem.data('id'),
                    'clickedItemSelected': self.lastAddedItem.hasClass('selected'),
                }, async)
                .done(function (data) {
                    callback(data);

                    if (section == 'device' || section == 'subscription') {
                        if (data.drc_eligible) {
                            $('#devicerc-block').show();
                            self.toggleSection('devicerc', false);
                        } else {
                            $('#devicerc-block').hide();
                        }
                    }

                    // no further calls are needed as the configurator gets refreshed
                    if (refreshConfigurator) {
                        return true;
                    }

                    // Update left side sections if they are complete or not
                    var sections;
                    if (data.hasOwnProperty('incomplete_sections')) {
                        sections = data['incomplete_sections'];
                    } else {
                        sections = [];
                    }
                    // If the addMulti call fulfilled the bundle choice requirements, remove the errors
                    if(data.hasOwnProperty('removeBundleErrorNotice')) {
                        $('.bundle-incomplete').removeClass('bundle-incomplete');
                        hideNotificationBar();

                    }
                    // If the addMulti call fulfilled the bundle choice requirements, remove the errors
                    if(data.hasOwnProperty('removeBundleChoiceBlock')) {
                        $('#bundle-options').html("");
                    }
                    self.showWarnings(sections);

                    if (data.hasOwnProperty("hintMessages") && Object.keys(data.hintMessages).length !== 0) {
                        configuratorUI.showHintNotifications(data.hintMessages);
                    } else {
                        $("#notification-bar").removeClass('notification-bar-error');
                    }

                    if (data.clearConfigurator) {
                        if (window['configurator']) {
                            window['configurator'].destroy();

                            $(".side-cart.block[data-package-id='" + data.activePackageId + "']").removeClass('active');
                            refreshConfigurator = !refreshConfigurator;

                            // hide page loader
                            window.setPageLoadingState && window.setPageLoadingState(false);
                            window.setPageOverrideLoading(false);
                            self.decreaseQueueSize();
                        }
                    }

                    if (data.hasOwnProperty('cartIsRedPlusEligible') && data.cartIsRedPlusEligible) {
                        jQuery('#package-types').find("[data-package-creation-type-id='" + REDPLUS_CREATION_TYPE_ID + "']").removeClass('disabled');
                    } else {
                        jQuery('#package-types').find("[data-package-creation-type-id='" + REDPLUS_CREATION_TYPE_ID + "']").addClass('disabled');
                    }

                    if (data.hasOwnProperty('packageEntityId')) {
                        self.activePackageEntityId = data.packageEntityId;
                    } else {
                        self.activePackageEntityId = null;
                    }
                    updateCartPackageLine();
                }).then(function () {
                // no further calls are needed as the configurator gets refreshed
                if (!refreshConfigurator) {
                    // Will trigger getRules call (if not already cached at browser storage level)
                    self.reapplyRules(0, function() {
                        if (self.products.tariff) {
                            self.getEligibleBundles();
                        }
                    }.bind(self));
                    // getPrices call (also, if not already cached at browser storage level)
                    self.getPrices();

                    self.decreaseQueueSize();
                }

                endPiwikCall(MAIN_URL + "configurator/cart/addMulti");

                if (!proceedButtonDisabled) {
                    nextButton.removeAttr('disabled');
                }
            });
        },

        showWarnings: function (sections) {
            // Remove highlight from all sections
            var oldHighlighted = $('#config-wrapper div.incomplete');
            oldHighlighted.removeClass('incomplete');
            // Clear all the warnings for the sections
            $('#config-wrapper span.incomplete').addClass('hidden');

            $.each(sections, function (section) {
                $('#' + section + '-block span.incomplete').removeClass('hidden');
                // If the old sections are still incomplete, highlight again
                oldHighlighted.each(function (id, el) {
                    el = $(el);
                    if (el.attr('id') == (section + '-block')) {
                        el.addClass('incomplete');
                    }
                })
            });
        },

        removeFromCart: function (id, callback, async) {
            this.http.post('cart.delete', {
                'product': id,
                'type': this.options.type,
                'packageId': this.packageId
            }, callback, null, async);
        },

        getProductType: function (productId) {
            var item = $('.selection-block [data-id="{0}"]'.format(productId));
            if (!item.length) {
                return '';
            }
            return item.parents('.conf-block').data('type');
        },

        setTemplates: function (templates) {
            this.templates = templates;
        },

        getProductPopupTemplate: function () {
            return window.templates['partials'][this.options.type]['general']['product_popup'];
        },

        setProducts: function (products, families, callback) {
            this.allowedProducts = root.allowedProducts || [];
            this.products = products;
            this.families = families;

            if (root.getType(callback) == 'function') {
                callback();
            }
        },

        getAllProductsIds: function () {
            var ids = [];
            for (var category in this.products) {
                if (this.products.hasOwnProperty(category)) {
                    var len = this.products[category].length;
                    for (var i = 0; i < len; ++i) {
                        ids.push(this.products[category][i]['entity_id']);
                    }
                }
            }
            return ids;
        },

        setFilters: function (filters, callback) {
            this.filters = filters;
            if (root.getType(callback) == 'function') {
                callback();
            }
        },

        startConfigurator: function (callback) {
            var self = this;

            // set all products to corresponding btw state
            this.updateBtwState(this.options.include_btw);

            // build data to be sent to ui
            var uiData = [];

            // iterate through each configurator section
            this.options.sections.forEach(function(section, i) {
                // parse each configurator section by its given products
                if (this.templates[section]) {
                    // tell each section what products template to use
                    var productsTemplate = '{0}_selected_item'.format(section);
                    // section products are all exiting products in each configurator section, including the ones belonging to families
                    var sectionProducts = (typeof this.products != 'undefined' && this.products[section] ) ? this.products[section] : [];
                    // this is the list of products with no family sent to configuratorUI
                    var templateProducts = sectionProducts.reduce(function(templateProducts, cur, i) {
                        templateProducts[i] = cur;
                        return templateProducts;
                    }, {});
                    var templateFamilies = [];

                    // if partial is registered and section has families, proceed to fetching products to each family
                    if ((root.getType(Handlebars.partials['{0}_family'.format(section)]) === 'function') && this.families.length > 0 && sectionProducts.length > 0) {
                        var familyTemplate = '{0}_family'.format(section);
                        sectionProducts.forEach(function(product, k) {
                            this.families.every(function(family) {
                                if (parseInt(family['entity_id']) in product['families']) {
                                    // Init new family for current section
                                    if (!(family['entity_id'] in templateFamilies)) {
                                        family.products = [];
                                        templateFamilies[family['entity_id']] = family;
                                    }

                                    // Add product to template family
                                    templateFamilies[family['entity_id']].products.push(product);

                                    // If found in the first family, the product is removed from products list and added to the family product list which is rendered separately than products
                                    delete templateProducts[k];
                                    return false;
                                }

                                return true;
                            }.bind(this));
                        }.bind(this));
                    }

                    // OMNVFDE-778: Sort families inside configurator section
                    if (templateFamilies.length) {
                        templateFamilies.sort(function (oneFamily, anotherFamily) {
                            if(oneFamily.family_position.toString() == anotherFamily.family_position.toString()){
                                return oneFamily.name.localeCompare(anotherFamily.name);
                            }else {
                                return oneFamily.family_position - anotherFamily.family_position;
                            }
                        });
                    }
                    // EOF OMNVFDE-778: Sort families in configurator

                    var permanentFilters = {};
                    if (window.permanentFilters[self.options.sections[i]] !== undefined) {
                        permanentFilters = window.permanentFilters[self.options.sections[i]];
                    }

                    uiData[section] = {
                        index: i,
                        productType: section,
                        familyTemplate: familyTemplate ? familyTemplate : {},
                        families: jQuery.extend(true, {}, templateFamilies),
                        productsTemplate: productsTemplate,
                        products: jQuery.extend(true, {}, templateProducts),
                        permanentFilters: permanentFilters,
                        filters: (typeof self.filters != 'undefined' && self.filters[section]) ? self.filters[section] : {},
                        closed: !!i
                    };
                }
            }.bind(this));

            // let UI handle display
            window.configuratorUI.setConfigurator(this).loadConfigurator(uiData, callback);
        },

        assureElementsHeight: function () {
            var self = this;
            var checkExist = setInterval(function () {
                if ($('.calculateHeightLeftOver').length) {
                    /* get open section elements */
                    var openedSection = $('.selection-block.box-opened .product-table').first();
                    var openSectionHeight = openedSection.parent().height();

                    /* get element at the bottom of the list */
                    var leftOverHeightContainer = $('#config-wrapper').find('.calculateHeightLeftOver');

                    /* get offset and height */
                    var calculateHeightLeftOverOffset = leftOverHeightContainer.first().offset().top;
                    var calculateHeightLeftOverHeight = leftOverHeightContainer.first().height();

                    /* get window height */
                    var windowHeight = $(window).height();

                    if ($('#bundleModal').is(":visible")) {
                        jQuery('#cart_totals').addClass('bundle-configurator-bar');
                        jQuery('.cart_packages').css('height', "calc(100% - 150px)");
                        windowHeight -= 60;
                    } else {
                        jQuery('.cart_packages').css('height', "calc(100% - 100px)");
                        jQuery('#cart_totals').removeClass('bundle-configurator-bar');
                    }

                    /* calculate parent section height */
                    var sectionHeight = windowHeight - calculateHeightLeftOverHeight - calculateHeightLeftOverOffset;

                    /* calculate inner table height -- should be the rest of the parent available height */
                    var offsetInParent = 0;
                    openedSection.prevAll().each(function (i, _el) {
                        var elem = $(_el);
                        offsetInParent += elem.is(':visible') ? elem.outerHeight() : 0;
                    });

                    /* set the height for the parent and the table */
                    openedSection.height(sectionHeight + openSectionHeight - offsetInParent);

                    if (self.ltIE9) {
                        /* <= IE9 doesn't support transition so we use the slower js animate function   */
                        openedSection.parent().animate({
                            height: sectionHeight + openSectionHeight - 5
                        }, 200);
                    } else {
                        /* leave the transition to css */
                        openedSection.parent().height(sectionHeight + openSectionHeight - 10 /* padding */);
                    }

                    /* stop the repetition */
                    clearInterval(checkExist);
                    openedSection.parent().trigger('box.opened');
                }
            }, 100);
            /* check every 100ms */
        },

        filter: function (element, permanent) {
            var self = this;
            var el = $(element);
            var value = el.data('selection');
            var property = el.parents('.section-filters').data('filter-for');
            var type = el.parents('.conf-block').data('type');
            var searchOnType = el.parents('.conf-block').find('.search-on-type');

            if (undefined === this.filterStack[type]) {
                this.filterStack[type] = {};
            }

            if (undefined === this.filterValues[type]) {
                this.filterValues[type] = [];
            }

            if (typeof this.filterStack[type][property] === "undefined"){
                this.filterStack[type][property] = []
            }
            this.filterStack[type][property].push(value);
            self.filterValues[type].push(el.text());
            if (permanent) {
                if (!self.permanentFilterStack.hasOwnProperty(type)) {
                    self.permanentFilterStack[type] = {};
                }
                self.permanentFilterStack[type][property] = value;

                if (!self.permanentFilterValues.hasOwnProperty(type)) {
                    self.permanentFilterValues[type] = [];
                }
                self.permanentFilterValues[type].push(el.text());
            }

            // Check if the Clear Filters button should be shown
            this.showClearFilters(searchOnType, type);
            self.filterValues[type] = $.map(self.filterValues[type].unique(), function (n, i) {
                return n && n.length ? n : null;
            });
            if (permanent) {
                self.permanentFilterValues[type] = $.map(self.permanentFilterValues[type].unique(), function (n, i) {
                    return n && n.length ? n : null;
                });
            }
            this.filterProducts(self.products[type], type);

            searchOnType.trigger('keyup');
        },

        updateSimsList: function (possibleSims) {
            $('.radio.sim-new a').parent().addClass('hidden');
            if (possibleSims) {
                $.each(possibleSims, function (id, el) {
                    $('.radio.sim-new a[data-type="' + el + '"]').parent().removeClass('hidden');
                });
            }
        },

        activateDisabledItems: function (section, showPrices) {
            showPrices = showPrices != undefined ? showPrices : false;
            // show spinner in cart separate
            window.setPageLoadingState && window.setPageLoadingState(true);
            window.setPageOverrideLoading(true);

            var items = [];
            var self = this;
            self.temporaryAllowedProducts = [];
            // Display all the disabled items from the section
            section = $(section).parents('.conf-block');
            rulesCallback = function (result) {
                if (result.error) {
                    showModalError(result.message);
                    items = [-1];
                } else {
                    items = JSON.parse(result.ids);
                }

                section.find('.item-row').each(function (id, el) {
                    el = $(el);
                    if (items.length == 0 || jQuery.inArray("" + el.data('id'), items) != -1) {
                        if (el.css("display") == "none") {
                            el.css("display", 'block');
                            el.addClass('incompatible-row');
                            self.temporaryAllowedProducts.push("" + el.data('id'));
                            if (!showPrices) {
                                el.find('.item-pricing').addClass('hidden');
                            }
                        }
                    }
                });
                section.find('input.search-on-type').trigger('change');
            };
            var selectedItems = [];
            // Send all the selected items for cache purposes
            $('#config-wrapper .item-row.selected').each(function () {
                selectedItems.push($(this).data('id'));
            });
            selectedItems.sort(function (l, r) {
                return r < l;
            });
            self.increaseQueueSize();
            this.http.ajaxQueue('GET', 'cart.get' + section.data('type') + 'Rules', {
                "ids": selectedItems,
                "website_id": self.websiteId
            }, true)
                .done(function (data) {
                    rulesCallback(data);
                    self.decreaseQueueSize();
                    // hide page loader
                    window.setPageLoadingState && window.setPageLoadingState(false);
                    window.setPageOverrideLoading(false);
                });
            section.find('.hide-incompatible').removeClass('hidden');
            section.find('.show-incompatible').addClass('hidden');
        },

        deactivateDisabledItems: function (section) {
            var self = this;
            self.temporaryAllowedProducts = [];
            // Display all the disabled items from the section
            section = $(section).parents('.conf-block');
            section.find('.item-row.incompatible-row').each(function (id, el) {
                el = $(el);
                if (el.css("display") == "block") {
                    el.css("display", 'none');
                    el.removeClass('incompatible-row');
                }
            });
            section.find('.hide-incompatible').addClass('hidden');
            section.find('.show-incompatible').removeClass('hidden');
            section.find('input.search-on-type').trigger('change');
        },

        clearFilters: function (element) {
            var el = $(element);
            var self = this;
            var type = el.parents('.conf-block').data('type');
            this.filterStack[type] = {};
            self.filterValues[type] = [];
            this.filterProducts(self.products[type], type);
            var searchOnType = el.parents('.conf-block').find('.search-on-type');
            searchOnType.val('');
            el.parents('.conf-block').find('.section-filters li a').removeClass('active');
            searchOnType.trigger('keyup');
            el.addClass('hidden');
        },

        filterProducts: function (productsInitial, type) {
            if (typeof productsInitial === "undefined" || !productsInitial.length) {
                return null;
            }
            var searchOnType = root.getType(type) === 'object' ? $(type) : false;
            type = root.getType(type) === 'object' ? $(type).parents('.conf-block').data('type') : type;
            var products = this.filterCollection(productsInitial, type);
            var block = $('#'+type+'-block');
            var self = this;
            var families = $('.family');
            var familiesToRemove = [];
            var displayOnlyMandatory = false;
            if (self.filterStack[type] != undefined && self.filterStack[type]['mandatory'] != undefined) {
                switch (self.filterStack[type]['mandatory']) {
                    case 0:
                        if (families.length > 0) {
                            $.each(families, function (index, value) {
                                var hidden = $(value).find('.family-header .family-status .hidden');
                                if (hidden.hasClass('mandatory')) {
                                    self.displayFamily(value);
                                } else {
                                    $(value).css('display', 'none');
                                    familiesToRemove.push("" + $(value).data('family-id'));
                                }
                            });
                        }
                        break;
                    case 3:
                        if (families.length > 0) {
                            $.each(families, function (index, valueTmp) {
                                var hidden = $(valueTmp).find('.family-header .family-status .hidden');
                                if (hidden.hasClass('mandatory')) {
                                    $(valueTmp).css('display', 'none');
                                    familiesToRemove.push("" + $(valueTmp).data('family-id'));
                                } else {
                                    self.displayFamily(valueTmp);
                                }
                            });
                        }
                        displayOnlyMandatory = true;
                        break;
                }
            }
            var productsNew = [];

            for (var j = 0; j < products.length; j++) {
                if (familiesToRemove.length == 0 || !products[j].hasOwnProperty('families') || products[j]['families'].filter(function (n) {
                        return familiesToRemove.indexOf(n) != -1;
                    }).length == 0) {
                    if (displayOnlyMandatory && (!products[j].hasOwnProperty('families') || products[j]['families'].length == 0)) {
                        continue;
                    }
                    productsNew.push(products[j]);
                }
            }

            this.updateFilterStatus(productsNew, type);
            this.updateSelectedItemMessage(productsNew, type);

            var productsIds = $.map(productsNew, function (n, i) {
                return n ? n['entity_id'] : null
            });

            $('.conf-block[data-type="' + type + '"] .item-row').each(function () {
                // Avoid hiding discount items
                if (!$(this).data('disabled') && !$(this).hasClass('package-item')) {
                    if (-1 === productsIds.indexOf($.trim($(this).data('id'))) && !$(this).hasClass('discount-item')) {
                        $(this)[0].style.display = 'none';
                    } else {
                        $(this)[0].style.display = 'block';

                        if (self.temporaryAllowedProducts && self.temporaryAllowedProducts.length && (-1 !== self.temporaryAllowedProducts.indexOf($.trim("" + $(this).data('id'))))
                            && ((self.allowedProducts && self.allowedProducts.length && (-1 === self.allowedProducts.indexOf($.trim("" + $(this).data('id'))))) || (!self.allowedProducts || !self.allowedProducts.length))
                        ) {
                            $(this).addClass('incompatible-row');
                        } else {
                            $(this).removeClass('incompatible-row');
                        }
                    }
                }
            });

            $('.conf-block[data-type="' + type + '"] .removal-not-allowed').removeClass('mandatory-row').removeClass('removal-not-allowed');
            this.removalNotAllowed.forEach(function (productId) {
                $('.conf-block[data-type="' + type + '"] .item-row[id="item-product-' + productId + '"]').addClass('mandatory-row').addClass('removal-not-allowed');
            }.bind(this));
            this.selectableNotAllowedIdsFromContract.forEach(function (productId) {
                $('.conf-block[data-type="' + type + '"] .item-row[id="item-product-' + productId + '"]').removeClass('selected').removeClass('multi-option').addClass('not-allowed');
                $('.conf-block[data-type="' + type + '"] .item-row[id="item-product-' + productId + '"]').find('div').addClass('line-through');
            }.bind(this));

            // Check if the Clear Filters button should be shown
            if (searchOnType) {
                this.showClearFilters(searchOnType, type);
            }
            if (families.length > 0) {
                $.each(families, function (index, value) {
                    self.displayFamily(value);
                });
            }
            self.hideNotSelectedProducts();
            this.assureElementsHeight();
        },

        showClearFilters: function (searchOnType, type) {
            if (undefined === this.filterStack[type]) {
                this.filterStack[type] = {};
            }

            if (Object.keys(this.filterStack[type]).length == 0 && searchOnType.val().length == 0) {
                // Hide the clear filters button for this section
                searchOnType.parents('.selection-block').find('.clear-config-filters').addClass('hidden');
            } else {
                // Show the clear filters button for this section
                searchOnType.parents('.selection-block').find('.clear-config-filters').removeClass('hidden');
            }
        },

        filterCollection: function (products, type) {
            var self = this;
            var filtered = [];
            var hasFilters = false;
            var productsLength = products.length;
            for (var i = 0; i < productsLength; i++) {
                var valid = true;
                if (self.filterStack[type]) {
                    for (var filter in self.filterStack[type]) {
                        var productFilter = filter;
                        if (filter == 'mandatory' || self.filterStack[type][filter].length == 0) {
                            continue;
                        } else if (filter === 'price' && products[i]['include_btw'] === true) {
                            productFilter = 'price_with_tax';
                        }
                        if (self.filterStack[type].hasOwnProperty(filter)) {
                            hasFilters = true;
                            var getType = {};
                            if (products[i][productFilter]) {
                                if (!isNaN(products[i][productFilter])) {
                                    products[i][productFilter] = parseFloat(products[i][productFilter]);
                                }
                                if (getType.toString.call(products[i][productFilter]) === '[object Array]') {
                                    valid = (productFilter.filter(function(n) {
                                                return filter.indexOf(n) !== -1;
                                            }).length > 0);
                                } else if (getType.toString.call(products[i][productFilter]) === '[object Number]' &&
                                    self.filterStack[type][filter].map(parseFloat).indexOf(parseFloat(products[i][productFilter])) === -1) {
                                    valid = false;
                                }
                            } else if (self.filterStack[type][filter].map(parseFloat) > 0) {
                                // Exclude the product if the applied filter is non zero and product filter value is zero
                                valid = false;
                            }
                            if (!valid) {
                                break;
                            }
                        }
                    }
                }
                valid ? filtered.push(products[i]) : valid;
            }
            if ((self.allowedProducts && self.allowedProducts.length) || (self.temporaryAllowedProducts && self.temporaryAllowedProducts.length)) {
                var allowed = self.allowedProducts && self.allowedProducts.length;
                var temporary = self.temporaryAllowedProducts && self.temporaryAllowedProducts.length;
                var filteredProducts = hasFilters ? filtered : products;
                var filteredLength = filteredProducts.length;
                filtered = [];
                hasFilters = true;
                var j, skip;
                for (j = 0; j < filteredLength; j++) {
                    skip = false;
                    if (allowed) {
                        if (-1 !== self.allowedProducts.indexOf("" + filteredProducts[j]['entity_id'])) {
                            filtered.push(filteredProducts[j]);
                            skip = true;
                        }
                    }
                    if (temporary && !skip) {
                        if (-1 !== self.temporaryAllowedProducts.indexOf("" + filteredProducts[j]['entity_id'])) {
                            filtered.push(filteredProducts[j]);
                        }
                    }
                }
            } else {
                // no rules available for current selection => return only the products currently in cart
                var current_prods = self.getCartItems();
                products = products.filter(function (prod) {
                    return $.inArray(
                            parseInt(prod['entity_id']),
                            current_prods
                        ) != -1
                });
            }

            return hasFilters ? filtered : products;
        },

        /**
         * Searching through all families products to find at least one visible
         * @param valueTmp
         */
        displayFamily: function (valueTmp) {
            var oneVisible = false;
            $(valueTmp).find('.family-content .item-row').each(function(index, item) {
                if (item.style.display == 'block' || $(item).hasClass('package-item')) {
                    oneVisible = true;
                    return false;
                }
            });
            if (oneVisible) {
                $(valueTmp).css('display', 'block');
            } else {
                $(valueTmp).css('display', 'none');
            }
        },

        hasSelections: function () {
            var count = 0;
            $.each(this.cart, function (section, items) {
                if (items && items.length) {
                    count += items.length;
                }
            });
            return count;
        },

        getCartItems: function () {
            var products = [];
            $('.conf-block').find('.item-row.selected').each(function () {
                products.push(parseInt($.trim($(this).data('id'))));
            });
            return products.unique();
        },

        updateFilterStatus: function (products, type) {
            var block = $('#'+type+'-block');
            var statusText = block.find('.filter-status span');
            var element = block.find('.filter-input input');
            var searchString = $.trim(element.val());
            var searchFilters = this.filterValues[type] ? this.filterValues[type] : [];
            searchFilters = $.map(searchFilters.unique(), function (n, i) {
                return n && n.length ? n : null;
            });
            searchFilters = $.trim(searchFilters.join(', '));
            if (searchString.length) {
                searchFilters.length ? searchFilters += ', ' + searchString : searchFilters = searchString;
            }
            if (statusText.length) {

                var productsLength = 0;
                for (var i in products) {
                    if (products[i]['product_visibility'] == true) {
                        productsLength += 1;
                    }
                }

                if (searchFilters.length) {
                    statusText.html(statusText.data('message').format(productsLength, searchFilters)).show();
                } else {
                    statusText.html(statusText.data('default-message').format(productsLength)).show();
                }
            }
        },

        /**
         * Check if a selected item in the configurator has no products
         * If so, it sends the corresponding message
         * @param products
         * @param type
         */
        updateSelectedItemMessage: function (products,type) {
            var block = $('#'+type+'-block');
            var message = this.getSectionItemSelectableMessage(type);

            if (message != '') {
                block.find('.selected-item').data('no-item',Translator.translate(message));
                block.find('.no-item-message span.status').text(Translator.translate(message));
            }
        },

        getSectionItemSelectableMessage: function(sectionType) {
            var message = '';
            var productsNo = null;

            var block = $('#'+sectionType+'-block');
            var statusTextContainer = block.find('.filter-status span');
            var statusTextContainerHtml = statusTextContainer.text();

            if (statusTextContainerHtml !== '') {
                if (statusTextContainerHtml.indexOf('<strong>') !== -1) {
                    statusTextContainerHtml = statusTextContainerHtml.replace('<strong>','').replace('</strong>','');
                }
                productsNo = parseInt(statusTextContainerHtml.replace('products','').replace(' ',''), 10);
            }

            if (productsNo != NaN) {
                if (productsNo != 0) {
                    message = 'Please select';
                } else {
                    switch (sectionType) {
                        case 'hardware':
                        case 'device':
                            message = 'No hardware selectable';
                            break;
                        case 'goody':
                            message = 'No goodies selectable';
                            break;
                        case 'option':
                            message = 'No options selectable';
                            break;
                        case 'tariff':
                            message = 'No tariff selectable';
                            break;
                        case 'discount':
                            message = 'No discounts & promotions selectable';
                            break;
                        default:
                            message = 'No products selectable';
                    }
                }
            }

            return Translator.translate(message);
        },

        addPackage: function (package_id, callback) {
            this.http.post('cart.addPackage', {'product': package_id}, callback, false, true);
        },

        addSimOption: function (simType, makeCall) {
            simType = $.trim(simType);
            var self = this;
            var simSelect = $(this.options.container).find('.sim-select');
            var oldSim = simSelect.find('.sim-old');
            var newSim = simSelect.find('.sim-new');
            oldSim.find('input').prop('checked', false);
            newSim.find('input').prop('checked', true);

            triggerConfiguratorEvent('configurator.changesim', '');

            var validSimOptions = [];
            jQuery.each(jQuery('.radio.sim-new li:visible a'), function (id, el) {
                validSimOptions.push($(el).data('type'));
            });

            if (!!(simType && (typeof self.simType != 'undefined') && (self.simType == simType) && makeCall)) {
                return false;
            } else if (!!(simType && ($.inArray(simType, validSimOptions) != -1))) {
                self.simType = simType;
            } else if (simType == '') {
                self.simType = null;
            }

            newSim
                .find('.list-inline a')
                .removeClass('selected-sim')
                .show();

            $('a[data-type="{0}"]'.format(simType)).addClass('selected-sim');

            if (makeCall) {
                self.selectedSim = true;
                self.fromSimFooter = true;
                self.options.old_sim = false;
                self.addSelections(simSelect.parents('.conf-block').data('type'), true);
                self.fromSimFooter = false;
            }
        },

        clearSimOption: function (makeCall) {
            var self = this;
            var simSelect = $(this.options.container).find('.sim-select');
            var newSim = simSelect.find('.sim-new');

            newSim.find('input').prop('checked', false);
            newSim
                .find('.list-inline a')
                .removeClass('selected-sim')
                .hide();

            $(self.options.container)
                .find('.sim-select')
                .find('.sim-old')
                .find('input')
                .prop('checked', true);
            self.simType = null;
            if ((this.saleType == this.allowedSaleType) || this.isHollandsNieuwe) {
                self.options.old_sim = true;
            }

            if (makeCall) {
                self.clearSim = true;
                window.simCardChecker.setUseMySim(self.packageId);
                self.addSelections(simSelect.parents('.conf-block').data('type'), true);
            }
        },

        getDefaultSimOption: function (typeId, type) {
            var result = $.map(this.products[type], function (n, i) {
                return n && n['entity_id'] == typeId ? n : null
            })[0];
            return result && result['sim_type'] ? result['sim_type'] : null;
        },

        toggleSection: function (section, justUpdate) {
            if (this.templates.hasOwnProperty(section) && $.inArray(section, this.options.disabledSections)==-1) {
                configuratorUI.openSection(section, justUpdate);
            }
        },

        _closeBox: function ($box) {
            $box
                .find('.selection-block').removeClass('box-opened').addClass('box-closed')
                .find('.product-table').removeAttr('style').end()
                .removeAttr('style');
            /* also reset style */
            // Hide the "hide incompatible" button each time a section is closed
            $box.find('button.hide-incompatible').addClass('hidden');
        },

        addSelections: function (section, makeCall, externalCallBack, refreshConfigurator) {
            var self = this;
            makeCall = undefined === makeCall ? true : makeCall;
            var box = $('#'+section+'-block');
            var selectedItem = box.find('.selected-item');
            // send all the cart section ids; not only the ones selected in dom since some products might not be visible in the configurator
            var ids = self.cart[section];

            // Hide promo rules block
            $('#promo-rules').removeClass('open').find('.promo-rules-container').hide();

            var selectSections = function () {
                if (!self.cart[section]) {
                    self.cart[section] = [];
                }
                self.cart[section] = ids.unique();
                // Cart needs to remain persistent even though several products have product visibility in configurator set to none
            };

            var failed = false;

            var callback;
            if (typeof externalCallBack === "function") {
                callback = externalCallBack;
            } else {
                callback = function (data) {
                    var block = $(".side-cart.block[data-package-id='" + data.activePackageId + "']").find('.block-container')[0];
                    // if refresh configurator is set to true, than no further addMulti callback is necessary as it is irrelevant
                    if (refreshConfigurator === true && !$(block).closest('.side-cart').hasClass('active')) {
                        window.setPageLoadingState && window.setPageLoadingState(true);
                        window.setPageOverrideLoading(true);
                        activateBlock(block);
                        return;
                    } else {
                        refreshConfigurator = false;
                    }

                    if (data.hasOwnProperty('warningDefaulted')) {
                        showModalError(data.warningDefaulted);
                    }

                    if (data.hasOwnProperty('error') && data.error) {
                        if (data.hasOwnProperty('reload') && data.reload) {
                            window.location.reload();
                        } else {
                            if (data.hasOwnProperty('message')) {
                                showModalError(data.message);
                            }

                            self.lastAddedItem.removeClass('selected').removeClass('not-allowed').removeClass('multi-option');
                            self.cart[section].pop(self.lastAddedItem.data('id'));

                            if (self.cart[section].length == 0) {
                                var content = '<div class="no-item-message"><span>{0}</span></div>'.format(selectedItem.data('no-item'));

                                selectedItem.find('.content')[0].innerHTML = content;
                            }

                            return;
                        }
                        failed = true;
                        return;
                    }


                    if (data.hasOwnProperty('simError') && data.simError) {
                        showModalError(data.simError);
                        // remove selected sim
                        $('.conf-block[data-type="subscription"]')
                            .find('.sim-select .list-inline a.selected-sim').removeClass('selected-sim');
                    } else if (data.hasOwnProperty('sim') && data.sim) {
                        self.addSimOption(data.sim);
                    }

                    // Save a reference to the section containing the last added/removed product
                    self.lastSection = section;

                    selectSections();

                    self.setPackageId(data['activePackageId']);
                    self.applyRules(data);

                    if (configurator.ajaxQueueSize > 1 || window.checkoutEnabled === false) {
                        $('#cart_totals input').attr('disabled', 'disabled');
                    }

                    /* keep amount of promorules synchronized when callback is done */
                    if (data.promoRules) {
                        $('.promo-rules-duplicate').parent().removeClass('hidden');
                        $('.promo-rules-duplicate').html(data.promoRules);
                    }

                    if (data.hasOwnProperty('isHollandsNieuwe') && data.isHollandsNieuwe) {
                        self.isHollandsNieuwe = true;
                        $('.conf-block[data-type="subscription"]').parent().find('.sim-select').find('.sim-old').show();
                    } else if (self.saleType != self.allowedSaleType) {
                        $('.conf-block[data-type="subscription"]').parent().find('.sim-select').find('.sim-old').hide();
                    }
                    // Add selected class to all products from quote
                    if (data.hasOwnProperty('cartProducts')) {
                        //refresh each product row
                        $.each(self.cart, function (section, productIds) {
                            //parse each section of self.cart
                            $.each(productIds, function (id, productId) {
                                $(".product-table [data-id=" + productId + "]").removeClass('selected').removeClass('multi-option').removeClass('mandatory-row').removeClass('hide_not_selected_products').removeClass('not-allowed');
                            });
                        });

                        self.cart = {};
                        self.acqCart = [];
                        self.ilsCart = [];

                        // set products states in configurator UI
                        self.highlightCartProducts(data.cartProducts, data.processContextCode);

                        $.each(self.cart, function (section, productIds) {
                            self.toggleSection(section, true);
                        });
                    }

                    if (data.hasOwnProperty('quoteHash')) {
                        root.quoteHash = data.quoteHash;
                    }

                    if (data.hasOwnProperty('salesId')) {
                        root.SALES_ID = data.salesId;
                    }

                    if (data.hasOwnProperty('processContextCode')) {
                        root.processContextCode = data.processContextCode;
                    }
                    // Remove the defaulted products from the configurator visually
                    // Not needed anymore: logic moved to parsing productsInCart addMulti response node

                    // Logic moved to parsing cartProducts response within addMulti call

                    //if automatically selected an addon, refresh the addon block
                    if (automatically_refresh_selection) {
                        //configurator.toggleSection(automatically_refresh_selection, true);
                        window.setPageLoadingState && window.setPageLoadingState(false);
                        automatically_refresh_selection = false;
                    }

                    if (store.get('btwState') == null || store.get('btwState') == true) {
                        ToggleBullet.switchOn('.tax-toggle');
                        sidebar.showSummarizedPrice();
                    } else {
                        ToggleBullet.switchOff('.tax-toggle');
                        sidebar.showDetailedPrice();
                    }
                    if (typeof data.cartProducts !== 'undefined' && Object.keys(data.cartProducts).length) {
                        $('#configurator_checkout_btn').removeClass('disabled');
                        $('#configurator_checkout_btn').removeClass('incomplete');
                        $('#configurator_checkout_btn').addClass('canProceed');
                    } else {
                        $('#configurator_checkout_btn').removeClass('canProceed');
                        $('#configurator_checkout_btn').addClass('disabled');
                        $('#configurator_checkout_btn').addClass('incomplete');
                    }

                    if (data.hasOwnProperty('rightBlock')) {
                        togglePackage(jQuery('.cart_packages').find('div[data-package-id="' + data.activePackageId + '"]').find('.side-block-activate'));
                    }

                    jQuery('[data-toggle="tooltip"]').tooltip();
                };
            }

            var simSelect = $('.conf-block[data-type="subscription"]').parent().find('.sim-select');
            var oldSimSelect = simSelect.find('.sim-old');
            var newSimSelect = simSelect.find('.sim-new');

            if (self.subscriptionIdentifier != '') {
                if (self.cart[self.subscriptionIdentifier] && self.cart[self.subscriptionIdentifier].length) {
                    var checkIfIsSimOnly = $.map(self.products[self.subscriptionIdentifier], function (i, y) {
                        if (i.entity_id == self.cart[self.subscriptionIdentifier][0]) {
                            return i;
                        }
                    });
                }
            }

            if (!this.clearSim) {
                var subscriptionIdentifier = self.subscriptionIdentifier;
                var deviceIdentifier = self.deviceIdentifier;

                if ((self.cart[subscriptionIdentifier] && self.cart[subscriptionIdentifier].length)
                    && (
                        (checkIfIsSimOnly.length > 0 && checkIfIsSimOnly[0]['sim_only'] == true)
                        || (self.cart[deviceIdentifier] && self.cart[deviceIdentifier].length)
                    )
                ) {
                    if (simSelect.length && ((this.saleType == this.allowedSaleType)
                        || this.isHollandsNieuwe)
                    ) {
                        oldSimSelect.show();
                        if (self.options.old_sim) {
                            self.clearSimOption();
                        } else if ((typeof self.selectedSim == 'undefined' || self.selectedSim == false) && ((this.saleType == this.allowedSaleType) || this.isHollandsNieuwe ) && !self.simType) {
                            // Use old sim as default for retention packages
                            self.options.old_sim = true;
                        }
                    } else {
                        newSimSelect.find('input').prop('checked', true);
                        newSimSelect
                            .find('.list-inline a')
                            .removeClass('selected-sim')
                            .show();
                        oldSimSelect.hide();
                    }

                    if ((self.cart[deviceIdentifier] && self.cart[deviceIdentifier].length && (typeof self.products[self.deviceIdentifier]) != 'undefined') && !self.fromSimFooter) {
                        var checkIfHasSim = $.map(self.products[self.deviceIdentifier], function (i, y) {
                            if (i.entity_id == self.cart[self.deviceIdentifier][0]) {
                                return i;
                            }
                        });
                        if (checkIfHasSim.length > 0 && checkIfHasSim[0].sim_type == '' && (typeof self.selectedSim == 'undefined' || self.selectedSim == false)) {
                            this.simType = null;
                        }
                    }
                    if ((typeof self.selectedSim == 'undefined' || self.selectedSim == false) && (this.saleType == this.allowedDefaultSim) && !this.isHollandsNieuwe) {
                        if (typeof checkIfIsSimOnly == 'undefined' && checkIfHasSim.length > 0 && checkIfIsSimOnly[0]['sim_only']) {
//                            var typeId = self.cart[self.subscriptionIdentifier][0];
//                            var type = self.subscriptionIdentifier;
                        } else if (section == deviceIdentifier) {
                            var typeId = self.cart[deviceIdentifier][0];
                            var type = deviceIdentifier;
                            this.addSimOption(this.getDefaultSimOption(typeId, type));
                        } else {
                            this.addSimOption(self.simType);
                        }
                    }
                    simSelect.show();
                    simSelect.parents('.conf-block').find('.block-footer').removeClass('hidden');
                } else {
                    simSelect.hide();
                    simSelect.parents('.conf-block').find('.block-footer').addClass('hidden');
                    self.clearSimOption();
                }
            }

            if (makeCall) {
                this.addMultiToCart(ids, box.data('type'), callback, true, refreshConfigurator);
            } else {
                selectSections();
            }

            if (failed) {
                self.lastSection = false;
                return;
            }
        },

        chooseNewSim: function (item) {
            this.simType = null;
            this.options.old_sim = false;
            $(item).parents('.sim-new').find('a').css('font-weight', 'normal').show();
        },

        // This function will change item-row to include an text input and a submit button that will trigger serialValidation and selectProduct
        selectSerialNumber: function (item) {
            jItem = $(item);

            // Skip if this click focuses the serial input
            if (jItem.closest('.serial-input').find("input[type='serial-number']").is(':focus')) {
                return;
            }

            // If item has class selected, continue to selectProduct
            if (jItem.hasClass('selected')) {
                // Item is already selected, clear data-serial value
                jItem.attr('data-serial', '');
                // Hide ok button
                jItem.find('.btn-ok').addClass('hidden');
                // Show check button
                jItem.find('.btn-check').removeClass('hidden');
                // Continue
                this.selectProduct(item);
            } else if (jItem.attr('data-serial').length) {
                // Switch back to normal product view
                jItem.removeClass('smartcard-item-no-padding');
                jItem.find('.product-item').removeClass('hidden');
                jItem.find('.serial-input').addClass('hidden');

                // If item has data-serial, it means it has already been validated, so continue to selectProduct
                this.selectProduct(item);
            } else {
                // Show form for serial input
                jItem.find('.product-item').addClass('hidden');
                jItem.find('.serial-input').removeClass('hidden');
                jItem.addClass('smartcard-item-no-padding');
            }
        },
        // We do not need to know what type of hardware it is as validation will be made through an ajax call
        // The serial inputted for current product will be set on customer session and later set on package in addMulti call
        validateSerialNumber: function (item) {
            jQuery(item).val("").addClass('smartcard-loader');
            item = $(item);
            var serialNumber = item.closest('.serial-input').find("input[name='serial-number']").val();
            var productId = item.closest('.item-row').attr("data-id");
            var self = this;

            if (!serialNumber.length) {
                this.setSerialError(item, Translator.translate("You need to enter the device serial number."));
                return false;
            }

            var isNumber = /^[0-9]+$/.test(serialNumber);
            if (!isNumber) {
                this.setSerialError(item, Translator.translate("You need to enter only digits."));
                return false;
            }

            // Make ajax call for serial validation and device compatibilty
            $.ajax({
                url: '/configurator/cart/validateHardwareSerial/',
                type: "post",
                data: ({
                    serialNumber: serialNumber,
                    productId: productId
                }),
                dataType: "json",
                success: function (response) {
                    // Notify agent that there is an error regarding the inputted serial number
                    if (response.error) {
                        self.setSerialError(item, response.message);
                    } else {
                        // No error so let's set data-serial on closest item-row and trigger click
                        item.closest('.item-row').attr('data-serial', serialNumber);
                        // remove old popover
                        item.popover('destroy');
                        item.attr('data-content', '');

                        // Set valid class on text input and add on change event to rollback to Check button
                        // NOTE: Not using on change event because it's too slow (config-wrapper includes all products available to this package)
                        item.closest('.serial-input')
                            .find("input[name='serial-number']")
                            .on('click', function () {
                                $(this).attr('class', '');
                                // Remove data-serial from item-row, as it will be later set when clicking the Check button
                                item.closest('.item-row').attr('data-serial', "");
                                item.val(Translator.translate("Check")).removeClass('smartcard-success');
                            });
                        item.val("").addClass('smartcard-success');
                    }
                }
            });
        },

        setSerialError: function(jElement, errorMessage) {
            if(jQuery(jElement).hasClass('smartcard-loader')){
                jQuery(jElement).removeClass('smartcard-loader').val(Translator.translate('Check'));
            }
            var serialNode = jQuery('.serial-input');
            serialNode.find('.field-row').addClass('serial-error');
            serialNode.find('input[data-toggle=popover]').attr('data-content',errorMessage).popover('show');
        },
        /**
         * Determine whether or not a product is bundleComponent
         * @param productId
         * @returns {boolean}
         */
        productIsBundleComponent: function (productId)
        {
            var bundleComponent = false;
            var bundleComponents = this.bundleComponents.map(function (a) { return parseInt(a.productId);});

            if (bundleComponents.indexOf(parseInt(productId)) !== -1) {
                bundleComponent = true;
            }

            return bundleComponent;
        },

        /**
         * Determine whether a product id belongs to a certain subtype
         * @param productId
         * @param section
         * @returns {boolean}
         */
        productIsOfType: function (productId, section)
        {
            var is = false;
            // iterate through all configurator products in section (if exist)
            this.products && this.products[section] && this.products[section].every(function (product) {
                if (parseInt(product.entity_id) === productId) {
                    is = true;
                }
                // break loop when found
                return !is;
            }.bind(this));

            return is;
        },

        /**
         * Determine whether or not clicking on a product in configurator will break a bundle
         * Will iterate through each tariffs and if one that is bundle component is selected will show warning
         * @param productId
         * @returns {boolean}
         */
        productBreaksBundle: function (productId)
        {
            var breaks = false;

            // Special case for fixed packages in which when clicking a tariff will remove all other tariffs from cart
            if (FIXED_PACKAGE_TYPES.indexOf(this.options.type) !== -1) {
                // Iterate through all subscription identifiers
                SUBSCRIPTION_IDENTIFIERS.forEach(function (lowerCaseIdentifier) {
                    // If cart has the subscription section
                    if (this.cart.hasOwnProperty(lowerCaseIdentifier)) {
                        // Clicked on a tariff
                        this.productIsOfType(productId, lowerCaseIdentifier) && this.cart[lowerCaseIdentifier].every(function (cartProductId) {
                            // Already a tariff in cart, whatever tariff the agent clicked will break the bundle
                            if (this.productIsBundleComponent(cartProductId)) {
                                breaks = true;
                                // equivalent of break in a loop, will stop at the first that evals to true
                                return false;
                            }
                        }.bind(this));
                    }
                }.bind(this));
                breaks = breaks ? breaks : this.productIsBundleComponent(productId);
            } else {
                return this.productIsBundleComponent(productId);
            }

            return breaks;
        },

        /**
         * Checks if a product is the related_product of a Red+ gorup
         * @param productId
         * @returns {boolean}
         */
        itemIsRedPlusRelatedProduct: function(productId) {
            var self = this;
            for (var i = 0; i < self.activeBundles.length; i++) {
                if (self.activeBundles[i].redPlusBundle && self.activeBundles[i].packages) {
                    for (var b = 0; b < self.activeBundles[i].packages.length; b++) {
                        if (self.activeBundles[i].packages[b].relatedProductId.indexOf(productId) !== -1 &&
                            self.activeBundles[i].packages[b].packageId == configurator.packageId) {
                            return true;
                        }
                    }
                }
            }

            return false;
        },
        hasSusoBundle: function () {
            var has = false;
            this.activeBundles.forEach(function (bundle) {
                if (bundle['susoBundle']) {
                    has = true;
                }
            });

            return has;
        },
        itemBreaksSusoBundle: function(productId) {
            var breaks = false;
            if (!this.activeBundles) {
                return false;
            }

            this.activeBundles.every(function (activeBundle) {
                if (!activeBundle.susoBundle) {
                    // continue to next bundle, this one is not suso
                    return true;
                }

                breaks = this.productBreaksBundle(productId);
            }.bind(this));

            return breaks;
        },

        breakSusoBundle: function() {
            var self = this;

            self.showSusoModal = false;
            var refreshConfigurator = self.options.type == PREPAID_PACKAGE;
            self.selectProduct(self.susoRelatedItem, true, false, refreshConfigurator);
            self.showSusoModal = true;
            self.susoRelatedItem = null;
        },
        /**
         * calls selectProduct to break the Red+ group
         */
        breakRedPlusGroup: function() {
            var self = this;

            self.showRedPlusModal = false;
            self.selectProduct(self.redPlusRelatedItem, true);
            self.showRedPlusModal = true;
        },

        breakBundle: function() {
            var self = this;

            self.showBundleBreakingModal = false;
            jQuery('#cancel-bundle-modal').modal('hide');

            self.selectProduct(self.bundleComponentItem, true);
            self.showBundleBreakingModal = true;
        },

        selectProduct: function (item, makeCall, externalCallBack, refreshConfigurator) {
            var self = this;
            makeCall = undefined !== makeCall ? makeCall : true;
            item = $(item);
            if (item.hasClass('not-allowed')) {
                return false;
            }

            var productId = item.data('id');
            var itemDeselection = item.hasClass('selected');

            var section = this.getProductType(productId);
            var handleMethod = 'select{0}'.format(section.charAt(0).toUpperCase() + section.slice(1));

            if (self.showSusoModal === true && self.itemBreaksSusoBundle(productId)) {
                var susoWarningModal = jQuery('#break-suso-after-deselecting-modal');
                self.susoRelatedItem = item;
                susoWarningModal.modal();
                return;
            }

            if (itemDeselection && self.itemIsRedPlusRelatedProduct(productId) && self.showRedPlusModal === true) {
                var redPlusWarningModal = jQuery('#break-redplus-after-deselecting-modal');
                self.redPlusRelatedItem = item;
                redPlusWarningModal.modal();
                return;
            }

            if (self.productBreaksBundle(productId) && self.showBundleBreakingModal === true && self.showSusoModal === true && self.showRedPlusModal === true) {
                var bundleComponentModal = jQuery('#cancel-bundle-modal');
                self.bundleComponentItem = item;
                bundleComponentModal.modal();
                return;
            }

            // if current clicked item is not the selected one, search for current selected item
            // to identify if clicking another tariff will break the bundle
            var currentSelectedItem = false;
            if (!itemDeselection) {
                item.parent().children('div').each(function () {
                    if ($(this).hasClass('selected')) {
                        currentSelectedItem = $(this).data('id');
                        return false;
                    }
                });

                if (currentSelectedItem &&
                    currentSelectedItem != productId &&
                    self.productBreaksBundle(currentSelectedItem) &&
                    self.showBundleBreakingModal === true &&
                    self.showSusoModal === true &&
                    self.showRedPlusModal === true) {
                    var bundleComponentModal = jQuery('#cancel-bundle-modal');
                    self.bundleComponentItem = $('#item-product-'+productId);
                    bundleComponentModal.modal();
                    return;
                }
            }

            /** Do nothing if item is not changeable **/
            if (item.hasClass('mandatory-row')) {
                return;
            }

            // Hide all the disabled items from the section once a product has been clicked
            item.parents('.content').find('.item-row.incompatible-row').each(function (id, el) {
                if ($(el).css("display") == "block") {
                    $(el).css("display", 'none');
                    $(el).removeClass('incompatible-row');
                    $(el).find('.item-pricing').removeClass('hidden');
                }
            });

            // If the item has class 'exclusive-row' it means we need to deselect the other product prior to selecting it, as it is part of a mutually-excluded family
            if (item.hasClass('exclusive-row')) {
                var removeFromExclusiveFamily = [];
                item.parent().find('.item-row.selected').each(function (id, el) {
                    $(el).removeClass('selected');
                    $(el).removeClass('multi-option');
                    removeFromExclusiveFamily.push($(el).data('id'));
                });
            }


            this.lastAddedItem = item;
            // handle subscription sim only
            if (section == self.subscriptionIdentifier && !item.hasClass('selected')) {
                var product = this.products[section].filter(function (prod) {
                    return prod['entity_id'] == productId
                }).first();
                this.updateSimsList(product.identifier_simcard_allowed);
                simCardChecker.checkPackageSubscription(product, this.selectedSim || this.options.old_sim, this.packageId);
            }

            if (root.getType(this[handleMethod]) === 'function') {
                this[handleMethod](item);
            } else {
                if ((makeCall && (item.hasClass('disabled') || item.hasClass('disabled-row'))) || item.parents('.selected-item').length) {
                    return;
                }
                var box = item.parents('.conf-block');

                // Already selected so we need to deselect it
                if (item.hasClass('selected')) {
                    var greyOut = false;
                    if (item.parents('.family').find('.family-header .family-status .mandatory:visible').length > 0) {
                        if (item.siblings('.item-row:visible').length == 0) {
                            return;
                        }
                    }

                    // remove given label
                    item.find('span.item-label').removeClass('given');

                    if(section == 'devicerc') {
                        return;
                    }
                    if (section == 'device' || section == 'subscription') {
                        if (jQuery('#devicerc-block .item-row.selected').length > 0) {
                            jQuery('#devicerc-block .item-row.selected').removeClass('selected');
                            this.cart['devicerc'] = [];
                            var selectedItem = jQuery('#devicerc-block').find('.selected-item');
                            selectedItem.find('.content')[0].innerHTML = '<div class="no-item-message"><span>{0}</span></div>'.format(selectedItem.data('no-item'));
                            selectedItem.find('.block-header .left p span').first().hide();
                        }
                    }

                    item.removeClass('selected');
                    item.removeClass('multi-option');
                    var remainingSelectedItems = item.parents('.content').find('.item-row.selected').filter(function (index) {
                        return $(this).parents('.conf-block').data('type') == section;
                    });
                    if (remainingSelectedItems.length == 1) {
                        remainingSelectedItems.removeClass('multi-option');
                    }
                    //remove hints as well
                    window.configuratorUI.updateHintsOnDeselect(section);

                    if (!this.cart[section]) {
                        this.cart[section] = [];
                    } else {
                        productId = parseInt(productId);
                        delete this.cart[section][$.inArray(productId, this.cart[section])];
                        this.cart[section] = $.map(this.cart[section].unique(), function (n, i) {
                            return n ? n : null;
                        });
                    }
                // We need to select the item
                } else {
                    item.addClass('selected');

                    var greyOut = true;

                    if (!this.cart[section]) {
                        this.cart[section] = [];
                    }
                    // Block is of type multi
                    if (!box.data('multi')) {
                        item.siblings().removeClass('selected').removeClass('multi-option');
                        item.parents('.product-table').find('.item-row').removeClass('selected').removeClass('multi-option');
                        item.addClass('selected');
                        this.cart[section] = [productId];
                    } else {
                        this.cart[section].push(productId);
                        this.cart[section] = $.map(this.cart[section].unique(), function (n, i) {
                            return n ? n : null;
                        });
                    }

                    // add multi-option class if multiple products are selected
                    var previouslySelectedItems = item.parents('.content').find('.item-row.selected').filter(function (index) {
                        return $(this).parents('.conf-block').data('type') == section;
                    });
                    if (previouslySelectedItems.length > 1) {
                        previouslySelectedItems.each(function (id, el) {
                            $(el).addClass('multi-option');
                        });
                    }
                }
            }

            if (makeCall) {
                this.cart[section] = $(this.cart[section]).not(removeFromExclusiveFamily).get();
                this.selectedSim = false;
                this.addSelections(section, makeCall, externalCallBack, refreshConfigurator);
                // Get the current section
                var currentSection = $('.conf-block.panel[data-type=' + section + ']');
                // If a product is selected in this section -> automatically go to the next section in the configurator
                if (currentSection.data('multi') != true) {
                    if (!itemDeselection && currentSection.parent().find('[data-index=' + (currentSection.data('index') + 1) + ']')) {
                        this.toggleSection(currentSection.parent().find('[data-index=' + (currentSection.data('index') + 1) + ']').data('type'), false);
                    }
                }
            }

            if (greyOut != undefined && item.parents('.family').length > 0) {
                var familyId = item.parents('.family').data('family-id');
                var family = false;
                window.families.every(function (el) {
                    if (el.entity_id == familyId) {
                        family = el;
                        return false;
                    }
                    return true;
                });
                if (family && (family.hasOwnProperty('allow_mutual_products') && family.allow_mutual_products == 1)) {
                    if (greyOut == true) {
                        item.siblings().addClass('exclusive-row');
                    } else {
                        $.each(item.siblings(), function (index, value) {
                            $(value).removeClass('exclusive-row');
                        });
                    }
                }
            }
        },

        applyRules: function (data) {
            var self = this;
            $('.item-row').each(function () {
                $(this).removeClass('disabled');
            });
            if (data.hasOwnProperty('totalMaf')) self.packageTotals.maf = data.totalMaf;
            if (data.hasOwnProperty('totalPrice')) self.packageTotals.price = data.totalPrice;

            /* update side cart */
            var rightBlock = jQuery('.cart_packages');
            var cartSidebar = rightBlock.find('.content').first();
            var cartTotal = rightBlock.find('.coupon-section').first();
            var totals = $('#cart_totals');
            var products = [];
            $.each(self.products, function (section, items) {
                if (self.cart[section]) {
                    $.each(items, function (key, item) {
                        for (var i = 0; i < self.cart[section].length; i++) {
                            if (item['entity_id'] == self.cart[section][i]) {
                                products.push(item);
                            }
                        }
                    })
                }
            });

            /* make sure duplicate products are not added in the array */
            products = $.unique(products);
            rightBlock.replaceWith(data.rightBlock);

            if(data.hasOwnProperty('bundleChoiceBlock')) {
                $('#bundle-options').replaceWith(data.bundleChoiceBlock);
            }

            if (data.hasOwnProperty('totals')) totals.replaceWith(data.totals);

            self.packagePromoRules(data, data.activePackageId);
        },

        packagePromoRules: function (data, packageId) {
            /* check for promo rules after selection */
            if (data.hasOwnProperty('promoRules') && data.promoRules != 0) {
                $('div[data-package-id="{0}"]'.format(packageId)).find('.promo-rules-trigger span').text(data.promoRules).parent().removeClass('hidden');
            } else {
                $('div[data-package-id]').find('.promo-rules-trigger').addClass('hidden');
            }
        },

        changeSelection: function (section) {
            var block = $('#{0}-block'.format(section));
            var selectedItemBlock = block.find('.selected-item');
            selectedItemBlock.slideUp('fast', function () {
                block
                    .find('.selection-block')
                    .slideDown('fast', function () {
                        selectedItemBlock.find('.content')[0].innerHTML = '';
                    });
                block.find('.item-row.selected').show();
            });
        },

        deletePackages: function () {
            this.http.post('cart.empty', {}, function () {
                window.location.reload();
            }, null, true);
        },

        // middle state with the packages
        showBundleDetailsToaster: function (bundleId, activeBundleExists) {
            var self = this;
            if (bundleId) {
                var $_modalTarget = $j('#bundleModal');
                $_modalTarget.on('shown.bs.modal', function (e) {
                    $('div.modal-backdrop').hide();
                });

                var data = {
                    bundle_id: bundleId
                };

                this.http.ajax('POST', 'bundles.getHint', data, true).done(function (data) {
                    if (data.hasOwnProperty('error') && data['error'] == true) {
                        showModalError(data['message']);
                    } else {
                        $_modalTarget.find('.modal-content').html(data.html);
                        $_modalTarget.appendTo('body').modal();

                        // when a confirmation pop-up is visible hide the bundle toaster @VFDED1W3S-5881
                        if ($('#message-dialog-warning').hasClass('message-warning-enabled')) {
                            configuratorUI.hideBundleToaster();
                        }

                        configurator.showBundleBarInformationToaster();
                        if (activeBundleExists !== undefined && activeBundleExists === true) {
                            self.markSelectedBundleProductsIDs(data.bundleProductsIds);
                            $j('#bundleModal').find('input[type="submit"]').attr('disabled', true);
                        }

                        self.preselectSingleBundle();
                    }
                });
            }
        },
        /**
         * If there is just one cart package and no install base package, preselect it in the bundle drawer
         */
        preselectSingleBundle: function() {
            var bundleModalContainer = $('#bundle-alternatives');
            var cartPackages = bundleModalContainer.find('button.bundle-package-entry');

            if (cartPackages.length >= 1) {
                $(cartPackages[cartPackages.length-1]).trigger('click');
            }
        },
        selectRedPlusParentPackage: function(packageId, bundleId, object) {
            var self = this;

            if (jQuery(object).hasClass('btn-green-selected')) {
                return;
            }

            if (packageId) {
                var data = {
                    parentPackageId : packageId,
                    bundleId: bundleId
                };
                this.http.ajax('POST', 'redplus.updatePackageParent', data, true).done(function(data) {
                    if (data.hasOwnProperty('error') && data['error'] == true) {
                        showModalError(data['message']);
                    } else {
                        jQuery('button[data-redplus-related-product].btn-green-selected')
                            .removeClass('btn-green-selected')
                            .addClass('btn-green');

                        if (data.hasOwnProperty('rightBlock')) {
                            var rightBlock = jQuery('.cart_packages');
                            rightBlock.replaceWith(data.rightBlock);
                            jQuery(object).toggleClass('btn-green btn-green-selected');
                            activateBlock(jQuery('.cart_packages').find('div[data-package-id="' + data.packageId + '"]').find('.side-block-activate'));
                            togglePackage(jQuery('.cart_packages').find('div[data-package-id="' + data.packageId + '"]').find('.side-block-activate'));

                        }
                    }
                });
            }
        },
        moveMemberAfterConfirmation: function() {
            var self = this;

            self.showReservedMemberConfirmation = false;
            self.addRedPlusMemberFromConfigurator(self.installBaseOwnerSubscription);
            jQuery('#redplus-reserved-member-warning').modal('toggle');
            self.showReservedMemberConfirmation = true;
        },
        addRedPlusMemberFromConfigurator: function(element) {
            var self = this;
            var button = jQuery(element);
            var data = {};
            data.sharingGroupId = button.data('sharing-group-id');
            data.relatedProductSku = button.data('redplus-related-product');
            data.customerId = button.data('customer-id');
            data.productId = button.data('product-id');
            data.bundleId = button.data('bundle-id');
            var hasReservedMember = button.data('has-reserved-member');

            if( button.hasClass('btn-green-selected') ) {
                // Stop execution if current option is already selected
                return;
            }

            if (hasReservedMember && self.showReservedMemberConfirmation) {
                var modal = jQuery('#redplus-reserved-member-warning');
                modal.find('.confirmation-message').html(Translator.translate('The customer has future reserved activations for this Red+ group.'));
                self.installBaseOwnerSubscription = element;
                modal.find('#break-redplus-button').attr('onclick', 'configurator.moveMemberAfterConfirmation()');
                modal.modal();

                return;
            }

            if( button.hasClass('btn-green-selected') ) {
                // Stop execution if current option is already selected
                return;
            }

            jQuery('button[data-redplus-related-product].btn-green-selected')
                .removeClass('btn-green-selected')
                .addClass('btn-green');

            setPageLoadingState(true);
            $.ajax({
                type: 'POST',
                url: '/configurator/cart/addRedPlusMemberFromConfigurator',
                data: data,
                success: function(response) {
                    setPageLoadingState(false);
                    jQuery('.cart_packages').replaceWith(response.rightBlock);
                    button.toggleClass('btn-green btn-green-selected');
                    togglePackage(jQuery('.cart_packages').find('div[data-package-id="' + response.packageId + '"]').find('.side-block-activate'));

                    // update the create package buttons in shopping cart
                    if (response.hasOwnProperty('eligibleBundles')) {
                        self.toggleRedPlusCartButton(response.eligibleBundles);
                    }
                    // on this flow of Red+ will always return a redplus package id, no need of initConfigurator call
                    configurator.packageId = configurator.options.packageId = response.packageId;
                }
            });
        },
        toggleRedPlusCartButton: function(bundles) {
            var redPlusCartButton = $('#package-types').find('[data-target="redplus"]:first');
            redPlusCartButton.addClass('disabled');
            bundles.forEach(function(bundle) {
                if (bundle.redPlusBundle) {
                    redPlusCartButton.removeClass('disabled');
                }
            });
        },
        closeBundleDetailsModal: function() {
            $('#bundleModal').modal('hide');
        },
        addToCartBundle: function(element, fromConfigurator) {
            var self = this;
            var bundleId = $(element).data('bundle-id');
            var productId = $(element).data('product-id');
            var subscriptionNumber = $(element).data('subscription-number');
            var interStackPackageId = $(element).data('inter-stack-package-id');

            if (typeof fromConfigurator === undefined) {
                fromConfigurator = false;
            }

            var data = {
                bundleId: bundleId,
                subscriptionNumber: subscriptionNumber,
                productId: productId,
                interStackPackageId: interStackPackageId
            };

            // Avoid executing multiple ajax calls if button is pressed again (see OMNVFDE-3309)
            $(element).attr('onclick','');


            var callBack = function() {
                setPageLoadingState(true);
                self.http.ajax('POST', 'bundles.create', data, true).done(function (response) {
                    if (response.error) {
                        showModalError(response.message);
                    } else {
                        self.closeBundleDetailsModal();
                        // after done updating DOM, update customer section eligible bundles and trigger initConfigurator for newly created redPlus package
                        $('.cart_packages')
                            .replaceWith(response.rightBlock)
                            .promise()
                            .done(function () {
                                if (response.hasOwnProperty('eligibleBundles')) {
                                    customerSection.updateMyProductsEligibleBundles(response.eligibleBundles);
                                }
                                // initConfigurator for Red+ package
                                if (response.hasOwnProperty('initNewPackage') && response.initNewPackage) {
                                    var redPlusPackage = $('.cart_packages')
                                        .find('[data-package-id="' + response.initNewPackage + '"]')
                                        .first()
                                        .find('.block-container');
                                    activateBlock(redPlusPackage);
                                } else {
                                    var refreshBundles = true;

                                    if (configurator.packageId !== null && configurator.packageId !== undefined) {
                                        togglePackage($('.cart_packages').find('[data-package-id="' + configurator.packageId + '"]').find('.side-block-activate'));

                                        initConfigurator(
                                            $('.cart_packages').find('[data-package-id="' + configurator.packageId + '"]').first().find('.block-container').parents('.side-cart').attr('data-package-type'),
                                            $('.cart_packages').find('[data-package-id="' + configurator.packageId + '"]').first().find('.block-container').parents('.side-cart').attr('data-package-id'),
                                            $('.cart_packages').find('[data-package-id="' + configurator.packageId + '"]').first().find('.block-container').parents('.side-cart').attr('data-sale-type'),
                                            null, null, null, null, null, null, null, null, null, null,
                                            $('.cart_packages').find('[data-package-id="' + configurator.packageId + '"]').first().find('.block-container').parents('.side-cart').attr('data-package-creation-type-id')
                                        );

                                        refreshBundles = false;
                                    }
                                    if (refreshBundles) {
                                        // call eligible bundles again (otherwise init configurator will auto call them)
                                        self.getEligibleBundles();
                                    }
                                }
                                if (response.hasOwnProperty('optionsPanel')) {
                                    $('#config-wrapper').prepend(response.optionsPanel);
                                }
                                $(window).trigger('resize');
                            });
                    }
                });
            };

            if (fromConfigurator) {
                configurator
                    .selectProduct($(element).closest('.item-row'), true, callBack);
            } else {
                callBack();
            }
        },
        markSelectedBundleProductsIDs: function(bundleProductsIds) {
            var content = jQuery('#config-wrapper').find('.content');
            $.each(bundleProductsIds, function(productId) {
                var productRow = content.find('.selected.item-row[data-id="'+productId+'"]');
                productRow.attr('data-bundles-component', true);
            });
        },
        // @todo refactor the 3 states for the bundle details
        /**
         * $_modalDialog.removeClass('middle-state');
         $_modalDialog.addClass('first-state');
         */
        showBundleBarInformationToaster: function () {
            var $_modalDialog = $('#bundleModal');
            var $_cartTotals = $('#cart_totals');
            var $_minimiseToasterButton = $('#closeToasterButton');

            if ($('#bundleModal .bundle-to-cart-btn input[type=submit]').hasClass('disabled')) {
                $('.info-btn-bundle').addClass('disabled');
            } else {
                $('.info-btn-bundle').removeClass('disabled');
            }

            $_modalDialog.removeClass('middle-state');
            $_modalDialog.addClass('first-state');
            $_minimiseToasterButton.hide();

            configurator.assureElementsHeight();
        },
        showBundleBarMainDetailsToaster: function () {
            var $_modalDialog = $('#bundleModal');
            var $_minimiseToasterButton = $('#closeToasterButton');
            $('.info-btn-bundle').addClass('disabled');
            $_modalDialog.removeClass('first-state');
            $_modalDialog.addClass('middle-state');
            $_minimiseToasterButton.show();
        },
        showBundlesAlternatives: function(element) {
            this.baseAlternatives = element;
            var bundleAlternatives = $("#bundleModalAlternatives");
            var modalBody = $('#bundle-alternatives');
            var bundleInput = $('#bundleModal .bundle-to-cart-btn input[type=submit]');
            var selectedChangeablePackage = $(element).closest('.bundle-package-block').data('bundle-ui-section-id');

            $(modalBody.find('.bundle-alternatives-packages')[0]).data('bundle-ui-section-id',selectedChangeablePackage);

            $.each(modalBody.find('.btn-green'),function(key, button){
                if( $(button).data('product-id') == bundleInput.attr('data-product-id') &&
                    $(button).data('bundle-id') == bundleInput.attr('data-bundle-id') &&
                    $(button).data('contract-id') == bundleInput.attr('data-contract-id') &&
                    $(button).data('entity-id') == bundleInput.attr('data-entity-id'))
                {
                    $(button).addClass('disabled');
                    $(button).text(Translator.translate('Selected'));
                }else{
                    $(button).removeClass('disabled');
                    $(button).text(Translator.translate('Select'));
                }
            });

            bundleAlternatives.find('.modal-body').html(modalBody.html());

            bundleAlternatives.modal();

            $('.modal-backdrop.in:visible').css('z-index', 10040);
        },
        http: {

            endpoints: {},

            _ajaxQueue: $({}),

            spinnerId: '#spinner',

            addEndpoint: function (key, url) {
                this.endpoints[key] = url;
            },

            removeEndpoint: function (key) {
                if (key in this.endpoints) {
                    delete this.endpoints[key];
                }
            },

            hasEndpoint: function (key) {
                return key in this.endpoints;
            },

            getEndpoint: function (key) {
                if (!this.hasEndpoint(key)) {
                    throw new Error('Endpoint not present');
                }
                return this.endpoints[key];
            },

            setLoading: function (state) {
                /**
                 * If window has spinner and its value is false, do nothing
                 */
                if (false === window.canShowSpinner) {
                    return;
                }

                var loader = $(this.spinnerId);
                loader.find('img').css({
                    marginTop: ($(window).height() / 2) - 20
                });
                $(this.spinnerId).toggle(state);
                this.waiting = state ? true : false;
            },

            get: function (url, data, callback, errback, cache, async) {
                callback && getType(callback) === 'function' || (callback = function (data) {
                    return data;
                });
                errback && getType(errback) === 'function' || (errback = function (xhr, errMes) {
                    showModalError(errMes);
                });
                var response, status;

                this
                    .ajax('GET', url, data, async)
                    .done(function (res, sts) {
                        response = res;
                        status = sts;
                    })
                    .then(function () {
                        response = callback(response, status)
                    })
                    .fail(errback);

                return response;
            },

            post: function (url, data, callback, errback, async) {
                callback && getType(callback) === 'function' || (callback = function (data) {
                    return data;
                });
                errback && getType(errback) === 'function' || (errback = function (xhr, errMes) {
                    showModalError(errMes);
                });
                var response, status;
                this
                    .ajax('POST', url, data, async === undefined ? true: async)
                    .done(function (res, sts) {
                        response = res;
                        status = sts;
                    })
                    .then(function () {
                        response = callback(response, status)
                    })
                    .fail(errback);

                return response;
            },

            ajax: function (type, url, data, async) {
                var self = this;
                var endpoint = this.hasEndpoint(url) ? this.getEndpoint(url) : url;
                var response = $.Deferred();

                return $.ajax({
                    url: endpoint,
                    cache: true,
                    async: async || false,
                    data: data,
                    type: type
                }).done(function (res) {
                    response.resolve(res);
                });

                return response.promise();
            },

            ajaxQueue: function (type, url, data, async) {
                var jqXHR;
                var dfd = $.Deferred();
                var promise = dfd.promise();
                var self = this;

                function doRequest(next) {
                    jqXHR = self.ajax(type, url, data, async);
                    jqXHR.done(dfd.resolve)
                        .fail(dfd.reject)
                        .then(next, next);
                }

                self._ajaxQueue.queue(doRequest);

                promise.abort = function (statusText) {
                    if (jqXHR) {
                        return jqXHR.abort(statusText);
                    }

                    var queue = self._ajaxQueue.queue();
                    var index = $.inArray(doRequest, queue);

                    if (index > -1) {
                        queue.splice(index, 1);
                    }

                    dfd.rejectWith({
                        type: type,
                        url: url,
                        data: data,
                        async: async
                    }, [promise, statusText, ""]);

                    return promise;
                };

                return promise;
            }
        },
        markMultiOptions : function () {
            var self = this;
            $.each(self.cart, function (section, products) {
                var block = $('#'+section+'-block');
                if (block.length === 0) {
                    return;
                }

                var selectors = [];
                $.each(products, function (key, id) {
                    selectors.push('#item-product-'+id);
                });
                if (selectors.length === 0) {
                    return;
                }
                var products = block.find(selectors.join(','));
                if (products.length <= 1)
                {
                    return;
                }
                products.filter('.selected').addClass('multi-option');
            });
        },
        highlightCartProducts : function (cartProducts, processContextCode) {
            var self = this;

            $.each(cartProducts, function (id, el) {
                if (!self.cart.hasOwnProperty(el.type)) {
                    self.cart[el.type] = [];
                }
                self.cart[el.type].push(el.product_id);
                var item = $("#item-product-" + el.product_id);
                // mark item as selected in the configurator
                item.addClass('selected');
                var itemLabel = $("#item-product-" + el.product_id + " .item-label:first");
                if (el.hasOwnProperty('isCampaign') && el['isCampaign'] == 1) {
                    itemLabel.removeClass('hidden').addClass('campaign').text(Translator.translate('CAMPAIGN'));
                }
                if (el.hasOwnProperty('mandatory') && el['mandatory']) {
                    item.addClass('not-allowed');
                }
                if (ILS_PROCESS_CONTEXTS.indexOf(processContextCode.toLowerCase()) !== -1) {
                    if (el.hasOwnProperty('mandatory') && el['mandatory']) {
                        item.addClass('mandatory-row');
                        itemLabel.removeClass('hidden given').addClass('mandatory').text(Translator.translate('MANDATORY'));
                    } else
                    if (el.hasOwnProperty('isGiven') && el['isGiven'] && el.hasOwnProperty('isDefaulted') && el['isDefaulted']) {
                        itemLabel.removeClass('hidden contract').addClass('given').text(Translator.translate('GIVEN'));
                    }
                }
                if (el.hasOwnProperty('isContract') && el['isContract'] == 1) {
                    itemLabel.removeClass('hidden').addClass('contract').text(Translator.translate('CONTRACT'));
                    self.ilsCart.push(parseInt(el.product_id));
                } else {
                    self.acqCart.push(parseInt(el.product_id));
                }

                var itemParents = item.parents('.family:first');
                var familyId = itemParents.data('family-id');
                var family = false;

                window.families.each(function (el) {
                    if (el.entity_id == familyId) {
                        family = el;
                        return false;
                    }
                });

                if (family && (family.hasOwnProperty('allow_mutual_products') && family.allow_mutual_products == 1)) {
                    if (itemParents.length > 0) {
                        $.each(item.siblings(), function (index, value) {
                            $(value).addClass('exclusive-row');
                        });
                    }
                }
            });

            // mark multiple selected products in cart
            self.markMultiOptions();
        }
    });

    Cache = Configurator.Cache = function (prefix) {
        this.initialize.apply(this, prefix);
    };

    $.extend(Cache.prototype, {

        initialize: function (prefix) {
            this.prefix = prefix || 'ConfiguratorBase_';
        },

        prefix: 'ConfiguratorBase_',

        /**
         * @param key
         * @param value
         * @returns {*}
         */
        set: function (key, value) {
            if (root.getType(value) === 'object' || root.getType(value) === 'array') {
                value = JSON.stringify(value);
            }
            store.set(this.prefix + key, value);
            return value;
        },

        /**
         * @param key
         * @returns {*}
         */
        get: function (key) {
            var result = store.get(this.prefix + key);
            if (result !== null) {
                try {
                    result = JSON.parse(result);
                } catch (e) {
                    /* no-op */
                }
                return result;
            }
            return undefined;
        },

        /**
         * @param key
         * @returns {*}
         */
        pluck: function (key) {
            var result = localStorage.getItem(this.prefix + key);
            if (result !== null) {
                localStorage.removeItem(this.prefix + key);
                try {
                    result = JSON.parse(result);
                } catch (e) {
                    /* no-op */
                }
                return result;
            }
            return undefined;
        },

        /**
         * @returns {boolean}
         */
        flush: function () {
            var reg = new RegExp('^' + this.prefix);
            Object.keys(localStorage)
                .forEach(function (key) {
                    if (reg.test(key)) {
                        localStorage.removeItem(key);
                    }
                });
            return true;
        }
    });

}(jQuery));

if (!String.prototype.format) {
    String.prototype.format = function () {

        var args = arguments;
        var sprintfRegex = /\{(\d+)\}/g;

        var sprintf = function (match, number) {
            return number in args ? args[number] : match;
        };

        return this.replace(sprintfRegex, sprintf);
    };
}

if (!window.getType) {
    window.getType = function (item) {
        var getType = {};
        var type = getType.toString.call(item).match(/\s(.*)\]/)[1].toLowerCase();
        if (type === 'string') {
            if (/^[\],:{}\s]*$/
                    .test(item.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, '')) && item.length > 0
            ) {
                return 'json';
            } else {
                return 'string';
            }
        }
        return type;
    }
}

if (!Array.prototype.unique) {
    Array.prototype.unique = function () {
        var unique = [];
        for (var i = 0; i < this.length; i += 1) {
            if (unique.indexOf(this[i]) == -1) {
                unique.push(this[i])
            }
        }
        return unique;
    };
}

if (!Array.prototype.filter) {
    Array.prototype.filter = function (fun /*, thisArg */) {
        "use strict";

        if (this === void 0 || this === null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun != "function")
            throw new TypeError();

        var res = [];
        var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
        for (var i = 0; i < len; i++) {
            if (i in t) {
                var val = t[i];

                if (fun.call(thisArg, val, i, t))
                    res.push(val);
            }
        }

        return res;
    };
}

if (typeof window.performance === 'undefined') {
    window.performance = {};
}

if (!window.performance.now) {

    var nowOffset = Date.now();

    if (performance.timing && performance.timing.navigationStart) {
        nowOffset = performance.timing.navigationStart
    }

    window.performance.now = function now() {
        return Date.now() - nowOffset;
    }
}
jQuery(document).ready(function () {
    var catalogVersion = jQuery('#current_build').html();
    var localVersion = store.get('catalog_version');
    if (localVersion != null && jQuery('#current_build').length > 0 && localVersion != catalogVersion)
        store.clear(); // Clear cache if not latest version
    store.set('catalog_version', catalogVersion);
});
