(function ($) {
    var root = window;
    root.ConfiguratorUI = {};

    var Base = ConfiguratorUI.Base = function (options) {
        this.options = $.extend(this.options, options || {});
    };

    $.extend(Base.prototype, {
        // this variable holds reference to configurator DOM object
        // if destroyed is destroyed, it will be set to null
        _configurator: null,
        // this holds reference to configurator object on window
        /** @var {Configurator.Base} configurator **/
        configurator: null,
        initialized: false,
        section: {},
        refreshConfigurator: function () {

        },
        resetFilterButton: function (e) {
            var container = $('.dropDown');

            // check if the clicked area is dropDown or not
            if (container.has(e.target).length === 0) {
                $('.vfde-unfold-less').removeClass('vfde-unfold-less').addClass('vfde-unfold-more');
            }
        },
        loadConfigurator: function (configuratorData, callback) {
            // set configurator object first !important
            var configuratorHtml = this.generateConfiguratorContent(configuratorData);

            // the configurator container
            var configContainer = this.configurator.options.container;
            this._configurator = $(configContainer);

            // hide home wrapper
            $('#homepage-wrapper').hide();

            // append content to configurator container
            this._configurator.find('.content').html(configuratorHtml);

            // if some callback got here, call it
            if (root.getType(callback) === 'function') {
                callback();
            }

            // init product info popover
            this.initPopover();
        },
        updatePurpleBundleHints: function () {
            // Check for selected tariff that can be bundled (no yet selected a bundle) to show bundling hint
            if (!this.getConfigurator().bundleId) {
                var bundleIds = null;
                this._configurator.find('.item-row.selected').each(function () {
                    if (!bundleIds && $(this).attr('data-bundles')) {
                        bundleIds = $(this).attr('data-bundles');
                    }
                });
            } else {
                this._configurator.find('.item-row .bundle-identifier .label-purple').each(function () {
                    $(this).removeClass('hidden').addClass('hidden');
                });
            }

            return this;
        },
        updateBundleHints: function () {
            var self = this;
            // clear any previously set hints
            $('div.modal.show-bundle-hints').remove();

            // building relevant hints for active package in cart
            // do not show hints for other packages as these are already highlighted
            var packageBundles = [];
            var activePackageId = parseInt(self.getConfigurator().packageId);
            this.getConfigurator().eligibleBundles.forEach(function (bundle) {
                if (bundle.hasOwnProperty('highlightPackages')) {
                    bundle.highlightPackages.forEach(function (packageData) {
                        if (activePackageId === packageData.packageId) {
                            packageBundles.push(bundle.bundleId);
                        }
                    });
                }
            });

            // get bundle hints from configurator.js
            var bundleHints = this.getConfigurator().bundleHints;
            if (bundleHints.length) {
                bundleHints.forEach(function (hint) {
                    // if this bundle doesn't highlight the active package, continue to the next hint
                    if (packageBundles.indexOf(parseInt(hint.bundleId)) === -1) {
                        return this;
                    }

                    // if hints were shown, do not show them again. packages are already highlighted
                    if (window.bundleHintShown[hint["bundleId"]] && window.bundleHintShown[hint["bundleId"]].indexOf(parseInt(hint.bundleId)) !== -1) {
                        return this;
                    }

                    $.notify({
                        title: hint['title'],
                        message: hint['description']
                    }, {
                        delay: 4000,
                        timer: 1000,
                        placement: {
                            from: "bottom",
                            align: "right"
                        },
                        template:
                        '<div class=\"modal bundle-lightbulb-modal fade in show-bundle-hints\" role=\"dialog\" data-backdrop=\"false\" data-notify="container" style="left: initial;">' +
                        '        <div class=\"col-lg-12 col-md-10 col-sm-8 navbar-fixed-bottom\" style=\"position: initial;\">' +
                        '            <div class=\"modal-dialog modal-sm\" role=\"document\">' +
                        '                <div class=\"modal-content notify-bundle-hint\">' +
                        '                    <div class=\"modal-header\">' +
                        '                        <button type=\"button\" class=\"close\" aria-label=\"Close\" data-notify="dismiss"><span aria-hidden=\"true\">x<\/span><\/button>' +
                        '                        <h2 class=\"modal-title\">' +
                        '                            <div class=\"hint-description-badge\">' +
                        '                                <i class=\"fa fa-lightbulb-o\" aria-hidden=\"true\"><\/i>' +
                        '                            <\/div>' +
                        '                            <span class=\"hint-title\" data-notify="title">{1}<\/span>' +
                        '                        <\/h2>' +
                        '                    <\/div>' +
                        '                    <div class=\"modal-body\">' +
                        '                        <p data-notify="message">{2}<\/p>' +
                        '                    <\/div>' +
                        '                <\/div><!-- \/.modal-content -->' +
                        '            <\/div><!-- \/.modal-dialog -->' +
                        '        <\/div>' +
                        '<\/div><!-- \/#show-bundle-hints -->',
                        onClose: function () {
                            $(this).fadeOut("slow");
                        },
                        onClosed: self.updateCartPackagesWithBundleHighlights(hint['bundleId'])
                    });
                });
            }
            return this;
        },
        updateCartPackagesWithBundleHighlights: function (bundleId) {
            configurator.eligibleBundles.forEach(function (bundle) {
                if (bundle.bundleId === bundleId) {
                    window.bundleHintShown[bundleId] = window.bundleHintShown[bundleId] || [];
                    bundle.highlightPackages.forEach(function (highLightPackage) {
                        window.bundleHintShown[bundleId].push(parseInt(highLightPackage.packageId));
                    });
                }
            });
        },
        updateBundleComponents: function () {
            var self = this;
            // get bundle components from configurator.js
            var bundleComponents = this.getConfigurator().bundleComponents;
            // clear all bundle components from configurator
            this._configurator.find('.item-row').attr('data-bundles-component', '');
            bundleComponents.forEach(function (bundleProduct) {
                var productId = bundleProduct['productId'], bundleIds = bundleProduct['bundleId'];
                self._configurator.find('.item-row[data-id="' + productId + '"]').attr('data-bundles-component', bundleIds);
            });

            return this;
        },
        parseBundleData: function () {
            var eligibleBundleId = null, activeBundleExists = false;


            this.getConfigurator().eligibleBundles.forEach(function (bundle) {
                // showing hint if bundle location is draw and targetedProducts is empty (otherwise it is a simulation)
                if (bundle.addButtonInSection.indexOf(BUNDLE_LOCATION_DRAW) !== -1 && !bundle.targetedProducts.length && bundle.targetedPackagesInCart.length) {
                    bundle.targetedPackagesInCart.forEach(function (targetedPackage) {
                        if (configurator.packageId === targetedPackage.packageId) {
                            eligibleBundleId = bundle.bundleId;
                            // check if bundle is active
                            this.getConfigurator().activeBundles.forEach(function (activeBundle) {
                                if (activeBundle.bundleId === bundle.bundleId) {
                                    activeBundleExists = true;
                                    return true;
                                }
                            });
                        }
                    }.bind(this));
                }
            }.bind(this));

            return {
                'eligibleBundleId': eligibleBundleId,
                'activeBundleExists': activeBundleExists
            }
        },
        /**
         * Method that determines whether or not hint panel should be shown
         * @returns {ConfiguratorUI.Base}
         */
        showBundleHintPanel: function () {
            var bundleData = this.parseBundleData(), bundleModal = $('#bundleModal');

            switch (true) {
                case !!bundleData.eligibleBundleId :
                    this.getConfigurator().showBundleDetailsToaster(bundleData.eligibleBundleId, bundleData.activeBundleExists);
                    break;
                case bundleModal.is(':visible') :
                    bundleModal.modal('hide');
                    break;
                default:
                    break;
            }

            this.getConfigurator().assureElementsHeight();

            return this;
        },
        /**
         * Generate configurator data for each section based on a list of products given by configurator
         * @param configuratorData
         */
        generateConfiguratorContent: function (configuratorData) {
            var fragment = '', child;
            // for each section of configurator apply data to handlebars templates
            var configuratorSection;
            for (configuratorSection in configuratorData) {
                if (configuratorData.hasOwnProperty(configuratorSection)) {
                    var template = this.getConfigurator().templates[configuratorSection];
                    var templateData = configuratorData[configuratorSection];
                    fragment += Handlebars.compile(template, {noEscape: true})(templateData);
                }
            }

            var el = $('<div>' + fragment + '</div>');
            fragment = document.createDocumentFragment();
            while (child = el[0].firstChild) {
                fragment.appendChild(child);
            }

            var configuratorHtml = $(fragment.children || fragment.childNodes);
            var searchFields = configuratorHtml.find('[data-search-on-type]');
            var searchFieldsCount = searchFields.length;
            for (var j = 0; j < searchFieldsCount; j++) {
                var section = searchFields[j].attributes['name'].value.split('-')[1];
                if (section && typeof self.products != 'undefined' && self.products[section]) {
                    searchFields[j].attributes['data-documents'].value = JSON.stringify(self.products[section]);
                }
            }

            return configuratorHtml;

        },
        /**
         * Get configurator instance
         * @returns {Configurator.Base}
         */
        getConfigurator: function () {
            return this.configurator;
        },
        initPopover: function () {
            $('.item-row [data-toggle="popover"]').popover({
                html: true,
                trigger: 'manual',
            }).click(function () {
                $(this).popover('toggle');
            }).focusout(function () {
                $(this).popover('hide');
            });

            return this;
        },

        updateProductPrices: function () {
            var self = this, productId, item;
            // window.prices is always updated after getPrices call
            var productIdsWithPrices = Object.keys(window.prices);
            var itemForProductId = new Map();

            // iterating through each section to see if product prices have been updated
            if (typeof self.configurator.products == 'undefined') {
                self.configurator.products = [];
            }
            this.configurator.options.sections.forEach(function (section) {
                if (self.configurator.products.hasOwnProperty(section)) {
                    self.configurator.products[section].forEach(function (product) {
                        productId = product["entity_id"];
                        var hasSpecialPrice = false;

                        if (window.specialPriceProducts.hasOwnProperty(productId)) {
                            hasSpecialPrice = window.specialPriceProducts[productId];
                        }

                        // checking if product has special price
                        if ((productIdsWithPrices.indexOf(productId) !== -1)) {
                            if (!itemForProductId.has(productId)) {
                                itemForProductId.set(productId, $('#item-product-' + productId));
                            }
                            item = itemForProductId.get(productId);

                            // avoid updating prices if null or false come on response
                            var mafTaxPrice = window.prices[parseInt(productId)].maf_with_tax;
                            if (mafTaxPrice !== null && mafTaxPrice !== false && mafTaxPrice !== undefined) {
                                // checking maf
                                self.updateConfiguratorPrice(item, '.per-month', mafTaxPrice, product["regular_maf_with_tax"], section);
                                item.find('.incl-vat-tax.per-month:first').removeClass('hide');
                            } else {
                                item.find('incl-vat-tax.per-month:first').addClass('hide');
                            }

                            var oneTimeTaxPrice = hasSpecialPrice ? hasSpecialPrice : window.prices[parseInt(productId)].price_with_tax;
                            // avoid updating prices if null or false come on response
                            if (oneTimeTaxPrice !== null && oneTimeTaxPrice !== false && oneTimeTaxPrice !== undefined) {
                                // checking price
                                self.updateConfiguratorPrice(item, '.one-time', oneTimeTaxPrice, product["price_with_tax"], section);
                                item.find('.incl-vat-tax.one-time:first').removeClass('hide');
                            } else {
                                item.find('.incl-vat-tax.one-time:first').addClass('hide');
                            }
                        }
                    });
                }
            });
        },
        /**
         * Updates frontend prices in configurator (used specially for mixmatches)
         * @param item
         * @param whatPrice
         * @param thePrice
         * @param oldPrice
         * @param section
         */
        updateConfiguratorPrice: function (item, whatPrice, thePrice, oldPrice, section) {
            if (oldPrice && oldPrice > thePrice) {
                // setting new price including tax
                item.find('.incl-vat-tax' + whatPrice).find('.tax-price').text(Handlebars.helpers.money(thePrice, {hash: {}}));
                item.find('.incl-vat-tax' + whatPrice).find('.top').find('.value').text(Handlebars.helpers.money(oldPrice, {hash: {}}));
                // only update the top div for sections different than tariffs
                if (SUBSCRIPTION_IDENTIFIERS.indexOf(section) === -1) {
                    item.find('.incl-vat-tax' + whatPrice).find('.top').removeClass('hide');
                }
            } else {
                // setting new price
                item.find('.incl-vat-tax' + whatPrice).find('.tax-price').text(Handlebars.helpers.money(thePrice, {hash: {}}));

                if (!item.find('.incl-vat-tax' + whatPrice).find('.top').hasClass('hide')) {
                    // hiding top price hint
                    item.find('.incl-vat-tax' + whatPrice).find('.top').addClass('hide');
                }
            }
        },
        toggleFilters: function (section) {
            var filterSection = $('.section-filters[data-section="' + section + '"]');

            if (filterSection.hasClass('hide')) {
                filterSection.removeClass('hide');
                $('.dropdown-toggle').dropdown();
            } else {
                filterSection.addClass('hide');
            }
        },
        selectFilterOption: function (object) {
            var $section = $(object).parent().parent().data('section');
            $('.search-' + $section).tagsinput('add', $(object).html());
        },
        /**
         * Set configurator instance on this UI handler
         * @param configurator
         * @returns {ConfiguratorUI.Base}
         */
        setConfigurator: function (configurator) {
            this.configurator = configurator;
            return this;
        },
        /**
         * opens section in configurator and returns it's previous open state
         * @param sectionType
         * @param justUpdate
         * @returns {boolean}
         */
        openSection: function (sectionType, justUpdate) {
            if (justUpdate) {
                this.updateSectionHints(sectionType);
                return true;
            }

            var self = this;
            // if section is undefined, opening first section from configurator
            sectionType = (sectionType === undefined) ? this.getConfigurator().options.sections[0] : sectionType;
            var section = $('#'+sectionType+'-block');
            var selectedItem = section.find('.selected-item');

            // check if it already opened
            if (this.isSectionOpened(sectionType)) {
                return true;
            }

            var sections = this.getConfigurator().options.sections;
            // Hide all sections, excepting the last one
            this.getConfigurator().options.sections.forEach(function (section) {
                if (section !== sectionType) {
                    if (sectionType === null) {
                        if (section !== sections[sections.length - 1]) {
                            self.closeSection(section);
                        }
                    } else {
                        self.closeSection(section);
                    }
                }
            });
            selectedItem.slideUp('fast', function () {
                section.find('.selection-block').removeClass('box-closed').addClass('box-opened');
                self.updateSectionHints(sectionType);
                self.getConfigurator().assureElementsHeight();
            });
            section.find('.block-header').hide();
            section.find('.block-footer').show();

            this.indexSearchInputs();

            return false;
        },
        /**
         * Determine whether or not a certain configurator section is opened
         * @param sectionType
         * @returns bool
         */
        isSectionOpened: function (sectionType) {
            return this.getConfiguratorSection(sectionType).find('.selection-block').hasClass('box-opened');
        },
        /**
         * Get configurator section using jQuery selector
         * @param sectionType
         * @returns {jQuery}
         */
        getConfiguratorSection: function (sectionType) {
            return $('#'+sectionType+'-block');
        },
        /**
         * Close certain configurator section
         * @param sectionType
         * @returns {ConfiguratorUI.Base}
         */
        closeSection: function (sectionType) {
            var self = this;
            var section = self.getConfiguratorSection(sectionType);
            if (this.isSectionOpened(sectionType)) {
                section.find('.selected-item').slideDown('fast');
                section
                    .find('.selection-block').removeClass('box-opened').addClass('box-closed')
                    .find('.product-table').removeAttr('style').end()
                    .removeAttr('style');
                section
                    .find('.expand-handle')
                    .text(section.find('.expand-handle').data('open'));
            }

            self.updateSectionHints(sectionType);
            section.find('.block-header').show();
            return this;
        },
        /**
         * Determines the hint text specified when section is not expanded
         * Ex:
         * If one product, show it's name
         * If more than one product, specify how many are selected
         * If no product, specify that no product is selected
         * @param sectionType
         */
        updateSectionHints: function (sectionType) {
            var section = this.getConfiguratorSection(sectionType);
            var textHint = "";
            // If there is a product in this section, than configurator.cart will have sectionType as property

            var sectionSelectedItems = section.find('.item-row.selected');

            if (sectionSelectedItems.length === 1) {
                // Just one product selected, get it's name
                var productId = sectionSelectedItems.first().data('id').toString();

                var name = $('#item-product-' + productId + ' .item-title').text();
                textHint ='<span class="status">' + name + '</span><span class="check"></span>';

            } else if (sectionSelectedItems.length > 1) {
                // Otherwise show a message containing the number of selected products
                textHint = '<span class="status">' + Translator.translate('Multiple options selected') + '</span><span class="check"></span>';
            } else {
                // No item selected message
                textHint = '<div class="no-item-message"><span class="status">' + this.getConfigurator().getSectionItemSelectableMessage(sectionType) + '</span><span class="please-select"></span></div>';
            }

            // Update section closed
            section.find('.selected-item .content').html(textHint);
            // Update section opened
            section.find('.panel-heading .content.text-right').html(textHint);

            return this;
        },
        /**
         * Using configurator structure determine whether or not create packages for bundles should be shown or hidden
         * @returns {ConfiguratorUI.Base}
         */
        updateBundleCreationButtons: function () {
            var eligibleBundles = this.getConfigurator().eligibleBundles;
            var self = this;
            var configuratorType = this.getConfigurator().options.type;
            var tempPackages = self.getTemporarilyAllowedPackages();
            var allowedPackages = [];
            var bundleHints = [];
            var bundleHintsBuilt = false;

            this.getConfigurator().options.sections.forEach(function (section) {
                // current configurator section for which we are searching for bundle buttons
                var configuratorSection = self.getConfiguratorSection(section);
                // remove this element from this section (if found) because it will be built later
                configuratorSection.find('.packages-family').remove();
                if (eligibleBundles.length) {
                    var packages = [];
                    eligibleBundles.forEach(function (bundle) {
                        // this property comes from response (which, at this time, is built in Dyna_Bundles_Helper_Data::getFrontendEligibleBundles)
                        if (bundle.addButtonInSection.indexOf(section) !== -1) {
                            if (bundle.activatePackages.length) {
                                // check whether or not current response activates packages
                                bundle.activatePackages.forEach(function (packageCode) {
                                    allowedPackages.push(bundle.creationTypeId);
                                    // activating button in cart list
                                    var addPackageButton = $('#package-types').find("[data-package-creation-type-id='" + bundle.creationTypeId + "']");
                                    if (addPackageButton.hasClass('disabled')) {
                                        addPackageButton.removeClass('disabled').addClass('temporarilyAllowed');
                                    }
                                    // bundle id is not relevant anymore on this flow
                                    packages.push({
                                        bundleId: bundle.bundleId,
                                        name: bundle.packageName,
                                        type: packageCode,
                                        buttonTitle: bundle.addButtonTitle,
                                        parentId: bundle.parentPackageId,
                                        relatedProductSku: bundle.relatedProductSku,
                                        creationTypeId: bundle.creationTypeId,
                                        filterAttributes: bundle.filterAttributes
                                    });
                                });
                            } else {
                                // just add the bundle to template list
                                // not on RedPlus flow, so no related product needed
                                var targetedPackages = [];
                                if (bundle.targetedPackagesInCart) {
                                    bundle.targetedPackagesInCart.forEach(function (entry) {
                                        targetedPackages.push(entry['packageId']);
                                    });
                                }
                                bundleHints.push({
                                    bundleId: bundle.bundleId,
                                    susoBundle: bundle.susoBundle,
                                    redPlusBundle: bundle.redPlusBundle,
                                    buttonTitle: bundle.addButtonTitle,
                                    targetProducts: bundle.targetedProducts,
                                    targetedPackages: targetedPackages,
                                    section: bundle.addButtonInSection
                                });
                            }
                        } else if (!bundleHintsBuilt && bundle.addButtonInSection.indexOf(BUNDLE_LOCATION_DRAW) !== -1) {
                            // if section is draw, then show bundle hint for targeted products
                            bundleHints.push({
                                bundleId: bundle.bundleId,
                                susoBundle: bundle.susoBundle,
                                redPlusBundle: bundle.redPlusBundle,
                                buttonTitle: bundle.addButtonTitle,
                                targetProducts: bundle.targetedProducts,
                                section: bundle.addButtonInSection
                            });
                        }
                    });

                    // after iteration through all eligible bundles, let's see what we have
                    if (packages.length) {
                        var template = window.templates.partials[configuratorType][section]["optional_packages_family"];
                        var html = Handlebars.compile(template, {noEscape: true})(packages);
                        // inject this html code inside DOM
                        configuratorSection.find('.product-table').append(html);
                    }
                }

                // prevent rebuilding draw index again
                bundleHintsBuilt = true;
            });

            var foundRed = false;
            if (typeof eligibleBundles !== 'undefined' && eligibleBundles.length > 0) {
                eligibleBundles.forEach(function (bundle) {
                    if (bundle.redPlusBundle && !foundRed) {
                        foundRed = true;
                    }
                });
            }
            if (!foundRed) {
                $("div#package-types").find(".btn[data-package-creation-type-id='" + REDPLUS_CREATION_TYPE_ID + "']:first").removeClass('temporarilyAllowed').addClass('disabled');
            }

            tempPackages.forEach(function (packageCreationTypeId) {
                // check if other temporarily allowed packages are still allowed
                var tempButton = $('#package-types').find("[data-package-creation-type-id='" + packageCreationTypeId + "']");
                // if this button still hasClass
                if (allowedPackages.indexOf(packageCreationTypeId) === -1) {
                    tempButton.removeClass('temporarilyAllowed').addClass('disabled');
                }
            });

            self.addBundleHints(bundleHints)
                .updatePurpleBundleHints()
                .updateBundleHints()
                .updateBundleComponents()
                .showBundleHintPanel();

            return this;
        },
        /**
         * Goes through configurator and cart adds specific bundle hints
         * @param bundles {Array}
         * @param section {String}
         * @returns {ConfiguratorUI.Base}
         */
        addBundleHints: function (bundles) {
            var configSection = $("#config-wrapper");
            var self = this;

            // reset draw bundle hints
            configSection.find('.bundle-identifier').find('.label').addClass('hidden');
            configSection.find('.item-row[onclick=""]').attr('onclick', "configurator.selectProduct(this); return false;");

            // reset add bundle buttons
            configSection.find('.add-bundle-button').siblings('.item-price').removeClass('hide');
            configSection.find('.add-bundle-button').remove();

            // show bundle hints for targeted products
            bundles.forEach(function (bundle) {
                if (bundle.section.indexOf(BUNDLE_LOCATION_DRAW) === -1) {
                    // setting button on product row
                    bundle.targetProducts.forEach(function (productId) {
                        // first hiding price columns to make room for the create bundle button
                        configSection
                            .find('.item-row[data-id="' + productId + '"]')
                            .find('.item-price')
                            .addClass('hide');
                        // define the button
                        var button = $('<div class="add-bundle-button col-xs-2 pull-right"><button class="btn btn-info right" '
                            + 'data-bundle-id="' + bundle.bundleId + '" ' + (self.getConfigurator().hasSusoBundle() ? ' disabled ' : '')
                            + 'data-inter-stack-package-id="' + bundle.targetedPackages.join(",") + '" '
                            + 'onclick="configurator.addToCartBundle(this, true)">'
                            + ' ' + Translator.translate(bundle.buttonTitle) + ' <i class="vfde-add-circle"></i></button></div>');
                        // inject the button
                        configSection
                            .find('.item-row[data-id="' + productId + '"]')
                            .attr('onclick', '')
                            .addClass((self.getConfigurator().hasSusoBundle() ? ' disabled ' : ''))
                            .find('.item').append(button);
                    });
                } else {
                    // showing bundle hint
                    bundle.targetProducts.forEach(function (productId) {
                        var itemRow = configSection.find('.item-row[data-id="' + productId + '"]');
                        itemRow.find('.bundle-identifier').find('.label').removeClass('hidden');
                        var dataBundles = itemRow.attr('data-bundles') ? itemRow.attr('data-bundles') + ',' : '';
                        itemRow.attr('data-bundles', dataBundles + bundle.bundleId);
                    });
                }
            });

            return this;
        },
        switchBundle: function (element, bundleId, customerNumber, productId, interStackBundle, packageId, stack) {
            var self = this;
            var containerAlternatives = $('.bundle-alternatives-packages');
            var containerHeaderAlternatives = $('.bundle-alternatives-header');

            var bundleInput = $('#bundleModal .bundle-to-cart-btn input[type=submit]');
            bundleInput.attr('data-bundle-id', bundleId).attr('data-product-id', productId);
            bundleInput.attr('data-entity-id', $(element).data('entity-id'));
            bundleInput.attr('data-contract-id', customerNumber);

            if (!interStackBundle) {
                bundleInput.attr('data-subscription-number', customerNumber);
                bundleInput.prop('value', Translator.translate('Merge into bundle'));
            } else {
                bundleInput.attr('data-inter-stack-package-id', packageId);
                bundleInput.prop('value',Translator.translate('Add to basket'));
            }

            var bundleContainer = $(".bundle-details-header").find("[data-bundle-ui-section-id='" + $(containerAlternatives).data('bundle-ui-section-id') + "']")[0];
            var selectedPackage = $(element).closest('.bundle-alternative-product');
            //put text into bundle package holder
            $(bundleContainer).find('.orderline-text').html($(selectedPackage).find('.orderline-text').html());
            $(bundleContainer).find('.orderline-text-description').html($(selectedPackage).find('.orderline-text-description').html());
            var bundleIcon = $(bundleContainer).find('.stack-identificator');
            bundleIcon.removeClass().addClass($(selectedPackage).find('.stack-identificator').attr('class'));

            if (packageId) {
                bundleIcon.addClass('new_indicator');
            }
            //make active bundle package holder
            $(bundleContainer).find('.bundle-package-block-body').removeClass('bordered');

            //enable buttons
            bundleInput.removeClass('disabled').prop("disabled", false);
            $('.info-btn-bundle').removeClass('disabled');
            //hide modal
            $('#bundleModalAlternatives').modal('hide');

            $('#bundle-details-packages-list').find('.target-package').attr('data-stack', stack.toLowerCase());

            self.updateBundleInformation(bundleId, customerNumber, productId, $(element).data('entity-id'), packageId);
        },

        updateBundleInformation: function (bundleId, customerNumber, productId, entityId, interStackBundlePackageId) {
            var bundleInfoContainer = $('#bundle-details-packages-list');
            configurator.showBundleBarInformationToaster();

            $.ajax({
                url: "bundles/index/getAdditionalBundleInfo",
                type: "POST",
                data: {
                    'bundle_id': bundleId,
                    'customer_number': customerNumber,
                    'product_id': productId,
                    'entity_id': entityId,
                    'inter_stack_package_id': interStackBundlePackageId
                }
            }).done(function (response) {
                if (response.hasOwnProperty('bundleProducts') && response.bundleProducts) {
                    $.each(response.bundleProducts, function (packageType, sectionData) {
                        var sectionBlock = $(bundleInfoContainer).find('[data-stack="' + packageType + '"]');
                        var packageNameElement = $(sectionBlock).find('h4').text('');
                        var productListElement = $(sectionBlock).find('ul').text('');
                        var oldPriceElement = $(sectionBlock).find('p.old-price').text('');
                        var newPriceElement = $(sectionBlock).find('h2.new-price').text('');

                        var packageName = response.packageNames && response.packageNames[packageType] ? response.packageNames[packageType] : packageType;
                        $(packageNameElement).text(packageName);
                        var listHtml = '';
                        $.each(sectionData, function (infoKey, info) {
                            var liClass, iconClass = null;
                            if (info['type'] === 'add') {
                                liClass = 'bundle-added';
                                iconClass = 'vfde-verified';
                            } else if (info['type'] === 'remove') {
                                liClass = 'bundle-removed';
                                iconClass = 'vfde-clear';
                            }

                            listHtml += '<li class="' + liClass + '"><i class="vfde ' + iconClass + '"></i> <p>' + info['name'] + (info['discountFormatted'] ? ' (' + info['discountFormatted'] + ')' : '') + '</p> </li>';
                        });
                        $(productListElement).append($(listHtml));

                        if (response.hasOwnProperty('totals') && response.totals[packageType]) {
                            var prices = response.totals[packageType];
                            $(oldPriceElement).append('&euro; ' + prices['oldPrice']);
                            $(newPriceElement).append('&euro; ' + prices['newPrice'] + ' <span class="small-text">per-month</span>');
                        }
                    });
                }
            });
        },

        toggleBundlesAlternativesTabs: function (element) {
            var tab = $(element);
            var tabToShow = tab.data('tab');
            var tabsSection = $('.bundle-alternatives-body');
            var headersSection = $('.bundle-alternatives-header');

            /*Header sections*/
            $.each(headersSection.find('.tab-details'), function (key, tabElement) {
                var tabSection = $(tabElement);
                if (tabSection.data('tab') === tabToShow) {
                    tabSection.addClass('active');
                } else {
                    tabSection.removeClass('active');
                }
            });

            /*Tab sections*/
            $.each(tabsSection, function (key, tabElement) {
                var tabSection = $(tabElement);
                if (tabSection.data('tab') === tabToShow) {
                    tabSection.removeClass('hide');
                } else {
                    tabSection.addClass('hide');
                }
            });
        },

        deleteBundleConfigurator: function (element) {
            alert('Not implemented yet!');
        },

        /**
         * Get a list of temporarily allowed "Create package" buttons that will be later checked if these are needed to remain enabled
         * @return {Array}
         */
        getTemporarilyAllowedPackages: function () {
            var allowed = [];
            $('#package-types').find('.temporarilyAllowed').each(function () {
                allowed.push($(this).attr('data-target'));
            });

            return allowed;
        },

        indexSearchInputs: function () {
            // enable tags input
            $("input[data-role='tagsinput']").tagsinput();

            var sectionFilters = $('.section-filters');
            sectionFilters.on('show.bs.dropdown', function () {
                $(this).find('i').removeClass('vfde-unfold-more').addClass('vfde-unfold-less');
            });

            sectionFilters.on('hide.bs.dropdown', function () {
                $(this).find('i').removeClass('vfde-unfold-less').addClass('vfde-unfold-more');
            });
        },
        toggleRedPlusParentSelectionPanel: function (object) {
            var $this = jQuery(object);
            if (!$this.hasClass('panel-collapsed')) {
                $this.parents('.panel').find('.panel-body').slideUp("fast", function () {
                    configurator.assureElementsHeight();
                });
                $this.addClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                store.set('redPlusPanelState', 'closed');
            } else {
                $this.parents('.panel').find('.panel-body').removeClass('hide');
                $this.parents('.panel').find('.panel-body').slideDown("fast", function () {
                    configurator.assureElementsHeight();
                });
                $this.removeClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                store.set('redPlusPanelState', 'open');
            }
        },
        updateHintsOnDeselect: function (sectionType) {
            var self = this;
            // remove hints of deselected products from configurator
            if ((DEVICES_IDENTIFIERS.indexOf(sectionType) !== -1) || (SUBSCRIPTION_IDENTIFIERS.indexOf(sectionType) !== -1)) {
                var textHint = '<div class="no-item-message"><span class="status">' + this.getConfigurator().getSectionItemSelectableMessage(sectionType) + '</span><span class="please-select"></span></div>';
                $('.conf-block.panel').each(function () {
                    var sectionName = $(this).attr('data-type');
                    var currentSection = $('.conf-block.panel[data-type=' + sectionName + ']');
                    currentSection.find('.panel-heading').find('.content.text-right').html(textHint);
                });
            } else {
                self.updateSectionHints(sectionType);
            }
        },
        handleLinkedAccountsPanelState: function () {
            var panelContainer = jQuery('#linkedAccountSelectionContainer');
            if (store.get('linkedAccountPanelState') === 'closed') {
                panelContainer.find('.panel-body').addClass("hide");
                panelContainer.find('.panel-heading').addClass("panel-collapsed");
                panelContainer.find('.panel-heading').find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            }
        },

        handleRedPlusParentsPanelState: function () {
            var panelContainer = jQuery('#redplus-parent-selection-container');
            if (store.get('redPlusPanelState') === 'closed') {
                panelContainer.find('.panel-body').addClass("hide");
                panelContainer.find('.panel-heading').addClass("panel-collapsed");
                panelContainer.find('.panel-heading').find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            }
        },

        markConfiguratorInstalledBaseProducts: function (installedBaseCartProducts) {
            var self = this;
            self.configurator.ilsCart = [];
            $.each(installedBaseCartProducts, function (productId, productAttributes) {
                self.configurator.ilsCart.push(parseInt(productId));
                $('.selection-block [data-id="' + productId + '"]')
                    .find('span.item-label')
                    .removeClass('hidden')
                    .addClass('contract')
                    .text(Translator.translate('CONTRACT'));
                $.each(productAttributes, function (index, value) {
                    if (index == "contract_end_date" || "contract_possible_cancellation_date") {
                        value = value.split('-').join('.');
                        $('.selection-block [data-id="' + productId + '"]')
                            .find('span.item-date')
                            .removeClass('hidden')
                            .text(Translator.translate('Contract end date')+': '+ value);
                        return false;
                    }
                });
            })
        },
        markConfiguratorDisabledSections: function (disabledSections) {
            $.each(disabledSections, function (key, section) {
                 this.getConfiguratorSection(section).find('.panel-title').addClass('disabled');
            }.bind(this));
        },
        /**
         * Expecting hintData as object of objects {"hintOnce" : true|false, "message": string, "ruleId" : int}
         * @param hintData
         */
        showHintNotifications: function (hintData) {
            var container = $("#notification-bar");
            var atLeastOneShown = false;
            var activePackageId = this.getConfigurator().activePackageEntityId;
            // check if hint has already been shown
            var shownHints = store.get("shownHints");
            shownHints = shownHints || {};
            // clear container
            container.find('p').remove();

            for (var hintId in hintData) {
                // check property for existence
                if (!hintData.hasOwnProperty(hintId)) {
                    continue;
                }
                // check if hint is shown once or multiple times
                var hint = hintData[hintId];
                if (hint.hintOnce) {
                    if (!shownHints.hasOwnProperty(activePackageId)) {
                        shownHints[activePackageId] = [];
                    }
                    if (shownHints[activePackageId].indexOf(hintId) === -1) {
                        shownHints[activePackageId].push(hintId);
                        // show hint and add it to shown hints list
                        container.append(this.getHintElement(hint).render());
                        atLeastOneShown = true;
                    }
                } else {
                    atLeastOneShown = true;
                    container.append(this.getHintElement(hint).render());
                }
            }
            store.set("shownHints", shownHints);
            if (atLeastOneShown) {
                container.removeClass('hidden');
                container.addClass('notification-bar-error');

                // register close hint action on click
                container.find('p').each(function () {
                    $(this).attr("onClick", "configuratorUI.hideHint(this)");
                });
                setTimeout(function () {
                    container.find('p').each(function () {
                        configuratorUI.hideHint($(this));
                    });
                }, 10000);
            } else {
                container.removeClass('notification-bar-error');
                container.addClass('hidden');
            }
        },
        /**
         * Expecting object {"hintOnce" : true|false, "message": string, "ruleId" : int}
         * @param hintData
         */
        getHintElement: function (hintData) {
            // add render function to object
            hintData.render = function () {
                // prepare hint message
                var messageSpan = document.createElement("span");
                messageSpan.className = "notification-bar-message";
                var message = document.createTextNode(hintData.message);
                messageSpan.append(message);
                // prepare the hide hint element
                var closeTag = document.createElement("a");
                closeTag.className = "notification-bar-close fa fa-times";
                // prepare container
                var para = document.createElement("p");
                para.append(messageSpan);
                para.append(closeTag);

                return para;
            }.bind(this);

            return hintData;
        },
        /**
         * Hide message by message from container
         * If message list is empty, clear all messages
         * @param element
         */
        hideHint: function (element) {
            var container = $("#notification-bar");
            $(element).remove();
            if (!container.find('p').length) {
                container.removeClass("notification-bar-error");
                container.addClass('hidden');
            }
        },
        /**
         * Shows an information message modal
         *
         * @param infoMessage
         */
        showInfoMessage: function (infoMessage) {
            var shownMessages = store.get("shownMessages");

            if (!shownMessages || shownMessages.indexOf(infoMessage.id) === -1) {
                shownMessages = shownMessages || [];
                shownMessages.push(infoMessage.id);

                store.set("shownMessages", shownMessages);

                var modal = $("#information-message-modal");
                modal.find("p.message-content").text(infoMessage.message);
                modal.modal();
            }
        },

        showMessageHint: function (message_hint) {
            if (message_hint && message_hint.message_hint && message_hint.message_hint.message!='') {
                var modal = $("#message-simple-dialog");
                modal.find("h4.modal-title").text(message_hint.message_hint.hint_title);
                modal.find("p.message-content").text(message_hint.message_hint.message);
                modal.modal();
            }
            if (message_hint && message_hint.message_dialogue && message_hint.message_dialogue.message!='') {
                var modal = $("#message-dialog-action");
                modal.find("p.message-content").text(message_hint.message_dialogue.message);
                modal.modal();
            }
            if (message_hint && message_hint.message_warning && message_hint.message_warning.message!='') {
                var modal = $("#message-dialog-warning");
                modal.find("p.message-content").text(message_hint.message_warning.message);
                modal.modal();
            }
        },
        showBundleToaster: function () {
            var bundleDrawer = $('#bundleModal');
            var bundleInput = $(bundleDrawer).find('.bundle-to-cart-btn input[type=submit]');

            if (bundleDrawer.find('.bundle-details-header').length && bundleInput.attr('onclick')) {
                bundleDrawer.modal('show');
            }

            $('#message-dialog-warning').removeClass('message-warning-enabled');

            this.getConfigurator().assureElementsHeight();

            return this;
        },
        hideBundleToaster: function () {
            var bundleDrawer = $('#bundleModal');
            bundleDrawer.modal('hide');

            this.getConfigurator().assureElementsHeight();
            $('#message-dialog-warning').removeClass('message-warning-enabled');


            return this;
        }
    });
}(jQuery));

// Define configurator events
jQuery(document).ready(function ($) {
    // catch rules updated event
    $(document).on('omnius.configurator.rules.updated', function () {
        window.configuratorUI.refreshConfigurator();
    });

    // catch prices updated event
    $(document).on('omnius.configurator.prices.updated', function () {
        window.configuratorUI.updateProductPrices();
    });

    // catch rules updated event
    $(document).on('omnius.configurator.rule.updated', function () {
        window.configuratorUI.updateBundleCreationButtons();
    });

    $('body').on('click', function (e) {
        if (window.configuratorUI) {
            window.configuratorUI.resetFilterButton(e);
        }
    });
});