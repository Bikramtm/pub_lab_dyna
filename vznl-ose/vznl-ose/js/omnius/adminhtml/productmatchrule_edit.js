toggleDisableCategory = function (position, status) {
  var button = '.open_' + position + '_category';
  var input = '.' + position + '_category';
  var clearButton = '#' + position + '_clear_button';
  jQuery(input).prop('disabled', status);
  jQuery(button).prop('disabled', status);
  jQuery(clearButton).prop('disabled', status);
  if (status) {
    jQuery(button).addClass('disabled');
  } else {
    jQuery(button).removeClass('disabled');
  }

  // toggle open category products button
  var button = '.open_' + position + '_category_products';

  jQuery(button).prop('disabled', status);

  if (status) {
    jQuery(button).addClass('disabled');
  } else {
    jQuery(button).removeClass('disabled');
  }

  // toggle open category products button
  var button = '.open_' + position + '_category_products';

  jQuery(button).prop('disabled', status);

  if (status) {
    jQuery(button).addClass('disabled');
  } else {
    jQuery(button).removeClass('disabled');
  }
};

getIdBySku = function (sku) {
  var id = '';
  skusAutocomplete.each(function (el) {
    if (el.value == sku) {
      id = el.data;
    }
  });

  return id;
};

clearRightCat = function () {
  toggleDisableCategory('right', false);
  jQuery('.right_product').prop('disabled', false);
  jQuery('.right_product_sku').prop('disabled', false);
  jQuery('.right_category_visible').val(-1);
};

/*clearLeftCat = function () {
    toggleDisableCategory('left', false);
    jQuery('.left_product').prop('disabled', false);
    jQuery('.left_product_sku').prop('disabled', false);
    jQuery('.left_category_visible').val(-1);
};*/
setSku = function (pos, val) {
  var field = '.' + pos + '_product_sku';
  var sku = '';

  skusAutocomplete.each(function (el) {
    if (el.data == val) {
      sku = el.value;
      return false;
    }
  });

  jQuery(field).val(sku);
};
jQuery(document).ready(function () {
  /* jQuery('.left_product').on('change', function () {
        if (jQuery(this).val() == -1) {
            toggleDisableCategory('left', false);
            setSku('left', '');
        } else {
            toggleDisableCategory('left', true);
            setSku('left', jQuery(this).val());
        }
    });
*/
  /* jQuery('.left_category').on('change', function () {
        if (jQuery(this).val() == -1) {
            if (jQuery('.right_category').val() == -1) {
                jQuery('.right_product').prop('disabled', false);
                jQuery('.right_product_sku').prop('disabled', false);
            }
            jQuery('.left_product').prop('disabled', false);
            jQuery('.left_product_sku').prop('disabled', false);
            toggleDisableCategory('left', false);
        } else {
            jQuery('.left_product').prop('disabled', true);
            jQuery('.left_product_sku').prop('disabled', true);
        }
    });
*/
  jQuery('.right_product').on('change', function () {
    if (jQuery(this).val() == -1) {
      toggleDisableCategory('right', false);
      setSku('right', '');
    } else {
      toggleDisableCategory('right', true);
      setSku('right', jQuery(this).val());
    }
  });

  jQuery('.right_category').on('change', function () {
    if (jQuery(this).val() == -1) {
      if (jQuery('.right_product').val() == -1) {
        toggleDisableCategory('right', false);
      }
      jQuery('.right_product_sku').prop('disabled', false);
      jQuery('.right_product').prop('disabled', false);
    } else {
      jQuery('.right_product_sku').prop('disabled', true);
      jQuery('.right_product').prop('disabled', true);
    }
  });
  jQuery('.right_product_sku')
    .on('autocompleteselect', function (event, ui) {
      jQuery('.right_product option[value="' + ui.item.data + '"]').prop('selected', true);
    })
    .on('change', function () {
      var el = jQuery('.right_product option[value="' + getIdBySku(jQuery(this).val()) + '"]');
      if (el.length) {
        el.prop('selected', true);
      } else {
        jQuery('.right_product option[value="-1"]').prop('selected', true);
        jQuery(this).val('');
      }
      jQuery('.right_product').trigger('change');
    });
/*    jQuery(".left_product_sku").on("change", function () {
        var el = jQuery('.left_product option[value="' + getIdBySku(jQuery(this).val()) + '"]');
        if (el.length) {
            el.prop('selected', true);
        } else {
            jQuery('.left_product option[value="-1"]').prop('selected', true);
            jQuery(this).val('');
        }
        jQuery('.left_product').trigger('change');
    });*/

/*    jQuery(".left_product_sku").on("autocompleteselect", function (event, ui) {
        jQuery('.left_product option[value="' + ui.item.data + '"]').prop('selected', true);
        jQuery('.left_product').trigger('change');
    });*/
});
