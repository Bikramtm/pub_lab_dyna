/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */
jQuery(document).ready(function () {
  setInterval(function () {
    jQuery.ajax({
      type: 'POST',
      url: checkImportExecution,
      data: {form_key: form_key_checkImportExecution}
    })
      .done(function (data) {
        var importInformation = jQuery(data).html();
        var insertHere = jQuery('[id="page:main-container"]').find('.main-col-inner').next().find('.entry-edit');
        var status = jQuery(insertHere.find('[id="sandbox_general details_form"]').find('tr').get(3)).find('td.value').html().trim();
        if (status !== 'Running') {
          jQuery('.kill').addClass('disabled');
        } else {
          jQuery('.kill').removeClass('disabled');
        }
        insertHere.html(importInformation);
        jQuery('form#edit-form div').html('<input type="hidden" name="form_key" value="' + FORM_KEY + '"/>');
      });
  }, 5000);
});

function killImportExecution() {
  jQuery.ajax({
    type: 'POST',
    url: killImportExecutionUrl,
    data: {form_key: form_key_checkImportExecution}
  });
}
