jQuery(document).ready(function () {
  initReleaseForm();
});

function initReleaseForm() {
  // Hide all fields
  hideOneTimeFields();
  hidePeriodicFields();

  // Add event handlers
  jQuery('#schedule_type').change(function(){
    handleExecutionDate(jQuery(this));
  });
  jQuery('#period').change(function(){
    handlePeriodicOptions(jQuery(this));
  });

  // Trigger change event in case the release is edited
  jQuery('#schedule_type').trigger('change');

  // Make additional tweaks
  jQuery('#time')
    .attr('placeholder', 'hh:mm')
    .attr('maxlength', '5');
}

function hideOneTimeFields() {
  jQuery('#one_time_date').closest('tr').hide();
  jQuery('#one_time_date').prop('disabled', true);
}

function hidePeriodicFields() {
  jQuery('#period').closest('tr').hide();
  jQuery('#period').prop('disabled', true);
  hidePeriodicOptions();
}

function hidePeriodicOptions() {
  jQuery('#week_day').closest('tr').hide();
  jQuery('#week_day').prop('disabled', true);

  jQuery('#month_day').closest('tr').hide();
  jQuery('#month_day').prop('disabled', true);

  jQuery('#time').closest('tr').hide();
  jQuery('#time').prop('disabled', true);
}

function showOneTimeFields() {
  jQuery('#one_time_date').closest('tr').show();
  jQuery('#one_time_date').prop('disabled', false);
}

function showPeriodicFields() {
  jQuery('#period').closest('tr').show();
  jQuery('#period').prop('disabled', false);
  showPeriodicOptions();
}

function showPeriodicOptions() {
  jQuery('#week_day').closest('tr').show();
  jQuery('#week_day').prop('disabled', false);

  jQuery('#month_day').closest('tr').show();
  jQuery('#month_day').prop('disabled', false);

  jQuery('#time').closest('tr').show();
  jQuery('#time').prop('disabled', false);
}

function handleExecutionDate(jElem) {
  hideOneTimeFields();
  hidePeriodicFields();
  if (jElem.val() == '1') {
    showOneTimeFields();
  } else if (jElem.val() == '2') {
    showPeriodicFields();
    hidePeriodicOptions();
    handlePeriodicOptions(jQuery('#period'));
  }
}

function handlePeriodicOptions(jElem) {
  hidePeriodicOptions();

  jQuery('#time').closest('tr').show();
  jQuery('#time').prop('disabled', false);
  if (jElem.val() == '2') {
    jQuery('#week_day').closest('tr').show();
    jQuery('#week_day').prop('disabled', false);
  } else if (jElem.val() == '3') {
    jQuery('#month_day').closest('tr').show();
    jQuery('#month_day').prop('disabled', false);

    jQuery('#time').closest('tr').show();
    jQuery('#time').prop('disabled', false);
  } else if (jElem.val() != '1') {
    jQuery('#time').closest('tr').hide();
    jQuery('#time').prop('disabled', true);
  }
}
