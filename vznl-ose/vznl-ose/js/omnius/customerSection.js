'use strict';

(function ($) {
  window.CustomerSection = function () {
    this.newAddressRequested = false;
    this.migrationAddressSelected = false;
    this.migrationIconClicked = null;
    this.contractNoCheckedForRetention = []; // in the customer details, products tab -> the accounts checked for retention in the form {contract_no -> kias}
    this.initialize.apply(this, arguments);
  };
  window.CustomerSection.prototype = $.extend(VEngine.prototype, {
    topAddressSearch: '#topaddressCheckForm',
    prolongationCheckButton: null,
    checkProlongationMark: '.check-prolongation',
    redPlusParentSubscription: null,
    showRedPlusMemberWarning: true,
    migrationPartOfBundle: false,
    /**
         * This function is called from products-content.phtml template to setCustomerDetailsForProlongation
         * @param element
         */
    setCustomerDetailsForProlongation: function (element) {
      var self = this;
      var contractNr = $(element).data('contract');
      var ctn = {};
      ctn.ctn =  $(element).data('ctn');
      ctn.localAreaCode = $(element).data('localareacode');
      ctn.phoneNumber = $(element).data('phonenumber');

      self.contractNoCheckedForRetention.push({
        contract: contractNr,
        ctn: ctn
      });
    },
    prolongationCheckAction: function (currentId, callBack) {
      var self = this;
      // Get service call data
      $.ajax({
        url: '/customerde/details/prolongationCheck/',
        type: 'post',
        data: {
          accounts: self.contractNoCheckedForRetention,
          currentId :currentId
        },
        dataType: 'json',
        success: function (response) {
          if (response.validationError || response.error) {
            self.clearProlongationList();
            showModalError(response.message);
            return;
          }

          if (typeof callBack === 'function') {
            self.clearProlongationList();
            //Render response on callback function
            callBack(response);
          }
        }
      });
    },
    clearProlongationList: function () {
      //Clear prolongation list
      this.contractNoCheckedForRetention = [];
    },
    toggle: function (clickedButton, elementSelector, time) {
      var isContracted = $(clickedButton).hasClass('contract-product-details');
      $(elementSelector).slideToggle(time, function () {
        if(isContracted) {
          $(clickedButton).removeClass('contract-product-details');
          $(clickedButton).addClass('expand-product-details');
        }
        else {
          $(clickedButton).removeClass('expand-product-details');
          $(clickedButton).addClass('contract-product-details');
        }
      });
    },
    checkProlongation: function (element) {
      var self = customerSection;
      var prologationState = false;
      var prolongationBtn = $(element).parent().parent().parent().find('.prolongation-button');
      var prolongationWarning = $(element).parent().parent();
      var convertToMemberBtn = $(element).parent().parent().parent().find('.convert-to-member-button');
      //Set the customer for which the prolongation is done
      $('.prolongation-modal').addClass('prolongation-modal-centered');
      $('.prolongation-modal').show();
      $('.no-prolongation-available').removeClass('hidden').addClass('hidden');
      $('.template-content').html('<span class="loading-search-prolongation-modal"></span>');
      $('.list-group').removeClass('hidden').addClass('hidden');
      self.setCustomerDetailsForProlongation(element);
      var phoneNumber = $.trim($(element).data('phonenumber'));
      var localAreaCode = $.trim($(element).data('localareacode'));
      phoneNumber = localAreaCode != '' ? localAreaCode + phoneNumber : phoneNumber;
      var currentId = $(element).data('contract') + '-' +  phoneNumber;

      //Executing prolongation check within customer object
      self.prolongationCheckAction(currentId, function (prolongationResult) {
        $.each(prolongationResult.options, function (htmlId, items) {
          if(htmlId === currentId) {
            prologationState = true;
            var template = Handlebars.compile($('#prolongation-check-info').html());
            var prolongationDetails = template(items);
            if(items.length > 0){
              $('.list-group').removeClass('hidden');
              $('.template-content').html(prolongationDetails);
              $.each([convertToMemberBtn, prolongationBtn], function (keyButton, button) {
                var dropdown = button.next('.dropdown-menu');
                var countItems = 0;
                if (dropdown.find('li[data-type=\'CONVERT-TO-MEMBER\']')) {
                  countItems = 1;
                }
                $.each(items, function (key, data) {
                  if(data.disabledButton){
                    jQuery(dropdown.find('li[data-type=\'' + data.type.key + '\']')).children('a').removeClass('disabled').addClass('disabled').attr('onclick','');
                    dropdown.find('li[data-type=\'' + data.type.key + '\']').removeClass('disabled').addClass('disabled');
                  }
                  if (data.status.key !== 'N') {
                    if (data.type.key == 'INO') {
                      data.type.key = 'INO'
                    } else if (data.type.key == 'INL') {
                      data.type.key = 'INL'
                    }
                    if (data.days !== '30+') {
                      dropdown.find('li[data-type=\'' + data.type.key + '\']').removeClass('hidden').children('a.trigger')
                        .data('prolongation-type', data.type.key)
                        .data('prolongation-info-eligibility', data.status.key);
                      countItems++;
                    }
                  }
                });
                if (countItems > 0) {
                  if(button == prolongationBtn && button.hasClass('disabled')){
                    button.removeClass('disabled');
                  }
                  dropdown.removeClass('hidden');
                  button.find('span.dropdown-changeable-icon').removeClass('hidden');
                  button.removeAttr('onclick');
                } else {
                  button.removeClass('disabled').addClass('disabled');
                  button.find('span.dropdown-changeable-icon').removeClass('hidden').addClass('hidden');
                }
                prolongationWarning.addClass('hidden');
              });
            }
          }
        });

        if(prologationState){
          var container = prolongationBtn.parent().parent();
          container.find('div.ils-button').removeClass('hidden');
        }else{
          $('.loading-search-prolongation-modal').removeClass('hidden').addClass('hidden');
          $('.prolongation-modal-title').removeClass('hidden').addClass('hidden');
          $('.template-content').html('<span class="hidden no-prolongation-available">No prolongation check is available for this BAN!</span>');
          $('.no-prolongation-available').removeClass('hidden');
        }

      });
    },
    customerNoToMigrateOrMoveOffnet: null,
    migrationOrMoveStarted: false,
    //Once the Open configurator button is clicked, an ajax call sets the migration flag on session
    migrateToCable: function (element, installedProductName, customerNoToMigrate, partOfBundle) {
      var self = this;
      var alertElement = $(element).parents('.product-details-content-container').find('.alert-disable-buttons');
      if (alertElement.length > 0 && !alertElement.hasClass('hidden')) {
        console.error('Cannot perform migration to cable due to existing alert');
        return;
      }
      self.performSetSourceMigrationPackage(element);
      if (!self.checkMoveMigrationCanStart('migration')) {
        showModalError(Translator.translate('You already have a package in the shopping cart. Please remove the package in order to continue!'), 'Warning');
        return;
      }
      self.customerNoToMigrateOrMoveOffnet = customerNoToMigrate;
      self.migrationPartOfBundle = partOfBundle;
      self.migrationIconClicked = $(element);
      //By default icon is inactive
      if (self.migrationIconClicked.hasClass('selected')) {
        this.disableFixedToCableMigration();
      } else {
        this.enableFixedToCableMigration(installedProductName)
      }
    },
    moveOffnetToDsl: function (element, baseKDProductAddressId, installedProductName, customerNoToMoveOffnet, partOfBundle) {
      var self = this;
      var alertElement = $(element).parents('.product-details-content-container').find('.alert-disable-buttons');
      if (alertElement.length > 0 && !alertElement.hasClass('hidden')) {
        console.error('Cannot perform move offnet due to existing alert');
        return;
      }
      address.resetMoveServiceAddressCheckModal();
      self.performSetSourceMigrationPackage(element);
      if (!self.checkMoveMigrationCanStart('move-offnet')) {
        showModalError(Translator.translate('You already have a package in the shopping cart. Please remove the package in order to continue!'), 'Warning');
        return;
      }
      self.customerNoToMigrateOrMoveOffnet = customerNoToMoveOffnet;
      self.migrationPartOfBundle = partOfBundle;
      var migrationIcon = $(element);
      //By default icon is inactive
      if (migrationIcon.hasClass('selected')) {
        this.disableCableToFixedMoveOffnet(migrationIcon);
      } else {
        this.enableCableToFixedMoveOffnet(migrationIcon, baseKDProductAddressId, installedProductName, element);
      }
    },
    // todo delete or reuse after clarification VFDED1W3S-1465
    // moveOffnetToDsl: function (element) {
    //     var self = this;
    //     if (!self.checkMoveMigrationCanStart('move-offnet')) {
    //         showModalError(Translator.translate("You already have a package in the shopping cart. Please remove the package in order to continue!"), 'Warning');
    //         return;
    //     }
    //     var serviceabilityForMoveOffnetBtn = $(element);
    //     var baseKDProductAddressId = serviceabilityForMoveOffnetBtn.data('id');
    //     var customerNo = serviceabilityForMoveOffnetBtn.data('customer');
    //     //By default icon is inactive
    //     if (serviceabilityForMoveOffnetBtn.hasClass('selected')) {
    //         this.disableCableToFixedMoveOffnet(serviceabilityForMoveOffnetBtn);
    //     } else {
    //         this.enableCableToFixedMoveOffnet(serviceabilityForMoveOffnetBtn, baseKDProductAddressId, customerNo);
    //     }
    // },
    disableFixedToCableMigration: function () {
      var self = this;
      self.migrationIconClicked.removeClass('selected');
    },
    disableCableToFixedMoveOffnet: function (migrationIcon) {
      migrationIcon.removeClass('selected');
    },
    enableFixedToCableMigration: function (installedProductName) {
      var self = this;
      self.installedProductName = installedProductName;
      self.migrationAddressSelected = true;

      var callback = function () {
        // if Cable or just KIP is not available at the address show error message
        if (!self.checkIsKipAvailableAtAddress()) {
          // show error stating No technology available at new address
          if (!self.checkCableIsAvailableAtAddress()) {
            showModalError(Translator.translate('Cable is not possible at this service address, therefore migration is not possible.'));
          } else {
            showModalError(Translator.translate('Cable Internet & telephone is not possible at this service address, therefore migration is not possible.'));
          }
        }
      };

      // Perform serviceAvailability call for the installed DSL product service address if the address-id is found
      if(self.migrationIconClicked.attr('data-id')) {
        address.checkService(self.migrationIconClicked);
      }
      // No service address id received, triggering validate address (see OMNVFDE-2444)
      else {
        var topAddressSearch = $(self.topAddressSearch);
        topAddressSearch.find('input[name="postcode"]').val(self.migrationIconClicked.attr('data-postalcode'));
        topAddressSearch.find('input[name="city"]').val(self.migrationIconClicked.attr('data-city-name'));
        topAddressSearch.find('input[name="street"]').val(self.migrationIconClicked.attr('data-street'));
        topAddressSearch.find('input[name="houseNo"]').val(self.migrationIconClicked.attr('data-building-nr'));
        topAddressSearch.find('input[name="addition"]').val( self.migrationIconClicked.attr('data-house-addition'));
        address.callBack = callback;
        address.checkServiceabilityHeader(self.topAddressSearch);
      }

      // if Cable or just KIP is not available at the address show error message
      if (!self.checkIsKipAvailableAtAddress()) {
        // show error stating No technology available at new address
        if (!self.checkCableIsAvailableAtAddress()) {
          showModalError(Translator.translate('Cable is not possible at this service address, therefore migration is not possible.'));
        } else {
          showModalError(Translator.translate('Cable Internet & telephone is not possible at this service address, therefore migration is not possible.'));
        }
      }
    },
    // todo delete or reuse after clarification VFDED1W3S-1465
    // enableCableToFixedMoveOffnet: function (serviceabilityForMoveOffnetBtn, baseKDProductAddressId, customerNo) {
    //     serviceabilityForMoveOffnetBtn.addClass('selected');
    //     $('#cart-spinner').removeClass('hide');
    //
    //     var self = this;
    //     address.showCheckModal(function () {
    //         // the address ID inserted in the serviceability call must be different than the current address id of the base KD product
    //         if (baseKDProductAddressId == address.services.address_id) {
    //             showModalError(Translator.translate("Please insert a different address than the service address on this installed cable product."));
    //             serviceabilityForMoveOffnetBtn.removeClass('selected');
    //             return;
    //         }
    //
    //         // OMNVFDE-2663
    //         // show error stating No technology available at new address
    //         if (!self.checkCableIsAvailableAtAddress() && !self.checkDslIsAvailableAtAddress()) {
    //             showModalError(Translator.translate("No technology available, move-offnet is not possible."));
    //             // mark the migration icon as not selected
    //             serviceabilityForMoveOffnetBtn.removeClass('selected');
    //             return;
    //         }
    //         // show error stating DSL technology is not available at new address
    //         if (!self.checkDslIsAvailableAtAddress()) {
    //             showModalError(Translator.translate("DSL is not available. Move off-net is not possible."));
    //             // mark the migration icon as not selected
    //             serviceabilityForMoveOffnetBtn.removeClass('selected');
    //             return;
    //         }
    //         // show error stating KIP technology is available at new address
    //         if (self.checkIsKipAvailableAtAddress() && self.checkDslIsAvailableAtAddress()) {
    //             showModalError(Translator.translate("Cable is available. Move off-net is not possible."));
    //             // mark the migration icon as not selected
    //             serviceabilityForMoveOffnetBtn.removeClass('selected');
    //             return;
    //         }
    //
    //         // enable button
    //         if (!self.checkIsKipAvailableAtAddress() && self.checkDslIsAvailableAtAddress()) {
    //             serviceabilityForMoveOffnetBtn.parent().parent().addClass('hidden');
    //             var containerButtons = serviceabilityForMoveOffnetBtn.parent().parent().parent();
    //             containerButtons.find('button.button-requires-serviceability-kip').removeClass('disabled');
    //             sessionStorage.setItem('modeOffNetAddressLoadedForKip', JSON.stringify(customerNo));
    //         }
    //     });
    // },
    enableCableToFixedMoveOffnet: function (migrationIcon, baseKDProductAddressId, installedProductName, element) {
      migrationIcon.addClass('selected');
      $('#cart-spinner').removeClass('hide');

      var self = this;
      var params = {
        moveOffnetScenario: true
      };

      address.showCheckModal(function () {
        // the address ID inserted in the serviceability call must be different than the current address id of the base KD product
        if (baseKDProductAddressId == address.services.address_id) {
          showModalError(Translator.translate('Please insert a different address than the service address on this installed cable product.'));
          migrationIcon.removeClass('selected');
          return;
        }

        // OMNVFDE-2663
        // show error stating No technology available at new address
        if (!self.checkCableIsAvailableAtAddress() && !self.checkDslIsAvailableAtAddress()) {
          $(address.moveServiceAddressCheckModal).find('#notificationContainer').removeClass('hidden');

          showModalError(Translator.translate('No technology available, move-offnet is not possible.'));
          // mark the migration icon as not selected
          migrationIcon.removeClass('selected');
          return;
        }
        // show error stating DSL technology is not available at new address
        if (!self.checkDslIsAvailableAtAddress()) {
          $(address.moveServiceAddressCheckModal).find('#notificationContainer').removeClass('hidden');

          showModalError(Translator.translate('DSL is not available. Move off-net is not possible.'));
          // mark the migration icon as not selected
          migrationIcon.removeClass('selected');
          return;
        }
        // show error stating KIP technology is available at new address
        if (self.checkIsKipAvailableAtAddress() && self.checkDslIsAvailableAtAddress()) {
          $(address.addressCheckModal).modal('hide');
          $(address.moveServiceAddressCheckModal).modal('hide');

          showModalError(Translator.translate('Cable is available. Move off-net is not possible.'));
          // mark the migration icon as not selected
          migrationIcon.removeClass('selected');
          return;
        }
        // show right side with "to configurator" btn and block "Kable: move offnet <br/> <installed cable product name>"
        if (!self.checkIsKipAvailableAtAddress() && self.checkDslIsAvailableAtAddress()) {
          $(address.moveServiceAddressCheckModal).find('#notificationContainer').removeClass('hidden');
          $(address.moveServiceAddressCheckModal).find('#suggestionContainer').removeClass('hidden');
          $(address.moveServiceAddressCheckModal + ' #move-to-dsl-or-keep-product').attr('data-package-creation-type-id', $(element).data('package-creation-type-id'));
          self.migrationOrMoveStarted = true;
        }
      }, params);
    },

    // todo delete or reuse after clarification VFDED1W3S-1465
    // initiateMoveOffnet: function(button, installedProductName, customerNoToMoveOffnet) {
    //     var self = this;
    //     self.customerNoToMigrateOrMoveOffnet = customerNoToMoveOffnet;
    //     // show right side with "to configurator" btn and block "Kable: move offnet <br/> <installed cable product name>"
    //     // cable is available: add the right side with a "To configurator" btn and a block "Kabel: Migration <br> <name of the fixed installed product>"
    //     self.addRightSideToConfiguratorForCableToDsl(installedProductName);
    // },
    addRightSideToConfiguratorForDslToCable: function (installedProductName) {
      $(address.moveServiceAddressCheckModal).find('#suggestionContainer').removeClass('hidden');
      var self = this,
        packageTypes = $('#package-types'),
        // this is the right side content that should appear instead of the packages
        contentHtml = $('#migrate-to-cable-template').html(),
        noPackageZone = $('.cart_packages').find('.no-package-zone');

      // Hide list of package type
      packageTypes.addClass('hide').after(contentHtml);
      // add the name of the installed fixed product that will be migrated
      $('#installed-fixed-product-name').text(installedProductName);
      self.migrationOrMoveStarted = true;

      // show the proper "to configurator" btn depending on the services available
      var migrateToKipBtn = $('#migrate-to-configurator-kip'),
        migrateToKaaKadBtn = $('#migrate-to-configurator-kaa-kad'),
        migrateToKipKaaKadBtn = $('#migrate-to-configurator-kip-kaa-kad');

      migrateToKipBtn.removeClass('hide');
      migrateToKaaKadBtn.remove();
      migrateToKipKaaKadBtn.remove();
      // remove the create package cart button, only leave the menu available
      noPackageZone.addClass('hide');

    },
    addRightSideToConfiguratorForCableToDsl: function (installedProductName, element) {
      var self = this,
        packageTypes = $('#package-types'),
        // this is the right side content that should appear instead of the packages
        contentHtml = $('#migrate-to-dsl-template').html();
      var template = Handlebars.compile($('#migrate-to-dsl-template').html());
      contentHtml = template({
        'packageCreationTypeId' : $(element).data('package-creation-type-id')
      });
      // Hide list of package type
      packageTypes.addClass('hide').after(contentHtml);
      // add the name of the installed cable product that will be migrated
      $('#installed-cable-product-name').text(installedProductName);
      self.migrationOrMoveStarted = true;

      // show the proper "to configurator" btn depending on the services available
      var moveToDsl = $('#move-to-dsl'),
        moveToDslOrKeepCable = $('#move-to-dsl-or-keep-product');
      /**
             * Agent wants to migrate a Cable product. The only address found is a DSL address =>
             * the configurator must be opened with a DSL package (with the migration flag)
             */
      if (self.checkDslIsAvailableAtAddress() && !self.checkCableIsAvailableAtAddress()) {
        moveToDsl.removeClass('hide');
        moveToDslOrKeepCable.remove();
      }
      else {
        moveToDslOrKeepCable.removeClass('hide');
        moveToDsl.remove();
      }
    },
    showMigrationModalDecision: function () {
      var self = this;
      var template = Handlebars.compile($('#migrate-dsl-to-cable-tv-or-cable-internet-and-phone').html()),
        chooseCablePackageContent = template({
          'fullAddress': address.fullAddress,
          'partOfBundle': self.migrationPartOfBundle
        });
      $('#migrateToCableDecideModal').html(chooseCablePackageContent).appendTo('body').modal();
    },
    showMoveOffnetModalDecision: function (element) {
      var self = this;
      //Cable and DSL technology available: Show pop-up and ask if client wants to switch to cable instead of migrating
      var template = Handlebars.compile($('#migration-switch-to-cable-modal-content').html());
      var switchToCableContent = template({
        'fullAddress': address.fullAddress,
        'addressId': address.addressId,
        'partOfBundle': self.migrationPartOfBundle,
        'packageCreationTypeId' : $(element).data('package-creation-type-id')
      });
      $('#migrateToDslModal').html(switchToCableContent).appendTo('body').modal();
    },
    removeTempRightSideBlock: function () {
      $('.migrate-or-move-offnet-container').remove();
    },
    cancelMoveOffnet: function () {
      var self = this;
      if ($('.cart_packages div.side-cart.block:visible').length == 0) {
        //$("#package-types").removeClass('hide');
        $('.text-center.no-package-zone').removeClass('hide');
      }
      self.migrationOrMoveStarted = false;
      self.removeTempRightSideBlock();
    },
    /**
         * remove the right side with the "to config" btn and the block with "cable/dsl move/migration label and product name"
         * and after initialize the configurator
         * @param element
         * @param confirmed
         */
    initMigrationOrMoveOffnetConfigurator: function (element, confirmed) {
      var self = this;
      $(element).attr('data-customer-no-migration-or-move-offnet', self.customerNoToMigrateOrMoveOffnet);
      if (self.migrationPartOfBundle == true && !confirmed) {
        self.migrationConfirmationElement = element;
        var confirmationModal = $('#migration-bundle-modal');
        confirmationModal.modal();
        return;
      }
      sidebar.initConfigurator(element);
      self.removeTempRightSideBlock();
    },
    checkKipAddressForMoveOffnetAlreadyLoaded: function() {
      var modeOffNetAddressLoadedForKipJson = sessionStorage.getItem('modeOffNetAddressLoadedForKip');
      var modeOffNetAddressLoadedForKip = JSON.parse(modeOffNetAddressLoadedForKipJson);
      if(modeOffNetAddressLoadedForKip) {
        $('button.button-requires-serviceability-kip[data-customer=\''+modeOffNetAddressLoadedForKip+'\']').removeClass('disabled');
        $ ('.alert-moveoffnet[data-customer=\''+modeOffNetAddressLoadedForKip+'\']').addClass('hidden');
      }
    },
    checkServiceabilityIsLoaded: function () {
      if (typeof address == 'undefined') {
        return false;
      }
      if (address == null) {
        return false;
      }
      if (!address.hasOwnProperty('services')) {
        return false;
      }
      if (address.services == null) {
        return false;
      }
      if (!address.services.hasOwnProperty('top_bar_available_services')) {
        return false;
      }
      if (address.services.top_bar_available_services == null) {
        return false;
      }

      $('.products-icon-info.serviceability-loading').hide();
      $('.products-icon-info.serviceability-done').show();
    },
    checkCableIsAvailableAtAddress: function () {
      var self = this;
      if (self.checkIsKipAvailableAtAddress() || self.checkIsKAAorKADAvailableAtAddress()) {
        return true;
      }
      return false;
    },
    checkIsKipAvailableAtAddress: function () {
      if (
        address.services.top_bar_available_services.hasOwnProperty('KIP') &&
                address.services.top_bar_available_services.KIP.hasOwnProperty('color') &&
                address.services.top_bar_available_services.KIP.color == 'green') {
        return true;
      }
      return false;
    },
    checkIsKADAvailableAtAddress: function () {
      if (address.services.top_bar_available_services.hasOwnProperty('KAD') &&
                address.services.top_bar_available_services.KAD.hasOwnProperty('color') &&
                address.services.top_bar_available_services.KAD.color == 'green'
      ) {
        return true;
      }
      return false;
    },
    checkIsKAAAvailableAtAddress: function () {
      if (address.services.top_bar_available_services.hasOwnProperty('KAA') &&
                address.services.top_bar_available_services.KAA.hasOwnProperty('color') &&
                address.services.top_bar_available_services.KAA.color == 'green'
      ) {
        return true;
      }
      return false;
    },
    checkIsKAAorKADAvailableAtAddress: function () {
      if (this.checkIsKAAAvailableAtAddress() || this.checkIsKADAvailableAtAddress()) {
        return true;
      }
      return false;
    },
    checkDslIsAvailableAtAddress: function () {
      if (
        address.services.top_bar_available_services.hasOwnProperty('(V)DSL') &&
                address.services['top_bar_available_services']['(V)DSL'].hasOwnProperty('color') &&
                address.services['top_bar_available_services']['(V)DSL']['color'] == 'green') {
        return true;
      }
      return false;
    },
    /**We cannot add 2 packages: nor 2 migrations/move offnet nor 1 normal package and a migration/move
         * OMNVFDE-1690
         *
         * Exception: OMNVFDE-3735
         * We can have migration with cable Tv or Cable independent in the cart
         * @returns {boolean}
         */
    checkMoveMigrationCanStart: function (type) {
      var self = this;
      var packageAddedInCart = $('.cart_packages .side-cart').is('[data-package-id]');
      var packagesAdded = $('.cart_packages .side-cart[data-package-id]');
      var packagesTypesInCart = [];
      var packCableInternetAndPhoneAddedInCart = false;
      var otherPackAddedExceptCableInCart = false;
      if(packagesAdded.length) {
        $.each(packagesAdded, function(key, pack) {
          var packageType = $(pack).data('package-type');
          packagesTypesInCart[key] = packageType;
          if(packageType == CABLE_INTERNET_PHONE_PACKAGE) {
            packCableInternetAndPhoneAddedInCart = true;
          }
          if(CABLE_PACKAGES.indexOf(packageType) < 0) {
            otherPackAddedExceptCableInCart = true;
          }
        });
      }

      if (type == 'migration') {
        packageAddedInCart = $('.cart_packages .side-cart[data-package-type="cable_internet_phone"]').is('[data-package-id]');
      } else if (type == 'move-offnet') {
        packageAddedInCart = $('.cart_packages .side-cart[data-package-type="dsl"]').is('[data-package-id]');
      }

      if (self.migrationOrMoveStarted || packageAddedInCart) {
        return false;
      }

      // if there is a cable internet and phone package in the cart => error
      if(packCableInternetAndPhoneAddedInCart) {
        return false;
      }
      // if there is a cable package in cart and we are in migration => allow
      if(packageAddedInCart) {
        return isMigration;
      }

      // if there no package cart => allow
      return true;

    },
    switchProductDetails: function (type, elem, index) {
      if ($(elem).hasClass('active')) {
        return;
      }

      var customerNumber = $(elem).parents('.products-sub-package:first').data('customer-number');
      var data = customerDe.customerProducts.KD[customerNumber].contracts[0].subscriptions[0].products[type];
      var template = Handlebars.compile($('#customer-screen-cable-{0}-product'.format(type.toLowerCase())).html()),
        cableTvPackageContent = template({products: data, position: index, activeProduct: data[index]});


      $(elem).parents('.card-block:first').html(cableTvPackageContent);
    },
    switchPanel: function (type, elem, index) {
      if ($(elem).hasClass('active')) {
        return;
      }
      var data = null;
      var devices = null;
      var customerNumber = $(elem).parents('.products-sub-package:first').data('customer-number');

      if (index !== null) {
        data = customerDe.customerProducts.FN[customerNumber].contracts[0].subscriptions[index].options[0];
        devices = customerDe.customerProducts.FN[customerNumber].contracts[0].subscriptions[index].devices;
      } else {
        data = customerDe.customerProducts.FN[customerNumber].contracts[0];
      }

      if (devices) {
        data['devices'] = devices;
      }

      var template = Handlebars.compile($('#fixed-products-details-{0}-template'.format(type.toLowerCase())).html()),
        subPackageContent = template(data);

      $(elem).siblings().removeClass('active');
      $(elem).addClass('active');

      $(elem).parents('.card-block:first').find('.right-column-content').html(subPackageContent);
    },
    addRedPlusMemberAfterConfirmation: function() {
      var self = this;

      self.showRedPlusMemberWarning = false;
      self.addRedPlusMember(self.redPlusParentSubscription);
      jQuery('#redplus-reserved-member-warning').modal('toggle');
      self.showRedPlusMemberWarning = true;
    },
    addRedPlusMember: function(element) {
      var self = this;
      var button = jQuery(element);

      var showReservedMembersWarning = button.attr('data-has-reserved-member');

      if (showReservedMembersWarning && self.showRedPlusMemberWarning) {
        var modal = jQuery('#redplus-reserved-member-warning');
        modal.find('.confirmation-message').html(Translator.translate('The customer has future reserved activations for this Red+ group.'));
        self.redPlusParentSubscription = element;
        modal.find('#break-redplus-button').attr('onclick', 'customerSection.addRedPlusMemberAfterConfirmation()');
        modal.modal();

        return;
      }
      window.setPageLoadingState(true);
      var data = {};
      data.sharingGroupId = button.data('sharing-group-id');
      data.relatedProductSku = button.data('redplus-related-product');
      data.bundleId = button.data('bundle-id');
      data.subscriptionNumber = button.data('customer-number');
      data.productId = button.data('product-id');

      $.ajax({
        type: 'POST',
        url: '/configurator/cart/addRedPlusMember',
        data: data,
        success: function(response) {
          window.setPageLoadingState(false);
          if (response.hasOwnProperty('error') && response.error == true) {
            showModalError(response.message);
          } else {
            $('.cart_packages')
              .replaceWith(response.rightBlock)
              .promise()
              .done(function () {
                // initConfigurator for Red+ package
                if (response.hasOwnProperty('initNewPackage') && response.initNewPackage) {
                  $('.enlargeSearch').trigger('click');
                  var redPlusPackage = $('.cart_packages')
                    .find('[data-package-id="' + response.initNewPackage + '"]')
                    .first()
                    .find('.block-container');
                  activateBlock(redPlusPackage);
                }
              });
          }
        }
      });
    },
    createRedPlusGroup: function(element) {
      var self = this;
      var button = jQuery(element);

      var data = {};
      data.subscriptionNumber = button.data('customer-number');
      data.productId = button.data('product-id');
      data.bundleId = button.data('bundle-id');

      // fix OVG-2067
      button.addClass('disabled');

      $.ajax({
        type: 'POST',
        url: '/bundles/index/create',
        data: data,
        success: function(response) {
          if (response.hasOwnProperty('error') && response.error == true) {
            showModalError(response.message);
          } else {
            // after done updating DOM, update customer section eligible bundles and trigger initConfigurator for newly created redPlus package
            $('.cart_packages')
              .replaceWith(response.rightBlock)
              .promise()
              .done(function () {
                if (response.hasOwnProperty('eligibleBundles')) {
                  self.updateMyProductsEligibleBundles(response.eligibleBundles);
                }
                // initConfigurator for Red+ package
                if (response.hasOwnProperty('initNewPackage') && response.initNewPackage) {
                  $('.enlargeSearch').trigger('click');
                  var redPlusPackage = $('.cart_packages')
                    .find('[data-package-id="' + response.initNewPackage + '"]')
                    .first()
                    .find('.block-container');
                  activateBlock(redPlusPackage);
                  $('#package-types').addClass('hide');
                  $('.no-package-zone').addClass('hide');
                }
              });
          }
        },
        done: function (res) {
          button.removeClass('disabled');
        }
      });
    },
    updateMyProductsEligibleBundles: function(bundles) {
      if (bundles) {
        // first disable all Add Member buttons
        jQuery('input.add-redplus-my-products-button').addClass('hidden');
        bundles.forEach(function (bundle) {
          // find any eligible subscriptions and enable/disable the button
          bundle.targetedSubscriptions.forEach(function(subscription) {
            var container = jQuery('#mobile-product-' + subscription.customerNumber + '-' + subscription.productId);
            var button = container.find('input.add-redplus-my-products-button');
            button.removeClass('hidden');
          });
        });
      }
    },
    toggleProductTabDetails: function(element) {
      var tab = $(element);
      var tabToShow = tab.data('tab');
      var productDetailsSection = $(tab).closest('.collapse');
      $.each(productDetailsSection.find('.tab-title'), function(key, tabTitle) {
        var tabTitleElem = $(tabTitle);
        if(tabTitleElem.data('tab') == tabToShow) {
          tabTitleElem.addClass('active');
        }
        else {
          tabTitleElem.removeClass('active');
        }
      });

      $.each(productDetailsSection.find('.tab-details'), function(key, tabDetails) {
        var tabDetailsElem = $(tabDetails);
        if(tabDetailsElem.hasClass(tabToShow)) {
          tabDetailsElem.removeClass('hide');
        }
        else {
          tabDetailsElem.addClass('hide');

        }
      });
    },
    changePackage: function(element, context){
      var self = this;
      if (!self.checkMoveMigrationCanStart('migration') || !self.checkMoveMigrationCanStart('move-offnet')) {
        self.cancelMoveOffnet();
      }
      setPageLoadingState(true);
      var data = {
        'button': $(element).data('button'),
        'stack': $(element).data('stack'),
        'customer_id': $(element).data('customer-id'),
        'service_line_id': $(element).data('service-line-id'),
        'products': $(element).data('skus'),
        'tariff': $(element).data('tariff'),
        'allSocs': $(element).data('all-socs'),
        'package_type': $(element).data('package-type'),
        'package_creation_type': $(element).data('package-creation-type'),
        'ctn': $(element).data('ctn'),
        'context': context,
        'prolongation_type': $(element).data('prolongation-type'),
        'prolongation_info_eligibility': $(element).data('prolongation-info-eligibility'),
        'contract_end_date' : $(element).data('contract-end-date'),
        'contract_possible_cancellation_date' : $(element).data('contract-possible-cancellation-date'),
        'convert_to_member': $(element).data('convert-to-member'),
        'product_id': $(element).data('product-id'),
        'customer_number': $(element).data('customer-number'),
        'redplus_role' : $(element).data('redplus-role'),
        'bundle_product_ids': $(element).data('bundle-product-ids')
      };

      $.ajax({
        type: 'POST',
        url: '/configurator/index/inlife',
        data: data,
        success: function (response) {
          if (response.error) {
            showModalError(response.message, 'Error creating Inlife package');
            setPageLoadingState(false);
          } else {
            if (response.hasOwnProperty('rightBlock') && response.rightBlock) {
              $('.cart_packages')
                .replaceWith(response.rightBlock)
                .promise()
                .done(function () {
                  // initConfigurator for inlife packages
                  if (response.hasOwnProperty('package_id') && response.package_id) {
                    $('.enlargeSearch').trigger('click');
                    var inlifePackage = $('.cart_packages')
                      .find('[data-package-id="' + response.package_id + '"]')
                      .first()
                      .find('.block-container');
                    activateBlock(inlifePackage);
                    $('#package-types').addClass('hide');
                    $('.no-package-zone').addClass('hide');
                  }
                });
            }
            if (response.hasOwnProperty('totals')) {
              var totals = $('#cart_totals');
              totals.replaceWith(response.totals)
            }
            if(response.hasOwnProperty('customMessage')){
              showModalError(response.customMessage, 'Inlife Information');
              setPageLoadingState(false);
            }
          }
        },
        error: function () {
          setPageLoadingState(false);
        }
      });
    },

    performSetSourceMigrationPackage: function (element){
      var data = {
        'package_creation_type': $(element).data('package-creation-type')
      };

      $.ajax({
        type: 'POST',
        url: '/configurator/index/setSourceMigration',
        data: data,
        success: function (response) {
          if (response.error) {
            showModalError(response.message, 'Error setting source package');
            setPageLoadingState(false);
          }
        },

        error: function () {
          setPageLoadingState(false);
        }
      });

    },

    performCheckServiceEligibility: function (element) {
      setPageLoadingState(true);
      var self = this;
      if (!self.checkMoveMigrationCanStart('migration') || !self.checkMoveMigrationCanStart('move-offnet')) {
        self.cancelMoveOffnet();
      }
      address.callBack = function () {
        address.callBack = null;
        setPageLoadingState(false);
        if (address.services) {
          if (address.services.clearCart) {
            if ($('.enlargeSearch').hasClass('left-direction')) {
              $('.enlargeSearch').trigger('click');
            }
          }
          var currentEligibilityContainer = $(element).parent().parent();
          var eligibilityContainers = $('.product-details-content-container .eligibility-check');

          eligibilityContainers.each(function (index, eligibilityElement) {
            var containerILSbuttons = $(eligibilityElement).parent();
            if (currentEligibilityContainer.data('serialized-key') !== $(eligibilityElement).data('serialized-key')) {
              // Do NOT enable eligibility check for products with dunning alert
              var productsSection = $(eligibilityElement).closest('div.products-section');
              if ( productsSection.find('div.products-order-dunning').length == 0 ||
                                (productsSection.find('div.products-order-dunning').length != 0 &&
                                 productsSection.find('div.products-order-dunning').data('customer-number') != $(eligibilityElement).data('customer-number')))
              {
                $(eligibilityElement).removeClass('hidden');
              }
              containerILSbuttons.find('button:not(.eligibility-btn)').addClass('disabled-mandatory');
            } else {
              $(eligibilityElement).addClass('hidden');
              containerILSbuttons.find('button:not(.eligibility-btn)').removeClass('disabled-mandatory');
            }
          });
          // if KIP technology is not available, disable modify package button
          var modifyPackageBtn = jQuery($(element)).parent().parent().prev().find('.products-buttons-container button').first();
          if ($(element).attr('data-kip-stack') && (address.services.cableAvailability.KIP === false)){
            modifyPackageBtn.addClass('disabled-mandatory');
          } else {
            modifyPackageBtn.removeClass('disabled-mandatory');
          }
          // if DSL technology is not available disable modify package button
          if ($(element).attr('data-fn-stack') && (address.services.fnAvailability['(V)DSL'] === false)) {
            modifyPackageBtn.addClass('disabled-mandatory');
          } else {
            modifyPackageBtn.removeClass('disabled-mandatory');
          }
          //clear bundles
          if(typeof configurator !== 'undefined'){
            configurator.eligibleBundles = [];
          }
        } else {
          showModalError(Translator.translate('Invalid address'));
        }
      };
      address.checkService(element);

      // setPageLoadingState(true);
      // address.callBack = function(){
      //     setPageLoadingState(false);
      //     if (address.services) {
      //         $(element).parent().parent().addClass('hidden');
      //         var containerILSbuttons = $(element).parent().parent().parent();
      //         containerILSbuttons.find('button').removeClass('hidden');
      //     }
      // };
    },
    showMoreInfo: function(element) {
      var entireTab = $(element).closest('.tab-details');
      var currentTab = $(element).closest('.expandable-info').find('.tab-content');
      if(currentTab.hasClass('expanded')) {
        currentTab.removeClass('expanded');
        currentTab.addClass('reduced');
        return;
      }

      entireTab.find('.expanded').each(function() {
        $( this ).removeClass('expanded');
        $( this ).addClass('reduced');
      });

      currentTab.removeClass('reduced');
      currentTab.addClass('expanded');
    }
  });
  $(document).ready(function () {
    $(document).bind('serviceability-check-completed', function () {
      if(customerSection.newAddressRequested && customerSection.migrationAddressSelected) {
        customerSection.newAddressRequested = false;
        customerSection.migrationAddressSelected = false;
        // mark the migration icon as not selected
        customerSection.migrationIconClicked.removeClass('selected');

        // cable is available: add the right side with a "To configurator" btn and a block "Kabel: Migration <br> <name of the fixed installed product>"
        if (customerSection.checkIsKipAvailableAtAddress()) {
          customerSection.addRightSideToConfiguratorForDslToCable(customerSection.installedProductName);
        }
      }
      $('#cart-spinner').addClass('hide');
      $('.products-icon-info.serviceability-loading').hide();
      $('.products-icon-info.serviceability-done').show();
    });
    $(document).bind('serviceability-check-started', function () {
      customerSection.newAddressRequested = true;
      if(customerSection.migrationAddressSelected) {
        $('#cart-spinner').removeClass('hide');
        // mark the migration icon as selected
        customerSection.migrationIconClicked.addClass('selected');
      }
      // if the servicability changes => we need to check again the serviceability if it is later desired to make moveoffnet
      if(sessionStorage.getItem('modeOffNetAddressLoadedForKip')) {
        sessionStorage.removeItem('modeOffNetAddressLoadedForKip');
        $('button.button-requires-serviceability-kip').addClass('disabled');
        $ ('.alert-moveoffnet').removeClass('hidden').show();
      }
      $('.products-icon-info.serviceability-loading').show();
      $('.products-icon-info.serviceability-done').hide();
    });
  });

  // Init customerSection object on document ready
  window.customerSection = new CustomerSection({
    baseUrl: MAIN_URL,
    spinner: MAIN_URL + 'skin/frontend/omnius/default/images/spinner.gif'
  });

})(jQuery);


