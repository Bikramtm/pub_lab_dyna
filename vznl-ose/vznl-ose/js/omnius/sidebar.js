'use strict';

(function ($) {
  window.Sidebar = function () {
    this.initialize.apply(this, arguments)
  };
  window.Sidebar.prototype = $.extend(VEngine.prototype, {
    openPackage: null,
    requiredServiceAbilityFlag: 'requires-serviceability',
    canOrderKipFlag: 'single-kip-at-address',
    canOrderKudFlag: 'single-kud-at-address',
    showReservedMemberConfirmation: true,
    initConfiguratorPackage: null,
    callBack: null,

    initConfiguratorConfirmation: function() {
      var self = this;

      self.showReservedMemberConfirmation = false;
      self.initConfigurator(self.initConfiguratorPackage, null, true);
      jQuery('#redplus-reserved-member-warning').modal('toggle');
      self.showReservedMemberConfirmation = true;
    },

    checkReservedMemberWarning: function(element) {
      var self = this;
      setPageLoadingState(true);
      $.ajax({
        url: 'configurator/cart/checkReservedMemberStatus',
        type: 'POST',
        async: false,
        success: function(response) {
          setPageLoadingState(false);
          if (response.hasOwnProperty('error') && response.error) {
            showModalError(response.message);
          } else {
            if (response.hasOwnProperty('showWarning') && response.showWarning && self.showReservedMemberConfirmation) {
              var modal = jQuery('#redplus-reserved-member-warning');
              modal.find('.confirmation-message').html(Translator.translate('The customer has future reserved activations for this Red+ group.'));
              self.initConfiguratorPackage = element;
              modal.find('#break-redplus-button').attr('onclick', 'sidebar.initConfiguratorConfirmation()');
              modal.modal();
            } else {
              // just trigger init again
              self.initConfigurator(self.initConfiguratorPackage, null, true);
            }
          }

        }
      });
    },
    initConfigurator: function (element, hasParent, skipWarningCheck) {
      var self = this;
      /** Close customer panel if we want to add a new package**/
      if ($('.enlargeSearch').hasClass('left-direction')) {
        LeftSidebar.toggleLeftSideBar(false, true);
      }
      /** If no serviceability check has been made, request address check and come back here */
      if ($(element).hasClass(this.requiredServiceAbilityFlag) && !address.services) {
        address.showCheckModal(this.initConfigurator, element, window.sidebar);
      } else if($(element).hasClass(this.requiredServiceAbilityFlag) && address.services && $(element).hasClass('disabled')) {
        return false;
      } else if ($(element).hasClass('disabled')) {
        return false;
      } else {
        self.initConfiguratorPackage = element;
        if (skipWarningCheck) {
          // just continue with init
        } else {
          if ($(element).data('requires-group-confirmation') && self.showReservedMemberConfirmation) {
            self.checkReservedMemberWarning(element);
            return false;
          }
        }

        /** Init configurator */
        var target = $(element).attr('data-target');
        var packageTypeId = $(element).attr('data-package-creation-type-id');
        var panelId = $('#buttons-trigger').attr('data-target');
        var migrationOrMoveOffnet = $(element).attr('data-migration-or-moveoffnet');
        var migrationOrMoveOffnetCustomerNo = $(element).attr('data-customer-no-migration-or-move-offnet');
        var relatedProductSku = $(element).attr('data-related-product-sku');
        var bundleId = $(element).attr('data-bundle-id');
        var filters = $(element).attr('data-filter-attributes');
        var saleType = $(element).attr('data-sale-type');
        $('#' + panelId).hide();

        if (controller != 'cart') {
          $('#' + target).show();

          // minimize all
          $('#' + target + ' .content').each(function () {
            $(element).removeClass('hidden');
          });
          if (this.openPackage) {
            $('#' + this.openPackage + ' .content').each(function () {
              $(element).addClass('hidden');
            });
          }

          this.openPackage = target;

          if ($(element).hasClass('show-clear-cart-warning')) {
            // prepare call back for confirmation of deleting all packages
            self.callBack = function () {
              $('#cancel-cart-by-package-modal').modal('hide');
              initConfigurator(target, null, saleType, null, null, true, migrationOrMoveOffnet, migrationOrMoveOffnetCustomerNo, null, hasParent, relatedProductSku, bundleId, filters, packageTypeId, 1);
            };
            // show confirmation dialog and ensure on modal closing that callback is reset
            $('#cancel-cart-by-package-modal').modal().on('hide.bs.modal', function() {
              self.callBack = null;
            });
            $('.cancel-cart-by-package-button').unbind('click').on('click', function() {
              self.callBack();
              self.callBack = null;
            });
          } else {
            initConfigurator(target, null, saleType, null, null, true, migrationOrMoveOffnet, migrationOrMoveOffnetCustomerNo, null, hasParent, relatedProductSku, bundleId, filters, packageTypeId);
          }


          $('#package-types').addClass('hide');

          // OMNVFDE-3735 the buttons to add new package for cable should be available at migration
          if (!showOnlyOnePackage) {
            $('#show-packages').removeClass('hide');
            $('#cart-spinner-wrapper').find('.cart_packages').removeClassPrefix('control-buttons-');
          }
          // if the agent adds a KIP package and the migration menu is present in cart, hide the menu
          if (target == CABLE_INTERNET_PHONE_PACKAGE) {
            $('.migration-info').addClass('hide');
          }
          $('.no-package-zone').addClass('hide');
          jQuery('#btn-delete-all').removeClass('disabled');
          jQuery('#btn-save-and-send').removeClass('disabled');
        } else {
          e.preventDefault();
          window.location.href = MAIN_URL + '?type=' + target;
        }

      }
    },
    disableServiceNeededPackages: function () {
      $('#package-types').find('.' + sidebar.requiredServiceAbilityFlag).removeClass('disabled').addClass('disabled');
    },
    updatePrices: function (element) {
      window.setPageOverrideLoading && window.setPageOverrideLoading(true);
      /** We need the new state, which will be updated after the ajax call **/
      var state = !ToggleBullet.state('.tax-toggle');
      var self = this;
      store.set('btwState', state);
      ToggleBullet.onSwitchChange('.tax-toggle', 'tax');

      if (store.get('btwState') == null || store.get('btwState') == true) {//show max
        self.showSummarizedPrice();
      } else {//show min
        self.showDetailedPrice();
      }
    },
    correctPricesVisibility: function () {
      window.setPageOverrideLoading && window.setPageOverrideLoading(true);
      /** We need the new state, which will be updated after the ajax call **/
      var state = ToggleBullet.state('.tax-toggle');
      var self = this;
      store.set('btwState', state);

      if (store.get('btwState') == null || store.get('btwState') == true) {//show max
        self.showSummarizedPrice();
      } else {//show min
        self.showDetailedPrice();
      }
    },
    showDetailedPrice: function() {
      // OMNVFDE-705: When including tax is selected, hide the price details block
      $('#cartTotalsDetails').addClass('hide');
      // Increase cart height
      $('#cart-spinner-wrapper').addClass('long-style');
      $('.gesamt-min').removeClass('hide');
    },
    showSummarizedPrice: function() {
      $('#cartTotalsDetails').removeClass('hide');
      $('.cart-totals-table').removeClass('hide');

      // Decrease cart height
      $('#cart-spinner-wrapper').removeClass('long-style');
      $('.gesamt-min').addClass('hide');
    },
    showHideVatPrices: function (element) {
      window.setPageOverrideLoading && window.setPageOverrideLoading(true);
      /** We need the new state, which will be updated after the ajax call **/
      var state = !ToggleBullet.state('.expanded-shoppingcart-totals');
      var self = this;
      store.set('btwState', state);
      ToggleBullet.onSwitchChange('.expanded-shoppingcart-totals', 'tax');
      if (store.get('btwState') === null || store.get('btwState')) {
        self.showEachPrice();
      } else {
        self.showPriceWithVAT();
      }
    },
    showPriceWithVAT: function() {
      $('#vat-price-details').addClass('hide');
    },
    showEachPrice: function() {
      $('#vat-price-details').removeClass('hide');
    },
    handlePriceDisplayState: function() {
      ToggleBullet.onSwitchChange('.tax-toggle', 'tax');
      //By default if no state is set, hide the details
      if (store.get('btwState') == null || store.get('btwState') == true) {//show max
        ToggleBullet.switchOn('.tax-toggle');

        jQuery('#cartTotalsDetails').removeClass('hide');
        jQuery('.cart-totals-table').removeClass('hide');

        // Decrease cart height
        jQuery('#cart-spinner-wrapper').removeClass('long-style');

        $('.gesamt-min').addClass('hide');

      } else {//show min
        ToggleBullet.switchOff('.tax-toggle');

        jQuery('#cartTotalsDetails').addClass('hide');
        // Increase cart height
        jQuery('#cart-spinner-wrapper').addClass('long-style');

        $('.gesamt-min').removeClass('hide');
      }
    },

    escapeHtml: function(str) {
      if ($.trim(str) != '') {
        var tmp = document.createElement('DIV');
        tmp.innerHTML = str;
        var result = tmp.innerText || tmp.textContent;

        $(tmp).remove();
        if (result != str) {
          return this.escapeHtml(result);
        } else {
          return result;
        }
      } else {
        return str;
      }
    }
  });

  window.sidebar = new Sidebar({
    baseUrl: MAIN_URL,
    spinner: MAIN_URL + 'skin/frontend/omnius/default/images/spinner.gif'
  });

  // By default, all packages that require serviceability will be disabled and further enabled by serviceability call
  $('#package-types').find('.requires-serviceability').removeClass('disabled').addClass('disabled');

  $(document).ready(function () {
    $(document).bind('serviceability-check-completed', function () {
      var packageType = $('#package-types');
      if (address.services) {
        packageType.find('.' + sidebar.requiredServiceAbilityFlag).each(function(index, element) {
          element = $(element);
          //Do this check because cable tv should be disabled if customer is soho OMNVFDE-2098
          if(customerDe.isBusiness && element.data('target') === CABLE_TV_PACKAGE){
            element.removeClass('disabled').addClass('disabled');
            return true;
          }
          if(element.hasClass(sidebar.canOrderKipFlag) && address.services.customerHasKipInstalled) {
            element.addClass('disabled');
            return;
          }
          if(element.hasClass(sidebar.canOrderKudFlag) && address.services.customerHasKudInstalled) {
            element.addClass('disabled');
            return;
          }
          var cableAvailability = Object.assign(address.services.cableAvailability, address.services.fnAvailability);
          var service = element.data('requires-green-service');
          if (typeof service === 'undefined') {
            element.removeClass('disabled');
            return true;
          }

          // when a list of services is provided (split by |), at least one must be available.
          if (service.match(/\|/)) {
            service.split('|').every(function (s) {
              if (cableAvailability.hasOwnProperty(s) && cableAvailability[s]) {
                element.removeClass('disabled');
                return false;
              }
              // if this statement has been reached, no service that enables this package exists
              element.addClass('disabled');
            });
          } else if (cableAvailability.hasOwnProperty(service) && cableAvailability[service]) {
            element.removeClass('disabled')
          } else if (!element.hasClass('disabled')) {
            element.addClass('disabled')
          }
        });
      } else {
        packageType.find('.' + sidebar.requiredServiceAbilityFlag).addClass('disabled');
      }

      // Disable if has open order. !!! Same logic in sidebar.phtml
      $('#package-types')
        .find('.btn-block.cursor[data-has-open-order="true"][data-target!="' + MOBILE_PACKAGE_TYPES.join('"][data-target!="') + '"]')
        .removeClass('disabled')
        .addClass('disabled');
    });


    window.sidebar.handlePriceDisplayState();

    /*$(".content").on("click", ".panel-heading", function(){
         var $panel = $(this).closest('.panel');
         var $item = $panel.find(".selected-item");
         var $block = $panel.find(".selection-block");
         //close the other ones

         jQuery(".panel").each(function(index){
         if(!$(this).is($panel)) {
         var $item = $(this).find(".selected-item");
         var $block = $(this).find(".selection-block");
         $item.show();
         $block.hide();
         }
         })
         $item.toggle();
         $block.toggle();

         });
         $(".content").on("click", "input, select", function(){
         return false;
         });
         $('.content').on('keyup', ".search", function(){
         var $value = $(this).val();
         var $panel = $(this).closest('.panel');
         if($panel.hasClass('panel-searchable')) {
         $panel.find(".item-row").each(function(index) {
         var $itemText = $(this).find('.item-title').text();
         if($itemText.indexOf($value) !=0) {
         $(this).hide();
         } else {
         $(this).show();
         }
         });
         }
         });*/

  })

})(jQuery);
