'use strict';

// define object and validate it through constructor
var AddressForm = function (config, container) {
  // Validate config data
  if (!config.zipCodeInputName || !config.streetInputName || !config.cityInputName || !config.houseNumberInputName) {
    var configObject = {
      streetInputName: 'Street input element name (mandatory)',
      zipCodeInputName: 'Zip Code input element name (mandatory)',
      cityInputName: 'City input element name (mandatory)',
      houseNumberInputName: 'House Number input element name (mandatory)',
      houseNumberAddition: 'House Number Addition input element name (mandatory, can be empty)'
    };
    console.warn('Got the following config: ' + JSON.stringify(config, null, 4));
    console.warn('Expected: ' + JSON.stringify(configObject, null, 4));
    console.error('Invalid binding on address form. Binding halted for: ' + container.attr('id'));
    this.isValid = false;
  } else {
    // proceed with mapping jQuery elements to this properties
    var errors = '';
    this.container = container;

    // map zipCode input
    if (container.find('input[name="' + config.zipCodeInputName + '"]').length) {
      this.zipCode = container.find('input[name="' + config.zipCodeInputName + '"]');
    } else {
      errors += 'cannot find input with name: ' + config.zipCodeInputName + ' ';
    }
    // map city input
    if (container.find('input[name="' + config.cityInputName + '"]').length) {
      this.city = container.find('input[name="' + config.cityInputName + '"]');
    } else {
      errors += 'cannot find input with name: ' + config.cityInputName + ' ';
    }
    // map street
    if (container.find('input[name="' + config.streetInputName + '"]').length) {
      this.street = container.find('input[name="' + config.streetInputName + '"]');
    } else {
      errors += 'cannot find input with name: ' + config.streetInputName + ' ';
    }
    // map house number
    if (container.find('input[name="' + config.houseNumberInputName + '"]').length) {
      this.houseNo = container.find('input[name="' + config.houseNumberInputName + '"]');
    } else {
      errors += 'cannot find input with name: ' + config.houseNumberInputName + ' ';
    }
    // map house number
    if (container.find('input[name="' + config.houseNumberAddition + '"]').length) {
      this.houseNoAddition = container.find('input[name="' + config.houseNumberAddition + '"]');
    } else {
      this.houseNoAddition = null;
    }

    this.customContainerStyle = config.customContainerStyle || {};

    if (errors.length) {
      console.warn('The following errors were found for address container: ' + container.attr('id'));
      console.warn(errors);
      console.error('Invalid binding on address form. Binding halted.');
      this.isValid = false;
    }
  }
};

// additional function that need to be set on AddressForm object
AddressForm.prototype = {
  isValid: true,
  /** @var googleSearchAddress container */
  container: null,
  zipCode: null,
  street: null,
  city: null,
  houseNo: null,
  houseNoAddition: null,
  timeout: null,
  customContainerStyle: {},
  // defining a list of inputs to which events will be bound
  bindingInputs: ['zipCode', 'street', 'city', 'houseNo'],
  orderInputs: ['zipCode', 'city', 'street', 'houseNo', 'houseNoAddition'],
  // keeping an array of results on current instance for knowing which one's data to fill inputs with
  results: [],
  previousInput: null,

  /**
     * Bind events to this form's inputs
     * @return {AddressForm}
     */
  bindEvents: function () {
    this.bindingInputs.forEach(function (input) {
      // take over this object's timeout
      this[input] && this[input].off('keyup').on('keydown', function (event) {
        var keyCode = event.keyCode;

        // if key is valid, then it is an addition to this input
        if (this.isKeyValidForSearch(keyCode)) {
          clearTimeout(this.timeout);

          // we can proceed even if this input has no value
          if (this.isHouseNumberAddition(this[input]) || (this.isHouseNumber(this[input]) && !this.street.val())) {
            return this;
          }

          if (this.isHouseNumber(this[input]) && this.street.val() && this.zipCode.val()) {
            return this;
          }

          if (this.isKeyValidForSearch(keyCode)) {
            this.timeout = setTimeout(function () {
              this.container.searchAddress(this[input]);
            }.bind(this), 200);
          }
        } else {
          // check for arrows
          if ([38, 40, 13].indexOf(keyCode) === -1 || !this.container.find('.google-search-address').length) {
            return this;
          }

          // if we reached this point, than no further events callbacks should be executed as the logic applies strictly on interaction with this input
          event.preventDefault();
          event.stopImmediatePropagation();

          // arrow selection mechanism
          var resultsContainer = this.container.find('.google-search-address');
          var selectedResult = -1;
          if (resultsContainer.find('.focused').length) {
            selectedResult = parseInt(resultsContainer.find('.focused').attr('data-address-id'));
          }

          if (keyCode === 38) {
            // up: start or start all over
            if (selectedResult === -1 || !selectedResult) {
              selectedResult = this.results.length-1;
            } else {
              selectedResult -= 1;
            }
            resultsContainer.find('.focused').removeClass('focused');
            resultsContainer.find('.entry[data-address-id="' + selectedResult + '"]').addClass('focused');
          } else if (keyCode === 40) {
            // down: start or start all over
            if (selectedResult === (this.results.length - 1)) {
              selectedResult = 0;
            } else {
              selectedResult += 1;
            }
            resultsContainer.find('.focused').removeClass('focused');
            resultsContainer.find('.entry[data-address-id="' + selectedResult + '"]').addClass('focused');
          } else {
            // enter pressed, trigger click
            if (selectedResult !== -1) {
              this.loadResult(selectedResult, this[input].attr('name'));
            }
          }
          // EOF arrow selection mechanism
        }
      }.bind(this));

      // clear shown results on blur
      this[input] && this[input].keyup(function () {
        this[input] && this[input].blur(function () {
          this.closeResults();
        }.bind(this));
      }.bind(this));

    }.bind(this));

    this.orderInputs.forEach(function (input) {
      this[input] && this[input].blur(function () {
        this.previousInput = this[input];
      }.bind(this));
    }.bind(this));


    return this;
  },

  isKeyValidForSearch: function (keycode)
  {
    var valid = (keycode > 47 && keycode < 58)   || // number keys
            keycode == 32 || // spacebar & return key(s) (if you want to allow carriage returns)
            (keycode > 64 && keycode < 91)   || // letter keys
            (keycode > 95 && keycode < 112)  || // numpad keys
            (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
            (keycode > 218 && keycode < 223);   // [\]' (in order)

    return valid;
  },

  /**
     * Set results on current
     * @param results
     * @param input
     * @param callBack
     * @returns {AddressForm}
     */
  showResults: function (results, input, callBack) {
    // override previous results
    this.results = results.length ? results : [];
    this.closeResults(true);
    if (this.results.length) {
      var resultsContainer = this.container.getNewElement('ul');
      resultsContainer.addClass('google-search-address').addClass('results-container').addClass('list-group');
      for (var property in this.customContainerStyle) {
        if (this.customContainerStyle.hasOwnProperty(property)) {
          resultsContainer.css(property, this.customContainerStyle[property])
        }
      }
      this.results.forEach(function(address, key) {
        resultsContainer.append('<li class=\'entry list-group-item\' data-focused-input=\'' + input.attr('name') + '\' data-address-id=\'' + key + '\'>' + this.buildStringAddress(address, input) + '</li>');
      }.bind(this));
      input.parent().append(resultsContainer);
    }
    callBack();
    return this;
  },

  /**
     * Close search results for a certain input
     * @returns {AddressForm}
     */
  closeResults: function (now) {
    if (now) {
      this.container.find('.results-container').remove();
    } else {
      setTimeout(function() {
        this.container.find('.results-container').remove();
      }.bind(this), 100);
    }

    return this;
  },

  loadResult: function (id, focusedInputName) {
    this.zipCode.val(this.results[id].zipCode);
    this.city.val(this.results[id].city);
    this.street.val(this.results[id].street);
    this.houseNo.val(this.results[id].houseNo);
    this.houseNoAddition.val(this.results[id].houseNoAddition);

    var reachedClickedInput = false;
    this.orderInputs.every(function(inputName) {
      // check if this inputName is the focused one
      if (this[inputName].attr('name') === focusedInputName) {
        reachedClickedInput = true;
      }
      // if focused one not reached in list, continue
      if (!reachedClickedInput) {
        return true;
      }
      if (!this[inputName].val()) {
        this[inputName].focus();
        return false;
      }
      return true;
    }.bind(this));

    return this;
  },

  /**
     * Build an address string out of address parts
     * @param address
     */
  buildStringAddress: function (address, input) {
    if (input === this.street) {
      return (address.street ? ' ' + address.street : '')
                + (address.houseNo ? ' ' + address.houseNo : '')
                + (address.houseNoAddition ? address.houseNoAddition : '')
                + (address.zipCode ? ' ' + address.zipCode : '')
                + (address.city ? ' ' + address.city : '')
      ;
    } else if (input === this.city) {
      return (address.zipCode ? address.zipCode : '')
                + (address.city ? ' ' + address.city : '')
      ;
    } else {
      return (address.zipCode ? address.zipCode : '')
                + (address.city ? ' ' + address.city : '')
                + (address.street ? ' ' + address.street : '')
                + (address.houseNo ? ' ' + address.houseNo : '')
                + (address.houseNoAddition ? address.houseNoAddition : '')
      ;
    }
  },

  /**
     * Determine whether or not a certain input is for postcode
     * @param input
     * @returns {boolean}
     */
  getInputType: function (input) {
    switch (true) {
    case this.zipCode === input:
      return 'zipCode';
    case this.city === input:
      return 'city';
    case this.houseNo === input:
      return 'houseNo';
    case this.houseNoAddition === input:
      return 'houseNoAddition';
    default:
      return 'street';
    }
  },
  isHouseNumber: function(input) {
    return this.houseNo === input;
  },
  isHouseNumberAddition: function(input) {
    return this.houseNoAddition === input;
  }
};

jQuery.fn.googleSearchAddress = function (config) {
  var $ = jQuery;
  var $self = $(this);
  this.searchAddress = function (input) {
    $.ajax({
      url: '/asq.php?t=' + Date.now(),
      cache: false,
      data: {
        'previous' : this.address.previousInput ? this.address.getInputType(this.address.previousInput) : null,
        'focused' : this.address.getInputType(input),
        'zipCode' : this.address.zipCode.val(),
        'city' : this.address.city.val(),
        'street' : this.address.street.val(),
        'houseNo' : this.address.houseNo.val(),
        'houseNoAddition' : this.address.houseNoAddition ? this.address.houseNoAddition.val() : '',
        'apiKey' : GOOGLE_AUTOCOMPLETE_API_KEY,
        'country' : GOOGLE_AUTOCOMPLETE_COUNTRY
      },
      success: function (response) {
        var self = this;
        // response ok, proceed with filling data
        this.address.showResults(response.results, input, function() {
          $self.find('.google-search-address').find('.entry').off('mousedown').mousedown(function() {
            self.address.loadResult($(this).attr('data-address-id'), $(this).attr('data-focused-input')).closeResults();
          });
        }.bind(this));
      }.bind(this)
    });
  }.bind(this);

  this.getNewElement = function(element)
  {
    return $('<' + element + '>' + '</' + element + '>');
  };

  // validate received settings through address form object's constructor
  this.address = new AddressForm(config, this, 'DE');
  if (this.address.isValid) {
    // bind to input focus
    this.address.bindEvents();


    return this;
  }
};
