/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

jQuery(document).ready(function($) {
  var minWidth = $('#add_root_category_button').width() + $('#add_root_category_button').position().left + 30;
  var totalWidth = parseInt($(window).width()*0.6);
  $('#anchor-content .columns').css('background', 'none');
  $('#anchor-content .main-col').css('border-left', '3px solid #eee');
  $('#anchor-content .side-col').resizable({
    minWidth: minWidth,
    maxWidth: totalWidth,
    resize: function(event, ui) {
      var leftWidth = ui.size.width;
      $('#anchor-content .side-col').css('width', leftWidth + 'px');
      $('#anchor-content .side-col').css('margin-right', '-' + leftWidth + 'px');
      $('#anchor-content .main-col').css('margin-left', leftWidth + 'px');
    }
  });
});
