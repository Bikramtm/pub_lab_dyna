/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

(function (Handlebars) {
    var a = function (op, v1, v2) {
        switch (op) {
            case "==":
                return (v1 == v2);
            case "!=":
                return (v1 != v2);
            case "===":
                return (v1 === v2);
            case "!==":
                return (v1 !== v2);
            case "&&":
                return (v1 && v2);
            case "||":
                return (v1 || v2);
            case "<":
                return (v1 < v2);
            case "<=":
                return (v1 <= v2);
            case ">":
                return (v1 > v2);
            case ">=":
                return (v1 >= v2);
            default:
                return eval("" + v1 + op + v2);
        }

    };

    Handlebars.registerHelper('money', function (string, options) {
        if (string === undefined || (string == false && string !== 0)) return '';
        var prefix = '€',
            numeric = (+String(string).replace(/[-,]/g, '')).toFixed(2) || '0,00',
            parts = numeric.split('.'),
            isNegative = /^-/.test(String(string));

        if (isNegative) {
            prefix = '-€';
        }

        var result = [
            prefix,

            parts[0]
                .split('')
                .reverse()
                .reduce(function (acc, num, i) {
                    return num + (i && !(i % 3) ? '.' : '') + acc;
                }, ','),

            parts[1]
        ].join('');

        //	Strip out cents if not wanted.
        if (options.hash.cents === false) {
            result = result.replace(/,{3}$/, '');
        }

        return result;
    });

    Handlebars.registerHelper('dynamicPartial', function (partial, context, opts) {
        partial = partial.replace(/\//g, '_');
        var func = Handlebars.partials[partial];
        if (!func) {
            return '';
        }

        return new Handlebars.SafeString(func(context));
    });

    Handlebars.registerHelper('columnValue', function (products, raw) {
        raw = undefined != raw;
        var total = 0;
        for (var i = 0; i < products.length; i++) {
            total += parseFloat(products[i]['price']);
        }

        return raw ? Handlebars.helpers['money'](total, {hash: {}}) : total;
    });

    Handlebars.registerHelper('columnValueWithVAT', function (products, vat) {
        var total = Handlebars.helpers['columnValue'](products);
        var result = (vat / 100 * total) + total;

        return Handlebars.helpers['money'](result, {hash: {}});
    });

    Handlebars.registerHelper('math', function (lvalue, operator, rvalue, options) {
        lvalue = parseFloat(lvalue);
        rvalue = parseFloat(rvalue);

        return {
            "+": lvalue + rvalue,
            "-": lvalue - rvalue,
            "*": lvalue * rvalue,
            "/": lvalue / rvalue,
            "%": lvalue % rvalue
        }[operator];
    });

    Handlebars.registerHelper("ifComplex", function (v1, operator1, v2, operator2, v3, operator3, v4, options) {
        var eval1 = a(operator1, v1, v2);
        var eval2 = a(operator3, v3, v4);

        switch (operator2) {
            case "||":
                return (eval1 || eval2) ? options.fn(this) : options.inverse(this);
            case "&&":
                return (eval1 && eval2) ? options.fn(this) : options.inverse(this);
            default:
                return (eval("" + v1 + operator1 + v2 + operator2 + v3 + operator3 + v4) ? options.fn(this) : options.inverse(this));
        }
    });

    Handlebars.registerHelper("ifCond", function (v1, operator, v2, options) {
        return (a(operator, v1, v2) ? options.fn(this) : options.inverse(this));
    });

    Handlebars.registerHelper("ifYesNo", function (v1, options) {
        return ((v1 && (v1 != "0")) ? options.fn(this) : options.inverse(this));
    });

    Handlebars.registerHelper('assertSection', function (filterSection, options) {
        var filterOptions = filterSection.options;
        if (!filterOptions || Object.keys(filterOptions).length == 1) {
            // skip sections with only one filter option
            return '';
        }
        // pass on the context
        return options.fn(filterSection);
    });

})(Handlebars);
