;(function ( $, window, document, undefined ) {
    
    var defaults = {
        orientation: 'left',
        mode: 'push',
        static: false,
        offset: 0
    };

    // The actual plugin constructor
    function Slidepanel( $element, options ) {
        this.$element = $element;
        this.content_id = jQuery($element).attr('href');
        this.options = $.extend( {}, defaults, options) ;
        this._defaults = defaults;
        this.init();
    }

    Slidepanel.prototype.init = function () {
        
        var base = this;

        if($(this.content_id).length == 0){
            var panel_html = '<div id="slidepanel" class="cb_slide_panel"><div class="wrapper"><a href="#" class="close">Close</a><div class="inner"><div class="wrapper"></div></div></div></div>';
            $(panel_html).hide().appendTo($('body'));
            this.$panel = $('#slidepanel');
        }
        else {
            this.$panel = $(this.content_id);
        }

        this.$body = $('body');
        this.$body_position = this.$body.css('position');

        //hide the panel and set orientation class for display
        this.$panel.hide().addClass('panel_' + this.options.orientation);
        
        //set current trigger link to false for the current panel
        this.$panel.data('slidepanel-current', false);
        this.$panel.data('slidepanel-loaded', false);

        //reset any defined a positions
        this.$panel.css('left', '').css('right', '').css('top', '').css('bottom', '');

        //set a default top value for left and right orientations
        //and set the starting position based on element width
        if(this.options.orientation == 'left' || this.options.orientation == 'right') {
            var options = {};
            options['top'] = 0;
            options[this.options.orientation] = -(this.$panel.width() + this.options.offset);
            this.$panel.css(options);
        }

        //set a default left value for top and bottom orientations
        //and set the starting position based on element height
        if(this.options.orientation == 'top' || this.options.orientation == 'bottom') {
            var options = {};
            options['left'] = 0;
            options[this.options.orientation] = -(this.$panel.height() + this.options.offset);
            this.$panel.css(options);
        }

        //bind click event to trigger ajax load of html content
        //and panel display to any elements that have the attribute rel="panel"
//        $(this.$element).on('click', function(e) {
//            e.preventDefault();
//
//            //if the request mode is static
//            if(base.options.static) {
//                //show the panel
//                base.expand();
//            }
//            // if the request mode is ajax
//            else {
//                //load the external html
//                base.load();
//            };
//        });

        //listen for a click on the close buttons for this panel
        $('.close', this.$panel).click(function(e) {
            e.preventDefault();
            var after_collapse_callback = false;
            if(base.options != undefined && base.options.after_collapse_callback != undefined && typeof base.options.after_collapse_callback === "function") {
                // Call it, since we have confirmed it is callable
                after_collapse_callback = base.options.after_collapse_callback;
            }

            base.collapse(base.options,after_collapse_callback);
        });
        
    };

    Slidepanel.prototype.click = function() {
        var base = this;

        //if the request mode is static
        if(base.options.static) {
            //show the panel
            base.expand(this.$element, this.options.after_expand);
        }
        // if the request mode is ajax
        else {
            //load the external html
            base.load();
        };
    }

    Slidepanel.prototype.load = function() {
            var base = this;
            //if the current trigger element is the element that just triggered a load
            if(this.$panel.data('slidepanel-current') == this.$element) {
                //collapse the current panel
                this.collapse();
                return;
            } else {
                //show the slide panel
                this.expand(this.$element, this.options.after_expand);
                //get the target url
                var href = $(this.$element).attr('href');

                //prevent an ajax request if the current URL is the the target URL
                if(this.$panel.data('slidepanel-loaded') !== href){
                    //load the content from the target url, and update the panel html
                    $('.inner .wrapper', this.$panel).html('').load(href, function() {
                        //remove the loading indicator
                        base.$panel.removeClass('loading');
                        //set the current loaded URL to the target URL
                        base.$panel.data('slidepanel-loaded', href);
                    });
                //  the current URL is already loaded
                } else {
                    //remove the loading indicator
                    this.$panel.removeClass('loading');
                }
            }
            //set the current source element to this element that triggered the load
            this.$panel.data('slidepanel-current', this.$element);
    };


    Slidepanel.prototype.expand = function(options, callback) {
        var base = this;
                //set the css properties to animatate

        var panel_options = {};
        var body_options = {};
        panel_options.visible = 'show';
        panel_options[this.options.orientation] = 0 + this.options.offset;
        body_options[this.options.orientation] = (this.options.orientation == 'top' || this.options.orientation == 'bottom') ? this.$panel.height() + this.options.offset : this.$panel.width() + this.options.offset;
        
        //if the animation mode is set to push, we move the body in relation to the panel
        //else the panel is overlayed on top of the body
        if(this.options.mode == 'push'){
            //animate the body position in relation to the panel dimensions
            this.$body.css('position', 'absolute').animate(body_options, 250);
        }

        if(typeof this.options.before_expand === "function") {
            // Call it, since we have confirmed it is callable
            this.options.before_expand(options);
        }
        //animate the panel into view
        this.$panel.addClass('loading').animate(panel_options, 250, function() {
            //show the panel's close button
            $('.close', base.$panel).fadeIn(250);

            if (typeof callback === "function") {
                // Call it, since we have confirmed it is callable
                callback(options);
            }
        });
    };

    Slidepanel.prototype.collapse = function(options, callback) {
        //hide the close button for this panel
        $('.close', this.$panel).hide();

        //set the css properties to animatate
        var panel_options = {};
        var body_options = {};
        panel_options.visible = 'hide';
        panel_options[this.options.orientation] = (this.options.orientation == 'top' || this.options.orientation == 'bottom') ? -(this.$panel.height() + this.options.offset) : -(this.$panel.width() + this.options.offset);
        body_options[this.options.orientation] = 0;
        
        //if the animation mode is push, move the document body back to it's original position
        if(this.options.mode == 'push'){
            this.$body.css('position', this.$body_position).animate(body_options, 250);
        }
        //animate the panel out of view
        this.$panel.animate(panel_options, 250, function(){
            if (typeof callback === "function") {
                // Call it, since we have confirmed it is callable
                callback(options);
            }

            $(this).data('slidepanel-current', false);
        });

        //because IE9&10 does not allow "new Event"
        //this.$element.dispatchEvent(new Event('collapsed'));
        $(this.$element).trigger('collapsed');
    };

    $.fn['slidepanel'] = function ( options ) {
        var openPanel = false;

        return this.each(function () {
            if (!$.data(this, 'plugin_slidepanel')) {

                $.data(this, 'plugin_slidepanel', new Slidepanel( this, options ));

                if(options != undefined && options.container != undefined && $(options.container) != undefined) {
                    $(options.container).append($(this).data('plugin_slidepanel').$panel)
                }

                $(this).on('click', function(e) {
                    e.preventDefault();

                    if(openPanel && openPanel != $(this).data('plugin_slidepanel')) {
                        openPanel.collapse($(this).data('plugin_slidepanel'), function(trigger) {
                            if(trigger != undefined) {
                                trigger.click();
                                openPanel = trigger;
                            }
                        });
                    } else {
                        var after_collapse_callback = false;
                        if(options != undefined && options.after_collapse_callback != undefined && typeof options.after_collapse_callback === "function") {
                            // Call it, since we have confirmed it is callable
                            after_collapse_callback = options.after_collapse_callback;
                        }
                        if(openPanel && (openPanel == $(this).data('plugin_slidepanel')) && after_collapse_callback) {
                            openPanel.collapse($(this).data('plugin_slidepanel'), after_collapse_callback);
                        } else {
                            $(this).data('plugin_slidepanel').click();
                            openPanel = $(this).data('plugin_slidepanel');
                        }

                    }
                });

                $(this).on('collapsed', function(e) {
                    e.preventDefault();

                    if(options != undefined && options.after_collapse != undefined && typeof options.after_collapse === "function") {
                        // Call it, since we have confirmed it is callable
                        options.after_collapse($(this));
                    }

                    openPanel = false;
                });
            }
        });
    }

})(jQuery, window);