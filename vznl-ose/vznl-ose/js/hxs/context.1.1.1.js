function _getIeVer() {
    var e = -1;
    if (navigator.appName == "Microsoft Internet Explorer") {
        var t = navigator.userAgent;
        var n = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");
        if (n.exec(t) != null) e = parseFloat(RegExp.$1)
    }
    return e
}

function _browserSupportsLocalStorage() {
    try {
        return "localStorage" in window && window["localStorage"] !== null
    } catch (e) {
        return false
    }
}

function _getRequestData(e) {
    var t = {
        orig_request: window.location.href.substr(window.location.protocol.length + 2 + window.location.host.length),
        full_action_name: _contextAction,
        message_sessions: _contextMessageSessions,
        blocks: "",
        counter: 0
    };
    var n = _getBlocks(e);
    t.blocks = Object.toJSON(n);
    t.counter = n.counter;
    if (typeof _contextProductId !== "undefined" && _contextProductId) {
        t.currentProductId = _contextProductId
    }
    if (typeof _contextCmsId !== "undefined" && _contextCmsId) {
        t.currentCmsId = _contextCmsId
    }
    if (typeof _contextCategoryId !== "undefined" && _contextCategoryId) {
        t.currentCategoryId = _contextCategoryId
    }
    if (document.referrer !== "undefined" && document.referrer) {
        t.refferer = document.referrer
    }
    return t
}

function _getBlocks(e) {
    var t = {};
    var n = 0;
    $$("." + e).each(function (e) {
        var r = e.readAttribute("id");
        if (!r) {
            r = "c_" + n;
            e.setAttribute("id", r)
        }
        var i = e.readAttribute("rel");
        if (i) {
            t[r] = i;
            if (!_contextInlineLocalCache && _browserSupportsLocalStorage()) {
                var s = _getCachedBlock(r);
                if (s == null) s = _getCachedBlockFromSession(r);
                if (s != null) {
                    $(r).update(s.Content)
                }
            }
            n++
        }
    });
    t.counter = n;
    return t
}

function _doContextBlocks() {
    var e = _getRequestData("ctdblock");
    if(typeof window.hxs.context == "undefined") { window.hxs.context = new Array(); }
    if (typeof e.currentProductId !== "undefined" || e.counter > 0) {
        var t = _getBlocks("ctpriceblock");
        if(!!window.hxs.context[e]) { return; }
        window.hxs.context[e] = true;
        if (t.counter > 0) e.prices = Object.toJSON(t);
        new Ajax.Request(_contextRequestUrl, {
            method: "get",
            parameters: e,
            evalJS: "force",
            onSuccess: function (e) {
                var t = e.responseText.evalJSON();
                for (var n in t.blocks) {
                    $(n).update(t.blocks[n]);
                    if (_browserSupportsLocalStorage()) _cacheBlock(n, t.blocks[n])
                }
                for (var n in t.prices) {
                    $(n).update(t.prices[n]);
                    if (_browserSupportsLocalStorage()) _cacheBlockForSession(n, t.prices[n])
                }
            },
            onComplete: function(e) {
            	window.hxs.context[e] = false;
            }
        })
    }
}

function _doLazyDataBlocks(e) {
    var t = _getRequestData("ctddatablock");
    if(typeof window.hxs.lazy == "undefined") { window.hxs.lazy = new Array(); }
    if ( ( typeof t.currentProductId !== "undefined" || typeof t.currentCategoryId !== "undefined" || typeof t.currentCmsId !== "undefined" ) && t.counter > 0) {
    	if(!!window.hxs.lazy[e]) { return; }
    	window.hxs.lazy[e] = true;
        new Ajax.Request(e, {
            method: "get",
            parameters: t,
            evalJS: "force",
            onSuccess: function (e) {
                var t = e.responseText.evalJSON();
                for (var n in t.blocks) {
                    $(n).update(t.blocks[n])
                }
            },
            onComplete: function(e) {
            	window.hxs.lazy[e] = false;
            }
        })
    }
}

function _loadSessionContext() {
	if(typeof window.hxs == "undefined") { window.hxs = new Object(); }
    _doContextBlocks();
    if (typeof _contextProductId !== "undefined" && _contextProductId) {
        _doLazyDataBlocks(_contextProductLazyRequestUrl)
    }
    if (typeof _contextCategoryId !== "undefined" && _contextCategoryId) {
        _doLazyDataBlocks(_contextCategoryLazyRequestUrl)
    }
    if (typeof _contextCmsId !== "undefined" && _contextCmsId) {
        _doLazyDataBlocks(_contextCmsLazyRequestUrl)
    }
}

function _cacheBlock(e, t) {
    var n = {
        Id: e,
        Content: t,
        Timestamp: (new Date).getTime()
    };
    localStorage.setItem("rclc_" + e, JSON.stringify(n))
}

function _cacheBlockForSession(e, t) {
    var n = {
        Id: e,
        Content: t,
        Timestamp: (new Date).getTime()
    };
    sessionStorage.setItem("rclc_" + e, JSON.stringify(n))
}

function _getCachedBlockFromSession(e) {
    var t = sessionStorage.getItem("rclc_" + e);
    if (t == null) return null;
    return JSON.parse(t)
}

function _checkCacheBlockValid(e) {
    var t = localStorage.getItem("rclc_" + e);
    if (t == null) return false;
    createdTimestamp = JSON.parse(t).Timestamp;
    return (new Date).getTime() - createdTimestamp <= _sessionTimeout
}

function _getCachedBlock(e) {
    if (_checkCacheBlockValid(e)) {
        var t = localStorage.getItem("rclc_" + e);
        if (t == null) return null;
        return JSON.parse(t)
    } else {
        return null
    }
}
var _HashSearch = new function () {
        var e;
        this.set = function (t, n) {
            e[t] = n;
            this.push()
        };
        this.remove = function (t, n) {
            delete e[t];
            this.push()
        };
        this.get = function (t, n) {
            return e[t]
        };
        this.keyExists = function (t) {
            return e.hasOwnProperty(t)
        };
        this.push = function () {
            var t = [],
                n, r;
            for (n in e)
                if (e.hasOwnProperty(n)) {
                    n = escape(n), r = escape(e[n]);
                    t.push(n + (r !== "undefined" ? "=" + r : ""))
                }
            window.location.hash = t.join("&")
        };
        (this.load = function () {
            e = {};
            var t = window.location.hash,
                n, r;
            t = t.substring(1, t.length);
            n = t.split("&");
            for (var i = 0; i < n.length; i++) {
                r = n[i].split("=");
                e[unescape(r[0])] = typeof r[1] != "undefined" ? unescape(r[1]) : r[1]
            }
        })()
    };
var _sessionTimeout = 86400 * 1e3;
var _browserVer = _getIeVer();
if (_browserVer > -1 && _browserVer < 9) {
    Event.observe(window, "load", function () {
        _loadSessionContext()
    })
} else {
    document.observe("dom:loaded", function () {
        _loadSessionContext()
    })
}
