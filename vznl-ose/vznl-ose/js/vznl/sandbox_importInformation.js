// /*
//  * Copyright (c) 2016. Dynacommerce B.V.
//  */
isKill = false;
jQuery(document).ready(function () {
  setInterval(function () {
    jQuery.ajax({
      type: 'POST',
      url: checkImportExecution,
      data: {form_key: form_key_checkImportExecution}
    })
        .done(function (data) {
          var importInformation = jQuery(data).html();
          var insertHere = jQuery('[id="page:main-container"]').find('.main-col-inner').next().find('.entry-edit');
          var status = getStatus();
          if (status !== 'running' || isKill) {
            jQuery('.kill').addClass('disabled');
          } else {
            jQuery('.kill').removeClass('disabled');
          }
          insertHere.html(importInformation);
          jQuery('form#edit-form div').html('<input type="hidden" name="form_key" value="' + FORM_KEY + '"/>');
        });
  }, 5000);
});

function displayLoadingMask() {
  var loaderArea = $$('#html-body .wrapper')[0]; // Blocks all page
  Position.clone($(loaderArea), $('loading-mask'), {offsetLeft: -2});
  toggleSelectsUnderBlock($('loading-mask'), false);
  Element.show('loading-mask');
}

function getStatus() {
  var status;
  jQuery.ajax({
    async: false,
    cache: false,
    type: 'POST',
    url: getStatusUrl,
    data: {form_key: form_key_checkImportExecution}
  }).done(function (response) {
    status = response;
  });
  return status;
}

function killImportExecution() {
  isKill = true;
  jQuery('.kill').addClass('disabled');
  displayLoadingMask();

  window.setTimeout(function () {
    jQuery.ajax({
      async: false,
      cache: false,
      type: 'POST',
      url: killImportExecutionUrl,
      data: {form_key: form_key_checkImportExecution}
    }).done(function (response) {
      setInterval(function () {
        var status = getStatus();
        if (status !== 'running') {
          window.location.href = response;
        }
      }, 3000);
    });
  }, 5000);
}