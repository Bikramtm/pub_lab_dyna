(function ($) {
  window.Polling.prototype.saveOverviewPollingSuccess = function (response) {
    var self = this;
    var isTelesalesStore = $('#current_store_is_telesales').val() != 0;
    $.post(MAIN_URL + 'checkout/index/saveOverviewAfter', {'polling_id': self.modalContainer.find('#polling-id').val()}, function (response) {
      if (response.error == true && !isTelesalesStore) {
        self.pollingFailed(response.message);
      } else {
        $('#saveCustomer, #saveIncomeTest, #saveDeliveryAddress, #savePaymentMethod, #saveOverview').parent().addClass('hidden');
        $('#saveNumberPorting').parent().parent().addClass('hidden');
        $('.breadcrumb-bar').find('[data-section="order-overview"]').addClass('completed');
        var isTelesalesStore = $('#current_store_is_telesales').val() != 0;
        var hasOnlyOrderConfirmationStepLeft = $('.breadcrumb-bar').find('.vertex:not(.completed,.skip)').length === 2;
        if (isTelesalesStore || (!isTelesalesStore && hasOnlyOrderConfirmationStepLeft) ) {
          $('.breadcrumb-bar span, .breadcrumb-bar .bar, .breadcrumb-bar .bar-active').hide();
          disableAll();
          $('.breadcrumb-bar').find('[data-section="order-confirmation"]').addClass('active');
          $('#showOrderConfirmation').parent().css('display','block');
        }
        if (undefined != window.creditCheckData) {
          $('.breadcrumb-bar').find('[data-section="credit_check"]').addClass('active');
          $('#saveCreditCheck').parent().css('display','block');
          doCreditCheck(window.creditCheckData.addresses, window.creditCheckData.order_no, window.creditCheckData.creditStub);
        } else {
          $('.breadcrumb-bar').find('[data-section="number_device_section"]').addClass('active');
          $('#saveNumberSelection').parent().parent().css('display','block');
          $('[data-ref="number_device_section"]').removeClass('hidden').show();
          window.checkout.initSubmitButton('number_device_section', false);
          // Deactivate previous steps and go to next available step
          //$('.bs-docs-section').addClass('hidden');
          //$('#cart-left li').addClass('disabled');
          //$('#cart-left li a.pre-order').attr('href', '');

          scrollToSection($('#' + self.section));
        }

        self.enablePollingButton();
      }
    });

    self.successCallback();
  };

  window.Polling.prototype.saveSuperOrderPollingSuccess = function (response) {
    var self = this;
    $('#saveCustomer, #saveIncomeTest, #saveDeliveryAddress, #savePaymentMethod, #saveOverview').parent().addClass('hidden');
    $('#saveNumberPorting').parent().parent().addClass('hidden');
    var orderStatus = self.modalContainer.find('#order-status').val();
    var orderNo = self.modalContainer.find('#polling-superorder-no').val() === undefined ? window.Polling.polling_superorder_no : self.modalContainer.find('#polling-superorder-no').val();
    if ($('#is_order_edit_mode').val() != 1 && orderNo != "") {
      loadOrderDetails({number: orderNo});
    }
    $('.breadcrumb-bar').find('[data-section="order-overview"]').addClass('completed');
    var isTelesalesStore = $('#current_store_is_telesales').val() != 0;
    var hasOnlyOrderConfirmationStepLeft = $('.breadcrumb-bar').find('.vertex:not(.completed,.skip)').length === 1;
    if (orderStatus == 'cancel') {
      $(".change_cancel").hide();
      showValidateNotificationBar('<strong>' + Translator.translate('Order updated!') + '</strong> ' + Translator.translate('Package(s) has been successfully cancelled'), 'success');
    }
    if ($('#is_order_edit_mode').val() == 1){
      $('.col-left').removeClass('mediumView').addClass('smallView');
      $('#normal-order-message').hide();
      $('#order-confirmation-old-order-message').show();
    }

    if(orderNo && orderStatus == 'cancel-before-return') {
      window.location.href=MAIN_URL + '?orderId='+ orderNo;
    }

    if (isTelesalesStore) {
        $(".summary").hide();
        $(".service-address").hide();
        $('.breadcrumb-bar').find('[data-section="order-confirmation"]').addClass('active');
        $('#showOrderConfirmation').parent().css('display','block');
        showValidateNotificationBar('<strong>' + Translator.translate('Order updated!') + '</strong> ' + Translator.translate('Your order is successfully submitted'), 'success');
    } else {
      if (typeof response.device_selection !== "undefined") {
        updateNumberSelection(response.device_selection);
        updateSelectDevice(response.device_selection);
      }

      if (typeof response.contract_page !== "undefined") {
        var contract_content = $('#contract_page').first();
        if ($('#contract_page') && contract_content) {
          contract_content.html(response.contract_page);
        }
        var contractContent = $(response.contract_page).find('#contract-content');
        var contract_content = $('#contract_page').find('#contract-content');
        if (contract_content && contractContent) {
          contract_content.replaceWith($(contractContent).find('#contract-content'));
        }
        var skipContract = $(response.contract_page).find('#contract_page').hasClass('skip');
        if (skipContract) {
          $('#contract_page').addClass('skip');
        }
      }
    }
    
    if (undefined != window.creditCheckData) {
      $('.breadcrumb-bar').find('[data-section="credit_check"]').addClass('active');
      $('#saveCreditCheck').parent().css('display','block');
      if (typeof(doCreditCheck) == 'function') {
        doCreditCheck(window.creditCheckData.addresses, window.creditCheckData.order_no, window.creditCheckData.creditStub);
      }
    }

    self.successCallback();
    self.enablePollingButton();

    disableLeavePageDialog();
  };

  window.Polling.prototype.saveContractPollingSuccess = function (response) {
    var self = this;

    var nextStep = checkout.stepsOrder[checkout.stepsOrder.indexOf(self.section) + 1];

    processPreviousSteps(nextStep);

    $.each(checkout.stepsOrder, function (id, el) {
      if (el != nextStep) {
        $('#cart-left').find('[name=' + el + ']').parent().addClass('disabled');
        $('.bs-docs-section').find('[action=' + el + ']').addClass('hidden');
        if (el == self.section) {
          $('#cart-left').find('[name=' + el + ']').parent().addClass('hidden');
        }
      } else {
        return false;
      }
    });
    if (undefined != nextStep) {
      $('#credit-check-menu').parent().find('[name=' + nextStep + ']').parent().addClass('active').removeClass('hidden disabled');
      $('#'.nextStep).removeClass('hidden');
    }

    var isDeliveryFlow = parseInt($('#is_delivery_flow').val()) === 1;
    var isOrderEditMode = parseInt($('#is_order_edit_mode').val()) === 1;
    if (isDeliveryFlow || isOrderEditMode) {
      showValidateNotificationBar('<strong>' + Translator.translate(isDeliveryFlow ? 'Order placed!' : 'Order updated!') + '</strong> ' + Translator.translate('Your order is successfully submitted'), 'success');
      $('#normal-order-message').hide();
      $('#order-confirmation-old-order-message').show();
    }
    $('#saveCreditCheck, #saveNumberSelection').parent().addClass('hidden');
    $('#saveContract').parent().parent().addClass('hidden');
    $('.breadcrumb-bar span, .breadcrumb-bar .bar, .breadcrumb-bar .bar-active').hide();
    $('.breadcrumb-bar').find('[data-section="order-confirmation"]').addClass('active');
    if($('[name="isFixed"]').val() == 0) {
      $('#order-details-btn, #order-details-btn-new').addClass('hidden');
    }
    $('#showOrderConfirmation').parent().css('display','block');

    self.successCallback();

    self.enablePollingButton();
    scrollToSection($('#' + self.section));
  };

  window.Polling.prototype.initialize = function (section, pollingCount) {
    if (pollingCount === undefined) {
      pollingCount = pollingRetryLimit;
    }

    this.pollingInProgress = false;
    this.modalId = Polling.modalId;
    this.section = section;
    this.performPollingCount = pollingCount;
    this.intervalId = null;
    this.modalContainer = $(this.modalId).data('section', section);
  };

  window.Polling.prototype.pollingFailed = function (message) {
    var self = this;
    checkout.showVodError(true, message, 1, 1);
    clearInterval(self.intervalID);
    self.intervalID = false;
  };

  window.Polling.prototype.doPolling = function (id) {
    var self = this;

    if (self.jobProcessed !== true) {
      self.modalContainer.find('#progress-bar .progress-indicator-information .progress-indicator-information-title').html(Translator.translate('Waiting for reply'));
      var progress = '40%';
      self.modalContainer.find('#progress-bar .progress-indicator-bar .progress-indicator-bar-progress').width(progress);
      self.modalContainer.find('#progress-bar .progress-indicator-information .progress-indicator-information-percentage').text(progress);
    }
    --self.performPollingCount;
    if (self.intervalID) {
      window.canShowSpinner = false;

      var agent_id = $('#agent_id');
      var dealer_id = $('#dealer_id');
      var is_order_edit_mode = $('#is_order_edit_mode');
      var current_website_code = $('#current_website_code');
      var axi_store_id = $('#axi_store_id');
      var dealer_group_ids = $('#dealer_code');

      if (self.jobProcessed !== true) {
        $.post(MAIN_URL + 'job.php', {
          'id': undefined === id ? 'undefined' : id,
          'is_order_edit_mode': is_order_edit_mode.length ? is_order_edit_mode.val() : null,
          'agent_id': agent_id.length ? agent_id.val() : null,
          'dealer_id': dealer_id.length ? dealer_id.val() : null,
          'website_code': current_website_code.length ? current_website_code.val() : null,
          'axi_store_id': axi_store_id.length ? axi_store_id.val() : null,
          'dealer_group_ids': dealer_group_ids.length ? dealer_group_ids.val() : null
        }, function (response) {
          if (response.error == true && jQuery('#current_website_code').val() != 'telesales') {
            self.pollingFailed(response.message);
          } else if ((response.error == false || jQuery('#current_website_code').val() == 'telesales') && (undefined !== response.state) && (response.state != -1) && (response.state != -100)) {
            self.jobProcessed = true;
            self.modalContainer.find('#progress-bar .progress-indicator-information .progress-indicator-information-title').html(Translator.translate('Processing answer'));
            var progress = '60%';
            self.modalContainer.find('#progress-bar .progress-indicator-bar .progress-indicator-bar-progress').width(progress);
            self.modalContainer.find('#progress-bar .progress-indicator-information .progress-indicator-information-percentage').text(progress);
          } else {
            console.log('error occurred / no response received');
          }
        });
      } else {
        if(!self.pollingInProgress){
          self.startJobAfterPolling(id, self.jobResponse);
        }
      }

      window.canShowSpinner = true;
    }

    if (self.performPollingCount == 0) {
      self.pollingFailed(Translator.translate('Retry limit exceeded'));
    }
  };

  window.Polling.prototype.successCallback = function () {
    var self = this;

    self.modalContainer.find('#progress-bar .progress-indicator-information .progress-indicator-information-title').html('');
    var progress = '100%';
    self.modalContainer.find('#progress-bar .progress-indicator-bar .progress-indicator-bar-progress').width(progress);
    self.modalContainer.find('#progress-bar .progress-indicator-information .progress-indicator-information-percentage').text(progress);
    clearInterval(self.intervalID);
    self.intervalID = false;
  };

  window.Polling.prototype.enablePollingButton = function () {
    var self = this;

    self.modalContainer.modal('hide');
    var progress = '20%';
    self.modalContainer.find('#progress-bar .progress-indicator-bar .progress-indicator-bar-progress').width(progress);
    self.modalContainer.find('#progress-bar .progress-indicator-information .progress-indicator-information-percentage').text(progress);
  };

  window.Polling.prototype.updatePolling = function (pollingId) {
    var self = this;

    self.updateModal(pollingId);
    self.doPolling(pollingId);
    self.intervalID = setInterval(function () {
    self.doPolling(pollingId);
    }, pollingRetryGap);
  };

  window.Polling.prototype.updateModal = function (pollingId) {
    var self = this;

    if (pollingId !== undefined) {
      self.modalContainer.find('#polling-id').val(pollingId);
    }
    self.modalContainer.find('#progress-bar').removeClass('hidden');
    self.modalContainer.find('#retry-button').addClass('hidden');
    self.modalContainer.find('#progress-bar .progress-indicator-information .progress-indicator-information-title').html(Translator.translate('Sending message'));
    var progress = '20%';
    self.modalContainer.find('#progress-bar .progress-indicator-bar .progress-indicator-bar-progress').width(progress);
    self.modalContainer.find('#progress-bar .progress-indicator-information .progress-indicator-information-percentage').text(progress);

  };
  
    window.Polling.prototype.startJobAfterPolling = function (id) {
      var self = this;
      self.pollingInProgress = true;
      $.post(MAIN_URL + 'checkout/index/' + self.section + 'Polling', {'id': id}, function (response) {
        self.pollingInProgress = false;
        if(response.hasOwnProperty('serviceError') && (jQuery('#current_website_code').val() != 'telesales')){
          self.pollingFailed(response.message);
        } else if (response.error == true && (jQuery('#current_website_code').val() != 'telesales')) {
          self.modalContainer.find('#retry-button').removeClass('hidden');
          self.modalContainer.find('#progress-bar').addClass('hidden');
          self.modalContainer.find('#error-message').html(response.message);
        } else if (response.error == false || (jQuery('#current_website_code').val() == 'telesales')) {
          if ((response.hasOwnProperty('performed') && response.performed == true) || jQuery('#current_website_code').val() == 'telesales') {
            self[self.section + 'PollingSuccess'](response);
            if (self.section == 'saveSuperOrder') {
              $('#contract-continue-button').parent().removeClass('hidden');
              $('#saveContract .checkbox-inline').removeClass('hidden');
            }
          }
        }       
        else {
          console.log('error occurred / no response received');
        }
      });
    }
})(jQuery);
