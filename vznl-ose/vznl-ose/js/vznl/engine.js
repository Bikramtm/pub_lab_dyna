/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */

'use strict';

function updateOrdersSection (section, cluster, customer, page, orderByField, orderType, resultsOnPage, customerScreen, async, myOrders) {
  startPiwikCall(MAIN_URL + 'dataimport/index/searchByCustomer');
  cluster = cluster || null;
  customer = typeof customer !== 'undefined' ? customer : 0;
  page = page || 1;
  orderByField = orderByField || 'id';
  orderType = orderType || 'desc';
  resultsOnPage = resultsOnPage || '';
  customerScreen = customerScreen || false;
  customerScreen = customerScreen ? 1 : 0;
  async = async || true;

  if (customer !== 0) {
    jQuery('.user-menu-buttons a:first').addClass('active');
  }

  // Get data
  jQuery.ajax({
    type: 'POST',
    url: MAIN_URL + 'dataimport/index/searchByCustomer',
    data: {
      'section': section,
      'customerId': customer,
      'customerScreen': customerScreen,
      'type': '',
      'page': page,
      'cluster': cluster,
      'orderbyfield': orderByField,
      'countQuery': false,
      'ordertype': orderType,
      'resultsOnPage': resultsOnPage,
      'myOrders': myOrders
    },
    async: async
  }).done(function (response) {
    // Store the current counts
    var countKey = customerScreen ? '#customer-screen-orders' : '#' + section + '-search-results';
    var countNodes = jQuery(countKey + ' .panel-heading strong');
    var countData = [];
    var countVal;

    // Retrieve count data from current nodes
    jQuery.each(countNodes, function (index, node) {
      var countCluster = jQuery(node).attr('name').split('count_')[1];
      // If current cluster is cluster that was selected, get the data from response.
      if (countCluster == cluster) {
        countVal = '(' + response.body.buckets[cluster].count.label + ')';
      } else {
        countVal = jQuery(node).html();
      }
      countData.push({
        id: countCluster,
        value: countVal
      });
    });
    countData.push({
      id: 'total',
      value: jQuery('p.totaltext span[name="count_total"]').html()
    });

    // var htmlResponse = setHtmlTemplateData(section, response);
    var htmlResponse = setHtmlTemplateData('dynamic-order', response);
    var container;
    if (customerScreen) {
      // window.eventEmitter.emit('CUSTOMER_ORDERS_RENDER');
      return;
      // jQuery('#customer-screen-orders').html(htmlResponse);
      // jQuery(window).trigger('resize');
      // container = jQuery('#customer-screen-orders');
    } else {
      jQuery('#' + section + '-no-search-results').addClass('hidden');
      jQuery('#' + section + '-search-results').html(htmlResponse).removeClass('hidden');
      container = jQuery('#' + section + '-search-results');
    }

    // Update the header sections
    updateSectionOrdersIndexes(container, response, customerScreen, section, cluster);

    jQuery.each(countData, function (key, obj) {
      jQuery(countKey).find('[name="count_' + obj.id + '"]').html(obj.value);
      if (obj.id === 'total') {
        jQuery(countKey).find('p.totaltext span[name="count_total"]').html(obj.value);
      }
    });

    if (cluster === null) {
      // Get the totals
      var clusters = [];
      jQuery.each(response.body.buckets, function (i, n) {
        clusters.push(i);
      });

      var totalOrderCount = 0;
      jQuery.ajax({
        type: 'POST',
        url: MAIN_URL + 'dataimport/index/searchByCustomerAllClusters',
        data: {
          'section': section,
          'customerId': customer,
          'customerScreen': customerScreen,
          'type': '',
          'page': page,
          'clusters': clusters.join(','),
          'orderbyfield': orderByField,
          'countQuery': true,
          'ordertype': orderType,
          'resultsOnPage': resultsOnPage,
          'myOrders': myOrders
        },
        async: true

      }).done(function (response) {
        if (response.error === false) {
          jQuery.each(response.body.buckets, function (i, n) {
            var countValue = response.body.buckets[i].count.value;
            var countLabel = response.body.buckets[i].count.label;
            totalOrderCount += countValue;
            jQuery(countKey).find('[name="count_' + i + '"]').html('(' + countLabel + ')');
            var obj = jQuery('#' + section + '-search-results .totaltext span[name="count_total"]');
            if (jQuery(obj).is(':visible')) {
              jQuery(obj).html(totalOrderCount);
            }
          });
        }
      });

    }
    endPiwikCall(MAIN_URL + 'dataimport/index/searchByCustomer');
  });
}

function updateLinkAccountSection () {
  window.eventEmitter.emit('CUSTOMER_LINKED_RENDER');
}

function updateSectionOrdersIndexes (container, response, customerScreen, section, cluster) {
  var $ = jQuery;
  if (response.body.buckets[cluster] != undefined) {
    if (customerScreen) {
      var ordersPanel = '#customer-screen-orders';
    } else {
      var ordersPanel = '#' + section + '-search-results';
    }
    $(ordersPanel + ' ' + '.order_table_details[target=\'' + cluster + '_list\']').find('span').html('k');
    $(ordersPanel + ' ' + '.' + cluster + '_list').show();
  }

  container.find('.openorderby').off('click').click(function () {
    var cluster = $(this).attr('cluster');
    var page = 1;
    var orderByField = $(this).attr('order-by');
    var orderType = $(this).attr('oder-type');
    var customer = $(this).attr('orders_customer');
    var results = $(this).attr('orders-results');
    updateOrdersSection(section, cluster, customer, page, orderByField, orderType, results, customerScreen);
  });

  container.find('.open_order_error_info').hover(
    function () {
      $(this).find('.open_order_error').show();
    },
    function () {
      $(this).find('.open_order_error').hide();
    }
  );

  container.find('.order_details').off('click').click(function () {
    var container = $(this);
    if (container.attr('cluster') === 'inlife') {
      customer.loadInlifeOrder(container.attr('order_number'));
    } else {
      var target = container.next();
      toggleOrderDetails(container, !target.is(':visible'));
    }
  });

  container.find('.order_table_details').off('click').click(function () {
    var target = $(this).next('.' + $(this).attr('target'));
    if (target.is(':visible')) {
      $(this).find('span').html('j');
      target.hide();
    } else {
      $(this).parent().find('.orders_table').hide();
      $(this).parent().find('.order_table_details').find('span').html('j');
      var cluster = $(this).attr('orders_type');
      var customer = $(this).attr('orders_customer');
      updateOrdersSection(section, cluster, customer, 1, 'id', 'desc', '', customerScreen);
      $(this).find('span').html('k');
      target.show();
    }
  });

  container.find('.open_order_results_change').off('click').click(function () {
    var cluster = $(this).parent().find('span input').attr('orders_section');
    var orderByField = $(this).parent().find('span input').attr('order-by');
    var orderType = $(this).parent().find('span input').attr('orders_type');
    var customer = $(this).parent().find('span input').attr('orders_customer');
    var page = 1;
    var results = $(this).parent().find('span input').val();
    var maximum_results = $('#maximum_oo_results').val();
    if (parseInt(results) > parseInt(maximum_results)) {
      showModalError(Translator.translate('Sorry, but the maximum number of results per page is ') + ' ' + maximum_results);
    } else {
      updateOrdersSection(section, cluster, customer, page, orderByField, orderType, results, customerScreen);
    }
  });

  container.find('.open_orders_results input').keyup(function (e) {
    if (e.keyCode === 13) {
      var cluster = $(this).attr('orders_section');
      var orderByField = $(this).attr('order-by');
      var orderType = $(this).attr('orders_type');
      var customer = $(this).attr('orders_customer');
      var page = 1;
      var results = $(this).val();
      var maximum_results = $('#maximum_oo_results').val();
      if (parseInt(results) > parseInt(maximum_results)) {
        showModalError(Translator.translate('Sorry, but the maximum number of results per page is ') + ' ' + maximum_results);
      } else {
        updateOrdersSection(section, cluster, customer, page, orderByField, orderType, results, customerScreen);
      }
    }
  });

  container.find('.pagination-' + section + ' li').click(function () {
    if (!$(this).hasClass('disabled')) {
      var cluster = $(this).attr('orders_section');
      var orderByField = $(this).attr('order-by');
      var orderType = $(this).attr('orders_type');
      var customer = $(this).attr('orders_customer');
      var page = $(this).attr('orders-page');
      var results = $(this).attr('orders-results');
      updateOrdersSection(section, cluster, customer, page, orderByField, orderType, results, customerScreen);
    }
  });
}

function updateSelectedSectionResults (section) {
  setPageLoadingState(true);

  var ids = [];
  var reload_section = null;
  jQuery.each(jQuery('#' + section + '-search-results .order_table_details, #customer-screen-orders .order_table_details').next().find('tr.order_details input[type="checkbox"]:checked'), function (id, el) {
    ids.push(jQuery(el).parent().parent().parent().data('order-number'));
    if (reload_section === null) {
      reload_section = jQuery(el).parents('.orders_table').prev('.order_table_details');
    }
  });

  if (!ids.length) {
    jQuery('#order_status_refresh.modal').appendTo('body').modal();
    setPageLoadingState(false);
    return false;
  }

  jQuery.post(MAIN_URL + 'dataimport/index/updateSectionData', {section: section, ids: ids}, function (response) {
    if (response.error) {
      showModalError(response.message);
    } else {
      reload_section.next('.' + reload_section.attr('target')).hide();
      reload_section.click();
    }
  }).always(function () {
    setPageLoadingState(false);
  });
}

function showSaveCartModal(customerIsLoaded, cartId, isOffer) {
  jQuery('.col-main').css('display', 'block');
  jQuery('#config-wrapper').css('display', 'none');

  jQuery('#save-cart-form input').each(function (index, element) {
    jQuery(element).removeClass('validation-passed');
  });
  var $modalTitle = jQuery('#save-cart-modal .modal-title');


  const saveButton = jQuery('#save-cart-modal .save-cart-button');
  const saveAndSendButton = jQuery('#save-cart-modal .save-and-send-the-cart-button');
  const sendButton = jQuery('#save-cart-modal .send-the-cart-button');
  const closeButton = jQuery('#save-cart-modal .close-cart-button');

  var cartIdField = jQuery('#save-cart-modal input[name=cart_id]');
  if (cartId) {
    cartIdField.val(cartId);
    saveButton.addClass('hidden');
    saveAndSendButton.addClass('hidden');
    sendButton.removeClass('hidden');
    closeButton.removeClass('hidden');
    $modalTitle.text($modalTitle.data('title-send-shoppping-bag'));

  } else {
    if (customerIsLoaded == 0) {
      var isSoho = ToggleBulletSoho.getSohoState('.soho-toggle');
      jQuery('#save-cart-modal select[name=type]').val(isSoho);
      jQuery('#save-cart-modal select[name=type]').parent().parent('.form-group').addClass('disabled');
    }
    cartIdField.val("");
    saveButton.removeClass('hidden');
    saveAndSendButton.removeClass('hidden');
    sendButton.addClass('hidden');
    closeButton.addClass('hidden');
  }

  /** disable submit buttons */
  if (customerIsLoaded == 0) {
    saveButton.prop('disabled', true);
    saveAndSendButton.prop('disabled', true);
    sendButton.removeProp('disabled');
  } else {
    saveButton.removeProp('disabled');
    saveAndSendButton.removeProp('disabled');
    sendButton.removeProp('disabled');
  }

  if(isOffer) {
    $modalTitle.text($modalTitle.data('title-send-offer'));
  }

  if (window.customerDe.isBusiness) {
    jQuery(".private-customer-fields").addClass('hide');
    jQuery(".business-customer-fields").removeClass('hide');
    jQuery(".business-customer-fields input").addClass('required-entry');
    jQuery(".private-customer-fields input").attr('class','');
  } else {
    jQuery(".private-customer-fields").removeClass('hide');
    jQuery(".business-customer-fields").addClass('hide');
    jQuery(".business-customer-fields input").removeClass('required-entry');
    if (customerIsLoaded == 0) {
      jQuery("#save-cart-modal #firstname").addClass('required-entry validate-name');
      jQuery("#save-cart-modal #lastname").addClass('required-entry validate-name');
      jQuery("#save-cart-modal #dob").addClass('required-entry validate-dob validate-date-nl validate-min-age validate-max-age date-format-nl');
    }
  }

  jQuery('#save-cart-form').on('focus blur input change', '.form-group input, .form-group textarea, .form-group select', function(event) {
    validateSaveCartModal(saveButton, saveAndSendButton, sendButton);
  });

  // jQuery('#save-cart-modal').modal();
  jQuery('#save-cart-modal').appendTo('body').modal();
  jQuery('.select-wrapper').initSelectElement();
  jQuery('.input-wrapper').initInputElement();
}

function validateSaveCartModal(saveButton, saveAndSendButton, sendButton) {
  const validationFailed = jQuery('#save-cart-form .form-group .validation-failed').length;
  if (validationFailed > 0) {
    saveButton.prop('disabled', true);
    saveAndSendButton.prop('disabled', true);
    sendButton.prop('disabled', true);
  } else {
    saveButton.removeProp('disabled');
    saveAndSendButton.removeProp('disabled');
    sendButton.removeProp('disabled');
  }
}

function closeSaveCartModal() {
  jQuery('.col3-layout .col-main').css('z-index', '');
  jQuery('.col-main').css('display', '');
  if (jQuery('#right-panel-cart').css('display') != 'block')
    jQuery('#config-wrapper').css('display', '');
}

/**
 * Function used when saving a shopping cart
 * @param send_email
 * @param save_cart
 * @returns {boolean}
 */
function handleCartSendAndSaveAction(send_email, save_cart) {
  if (typeof(save_cart)==="undefined") {
    save_cart = true;
  }
  sessionStorage.removeItem('warnings');
  var modal = $j('#save-cart-modal');
  var saveCartModalForm = new VarienForm('save-cart-form', true);
  if (!saveCartModalForm.validator.validate())
    return false;

  var saveCartForm = $j('#save-cart-form');
  var formData = saveCartForm.serializeObject();

  formData['to_send'] = send_email;
  formData['to_save'] = save_cart;
  formData['new_cart'] = save_cart;

  $j.ajax({
    type: "POST",
    url: MAIN_URL + 'checkout/cart/persistCart',
    data: formData
  }).done(function (response) {

    if (response.error) {
      modal.find('div.alert').addClass('alert-danger').removeClass('hidden').html(Translator.translate(response.message));
    } else {
      modal.modal('hide');
      closeSaveCartModal();
      var notificationHeader = Translator.translate('Shopping bag saved!');
      if (send_email && save_cart) {
        notificationHeader = Translator.translate('Shopping bag saved and sent!');
      } else if (send_email) {
        notificationHeader = Translator.translate('Shopping bag sent!');
      }
      showValidateNotificationBar("<strong>" + notificationHeader + " </strong> " + response.message, 'success');
      if (formData['new_cart']) {
        setTimeout(function () {
          window.location.reload();
        }, 5000);
      }
    }
  });
}

function attachBootstrapScrollspyEventhandlers($) {
  var interPackageCounter = false;
  var interPackageNode = '';

  $(window).on("activate.bs.scrollspy", function (element) {
    if (interPackageCounter) {
      element.target = interPackageNode;
    }
    $('.right_expanded_packets').find('.vfde-arrow-right2').addClass('hidden');
    $(".right_expanded_packets li").removeClass('active');
    $(element.target).addClass('active');
    $(element.target).find('.vfde-arrow-right2').removeClass('hidden');
  });

  $(window).click('bs.scrollspy', function(element) {
    var selectedPackage = $(element.target).parents('li').first();
    var indexOfSelectedPackage = $(element.target).parents('li').first().index();
    var packagesLength = $("#right_expanded_packets li").length;
    if ((indexOfSelectedPackage > 0) && (indexOfSelectedPackage < (packagesLength - 1))) {
      interPackageCounter = true;
      interPackageNode = selectedPackage;
    } else {
      if (indexOfSelectedPackage == 0) {
        $('.right_expanded_packets').find('.vfde-arrow-right2').addClass('hidden');
        selectedPackage.find('.vfde-arrow-right2').removeClass('hidden');
      }
      interPackageCounter = false;
    }
  });
}

function expandTheRightSidebar() {
  var $ = jQuery;
  var enlargeButton = $('.col-right.sidebar .enlarge');
  if (enlargeButton.hasClass('expanded')) {
    expandRightSidebar();
    return;
  }
  var proceedButton = $('#configurator_checkout_btn');
  if (proceedButton.hasClass('loading')) {
    return;
  }
  $('body').addClass('block-click');
  proceedButton.addClass('loading');
  var isFixed = 0;

  if ($('input:hidden[name=isFixed]').val()) {
    isFixed = $('input:hidden[name=isFixed]').val();
  }
  var expand = true;
  if ($(enlargeButton).hasClass('right-direction')) {
    expand = false;
  }

  $.post(
    MAIN_URL + 'configurator/init/expandCart',
    {
      'checkout': window['onCheckout']
    },
    function(response) {
      getConfiguratorWarnings(response.cartProducts);
      $('#right-panel-cart').remove();
      if (isFixed == 0 || !expand) {
        $('.col-right').append(response.html).find('#right-panel-cart').show();
        $('.expanded-package-details').scrollspy('refresh');
        if (enlargeButton.data('activate-id')) {
          $('[data-summary-id="' + enlargeButton.data('activate-id') + '"]').trigger('click');
        }
        if (!supports_calc()) {
          CSSCalc();
        }
      }
      if (response.fixedSubscriptionName) {
        $(".fixed-subscription-name").text(response.fixedSubscriptionName);
      }
      $("body").tooltip({ selector: '[data-toggle=tooltip]' });

    });
  attachBootstrapScrollspyEventhandlers($);
}

jQuery( document ).ready(function() {
  $(document).on('click', '.consolidate-detail-expander', function () {
    if (!jQuery(".consolidate-detail-content").is(":visible")) {
      jQuery(this).find('.consolidate-detail-expand').attr("src", collapseArrowImg);
    } else {
      jQuery(this).find('.consolidate-detail-expand').attr("src", expandArrowImg);
    }
    jQuery(".consolidate-detail-content").toggle();
  });
});

function showConsolidationModal(consolidation) {
  if (consolidation) {
    jQuery('#consolidate_yes_button').addClass('disabled-mandatory');
  } else {
    jQuery('#consolidate_yes_button').removeClass('disabled-mandatory');
  }
  jQuery('#consolidation-modal').modal();
}

function showPartialTerminationModal() {
  jQuery('#partial-termination-modal')
    .modal()
    .find('.select-wrapper')
    .initSelectElement();
}

function showServiceErrorModal() {
  jQuery('#service-error-modal').modal();
}

function showValidateErrorModal(container) {
  if (container) {
    var message = jQuery(container).data('message');
    if (message) {
      jQuery("#validate-error-modal .message-content").html(message);
    }
  }
  jQuery('#validate-error-modal').modal();
}

function showBasketUnsyncedModal() {
  jQuery("#basket-unsynced-modal").modal();
}

function showServiceabilityWarningModal() {
  jQuery("#serviceability-warning-modal").modal();
}

function consolidateBasket(consolidate) {
  var $ = jQuery;
  $('#configurator_checkout_btn').addClass('loading');
  $.post(
    MAIN_URL + 'validatecart/Validate/consolidate',
    {
      'checkout': window['onCheckout'],
      'consolidate' : consolidate
    },
    function(response) {
      $('#configurator_checkout_btn').removeClass('loading');
      if (response.consolidation) {
        showConsolidationModal(response.consolidationFalse);
      } else if (response.callValidatecart) {
        $('#consolidation-modal').modal('hide');
        $('.col-right').find('#right-panel-cart').remove('#right-panel-cart');
        $('.col-right').append(response.html).find('#right-panel-cart').show();
        $('.expanded-package-details').scrollspy('refresh');
        if (!supports_calc()) {
          CSSCalc();
        }
        expandRightSidebar();
        if (response.hasHardwareProduct) {
            var message = "<b>"+Translator.translate("CUSTOMIZED PACKAGE")+"</b>"+ Translator.translate(" there is hardware added to it ")+response.hasHardwareProduct;
            showNotificationBar(message, 'success');
          }
      }
    }
  );
}

function validateBasketWithCancelledItems() {
  var $ = jQuery;
  var btn = $('.col-right.sidebar .enlarge');
  var terminationReason  = $('#partial-termination-reason').val();
  var terminationSubreason = $('#partial-termination-subreason').val();
  var terminationCompitator = $('#partial-termination-competitor').val();
  if (!terminationReason || !terminationSubreason) {
    if(!terminationReason){
      $('.pt-main-select').addClass('error');
    }
    if(!terminationSubreason){
      $('.pt-subSelect-select').addClass('error');
    }
    return false;
  }
  if(!$('#partial-termination-competitor').parent().parent().hasClass("hidden") && !terminationCompitator) {
    return false;
  }
  var data = {termination_reason: terminationReason, termination_subreason: terminationSubreason, termination_competitor: terminationCompitator};
  $('#configurator_checkout_btn').addClass('loading');
  $.post(MAIN_URL + 'validatecart/Validate/validateCancelledItems', data, function(response) {
    $('#configurator_checkout_btn').removeClass('loading');
    if (response.error) {
      $("#partial-termination-modal").modal('hide');
      if (response.message && !response.validateErrorMessage && !response.unsyncedBasketItems) {
        jQuery('#loading-modal').modal('hide');
        showNotificationBar("<strong>" + Translator.translate("Something went wrong") + "!</strong>" +response.message, 'error');
        return false;
      }
      $('.cart_packages div[data-package-type="int_tv_fixtel"] div[data-search-results-drawer-visibility="hidden"]').first().addClass("incomplete");
      if (response.validateErrorMessage) {
        showValidateNotificationBar(
          '<strong>' + Translator.translate('Something went wrong') + '!</strong>' + response.message,
          'error',
          response.validateErrorMessage ? '<button type="button" class="btn" onclick="showValidateErrorModal(this)" data-message='+JSON.stringify(response.validateErrorMessage)+'>'+Translator.translate('More info')+'</button>' : false
        );
      } else if (response.unsyncedBasketItems && response.unsyncedBasketItems.length) {
        showValidateNotificationBar("<strong>SOMETHING WENT WRONG! </strong>" + response.message, 'error', response.button);
        var unsyncedBasketItemsHtml = '';
        response.unsyncedBasketItems.each(function(item){
          unsyncedBasketItemsHtml +=
            '<div class="unsynced-basket-item">'
                +'<span class="unsynced-basket-item-icon"></span>'
                +'<div class="unsynced-basket-item-details">'
                    +'<div class="unsynced-basket-item-name">' + item.productName + '</div>'
                    +'<div class="unsynced-basket-item-id">Offer ID: ' + item.offerId +'</div>'
                +'</div>'
            +'</div>';
        });
        $("#basket-unsynced-modal .unsynced-basket-items").html(unsyncedBasketItemsHtml);
      }
      return false;
    }
    $('.cart_packages div[data-package-type="int_tv_fixtel"] div[data-search-results-drawer-visibility="hidden"]').first().removeClass("incomplete");
    $('#partial-termination-modal').modal('hide');
    $('.col-right').find('#right-panel-cart').remove('#right-panel-cart');
    $('.col-right').append(response.html).find('#right-panel-cart').show();
    $('.expanded-package-details').scrollspy('refresh');
    if (btn.data('activate-id')) {
      $('[data-summary-id="' + btn.data('activate-id') + '"]').trigger('click');
    }
    if (!supports_calc()) {
      CSSCalc();
    }

    $("body").tooltip({ selector: '[data-toggle=tooltip]' });

    if (response.reload) {
      if (response.callValidatecart) {
        showNotificationBar(response.message, 'warning');
      }
    }
    else if (response.consolidation) {
      $('#loading-modal').modal('hide');
      showConsolidationModal(response.consolidationFalse);
      if (response.hasHardwareProduct) {
        var message = "<b>"+Translator.translate("CUSTOMIZED PACKAGE")+"</b>"+ Translator.translate(" there is hardware added to it ")+response.hasHardwareProduct;
        showNotificationBar(message, 'success');
      }
    } else {
      $('#loading-modal').modal('hide');
      if (response.callValidatecart) {
        if (response.hasHardwareProduct) {
          var message = "<b>"+Translator.translate("CUSTOMIZED PACKAGE")+"</b>"+ Translator.translate(" there is hardware added to it ")+response.hasHardwareProduct;
          showNotificationBar(message, 'success');
        }
      }
      expandRightSidebar();
    }
  });
}

function  clearCart() {
  window.location = "/";
}

function enableCouponCodeField(el) {
  jQuery(el).hide()
  const coupon = jQuery(el).parent().find('.coupon');
  coupon.show();
  coupon.find('.input-wrapper').initInputElement();
}

function enableCouponCodeField(el) {
  jQuery(el).hide()
  const coupon = jQuery(el).parent().find('.coupon');
  coupon.show();
  coupon.find('.input-wrapper').initInputElement();
}

/**
 *
 * @param form
 * @param parent_class
 * @returns {boolean}
 */
function applyCouponCode(form, parent_class) {
  var $ = jQuery;
  var isCheckout = (arguments.length === 3 && arguments[2] != 0);
  var additional_param = '';
  var field = $(form).find('[name=coupon]');
  var coupon = field.val();
  var package_id = $(form).parents(parent_class).data('package-id');

  if (!isCheckout) {
    var checkoutButton = $('#configurator_checkout_btn');
    var classList = checkoutButton.attr('class');
    var oldDisabled = checkoutButton.prop('disabled');
  }
  else
  {
    var package_id = $(form).parent().attr('package-id');
  }

  var hasWarningClass = classList ? classList.indexOf("btn-warning") : false;

  if ($.trim(coupon) !== '') {
    window.VEngine.prototype.setLoading(true);
    $.post(MAIN_URL + 'vznl_pricerules/promo/coupon', {coupon:coupon, packageId:package_id, parentClass: parent_class, isCheckout: isCheckout }, function(response) {
      if (response.error == true) {
        $('#coupon-check-invalid-coupon .message').html(Translator.translate(response.message));
        $('#coupon-check-invalid-coupon').modal();
        //$(form).find('.cart-coupon-input').val('');
      } else {
        if (!oldDisabled) {
          response.totals = response.totals.replace('disabled="disabled" ', '');
        }
        if (hasWarningClass && hasWarningClass != -1) {
          response.totals = response.totals.replace('btn-info ', 'btn-warning ');
        }
        if (isCheckout) {
          additional_param = ', true';
          $('#checkout_cart_totals').replaceWith(response.totals);
          if (typeof response.quoteTotals !== 'undefined' && response.quoteTotals) {
            window['quoteGrandTotal'] = response.quoteTotals.quoteGrandTotal.rawValue;
            $('.quoteGrandTotal').text(response.quoteTotals.quoteGrandTotal.formatted);
            $('.split-payment-method[data-package-id="{0}"]'.format(package_id)).find('.amount').text(response.quoteTotals.packageTotal);
            window['checkout'].ignoreOffer = true;
            $('[name="customer[type]"]:checked').trigger('click');
            window['checkout'].ignoreOffer = false;
          }
        }

        $('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('.submit-coupon-button').hide();
        $('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('[name=coupon]').attr('disabled', 'disabled').hide();
        $('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).append($('<span />', { class: 'remove-coupon', html: Translator.translate('Coupon added: %s').replace('%s', coupon), onclick: 'removeCouponCode(this,\''+coupon+'\',\''+parent_class+'\''+ additional_param +')' }))
        $('#couponCodeInput_{0}'.format(package_id)).hide();

        $('.sticker #cart_totals').replaceWith(response.totals);
        $('.expanded-shoppingcart-totals #cart_totals').replaceWith(response.totals);

        if (parent_class === '.side-cart' && $('.side-cart').length > 0) {
          $('.side-cart.active').replaceWith(response.rightBlock);
        }
        if (parent_class === '.package-expand' && $('#right-panel-cart').length > 0) {
          $('#right-panel-cart').replaceWith(response.rightBlock);
          $('.cart_packages div[data-package-id="'+ package_id +'"]').replaceWith(response.rightBlockCollapsed);
          $('#right-panel-cart').show();
        }
        if (parent_class === '.coupon-class' && $('#right-panel-cart').length > 0) {
          $('#right-panel-cart').replaceWith(response.rightBlock);
          $('.cart_packages div[data-package-id="'+ package_id +'"]').replaceWith(response.rightBlockCollapsed);
          $('#right-panel-cart').show();
        }

        if (parent_class === '.package-item-description-coupon' && $('#right-panel-cart').length > 0) {
          $('#right-panel-cart').replaceWith(response.rightBlock);
          $('.cart_packages div[data-package-id="'+ package_id +'"]').replaceWith(response.rightBlockCollapsed);
          $('#right-panel-cart').show();
        }
      }
    });
  }
  return false;
}

/**
 *
 * @param self
 * @param coupon
 * @param parent_class
 * @returns {boolean}
 */
function removeCouponCode(self, coupon, parent_class) {
  if (jQuery.trim(coupon) !== '') {
    var package_id = jQuery(self).parents(parent_class).data('package-id');
    var isCheckout = arguments.length === 4;

    if (!isCheckout) {
      var checkoutButton = jQuery('#configurator_checkout_btn');
      var classList = checkoutButton.attr('class');
      var oldDisabled = checkoutButton.prop('disabled');
    }
    else
    {
      var package_id = jQuery(self).parent().attr('package-id');
    }

    var hasWarningClass = classList ? classList.indexOf('btn-warning') : false;

    setPageLoadingState(true);
    jQuery.post(MAIN_URL + 'vznl_pricerules/promo/coupon', {coupon:jQuery.trim(coupon), packageId:package_id, remove: true, parentClass: parent_class , isCheckout: isCheckout}, function(response) {
      setPageLoadingState(false);
      if (response.error == true) {
        jQuery('#coupon-check-invalid-coupon .message').html(Translator.translate(response.message));
        jQuery('#coupon-check-invalid-coupon').modal();
      } else {
        jQuery('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('.cart-coupon-input').attr('disabled', false).val('').show();
        jQuery('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('.submit-coupon-button').show();
        jQuery('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('.promocodelink').show();
        jQuery('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('.remove-coupon').remove();
        jQuery('#couponCodeInput_{0}'.format(package_id)).show();
        if (!oldDisabled) {
          response.totals = response.totals.replace('disabled="disabled" ', '');
        }
        if (hasWarningClass && hasWarningClass != -1) {
          response.totals = response.totals.replace('btn-info ', 'btn-warning ');
        }
        jQuery('.sticker #cart_totals').replaceWith(response.totals);
        jQuery('.expanded-shoppingcart-totals #cart_totals').replaceWith(response.totals);
        if (isCheckout) {
          jQuery('.checkout-totals #cart_totals').replaceWith(response.totals);
          if (typeof response.quoteTotals != 'undefined' && response.quoteTotals) {
            window['quoteGrandTotal'] = response.quoteTotals.quoteGrandTotal.rawValue;
            jQuery('.quoteGrandTotal').text(response.quoteTotals.quoteGrandTotal.formatted);
            jQuery('.split-payment-method[data-package-id="{0}"]'.format(package_id)).find('.amount').text(response.quoteTotals.packageTotal);
            window['checkout'].ignoreOffer = true;
            jQuery('[name="customer[type]"]:checked').trigger('click');
            window['checkout'].ignoreOffer = false;
          }
        }

        if (parent_class == '.side-cart') {
          jQuery('.side-cart.active').replaceWith(response.rightBlock);
        }
        if (parent_class == '.package-expand') {
          jQuery('#right-panel-cart').replaceWith(response.rightBlock);
          jQuery('.cart_packages div[data-package-id="'+ package_id +'"]').replaceWith(response.rightBlockCollapsed);
          jQuery('#right-panel-cart').show();
        }
        if (parent_class === '.coupon-class') {
          jQuery('#right-panel-cart').replaceWith(response.rightBlock);
          jQuery('.cart_packages div[data-package-id="'+ package_id +'"]').replaceWith(response.rightBlockCollapsed);
          jQuery('#right-panel-cart').show();
        }

        if (parent_class == '.package-item-description-coupon') {
          jQuery('#right-panel-cart').replaceWith(response.rightBlock);
          jQuery('.cart_packages div[data-package-id="'+ package_id +'"]').replaceWith(response.rightBlockCollapsed);
          jQuery('#right-panel-cart').show();
        }
      }
    });
  }
  return false;
}

// note functionality
jQuery(document).ready(function ($) {
  $(document).on('click', '.package-notes, .package-notes-checkout', function () {
    window.customerDe.getNoteOnPackage(this, 0);
  });

  $('.edit-note-btn').click(function () {
    var noteId = $(this).attr('data-note-id');
    window.customerDe.getNoteOnPackage(this, noteId);
  });
});

/**
 *
 * @param ctn
 * @param sale_type
 * @param current_products
 * @returns {boolean}
 */
function createPackage(ctn, sale_type, current_products, package_type, package_creation_type_id, contract_end_date, contract_start_date) {
  if(checkoutDeliverPageDialog == true) {
    // If agent tries to load a customer via advanced search while on Checkout delivery show confirmation modal
    jQuery('#checkout_deliver_confirmation').data('function','createPackage').data('param1', ctn).data('param2', sale_type).data('param3', current_products).appendTo('body').modal();
    return false;
  }

  ctn = jQuery.trim(ctn);
  current_products = current_products || null;

  // Handle situation when it is called from order edit page
  if (typeof initConfigurator === 'undefined' || typeof activateBlock === 'undefined') {
    jQuery('#spinner').toggle('show', function() {
      Mage.Cookies.set('callFunc', JSON.stringify({"fn": "initConfigurator", "args": ["mobile", null, sale_type, ctn, current_products]}));
      window.location.replace('/');
    });
    return;
  }

  jQuery('#close_customer_details').trigger('click');
  if (jQuery('.mark_ctn[data-rawctn="' + ctn + '"]').length) {
    activateBlock(jQuery('.mark_ctn[data-rawctn="' + ctn + '"]').first().siblings('.package-type').first());
  } else {
    jQuery('#package-types').addClass('hide');

    if (!showOnlyOnePackage) {
      jQuery('#show-packages').removeClass('hide');
      jQuery('#cart-spinner-wrapper').find('.cart_packages').removeClassPrefix('control-buttons-');
    }

    initConfigurator(package_type, null, sale_type, ctn, current_products, false, '', '', null, '', null, null, null, package_creation_type_id, package_type == 'fixed' ? 1 : 0, contract_end_date, contract_start_date);
  }
}

function toggleOrderDetails(container, state) {
    var target = jQuery(container).next();
    if (!state) {
        container.find('span.menu-arrow-down').html('j');
        target.hide();
    } else {
        container.parent().find('.package_target').hide();
        container.parent(".order_details span.menu-arrow-down").html('j');
        container.find('span.menu-arrow-down').html('k');
        target.show();
    }
}

function createOneOffDeal(nonRetainableArgs) {
  if (jQuery('#is_order_edit_mode').val() == 1) {
    showModalError(Translator.translate('You can not renew an order because you are changing and order. You have to cancel your changes first and go to the configurator.'));
    return;
  }

  var msg = jQuery('#no_retainable_msg');
  msg.hide();
  if (typeof nonRetainableArgs === "undefined") {
    var nonRetainable = {};
    jQuery('.ctn_record').not('.retainable-ctn').each(function(i, _el) {
      jQuery.extend(nonRetainable, {0: jQuery(_el).data('rawctn')});
    });
  } else {
    var nonRetainable = nonRetainableArgs;
  }

  // Handle situation when it is called from order edit page
  if (typeof initConfigurator === 'undefined' || typeof createCartPackageLine === 'undefined') {
    jQuery('#spinner').toggle('show', function() {
      Mage.Cookies.set('callFunc', JSON.stringify({"fn": "createOneOffDeal", "args": [nonRetainable]}));
      window.location.replace('/');
    });
    return;
  }

  jQuery.post(MAIN_URL + 'customerde/details/oneOffDeal', { nonRetainable: jQuery.makeArray(nonRetainable) }, function(data) {
    if (data.hasOwnProperty('error')) {
      msg.html(data.message).show();
      return;
    }
    jQuery('.col-right .sticker').replaceWith(data.rightSidebar);
    jQuery('#close_customer_details').trigger('click');
    jQuery('.buttons-container #package-types').addClass('hide');
    jQuery('#show-packages').addClass('hide');

    var _ctn;
    var _packageId;
    var _packageTypeId=1;
    jQuery.each(data.packageIds, function(ctn, packageData) {
      _ctn = ctn;
      _packageId = packageData.id;
    });

    initConfigurator(data.type, _packageId,  data.saleType, _ctn, null, true, null, null, null, null, null, null, null, _packageTypeId, 1);
  });

  jQuery('#create-one-of-deal-modal').modal('hide');
}

function toggleDropdownButton() {
  jQuery('#cart-overview [data-toggle=dropdown]').on('click', function(e) {
    e.preventDefault();
    e.stopPropagation();
    if (jQuery(this).parent().hasClass('open')) {
      jQuery(this).parent().removeClass('open');
    } else {
      jQuery('.cart_overview').find('.btn-group.open').removeClass('open');
    }
  });
}

function reloadQuote(quoteId, isOffer, element) {
  element = jQuery(element);

  if (element.data('continue-order') === false) {
    showModalError(Translator.translate('You cannot continue offers saved by other agencies'));
    return false;
  }

  if(checkoutDeliverPageDialog == true) {
    // If agent tries to load a customer via advanced search while on Checkout delivery show confirmation modal
    jQuery('#checkout_deliver_confirmation').data('function','reloadQuote').data('param1', quoteId).appendTo('body').modal();
    return false;
  }

  if(element.data('saleidvalidation') === false) {
    //If created agent sales triple id validition failed
    showModalError(Translator.translate('Sales Id (created the offer) validation failed'));
    return false;
  }

  if (jQuery('#is_order_edit_mode').val() == 1) {
    showModalError(Translator.translate('You can not renew an order because you are changing and order. You have to cancel your changes first and go to the configurator.'));
    return;
  } else {
    setPageLoadingState(true);
    jQuery.post(MAIN_URL + 'checkout/cart/activateCart', {quote_id: quoteId, is_offer: isOffer}, function (data) {
      setPageLoadingState(false);
      if (data.error) {
        showModalError(data.message);
      } else {
        if (data.serviceAddress) {
          var attrs = {
            'data-id': data.serviceAddress.addressId,
            'data-street': data.serviceAddress.street,
            'data-building-nr': data.serviceAddress.housenumber,
            'data-postalcode': data.serviceAddress.postalcode,
            'data-city-name': data.serviceAddress.city,
            'data-house-addition': data.serviceAddress.housenumberaddition,
            'data-is-customer': true
          };
          var createAddress = document.createElement('div');
          for (let key in attrs) {
            createAddress.setAttribute(key, attrs[key]);
          }
          // Perform serviceAvailability call
          address.checkService(createAddress);
        }
      }
    })
    .complete(function(data) {
      var data = data.responseJSON;
      if (data.hasOwnProperty('packageId') && data.packageId) {
        window.location.href = MAIN_URL + '?packageId=' + data.packageId;
      } else if (data.hasOwnProperty('redirect') && data.redirect) {
        window.location.href = MAIN_URL + data.redirect;
      }
    });
  }
}

function showStockStatus(el, e, showOtherStores) {
  setPageLoadingState(true);
  e.stopPropagation();
  if (jQuery('#stock-modal').hasClass('in')) { // Check if modal is already active
    jQuery('#stock-modal').modal('hide');
  }
  var url = MAIN_URL + 'configurator/init/getStock';
  var id = jQuery(el).data('id');
  jQuery.ajax(url, {
      type: 'POST',
      data: {axi_store_id: id}
    }
  ).done(function (data) {
      jQuery('div.modal-backdrop').hide();
      setPageLoadingState(false);
      if (data.hasOwnProperty('error') && data.error) {
        showModalError(Translator.translate(data.message));
      } else {
        jQuery('#stock-modal .modal-dialog .modal-body .modal-block').html(data);
        jQuery('#stock-modal').appendTo('body').modal();
        if (showOtherStores) {
          jQuery('#stock-modal .modal-dialog .modal-body .modal-block #display-stores').trigger('click');
        }
      }
    }
  );
}

function highlightErrors(content) {
  var $ = jQuery;
  if (!content) {
    return false;
  }
  var active = false;
  var activePackage = $('#cart-spinner-wrapper .cart_packages .active');
  if (activePackage) {
    active = activePackage.data('package-id');
  }

  var moreInfo = [];
  $.each(content, function(id, el) {
    if (el.hasOwnProperty('soc-error')) {
      showModalError(el['soc-error']);
    } else if (active && id == active) {
      // Remove highlight from all sections
      $('#config-wrapper div.incomplete').removeClass('incomplete');
      // Highlight the missing sections
      $.each(el, function(section, message) {
        var block = $('#' + section.toLowerCase() + '-block');
        if (block.length > 0) {
          block.addClass('incomplete');
          if (message) {
            moreInfo.push('<p>'+message+'</p>');
          }
        } else {
          moreInfo.push('<p>'+message+'</p>');
        }
      });
      // Highlight a package in side cart
      $('#cart-spinner-wrapper .cart_packages [data-package-id="' + id + '"]').addClass('incomplete');

      // Scroll to the highest section that is incomplete.
      const changingOrder = $('#config-wrapper').find('.change-order-notification').length;
      const incompleteBlocks = $('#config-wrapper').find('.conf-block.incomplete');
      if (incompleteBlocks.length > 0) {
        const position = incompleteBlocks[0].offsetTop;
        const offset = changingOrder ? 130 : 20;
        $('#config-wrapper').scrollTop(position - offset);
      }

    } else {
      // When package is not active highlight the entire package
      $('#cart-spinner-wrapper .cart_packages [data-package-id="' + id + '"]').addClass('incomplete');
    }
  });

  return moreInfo.join("\n");
}

function loadSuperOrder (orderId, redirectImei) {
  if (checkoutDeliverPageDialog == true) {
    // If agent tries to load a customer via advanced search while on Checkout delivery show confirmation modal
    jQuery('#checkout_deliver_confirmation').data('function', 'loadSuperOrder').data('param1', orderId).appendTo('body').modal();
    return false;
  }

  if (customer) {
    customer.setLoading();
  }

  window.location.href = MAIN_URL + '?orderId=' + orderId;
}

/**
 *
 * @param dom
 */
function loadRegisterImei (orderId, customerId) {
  self.order_id = orderId;
  var callback = function () {
    window.setPageOverrideLoading(true);
    var refreshCallback = function () {
      loadSuperOrder(self.order_id,true);
    };
    return customer.poolRefreshData(refreshCallback);
  };

  jQuery('#customer_init_order').modal('hide').data('modal', null); // destroy modal

  // Only reload the customer if the order belongs to another customer
  if (customerId != customer.getLoggedInCustomer()) {
    customer.loadCustomerData(customerId, false, false, callback, true);
  } else {
    loadSuperOrder(self.order_id,true);
  }
}

function getConfiguratorWarnings(cartProductsHash) {
  var $ = jQuery;
  var isFixed = 0;
  if ($('input:hidden[name=isFixed]').val()) {
    isFixed = $('input:hidden[name=isFixed]').val();
  }
  var enlargeButton = $('.enlarge');
  var expand = true;
  if ($(enlargeButton).hasClass('right-direction')) {
    expand = false;
  }
  // Getting min/max/eql and cardinality warning messages for cart
  $.get(MAIN_URL + 'configurator/cart/getConfiguratorWarnings', {
    'cartProductsHash': cartProductsHash
  }, true)
    .done(function(data) {
      if (data.warnings.error) {
        if (data.warnings.message && !data.warnings.validateErrorMessage && !data.warnings.unsyncedBasketItems) {
          jQuery('#loading-modal').modal('hide');
          showNotificationBar("<strong>" + Translator.translate("Something went wrong") + "!</strong>" + data.warnings.message, 'error');
          return false;
        }
        $('.cart_packages div[data-package-type="int_tv_fixtel"] div[data-search-results-drawer-visibility="hidden"]').first().addClass("incomplete");
        $('#loading-modal').modal('hide');
        if (data.warnings.validateErrorMessage) {
          showNotificationBar(data.warnings.validateErrorMessage, 'error');
        } else if (data.warnings.unsyncedBasketItems && data.warnings.unsyncedBasketItems.length) {
          showValidateNotificationBar("<strong>" + Translator.translate("Something went wrong") + "!</strong>" + data.warnings.message, 'error', data.warnings.button);
          var unsyncedBasketItemsHtml = '';
          data.warnings.unsyncedBasketItems.each(function(item){
            unsyncedBasketItemsHtml +=
                '<div class="unsynced-basket-item">'
                +'<span class="unsynced-basket-item-icon"></span>'
                +'<div class="unsynced-basket-item-details">'
                +'<div class="unsynced-basket-item-name">' + item.name + '</div>'
                +'<div class="unsynced-basket-item-id">Commercial ID: ' + item.sku +'</div>'
                +'</div>'
                +'</div>';
          });
          $("#serviceability-warning-modal .unsynced-subscription-name").text(data.warnings.fixedSubscriptionName + " ");
          $("#serviceability-warning-modal .unsynced-basket-items").html(unsyncedBasketItemsHtml);
        }
        $('#configurator_checkout_btn').removeClass('loading');
        $('#configurator_checkout_btn').addClass('disabled');
      } else {
        $('body').removeClass('block-click');
        $('#configurator_checkout_btn').removeClass('loading');
        var warnings = [];
        $.each(data.warnings, function(packageId, packageWarnings) {
          if (typeof packageWarnings === 'string') {
            packageWarnings = packageWarnings.split();
          }
          $.each(packageWarnings, function(type, warning) {
            warnings.push(warning);
          });
        });

        warnings = warnings ? warnings.join('<br>') : '';
        if (warnings !== '') {
          var moreInfo = highlightErrors(data.warnings);
          showValidateNotificationBar(
              '<strong>' + Translator.translate('Something went wrong') + '!</strong>' + Translator.translate('Please check your package configuration'),
              'error',
              moreInfo ? '<button type="button" class="btn" onclick="showValidateErrorModal(this)" data-message='+JSON.stringify(moreInfo)+'>'+Translator.translate('More info')+'</button>' : false
          );
        } else {
          if (isFixed == 0) {
            expandRightSidebar();
          } else if (isFixed == 1 && expand) {
            getValidateCart();
          } else if(!expand) {
            expandRightSidebar();
          }
        }

        $('#expanded_checkout_btn').prop('disabled', !data.complete);
        if (data.complete) {
          $('#expanded_checkout_btn').removeClass('disabled');
          $('#expanded_checkout_btn').removeClass('incomplete');
        } else {
          $('#expanded_checkout_btn').addClass('disabled');
          $('#expanded_checkout_btn').addClass('incomplete');
        }
      }
    });
}

function getValidateCart() {
  var $ = jQuery;
  var isFixed = 0;
  var btn = $('.col-right.sidebar .enlarge');

  if ($('input:hidden[name=isFixed]').val()) {
    isFixed = $('input:hidden[name=isFixed]').val();
  }

  if (isFixed == 1) {
    $('#configurator_checkout_btn').addClass('loading');
    $.post(MAIN_URL + 'configurator/cart/getValidateCart', function(response) {
      $('#configurator_checkout_btn').removeClass('loading');
      if (response.error) {
       if (response.message && !response.validateErrorMessage && !response.unsyncedBasketItems) {
          jQuery('#loading-modal').modal('hide');
          showNotificationBar("<strong>" + Translator.translate("Something went wrong") + "!</strong>" + response.message, 'error');
          return false;
        }
        $('.cart_packages div[data-package-type="int_tv_fixtel"] div[data-search-results-drawer-visibility="hidden"]').first().addClass("incomplete");
        $('#loading-modal').modal('hide');
        if (response.validateErrorMessage) {
          showValidateNotificationBar(
            '<strong>' + Translator.translate('Something went wrong') + '!</strong>' + response.message,
            'error',
            response.validateErrorMessage ? '<button type="button" class="btn" onclick="showValidateErrorModal(this)" data-message='+JSON.stringify(response.validateErrorMessage)+'>'+Translator.translate('More info')+'</button>' : false
          );
        } else if (response.unsyncedBasketItems && response.unsyncedBasketItems.length) {
          showValidateNotificationBar("<strong>" + Translator.translate("Something went wrong") + "!</strong>" + response.message, 'error', response.button);
          var unsyncedBasketItemsHtml = '';
          response.unsyncedBasketItems.each(function(item){
            unsyncedBasketItemsHtml +=
              '<div class="unsynced-basket-item">'
                  +'<span class="unsynced-basket-item-icon"></span>'
                  +'<div class="unsynced-basket-item-details">'
                      +'<div class="unsynced-basket-item-name">' + item.productName + '</div>'
                      +'<div class="unsynced-basket-item-id">Offer ID: ' + item.offerId +'</div>'
                  +'</div>'
              +'</div>';
          });
          $("#basket-unsynced-modal .unsynced-basket-items").html(unsyncedBasketItemsHtml);
        }
        return false;
      }
      $('.cart_packages div[data-package-type="int_tv_fixtel"] div[data-search-results-drawer-visibility="hidden"]').first().removeClass("incomplete");
      $('.col-right').append(response.html).find('#right-panel-cart').show();
      $('.expanded-package-details').scrollspy('refresh');
      if (btn.data('activate-id')) {
        $('[data-summary-id="' + btn.data('activate-id') + '"]').trigger('click');
      }
      if (!supports_calc()) {
        CSSCalc();
      }

      $("body").tooltip({ selector: '[data-toggle=tooltip]' });
      var html = '';
      if (response.isItemCancelled) {
        $('#loading-modal').modal('hide');
        if (response.fixedSubscriptionName) {
          $(".orderline-header-title-order").text(response.fixedSubscriptionName);
          $("#basket-unsynced-modal .unsynced-subscription-name").text(response.fixedSubscriptionName + " ");
        }
        if (response.packageTypeGuiName) {
          $(".orderline-header-title-category").text(response.packageTypeGuiName);
        }
        const title = Translator.translate("Package configuration");
        html = '<ul>';
        for(var i=0; i<response.allPackageItems.length; i++) {
          html +='<li>'+response.allPackageItems[i]+'</li>';
        }
        html +='</ul>';

        var itemCancelledHtml = '';
        response.itemCancelledArray.each(function(item){
          itemCancelledHtml +=
            '<div class="unsynced-basket-item">'
            +'<span class="unsynced-basket-item-icon" data-toggle="tooltip" title="'+ Translator.translate("Terminated")+'"></span>'
            +'<div class="unsynced-basket-item-details">'
            +'<div class="unsynced-basket-item-name">' + item.productName + '</div>'
            +'<div class="unsynced-basket-item-id">Offer ID: ' + item.offerId +'</div>'
            +'</div>'
            +'</div>';
        });
        $("#partial-termination-modal .canceled-basket-items").html(itemCancelledHtml);

        $("#partial-termination-modal .tip-trig-hover-popover")
          .attr('title', title)
          .attr('data-content', html);
        $("#partial-termination-modal .tip-trig-hover-popover").popover({
          trigger: 'hover',
          animation: false
        });
        showPartialTerminationModal();
      }

      else if (response.reload) {
        $('#loading-modal').modal('hide');
        if (response.callValidatecart) {
          if (response.message) {
            showValidateNotificationBar(response.message, 'error');
          }
        }
      }
      else if (response.consolidation) {
        showConsolidationModal(response.consolidationFalse);
      } else {
        if (response.callValidatecart) {
          if (response.hasHardwareProduct) {
            var message = "<b>"+Translator.translate("CUSTOMIZED PACKAGE")+"</b>"+ Translator.translate(" there is hardware added to it ")+response.hasHardwareProduct;
            showNotificationBar(message, 'success');
          }
        }
        expandRightSidebar();
      }
    });
  }
}

function expandRightSidebar() {
  var $ = jQuery;
  var direction = '';
  var enlargeButton = $('.enlarge');
  var bundleModal = $('.modal.bundleModal.bundleDetails');
  var distance = $('.main').outerWidth() - (enlargeButton.outerWidth() * 2);
  var panelDefaultWidth = 300;

  if ($(enlargeButton).hasClass('left-direction')) {
    direction = 'left';
    var leftSide = $('.col-left').outerWidth();
    enlargeButton.removeClass('left-direction');
    enlargeButton.addClass('right-direction');
    bundleModal.removeClass('hidden').addClass('hidden');
  } else {
    direction = 'right';
    var rightSide = $('.col-left').outerWidth();
    enlargeButton.removeClass('right-direction');
    enlargeButton.addClass('left-direction');
    $('#expanded_checkout_btn[extendcartvalue="true"]').attr('extendcartvalue','false');
    bundleModal.removeClass('hidden')
  }

  var minWidth = ($('.main').outerWidth() - (enlargeButton.outerWidth() * 2) - $('.sticker').parent().outerWidth()) + 'px';
  var panelContent = enlargeButton.parent().find('.panel-content');
  panelContent.css('min-width', minWidth);
  if (enlargeButton.hasClass('expanded')) {
    enlargeButton.siblings('.sticker').css('z-index', 2).fadeIn(500);
    panelContent
      .css('z-index', 1)
      .fadeOut(500)
    ;
    if (direction === 'left') {
      $('.col-right').find('#right-panel-cart').remove();
    }

    enlargeButton.parent().animate({width: panelDefaultWidth}, 500);
    $('.col-main').fadeIn({queue: true, duration: 500});
    if ($('#config-wrapper').css('display') === 'none')
      $('#config-wrapper').css('display', '');
    enlargeButton.removeClass('expanded').addClass('contracted');
  } else {

    // If the other panel is already expanded, contract it back
    if ($('button.expanded').length > 0) {
      $('button.expanded').first().trigger('click');
    }

    var difference = enlargeButton.parent().hasClass('col-left') ? rightSide : leftSide;
    panelContent
      .css('display', 'none')
      .css('z-index', 2)
      .fadeIn(500)
    ;
    animateWidth(enlargeButton.parent(), distance - difference);
    $('.col-main').fadeOut(500);
    enlargeButton.parent().find('.sticker').css('z-index', 1).fadeOut(500);
    enlargeButton.removeClass('contracted').addClass('expanded');
  }
  setTimeout(function(){
    panelContent.css('min-width', 'auto');
  }, 500);

  $('#expanded_checkout_btn').prop('disabled', false);
  $('#expanded_checkout_btn').removeClass('disabled');
  $('#expanded_checkout_btn').removeClass('incomplete');
  var epdHeight = jQuery(window).height() - jQuery('.expanded-shoppingcart-totals').height() - 140;
  jQuery('.expanded-package-details').css({'height' : epdHeight});
}

function registerImeiTopMenuPackage(orderNumber, orderId, customerId) {
  var myFunc = 'fillPackageImei';
  var myParams = [];
  appendDataToModal(myFunc, myParams, orderNumber, orderId, customerId);
}

function registerImeiTopMenuPackageWithoutModal(orderNumber, orderId, customerId) {
  var myFunc = 'fillPackageImei';
  var myParams = [];
  continueEditOrCancel(myFunc, myParams, orderId, orderNumber, customerId);
}

function continueEditOrCancel(myFunc, myParams, orderId, orderNumber, customerId) {
  var $ = jQuery;
  // Force exit order edit mode
  $.ajax({
    async: false,
    type: 'get',
    url: MAIN_URL + 'checkout/cart/cancelEditOrder',
    data: {
      redirect: 0
    }
  }).done(function() {
    // If the logged in customer is not the same as the customer to which the order belongs, we need to log him
    if (customerId != customer.getLoggedInCustomer()) {
      jQuery.ajax({
        url: '/customerde/details/retrieveCustomerJson',
        type: 'post',
        data: {
          parentAccountNumberId: customerId,
          id: customerId,
          isProspect: typeof isProspect !== 'undefined' ? isProspect : 0,
          cartWasCleared: typeof cartWasCleared !== 'undefined' ? cartWasCleared : false,
          mode: 1,
          doAjaxUpdate: typeof doAjaxUpdate !== 'undefined' ? doAjaxUpdate : false
        },
        loader: true
      }).done(function(){
        loadTopMenuOrder.call(this, $, orderNumber, orderId, myFunc, myParams);
      });
    } else {
      loadTopMenuOrder.call(this, $, orderNumber, orderId, myFunc, myParams);
    }
  });
}

var openOrdersMenu = {};
function registerOpenOrdersMenu(bucket) {
  openOrdersMenu.active = jQuery('#header div.dropdown #open-orders-menu').hasClass('menu_active');
  openOrdersMenu.bucket = bucket;
}

function expandedFormSubmit() {
  var $ = jQuery;
  var form = $('#expanded_form');
  var packageNotes = $('.expanded-package-details').find('.package-item.note-placeholder .manual-notes .save-package-note');
  var proceedButton = $('#expanded_checkout_btn');
  proceedButton.addClass('loading');
  window.cartForm = new VarienForm('expanded_form', true);
  if (!window.cartForm.validator.validate()) {
    proceedButton.removeClass('loading');
    return false;
  }
  $.each(packageNotes, function (id, note) {
    if ($(note).val()) {
      $.ajax({
        type: "POST",
        url: form.attr( 'action' ),
        data: form.serialize(),
        success: function() {
          window.location.href = MAIN_URL + 'checkout/cart';
        }
      });
      return false;
    }
  });
  $('#expanded_form').submit();
}

function getPackageHardware(obj) {
  var $ = jQuery;
  // TODO: cache mechanism?!
  var $container = $(obj).parents('#edit_delivered_package');
  var selectedValue = $(obj).find(':selected').val();
  if (selectedValue === 'ONTEVREDEN' || $('#current_store_is_indirect').val() == 1) {
    $container.find('#package_hardware_items_row').addClass('hidden');
    $container.find('#package_hardware_items').empty();
  }
  setPageOverrideLoading(true);
  $container.find('.loader').toggleClass('hide');

  var packageId = $container.data('packageId');

  $.post(MAIN_URL + 'checkout/cart/getPackageHardware', {'package_id': packageId}, function (results) {
    if (!results.error) {
      $('#edit_delivered_package .modal-footer button.pull-right').removeAttr('disabled');
      if (selectedValue !== 'ONTEVREDEN' &&  $('#current_store_is_indirect').val() != 1) {
        // Set hardware rows
        var html = '';
        if (results.data.hardwareitems && Object.keys(results.data.hardwareitems).length > 0) {

          html += '<label class="control-label">' + Translator.translate('Are there products dead on arrival (D.O.A.)?') + '</label>';
          if (results.notice.length > 0) {
            $container.find('.notice-hardware').removeClass('hidden');
          }
          $.each(results.data.hardwareitems, function (sku, name) {
            var classGrey = '';
            var deleted = '';

            if ($.inArray(sku, results.notice) > -1) {
              classGrey = 'grey';
              deleted = 'deleted';
            }
            html += '<select class="form-control" name="item_doa[]">';
            html += ('<option value=""></option>');
            html += ('<option value="{1}">{0}</option>').format(name, sku);
            html += '</select>';
          });
        } else {
          html = '<label class="control-label">'+ Translator.translate('No hardware items found in the package')+ '</label>';
        }
        $container.find('#package_hardware_items_row').removeClass('hidden');
        $container.find('#package_hardware_items').html(html);
      }
      // Set return channel
      $container.find('input[name="return_channel"]').val(results.data.return_channel);
    } else {
      $container.find('#package_hardware_items_row').removeClass('hidden');
      $container.find('#package_hardware_items').html(message);
    }
    $container.find('.loader').toggleClass('hide');
    jQuery('[name="item_doa[]"]').initSelectElement();
  });
  setPageOverrideLoading(false);
}

function showInitOrderModal(dom) {
  var item = jQuery(dom);
  var modalDom = jQuery('#customer_init_order');
  var submitBtn = modalDom.find('.modal-dialog .modal-content .modal-footer .btn-primary');
  submitBtn.data('customer', item.data('customer'));
  submitBtn.data('order', item.data('order'));
  modalDom.modal();
  modalDom.css('z-index', '9999');
}

function checkPackage(button, orderNumber) {
  var $ = jQuery;
  if (!orderNumber) {
    return;
  }
  button = $(button);
  button.addClass('hide');
  button.parent().find('.order-details-spinner').removeClass('hide');
  toggleOrderDetails(button.parents('tr'), true);
  var oldLoader = window.manualLoader;
  window.manualLoader = true;
  $.ajax({
    async: true,
    type: 'POST',
    url: MAIN_URL + 'superorder/index/checkPackage',
    data: {orderNumber: orderNumber},
    success: function (response) {
      button.parent().find('.order-details-spinner').addClass('hide');
      button.removeClass('hide');
      if (!response.error) {
        $.each(response.html, function (id, el) {
          $('tr[data-id="' + orderNumber + '.' + id + '"] .order-edit-buttons').html(el);
        });
      } else {
        showModalError(response.message);
      }
    },
    error: function () {
      button.removeClass('hide');
      button.parent().find('.order-details-spinner').addClass('hide');
    }
  });
  window.manualLoader = oldLoader;
}

function editTopMenuPackageWithoutModal(orderNumber, orderId, packageId, isDelivered, customerId) {
  var myFunc = 'editPackage';
  var myParams = [packageId, isDelivered];

  continueEditOrCancel(myFunc, myParams, orderId, orderNumber, customerId);
}

function cancelTopMenuPackageSwitch(type, packageId) {
  var myFunc = '';
  var myParams = [];
  switch (type) {
    case '1':
      myFunc = 'cancelOneOffPackages';
      break;
    case '2':
      myFunc = 'cancelDeliveredPackage';
      myParams = [packageId];
      break;
    case '3':
      myFunc = 'cancelPackage';
      myParams = [packageId];
      break;
  }
  return [myFunc, myParams];
}

function cancelDeliveredPackage(packageId,orderId) {
  // Because we share the same modal with Edit action, hide the edit text
  jQuery('#edit_delivered_package .modal-body #package_hardware_items_row').addClass('hidden');
  jQuery('#edit_delivered_package .modal-body #package_hardware_items').empty();
  jQuery('#edit_delivered_package .modal-body .change-text').hide();
  jQuery('#refund_reason_cancel').attr('name', 'refund_reason').removeClass('hidden');
  jQuery('#refund_reason_change').attr('name', '').addClass('hidden');
  jQuery('#edit_delivered_package').data('isCancel', 1).data('packageId', packageId).data('orderId', orderId).appendTo('body').modal();
  jQuery('#refund_reason_cancel').trigger('change');
}

function beforeEditSuperorderPackage(obj) {
  var $ = jQuery;
  setPageLoadingState(true);
  var $container = $(obj).parents('#edit_delivered_package');
  var packageId = $container.data('packageId');
  var orderId = $container.data('orderId');
  var isCancel = $container.data('isCancel');
  var redirect = $container.data('redirect');
  var checkedItems = $('[name="item_doa[]"].deleted:checked').length > 0;
  if (packageId) {
    var formData = $.extend($container.children('form').serializeObject(), {'package_id': packageId});
    $.post(MAIN_URL + 'checkout/cart/beforeEditSuperorderPackage', formData, function (data) {
      var controllerAction = isCancel ? 'removePackage?' : 'editSuperorderPackage?';
      controllerAction = (checkedItems && !isCancel) ? 'editSuperorderPackage?force=1&' : controllerAction;
      jQuery.get(MAIN_URL + 'checkout/cart/' + controllerAction + 'packageId=' + packageId, {}, true)
        .done(function(data) {
          jQuery('.modal.in').modal('hide');
          setPageLoadingState(false);
          if (isCancel && redirect) {
            return window.location.reload();
          }
          if (data.redirect) {
            window.location.href = data.redirect;
          } else {
            // reload package contents
            if (orderId !== undefined) {
              loadOrderDetails({number:orderId});
            }
            if (data.orderId !== undefined) {
              loadOrderDetails({number:data.orderId});
            }
          }
        });
    });
  }
}

function cancelTopMenuPackageWithoutModal(orderNumber, orderId, packageId, type, customerId) {
  var res = [];
  res = cancelTopMenuPackageSwitch(type, packageId);

  var modal = jQuery('#edit_delivered_package');
  modal.data('redirect', true);

  continueEditOrCancel(res[0], res[1], orderId, orderNumber, customerId);
}

function cancelTopMenuPackage(orderNumber, orderId, packageId, type, customerId) {
  var res = [];
  res = cancelTopMenuPackageSwitch(type, packageId);
  appendDataToModal(res[0], res[1], orderNumber, orderId, customerId);
}

function editTopMenuPackage(orderNumber, orderId, packageId, isDelivered, customerId) {
  var myFunc = 'editPackage';
  var myParams = [packageId, isDelivered];
  appendDataToModal(myFunc, myParams, orderNumber, orderId, customerId);
}

// Append the right method to the modal and execute the command
function appendDataToModal(myFunc, myParams, orderNumber, orderId, customerId) {
  var modal = jQuery('#top_menu_lock_modal').first();
  modal.data('function', myFunc);
  modal.data('params', myParams);
  modal.data('orderId', orderId);
  modal.data('orderNumber', orderNumber);
  modal.data('customerId', customerId);
  modal.appendTo('body').modal();
}

function loadTopMenuOrder($, orderNumber, orderId, myFunc, myParams) {
  // enter order edit mode:
  $ = $ || jQuery;
  $.ajax({
    async: true,
    type: 'get',
    url: MAIN_URL + 'checkout/cart/editSuperorder',
    data: {superorderId: orderNumber, redirect: 0},
    success: function (response) {
      // Once confirmed, lock the order:
      $.ajax({
        async: true,
        type: 'post',
        url: MAIN_URL + 'checkout/cart/lockSuperorder',
        data: {state: "unlocked", orderId: orderId},
        success: function (response) {
          if (response.error) {
            $.ajax({
              async: true,
              type: 'get',
              url: MAIN_URL + 'checkout/cart/cancelEditOrder',
              data: {redirect: 0}
            });
            showModalError(response.message);
          } else {
            // redirect to the correct url:
            window[myFunc].apply(this, myParams);
          }
        },
        error: function () {
        }
      });
    },
    error: function () {
    }
  });
}

function cancelPackage(packageId, oneOff, orderId) {
  if (customer) {
    customer.setLoading();
  }
  if (oneOff) {
    packageId = jQuery('#all-package-ids').val();
  }
  if (oneOff) {
    jQuery('#remove-one-of-deal-order-modal').modal('hide');
  }
  window.location.href = MAIN_URL + 'checkout/cart/removePackage?packageId=' + packageId;
}


function stopCancelPackage(orderId) {
  if (customer) {
    customer.setLoading();
  }
  jQuery.get(MAIN_URL + 'checkout/cart/stopRemovePackage', {}, true)
    .done(function(data) {
      jQuery('#stop_cancel_package_modal').modal('hide');
      // reload package contents
      customer.setLoading(false);
      if (orderId !== undefined) {
        loadOrderDetails({number:orderId});
      }
    });
}



function proceedToCheckout() {
  var simCardOnly = simCardChecker.isSimOnlyWithoutSim();
  if (!simCardOnly)
    return false;
  canProceedChangeOrder();
  return false;
}

function canProceedChangeOrder() {
  var $ = jQuery;
  var proceedButton = $('#configurator_checkout_btn');
  proceedButton.addClass('loading');
  $.post(MAIN_URL + 'configurator/init/expandCart', {'checkout': window['onCheckout']}, function(response) {
    $.get(MAIN_URL + 'configurator/cart/getConfiguratorWarnings', {
      'cartProductsHash': response.cartProducts
    }, true)
      .done(function(data) {
        proceedButton.removeClass('loading');
        var warnings = [];
        $.each(data.warnings, function(packageId, packageWarnings) {
          if (typeof packageWarnings === 'string') {
            packageWarnings = packageWarnings.split();
          }
          $.each(packageWarnings, function(type, warning) {
            warnings.push(warning);
          });
        });
        warnings = warnings ? warnings.join('<br>') : '';
        if (warnings !== '') {
          showModalError(warnings, 'Warning');
          highlightErrors(data.warnings);
        } else {
          window.location.href = MAIN_URL + 'checkout/cart/saveEditedQuote';
        }
      });
  });
}

function fillPackageImei() {
  setPageLoadingState(true);
  window.location.href = MAIN_URL + 'checkout/cart/registerImei';
}

function logErrors(jqXHR, xhr) {
  jQuery.ajax({
    url: 'validatebasket/Validate/logError',
    type: "post",
    data: ({
      'data': {"code": xhr.status, "message": xhr.statusText}
    }),
    dataType: "json",
    cache: false,
    success: function (response) {
    }
  });
  return true;
}


function activateBlock(element) {
  const $package = jQuery(element).closest('.block-container');
  var currentPackageId =  $package.parent().attr('data-package-id');
  var filters =  $package.parent().attr('data-filter-attributes') ? $package.parent().attr('data-filter-attributes') : jQuery(element).parent().attr('data-filter-attributes');
  var notActive = !$package.parents('.side-cart.block').hasClass('active');
  var packageCreationTypeId = $package.parent().attr('data-package-creation-type-id') ? $package.parent().attr('data-package-creation-type-id') : jQuery(element).parent().attr('data-package-creation-type-id');
  var packageType = $package.parents('.side-cart').attr('data-package-type') ? $package.parents('.side-cart').attr('data-package-type') : jQuery(element).parents('.side-cart').attr('data-package-type');
  var packageId = $package.parents('.side-cart').attr('data-package-id') ? $package.parents('.side-cart').attr('data-package-id') : jQuery(element).parents('.side-cart').attr('data-package-id');
  var saleType = $package.parents('.side-cart').attr('data-sale-type') ? $package.parents('.side-cart').attr('data-sale-type') : jQuery(element).parents('.side-cart').attr('data-sale-type');
  if(notActive) {
    if(controller != 'cart') {
      jQuery('div[data-package-type]:visible:not([data-package-id])').remove();
      jQuery('div[data-package-type]:visible').removeClass('active');
      jQuery('.promo-rules-trigger').addClass('hidden');
      $package.parents('.side-cart').addClass('active');
      initConfigurator(packageType, packageId, saleType, null, null, null, null, null, null, null, null, null, filters, packageCreationTypeId);
    } else {
      var packageId = $package.parents('.side-cart').data('package-id');
      window.location.href = MAIN_URL + '?packageId=' + packageId;
    }
  }
}

function togglePackageConfigurator(element, activate)
{
  if(activate){
    jQuery('div[data-package-type]:visible:not([data-package-id])').remove();
    jQuery('div[data-package-type]:visible').removeClass('active');
    jQuery('.promo-rules-trigger').addClass('hidden');
    jQuery(element).parents('.side-cart').addClass('active');
  }
  else{
    jQuery(element).parents('.side-cart').removeClass('active');
  }
}

function switchPackageMenu(event, self) {
  event.stopPropagation();
  self = jQuery(self);
  if(!self.hasClass('open')) {
    jQuery('.col-right .block-container').find('div').removeClass('open');
    jQuery('.checkout-package-details .package-expand').find('div').removeClass('open');
  }
  self.toggleClass('open');
  self.siblings('.menu-expand').first().toggleClass('open');
  if(!self.siblings('.menu-expand').first().hasClass('top-menu')){
    jQuery('.menu-expand.top-menu').removeClass('open');
    jQuery('.drop-menu.top-menu').removeClass('open');
  }
}

function expandPackageMenu(event, self) {
  event.stopPropagation();
  const contextMenu = jQuery(self);
  contextMenu.toggleClass('expanded');

  if (contextMenu.hasClass('expanded')) {
    const closeContextMenu = function() {
      contextMenu.removeClass('expanded');
      window.removeEventListener('click', function() {closeContextMenu(contextMenu)});
    };
    window.addEventListener('click', function() {closeContextMenu(contextMenu)});
  }
}

/**
 * updatePageDialogSwitcher
 * @param value
 */
function updatePageDialogSwitcher(value) {
  jQuery.ajax({
    async: false,
    type: 'POST',
    url: MAIN_URL + 'checkout/cart/leavePageDialogSwitcher',
    data: {locked: value},
    success: function () {
    },
    error: function () {
      console.log('Failed to retrieve the data');
    }
  });
}
/**
 * Show a Notic with specific content
 * @param {string|html} body
 * @param {string|html} title // optional
 * @param Notic Diplay
 */
function showNoticeBarError(body, title, noticPlace) {
  var $  = jQuery;
  title = Translator.translate(title);

  if($.trim(body) == '') {
    body = Translator.translate('Unknown error or no response received');
  } else if($.trim(body) == 'NULL') {
    body = "";
  }

  title = title || Translator.translate('Error');

  if (title !== 'NULL') {
    title = Translator.translate(title);
    title = title || Translator.translate('Error');
    var notic = $('<div class="notification-banner error">' +
      '<div class="notification-banner-text">' +
      '<div class="notification-banner-title">' + title + '&nbsp;</div>' +
      '<div class="notification-banner-description">' + body + '</div>' +
      '</div>' +
      '</div>');
  } else {
    var notic = $('<div class="notification-banner error">' +
      '<div class="notification-banner-text">' +
      '<div class="notification-banner-description">' + body + '</div>' +
      '</div>' +
      '</div>');
  }
  $('#' + noticPlace).html(notic);
  $('#' + noticPlace).removeClass('hidden');

  $(noticPlace).html(notic);
  $(noticPlace).removeClass('hidden');
}

/**
 * Hide a Notic with specific content
 * @param Notic Diplay
 */
function hideNoticeBarError(noticPlace) {
  var $  = jQuery;
  $('#' + noticPlace).addClass('hidden');
  $(noticPlace).addClass('hidden');
}

function createRenewAll(customerNumber,nonRetainableArgs) {
  if (jQuery('#is_order_edit_mode').val() == 1) {
    showModalError(Translator.translate('You can not renew an order because you are changing and order. You have to cancel your changes first and go to the configurator.'));
    return;
  }

  var msg = jQuery('#no_retainable_msg');
  msg.hide();
  if (typeof nonRetainableArgs === "undefined") {
    var nonRetainable = {};
    jQuery('.ctn_record').not('.retainable-ctn').each(function(i, _el) {
      jQuery.extend(nonRetainable, {0: jQuery(_el).data('rawctn')});
    });
  } else {
    var nonRetainable = nonRetainableArgs;
  }

  // Handle situation when it is called from order edit page
  if (typeof initConfigurator === 'undefined' || typeof createCartPackageLine === 'undefined') {
    jQuery('#spinner').toggle('show', function() {
      Mage.Cookies.set('callFunc', JSON.stringify({"fn": "createRenewAll", "args": [nonRetainable]}));
      window.location.replace('/');
    });
    return;
  }

  jQuery.post(MAIN_URL + 'customerde/details/renewAll', { nonRetainable: jQuery.makeArray(nonRetainable), customernumber: customerNumber }, function(data) {
    if (data.hasOwnProperty('error')) {
      msg.html(data.message).show();
      return;
    }
    jQuery('.col-right .sticker').replaceWith(data.rightSidebar);
    jQuery('#close_customer_details').trigger('click');
    jQuery('.buttons-container #package-types').addClass('hide');
    jQuery('#show-packages').addClass('hide');

    var _ctn;
    var _packageId;
    var _packageCreationTypeId;
    jQuery.each(data.packageIds, function(ctn, packageData) {
      _ctn = ctn;
      _packageCreationTypeId = packageData.packageCreationTypeId;
    });

    initConfigurator(data.type, _packageId,  data.saleType, _ctn, null, true, null, null, null, null, null, null, null, _packageCreationTypeId, 1);
  });

  jQuery('#create-one-of-deal-modal').modal('hide');
}

/* Closing Drawer when clicked modal backdrop and Cross */
jQuery(document).ready(function() {
  jQuery('.col-left .search-result-cross').click(function(){
    jQuery('.col-left .reduce').show();
  });

  window.addResizeEvent(function () {
    assurePromosContainerHeight();
  });

  jQuery('#fadded-bg').click(function(){
    colapsePackagePromosContainer();
  });

  jQuery('body').click(function(e){
    if (jQuery(this).hasClass('block-click')) {
      e.stopPropagation();
      e.preventDefault();
      return false;
    }
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if (jQuery(e.target).closest('.modal').length) {
      closeSaveCartModal();
    }
  });

  jQuery(document).keyup(function(e) {
    if(e.which===27){ closeSaveCartModal(); } // 27 is the keycode for the Escape key
  });
});

function assurePromosContainerHeight() {
  if (jQuery('#promo-rules-container .content').length) {
    var promosHeight = jQuery(window).height() -
      jQuery('#header').height()/*page header height*/ -
      (jQuery('.promos-container-header').height() + 20)/*promos container header +  margin bottom*/ -
      jQuery('button.colapse-promos-button').outerHeight()/*close btn height including borders*/ -
      20 * 2/*spaces for close btn*/;
    if (jQuery('#promo-rules-container .notification-banner.warning').length) {
      promosHeight = promosHeight - jQuery('#promo-rules-container .notification-banner.warning').outerHeight();
    }
    jQuery('#promo-rules-container .content').css('height', promosHeight+'px');
  }
}

/**
 * Load order details
 * @param item
 * @param callback
 */

function loadOrderDetails(item, callback) {
  setPageLoadingState(true);
  //jQuery('.modal.in').modal('hide'); // hide all currently visible modals
  if( item === undefined ) {
    item = {number:'undefined'};
  }
  let orderUrl = MAIN_URL + 'checkout/cart/editSuperorder?superorderId=' + item.number + '&_=' + new Date().getTime();
  jQuery.ajax({
    cache: false,
    type: 'POST',
    url: orderUrl,
    success: function (response) {
      window.eventEmitter.emit('ORDER_DETAILS_UNMOUNT');
      let container = jQuery("#customer-search-results");
      container.html(response);
      initFormElements(container);
      window.eventEmitter.emit('ORDER_STATUS_RENDER');
      // Make order menu disabled in case there are no options inside
      if( jQuery("#customer-search-results .more-actions .actions-hover-menu li").length === 0 ) {
        jQuery("#customer-search-results .more-actions").addClass('disabled');
      }
      // Hide package-level menu in case there are no options inside
      jQuery("#customer-search-results .context-menu").each(function( index ) {
        if (jQuery('li', this).length === 0) {
          jQuery(this).hide();
        }
      });
      LeftSidebar.toggleLeftSideBar(true);
      jQuery('#order_info_left').parent().addClass('active');
      if (typeof callback === 'function') {
        callback();
      }
    },
    error: function () {
      console.log('Failed to retrieve the data');
    }
  }).always(function () {
    setPageLoadingState(false);
  });
}

function loadChangedOrderDetails() {
  let orderUrl = MAIN_URL + 'checkout/cart/preview';
  jQuery.ajax({
    cache: false,
    type: 'POST',
    url: orderUrl,
    success: function (response) {
      jQuery('.custom-backdrop').remove();
      let container = jQuery("#customer-search-results");
      container.html(response);
      initFormElements(container);
      jQuery(".process-context-indication").tooltip({ selector: '[data-toggle=tooltip]' });
      var defaultMinDateDatepicker = 4;
      var defaultMaxDateDatepicker = 6;
      jQuery('.return-date').datepicker({
          format: "dd-mm-yyyy",
          showOn: 'button',
          buttonText: 'date',
          minDate: "+" + defaultMinDateDatepicker + "W",
          maxDate: "+" + defaultMaxDateDatepicker + "M"
      });
    },
    error: function () {
      console.log('Failed to retrieve the data');
    }
  });
}

function viewOrderOverview(dom) {
  setPageLoadingState(true);
  var valueSource = jQuery(dom);
  var order_id = valueSource.data('order');
  var customerId = valueSource.data('customer');

  var callback = function(data) {
    customer.loadRetrievedCustomerData(customerId, false, false, false, false, false, order_id);
  }

  jQuery.ajax({
    url: '/customerde/details/retrieveCustomerJson',
    type: 'post',
    data: {
      parentAccountNumberId: customerId,
      id: customerId,
      isProspect: typeof isProspect !== 'undefined' ? isProspect : 0,
      cartWasCleared: typeof cartWasCleared !== 'undefined' ? cartWasCleared : false,
      mode: 1,
      doAjaxUpdate: typeof doAjaxUpdate !== 'undefined' ? doAjaxUpdate : false
    },
    loader: true
  })
    .done(callback)
    .fail(function(err, status, xhr) {
      if (xhr && xhr.status != 0) {
        showModalError(xhr.message);
      }
    });
}

/**
 * Load cart details
 * @param cartId
 * @param cartType
 * @param createdDate
 */
function showDetailsCartModal(cartId,cartType,createdDate) {
  jQuery.ajax(MAIN_URL + 'configurator/init/showDetailsCart', {
    type: 'POST',
    data: {cart_id: cartId,cart_type: cartType, created_date: createdDate},
    success: function (response) {
      jQuery("#customer-search-results").html(response.html);
      jQuery('.vn-popup').removeClass('visible');
    },
    error: function () {
      console.log('Failed to retrieve the data');
    }
  });
}

/**
 * Re-trigger shopping cart Email
 * @param cartId
 */
function showCartEmailButton(cartId) {
  jQuery.ajax(MAIN_URL + 'checkout/cart/emailQuote', {
    type: 'POST',
    data: {cartId: cartId},
    success: function (response) {
      if (response.error) {
        showValidateNotificationBar('<strong>'+Translator.translate('Email not sent')+'!</strong> ' + response.message, 'error', response.button);
      } else {
        showValidateNotificationBar('<strong>'+Translator.translate('Email sent')+'!</strong> ' + response.message, 'success');
      }
    },
    error: function () {
      console.log('Failed to retrieve the data');
    }
  });
}

function showCancelPackageModal(event, element) {
  event.stopPropagation();
  var sideBlock = jQuery(element).parents('.side-cart');
  var blockPackageId = sideBlock.data('package-id') ? sideBlock.data('package-id') : jQuery(element).data('package-id');
  var modalType = jQuery(element).data('modal-type');
  var bundleId = jQuery(element).data('bundle-id');
  var bundleDummyPackage = jQuery(element).data('bundle-dummy-package-name');
  var packageTitle = jQuery(element).data('package-title');
  var message = jQuery(element).data('modal-message');

  if (modalType === 'alert-gigakombi-delete-modal' || modalType === 'break-suso-after-deselecting-modal') {
    jQuery('.btn-remove-single-package').attr('data-pack-id-for-removal', blockPackageId);
    applyDeletePackageMessages(bundleId, bundleDummyPackage, packageTitle, message);
    jQuery('#cancel-single-package-modal').modal();
  } else if (modalType === 'alert-redplus-group-delete') {
    jQuery('.btn-remove-single-package').attr('data-pack-id-for-removal', blockPackageId);
    jQuery('#cancel-single-package-with-redplus-modal').modal();
  } else {
    // Check if this package is a lowentry parent
    var isLowentryParent = false;
    if (jQuery(element).closest('.side-cart').find('.hint').length) {
      isLowentryParent = true;
    }

    if (isLowentryParent) {
      jQuery('#remove-lowentry-products').attr('data-pack-id-for-removal', blockPackageId);
      jQuery('#cancel-lowentry-bundle-modal').modal();
    } else {
      jQuery('.btn-remove-single-package').attr('data-pack-id-for-removal', blockPackageId);
      applyDeletePackageMessages(bundleId);
      jQuery('#cancel-single-package-modal').modal();
    }
  }
}

function removeOverviewPackage(event, element) {
  var $ = jQuery;
  var id = $(element).parent().attr('pkg-id');
  var element = $('.orderline-advanced[data-package-id="' + id + '"]')[0];
  jQuery(element).attr('data-pack-id-for-removal', id);

  window.removePackage(event, element, true);
}

function manualActivateNew(isRetail, needSim, element) {
  var $ = jQuery;
  var packageId = $(element).parent().attr('pkg-id');
  var note = $(element).attr("note");
  var sim = $(element).attr("sim");
  var connectManuallyModal = $('#connectManuallyModal');
  connectManuallyModal.find('.connect-manually-note').removeClass('hidden');
  connectManuallyModal.find('.connect-manually-note').addClass('hidden');
  connectManuallyModal.find('select').attr("id", 'connect_manually_reason_'+packageId +'');
  connectManuallyModal.find('select').attr("name", 'connect_manually_reason_'+packageId +'');
  //connectManuallyModal.find('select option[value='+reason+']').attr('selected', 'selected');
  connectManuallyModal.find('textarea').attr("id", 'connect_manually_note_'+packageId +'');
  connectManuallyModal.find('textarea').attr("name", 'connect_manually_note_'+packageId +'');

  if (note != '') {
    connectManuallyModal.find('textarea').text(note);
  }
  if(isRetail == true && needSim == true)
  {
    connectManuallyModal.find('input').attr("id", 'connect_manually_simcard_'+packageId +'');
    connectManuallyModal.find('input').attr("value", sim);
    connectManuallyModal.find('input').parent().parent().parent().removeClass('hidden');
    connectManuallyModal.find('input').attr("name", 'connect_manually_simcard_'+packageId +'');
  }
  connectManuallyModal.find('select').on('change', function () {
    var el = $j(this);
    var hasRemark = el.find('option:selected').data('remark');
    if (hasRemark) {
      connectManuallyModal.find('.connect-manually-note').removeClass('hidden');
    } else {
      connectManuallyModal.find('.connect-manually-note').addClass('hidden');
      connectManuallyModal.find('textarea').val('');
      connectManuallyModal.find('.character-left').text('150');
    }
  });


  connectManuallyModal.find('.do-not-btn').attr("id", packageId);
  connectManuallyModal.find('.save-manual-btn').attr("id", packageId);
  connectManuallyModal.modal('show');
  $j('.input-wrapper').initInputElement();
  $('.select-wrapper').initSelectElement();

  /*var block = $('.manual_activation_remarks_div_' + packageId);
  var simcardInput = block.find('[id*="manual_activation_simcard"]').first();
  if (block.is(":visible")) {
      $('[id="manual_activation_reason[' + packageId + ']"]').val('');
      block.toggleClass('hidden');
      // Disable the validations when the textbox is hidden
      block.find('select').first().removeClass('validate-select');
      simcardInput.removeClass('required-entry validate-sim-cart');
      block.find('.validation-advice').remove();
      block.find('.validation-failed').removeClass('validation-failed');
      block.find('textarea').first().removeClass('required-entry');
  } else {
      block.toggleClass('hidden');
      block.find('select').first().addClass('validate-select');
      simcardInput.addClass('validate-sim-cart');
      if (isRetail) {
          simcardInput.addClass('required-entry');
      }
  }*/
}

function updateConnectManuallyDet(element) {
  var packageId = $j(element).attr('id');
  if($j(element).hasClass("do-not-btn"))
  {
    $j("#connectManuallyModal").modal('hide');
    $j('.connect-manualy-'+packageId).addClass('hidden');
    $j('#manual_activation_reason_'+packageId +'').val("");
    $j('#manual_activation_simcard_'+packageId +'').val("");
    $j('#manual_activation_remarks_'+packageId +'').val("");
    $j('#simerrormsg').html("");
    $j('#simerrormsg').parent().removeClass("error");
    $j('#reasonerrormsg').html("");
    $j('#reasonerrormsg').parent().removeClass("error");
    $j('#manual_activate_'+packageId+'').removeClass('hidden');
  }
  else
  {
    var reasonelement = $j('#addManualDetails').find('#connect_manually_reason_'+packageId +'');
    var simacardelement = $j('#addManualDetails').find('#connect_manually_simcard_'+packageId +'');
    var reason = reasonelement.val();
    var simcard = simacardelement.val();
    var remarks = $j('#addManualDetails').find('#connect_manually_note_'+packageId +'').val();
    var isSimcardRequired = $j('#addManualDetails').find('#connect_manually_simcard_'+packageId +'').parent().parent().parent().is(':visible');
    var error = 1;
    var reg = /^893[\d]{16}$/;
    if(isSimcardRequired && simcard && !reg.test(simcard)) {
      simacardelement.attr('data-original-title', Translator.translate('Please enter the sim card')).tooltip();
      simacardelement.parent().parent().addClass("error");
      error = 1;
    } else if(isSimcardRequired && !simcard) {
      simacardelement.attr('data-original-title', Translator.translate('Invalid simcard number')).tooltip();
      simacardelement.parent().parent().addClass("error");
      error = 1;
    } else {
      simacardelement.attr('data-original-title','');
      simacardelement.parent().parent().removeClass("error");
      error = 0;
    }

    if(reason == "") {
      reasonelement.attr('data-original-title', Translator.translate('Please select the reason')).tooltip();
      reasonelement.parent().parent().addClass("error");
      error = 1;
    } else {
      reasonelement.attr('data-original-title', '');
      reasonelement.parent().parent().removeClass("error");
    }

    if (error == 0) {
      $j('.connect-manualy-'+packageId).removeClass('hidden');
      $j('.not-connect-manualy-'+packageId).removeClass('hidden');
      $j('#manual_activation_reason_'+packageId +'').val(reason);
      $j('#manual_activation_simcard_'+packageId +'').val(simcard);
      $j('#manual_activation_remarks_'+packageId +'').val(remarks);

      $j('#manual_reason_'+packageId +'').html(reason);
      $j('#manual_simcard_'+packageId +'').html(simcard);
      $j('#manual_remarks_'+packageId +'').html(remarks);
      $j('#manual_activate_'+packageId+'').addClass('hidden');
      $j("#connectManuallyModal").modal('hide');
    }
    else
      $j('#connectManuallyModal').modal('show');
  }
}

function doNotConnnectManually(element) {
  $ = jQuery;
  var packageId = $(element).parent().attr('pkg-id');
  $('.connect-manualy-'+packageId).addClass('hidden');
  $('.not-connect-manualy-'+packageId).addClass('hidden');
  $('#manual_activate_'+packageId+'').removeClass('hidden');
  $('#manual_activation_reason_'+packageId +'').val("");
  $('#manual_activation_simcard_'+packageId +'').val("");
  $('#manual_activation_remarks_'+packageId +'').val("");
}

function showCancelCartModal() {
  var customerId = jQuery('#left-customer-info').data('id');
  if (customerId) {
    jQuery('#logout-customer-modal').modal();
  } else {
    jQuery('#cancel-cart-modal').modal();
  }
}

function showChangeCustomerTypeModal() {
  jQuery('#change-customer-type-modal').modal();
}

function expandPackagePromosContainer(event, elem) {
  event.stopPropagation();

  if (jQuery(elem).parent().hasClass('disabled')) {
    return;
  }

  var packageId = jQuery(elem).closest('.side-cart.block').data('package-id');

  jQuery(elem).closest('.menu-expand').removeClass('open');
  jQuery('#fadded-bg').fadeIn(300);

  jQuery.get(MAIN_URL + 'configurator/init/expandPackagePromosDrawer', {'packageId': packageId}, function(response, jqXHR, xhr) {
    jQuery(elem).closest('.sticker').fadeOut({
      queue: false,
      duration: 500,
      complete: function() {
        var promosDrawer = jQuery(this).siblings('.package-promos-container');
        jQuery(promosDrawer).html(response.html);

        jQuery('.enlarge').css('visibility', 'hidden');
        jQuery('.col-right.sidebar').addClass('promos-sidebar');

        jQuery(this).parent().animate({ width: "390px" }, 500);
        jQuery(promosDrawer).fadeIn(500);

        assurePromosContainerHeight();
      }
    });
  });
}

function colapsePackagePromosContainer() {
  jQuery('.package-promos-container').fadeOut({
    queue: false,
    duration: 500,
    complete: function() {
      var cartContainer = jQuery('.col-right.sidebar .sticker');
      jQuery(cartContainer).fadeIn(500);

      jQuery(this).parent().animate({ width: "300px" }, 500);
      jQuery('#promo-rules-container').remove();
      jQuery('.enlarge').css('visibility', 'visible');
      jQuery('.col-right.sidebar').removeClass('promos-sidebar');

      jQuery('#fadded-bg').fadeOut(300);
    }
  });
}

jQuery( document ).ready(function() {
  jQuery('#save-cart-modal').mouseover(function() {
    if(jQuery('#save-cart-modal').attr('aria-hidden') === "false" && jQuery('.select-selected').is(':visible')) {
      if (ToggleBulletSoho.getSohoState('.soho-toggle') == 1) {
        jQuery('#type option[value="1"]').attr('selected','selected');
        jQuery('#type option[value="0"]').attr('selected',false);
        jQuery('#type').next('.select-selected').text(jQuery('#type option[value="1"]').text());
      } else {
        jQuery('#type option[value="0"]').attr('selected','selected');
        jQuery('#type option[value="1"]').attr('selected',false);
        jQuery('#type').next('.select-selected').text(jQuery('#type option[value="0"]').text());
      }
    }
  });
});

/**
 * Support for 360 view / nba view iframed content
 * @param section
 * @return void
 */
function addTabFunctionUserInfo(section){
  var iframeBaseUrl = window[section + 'IframeUrl'];
  jQuery('.user-info-nav-buttons .btn, .nba-btn').on('click', function(){
    if (jQuery(this).hasClass('active')) {
      return false;
    }

    jQuery(this).parent().find('.active').removeClass('active');
    jQuery(this).addClass('active');
    var CTNNumber = jQuery(this).data('phone');

    if (section == 'nba') {
      jQuery('.connection-problem-state').addClass('hidden');
      var iframeUrl = iframeBaseUrl.replace('CustomerCTNValue', CTNNumber);
      jQuery('.nba-iframe').attr('src',iframeUrl).removeClass('hidden');
    } else {
      var iframeUrl = iframeBaseUrl + CTNNumber;
      jQuery('.user-info-iframe').attr('src',iframeUrl).removeClass('hidden');
    }
  });

  if (section != 'nba') {
    jQuery('.user-info-nav-buttons .btn').first().click();
  }
}

function showContextMenu(event, self) {
  event.stopPropagation();
  const elem = jQuery(self);
  elem.toggleClass('open');

  if (elem.hasClass('open')) {
    const hideContextMenu = function() {
      elem.removeClass('open');
      window.removeEventListener('click', function() {hideContextMenu(elem)});
    };
    window.addEventListener('click', function() {hideContextMenu(elem)});
  }
}

/**
 *
 * @param data
 * @param clone
 * @param callBack
 */
function callbackInit(data, clone, callBack){

  if (data.error) {
    var errorMessage = Translator.translate('Failed to initiate configurator');
    if(data.hasOwnProperty('message') && typeof(data.message) == 'string') {
      errorMessage += ". " + data.message;
    }
    showModalError(errorMessage);
  } else {
    data.products = window.products;
    data.filters = window.filters;

    var content = jQuery('#config-wrapper').find('.content');
    content.get(0).innerHTML = '';

    window.initCart = data.initCart;
    window.installedBaseCart = data.installedBaseCart;
    window.defaultedItems = data.defaultedItems;
    window.defaultedObligatedItems = data.defaultedObligatedItems;
    window.sim = data.sim;
    window.prices = data.prices;
    window.allowedProducts = data.allowedProducts;
    window.quoteHash = data.quoteHash;
    window.SALES_ID = data.salesId;
    window.processContextCode = data.processContextCode;
    window.specialPriceProducts = data.specialPriceProducts || {};
    window.simCardName = data.simCardName;

    if (data.ctnPackages) {
      // add package id to dummy ctn
      handleCTNPackage(data.ctnPackages, clone);
    }

    if (clone) {
      clone.attr('data-package-id', data.packageId);
    }

    // the javascript UI handler that manages content (uses configurator for that)
    window.configuratorUI = new ConfiguratorUI.Base();

    // use configurator only for configuration logic - setting and updating objects on it
    window.configurator = new Configurator.Base(data.config);
    // window property that stores combinations of bundle id - package id to show bundle hint only once
    window.configurator.activePackageEntityId = data.hasOwnProperty("packageEntityId") ? data.packageEntityId : null;
    // initializing only once to prevent over hinting when switching between packages
    window.bundleHintShown = window.bundleHintShown || [];
    // tell configurator when where prices last updated
    window.configurator.lastPrices = new Date().getTime();

    setUpUI(content, data.config.include_btw);

    configurator.packagePromoRules(data, data.packageId);

    var clear_filters = jQuery('#config-wrapper #subscription-block .clear-config-filters');
    if (clear_filters.length) {
      clear_filters.trigger('click');
    }

    $j.each(data.campaignProducts, function (index, product_id) {
      jQuery("#config-wrapper")
        .find(".item-row[data-id='" + product_id + "']")
        .find('span.item-label')
        .removeClass('hidden')
        .addClass('campaign').text(Translator.translate('CAMPAIGN'));
    });

    if (INIT_CONFIGURATOR_WARNING) {
      showModalError(INIT_CONFIGURATOR_WARNING);
      INIT_CONFIGURATOR_WARNING = "";
    }

    if (NOTIFICATION) {
      showNotificationBar(NOTIFICATION, 'warning');
      NOTIFICATION = null;
    }

    // mark tariffs and options in configurator which are from installed base
    // window.configuratorUI.markConfiguratorInstalledBaseProducts(Object.keys(data.installedBaseCart));
    window.configuratorUI.markConfiguratorInstalledBaseProducts(data.installedBaseCart);
    window.configuratorUI.hardwareDetailsforInstallBase(data.hardwareDetailsIls);
    if(data.processContextCode == 'MOVE' && window.customer.moveRemovedProducts != false){
      window.configuratorUI.showMoveRemovedProductsModal(window.customer.moveRemovedProducts);
      window.customer.moveRemovedProducts = false;
    }

    // mark disabled sections
    window.configuratorUI.markConfiguratorDisabledSections(data.config.disabledSections);

    if (data.hasOwnProperty("hintMessages") && Object.keys(data.hintMessages).length !== 0) {
      configuratorUI.showHintNotifications(data.hintMessages);
    }

    if (data.hasOwnProperty("infoMessage")) {
      configuratorUI.showInfoMessage(data.infoMessage);
    }

    if (typeof callBack == "function") {
      callBack();
      // decrease cart loading state
      configurator.decreaseQueueSize();
      // reset loading state if any
      window.setPageLoadingState && window.setPageLoadingState(false);
      window.setPageOverrideLoading(false);
    }
  }
}
