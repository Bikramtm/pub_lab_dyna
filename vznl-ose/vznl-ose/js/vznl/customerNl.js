'use strict';

(function ($) {
  window.CustomerDe.prototype = $.extend(VEngine.prototype, {
    validateExactAddress: function (data) {
      var elem = $(data);
      var self = this;
      var exactModal = $('#validateExactAddressMultipleResultsModal');
      var exactAddresses = exactModal.find('.ajax-results-addresses:first');

      var formData = {
        'street': elem.attr('data-street'),
        'housenumber': elem.attr('data-building-nr'),
        'postalcode': elem.attr('data-postalcode'),
        'city': elem.attr('data-city-name'),
        'housenumberaddition': elem.attr('data-house-addition')
      };

      // create the callback
      var callback = function (results) {
        $('#spinner').modal('hide');
        if (results.error || (!results.data.length)) {
          results.message = !results.data || !results.data.length ? results.message : Translator.translate('Invalid address');
          showModalError(results.message);
        } else {
          if (results.data) {
            if (results.data.length == 1) {
              elem = jQuery('<div/>', {
                'data-id': data.id,
                'data-street': results.data[0].attributes.street,
                'data-housenumber': results.data[0].attributes.houseNumber,
                'data-postalcode': results.data[0].attributes.postalCode,
                'data-city': results.data[0].attributes.city,
                'data-housenumberaddition': results.data[0].attributes.houseAdditionNumber
              });
              address.checkService(elem);
            } else {
              var template = Handlebars.compile($('#HT-validateExactAddressMultipleResults').html());
              var responseHTML = template(results);

              exactAddresses.html(responseHTML);
              exactModal.modal('show');
            }
          }
        }
      };

      // get data
      $.get(validateApiUrl + '?' + encodeURIComponent($.param(formData))).done(callback);
    },
    searchExactAddress: function (data) {
      address.checkService(data);
      $('#validateExactAddressMultipleResultsModal').modal('hide');
    },

    loadCustomerData: function(parentAccountNumberId, contactId, isProspect, afterLoadCallback, cartWasCleared, doAjaxUpdate) {
      var self = this;
      var callback = function(data) {
        if (data.customerData) {
          var leftDetailsSticker = $('#Handlebars-Template-Details-Customer'),
            leftDetailsStickerTemplate = (leftDetailsSticker.html()) ? leftDetailsSticker.html() : '';

          //validate address and load the serviceability content in the top bar
          var customerAddress = typeof data.addressData === 'object' ? data.addressData : {};
          var customerAddressArray = Object.keys(customerAddress).map(function (data) {
            return [data, customerAddress[data]];
          });

          for (var i = 0; i < customerAddressArray.length; i++) {
            switch (customerAddressArray[i]["0"]) {
              case "postcode":
                document.getElementById('topBar.addressCheck.postcode').value = customerAddressArray[i]["1"];
                break;
              case "street":
                document.getElementById('topBar.addressCheck.street').value = customerAddressArray[i]["1"];
                break;
              case "house_no":
                document.getElementById('topBar.addressCheck.houseNo').value = customerAddressArray[i]["1"];
                break;
              case "house_no_addition":
                document.getElementById('topBar.addressCheck.addition').value = customerAddressArray[i]["1"];
                break;
              case "city":
                document.getElementById('topBar.addressCheck.city').value = customerAddressArray[i]["1"];
                break;
            }
          }
          document.getElementById('topBar_addressCheck_isReloadPage').value = 1;
          $('.check-button').click();

          data.customerData.addressData = data.addressData;
          // Translating customer prefix
          data.customerData.prefix = Translator.translate(data.customerData.prefix);

          // Parse the json with customer results in the handlebars template
          var template = Handlebars.compile(leftDetailsStickerTemplate),
            customersHTML = template(data.customerData),
            leftCustomerInfoContainer = $('.col-left').find('.sticker');
          leftCustomerInfoContainer.html(customersHTML);

          // Disable check serviceability form until mode2 call has completed
          address.disableTopAddressCheckForm();

          if (data.saveCartModal) {
            $('#save-cart-modal').replaceWith(data.saveCartModal);
          }

          $.ajax({
            async: true,
            type: 'POST',
            data: {},
            url: '/customerde/details/poolCustomerData',
            success: function(data) {
              afterLoadCallback();
              setPageLoadingState(false);
              if (data.hasOwnProperty('serviceError')) {
                if (data.hasOwnProperty('code')) {
                  if (self.errorCodes.hasOwnProperty(data.code)) {
                    var pinModal = $('#customer_pin_modal');
                    pinModal.find('input[name="cart"]').val(true);
                    pinModal.modal();
                    return;
                  }
                }
                showModalError(data.message, data.serviceError);
                return;
              } else if (data.error) {
                showModalError(data.message);
                return;
              }
            },
            error: function() {
              // enable the continue button
              if(window.hasCompletePackage == true) {
                $('#cart_totals input').attr('disabled', false);
              }
              window.checkoutEnabled = true;
              console.log('Failed to update customer');
            }
          });
        } else if (data.error) {
          showModalError(data.message);
        }
      };

      $.ajax({
        url: window.customerDe.settings.endpoints['customer.load'],
        type: 'post',
        data: {
          contactId: contactId,
          parentAccountNumberId: parentAccountNumberId,
          id: parentAccountNumberId,
          isProspect: typeof isProspect !== 'undefined' ? isProspect : 0,
          cartWasCleared: typeof cartWasCleared !== 'undefined' ? cartWasCleared : false,
          mode: 1,
          doAjaxUpdate: typeof doAjaxUpdate !== 'undefined' ? doAjaxUpdate : false
        },
        loader: true
      })
      .done(callback)
      .fail(function(err, status, xhr) {
        if (xhr && xhr.status != 0) {
          showModalError(xhr.message);
        }
      });

      // Set the logged in customer as current
      self.setLoggedInCustomer(parentAccountNumberId);
    },

    loadRetrievedCustomerData: function(parentAccountNumberId, contactId, isProspect, afterLoadCallback, cartWasCleared, doAjaxUpdate, orderId) {
      var self = this;
      window.CustomerDe.redirectURL = "";
      var callback = function(data) {
        if (data.customerData) {
          //validate address and load the serviceability content in the top bar
          var customerAddress = typeof data.addressData === 'object' ? data.addressData : {};
          var customerAddressArray = Object.keys(customerAddress).map(function (data) {
            return [data, customerAddress[data]];
          });

          for (var i = 0; i < customerAddressArray.length; i++) {
            switch (customerAddressArray[i]["0"]) {
              case "postcode":
                document.getElementById('topBar.addressCheck.postcode').value = customerAddressArray[i]["1"];
                break;
              case "street":
                document.getElementById('topBar.addressCheck.street').value = customerAddressArray[i]["1"];
                document.getElementById('topBar.addressCheck.houseNo').value = customerAddressArray[i]["1"].match(/\d+/);
                break;
              case "house_no_addition":
                document.getElementById('topBar.addressCheck.addition').value = customerAddressArray[i]["1"];
                break;
              case "city":
                document.getElementById('topBar.addressCheck.city').value = customerAddressArray[i]["1"];
                break;
            }
          }
          document.getElementById('topBar_addressCheck_isReloadPage').value = '';
          $('.check-button').click();
          setTimeout(function() {
            if (!jQuery("#serviceabilityMultipleResultsModal").hasClass('in')) {
              window.location.href = MAIN_URL + '?orderId=' + orderId;
            } else {
              window.CustomerDe.redirectURL = MAIN_URL + '?orderId=' + orderId; 
            }
          }, 2000);

        } else if (data.error) {
          showModalError(data.message);
        }
      };

      $.ajax({
        url: window.customerDe.settings.endpoints['customer.load'],
        type: 'post',
        data: {
          contactId: contactId,
          parentAccountNumberId: parentAccountNumberId,
          id: parentAccountNumberId,
          isProspect: typeof isProspect !== 'undefined' ? isProspect : 0,
          cartWasCleared: typeof cartWasCleared !== 'undefined' ? cartWasCleared : false,
          mode: 1,
          doAjaxUpdate: typeof doAjaxUpdate !== 'undefined' ? doAjaxUpdate : false
        },
        loader: true
      })
        .done(callback)
        .fail(function(err, status, xhr) {
          if (xhr && xhr.status != 0) {
            showModalError(xhr.message);
          }
        });

      // Set the logged in customer as current
      self.setLoggedInCustomer(parentAccountNumberId);
    },


    /**
     * If there is already an address loaded
     * check if this existing address is equals to the customer address that needs to be loaded
     * @returns {boolean}
     */
    customerAddressIsLoadedAddress: function (data) {
      if (address == undefined || !address.hasOwnProperty('services') || !address.services || !address.services.hasOwnProperty('address_id')) {
        return false;
      }
      var self = this;
      var topAddressSearch = $(self.topAddressSearch);
      if (data.customerData.service_address !== undefined && data.customerData.service_address) {
        if (data.customerData.service_address.id && address.services.address_id == data.customerData.service_address.id) {
          return true;
        } else if (data.customerData.service_address.postal_code == topAddressSearch.find('input[name="postalcode"]').val() &&
          data.customerData.service_address.city == topAddressSearch.find('input[name="city"]').val() &&
          data.customerData.service_address.street == topAddressSearch.find('input[name="street"]').val() &&
          data.customerData.service_address.no == topAddressSearch.find('input[name="housenumber"]').val() &&
          data.customerData.service_address.house_addition == topAddressSearch.find('input[name="housenumberaddition"]').val()) {
          return true;
        }
      }
      else if (data.customerData.legal_address_id !== undefined) {
        if (data.customerData.legal_address_id && address.services.address_id == data.customerData.legal_address_id) {
          return true;
        }
        else if (data.customerData.postal_code == topAddressSearch.find('input[name="postalcode"]').val() &&
          data.customerData.city == topAddressSearch.find('input[name="city"]').val() &&
          data.customerData.street == topAddressSearch.find('input[name="street"]').val() &&
          data.customerData.no == topAddressSearch.find('input[name="housenumber"]').val() &&
          data.customerData.house_addition == topAddressSearch.find('input[name="housenumberaddition"]').val()) {
          return true;
        }
      }
      return false;
    },

    loadCustomerInfo: function (element) {
      if ( $(element).hasClass('disabled') ) {
        return;
      }
      var self = this;
      element = $(element);
      if (!element.parent().hasClass('active')) {
        var targetId = $('<div/>').html(element.attr('data-toggle')).text();
        self.targetPanelId = targetId;
        $.post(MAIN_URL + 'customerde/details/showCustomerPanel', {'section': targetId}, function (response) {
          if (response.error == true) {
            showModalError(response.message);
          } else {
            var customerSearch=true;
            if(response.customerSearch) {
              customerSearch=false;
            }

            // set the shopping cart data on a customer variable
            switch (targetId) {
            case 'carts-content':
              self.customerShoppingCartData = response;
              break;
            case 'products-content':
              self.customerProducts = response;
              break;
            }

            // Adding breadcrumbs
            self.breadcrumb.addClass('hidden');
            if (response.breadcrumb != undefined) {
              // Empty breadcrumbs value as first will be parent here, if we want to add child then we'll not empty existing breadcrumb value
              self.breadcrumbValue = [];
              self.breadcrumbValue.push({
                text: response.breadcrumb,
                link: element.attr('id'),
                type: 'click'
              });

              self.breadcrumb.removeClass('hidden');
              self.createBreadcrumb();
            }

            if ((targetId == 'products-content' || targetId == 'link-details' || targetId == 'update-details') && customerSearch) {
              if ($('#left-sidebar-' + targetId).length > 0) {
                var template = Handlebars.compile($('#left-sidebar-' + targetId).html()),
                  customersHTML = template(response);
                self.customerInfoContainer.html(customersHTML);
              }
              //display the billing info button if we have pospaid
              if ($('.products-order-number-kias-show').length > 0) {
                $.each($('.products-order-number-kias-show'), function (key, element) {
                  var customerNumber = jQuery(element).attr('data-customer-number');
                  jQuery('#products-order-number-kias-' + customerNumber).find('span').removeClass('hidden');
                  jQuery('#products-order-number-kias-' + customerNumber).attr('data-toggle', 'popover');
                });
              }
            } else {
              self.customerInfoContainer.html(response.customerPanel);
              if (targetId === 'carts-content' ) {
                window.eventEmitter.emit('CUSTOMER_BASKETS_RENDER');
              }
            }
            /** Clear previously active element **/
            element.parent().siblings().removeClass('active');

            /** Mark selected element as active **/
            element.parent().addClass('active');

            /** Apply bootstrap dropDown (if needed) for newly parsed content **/
            $('.dropdown-toggle').dropdown();

            switch (targetId) {
            case 'products-content':
              customerSection.checkServiceabilityIsLoaded();
              customerSection.checkKipAddressForMoveOffnetAlreadyLoaded();
              var isILS_ACQ = $('.side-cart[data-package-type="int_tv_fixtel"]').data('sale-type');
              if (isILS_ACQ) {
                $('#buttons-off').addClass('disabled');
                $('#move-off').addClass('disabled');
              }
              break;
            }

            LeftSidebar.toggleLeftSideBar(true);

            // Set large styles for 360 and NBA
            if (targetId === 'customer-content' || targetId === 'nba-content') {
              $('#'+targetId).closest('.col-left.sidebar').addClass('veryBigView');
            }

            // init popovers
            $('[data-toggle="popover"]').popover({
              trigger: 'focus',
              html: true,
              title: function () {
                return $('#totalBillingDetailsModal' + $(this).attr('data-popover-type') + '-' + $(this).attr('data-customer-number') + ' .popover-header').html();
              },
              content: function () {
                return $('#totalBillingDetailsModal' + $(this).attr('data-popover-type') + '-' + $(this).attr('data-customer-number') + ' > .popover-content').html();
              }
            });

            // Initialize tooltips after ajax content
            $('[data-toggle="tooltip"]').tooltip();

            var dropdownToggle = $('.dropdown[data-icon-collapse="true"]');
            dropdownToggle.on('shown.bs.dropdown', function () {
              $(this).children()
                .find('span.dropdown-changeable-icon')
                .removeClass($(this).data('expand-class'))
                .addClass($(this).data('collapse-class'));
            });

            dropdownToggle.on('hidden.bs.dropdown', function () {
              $(this).children()
                .find('span.dropdown-changeable-icon')
                .removeClass($(this).data('collapse-class'))
                .addClass($(this).data('expand-class'));
            });

            $('html').on('click', function (event) {
              if (dropdownToggle !== event.target && !dropdownToggle.has(event.target).length) {
                dropdownToggle.each(function () {
                  $(this).children()
                    .find('span.dropdown-changeable-icon')
                    .removeClass($(this).data('collapse-class'))
                    .addClass($(this).data('expand-class'));
                });
              }
            });

            // modals enable
            $('[data-toggle="modal"]').click(function () {
              var modalId = $(this).data('target');
              $(modalId).modal({
                'backdrop': false
              });
            });

            var notServiceable = store.get('notServiceable');
            if (notServiceable) {
              $('#buttons-off').addClass('ils-disabled-mandatory');
            }

            // show permission messages
            if (sessionStorage.getItem('permissionShowKias') == 'true') {
              jQuery('.kias-no-permission-wrapper').removeClass('hidden');
            }

            if (sessionStorage.getItem('permissionShowKd') == 'true') {
              jQuery('.kd-no-permission-wrapper').removeClass('hidden');
            }

            if (sessionStorage.getItem('permissionShowFn') == 'true') {
              jQuery('.fn-no-permission-wrapper').removeClass('hidden');
            }

          }
        });

      } else {
        // remove active class of siblings
        element.parent().siblings().each(function () {
          $(this).removeClass('active');
        });
      }
    },
    // END note on package functionality chars. remaining
    expandRightSidebar: function () {
      expandTheRightSidebar();
    },
    getNoteOnPackage: function (element, noteId) {
      var packageId = $(element).attr('id');
      var packageNotesModal = $('#packageNotesModal');

      packageNotesModal.find('button.pull-right').each(function () {
        if (!$(this).hasClass('hidden')) {
          $(this).addClass('hidden');
        }
      });

      $.post(MAIN_URL + 'checkout/cart/getNotesOfPackage', {
        packageId: packageId,
        noteId: noteId
      }).done(function (response) {
        packageNotesModal.find('.modal-title').text('');
        packageNotesModal.find('.modal-body .modal-block').empty();

        var editedNoteId = false;
        packageNotesModal.find('.modal-title').text(Translator.translate('Note on %s').replace('%s', response['packageTitle']));
        if (response['notes']) {
          response['notes'].forEach(function (note) {
            editedNoteId = note['entity_id'];
            var noteDisplayed = (noteId !== 0) ? '' : '<div class="note-body">' + note['note'] + '</div>';
            packageNotesModal.find('.modal-body .modal-block').append('<div class="note-description"><div class="note-date">' + note['package_agent_status'] + ' ' + '<span>' + note['agent_name'] + '</span>' + ' - ' + note['last_update_date'] + '</div>' + noteDisplayed + '</div>');
            packageNotesModal.find('.modal-body .modal-block .note-date span').attr({
              'onmouseover': 'jQuery(this).tooltip("show")',
              'onmouseleave': 'jQuery(this).tooltip("hide")',
              'data-trigger': 'manual',
              'data-placement': 'bottom',
              'data-html': 'true',
              'data-template': '<div class="tooltip ils-tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
              'title': '<p class="note-agent-name">' + note['agent_name'] + '</p>' +
              '<p class="note-agent-email">' + Translator.translate('E-mail:') + ' ' + note['agent_email'] + '</p>' +
              '<p class="note-agent-phone">' + Translator.translate('Phonenumber:') + ' ' + note['agent_phone'] + '</p>'
            });
          });
        }

        if (!noteId) {
          packageNotesModal.find('#addNoteOnPackage').addClass('hidden');
          $('.edit-note-btn').attr({'id': packageId, 'data-note-id': editedNoteId}).removeClass('hidden');
          if ($(element).hasClass('package-notes-checkout')) {
            $('.edit-note-btn').attr('onclick', 'window.customerDe.getNoteOnPackage(this,' + editedNoteId + ')');
          }
        } else {
          var noteForm = packageNotesModal.find('#addNoteOnPackage');
          var noteTextarea = packageNotesModal.find('textarea');
          noteForm.removeClass('hidden');
          noteTextarea.val(response['notes'][0]['note']);
          noteTextarea.focus();
          packageNotesModal.find('#addNoteOnPackage .characters-left').text((150 - noteTextarea.val().length));
          $('.save-note-btn').removeClass('hidden');
        }

        packageNotesModal.find('.modal-body .modal-block').removeClass('hidden');
        packageNotesModal.modal('show');
      });
    },

    postNoteOnPackage: function (element) {
      var packageId = $(element).attr('id');
      var note = $('#addNoteOnPackage').find('textarea').val();
      if (note.length) {
        $.post(MAIN_URL + 'checkout/cart/setNoteOnPackage', {
          packageId: packageId,
          note: note
        }).done(function (response) {
          $('#addNoteOnPackage').find('textarea').val('');

          $('.edit-note-btn').attr({'id': packageId, 'data-note-id': response['noteId']});

          var checkoutNoteBtn = $('.checkout-section-container.review-order').find('#' + packageId + '.package-notes-checkout');
          if (checkoutNoteBtn.length) {
            checkoutNoteBtn.removeClass('hidden');
          } else {
            var noteMarkerPackage = $('#' + packageId + '.package-notes-extended');
            if (noteMarkerPackage.hasClass('hidden')) {
              noteMarkerPackage.removeClass('hidden');
            }
            var noteMarkerPackageNotes = $('#' + packageId + '.package-notes');
            if (noteMarkerPackageNotes.hasClass('hidden')) {
              noteMarkerPackageNotes.removeClass('hidden');
            }
          }
          $('.mtm-' + packageId).addClass('hidden');
          $('.note-field-btn').removeClass('hidden');
          $('.post-note-btn').addClass('hidden');
        });
      } else {
        showModalError('Please enter your note!', 'Error', function () {
          $('#packageNotesModal').modal('show');
        });
      }

    },

    displayNoteAddForm: function (element) {
      var packageNotesModal = $('#packageNotesModal');
      packageNotesModal.find('textarea').val('');
      packageNotesModal.find('#addNoteOnPackage .characters-left').text('150');
      packageNotesModal.find('button.btn-default').each(function () {
        if (!$(this).hasClass('hidden')) {
          $(this).addClass('hidden');
        }
      });
      $('.post-note-btn').removeClass('hidden');

      var packageId = $(element).attr('pkg-id');
      var packageName = $(element).attr('pkg-name');
      packageNotesModal.find('.post-note-btn').attr('id', packageId);
      packageNotesModal.find('.modal-title').text(Translator.translate('Note on %s').replace('%s', packageName));
      packageNotesModal.find('.modal-body .modal-block').addClass('hidden');
      packageNotesModal.find('#addNoteOnPackage').removeClass('hidden');
      packageNotesModal.modal('show');
    },
    checkNoteCharactersLeft: function (element, parent, limit, e) {
      var left = limit - ($(element).val().length);
      if (left <= 0) {
        $(parent).find('.characters-left').text((0));
        e.preventDefault();
      }
      if (left > 0) {
        $(parent).find('.characters-left').text((left));
      }
    },
    checkRemarksCharactersLeft: function (element, limit, e) {
      var left = limit - ($(element).val().length);
      if (left <= 0) {
        $('.modal').find('.characters-left').text((0));
        e.preventDefault();
      }
      if (left > 0) {
        $('.modal').find('.characters-left').text((left));
      }
    },
    unloadCustomer: function (element) {
      var self = this;
      self.customerNumber = null;
      // Avoid executing multiple ajax calls if button is pressed again
      $(element).attr('onclick','');
      // Unload customer
      $.ajax({
        url: '/customerde/details/unloadCustomer/',
        type: 'post',
        cache: false,
        data: ({customer: self.customerData != null  ? self.customerData.id : ''}),
        dataType: 'json',
        success: function (response) {
          setPageLoadingState(true);
          self.customerData = null;
          if (response.saveCartModal) {
            $('#save-cart-modal').replaceWith(response.saveCartModal);
          }
          window.location = '/';
          // remove cart warnings from session
          sessionStorage.removeItem('warning');
          sessionStorage.removeItem('warningMessage');
          sessionStorage.removeItem('warningMessageCardinality');

          sessionStorage.removeItem('permissionShowKias');
          sessionStorage.removeItem('permissionShowKd');
          sessionStorage.removeItem('permissionShowFn');

          // remove linked accounts & red+ panel states
          store.remove('redPlusPanelState');
          store.remove('linkedAccountPanelState');
          store.remove('validateApiResponse');
          store.remove('serviceApiResponse');
          store.remove('serviceApiAddress');
          // disable Proceed button from cart
          $('#configurator_checkout_btn').addClass('disabled');
        }
      });
    },
    changeCustomerType: function(element, state){
      if ($(element).parent().hasClass('selected'))
      {
        return;
      }
      if (state !== false)
      {
        $('#shopping-bag #' + (state === 0 ? 'individual' : 'organization')).trigger('click');
      } else
      {
        $(element)
          .parent()
          .addClass('selected')
          .siblings()
          .removeClass('selected')
        ;
      }
    },
    setBusinessState: function (element) {
      if (element !== 'undefined' && $(element).parent().hasClass('selected')) {
        return;
      }

      var toggleClass = '.soho-toggle';

      //  If customer loaded, customer type cannot be changed
      var customerId = jQuery('#left-customer-info').data('id');
      if (this.customerData || customerId) {
        jQuery(toggleClass).addClass('disabled');
        // Both states are applicable depending on data source
        if (this.customerData.is_business === '1' || customerDe.customerData.isSoho) {
          jQuery('#organizationButton').addClass('selected');
          jQuery('#individualButton').removeClass('selected');
        } else {
          jQuery('#organizationButton').removeClass('selected');
          jQuery('#individualButton').addClass('selected');
        }
        return;
      }

      if (!window.configurator && $('.cart_packages').children('.side-cart:visible').length == 0) {
        ToggleBullet.onSwitchChange(toggleClass, 'soho');
        /** customer switch has been updated */
        if (this.isBusiness = ToggleBullet.state(toggleClass)) {
          /** OMNVFDE-301: For SOHO, nationality and DOB not displayed, **/
          $('[data-id="customer-details-dob"]').hide();
          $('[data-target=' + CABLE_TV_PACKAGE + ']').addClass('disabled');
        } else {
          $('[data-id="customer-details-dob"]').show();
          if (address.addressId) {
            $('[data-target=' + CABLE_TV_PACKAGE + ']').removeClass('disabled');
          }
        }

        // Change customer type on landing page simultaneously (if applicable)
        if (typeof window.campaign !== 'undefined')
        {
          this.changeCustomerType(
            $('#campaigns-top-customer-type [data-type=\''+ToggleBullet.state(toggleClass)+'\']')[0],
            false
          );
        }
        var specificationsToggle = jQuery('#specificationsToggle');
        var btwSpecActive = specificationsToggle.is(':checked');
        if ((this.isBusiness && btwSpecActive === false) || (!this.isBusiness && btwSpecActive === true)) {
          specificationsToggle.trigger('click');
        }
      }
      else {
        ToggleBullet.switchPending = true;
        showChangeCustomerTypeModal();
      }
    }
  });
})(jQuery);
