'use strict';

(function ($) {
  window.CustomerSection = function () {
    this.newAddressRequested = false;
    this.migrationAddressSelected = false;
    this.migrationIconClicked = null;
    this.contractNoCheckedForRetention = []; // in the customer details, products tab -> the accounts checked for retention in the form {contract_no -> kias}
    this.initialize.apply(this, arguments);
    this.changePackageElement = null;
  };

  window.CustomerSection.prototype = $.extend(VEngine.prototype, {
    moveRemovedProducts: false,
    topAddressSearch: '#topaddressCheckForm',
    prolongationCheckButton: null,
    checkProlongationMark: '.check-prolongation',
    redPlusParentSubscription: null,
    showRedPlusMemberWarning: true,
    migrationPartOfBundle: false,
    performCheckServiceEligibility: function (element) {
      setPageLoadingState(true);
      address.callBack = function () {
        address.callBack = null;
        setPageLoadingState(false);
        if (address.services) {
          if (address.services.clearCart) {
            if ($('.enlargeSearch').hasClass('left-direction')) {
              $('.enlargeSearch').trigger('click');
            }
          }
          var eligibilityContainersFixed = $('.product-details-content-container .eligibility-check-fixed');
          $('.product-details-content-container .ellipsis-overflow[data-package-type="int_tv_fixtel"]').removeClass('disabled-mandatory');
          $(eligibilityContainersFixed).addClass('hidden');
        } else {
          showModalError(Translator.translate('Invalid address'));
        }
      };
      if (store.get('validateApiResponse') === null) {
        var validate = {
          data:[{
            type:'address',
            id:$(element).attr('data-id'),
            attributes: {
              street : $(element).attr('data-street'),
              houseNumber : $(element).attr('data-building-nr'),
              postalCode : $(element).attr('data-postalcode'),
              city : $(element).attr('data-city-name'),
              houseNumberAddition : $(element).attr('data-house-addition'),
            }
          }]
        };
        store.set('validateApiResponse', JSON.stringify(validate));
      }
      address.checkService(element);
    },
    changePackage: function (element, context) {
      var customerData = customerDe.customerData || [];
      var ban = customerData.ban || null;
      var pan = customerData.pan || null;
      var isConverged = ban && pan;
      var address_id='';
      if (context == 'SERVICECHECK') {
        this.changePackageElement = element;
        jQuery('#moveServiceAddressCheckModal').modal('show');
        jQuery('#move-service').val('true');
        $('.input-wrapper').initInputElement();
        $('.select-wrapper').initSelectElement();
        if (isConverged) {
          jQuery('#currentLinkBreakingContainer').removeClass('hidden');
          jQuery('#currentLinkBreakingContainer').removeClass('move-link-notification');
        }
        return;
      }
      if (context == "MOVE") {
        jQuery('#moveServiceAddressCheckModal').modal('hide');
        jQuery('#move-service').val('false');
        jQuery('#move-cancel-package').val('true');
        element = this.changePackageElement;
        var serviceApiAddress = store.get('serviceApiAddress');
        address_id = serviceApiAddress.addressId;
      }
      var self = this;
      if (!self.checkMoveMigrationCanStart('migration') || !self.checkMoveMigrationCanStart('move-offnet')) {
        self.cancelMoveOffnet();
      }
      setPageLoadingState(true);
      var data = {
        'button': $(element).data('button'),
        'stack': $(element).data('stack'),
        'customer_id': $(element).data('customer-id'),
        'service_line_id': $(element).data('service-line-id'),
        'products': $(element).data('skus'),
        'tariff': $(element).data('tariff'),
        'pakket': $(element).data('pakket'),
        'allSocs': $(element).data('all-socs'),
        'package_type': $(element).data('package-type'),
        'package_creation_type': $(element).data('package-creation-type'),
        'ctn': $(element).data('ctn'),
        'context': context,
        'prolongation_type': $(element).data('prolongation-type'),
        'prolongation_info_eligibility': $(element).data('prolongation-info-eligibility'),
        'contract_start_date': $(element).data('contract-start-date'),
        'contract_end_date': $(element).data('contract-end-date'),
        'contract_possible_cancellation_date': $(element).data('contract-possible-cancellation-date'),
        'convert_to_member': $(element).data('convert-to-member'),
        'product_id': $(element).data('product-id'),
        'customer_number': $(element).data('customer-number'),
        'redplus_role': $(element).data('redplus-role'),
        'bundle_product_ids': $(element).data('bundle-product-ids'),
        'address_id': address_id,
      };

      $.ajax({
        type: 'POST',
        url: '/configurator/index/inlife',
        data: data,
        success: function (response) {
          if (response.error) {
            jQuery('#loading-modal').modal('hide');
            setPageLoadingState(false);
            showNotificationBar(response.message, 'error');
            return false;
          } else {
            if (response.hasOwnProperty('rightBlock') && response.rightBlock) {
              window.customer.moveRemovedProducts = response.moveRemovedProducts;
              $('.cart_packages')
                .replaceWith(response.rightBlock)
                .promise()
                .done(function () {
                  // initConfigurator for inlife packages
                  if (response.hasOwnProperty('package_id') && response.package_id) {
                    $('.enlargeSearch').trigger('click');
                    var inlifePackage = $('.cart_packages')
                      .find('[data-package-id="' + response.package_id + '"]')
                      .first()
                      .find('.block-container');
                    activateBlock(inlifePackage);
                    $('#package-types').addClass('hide');
                    $('.no-package-zone').addClass('hide');
                  }
                });
            }
            if (response.hasOwnProperty('totals')) {
              var totals = $('#cart_totals');
              totals.replaceWith(response.totals)
            }
            if (response.hasOwnProperty('customMessage')) {
              showModalError(response.customMessage, 'Inlife Information');
              setPageLoadingState(false);
            }
          }
        },
        error: function () {
          setPageLoadingState(false);
        }
      });
    }
  });
})(jQuery);
