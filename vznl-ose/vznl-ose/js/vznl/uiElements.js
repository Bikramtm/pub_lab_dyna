// A dedicated JavaScript file for site-wide UI elements
// which do not belong to any particular section.
(function( $ ) {
  // Multiselect element
  $.fn.initMultiselectElement = function () {
    const showValue = function () {
      const target = jQuery(this);
      const multiselect = target.closest('.multiselect-element');
      const groupedInput = multiselect.find('input')[0];
      let values = [];

      target
        .closest('.multiselect-element-dropdown')
        .find('input')
        .each(function () {
          const option = jQuery(this);
          const isChecked = option.is(':checked');
          const value = option.data('name') ? option.data('name') : option.attr('name');
          if (isChecked) values.push(value);
        });

      const isEmpty = !values.length;

      if (isEmpty) {
        multiselect.addClass('is-empty');
      } else {
        multiselect.removeClass('is-empty');
      }

      // Insert values as text
      jQuery(groupedInput).val(values.join(', '));
    };

    const toggleMultiselectDropdown = function () {
      const multiselect = jQuery(this).parent();
      multiselect.toggleClass('active');

      const closeMultiselectDropdown = function (e) {
        e.stopPropagation();
        const target = jQuery(e.target).closest('.multiselect-element');
        const isTarget = target[0] === multiselect[0];
        if (!isTarget) {
          multiselect.removeClass('active');
          document.removeEventListener('click', closeMultiselectDropdown);
        }
        return false;
      };

      document.addEventListener('click', closeMultiselectDropdown);
    };

    const positionMultiselectLabel = function () {
      const target = jQuery(this);
      const value = target.children().children('input').val();

      if (value) {
        target.removeClass('is-empty');
      } else {
        target.addClass('is-empty');
      }
    };

    jQuery('.multiselect-element').each(positionMultiselectLabel);
    jQuery('.multiselect-element-dropdown input').on('change', showValue);
    jQuery('.multiselect-element-value').off('click').on('click', toggleMultiselectDropdown);
  }

  // Select element
  $.fn.initSelectElement = function () {
    function closeAllSelect(event) {
      const target = event ? event.target : null;
      // A function that will close all select boxes in
      // the document, except the current select box.
      const $dropdownLists = $('.select-items');
      $dropdownLists.each(function() {
        const $selectedValueContainer = $(this).prev();
        const isNotSameSelect = $selectedValueContainer[0] !== target;
        if (isNotSameSelect) $(this).addClass('select-hide')
      });
    }

    function changeSelectValue($select, value) {
      const $options = $select.find('option');
      $options.each(function(i) {
        if ($(this).val() === value) {
          $select.prop('selectedIndex', i);
          $(this).attr('selected', 'selected');
        } else {
          $(this).removeAttr('selected');
        }
      });
    }

    // Change current selected value
    function changeSelection($select, index) {
      const $dropdownList = $select.parent().children('.select-items');
      const $dropdownItems = $dropdownList.children()
      /* On hover select respective option in dropdown */
      $dropdownItems.removeClass('item-selected');
      $dropdownItems.eq(index).addClass('item-selected');
      updateSelectBox($dropdownItems.eq(index));
    }

    function updateSelectBox(element) {
      const elementIsJquery = element instanceof $;
      const $dropdownItem = elementIsJquery ? element : $(this);
      const $selectWrapper = $dropdownItem.closest('.select-wrapper');
      const $dropdownList = $dropdownItem.parent();
      const $select = $selectWrapper.find('select');
      const $options = $select.find('option');
      const $selectedValueContainer = $selectWrapper.find('.select-selected');
      const value = $dropdownItem.attr('data-value').trim();
      const valueText = $dropdownItem.text().trim();

      $dropdownList.children().removeClass('same-as-selected');

      $options.each(function() {
        const $option = $(this);
        const isSameValue = $option.text().trim() === $dropdownItem.text().trim();
        if (isSameValue) {
          $dropdownItem.addClass('same-as-selected');
        }
      });
      $selectedValueContainer.text(valueText);
      changeSelectValue($select, value);
      $selectWrapper.removeClass('is-empty');

      if (!elementIsJquery) {
        $select.trigger('change');
        closeAllSelect();
      }
    }

    function showDropdown() {
      const $currentValue = $(this);
      const $select = $currentValue.prev();
      const $dropdownList = $currentValue.next();
      const $isCurrentlyOpen = $dropdownList.hasClass('select-hide') === false;
      closeAllSelect();
      if ($isCurrentlyOpen) {
        $dropdownList.addClass('select-hide');
        return;
      }
      $dropdownList.removeClass('select-hide');

      const options = [];
      $select.find('option').each(function(index) {
        const value = $(this).text().trim();
        index !== 0 && options.push(value)
      });

      // Controls for scrolling through dropdown
      function keyControls(event) {
        const keyCode = event.keyCode;
        const currentIndex = $select.prop('selectedIndex') - 1 || 0;
        const totalItems = $select.find('option').length - 1;
        let index;
        switch (keyCode) {
        case 13: // Enter key
          $select.trigger('change');
          $dropdownList.addClass('select-hide');
          $currentValue.off('keydown');
          break;
        case 27: // Escape key
          $dropdownList.addClass('select-hide');
          $currentValue.off('keydown');
          break;
        case 38: // Up arrow key
          event.preventDefault();
          const indexAtStart = currentIndex === 0;
          index = indexAtStart ? totalItems - 1 : currentIndex - 1;
          changeSelection($select, index);
          break;
        case 40: // Down arrow key
          event.preventDefault();
          const indexAtEnd = currentIndex === totalItems - 1;
          index = indexAtEnd ? 0 : currentIndex + 1;
          changeSelection($select, index);
          break;
        default:
          const char = String.fromCharCode(keyCode);
          if (/[a-zA-Z]/.test(char)) {
            const result = options.find(function(element) {
              return element[0] === char;
            });
            index = options.findIndex(function(element) {
              return element === result;
            });
            changeSelection($select, index);
          }
        }
        $dropdownList.scrollTop(index * 40);
      }
      $currentValue.off('keydown').on('keydown', keyControls);
    }

    $(this).each(function() {
      const isSelectWrapper = $(this).hasClass('select-wrapper');
      const $selectWrapper = isSelectWrapper? $(this) : $(this).closest('.select-wrapper');
      const $select = $selectWrapper.find('select');
      if(!$selectWrapper.length || !$select.length ) return; // When select is inserted async
      const options = $select.find('option');
      const selectedIndex = $select[0].selectedIndex;
      const text = $(options[selectedIndex]).text().trim();
      const $currentValue = $('<div></div>');
      const $dropdownList = $('<div></div>');
      // Remove previous dropdowns on this select
      $selectWrapper.find('.select-selected, .select-items').remove();
      // Adjusting label if empty value
      if (!text) {
        $selectWrapper.addClass('is-empty');
      } else {
        $selectWrapper.removeClass('is-empty');
      }
      $currentValue.addClass('select-selected')
        .attr('tabindex', 0)
        .html(text);

      $selectWrapper.append($currentValue);
      $dropdownList.addClass('select-items select-hide');

      options.each(function(i) {
        if (i !== 0) {
          const $option = $(this);
          const value = $option.val();
          const text = $option.text().trim();
          const $dropdownItem = $('<div></div>');
  
          $dropdownItem.text(text)
            .attr('data-value', value)
            .addClass('option-selected')
            .on('click', updateSelectBox);

          if (i === selectedIndex) $dropdownItem.addClass('same-as-selected')
  
          $dropdownList.append($dropdownItem);
        }
      });
      $selectWrapper.append($dropdownList);
      $currentValue.on('click', showDropdown);
    });

    // Close all select boxes when user clicks outside the select box.
    // As far as functionality goes this works, however multiple eventListeners
    // are added when this function is called which should be avoided.
    $('body')
      .unbind('click.selectElement')
      .bind('click.selectElement', closeAllSelect)
    ;
  }

  

  // Input element
  $.fn.initInputElement = function () {
    function toggleLabel(element) {
      const $input = element instanceof $ ? element : $(this);
      const $inputWrapper = $input.closest('.input-wrapper');
      const isEmpty = !($input.val() || $input.attr('placeholder')) && !$input.is(':focus');

      if (isEmpty) $inputWrapper.addClass('is-empty');
      else $inputWrapper.removeClass('is-empty');
    }

    $(this).each(function() {
      const isInputWrapper = $(this).hasClass('input-wrapper');
      const $inputWrapper = isInputWrapper ? $(this) : $(this).closest('.input-wrapper');
      const $input = $inputWrapper.find('input, textarea');

      if ($inputWrapper.length && $input.length) {
        toggleLabel($input);
        $input.on('change input blur', toggleLabel);
        $input.on('focus', function() {
          const $inputWrapper = $(this).closest('.input-wrapper');
          $inputWrapper.removeClass('is-empty');
        });
      }
    });
  }
}( jQuery ));

function initFormElements(container)
{
  container.find('.multiselect-element.fluid').initMultiselectElement();
  container.find('.select-wrapper').initSelectElement();
  container.find('.input-wrapper').initInputElement();
}

function showContextMenu(event, self) {
  event.stopPropagation();

  // prevent multiple instances of package actions dropdown
  if (jQuery('.cart_packages').find('.orderline-action.contextmenu-action.expanded').length > 0) {
    jQuery('.cart_packages').find('.orderline-action.contextmenu-action.expanded').removeClass('expanded')
  }

  //remove promo button if package is not expanded
  if ((jQuery('.cart_packages').children('.active').length === 0 &&
      jQuery('.orderline-action.contextmenu-action:visible').children('.contextmenu').children('.contextmenu-item[onclick*="expandPackagePromosContainer"]').length > 0) ||
      !jQuery(self).parents().hasClass('active')
  ) {
    jQuery('.orderline-action.contextmenu-action:visible').children('.contextmenu').children('.contextmenu-item[onclick*="expandPackagePromosContainer"]').hide();
    jQuery('.side-cart.block.active').find('.contextmenu-item[onclick*="expandPackagePromosContainer"]').addClass("disabled");
    jQuery('.side-cart.block.active').find('.promotion-icon').addClass("disabled");
  } else {
    jQuery('.orderline-action.contextmenu-action:visible').children('.contextmenu').children('.contextmenu-item[onclick*="expandPackagePromosContainer"]').show();
    var promoCount = parseInt(jQuery('.side-cart.block.active').find('.promotion-icon').attr('data-count'));
    if (promoCount > 0) {
      jQuery('.side-cart.block.active').find('.contextmenu-item[onclick*="expandPackagePromosContainer"]').removeClass("disabled");
      jQuery('.side-cart.block.active').find('.promotion-icon').removeClass("disabled");
    }
  }

  const contextMenu = jQuery(self);
  contextMenu.toggleClass('expanded');

  if (contextMenu.hasClass('expanded')) {
    const closeContextMenu = function() {
      contextMenu.removeClass('expanded');
      window.removeEventListener('click', function() {closeContextMenu(contextMenu)});
    };
    window.addEventListener('click', function() {closeContextMenu(contextMenu)});
  }
}

jQuery(document).ready(function($) {
  initFormElements($(document));
});