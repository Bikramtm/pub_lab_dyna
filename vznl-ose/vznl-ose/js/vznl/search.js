/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

'use strict';

(function($){

  window.Search = function() { this.initialize.apply(this, arguments) };
  window.Search.prototype = (function() {
    var search = {};

    search.settings = {
      baseUrl: location.protocol + '//' + location.host
      ,endpoints: {}
      ,searchType: 'loose'
      ,documentIdAttr: 'document-id'
      ,defaultDocumentId: 'id'
      ,searchFieldsAttr: 'search-fields'
      ,events: 'keyup input propertychange'
      ,eventsIE: 'mouseup'
      ,eventsTriggerIE: 'change'
      ,callback: function(ids, element){console.log('Number of found docs: {0}'.format(ids.length))}
      ,elClass: 'search-on-type'
      ,namespaceKey: 'search-namespace'
      ,dataAttr: 'data-search-on-type'
      ,documentsAttr: 'documents'
      ,errorMessage : 'An issue has been encountered. Please try again.'
      ,cacheKey: 'SearchIndex'
      ,debug: true
    };

    var acceptedSearchTypes = {loose: 1, exact: 2};
    var alphabet = {
      a:/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/ig,
      aa:/[\uA733]/ig,
      ae:/[\u00E6\u01FD\u01E3]/ig,
      ao:/[\uA735]/ig,
      au:/[\uA737]/ig,
      av:/[\uA739\uA73B]/ig,
      ay:/[\uA73D]/ig,
      b:/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/ig,
      c:/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/ig,
      d:/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/ig,
      dz:/[\u01F3\u01C6]/ig,
      e:/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/ig,
      f:/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/ig,
      g:/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/ig,
      h:/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/ig,
      hv:/[\u0195]/ig,
      i:/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/ig,
      j:/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/ig,
      k:/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/ig,
      l:/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/ig,
      lj:/[\u01C9]/ig,
      m:/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/ig,
      n:/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/ig,
      nj:/[\u01CC]/ig,
      o:/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/ig,
      oi:/[\u01A3]/ig,
      ou:/[\u0223]/ig,
      oo:/[\uA74F]/ig,
      p:/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/ig,
      q:/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/ig,
      r:/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/ig,
      s:/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/ig,
      t:/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/ig,
      tz:/[\uA729]/ig,
      u:/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/ig,
      v:/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/ig,
      vy:/[\uA761]/ig,
      w:/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/ig,
      x:/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/ig,
      y:/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/ig,
      z:/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/ig,
      '':/[\u0300\u0301\u0302\u0303\u0308]/ig
    };

    var defaultDiacriticsRemovalMap = [
      {'base':'A', 'letters':'\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F'},
      {'base':'AA','letters':'\uA732'},
      {'base':'AE','letters':'\u00C6\u01FC\u01E2'},
      {'base':'AO','letters':'\uA734'},
      {'base':'AU','letters':'\uA736'},
      {'base':'AV','letters':'\uA738\uA73A'},
      {'base':'AY','letters':'\uA73C'},
      {'base':'B', 'letters':'\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181'},
      {'base':'C', 'letters':'\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E'},
      {'base':'D', 'letters':'\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779\u00D0'},
      {'base':'DZ','letters':'\u01F1\u01C4'},
      {'base':'Dz','letters':'\u01F2\u01C5'},
      {'base':'E', 'letters':'\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E'},
      {'base':'F', 'letters':'\u0046\u24BB\uFF26\u1E1E\u0191\uA77B'},
      {'base':'G', 'letters':'\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E'},
      {'base':'H', 'letters':'\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D'},
      {'base':'I', 'letters':'\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197'},
      {'base':'J', 'letters':'\u004A\u24BF\uFF2A\u0134\u0248'},
      {'base':'K', 'letters':'\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2'},
      {'base':'L', 'letters':'\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780'},
      {'base':'LJ','letters':'\u01C7'},
      {'base':'Lj','letters':'\u01C8'},
      {'base':'M', 'letters':'\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C'},
      {'base':'N', 'letters':'\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4'},
      {'base':'NJ','letters':'\u01CA'},
      {'base':'Nj','letters':'\u01CB'},
      {'base':'O', 'letters':'\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C'},
      {'base':'OI','letters':'\u01A2'},
      {'base':'OO','letters':'\uA74E'},
      {'base':'OU','letters':'\u0222'},
      {'base':'OE','letters':'\u008C\u0152'},
      {'base':'oe','letters':'\u009C\u0153'},
      {'base':'P', 'letters':'\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754'},
      {'base':'Q', 'letters':'\u0051\u24C6\uFF31\uA756\uA758\u024A'},
      {'base':'R', 'letters':'\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782'},
      {'base':'S', 'letters':'\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784'},
      {'base':'T', 'letters':'\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786'},
      {'base':'TZ','letters':'\uA728'},
      {'base':'U', 'letters':'\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244'},
      {'base':'V', 'letters':'\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245'},
      {'base':'VY','letters':'\uA760'},
      {'base':'W', 'letters':'\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72'},
      {'base':'X', 'letters':'\u0058\u24CD\uFF38\u1E8A\u1E8C'},
      {'base':'Y', 'letters':'\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE'},
      {'base':'Z', 'letters':'\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762'},
      {'base':'a', 'letters':'\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250'},
      {'base':'aa','letters':'\uA733'},
      {'base':'ae','letters':'\u00E6\u01FD\u01E3'},
      {'base':'ao','letters':'\uA735'},
      {'base':'au','letters':'\uA737'},
      {'base':'av','letters':'\uA739\uA73B'},
      {'base':'ay','letters':'\uA73D'},
      {'base':'b', 'letters':'\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253'},
      {'base':'c', 'letters':'\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184'},
      {'base':'d', 'letters':'\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A'},
      {'base':'dz','letters':'\u01F3\u01C6'},
      {'base':'e', 'letters':'\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD'},
      {'base':'f', 'letters':'\u0066\u24D5\uFF46\u1E1F\u0192\uA77C'},
      {'base':'g', 'letters':'\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F'},
      {'base':'h', 'letters':'\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265'},
      {'base':'hv','letters':'\u0195'},
      {'base':'i', 'letters':'\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131'},
      {'base':'j', 'letters':'\u006A\u24D9\uFF4A\u0135\u01F0\u0249'},
      {'base':'k', 'letters':'\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3'},
      {'base':'l', 'letters':'\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747'},
      {'base':'lj','letters':'\u01C9'},
      {'base':'m', 'letters':'\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F'},
      {'base':'n', 'letters':'\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5'},
      {'base':'nj','letters':'\u01CC'},
      {'base':'o', 'letters':'\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275'},
      {'base':'oi','letters':'\u01A3'},
      {'base':'ou','letters':'\u0223'},
      {'base':'oo','letters':'\uA74F'},
      {'base':'p','letters':'\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755'},
      {'base':'q','letters':'\u0071\u24E0\uFF51\u024B\uA757\uA759'},
      {'base':'r','letters':'\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783'},
      {'base':'s','letters':'\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B'},
      {'base':'t','letters':'\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787'},
      {'base':'tz','letters':'\uA729'},
      {'base':'u','letters': '\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289'},
      {'base':'v','letters':'\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C'},
      {'base':'vy','letters':'\uA761'},
      {'base':'w','letters':'\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73'},
      {'base':'x','letters':'\u0078\u24E7\uFF58\u1E8B\u1E8D'},
      {'base':'y','letters':'\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF'},
      {'base':'z','letters':'\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763'}
    ];

    var diacriticsMap = {};
    for (var i=0; i < defaultDiacriticsRemovalMap .length; i++){
      var letters = defaultDiacriticsRemovalMap [i].letters;
      for (var j=0; j < letters.length ; j++){
        diacriticsMap[letters[j]] = defaultDiacriticsRemovalMap [i].base;
      }
    }

    search.documentStore = {};
    search.timeout = 0;

    /**
     * Bootstrapping the app
     */
    search.initialize = function(opts) {
      /** Merge options with default ones */
      $.extend(this.settings, opts);
      this.assureProperConfigs();

      /**
       * Start script
       */
      this.start();

      /** Set cache prefix */
      this.cache.prefix = 'vSearchIndex_' + this.settings.cacheKey;

      this.debug('Search index started');
    };

    /**
     * Bootstrap the script
     */
    search.start = function() {
      $('[{0}]'.format(search.settings.dataAttr)).each(function() {
        if (!$(this).data(search.settings.namespaceKey)) {
          search.attach($(this));
        }
      });
    };

    /**
     * Binds the search functionality to the given element
     * @param el
     */
    search.attach = function(el) {
      try {
        var element = $(el);
        // data might arrive empty since packages are configurable through backend
        if (!element.data(search.settings.documentsAttr)) {
          element.data(search.settings.documentsAttr, {});
        }

        element.addClass(search.settings.elClass);

        element.data(search.settings.namespaceKey, search.helper.randStr());

        search.loadDocuments(element.data(search.settings.documentsAttr), element);

        element.on(search.settings.events, this.delay(function() {
          var value = $.trim($(this).val());
          if ($(this).data('previousValue') === value) {
            return;
          }
          $(this).data('previousValue', value);
          
          var callback = element.data('callback')
            ? search.findMethod(element.data('callback'))
            : search.settings.callback;

          callback(search.search(value, element), element);
        }, 150));

        element.on(search.settings.eventsIE, function() {
          var input = $(this),
            oldValue = input.val();
          if (oldValue == "") return;
          setTimeout(function(){
            var newValue = input.val();
            if (newValue == ""){
              input.trigger(search.settings.eventsTriggerIE);
            }
          }, 1);
        });

      } catch (e) {
        console.log(e);
      }
    };

    search.delay = function(callback, delay) {
      return function() {
        if ($(this).data('forceSearch') === true) {
          $(this).removeData('forceSearch').removeData('previousValue');
          callback.apply(this, arguments);
          return;
        }
        var context = this, args = arguments;
        clearTimeout(search.timeout);
        search.timeout = setTimeout(function () {
          callback.apply(context, args);
        }, delay);
      };
    };

    /**
     * Unbinds the search functionality to the given element
     * @param el
     */
    search.detach = function(el) {
      var namespace = $(el).data(search.settings.namespaceKey);
      if (namespace in search.documentStore) {
        delete search.documentStore[namespace];
      }

      $(el)
        .unbind(search.settings.events)
        .data(search.settings.namespaceKey, null)
        .removeClass(search.settings.elClass);
    };

    /**
     * Unbinds the search functionality from all elements
     */
    search.detachAll = function() {
      $('.' + search.settings.elClass).each(function() {
        search.detach($(this));
      });
    };

    search.findMethod = function(methodName) {
      var namespaces = methodName.split('.');
      var namespacesLength = namespaces.length;
      var method = window;
      var parent = null;
      for (var i = 0; i < namespacesLength; ++i) {
        if (undefined !== method[namespaces[i]]) {
          parent = method;
          method = method[namespaces[i]];
        } else {
          return false;
        }
      }
      if (window === method) {
        return false;
      }
      return method.bind(parent);
    };

    /**
     * Make sure given configs are valid
     */
    search.assureProperConfigs = function() {
      if ( !( search.settings.searchType in acceptedSearchTypes)) {
        throw new Error('Invalid search type attribute');
      }

      if ( ! search.helper.isFunction(search.settings.callback)) {
        throw new Error('Callback property must be a valid callback');
      }
    };

    /**
     * Accepts either a JSON string, the object containing
     * the documents or a valid URL from where we should
     * retrieve the documents
     */
    search.loadDocuments = function(serialisedData, element) {
      if ('string' === typeof serialisedData) {
        if (search.helper.isUrl(serialisedData)) {
          return search.get(serialisedData, {}, function(data) {search.parseDocuments(data, element);}, function(err) {alert(err)});
        } else if(search.helper.isJson(serialisedData)) {
          return search.parseDocuments(JSON.parse(serialisedData), element);
        } else if(search.findMethod(serialisedData)) {
          return search.loadDocuments(search.findMethod(serialisedData)(), element);
        } else {
          throw new Error('Invalid serialized data received');
        }
      } else if (search.helper.isObject(serialisedData) || 'object' === typeof serialisedData) {
        return search.parseDocuments(serialisedData, element);
      } else if(search.helper.isArray(serialisedData) || 'array' === typeof serialisedData) {
        return search.parseDocuments(serialisedData, element);
      } else {
        throw new Error('Invalid serialized data received');
      }
    };

    /**
     * @param data
     * @param element
     */
    search.parseDocuments = function(data, element) {
      var namespace = element.data(search.settings.namespaceKey);
      var documentId = element.data(search.settings.documentIdAttr)
        ? element.data(search.settings.documentIdAttr)
        : search.settings.defaultDocumentId;

      var fields = element.data(search.settings.searchFieldsAttr)
        ? element.data(search.settings.searchFieldsAttr).split(',')
        : false;

      if (undefined === search.documentStore[namespace]) {
        search.documentStore[namespace] = {};
      }

      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          search.documentStore[namespace][data[key][documentId]] = {
            root: data[key],
            tokens: search.parseTokens(data[key], fields)
              .sort(function(a, b) {
                  return b.length - a.length;
                }
              )
          };
        }
      }
    };

    /**
     * @param doc
     * @param fields
     * @returns {Array}
     */
    search.parseTokens = function(doc, fields) {
      var bucket = [];
      for (var key in doc) {
        if (doc.hasOwnProperty(key)) {
          if (fields) {
            if (-1 !== fields.indexOf(key)) {
              bucket = search.addToken(doc[key], bucket);
            }
          } else {
            bucket = search.addToken(doc[key], bucket);
          }
        }
      }
      return bucket.unique();
    };

    /**
     * @param id
     * @param namespace
     * @returns {*}
     */
    search.getDocument = function(id, namespace) {
      if (undefined !== search.documentStore[namespace] && undefined !== search.documentStore[namespace][id]) {
        return search.documentStore[namespace][id].root;
      }
    };

    /**
     *
     * @param str
     * @param element
     * @returns {Array}
     */
    search.search = function(str, element) {
      if ('string' === typeof str) {
        var docs = [];
        var results = [];
        var searchStrings = str.split(',');
        searchStrings.each(function(searchString){
          var namespace = element.data(search.settings.namespaceKey);
          var documentId = element.data(search.settings.documentIdAttr)
            ? element.data(search.settings.documentIdAttr)
            : search.settings.defaultDocumentId;

          if ( ! $.trim(searchString).length && undefined !== search.documentStore[namespace]) {
            for (var key in search.documentStore[namespace]) {
              results.push(search.documentStore[namespace][key].root);
            }
          }

          var term = $.trim(searchString);

          results = search.searchDocument(term, namespace);
          var length = results.length;

          for (var i = 0; i < length; i++) {
            var document = search.getDocument($.trim(results[i].root[documentId]), namespace);
            if (document) {
              docs.push(document);
            }
          }
        });
        return docs;
      } else {
        console.warn('Expected string, got {0}'.format(typeof str));
        return [];
      }
    };

    /**
     * @param token
     * @param bucket
     * @returns {*}
     */
    search.addToken = function(token, bucket) {
      var tokenLength,
        i;
      if (search.helper.isArray(token)) {
        tokenLength = token.length;
        for (i = 0; i < tokenLength; i++) {
          search.addToken(token[i], bucket);
        }
      } else if (search.helper.isObject(token)) {
        for (var key in token) {
          if (token.hasOwnProperty(key)) {
            search.addToken(token[key], bucket);
          }
        }
      } else {
        if (token && token.length > 1) {
          bucket.push(token);
          var cleanToken = search.replaceDiacritics(token);
          if (token !== cleanToken) {
            bucket.push(cleanToken);
          }
          token = $.trim(token).replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/g,'').split(' ');
          tokenLength = token.length;
          for (i = 0; i < tokenLength; i++) {
            var cleanTag = search.replaceDiacritics(token[i]);
            if (token[i] !== cleanTag) {
              bucket.push(cleanTag);
            }
            bucket.push(token[i]);
          }
        }
      }
      return bucket;
    };

    /**
     * @param tokenStr
     * @param namespace
     * @returns {Array}
     */
    search.searchDocument = function (tokenStr, namespace) {
      var token = search.replaceDiacritics(tokenStr);
      var results = [];
      var startExpression,
        documentToken,
        key,
        tokenLength,
        i,
        j;
      switch (search.settings.searchType) {
        case 'loose':
          if (search.helper.isArray(token)) {
            for (key in search.documentStore[namespace]) {
              if (search.documentStore[namespace].hasOwnProperty(key)) {
                tokenLength = token.length;
                for (j = 0; j < tokenLength; j++) {
                  var documentCount = search.documentStore[namespace][key].tokens.length;
                  for(i = 0; i < documentCount; i++) {
                    startExpression = new RegExp(search.escapeValue(token[j]), 'ig');
                    documentToken = search.replaceDiacritics(search.documentStore[namespace][key].tokens[i]);
                    if(startExpression.test(documentToken)) {
                      results.push(search.documentStore[namespace][key]);
                      break;
                    }
                  }
                }
              }
            }
          } else {
            for (key in search.documentStore[namespace]) {
              if (search.documentStore[namespace].hasOwnProperty(key)) {
                tokenLength = search.documentStore[namespace][key].tokens.length;
                for(i = 0; i < tokenLength; i++) {
                  startExpression = new RegExp(search.escapeValue(token), 'ig');
                  documentToken = search.replaceDiacritics(search.documentStore[namespace][key].tokens[i]);
                  if(startExpression.test(documentToken)) {
                    results.push(search.documentStore[namespace][key]);
                    break;
                  }
                }
              }
            }
          }
          break;
        case 'exact':
          if (search.helper.isArray(token)) {
            for (key in search.documentStore[namespace]) {
              if (search.documentStore[namespace].hasOwnProperty(key)) {
                tokenLength = token.length;
                for (j = 0; j < tokenLength; j++) {
                  var documentTokenCount = search.documentStore[namespace][key].tokens.length;
                  for(i = 0; i < documentTokenCount; i++) {
                    if(search.documentStore[namespace][key].tokens[i] == token[j]) {
                      results.push(search.documentStore[namespace][key]);
                      break;
                    }
                  }
                }
              }
            }
          } else {
            for (key in search.documentStore[namespace]) {
              if (search.documentStore[namespace].hasOwnProperty(key)) {
                tokenLength = search.documentStore[namespace][key].tokens.length;
                for(i = 0; i < tokenLength; i++) {
                  if(search.documentStore[namespace][key].tokens[i] == token) {
                    results.push(document);
                    break;
                  }
                }
              }
            }
          }
          break;
      }
      return results;
    };

    /**
     * @param value
     * @returns {*}
     */
    search.escapeValue = function(value) {
      if ( ! /[\~\!\@\#\$\%\^\&\*\(\)\_\+\\\?\:\<\>\.]/.test(value)) {
        return value;
      }

      var sequence = {
        '\\\\': /\\+/g, //important to be first
        '\\+': /\++/g,
        '\\.': /\.+/g,
        '\\?': /\?+/g,
        '\\!': /\!+/g,
        '\\-': /\-+/g,
        '\\*': /\*+/g,
        '\\#': /\#+/g,
        '\\=': /\=+/g,
        '\\$': /\$+/g,
        '\\&': /\&+/g,
        '\\@': /\@+/g,
        '\\%': /\%+/g,
        '\\^': /\^+/g,
        '\\(': /\(+/g,
        '\\)': /\)+/g,
        '\\{': /\{+/g,
        '\\}': /\}+/g,
        '\\<': /\<+/g,
        '\\>': /\>+/g,
        '\\:': /\:+/g,
        '\\;': /\;+/g,
        '\\/': /\/+/g,
        '\\~': /\~+/g
      };

      for (var escaped in sequence) {
        if (sequence.hasOwnProperty(escaped)) {
          value = value.replace(sequence[escaped], escaped);
        }
      }

      return value;
    };

    /**
     * @param str
     * @returns {*}
     */
    search.replaceDiacritics = function(str) {
      if ('string' === typeof str) {
        if(String.prototype.normalize) {
          return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
        }
        return str.replace(/[^\u0000-\u007E]/g, function(a){
          return diacriticsMap[a] || a;
        });

        for (var letter in alphabet) {
          if (alphabet.hasOwnProperty(letter)) {
            str = str.replace(alphabet[letter], letter);
          }
        }
        return str;
      }
      return str;
    };

    /**
     * @param key
     * @param url
     */
    search.addEndpoint = function(key, url)
    {
      this.settings.endpoints[key] = url;
    };

    /**
     * @param key
     */
    search.removeEndpoint = function(key)
    {
      if (key in this.settings.endpoints) {
        delete this.settings.endpoints[key];
      }
    };

    /**
     * @param key
     * @returns {boolean}
     */
    search.hasEndpoint = function(key)
    {
      return key in this.settings.endpoints;
    };

    /**
     * Make a GET call
     *
     * @param url
     * @param data
     * @param callback
     * @param errback
     */
    search.get = function(url, data, callback, errback)
    {
      this.ajax('GET', url, data, callback, errback);
    };

    /**
     * Make a POST call
     *
     * @param url
     * @param data
     * @param callback
     * @param errback
     */
    search.post = function(url, data, callback, errback)
    {
      this.ajax('POST', url, data, callback, errback);
    };

    /**
     *
     * @param type
     * @param url
     * @param data
     * @param callback
     * @param errback
     */
    search.ajax = function(type, url, data, callback, errback)
    {
      var endpoint = this.hasEndpoint(url) ? this.settings.endpoints[url] : url;
      this.debug('START "{0}" call to {1}'.format(type, url));
      var eng = this;
      $.ajax({
        url: endpoint,
        cache: true,
        data: data,
        type: type
      })
        .success(function(data, status) {
          callback(data, status);
          eng.debug('END "{0}" call to {1}'.format(type, url));
        })
        .error(function(xhr, textStatus, errorMessage) {
          undefined !== errback ? errback(errorMessage, textStatus) : function() {};
          eng.error('FAILED "{0}" call to {1} failed with the message "{2}"'.format(type, url, errorMessage));
        });
    };

    /**
     * Logs a debug message in the console
     * The logging is made only if window.console
     * is available and if settings.debug is true
     *
     * @param message
     */
    search.debug = function(message) {
      if (this.settings.debug && window.console) {
        if ( ! window.console.debug) {
          window.console.debug = window.console.log;
        }
        console.debug(this.time() + '  ' + message);
      }
    };

    /**
     * Logs an error message in the console
     * and triggers an alert with the message
     * The logging is made only if window.console
     * is available and if settings.debug is true
     *
     * @param message
     */
    search.error = function(message) {
      if (this.settings.debug && window.console) {
        if ( ! window.console.error) {
          window.console.error = window.console.log;
        }
        console.error(new Date() + ': ' + message);
        alert(this.time() + '  ' + message);
      }
    };

    /**
     * Logs a warn message in the console
     * The logging is made only if window.console
     * is available and if settings.debug is true
     *
     * @param message
     */
    search.warn = function(message) {
      if (this.settings.debug && window.console) {
        if ( ! window.console.warn) {
          window.console.warn = window.console.log;
        }
        console.warn(this.time() + '  ' + message);
      }
    };

    /**
     * Internal method that returns the current
     * date and time well formatted for use
     * when logging message in the console
     *
     * @returns {string}
     */
    search.time = function() {
      var d = new Date();
      var hour = (d.getHours() % 10 == d.getHours()) ? '0' + d.getHours() : d.getHours();
      var min = (d.getMinutes() % 10 == d.getMinutes()) ? '0' + d.getMinutes() : d.getMinutes();
      var sec = (d.getSeconds() % 10 == d.getSeconds()) ? '0' + d.getSeconds() : d.getSeconds();
      var day = (d.getDate() % 10 == d.getDate()) ? '0' + d.getDate() : d.getDate();
      var month = d.getMonth();
      month++;
      month = (month % 10 == month) ? '0' + month : month;
      var year = d.getFullYear();
      return '['+year+'-'+month+'-'+day+' '+hour+':'+min+':'+sec+']';
    };

    /**
     * Internal helper
     *
     * @type {{isFunction: Function}}
     */
    search.helper = {

      /**
       * Checks if the given object
       * is a function
       *
       * @param item
       * @returns {*|boolean}
       */
      isFunction: function(item) {
        var getType = {};
        return item && getType.toString.call(item) === '[object Function]';
      },
      isObject: function(item) {
        var getType = {};
        return item && getType.toString.call(item) === '[object Object]';
      },
      isArray: function(item) {
        var getType = {};
        return item && getType.toString.call(item) === '[object Array]';
      },
      isJson: function(item) {
        return $.trim(item).length && /^[\],:{}\s]*$/
          .test(item.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
            .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
            .replace(/(?:^|:|,)(?:\s*\[)+/g, ''));
      },
      isUrl: function(item) {
        var urlRegex = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return urlRegex.test(item);
      },
      randStr: function(prepend) {
        var prependStr = undefined !== prepend && 'string' === typeof prepend ? prepend : '';
        return prependStr + (((1+Math.random())*0x10000)|0).toString(16).substring(1);
      }
    };

    /**
     * Simple cache store using localStorage
     *
     * @type {{prefix: string, set: Function, get: Function, pluck: Function, flush: Function}}
     */
    search.cache = {
      /**
       * Internal key prefix
       * This prefix will be prepended
       * to all stored keys
       */
      prefix : 'vSearchIndex_',

      /**
       * @param key
       * @param value
       * @returns {*}
       */
      set : function(key, value) {
        if (search.helper.isObject(value) || search.helper.isArray(value)) {
          value = JSON.stringify(value);
        }

        if (window['store'] && window['store'].enabled) {
          window['store'].set(this.prefix + key, value);
        } else {
          localStorage.setItem(this.prefix + key, value);
        }
        return value;
      },

      /**
       * @param key
       * @returns {*}
       */
      get : function(key) {
        var result;
        if (window['store'] && window['store'].enabled) {
          result = window['store'].get(this.prefix + key);
        } else {
          result = localStorage.getItem(this.prefix + key);
        }
        if (result != null) {
          try {
            result = JSON.parse(result);
          } catch(e) {
            //no-op
          }
          return result;
        }
        return undefined;
      },

      /**
       * @param key
       * @returns {*}
       */
      pluck : function(key) {
        var result = localStorage.getItem(this.prefix + key);
        if(result !== null) {
          localStorage.removeItem(this.prefix + key);
          try {
            result = JSON.parse(result);
          } catch(e) {
            //no-op
          }
          return result;
        }
        return undefined;
      },

      /**
       * @returns {boolean}
       */
      flush : function() {
        var reg = new RegExp('^' + this.prefix);
        Object.keys(localStorage)
          .forEach(function(key){
            if (reg.test(key)) {
              localStorage.removeItem(key);
            }
          });
        return true;
      }
    };

    return search;
  })();
})(jQuery);


if (!String.prototype.format) {
  String.prototype.format = function() {

    var args = arguments;
    var sprintfRegex = /\{(\d+)\}/g;

    var sprintf = function (match, number) {
      return number in args ? args[number] : match;
    };

    return this.replace(sprintfRegex, sprintf);
  };
}
if (!Array.prototype.unique) {
  Array.prototype.unique= function() {
    var unique= [];
    for (var i = 0; i < this.length; i += 1) {
      if (unique.indexOf(this[i]) == -1) {
        unique.push(this[i])
      }
    }
    return unique;
  };
}
