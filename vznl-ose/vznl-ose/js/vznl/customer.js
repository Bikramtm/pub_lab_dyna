'use strict';

(function($) {
  window.LeftSidebar.setCustomerMode2ButtonsState = function(enable) {
    var customerDetailsPage = $('.customer_ctns');

    if (!customerDetailsPage)
      return;
    if (enable) {
      customerDetailsPage.find('#product_info_left').removeClass('disabled');
      customerDetailsPage.find('#cart_info_left').removeClass('disabled');
      customerDetailsPage.find('#order_info_left').removeClass('disabled');
      customerDetailsPage.find('#customer_updates_left').removeClass('disabled');
      customerDetailsPage.find('#customer_360_left').removeClass('disabled');
      customerDetailsPage.find('#customer_good_deal_left').removeClass('disabled');
      customerDetailsPage.find('#customer_info_left').removeClass('disabled');
    } else {
      customerDetailsPage.find('#product_info_left').addClass('disabled');
      customerDetailsPage.find('#cart_info_left').addClass('disabled');
      customerDetailsPage.find('#order_info_left').addClass('disabled');
      customerDetailsPage.find('#customer_updates_left').addClass('disabled');
      customerDetailsPage.find('#customer_360_left').addClass('disabled');
      customerDetailsPage.find('#customer_good_deal_left').addClass('disabled');
      customerDetailsPage.find('#customer_info_left').addClass('disabled');
    }
  };

  // bind to ajax requests and perform actions after they finish
  $(document).ajaxStop(function() {
    if ($('.display-360-view').length) {
      var $iframe = $(document).find('#my-360-view-iframe'),
        $buttons = $(document).find('.btn-ctn'),
        iframeBaseUrl = $iframe.data('src');

      $buttons.on('click', function() {
        var $this = $(this);
        $buttons.removeClass('btn-primary').addClass('btn-secondary');
        $this.addClass('btn-primary');
        $iframe.attr('src', iframeBaseUrl.replace('{ctn}', $this.data('ctn')));
      });

      // trigger click on first button
      $(document).find('.btn-ctn:first').click();
    }
  });

  window.Customer.prototype = $.extend(Customer.prototype, {
    modifyInlifeCtn: function(ctn) {
      if (ctn) {
        $(location).attr('href', '/inlife/?ctn=' + ctn);
        return true;
      }
      return false;
    },

    searchCustomer: function(data, mode) {
      store.remove('validateApiResponse');
      store.remove('serviceApiResponse');
      store.remove('serviceApiAddress');
      if (checkoutDeliverPageDialog == true) {
        // If agent tries to load a customer via advanced search while on Checkout delivery show confirmation modal
        $('#checkout_deliver_confirmation').data('function', 'customer.searchCustomer').data('param1', data).appendTo('body').modal();
        return false;
      }

      var el = $(data).parents('form');
      var isOrdersList = $(data).parents('.menu-wrapper').length > 0;
      var isAdvanced = false;
      // Restrict search for house number if no postcode is entered
      var hn = false;
      var pc = false;
      if (this.helper.isObject(el)) {
        data = $.map($(el).serializeArray(el), function(obj) {
          if (obj['name'] == 'customer_type') {
            isAdvanced = true;
          }
          obj['value'] = $.trim(obj['value']);

          if (obj.name == 'billing_postcode' && obj.value != '') {
            pc = true;
          }
          if (obj.name == 'billing_street' && obj.value != '') {
            hn = true;
          }
          return obj;
        });
      }

      if (hn && !pc) {
        showModalError(Translator.translate('House number search must always contain postcode'));
        return false;
      }
      switch (mode) {
        case 'cart':
          return this.searchCustomerOnCart(data, el, isAdvanced);
          break;
        case "indirect":
          return this.searchCustomerIndirect(data, el);
          break;
      }

      var self = this;
      var callback = function(data, status) {
        var customerList = el.find('#customer_list');
        var customerSearchResults = el.find('#customer_search_results');

        customerList.html('');
        customerSearchResults.removeClass('empty');

        //check if returned customer has retention packages (data.additionalInfo)
        // prompt user of loss, continue only if yes
        if (undefined != data.additionalInfo) {
          //has retention OR promo
          //TODO
        }

        if (data.message) {
          if (isAdvanced) {
            customerList.html('');
            customerSearchResults.addClass('empty');
            var body = '';
            var title = '';
            if (data.hasOwnProperty('error_fields') && data.error_fields.length > 0) {
              var errors = "";
              if (data.error_fields) {
                $.each(data.error_fields, function(index, error) {
                  errors += "<span class='error'>" + error + "</span><br>";
                });
              }
              body = errors;
            } else {
              body = data.message;
            }

            showModalError(body);
          } else {
            customerSearchResults.addClass('empty');

            var errors = "";
            if (data.error_fields) {
              $.each(data.error_fields, function(index, error) {
                errors += "<span class='error'>" + error + "</span><br>";
              });
            }
            errors = errors ? errors : data.message + errors;
            customerList.html(errors);
          }
        } else {
          if (isAdvanced) {
            if (data.addressData) {
              //validate address and load the serviceability content in the top bar
              var customerAddress = typeof data.addressData === 'object' ? data.addressData : {};
              var customerAddressArray = Object.keys(customerAddress).map(function (data) {
                return [data, customerAddress[data]];
              });

              for (var i = 0; i < customerAddressArray.length; i++) {
                switch (customerAddressArray[i]["0"]) {
                  case "postcode":
                    document.getElementById('topBar.addressCheck.postcode').value = customerAddressArray[i]["1"];
                    break;
                  case "street":
                    document.getElementById('topBar.addressCheck.street').value = customerAddressArray[i]["1"];
                    document.getElementById('topBar.addressCheck.houseNo').value = customerAddressArray[i]["1"].match(/\d+/);
                    break;
                  case "house_no_addition":
                    document.getElementById('topBar.addressCheck.addition').value = customerAddressArray[i]["1"];
                    break;
                  case "city":
                    document.getElementById('topBar.addressCheck.city').value = customerAddressArray[i]["1"];
                    break;
                }
              }
              document.getElementById('topBar_addressCheck_isReloadPage').value = 1;
              document.getElementById('legacy_reload').value = 1;
              $('.check-button').click();
            }
            return;
          }

          $.each(data, function(index, item) {
            if (item.hasOwnProperty('limit_reached')) {
              customerList.prepend($('<div></div>').addClass('search-result-warning').append('<b>' + item.limit_reached + '</b>'));
              return true;
            }

            var name = item.prefix + ' ' + item.firstname + ' ' + (item.middlename && item.middlename.length ? item.middlename : '') + ' ' + item.lastname; // (item.middlename ? (' ' + item.middlename + ' ') : '')

            if (item.is_business == 1) {
              name = item.company_name;
            }

            var div = $('<div></div>').addClass('result').attr('id', item.entity_id);

            if (item.is_prospect == 0) {
              div.data('prospect', 'false');
              div.append('<b>' + name + ' (BAN: ' + item.ban + ')' + '</b>');
              div.append('<p>' + item.billing_street + ', ' + item.billing_postcode + ', ' + item.billing_city + '</p>');
            } else {
              div.data('prospect', 'true');
              div.append('<b>' + name + ' (EMAIL: ' + item.is_prospect + ')' + '</b>');
            }

            if ($('#customer_ctn').val() && typeof person !== 'undefined') {
              // person.ContactPhoneNumber can be an array or object
              var contactDetails = person.ContactPhoneNumber instanceof Array ? person.ContactPhoneNumber[0] : person.ContactPhoneNumber;

              div.append('<p>CTN:' + contactDetails.PhoneNumber + '</p>');
            }
            if ($('#postcode_zip').val() || $('#postcode_letters').val()) {
              div.append('<p>PC:' + item.billing_postcode + '</p>');
            }
            div.click(function() {
              if (isOrdersList) {
                var customerId = $(this).prop('id');
                var section = el.parents('.cb_slide_panel').data('section');
                updateOrdersSection(section, null, customerId);
              } else {
                $(".col-left .enlarge.right-direction").css('display', 'block');
                var afterCallBack = function() {
                  self.post('customer.sticker', {}, function(data) {
                    if (data.sticker) {
                      $('.col-left').find('#ctn-tabs').html(jQuery(data.sticker).find('#ctn-tabs').html());
                      //bindSlidepanelLeft();
                    }
                  });
                };

                self.loadCustomerData($(this).prop('id'), false, $(this).data('prospect'), afterCallBack, false, true);
              }

              jQuery('.el-tab').removeClass('active');
              jQuery('a[href=#oo-customer-search]').addClass('active');
            });
            customerList.append(div);
          });
        }
        customerSearchResults.first().show();
        endPiwikCall(MAIN_URL + "customer/search/index");
        triggerConfiguratorEvent('configurator.customersfound', '');
      };
      startPiwikCall(MAIN_URL + "customer/search/index");
      this.post('customer.search', data, callback, function(err, status, xhr) {
        endPiwikCall(MAIN_URL + "customer/search/index");
        /**@var {XMLHttpRequest} xhr */
        if (xhr && xhr.status == 414) {
          err = 'Request too long.';
        }
        if (xhr && xhr.status != 0) {
          showModalError(err);
        }
      });
    },

    logoutCustomer: function() {

      store.remove('validateApiResponse');
      store.remove('serviceApiResponse');
      store.remove('serviceApiAddress');
      // If agent tries to change the customer while on Checkout delivery show confirmation modal
      if (checkoutDeliverPageDialog === true) {
        jQuery('#checkout_deliver_confirmation').data('function','customer.logoutCustomer').appendTo('body').modal();
        return false;
      }

      var callback = function(data, status) {
        if (data.sticker) {
          $('.col-left').find('.sticker').html(data.sticker);
        }
        if (data.saveCartModal) {
          $('#save-cart-modal').replaceWith(data.saveCartModal);
        }
        $('html body').html('<p style="padding-top: 25%;text-align: center;">'+$('#unloading_customer_data_label').val()+'</p>');
        window.location.reload(true);
      };
      this.post('customer.logout', {}, callback, function(err, status, xhr) {if(xhr && xhr.status != 0) showModalError(err);});
    },

    showOnlyRetainable: function (showRetainableBtn) {
      $(showRetainableBtn).attr('disabled', true);
      var self = this;
      window.checkoutEnabled = true;
      // Wait for poolCustomerData to finish
      if (!window.checkoutEnabled) {
        setPageLoadingState(true);
        if (!window.showOnlyRetainableInterval) {
          window.showOnlyRetainableInterval = setInterval(function () {
            self.showOnlyRetainable(showRetainableBtn)
          }, 1000);
        } else if (self.customerNeedsPin === true) {
          setPageLoadingState(false);
          clearInterval(window.showOnlyRetainableInterval);
          var pinModal = $('#customer_pin_modal');
          pinModal.find('input[name="cart"]').val(false);
          pinModal.modal();
          $(showRetainableBtn).attr('disabled', false);
          return;
        }
        return;
      } else {
        if (window.showOnlyRetainableInterval) {
          clearInterval(window.showOnlyRetainableInterval);
        }
      }

      if (self.customerNeedsPin === true) {
        var pinModal = $('#customer_pin_modal');
        pinModal.find('input[name="cart"]').val(false);
        pinModal.modal();
        $(showRetainableBtn).attr('disabled', false);
        return;
      }

      var retainableSelect = $('#select_only_retainable');
      $('#no_retainable_msg').empty().hide();
      var callback = function (data, status) {
        var creditProfileCheckResult = data.creditProfileCheckResult;
        setPageLoadingState(false);
        $(showRetainableBtn).attr('disabled', false);
        if (creditProfileCheckResult.hasOwnProperty('serviceError')) {
          showModalError(creditProfileCheckResult.message, creditProfileCheckResult.serviceError);
          return;
        } else if (creditProfileCheckResult.error) {
          if (creditProfileCheckResult.manualOverride) {
            self.showOverrideCtnPopup();
          } else {
            $('#no_retainable_msg').text(data.message && data.message.length ? data.message : 'Failed to process your request. Please try again.').show();
          }
          return;
        } else {
          $('.ctn_box.retainable').hide();
          $('.retainable').hide();
          $('retainable-ctn').removeClass('retainable-ctn');
          $('.ctn_checkbox').prop('checked', false).prop('disabled', true);
          if (data.hasOwnProperty('ctns')) {
            $.each(data['ctns'], function (i, value) {
              var el = $('.ctn_record[data-rawctn="' + value.ctn + '"]');
              el.addClass('retainable-ctn')
                .find('.retainable').show().end()
                .find('.ctn_checkbox').prop('disabled', false).end();
              if (value.status != 'ACTIVE') {
                el.find('.retainable-number').html(value.ctn + ' (' + value.status + ')');
              }
              // Show retainable status
              var retainableRoot = el.find('[data-type="retainablestatus"]');
              retainableRoot.find('p:eq(0)').show().find('span').text(Translator.translate(value.retainableStatus));
              retainableRoot.find('p:eq(1)').show().find('span').text(value.reasoncode).show();
              if (value.reasoncode === null) {
                retainableRoot.find('p:eq(1)').hide();
              }
            });
          }

          $('#one-off-deal').prop('disabled', !data.oneOffDeal);
          if (data.creditProfileCheckResult.ctns.length > 0) {
            $.each(data.creditProfileCheckResult.ctns, function (index, value) {
              if (value['retainable'] == false) {
                var el = $('.ctn_record[data-rawctn="' + value.ctn + '"]');
                el.find('.retainablectn').removeClass('hide').parent().addClass('hide');
                el.find('.notretainablectn').parent().removeClass('hide');
                el.find('.notretainablectn')
                  .attr('data-original-title', value.reasoncode)
                  .tooltip('fixTitle');
                //el.find('.notretainablectn').parent().removeClass('hide');
              } else {
                var el = $('.ctn_record[data-rawctn="' + value.ctn + '"]');
                el.find('.retainablectn').removeClass('hide').parent().removeClass('hide');
                el.find('.notretainablectn').parent().addClass('hide');
              }
            });
          } else {
            $('#profile_credit_status').addClass('hidden');
          }

          if (retainableSelect.length) {
            retainableSelect.find('#only_retainable_check').removeAttr('disabled');
          }
        }
        window.checkCanAddToCart && checkCanAddToCart();
      };
      this.post('customer.showExtendable', {}, callback, function (err) {
        showModalError('Er is een probleem opgetreden bij de communicatie met de Dealer Adapter. De volgende fout is opgetreden: ' + err)
      });
    },
    searchCustomerIndirect: function(data, el) {

      var callback = function(data, status) {
        var url = window.location.href;
        if (data.error == true) {
          showModalError(data.message);
        } else {
          if(el.prop('id') == 'top_menu_search') {
            var customerId = data.customerData.entity_id;
            var section = el.parents('.cb_slide_panel').data('section');
            topMenuSearch(customerId,section);
          } else {
            if (data.addressData) {
              //validate address and load the serviceability content in the top bar
              var customerAddress = typeof data.addressData === 'object' ? data.addressData : {};
              var customerAddressArray = Object.keys(customerAddress).map(function (data) {
                return [data, customerAddress[data]];
              });

              for (var i = 0; i < customerAddressArray.length; i++) {
                switch (customerAddressArray[i]["0"]) {
                  case "postcode":
                    document.getElementById('topBar.addressCheck.postcode').value = customerAddressArray[i]["1"];
                    break;
                  case "street":
                    document.getElementById('topBar.addressCheck.street').value = customerAddressArray[i]["1"];
                    document.getElementById('topBar.addressCheck.houseNo').value = customerAddressArray[i]["1"].match(/\d+/);
                    break;
                  case "house_no_addition":
                    document.getElementById('topBar.addressCheck.addition').value = customerAddressArray[i]["1"];
                    break;
                  case "city":
                    document.getElementById('topBar.addressCheck.city').value = customerAddressArray[i]["1"];
                    break;
                }
              }
              document.getElementById('topBar_addressCheck_isReloadPage').value = 1;
              document.getElementById('legacy_reload').value = 1;
              $('.check-button').click();
            }
          }
        }
      };
    },
    checkOnlyRetainable: function (showRetainableBtn) {
      var customerNumber = $(showRetainableBtn).data('customer-number');
      var inputCtn = $(showRetainableBtn).data('ctn');
      if (inputCtn) {
        $('.show_retainable_check_'+inputCtn).addClass('loading');
      } else {
        $('.show_retainable_check_'+customerNumber).addClass('loading');
      }
      $(showRetainableBtn).attr('disabled', true);
      var self = this;
      window.checkoutEnabled = true;
      // Wait for poolCustomerData to finish
      if (!window.checkoutEnabled) {
        setPageLoadingState(true);
        if (!window.showOnlyRetainableInterval) {
          window.showOnlyRetainableInterval = setInterval(function () {
            self.showOnlyRetainable(showRetainableBtn)
          }, 1000);
        } else if (self.customerNeedsPin === true) {
          setPageLoadingState(false);
          clearInterval(window.showOnlyRetainableInterval);
          var pinModal = $('#customer_pin_modal');
          pinModal.find('input[name="cart"]').val(false);
          pinModal.modal();
          $(showRetainableBtn).attr('disabled', false);
          return;
        }
        return;
      } else {
        if (window.showOnlyRetainableInterval) {
          clearInterval(window.showOnlyRetainableInterval);
        }
      }

      if (self.customerNeedsPin === true) {
        var pinModal = $('#customer_pin_modal');
        pinModal.find('input[name="cart"]').val(false);
        pinModal.modal();
        $(showRetainableBtn).attr('disabled', false);
        return;
      }

      var retainableSelect = $('#select_only_retainable');
      $('#no_retainable_msg').empty().hide();
      var callback = function (data, status) {
        var creditProfileCheckResult = data.creditProfileCheckResult;
        setPageLoadingState(false);
        $(showRetainableBtn).attr('disabled', false);
        if (creditProfileCheckResult.hasOwnProperty('serviceError')) {
          showModalError(creditProfileCheckResult.message, creditProfileCheckResult.serviceError);
          if (!inputCtn) {
            if (!data.oneOffDeal) {
              $('.show_retainable_check_' + customerNumber).removeClass('hidden');
              $('.renewal-check-spinner_' + customerNumber).addClass('hidden');
              $('.show_retainable_check_'+customerNumber).removeClass('loading');
            }
          } else {
            $('.show_retainable_check_'+inputCtn).removeClass('hidden');
            $('.renewal-check-spinner_'+inputCtn).addClass('hidden');
            $('.show_retainable_check_'+inputCtn).removeClass('loading');
          }
          return;
        } else if (creditProfileCheckResult.error) {
          if (creditProfileCheckResult.manualOverride) {
            self.showOverrideCtnPopup();
          } else {
            $('#no_retainable_msg').text(data.message && data.message.length ? data.message : 'Failed to process your request. Please try again.').show();
          }
          return;
        } else {
          $('.ctn_box.retainable').hide();
          $('.retainable').hide();
          $('retainable-ctn').removeClass('retainable-ctn');
          $('.ctn_checkbox').prop('checked', false).prop('disabled', true);
          if (data.hasOwnProperty('ctns')) {
            $.each(data['ctns'], function (i, value) {
              var el = $('.ctn_record[data-rawctn="' + value.ctn + '"]');
              el.addClass('retainable-ctn')
                .find('.retainable').show().end()
                .find('.ctn_checkbox').prop('disabled', false).end();
              if (value.status != 'ACTIVE') {
                el.find('.retainable-number').html(value.ctn + ' (' + value.status + ')');
              }
              // Show retainable status
              var retainableRoot = el.find('[data-type="retainablestatus"]');
              retainableRoot.find('p:eq(0)').show().find('span').text(Translator.translate(value.retainableStatus));
              retainableRoot.find('p:eq(1)').show().find('span').text(value.reasoncode).show();
              if (value.reasoncode === null) {
                retainableRoot.find('p:eq(1)').hide();
              }
            });
          }
          $('.one-off-deal-renewall_' + customerNumber).prop('disabled', !data.oneOffDeal);
          if (!inputCtn) {
            if (data.oneOffDeal) {
              $('.show_retainable_check_' + customerNumber).addClass('hidden');
              $('.one-off-deal-renewall_' + customerNumber).removeClass('loading');
              $('.prolongation-button[data-contract="'+customerNumber+'"]').removeClass('disabled');
            }
          }

          if (data.creditProfileCheckResult.ctns.length > 0) {
            $.each(data.creditProfileCheckResult.ctns, function (index, value) {
              if (inputCtn && inputCtn != value.ctn) {
                return;
              }
              $('.eligibility-check[data-customer-number=\''+value.ctn+'\']').addClass('hidden');
              var ctnButton = $('.prolongation-button[data-ctn="'+value.ctn+'"]');
              var ctnContainer = ctnButton.closest('.products-sub-package');
              if (value['retainable'] == false) {
                var el = $('.ctn_record[data-rawctn="' + value.ctn + '"]');
                el.find('.retainablectn').removeClass('hide').parent().addClass('hide');
                el.find('.notretainablectn').parent().removeClass('hide');
                el.find('.notretainablectn')
                  .attr('data-original-title', value.reasoncode)
                  .tooltip('fixTitle');
                ctnButton.addClass('disabled');
                ctnContainer.find('.renewable').addClass('hidden');
                ctnContainer.find('.not-renewable').removeClass('hidden');
              } else {
                var el = $('.ctn_record[data-rawctn="' + value.ctn + '"]');
                el.find('.retainablectn').removeClass('hide').parent().removeClass('hide');
                el.find('.notretainablectn').parent().addClass('hide');
                ctnButton.removeClass('disabled');
                ctnContainer.find('.renewable').removeClass('hidden');
                ctnContainer.find('.not-renewable').addClass('hidden');
                $('.notification-banner[data-customer-number="'+ value.ctn +'"]').addClass('hidden');
              }
            });
          } else {
            $('#profile_credit_status').addClass('hidden');
          }

          if (retainableSelect.length) {
            retainableSelect.find('#only_retainable_check').removeAttr('disabled');
          }
        }
        if (!inputCtn) {
          if (!data.oneOffDeal) {
            $('.show_retainable_check_' + customerNumber).removeClass('hidden');
            $('.renewal-check-spinner_' + customerNumber).addClass('hidden');
            $('.show_retainable_check_'+customerNumber).removeClass('loading');
          }
        } else {
          $('.show_retainable_check_'+inputCtn).removeClass('hidden');
          $('.renewal-check-spinner_'+inputCtn).addClass('hidden');
          $('.show_retainable_check_'+inputCtn).removeClass('loading');
        }
        window.checkCanAddToCart && checkCanAddToCart();
      };
      this.post('customer.showExtendable', {}, callback, function (err) {
        showModalError('Er is een probleem opgetreden bij de communicatie met de Dealer Adapter. De volgende fout is opgetreden: ' + err)
      });
    }
  });
})(jQuery);

function searchOtherAddressModal(self) {
  var container = jQuery(self).parents('.search-address');
  var postcode = container.find('.search-address-form input[id*="postcode"]').val().replace(/ /g,'');
  var houseno = container.find('.search-address-form input[id*="houseno"]').val().replace(/ /g,'');
  var addition = container.find('.search-address-form input[id*="addition"]').val().replace(/ /g,'');
  var submit = jQuery('#address-check-modal-submit');
  submit.prop('disabled', true);
  if(jQuery.trim(postcode) != '' && jQuery.trim(houseno) != '') {
    submit.addClass('loading');
    jQuery.post(
      '/checkout/index/searchAddress',
      {postcode: jQuery.trim(postcode), houseno:jQuery.trim(houseno), addition:jQuery.trim(addition)},
      function(result) {
        submit.removeClass('loading');
        container.find('.otherAddressEmpty').remove();
        if (!result.error) {
          submit.prop('disabled', false);
          container.find('.search-address-form input[id*="street"]').val(result.fields.street).trigger('change');
          container.find('.search-address-form input[id*="city"]').val(result.fields.city).trigger('change');
        }
      }
    );
  } else
  {
    container.find('.search-address-form input[id*="street"]').val('').trigger('change');
    container.find('.search-address-form input[id*="city"]').val('').trigger('change');
  }
}

function searchAddressModal(self) {
  var container = jQuery(self).parents('.search-address-modal');
  var postcode = container.find('.search-address-form-modal input[id*="postcode"]').val().replace(/ /g,'');
  var houseno = container.find('.search-address-form-modal input[id*="houseno"]').val().replace(/ /g,'');
  var addition = container.find('.search-address-form-modal input[id*="addition"]').val().replace(/ /g,'');
  if(jQuery.trim(postcode) != '' && jQuery.trim(houseno) != '') {
    jQuery.post(
      '/checkout/index/searchAddress',
      {postcode: jQuery.trim(postcode), houseno:jQuery.trim(houseno), addition:jQuery.trim(addition)},
      function(result) {
        container.find('.otherAddressEmpty').remove();
        if (result.error) {
          container.after('<p class="otherAddressEmpty validation-advice">{0}</p>'.format(Translator.translate(result.message)));
        } else {
          container.find('.search-address-form-modal input[id*="street"]').val(result.fields.street).trigger('change');
          container.find('.search-address-form-modal input[id*="city"]').val(result.fields.city).trigger('change');
        }
      }
    );
  } else
  {
    container.find('.search-address-form-modal input[id*="street"]').val('').trigger('change');
    container.find('.search-address-form-modal input[id*="city"]').val('').trigger('change');
  }
}

function convertToUpperCase(element) {
  element = jQuery(element);
  var val = element.val();
  val = val.replace(/[0987654321\s\.\~\`\!\@\#\$\%\^\&\*\(\)\_\+\=\-\[\]\{\}\;'\:\|\"\<\>\,\/\?\\"]/gi, '');
  if (val) {
    val = val.toUpperCase().split('').join('.') + '.';
  }
  element.val(val);
}
