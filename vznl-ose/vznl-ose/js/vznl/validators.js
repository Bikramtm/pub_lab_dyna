jQuery(document).ready(function ($) {
    Validation.add('validate-callback-date-nl', Translator.translate('Date should be greater or equal to today'), function(v, element) {
        if ((v != '') && (v != null) && (v.length != 0)) {
            var date = new Date();
            var input = v.split('-');
            var test = new Date(input[2], parseInt(input[1], 10) - 1, input[0]);
            var dateNow = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            return test.valueOf() >= dateNow.valueOf();
        } else {
            return true;
        }
    });
    Validation.add('validate-postcode-nl', Translator.translate('Invalid postcode or wrong length, must be 6 characters long'), function (v) {
        if (v == '' || (v == null) || (v.length == 0)) {
            return true;
        } else {
            v = $.trim(v).replace(/\s+/g, '');
            res = /^\d{4}\s?[a-zA-Z]{2}$/.test(v);
            if(res == true) {
                jQuery('#postcodeWarningContainer').addClass('hidden');
                jQuery('#postcodeWarningContainer').addClass('move-postcode-notification');
            } else {
                jQuery('#postcodeWarningContainer').removeClass('hidden');
                jQuery('#postcodeWarningContainer').removeClass('move-postcode-notification');
            }
            return res;
        }
    });

    Validation.add('sms-code-validation', Translator.translate('Invalid SMS code'), function (v, element) {
        if (jQuery(element).hasClass('sms-code-not-valid')) {
            return false;
        } else {
            return true;
        }
    });
});