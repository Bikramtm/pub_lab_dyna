<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 * The address search query which forwards parses requests and responses to and from google search api
 * Not created in Magento for preventing performance overhead
 */

abstract class Parser {
    protected $response = null;
    /** @var RequestParser */
    protected $requestParser = null;
    // this is the format of the parsed response
    protected $parsedResponse = array();

    /**
     * Parser constructor. Set response from request on current instance
     * @param $response
     */
    public function __construct($response, $requestParser)
    {
        $this->response = $response;
        $this->requestParser = $requestParser;
    }

    /*
     * Return the parsed response handled by the extending classes
     * @return array
     */
    public function getParsedResponse()
    {
        $this->parse();
        return $this->parsedResponse;
    }

    /**
     * The response to be sent in front-end needs to be a list of response entries (of arrays with the folowing structure)
     * @return array
     */
    public function getEmptyResponseEntry()
    {
        return array(
            'zipCode' => null,
            'city' => null,
            'street' => null,
            'houseNo' => null,
            'houseNoAddition' => null,
        );
    }

    abstract function parse();
}

class CityResponseParser extends Parser
{
    public function parse()
    {
        $this->response = json_decode($this->response, true);
        // iterate through all predictions and add them to the parsed response
        foreach ($this->response['predictions'] ?? array() as $prediction) {
            // skip this result if it is not a street or it cannot be interpreted
            if (!isset($prediction['types']) || !in_array("locality", $prediction['types']) || !isset($prediction['terms'][0])) {
                continue;
            }
            // this type of search will return a string consisting of street name and city, so there is not too much info to be sent in frontend
            // the other nodes will be sent empty
            $result = $this->getEmptyResponseEntry();
            $result['zipCode'] = $this->requestParser->zipCode;
            $result['city'] = $prediction['terms'][0]['value'];
            $result['street'] = null;
            $result['houseNo'] = null;
            $result['houseNoAddition'] = null;
            switch ($this->requestParser->previousInput) {
                case "houseNo":
                    $result['houseNoAddition'] = $this->requestParser->houseNoAddition;
                    break;
                case "street":
                    $result['houseNo'] = $this->requestParser->houseNo;
                    $result['houseNoAddition'] = $this->requestParser->houseNoAddition;
                    break;
                case "houseNoAddition":
                default:
                    break;
            }
            // add this parsed result to the list
            $this->parsedResponse[] = $result;
        }
        usort($this->parsedResponse, function ($a, $b) {
            return strcmp($a['city'], $b['city']);
        });
    }
}

class StreetResponseParser extends Parser
{
    public function parse()
    {
        $this->response = json_decode($this->response, true);
        // iterate through all predictions and add them to the parsed response
        foreach ($this->response['predictions'] ?? array() as $prediction) {
            // skip this result if it is not a street or it cannot be interpreted
            if (!isset($prediction['types']) || !in_array("route", $prediction['types']) || !isset($prediction['terms'][0]) || !isset($prediction['terms'][1])) {
                continue;
            }
            // this type of search will return a string consisting of street name and city, so there is not too much info to be sent in frontend
            // the other nodes will be sent empty
            $result = $this->getEmptyResponseEntry();
            // request had zip code for filtering which changes prediction response structure
            $result['street'] = $prediction['terms'][0]['value'];
            if ($this->requestParser->zipCode) {
                // it's a trap, this is a street followed by a house number with the same values as our zipcode
                if (strpos($result['street'], $this->requestParser->zipCode) !== false) {
                    continue;
                }
                $result['zipCode'] = $prediction['terms'][1]['value'];
                $result['city'] = $prediction['terms'][2]['value'];
                $result['houseNo'] = $this->requestParser->houseNo;
                $result['houseNoAddition'] = $this->requestParser->houseNoAddition;
            } else {
                $result['city'] = $prediction['terms'][1]['value'];
            }
            // add this parsed result to the list
            $this->parsedResponse[] = $result;
        }
        usort($this->parsedResponse, function ($a, $b) {
            return strcmp($a['street'], $b['street']);
        });
    }
}

class ZipCodeResponseParser extends Parser
{
    public function parse()
    {
        $this->response = json_decode($this->response, true);
        // iterate through all predictions and add them to the parsed response
        foreach ($this->response['predictions'] ?? array() as $prediction) {
            // skip this result if it is not a street or it cannot be interpreted
            if (!isset($prediction['types']) || !in_array("postal_code", $prediction['types']) || !isset($prediction['terms'][0]) || !isset($prediction['terms'][1])) {
                continue;
            }
            // this type of search will return a string consisting of street name and city, so there is not too much info to be sent in frontend
            // the other nodes will be sent empty
            $result = $this->getEmptyResponseEntry();
            $result['zipCode'] = $prediction['terms'][0]['value'];
            $result['city'] = $prediction['terms'][1]['value'];
            $result['street'] = null;
            $result['houseNo'] = null;
            $result['houseNoAddition'] = null;
            // add this parsed result to the list
            $this->parsedResponse[] = $result;
        }
        usort($this->parsedResponse, function ($a, $b) {
            return $a['zipCode'] - $b['zipCode'];
        });
    }
}


class GeocodeResponseParser extends Parser
{
    public function parse()
    {
        header("Content-type: application/json");
        $this->response = json_decode($this->response, true);
        // iterate through all results
        foreach ($this->response['results'] ?? array() as $result) {
            // skip this result if it is not a street or it cannot be interpreted
            if (!isset($result['types']) || (!in_array("street_address", $result['types']) && !in_array("postal_code", $result['types']))) {
                continue;
            }
            // this type of search will return a string consisting of street name and city, so there is not too much info to be sent in frontend
            // the other nodes will be sent empty
            $parsedResult = $this->getEmptyResponseEntry();
            foreach ($result['address_components'] as $component) {
                switch (true) {
                    case in_array("postal_code", $component['types']):
                        $parsedResult['zipCode'] = $component['long_name'];
                        break;
                    case in_array("street_number", $component['types']):
                        if (is_numeric($component['long_name'])) {
                            $parsedResult['houseNo'] = $component['long_name'];
                        } else {
                            $parsedResult['houseNo'] = preg_replace('/[^0-9]/s', '', $component['long_name']);
                            $parsedResult['houseNoAddition'] = preg_replace('/[0-9]/s', '', $component['long_name']);
                        }
                        break;
                    case in_array("route", $component['types']):
                        $parsedResult['street'] = $component['long_name'];
                        break;
                    case in_array("sublocality", $component['types']) && !$parsedResult['city']:
                    case in_array("locality", $component['types']):
                        $parsedResult['city'] = $component['long_name'];
                        break;
                    default:
                        break;
                }
            }
            // add this parsed result to the list
            $this->parsedResponse[] = $parsedResult;
        }
        usort($this->parsedResponse, function ($a, $b) {
            return strcmp($a['street'], $b['street']);
        });
    }
}

class RequestParser
{
    protected $apiKey = null;
    protected $stickToCountry = null;

    public $focused;
    public $zipCode;
    public $city;
    public $street;
    public $houseNo;
    public $houseNoAddition;
    public $previousInput;

    protected $searchType = null;
    protected $url = null;

    public function __construct($get)
    {
        $this->stickToCountry = strtolower($get['country']);
        $this->apiKey = $get['apiKey'];
        $this->focused = $get['focused'];
        $this->zipCode = $get['zipCode'];
        $this->city = $get['city'];
        $this->street = $get['street'];
        $this->houseNo = $get['houseNo'];
        $this->houseNoAddition = $get['houseNoAddition'];
        $this->previousInput = $get['previous'];

        $this->buildSearchData();
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function buildSearchData()
    {
        switch (true) {
            case $this->focused == "zipCode":
                $this->searchType = "zipCode";
                $queryString = urlencode($this->zipCode);
                $this->url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" . $queryString . "&types=(regions)&language=de&components=country%3A" . $this->stickToCountry . "&key=" . $this->apiKey;
                break;
            case $this->focused == "street" && !empty($this->street):
                $this->searchType = "street";
                $queryString = ($this->zipCode ? "+" . urlencode($this->zipCode) : "")
                    . ($this->city ? "+" . urlencode($this->city) : "")
                    . "+" . urlencode($this->street)
                ;
                $this->url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=address&language=de&components=country%3A" . $this->stickToCountry . "&key=" . $this->apiKey . "&input=" . $queryString;
                break;
            case $this->focused == "city" && !empty($this->city):
                $this->searchType = "city";
                $queryString = urlencode($this->city);
                $this->url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=(cities)&language=de&components=country%3A" . $this->stickToCountry . "&key=" . $this->apiKey . "&input=" . $queryString;
                break;
            default:
                if ($this->focused == "street") {
                    $this->searchType = "street";
                    $queryString =
                        ($this->zipCode ? "+" . urlencode($this->zipCode) : "")
                        . ($this->city ? "+" . urlencode($this->city) : "")
                        . ($this->street ? "+" . urlencode($this->street) : "")
                        . ($this->houseNo ? "+" . urlencode($this->houseNo) : "")
                        . ($this->houseNoAddition ? "+" . urlencode($this->houseNoAddition) : "");
                    $this->url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=address&language=de&components=country%3A" . $this->stickToCountry . "&key=" . $this->apiKey . "&input=" . $queryString;
                } else {
                    $this->searchType = "address";
                    $queryString =
                        ($this->zipCode ? "+" . urlencode($this->zipCode) : "")
                        . ($this->city ? "+" . urlencode($this->city) : "")
                        . ($this->street ? "+" . urlencode($this->street) : "")
                        . ($this->houseNo ? "+" . urlencode($this->houseNo) : "")
                        . ($this->houseNoAddition ? "+" . urlencode($this->houseNoAddition) : "");
                    $this->url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $queryString . "&types=(geocodes)&language=de&components=country%3A" . $this->stickToCountry . "&key=" . $this->apiKey;
                }
                break;
        }
    }

    public function getSearchType()
    {
        return $this->searchType;
    }
}

class AutoCompleteSearchQuery
{
    protected $results;
    protected $response = array();

    /** @var RequestParser */
    protected $requestParser;

    public function __construct(RequestParser $requestParser)
    {
        $this->results = $this->getResponse($requestParser->getUrl());
        header("Content-type: application/json; charset=utf-8");
        $this->requestParser = $requestParser;
        $this->parseResponse();
    }

    protected function getResponse($url)
    {
        $etcDir =getcwd(). '/app/etc';
        $localConfigFile = $etcDir .'/local.xml';
        $xmlFile = simplexml_load_file($localConfigFile);
        if ($proxyServer= getenv('ZS_PROXY_SERVER')) {
            $proxyUsername = getenv('ZS_PROXY_USERNAME');
            $proxyPassword = getenv('ZS_PROXY_PASSWORD');
        } else {
            $proxyServer = (string) $xmlFile->global->proxy_server;
            $proxyUsername = (string) $xmlFile->global->proxy_username;
            $proxyPassword = (string) $xmlFile->global->proxy_password;
        }

        if ($proxyServer) {
            $context = array(
                'http' => array(
                    'proxy' => "$proxyServer",
                    'request_fulluri' => true,
                ),
                'ssl' => [
                    'verify_peer' => false,
                ]
            );

            if(($proxyUsername != '') && ($proxyPassword != '')){
                $auth = base64_encode($proxyUsername . ':' . $proxyPassword);
                $context['http']['header'] = "Proxy-Authorization: Basic $auth";
            }
            $cxContext = stream_context_create($context);
            $sFile = file_get_contents($url, False, $cxContext);
            return $sFile;
        }
        return file_get_contents($url);
    }

    protected function parseResponse()
    {
        switch ($this->requestParser->getSearchType()) {
            case "street" :
                $this->response = (new StreetResponseParser($this->results, $this->requestParser))->getParsedResponse();
                break;
            case "zipCode" :
                $this->response = (new ZipCodeResponseParser($this->results, $this->requestParser))->getParsedResponse();
                break;
            case "city" :
                $this->response = (new CityResponseParser($this->results, $this->requestParser))->getParsedResponse();
                break;
            default:
                $this->response = (new GeocodeResponseParser($this->results, $this->requestParser))->getParsedResponse();
                break;
        }
    }

    public function sendJsonResponse()
    {
        header("Content-type: application/json; charset=utf-8");
        if (is_array($this->response)) {
            $results = $this->response;
            $this->response = array(
                'results' => $results,
                'url' => $this->requestParser->getUrl(),
                'rawResults' => json_decode($this->results),
            );
        }
        echo is_array($this->response) ? json_encode($this->response) : $this->response;
    }
}

$autocomplete = new AutoCompleteSearchQuery((new RequestParser($_GET)));
$autocomplete->sendJsonResponse();
