<?php
chdir(dirname(__FILE__));

require_once '..'.DIRECTORY_SEPARATOR.'app' . DIRECTORY_SEPARATOR . 'Mage.php';
Mage::app('admin', 'store');

Mage::setIsDeveloperMode(true);
$mageErrorHandler = set_error_handler(
    function () {
        return false;
    }
);
set_error_handler(
    function ($errno, $errstr, $errfile) use ($mageErrorHandler) {
        if (substr($errfile, -19) === 'Varien/Autoload.php') {
            return null;
        }
        return is_callable($mageErrorHandler) ? call_user_func_array(
            $mageErrorHandler,
            func_get_args()
        ) : false;
    }
);
session_start();
