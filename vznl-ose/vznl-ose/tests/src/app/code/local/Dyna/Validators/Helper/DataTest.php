<?php

use PHPUnit\Framework\TestCase;

class Dyna_Validators_Helper_Data_Test extends TestCase
{

    protected function getInstance()
    {
        return Mage::helper('dyna_validators/data');
    }

    public function testCorrectHelper()
    {
        $this->assertInstanceOf('Dyna_Validators_Helper_Data',
            $this->getInstance());
    }

    public function testNumberValidation()
    {
        $this->assertTrue($this->getInstance()->validateNumber('1234'));

        $this->assertFalse($this->getInstance()->validateNumber('not-a-number'));
        $this->assertFalse($this->getInstance()->validateNumber(''));
        $this->assertFalse($this->getInstance()->validateNumber('123abc'));
        $this->assertFalse($this->getInstance()->validateNumber('abc123'));
        $this->assertFalse($this->getInstance()->validateNumber(1234)); // Regex check only valid and strings
    }

    public function testValidDateDotFormatValidation()
    {
        $this->assertTrue($this->getInstance()->validateIsDate('02.04.2010'));

        $this->assertFalse($this->getInstance()->validateIsDate(''));
        $this->assertFalse($this->getInstance()->validateIsDate('10.30.2010'));
        $this->assertFalse($this->getInstance()->validateIsDate('2010.04.02'));
        $this->assertFalse($this->getInstance()->validateIsDate('35.10.2010'));
        $this->assertFalse($this->getInstance()->validateIsDate('10.35.2010'));
        $this->assertFalse($this->getInstance()->validateIsDate('02.04'));
        $this->assertFalse($this->getInstance()->validateIsDate('02.04.10.10'));
        $this->assertFalse($this->getInstance()->validateIsDate('2.4.2010'));
        $this->assertFalse($this->getInstance()->validateIsDate('2.4.10'));
    }

    public function testInvalidDateFormatDashValidation()
    {
        $this->assertFalse($this->getInstance()->validateIsDate('10-10-2017'));
    }

    public function testMaxPortingDays()
    {
        $maxConsumer = Dyna_Validators_Helper_Data::MAX_PORTING_DAYS_CONSUMER;
        $maxBusiness = Dyna_Validators_Helper_Data::MAX_PORTING_DAYS_BUSINESS;

        // Consumer dates
        $date = new DateTime('+1 day');
        $this->assertTrue($this->getInstance()->validateMaxPortingDaysC($date->format('d-m-Y')));
        $date = new DateTime('+'.$maxConsumer.' day');
        $this->assertTrue($this->getInstance()->validateMaxPortingDaysC($date->format('d-m-Y')));
        $date = new DateTime('+'.($maxConsumer+1).' day');
        $this->assertFalse($this->getInstance()->validateMaxPortingDaysC($date->format('d-m-Y')));

        // Business dates
        $date = new DateTime('+1 day');
        $this->assertTrue($this->getInstance()->validateMaxPortingDaysB($date->format('d-m-Y')));
        $date = new DateTime('+'.$maxBusiness.' day');
        $this->assertTrue($this->getInstance()->validateMaxPortingDaysB($date->format('d-m-Y')));
        $date = new DateTime('+'.($maxBusiness+1).' day');
        $this->assertFalse($this->getInstance()->validateMaxPortingDaysB($date->format('d-m-Y')));
    }

    public function testWorkingDays()
    {
        $date = new DateTime('next monday');
        $this->assertTrue($this->getInstance()->validateWorkingDay($date->format('d-m-Y')));
        $date = new DateTime('next tuesday');
        $this->assertTrue($this->getInstance()->validateWorkingDay($date->format('d-m-Y')));
        $date = new DateTime('next wednesday');
        $this->assertTrue($this->getInstance()->validateWorkingDay($date->format('d-m-Y')));
        $date = new DateTime('next thursday');
        $this->assertTrue($this->getInstance()->validateWorkingDay($date->format('d-m-Y')));
        $date = new DateTime('next friday');
        $this->assertTrue($this->getInstance()->validateWorkingDay($date->format('d-m-Y')));
        $date = new DateTime('next saturday');
        $this->assertFalse($this->getInstance()->validateWorkingDay($date->format('d-m-Y')));
        $date = new DateTime('next sunday');
        $this->assertFalse($this->getInstance()->validateWorkingDay($date->format('d-m-Y')));
    }

    public function testValidEmailSyntax()
    {
        $allowed = ['foo@bar.com', 'foo.bar@domain.tld', 'foo@sub.subsub.domain.tld', 'foo@bar.mylongtopleveldomain'];
        foreach($allowed as $email){
            $this->assertTrue($this->getInstance()->validateEmailSyntax($email));
        }
    }

    public function testInValidEmailSyntax()
    {
        $disallowed = ['foo@bar@', 'foo'];
        foreach($disallowed as $email){
            $this->assertFalse($this->getInstance()->validateEmailSyntax($email));
        }
    }
}
