<?php

use PHPUnit\Framework\TestCase;

/**
 * Class Vznl_Catalog_Model_Product_Test
 */
class Vznl_Catalog_Model_Product_Test extends TestCase
{
    /**
     * Get model instance
     *
     * @return Vznl_Catalog_Model_Product
     */
    private function getInstance()
    {
        return Mage::getModel('vznl_catalog/product');
    }

    /**
     * Test correct model
     */
    public function testCorrectModel()
    {
        $this->assertInstanceOf('Vznl_Catalog_Model_Product', $this->getInstance());
    }

    /**
     * Test hasVoLTE
     */
    public function testHasVoLTE()
    {
        /** @var Vznl_Catalog_Model_Product $product */
        $product = $this->mockProduct(
            Vznl_Catalog_Model_Product::VOLTE_ATTRIBUTE_NAME,
            Vznl_Catalog_Model_Product::VOLTE_ATTRIBUTE_VALUE
        );

        $this->assertEquals(true, $product->hasVoLTE());
    }

    /**
     * Test isLoanRequired
     */
    public function testIsLoanRequired()
    {
        /** @var Vznl_Catalog_Model_Product $product */
        $product = $this->mockProduct(
            Vznl_Catalog_Model_Product::LOAN_INDICATED_ATTRIBUTE,
            Vznl_Catalog_Model_Product::LOAN_ATTRIBUTE_VALUE
        );

        $this->assertEquals(true, $product->isLoanRequired());
    }

    /**
     * Test isLoanRequired for ILT
     */
    public function testIsLoanRequiredForIlt()
    {
        /** @var Vznl_Catalog_Model_Product $product */
        $product = $this->mockProduct(
            Vznl_Catalog_Model_Product::LOAN_INDICATED_ATTRIBUTE,
            Vznl_Catalog_Model_Product::ILT_ATTRIBUTE_VALUE
        );

        $this->assertEquals(true, $product->isLoanRequired());
    }

    /**
     * Test isIltRequired
     */
    public function testIsIltRequired()
    {
        /** @var Vznl_Catalog_Model_Product $product */
        $product = $this->mockProduct(
            Vznl_Catalog_Model_Product::LOAN_INDICATED_ATTRIBUTE,
            Vznl_Catalog_Model_Product::ILT_ATTRIBUTE_VALUE
        );

        $this->assertEquals(true, $product->isIltRequired());
    }

    /**
     * Mock product object
     *
     * @param string $attrName
     * @param string $attrValue
     *
     * @return Vznl_Catalog_Model_Product
     */
    protected function mockProduct($attrName, $attrValue, $productType = Dyna_Catalog_Model_Type::SUBTYPE_DEVICE)
    {
        /** @var Vznl_Catalog_Model_Product $product */
        $product = $this->getMockBuilder(Vznl_Catalog_Model_Product::class)
            ->setMethods([
                'getAttributeText',
                'getType',
            ])
            ->getMock();

        $product->method('getAttributeText')
            ->with($attrName)
            ->willReturn($attrValue);

        $product->method('getType')
            ->willReturn($productType);

        return $product;
    }

    /**
     * Test isofHardwareType
     */
    public function testIsOfHardwareTypeDynaSim()
    {
        $type = Dyna_Catalog_Model_Type::SUBTYPE_SIMCARD;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertTrue($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareTypeOmniusSim()
    {
        $type = Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertTrue($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareTypeDynaDevice()
    {
        $type = Dyna_Catalog_Model_Type::SUBTYPE_DEVICE;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertTrue($product->isOfHardwareType(false));
        $this->assertTrue($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareTypeOmniusDevice()
    {
        $type = Omnius_Catalog_Model_Type::SUBTYPE_DEVICE;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertTrue($product->isOfHardwareType(false));
        $this->assertTrue($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeDynaSubscription()
    {
        $type = Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeOmniusSubscription()
    {
        $type = Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeDynaAccessory()
    {
        $type = Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertTrue($product->isOfHardwareType(false));
        $this->assertTrue($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeOmniusAccessory()
    {
        $type = Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertTrue($product->isOfHardwareType(false));
        $this->assertTrue($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeDynaAddon()
    {
        $type = Dyna_Catalog_Model_Type::SUBTYPE_ADDON;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeOmniusAddon()
    {
        $type = Omnius_Catalog_Model_Type::SUBTYPE_ADDON;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeDynaGoody()
    {
        $type = Dyna_Catalog_Model_Type::SUBTYPE_GOODY;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeOmniusGeneral()
    {
        $type = Omnius_Catalog_Model_Type::SUBTYPE_GENERAL;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeDynaGeneral()
    {
        $type = Dyna_Catalog_Model_Type::SUBTYPE_GENERAL;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeDynaPromotion()
    {
        $type = Dyna_Catalog_Model_Type::SUBTYPE_PROMOTION;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeOmniusPromotion()
    {
        $type = Omnius_Catalog_Model_Type::SUBTYPE_PROMOTION;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeOmniusNone()
    {
        $type = Omnius_Catalog_Model_Type::SUBTYPE_NONE;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeDynaNone()
    {
        $type = Dyna_Catalog_Model_Type::SUBTYPE_NONE;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }

    public function testIsOfHardwareSubTypeDynaSetting()
    {
        $type = Dyna_Catalog_Model_Type::SUBTYPE_SETTING;
        $product = $this->mockProduct('', '', [$type]);
        $this->assertFalse($product->isOfHardwareType(false));
        $this->assertFalse($product->isOfHardwareType(true));
    }
    /**
     * Test isHollandsNieuweNetworkProduct
     */
    public function testIsHollandsNieuweNetworkProduct()
    {
         $network = $this->mockProduct(
                 Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK,
                 Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK_HOLLANDSNIEUWE
        );
         
        $this->assertEquals(true, $network->isHollandsNieuweNetworkProduct());
    }
    /**
     * Test isSubscription
     */
    public function testIsSubscriptionForDyna()
    {
       $type = Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION;
       $product = $this->mockProduct('', '',[$type]);
       $this->assertTrue($product->isSubscription(true));
    }
    
    public function testIsSubscriptionForVznl()
    {
       $type = Vznl_Catalog_Model_Type::FIXED_SUBTYPE_SUBSCRIPTION;
       $product = $this->mockProduct('', '',[$type]);
       $this->assertTrue($product->isSubscription(true));
    }
    
    public function testIsSubscriptionForOmnius()
    {
       $type = Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION;
       $product = $this->mockProduct('', '',[$type]);
       $this->assertTrue($product->isSubscription(true));
    }
    
    /**
     * Test isOption isDeviceSubscription
     */
    public function testIsOption()
    {
        $option = Vznl_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON;
        $product = $this->mockProduct('', '',[$option]);
        $this->assertEquals(true, $product->isOption());
    }
   /**
     * Test isDeviceSubscription 
     */
   public function testIsDeviceSubscription()
    {
       $devicerc = Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION;
       $product = $this->mockProduct('', '',[$devicerc]);
       $this->assertEquals(true, $product->isDeviceSubscription());
    }
    
}


