<?php

use PHPUnit\Framework\TestCase;

class Vznl_Import_Helper_ImportSupportedFiles_Test extends TestCase
{
	public function getInstance()
	{
		return $obj = new Vznl_Import_Helper_ImportSupportedFiles;
	}
	public function testisValid()
	{
		$this->assertFalse($this->getInstance()->isValid('Fixed_Package.csv'));
	}

	public function testgetSupportedFiles()
	{
		$this->assertEquals(count($this->getInstance()->getSupportedFiles()), 37);
	}

	public function testgetSupportedFilesFormatted()
	{
		$this->assertInternalType('string',$this->getInstance()->getSupportedFilesFormatted());
	}
}