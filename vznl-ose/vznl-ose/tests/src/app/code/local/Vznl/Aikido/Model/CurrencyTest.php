<?php

use PHPUnit\Framework\TestCase;

class Vznl_Aikido_Model_Currency_Test extends TestCase
{

	private function getInstance()
	{
		return Mage::getModel('directory/currency');
	}

	public function testGetNumberFormat()
	{
		$currencyModel = $this->getInstance();
		$this->assertNotNull($currencyModel->getNumberFormat());
	}

	public function testGetNumberFormatNoDot()
	{
		$currencyModel = $this->getInstance();
		$this->assertNotNull($currencyModel->getNumberFormatNoDot());
	}

	public function testFormat()
	{
		$currencyModel = $this->getInstance();
		$price = $currencyModel->format(1.99,[], false);
		$this->assertEquals($price,"€ 1,99");
	}	
}