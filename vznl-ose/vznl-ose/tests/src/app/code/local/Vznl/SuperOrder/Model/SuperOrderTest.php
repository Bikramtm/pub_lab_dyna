<?php

namespace tests\src\app\code\local\Vznl\SuperOrder\Model;

use PHPUnit\Framework\TestCase;
use Vznl_Superorder_Model_Superorder;

class SuperOrderTest extends TestCase
{

    public function testGetDeliveryOrders()
    {
        $mock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods([
                'getDeliveryOrders'
            ])->getMock();

        $mock->method('getDeliveryOrders')->willReturn(new \stdClass());

        $this->assertInternalType('object',($mock)->getDeliveryOrders());
    }

    public function testIsLoanRequired()
    {
        $mock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods([
                'getCustomer',
                'getIsBusiness',
                'getDeliveryOrders',
                'isIltRequired'
            ])->getMock();

        $mock->method('getCustomer')
            ->willReturn($this->returnSelf());

        $mock->method('getIsBusiness')
            ->willReturn(true);

        $mock->method('getDeliveryOrders')
            ->willReturn($this->returnSelf());

        $mock->method('isIltRequired')
            ->willReturnOnConsecutiveCalls(true,false);

        $this->assertTrue($mock->isIltRequired());

        $this->assertFalse($mock->isIltRequired());
    }

    public function testCreditCheckCompleted()
    {
        $mock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['getPackages'])->getMock();

        $mockPackageClass  = $this->getMockBuilder('Dyna_Superorder_Model_Superorder')
        ->setMethods([
            'getCreditcheckStatus',
            'isFixed'
        ])->getMock();

        $mockPackageClass->method('getCreditcheckStatus')
            ->willReturnOnConsecutiveCalls('APPROVED','PARTIAL','INVALID','INVALID','INVALID');

        $mockPackageClass->method('isFixed')
            ->willReturnOnConsecutiveCalls(true,true,true,false,true);

        $mock->method('getPackages')
            ->willReturn([$mockPackageClass]);

        $this->assertTrue($mock->creditCheckCompleted());

        $this->assertTrue($mock->creditCheckCompleted());

        $this->assertTrue($mock->creditCheckCompleted());

        $this->assertFalse($mock->creditCheckCompleted());

        $this->assertTrue($mock->creditCheckCompleted());

    }

    public function testCreditCheckReferredOrMoreInfoNeeded(){

        $mock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['getPackages'])
            ->getMock();

        $mockPackageClass  = $this->getMockBuilder('Dyna_Superorder_Model_Superorder')
            ->setMethods([
                'getCreditcheckStatus',
                'isFixed'
            ])->getMock();

        $mockPackageClass->expects($this->any())
            ->method('getCreditcheckStatus')
            ->willReturnOnConsecutiveCalls('REFERRED','ADDITIONNAL_INFO_REQUIRED','INVALID','INVALID','INVALID');

        $mockPackageClass->expects($this->any())
            ->method('isFixed')
            ->willReturnOnConsecutiveCalls(true,true,true,false,true);

        $mock->method('getPackages')
            ->willReturn([$mockPackageClass]);

        $this->assertTrue($mock->creditCheckCompleted());

        $this->assertTrue($mock->creditCheckCompleted());

        $this->assertTrue($mock->creditCheckCompleted());

        $this->assertFalse($mock->creditCheckCompleted());

        $this->assertTrue($mock->creditCheckCompleted());
    }

    public function testHasPackageWithStatusIn()
    {
        $mock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['getPackages'])
            ->getMock();

        $mockPackageClass  = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['getData'])
            ->getMock();

        $mockPackageClass->expects($this->any())
            ->method('getData')
            ->willReturn('TEST_INDEX');

        $mock->method('getPackages')->willReturn([$mockPackageClass]);

        $this->assertFalse($mock->hasPackageWithStatusIn('creditcheck_status',['INITIAL','PENDING']));
    }

    public function testGetCorrespondanceEmail()
    {
        $mock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['getDeliveryOrders'])
            ->getMock();

        $mockDeliveryClass = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['getAdditionalEmail','getIncrementId'])
            ->getMock();

        $mockDeliveryClass->method('getAdditionalEmail')->willReturnOnConsecutiveCalls('test@testing.com;test@testing.com',null);
        $mockDeliveryClass->method('getIncrementId')->willReturn(12);

        $mock->method('getDeliveryOrders')->willReturn([$mockDeliveryClass]);


        $this->assertEquals('test@testing.com',$mock->getCorrespondanceEmail());
        $this->assertEquals('',$mock->getCorrespondanceEmail());

    }

    public function testGetGrantProductOrdersForInvalidOrderId()
    {
        $this->assertFalse((new Vznl_Superorder_Model_Superorder())->getGarantProductOrders());
    }

    protected function helperFunction(array $functions,string $helperFunction){

        $mock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods($functions)
            ->getMock();

        $mockPackageClass = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['getItems',$helperFunction])
            ->getMock();

        $mockPackageClass->method($helperFunction)
            ->willReturnOnConsecutiveCalls(true,false);

        $mockPackageClass->method('getItems')
            ->willReturnOnConsecutiveCalls([$mockPackageClass],[$mockPackageClass]);

        $mock->method($functions[0])
            ->willReturn($mockPackageClass);

        return $mock;
    }

    public function testHasMobilePackage()
    {
        $mock = $this->helperFunction(['getPackages'],'isMobile');

        $this->assertTrue($mock->hasMobilePackage());

        $this->assertFalse($mock->hasMobilePackage());
    }

    public function testHasRetention()
    {
        $mock = $this->helperFunction(['getPackages'],'isRetention');

        $this->assertTrue($mock->hasRetention());

        $this->assertFalse($mock->hasRetention());

    }

    public function testHasAcquisition(){

       $mock = $this->helperFunction(['getPackages'],'isAcquisition');

        $this->assertTrue($mock->hasAcquisition());

        $this->assertFalse($mock->hasAcquisition());
    }

    public function testHasFixed(){

        $mock = $this->helperFunction(['getPackages'],'isFixed');

        $this->assertTrue($mock->hasFixed());

        $this->assertFalse($mock->hasFixed());
    }

    public function testStoreDelivery()
    {
        $mock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['getOrders'])
            ->getMock();

        $mockOrderClass = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods([
                'getShippingAddress',
                'getDeliveryType'
            ])->getMock();

        $mockOrderClass->method('getDeliveryType')
            ->willReturnOnConsecutiveCalls('INVALID_VALUE','other_address');

        $mockOrderClass->method('getShippingAddress')
            ->willReturn($mockOrderClass);

        $mock->method('getOrders')
            ->willReturn([$mockOrderClass]);

        $this->assertTrue($mock->hasStoreDelivery());

        $this->assertFalse($mock->hasStoreDelivery());

    }

    public function testHasOtherStoreDelivery()
    {
        $mock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['getOrders'])
            ->getMock();

        $mockOrderClass = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods([
                'getShippingAddress',
                'getDeliveryType',
                'isOtherStoreDelivery'
            ])->getMock();

        $mockOrderClass->method('isOtherStoreDelivery')
            ->willReturnOnConsecutiveCalls(true,false);

        $mockOrderClass->method('getDeliveryType')
            ->willReturnOnConsecutiveCalls($mockOrderClass);

        $mockOrderClass->method('getShippingAddress')
            ->willReturn($mockOrderClass);

        $mock->method('getOrders')->willReturn([$mockOrderClass]);

        $this->assertTrue($mock->hasOtherStoreDelivery());

        $this->assertFalse($mock->hasOtherStoreDelivery());
    }

    public function testGetFullStatusText()
    {
        $mock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['getOrders','getPealOrderStatus','getOrderStatus'])
            ->getMock();

        $mockOrderClass = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods(['hasFixed','hasMobilePackage'])
            ->getMock();

        $mockOrderClass->expects($this->exactly(3))
            ->method('hasFixed')
            ->willReturnOnConsecutiveCalls(true,false,true);

        $mockOrderClass->expects($this->exactly(3))
            ->method('hasMobilePackage')
            ->willReturnOnConsecutiveCalls(true,false,false);

        $mock->expects($this->exactly(1))
            ->method('getPealOrderStatus')
            ->willReturnOnConsecutiveCalls(true,false,true);

        $mock->expects($this->exactly(2))
            ->method('getOrderStatus')
            ->willReturnOnConsecutiveCalls(true,false);

        $mock->expects($this->exactly(3))
            ->method('getOrders')
            ->willReturn([$mockOrderClass]);

        $this->assertTrue($mock->getFullStatusText());
        $this->assertFalse($mock->getFullStatusText());
        $this->assertTrue($mock->getFullStatusText());
    }
}
