<?php

namespace tests\src\app\code\local\Vznl\RemoveItemFromBasket\Adapter\Stub;

use PHPUnit\Framework\TestCase;
use Vznl_RemoveItemFromBasket_Adapter_Stub_Adapter;
use Vznl_RemoveItemFromBasket_Adapter_Stub_Factory;

class FactoryTest extends TestCase
{
    public function testFactoryCreateMethod()
    {
        $this->assertInstanceOf(Vznl_RemoveItemFromBasket_Adapter_Stub_Adapter::class,Vznl_RemoveItemFromBasket_Adapter_Stub_Factory::create());
    }

}
