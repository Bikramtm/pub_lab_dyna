<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Vznl_CreateBasket_Helper_Data_Test extends TestCase
{

    public function testGetStubClient()
    {
        $helper = Mage::helper('vznl_createbasket');
        $stubClient = new Omnius_Service_Model_Client_StubClient;
        $this->assertEquals($stubClient, $helper->getStubClient());
    }

    protected function mockGetIsStub($value)
    {
        $mock = $this->getMockBuilder(Vznl_CreateBasket_Helper_Data::class)
            ->setMethods([
                'getAdapterChoice',
                'getAdapterCall',
                'getLastName',
                'getAddressId',
                'getLogin',
                'getPassword'
            ])->getMock();
        $mock->method('getAdapterChoice')->willReturn($value);
        $mock->method('getLastName')->willReturn('lastname');
        $mock->method('getAddressId')->willReturn('101');
        $mock->method('getLogin')->willReturn('demouser');
        $mock->method('getPassword')->willReturn('demopassword');
        $mock->method('getAdapterCall')->willReturn(array());
        return $mock;
    }

    public function testCreateBasket()
    {
        $mock = $this->mockGetIsStub('Vznl_CreateBasket_Adapter_Stub_Factory');
        $this->assertInternalType('array', $mock->createBasket());

        $mock = $this->mockGetIsStub('invalid');
        $this->assertInternalType('string', $mock->createBasket());
    }
} 