<?php

use PHPUnit\Framework\TestCase;

class Vznl_DeliveryWishDates_Adapter_Stub_FactoryTest extends TestCase
{
    public function testFactoryCreated()
    {
        $adapter = Vznl_DeliveryWishDates_Adapter_Stub_Factory::create();
        $this->assertInstanceOf(Vznl_DeliveryWishDates_Adapter_Stub_Adapter::class, $adapter);
    }
}
