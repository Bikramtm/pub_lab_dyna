<?php


namespace tests\src\app\code\local\Vznl\Barcode\Model;

use Mockery;
use PHPUnit\Framework\TestCase;
use Vznl_Barcode_Model_Barcode;

class BarcodeTest extends TestCase
{
    public function testGenerate(){

        $class = new Vznl_Barcode_Model_Barcode();

        $this->assertInternalType('string',$class->generate('TEST_DATA','code128',null));

        Mockery::mock('overload:Varien_Io_File')->makePartial()
            ->shouldReceive([
                'streamOpen'  => true,
                'streamWrite' => true,
                'streamClose' => true,
                'mkdir'       => true
            ]);

        $this->assertContains('testpath/hello.png',$class->generate('TEST_DATA','code128','testpath/hello.png'));

    }
}
