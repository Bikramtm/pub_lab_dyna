<?php


namespace tests\src\app\code\local\Vznl\ReserveTelephoneNumber\Adapter\Stub;


use PHPUnit\Framework\TestCase;
use Vznl_ReserveTelephoneNumber_Adapter_Stub_Adapter;

class AdapterTest extends TestCase
{
    public function testStubCallFunction()
    {
        $this->assertInternalType('array',(new Vznl_ReserveTelephoneNumber_Adapter_Stub_Adapter())->call([]));
    }

}
