<?php

namespace tests\src\app\code\local\Vznl\GetBasket\Model\System\Config\Source\GetBasket;

use PHPUnit\Framework\TestCase;
use Vznl_GetBasket_Model_System_Config_Source_GetBasket_Adapter;

class AdapterTest extends TestCase
{
    public function testToOptionsArray()
    {
        $init = new Vznl_GetBasket_Model_System_Config_Source_GetBasket_Adapter();

        $this->assertInternalType('array',
            $init->toOptionArray());

    }

    public function testToArray()
    {
        $init = new Vznl_GetBasket_Model_System_Config_Source_GetBasket_Adapter();

        $this->assertInternalType('array',
            $init->toArray());

    }
}
