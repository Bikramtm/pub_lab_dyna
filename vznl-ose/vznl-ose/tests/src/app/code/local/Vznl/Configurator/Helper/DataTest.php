<?php

use PHPUnit\Framework\TestCase;

class Vznl_Configurator_Helper_Data_Test extends TestCase
{
    /**
     * @return Vznl_Configurator_Helper_Data
     */
    protected function getInstance()
    {
        return Mage::helper('vznl_configurator');
    }

    public function testCorrectHelper()
    {
        $helper = $this->getInstance();
        $this->assertInstanceOf('Vznl_Configurator_Helper_Data', $helper);
        $this->assertInstanceOf('Dyna_Configurator_Helper_Data', $helper);
        $this->assertInstanceOf('Omnius_Configurator_Helper_Data', $helper);
    }

    public function buildHelper()
    {
        $configuator = $this->getMockBuilder('Vznl_Configurator_Helper_Data')
            ->setMethods([
                'getPackageType'
            ])
            ->getMock();

        $configuator->method('getPackageType')
            ->willReturn(array());
        return $configuator;
    }

    public function testGetItemInformationAddon()
    {
        $configuator = $this->buildHelper();
        $product = $this->getProductMock(Dyna_Catalog_Model_Type::SUBTYPE_ADDON);
        $result = $configuator->getProductInformation($product);
        $this->assertFalse($result);
    }

    public function testGetItemInformationGeneral()
    {
        $configuator = $this->buildHelper();
        $product = $this->getProductMock(Dyna_Catalog_Model_Type::SUBTYPE_GENERAL);
        $result = $configuator->getProductInformation($product);
        $this->assertFalse($result);
    }

    public function testGetItemInformationNone()
    {
        $configuator = $this->buildHelper();
        $product = $this->getProductMock(Dyna_Catalog_Model_Type::SUBTYPE_NONE);
        $result = $configuator->getProductInformation($product);
        $this->assertFalse($result);
    }

    public function testGetItemInformationPromotion()
    {
        $configuator = $this->buildHelper();
        $product = $this->getProductMock(Dyna_Catalog_Model_Type::SUBTYPE_PROMOTION);
        $result = $configuator->getProductInformation($product);
        $this->assertFalse($result);
    }

    public function testGetItemInformationSim()
    {
        $configuator = $this->buildHelper();
        $product = $this->getProductMock(Dyna_Catalog_Model_Type::SUBTYPE_SIMCARD);
        $result = $configuator->getProductInformation($product);
        $this->assertFalse($result);
    }

    public function testGetItemInformationSubscription()
    {
        $configuator = $this->buildHelper();
        $product = $this->getProductMock(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION);
        $result = $configuator->getProductInformation($product);
        $this->assertFalse($result);
    }

    public function testGetItemInformationDevice()
    {
        $configuator = $this->buildHelper();
        $product = $this->getProductMock(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE);
        $result = $configuator->getProductInformation($product);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('entity_id', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('sections', $result);
        $sections = $result['sections'];
        $this->assertArrayHasKey('overview', $sections);
        $this->assertArrayHasKey('device', $sections);
        $this->assertArrayHasKey('entertainment', $sections);
        $this->assertArrayHasKey('connection', $sections);
        $this->assertArrayHasKey('otherspecs', $sections);
    }

    public function testGetItemInformationAccessory()
    {
        $configuator = $this->buildHelper();
        $product = $this->getProductMock(Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY);
        $result = $configuator->getProductInformation($product);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('entity_id', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('sections', $result);
        $sections = $result['sections'];
        $this->assertArrayHasKey('overview', $sections);
        $this->assertArrayHasKey('specifications', $sections);
    }

    public function testGetItemInformationDeviceWhenSectionIsNotSet()
    {
        $productStub = $this->getProductMock(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE);
        $helperMock = $this->getConfiguratorHelperDataMock($productStub);
        $result = $helperMock->getProductInformation($productStub);

        $this->assertInternalType('array', $result);
        $this->assertArrayNotHasKey('sections', $result);
    }

    public function testGetItemInformationAccessoryWhenSectionIsNotSet()
    {
        $productStub = $this->getProductMock(Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY);
        $helperMock = $this->getConfiguratorHelperDataMock($productStub);
        $result = $helperMock->getProductInformation($productStub);

        $this->assertInternalType('array', $result);
        $this->assertArrayNotHasKey('sections', $result);
    }

    /**
     * Create a mock version of the Vznl_Catalog_Model_Product class.
     * @param $productType The product type to use
     * @return Vznl_Catalog_Model_Product The mock product
     */
    protected function getProductMock($productType)
    {
        $product = $this->getMockBuilder('Vznl_Catalog_Model_Product')
            ->setMethods([
                'getType',
                'getPackageType',
                'getAttributeText',
                'getAttributesInfo',
            ])
            ->getMock();
        $product->method('getAttributeText')
            ->willReturn('packagetype');
        $product->method('getType')
            ->willReturn([$productType]);
        $product->method('getAttributesInfo')
            ->willReturn(['Name (marketingnaam)' => ['type' => 'text'], 'prodspecs_4g_lte' => ['type' => 'boolean']]);

        return $product;
    }

    /**
     * Create a mock version of the Vznl_Configurator_Helper_Data class to return
     * product information with sections not set.
     * @param $product The product
     * @return Vznl_Configurator_Helper_Data The mock product
     */
    protected function getConfiguratorHelperDataMock($product)
    {
        $mock = $this->getMockBuilder('Vznl_Configurator_Helper_Data')
            ->setMethods(['getProductInformation'])
            ->getMock();

        $mock->expects($this->once())
            ->method('getProductInformation')
            ->with($this->equalTo($product))
            ->willReturn([]);

        return $mock;
    }
}
