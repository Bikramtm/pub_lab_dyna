<?php
use PHPUnit\Framework\TestCase;

/**
 * Class Vznl_Stock_Model_Stock_Test
 */
class Vznl_Stock_Model_Stock_Test extends TestCase
{
    protected $jsonData = '[{"product_i_d":"3003111","store":"0","stock":"30","other_store_stock_list":{"item":[{"store":"500","stock":"75","distance_store":"500","store_name":null},{"store":"501","stock":"30","distance_store":"501","store_name":"VFW Amsterdam Kinkerstraat"},{"store":"503","stock":"30","distance_store":"503","store_name":"VFW Amsterdam Rokin"}]}}]';

    /**
     * Get model instance
     *
     * @return Vznl_Stock_Model_Stock
     */
    private function getInstance()
    {
        return Mage::getModel('stock/stock');
    }

    /**
     * Test of getOtherStoreAttributes
     */
    public function testGetOtherStoreAttributes()
    {
        $data = json_decode($this->jsonData, true);
        $dataItem = $data[0]['other_store_stock_list']['item'][0];

        $stock = $this->getStockModelStock();
        $result = $stock->getOtherStoreAttributes($dataItem);
        $this->assertInternalType('array', $result);

        $result = $stock->getOtherStoreAttributes(null);
        $this->assertFalse($result);
    }


    /**
     * Mock product object
     *
     * @return Vznl_Catalog_Model_Product
     */
    protected function getMockProduct()
    {
        /** @var Vznl_Catalog_Model_Product $product */
        $product = $this->getMockBuilder(Vznl_Catalog_Model_Product::class)
            ->getMock();

        return $product;
    }

    /**
     * Mock the stock model and put it as singleton
     * @param int $storeCode
     * @return mixed
     */
    private function getStockModelStock($storeCode = 0)
    {
        $mock = $this->getMockBuilder(Vznl_Stock_Model_Stock::class)
            ->setMethods([
                'getStoreCode',
            ])
            ->getMock();

        $mock->method('getStoreCode')
            ->willReturn($storeCode);

        return $mock;
    }

    /**
     * Mock accessor object
     * @return mixed
     */
    private function getAccessorMock()
    {
        $mock = $this->getMockBuilder(Dyna_Service_Model_DotAccessor::class)
            ->getMock();

        return $mock;
    }


}
