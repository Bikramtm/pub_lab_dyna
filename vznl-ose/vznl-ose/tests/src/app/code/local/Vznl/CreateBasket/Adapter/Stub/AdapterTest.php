<?php

use PHPUnit\Framework\TestCase;

class Vznl_CreateBasket_Adapter_Stub_AdapterTest extends TestCase
{
    public function testCallIsMapped()
    {
        $adapter = new Vznl_CreateBasket_Adapter_Stub_Adapter();
        $response = $adapter->call();
        $this->assertInternalType("array", $response);
    }
}
