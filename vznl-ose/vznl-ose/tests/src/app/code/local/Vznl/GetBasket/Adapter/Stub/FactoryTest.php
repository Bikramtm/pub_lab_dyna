<?php


namespace tests\src\app\code\local\Vznl\GetBasket\Stub;


use PHPUnit\Framework\TestCase;
use Vznl_GetBasket_Adapter_Stub_Adapter;
use Vznl_GetBasket_Adapter_Stub_Factory;

class FactoryTest extends TestCase
{
    public function testFactoryStaticCreate()
    {
        $this->assertInstanceOf(Vznl_GetBasket_Adapter_Stub_Adapter::class,Vznl_GetBasket_Adapter_Stub_Factory::create());
    }
}
