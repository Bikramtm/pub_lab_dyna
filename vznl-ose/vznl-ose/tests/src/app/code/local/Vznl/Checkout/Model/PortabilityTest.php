<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use PHPUnit\Framework\TestCase;
class Vznl_Checkout_Model_PortabilityTest extends TestCase{
    /**
     * @param string $quoteId
     * @param string $packageId
     * @return array
     */
    public function testretrievePortingInfo(){
        # Sample array and variables for test.
        $customerVal = 'CustomerID';
        $portInfoArr [0]['validation_type']= $customerVal;        
        $quoteId = 123; $packageId = null;
        # Mock method
        $objMock = $this->getMockBuilder(Vznl_Checkout_Model_Portability::class)
                ->setMethods(['retrievePortingInfo'])->getMock();
        $objMock->method('retrievePortingInfo')->willReturn($portInfoArr);        
        $result = $objMock->retrievePortingInfo($quoteId);
        # Check assertions.
        $this->assertInternalType('array', $result); # Check is array or not.        
        $val = $result[0]['validation_type']; # Check returning value.
        $this->assertEquals($customerVal, $val); # Check returning value.        
        $this->assertCount(1, $result); # Check Array count.
        $this->assertArrayHasKey('validation_type', $result[0]); # Check array has key.        
    }
}
