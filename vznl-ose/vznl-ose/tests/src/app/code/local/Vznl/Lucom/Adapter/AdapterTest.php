<?php

use Zend\Soap\Client;
use PHPUnit\Framework\TestCase;

class Vznl_Lucom_Adapter_Adapter_Test extends TestCase
{

    const WSDL = __DIR__ . '/../GenericDataService.wsdl';
    const XSD = __DIR__ . '/../startUBuyContractRequest.xsd';

    private function salesOrderMockery()
    {
        $mock = Mockery::mock('overload:Vznl_Checkout_Model_Sales_Order');
        $mock->shouldReceive('hasFixed')->once();
        $mock->shouldReceive('isOrderedInIndirect')->once();
    }

    /**
     * Test the request against the wsdl
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testValidAgainstWSDL()
    {
        $this->salesOrderMockery();
        $soapClient = new Client(self::WSDL, ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            ['endpoint' => 'https://myfakelucomservice.com'],
            false
        ));

        $deliveryOrder = $this->getDeliveryOrder();
        $lucomAdapter->send($deliveryOrder);

        // The 'could not connect to host' error is give after wsdl validation and will be given since the endpoint is invalid.
        $this->assertEquals('Could not connect to host', $lucomAdapter->getLastException()->getMessage());
    }

    /**
     * Test the request against the XSD
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testValidAgainstXSDConsumer()
    {
        $this->salesOrderMockery();
        $soapClient = new Client(self::WSDL, ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            ['endpoint' => 'https://myfakelucomservice.com'],
            false
        ));

        $deliveryOrder = $this->getDeliveryOrder(['isBusiness' => false]);
        $lucomAdapter->send($deliveryOrder);

        // Load the request
        $dom = new DOMDocument();
        $requestData = htmlspecialchars_decode($soapClient->getLastRequest());
        $dom->loadXML($requestData);

        /** @var DOMElement $xmlElement */
        $xmlElement = $dom->getElementsByTagName('startUBuyContractRequest')->item(0);
        $this->assertNotEquals(null, $xmlElement);
        $xmlString = $dom->saveXML($xmlElement);

        // Load the request data as dom
        $requestDataXml = new DOMDocument();
        $requestDataXml->loadXML($xmlString);
        $this->assertTrue($requestDataXml->schemaValidate(self::XSD));
    }

    /**
     * Test the request against the XSD
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testValidAgainstXSDBusiness()
    {
        $this->salesOrderMockery();
        $soapClient = new Client(self::WSDL, ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            ['endpoint' => 'https://myfakelucomservice.com'],
            false
        ));

        $deliveryOrder = $this->getDeliveryOrder(['isBusiness' => true]);
        $lucomAdapter->send($deliveryOrder);

        // Load the request
        $dom = new DOMDocument();
        $requestData = htmlspecialchars_decode($soapClient->getLastRequest());
        $dom->loadXML($requestData);

        /** @var DOMElement $xmlElement */
        $xmlElement = $dom->getElementsByTagName('startUBuyContractRequest')->item(0);
        $this->assertNotEquals(null, $xmlElement);
        $xmlString = $dom->saveXML($xmlElement);

        // Load the request data as dom
        $requestDataXml = new DOMDocument();
        $requestDataXml->loadXML($xmlString);
        $this->assertTrue($requestDataXml->schemaValidate(self::XSD));
    }

    /**
     * Test the request against the XSD, indirect sim only with device rc.
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testValidAgainstXSDConsumerNoDevice()
    {
        $this->salesOrderMockery();
        $soapClient = new Client(self::WSDL, ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            ['endpoint' => 'https://myfakelucomservice.com'],
            false
        ));

        $deliveryOrder = $this->getDeliveryOrder(['isBusiness' => false, 'noDevice' => true]);
        $lucomAdapter->send($deliveryOrder);

        // Load the request
        $dom = new DOMDocument();
        $requestData = htmlspecialchars_decode($soapClient->getLastRequest());
        $dom->loadXML($requestData);

        /** @var DOMElement $xmlElement */
        $xmlElement = $dom->getElementsByTagName('startUBuyContractRequest')->item(0);
        $this->assertNotEquals(null, $xmlElement);
        $xmlString = $dom->saveXML($xmlElement);

        // Load the request data as dom
        $requestDataXml = new DOMDocument();
        $requestDataXml->loadXML($xmlString);
        $this->assertTrue($requestDataXml->schemaValidate(self::XSD));
    }

    /**
     * Test the request against the XSD, normal sim only without device rc.
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testValidAgainstXSDConsumerNoDeviceNorDeviceRc()
    {
        $this->salesOrderMockery();
        $soapClient = new Client(self::WSDL, ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            ['endpoint' => 'https://myfakelucomservice.com'],
            false
        ));

        $deliveryOrder = $this->getDeliveryOrder(['isBusiness' => false, 'noDevice' => true, 'noDeviceRc' => true]);
        $lucomAdapter->send($deliveryOrder);

        // Load the request
        $dom = new DOMDocument();
        $requestData = htmlspecialchars_decode($soapClient->getLastRequest());
        $dom->loadXML($requestData);

        /** @var DOMElement $xmlElement */
        $xmlElement = $dom->getElementsByTagName('startUBuyContractRequest')->item(0);
        $this->assertNotEquals(null, $xmlElement);
        $xmlString = $dom->saveXML($xmlElement);

        // Load the request data as dom
        $requestDataXml = new DOMDocument();
        $requestDataXml->loadXML($xmlString);
        $this->assertTrue($requestDataXml->schemaValidate(self::XSD));
    }

    /**
     * Test the request against the XSD, with all optional data missing
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testValidAgainstXSDConsumerMissingAllOptionalData()
    {
        $this->salesOrderMockery();
        $soapClient = new Client(self::WSDL, ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            ['endpoint' => 'https://myfakelucomservice.com'],
            false
        ));

        $deliveryOrder = $this->getDeliveryOrder(['isBusiness' => false], false);
        $lucomAdapter->send($deliveryOrder);

        // Load the request
        $dom = new DOMDocument();
        $requestData = htmlspecialchars_decode($soapClient->getLastRequest());
        $dom->loadXML($requestData);

        /** @var DOMElement $xmlElement */
        $xmlElement = $dom->getElementsByTagName('startUBuyContractRequest')->item(0);
        $this->assertNotEquals(null, $xmlElement);
        $xmlString = $dom->saveXML($xmlElement);

        // Load the request data as dom
        $requestDataXml = new DOMDocument();
        $requestDataXml->loadXML($xmlString);
        $this->assertTrue($requestDataXml->schemaValidate(self::XSD));
    }

    /**
     * Test the request against the XSD, with all optional data missing
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testValidAgainstXSDBusinessMissingAllOptionalData()
    {
        $this->salesOrderMockery();
        $soapClient = new Client(self::WSDL, ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            ['endpoint' => 'https://myfakelucomservice.com'],
            false
        ));

        $deliveryOrder = $this->getDeliveryOrder(['isBusiness' => true], false);
        $lucomAdapter->send($deliveryOrder);

        // Load the request
        $dom = new DOMDocument();
        $requestData = htmlspecialchars_decode($soapClient->getLastRequest());
        $dom->loadXML($requestData);

        /** @var DOMElement $xmlElement */
        $xmlElement = $dom->getElementsByTagName('startUBuyContractRequest')->item(0);
        $this->assertNotEquals(null, $xmlElement);
        $xmlString = $dom->saveXML($xmlElement);

        // Load the request data as dom
        $requestDataXml = new DOMDocument();
        $requestDataXml->loadXML($xmlString);
        $this->assertTrue($requestDataXml->schemaValidate(self::XSD));
    }

    /**
     * Test the SUCCESS stub response
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testStubResponseValid()
    {
        $this->salesOrderMockery();
        $soapClient = new Client('https://lucom.nl/wsdl', ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            [],
            true
        ));

        $deliveryOrder = $this->getDeliveryOrder(['lastName' => 'validCustomer']);
        $result = $lucomAdapter->send($deliveryOrder);

        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('code', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('SUCCESS', $result['status']);
        $this->assertEquals('0', $result['code']);
        $this->assertEquals('https://google.nl', $result['message']);
    }

    /**
     * Test the first ERROR stub response
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testStubResponseInvalid1()
    {
        $this->salesOrderMockery();
        $soapClient = new Client('https://lucom.nl/wsdl', ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            [],
            true
        ));

        $deliveryOrder = $this->getDeliveryOrder(['lastName' => 'LucomInvalid']);
        $result = $lucomAdapter->send($deliveryOrder);

        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('code', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('ERROR', $result['status']);
        $this->assertEquals('100', $result['code']);
        $this->assertEquals('Input is not a valid XML string', $result['message']);
    }

    /**
     * Test the second ERROR stub response
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testStubResponseInvalid2()
    {
        $this->salesOrderMockery();
        $soapClient = new Client('https://lucom.nl/wsdl', ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            [],
            true
        ));

        $deliveryOrder = $this->getDeliveryOrder(['lastName' => 'LucomNotValid']);
        $result = $lucomAdapter->send($deliveryOrder);

        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('code', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('ERROR', $result['status']);
        $this->assertEquals('110', $result['code']);
        $this->assertEquals('XML validation error: \<message\>', $result['message']);
    }

    /**
     * Test the third ERROR stub response
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testStubResponseInvalid3()
    {
        $this->salesOrderMockery();
        $soapClient = new Client('https://lucom.nl/wsdl', ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            [],
            true
        ));

        $deliveryOrder = $this->getDeliveryOrder(['lastName' => 'LucomDocType']);
        $result = $lucomAdapter->send($deliveryOrder);

        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('code', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('ERROR', $result['status']);
        $this->assertEquals('120', $result['code']);
        $this->assertEquals('Document type not supported', $result['message']);
    }

    /**
     * Test the fourth ERROR stub response
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testStubResponseInvalid4()
    {
        $this->salesOrderMockery();
        $soapClient = new Client('https://lucom.nl/wsdl', ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            [],
            true
        ));

        $deliveryOrder = $this->getDeliveryOrder(['lastName' => 'LucomMissingContract']);
        $result = $lucomAdapter->send($deliveryOrder);

        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('code', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('ERROR', $result['status']);
        $this->assertEquals('200', $result['code']);
        $this->assertEquals('Missing Contract PDF', $result['message']);
    }

    /**
     * Test the fifth ERROR stub response
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testStubResponseInvalid5()
    {
        $this->salesOrderMockery();
        $soapClient = new Client('https://lucom.nl/wsdl', ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            [],
            true
        ));

        $deliveryOrder = $this->getDeliveryOrder(['lastName' => 'LucomInvalidContract']);
        $result = $lucomAdapter->send($deliveryOrder);

        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('code', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('ERROR', $result['status']);
        $this->assertEquals('210', $result['code']);
        $this->assertEquals('Invalid Contract PDF', $result['message']);
    }

    /**
     * Test the sixth ERROR stub response
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testStubResponseInvalid6()
    {
        $this->salesOrderMockery();
        $soapClient = new Client('https://lucom.nl/wsdl', ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            [],
            true
        ));

        $deliveryOrder = $this->getDeliveryOrder(['lastName' => 'LucomMissingSignature']);
        $result = $lucomAdapter->send($deliveryOrder);

        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('code', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('ERROR', $result['status']);
        $this->assertEquals('220', $result['code']);
        $this->assertEquals('Missing PDF signature field', $result['message']);
    }

    /**
     * Test the seventh ERROR stub response
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testStubResponseInvalid7()
    {
        $this->salesOrderMockery();
        $soapClient = new Client('https://lucom.nl/wsdl', ['soap_version'=>SOAP_1_1]);
        $lucomAdapter = $this->getAdapterMock(array(
            $soapClient,
            $this->getLogger(),
            [],
            true
        ));

        $deliveryOrder = $this->getDeliveryOrder(['lastName' => 'LucomError']);
        $result = $lucomAdapter->send($deliveryOrder);

        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('code', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('ERROR', $result['status']);
        $this->assertEquals('999', $result['code']);
        $this->assertEquals('Internal error / Unhandled exception', $result['message']);
    }

    protected function getAdapterMock($constructorArgs)
    {
        $mock = Mockery::mock('overload:Vznl_Checkout_Helper_Sales');
        $mock->shouldReceive('generateContract')->once();
        $adapterMock = $this->getMockBuilder('Vznl_Lucom_Adapter_Adapter')
            ->setConstructorArgs($constructorArgs)
            ->setMethods([
                'log',
                'logStub'
            ])->getMock();

        $adapterMock->method('log')
            ->willReturn(true);
        $adapterMock->method('logStub')
            ->willReturn(true);

        return $adapterMock;
    }

    /**
     * Create a mocked delivery order
     * @param array $options
     * @param boolean $optionalData
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getDeliveryOrder($options = [], $optionalData = true)
    {
        $deliveryOrderMock = $this->getMockBuilder('Vznl_Checkout_Model_Sales_Order')
            ->setMethods([
                'getCustomer',
                'getBillingAddress',
                'getPayment',
                'getDealer',
                'getCreatedAt',
                'getCustomerDob',
                'getContractantDob',
                'getCustomerValidUntil',
                'getCustomerPrefix',
                'getCustomerFirstname',
                'getCustomerMiddlename',
                'getCustomerLastname',
                'getCustomerIdType',
                'getContractantIdType',
                'getCustomerIdNumber',
                'getContractantIdNumber',
                'getCustomerAccountNumberLong',
                'getCustomerAccountNumber',
                'getCustomerAccountHolder',
                'getCompanyName',
                'getCompanyCoc',
                'getCompanyVatId',
                'getContractantPrefix',
                'getContractantFirstname',
                'getContractantMiddlename',
                'getContractantLastname',
                'getPackages',
                'getAllItems',
                'getSuperOrder',
                'getWebsiteCode',
                'getCorrespondanceEmail',
                'getAdditionalEmail'
            ])->getMock();

        $dateOfBirth = '1990-01-01';
        $prefix = 'Mr.';
        $firstName = 'F.';
        $middleName = '';
        $lastName = 'Bar';
        $deliveryOrderMock->method('getCustomer')
            ->willReturn($this->getCustomerMock($options, $optionalData));
        $deliveryOrderMock->method('getBillingAddress')
            ->willReturn($this->getBillingAddressMock($optionalData));
        $deliveryOrderMock->method('getPayment')
            ->willReturn($this->getPaymentMock());
        $deliveryOrderMock->method('getDealer')
            ->willReturn($this->getDealerMock());
        $deliveryOrderMock->method('getCreatedAt')
            ->willReturn('2017-10-10 12:00:00');
        $deliveryOrderMock->method('getCustomerDob')
            ->willReturn($dateOfBirth);
        $deliveryOrderMock->method('getContractantDob')
            ->willReturn($dateOfBirth);
        $deliveryOrderMock->method('getCustomerValidUntil')
            ->willReturn($optionalData ? '2020-01-01' : null);
        $deliveryOrderMock->method('getCustomerPrefix')
            ->willReturn($prefix);
        $deliveryOrderMock->method('getCustomerFirstname')
            ->willReturn($firstName);
        $deliveryOrderMock->method('getCustomerMiddlename')
            ->willReturn($middleName);
        $deliveryOrderMock->method('getCustomerLastname')
            ->willReturn($lastName);
        $deliveryOrderMock->method('getCustomerIdType')
            ->willReturn($optionalData ? 'P' : null);
        $deliveryOrderMock->method('getContractantIdType')
            ->willReturn($optionalData ? 'P' : null);
        $deliveryOrderMock->method('getCustomerIdNumber')
            ->willReturn($optionalData ? 'NL1234567' : null);
        $deliveryOrderMock->method('getContractantIdNumber')
            ->willReturn($optionalData ? 'NL1234567' : null);
        $deliveryOrderMock->method('getCustomerAccountNumberLong')
            ->willReturn('NL55INGB0000000000');
        $deliveryOrderMock->method('getCustomerAccountNumber')
            ->willReturn('NL55INGB0000000000');
        $deliveryOrderMock->method('getCustomerAccountHolder')
            ->willReturn('F. Bar');
        $deliveryOrderMock->method('getCompanyName')
            ->willReturn('FooBar rental');
        $deliveryOrderMock->method('getCompanyCoc')
            ->willReturn('543863464');
        $deliveryOrderMock->method('getCompanyVatId')
            ->willReturn($optionalData ? '45345' : null);
        $deliveryOrderMock->method('getContractantPrefix')
            ->willReturn($prefix);
        $deliveryOrderMock->method('getContractantFirstname')
            ->willReturn($firstName);
        $deliveryOrderMock->method('getContractantMiddlename')
            ->willReturn($middleName);
        $deliveryOrderMock->method('getContractantLastname')
            ->willReturn($lastName);
        $deliveryOrderMock->method('getPackages')
            ->willReturn([$this->getPackageMock(1)]);
        $deliveryOrderMock->method('getAllItems')
            ->willReturn($this->getItemMocks(1, $optionalData));
        $deliveryOrderMock->method('getSuperOrder')
            ->willReturn($this->getSuperOrderMock());
        $deliveryOrderMock->method('getWebsiteCode')
            ->willReturn(Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE);
        $deliveryOrderMock->method('getCorrespondanceEmail')
            ->willReturn($optionalData ? 'foo.bar@fakelu.com' : null);
        $deliveryOrderMock->method('getAdditionalEmail')
            ->willReturn('');

        return $deliveryOrderMock;
    }

    /**
     * Create a mocked customer
     * @param array $options
     * @param boolean $optionalData
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getCustomerMock($options = [], $optionalData = true)
    {
        $customerMock = $this->getMockBuilder('Vznl_Customer_Model_Customer_Customer')
            ->setMethods([
                'getIsBusiness',
                'getContractantLastname',
                'getLastname',
                'getContractantValidUntil'
            ])->getMock();

        $customerMock->method('getIsBusiness')
            ->willReturn(isset($options['isBusiness']) ? $options['isBusiness'] : false);
        $customerMock->method('getContractantLastname')
            ->willReturn(isset($options['lastName']) ? $options['lastName'] : 'lastname');
        $customerMock->method('getLastname')
            ->willReturn(isset($options['lastName']) ? $options['lastName'] : 'lastname');
        $customerMock->method('getContractantValidUntil')
            ->willReturn($optionalData ? '2020-01-01' : null);

        return $customerMock;
    }

    /**
     * Create a mocked billing address
     * @param boolean $optionalData
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getBillingAddressMock($optionalData = true)
    {
        $addressMock = $this->getMockBuilder('Mage_Sales_Model_Order_Address')
            ->setMethods([
                'getStreet1',
                'getStreet2',
                'getStreet3',
                'getPostcode',
                'getCity',
                'getCountry'
            ])->getMock();

        $addressMock->method('getStreet1')
            ->willReturn('Mercator');
        $addressMock->method('getStreet2')
            ->willReturn('2');
        $addressMock->method('getStreet3')
            ->willReturn($optionalData ? 'a' : null);
        $addressMock->method('getPostcode')
            ->willReturn('6135 KW');
        $addressMock->method('getCity')
            ->willReturn('Sittard');
        $addressMock->method('getCountry')
            ->willReturn('The Netherlands');

        return $addressMock;
    }

    /**
     * Create a mocked payment object
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getPaymentMock()
    {
        $paymentMock = $this->getMockBuilder('Mage_Sales_Model_Order_Address')
            ->setMethods([
                'getMethod'
            ])->getMock();

        $paymentMock->method('getMethod')
            ->willReturn('Rembours');

        return $paymentMock;
    }

    /**
     * Create a mocked dealer
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getDealerMock()
    {
        $dealerMock = $this->getMockBuilder('Vznl_Agent_Model_Dealer')
            ->setMethods([
                'getVfDealerCode'
            ])->getMock();

        $dealerMock->method('getVfDealerCode')
            ->willReturn(1001);

        return $dealerMock;
    }

    /**
     * Create a mocked package
     * @param $packageId the package id to use
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getPackageMock($packageId)
    {
        $packageMock = $this->getMockBuilder('Vznl_Package_Model_Package')
            ->setMethods([
                'getPackageId',
                'getSuperOrder',
                'isNumberPorting',
                'isAcquisition',
                'isRetention',
                'getTelNumber',
                'getCreatedAt'
            ])->getMock();

        $packageMock->method('getPackageId')
            ->willReturn($packageId);
        $packageMock->method('getSuperOrder')
            ->willReturn($this->getSuperOrderMock());
        $packageMock->method('isNumberPorting')
            ->willReturn(false);
        $packageMock->method('isAcquisition')
            ->willReturn(true);
        $packageMock->method('isRetention')
            ->willReturn(false);
        $packageMock->method('getTelNumber')
            ->willReturn('0600000000');
        $packageMock->method('getCreatedAt')
            ->willReturn('2017-10-10 12:00:00');

        return $packageMock;
    }

    /**
     * Create a list of item mocks
     * @param $packageId the package id to use
     * @param boolean $optionalData
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getItemMocks($packageId, $optionalData = true)
    {
        $productMocks = $this->getProductMocks($optionalData);

        $itemMocks = [];
        foreach($productMocks as $key => $productMock) {
            $itemMocks[$key] = $this->getMockBuilder('Vznl_Checkout_Model_Sales_Order_Item')
                ->setMethods([
                    'getPackageId',
                    'getProduct'
                ])->getMock();

            $itemMocks[$key]->method('getPackageId')
                ->willReturn($packageId);
            $itemMocks[$key]->method('getProduct')
                ->willReturn($productMock);
        }

        return $itemMocks;
    }

    /**
     * Create a list of product mocks
     * @param boolean $optionalData
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getProductMocks($optionalData = true)
    {
        $productMocks = [];

        $productMocks[0] = $this->getMockBuilder('Vznl_Catalog_Model_Product')
            ->setMethods([
                'getType',
                'getName',
                'getAttributeText'
            ])->getMock();

        $productMocks[1] = $this->getMockBuilder('Vznl_Catalog_Model_Product')
            ->setMethods([
                'getType',
                'getName',
            ])->getMock();

        $productMocks[2] = $this->getMockBuilder('Vznl_Catalog_Model_Product')
            ->setMethods([
                'getType',
                'getAttributeText'
            ])->getMock();

        $productMocks[0]->method('getType')
            ->willReturn([Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION]);
        $productMocks[0]->method('getName')
            ->willReturn('Red Essential 2 jaar');
        $productMocks[0]->method('getAttributeText')
            ->willReturn('24');
        $productMocks[1]->method('getType')
            ->willReturn([Vznl_Catalog_Model_Type::SUBTYPE_DEVICE]);
        $productMocks[1]->method('getName')
            ->willReturn('Samsung S9');
        $productMocks[2]->method('getType')
            ->willReturn([Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION]);
        $productMocks[2]->method('getAttributeText')
            ->willReturn($optionalData ? '24' : null);

        return $productMocks;
    }

    /**
     * Create a super order mock
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getSuperOrderMock()
    {
        $packageMock = $this->getMockBuilder('Vznl_Superorder_Model_Superorder')
            ->setMethods([
                'getWebsiteId',
                'getOrderNumber'
            ])->getMock();

        $packageMock->method('getWebsiteId')
            ->willReturn(1);
        $packageMock->method('getOrderNumber')
            ->willReturn('6000000001');

        return $packageMock;
    }

    /**
     * Create a logger and return it.
     * @return Aleron75_Magemonolog_Model_Logger The logger
     */
    protected function getLogger()
    {
        $loggerWriter = $this->getMockBuilder('Aleron75_Magemonolog_Model_Logger')
            ->setConstructorArgs(array('unittest/services/Communication/sms/' . \Ramsey\Uuid\Uuid::uuid4() . '.log'))
            ->setMethods([
                'getLogger',
                'log'
            ])->getMock();

        return $loggerWriter;
    }
}