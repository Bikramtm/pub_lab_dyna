<?php

use PHPUnit\Framework\TestCase;

class Vznl_Import_Model_Summary_Test extends TestCase
{
	
	public function invokeMethod(&$object, $methodName, array $parameters = array())
	{
	    $reflection = new \ReflectionClass(get_class($object));
	    $method = $reflection->getMethod($methodName);
	    $method->setAccessible(true);

	    return $method->invokeArgs($object, $parameters);
	}

	public function testGetFileLogName()
	{
		$obj = new Vznl_Import_Model_Summary;
		$this->assertEquals($obj->getFileLogName(), 'version_import.log');
		$this->assertNull($this->invokeMethod($obj, '_construct'));
	}
}