<?php

use PHPUnit\Framework\TestCase;

class Vznl_Import_Helper_Data_Test extends TestCase
{
	public function getInstance()
	{
		$obj = new Vznl_Import_Helper_Data;
		return $obj;
	}

	public function testgetHeader()
	{
		$this->assertArrayHasKey('required', $this->getInstance()->getHeader('product_template'));
	}

	public function testgetAllWebsitesCodes()
	{
		$this->assertEquals($this->getInstance()->getAllWebsitesCodes(), '');
	}

	public function testgetCategoriesToFiles()
	{
		$this->assertEquals(count($this->getInstance()->getCategoriesToFiles()), 40);
	}

	public function testsetProductVisibilityStrategy()
	{
		$product = Mage::getModel('catalog/product');
		$this->assertNull($this->getInstance()->setProductVisibilityStrategy($product, ['group_ids'=>[1],'strategy'=>[1]]));
	}

	public function testpreventExtraColumns()
	{
		$data = [];
		$this->assertNull($this->getInstance()->preventExtraColumns($data));
	}
}