<?php

use PHPUnit\Framework\TestCase;

class Vznl_Checkout_Helper_Data_Test extends TestCase
{
    /**
     * Test if valid helper is received
     */
    public function testValidCheckoutDataHelper()
    {
        $this->assertInstanceOf('Vznl_Checkout_Helper_Data', Mage::helper('dyna_checkout'));
    }

    /**
     * Test the auto formatting of a CTN
     */
    public function testFormatCtn()
    {
        $helper = Mage::helper('vznl_checkout');

        // With spaces
        $this->assertEquals('06 11 22 33 44', $helper->formatCtn(' 0611223344 '));
        $this->assertEquals('06 11 22 33 44', $helper->formatCtn('06 11 223 344 '));

        $this->assertEquals('06 11 22 33 44', $helper->formatCtn('0611223344'));
        $this->assertEquals('06 11 22 33 44', $helper->formatCtn('31611223344'));

        $this->assertEquals('097 11 22 33 44', $helper->formatCtn('09711223344'));
        $this->assertEquals('097 11 22 33 44', $helper->formatCtn('319711223344'));
        $this->assertEquals('0970 11 22 33 44', $helper->formatCtn('097011223344'));
        $this->assertEquals('0970 11 22 33 44', $helper->formatCtn('3197011223344'));

        $this->assertEquals('98 76 54 32 10', $helper->formatCtn('9876543210'));
        $this->assertEquals('098 76 54 32 10', $helper->formatCtn('319876543210'));

        // Without spaces
        $this->assertEquals('0611223344', $helper->formatCtn(' 0611223344 ', false));
        $this->assertEquals('0611223344', $helper->formatCtn('06 11 223 344 ', false));

        $this->assertEquals('0611223344', $helper->formatCtn('0611223344', false));
        $this->assertEquals('0611223344', $helper->formatCtn('31611223344', false));

        $this->assertEquals('09711223344', $helper->formatCtn('09711223344', false));
        $this->assertEquals('09711223344', $helper->formatCtn('319711223344', false));
        $this->assertEquals('097011223344', $helper->formatCtn('097011223344', false));
        $this->assertEquals('097011223344', $helper->formatCtn('3197011223344', false));

        $this->assertEquals('9876543210', $helper->formatCtn('9876543210', false));
        $this->assertEquals('09876543210', $helper->formatCtn('319876543210', false));

    }

    public function testLongIbanToShort()
    {
        $helper = Mage::helper('vznl_checkout');
        
        //assertInternalType
        $this->assertInternalType('string', $helper->longIbanToShort('NL46INGB0008750629'));

        //assertEquals
        $this->assertEquals('417164300', $helper->longIbanToShort('NL91ABNA0417164300'));
        $this->assertEquals('417164300', $helper->longIbanToShort('  NL91ABNA0417164300')); // With spaces

        $this->assertEquals('8750629', $helper->longIbanToShort('NL46INGB0008750629'));
        $this->assertEquals('8750629', $helper->longIbanToShort('     NL46INGB0008750629')); // With spaces

        $this->assertEquals('0750629', $helper->longIbanToShort('NL46INGB0000750629'));
        $this->assertEquals('0750629', $helper->longIbanToShort(' NL46INGB0000750629')); // With spaces
    }

    public function testIbanNumber()
    {
       $iban = 'NL91ABNA0417164300'; 
       $newiban = new Vznl_Checkout_Helper_Data();
       $result = $newiban->longIbanToShort($iban);
       //$this->assertEquals($testresult, $result);
       $this->assertInternalType('string', $result);
    }

	public function testunderscoreToCamelCase()
	{
		$helper = Mage::helper('vznl_checkout');
		$string = 'adsfa_______asdf';
		$capitalizeFirstCharacter = true;
		$result = $helper->underscoreToCamelCase($string, $capitalizeFirstCharacter);
		$this->assertInternalType('string', $result);
		$this->assertEquals('AdsfaAsdf', $result);
	}
}
       
