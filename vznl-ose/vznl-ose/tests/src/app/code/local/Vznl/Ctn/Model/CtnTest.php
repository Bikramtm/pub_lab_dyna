<?php

use PHPUnit\Framework\TestCase;

class CtnTest extends TestCase
{
    protected function getInstance()
    {
        return Mage::helper('vznl_customer');
    }

    public function testcanActivateOneOffDeal()
    {
        $mock = $this->getMockBuilder('Vznl_Ctn_Model_Ctn')
        ->setMethods(['getCustomer','getId','getIsBusiness'])
            ->getMock();

        $mock->method('getId')->willReturn(true);
        $mock->method('getIsBusiness')->willReturn(true);
        $mock->method('getCustomer')->willReturnSelf();

        $this->assertEquals($mock->canActivateOneOffDeal(4,2), 1);


    }

    public function testgetCurrentSubscription()
    {
        $mock = $this->getMockBuilder('Vznl_Ctn_Model_Ctn')
            ->setMethods(['getAdditional'])
            ->getMock();
        $mock->method('getAdditional')->willReturn(array('products'=>['type'=>'Mobile_Main','description'=>'Working']));

        $this->assertInternalType('string', $mock->getCurrentSubscription(true));

    }

    public function testGetCtn()
    {
        $mock = $this->getMockBuilder('Vznl_Ctn_Model_Ctn')
            ->setMethods(['parseCtn'])
            ->getMock();
        $mock->method('parseCtn')->willReturn('314567890');

        $this->assertInternalType('string',$mock->getCtn());

    }
}

?>

