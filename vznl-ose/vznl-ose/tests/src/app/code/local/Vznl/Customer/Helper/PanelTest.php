<?php

use PHPUnit\Framework\TestCase;

/**
 * Class Vznl_Customer_Helper_Panel_Test
 */
class Vznl_Customer_Helper_Panel_Test extends TestCase
{
    /**
     * @var array
     */
    protected $CTNs = ['491736595987', '4903013898575', '4903050566291', '087114254443'];

    /**
     * @var string
     */
    protected $message = 'Some error message';

    /**
     * Set instance of object
     *
     * @return Vznl_Customer_Helper_Panel
     */
    protected function getInstance()
    {
        return Mage::helper('vznl_customer/panel');
    }

    /**
     * Test if it uses correct class
     */
    public function testCorrectHelper()
    {
        $this->assertInstanceOf('Vznl_Customer_Helper_Panel', $this->getInstance());
    }

    /**
     * Test getPanelDataUpdateDetails
     */
    public function testGetPanelDataUpdateDetails()
    {
        $data = [
            'getFirstName' => 'name',
            'getCustomerGlobalId' => 'globalID',
            'getLastName' => 'lastname',
            'getPostalCode' => 'postalcode',
            'getHouseNo' => 'houseno',
            'getHouseAddition' => 'housenoaddition',
            'getPhoneNumber' => 'phonenumber',
            'getEmail' => 'email',
            'getBankAccountNumber' => 'iban',
        ];
        $stub = $this->gotMockedPanelHelper($data);
        $result = $stub->getPanelData('update-details');
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('customer_information', $result);
        //$this->assertArrayHasKey('message', $result);
        $this->assertArrayHasKey('vodafone', $result['customer_information']);
        $this->assertEquals($data['getCustomerGlobalId'], $result['customer_information']['vodafone'][$stub->__('Customer ID')]);
        $this->assertEquals($data['getFirstName'], $result['customer_information']['vodafone'][$stub->__('First name')]);
        $this->assertEquals($data['getLastName'], $result['customer_information']['vodafone'][$stub->__('Surname')]);
        $this->assertEquals($data['getPostalCode'], $result['customer_information']['vodafone'][$stub->__('Postal Code')]);
        $this->assertEquals($data['getHouseNo'], $result['customer_information']['vodafone'][$stub->__('House number')]);
        $this->assertEquals($data['getHouseAddition'], $result['customer_information']['vodafone'][$stub->__('House number addition')]);
        $this->assertEquals($data['getPhoneNumber'], $result['customer_information']['vodafone'][$stub->__('Phone number')]);
        $this->assertEquals($data['getEmail'], $result['customer_information']['vodafone'][$stub->__('Email address')]);
        $this->assertEquals($data['getBankAccountNumber'], $result['customer_information']['vodafone'][$stub->__('IBAN')]);
    }

    /**
     * Test getPanelDataGoodDeal
     */
    public function testGetPanelDataGoodDeal()
    {
        $stub = $this->getMockedCustomerPanelHelper();
        $result = $stub->getPanelData('good-deal');
        $this->assertInternalType('array', $result);
        $this->assertEquals(2, count($result));
        $this->assertEquals($stub->__("Good Deal"), $result['message']);
        $this->assertEquals($this->CTNs, $result['ctn']);
    }

    /**
     * Test getPanelDataNoSection
     */
    public function testGetPanelDataNoSection()
    {
        $stub = $this->getMockedCustomerPanelHelper();
        $result = $stub->getPanelData('No-Section');

        $this->assertInternalType('array', $result);
        $this->assertEquals(2, count($result));
        $this->assertEquals("No valid section requested for customer panel",
            $result['message']);
        $this->assertEquals(1, $result['error']);
    }

    /**
     * Test getPanelData with no errors
     */
    public function testGetPanelData360View()
    {
        $stub = $this->getMockedCustomerPanelHelper();
        $result = $stub->getPanelData('my-360-view');

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('error', $result);
        $this->assertArrayHasKey('ctns', $result);
        $this->assertEquals(false, $result['error']);
        $this->assertEquals($this->CTNs, $result['ctns']);
    }

    /**
     * Mock customer CTNs
     *
     * @return mixed
     */
    protected function getMockedCustomerPanelHelper()
    {
        $mock = $this->getMockBuilder(Vznl_Customer_Helper_Panel::class)
            ->setMethods([
                'getCustomerCtn'
            ])
            ->getMock();

        $mock->method('getCustomerCtn')->willReturn($this->CTNs);
        return $mock;
    }

    protected function getMockedCustomerAddress()
    {
        $mock = $this->getMockBuilder(Omnius_Customer_Model_Address::class)
            ->setMethods([
                'getData'
            ])
            ->getMock();
        $mock->method('getData')
            ->willReturn('');
        return $mock;
    }

    /**
     * getMockedCustomerData mock Dyna_Customer_Model_Customer
     * @param  array  $data
     * @return Dyna_Customer_Model_Customer
     */
    protected function getMockedCustomerData($data = [])
    {
        $toMockMethods = array_keys($data);
        $toMockMethods = array_merge($toMockMethods, [
            'getAddress'
        ]);
        $mock = $this->getMockBuilder(Dyna_Customer_Model_Customer::class)
            ->setMethods($toMockMethods)
            ->getMock();
        $mock->method('getAddress')
            ->willReturn($this->getMockedCustomerAddress());

        foreach ($data as $key => $value) {
            $mock->method($key)
                ->willReturn($value);
        }
        return $mock;
    }

    /**
     * gotMockedPanelHelper mock panel helper's getCustomer method
     * @param  array  $customerData
     * @return Vznl_Customer_Helper_Panel
     */
    protected function gotMockedPanelHelper($customerData = [])
    {
        $mock = $this->getMockBuilder(Vznl_Customer_Helper_Panel::class)
            ->setMethods(['getCustomer'])
            ->getMock();
        $mock->method('getCustomer')
            ->willReturn($this->getMockedCustomerData($customerData));
        return $mock;
    }
}
