<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Vznl_DeliveryWishDates_Helper_DataTest extends TestCase
{
    public function testGetStubClient()
    {
        $helper = Mage::helper('vznl_deliverywishdates');
        $stubClient = new Omnius_Service_Model_Client_StubClient;
        $this->assertEquals($stubClient, $helper->getStubClient());
    }

    public function testGetDeliveryWishDates()
    {
        $helper = $this->mockHelperClass("Vznl_DeliveryWishDates_Adapter_Stub_Factory");

        $this->assertInternalType(
            'array',
            $helper->getDeliveryWishDates(
                $basketId = 1, 
                $customerType = 'example',
                $deliveryMethod = 'LSP',
                $isOverstappen = true,
                $newHardware = false
            )
        );
    }

    protected function mockHelperClass($value = '')
    {
        $mock = $this->getMockBuilder(Vznl_DeliveryWishDates_Helper_Data::class)
            ->setMethods([
                'getStubClient',
                'getAdapterChoice'
            ])->getMock();
        $mock->method('getAdapterChoice')->willReturn($value);
        return $mock;
    }
}