<?php

use PHPUnit\Framework\TestCase;

class Vznl_Serviceaddress_Model_Customer_Attribute_Backend_Serviceaddress_Test extends TestCase
{
	public function testBeforeSave()
	{
		$object = new Mage_Customer_Model_Customer;
		$obj = new Vznl_Serviceaddress_Model_Customer_Attribute_Backend_Serviceaddress;
		$obj->beforeSave($object);
		$this->assertNull($object->getData('default_service_address_id'));
	}

	public function testAfterSave()
	{
		$addressMock = $this->getMockBuilder(Vznl_Serviceaddress_Helper_Address::class)
			->disableOriginalConstructor()
            ->setMethods(['getPostIndex','getId'])
            ->getMock();
        $addressMock
            ->method('getPostIndex')
            ->willReturn(1);
        $addressMock
            ->method('getId')
            ->willReturn(10);

		$mockCustomer = $this->getMockBuilder(Mage_Customer_Model_Customer::class)
            ->disableOriginalConstructor()
            ->setMethods(['getData','getAddresses'])
            ->getMock();

        $mockCustomer
            ->method('getData')
            ->willReturn(1);
        $mockCustomer
            ->method('getAddresses')
            ->willReturn([$addressMock]);

        $attribute = $this->getMockBuilder(stdClass::class)
			->setMethods(['saveAttribute'])
            ->getMock();

		$entity = $this->getMockBuilder(stdClass::class)
			->setMethods(['getEntity','getAttributeCode'])
            ->getMock();
        $entity
            ->method('getEntity')
            ->willReturn($attribute);
        $entity
            ->method('getAttributeCode')
            ->willReturn(1);

        $serviceAddress = $this->getMockBuilder(Vznl_Serviceaddress_Model_Customer_Attribute_Backend_Serviceaddress::class)
        	->disableOriginalConstructor()
            ->setMethods(['getAttribute'])
            ->getMock();

        $serviceAddress
            ->method('getAttribute')
            ->willReturn($entity);

		$this->assertNull($serviceAddress->afterSave($mockCustomer));
	}
}