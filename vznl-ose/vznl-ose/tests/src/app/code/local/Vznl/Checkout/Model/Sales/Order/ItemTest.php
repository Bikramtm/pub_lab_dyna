<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use PHPUnit\Framework\TestCase;

class Vznl_Checkout_Model_Sales_Order_ItemTest extends TestCase {

    /**
     * @param bool $customFormat
     * @return bool|string
     *
     * Return custom date format when needed
     */
    public function testgetContractEndDate() {
        $flag = false;
        $customFormat = '2019-05-27';
        if ($flag) {
            $customFormat = '2019-05-27T00:00:00-04:00';
        }
        $objMock = $this->getMockBuilder(Vznl_Checkout_Model_Sales_Order_Item::class)
                        ->setMethods(['getContractEndDate'])->getMock();
        $objMock->method('getContractEndDate')->willReturn($customFormat);
        $this->assertEquals('2019-05-27', $objMock->getContractEndDate($flag));
    }
    public function testgetFinalPrice() {        
        $taxAmountArr[0]['base_amount'] = 3.5;
        $taxAmountArr[1]['base_amount'] = 5;
        $taxAmountArr[2]['base_amount'] = 4.2;
        $taxAmountArr = serialize($taxAmountArr);

        $objMock = $this->getMockBuilder(Vznl_Checkout_Model_Sales_Order_Item::class)
                ->setMethods(['getRowTotalInclTax', 'getDiscountAmount', 'getWeeeTaxApplied'])
                ->getMock();
        $objMock->method('getRowTotalInclTax')->willReturn(100);
        $objMock->method('getDiscountAmount')->willReturn(10);
        $objMock->method('getWeeeTaxApplied')->willReturn($taxAmountArr);

        $result = $objMock->getFinalPrice();
        $this->assertInternalType('float', $result); # Check type is float or not.
        $this->assertGreaterThanOrEqual(0, $result); # Check result greater or equal than 0.
    }
}
