<?php

use PHPUnit\Framework\TestCase;

class Vznl_Checkout_Helper_Reason_Test extends TestCase
{
    /**
     * Function to test get cache object
     */
    public function testgetCache()
    {
        $reason = new Vznl_Checkout_Helper_Reason();
        $res = $this->invokeMethod($reason, 'getCache', $inputArr = []);
        $this->assertInternalType('object', $res);
    }

    /**
     * Call protected/private method of a class.
     * @param object &$object Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param parameters $parameters Array of parameters to pass into method..
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $parameters);
    }
}
?>