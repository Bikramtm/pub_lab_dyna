<?php

use PHPUnit\Framework\TestCase;

class Vznl_GetTelephoneNumbers_Adapter_Stub_AdapterTest extends TestCase
{
    public function getMockValidateAddress()
    {
        $validateAddress = array(
            'endpoint' => 'www.dynacommerce.com',
            'channel' => 'channel',
            'cty' => 'NL',
            'houseFlatNumber' => 1,
            'houseFlatExt' => 'A',
            'postCode' => '6221KL',
            'returnset' => 5,
            'consecutiveNum' => 0,
            'customerType' => 'Business',
            'footPrint' => 'ziggo',
        );
        return $validateAddress;
    }

    public function testCallIsMapped()
    {
        $adapter = new Vznl_GetTelephoneNumbers_Adapter_Stub_Adapter();
        $response = $adapter->call($this->getMockValidateAddress());
        $this->assertInternalType("array", $response);
    }

    public function testCallWhenTelephoneNo()
    {
        $adapter = new Vznl_GetTelephoneNumbers_Adapter_Stub_Adapter();
        $response = $adapter->call(Null);
        $this->assertInternalType("string", $response);
    }

}
