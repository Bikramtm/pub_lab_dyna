<?php


namespace tests\src\app\code\local\Vznl\RemoveItemFromBasket\Adapter\Peal;


use PHPUnit\Framework\TestCase;
use Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter;
use Vznl_RemoveItemFromBasket_Adapter_Peal_Factory;

class FactoryTest extends TestCase
{
	/**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testFactoryCreateFunction()
    {
    	$mock = \Mockery::mock('overload:Vznl_Agent_Helper_Data')->makePartial();
    	$mock->shouldReceive('getDealerSalesChannel')->andReturn("");

        $factory = Vznl_RemoveItemFromBasket_Adapter_Peal_Factory::create();

        $this->assertInstanceOf(Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter::class,$factory);
    }

}
