<?php

use PHPUnit\Framework\TestCase;

class Vznl_GetOrderStatus_Adapter_Peal_FactoryTest extends TestCase
{
	/**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testFactoryCreated()
    {
    	$mock = Mockery::mock('overload:Vznl_Agent_Helper_Data')->makePartial();
    	$mock->shouldReceive('getDealerSalesChannel')->andReturn("");

        $adapter = Vznl_GetOrderStatus_Adapter_Peal_Factory::create();
        $this->assertInstanceOf(Vznl_GetOrderStatus_Adapter_Peal_Adapter::class, $adapter);
    }
}
