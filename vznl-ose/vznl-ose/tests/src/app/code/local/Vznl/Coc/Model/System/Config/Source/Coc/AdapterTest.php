<?php

namespace tests\src\app\code\local\Vznl\Coc\Model\System\Config\Source\Coc;

use PHPUnit\Framework\TestCase;
use Vznl_Coc_Model_System_Config_Source_Coc_Adapter;

class AdapterTest extends TestCase
{
    public function testToOptionsArray()
    {
        $this->assertInternalType('array',(new Vznl_Coc_Model_System_Config_Source_Coc_Adapter)->toOptionArray());
    }

    public function testToArray()
    {
        $this->assertInternalType('array',(new Vznl_Coc_Model_System_Config_Source_Coc_Adapter())->toArray());
    }

}
