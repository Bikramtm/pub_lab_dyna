<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Vznl_ValidateBasket_Helper_DataTest extends TestCase
{
    public function testGetStubClient()
    {
        $helper = Mage::helper('vznl_validatebasket');
        $stubClient = new Omnius_Service_Model_Client_StubClient;
        $this->assertEquals($stubClient, $helper->getStubClient());
    }

    protected function mockGetIsStub($value)
    {
        $mock = $this->getMockBuilder(Vznl_ValidateBasket_Helper_Data::class)
            ->setMethods([
                'getAdapterChoice',
                'getLogin',
                'getPassword'
            ])->getMock();
        $mock->method('getAdapterChoice')->willReturn($value);
        $mock->method('getLogin')->willReturn('demouser');
        $mock->method('getPassword')->willReturn('demopassword');
        return $mock;
    }

    public function testGetValidateBasketIsMapped()
    {
        $mock = $this->mockGetIsStub('Vznl_ValidateBasket_Adapter_Stub_Factory');
        $this->assertInternalType('array', $mock->getValidateBasket('4000055045800'));
        $mock = $this->mockGetIsStub('invalid');
        $this->assertInternalType('string', $mock->getValidateBasket('4000055045800'));
    }
} 