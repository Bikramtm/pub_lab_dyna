<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Vznl_ValidateBasket_Adapter_Peal_AdapterTest extends TestCase
{
    private function getClient(int $httpResponse = 200, $stub = 200)
    {
        switch ($httpResponse) {
            case 200:
                $mock = new MockHandler([
                    $this->getMockResponse($stub)
                ]);
                break;
            case 400:
                $mock = new MockHandler([
                    new Response(400, [], ''),
                ]);
                break;
            case 500:
                $mock = new MockHandler([
                    new RequestException("Error Communicating with Server", new Request('GET', 'test'))
                ]);
                break;
        }
        $handler = HandlerStack::create($mock);
        return new Client(['handler' => $handler]);
    }

    private function getMockResponse($stub): Response
    {
        return new Response(200, [], file_get_contents(Mage::getModuleDir('etc', 'Vznl_ValidateBasket') . '/stubs/ValidateBasket/result.json'));
    }

    protected function mockBasketHelperFunctions($user = '', $password = '')
    {
        $mock = $this->getMockBuilder(Vznl_ValidateBasket_Helper_Data::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getLogin',
                'getPassword'
            ])
            ->getMock();
        $mock->method('getLogin')
            ->willReturn('demouser');
        $mock->method('getPassword')
            ->willReturn('demopassword');
        return $mock;
    }

    protected function mockAdapterFunctions($user = '', $password = '')
    {
        $endpoint = 'www.dynacommerce.com/';
        $channel = 'channel';
        $cty = 'NL';
        $mock = $this->getMockBuilder(Vznl_ValidateBasket_Adapter_Peal_Adapter::class)
            ->setConstructorArgs(array($this->getClient(), $endpoint, $channel, $cty))
            ->setMethods([
                'getHelper',
            ])
            ->getMock();
        $mock->method('getHelper')
            ->willReturn($this->mockBasketHelperFunctions($user, $password));
        return $mock;
    }

    public function testCallIsMapped()
    {
        $endpoint = 'www.dynacommerce.com';
        $channel = 'channel';
        $cty = 'NL';
        $adapter = new Vznl_ValidateBasket_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $mockCall = $this->mockAdapterFunctions('demouser', 'demopassword');
        $response = $mockCall->call('4000055045800', null, 'PRODUCT_ORDER');
        $this->assertInternalType("array", $response);
        $adapter = new Vznl_ValidateBasket_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $this->assertInternalType("array", $adapter->call('4000055045800', null, Vznl_ValidateBasket_Adapter_Peal_Adapter::ORDER_TYPE));
    }

    public function testBasketIdNotProvided()
    {
        $endpoint = 'www.dynacommerce.com';
        $channel = 'channel';
        $cty = 'NL';
        $adapter = new Vznl_ValidateBasket_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $this->assertContains('No basketId provided to adapter', $adapter->call(NULL));
    }

    public function testGetUrlMapped()
    {
        $endpoint = 'www.dynacommerce.com/';
        $channel = 'channel';
        $cty = 'NL';
        $adapter = new Vznl_ValidateBasket_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $url = $adapter->getUrl('4000055045800');
        $testUrl = "www.dynacommerce.com/4000055045800/validate?cty=NL&chl=channel";
        $this->assertEquals($testUrl, $url);
    }


    public function test400ResponseFromService()
    {
        $adapter = new Vznl_ValidateBasket_Adapter_Peal_Adapter($this->getClient(400), 'www.dynacommerce.com', 'channelname', 'chl');
        $mockCall = $this->mockBasketHelperFunctions();
        $response = $adapter->call('4000055045800', null,'PRODUCT_ORDER');
        $this->assertEquals($response['error'], true);
        $this->assertEquals($response['statusCode'], 400);
    }

    public function test500ResponseFromService()
    {
        $adapter = new Vznl_ValidateBasket_Adapter_Peal_Adapter($this->getClient(500), 'www.dynacommerce.com', 'channelname', 'chl');
        $mockCall = $this->mockBasketHelperFunctions();
        $response = $adapter->call('4000055045800', null, 'PRODUCT_ORDER');
        $this->assertEquals($response['error'], true);
        $this->assertContains($response['statusCode'], array(500, 0));
    }
}
