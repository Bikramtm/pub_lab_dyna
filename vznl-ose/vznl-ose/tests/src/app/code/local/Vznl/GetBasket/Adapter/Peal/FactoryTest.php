<?php


namespace tests\src\app\code\local\Vznl\GetBasket\Peal;


use PHPUnit\Framework\TestCase;
use Vznl_GetBasket_Adapter_Peal_Adapter;
use Vznl_GetBasket_Adapter_Peal_Factory;

class FactoryTest extends TestCase
{
	/**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testFactoryCreateFunction(){
    	$mock = \Mockery::mock('overload:Vznl_Agent_Helper_Data')->makePartial();
    	$mock->shouldReceive('getDealerSalesChannel')->andReturn("");

        $this->assertInstanceOf(Vznl_GetBasket_Adapter_Peal_Adapter::class,
            Vznl_GetBasket_Adapter_Peal_Factory::create());

    }

}
