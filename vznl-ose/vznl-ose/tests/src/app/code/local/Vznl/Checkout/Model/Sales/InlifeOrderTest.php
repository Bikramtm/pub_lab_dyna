<?php
use PHPUnit\Framework\TestCase;
/**
 * Class Vznl_Checkout_Model_Sales_InlifeOrderTest
 */
class Vznl_Checkout_Model_Sales_InlifeOrderTest extends TestCase
{
    /**
    * @var objInlifeOrder
    */
    private $objInlifeOrder;
    
    protected function setup(){
        $this->objInlifeOrder = new Vznl_Checkout_Model_Sales_InlifeOrder();
    }

    /**
    * Function to test order status 
    */
    public function testGetOrderStatus()
    {
        $this->objInlifeOrder->setData( array('order_status'=>'DC') );
        $this->assertNotNull( $this->objInlifeOrder->getOrderStatus() );
        $this->assertEquals( 'Discontinued' , $this->objInlifeOrder->getOrderStatus() );
    }

    public function testGetOrderDescription()
    {
        $this->objInlifeOrder->setData( array('order_description'=>'PR') );
        $this->assertNotNull( $this->objInlifeOrder->getOrderDescription() );
        $this->assertEquals( 'Provide' , $this->objInlifeOrder->getOrderDescription() );
    }
    
    public function testGetFlow()
    {
        $this->objInlifeOrder->setData( array('flow'=>'PR') );
        $this->assertNotNull( $this->objInlifeOrder->getFlow() );
        $this->assertEquals( 'Provide' , $this->objInlifeOrder->getFlow() );
    }

    public function testGetPortingStatus()
    {
        $this->objInlifeOrder->setData( array('porting_status'=>'OD') );
        $this->assertNotNull( $this->objInlifeOrder->getPortingStatus() );
        $this->assertEquals( 'Overdue' , $this->objInlifeOrder->getPortingStatus() );
    }

}