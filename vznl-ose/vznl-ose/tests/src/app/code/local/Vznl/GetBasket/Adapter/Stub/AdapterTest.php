<?php


namespace tests\src\app\code\local\Vznl\GetBasket\Stub;

use PHPUnit\Framework\TestCase;

use Vznl_GetBasket_Adapter_Stub_Adapter;

class AdapterTestTestCase extends TestCase
{
    public function testStubCallFunction()
    {
        $this->assertInternalType('array', (new Vznl_GetBasket_Adapter_Stub_Adapter())->call([]));
    }

}
