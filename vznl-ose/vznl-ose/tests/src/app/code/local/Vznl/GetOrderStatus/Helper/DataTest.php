<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Vznl_GetOrderStatus_Helper_DataTest extends TestCase
{
    public function testGetStubClient()
    {
        $helper = Mage::helper('vznl_getorderstatus');
        $stubClient = new Omnius_Service_Model_Client_StubClient;
        $this->assertEquals($stubClient, $helper->getStubClient());
    }

    protected function mockGetIsStub($value)
    {
        $mock = $this->getMockBuilder(Vznl_GetOrderStatus_Helper_Data::class)
            ->setMethods([
                'getAdapterChoice',
                'getLogin',
                'getPassword'
            ])->getMock();
        $mock->method('getAdapterChoice')->willReturn($value);
        $mock->method('getLogin')->willReturn('demouser');
        $mock->method('getPassword')->willReturn('demopassword');
        return $mock;
    }

    public function testGetOrderStatusIsMapped()
    {
        $mock = $this->mockGetIsStub('Vznl_GetOrderStatus_Adapter_Stub_Factory');
        $this->assertInternalType('array', $mock->getOrderStatus('399991603'));
        $mock = $this->mockGetIsStub('invalid');
        $this->assertInternalType('string', $mock->getOrderStatus('399991603'));
    }
} 