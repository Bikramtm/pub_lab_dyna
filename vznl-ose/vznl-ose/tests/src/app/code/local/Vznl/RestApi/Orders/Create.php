<?php

use PHPUnit\Framework\TestCase;

class OrdersCreateTest extends TestCase
{
    protected $cleanData = '{
        "is_business": false,
        "chosen_order_type_is_business": false,
        "dealer_code": "00822033",
        "contracting_party": {
            "billing_customer_id": "343385420",
            "gender": "male",
            "initials": "P",
            "last_name_prefix": "van",
            "last_name": "Jansen",
            "email": "ubuymailtest@dynalean.eu",
            "birthdate": "147394800000",
            "phone_1": "0612345678",
            "phone_2": "",
            "invoice_address": {
                "city": "Venlo",
                "country": "NLD",
                "postal_code": "5911AC",
                "street": "Deken van Oppensingel",
                "house_number": "125",
                "house_number_addition": ""
            }
        },
        "business_details": {
            "contracting_party": {
                "gender": "male",
                "initials": "P",
                "last_name_prefix": "",
                "last_name": "Jansen",
                "birthdate": "147394800000",
                "phone_1": "0612345678",
                "email": "ubuymailtest@dynalean.eu"
            },
            "company": {
                "billing_customer_id": "343386205",
                "coc_number": "14129925",
                "establish_date": "147394800000",
                "legal_form": "7",
                "name": "Company to test with",
                "vat_number": "NL0032421392",
                "address": {
                    "city": "Venlo",
                    "country": "NLD",
                    "postal_code": "5911AC",
                    "street": "Deken van Oppensingel",
                    "house_number": "125",
                    "house_number_addition": ""
                }
            }
        },
        "delivery_options": {
            "selected_delivery_option": "home_delivery",
            "home_delivery": {
                "price": "0,00",
                "address_same_as_invoice": true
            }
        },
        "privacy": {
            "terms_agreed": true,
            "marketing": true,
            "number_info": false
        },
        "payment_details": {
            "account_name": "Pim Jansen",
            "account_nr": "NL66RABO0120785498"
        },
        "identity": {
            "identity_expiry_date": "1453294800000",
            "identity_number": "PJ1234567",
            "identity_type": "PASSPORT",
            "nationality": "NL"
        },
        "payment_method": "adyen_hpp",
        "products": [{
                "package_id": "1",
                "quantity": 1,
                "salestype": "acquisition",
                "ctn": "",
                "device_sku": "2100018",
                "subscription_sku": "4100010",
                "sim_sku": ""
        }]
    }';
    protected $pointer;
    protected $endpointQuote = 'rest/quotes';
    protected $endpointOrder = 'rest/orders';
    protected $endpointOrderSubmit = 'rest/orders/[order_number]/submit';
    
    /**
     * Test happy flow shopping cart mutations
     */
    public function testHappyFlowAcquisition()
    {
        $this->pointer = json_decode($this->cleanData);
        
        // Init shopping cart
        $response = $this->execute('quote');
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertCount(0, $response->errors); // Check if there are 0 errors
        
        // Create an order from the cart
        $response = $this->execute('order');
        $this->assertNotNull($response->dynalean_order_id); // Check if order number was returned
        
        // Check if order build is done and can be retrieved
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpointOrder.'/'.$response->dynalean_order_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $data = json_decode($response);
        $this->assertNotNull($data->dynalean_order_id); 
        
        // Submit the order towards ESB
        $response = $this->execute('orderSubmit', [
            'order_number'=>$data->dynalean_order_id
        ]);
        $this->assertNotNull($response->dynalean_order_id); // Check if order number was returned
        // Check if there are 0 errors
        if(isset($response->errors)){
            $this->assertCount(0, $response->errors); 
        }
        
        $saleOrderNumber = $response->dynalean_order_id;
        // Check if order status is updated and CC is approved
        for($i=0;$i<10;$i++){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpointOrder.'/'.$saleOrderNumber);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            $data = json_decode($response);

            $ban = $data->is_business ? $data->business_details->company->billing_customer_id : $data->contracting_party->billing_customer_id;
            if(!is_null($ban))
            {
                break;
            }
            sleep(5);
        }
        $this->assertNotNull($ban);
    }
    
    /**
     * Test acquisition business customer
     */
    public function testHappyFlowBusiness()
    {
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->is_business = true;
        $this->pointer->chosen_order_type_is_business = true;
        $response = $this->execute();
        
        // Create an order from the cart
        $response = $this->execute('order');
        $this->assertNotNull($response->dynalean_order_id); // Check if order number was returned
        
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertCount(0, $response->errors); // Check if there are 0 errors
        
        // Check if order build is done and can be retrieved
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpointOrder.'/'.$response->dynalean_order_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $data = json_decode($response);
        $this->assertNotNull($data->dynalean_order_id); 
        
        // Submit the order towards ESB
        $response = $this->execute('orderSubmit', [
            'order_number'=>$data->dynalean_order_id
        ]);
        $this->assertNotNull($response->dynalean_order_id); // Check if order number was returned
        // Check if there are 0 errors
        if(isset($response->errors)){
            $this->assertCount(0, $response->errors); 
        }
        
        $saleOrderNumber = $response->dynalean_order_id;
        // Check if order status is updated and CC is approved
        for($i=0;$i<10;$i++){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpointOrder.'/'.$saleOrderNumber);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            $data = json_decode($response);
            $ban = $data->is_business ? $data->business_details->company->billing_customer_id : $data->contracting_party->billing_customer_id;
            if(!is_null($ban))
            {
                break;
            }
            sleep(5);
        }
        $this->assertNotNull($ban);
    }

    /**
     * Test retention consumer with ESB
     */
    public function testRetentionConsumer()
    {
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->products[0]->ctn = '31652936081';
        $this->pointer->products[0]->salestype = 'retention';
        $response = $this->execute();

        // Create an order from the cart
        $response = $this->execute('order');
        $this->assertNotNull($response->dynalean_order_id); // Check if order number was returned

        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertCount(0, $response->errors); // Check if there are 0 errors

        // Check if order build is done and can be retrieved
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpointOrder.'/'.$response->dynalean_order_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $data = json_decode($response);
        $this->assertNotNull($data->dynalean_order_id);

        // Submit the order towards ESB
        $response = $this->execute('orderSubmit', [
            'order_number'=>$data->dynalean_order_id
        ]);
        $this->assertNotNull($response->dynalean_order_id); // Check if order number was returned
        // Check if there are 0 errors
        if(isset($response->errors)){
            $this->assertCount(0, $response->errors);
        }

        $saleOrderNumber = $response->dynalean_order_id;
        // Check if order status is updated and CC is approved
        for($i=0;$i<10;$i++){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpointOrder.'/'.$saleOrderNumber);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            $data = json_decode($response);
            $ban = $data->is_business ? $data->business_details->company->billing_customer_id : $data->contracting_party->billing_customer_id;
            if(!is_null($ban))
            {
                break;
            }
            sleep(5);
        }
        $this->assertNotNull($ban);
    }

    /**
     * Test retention business customer with ESB
     */
    public function testRetentionBusiness()
    {
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->is_business = true;
        $this->pointer->chosen_order_type_is_business = true;
        $this->pointer->products[0]->ctn = '31652998059';
        $this->pointer->products[0]->salestype = 'retention';
        $response = $this->execute();

        // Create an order from the cart
        $response = $this->execute('order');
        $this->assertNotNull($response->dynalean_order_id); // Check if order number was returned

        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertCount(0, $response->errors); // Check if there are 0 errors

        // Check if order build is done and can be retrieved
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpointOrder.'/'.$response->dynalean_order_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $data = json_decode($response);
        $this->assertNotNull($data->dynalean_order_id);

        // Submit the order towards ESB
        $response = $this->execute('orderSubmit', [
            'order_number'=>$data->dynalean_order_id
        ]);
        $this->assertNotNull($response->dynalean_order_id); // Check if order number was returned
        // Check if there are 0 errors
        if(isset($response->errors)){
            $this->assertCount(0, $response->errors);
        }

        $saleOrderNumber = $response->dynalean_order_id;
        // Check if order status is updated and CC is approved
        for($i=0;$i<10;$i++){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpointOrder.'/'.$saleOrderNumber);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            $data = json_decode($response);
            $ban = $data->is_business ? $data->business_details->company->billing_customer_id : $data->contracting_party->billing_customer_id;
            if(!is_null($ban))
            {
                break;
            }
            sleep(5);
        }
        $this->assertNotNull($ban);
    }

    /**
     * Make the API call with the object pointer
     * @return array
     */
    protected function execute($type='quote', $params=[])
    {
        $ch = curl_init();
        switch($type)
        {
            case 'quote': $url = API_HOST.$this->endpointQuote; break;
            case 'order': $url = API_HOST.$this->endpointOrder; break;
            case 'orderSubmit': $url = API_HOST.$this->endpointOrderSubmit; break;
        }
        // Replace the params
        foreach($params as $key=> $value)
        {
            $url = str_replace('['.$key.']', $value, $url);
        }
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->pointer));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,  array('Content-Type: text/plain')); 
        $response = curl_exec ($ch);
        curl_close ($ch);
        
        $this->pointer = json_decode($response);
        return $this->pointer;
    }
}