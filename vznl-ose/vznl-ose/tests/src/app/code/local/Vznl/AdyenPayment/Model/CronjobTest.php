<?php

use PHPUnit\Framework\TestCase;

class Vznl_AdyenPayment_Model_Cronjob_Test extends TestCase
{
	/**
     * Get model instance
     *
     * @return Adyen_Payment_Model_Cronjob
     */
    private function getInstance()
    {
        return Mage::getModel('adyen/cronjob');
    }

	public function testUpdateNotificationQueue()
	{
		$mock = Mockery::mock('overload:Adyen_Payment_Model_Cronjob');
    	$mock->shouldReceive('updateNotificationQueue')->once();
		$this->assertNull($this->getInstance()->updateNotificationQueue());
	}
}