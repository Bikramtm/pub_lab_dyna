<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Vznl_CreateBasket_Adapter_Peal_AdapterTest extends TestCase
{
    private function getClient(int $httpResponse = 200, $stub = 200)
    {
        switch ($httpResponse) {
            case 200:
                $mock = new MockHandler([
                    $this->getMockResponse($stub)
                ]);
                break;
            case 400:
                $mock = new MockHandler([
                    new Response(400, [], ''),
                ]);
                break;
            case 500:
                $mock = new MockHandler([
                    new RequestException("Error Communicating with Server", new Request('GET', 'test'))
                ]);
                break;
        }
        $handler = HandlerStack::create($mock);
        return new Client(['handler' => $handler]);
    }

    private function getMockResponse($stub): Response
    {
        return new Response(200, [], file_get_contents(Mage::getModuleDir('etc', 'Vznl_CreateBasket') . '/stubs/CreateBasket/result.json'));
    }

    protected function mockBasketHelperFunctions()
    {
        $mock = $this->getMockBuilder(Vznl_CreateBasket_Helper_Data::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getLogin',
                'getPassword'
            ])
            ->getMock();

        $mock->method('getLogin')
            ->willReturn('demouser');
        $mock->method('getPassword')
            ->willReturn('demopassword');
        return $mock;
    }

    protected function mockAdapterFunctions()
    {
        $endpoint = 'www.dynacommerce.com/';
        $channel = 'channel';
        $cty = 'NL';
        $mock = $this->getMockBuilder(Vznl_CreateBasket_Adapter_Peal_Adapter::class)
            ->setConstructorArgs(array($this->getClient(), $endpoint, $channel, $cty))
            ->setMethods([
                'getHelper',
            ])
            ->getMock();
        $mock->method('getHelper')
            ->willReturn($this->mockBasketHelperFunctions());
        return $mock;
    }

    public function testCallIsMapped()
    {
        $endpoint = 'www.dynacommerce.com';
        $channel = 'channel';
        $cty = 'NL';
        $adapter = new Vznl_CreateBasket_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $mockCall = $this->mockAdapterFunctions();
        $validateAddress = array('addressId' => '101', 'lastName' => 'lastname');
        $response = $mockCall->call($validateAddress['lastName'], $validateAddress['addressId']);
        $this->assertInternalType("array", $response);
        $adapter->call($validateAddress['lastName'], $validateAddress['addressId']);
        $this->assertInternalType("array", $response);
    }

    public function testGetUrlMapped()
    {
        $endpoint = 'www.dynacommerce.com';
        $channel = 'channel';
        $cty = 'NL';
        $adapter = new Vznl_CreateBasket_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $url = $adapter->getUrl('lastname', '1001');
        $testUrl = "www.dynacommerce.com?cty=NL&chl=channel&orderType=PRODUCT_ORDER&lastname=lastname&addressid=1001";
        $this->assertEquals($testUrl, $url);
    }

    public function test400ResponseFromService()
    {
        $adapter = new Vznl_CreateBasket_Adapter_Peal_Adapter($this->getClient(400), 'www.dynacommerce.com', 'channelname', 'chl');
        $validateAddress = array('addressId' => '101', 'lastName' => 'lastname');
        $response = $adapter->call('lastname', '1001');
        $this->assertEquals($response['error'], true);
        $this->assertEquals($response['code'], 400);
    }

    public function test500ResponseFromService()
    {
        $this->expectException(Exception::class);
        $adapter = new Vznl_CreateBasket_Adapter_Peal_Adapter($this->getClient(500), 'www.dynacommerce.com', 'channelname', 'chl');
        $validateAddress = array('addressId' => '101', 'lastName' => 'lastname');
        $adapter->call($validateAddress);
        $response = $adapter->call('lastname', '1001');
        $this->assertEquals($response['error'], true);
        $this->assertContains($response['code'], array(500, 0));
    }
}
