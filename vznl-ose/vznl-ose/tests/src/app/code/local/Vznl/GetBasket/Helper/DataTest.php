<?php


namespace tests\src\app\code\local\Vznl\GetBasket\Helper;


use PHPUnit\Framework\TestCase;
use Vznl_GetBasket_Helper_Data;

class DataTest extends TestCase
{
    protected function helperFunction()
    {
        $class = (new Vznl_GetBasket_Helper_Data);

        return $class;
    }

    public function testGetAdapterChoice()
    {
        $mock = $this->helperFunction();

        $this->assertInternalType('null',$mock->getAdapterChoice());
    }

    public function testGetEndPoint()
    {
        $mock = $this->helperFunction();

        $this->assertEquals('',$mock->getEndPoint());
    }

    public function testGetLogin()
    {
        $mock = $this->helperFunction();

        $this->assertEquals('',$mock->getLogin());
    }

    public function testGetPassword()
    {
        $mock = $this->helperFunction();

        $this->assertEquals('',$mock->getPassword());
    }

    public function testGetCountry()
    {
        $mock = $this->helperFunction();

        $this->assertEquals('',$mock->getCountry());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetChannel()
    {
        $mock1 = \Mockery::mock('overload:Vznl_Agent_Helper_Data')->makePartial();
        $mock1->shouldReceive('getDealerSalesChannel')->andReturn("");

        $mock = $this->helperFunction();

        $this->assertEquals('',$mock->getChannel());
    }

    public function testGetStubClient()
    {
        $mock = $this->helperFunction();

        $this->assertInternalType('object',$mock->getStubClient());
    }

    public function testGetVerify()
    {
        $mock = $this->helperFunction();

        $this->assertInternalType('boolean',$mock->getVerify());
    }

    public function testGetProxy()
    {
        $mock = $this->helperFunction();

        $this->assertInternalType('boolean',$mock->getProxy());
    }

    public function testGetLogger()
    {
        $mock = $this->helperFunction();

        $this->assertInstanceOf('Omnius_Service_Model_Logger',$mock->getLogger());
    }

}
