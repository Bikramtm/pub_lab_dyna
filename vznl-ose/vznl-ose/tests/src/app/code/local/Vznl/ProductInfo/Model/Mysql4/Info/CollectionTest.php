<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

use PHPUnit\Framework\TestCase;

class Vznl_ProductInfo_Model_Mysql4_Info_Collection_Test extends TestCase
{
	/**
      * @runInSeparateProcess
      * @preserveGlobalState disabled
      */
	public function testConstruct()
	{
		$mock = Mockery::mock('overload:Mage_Core_Model_Resource_Db_Collection_Abstract');
		$mock->shouldReceive('setConnection')->once();
		$mock->shouldReceive('_init')->once();
		$model = new Vznl_ProductInfo_Model_Mysql4_Info_Collection;
		$this->assertNull($model->_construct());
	}
}