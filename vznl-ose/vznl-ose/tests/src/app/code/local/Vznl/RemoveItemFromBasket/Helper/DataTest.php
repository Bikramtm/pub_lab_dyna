<?php

namespace tests\src\app\code\local\Vznl\RemoveItemFromBasket\Helper;

use PHPUnit\Framework\TestCase;

class DataTest extends TestCase
{

    private function helperFunction($returnValue){

        $mockDataClass = $this->getMockBuilder(\Vznl_RemoveItemFromBasket_Helper_Data::class)
            ->setMethods(['getAdapterChoice'])
            ->getMock();

        $mockDataClass->method('getAdapterChoice')->willReturn($returnValue);

        return $mockDataClass;

    }

    public function testRemoveItemFromBasketForInvalidAdapter()
    {
        $this->assertInternalType('string',$this->helperFunction('invalid_adapter_config_stub')->removeitemfrombasket('735465364565'));
    }

    public function testRemoveItemFromBasketForValidAdapter()
    {
        $this->assertInternalType('array',$this->helperFunction('Vznl_RemoveItemFromBasket_Adapter_Stub_Factory')->removeitemfrombasket('735465364565'));
    }

}
