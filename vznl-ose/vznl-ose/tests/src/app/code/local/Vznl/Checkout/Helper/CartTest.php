<?php

use PHPUnit\Framework\TestCase;

class Vznl_Checkout_Helper_Cart_Test extends TestCase
{
    protected function getInstance()
    {
        return Mage::helper('vznl_checkout/cart');
    }
    
    public function testCorrectHelper()
    {
        $helper = $this->getInstance();
        $this->assertInstanceOf('Vznl_Checkout_Helper_Cart', $helper);
        $this->assertInstanceOf('Dyna_Checkout_Helper_Cart', $helper);
    }

    protected function getCustomerModel()
    {
        $mock = $this->getMockBuilder(Dyna_Customer_Model_Customer::class)
                ->setMethods([
                    'save'
                ])->getMock();
        $mock->method('save')
             ->willReturn(true);
        return $mock;
    }

    public function testcreateDeProspectFromCart()
    {
        $var = array (
           'firstname' => 'John',
           'lastname' => 'De',
           'email_send' => 'john.de@gmail.com',
           'telephone' => '0987654321',
           'guestCustomerIsSoho'=>false
        );
        
        $mock = $this->getMockBuilder(Vznl_Checkout_Helper_Cart::class)
                ->setMethods([
                    'getCustomerModel'
                ])->getMock();
        $mock->method('getCustomerModel')
             ->willReturn($this->getCustomerModel());
        
        $result = $mock->createDeProspectFromCart($var);
        $this->assertInstanceOf('Dyna_Customer_Model_Customer', $result);
        
        $resultData = $result->getData();
        $this->assertInternalType('array', $resultData);
        
        $this->assertEquals($var['firstname'], $resultData['firstname']);
        $this->assertEquals($var['lastname'], $resultData['lastname']);
        $this->assertEquals($var['email_send'], $resultData['additional_email']);
        $this->assertEquals($var['telephone'], $resultData['phone_number']);
    }

}

