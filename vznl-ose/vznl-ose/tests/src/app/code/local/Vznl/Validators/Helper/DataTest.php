<?php


namespace tests\src\app\code\local\Vznl\Validators\Helper;

use Mage;
use Mockery;
use PHPUnit\Framework\TestCase;
use Vznl_Validators_Helper_Data;

class DataTest extends TestCase
{
    public function testValidateIsDate()
    {
        $this->assertTrue((new Vznl_Validators_Helper_Data)->validateIsDate('12-12-2009','-'));

        $this->assertFalse((new Vznl_Validators_Helper_Data)->validateIsDate('12.12.2009','-'));

        $this->assertFalse((new Vznl_Validators_Helper_Data)->validateIsDate('12.12.1990000','.'));

        $this->assertFalse((new Vznl_Validators_Helper_Data)->validateIsDate(null,'.'));

    }

    public function testValidateNLPostcode()
    {
        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateNLPostcode('1111QQ'));

        $this->assertFalse((new Vznl_Validators_Helper_Data())->validateNLPostcode('11111111QQ'));

        $this->assertFalse((new Vznl_Validators_Helper_Data())->validateNLPostcode('111QQ'));

        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateNLPostcode('1111qq'));
    }

    public function testGetRegexesForDummyEmail()
    {
        $this->assertInternalType('array',(new Vznl_Validators_Helper_Data)->getRegexesForDummyEmail());

        $this->assertGreaterThan(10,count((new Vznl_Validators_Helper_Data)->getRegexesForDummyEmail()));
    }

    public function testGetDisallowedAdresses()
    {
        $this->assertInternalType('array',(new Vznl_Validators_Helper_Data)->getDisallowedAdresses());
    }

    public function testValidateIsNLDate(){

        $this->assertTrue((new Vznl_Validators_Helper_Data)->validateIsNLDate('12-12-2009'));

        $this->assertFalse((new Vznl_Validators_Helper_Data)->validateIsNLDate('12.12.2009'));

        $this->assertFalse((new Vznl_Validators_Helper_Data)->validateIsNLDate(null));

    }

    protected function helperFunction(){

        $mock =  $this->getMockBuilder('Vznl_Validators_Helper_Data')
            ->setMethods([
                'testRegex',
                'validateEmailSyntax',
                'getRegexesForDummyEmail',
                'getDisallowedAdresses',
                'getCache',
                'load',
                'validate18IBAN',
                'validateBicIban',
                'convertBicToIban',
                'save',
                'getTtl'
            ])->getMock();

        $mock->method('testRegex')
            ->willReturnOnConsecutiveCalls(true,false,false);

        return $mock;
    }

    public function testValidateAddonCode(){

        $mock = $this->helperFunction();

        $this->assertFalse(($mock)->validateAddonCode('crd'));

        $this->assertTrue(($mock)->validateAddonCode('crd'));

    }

    public function testValidateAddonAttributes()
    {
        $mock = $this->helperFunction();

        $this->assertFalse($mock->validateAddonAttributes([['name'=>'test','value'=>true]],'test'));

        $this->assertTrue($mock->validateAddonAttributes([['name'=>'test']],'test'));

        $this->assertTrue($mock->validateAddonAttributes([['name'=>'test','value'=>true]],'test'));

    }

    public function testValidateName()
    {
        $mock = $this->helperFunction();

        $this->assertFalse($mock->validateName('VALID_NAME'));

        $this->assertTrue($mock->validateName('INVALID_NAME'));
    }

    public function testIsValidEmail(){

        $regexMock = $this->helperFunction();

        $regexMock->method('validateEmailSyntax')->willReturnOnConsecutiveCalls(true,true,false);

        $regexMock->method('getRegexesForDummyEmail')->willReturn([1]);

        $this->assertFalse($regexMock->isValidEmail('valid@valid-email.com'));

        $this->assertTrue($regexMock->isValidEmail('invalid@invalid-email.com'));

        $this->assertFalse($regexMock->isValidEmail('invalid@invalid-email.com'));

    }


    public function testValidateForeignPostcode(){

        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateForeignPostcode('12345','DEU'));

        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateForeignPostcode('1234','BEL'));

        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateForeignPostcode('1234','BE'));

        $this->assertFalse((new Vznl_Validators_Helper_Data())->validateForeignPostcode('1234567','DEU'));

        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateForeignPostcode('1234567','DEFAULT'));
    }

    public function testValidateAPIIdType()
    {
        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateAPIIdType('P','NL1234567','NL'));

        $this->assertFalse((new Vznl_Validators_Helper_Data())->validateAPIIdType('P','NL123456778777777','NL'));

        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateAPIIdType('N','NL1234567','NL'));

        $this->assertFalse((new Vznl_Validators_Helper_Data())->validateAPIIdType('N','NL12345677877','NL'));

        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateAPIIdType('P','N12345678','NL'));

        $this->assertFalse((new Vznl_Validators_Helper_Data())->validateAPIIdType('P','112345678','NL'));

        $this->assertFalse((new Vznl_Validators_Helper_Data())->validateAPIIdType('R','NL123456789','NL'));

        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateAPIIdType('P','NL123456789','NLP'));

        $this->assertFalse((new Vznl_Validators_Helper_Data())->validateAPIIdType('N','NL123456789','NLP'));

    }

    public function testValidateIsNotEmpty()
    {
        $this->assertTrue((new Vznl_Validators_Helper_Data())->validateIsNotEmpty('TEST_DATA'));
        $this->assertFalse((new Vznl_Validators_Helper_Data())->validateIsNotEmpty(null));
    }

    public function testGetRegexesForDisallowedAddresses()
    {
        $mock = $this->helperFunction();

        $mock->method('getDisallowedAdresses')->willReturn(['test@gmail.com','testing@gmail.com','dummy@dummy.com']);

        $this->assertArrayHasKey('/^test@gmail\.com$/',array_flip($mock->getRegexesForDisallowedAddresses()));

        $this->assertArrayHasKey('/^testing@gmail\.com$/',array_flip($mock->getRegexesForDisallowedAddresses()));

        $this->assertArrayHasKey('/^dummy@dummy\.com$/',array_flip($mock->getRegexesForDisallowedAddresses()));
    }

    public function testValidateNLIBAN()
    {
        $mock = $this->helperFunction();

        $mock->method('getTtl')
            ->willReturn(true);

        $mock->method('save')
            ->willReturn(true);

        $mock->method('load')
            ->willReturnOnConsecutiveCalls(serialize('TESTING'),false);

        $mock->method('getCache')
            ->willReturn($mock);

        $mock->method('validate18IBAN')
            ->willReturn(true);

        $mock->method('validateBicIban')
            ->willReturn(true);

        $mock->method('convertBicToIban')
            ->willReturn(true);

        $this->assertEquals('TESTING',$mock->validateNLIBAN('TEST'));

        $this->assertTrue($mock->validateNLIBAN('123456789123456789'));

        $this->assertTrue($mock->validateNLIBAN('123456789'));

        $this->assertTrue($mock->validateNLIBAN('12345678'));

        $this->assertTrue($mock->validateNLIBAN('1234567'));

        $this->assertFalse($mock->validateNLIBAN('123456'));
    }

}
