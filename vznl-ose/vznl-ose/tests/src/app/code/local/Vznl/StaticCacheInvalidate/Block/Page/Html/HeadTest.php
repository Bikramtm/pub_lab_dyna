<?php

use PHPUnit\Framework\TestCase;

class Vznl_StaticCacheInvalidate_Block_Page_Html_Head_Test extends TestCase
{
	public function invokeMethod(&$object, $methodName, array $parameters = array())
	{
	    $reflection = new \ReflectionClass(get_class($object));
	    $method = $reflection->getMethod($methodName);
	    $method->setAccessible(true);

	    return $method->invokeArgs($object, $parameters);
	}

	public function testRemoveByType()
	{
		$obj = new Vznl_StaticCacheInvalidate_Block_Page_Html_Head;
		$obj->_data = '1';
		$this->assertInstanceOf('Vznl_StaticCacheInvalidate_Block_Page_Html_Head', $obj->removeByType('1'));
		$this->assertEquals($this->invokeMethod($obj, '_prepareStaticAndSkinElements', array('xml',[],[])), '');
	}
}