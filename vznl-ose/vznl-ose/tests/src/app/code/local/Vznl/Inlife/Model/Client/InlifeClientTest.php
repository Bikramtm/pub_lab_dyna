<?php

namespace tests\src\app\code\local\Vznl\Inlife\Model\Client;

use PHPUnit\Framework\TestCase;
use Vznl_Inlife_Model_Client_InlifeClient;

class InLifeClientTest extends TestCase
{
    protected function helperFunction()
    {
        $mock = $this->getMockBuilder(Vznl_Inlife_Model_Client_InlifeClient::class)
            ->setMethods([
                'GetAddons',
                'addAddOn',
                'GetProductSettings',
                'GetAssignedProductDetails',
                'changeSIM'
            ])->getMock();

        return $mock;
    }

    public function testGetInlifeAddons()
    {
        $mock = $this->helperFunction();

        $mock->method('GetAddons')
            ->willReturn(['TEST' => true]);

       $this->assertArrayHasKey('TEST',$mock->getInlifeAddons('TEST',true));
    }

    public function testAddInlifeAddon()
    {
        $nonExistingClass = $this->getMockBuilder('addNode')
            ->setMethods(['addChild','GetAddons'])
            ->getMock();

        $nonExistingClass->method('addChild')
            ->willReturn(true);

        $mock = $this->helperFunction();

        $mock->method('addAddOn')
            ->willReturn(['TEST' => true]);

        $response = $mock->addInlifeAddon(1,11,1,1,['billing_offer_code'=>true,'AddOnsData'=>true,'vom_addon' =>true]);

       $this->assertInternalType('array',$response);
    }

    public function testGetInlifeProductSettings()
    {
        $mock = $this->helperFunction();

        $mock->method('GetProductSettings')
            ->willReturn(true);

        $this->assertTrue($mock->getInlifeProductSettings(12345));

    }

    public function testGetAssignedInlifeProductDetails()
    {
        $mock = $this->helperFunction();

        $mock->method('GetAssignedProductDetails')
            ->willReturn(true);

        $this->assertTrue($mock->getAssignedInlifeProductDetails(12345));

    }

    public function testChangeInlifeSim()
    {
        $mock = $this->helperFunction();

        $mock->method('changeSIM')
            ->willReturn(true);

        $this->assertTrue($mock->changeInlifeSim(1,2,3,4));
    }


}
