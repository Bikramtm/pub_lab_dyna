<?php

namespace tests\src\app\code\local\Vznl\Utility\Model;

use PHPUnit\Framework\TestCase;
use Vznl_Utility_Helper_Stubs;

class StubTest extends TestCase
{
    /**
     * @param string $params
     * @param string $arrayKey
     * @return array
     */
    protected function webCommandHelperFunction(string $params,string $arrayKey)
    {
        $helper = (new Vznl_Utility_Helper_Stubs)->parseWebCommand($params);

        return $helper[$arrayKey];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testExecuteForException()
    {

        $dummyMock = $this->getMockBuilder('DummyClass')
            ->setMethods(['setOrder','setPageSize','getLastItem'])
            ->getMock();

        $dummyMock
            ->method('getLastItem')
            ->willReturn('hello');
        $dummyMock
            ->method('setPageSize')
            ->willReturn($dummyMock);

        $dummyMock
            ->method('setOrder')
            ->willReturn($dummyMock);

        $data = [
            'stub'           => 5,
            'orderNumber'    => 123,
            'packageId'      => 123,
            'additionalData' => 123
        ];

        $this->assertContains('Invalid',
            (new Vznl_Utility_Helper_Stubs)->execute($data));

    }

    public function testParseWebCommand()
    {

        $this->assertEquals('12345',
            $this->webCommandHelperFunction('-s 12345','stub'));

        $this->assertEquals('12345',
            $this->webCommandHelperFunction('-o 12345','orderNumber'));

        $this->assertEquals('12345',
            $this->webCommandHelperFunction('-p 12345','packageId'));

        $this->assertEquals('12345',
            $this->webCommandHelperFunction('-u 12345','uri'));

        $this->assertEquals('12345',
            $this->webCommandHelperFunction('-a 12345','additionalData'));

    }

}
