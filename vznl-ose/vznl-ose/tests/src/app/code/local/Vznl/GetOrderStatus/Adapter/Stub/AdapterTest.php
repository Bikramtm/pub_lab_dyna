<?php

use PHPUnit\Framework\TestCase;

class Vznl_GetOrderStatus_Adapter_Stub_AdapterTest extends TestCase
{
    public function testCallIsMapped()
    {
        $adapter = new Vznl_GetOrderStatus_Adapter_Stub_Adapter();
        $response = $adapter->call('399991603');
        $this->assertInternalType("array", $response);
    }

    public function testCallWhenOrderNumber()
    {
        $adapter = new Vznl_GetOrderStatus_Adapter_Stub_Adapter();
        $response = $adapter->call(Null);
        $this->assertInternalType("string", $response);
    }

}
