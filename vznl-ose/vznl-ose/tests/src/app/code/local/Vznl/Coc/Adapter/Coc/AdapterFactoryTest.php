<?php


namespace tests\src\app\code\local\Vznl\Coc\Adapter\Coc;


use Mockery;
use PHPUnit\Framework\TestCase;
use Vznl_Coc_Adapter_Coc_Adapter;
use Vznl_Coc_Adapter_Coc_AdapterFactory;

class AdapterFactoryTest extends TestCase
{
    public function testStaticCreate()
    {
        Mockery::mock('overload:Vznl_Coc_Helper_Data')
            ->shouldReceive('getCocConfig')
            ->andReturn(true);

        $this->assertInstanceOf(Vznl_Coc_Adapter_Coc_Adapter::class,Vznl_Coc_Adapter_Coc_AdapterFactory::create());
    }
}
