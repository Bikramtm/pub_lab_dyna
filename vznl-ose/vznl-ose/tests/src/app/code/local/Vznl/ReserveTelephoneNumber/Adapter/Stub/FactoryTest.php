<?php


namespace tests\src\app\code\local\Vznl\ReserveTelephoneNumber\Adapter\Stub;


use PHPUnit\Framework\TestCase;
use Vznl_ReserveTelephoneNumber_Adapter_Stub_Adapter;
use Vznl_ReserveTelephoneNumber_Adapter_Stub_Factory;

class FactoryTest extends TestCase
{
    public function testFactoryStaticCreate()
    {
        $this->assertInstanceOf(Vznl_ReserveTelephoneNumber_Adapter_Stub_Adapter::class,Vznl_ReserveTelephoneNumber_Adapter_Stub_Factory::create());
    }

}
