<?php

use PHPUnit\Framework\TestCase;

class Vznl_Checkout_Helper_Email_Test extends TestCase
{
    /**
     * Function to test date return by stripTimeFromDate
     */
    public function teststripTimeFromDate()
    {
        $email = new Vznl_Checkout_Helper_Email();
        $inputDate = array('date'=>'24-03-2019');
        $res = $this->invokeMethod($email, 'stripTimeFromDate', $inputDate);
        $this->assertEquals($inputDate['date'], $res);
        $this->assertInternalType('string', $res);
    }

    /**
     * Call protected/private method of a class.
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param parameters $parameters Array of parameters to pass into method..
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $parameters);
    }
}
?>