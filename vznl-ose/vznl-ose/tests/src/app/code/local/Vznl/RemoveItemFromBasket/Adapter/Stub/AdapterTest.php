<?php

namespace tests\src\app\code\local\Vznl\RemoveItemFromBasket\Adapter\Stub;
use PHPUnit\Framework\TestCase;
use Vznl_RemoveItemFromBasket_Adapter_Stub_Adapter;

class AdapterTest extends TestCase
{
    public function testCallForNullBasketId(){

        $adapter = new Vznl_RemoveItemFromBasket_Adapter_Stub_Adapter();

        $this->assertInternalType('string',$adapter->call(null));
    }

    public function testCall()
    {
        $adapter = new Vznl_RemoveItemFromBasket_Adapter_Stub_Adapter();

        $this->assertInternalType('array',$adapter->call('4252425242'));
    }


}
