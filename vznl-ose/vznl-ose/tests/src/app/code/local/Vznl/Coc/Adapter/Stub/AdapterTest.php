<?php

namespace tests\src\app\code\local\Vznl\Coc\Adapter\Stub;

use PHPUnit\Framework\TestCase;
use Vznl_Coc_Adapter_Stub_Adapter;

class AdapterTest extends TestCase
{
    public function testSend()
    {
        $this->assertInternalType('array',(new Vznl_Coc_Adapter_Stub_Adapter)->send(1234));
    }
}
