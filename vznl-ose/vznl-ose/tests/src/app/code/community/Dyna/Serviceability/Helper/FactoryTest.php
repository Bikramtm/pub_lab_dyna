<?php
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

/**
 * Class Dyna_Serviceability_Helper_ServiceabilityFactory_Test
 */
class Dyna_Serviceability_Helper_Factory_Test extends TestCase
{
    public function getInstance()
    {
        return Mage::helper('dyna_serviceability/factory');
    }

    public function testClient()
    {
        $serviceabilityFactory = $this->getInstance();
        $this->assertInstanceOf(get_class(Mage::helper('dyna_serviceability/factory')), $serviceabilityFactory);
    }

}