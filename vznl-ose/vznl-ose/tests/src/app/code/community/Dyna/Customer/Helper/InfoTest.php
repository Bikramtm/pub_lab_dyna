<?php

use PHPUnit\Framework\TestCase;

/**
* Class Dyna_Customer_Helper_Info_Test
*/
class Dyna_Customer_Helper_Info_Test extends TestCase
{
    public function testCurrentCustomerHasMobile()
    {
    	$serviceArray = $this->getMockedCustomerStub();
    	$stub = $this->getMockedCustomerModel($serviceArray);
    	$result = $stub->currentCustomerHasMobile();
    	$this->assertTrue($result);
    }

    public function testCurrentCustomerHasMobileFalse()
    {
    	$serviceArray = $this->getMockedCustomerStub();
    	unset($serviceArray[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS]);
    	$stub = $this->getMockedCustomerModel($serviceArray);
    	$result = $stub->currentCustomerHasMobile();
    	$this->assertFalse($result);
    }

    public function testCurrentCustomerHasCable()
    {
    	$serviceArray = $this->getMockedCustomerStub();
    	$stub = $this->getMockedCustomerModel($serviceArray);
    	$result = $stub->currentCustomerHasCable();
    	$this->assertTrue($result);
    }

    public function testCurrentCustomerHasCableFalse()
    {
    	$serviceArray = $this->getMockedCustomerStub();
    	unset($serviceArray[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD]);
    	$stub = $this->getMockedCustomerModel($serviceArray);
    	$result = $stub->currentCustomerHasCable();
    	$this->assertFalse($result);
    }

    public function testCurrentCustomerHasFixed()
    {
    	$serviceArray = $this->getMockedCustomerStub();
    	$stub = $this->getMockedCustomerModel($serviceArray);
    	$result = $stub->currentCustomerHasFixed();
    	$this->assertTrue($result);
    }

    public function testCurrentCustomerHasFixedFalse()
    {
    	$serviceArray = $this->getMockedCustomerStub();
    	unset($serviceArray[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN]);
    	$stub = $this->getMockedCustomerModel($serviceArray);
    	$result = $stub->currentCustomerHasFixed();
    	$this->assertFalse($result);
    }

    /**
     * @param array
     * @return Dyna_Customer_Helper_Info
     */
    protected function getMockedCustomerModel($serviceArray = [])
    {
        $sessionMock = $this->getMockBuilder(Dyna_Customer_Model_Session::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getServiceCustomers'
            ])->getMock();
        $sessionMock->method('getServiceCustomers')
            ->willReturn($serviceArray);

        $mock = $this->getMockBuilder(Dyna_Customer_Helper_Info::class)
            ->setMethods([
                'getCustomerSession'
            ])->getMock();
        $mock->method('getCustomerSession')
            ->willReturn($sessionMock);

        return $mock;
    }

    protected function getMockedCustomerStub()
    {
        return array(
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS => '',
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD => '',
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN => ''
        );
    }
}