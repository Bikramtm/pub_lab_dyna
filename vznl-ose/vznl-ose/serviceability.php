<?php
header('Access-Control-Allow-Origin: *');
header("Content-type:application/json");

$response = array (
    'links' =>
        array (
            'self' => 'http://st2-telesales-vznl.dynacommercelab.com/api/v1/serviceability/check',
        ),
    'data' =>
        array (
            0 =>
                array (
                    'type' => 'serviceability',
                    'id' => 'cable-internet-phone',
                    'attributes' =>
                        array (
                            'category' => 'Cable Internet & Phone',
                            'availability' =>
                                array (
                                    'key' => 'available',
                                    'value' => true,
                                ),
                            'items' =>
                                array (
                                    0 =>
                                        array (
                                            'key' => 'Internet',
                                            'value' => true,
                                        ),
                                    1 =>
                                        array (
                                            'key' => 'MaxBandwidth',
                                            'value' => 600,
                                        ),
                                    2 =>
                                        array (
                                            'key' => 'AnalogTV',
                                            'value' => true,
                                        ),
                                    3 =>
                                        array (
                                            'key' => 'Additional Line',
                                            'value' => true,
                                        ),
                                    4 =>
                                        array (
                                            'key' => 'DTV',
                                            'value' => true,
                                        ),
                                    5 =>
                                        array (
                                            'key' => 'DTV one way',
                                            'value' => false,
                                        ),
                                    6 =>
                                        array (
                                            'key' => 'DTV two way',
                                            'value' => true,
                                        ),
                                    7 =>
                                        array (
                                            'key' => 'Telephony',
                                            'value' => true,
                                        ),
                                    8 =>
                                        array (
                                            'key' => 'Priority',
                                            'value' => true,
                                        ),
                                    9 =>
                                        array (
                                            'key' => 'VOIP',
                                            'value' => true,
                                        ),
                                    10 =>
                                        array (
                                            'key' => 'VOIP SIP',
                                            'value' => true,
                                        ),
                                    11 =>
                                        array (
                                            'key' => 'Horizon',
                                            'value' => true,
                                        ),
                                    12 =>
                                        array (
                                            'key' => 'Footprint',
                                            'value' => 'ZIGGO',
                                        )
                                ),
                        ),
                ),
        ),
);

echo json_encode($response);
