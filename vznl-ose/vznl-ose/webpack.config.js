const webpack = require('webpack');
const path = require('path');
const glob = require("glob");
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const omniusSkin = 'skin/frontend/omnius/';
const distFolder = omniusSkin + 'default/dist/';

const config = {
  entry : {
    app: glob.sync('./' + omniusSkin + 'default/css/style/*.css')
  },
  output : {
    filename: '[name].css',
    path: path.resolve(__dirname, distFolder, 'css')
  },
  devtool: process.env.NODE_ENV === 'dev' ? 'inline-source-map' : '',
  module: {
    rules: [{
      enforce: "pre",
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "eslint-loader",
      },{
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader',
          options: {
            sourceMap: process.env.NODE_ENV === 'dev' ? true : false,
            minimize: process.env.NODE_ENV === 'dev' ? false : true,
            url: false
          }
        }]
      })
    }, {
      test: /\.(woff|woff2|eot|ttf)$/,
      loader: 'url-loader?limit=100000&name=/assets/fonts/[name].[ext]'
    }, {
      test: /\.(png|jpg|gif|svg)$/,
      loader: 'url-loader?limit=100000&name=/assets/images/[name].[ext]'
    }]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '[name].css'
    }),
  ]
};

module.exports = config;
