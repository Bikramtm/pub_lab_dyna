#!/usr/bin/env bash

DIRECTORY=`dirname $0`
ROOT=${DIRECTORY}"/.."
cd ${ROOT}

poFileDir="./locales/"
conversionScript="../node_modules/i18next-conv/bin/"

for language in `ls ${poFileDir}`
do
  languageFiles="$poFileDir$language"

  for languageFile in `ls ${languageFiles}`
    do
       node ${conversionScript} -l ${language} -s ${languageFiles}/${languageFile} -t ../dist/locales/${language}/${languageFile%.*}".json"
    done
done
