import React from 'react';
import PropTypes from 'prop-types';
import AppContext from './AppContext';

export default class ConfigProvider extends React.Component {

  static propTypes = {
    children: PropTypes.node.isRequired
  };

  state = {
    isLoading: false,
    config: null,
    error: null
  };

  componentDidMount() {
    this.setState({ isLoading: true });

    fetch('/config/config.json', { credentials: 'same-origin' })
      .then(response => response.json())
      .then(data => {
        const customerSearchConfig = this.getCustomerSearchConfig(data);
        
        if (!customerSearchConfig) {
          return Promise.reject('Customer search config not exists, please check /config/config.json');
        }
        this.setState({ data, isLoading: false });
      })
      .catch(error => this.setState({ error, isLoading: false }));
  }

  getCustomerSearchConfig(config) {
    return config && config.customer && config.customer.apiUrl &&
      config.customer;
  }

  getCheckoutConfig(config) {
    return config && config.checkout && config.checkout.apiUrl &&
      config.checkout;
  }

  render() {
    const { data, error } = this.state;
    const customerSearchConfig = data && data.customer;

    if (error) {
      return <div>Reading config error: {error}</div>;
    }

    return (
      <AppContext.Provider
        value={{
          customerSearchConfig
        }}>
        {this.props.children}
      </AppContext.Provider>
    );
  }
}