import { createContext } from 'react';

export default createContext({
  customerSearchConfig: {
    apiUrl: 'dummy'
  }
});
