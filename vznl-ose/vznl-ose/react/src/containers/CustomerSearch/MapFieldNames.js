export default {  
  _accountNumber: {
    fieldName: 'accountNumber'
  },
  _orderNumbers: {
    fieldName: 'orderNumber'
  },
  postalCode: {
    fieldName: 'postalCode'
  },
  houseNumber: {
    fieldName: 'houseNumber'
  },
  email: {
    fieldName: 'email'
  },
  _firstName: {
    fieldName: 'initials'
  },
  _lastName: {
    fieldName: 'lastName'
  },
  _type: {
    fieldName: 'customerType'
  },
  number: {
    fieldName: 'phoneNumber'
  },
  _dateOfBirth: {
    fieldName: 'dateOfBirth'
  },
  _lineOfBusiness: {
    fieldName: 'adapter'
  },
  _party: {
    fieldName: '_party'
  },
  _contactDetails: {
    fieldName: '_contactDetails'
  },
  _chamberOfCommerce: {
    fieldName: 'chamberOfCommerce'
  }
};
