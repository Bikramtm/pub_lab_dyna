export default {
  mobile: 'vodafone_search_customer',
  fixed: 'ziggo_search_customer',
  other: 'general'
};
