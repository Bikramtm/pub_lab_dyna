/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */
/*
 * LINT is disabled because there are many flying variables that are initialized via jQuery in the window.
 * Due this reasons the vars are accesible but are not recognized by the linter in the React code
 */
/* eslint-disable */
import 'whatwg-fetch';

export default class Proceed {

  constructor() {
    this.customerData = {};
    this.typeSoho = null;
    this.customerInfoContainer = document.getElementById('customer-search-results');
    this.topAddressSearch = 'topaddressCheckForm';
    this.loadView = this.loadView.bind(this);
  }

  loadView(data, callback) {

    //Remove vnext popup
    jQuery('.vn-popup').removeClass('visible');
    if (data.error) {
      if(data.wrong_password) {
        showNotificationBar(Translator.translate('Incorrect customer password! Please perform an advanced search with a customer password'), 'error');
      } else {
        showNotificationBar(Translator.translate('Something went wrong! Customer could not be loaded. Please try again'), 'error');
      }
      setPageLoadingState(false);

      if (callback) {
        callback();
      }
      return;
    }

    let pan = data.customerData.pan;
    let no_ordering_allowed = data.customerData.no_ordering_allowed;
    let sell_fixed_to_blacklisted = data.customerData.sell_fixed_to_blacklisted;
    let message = Translator.translate('ORDER RESTRICTION! Placing fixed orders is not allowed due to an administrative order restriction. Please contact Billing & Collections.');

    if ((pan != null) && (no_ordering_allowed == true)) {
      if (sell_fixed_to_blacklisted == true) {
        showNotificationBar(Translator.translate(message), 'warning');
      } else {
        // disable fixed if permission not valid
        jQuery('#package-types').find('.requires-serviceability').removeClass('disabled-mandatory').addClass('disabled-mandatory');
        showNotificationBar(Translator.translate(message), 'error');
      }
      setTimeout(function () {
        hideNotificationBar();
      }, 10000);
    }

    if (data.warning) {
      showNotificationBar(data.message, 'error');
      // Set the proceed/link and proceed btns to show the warning in clicked
      let buttonElements = document.getElementsByClassName('potential-matches-header')[0];
      let proceedButton = buttonElements.querySelectorAll('button');
      for (let i=0; i < proceedButton.length; i++) {
        proceedButton[i].setAttribute('onclick', 'showNotificationBar(\'' + data.message + '\', \'error\')');
      }
      setTimeout(function () {
        hideNotificationBar();
      }, 10000);

      if (callback) {
        callback();
      }
      
      return;
    }

    address.resetServices();

    if (data.customerData) {
      this.customerData = data.customerData;
      customerDe.customerData = data.customerData;
      if (this.customerData.fixedHasInstalledBase != undefined && this.customerData.fixedHasInstalledBase == true) {
        jQuery('.select_fixed_package').removeClass('requires-serviceability').addClass('disabled');
      }
      let cartWasCleared = true;
      let leftDetailsSticker = document.getElementById('Handlebars-Template-Details-Customer'),
        leftDetailsStickerTemplate = (leftDetailsSticker.innerHTML) ? leftDetailsSticker.innerHTML : '';
      let leftCustomerInfoContainerParent = document.getElementsByClassName('col-left')[0];
      let leftCustomerInfoContainer = leftCustomerInfoContainerParent.querySelectorAll('.sticker')[0];
      // Parse the json with customer results in the handlebars template
      let template = Handlebars.compile(leftDetailsStickerTemplate),
        customersHTML = template(data.customerData);
      leftCustomerInfoContainer.innerHTML = customersHTML;

      // Check customer type, set switch accordingly and disable switch
      this.handleCustomerState();

      //disable check serviceability form until mode2 call has completed
      address.disableTopAddressCheckForm();

      //disable links that depend on the next calls
      LeftSidebar.setCustomerMode2ButtonsState(false);
      LeftSidebar.setCustomerHouseHoldButtonsState(false);

      /**
        * if the cart was previously empties => update the cart, update totals, destroy configurator
      */
      if (cartWasCleared !== undefined && cartWasCleared === true) {
        if (window['configurator']) {
          window['configurator'].destroy();
        }
        this.replaceHTMLWith('cart_totals', data.totals);
        let sideCart = document.querySelectorAll('.side-cart[data-package-id]');
        this.removeElement(sideCart, true);
        let noPackageZone = document.querySelectorAll('.no-package-zone')[0];
        this.removeClass(noPackageZone, 'hide');
        let packageTypes = document.getElementById('package-types');
        let packageTypesList = packageTypes.querySelectorAll('.btn');
        for (let j=0; j < packageTypesList.length; j++) {
          this.removeClass(packageTypesList[j], 'hide');
        }
        window.sidebar.handlePriceDisplayState();
        window.configurator && window.configurator.closeBundleDetailsModal();
      }

      LeftSidebar.setCustomerMode2ButtonsState(true);
      address.enableTopAddressCheckForm();
      if (!data.customerData.isProspect) {
        sessionStorage.setItem('permissionShowKias', data.customerData.permissionMessages.show_kias);
        sessionStorage.setItem('permissionShowFn', data.customerData.permissionMessages.show_fn);
        sessionStorage.setItem('permissionShowKd', data.customerData.permissionMessages.show_kd);

        let pType = document.getElementById('package-types');

        for (let key in data.customerData.has_open_orders) {
          let pTypes = pType.querySelectorAll('.btn-block.cursor[data-target="' + key + '"]');
          for (let k=0;k<pTypes.length;k++) {
            pTypes[k].setAttribute('data-has-open-order', String(data.customerData.has_open_orders[key]));
          }
        }
        let pWholeTypes = pType.querySelectorAll('.btn-block.cursor[data-has-open-order]');
        let ct = 0;
        for (let l=0;l<pWholeTypes.length;l++) {
          if (pWholeTypes[l].getAttribute('data-has-open-order') !== 'false') {
            ct++;
          }
        }
      }
      let leftCustomerInfoLoading = document.querySelector('#left-customer-info').querySelector('.loading-search');
      if (leftCustomerInfoLoading ) {
        this.addClass(leftCustomerInfoLoading[0], 'invisible');
      }
      // Preserve customer data

      let leftCustomerInfo = document.getElementById('customer-menu');
      if (data.customerData.products_count > 0) {
        let productSpanDiv = leftCustomerInfo.querySelector('a[data-toggle="products-content"]').parentNode;
        let productSpan = productSpanDiv.querySelectorAll('.number-of-circle')[0];
        productSpan.innerHTML = data.customerData.products_count;
        this.removeClass(productSpan, 'hidden');
      }
      if (data.customerData.orders_count > 0) {
        let orderSpanDiv = leftCustomerInfo.querySelector('a[data-toggle="orders-content"]').parentNode;
        let orderSpan = orderSpanDiv.querySelectorAll('.number-of-circle')[0];
        orderSpan.innerHTML = data.customerData.orders_count;
        this.removeClass(orderSpan, 'hidden');
      }

      // Check customer type, set switch accordingly and disable switch
      this.handleCustomerState();

      // Check if element is expanded and hide it for customer campaigns content to be visible
      // Check if element is expanded and hide it for customer campaigns content to be visible
      let enlargeSearch = document.getElementsByClassName('enlargeSearch')[0];
      if (this.hasClass(enlargeSearch, 'expanded')) {
        LeftSidebar.toggleLeftSideBar(false);
      }

      if(this.customerData.peal_party_subtype) {
        jQuery('#package-types').find('.requires-serviceability').removeClass('disabled-mandatory').addClass('disabled-mandatory');
      }

      let topAddressSearch = document.getElementById(this.topAddressSearch);
      let attrs = {};
      let createAddress = '';
      if (this.customerData.allow_ziggo) {
        if (this.customerData.service_address !== undefined && this.customerData.service_address && this.customerData.service_address.street && this.customerData.service_address.no) {
          if (this.customerData.service_address.id) {
            if (this.customerData.service_address.postal_code) {
              // Build new complete customer address element used by checkService function
              attrs = {
                'data-id': this.customerData.service_address.id,
                'data-street': this.customerData.service_address.street,
                'data-building-nr': this.customerData.service_address.no,
                'data-postalcode': this.customerData.service_address.postal_code,
                'data-city-name': this.customerData.service_address.city,
                'data-house-addition': this.customerData.service_address.house_addition,
                'data-is-customer': true
              };
              createAddress = document.createElement('div');
              this.setAttributes(createAddress, attrs);

              // Perform serviceAvailability call
              address.checkService(createAddress);
              // Filter available campaigns
              phone.filterCustomerCampaigns(this.customerData);
            }
          } else {
            if (this.customerData.service_address.postal_code) {
              // No service address id received, triggering validate address (see OMNVFDE-2444)
              document.getElementById('topBar.addressCheck.postcode').value = this.customerData.service_address.postal_code ? this.customerData.service_address.postal_code : '1';
              document.getElementById('topBar.addressCheck.city').value = this.customerData.service_address.city ? this.customerData.service_address.city : '';
              document.getElementById('topBar.addressCheck.street').value = this.customerData.service_address.street ? this.customerData.service_address.street : '';
              document.getElementById('topBar.addressCheck.houseNo').value = this.customerData.service_address.no ? this.customerData.service_address.no : '';
              document.getElementById('topBar.addressCheck.addition').value = this.customerData.service_address.house_addition ? this.customerData.service_address.house_addition : '';
              jQuery('.check-button').click();
            }
          }
        } else if (this.customerData.legal_address !== undefined && this.customerData.pan == null) {
          if (this.customerData.legal_address && this.customerData.legal_address.postal_code && this.customerData.legal_address.no) {
            // Build new complete customer address element used by validateaddress which will call checkService function
            document.getElementById('topBar.addressCheck.postcode').value = this.customerData.legal_address.postal_code;
            document.getElementById('topBar.addressCheck.street').value = this.customerData.legal_address.street;
            document.getElementById('topBar.addressCheck.houseNo').value = this.customerData.legal_address.no;
            document.getElementById('topBar.addressCheck.addition').value = this.customerData.legal_address.house_addition;
            document.getElementById('topBar.addressCheck.city').value = this.customerData.legal_address.city;

            jQuery('.check-button').click();

            // Filter available campaigns
            phone.filterCustomerCampaigns(this.customerData);
          } else {
            if (this.customerData.postal_code) {
              // No service address id received, triggering validate address (see OMNVFDE-2444)
              topAddressSearch.querySelector('input[name="postcode"]').value = this.customerData.postal_code ? this.customerData.postal_code : '1';
              topAddressSearch.querySelector('input[name="city"]').value = this.customerData.city ? this.customerData.city : '';
              topAddressSearch.querySelector('input[name="street"]').value = this.customerData.street ? this.customerData.street : '';
              topAddressSearch.querySelector('input[name="houseNo"]').value = this.customerData.no ? this.customerData.no : '';
              topAddressSearch.querySelector('input[name="addition"]').value = this.customerData.house_addition ? this.customerData.no : '';
              address.checkServiceabilityHeader('#' + this.topAddressSearch);
            }
          }
        } else {
          console.error('No service address or legal address received for current customer. Serviceability/Validate address not triggered.');
        }
      }
    }
    if (data.saveCartModal) {
      this.replaceHTMLWith('save-cart-modal', data.saveCartModal);
    }
    if (data.createWorkItemModal) {
      this.replaceHTMLWith('workitem_service_create_modal', data.createWorkItemModal);
    }

    setPageLoadingState(false);
  }

  handleCustomerState () {
    let toggleClass = '.soho-toggle';

    if (this.customerData['isSoho'] || this.customerData['PartyType'] === this.typeSoho) {
      ToggleBullet.switchOn(toggleClass);
    } else {
      ToggleBullet.switchOff(toggleClass);
    }
    ToggleBullet.disable(toggleClass);
  }

  fetchSecureRequest(url) {
    const post_key = document.getElementsByName('post_key');
    let request = new Request(url, {
      headers: new Headers({
        'Accept': '*/*',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'PostKey': post_key[0].defaultValue,
        'X-Requested-With': 'XMLHttpRequest'
      }),
      credentials: 'same-origin'
    });
    return request;
  }

  async fetchCustomer(customerAccount) {
    try {
      let response = await fetch(this.fetchSecureRequest('./customer/details/load'), {
      method: 'POST',
      credentials: "same-origin",
      body: JSON.stringify(customerAccount),
    });
      return response.json();
    } catch (e) {
      console.log('Request failed', e);
    }
  }

  resetCssOfLeftPanel(){
    const smallView = document.querySelectorAll('.smallView');
    const bigView = document.querySelectorAll('.bigView');

    if (smallView.length) {
      this.removeClass(smallView[0], 'smallView');
      this.addClass(smallView[0], 'bigView');

      if (bigView.length) {
        this.removeClass(bigView[0], 'bigView');
        this.addClass(bigView[0], 'smallView');
      }
    }
  }

  async loadCustomer(customerAccount, callback) {
    
    this.resetCssOfLeftPanel();

    const cartPackages = document.querySelectorAll('.cart_packages .side-cart');
    let packageAddedInCart = false;

    for (let cartPackage of cartPackages) {
      if (cartPackage.hasAttribute('data-package-id')) {
        packageAddedInCart = true;
        break;
      }
    }

    if (packageAddedInCart) {
      const post_key = document.getElementsByName('post_key');
      try {
        let response = await fetch(this.fetchSecureRequest('configurator/cart/empty'), {
          method: 'POST',
          credentials: 'same-origin',
          body: '',
        });
        let json = await response.text();
        const customerData = await this.fetchCustomer(customerAccount);
        this.loadView(customerData, callback);
      } catch (e) {
        console.log('Request failed', e);

        if (callback) {
          callback();
        }
      }
    } else {
      const customerData = await this.fetchCustomer(customerAccount);
      if (customerData.wrong_password) {
        if (callback) {
          callback(customerData);
        }
      } else {
        this.loadView(customerData, callback);
      }
    }
  }

  hasClass(target, className) {
    return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
  }

  removeClass(x, classname) {
    if (x.classList) {
      x.classList.remove(classname);
    } else {
      if (!x || !x.className) {
        return false;
      }
      let regexp = new RegExp('(^|\\s)' + classname + '(\\s|$)', 'g');
      x.className = x.className.replace(regexp, '$2');
    }
  }

  addClass(x, classname) {
    if (x.classList) {
      x.classList.add(classname);
    } else {
      let name = classname;
      const arr = x.className.split(' ');
      if (arr.indexOf(name) === -1) {
        x.className += ' ' + name;
      }
    }
  }

  removeElement(element, loop = false) {
    if (loop) {
      for (let i=0;i<element.length;i++) {
        if (element[i].innerHTML.length !== 0) {
          element[i].parentNode.removeChild(element[i]);
        }
      }
    } else {
      element.parentNode.removeChild(element);
    }
  }

  setAttributes(el, attrs) {
    for (let key in attrs) {
      el.setAttribute(key, attrs[key]);
    }
  }

  replaceHTMLWith(id, data) {
    let model = document.getElementById(id),
      modelParent = model.parentNode;
    let tempDiv = document.createElement('div');
    data = data.replace(/\n/g, '').trim();
    tempDiv.innerHTML = data;
    let input = tempDiv.childNodes[0];
    modelParent.replaceChild(input, model);
  }

  handleCustomerSearchErrors(errorObject) {
    const error = errorObject.error;
    let errorType = errorObject.type || "error";
    let errorMessage = Translator.translate("<b>ERROR!</b> Something went wrong while searching. Please try again");
    if (error && error["errors"]) {
      error["errors"].forEach(errorData => {
        if (errorData.status === 6) {
          errorType = "warning";
          errorMessage = Translator.translate(errorObject.message);
        }
      });
    }
    const moreInfo = Translator.translate("More info");
    let moreInfoDetails = '';
    if(typeof error["errors"][0].detail !== 'undefined') {
      moreInfoDetails = '<div class="btn" onclick="showModalError(\''+Translator.translate(error["errors"][0].detail)+'\',\''+moreInfo+'\')">'+moreInfo+'</div>'; 
    }
    showNotificationBar(errorMessage, errorType, moreInfoDetails);
  }

}
