/* eslint-disable quotes */
/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { I18n } from 'react-i18next';
import { Tab, Table, Popup, EmptyState } from '@omnius/react-ui-elements';
import _ from 'lodash';
import { MdRemoveRedEye, MdEdit, MdDelete } from 'react-icons/md';
import './MyOrders.scss';

class MyOrders extends Component {
  state = {
    orders: [],
    ufixorders: {},
    ubuyOrdersCount: 0,
    ufixOrdersCount: 0
  };

  componentDidMount = () => {
    this.fetchData().then(data => {
      const orders = data["body"]["buckets"];
      let ubuyOrdersCount = 0;
      Object.keys(orders).forEach(key => {
        const { data } = orders[key];
        const amount = data === undefined || data === null ? 0 : data.length;
        ubuyOrdersCount = ubuyOrdersCount + amount;
      });
      this.setState({
        orders: orders,
        ubuyOrdersCount: ubuyOrdersCount
      })
    });
    this.fetchFixData().then(data => {
      const ufixorders = data["body"]["buckets"];
      let ufixOrdersCount = 0;
      Object.keys(ufixorders).forEach(key => {
        const { data } = ufixorders[key];
        const amount = data === undefined || data === null ? 0 : data.length;
        ufixOrdersCount = ufixOrdersCount + amount;
      });
      this.setState({
        ufixorders: ufixorders,
        ufixOrdersCount: ufixOrdersCount
      })
    });
  };

  fetchData = async () => {
    try {
      const post_key = document.getElementsByName('post_key')[0].defaultValue;
      const url = '/dataimport/index/searchByCustomer';
      const headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'PostKey': post_key
      };
      const body = this.toUrlEncoded({
        'section': 'open-orders',
        'customerId': 1,
        'customerScreen': 1,
        'type': '',
        'page': 1,
        'cluster': '',
        'orderbyfield': 'id',
        'countQuery': 'false',
        'ordertype': 'desc',
        'resultsOnPage': '',
        'myOrders': true
      });
      const data = await fetch(url, {
        method: 'POST',
        headers,
        body,
        credentials: 'same-origin'
      });
      return data.json();
    } catch (err) {
      console.error(err);
    }
  };

  fetchFixData = async () => {
    try {
      const post_key = document.getElementsByName('post_key')[0].defaultValue;
      const url = '/dataimport/index/searchByCustomer';
      const headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'PostKey': post_key
      };
      const body = this.toUrlEncoded({
        'section': 'open-orders',
        'customerId': 1,
        'customerScreen': 1,
        'type': '',
        'page': 1,
        'cluster': 'oo-inlife',
        'orderbyfield': 'id',
        'countQuery': false,
        'ordertype': 'desc',
        'resultsOnPage': '',
        'myOrders': true
      });
      const data = await fetch(url, {
        method: 'POST',
        headers,
        body,
        credentials: 'same-origin'
      });
      return data.json();
    } catch (err) {
      console.error(err);
    }
  };

  toUrlEncoded = obj => Object.keys(obj).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(obj[k])).join('&');

  columnsUFIX = (t) => {
    return [
      {
        header: t('Order'),
        accessor: 'order'
      }, {
        header: t('Date'),
        accessor: 'date'
      }, {
        header: t('Order status'),
        accessor: 'orderStatus',
        Cell: this.StatusLabel
      }, {
        header: t('Details'),
        accessor: 'details'
      }
    ];
  }


  columnsUBUY = (t) => {
    return [
      {
        header: t('Order'),
        accessor: 'order'
      }, {
        header: t('Date'),
        accessor: 'date'
      }, {
        header: t('PhoneNumber'),
        accessor: 'phone_number' // should be salesLocation from PEAL
      }, {
        header: t('Mobile order status'),
        accessor: 'orderStatus',
        Cell: this.StatusLabel
      }, {
        header: t('Fixed order status'), // should be consolidatedStatus(?) from PEAL
        accessor: 'fixedOrderStatus',
        Cell: this.StatusLabel
      }
    ];
  }

  StatusLabel = props => <span className='table-order-status'>{props.value}</span>

  transformTableDataUBUY = (data, t) => {
    const array = [];
    Object.keys(data).forEach(key => {
      const category = _.get(data[key], 'data');

      if (category) {
        category.forEach(item => {
          const elementFound = _.find(array, element => {
            return element.data.order === item.number;
          });
          if (!elementFound) {
            array.push ({
              data: {
                order: item.number,
                date: item.date,
                orderStatus: item.order_Status.toUpperCase(),
                fixedOrderStatus: item.peal_order_status,
                details: 'item.details',
                phone_number: item.phone_number
              },
              contextMenu: [
                {
                  name: t('View'),
                  icon: <MdRemoveRedEye />,
                  callback: () => loadOrderDetails(item)
                }
              ]
            });
          }
        });
      }
    });
    // Sort orders list by order number
    array.sort(function(a, b) {
      return b.data.order - a.data.order;
    });
    return array;
  };

  transformTableDataUFIX = data => {
    const array = [];
    Object.keys(data).forEach(key => {
      const category = _.get(data[key], 'data');
      if (category)
        array.push(..._.get(data[key], 'data').map(item => {
          var curdate = new Date(item.created_date * 1000);
          return {
            data: {
              order: item.order_id,
              date: curdate.getDate() + "-" + (curdate.getMonth() + 1) + "-" + curdate.getFullYear(),
              orderStatus: item.order_status,
              details: item.order_description
            },
            children: () => (
              <div>
                Content
              </div>
            )
          }
        }))
    });
    return array;
  };

  rowClickAction = (value, event) => {
    let menuItemIndex = 0;
    jQuery(event.currentTarget).find('.context-menu-item').eq(menuItemIndex).trigger('click');
  };

  panes = (t) => {

    return [
      {
        menuItem: `${t('U-BUY orders')} (${this.state.ubuyOrdersCount !== 0 ? this.state.ubuyOrdersCount : 0})`,
        render: () => {
          return (
            this.state.ubuyOrdersCount !== 0 ? (
              <Table data={this.transformTableDataUBUY(this.state.orders, t)} columns={this.columnsUBUY(t)} onRowClick={this.rowClickAction} />
            ) : (
              <EmptyState title={t('No U-BUY orders')}>
                <span>
                  {t('If the customer has U-BUY orders, you\'ll see them here')}
                </span>
              </EmptyState>
            )
          )
        }
      },
      {
        menuItem: `${t('U-FIX orders')} (${this.state.ufixOrdersCount !== 0 ? this.state.ufixOrdersCount : 0})`,
        render: () => {
          return (
            this.state.ufixOrdersCount !== 0 ? (
              <Table data={this.transformTableDataUFIX(this.state.ufixorders)} columns={this.columnsUFIX(t)} onRowClick={this.rowClickAction} />
            ) : (
              <EmptyState title={t('No U-FIX orders')}>
                <span>
                  {t('If the customer has U-FIX orders, you\'ll see them here')}
                </span>
              </EmptyState>
            )
          )
        }
      }
    ];
  }

  render() {
    return (
      <I18n ns={['baseline']}>
        {t => (
          <Tab panes={this.panes(t)} />
        )}
      </I18n>

    );
  }
}

MyOrders.propTypes = {
};

export default MyOrders;
