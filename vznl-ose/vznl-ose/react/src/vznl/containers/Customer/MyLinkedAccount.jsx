/* eslint-disable quotes */
/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { I18n } from 'react-i18next';
import { Tab, Table, Popup, EmptyState, StackIndicator } from '@omnius/react-ui-elements';
import _ from 'lodash';
import { MdRemoveRedEye, MdEdit, MdDelete } from 'react-icons/md';

class MyLinkedAccount extends Component {
  state = {
    customer: null
  };

  componentDidMount = () => {
    fetch('dataimport/index/loadLinkedAccount', { credentials: 'same-origin' })
      .then((response) => {
        return response.json();
      })
      .then(data => {
        this.setState({
          customer: data
        });
      });
  };

  panes = [
    {
      render: () =>
        <Table data={this.transformTableData(this.state.customer)} columns={this.columns} />
    }
  ];

  columns = [
    {
      header: 'Account ID',
      accessor: 'customer_number'
    }, {
      header: 'Customer',
      accessor: 'lastname'
    }, {
      header: 'LoB',
      accessor: 'account_category',
      Cell: prop => {
        return <StackIndicator
          stacks={
            [{
              name: Translator.translate('mobile'),
              cssClass: "stack1",
              enabled: prop.value === 'mobile'
            },{
              name: Translator.translate('fixed'),
              cssClass: "stack2",
              enabled: prop.value === 'fixed'
            }]
          }
        />
      }
    }
  ];

  transformTableData = data => {
    const array = [];
    if (data) {
      Object.keys(data['body']['linked_accounts']).forEach(key => {
        var item = _.get(data["body"]["linked_accounts"],key);
        array.push({data:{
          customer_number: item.customer_number,
          lastname: item.lastname,
          account_category: item.account_category
        }});
      });
    }
    return array;
  };

  render() {
    const {customer} = this.state;
    return (
      (customer && customer.body.linked_accounts) ? <Tab panes={this.panes} /> :
        <EmptyState title={Translator.translate('No linked accounts')}>
          <span>
            {Translator.translate('If the customer has linked accounts, you\'ll see them here')}
          </span>
        </EmptyState>
    );
  }
}

MyLinkedAccount.propTypes = {
};

export default MyLinkedAccount;
