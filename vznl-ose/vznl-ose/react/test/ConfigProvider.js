'use strict';

import React from 'react';
import ConfigProvider from '../src/ConfigProvider';
import { mount } from 'enzyme';
import AppContext from '../src/AppContext';
// import sinon from 'sinon';
import './bootstrap';
import 'whatwg-fetch';

describe('/ConfigProvider', function () {
  let fetchStub: sinon.SinonStub;

  beforeEach(function () {
    fetchStub = sinon.stub(window, 'fetch');
    fetchStub.resolves(
      new window.Response(
        JSON.stringify({
          customer: {
            apiUrl: 'dummy'
          }
        }), {
          status: 200,
          headers: {
            'Content-type': 'application/json'
          }
        })
    );
  });

  afterEach(function () {
    fetchStub.restore();
  });

  // https://github.com/airbnb/enzyme/issues/1598
  it.skip('should render a child component', function () {
    const wrapper = mount(
      <ConfigProvider>
        <AppContext.Consumer>
          {() => <div className="test"></div>}
        </AppContext.Consumer>
      </ConfigProvider>
    );

    expect(wrapper.find('.test')).to.have.lengthOf(1);
  });
});