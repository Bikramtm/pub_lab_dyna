const path = require('path');
const webpack = require('webpack');

// Set node environment to testing
process.env.NODE_ENV = 'development';
process.env.NODE_ENV_TEST = true;

module.exports = function (config) {
  config.set({
    basePath: '',
    files: [
      '../../node_modules/babel-polyfill/dist/polyfill.js',
      '../test/**/*.js'
    ],
    preprocessors: {
      '../src/**/*.js': ['coverage'],
      '../test/**/*.js': ['webpack', 'sourcemap']
    },
    frameworks: ['mocha', 'chai'],
    reporters: ['mocha', 'coverage-istanbul'],
    coverageIstanbulReporter: {
      reports: ['lcov', 'text'],
      dir: path.join(__dirname, '../../reports/react/coverage'),
      fixWebpackSourcePaths: true,
      skipFilesWithNoCoverage: false,
      thresholds: {
        emitWarning: true,
        global: {
          statements: 50,
          lines: 50,
          branches: 50,
          functions: 50
        }
      }
    },
    browsers: [
      'PhantomJS'
    ],
    browserDisconnectTolerance: 2,
    browserNoActivityTimeout: 50000,
    webpack: {
      devtool: 'inline-source-map',
      module: {
        loaders: [{
          test: /\.scss$/,
          loaders: [ 'style-loader', 'css-loader', 'sass-loader' ]
        }, {
          test: /\.css$/,
          loader: 'style-loader!css-loader'
        }, {
          test: /\.(js|jsx)$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        }, {
          test: /\.(woff|woff2|eot|ttf|svg)$/,
          loader: 'url-loader?limit=100000&name=/assets/fonts/[name].[ext]'
        },{
          enforce: 'post',
          test: /\.(js|jsx)$/,
          include: /src/,
          exclude: /node_modules/,
          loader: 'istanbul-instrumenter-loader'
        }]
      },
      resolve: {
        alias: {
          src: path.resolve(__dirname, 'src/')
        },
        extensions: ['.js', '.jsx']
      },
      externals: {
        'react/addons': true,
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true
      },
      plugins: [
        new webpack.DefinePlugin({
          'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            NODE_ENV_TEST: JSON.stringify(process.env.NODE_ENV_TEST)
          }
        })
      ]
    },
    webpackMiddleware: {
      noInfo: true,
      stats: {
        chunks: false
      }
    },
    plugins: [
      require('karma-webpack'),
      require('karma-sourcemap-loader'),
      require('karma-mocha-reporter'),
      require('karma-coverage'),
      require('karma-phantomjs-launcher'),
      require('karma-mocha'),
      require('karma-chai'),
      require('karma-coverage-istanbul-reporter'),
      require('istanbul-instrumenter-loader')
    ]
  });
};
